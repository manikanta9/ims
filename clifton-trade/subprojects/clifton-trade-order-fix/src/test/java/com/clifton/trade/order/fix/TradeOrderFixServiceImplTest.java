package com.clifton.trade.order.fix;

import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorImpl;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeDestinationBuilder;
import com.clifton.trade.builder.TradeTypeBuilder;
import com.clifton.trade.destination.TradeDestinationMappingBuilder;
import com.clifton.trade.group.TradeGroupBuilder;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatusBuilder;
import com.clifton.trade.order.TradeOrderTypeBuilder;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatusBuilder;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessorImpl;
import com.clifton.trade.order.workflow.TradeOrderWorkflowServiceImpl;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.definition.WorkflowStatusBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


@ExtendWith(MockitoExtension.class)
public class TradeOrderFixServiceImplTest {

	@InjectMocks
	TradeOrderFixServiceImpl tradeOrderFixService;

	@InjectMocks
	TradeOrderAllocationProcessorImpl tradeOrderAllocationProcessor;

	@InjectMocks
	InvestmentCalculatorImpl investmentCalculator;

	@Mock
	private SecurityAuthorizationService securityAuthorizationService;

	@Mock
	private TradeOrderService tradeOrderService;

	@Mock(lenient = true)
	private TradeService tradeService;

	@Mock
	private TradeOrderWorkflowServiceImpl tradeOrderWorkflowService;

	private Trade trade;
	private TradeOrder tradeOrder;


	@BeforeEach
	public void before() {
		this.tradeOrderFixService.setTradeOrderAllocationProcessor(this.tradeOrderAllocationProcessor);

		// setup trade
		this.trade = new Trade();
		this.trade.setId(587781);
		this.trade.setTradeType(TradeTypeBuilder.createFutures().toTradeType());
		this.trade.setExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany());
		this.trade.setPayingSecurity(InvestmentSecurityBuilder.newUSD());
		this.trade.setInvestmentSecurity(InvestmentSecurityBuilder.createEsu6().toInvestmentSecurity());
		this.trade.setTradeDate(DateUtils.toDate("08/22/2016"));
		this.trade.setSettlementDate(DateUtils.toDate("08/22/2016"));
		this.trade.setQuantityIntended(BigDecimal.valueOf(210.0000000000));
		this.trade.setExpectedUnitPrice(BigDecimal.valueOf(2181.700000000000000));
		this.trade.setBuy(true);
		this.trade.setWorkflowState(WorkflowStateBuilder.createExecution().toWorkflowState());
		this.trade.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		this.trade.setClientInvestmentAccount(InvestmentAccountBuilder.createTestAccount2().toInvestmentAccount());
		this.trade.setHoldingInvestmentAccount(InvestmentAccountBuilder.createTestHoldingAccount1().toInvestmentAccount());
		this.trade.setAverageUnitPrice(BigDecimal.valueOf(2179.334134000000000));
		this.trade.setFeeAmount(BigDecimal.valueOf(0.00));
		this.trade.setAccountingNotional(BigDecimal.valueOf(22665074.99));
		this.trade.setOriginalFace(BigDecimal.valueOf(210.00));
		this.trade.setCurrentFactor(BigDecimal.valueOf(1.0000000000));
		this.trade.setNotionalMultiplier(BigDecimal.valueOf(1.0000000000));
		this.trade.setTradeDestination(TradeDestinationBuilder.createRediTicket().toTradeDestination());
		this.trade.setTradeGroup(TradeGroupBuilder.createPortfolioRun().toTradeGroup());

		this.trade.getInvestmentSecurity().setInstrument(InvestmentInstrumentBuilder.createSandPMiniFutures().toInvestmentInstrument());
		this.trade.getInvestmentSecurity().getInstrument().setTradingCurrency(InvestmentSecurityBuilder.newUSD());
		this.trade.getInvestmentSecurity().getInstrument().getTradingCurrency().setInstrument(InvestmentInstrumentBuilder.createUSDollar().toInvestmentInstrument());

		List<Trade> trades = new ArrayList<>();
		trades.add(this.trade);

		// setup trade order
		this.tradeOrder = new TradeOrder();
		this.tradeOrder.setId(196466);
		this.tradeOrder.setStatus(TradeOrderStatusBuilder.createPartiallyFilledCanceled().toTradeOrderStatus());
		this.tradeOrder.setInvestmentSecurity(InvestmentSecurityBuilder.createEsu6().toInvestmentSecurity());
		this.tradeOrder.setQuantity(BigDecimal.valueOf(210.0000000000));
		this.tradeOrder.setQuantityFilled(BigDecimal.valueOf(208.0000000000));
		this.tradeOrder.setAveragePrice(BigDecimal.valueOf(2179.334134000000000));
		this.tradeOrder.setBuy(true);
		this.tradeOrder.setDestinationMapping(TradeDestinationMappingBuilder.createGoldmanFutures().toTradeDestinationMapping());
		this.tradeOrder.setTradeDate(DateUtils.toDate("08/22/2016"));
		this.tradeOrder.setExternalIdentifier(388166);
		this.tradeOrder.setType(TradeOrderTypeBuilder.createExecuteSeparately().toTradeOrderType());
		this.tradeOrder.setAllocationStatus(TradeOrderAllocationStatusBuilder.createCompleteNoAllocationReport().toTradeOrderAllocationStatus());
		this.tradeOrder.setActiveOrder(true);
		this.tradeOrder.setTradeList(trades);

		this.trade.setOrderIdentifier(this.tradeOrder.getId());

		// mock service methods
		Mockito.when(this.tradeOrderService.getTradeOrder(1)).thenReturn(this.tradeOrder);
		Mockito.when(this.tradeService.getTradeFillListByTrade(this.trade.getId())).thenReturn(this.trade.getTradeFillList());
		Mockito.when(this.tradeOrderAllocationProcessor.getTradeService().getTrade(this.trade.getId())).thenReturn(this.trade);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void bookTradeOrderFixListWithAveragePrice_partial() {
		// only accept partial_cancelled
		this.tradeOrder.setStatus(TradeOrderStatusBuilder.createPartiallyFilled().toTradeOrderStatus());
		Assertions.assertThrows(ValidationException.class, () ->
				this.tradeOrderFixService.bookTradeOrderFixListWithAveragePrice(new Integer[]{1}));
	}


	@Test
	public void bookTradeOrderFixListWithAveragePrice_partial_filled() {
		MatcherAssert.assertThat("Quantity filled [208] for the order should NOT be the same as the trade quantity [210].", this.trade.getQuantityIntended(), IsNot.not(IsEqual.equalTo(this.tradeOrder.getQuantityFilled())));
		this.tradeOrder.setStatus(TradeOrderStatusBuilder.createFilled().toTradeOrderStatus());
		Assertions.assertThrows(ValidationException.class, () ->
				this.tradeOrderFixService.bookTradeOrderFixListWithAveragePrice(new Integer[]{1}));
	}


	@Test
	public void bookTradeOrderFixListWithAveragePrice_filled() {
		this.tradeOrder.setQuantityFilled(BigDecimal.valueOf(210.0000000000));
		MatcherAssert.assertThat("Quantity filled [210] for the order should be the same as the trade quantity [210].", this.trade.getQuantityIntended(), IsEqual.equalTo(this.tradeOrder.getQuantityFilled()));
		this.tradeOrder.setStatus(TradeOrderStatusBuilder.createFilled().toTradeOrderStatus());
		this.tradeOrderFixService.bookTradeOrderFixListWithAveragePrice(new Integer[]{1});

		Assertions.assertFalse(this.trade.getTradeFillList().isEmpty(), "There should be trade fills created");
		Assertions.assertEquals(1, this.trade.getTradeFillList().size(), "Should be a single trade fill created.");

		// should have updated the workflow state to executed.
		// the booked workflow action is not triggered during this test.
		Mockito.verify(this.tradeOrderWorkflowService, Mockito.times(1)).setTradeWorkflowState(this.trade, TradeService.TRADE_EXECUTED_STATE_NAME);

		TradeFill tradeFill = this.trade.getTradeFillList().get(0);
		Assertions.assertEquals(this.trade.getId(), tradeFill.getTrade().getId());
		MatcherAssert.assertThat(this.trade.getQuantityIntended(), IsEqual.equalTo(tradeFill.getQuantity()));
		MatcherAssert.assertThat(this.tradeOrder.getAveragePrice(), IsEqual.equalTo(tradeFill.getNotionalUnitPrice()));
	}


	@Test
	public void bookTradeOrderFixListWithAveragePrice_partial_cancelled() {
		Assertions.assertNull(this.trade.getTradeFillList(), "There should be no trade fills initially");
		MatcherAssert.assertThat("Trade quantity intended [210] should be the same as trade order quantity [210]", this.trade.getQuantityIntended(), IsEqual.equalTo(this.tradeOrder.getQuantity()));
		MatcherAssert.assertThat("Quantity filled [208] for the order should NOT be the same as the trade quantity [210].", this.trade.getQuantityIntended(), IsNot.not(IsEqual.equalTo(this.tradeOrder.getQuantityFilled())));
		Assertions.assertNotEquals(TradeService.TRADE_BOOKED_STATE_NAME, this.trade.getWorkflowState().getName(), "The trade cannot be booked.");

		// do the actual test method
		this.tradeOrderFixService.bookTradeOrderFixListWithAveragePrice(new Integer[]{1});

		Assertions.assertFalse(this.trade.getTradeFillList().isEmpty(), "There should be trade fills created");
		Assertions.assertEquals(1, this.trade.getTradeFillList().size(), "Should be a single trade fill created.");

		// should have updated the workflow state to executed.
		// the booked workflow action is not triggered during this test.
		Mockito.verify(this.tradeOrderWorkflowService, Mockito.times(1)).setTradeWorkflowState(this.trade, TradeService.TRADE_EXECUTED_STATE_NAME);

		TradeFill tradeFill = this.trade.getTradeFillList().get(0);

		Assertions.assertEquals(this.trade.getId(), tradeFill.getTrade().getId());
		MatcherAssert.assertThat(this.trade.getQuantityIntended(), IsEqual.equalTo(tradeFill.getQuantity()));
		MatcherAssert.assertThat(this.tradeOrder.getQuantityFilled(), IsEqual.equalTo(tradeFill.getQuantity()));
		MatcherAssert.assertThat(BigDecimal.valueOf(208.0000000000), IsEqual.equalTo(tradeFill.getQuantity()));
		MatcherAssert.assertThat(this.tradeOrder.getAveragePrice(), IsEqual.equalTo(tradeFill.getNotionalUnitPrice()));

		BigDecimal notional = this.investmentCalculator.calculateNotional(this.trade.getInvestmentSecurity(), tradeFill.getNotionalUnitPrice(), BigDecimal.valueOf(208.0000000000), this.trade.getNotionalMultiplier());
		MatcherAssert.assertThat(notional, IsEqual.equalTo(this.trade.getAccountingNotional()));
	}


	@Test
	public void bookTradeOrderFixListWithAveragePrice_null_allocation_status() {
		this.tradeOrder.setAllocationStatus(null);
		// do the actual test method
		Assertions.assertDoesNotThrow(() -> this.tradeOrderFixService.bookTradeOrderFixListWithAveragePrice(new Integer[]{1}));
	}


	@Test
	public void bookTradeOrderFixListWithAveragePrice_rejected_allocation_status() {
		this.tradeOrder.setAllocationStatus(TradeOrderAllocationStatusBuilder.createRejectedAccountLevel().toTradeOrderAllocationStatus());
		// do the actual test method
		Assertions.assertThrows(ValidationException.class, () -> this.tradeOrderFixService.bookTradeOrderFixListWithAveragePrice(new Integer[]{1}));
	}
}
