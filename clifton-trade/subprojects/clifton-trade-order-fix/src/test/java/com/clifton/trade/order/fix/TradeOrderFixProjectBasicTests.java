package com.clifton.trade.order.fix;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class TradeOrderFixProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "trade-order-fix";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.beans.BeansException");
		imports.add("org.springframework.core.Ordered");
	}


	@Override
	protected void configureApprovedTestClassImports(@SuppressWarnings("unused") List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("com.thoughtworks.xstream.XStream");
	}
}
