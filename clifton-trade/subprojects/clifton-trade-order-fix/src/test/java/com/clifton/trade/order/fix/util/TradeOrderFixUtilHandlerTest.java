package com.clifton.trade.order.fix.util;

import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityBuilder;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixPartyBuilder;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.mapping.InvestmentAccountMapping;
import com.clifton.investment.account.mapping.InvestmentAccountMappingService;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.setup.InvestmentInstrumentHierarchyBuilder;
import com.clifton.investment.setup.InvestmentTypeBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationMappingBuilder;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class TradeOrderFixUtilHandlerTest {

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private TradeOrderFixUtilHandler tradeOrderFixUtilHandler;

	@Resource
	private InvestmentAccountMappingService investmentAccountMappingService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetTradeDestinationMappingFromFixPartyList() {
		// entity setup
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withInvestmentSecurity(
						InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
								.toInvestmentSecurity()
				)
				.withTradeDate(DateUtils.toDate("12/31/16"))
				.toTradeOrder();
		FixEntity fixEntity = FixEntityBuilder.createEmptyFixEntity()
				.toFixEntity();
		FixParty fixParty = FixPartyBuilder.createEmptyFixParty()
				.withPartyId("MS")
				.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
				.withPartyRole(PartyRoles.EXECUTING_FIRM)
				.toFixParty();

		List<FixParty> fixPartyList = Collections.singletonList(fixParty);
		TradeDestinationMappingAccountRelationshipCommand command = new TradeDestinationMappingAccountRelationshipCommand(
				tradeOrder
				, fixEntity
				, fixPartyList
				, true
		);

		TradeDestinationMapping tradeDestinationMapping = this.tradeDestinationService.getTradeDestinationMapping(378);
		Assertions.assertNotNull(tradeDestinationMapping);
		Assertions.assertFalse(tradeDestinationMapping.getMappingConfiguration().isSendRestrictiveBrokerList());
		tradeOrder.setDestinationMapping(tradeDestinationMapping);

		// run
		TradeDestinationMapping result = this.tradeOrderFixUtilHandler.getTradeDestinationMappingFromFixPartyList(command);

		// assert results
		Assertions.assertNotNull(result);
	}


	@Test
	// todo this is the same test as testGetTradeDestinationMappingFromFixPartyList
	public void testGetTradeDestinationMappingByAccountRelationship() {
		// build entities
		InvestmentSecurity investmentSecurity = InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
				.withInstrument(
						InvestmentInstrumentBuilder.createEmptyInvestmentInstrument()
								.withHierarchy(
										InvestmentInstrumentHierarchyBuilder.createEmptyInvestmentInstrumentHierarchy()
												.withInvestmentType(
														InvestmentTypeBuilder.createEmptyInvestmentType()
																.toInvestmentType()
												)
												.toInvestmentInstrumentHierarchy()
								)
								.toInvestmentInstrument()
				)
				.toInvestmentSecurity();
		Trade trade = TradeBuilder.createEmptyTrade()
				.withInvestmentSecurity(
						investmentSecurity
				)
				.withClientInvestmentAccount(
						InvestmentAccountBuilder.createEmptyInvestmentAccount()
								.withId(222)
								.toInvestmentAccount()
				)
				.build();
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withInvestmentSecurity(
						investmentSecurity
				)
				.withTradeDate(DateUtils.toDate("12/31/16"))
				.withTradeList(
						Collections.singletonList(
								trade
						)
				)
				.toTradeOrder();


		FixEntity fixEntity = FixEntityBuilder.createEmptyFixEntity().toFixEntity();
		FixParty fixParty = FixPartyBuilder.createEmptyFixParty()
				.withPartyId("MS")
				.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
				.withPartyRole(PartyRoles.EXECUTING_FIRM)
				.toFixParty();
		// setup entities
		TradeDestinationMappingAccountRelationshipCommand command = new TradeDestinationMappingAccountRelationshipCommand(
				tradeOrder
				, fixEntity
				, Collections.singletonList(fixParty)
				, false
		);

		TradeDestinationMapping tradeDestinationMapping = this.tradeDestinationService.getTradeDestinationMapping(377);
		Assertions.assertNotNull(tradeDestinationMapping);
		Assertions.assertFalse(tradeDestinationMapping.getMappingConfiguration().isSendRestrictiveBrokerList());
		tradeOrder.setDestinationMapping(tradeDestinationMapping);

		// run
		TradeDestinationMapping result = this.tradeOrderFixUtilHandler.getTradeDestinationMappingFromFixPartyList(command);

		// assert results
		Assertions.assertNotNull(result);
	}


	@Test
	public void testGetFixPartyListRediMorganForward() {
		TradeDestinationMapping tradeDestinationMapping = this.tradeDestinationService.getTradeDestinationMapping(379);
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Collections.singletonList(
						TradeBuilder.createEmptyTrade()
								.withInvestmentSecurity(
										InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
												.toInvestmentSecurity()
								)
								.withTradeType(tradeDestinationMapping.getTradeType())
								.build()
						)
				)
				.toTradeOrder();
		// Destination mapping does NOT have:
		// -> restrictive broker list
		// -> executing broker
		// -> override broker code
		Assertions.assertNotNull(tradeDestinationMapping);
		Assertions.assertFalse(tradeDestinationMapping.getMappingConfiguration().isSendRestrictiveBrokerList());
		tradeOrder.setDestinationMapping(tradeDestinationMapping);

		// run
		List<FixParty> partyList = this.tradeOrderFixUtilHandler.getFixPartyList(tradeOrder);

		// assert results
		Assertions.assertEquals(1, partyList.size());
		MatcherAssert.assertThat(partyList.get(0).getPartyId(), IsEqual.equalTo("MS"));
		MatcherAssert.assertThat(partyList.get(0).getPartyIdSource(), IsEqual.equalTo(PartyIdSources.PROPRIETARY_CUSTOM_CODE));
		MatcherAssert.assertThat(partyList.get(0).getPartyRole(), IsEqual.equalTo(PartyRoles.EXECUTING_FIRM));
	}


	@Test
	public void testGetFixPartyListBloombergCDS() {
		TradeDestinationMapping tradeDestinationMapping = this.tradeDestinationService.getTradeDestinationMapping(380);
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Collections.singletonList(
						TradeBuilder.createEmptyTrade()
								.withInvestmentSecurity(
										InvestmentSecurityBuilder.newCDS("XY3001J23U0500XX")
												.build()

								)
								.withTradeType(tradeDestinationMapping.getTradeType())
								.build()
						)
				)
				.toTradeOrder();
		InvestmentInstrument instrument = tradeOrder.getTradeList().get(0).getInvestmentSecurity().getInstrument();
		InvestmentExchange investmentExchange = new InvestmentExchange();
		investmentExchange.setExchangeCode("ICE");
		instrument.setExchange(investmentExchange);
		// Destination mapping does NOT have:
		// -> restrictive broker list
		// -> executing broker
		// -> override broker code
		Assertions.assertNotNull(tradeDestinationMapping);
		// NOTE: for this mapping on PROD isSendRestrictiveBrokerList=true, for the sake of this test, it's not needed
		Assertions.assertFalse(tradeDestinationMapping.getMappingConfiguration().isSendRestrictiveBrokerList());
		tradeOrder.setDestinationMapping(tradeDestinationMapping);

		// run
		List<FixParty> partyList = this.tradeOrderFixUtilHandler.getFixPartyList(tradeOrder);

		// assert results
		Assertions.assertEquals(1, partyList.size());
		MatcherAssert.assertThat(partyList.get(0).getPartyId(), IsEqual.equalTo("ICE"));
		MatcherAssert.assertThat(partyList.get(0).getPartyIdSource(), IsEqual.equalTo(PartyIdSources.PROPRIETARY_CUSTOM_CODE));
		MatcherAssert.assertThat(partyList.get(0).getPartyRole(), IsEqual.equalTo(PartyRoles.CLEARING_ORGANIZATION));
	}


	@Test
	public void testGetFixPartyListRediFuturesCiti() {
		TradeDestinationMapping tradeDestinationMapping = this.tradeDestinationService.getTradeDestinationMapping(174);

		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Collections.singletonList(
						TradeBuilder.createEmptyTrade()
								.withInvestmentSecurity(
										InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
												.toInvestmentSecurity()
								)
								.withTradeType(tradeDestinationMapping.getTradeType())
								.build()
						)
				)
				.toTradeOrder();
		// Destination mapping does NOT have:
		// -> restrictive broker list
		// -> executing broker
		// -> override broker code
		Assertions.assertNotNull(tradeDestinationMapping);
		Assertions.assertFalse(tradeDestinationMapping.getMappingConfiguration().isSendRestrictiveBrokerList());
		tradeOrder.setDestinationMapping(tradeDestinationMapping);

		// run
		List<FixParty> partyList = this.tradeOrderFixUtilHandler.getFixPartyList(tradeOrder);

		// assert results
		Assertions.assertEquals(1, partyList.size());
		MatcherAssert.assertThat(partyList.get(0).getPartyId(), IsEqual.equalTo("CITI"));
		MatcherAssert.assertThat(partyList.get(0).getPartyIdSource(), IsEqual.equalTo(PartyIdSources.PROPRIETARY_CUSTOM_CODE));
		MatcherAssert.assertThat(partyList.get(0).getPartyRole(), IsEqual.equalTo(PartyRoles.EXECUTING_FIRM));
	}


	@Test
	public void testGetFixPartyListRediOptionsGs() {
		TradeDestinationMapping tradeDestinationMapping = this.tradeDestinationService.getTradeDestinationMapping(66);
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Collections.singletonList(
						TradeBuilder.createEmptyTrade()
								.withInvestmentSecurity(
										InvestmentSecurityBuilder.createEmptyInvestmentSecurity()
												.toInvestmentSecurity()
								)
								.withTradeType(tradeDestinationMapping.getTradeType())
								.build()
						)
				)
				.toTradeOrder();
		// Destination mapping does NOT have:
		// -> restrictive broker list
		// -> executing broker
		// -> override broker code
		Assertions.assertNotNull(tradeDestinationMapping);
		Assertions.assertFalse(tradeDestinationMapping.getMappingConfiguration().isSendRestrictiveBrokerList());
		tradeOrder.setDestinationMapping(tradeDestinationMapping);

		// run
		List<FixParty> partyList = this.tradeOrderFixUtilHandler.getFixPartyList(tradeOrder);

		// assert results
		Assertions.assertEquals(1, partyList.size());
		MatcherAssert.assertThat(partyList.get(0).getPartyId(), IsEqual.equalTo("GS"));
		MatcherAssert.assertThat(partyList.get(0).getPartyIdSource(), IsEqual.equalTo(PartyIdSources.PROPRIETARY_CUSTOM_CODE));
		MatcherAssert.assertThat(partyList.get(0).getPartyRole(), IsEqual.equalTo(PartyRoles.EXECUTING_FIRM));
	}


	// getFixSingleOrderAllocationAccountNumber
	@Test
	public void testMultiTradeMappedMismatchTradeDestination() {
		TradeDestinationMapping tradeDestinationMapping = TradeDestinationMappingBuilder
				.createEmptyTradeDestinationBuilder()
				.withOrderAccountMappingPurpose(this.investmentAccountMappingService.getInvestmentAccountMappingPurpose((short) 1))
				.toTradeDestinationMapping();
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Arrays.asList(
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(1))
								.build(),
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(2))
								.build()
						)
				)
				.withDestinationMapping(tradeDestinationMapping)
				.toTradeOrder();
		String accountNumber = this.tradeOrderFixUtilHandler.getFixSingleOrderAllocationAccountNumber(tradeOrder);
		// two mappings with different numbers
		// more than one investment holding account
		// default to the destination mapping
		MatcherAssert.assertThat(accountNumber, IsEqual.equalTo(tradeDestinationMapping.getFixPostTradeAllocationTopAccountNumber()));
	}


	// getFixSingleOrderAllocationAccountNumber
	@Test
	public void testMultiTradeMappedInvestmentAccount() {
		TradeDestinationMapping tradeDestinationMapping = TradeDestinationMappingBuilder
				.createEmptyTradeDestinationBuilder()
				.withOrderAccountMappingPurpose(this.investmentAccountMappingService.getInvestmentAccountMappingPurpose((short) 1))
				.toTradeDestinationMapping();
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Arrays.asList(
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(1))
								.build(),
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(1))
								.build()
						)
				)
				.withDestinationMapping(tradeDestinationMapping)
				.toTradeOrder();
		String accountNumber = this.tradeOrderFixUtilHandler.getFixSingleOrderAllocationAccountNumber(tradeOrder);
		// one mapping
		// one investment holding account
		// use mapping value
		InvestmentAccountMapping mapping = this.investmentAccountMappingService.getInvestmentAccountMappingByAccountNumberAndPurpose(
				tradeOrder.getTradeList().get(0).getHoldingInvestmentAccount(),
				tradeDestinationMapping.getOrderAccountMappingPurpose()
		);
		MatcherAssert.assertThat(accountNumber, IsEqual.equalTo(mapping.getFieldValue()));
	}


	// getFixSingleOrderAllocationAccountNumber
	@Test
	public void testMultiTradeMapping() {
		TradeDestinationMapping tradeDestinationMapping = TradeDestinationMappingBuilder
				.createEmptyTradeDestinationBuilder()
				.withOrderAccountMappingPurpose(this.investmentAccountMappingService.getInvestmentAccountMappingPurpose((short) 1))
				.toTradeDestinationMapping();
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Arrays.asList(
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(6))
								.build(),
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(7))
								.build()
						)
				)
				.withDestinationMapping(tradeDestinationMapping)
				.toTradeOrder();
		String accountNumber = this.tradeOrderFixUtilHandler.getFixSingleOrderAllocationAccountNumber(tradeOrder);
		// two mappings with same numbers
		// more than one investment holding account
		// use the mapping value
		InvestmentAccountMapping mapping = this.investmentAccountMappingService.getInvestmentAccountMappingByAccountNumberAndPurpose(
				tradeOrder.getTradeList().get(0).getHoldingInvestmentAccount(),
				tradeDestinationMapping.getOrderAccountMappingPurpose()
		);
		MatcherAssert.assertThat(accountNumber, IsEqual.equalTo(mapping.getFieldValue()));
	}


	// getFixSingleOrderAllocationAccountNumber
	@Test
	public void testMultiTradeAccount() {
		TradeDestinationMapping tradeDestinationMapping = TradeDestinationMappingBuilder
				.createEmptyTradeDestinationBuilder()
				.withOrderAccountMappingPurpose(this.investmentAccountMappingService.getInvestmentAccountMappingPurpose((short) 1))
				.toTradeDestinationMapping();
		TradeOrder tradeOrder = TradeOrderBuilder.createEmptyTradeOrder()
				.withTradeList(Arrays.asList(
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(8))
								.build(),
						TradeBuilder.createEmptyTrade()
								.forHoldingAccount(this.investmentAccountService.getInvestmentAccount(8))
								.build()
						)
				)
				.withDestinationMapping(tradeDestinationMapping)
				.toTradeOrder();
		String accountNumber = this.tradeOrderFixUtilHandler.getFixSingleOrderAllocationAccountNumber(tradeOrder);
		// no mappings
		// one investment holding account
		// use the account value
		MatcherAssert.assertThat(accountNumber, IsEqual.equalTo(this.investmentAccountService.getInvestmentAccount(8).getNumber()));
	}
}
