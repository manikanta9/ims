package com.clifton.trade.order.fix.order.allocation.matcher;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class TradeOrderFixAllocationMatcherTests {

	@Test
	public void test() {
		TradeOrderFixAllocationMatcher matcher = new TradeOrderFixAllocationMatcher();

		List<FixAllocationDetail> internalList = new ArrayList<>();
		List<FixExecutionReport> externalList = new ArrayList<>();

		internalList.add(createInternal("260-71100", BigDecimal.valueOf(5), new BigDecimal("1164.05909091")));
		internalList.add(createInternal("157-15447", BigDecimal.valueOf(17), new BigDecimal("1164.05909091")));

		externalList.add(createExternal("MIRAC837420140123", new BigDecimal("1.00"), new BigDecimal("1164.20"), new BigDecimal("1164.05909091"), (long) 10));
		externalList.add(createExternal("MIRAC837220140123", new BigDecimal("1.00"), new BigDecimal("1164.30"), new BigDecimal("1164.05238095"), (long) 11));
		externalList.add(createExternal("MIRAC837020140123", new BigDecimal("1.00"), new BigDecimal("1164.30"), new BigDecimal("1164.04000000"), (long) 12));
		externalList.add(createExternal("MIRAC836420140123", new BigDecimal("1.00"), new BigDecimal("1164.40"), new BigDecimal("1164.02631579"), (long) 13));
		externalList.add(createExternal("MIRAC836220140123", new BigDecimal("1.00"), new BigDecimal("1164.30"), new BigDecimal("1164.00555556"), (long) 14));
		externalList.add(createExternal("MIRAC836020140123", new BigDecimal("1.00"), new BigDecimal("1164.20"), new BigDecimal("1163.98823529"), (long) 15));
		externalList.add(createExternal("MIRAC835820140123", new BigDecimal("1.00"), new BigDecimal("1164.20"), new BigDecimal("1163.97500000"), (long) 16));
		externalList.add(createExternal("MIRAC835520140123", new BigDecimal("1.00"), new BigDecimal("1164.20"), new BigDecimal("1163.96000000"), (long) 17));
		externalList.add(createExternal("MIRAC835320140123", new BigDecimal("1.00"), new BigDecimal("1164.10"), new BigDecimal("1163.94285714"), (long) 18));
		externalList.add(createExternal("MIRAC835120140123", new BigDecimal("1.00"), new BigDecimal("1164.00"), new BigDecimal("1163.93076923"), (long) 19));
		externalList.add(createExternal("MIRAC834820140123", new BigDecimal("1.00"), new BigDecimal("1164.10"), new BigDecimal("1163.92500000"), (long) 20));
		externalList.add(createExternal("MIRAC834620140123", new BigDecimal("1.00"), new BigDecimal("1164.00"), new BigDecimal("1163.90909091"), (long) 21));
		externalList.add(createExternal("MIRAC834320140123", new BigDecimal("1.00"), new BigDecimal("1164.00"), new BigDecimal("1163.90000000"), (long) 22));
		externalList.add(createExternal("MIRAC834120140123", new BigDecimal("1.00"), new BigDecimal("1164.00"), new BigDecimal("1163.88888889"), (long) 23));
		externalList.add(createExternal("MIRAC833820140123", new BigDecimal("1.00"), new BigDecimal("1163.90"), new BigDecimal("1163.87500000"), (long) 24));
		externalList.add(createExternal("MIRAC833620140123", new BigDecimal("1.00"), new BigDecimal("1163.90"), new BigDecimal("1163.87142857"), (long) 25));
		externalList.add(createExternal("MIRAC833420140123", new BigDecimal("1.00"), new BigDecimal("1163.90"), new BigDecimal("1163.86666667"), (long) 26));
		externalList.add(createExternal("MIRAC833020140123", new BigDecimal("1.00"), new BigDecimal("1163.90"), new BigDecimal("1163.86000000"), (long) 27));
		externalList.add(createExternal("MIRAC832820140123", new BigDecimal("1.00"), new BigDecimal("1163.90"), new BigDecimal("1163.85000000"), (long) 28));
		externalList.add(createExternal("MIRAC832620140123", new BigDecimal("1.00"), new BigDecimal("1163.80"), new BigDecimal("1163.83333333"), (long) 29));
		externalList.add(createExternal("MIRAC832420140123", new BigDecimal("1.00"), new BigDecimal("1163.80"), new BigDecimal("1163.85000000"), (long) 30));
		externalList.add(createExternal("MIRAC832220140123", new BigDecimal("1.00"), new BigDecimal("1163.90"), new BigDecimal("1163.90000000"), (long) 31));

		MatchingResult<FixAllocationDetail, FixExecutionReport> match = matcher.match(internalList, externalList);

		Assertions.assertEquals(2, match.getItemList().size());

		/*
		Collections.sort(match.getItemList(), new Comparator<MatchingResultItem<FixAllocationDetail, FixExecutionReport>>() {

			@Override
			public int compare(MatchingResultItem<FixAllocationDetail, FixExecutionReport> o1, MatchingResultItem<FixAllocationDetail, FixExecutionReport> o2) {
				return o1.getInternalList().get(0).getId().compareTo(o2.getInternalList().get(0).getId());
			}
		});
*/
		MatchingResultItem<FixAllocationDetail, FixExecutionReport> r1 = match.getItemList().get(0);
		Assertions.assertEquals(1, r1.getInternalList().size());
		Assertions.assertEquals(5, r1.getExternalList().size());

		Assertions.assertEquals(new BigDecimal("1164.0600000"), getAveragePrice(r1.getExternalList(), "lastPrice", "lastShares"));

		Assertions.assertEquals(new BigDecimal("1164.20"), r1.getExternalList().get(0).getLastPrice());
		Assertions.assertEquals(new BigDecimal("1164.00"), r1.getExternalList().get(1).getLastPrice());
		Assertions.assertEquals(new BigDecimal("1164.10"), r1.getExternalList().get(2).getLastPrice());
		Assertions.assertEquals(new BigDecimal("1164.00"), r1.getExternalList().get(3).getLastPrice());
		Assertions.assertEquals(new BigDecimal("1164.00"), r1.getExternalList().get(4).getLastPrice());

		Assertions.assertEquals(10, (long) r1.getExternalList().get(0).getId());
		Assertions.assertEquals(19, (long) r1.getExternalList().get(1).getId());
		Assertions.assertEquals(20, (long) r1.getExternalList().get(2).getId());
		Assertions.assertEquals(21, (long) r1.getExternalList().get(3).getId());
		Assertions.assertEquals(22, (long) r1.getExternalList().get(4).getId());

		MatchingResultItem<FixAllocationDetail, FixExecutionReport> r2 = match.getItemList().get(1);
		Assertions.assertEquals(1, r2.getInternalList().size());
		Assertions.assertEquals(17, r2.getExternalList().size());

		Assertions.assertEquals(new BigDecimal("1164.0588235"), getAveragePrice(r2.getExternalList(), "lastPrice", "lastShares"));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <E> BigDecimal getAveragePrice(List<E> entityList, String priceProperty, String quantityProperty) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal quantity = BigDecimal.ZERO;
		for (E entry : CollectionUtils.getIterable(entityList)) {
			result = MathUtils.add(result, MathUtils.multiply((BigDecimal) BeanUtils.getPropertyValue(entry, quantityProperty), (BigDecimal) BeanUtils.getPropertyValue(entry, priceProperty)));
			quantity = MathUtils.add(quantity, (BigDecimal) BeanUtils.getPropertyValue(entry, quantityProperty));
		}
		return MathUtils.divide(result, quantity, 7);
	}


	private FixAllocationDetail createInternal(String account, BigDecimal quantity, BigDecimal averagePrice) {
		FixAllocationInstruction instruction = new FixAllocationInstruction();
		instruction.setAveragePrice(averagePrice);
		FixAllocationDetail detail = new FixAllocationDetail();
		detail.setFixAllocationInstruction(instruction);
		detail.setAllocationAccount(account);
		detail.setAllocationShares(quantity);
		return detail;
	}


	private FixExecutionReport createExternal(String executionId, BigDecimal quantity, BigDecimal price, BigDecimal averagePrice, Long id) {
		FixExecutionReport report = new FixExecutionReport();
		report.setLastShares(quantity);
		report.setLastPrice(price);
		report.setAveragePrice(averagePrice);
		report.setExecutionId(executionId);
		report.setId(id);
		return report;
	}
}
