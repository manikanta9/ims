package com.clifton.trade.order.fix.converter.util.populators;


/**
 * An abstract base class for implementations of TradeOrderFixSecurityEntityPopulator.
 *
 * @author davidi
 */
public abstract class BaseTradeOrderFixSecurityEntityPopulator implements TradeOrderFixSecurityEntityPopulator {

	private int order;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void setOrder(int order) {
		this.order = order;
	}
}
