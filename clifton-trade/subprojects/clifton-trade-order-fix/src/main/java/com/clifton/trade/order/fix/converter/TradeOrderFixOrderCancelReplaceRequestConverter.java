package com.clifton.trade.order.fix.converter;


import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import com.clifton.trade.order.TradeOrder;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeOrderFixOrderConverter</code> defines a TradeOrder to FixOrderCancelReplaceRequest converter.
 *
 * @author mwacker
 */
@Component
public class TradeOrderFixOrderCancelReplaceRequestConverter extends AbstractTradeOrderFixOrderConverter<FixOrderCancelReplaceRequest> {

	@Override
	public FixOrderCancelReplaceRequest convert(TradeOrder tradeOrder) {
		FixOrderCancelReplaceRequest order = new FixOrderCancelReplaceRequest();
		return super.convert(tradeOrder, order);
	}
}
