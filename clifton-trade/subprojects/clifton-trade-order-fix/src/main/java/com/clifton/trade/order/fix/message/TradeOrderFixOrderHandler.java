package com.clifton.trade.order.fix.message;

import com.clifton.fix.order.FixOrder;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class TradeOrderFixOrderHandler extends AbstractTradeOrderFixMessageHandler<FixOrder> {


	@Override
	public Class<FixOrder> getMessageClass() {
		return FixOrder.class;
	}


	@Override
	public TradeOrderProcessorParameters handleMessage(FixOrder entity) {
		TradeOrder order = getTradeOrderService().getTradeOrder(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
		order.setExternalIdentifier(entity.getIdentifier().getId());
		order.setValidationError(entity.isValidationError());
		order.setAllocationValidationError(entity.isAllocationValidationError());
		getTradeOrderService().saveTradeOrder(order);
		return null;
	}
}
