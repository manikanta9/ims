package com.clifton.trade.order.fix.message;

import com.clifton.fix.order.FixEntity;


/**
 * @author mwacker
 */
public interface TradeFixMessageHandler<T extends FixEntity, R> {

	public R handleMessage(T report);


	public Class<T> getMessageClass();
}
