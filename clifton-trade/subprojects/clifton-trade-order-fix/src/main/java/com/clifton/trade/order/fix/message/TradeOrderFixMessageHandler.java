package com.clifton.trade.order.fix.message;

import com.clifton.fix.order.FixEntity;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;


/**
 * @author mwacker
 */
public interface TradeOrderFixMessageHandler<T extends FixEntity> {

	public TradeOrderProcessorParameters handleMessage(T report);
}
