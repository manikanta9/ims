package com.clifton.trade.order.fix;


import com.clifton.core.beans.IdentityObject;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.factory.FixEntityFactoryGenerationTypes;
import com.clifton.trade.order.TradeOrder;

import java.util.List;
import java.util.Map;


/**
 * The <code>TradeOrderFixHandler</code> defines a handler used convert FixEntities to outgoing
 * message types and send the message via JMS to the server.
 *
 * @author mwacker
 */
public interface TradeOrderFixHandler {

	/**
	 * Send an FIX message to fix server.
	 */
	public <T extends FixEntity> void send(T entity, Integer sourceId);


	public void sendDontKnowTrade(FixExecutionReport report);


	public <T extends IdentityObject> void sendEntity(T order, FixEntityFactoryGenerationTypes action);



	public void sendTradeOrderFixCancelRequest(TradeOrder order);


	public void resendOrderMessage(TradeOrder order);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Create the FixOrder objects for each order that is provided.
	 */
	public Map<TradeOrder, FixOrder> createFixTradeOrderMap(List<TradeOrder> orderList);


	/**
	 * Execute FIX order in the TradeOrder to FixOrder map
	 */
	public void executeFixTradeOrderMap(Map<TradeOrder, FixOrder> fixTradeOrderMap);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void reprocessFixMessage(FixMessage message);
}
