package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Equity trades.
 * Investment Type IN (Stocks, Funds, Notes)
 *
 * @author davidi
 */
@Component
public class EquityTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	public EquityTradeOrderFixSecurityEntityPopulator() {
		setOrder(40);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.STOCKS)
				|| InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.FUNDS)
				|| InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.NOTES);
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		InvestmentSecurity security = tradeOrder.getInvestmentSecurity();

		securityEntity.setSecurityType(SecurityTypes.COMMON_STOCK);
		TradeDestinationMapping destinationMapping = tradeOrder.getDestinationMapping();

		if (!destinationMapping.getMappingConfiguration().isFixUseSymbolForEquityOrders() && ((security.getSedol() == null) || security.getSedol().isEmpty())) {
			throw new RuntimeException("Cannot send fix trade for Stock [" + security.getSymbol() + "] because the SEDOL is not available.");
		}

		if (destinationMapping.getMappingConfiguration().isFixUseSymbolForEquityOrders()) {
			securityEntity.setSymbol(security.getSymbol());
			securityEntity.setSecurityIDSource(SecurityIDSources.BID);
		}
		else {
			securityEntity.setSymbol(security.getSedol());
			securityEntity.setSecurityIDSource(SecurityIDSources.SEDOL);
		}
	}
}
