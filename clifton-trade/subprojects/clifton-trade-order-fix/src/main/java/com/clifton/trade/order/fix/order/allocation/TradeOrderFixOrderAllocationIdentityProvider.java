package com.clifton.trade.order.fix.order.allocation;


import java.util.List;


/**
 * The <code>FixOrderAllocationIdentityProvider</code> defines an object used to lookup allocation
 * identifiers based on the FIX order id.
 * <p/>
 * NOTE: This interface needs to implemented by the OMS that is using this API.
 *
 * @author mwacker
 */
public interface TradeOrderFixOrderAllocationIdentityProvider {

	public List<Integer> getAllocationExternalIdentityListByOrder(Integer externalOrderIdentifier);


	public List<Integer> getAllocationExternalIdentityListByOrder(Integer externalOrderIdentifier, List<Integer> orderIdList);
}
