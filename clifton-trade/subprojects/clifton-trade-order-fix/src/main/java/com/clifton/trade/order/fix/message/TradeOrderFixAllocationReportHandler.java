package com.clifton.trade.order.fix.message;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetail;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessorParameters;
import com.clifton.trade.order.fix.order.allocation.TradeOrderFixOrderAllocationService;
import com.clifton.trade.order.processor.TradeOrderProcessorActonTypes;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.trade.order.util.TradeOrderUtils;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author mwacker
 */
@Component
public class TradeOrderFixAllocationReportHandler extends AbstractTradeOrderFixMessageHandler<FixAllocationReport> {

	private TradeOrderFixOrderAllocationService tradeOrderFixOrderAllocationService;

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	@Override
	public Class<FixAllocationReport> getMessageClass() {
		return FixAllocationReport.class;
	}


	@Override
	public TradeOrderProcessorParameters handleMessage(FixAllocationReport entity) {
		FixAllocationInstruction allocationInstruction = getTradeOrderFixOrderAllocationService().getTradeOrderFixAllocationInstruction(entity.getIdentifier().getId(), false);
		ValidationUtils.assertNotNull(allocationInstruction, "No FixAllocationInstruction could be found for allocation report with unique id [" + entity.getIdentifier().getUniqueStringIdentifier()
				+ "].");

		List<FixAllocationReport> fixAllocationReportList = getTradeOrderFixOrderAllocationService().getTradeOrderFixAllocationReportListByAllocation(entity.getIdentifier().getId(), false);

		BigDecimal allocationReportQuantityTotal = sumAllocationReportDetails(fixAllocationReportList);
		ValidationUtils.assertFalse(allocationInstruction.getShares().compareTo(allocationReportQuantityTotal) < 0, "In coming allocation report [" + entity.getIdentifier().getUniqueStringIdentifier()
				+ "] has a total quantity of [" + CoreMathUtils.formatNumberInteger(allocationReportQuantityTotal) + "] but the allocation is only for [" + allocationInstruction.getShares() + "].");
		// only set the filled flag on the order if all allocation reports have been received
		if (allocationInstruction.getShares().compareTo(allocationReportQuantityTotal) == 0) {
			List<TradeOrder> orderList = getTradeOrderService().getTradeOrderListByAllocationId(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));

			// note the first order will control the average price booking
			TradeOrder firstOrder = CollectionUtils.getFirstElement(orderList);

			for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
				TradeOrderAllocationStatus allocationStatus = BeanUtils.cloneBean(order.getAllocationStatus(), false, false);
				allocationStatus.setId(null);

				allocationStatus.setAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);
				allocationStatus.setAllocationFillReceived(true);
				saveOrderWithStatus(order, order.getStatus(), allocationStatus);
			}

			// get the list of allocation details
			TradeOrderAllocationProcessorParameters allocationParameters = new TradeOrderAllocationProcessorParameters();
			allocationParameters.setExternalAllocationIdentifier(entity.getIdentifier().getId());
			allocationParameters.setAllocationReports(fixAllocationReportList);
			List<TradeOrderAllocationDetail> allocationDetailList = getTradeOrderAllocationDetailProvider().getTradeOrderAllocationDetails(allocationParameters);

			TradeOrderProcessorParameters result = new TradeOrderProcessorParameters(orderList, TradeOrderProcessorActonTypes.PROCESS_ORDER_ALLOCATION);
			if (firstOrder != null && firstOrder.getDestinationMapping() != null
					&& firstOrder.getDestinationMapping().getMappingConfiguration().isFixForceExecutionAveragePriceWithAllocationReport()
					&& !TradeOrderUtils.isBticOrder(firstOrder)) {
				result.setForceAveragePriceAllocation(true);
			}
			result.setAllocationDetailList(allocationDetailList);
			return result;
		}
		// update validation error status
		if (entity.isValidationError() || entity.isAllocationValidationError()) {
			List<TradeOrder> orderList = getTradeOrderService().getTradeOrderListByAllocationId(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
			for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
				saveOrderWithValidationStatus(order, entity.isValidationError(), entity.isAllocationValidationError());
			}
		}
		return null;
	}


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	private BigDecimal sumAllocationReportDetails(List<FixAllocationReport> fixAllocationReportList) {
		BigDecimal total = BigDecimal.ZERO;
		for (FixAllocationReport fixAllocationReport : CollectionUtils.getIterable(fixAllocationReportList)) {
			total = total.add(CoreMathUtils.sumProperty(fixAllocationReport.getFixAllocationReportDetailList(), FixAllocationReportDetail::getAllocationShares));
		}
		return total;
	}

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	public TradeOrderFixOrderAllocationService getTradeOrderFixOrderAllocationService() {
		return this.tradeOrderFixOrderAllocationService;
	}


	public void setTradeOrderFixOrderAllocationService(TradeOrderFixOrderAllocationService tradeOrderFixOrderAllocationService) {
		this.tradeOrderFixOrderAllocationService = tradeOrderFixOrderAllocationService;
	}
}
