package com.clifton.trade.order.fix.order.allocation;


import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderIdentityProvider;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>TradeOrderAllocationIdentityProvider</code> implements FixOrderAllocationIdentityProvider used by the FIX services to look up
 * the list of allocation id's for an order.
 *
 * @author mwacker
 */
@Component("tradeOrderFixOrderAllocationIdentityProvider")
public class TradeOrderAllocationFixIdentityProvider implements TradeOrderFixOrderAllocationIdentityProvider {


	private TradeOrderFixOrderIdentityProvider tradeOrderFixOrderIdentityProvider;

	private TradeOrderAllocationService tradeOrderAllocationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Integer> getAllocationExternalIdentityListByOrder(Integer externalOrderIdentifier) {
		List<Integer> orderIds = getTradeOrderFixOrderIdentityProvider().getExternalIdentityListByOrder(externalOrderIdentifier);
		return getAllocationExternalIdentityListByOrder(externalOrderIdentifier, orderIds);
	}


	@Override
	public List<Integer> getAllocationExternalIdentityListByOrder(Integer externalOrderIdentifier, List<Integer> orderIdList) {
		List<Integer> result = new ArrayList<>();
		for (Integer eoi : CollectionUtils.getIterable(orderIdList)) {
			if (eoi != null) {
				List<TradeOrderAllocation> allocationList = getTradeOrderAllocationService().getTradeOrderAllocationListByExternalOrder(eoi);
				if (!CollectionUtils.isEmpty(allocationList)) {
					for (TradeOrderAllocation allocation : CollectionUtils.getIterable(allocationList)) {
						if (allocation.getExternalIdentifier() != null) {
							result.add(allocation.getExternalIdentifier());
						}
					}
				}
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderFixOrderIdentityProvider getTradeOrderFixOrderIdentityProvider() {
		return this.tradeOrderFixOrderIdentityProvider;
	}


	public void setTradeOrderFixOrderIdentityProvider(TradeOrderFixOrderIdentityProvider tradeOrderFixOrderIdentityProvider) {
		this.tradeOrderFixOrderIdentityProvider = tradeOrderFixOrderIdentityProvider;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}
}
