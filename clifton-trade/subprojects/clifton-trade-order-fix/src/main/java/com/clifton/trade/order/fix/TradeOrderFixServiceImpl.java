package com.clifton.trade.order.fix;


import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageService;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.trade.TradeService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessor;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;


@Service
public class TradeOrderFixServiceImpl implements TradeOrderFixService {

	public static final String RESOURCE_TRADE_ORDER_AVERAGE_PRICE = "TradeOrderAveragePrice";


	private FixMessageService fixMessageService;

	private SecurityAuthorizationService securityAuthorizationService;

	private TradeOrderAllocationProcessor tradeOrderAllocationProcessor;
	private TradeOrderFixHandler tradeOrderFixHandler;
	private TradeOrderService tradeOrderService;
	private TradeOrderWorkflowService tradeOrderWorkflowService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void resendTradeOrderFix(int tradeOrderId) {
		getTradeOrderFixHandler().resendOrderMessage(getTradeOrderService().getTradeOrder(tradeOrderId));
	}


	@Override
	@Transactional
	public void resendTradeOrderFixRejected(int tradeOrderId) {
		TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
		ValidationUtils.assertTrue(order != null && order.getStatus().getOrderStatus() == TradeOrderStatuses.REJECTED, "Cannot resend order messages for because the order was not rejected.");
		getTradeOrderFixHandler().resendOrderMessage(order);
	}


	@Override
	@Transactional
	public void sendTradeOrderFixCancelRequest(int tradeOrderId) {
		TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
		if (order == null) {
			throw new RuntimeException("Cannot cancel order because no order exists.");
		}
		getTradeOrderFixHandler().sendTradeOrderFixCancelRequest(order);
	}


	@Override
	@Transactional
	public void moveTradeOrderFixListToManualFill(Integer[] tradeOrderIdList) {
		if (tradeOrderIdList.length > 0) {
			for (Integer tradeOrderId : tradeOrderIdList) {
				TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
				getTradeOrderWorkflowService().setTradeWorkflowStates(order, TradeService.TRADE_AWAITING_FILLS_STATE_NAME);
			}
		}
	}


	@Override
	@Transactional
	public void cancelTradeOrderFixList(Integer[] tradeOrderIdList) {
		if (tradeOrderIdList.length > 0) {
			for (Integer tradeOrderId : tradeOrderIdList) {
				TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
				if (TradeOrderStatuses.SENT == order.getStatus().getOrderStatus()) {
					// do both if the order is sent, it may get rejected, but we don't care
					getTradeOrderFixHandler().sendTradeOrderFixCancelRequest(order);
					getTradeOrderWorkflowService().setTradeWorkflowStates(order, TradeService.FIX_CANCELED_STATE_NAME);
				}
				if (isCancelAllowed(order)) {
					getTradeOrderWorkflowService().setTradeWorkflowStates(order, TradeService.FIX_CANCELED_STATE_NAME);
				}
				else if (order.getDestinationMapping().getTradeDestination().getType().isFixTradeOnly()) {
					getTradeOrderFixHandler().sendTradeOrderFixCancelRequest(order);
				}
			}
		}
	}


	@Override
	@Transactional
	public void bookTradeOrderFixListWithAveragePrice(Integer[] tradeOrderIdList) {
		if (tradeOrderIdList.length > 0) {
			TradeOrderProcessorParameters allocationParameters = new TradeOrderProcessorParameters();
			allocationParameters.setTradeOrderList(new ArrayList<>());
			allocationParameters.setForceAveragePriceAllocation(true);
			boolean isAdmin = getSecurityAuthorizationService().isSecurityUserAdmin() || getSecurityAuthorizationService().isSecurityAccessAllowed(RESOURCE_TRADE_ORDER_AVERAGE_PRICE, SecurityPermission.PERMISSION_FULL_CONTROL);
			for (Integer tradeOrderId : tradeOrderIdList) {
				TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
				boolean isPartiallyFilledCancelled = (TradeOrderStatuses.PARTIALLY_FILLED_CANCELED == order.getStatus().getOrderStatus()
						&& CollectionUtils.asNonNullList(order.getTradeList()).size() == 1);
				ValidationUtils.assertTrue(order.getStatus().isFillComplete() || isPartiallyFilledCancelled, "The fill must be complete or associated with a single trade to create the trade fills.");
				ValidationUtils.assertTrue(isAdmin || order.getAllocationStatus() == null || !order.getAllocationStatus().isRejected(), () ->
						"Cannot book with average price because order is rejected with status [" + order.getAllocationStatus().getAllocationStatus() + "]");
				allocationParameters.getTradeOrderList().add(order);
			}
			getTradeOrderAllocationProcessor().processAllocationFills(allocationParameters);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                      Helper Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Allow cancelling underlying trades if the order is manual, there is not external order associated with the order or
	 * it not new nor sent and has no fills.
	 */
	private boolean isCancelAllowed(TradeOrder order) {
		if ((MathUtils.compare(order.getQuantityFilled(), BigDecimal.ZERO) > 0)
				|| TradeOrderStatuses.NEW == order.getStatus().getOrderStatus()) {
			return false;
		}
		return !order.getDestinationMapping().getTradeDestination().getType().isFixTradeOnly()
				|| (!order.getStatus().hasFills() && TradeOrderStatuses.NEW != order.getStatus().getOrderStatus())
				|| order.getExternalIdentifier() == null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void reprocessFixMessage(long messageId) {
		FixMessage message = getFixMessageService().getFixMessage(messageId);
		getTradeOrderFixHandler().reprocessFixMessage(message);
	}


	@Override
	public void reprocessFixMessageList(long[] messageIdList) {
		// used for exception handling in the for-loop below
		StringBuilder errors = null;
		Throwable firstCause = null;

		for (long messageId : messageIdList) {
			FixMessage message = getFixMessageService().getFixMessage(messageId);
			try {
				getTradeOrderFixHandler().reprocessFixMessage(message);
			}
			catch (Throwable e) {
				boolean firstError = false;
				if (errors == null) {
					firstCause = e;
					firstError = true;
					errors = new StringBuilder(1024);
				}
				else {
					errors.append("\n\n");
				}
				errors.append("Could not process FIX message [").append(message.getText()).append("]");
				if (!firstError) {
					errors.append("\nCAUSED BY ");
					errors.append(ExceptionUtils.getDetailedMessage(e));
				}
			}
		}

		if (errors != null) {
			throw new RuntimeException(errors.toString(), firstCause);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageService getFixMessageService() {
		return this.fixMessageService;
	}


	public void setFixMessageService(FixMessageService fixMessageService) {
		this.fixMessageService = fixMessageService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public TradeOrderAllocationProcessor getTradeOrderAllocationProcessor() {
		return this.tradeOrderAllocationProcessor;
	}


	public void setTradeOrderAllocationProcessor(TradeOrderAllocationProcessor tradeOrderAllocationProcessor) {
		this.tradeOrderAllocationProcessor = tradeOrderAllocationProcessor;
	}


	public TradeOrderFixHandler getTradeOrderFixHandler() {
		return this.tradeOrderFixHandler;
	}


	public void setTradeOrderFixHandler(TradeOrderFixHandler tradeOrderFixHandler) {
		this.tradeOrderFixHandler = tradeOrderFixHandler;
	}


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}
}
