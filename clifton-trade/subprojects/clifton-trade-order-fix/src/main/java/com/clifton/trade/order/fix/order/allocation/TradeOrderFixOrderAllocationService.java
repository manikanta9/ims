package com.clifton.trade.order.fix.order.allocation;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


public interface TradeOrderFixOrderAllocationService {

	////////////////////////////////////////////////////////////////////////////
	////////       FixAllocationInstruction Business Methods         /////////// 
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public FixAllocationInstruction getTradeOrderFixAllocationInstruction(Integer externalAllocationIdentifier, boolean fullyPopulated);


	////////////////////////////////////////////////////////////////////////////
	////////     FixAllocationAcknowledgement Business Methods       /////////// 
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixAllocationAcknowledgement> getTradeOrderFixAllocationAcknowledgementList(Integer externalAllocationIdentifier, boolean fullyPopulated);


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixAllocationAcknowledgement> getTradeOrderFixAllocationAcknowledgementListByOrder(Integer externalOrderIdentifier, boolean fullyPopulated);


	////////////////////////////////////////////////////////////////////////////
	////////          FixAllocationReport Business Methods           /////////// 
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixAllocationReport> getTradeOrderFixAllocationReportListByAllocation(Integer externalAllocationIdentifier, boolean fullyPopulated);


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixAllocationReport> getTradeOrderFixAllocationReportListByOrder(Integer externalOrderIdentifier, boolean fullyPopulated);


	////////////////////////////////////////////////////////////////////////////
	////////     FixAllocationReportDetailList Business Methods      /////////// 
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixAllocationReportDetail> getTradeOrderFixAllocationReportDetailListByAllocation(Integer externalAllocationIdentifier, boolean fullyPopulated);


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixAllocationReportDetail> getTradeOrderFixAllocationReportDetailListByOrder(Integer externalOrderIdentifier, boolean fullyPopulated);
}
