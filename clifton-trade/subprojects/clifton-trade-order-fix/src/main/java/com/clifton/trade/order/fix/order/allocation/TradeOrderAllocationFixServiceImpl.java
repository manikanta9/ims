package com.clifton.trade.order.fix.order.allocation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.factory.FixEntityFactory;
import com.clifton.fix.order.factory.FixEntityFactoryGenerationTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessor;
import com.clifton.trade.order.fix.TradeOrderFixHandler;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.trade.search.TradeSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class TradeOrderAllocationFixServiceImpl implements TradeOrderAllocationFixService {

	private TradeService tradeService;
	private TradeOrderService tradeOrderService;
	private TradeOrderAllocationProcessor tradeOrderAllocationProcessor;
	private TradeOrderAllocationService tradeOrderAllocationService;
	private TradeOrderWorkflowService tradeOrderWorkflowService;

	private FixEntityFactory fixEntityFactory;
	private TradeOrderFixHandler tradeOrderFixHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void sendTradeOrderFixAllocationInstruction(List<TradeOrder> orderList) {
		ValidationUtils.assertNotEmpty(orderList, "[orderList] must not be empty.");

		try {
			List<TradeOrderAllocation> allocationList = new ArrayList<>();
			List<List<TradeOrder>> allocationGroupList = groupOrdersForAllocation(orderList);
			for (List<TradeOrder> groupOrderList : CollectionUtils.getIterable(allocationGroupList)) {

				TradeOrder order = groupOrderList.get(0);
				// populate the partially filled order
				if (order.getStatus().isFillComplete() && order.getStatus().isPartialFill()) {
					order = populateOrderForPartialAllocation(order);
				}

				// Save the order and its updated trades and skip sending of FIX AllocationInstruction if the mapping configuration does not allow it.
				if (!order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation()) {
					TradeOrder newOrder = getTradeOrderService().saveTradeOrder(order);
					List<Trade> newTradeList = new ArrayList<>();
					for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
						newTradeList.add(getTradeService().saveTrade(trade));
					}
					newOrder.setTradeList(newTradeList);

					TradeOrderProcessorParameters tradeOrderProcessorParameters = new TradeOrderProcessorParameters();
					tradeOrderProcessorParameters.setTradeOrder(newOrder);
					tradeOrderProcessorParameters.setTradeOrderList(CollectionUtils.createList(newOrder));
					getTradeOrderAllocationProcessor().processAllocationFills(tradeOrderProcessorParameters);
					continue;
				}

				ValidationUtils.assertTrue(order.getStatus().isFillComplete(), "Cannot send allocation for an order until the fill is complete.");
				ValidationUtils.assertTrue(order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation(), "Cannot send allocation for trade that is not post trade allocated.");
				ValidationUtils.assertNotNull(order.getDestinationMapping().getExecutingBrokerCompany(), "Cannot send allocation for trade without an executing broker.");
				if ((order.getTradeList() == null) || order.getTradeList().isEmpty()) {
					throw new RuntimeException("Cannot allocate the order message because no trades are associated with the order.");
				}
				for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
					ValidationUtils.assertNotNull(trade.getExecutingBrokerCompany(), "Cannot send allocation for trade [" + trade.getId() + "] without an executing broker.");
					if (!TradeService.TRADE_EXECUTION_STATE_NAME.equals(trade.getWorkflowState().getName())) {
						throw new RuntimeException("Cannot allocate order because trades are not in the [" + TradeService.TRADE_EXECUTION_STATE_NAME + "] state.");
					}
				}
				allocationList.add(generateTradeOrderAllocation(orderList));
			}

			// send the fix messages after all db updates are complete
			for (TradeOrderAllocation allocation : CollectionUtils.getIterable(allocationList)) {
				// send the message last in case of an error
				sendEntity(allocation, FixEntityFactoryGenerationTypes.ALLOCATION);
			}
		}
		catch (Throwable e) {
			// try to set the allocation status to FAILED to send.
			for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
				try {
					if (order.getAllocationStatus() == null) {
						TradeOrderAllocationStatus newStatus = getTradeOrderAllocationService().getTradeOrderAllocationStatusByStatuses(TradeOrderAllocationStatuses.FAILED_TO_SEND, false);
						order.setAllocationStatus(newStatus);
						getTradeOrderService().saveTradeOrder(order);
					}
				}
				catch (Throwable ex) {
					LogUtils.error(getClass(), "Failed to set [FAILED_TO_SEND] allocation status for order [" + order.getId() + "]", ex);
				}
			}
			throw e;
		}
	}


	@Override
	@Transactional(timeout = 180)
	public void sendTradeOrderFixAllocationInstructionPartiallyFilled(TradeOrder bean) {
		TradeOrder order = populateOrderForPartialAllocation(bean);
		sendTradeOrderFixAllocationInstruction(CollectionUtils.createList(order));
	}


	@Override
	public List<Trade> calculateTradeOrderTradeProportionalQuantity(int tradeOrderId) {
		TradeOrder tradeOrder = getTradeOrderService().getTradeOrder(tradeOrderId);
		List<Trade> tradeList = CollectionUtils.getStream(tradeOrder.getTradeList()).filter(Objects::nonNull).collect(Collectors.toList());
		if (tradeList.isEmpty()) {
			return tradeList;
		}

		// Sort list in descending order based on quantity to prioritize distribution of remaining shares / contracts to trades with the largest quantities.
		tradeList = CollectionUtils.sort(tradeList, (trade1, trade2) -> MathUtils.compare(trade2.getQuantityIntended(), trade1.getQuantityIntended()));
		Map<Integer, BigDecimal> tradeQuantityMap = new HashMap<>();
		final int lastItemIndex = tradeList.size() - 1;
		boolean allQuantitiesAreEqual = MathUtils.isEqual(tradeList.get(0).getQuantityIntended(), tradeList.get(lastItemIndex).getQuantityIntended());

		BigDecimal quantityFilled = tradeOrder.getQuantityFilled() != null ? tradeOrder.getQuantityFilled() : BigDecimal.ZERO;
		BigDecimal quantityOrdered = tradeOrder.getQuantity();
		ValidationUtils.assertFalse(MathUtils.isNullOrZero(quantityOrdered), "Quantity Ordered cannot be null or zero for trade order: " + tradeOrderId);

		BigDecimal proportion = MathUtils.divide(quantityFilled, quantityOrdered);

		boolean usesFractionalQuantities = MathUtils.isGreaterThan(InvestmentUtils.getQuantityDecimalPrecision(tradeOrder.getInvestmentSecurity()), 0);
		boolean currencyTrade = TradeType.CURRENCY.equals(tradeList.get(0).getTradeType().getName());
		boolean forwardTrade = TradeType.FORWARDS.equals(tradeList.get(0).getTradeType().getName());

		BigDecimal quantityDistributed = BigDecimal.ZERO;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (usesFractionalQuantities) {
				if (trade.getAccountingNotional() != null && (currencyTrade || (forwardTrade && tradeOrder.isBuy()))) {
					trade.setAccountingNotional(MathUtils.multiply(proportion, trade.getAccountingNotional()));
				}
				else if (trade.getQuantityIntended() != null) {
					trade.setQuantityIntended(MathUtils.multiply(proportion, trade.getQuantityIntended()));
				}
				continue;
			}

			// store original trade quantities
			tradeQuantityMap.put(trade.getId(), trade.getQuantityIntended());

			BigDecimal proportionalQuantity = MathUtils.round(MathUtils.multiply(proportion, trade.getQuantityIntended()), 0, RoundingMode.FLOOR);
			trade.setQuantityIntended(proportionalQuantity);
			quantityDistributed = MathUtils.add(quantityDistributed, proportionalQuantity);
		}

		BigDecimal remainingQuantity = MathUtils.round(MathUtils.subtract(quantityFilled, quantityDistributed), 0, RoundingMode.FLOOR);
		if (!usesFractionalQuantities && MathUtils.isGreaterThan(remainingQuantity, BigDecimal.ZERO)) {
			// if all quantities are equal, randomize the list order for fair distribution of remaining quantity.
			if (allQuantitiesAreEqual) {
				Collections.shuffle(tradeList);
			}

			for (Trade trade : CollectionUtils.getIterable(tradeList)) {
				if (MathUtils.isLessThan(trade.getQuantityIntended(), tradeQuantityMap.get(trade.getId()))) {
					trade.setQuantityIntended(MathUtils.add(trade.getQuantityIntended(), BigDecimal.ONE));
					remainingQuantity = MathUtils.subtract(remainingQuantity, BigDecimal.ONE);
				}
				if (MathUtils.isEqual(remainingQuantity, BigDecimal.ZERO)) {
					break;
				}
			}
		}

		return tradeList;
	}


	@Override
	@Transactional
	public void resendTradeOrderFixAllocationInstruction(int tradeOrderId) {
		TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
		ValidationUtils.assertTrue(order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation(), "Cannot send allocation for trade that is not post trade allocated.");
//		if (order.getAllocationStatus().isAllocationFillReceived()) {
//			throw new RuntimeException("Cannot resend allocation because the order has been completed.");
//		}
		for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
			if (!TradeService.TRADE_EXECUTION_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				throw new RuntimeException("Cannot allocate order because trades are not in the [" + TradeService.TRADE_EXECUTION_STATE_NAME + "] state.");
			}
		}
		TradeOrderAllocation originalAllocation = getTradeOrderAllocationService().getTradeOrderAllocationActiveByOrder(order);
		ValidationUtils.assertNotNull(originalAllocation, "Cannot resend an allocation for an order that is not allocated.");

		List<TradeOrder> orderList = getTradeOrderService().getTradeOrderListByAllocationId(originalAllocation.getId());

		for (TradeOrder tradeOrder : CollectionUtils.getIterable(orderList)) {
			if (tradeOrder.getStatus().isPartialFill()) {
				if (tradeOrder.getTradeList().size() == 1) {
					tradeOrder.getTradeList().get(0).setQuantityIntended(order.getQuantityFilled());
				}
			}
		}

		sendEntity(generateTradeOrderAllocation(orderList), FixEntityFactoryGenerationTypes.ALLOCATION);
	}


	@Override
	@Transactional
	public void cancelTradeOrderFixAllocationInstruction(int tradeOrderId) {
		TradeOrder order = getTradeOrderService().getTradeOrder(tradeOrderId);
		ValidationUtils.assertTrue(order.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation(), "Cannot send allocation for trade that is not post trade allocated.");
//		if (order.getAllocationStatus().isAllocationFillReceived()) {
//			throw new RuntimeException("Cannot resend allocation because the order has been completed.");
//		}

		TradeOrderAllocation allocation = getTradeOrderAllocationService().getTradeOrderAllocationActiveByOrder(order);
		ValidationUtils.assertNotNull(allocation, "There is no active allocation to cancel.");

		List<TradeOrder> orderList = getTradeOrderService().getTradeOrderListByAllocationId(allocation.getId());
		allocation.setTradeOrderList(orderList);

		sendEntity(allocation, FixEntityFactoryGenerationTypes.ALLOCATION_CANCEL);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Helper Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends IdentityObject> void sendEntity(T order, FixEntityFactoryGenerationTypes action) {
		FixEntity entity = getFixEntityFactory().create(order, action);
		getTradeOrderFixHandler().send(entity, (Integer) order.getIdentity());
	}


	private TradeOrderAllocation generateTradeOrderAllocation(List<TradeOrder> orderList) {
		validateOrderListForAllocation(orderList);

		TradeOrderAllocation result = new TradeOrderAllocation();
		TradeOrderAllocation originalAllocation = getTradeOrderAllocationService().getTradeOrderAllocationActiveByOrder(orderList.get(0));
		result.setOriginalOrderAllocation(originalAllocation);
		result.setTradeOrderList(orderList);
		if (originalAllocation == null) {
			result.setActive(true);
		}

		if (orderList.get(0).getAllocationStatus() == null || orderList.get(0).getAllocationStatus().getAllocationStatus() == TradeOrderAllocationStatuses.FAILED_TO_SEND) {
			for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
				TradeOrderAllocationStatus newAllocationStatus = getTradeOrderAllocationService().getTradeOrderAllocationStatusByStatuses(TradeOrderAllocationStatuses.SENT, false);
				order.setAllocationStatus(newAllocationStatus);
				//order.setTradeList(null);
				getTradeOrderService().saveTradeOrder(order);
			}
		}

		return getTradeOrderAllocationService().saveTradeOrderAllocation(result);
	}


	private TradeOrder populateOrderForPartialAllocation(TradeOrder bean) {
		List<Trade> newTradeList = bean.getTradeList();
		boolean useAccountingNotional = useAccountingNotional(newTradeList.get(0));

		// CurrencyTrades (as well as Currency Forward Trades when tradeOrder is a buy) use accountingNotional to represent quantity.
		if (useAccountingNotional) {
			BigDecimal totalAccountingNotional = CoreMathUtils.sumProperty(bean.getTradeList(), Trade::getAccountingNotional);
			ValidationUtils.assertTrue(MathUtils.round(bean.getQuantityFilled(), 7).equals(MathUtils.round(totalAccountingNotional, 7)),
					"Total AccountingNotional calculated was [" + totalAccountingNotional + "] but [" + bean.getQuantityFilled() + "] was expected.");
		}
		else {
			BigDecimal totalShares = CoreMathUtils.sumProperty(bean.getTradeList(), Trade::getQuantityIntended);
			ValidationUtils.assertTrue(MathUtils.round(bean.getQuantityFilled(), 7).equals(MathUtils.round(totalShares, 7)),
					"Total quantity calculated was [" + totalShares + "] but [" + bean.getQuantityFilled() + "] was expected.");
		}

		// Determines if the incoming trade list has trades with 0 quantities after proportional distribution (non fractional quantities)
		if (!useAccountingNotional && CollectionUtils.anyMatch(newTradeList, trade -> MathUtils.isEqual(trade.getQuantityIntended(), BigDecimal.ZERO))) {
			removeAndCancelZeroQuantityTrades(newTradeList, bean.getId());
		}

		// get the fully populated order
		TradeOrder order = getTradeOrderService().getTradeOrder(bean.getId());
		// verify that the new trade quantity is less than or equal to the original quantity, and append note to trade description.
		for (Trade newTrade : CollectionUtils.getIterable(newTradeList)) {
			Trade existingTrade = CollectionUtils.getOnlyElement(BeanUtils.filter(order.getTradeList(), Trade::getId, newTrade.getId()));
			ValidationUtils.assertNotNull(existingTrade, "Cannot find an existing trade the matches [" + newTrade.getLabel() + "].");
			if (useAccountingNotional) {
				ValidationUtils.assertFalse(MathUtils.isGreaterThan(newTrade.getAccountingNotional(), existingTrade.getAccountingNotional()), "Cannot fill more than the initial AccountingNotional value.");
			}
			else {
				ValidationUtils.assertFalse(MathUtils.isGreaterThan(newTrade.getQuantityIntended(), existingTrade.getQuantityIntended()), "Cannot fill more than the initial quantity.");
			}

			// in some processing streams this function may get called twice.  Don't re-append message if it is already in the description.
			String reducedTradeQtyNote = "Quantity reduced due to insufficient fill quantity on partially filled trade order: " + bean.getId() + ".";
			if (StringUtils.isEmpty(existingTrade.getDescription()) || !existingTrade.getDescription().contains(reducedTradeQtyNote)) {
				String updatedDescription = StringUtils.isEmpty(existingTrade.getDescription()) ? reducedTradeQtyNote : existingTrade.getDescription() + " " + reducedTradeQtyNote;
				newTrade.setDescription(StringUtils.substring(updatedDescription, 0, CoreMathUtils.min(updatedDescription.length(), 500)));
			}
		}

		// set the trade list with the list for the input bean which contains the updated fills
		order.setTradeList(newTradeList);
		return order;
	}


	/**
	 * Removes all trades with 0 QuantityIntended property values.  The incoming list reference is modified.  Note:  this method is intended for trades that use whole numbers for quantities.
	 */
	private void removeAndCancelZeroQuantityTrades(List<Trade> newTradeList, int tradeOrderID) {
		if (newTradeList != null && !newTradeList.isEmpty()) {
			List<Integer> tradeIdsForCancellationList = new ArrayList<>();
			Iterator<Trade> tradeIterator = newTradeList.iterator();
			while (tradeIterator.hasNext()) {
				Trade trade = tradeIterator.next();
				if (MathUtils.isEqual(BigDecimal.ZERO, trade.getQuantityIntended())) {
					tradeIdsForCancellationList.add(trade.getId());
					tradeIterator.remove();
				}
			}
			if (!tradeIdsForCancellationList.isEmpty()) {
				cancelTradesWithNote(tradeIdsForCancellationList, "Canceled due to insufficient fill quantity on partially filled trade order: " + tradeOrderID + ".");
			}
		}
	}


	private void cancelTradesWithNote(List<Integer> tradeIdList, String noteText) {
		if (!tradeIdList.isEmpty()) {
			// look up original unmodified trades
			TradeSearchForm tradeSearchForm = new TradeSearchForm();
			tradeSearchForm.setIds(tradeIdList.toArray(new Integer[0]));
			List<Trade> tradeList = getTradeService().getTradeList(tradeSearchForm);

			for (Trade trade : tradeList) {
				String updatedDescription = StringUtils.isEmpty(trade.getDescription()) ? noteText : trade.getDescription() + " " + noteText;
				trade.setDescription(StringUtils.substring(updatedDescription, 0, CoreMathUtils.min(updatedDescription.length(), 500)));
				getTradeService().saveTrade(trade);
				getTradeOrderWorkflowService().setTradeWorkflowState(trade, TradeService.TRADES_CANCELLED_STATE_NAME);
			}
		}
	}


	private List<List<TradeOrder>> groupOrdersForAllocation(List<TradeOrder> orderList) {
		List<List<TradeOrder>> result = new ArrayList<>();
		for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
			List<TradeOrder> targetList = getOrderListForGrouping(result, order);
			if (targetList == null) {
				targetList = new ArrayList<>();
				result.add(targetList);
			}
			targetList.add(order);
		}
		return result;
	}


	private List<TradeOrder> getOrderListForGrouping(List<List<TradeOrder>> tradeOrderGroupList, TradeOrder order) {
		for (List<TradeOrder> orderList : CollectionUtils.getIterable(tradeOrderGroupList)) {
			if (!orderList.isEmpty() && isGroupingForAllocationAllowed(order, orderList.get(0))) {
				return orderList;
			}
		}
		return null;
	}


	private boolean isGroupingForAllocationAllowed(TradeOrder order1, TradeOrder order2) {
		if (!order1.getDestinationMapping().equals(order2.getDestinationMapping())) {
			return false;
		}
		if (!order1.getInvestmentSecurity().equals(order2.getInvestmentSecurity())) {
			return false;
		}
		if (order1.isBuy() != order2.isBuy()) {
			return false;
		}
		if (!order1.getTraderUser().equals(order2.getTraderUser())) {
			return false;
		}
		return true;
	}


	private void validateOrderListForAllocation(List<TradeOrder> orderList) {
		InvestmentSecurity symbol = orderList.get(0).getInvestmentSecurity();
		boolean side = orderList.get(0).isBuy();
		for (int i = 1; i < orderList.size(); i++) {
			if (!orderList.get(i).getInvestmentSecurity().equals(symbol)) {
				throw new RuntimeException("Cannot allocate orders of different security types.");
			}
			if (!orderList.get(i).isBuy() == side) {
				throw new RuntimeException("Cannot allocate multiple orders with different Buy/Sell.");
			}
		}
	}


	private boolean useAccountingNotional(Trade trade) {
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			if (!trade.getSettlementCurrency().equals(trade.getInvestmentSecurity().getInstrument().getTradingCurrency())) {
				return true;
			}
		}
		else if (trade.getInvestmentSecurity().isCurrency()) {
			return true;
		}

		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderAllocationProcessor getTradeOrderAllocationProcessor() {
		return this.tradeOrderAllocationProcessor;
	}


	public void setTradeOrderAllocationProcessor(TradeOrderAllocationProcessor tradeOrderAllocationProcessor) {
		this.tradeOrderAllocationProcessor = tradeOrderAllocationProcessor;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}


	public FixEntityFactory getFixEntityFactory() {
		return this.fixEntityFactory;
	}


	public void setFixEntityFactory(FixEntityFactory fixEntityFactory) {
		this.fixEntityFactory = fixEntityFactory;
	}


	public TradeOrderFixHandler getTradeOrderFixHandler() {
		return this.tradeOrderFixHandler;
	}


	public void setTradeOrderFixHandler(TradeOrderFixHandler tradeOrderFixHandler) {
		this.tradeOrderFixHandler = tradeOrderFixHandler;
	}
}
