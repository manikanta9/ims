package com.clifton.trade.order.fix.message;

import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class TradeOrderFixAllocationInstructionHandler extends AbstractTradeOrderFixMessageHandler<FixAllocationInstruction> {


	@Override
	public Class<FixAllocationInstruction> getMessageClass() {
		return FixAllocationInstruction.class;
	}


	@Override
	public TradeOrderProcessorParameters handleMessage(FixAllocationInstruction entity) {
		TradeOrderAllocation allocation = getTradeOrderAllocationService().getTradeOrderAllocation(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
		allocation.setExternalIdentifier(entity.getIdentifier().getId());
		getTradeOrderAllocationService().saveTradeOrderAllocation(allocation);
		return null;
	}
}
