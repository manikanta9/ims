package com.clifton.trade.order.fix.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.fix.lifecycle.BaseFixMessageLifeCycleRetriever;
import com.clifton.fix.message.FixMessage;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>TradeOrderFixLifeCycleRetriever</code> is applied to trade orders that utilize FIX for executions to include FIX messages
 * associated with the trade order
 *
 * @author manderson
 */
@Component
public class TradeOrderFixLifeCycleRetriever extends BaseFixMessageLifeCycleRetriever {

	private TradeOrderFixOrderService tradeOrderFixOrderService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		if (entity instanceof TradeOrder) {
			return ((TradeOrder) entity).getExternalIdentifier() != null;
		}
		return false;
	}


	@Override
	public List<FixMessage> getFixMessageList(SystemLifeCycleEventContext lifeCycleEventContext) {
		TradeOrder tradeOrder = (TradeOrder) lifeCycleEventContext.getCurrentProcessingEntity();
		return getTradeOrderFixOrderService().getTradeOrderFixMessageListForOrder(tradeOrder.getExternalIdentifier());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderFixOrderService getTradeOrderFixOrderService() {
		return this.tradeOrderFixOrderService;
	}


	public void setTradeOrderFixOrderService(TradeOrderFixOrderService tradeOrderFixOrderService) {
		this.tradeOrderFixOrderService = tradeOrderFixOrderService;
	}
}
