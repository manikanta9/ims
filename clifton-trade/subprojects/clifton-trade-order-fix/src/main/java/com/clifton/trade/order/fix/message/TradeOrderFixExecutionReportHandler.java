package com.clifton.trade.order.fix.message;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.TimedEvictionCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixCompDealerQuote;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.beans.CompDealerQuotePriceTypes;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.instrument.calculator.DayCountCommand;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.adjustment.TradeOrderAdjustmentHandler;
import com.clifton.trade.order.fix.converter.FixExecutionReportTradeOrderConverter;
import com.clifton.trade.order.fix.util.TradeDestinationMappingAccountRelationshipCommand;
import com.clifton.trade.order.processor.TradeOrderProcessorActonTypes;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.trade.quote.TradeQuote;
import com.clifton.trade.quote.TradeQuoteRequest;
import com.clifton.trade.quote.TradeQuoteService;
import com.clifton.trade.quote.search.TradeQuoteRequestSearchForm;
import com.clifton.trade.quote.search.TradeQuoteSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * When an ExecutionReport is received, this handler will update the TradeOrder with status, quantity filled and average price
 * from the execution report.  Then it will return the processed order and any other parameters need by the TradeOrderProcessor in a new
 * TradeOrderProcessorParameters object.
 *
 * @author mwacker
 */
@Component
public class TradeOrderFixExecutionReportHandler extends AbstractTradeOrderFixMessageHandler<FixExecutionReport> {

	private TradeOrderAdjustmentHandler tradeOrderAdjustmentHandler;
	private TradeQuoteService tradeQuoteService;
	private FixExecutionReportTradeOrderConverter fixExecutionReportTradeOrderConverter;
	private SystemColumnValueHandler systemColumnValueHandler;
	private MarketDataSourceCompanyService marketDataSourceCompanyService;
	private TimedEvictionCache<?> tradeOrderTimedStatusCache;


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	@Override
	public Class<FixExecutionReport> getMessageClass() {
		return FixExecutionReport.class;
	}


	@Override
	public TradeOrderProcessorParameters handleMessage(FixExecutionReport report) {
		TradeOrder order = getTradeOrderForExecutionReport(report);
		if (order.getExternalIdentifier() == null) {
			order.setExternalIdentifier(report.getIdentifier().getId());
		}
		TradeOrderStatus status;
		if (order.getStatus() != null) {
			status = BeanUtils.cloneBean(order.getStatus(), false, false);
		}
		else {
			status = new TradeOrderStatus();
			order.setStatus(status);
		}
		status.setId(null);

		// get the trade destination mapping
		order.setDestinationMapping(getTradeDestinationMapping(order, report));
		// set the order status
		status.setOrderStatus(TradeOrderStatuses.getTradeOrderStatus(order, report, isTradeListInExecutionState(order)));
		// update the order with the average price and the quantity
		updateOrderAveragePriceAndQuantity(order, report, status);
		// clear the trade from the waiting cache.
		getTradeOrderTimedStatusCache().remove(order.getId());
		// set the index ratio (notional multiplier)
		order.setIndexRatio(report.getIndexRatio());
		// the order with the new status
		saveOrderWithStatus(order, status, order.getAllocationStatus());
		// the order with the new validation results
		saveOrderWithValidationStatus(order, report.isValidationError(), report.isAllocationValidationError());
		processTradeQuotes(order, report);

		return new TradeOrderProcessorParameters(order, TradeOrderProcessorActonTypes.PROCESS_ORDER);
	}


	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	private boolean isTradeListInExecutionState(TradeOrder order) {
		if (CollectionUtils.isEmpty(order.getTradeList())) {
			return false;
		}
		return TradeService.TRADE_EXECUTION_STATE_NAME.equals(order.getTradeList().get(0).getWorkflowState().getName());
	}


	/**
	 * Returns the destination mapping found using the partly list on the execution report.
	 *
	 * @param order
	 * @param report
	 */
	private TradeDestinationMapping getTradeDestinationMapping(TradeOrder order, FixExecutionReport report) {
		if (!report.getIdentifier().isDropCopyIdentifier() && !CollectionUtils.isEmpty(report.getPartyList()) && (order.getInvestmentSecurity() != null)) {
			TradeDestinationMappingAccountRelationshipCommand command = new TradeDestinationMappingAccountRelationshipCommand(order, report, report.getPartyList(), false);
			TradeDestinationMapping mapping = getTradeOrderFixUtilHandler().getTradeDestinationMappingFromFixPartyList(command);
			if (mapping != null) {
				return mapping;
			}
		}
		return order.getDestinationMapping();
	}


	private void updateOrderAveragePriceAndQuantity(TradeOrder order, FixExecutionReport report, TradeOrderStatus newStatus) {
		if (!newStatus.isFillComplete() || (!StringUtils.isEmpty(report.getExecutionReferenceId()) && (report.getOrderStatus() == OrderStatuses.FILLED))) {
			if ((report.getCumulativeQuantity() != null) && ((order.getQuantityFilled() == null) || report.getCumulativeQuantity().compareTo(order.getQuantityFilled()) > 0)) {
				order.setQuantityFilled(report.getCumulativeQuantity());

				BigDecimal price = report.getPriceType() != null ? report.getPriceType().calculateExecutionPrice(report) : report.getAveragePrice();
				order.setAveragePrice(price);
				// get the adjusted price
				if (order.getInvestmentSecurity() != null) {
					getTradeOrderAdjustmentHandler().applyAdjustments(order, getMarketDataSourceService().getMarketDataSourceName(order.getDestinationMapping().getExecutingBrokerCompany()));
				}
			}
		}
	}


	private TradeOrder getTradeOrderForExecutionReport(FixExecutionReport report) {
		TradeOrder order;
		if (report.getIdentifier().getSourceSystemFkFieldId() != null) {
			order = getTradeOrderService().getTradeOrder(MathUtils.getNumberAsInteger(report.getIdentifier().getSourceSystemFkFieldId()));
		}
		else {
			order = getTradeOrderService().getTradeOrderByExternalIdentifier(report.getIdentifier().getId());
		}
		// if it's a drop copy order create the order or throw an unknown trade report if it's not a drop copy
		if (order == null) {
			if (!report.getIdentifier().isDropCopyIdentifier()) {
				// if we don't have an order for the execution report send a reject
				getTradeOrderFixHandler().sendDontKnowTrade(report);
				throw new RuntimeException("Unable to find order [" + report.getIdentifier() + "] for execution report.  Sent reject message to counterparty.");
			}
			order = getFixExecutionReportTradeOrderConverter().convert(report);
		}
		return order;
	}


	private void processTradeQuotes(TradeOrder order, FixExecutionReport report) {
		if (CollectionUtils.isEmpty(report.getCompDealerQuoteList())) {
			return;
		}
		ValidationUtils.assertNotNull(order.getDestinationMapping().getExecutingBrokerCompany(), "A destination with an executing broker is required to process quotes.");
		for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
			TradeQuoteRequestSearchForm qrs = new TradeQuoteRequestSearchForm();
			qrs.setTradeId(trade.getId());
			TradeQuoteRequest request = CollectionUtils.getOnlyElement(getTradeQuoteService().getTradeQuoteRequestList(qrs));
			// create a quote for the winning broker
			request = handleCompDealerQuote(trade, order, report.getSecurityType(), request, order.getDestinationMapping().getExecutingBrokerCompany(), report.getAveragePrice(), null);
			// create the quotes for the loosing dealers
			for (FixCompDealerQuote fixQuote : CollectionUtils.getIterable(report.getCompDealerQuoteList())) {
				TradeDestination destination = order.getDestinationMapping().getTradeDestination();
				BusinessCompany executingBroker = getMarketDataSourceCompanyService().getMappedCompanyByCodeStrict(destination.getMarketDataSource(), destination.getMarketDataSourcePurpose(), fixQuote.getCompDealerId());
				request = handleCompDealerQuote(trade, order, report.getSecurityType(), request, executingBroker, fixQuote.getCompDealerQuotePrice(), fixQuote.getCompDealerQuotePriceType());
			}
		}
	}


	private TradeQuoteRequest handleCompDealerQuote(Trade trade, TradeOrder order, SecurityTypes fixSecurityType, TradeQuoteRequest request, BusinessCompany executingBroker, BigDecimal price,
	                                                CompDealerQuotePriceTypes priceType) {
		price = price == null ? BigDecimal.ZERO : price;
		InvestmentSecurity security = trade.getInvestmentSecurity();

		TradeQuoteSearchForm searchForm = new TradeQuoteSearchForm();
		searchForm.setExecutingBrokerId(executingBroker.getId());
		searchForm.setTradeId(trade.getId());
		TradeQuote quote = CollectionUtils.getOnlyElement(getTradeQuoteService().getTradeQuoteList(searchForm));
		if (quote == null) {
			if (request == null) {
				request = new TradeQuoteRequest();
				request.setTrade(trade);
				request.setInvestmentSecurity(trade.getInvestmentSecurity());
				request.setTradeDate(trade.getTradeDate());
			}
			quote = new TradeQuote();
			quote.setQuoteRequest(request);
			quote.setExecutingBroker(executingBroker);
		}
		if (priceType != null) {
			switch (priceType) {
				case DISCOUNT:
					if (MathUtils.isGreaterThan(price, BigDecimal.ZERO)) {
						Date periodStartDate = trade.getSettlementDate();
						Date accrueUpToDate = trade.getInvestmentSecurity().getEndDate();
						Date periodEndDate = DateUtils.addDays(accrueUpToDate, 1);

						CouponFrequencies frequency = CouponFrequencies.ANNUAL;
						String couponFrequency = getSecurityFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY);
						if (!StringUtils.isEmpty(couponFrequency)) {
							frequency = CouponFrequencies.getEnum(couponFrequency);
						}

						DayCountConventions dayCountConvention = DayCountConventions.getDayCountConventionByKey(getSecurityFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION));
						ValidationUtils.assertNotNull(dayCountConvention, "'" + InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION + "' is not defined for: " + security.getLabel());

						BigDecimal rate = dayCountConvention.calculateAccrualRate(DayCountCommand.forAccrualRange(periodStartDate, accrueUpToDate).withAccrualPeriod(periodStartDate, periodEndDate).withFrequency(frequency));
						price = MathUtils.subtract(new BigDecimal(100), MathUtils.multiply(price, rate));
					}
					break;
				case YIELD:
					ValidationUtils.assertTrue(SecurityTypes.INTEREST_STRIP_FROM_ANY_BOND_OR_NOTE == fixSecurityType //
							|| SecurityTypes.PRINCIPAL_STRIP_FROM_A_NON_CALLABLE_BOND_OR_NOTE == fixSecurityType //
							|| SecurityTypes.PRINCIPAL_STRIP_OF_A_CALLABLE_BOND_OR_NOTE == fixSecurityType, "Yield price calculation is not supported for fix security type [" + fixSecurityType
							+ "]");
					if (!MathUtils.isEqual(price, BigDecimal.ZERO)) {
						BigDecimal yieldPeriodCount = calculateStripsYieldPeriodCount(trade.getSettlementDate(), security.getEndDate());
						BigDecimal divisor = MathUtils.pow(MathUtils.add(BigDecimal.ONE, MathUtils.divide(MathUtils.divide(price, new BigDecimal(100)), new BigDecimal("2"))), yieldPeriodCount);
						if (MathUtils.isEqual(BigDecimal.ZERO, divisor)) {
							price = BigDecimal.ZERO;
						}
						else {
							price = MathUtils.divide(new BigDecimal(100), divisor);
						}
					}
					break;
				default:
					break;
			}
		}
		// need to use the order.isBuy because in some case, such as forwards, the order buy does not match the trade buy.
		if (order.isBuy()) {
			quote.setAskPrice(price);
			quote.setBidPrice(null);
		}
		else {
			quote.setAskPrice(null);
			quote.setBidPrice(price);
		}
		if (MathUtils.compare(price, BigDecimal.ZERO) == 0) {
			quote.setNote("Dealer declined to quote");
		}
		getTradeQuoteService().saveTradeQuote(quote);
		return quote.getQuoteRequest();
	}


	/**
	 * This will calculate period count for Yield->Price calculation for STRIPS.
	 * <p>
	 * The price calculation is 100/((1+r/2)^(n*2)) where r is the yield and n is periods to maturity.
	 * <p>
	 * n = daysInFirstAccrualPeriod/daysInFirstPeriod + 2*DateUtils.getDaysDifference(securityMaturityDate, nextAccrualDate)/365
	 *
	 * @param settlementDate
	 * @param securityMaturityDate
	 * @return the period count
	 */
	private BigDecimal calculateStripsYieldPeriodCount(Date settlementDate, Date securityMaturityDate) {
		int monthAdjustment = 6;

		Date previousAccrualDate = DateUtils.addYears(securityMaturityDate, DateUtils.getYearsDifference(settlementDate, securityMaturityDate));
		Date nextAccrualDate = DateUtils.addMonths(previousAccrualDate, monthAdjustment);

		if (DateUtils.isDateAfterOrEqual(previousAccrualDate, settlementDate)) {
			monthAdjustment = -monthAdjustment;
		}
		if (!DateUtils.isDateAfterOrEqual(nextAccrualDate, settlementDate) && DateUtils.isDateAfterOrEqual(settlementDate, previousAccrualDate)) {
			int safetyCount = 0;
			while (true) {
				previousAccrualDate = DateUtils.addMonths(previousAccrualDate, monthAdjustment);
				nextAccrualDate = DateUtils.addMonths(nextAccrualDate, monthAdjustment);
				if (DateUtils.isDateAfterOrEqual(nextAccrualDate, settlementDate) && !DateUtils.isDateAfterOrEqual(settlementDate, previousAccrualDate)) {
					break;
				}
				safetyCount++;
				if (safetyCount > 100 || DateUtils.isDateAfterOrEqual(nextAccrualDate, securityMaturityDate)) {
					throw new RuntimeException("Cannot find next accrual date after [" + DateUtils.fromDate(settlementDate) + "].");
				}
			}
		}
		int daysInFirstPeriod = DateUtils.getDaysDifference(nextAccrualDate, previousAccrualDate);
		int daysInFirstAccrualPeriod = DateUtils.getDaysDifference(nextAccrualDate, settlementDate);
		BigDecimal firstPeriodCount = MathUtils.divide(new BigDecimal(daysInFirstAccrualPeriod), new BigDecimal(daysInFirstPeriod));
		BigDecimal remainingYearCount = MathUtils.round(MathUtils.divide(new BigDecimal(DateUtils.getDaysDifference(securityMaturityDate, nextAccrualDate)), new BigDecimal(365)), 1,
				BigDecimal.ROUND_FLOOR);
		return MathUtils.add(firstPeriodCount, MathUtils.round(MathUtils.multiply(new BigDecimal(2), remainingYearCount), 0));
	}


	protected String getSecurityFieldValue(InvestmentSecurity security, String fieldName) {
		return (String) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, fieldName, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderAdjustmentHandler getTradeOrderAdjustmentHandler() {
		return this.tradeOrderAdjustmentHandler;
	}


	public void setTradeOrderAdjustmentHandler(TradeOrderAdjustmentHandler tradeOrderAdjustmentHandler) {
		this.tradeOrderAdjustmentHandler = tradeOrderAdjustmentHandler;
	}


	public FixExecutionReportTradeOrderConverter getFixExecutionReportTradeOrderConverter() {
		return this.fixExecutionReportTradeOrderConverter;
	}


	public void setFixExecutionReportTradeOrderConverter(FixExecutionReportTradeOrderConverter fixExecutionReportTradeOrderConverter) {
		this.fixExecutionReportTradeOrderConverter = fixExecutionReportTradeOrderConverter;
	}


	public TradeQuoteService getTradeQuoteService() {
		return this.tradeQuoteService;
	}


	public void setTradeQuoteService(TradeQuoteService tradeQuoteService) {
		this.tradeQuoteService = tradeQuoteService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public MarketDataSourceCompanyService getMarketDataSourceCompanyService() {
		return this.marketDataSourceCompanyService;
	}


	public void setMarketDataSourceCompanyService(MarketDataSourceCompanyService marketDataSourceCompanyService) {
		this.marketDataSourceCompanyService = marketDataSourceCompanyService;
	}


	public TimedEvictionCache<?> getTradeOrderTimedStatusCache() {
		return this.tradeOrderTimedStatusCache;
	}


	public void setTradeOrderTimedStatusCache(TimedEvictionCache<?> tradeOrderTimedStatusCache) {
		this.tradeOrderTimedStatusCache = tradeOrderTimedStatusCache;
	}
}
