package com.clifton.trade.order.fix;


import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderIdentityProvider;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>TradeOrderAllocationIdentityProvider</code> implements FixOrderAllocationIdentityProvider used by the FIX services to look up
 * the list of allocation id's for an order.
 *
 * @author mwacker
 */
@Component("tradeOrderFixOrderIdentityProvider")
public class TradeOrderIdentityProvider implements TradeOrderFixOrderIdentityProvider {

	private TradeOrderService tradeOrderService;


	@Override
	public List<Integer> getExternalIdentityListByOrder(Integer externalOrderIdentifier) {
		TradeOrder order = getTradeOrderService().getTradeOrderByExternalIdentifier(externalOrderIdentifier);
		if (order != null) {
			List<Integer> result = new ArrayList<>();
			if (order.getOrderGroup() != null) {
				TradeOrderSearchForm searchForm = new TradeOrderSearchForm();
				searchForm.setOrderGroup(order.getOrderGroup());
				List<TradeOrder> tradeOrderList = getTradeOrderService().getTradeOrderList(searchForm);
				for (TradeOrder tradeOrder : CollectionUtils.getIterable(tradeOrderList)) {
					if ((tradeOrder.getExternalIdentifier() != null) && !result.contains(tradeOrder.getExternalIdentifier())) {
						result.add(tradeOrder.getExternalIdentifier());
					}
				}
			}
			else {
				recurseOrders(order, result);
			}
			return result;
		}
		return null;
	}


	private void recurseOrders(TradeOrder order, List<Integer> result) {
		if (order.getOriginalTradeOrder() != null) {
			recurseOrders(order.getOriginalTradeOrder(), result);
		}
		result.add(order.getExternalIdentifier());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}
}
