package com.clifton.trade.order.fix.util;


import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurpose;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.order.TradeOrder;

import java.util.List;


/**
 * The <code>TradeOrderFixUtilHandler</code> defines utility methods used to look common items for TradeOrders and FixEntities.
 *
 * @author mwacker
 */
public interface TradeOrderFixUtilHandler {

	/**
	 * Finds the executing broker from the fix party list and uses the party id as the broker code to lookup the trade destination mapping.
	 */
	public TradeDestinationMapping getTradeDestinationMappingFromFixPartyList(TradeDestinationMappingAccountRelationshipCommand command);


	/**
	 * Finds the trade destination mapping for the order using the provided broker code.
	 */
	public TradeDestinationMapping getTradeDestinationMapping(TradeOrder order, FixEntity entity, String brokerCode, boolean exceptionIfMoreThanOneOrNone);


	/**
	 * Gets the trade destination connection for specific FIX entity.  This will cache the connect by entity id.
	 */
	public TradeDestinationConnection getTradeDestinationConnection(FixEntity entity);


	/**
	 * Finds the <code>InvestmentSecurity</code> object for the <code>FixSecurityEntity</code>.
	 */
	public InvestmentSecurity getInvestmentSecurity(FixSecurityEntity entity);


	/**
	 * Finds the traders <code>InvestmentSecurity</code> object.  If a party list is provided, it
	 * will check all parties of type ORDER_ORIGINATION_TRADER and return the first one the maps
	 * to a security user.
	 *
	 * @param entity
	 * @param fixPartyList - optional party list
	 */
	public SecurityUser getTraderSecurityUser(FixEntity entity, List<FixParty> fixPartyList);


	/**
	 * Get the list of list of executing brokers a FixParty objects.
	 * <p/>
	 * NOTE:  This list will always have 1 entry, unless connection is set to send a restrictive broker list
	 * then it will have an entry for each broker that order can be executed with.
	 */
	public List<FixParty> getFixPartyList(TradeOrder order);


	/**
	 * Returns the exchange code for the security on the order.
	 */
	public String getExchangeCode(TradeOrder order, boolean requireDataSourceCode);


	public String getExchangeMICCode(TradeOrder order);


	public String getFixTopAccountNumber(TradeOrder tradeOrder);


	/**
	 * Gets the value for the top account (fix tag 1) of a FIX TradeOrder.  This facility checks for translated values for the holding account in the first trade, and returns
	 * that value as the top account.  If no mapping is found, returns the FixPostTradeAllocationTopAccountNumber in the account mapping.
	 * Note that for an account grouping within the TradeOrder, all accounts with mapped values will have the same mapped value and same mapping purpose.
	 */
	public String getFixSingleOrderAllocationAccountNumber(TradeOrder tradeOrder);


	/**
	 * Gets the value for the allocation account to be used in a FIX TradeOrder (allocation details).  This facility checks for mapped account values using the allocation account
	 * number and the mapping purpose.  If a mapping is found, the mapped value is returned otherwise the allocation account number is returned.
	 */
	public String getAllocationAccountNumber(InvestmentAccount allocationAccount, InvestmentAccountMappingPurpose purpose);
}
