package com.clifton.trade.order.fix.message;

import com.clifton.core.util.event.EventHandler;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetailProvider;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.event.TradeOrderCancellationEvent;
import com.clifton.trade.order.fix.TradeOrderFixHandler;
import com.clifton.trade.order.fix.util.TradeOrderFixUtilHandler;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;


/**
 * @author mwacker
 */
public abstract class AbstractTradeOrderFixMessageHandler<T extends FixEntity> implements TradeFixMessageHandler<T, TradeOrderProcessorParameters> {

	private TradeOrderService tradeOrderService;
	private TradeOrderAllocationService tradeOrderAllocationService;
	private TradeOrderFixHandler tradeOrderFixHandler;
	private TradeOrderFixUtilHandler tradeOrderFixUtilHandler;
	private MarketDataSourceService marketDataSourceService;
	private EventHandler eventHandler;

	/**
	 * Used to get allocation details.  This might eventually be a list of provider
	 * but for now there is on FIX so we'll leave it as a single provider for now.
	 */
	private TradeOrderAllocationDetailProvider tradeOrderAllocationDetailProvider;

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	/**
	 * Once a validation fails, it is never reset.
	 */
	protected void saveOrderWithValidationStatus(TradeOrder order, boolean validationError, boolean allocationValidationError) {
		order.setValidationError(order.isValidationError() || validationError);
		order.setAllocationValidationError(order.isAllocationValidationError() || allocationValidationError);
		getTradeOrderService().saveTradeOrder(order);
	}


	protected void saveOrderWithStatus(TradeOrder order, TradeOrderStatus status, TradeOrderAllocationStatus allocationStatus) {
		TradeOrderStatus newStatus = getOrderStatus(status);
		if (newStatus == null) {
			throw new RuntimeException("Cannot find status [" + status + "] for order [" + order + "].");
		}
		TradeOrderAllocationStatus newAllocationStatus = null;
		if (allocationStatus != null) {
			newAllocationStatus = getOrderAllocationStatus(allocationStatus);
		}
		TradeOrderStatuses originalStatus = order.getStatus().getOrderStatus();
		order.setStatus(newStatus);
		order.setAllocationStatus(newAllocationStatus);
		getTradeOrderService().saveTradeOrder(order);
		// send cancellation event if applicable
		if (TradeOrderCancellationEvent.isCancellationStatus(originalStatus, status.getOrderStatus(), order.isActiveOrder())) {
			getEventHandler().raiseEvent(new TradeOrderCancellationEvent(order));
		}
	}


	private TradeOrderStatus getOrderStatus(TradeOrderStatus status) {
		return getTradeOrderService().getTradeOrderStatusByStatuses(status.getOrderStatus());
	}


	private TradeOrderAllocationStatus getOrderAllocationStatus(TradeOrderAllocationStatus status) {
		return getTradeOrderAllocationService().getTradeOrderAllocationStatusByStatuses(status.getAllocationStatus(), status.isAllocationFillReceived());
	}


	protected TradeOrderAllocationStatuses getTradeOrderAllocationStatus(AllocationStatuses allocationStatus) {
		return TradeOrderAllocationStatuses.valueOf(allocationStatus.toString());
	}

	///////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderFixHandler getTradeOrderFixHandler() {
		return this.tradeOrderFixHandler;
	}


	public void setTradeOrderFixHandler(TradeOrderFixHandler tradeOrderFixHandler) {
		this.tradeOrderFixHandler = tradeOrderFixHandler;
	}


	public TradeOrderFixUtilHandler getTradeOrderFixUtilHandler() {
		return this.tradeOrderFixUtilHandler;
	}


	public void setTradeOrderFixUtilHandler(TradeOrderFixUtilHandler tradeOrderFixUtilHandler) {
		this.tradeOrderFixUtilHandler = tradeOrderFixUtilHandler;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}


	public TradeOrderAllocationDetailProvider getTradeOrderAllocationDetailProvider() {
		return this.tradeOrderAllocationDetailProvider;
	}


	public void setTradeOrderAllocationDetailProvider(TradeOrderAllocationDetailProvider tradeOrderAllocationDetailProvider) {
		this.tradeOrderAllocationDetailProvider = tradeOrderAllocationDetailProvider;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}
