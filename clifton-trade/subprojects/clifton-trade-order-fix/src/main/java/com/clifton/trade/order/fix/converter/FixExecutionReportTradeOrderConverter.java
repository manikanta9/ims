package com.clifton.trade.order.fix.converter;


import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.fix.util.TradeDestinationMappingAccountRelationshipCommand;
import com.clifton.trade.order.fix.util.TradeOrderFixUtilHandler;
import org.springframework.stereotype.Component;


/**
 * The <code>FixExecutionReportTradeOrderConverter</code> creates a new TradeOrder object from a
 * FixExecutionReport.
 * <p>
 * NOTE: This is used when the incoming execution report is a drop copy message.
 *
 * @author mwacker
 */
@Component
public class FixExecutionReportTradeOrderConverter implements Converter<FixExecutionReport, TradeOrder> {

	private TradeOrderService tradeOrderService;
	private TradeOrderFixUtilHandler tradeOrderFixUtilHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeOrder convert(FixExecutionReport from) {
		ValidationUtils.assertTrue(from.getIdentifier().isDropCopyIdentifier(), "[" + this.getClass() + "] only supports converting drop copy messages.");

		TradeOrder order = new TradeOrder();

		// set the investment security
		order.setInvestmentSecurity(getTradeOrderFixUtilHandler().getInvestmentSecurity(from));
		ValidationUtils.assertNotNull(order.getInvestmentSecurity(), "Cannot find security for symbol [" + from.getSecurityId() + "] of type [" + from.getSecurityIDSource() + "].");

		// set the general fields
		order.setQuantity(from.getOrderQuantity());
		order.setBuy(from.getSide() == OrderSides.BUY);
		order.setTradeDate(from.getTradeDate());

		// set the security user
		order.setTraderUser(getTradeOrderFixUtilHandler().getTraderSecurityUser(from, from.getPartyList()));

		// set the trade destination mapping
		TradeDestinationMappingAccountRelationshipCommand command = new TradeDestinationMappingAccountRelationshipCommand(order, from, from.getPartyList(), true);
		order.setDestinationMapping(getTradeOrderFixUtilHandler().getTradeDestinationMappingFromFixPartyList(command));

		FixParty broker = from.getPartyList().stream().filter((FixParty party) -> party.getPartyRole() == PartyRoles.EXECUTING_FIRM).findFirst().orElse(null);
		ValidationUtils.assertNotNull(order.getDestinationMapping(), "Could not find trade destination mapping " + (from.getIdentifier().isDropCopyIdentifier() ? "for FIX drop copy " : "") + "message with broker code [" + (broker != null ? broker.getPartyId() : "null") + "].  Order security is [" + order.getInvestmentSecurity() + "].");


		order.setExternalIdentifier(from.getIdentifier().getId());

		// set trade order type
		order.setType(getTradeOrderService().getTradeOrderTypeByName(TradeOrderService.DROP_COPY_TRADE_ORDER_TYPE_NAME));

		order.setIndexRatio(from.getIndexRatio());

		TradeOrderStatus status = getTradeOrderService().getTradeOrderStatusByStatuses(TradeOrderStatuses.NEW);
		order.setStatus(status);

		return order;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderFixUtilHandler getTradeOrderFixUtilHandler() {
		return this.tradeOrderFixUtilHandler;
	}


	public void setTradeOrderFixUtilHandler(TradeOrderFixUtilHandler tradeOrderFixUtilHandler) {
		this.tradeOrderFixUtilHandler = tradeOrderFixUtilHandler;
	}
}
