package com.clifton.trade.order.fix;


import com.clifton.core.beans.BeanUtils;
import com.clifton.fix.order.FixOrder;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderExecutionService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * The <code>TradeOrderFixExecutionService</code> executes any FIX orders in the provided list.
 *
 * @author mwacker
 */
@Component("tradeOrderExecutionService")
public class TradeOrderFixExecutionService implements TradeOrderExecutionService<FixOrder> {

	private TradeOrderFixHandler tradeOrderFixHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void executeTradeOrderList(List<TradeOrder> orderList) {
		executeTradeOrderTargetObjectMap(createTradeOrderToTargetObjectMap(orderList));
	}


	@Override
	public Map<TradeOrder, FixOrder> createTradeOrderToTargetObjectMap(List<TradeOrder> orderList) {
		return getTradeOrderFixHandler().createFixTradeOrderMap(BeanUtils.filter(orderList, o -> o.getDestinationMapping().getTradeDestination().getType().isFixTradeOnly()));
	}


	@Override
	public void executeTradeOrderTargetObjectMap(Map<TradeOrder, FixOrder> fixTradeOrderMap) {
		getTradeOrderFixHandler().executeFixTradeOrderMap(fixTradeOrderMap);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderFixHandler getTradeOrderFixHandler() {
		return this.tradeOrderFixHandler;
	}


	public void setTradeOrderFixHandler(TradeOrderFixHandler tradeOrderFixHandler) {
		this.tradeOrderFixHandler = tradeOrderFixHandler;
	}
}
