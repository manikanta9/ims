package com.clifton.trade.order.fix.message.validation;

import com.clifton.fix.message.validation.FixMessageValidationService;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <code>TradeOrderFixMessageValidationServiceImpl</code> client-side implementation of the fix validation service
 * interface, creates a request object, places it on the JMS queue and retrieves the server generated
 * response.  Provides an entry point to web clients to perform message validations.
 */
@Service
public class TradeOrderFixMessageValidationServiceImpl implements TradeOrderFixMessageValidationService {


	private TradeOrderFixOrderService tradeOrderFixOrderService;

	private FixMessageValidationService fixMessageValidationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixValidationResult> getTradeOrderFixMessageValidationsForTradeOrderList(Integer externalOrderIdentifier) {
		List<Integer> externalOrderIdentifierList = getTradeOrderFixOrderService().getTradeOrderExternalIdentityListByOrder(externalOrderIdentifier);
		return getFixMessageValidationService().validateFixMessageForOrderList(externalOrderIdentifierList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageValidationService getFixMessageValidationService() {
		return this.fixMessageValidationService;
	}


	public void setFixMessageValidationService(FixMessageValidationService fixMessageValidationService) {
		this.fixMessageValidationService = fixMessageValidationService;
	}


	public TradeOrderFixOrderService getTradeOrderFixOrderService() {
		return this.tradeOrderFixOrderService;
	}


	public void setTradeOrderFixOrderService(TradeOrderFixOrderService tradeOrderFixOrderService) {
		this.tradeOrderFixOrderService = tradeOrderFixOrderService;
	}
}
