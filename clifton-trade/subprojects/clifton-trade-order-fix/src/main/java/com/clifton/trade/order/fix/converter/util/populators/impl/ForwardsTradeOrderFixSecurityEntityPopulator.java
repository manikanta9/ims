package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Forwards trades.
 * InvestmentType = Forwards
 *
 * @author davidi
 */
@Component
public class ForwardsTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	public ForwardsTradeOrderFixSecurityEntityPopulator() {
		setOrder(70);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return (tradeOrder.getOriginalInvestmentSecurity() != null) && InvestmentUtils.isSecurityOfType(tradeOrder.getOriginalInvestmentSecurity(), InvestmentType.FORWARDS);
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		final InvestmentSecurity originalSecurity = tradeOrder.getOriginalInvestmentSecurity();

		String ccy1 = tradeOrder.getInvestmentSecurity().getSymbol();
		String ccy2;
		if (tradeOrder.getInvestmentSecurity().equals(originalSecurity.getInstrument().getTradingCurrency())) {
			ccy2 = originalSecurity.getInstrument().getUnderlyingInstrument().getIdentifierPrefix();
		}
		else {
			ccy2 = originalSecurity.getInstrument().getTradingCurrency().getSymbol();
		}
		securityEntity.setSymbol(ccy1 + "/" + ccy2);
		securityEntity.setCurrency(ccy1);
		securityEntity.setSettlementCurrency(ccy2);
		securityEntity.setExDestination(null);
	}
}
