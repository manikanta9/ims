package com.clifton.trade.order.fix.order.allocation;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.order.FixEntityService;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class TradeOrderFixOrderAllocationServiceImpl implements TradeOrderFixOrderAllocationService {

	private FixEntityService fixEntityService;

	private TradeOrderFixOrderService tradeOrderFixOrderService;
	private TradeOrderFixOrderAllocationIdentityProvider tradeOrderFixOrderAllocationIdentityProvider;


	////////////////////////////////////////////////////////////////////////////
	////////       FixAllocationInstruction Business Methods         ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationInstruction getTradeOrderFixAllocationInstruction(Integer externalAllocationIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalAllocationIdentifier, "An external order id is required.");

		List<FixAllocationInstruction> allocationList = getFixEntityService().getFixEntityListForIdentifierAndType(externalAllocationIdentifier, FixMessageTypeTagValues.ALLOCATION_INSTRUCTION_TYPE_TAG.getTagValue());
		if (fullyPopulated) {
			populateFixAllocationInstruction(CollectionUtils.getOnlyElement(allocationList), false);
		}
		return CollectionUtils.getOnlyElement(allocationList);
	}


	private void populateFixAllocationInstruction(FixAllocationInstruction allocation, boolean fullyPopulated) {
		List<FixOrder> newOrderList = new ArrayList<>();
		for (FixOrder order : CollectionUtils.getIterable(allocation.getFixOrderList())) {
			newOrderList.add(getTradeOrderFixOrderService().getTradeOrderFixOrder(order.getIdentifier().getId(), fullyPopulated));
		}
		allocation.setFixOrderList(newOrderList);
		if (allocation.getIdentifier().getReferencedIdentifier() != null) {
			allocation.setFixReferenceAllocation(getTradeOrderFixAllocationInstruction(allocation.getIdentifier().getReferencedIdentifier().getId(), fullyPopulated));
		}
		// TODO: support allocation linking
		//		if (allocation.getAllocationLinkId() != null) {
		//			allocation.setFixAllocationLink(getFixAllocationInstruction(allocation.getAllocationLinkId(), fullyPopulated));
		//		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////     FixAllocationAcknowledgement Business Methods       ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixAllocationAcknowledgement> getTradeOrderFixAllocationAcknowledgementList(Integer externalAllocationIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalAllocationIdentifier, "An external order id is required.");

		List<FixAllocationAcknowledgement> reportList = getFixEntityService().getFixEntityListForIdentifierAndType(externalAllocationIdentifier, FixMessageTypeTagValues.ALLOCATION_INSTRUCTION_ACK_TYPE_TAG.getTagValue());
		if (fullyPopulated) {
			for (FixAllocationAcknowledgement report : CollectionUtils.getIterable(reportList)) {
				populateFixAllocationAcknowledgement(report, false);
			}
		}
		return reportList;
	}


	@Override
	public List<FixAllocationAcknowledgement> getTradeOrderFixAllocationAcknowledgementListByOrder(Integer externalOrderIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<Integer> allocationIds = getTradeOrderFixOrderAllocationIdentityProvider().getAllocationExternalIdentityListByOrder(externalOrderIdentifier);
		if (!CollectionUtils.isEmpty(allocationIds)) {
			List<FixAllocationAcknowledgement> ackList = getFixEntityService().getFixEntityListForIdentifierListAndType(allocationIds, FixMessageTypeTagValues.ALLOCATION_INSTRUCTION_ACK_TYPE_TAG.getTagValue());
			if (fullyPopulated) {
				for (FixAllocationAcknowledgement report : CollectionUtils.getIterable(ackList)) {
					populateFixAllocationAcknowledgement(report, false);
				}
			}
			return ackList;
		}
		return null;
	}


	private void populateFixAllocationAcknowledgement(FixAllocationAcknowledgement allocationAck, boolean fullyPopulated) {
		allocationAck.setFixAllocationInstruction(getTradeOrderFixAllocationInstruction(allocationAck.getIdentifier().getId(), fullyPopulated));
	}


	////////////////////////////////////////////////////////////////////////////
	////////          FixAllocationReport Business Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixAllocationReport> getTradeOrderFixAllocationReportListByAllocation(Integer externalAllocationIdentifier, boolean fullyPopulated) {
		List<FixAllocationReport> reportList = getFixEntityService().getFixEntityListForIdentifierAndType(externalAllocationIdentifier, FixMessageTypeTagValues.ALLOCATION_REPORT_TYPE_TAG.getTagValue());
		if (fullyPopulated) {
			for (FixAllocationReport report : CollectionUtils.getIterable(reportList)) {
				populateFixAllocationReport(report, true);
			}
		}
		return reportList;
	}


	@Override
	public List<FixAllocationReport> getTradeOrderFixAllocationReportListByOrder(Integer externalOrderIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<Integer> allocationIds = getTradeOrderFixOrderAllocationIdentityProvider().getAllocationExternalIdentityListByOrder(externalOrderIdentifier);
		if (!CollectionUtils.isEmpty(allocationIds)) {
			List<FixAllocationReport> reportList = getFixEntityService().getFixEntityListForIdentifierListAndType(allocationIds, FixMessageTypeTagValues.ALLOCATION_REPORT_TYPE_TAG.getTagValue());
			if (fullyPopulated) {
				for (FixAllocationReport report : CollectionUtils.getIterable(reportList)) {
					populateFixAllocationReport(report, true);
				}
			}
			return reportList;
		}
		return null;
	}


	private void populateFixAllocationReport(FixAllocationReport report, boolean fullyPopulated) {
		report.setFixAllocationInstruction(getTradeOrderFixAllocationInstruction(report.getIdentifier().getId(), fullyPopulated));
	}


	////////////////////////////////////////////////////////////////////////////
	////////     FixAllocationReportDetailList Business Methods      ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixAllocationReportDetail> getTradeOrderFixAllocationReportDetailListByAllocation(Integer externalAllocationIdentifier, @SuppressWarnings("unused") boolean fullyPopulated) {
		List<FixAllocationReport> allocationReportList = getTradeOrderFixAllocationReportListByAllocation(externalAllocationIdentifier, false);
		return buildAllocationReportDetailList(allocationReportList);
	}


	@Override
	public List<FixAllocationReportDetail> getTradeOrderFixAllocationReportDetailListByOrder(Integer externalOrderIdentifier, @SuppressWarnings("unused") boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<FixAllocationReport> allocationReportList = getTradeOrderFixAllocationReportListByOrder(externalOrderIdentifier, false);
		return buildAllocationReportDetailList(allocationReportList);
	}


	private List<FixAllocationReportDetail> buildAllocationReportDetailList(List<FixAllocationReport> allocationReportList) {
		List<FixAllocationReportDetail> result = new ArrayList<>();
		for (FixAllocationReport report : CollectionUtils.getIterable(allocationReportList)) {
			result.addAll(report.getFixAllocationReportDetailList());
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}


	public TradeOrderFixOrderService getTradeOrderFixOrderService() {
		return this.tradeOrderFixOrderService;
	}


	public void setTradeOrderFixOrderService(TradeOrderFixOrderService tradeOrderFixOrderService) {
		this.tradeOrderFixOrderService = tradeOrderFixOrderService;
	}


	public TradeOrderFixOrderAllocationIdentityProvider getTradeOrderFixOrderAllocationIdentityProvider() {
		return this.tradeOrderFixOrderAllocationIdentityProvider;
	}


	public void setTradeOrderFixOrderAllocationIdentityProvider(TradeOrderFixOrderAllocationIdentityProvider tradeOrderFixOrderAllocationIdentityProvider) {
		this.tradeOrderFixOrderAllocationIdentityProvider = tradeOrderFixOrderAllocationIdentityProvider;
	}
}
