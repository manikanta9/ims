package com.clifton.trade.order.fix.converter;


import com.clifton.fix.order.FixOrder;
import com.clifton.trade.order.TradeOrder;
import org.springframework.stereotype.Component;


/**
 * The <code>TradeOrderFixOrderConverter</code> defines a TradeOrder to FixOrder converter.
 *
 * @author mwacker
 */
@Component
public class TradeOrderFixOrderConverter extends AbstractTradeOrderFixOrderConverter<FixOrder> {

	@Override
	public FixOrder convert(TradeOrder tradeOrder) {
		FixOrder order = new FixOrder();
		return super.convert(tradeOrder, order);
	}
}
