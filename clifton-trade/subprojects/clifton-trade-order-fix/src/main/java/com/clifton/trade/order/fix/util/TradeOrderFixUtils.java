package com.clifton.trade.order.fix.util;


import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.destination.connection.TradeDestinationConnectionSearchForm;


public class TradeOrderFixUtils {

	public static <T extends FixEntity> T setEntityItems(T entity, TradeDestinationMapping mapping) {
		return setEntityItems(entity, mapping, false);
	}


	public static <T extends FixEntity> T setEntityItems(T entity, TradeDestinationMapping mapping, boolean includeOnBehalfOf) {
		TradeDestinationConnection connection = mapping.getConnection();
		return setEntityItems(entity, connection, mapping.getTradeDestination().getFixTargetSubId(), includeOnBehalfOf);
	}


	public static <T extends FixEntity> T setEntityItemsForAllocations(T entity, TradeDestinationMapping mapping, boolean includeOnBehalfOf) {
		TradeDestinationConnection connection = mapping.getConnection();
		TradeDestinationConnection allocationConnection = ObjectUtils.coalesce(connection.getAllocationConnection(), connection);
		return setEntityItems(entity, allocationConnection, mapping.getTradeDestination().getFixTargetSubId(), includeOnBehalfOf);
	}


	public static TradeDestinationConnectionSearchForm getTradeDestinationConnectionSearchForm(FixEntity entity) {
		TradeDestinationConnectionSearchForm result = new TradeDestinationConnectionSearchForm();
		if (!StringUtils.isEmpty(entity.getFixDestinationName())) {
			result.setFixDestinationNameEquals(entity.getFixDestinationName());
		}
		else if (entity.getIdentifier() != null && entity.getIdentifier().getFixDestination() != null) {
			result.setFixDestinationNameEquals(entity.getIdentifier().getFixDestination().getName());
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static <T extends FixEntity> T setEntityItems(T entity, TradeDestinationConnection connection, String fixTargetSubId, boolean includeOnBehalfOf) {
		if (!StringUtils.isEmpty(connection.getFixDestinationName())) {
			entity.setFixDestinationName(connection.getFixDestinationName());
		}
		entity.setFixTargetSubId(fixTargetSubId);

		if (includeOnBehalfOf) {
			entity.setFixOnBehalfOfCompID(connection.getFixOnBehalfOfCompId());
		}
		return entity;
	}
}
