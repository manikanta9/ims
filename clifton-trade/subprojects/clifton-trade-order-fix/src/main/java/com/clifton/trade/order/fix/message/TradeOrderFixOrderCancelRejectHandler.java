package com.clifton.trade.order.fix.message;

import com.clifton.core.beans.BeanUtils;
import com.clifton.fix.order.FixOrderCancelReject;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderStatus;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.processor.TradeOrderProcessorActonTypes;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class TradeOrderFixOrderCancelRejectHandler extends AbstractTradeOrderFixMessageHandler<FixOrderCancelReject> {

	@Override
	public Class<FixOrderCancelReject> getMessageClass() {
		return FixOrderCancelReject.class;
	}


	@Override
	public TradeOrderProcessorParameters handleMessage(FixOrderCancelReject entity) {
		TradeOrder order = getTradeOrderService().getTradeOrder(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
		TradeOrderStatus status = BeanUtils.cloneBean(order.getStatus(), false, false);
		status.setId(null);

		if (!entity.isOrderCancelReplaceReject()) {
			if (entity.getStatus() != OrderStatuses.REJECTED) {
				status.setOrderStatus(TradeOrderStatuses.getTradeOrderStatus(order, entity.getStatus()));
			}
		}
		else {
			// TODO: what to do with cancel replace rejects?
			status.setOrderStatus(TradeOrderStatuses.getTradeOrderStatus(order, OrderStatuses.REJECTED));
		}

		saveOrderWithStatus(order, status, order.getAllocationStatus());
		saveOrderWithValidationStatus(order, entity.isValidationError(), entity.isAllocationValidationError());
		return new TradeOrderProcessorParameters(order, TradeOrderProcessorActonTypes.PROCESS_ORDER);
	}
}
