package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Currency trades.
 * InvestmentType = Currency
 *
 * @author davidi
 */
@Component
public class CurrencyTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	public CurrencyTradeOrderFixSecurityEntityPopulator() {
		setOrder(80);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.CURRENCY);
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		String ccy1 = tradeOrder.getInvestmentSecurity().getSymbol();
		String ccy2 = firstTrade.getPayingSecurity().getSymbol();
		securityEntity.setSymbol(ccy1 + "/" + ccy2);
		securityEntity.setCurrency(ccy1);
		securityEntity.setSettlementCurrency(ccy2);
		securityEntity.setExDestination(null);
	}
}
