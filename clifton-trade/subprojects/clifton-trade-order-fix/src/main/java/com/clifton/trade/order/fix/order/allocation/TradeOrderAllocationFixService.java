package com.clifton.trade.order.fix.order.allocation;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface TradeOrderAllocationFixService {

	/**
	 * Send the order allocation message.
	 */
	@DoNotAddRequestMapping
	public void sendTradeOrderFixAllocationInstruction(List<TradeOrder> orderList);


	/**
	 * Using a submitted trade order object that has the update fills for each trade
	 * generate and send the allocation.
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixAllocationInstructionPartiallyFilledSend")
	public void sendTradeOrderFixAllocationInstructionPartiallyFilled(TradeOrder bean);


	/**
	 * Calculates proportional trade quantities for trades within a partially filled order. Returns the trades
	 * with updated quantity values, whose sum match the order's fill quantity.
	 */
	@RequestMapping("tradeOrderTradeProportionalQuantityCalculate")
	public List<Trade> calculateTradeOrderTradeProportionalQuantity(int tradeOrderId);


	/**
	 * Re-send the order allocation message.
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixAllocationInstructionResend")
	public void resendTradeOrderFixAllocationInstruction(int tradeOrderId);


	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixAllocationInstructionCancel")
	public void cancelTradeOrderFixAllocationInstruction(int tradeOrderId);
}
