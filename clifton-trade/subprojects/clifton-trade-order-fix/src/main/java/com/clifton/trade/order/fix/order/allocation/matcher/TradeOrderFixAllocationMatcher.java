package com.clifton.trade.order.fix.order.allocation.matcher;


import com.clifton.core.beans.BeanUtils.BeanPropertyRetriever;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.matching.AbstractManyToManyMatcher;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.core.util.matching.PermutationScorer;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>TradeOrderFixAllocationMatcher</code> many to many matcher for Fix Execution Reports
 *
 * @author mwacker
 */
public class TradeOrderFixAllocationMatcher extends AbstractManyToManyMatcher<FixAllocationDetail, FixExecutionReport> {

	private BigDecimal averagePriceThresholdPercent = new BigDecimal("0.000001");

	private static final BeanPropertyRetriever<FixAllocationDetail, BigDecimal> INTERNAL_QTY_RETRIEVER = FixAllocationDetail::getAllocationShares;
	private static final BeanPropertyRetriever<FixExecutionReport, BigDecimal> EXTERNAL_QTY_RETRIEVER = FixExecutionReport::getLastShares;
	private static final BeanPropertyRetriever<FixAllocationDetail, BigDecimal> INTERNAL_PRICE_RETRIEVER = bean -> bean.getFixAllocationInstruction().getAveragePrice();
	private static final BeanPropertyRetriever<FixExecutionReport, BigDecimal> EXTERNAL_PRICE_RETRIEVER = FixExecutionReport::getLastPrice;


	public TradeOrderFixAllocationMatcher() {
		//
	}


	public TradeOrderFixAllocationMatcher(BigDecimal averagePriceThresholdPercent) {
		this.averagePriceThresholdPercent = averagePriceThresholdPercent;
	}


	@Override
	protected PermutationScorer<FixAllocationDetail> getInternalPermutationScorer() {
		return null;
	}


	@Override
	protected PermutationScorer<FixExecutionReport> getExternalPermutationScorer() {
		return null;
	}


	/**
	 * Get the score for the complete result, which is sum of item results.
	 * <p>
	 * NOTE: -1 means this option is not viable.
	 */
	@Override
	protected int getMatchResultScore(MatchingResult<FixAllocationDetail, FixExecutionReport> match) {
		int result = -1;
		boolean allHaveSideWithOne = true;
		for (MatchingResultItem<FixAllocationDetail, FixExecutionReport> item : CollectionUtils.getIterable(match.getItemList())) {
			int itemScore = getMatchResultItemScore(item);
			if (itemScore == -1) {
				return -1;
			}
			if (result == -1) {
				result = 0;
			}
			result += itemScore;

			if (item.getExternalList().size() != 1 && item.getInternalList().size() != 1) {
				allHaveSideWithOne = false;
			}
		}
		return allHaveSideWithOne ? 0 : result;
	}


	/**
	 * Get the score to sub match, 1 indicates a good match and -1 indicates a bad match.
	 * <p>
	 * NOTE: The the total if the quantities don't match or the average price difference is out side the threshold return -1 to
	 * indicate it's not a viable match.
	 */
	@Override
	protected int getMatchResultItemScore(MatchingResultItem<FixAllocationDetail, FixExecutionReport> matchItem) {
		BigDecimal iq = getQuantity(matchItem.getInternalList(), INTERNAL_QTY_RETRIEVER);
		BigDecimal eq = getQuantity(matchItem.getExternalList(), EXTERNAL_QTY_RETRIEVER);

		if (MathUtils.compare(iq, eq) != 0) {
			return -1;
		}
		BigDecimal internalAveragePrice = getAveragePrice(iq, matchItem.getInternalList(), INTERNAL_PRICE_RETRIEVER, INTERNAL_QTY_RETRIEVER);
		BigDecimal externalAveragePrice = getAveragePrice(eq, matchItem.getExternalList(), EXTERNAL_PRICE_RETRIEVER, EXTERNAL_QTY_RETRIEVER);
		BigDecimal threshold = MathUtils.multiply(internalAveragePrice, getAveragePriceThresholdPercent());
		BigDecimal priceDiff = MathUtils.abs(MathUtils.subtract(internalAveragePrice, externalAveragePrice));
		if (MathUtils.compare(threshold, priceDiff) < 1) {
			return -1;
		}
		return 1;
	}


	/**
	 * Get the total quantity from the list.
	 *
	 * @param entityList
	 */
	private <T> BigDecimal getQuantity(List<T> entityList, BeanPropertyRetriever<T, BigDecimal> retriever) {
		BigDecimal quantity = BigDecimal.ZERO;
		for (T entry : CollectionUtils.getIterable(entityList)) {
			quantity = MathUtils.add(quantity, retriever.getPropertyValue(entry));
		}
		return quantity;
	}


	/**
	 * Average price methods broker into 2, because profiler revealed that BeanUtils.getPropertyValue was 75% of the processing time
	 * when getting the score.
	 *
	 * @param entityList
	 */
	private <T> BigDecimal getAveragePrice(BigDecimal quantity, List<T> entityList, BeanPropertyRetriever<T, BigDecimal> priceRetriever, BeanPropertyRetriever<T, BigDecimal> quantityRetriever) {
		BigDecimal result = BigDecimal.ZERO;
		for (T entry : CollectionUtils.getIterable(entityList)) {
			result = MathUtils.add(result, MathUtils.multiply(quantityRetriever.getPropertyValue(entry), priceRetriever.getPropertyValue(entry)));
		}
		return MathUtils.divide(result, quantity);
	}


	public BigDecimal getAveragePriceThresholdPercent() {
		return this.averagePriceThresholdPercent;
	}


	public void setAveragePriceThresholdPercent(BigDecimal averagePriceThresholdPercent) {
		this.averagePriceThresholdPercent = averagePriceThresholdPercent;
	}
}
