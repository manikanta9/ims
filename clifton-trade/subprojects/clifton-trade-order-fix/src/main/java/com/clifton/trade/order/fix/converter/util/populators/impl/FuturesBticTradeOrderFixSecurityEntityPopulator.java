package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.core.util.AssertUtils;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.investment.instruction.delivery.InvestmentDeliveryMonthCodes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to BTIC Future trades.
 * It adds additional information from the base Futures populator specific for BTIC trades
 * InvestmentType = Futures and Trade Execution Type.IsBticTrade = true
 *
 * @author davidi
 */
@Component
public class FuturesBticTradeOrderFixSecurityEntityPopulator extends FuturesTradeOrderFixSecurityEntityPopulator {

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	public FuturesBticTradeOrderFixSecurityEntityPopulator() {
		setOrder(20);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return super.isApplicable(tradeOrder, firstTrade) && firstTrade.getTradeExecutionType() != null && firstTrade.getTradeExecutionType().isBticTrade();
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		super.populateFixSecurityEntityFields(securityEntity, tradeOrder, firstTrade);
		InvestmentSecurity investmentSecurity = tradeOrder.getInvestmentSecurity();

		String initialSymbol = investmentSecurity.getSymbol();
		AssertUtils.assertTrue(initialSymbol.length() >= 3, "Symbol for a Future must be at least 3 characters in length.");
		// The endIndex represents the index of second to last character, from which we will start parsing the string in a reverse direction.
		// The last character of the Future symbol is part of the symbol's date code, and the second to last may also be part of the date code or may be the month indicator.
		// Example:  ESM9, ESM19   In the first case we start reverse parsing at the M character (endIndex = 2), in the second case (2 digit year code) we start at the "1" character (endIndex = 3).
		final int endIndex = initialSymbol.length() - 2;

		// The index that indicates where the date code as found in the Future symbol.  -1 indicates we have not found it yet.
		int dateCodeIndex = -1;

		final String bticPrefix = getInvestmentSecurityUtilHandler().getBticPrefix(investmentSecurity);
		AssertUtils.assertNotEmpty(bticPrefix, "No valid BTIC Prefix found for Future security: " + investmentSecurity.getLabel());

		// Traverse symbol backwards to find position of date code
		for (int index = endIndex; index >= 0; --index) {
			if (InvestmentDeliveryMonthCodes.isValidCode(initialSymbol.substring(index, index + 1))) {
				dateCodeIndex = index;
				break;
			}
		}

		AssertUtils.assertTrue(dateCodeIndex > 0, "Could not locate the date code within Future symbol: \"" + initialSymbol + "\".");

		// suffix is the portion of the symbol tha contains the month code and year.
		String symbolSuffix = initialSymbol.substring(dateCodeIndex);
		String bticSymbol = bticPrefix.trim() + symbolSuffix.trim();
		MarketDataFieldMapping fieldMapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(investmentSecurity.getInstrument(), null);
		String symbolWithMarketSector = bticSymbol + " " + fieldMapping.getMarketSector().getName();
		securityEntity.setSymbol(bticSymbol);
		securityEntity.setSecurityId(symbolWithMarketSector);
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}
}
