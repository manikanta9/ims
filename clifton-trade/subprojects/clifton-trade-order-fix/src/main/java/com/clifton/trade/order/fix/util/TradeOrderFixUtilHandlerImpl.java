package com.clifton.trade.order.fix.util;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurpose;
import com.clifton.investment.account.mapping.InvestmentAccountMappingService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMapping;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMappingSearchForm;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.marketdata.datasource.exchange.MarketDataSourceExchangeMapping;
import com.clifton.marketdata.datasource.exchange.MarketDataSourceExchangeMappingSearchForm;
import com.clifton.marketdata.datasource.exchange.MarketDataSourceExchangeService;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.destination.connection.TradeDestinationConnectionSearchForm;
import com.clifton.trade.destination.connection.TradeDestinationConnectionService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.execution.TradeExecutingBrokerCompanyService;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.util.TradeOrderUtilHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class TradeOrderFixUtilHandlerImpl implements TradeOrderFixUtilHandler {

	private static final String TRADE_DESTINATION_CONNECTION_CACHE_NAME = "TradeDestinationConnectionToFixMessageCache";

	private BusinessCompanyService businessCompanyService;

	private CacheHandler<Long, TradeDestinationConnection> cacheHandler;

	private InvestmentAccountMappingService investmentAccountMappingService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataSourceService marketDataSourceService;
	private MarketDataSourceCompanyService marketDataSourceCompanyService;
	private MarketDataSourceExchangeService marketDataSourceExchangeService;

	private SecurityUserService securityUserService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	private TradeDestinationConnectionService tradeDestinationConnectionService;
	private TradeDestinationService tradeDestinationService;
	private TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService;
	private TradeOrderUtilHandler tradeOrderUtilHandler;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TradeDestinationMapping getTradeDestinationMappingFromFixPartyList(TradeDestinationMappingAccountRelationshipCommand command) {
		if (command.getOrder().getInvestmentSecurity() != null) {
			FixParty broker = command.getFixPartyList().stream().filter((FixParty party) -> party.getPartyRole() == PartyRoles.EXECUTING_FIRM &&
					(party.getPartyIdSource() == PartyIdSources.PROPRIETARY_CUSTOM_CODE || party.getPartyIdSource() == PartyIdSources.DIRECTED_BROKER)).findFirst().orElse(null);
			if (broker != null) {
				String brokerCode = broker.getPartyId();
				TradeDestinationMapping result = getTradeDestinationMapping(command.getOrder(), command.getEntity(), brokerCode, command.isExceptionIfMoreThanOneOrNone());
				if (result == null && !CollectionUtils.isEmpty(command.getOrder().getTradeList())) {
					return getTradeDestinationMappingByAccountRelationship(command, brokerCode);
				}
				return result;
			}
		}
		return null;
	}


	private TradeDestinationMapping getTradeDestinationMappingByAccountRelationship(TradeDestinationMappingAccountRelationshipCommand command, String brokerCode) {
		TradeDestination destination = command.getOrder().getDestinationMapping().getTradeDestination();
		BusinessCompany mappedCompany = getMarketDataSourceCompanyService().getMappedCompanyByCode(destination.getMarketDataSource(), destination.getMarketDataSourcePurpose(), brokerCode);
		ValidationUtils.assertNotNull(mappedCompany, "Could not find a mapped company for code [" + brokerCode + "] and destination [" + command.getOrder().getDestinationMapping().getTradeDestination() + "].");

		List<BusinessCompany> childCompanyList;
		if (command.isSearchCompanyHierarchy()) {
			childCompanyList = getBusinessCompanyService().getBusinessCompanyRelatedCompanyList(mappedCompany.getId());
		}
		else {
			CompanySearchForm searchForm = new CompanySearchForm();
			searchForm.setParentId(mappedCompany.getId());
			searchForm.setTradingAllowed(true);
			childCompanyList = getBusinessCompanyService().getBusinessCompanyList(searchForm);
		}

		for (BusinessCompany company : CollectionUtils.getIterable(childCompanyList)) {
			if (company.getType().isTradingAllowed()) {
				for (Trade trade : CollectionUtils.getIterable(command.getOrder().getTradeList())) {
					InvestmentAccount holdingAccount = getTradeOrderUtilHandler().getHoldingAccountForBroker(trade, company);
					if (holdingAccount != null) {
						return getTradeDestinationMapping(command.getOrder(), command.getEntity(), null, company, command.isExceptionIfMoreThanOneOrNone());
					}
				}
			}
		}
		return null;
	}


	@Override
	public TradeDestinationMapping getTradeDestinationMapping(TradeOrder order, FixEntity entity, String brokerCode, boolean exceptionIfMoreThanOneOrNone) {
		return getTradeDestinationMapping(order, entity, brokerCode, null, exceptionIfMoreThanOneOrNone);
	}


	private TradeDestinationMapping getTradeDestinationMapping(TradeOrder order, FixEntity entity, String brokerCode, BusinessCompany executingBroker, boolean exceptionIfMoreThanOneOrNone) {
		if (order.getInvestmentSecurity() != null) {
			TradeDestinationConnection connection;
			TradeType tradeType;

			TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
			if (order.getDestinationMapping() != null) {
				tradeType = order.getDestinationMapping().getTradeType();
				connection = order.getDestinationMapping().getConnection();

				command.setTradeTypeId(order.getDestinationMapping().getTradeType().getId());
				command.setTradeDestinationId(order.getDestinationMapping().getTradeDestination().getId());
			}
			else {
				tradeType = getTradeService().getTradeTypeForSecurity(order.getInvestmentSecurity().getId());
				connection = getTradeDestinationConnection(entity);

				command.setTradeTypeId(tradeType.getId());
				command.setTradeDestinationConnectionId(connection.getId());
			}
			command.setBrokerCode(brokerCode);
			if (executingBroker != null) {
				command.setExecutingBrokerCompanyId(executingBroker.getId());
			}
			command.setActiveOnDate(order.getTradeDate());

			List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingListByCommand(command);
			if (exceptionIfMoreThanOneOrNone) {
				ValidationUtils.assertTrue(mappingList.size() == 1,
						"Could not find [TradeDestinationMapping] for trade type [" + tradeType + "], connection [" + connection + "] and broker code [" + brokerCode + "].");
			}
			if (mappingList.size() == 1) {
				return mappingList.get(0);
			}
		}
		return null;
	}


	@Override
	public TradeDestinationConnection getTradeDestinationConnection(FixEntity entity) {
		TradeDestinationConnection result = getTradeDestinationConnection(entity.getId());
		if (result == null) {
			TradeDestinationConnectionSearchForm sf = TradeOrderFixUtils.getTradeDestinationConnectionSearchForm(entity);
			result = CollectionUtils.getOnlyElement(getTradeDestinationConnectionService().getTradeDestinationConnectionList(sf));
			if (result != null) {
				putTradeDestinationConnection(entity, result);
			}
		}
		return result;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurity(FixSecurityEntity entity) {
		String securityId = entity.getSecurityId();
		if (entity.getSecurityIDSource() == null) {
			return null;
		}
		switch (entity.getSecurityIDSource()) {
			case BID:
				if (securityId.lastIndexOf(" ") != -1) {
					return getInvestmentInstrumentService().getInvestmentSecurityBySymbol(securityId.substring(0, securityId.lastIndexOf(" ")), null);
				}
				return null;
			case CUSIP:
				SecuritySearchForm sf = new SecuritySearchForm();
				sf.setCusipEquals(securityId);
				return CollectionUtils.getOnlyElement(getInvestmentInstrumentService().getInvestmentSecurityList(sf));
			default:
				return null;
		}
	}


	@Override
	public SecurityUser getTraderSecurityUser(FixEntity entity, List<FixParty> fixPartyList) {
		// find the trader from the party list first
		if (fixPartyList != null) {
			List<FixParty> traderPartyList = BeanUtils.filter(fixPartyList, FixParty::getPartyRole, PartyRoles.ORDER_ORIGINATION_TRADER);
			for (FixParty party : CollectionUtils.getIterable(traderPartyList)) {
				SecurityUser user = getSecurityUserService().getSecurityUserByName(party.getSenderUserName());
				if (user != null) {
					return user;
				}
			}
		}
		// if the trade was not in the party list use the senderUserName
		return getSecurityUserService().getSecurityUserByName(entity.getSenderUserName());
	}


	@Override
	public List<FixParty> getFixPartyList(TradeOrder order) {
		List<String> brokerCodeList = new ArrayList<>();
		if (order.getDestinationMapping().getMappingConfiguration().isSendRestrictiveBrokerList() && (order.getDestinationMapping().getExecutingBrokerCompany() == null)) {
			//Trade firstTrade = order.getTradeList().get(0);

			List<BusinessCompany> companyList = findExecutingBrokerCompaniesForOrder(order);
			ValidationUtils.assertNotEmpty(companyList, "There are no mapped trade destinations or no allowed executing brokers for this order [" + order + "].");
			for (BusinessCompany company : CollectionUtils.getIterable(companyList)) {
				TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand();
				command.setTradeTypeId(order.getDestinationMapping().getTradeType().getId());
				command.setTradeDestinationId(order.getDestinationMapping().getTradeDestination().getId());
				command.setExecutingBrokerCompanyId(company.getId());
				command.setActiveOnDate(order.getTradeDate());

				List<TradeDestinationMapping> mappings = getTradeDestinationService().getTradeDestinationMappingListByCommand(command);
				if (!CollectionUtils.isEmpty(mappings) && (mappings.size() == 1)) {
					MarketDataSourceCompanyMapping brokerMapping = getBrokerMapping(mappings.get(0));
					if (brokerMapping != null) {
						brokerCodeList.add(brokerMapping.getExternalCompanyName());
					}
				}
			}
		}
		else if (!StringUtils.isEmpty(order.getDestinationMapping().getOverrideBrokerCode())) {
			brokerCodeList.add(order.getDestinationMapping().getOverrideBrokerCode());
		}
		else {
			MarketDataSourceCompanyMapping brokerMapping = getBrokerMapping(order.getDestinationMapping());
			if (brokerMapping != null) {
				brokerCodeList.add(brokerMapping.getExternalCompanyName());
			}
		}

		List<FixParty> result = null;
		if (!brokerCodeList.isEmpty()) {
			result = new ArrayList<>();

			for (String brokerCode : CollectionUtils.getIterable(brokerCodeList)) {
				FixParty party = new FixParty();
				party.setPartyId(brokerCode);
				party.setPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE);
				party.setPartyRole(brokerCodeList.size() == 1 ? PartyRoles.EXECUTING_FIRM : PartyRoles.ACCEPTABLE_COUNTERPARTY);

				result.add(party);
			}
		}

		// add the exchange code for Cleared CDS
		result = addExchangePartyCodeForClearedOTC(order, result);

		return result;
	}


	@Override
	public String getExchangeMICCode(TradeOrder order) {
		InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(order.getInvestmentSecurity());
		ValidationUtils.assertNotNull(exchange, String.format("InvestmentExchange value for security '%s' cannot be null.", order.getInvestmentSecurity().getSymbol()));
		return exchange.getMarketIdentifierCode();
	}


	@Override
	public String getExchangeCode(TradeOrder order, boolean requireDataSourceCode) {
		String result = null;
		InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(order.getInvestmentSecurity());
		// use the executing broker data source, if it is null, then use the data source from trade destination
		MarketDataSource dataSource = null;
		if (order.getDestinationMapping().getExecutingBrokerCompany() != null) {
			try {
				dataSource = getMarketDataSourceService().getMarketDataSourceByCompany(order.getDestinationMapping().getExecutingBrokerCompany());
			}
			catch (Throwable e) {
				// we do not care if no data source is found
			}
		}
		if (dataSource == null) {
			dataSource = order.getDestinationMapping().getTradeDestination().getMarketDataSource();
		}
		MarketDataSourcePurpose dataSourcePurpose = order.getDestinationMapping().getTradeDestination().getMarketDataSourcePurpose();
		if (dataSource != null) {
			if (exchange != null) {
				MarketDataSourceExchangeMappingSearchForm sf = new MarketDataSourceExchangeMappingSearchForm();
				sf.setExchangeId(exchange.getId());
				sf.setMarketDataSourceId(dataSource.getId());
				if (dataSourcePurpose == null) {
					sf.setMarketDataSourcePurposeIsNull(true);
				}
				else {
					sf.setMarketDataSourcePurposeId(dataSourcePurpose.getId());
				}
				MarketDataSourceExchangeMapping em = CollectionUtils.getOnlyElement(getMarketDataSourceExchangeService().getMarketDataSourceExchangeMappingList(sf));
				result = em == null ? null : em.getExchangeCode();
			}
		}
		return !requireDataSourceCode && result == null && exchange != null ? exchange.getExchangeCode() : result;
	}


	@Override
	public String getFixTopAccountNumber(TradeOrder tradeOrder) {
		TradeDestinationMapping destinationMapping = tradeOrder.getDestinationMapping();
		if (destinationMapping == null) {
			return null;
		}

		return destinationMapping.getFixPostTradeAllocationTopAccountNumber();
	}


	/**
	 * Gets the value for the top account (fix tag 1) of a FIX TradeOrder.  This facility checks for translated values for the holding account in the first trade, and returns
	 * that value as the top account.  If no mapping is found, returns the FixPostTradeAllocationTopAccountNumber in the account mapping.
	 * Note that for an account grouping within the TradeOrder, all accounts with mapped values will have the same mapped value and same mapping purpose.
	 */
	@Override
	public String getFixSingleOrderAllocationAccountNumber(TradeOrder tradeOrder) {
		TradeDestinationMapping destinationMapping = tradeOrder.getDestinationMapping();
		if (destinationMapping == null) {
			return null;
		}

		// check the mapped values first.
		String accountNumber = getSingleInvestmentAccountMappedValue(tradeOrder, destinationMapping.getOrderAccountMappingPurpose());
		if (accountNumber != null) {
			return accountNumber;
		}

		// return the configured top account
		return destinationMapping.getFixPostTradeAllocationTopAccountNumber();
	}


	/**
	 * Gets the value for the allocation account to be used in a FIX TradeOrder (allocation details).  This facility checks for mapped account values using the allocation account
	 * number and the mapping purpose.  If a mapping is found, the mapped value is returned otherwise the allocation account number is returned.
	 */
	@Override
	public String getAllocationAccountNumber(InvestmentAccount allocationAccount, InvestmentAccountMappingPurpose purpose) {
		String mappedValue = getInvestmentAccountMappingService().getInvestmentAccountMappedValue(allocationAccount, purpose);
		return StringUtils.isEmpty(mappedValue) ? allocationAccount.getNumber() : mappedValue;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * return the non-null string if all investment account mapped values are the same for the provided trade order's trade list.
	 */
	private String getSingleInvestmentAccountMappedValue(TradeOrder tradeOrder, InvestmentAccountMappingPurpose purpose) {
		List<InvestmentAccount> holdingInvestmentAccounts = CollectionUtils.getStream(tradeOrder.getTradeList())
				.map(Trade::getHoldingInvestmentAccount)
				.distinct()
				.collect(Collectors.toList());

		String accountNumber = null;
		for (InvestmentAccount holdingAccount : holdingInvestmentAccounts) {
			String tmpAccount = getInvestmentAccountMappingService().getInvestmentAccountMappedValue(holdingAccount, purpose);
			if (StringUtils.isEmpty(tmpAccount)) {
				tmpAccount = holdingAccount.getNumber();
			}
			if (accountNumber == null) {
				accountNumber = tmpAccount;
			}
			else if (!StringUtils.isEqual(accountNumber, tmpAccount)) {
				accountNumber = null;
				break;
			}
		}
		return accountNumber;
	}


	private TradeDestinationConnection getTradeDestinationConnection(Long id) {
		return getCacheHandler().get(TRADE_DESTINATION_CONNECTION_CACHE_NAME, id);
	}


	private TradeDestinationConnection putTradeDestinationConnection(FixEntity entity, TradeDestinationConnection connection) {
		getCacheHandler().put(TRADE_DESTINATION_CONNECTION_CACHE_NAME, entity.getId(), connection);
		return connection;
	}


	private List<FixParty> addExchangePartyCodeForClearedOTC(TradeOrder order, List<FixParty> partyList) {
		// add the exchange code for Cleared CDS/IRS
		Trade firstTrade = order.getTradeList().get(0);
		InvestmentSecurity security = firstTrade.getInvestmentSecurity();
		if (TradeType.CLEARED_CREDIT_DEFAULT_SWAPS.equals(firstTrade.getTradeType().getName()) ||
				TradeType.CLEARED_INTEREST_RATE_SWAPS.equals(firstTrade.getTradeType().getName())) {
			InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(security);
			if (exchange != null) {
				FixParty party = new FixParty();
				party.setPartyRole(PartyRoles.CLEARING_ORGANIZATION);
				party.setPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE);

				if (exchange.getParent() != null && !StringUtils.isEmpty(exchange.getParent().getExchangeCode())) {
					party.setPartyId(exchange.getParent().getExchangeCode());
				}
				else {
					party.setPartyId(exchange.getExchangeCode());
				}

				if (partyList == null) {
					partyList = new ArrayList<>();
				}
				partyList.add(party);
			}
		}
		return partyList;
	}


	private List<BusinessCompany> findExecutingBrokerCompaniesForOrder(TradeOrder order) {
		List<BusinessCompany> result = null;
		if (!order.getDestinationMapping().getMappingConfiguration().isBrokerRestrictedToRelatedHoldingAccountList()) {
			// the trades should be pre-grouped to have the same set of executing brokers, so we can just use the first trade
			result = getTradeExecutingBrokerCompanyService().getTradeExecutingBrokerCompanyListByTrade(order.getTradeList().get(0));
		}
		else {
			TradeExecutionType tradeExecutionType = order.getTradeList().get(0).getTradeExecutionType();
			for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
				InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
				searchForm.setMainAccountId(trade.getClientInvestmentAccount().getId());
				searchForm.setMainPurposeId(trade.getTradeType().getHoldingAccountPurpose().getId());
				searchForm.setMainPurposeActiveOnDate(trade.getTradeDate());
				searchForm.setOurAccount(false);
				searchForm.setWorkflowStateName("Active");
				searchForm.setContractRequired(true);
				List<InvestmentAccount> accounts = getInvestmentAccountService().getInvestmentAccountList(searchForm);
				List<BusinessCompany> companyList = new ArrayList<>();

				for (InvestmentAccount account : CollectionUtils.getIterable(accounts)) {
					if (!companyList.contains(account.getIssuingCompany())) {
						if (tradeExecutionType != null && tradeExecutionType.getExecutingBrokerFilter() != null) {
							EvaluationResult evaluationResult = getSystemConditionEvaluationHandler().evaluateCondition(tradeExecutionType.getExecutingBrokerFilter().getId(), account.getIssuingCompany());
							if (evaluationResult.isResult()) {
								companyList.add(account.getIssuingCompany());
							}
						}
						else {
							companyList.add(account.getIssuingCompany());
						}
					}
				}
				if (result == null) {
					result = companyList;
				}
				else {
					result.retainAll(companyList);
				}
			}
		}
		return result;
	}


	private MarketDataSourceCompanyMapping getBrokerMapping(TradeDestinationMapping mapping) {
		if (mapping.getExecutingBrokerCompany() == null) {
			return null;
		}
		ValidationUtils.assertTrue(mapping.getConnection() != null,
				"Cannot lookup [MarketDataSourceCompanyMapping] for a [TradeDestinationMapping] with id [" + mapping.getId() + "] because it does not have a [TradeDestinationConnection].");

		TradeDestination tradeDestination = mapping.getTradeDestination();
		if (tradeDestination.getMarketDataSource() != null) {
			MarketDataSourceCompanyMapping result = getMarketDataSourceCompanyMappingParentRecursive(tradeDestination.getMarketDataSource().getId(),
					mapping.getExecutingBrokerCompany(), tradeDestination.getMarketDataSourcePurpose().getId());
			if ((result == null) && (mapping.getTradeDestination().getMarketDataSourcePurpose() != null)) {
				result = getMarketDataSourceCompanyMappingParentRecursive(tradeDestination.getMarketDataSource().getId(), mapping.getExecutingBrokerCompany(), null);
			}
			return result;
		}
		return null;
	}


	private MarketDataSourceCompanyMappingSearchForm getCompanyMappingSearchForm(Short marketDataSourceId, Integer executingBrokerCompanyId, Short marketDataSourcePurposeId) {
		MarketDataSourceCompanyMappingSearchForm sf = new MarketDataSourceCompanyMappingSearchForm();
		sf.setMarketDataSourceId(marketDataSourceId);
		if (marketDataSourcePurposeId != null) {
			sf.setMarketDataSourcePurposeId(marketDataSourcePurposeId);
		}
		else {
			sf.setMarketDataSourcePurposeIsNull(true);
		}
		sf.setBusinessCompanyId(executingBrokerCompanyId);
		return sf;
	}


	private MarketDataSourceCompanyMapping getMarketDataSourceCompanyMappingParentRecursive(Short marketDataSourceId, BusinessCompany executingBrokerCompany, Short marketDataSourcePurposeId) {
		MarketDataSourceCompanyMappingSearchForm sf = getCompanyMappingSearchForm(marketDataSourceId, executingBrokerCompany.getId(), marketDataSourcePurposeId);
		MarketDataSourceCompanyMapping marketDataSourceCompanyMapping = CollectionUtils.getOnlyElement(getMarketDataSourceCompanyService().getMarketDataSourceCompanyMappingList(sf));
		if (marketDataSourceCompanyMapping != null) {
			return marketDataSourceCompanyMapping;
		}
		else if (executingBrokerCompany.getParent() != null) {
			return getMarketDataSourceCompanyMappingParentRecursive(marketDataSourceId, executingBrokerCompany.getParent(), marketDataSourcePurposeId);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountMappingService getInvestmentAccountMappingService() {
		return this.investmentAccountMappingService;
	}


	public void setInvestmentAccountMappingService(InvestmentAccountMappingService investmentAccountMappingService) {
		this.investmentAccountMappingService = investmentAccountMappingService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public TradeDestinationConnectionService getTradeDestinationConnectionService() {
		return this.tradeDestinationConnectionService;
	}


	public void setTradeDestinationConnectionService(TradeDestinationConnectionService tradeDestinationConnectionService) {
		this.tradeDestinationConnectionService = tradeDestinationConnectionService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public CacheHandler<Long, TradeDestinationConnection> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Long, TradeDestinationConnection> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public MarketDataSourceCompanyService getMarketDataSourceCompanyService() {
		return this.marketDataSourceCompanyService;
	}


	public void setMarketDataSourceCompanyService(MarketDataSourceCompanyService marketDataSourceCompanyService) {
		this.marketDataSourceCompanyService = marketDataSourceCompanyService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public MarketDataSourceExchangeService getMarketDataSourceExchangeService() {
		return this.marketDataSourceExchangeService;
	}


	public void setMarketDataSourceExchangeService(MarketDataSourceExchangeService marketDataSourceExchangeService) {
		this.marketDataSourceExchangeService = marketDataSourceExchangeService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public TradeExecutingBrokerCompanyService getTradeExecutingBrokerCompanyService() {
		return this.tradeExecutingBrokerCompanyService;
	}


	public void setTradeExecutingBrokerCompanyService(TradeExecutingBrokerCompanyService tradeExecutingBrokerCompanyService) {
		this.tradeExecutingBrokerCompanyService = tradeExecutingBrokerCompanyService;
	}


	public TradeOrderUtilHandler getTradeOrderUtilHandler() {
		return this.tradeOrderUtilHandler;
	}


	public void setTradeOrderUtilHandler(TradeOrderUtilHandler tradeOrderUtilHandler) {
		this.tradeOrderUtilHandler = tradeOrderUtilHandler;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
