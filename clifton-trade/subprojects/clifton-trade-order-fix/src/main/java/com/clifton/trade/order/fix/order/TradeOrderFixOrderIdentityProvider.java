package com.clifton.trade.order.fix.order;


import java.util.List;


public interface TradeOrderFixOrderIdentityProvider {

	public List<Integer> getExternalIdentityListByOrder(Integer externalOrderIdentifier);
}
