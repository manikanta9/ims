package com.clifton.trade.order.fix.order;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.order.FixEntityService;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.trade.order.fix.order.allocation.TradeOrderFixOrderAllocationIdentityProvider;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * The <code>FixOrderService</code> implements methods used to retrieve order specific objects from the FixMessage table.
 *
 * @author mwacker
 */
@Service
public class TradeOrderFixOrderServiceImpl implements TradeOrderFixOrderService {

	private FixEntityService fixEntityService;
	private FixMessageService fixMessageService;

	private TradeOrderFixOrderIdentityProvider tradeOrderFixOrderIdentityProvider;
	private TradeOrderFixOrderAllocationIdentityProvider tradeOrderFixOrderAllocationIdentityProvider;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns all FIX messages that pertain to a given order including the allocation messages.
	 */
	@Override
	public List<FixMessage> getTradeOrderFixMessageListForOrder(Integer externalOrderIdentifier) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<Integer> idList = getTradeOrderExternalIdentityListByOrder(externalOrderIdentifier);

		// parse the string id's to integers to avoid an extra join on the query
		Integer[] identifierIds = new Integer[idList.size()];
		identifierIds = idList.toArray(identifierIds);

		FixMessageEntrySearchForm searchFrom = new FixMessageEntrySearchForm();
		searchFrom.setIdentifierIdList(identifierIds);
		return getFixMessageService().getFixMessageList(searchFrom);
	}


	@Override
	public List<Integer> getTradeOrderExternalIdentityListByOrder(Integer externalOrderIdentifier) {
		List<Integer> idList = new ArrayList<>();
		List<Integer> orderIds = getTradeOrderFixOrderIdentityProvider().getExternalIdentityListByOrder(externalOrderIdentifier);
		if (!CollectionUtils.isEmpty(orderIds)) {
			idList.addAll(orderIds);
		}

		List<Integer> allocIds = getTradeOrderFixOrderAllocationIdentityProvider().getAllocationExternalIdentityListByOrder(externalOrderIdentifier, orderIds);
		if (allocIds != null) {
			idList.addAll(allocIds);
		}
		return idList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////               FixOrder Business Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrder getTradeOrderFixOrder(Integer externalOrderIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<FixOrder> orderList = getFixEntityService().getFixEntityListForIdentifierAndTypeList(externalOrderIdentifier, Arrays.asList(FixMessageTypeTagValues.NEW_ORDER_SINGLE_TYPE_TAG.getTagValue(), FixMessageTypeTagValues.ORDER_CANCEL_REPLACE_REQUEST_TYPE_TAG.getTagValue()));

		// TODO: use TradeOrderChange to lookup the current ID and filter the result for that ID.  For now, get the first message in the list, it doesn't matter if there are multiple
		FixOrder result = CollectionUtils.getFirstElement(orderList);
		if (result != null && fullyPopulated) {
			populateFixOrder(result, false);
		}
		return result;
	}


	private void populateFixOrder(FixOrder order, boolean fullyPopulated) {
		if (order.getIdentifier().getReferencedIdentifier() != null) {
			order.setFixOriginalOrder(getTradeOrderFixOrder(order.getIdentifier().getReferencedIdentifier().getId(), fullyPopulated));
		}
		order.setFixExecutionReportList(getTradeOrderFixExecutionReportList(order.getIdentifier().getId(), fullyPopulated));
	}


	////////////////////////////////////////////////////////////////////////////
	////////          FixExecutionReport Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixExecutionReport> getTradeOrderFixExecutionReportList(Integer externalOrderIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<Integer> idList = getTradeOrderFixOrderIdentityProvider().getExternalIdentityListByOrder(externalOrderIdentifier);
		List<FixExecutionReport> reportList = getFixEntityService().getFixEntityListForIdentifierListAndType(idList, FixMessageTypeTagValues.EXECUTION_REPORT_TYPE_TAG.getTagValue());
		if (fullyPopulated) {
			for (FixExecutionReport report : CollectionUtils.getIterable(reportList)) {
				populateFixExecutionReport(report, false);
			}
		}
		return reportList;
	}


	@Override
	public List<FixExecutionReport> getTradeOrderFixExecutionReportListForAllocation(Integer externalOrderIdentifier, boolean fullyPopulated) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<Integer> idList = getTradeOrderFixOrderIdentityProvider().getExternalIdentityListByOrder(externalOrderIdentifier);
		List<FixExecutionReport> reportList = getFixEntityService().getFixEntityListForIdentifierListAndType(idList, FixMessageTypeTagValues.EXECUTION_REPORT_TYPE_TAG.getTagValue());
		reportList = filterForActiveExecutions(reportList);
		if (fullyPopulated) {
			for (FixExecutionReport report : CollectionUtils.getIterable(reportList)) {
				populateFixExecutionReport(report, false);
			}
		}
		return reportList;
	}


	@Override
	public FixExecutionReport getTradeOrderFixExecutionReportLast(Integer externalOrderIdentifier, boolean fullyPopulated, boolean fillReportOnly) {
		ValidationUtils.assertNotNull(externalOrderIdentifier, "An external order id is required.");

		List<Integer> idList = getTradeOrderFixOrderIdentityProvider().getExternalIdentityListByOrder(externalOrderIdentifier);
		List<FixExecutionReport> reportList = getFixEntityService().getFixEntityListForIdentifierListAndType(idList, FixMessageTypeTagValues.EXECUTION_REPORT_TYPE_TAG.getTagValue());
		if (fillReportOnly) {
			reportList = filterForActiveExecutions(reportList);
		}
		BeanUtils.sortWithFunction(reportList, FixExecutionReport::getMessageDateAndTime, false);
		FixExecutionReport result = CollectionUtils.getFirstElement(reportList);
		if (result != null && fullyPopulated) {
			populateFixExecutionReport(result, false);
		}
		return result;
	}


	private void populateFixExecutionReport(FixExecutionReport report, boolean fullyPopulated) {
		report.setFixOrder(getTradeOrderFixOrder(report.getIdentifier().getId(), fullyPopulated));
		if (report.getIdentifier().getReferencedIdentifier() != null) {
			report.setFixOriginalOrder(getTradeOrderFixOrder(report.getIdentifier().getReferencedIdentifier().getId(), fullyPopulated));
		}
	}


	private List<FixExecutionReport> filterForActiveExecutions(List<FixExecutionReport> reportList) {
		// filter
		List<String> executionReportToBeExcluded = new ArrayList<>();
		for (FixExecutionReport report : CollectionUtils.getIterable(reportList)) {
			if ((report.getOrderStatus() == OrderStatuses.FILLED) || (report.getOrderStatus() == OrderStatuses.PARTIALLY_FILLED) || (report.getOrderStatus() == OrderStatuses.NEW)) {
				if (!StringUtils.isEmpty(report.getExecutionReferenceId())) {
					executionReportToBeExcluded.add(report.getExecutionReferenceId());
				}
			}
			else {
				executionReportToBeExcluded.add(report.getExecutionId());
			}
		}
		List<FixExecutionReport> executionReportList = new ArrayList<>();
		for (FixExecutionReport report : CollectionUtils.getIterable(reportList)) {
			if (!executionReportToBeExcluded.contains(report.getExecutionId())) {
				executionReportList.add(report);
			}
		}
		return executionReportList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}


	public FixMessageService getFixMessageService() {
		return this.fixMessageService;
	}


	public void setFixMessageService(FixMessageService fixMessageService) {
		this.fixMessageService = fixMessageService;
	}


	public TradeOrderFixOrderAllocationIdentityProvider getTradeOrderFixOrderAllocationIdentityProvider() {
		return this.tradeOrderFixOrderAllocationIdentityProvider;
	}


	public void setTradeOrderFixOrderAllocationIdentityProvider(TradeOrderFixOrderAllocationIdentityProvider tradeOrderFixOrderAllocationIdentityProvider) {
		this.tradeOrderFixOrderAllocationIdentityProvider = tradeOrderFixOrderAllocationIdentityProvider;
	}


	public TradeOrderFixOrderIdentityProvider getTradeOrderFixOrderIdentityProvider() {
		return this.tradeOrderFixOrderIdentityProvider;
	}


	public void setTradeOrderFixOrderIdentityProvider(TradeOrderFixOrderIdentityProvider tradeOrderFixOrderIdentityProvider) {
		this.tradeOrderFixOrderIdentityProvider = tradeOrderFixOrderIdentityProvider;
	}
}
