package com.clifton.trade.order.fix;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.TimedEvictionCache;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.configurer.FixMessagingMessageConfigurer;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityService;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import com.clifton.fix.order.factory.FixEntityFactory;
import com.clifton.fix.order.factory.FixEntityFactoryGenerationTypes;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class TradeOrderFixHandlerImpl implements TradeOrderFixHandler {


	private AsynchronousMessageHandler tradeOrderFixMessagingHandler;

	private FixEntityFactory fixEntityFactory;
	private FixEntityService fixEntityService;

	private TradeOrderFixProcessor tradeOrderFixProcessor;

	private TradeOrderFixOrderService tradeOrderFixOrderService;


	private TimedEvictionCache<TradeOrder> tradeOrderTimedStatusCache;

	/**
	 * Defines a map of message configures used to generate a message from a fix entity.
	 */
	private List<FixMessagingMessageConfigurer> tradeOrderFixMessagingMessageConfigurerList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends FixEntity> void send(T entity, Integer sourceId) {

		FixEntityMessagingMessage message = new FixEntityMessagingMessage();
		message.setFixEntity(entity);
		message.setSessionQualifier(entity.getFixSessionQualifier());
		message.setSourceSystemFkFieldId(MathUtils.getNumberAsLong(sourceId));

		for (FixMessagingMessageConfigurer configurer : CollectionUtils.getIterable(getTradeOrderFixMessagingMessageConfigurerList())) {
			if (configurer.applyTo(entity)) {
				configurer.configure(message);
			}
		}

		getTradeOrderFixMessagingHandler().send(message);
	}


	@Override
	public void sendDontKnowTrade(FixExecutionReport report) {
		send(report, null);
	}


	@Override
	public <T extends IdentityObject> void sendEntity(T order, FixEntityFactoryGenerationTypes action) {
		FixEntity entity = getFixEntityFactory().create(order, action);
		send(entity, (Integer) order.getIdentity());
	}


	@Override
	public void resendOrderMessage(TradeOrder order) {
		if (order == null) {
			throw new RuntimeException("Cannot resend the order message because no order exists.");
		}
		for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
			if (!TradeService.TRADE_EXECUTION_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				throw new RuntimeException("Cannot resend the order message because the trades are not in the [" + TradeService.TRADE_EXECUTION_STATE_NAME + "] work flow state.");
			}
		}
		sendEntity(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE);
	}


	@Override
	public void sendTradeOrderFixCancelRequest(TradeOrder order) {
		for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
			if (!TradeService.TRADE_EXECUTION_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				throw new RuntimeException("Cannot cancel order because trades are not in the [" + TradeService.TRADE_EXECUTION_STATE_NAME + "] state.");
			}
		}
		sendEntity(order, FixEntityFactoryGenerationTypes.ORDER_CANCEL_REQUEST);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<TradeOrder, FixOrder> createFixTradeOrderMap(List<TradeOrder> orderList) {
		Map<TradeOrder, FixOrder> fixOrderMap = new HashMap<>();
		for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
			if (order.getOriginalTradeOrder() == null) {
				fixOrderMap.put(order, getFixEntityFactory().create(order, FixEntityFactoryGenerationTypes.ORDER_SINGLE));
			}
			else {
				fixOrderMap.put(order, generateTradeOrderCancelReplace(order));
			}
		}
		return fixOrderMap;
	}


	@Override
	@Transactional
	public void executeFixTradeOrderMap(Map<TradeOrder, FixOrder> fixTradeOrderMap) {
		// send the fix messages after all db updates are complete
		for (Map.Entry<TradeOrder, FixOrder> orderEntry : fixTradeOrderMap.entrySet()) {
			// add trade to the timeout cache.
			getTradeOrderTimedStatusCache().add(orderEntry.getKey());
			// send the message last in case of an error
			send(orderEntry.getValue(), orderEntry.getKey().getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void reprocessFixMessage(FixMessage message) {
		FixEntity entity = getFixEntityService().getFixEntityForFixMessage(message, true);
		getTradeOrderFixProcessor().processDoNotSuppressErrors(entity);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////                      Helper Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	private FixOrderCancelReplaceRequest generateTradeOrderCancelReplace(TradeOrder tradeOrder) {
		ValidationUtils.assertTrue(tradeOrder.getStatus().getOrderStatus() != TradeOrderStatuses.SENT, "Cannot send a cancel replace message for an order with status '"
				+ tradeOrder.getStatus().getOrderStatus() + "'.");
		ValidationUtils.assertNotNull(tradeOrder.getOriginalTradeOrder(), "Cannot send an order as a replacement without an original order.");
		ValidationUtils.assertFalse(tradeOrder.isAllocationSent(), "Cannot cancel replace a message that is already allocated.");

		FixExecutionReport report = getTradeOrderFixOrderService().getTradeOrderFixExecutionReportLast(tradeOrder.getOriginalTradeOrder().getExternalIdentifier(), false, false);
		switch (report.getOrderStatus()) {
			case CANCELED:
				//case REPLACED:
			case PENDING_CANCEL:
			case STOPPED:
			case REJECTED:
			case PENDING_REPLACE:
				ValidationUtils.fail("Cannot replace an order with status '" + report.getOrderStatus() + "'.");
				break;
			default:
				break;
		}

		for (Trade trade : CollectionUtils.getIterable(tradeOrder.getTradeList())) {
			if (!TradeService.TRADE_EXECUTION_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				throw new RuntimeException("Cannot send the order cancel replace message because the trades are not in the [" + TradeService.TRADE_EXECUTION_STATE_NAME + "] work flow state.");
			}
		}
		ValidationUtils.assertTrue(MathUtils.compare(tradeOrder.getOriginalTradeOrder().getQuantityFilled(), tradeOrder.getQuantity()) <= 0,
				"Cannot update order quantity to less than the already filled amount.");

		return getFixEntityFactory().create(tradeOrder, FixEntityFactoryGenerationTypes.ORDER_CANCEL_REPLACE_REQUEST);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AsynchronousMessageHandler getTradeOrderFixMessagingHandler() {
		return this.tradeOrderFixMessagingHandler;
	}


	public void setTradeOrderFixMessagingHandler(AsynchronousMessageHandler tradeOrderFixMessagingHandler) {
		this.tradeOrderFixMessagingHandler = tradeOrderFixMessagingHandler;
	}


	public FixEntityFactory getFixEntityFactory() {
		return this.fixEntityFactory;
	}


	public void setFixEntityFactory(FixEntityFactory fixEntityFactory) {
		this.fixEntityFactory = fixEntityFactory;
	}


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}


	public TradeOrderFixProcessor getTradeOrderFixProcessor() {
		return this.tradeOrderFixProcessor;
	}


	public void setTradeOrderFixProcessor(TradeOrderFixProcessor tradeOrderFixProcessor) {
		this.tradeOrderFixProcessor = tradeOrderFixProcessor;
	}


	public List<FixMessagingMessageConfigurer> getTradeOrderFixMessagingMessageConfigurerList() {
		return this.tradeOrderFixMessagingMessageConfigurerList;
	}


	public void setTradeOrderFixMessagingMessageConfigurerList(List<FixMessagingMessageConfigurer> tradeOrderFixMessagingMessageConfigurerList) {
		this.tradeOrderFixMessagingMessageConfigurerList = tradeOrderFixMessagingMessageConfigurerList;
	}


	public TradeOrderFixOrderService getTradeOrderFixOrderService() {
		return this.tradeOrderFixOrderService;
	}


	public void setTradeOrderFixOrderService(TradeOrderFixOrderService tradeOrderFixOrderService) {
		this.tradeOrderFixOrderService = tradeOrderFixOrderService;
	}


	public TimedEvictionCache<TradeOrder> getTradeOrderTimedStatusCache() {
		return this.tradeOrderTimedStatusCache;
	}


	public void setTradeOrderTimedStatusCache(TimedEvictionCache<TradeOrder> tradeOrderTimedStatusCache) {
		this.tradeOrderTimedStatusCache = tradeOrderTimedStatusCache;
	}
}
