package com.clifton.trade.order.fix;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.fix.message.TradeFixMessageHandler;
import com.clifton.trade.order.processor.TradeOrderProcessor;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>TradeOrderFixProcessorImpl</code> processes incoming FIX objects, updates the TradeOrder object and then passes the resulting
 * TradeOrderProcessorParameters object to the TradeOrderProcessor which will execute the next action(s) on the Trade(s).
 *
 * @author mwacker
 */
@Component("tradeOrderFixProcessor")
public class TradeOrderFixProcessorImpl implements TradeOrderFixProcessor, InitializingBean, ApplicationContextAware {

	private TradeOrderService tradeOrderService;
	private TradeOrderAllocationService tradeOrderAllocationService;
	private TradeOrderProcessor tradeOrderProcessor;

	private ApplicationContext applicationContext;

	/**
	 * The map of message handlers by message class name.
	 */
	private Map<String, TradeFixMessageHandler<?, ?>> tradeOrderFixMessageHandlerMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		@SuppressWarnings("unchecked")
		Map<String, TradeFixMessageHandler<?, ?>> fixMessageHandlerByName = (Map<String, TradeFixMessageHandler<?, ?>>) (Map<?, ?>) getApplicationContext().getBeansOfType(TradeFixMessageHandler.class);
		Map<String, TradeFixMessageHandler<?, ?>> fixMessageHandlerByMessageClassName = fixMessageHandlerByName.values().stream().collect(Collectors.toMap(k -> k.getMessageClass().getName(), Function.identity()));
		setTradeOrderFixMessageHandlerMap(fixMessageHandlerByMessageClassName);
	}


	@Override
	public <T extends FixEntity> void process(T entity) {
		doProcess(entity, true);
	}


	@Override
	public <T extends FixEntity> void processDoNotSuppressErrors(T entity) {
		doProcess(entity, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends FixEntity> void doProcess(T entity, boolean suppressErrors) {
		TradeOrderProcessorParameters orderProcessParameters = doProcess(entity);
		if (orderProcessParameters != null) {
			try {
				processTradeOrders(orderProcessParameters);
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), "Failed to process fix orders for message: " + entity, e);

				List<TradeOrder> orderList = orderProcessParameters.getTradeOrder() != null ? CollectionUtils.createList(orderProcessParameters.getTradeOrder()) : orderProcessParameters.getTradeOrderList();
				// try to set the allocation status to FAILED to send.
				for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
					try {
						TradeOrder ord = getTradeOrderService().getTradeOrder(order.getId());
						if (ord.getStatus().isFillComplete() && ord.getAllocationStatus() == null && ord.getDestinationMapping().getMappingConfiguration().isFixPostTradeAllocation()) {
							TradeOrderAllocationStatus newStatus = getTradeOrderAllocationService().getTradeOrderAllocationStatusByStatuses(TradeOrderAllocationStatuses.FAILED_TO_SEND, false);
							ord.setAllocationStatus(newStatus);
							getTradeOrderService().saveTradeOrder(ord);
						}
					}
					catch (Throwable ex) {
						LogUtils.error(getClass(), "Failed to set [FAILED_TO_SEND] allocation status for order [" + order.getId() + "]", ex);
					}
				}
				if (!suppressErrors) {
					throw new RuntimeException("Failed to process fix orders for message: " + entity, e);
				}
			}
		}
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void processTradeOrders(TradeOrderProcessorParameters parameters) {
		getTradeOrderProcessor().processTradeOrder(parameters);
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected <T extends FixEntity> TradeOrderProcessorParameters doProcess(T entity) {
		TradeFixMessageHandler<T, TradeOrderProcessorParameters> handler = getMessageHandler(entity);
		if (handler != null) {
			return handler.handleMessage(entity);
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	private <T extends FixEntity> TradeFixMessageHandler<T, TradeOrderProcessorParameters> getMessageHandler(T entity) {
		String className = entity.getClass().getName();
		if (getTradeOrderFixMessageHandlerMap().containsKey(className)) {
			return (TradeFixMessageHandler<T, TradeOrderProcessorParameters>) getTradeOrderFixMessageHandlerMap().get(className);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderAllocationService getTradeOrderAllocationService() {
		return this.tradeOrderAllocationService;
	}


	public void setTradeOrderAllocationService(TradeOrderAllocationService tradeOrderAllocationService) {
		this.tradeOrderAllocationService = tradeOrderAllocationService;
	}


	public TradeOrderProcessor getTradeOrderProcessor() {
		return this.tradeOrderProcessor;
	}


	public void setTradeOrderProcessor(TradeOrderProcessor tradeOrderProcessor) {
		this.tradeOrderProcessor = tradeOrderProcessor;
	}


	public Map<String, TradeFixMessageHandler<?, ?>> getTradeOrderFixMessageHandlerMap() {
		return this.tradeOrderFixMessageHandlerMap;
	}


	public void setTradeOrderFixMessageHandlerMap(Map<String, TradeFixMessageHandler<?, ?>> tradeOrderFixMessageHandlerMap) {
		this.tradeOrderFixMessageHandlerMap = tradeOrderFixMessageHandlerMap;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
