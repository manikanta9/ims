package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Cleared Credit Default Swap trades.
 * InvestmentType = Swaps and Trade Type = Cleared Interest Rate Swaps
 *
 * @author davidi
 */
@Component
public class ClearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	public ClearedInterestRateSwapsTradeOrderFixSecurityEntityPopulator() {
		setOrder(110);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.SWAPS) && TradeType.CLEARED_INTEREST_RATE_SWAPS.equals(firstTrade.getTradeType().getName());
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		securityEntity.setSecurityId("[FpML]");
		securityEntity.setSymbol("[N/A]");
		securityEntity.setSecurityIDSource(SecurityIDSources.ISDA_FPML_PRODUCT_SPECIFICATION);
		securityEntity.setSecurityType(SecurityTypes.INTEREST_RATE_SWAP);
	}
}
