package com.clifton.trade.order.fix.util;

import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixParty;
import com.clifton.trade.order.TradeOrder;

import java.util.List;


public class TradeDestinationMappingAccountRelationshipCommand {

	private TradeOrder order;
	private FixEntity entity;
	private List<FixParty> fixPartyList;
	private boolean exceptionIfMoreThanOneOrNone;


	public TradeDestinationMappingAccountRelationshipCommand(TradeOrder order, FixEntity entity, List<FixParty> fixPartyList, boolean exceptionIfMoreThanOneOrNone) {
		this.order = order;
		this.entity = entity;
		this.fixPartyList = fixPartyList;
		this.exceptionIfMoreThanOneOrNone = exceptionIfMoreThanOneOrNone;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSearchCompanyHierarchy() {
		return this.order != null && this.order.getDestinationMapping() != null && this.order.getDestinationMapping().getMappingConfiguration().isSearchCompanyHierarchyForBrokerMatch();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrder getOrder() {
		return this.order;
	}


	public void setOrder(TradeOrder order) {
		this.order = order;
	}


	public FixEntity getEntity() {
		return this.entity;
	}


	public void setEntity(FixEntity entity) {
		this.entity = entity;
	}


	public List<FixParty> getFixPartyList() {
		return this.fixPartyList;
	}


	public void setFixPartyList(List<FixParty> fixPartyList) {
		this.fixPartyList = fixPartyList;
	}


	public boolean isExceptionIfMoreThanOneOrNone() {
		return this.exceptionIfMoreThanOneOrNone;
	}


	public void setExceptionIfMoreThanOneOrNone(boolean exceptionIfMoreThanOneOrNone) {
		this.exceptionIfMoreThanOneOrNone = exceptionIfMoreThanOneOrNone;
	}
}
