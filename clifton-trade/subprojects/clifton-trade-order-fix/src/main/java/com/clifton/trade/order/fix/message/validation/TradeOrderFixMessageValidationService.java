package com.clifton.trade.order.fix.message.validation;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.trade.order.TradeOrder;

import java.util.List;


/**
 * @author manderson
 */
public interface TradeOrderFixMessageValidationService {

	@SecureMethod(dtoClass = TradeOrder.class)
	public List<FixValidationResult> getTradeOrderFixMessageValidationsForTradeOrderList(Integer externalOrderIdentifier);
}
