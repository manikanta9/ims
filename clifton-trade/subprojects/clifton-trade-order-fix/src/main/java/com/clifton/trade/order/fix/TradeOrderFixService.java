package com.clifton.trade.order.fix;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>TradeOrderFixService</code> defines the methods used by IMS and the UI to create outgoing FixEntities or
 * take FIX specific actions on trades.
 *
 * @author mwacker
 */
public interface TradeOrderFixService {

	public static final String FIX_UNIQUE_ORDER_ID_PREFIX = "O";
	public static final String FIX_UNIQUE_ALLOCATION_ID_PREFIX = "A";

	public static final String EXECUTING_PURPOSE_NAME_PREFIX = "Executing";


	/**
	 * Resends the original order, if the order was received by the counter party this will be a duplicate.
	 */
	@SecureMethod
	@RequestMapping("tradeOrderFixResend")
	public void resendTradeOrderFix(int tradeOrderId);


	/**
	 * Resends the original order message if the order status is REJECTED
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixRejectedResend")
	public void resendTradeOrderFixRejected(int tradeOrderId);


	/**
	 * Cancel the TradeOrder
	 */
	@RequestMapping("tradeOrderFixCancelRequestSend")
	@SecureMethod(dtoClass = Trade.class)
	public void sendTradeOrderFixCancelRequest(int tradeOrderId);


	/**
	 * Cancel underlying trades for order that have failed to execute.
	 */
	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixListCancel")
	public void cancelTradeOrderFixList(Integer[] tradeOrderIdList);


	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixListMoveToManualFill")
	public void moveTradeOrderFixListToManualFill(Integer[] tradeOrderIdList);


	@SecureMethod(dtoClass = Trade.class)
	@RequestMapping("tradeOrderFixListWithAveragePriceBook")
	public void bookTradeOrderFixListWithAveragePrice(Integer[] tradeOrderIdList);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@RequestMapping("tradeOrderFixReprocessFixMessage")
	public void reprocessFixMessage(long messageId);


	@RequestMapping("tradeOrderFixReprocessFixMessageList")
	public void reprocessFixMessageList(long[] messageIdList);
}
