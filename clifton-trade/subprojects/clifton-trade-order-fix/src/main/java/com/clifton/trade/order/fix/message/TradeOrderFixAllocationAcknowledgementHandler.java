package com.clifton.trade.order.fix.message;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetail;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatus;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessorParameters;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationSearchForm;
import com.clifton.trade.order.processor.TradeOrderProcessorActonTypes;
import com.clifton.trade.order.processor.TradeOrderProcessorParameters;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
@Component
public class TradeOrderFixAllocationAcknowledgementHandler extends AbstractTradeOrderFixMessageHandler<FixAllocationAcknowledgement> {


	@Override
	public Class<FixAllocationAcknowledgement> getMessageClass() {
		return FixAllocationAcknowledgement.class;
	}


	@Override
	public TradeOrderProcessorParameters handleMessage(FixAllocationAcknowledgement entity) {
		List<TradeOrder> ordersToProcess = new ArrayList<>();
		List<TradeOrder> orderList = getTradeOrderService().getTradeOrderListByAllocationId(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));

		// get the status from the first order
		TradeOrder firstOrder = orderList.get(0);
		TradeOrderAllocationStatus allocationStatus = BeanUtils.cloneBean(orderList.get(0).getAllocationStatus(), false, false);
		allocationStatus.setId(null);

		switch (entity.getAllocationStatus()) {
			case REVERSED:
			case CANCELED: {
				TradeOrderAllocation tradeOrderAllocation = getTradeOrderAllocationService().getTradeOrderAllocation(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
				tradeOrderAllocation.setActive(false);
				getTradeOrderAllocationService().saveTradeOrderAllocation(tradeOrderAllocation);
				break;
			}
			case ACCEPTED: {
				// Update the allocation statuses since the allocation was successfully replaced.
				TradeOrderAllocation tradeOrderAllocation = getTradeOrderAllocationService().getTradeOrderAllocation(MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
				tradeOrderAllocation.setActive(true);
				getTradeOrderAllocationService().saveTradeOrderAllocation(tradeOrderAllocation);

				// deactivate all other allocations
				TradeOrderAllocationSearchForm sf = new TradeOrderAllocationSearchForm();
				sf.setActive(true);
				sf.setTradeOrderId(firstOrder.getId());
				List<TradeOrderAllocation> allocationList = getTradeOrderAllocationService().getTradeOrderAllocationList(sf);
				for (TradeOrderAllocation alloc : CollectionUtils.getIterable(allocationList)) {
					if (!alloc.equals(tradeOrderAllocation)) {
						alloc.setActive(false);
						getTradeOrderAllocationService().saveTradeOrderAllocation(alloc);
					}
				}

				break;
			}
			default:
			case RECEIVED:
			case ALLOCATION_PENDING:
			case ACCOUNT_LEVEL_REJECT:
			case BLOCK_LEVEL_REJECT:
			case REJECTED_BY_INTERMEDIARY:
			case INCOMPLETE: {
				// Do nothing, the current allocation is still active
				break;
			}
		}

		boolean updateStatus = false;
		// only set the allocation status of order if it has not already been accepted.
		// if has previously been accepted and then rejected then the original allocation is still in effect
		if (firstOrder.getAllocationStatus().getAllocationStatus() != TradeOrderAllocationStatuses.ACCEPTED) {
			allocationStatus.setAllocationStatus(getTradeOrderAllocationStatus(entity.getAllocationStatus()));
			updateStatus = true;
		}
		// if the new status is ACCEPTED and no allocation fill is expected, set the allocation as complete
		if (!firstOrder.getDestinationMapping().getMappingConfiguration().isFixAllocationReportExpected()
				&& ((entity.getAllocationStatus() == AllocationStatuses.ACCEPTED) || (firstOrder.getDestinationMapping().getMappingConfiguration().isFixAllocationCompleteWhenStatusIsReceived() && (entity.getAllocationStatus() == AllocationStatuses.RECEIVED)))) {
			allocationStatus.setAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);
			allocationStatus.setAllocationFillReceived(false);
			allocationStatus.setAllocationComplete(true);
			updateStatus = true;
		}
		// update the status on the orders
		if (updateStatus) {
			for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
				saveOrderWithStatus(order, order.getStatus(), allocationStatus);
				ordersToProcess.add(order);
			}
		}
		// update validation error status
		if (entity.isValidationError() || entity.isAllocationValidationError()) {
			for (TradeOrder order : CollectionUtils.getIterable(orderList)) {
				saveOrderWithValidationStatus(order, entity.isValidationError(), entity.isAllocationValidationError());
			}
		}

		// if the allocation is complete, then create the TradeOrderProcessorParameters
		if (!CollectionUtils.isEmpty(ordersToProcess) && allocationStatus.isAllocationComplete()) {
			TradeOrderProcessorParameters result = new TradeOrderProcessorParameters(ordersToProcess, TradeOrderProcessorActonTypes.PROCESS_ORDER_ALLOCATION);

			TradeOrderAllocationProcessorParameters allocationParameters = new TradeOrderAllocationProcessorParameters();
			allocationParameters.setExternalAllocationIdentifier(entity.getIdentifier().getId());
			allocationParameters.setDestinationMapping(firstOrder.getDestinationMapping());

			List<TradeOrderAllocationDetail> allocationDetailList = getTradeOrderAllocationDetailProvider().getTradeOrderAllocationDetails(allocationParameters);
			result.setAllocationDetailList(allocationDetailList);

			return result;
		}
		return null;
	}
}
