package com.clifton.trade.order.fix.converter;

import com.clifton.business.company.BusinessCompany;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.schedule.BusinessDayConventionUtilHandler;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarScheduleUtils;
import com.clifton.calendar.schedule.api.Schedule;
import com.clifton.calendar.schedule.api.ScheduleApiService;
import com.clifton.calendar.schedule.api.ScheduleOccurrenceCommand;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedParty;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OpenCloses;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.order.beans.ProcessCodes;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.clifton.fix.util.FixUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.CouponFrequencies;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeTimeInForceType;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.TradeOrderFixService;
import com.clifton.trade.order.fix.converter.util.populators.TradeOrderFixSecurityEntityPopulator;
import com.clifton.trade.order.fix.util.TradeOrderFixUtilHandler;
import com.clifton.trade.order.fix.util.TradeOrderFixUtils;
import com.clifton.trade.order.util.TradeOrderUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AbstractTradeOrderFixOrderConverter</code> defines a base TradeOrder to FixOrder converter.
 *
 * @author mwacker
 */
public abstract class AbstractTradeOrderFixOrderConverter<T extends FixOrder> implements Converter<TradeOrder, T> {

	private BusinessDayConventionUtilHandler businessDayConventionUtilHandler;

	private CalendarBusinessDayService calendarBusinessDayService;
	private ScheduleApiService scheduleApiService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private SystemColumnValueHandler systemColumnValueHandler;

	private TemplateConverter templateConverter;
	private TradeOrderFixUtilHandler tradeOrderFixUtilHandler;

	@Autowired
	private List<TradeOrderFixSecurityEntityPopulator> tradeOrderFixSecurityEntityPopulatorList;

	// TODO: This needs to be moved to a Investment specificity so we can look it up by type.
	@SuppressWarnings("SpellCheckingInspection")
	private static final String CLEARED_IRS_FPML = "<?xml version='1.0' encoding='UTF-8'?><dataDocument xmlns=\"http://www.fpml.org/FpML-5/confirmation\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" fpmlVersion=\"5-5\" xsi:schemaLocation=\"http://www.fpml.org/FpML-5/confirmation fpml-main-5-5.xsd http://www.bloomberg.com/BBFpML-1 bbfpml-1-1.xsd\"><trade><tradeHeader><partyTradeIdentifier><partyReference href=\"partyA\"/><tradeId tradeIdScheme=\"http://www.fpml.org/tradeId\">${TRADE_ORDER_ID}</tradeId></partyTradeIdentifier><partyTradeIdentifier><partyReference href=\"partyB\"/><tradeId tradeIdScheme=\"http://www.fpml.org/tradeId\">DUMTRADEID</tradeId></partyTradeIdentifier><tradeDate>${TRADE_DATE?string(\"yyyy-MM-dd\")}</tradeDate></tradeHeader><swap><productType productTypeScheme=\"http://www.fpml.org/coding-scheme/product-type-simple\">InterestRateSwap</productType><productId>USD SWAP VS 3M ${TENOR}YR</productId><swapStream id=\"fixedLeg\"><payerPartyReference href=\"${FIXED_PAYER_PARTY}\"/><receiverPartyReference href=\"${FIXED_RECEIVER_PARTY}\"/><calculationPeriodDates id=\"payCalcDates\"><effectiveDate><unadjustedDate>${EFFECTIVE_DATE?string(\"yyyy-MM-dd\")}</unadjustedDate><dateAdjustments><businessDayConvention>NONE</businessDayConvention></dateAdjustments></effectiveDate><terminationDate id=\"fixedLegTerminationDate\"><unadjustedDate>${MATURITY_DATE?string(\"yyyy-MM-dd\")}</unadjustedDate><dateAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></dateAdjustments></terminationDate><calculationPeriodDatesAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></calculationPeriodDatesAdjustments><calculationPeriodFrequency><periodMultiplier>6</periodMultiplier><period>M</period><rollConvention>25</rollConvention></calculationPeriodFrequency></calculationPeriodDates><paymentDates><calculationPeriodDatesReference href=\"payCalcDates\"/><paymentFrequency><periodMultiplier>6</periodMultiplier><period>M</period></paymentFrequency><firstPaymentDate>${FIX_LEG_FIRST_PAYMENT_DATE?string(\"yyyy-MM-dd\")}</firstPaymentDate><payRelativeTo>CalculationPeriodEndDate</payRelativeTo><paymentDatesAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></paymentDatesAdjustments></paymentDates><calculationPeriodAmount><calculation><notionalSchedule id=\"payNotionalSchedule\"><notionalStepSchedule><initialValue>${NOTIONAL}</initialValue><currency currencyScheme=\"http://www.fpml.org/ext/iso4217-2001-08-15\">USD</currency></notionalStepSchedule></notionalSchedule><dayCountFraction dayCountFractionScheme=\"http://www.fpml.org/coding-scheme/day-count-fraction\">30/360</dayCountFraction></calculation></calculationPeriodAmount></swapStream><swapStream id=\"floatingLeg\"><payerPartyReference href=\"${FIXED_RECEIVER_PARTY}\"/><receiverPartyReference href=\"${FIXED_PAYER_PARTY}\"/><calculationPeriodDates id=\"recCalcDates\"><effectiveDate><unadjustedDate>${EFFECTIVE_DATE?string(\"yyyy-MM-dd\")}</unadjustedDate><dateAdjustments><businessDayConvention>NONE</businessDayConvention></dateAdjustments></effectiveDate><terminationDate id=\"floatingLegTerminationDate\"><unadjustedDate>${MATURITY_DATE?string(\"yyyy-MM-dd\")}</unadjustedDate><dateAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></dateAdjustments></terminationDate><calculationPeriodDatesAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></calculationPeriodDatesAdjustments><calculationPeriodFrequency><periodMultiplier>3</periodMultiplier><period>M</period><rollConvention>25</rollConvention></calculationPeriodFrequency></calculationPeriodDates><paymentDates><calculationPeriodDatesReference href=\"recCalcDates\"/><paymentFrequency><periodMultiplier>3</periodMultiplier><period>M</period></paymentFrequency><firstPaymentDate>${FLOATING_LEG_FIRST_PAYMENT_DATE?string(\"yyyy-MM-dd\")}</firstPaymentDate><payRelativeTo>CalculationPeriodEndDate</payRelativeTo><paymentDatesAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></paymentDatesAdjustments></paymentDates><resetDates id=\"recResetDates\"><calculationPeriodDatesReference href=\"recCalcDates\"/><resetRelativeTo>CalculationPeriodStartDate</resetRelativeTo><fixingDates><periodMultiplier>-2</periodMultiplier><period>D</period><dayType>Business</dayType><businessDayConvention>PRECEDING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter></businessCenters><dateRelativeTo href=\"recResetDates\"/></fixingDates><resetFrequency><periodMultiplier>3</periodMultiplier><period>M</period></resetFrequency><resetDatesAdjustments><businessDayConvention>MODFOLLOWING</businessDayConvention><businessCenters><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">GBLO</businessCenter><businessCenter businessCenterScheme=\"http://www.fpml.org/coding-scheme/business-center\">USNY</businessCenter></businessCenters></resetDatesAdjustments></resetDates><calculationPeriodAmount><calculation><notionalSchedule id=\"receiveNotionalSchedule\"><notionalStepSchedule><initialValue>${NOTIONAL}</initialValue><currency currencyScheme=\"http://www.fpml.org/ext/iso4217-2001-08-15\">USD</currency></notionalStepSchedule></notionalSchedule><floatingRateCalculation><floatingRateIndex floatingRateIndexScheme=\"http://www.fpml.org/coding-scheme/floating-rate-index\">USD-LIBOR-BBA</floatingRateIndex><indexTenor><periodMultiplier>3</periodMultiplier><period>M</period></indexTenor></floatingRateCalculation><dayCountFraction dayCountFractionScheme=\"http://www.fpml.org/coding-scheme/day-count-fraction\">ACT/360</dayCountFraction></calculation></calculationPeriodAmount></swapStream></swap></trade><party id=\"partyA\"><partyId partyIdScheme=\"http://www.bloomberg.com/fiet/spec/UUID\">${UUID}</partyId></party><party id=\"partyB\"><partyId partyIdScheme=\"http://www.bloomberg.com/fiet/spec/BrokerCode\">DUMMYFIRMID</partyId></party></dataDocument>\t";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected T convert(TradeOrder tradeOrder, T result) {
		BigDecimal orderQty = tradeOrder.getQuantity();
		ValidationUtils.assertTrue(orderQty.doubleValue() > 0, "Cannot send a FIX order with a quantity less than or equal to '0'");

		Trade firstTrade = tradeOrder.getTradeList().get(0);
		TradeDestinationMapping destinationMapping = tradeOrder.getDestinationMapping();

		TradeOrderFixUtils.setEntityItems(result, destinationMapping);

		// check that the holding account has a stock execution account setup
		if (destinationMapping.getMappingConfiguration().isFixExecutingAccountUsed() && "Stocks".equals(tradeOrder.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName())) {
			for (Trade trade : CollectionUtils.getIterable(tradeOrder.getTradeList())) {
				InvestmentAccount ia = lookupExecutingAccount(trade, TradeOrderFixService.EXECUTING_PURPOSE_NAME_PREFIX + ": "
						+ trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName());
				ValidationUtils.assertNotNull(ia, "No stock executing account defined for client [" + trade.getHoldingInvestmentAccount().getLabel() + "].");
			}
		}

		setOrderDates(result, tradeOrder);

		result.setText(getDescription(tradeOrder.getTradeList(), destinationMapping));

		// set the trader user
		result.setSenderUserName(tradeOrder.getTraderUser().getUserName());

		setAccountField(result, tradeOrder);
		// set the broker codes
		result.setPartyList(getTradeOrderFixUtilHandler().getFixPartyList(tradeOrder));

		if (!StringUtils.isEmpty(destinationMapping.getTradeDestination().getFixTargetSubId())) {
			result.setFixTargetSubId(destinationMapping.getTradeDestination().getFixTargetSubId());
		}
		result.setHandlingInstructions(HandlingInstructions.valueOf(destinationMapping.getTradeDestination().getFixHandlingInstructions()));

		if (tradeOrder.isBuy()) {
			result.setSide(OrderSides.BUY);
		}
		else {
			result.setSide(OrderSides.SELL);
		}
		setOpenClose(result, firstTrade);
		result.setOrderQty(orderQty);

		if (tradeOrder.getLimitPrice() != null) {
			result.setPrice(tradeOrder.getLimitPrice());
			result.setOrderType(OrderTypes.LIMIT);
		}
		else {
			result.setOrderType(OrderTypes.MARKET);
		}

		// set the order times
		if (firstTrade.getTradeTimeInForceType() != null) {
			TradeTimeInForceType tradeTimeInForceType = firstTrade.getTradeTimeInForceType();
			result.setTimeInForce(TimeInForceTypes.valueOf(tradeTimeInForceType.getName()));

			// The timestamp is in the local time zone, but will be converted to GMT.  This value will appear in FIX Tag 126.
			if (tradeTimeInForceType.isExpirationDateRequired()) {
				// make it a timestamp for the full day, so it ends at 23:59:00.
				final int timeOffsetInMinutes = 23 * 60 + 59;
				Date expirationDate = DateUtils.addMinutes(firstTrade.getTimeInForceExpirationDate(), timeOffsetInMinutes);
				result.setExpireTime(expirationDate);
			}
		}
		else {
			result.setTimeInForce(TimeInForceTypes.DAY);
		}
		result.setTransactionTime(new Date());

		// add the security fields
		String exchangeCode = null;
		if (destinationMapping.getConnection() != null) {
			exchangeCode = getTradeOrderFixUtilHandler().getExchangeCode(tradeOrder, true);
		}
		if (destinationMapping.getMappingConfiguration().isFixMICExchangeCodeRequiredOnOrderMessages()) {
			result.setSecurityExchange(getTradeOrderFixUtilHandler().getExchangeMICCode(tradeOrder));
		}
		setSecurityFields(tradeOrder, result, exchangeCode, firstTrade);

		// set the unique id of the order
		result.setClientOrderStringId(FixUtils.createUniqueId(tradeOrder, TradeOrderFixService.FIX_UNIQUE_ORDER_ID_PREFIX));
		if (tradeOrder.getOriginalTradeOrder() != null) {
			result.setOriginalClientOrderStringId(FixUtils.createUniqueId(tradeOrder.getOriginalTradeOrder(), TradeOrderFixService.FIX_UNIQUE_ORDER_ID_PREFIX));
		}

		if (InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.SWAPS) && TradeType.CLEARED_INTEREST_RATE_SWAPS.equals(firstTrade.getTradeType().getName())) {
			if (tradeOrder.isBuy()) {
				result.setSide(OrderSides.RECEIVES);
			}
			else {
				result.setSide(OrderSides.PAYS);
			}

			result.setEncodeSecurityDescription(getClearedIRSFpML(tradeOrder, result));
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setOpenClose(T result, Trade firstTrade) {
		if (InvestmentUtils.isSecurityOfType(firstTrade.getInvestmentSecurity(), InvestmentType.OPTIONS)) {
			ValidationUtils.assertTrue(firstTrade.getOpenCloseType().isExplicitOpenClose(), "[" + firstTrade.getOpenCloseType() + "] is not validate for Options via FIX.  Open/Close must be explicitly set to execute Options orders on FIX.");
			result.setOpenClose(firstTrade.getOpenCloseType().isOpen() ? OpenCloses.OPEN : OpenCloses.CLOSE);
		}
	}


	private void setSecurityFields(TradeOrder tradeOrder, FixSecurityEntity entity, String exchangeCode, Trade firstTrade) {
		// when set to true, indicates a populator was found for the entity
		boolean foundApplicablePopulator = false;

		if (!StringUtils.isEmpty(exchangeCode)) {
			entity.setExDestination(exchangeCode);
		}

		for (TradeOrderFixSecurityEntityPopulator populator : getTradeOrderFixSecurityEntityPopulatorList()) {
			if (populator.isApplicable(tradeOrder, firstTrade)) {
				populator.populateFixSecurityEntityFields(entity, tradeOrder, firstTrade);
				foundApplicablePopulator = true;
				break;
			}
		}

		if (!foundApplicablePopulator) {
			throw new RuntimeException("'" + tradeOrder.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName() + "' is not supported by FIX session [" + entity.getFixSessionString() + "].");
		}
	}


	@SuppressWarnings("unchecked")
	private <V> V getInvestmentSecurityCustomFieldValue(InvestmentSecurity bean, String fieldName) {
		Object obj = getSystemColumnValueHandler().getSystemColumnValueForEntity(bean, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, fieldName, true);
		return obj != null ? (V) obj : null;
	}


	private String getClearedIRSFpML(TradeOrder order, FixOrder fixOrder) {
		InvestmentSecurity security = order.getInvestmentSecurity();
		Calendar settlementCalendar = security.getInstrument().getSettlementCalendar();

		Integer tenor = getInvestmentSecurityCustomFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_TENOR);
		ValidationUtils.assertNotNull(tenor, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_TENOR + "] is null.");

		BusinessDayConventions businessDayConventions = BusinessDayConventions.valueOf(getInvestmentSecurityCustomFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_BUSINESS_DAY_CONVENTION));
		ValidationUtils.assertNotNull(businessDayConventions, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_BUSINESS_DAY_CONVENTION + "] is null.");

		BusinessDayConventions floatingBusinessDayConventions = BusinessDayConventions.valueOf(getInvestmentSecurityCustomFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_FLOATING_BUSINESS_DAY_CONVENTION));
		ValidationUtils.assertNotNull(businessDayConventions, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_FLOATING_BUSINESS_DAY_CONVENTION + "] is null.");

		String couponFrequencyFieldValue = getInvestmentSecurityCustomFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY);
		ValidationUtils.assertNotEmpty(couponFrequencyFieldValue, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY + "] is null.");
		CouponFrequencies couponFrequency = CouponFrequencies.getEnum(couponFrequencyFieldValue);
		ValidationUtils.assertNotNull(couponFrequency, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_COUPON_FREQUENCY + "] is invalid.");

		String paymentFrequencyFieldValue = getInvestmentSecurityCustomFieldValue(security, InvestmentSecurity.CUSTOM_FIELD_PAYMENT_FREQUENCY);
		ValidationUtils.assertNotEmpty(paymentFrequencyFieldValue, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_PAYMENT_FREQUENCY + "] is null.");
		CouponFrequencies paymentFrequency = CouponFrequencies.getEnum(paymentFrequencyFieldValue);
		ValidationUtils.assertNotNull(paymentFrequency, "Cannot send FIX order for security [" + security.getLabel() + "] because the value of field [" + InvestmentSecurity.CUSTOM_FIELD_PAYMENT_FREQUENCY + "] is invalid.");

		Date effectiveDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(order.getTradeDate(), settlementCalendar.getId()), 2);
		Date maturityDate = getSwapMaturityDate(effectiveDate, settlementCalendar, tenor, businessDayConventions);

		TemplateConfig config = new TemplateConfig(CLEARED_IRS_FPML);
		config.addBeanToContext("TRADE_DATE", order.getTradeDate());
		config.addBeanToContext("NOTIONAL", order.getQuantity());

		config.addBeanToContext("EFFECTIVE_DATE", effectiveDate); // Trade Date T+2 with Settlement Calendar
		config.addBeanToContext("MATURITY_DATE", maturityDate); // effective + tenor and adjust for business days using settlement calendar and fixed BD convention

		config.addBeanToContext("FIX_LEG_FIRST_PAYMENT_DATE", getSwapNextEventOccurrence(effectiveDate, maturityDate, settlementCalendar, couponFrequency, businessDayConventions)); // effective + payment frequency (Coupon Frequency) and adjust for business days using settlement calendar and fixed BD (Business Day Convention) convention
		config.addBeanToContext("FLOATING_LEG_FIRST_PAYMENT_DATE", getSwapNextEventOccurrence(effectiveDate, maturityDate, settlementCalendar, paymentFrequency, floatingBusinessDayConventions)); // effective + payment frequency (Payment Frequency) and adjust for business days using settlement calendar and fixed Floating BD convention

		config.addBeanToContext("TENOR", tenor);

		config.addBeanToContext("FIXED_PAYER_PARTY", order.isBuy() ? "partyB" : "partyA");
		config.addBeanToContext("FIXED_RECEIVER_PARTY", order.isBuy() ? "partyA" : "partyB");

		config.addBeanToContext("TRADE_ORDER_ID", fixOrder.getClientOrderStringId());
		config.addBeanToContext("UUID", fixOrder.getSenderUserName());

		return getTemplateConverter().convert(config);
	}


	private Date getSwapMaturityDate(Date effectiveDate, Calendar settlementCalendar, Integer tenor, BusinessDayConventions businessDayConvention) {
		Date maturityDate = DateUtils.addYears(effectiveDate, tenor);
		return getBusinessDayConventionUtilHandler().applyBusinessDayConvention(maturityDate, businessDayConvention, settlementCalendar);
	}


	private Date getSwapNextEventOccurrence(Date effectiveDate, Date endDate, Calendar settlementCalendar, CouponFrequencies frequency, BusinessDayConventions businessDayConvention) {
		Schedule schedule = new Schedule();
		schedule.setCalendar(settlementCalendar.toCalendar());
		schedule.setBusinessDayConvention(businessDayConvention.name());

		schedule.setStartDate(effectiveDate);
		schedule.setStartTime(new Time(0));
		schedule.setEndDate(endDate);

		schedule.setFrequency(frequency.getScheduleFrequency().name());
		schedule.setRecurrence(frequency.getRecurrence());

		updateScheduleWithFrequency(schedule, frequency, effectiveDate);

		return getScheduleApiService().getScheduleOccurrence(ScheduleOccurrenceCommand.forOccurrences(schedule, 1, DateUtils.addDays(effectiveDate, 1)));
	}


	private void updateScheduleWithFrequency(Schedule schedule, CouponFrequencies frequency, Date startDate) {
		switch (frequency.getScheduleFrequency()) {
			case WEEKLY:
				CalendarScheduleUtils.updateWeekDay(schedule, DateUtils.getDayOfWeek(startDate));
				break;
			case MONTHLY:
				schedule.setDayOfMonth(DateUtils.getDayOfMonth(startDate));
				schedule.setFirstOccurrence(startDate);
				schedule.setCountFromLastDayOfMonth(false);
				break;
			case YEARLY:
				schedule.setDayOfMonth(DateUtils.getDayOfMonth(startDate));
				CalendarScheduleUtils.updateMonth(schedule, DateUtils.getMonthOfYear(startDate));
				break;
			case ONCE:
				schedule.setStartDate(schedule.getEndDate());
				schedule.setEndDate(null);
				break;
			default:
				break;
		}
	}


	private void setAccountField(FixOrder fixOrder, TradeOrder order) {
		Trade firstTrade = order.getTradeList().get(0);
		TradeDestinationMapping destinationMapping = order.getDestinationMapping();

		if (destinationMapping.getMappingConfiguration().isFixPostTradeAllocation()) {
			// POST-TRADE ALLOCATION
			if (!StringUtils.isEmpty(destinationMapping.getFixPostTradeAllocationTopAccountNumber()) && !destinationMapping.getMappingConfiguration().isFixSendTopAccountOnAllocationMessages()) {
				fixOrder.setAccount(getTradeOrderFixUtilHandler().getFixTopAccountNumber(order));
			}
		}
		else if (destinationMapping.getMappingConfiguration().isFixDisablePreTradeAllocations()) {
			fixOrder.setAccount(getTradeOrderFixUtilHandler().getFixSingleOrderAllocationAccountNumber(order));
		}
		else {
			// PRE-TRADE ALLOCATION
			if (!destinationMapping.getMappingConfiguration().isFixTradeGroupingAllowed()) {
				if (destinationMapping.getMappingConfiguration().isSendClientAccountWithOrder()) {
					String mappedAccountNumber = getTradeOrderFixUtilHandler().getAllocationAccountNumber(firstTrade.getClientInvestmentAccount(), destinationMapping.getOrderAccountMappingPurpose());
					fixOrder.setAccount(mappedAccountNumber);
				}
				else {
					String allocationAccountNumber = getTradeOrderFixUtilHandler().getAllocationAccountNumber(getAllocationAccount(destinationMapping, firstTrade), destinationMapping.getOrderAccountMappingPurpose());
					fixOrder.setAccount(allocationAccountNumber);
				}
			}
			else {
				List<FixAllocationDetail> allocationDetailList = new ArrayList<>();
				// Send the SEF Sponsor account from the top account as the only allocation
				if (firstTrade.getExecutingSponsorCompany() != null) {
					FixAllocationDetail detail = new FixAllocationDetail();
					detail.setAllocationAccount(destinationMapping.getFixPostTradeAllocationTopAccountNumber());
					detail.setAllocationShares(TradeOrderUtils.getTotalTradeQuantity(order.getTradeList()));
					addAllocationDetailParties(firstTrade, destinationMapping, detail);

					allocationDetailList.add(detail);
				}
				else {
					for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
						FixAllocationDetail detail = new FixAllocationDetail();

						InvestmentAccount allocationAccount = getAllocationAccount(destinationMapping, trade);
						String allocationAccountNumber = getTradeOrderFixUtilHandler().getAllocationAccountNumber(getAllocationAccount(destinationMapping, trade), destinationMapping.getAllocationAccountMappingPurpose());
						if (allocationAccount != null) {
							detail.setAllocationAccount(allocationAccountNumber);
							if (!allocationAccount.getIssuingCompany().equals(trade.getExecutingBrokerCompany())) {
								detail.setProcessCode(ProcessCodes.STEP_OUT);
							}
							else {
								detail.setProcessCode(ProcessCodes.REGULAR);
							}
						}

						detail.setAllocationShares(TradeOrderUtils.getTradeQuantity(trade));
						addAllocationDetailParties(trade, destinationMapping, detail);

						allocationDetailList.add(detail);
					}
				}
				fixOrder.setFixAllocationDetailList(allocationDetailList);
				//throw new RuntimeException("Grouped pre-trade allocations are not supported.");
			}
		}
	}


	private void addAllocationDetailParties(Trade trade, TradeDestinationMapping destinationMapping, FixAllocationDetail detail) {
		BusinessCompany clearingFirm = null;
		if (trade.getExecutingSponsorCompany() != null) {
			clearingFirm = trade.getExecutingSponsorCompany();
		}
		else if (destinationMapping.getMappingConfiguration().isFixSendClearingFirmLEIWithAllocation()) {
			clearingFirm = trade.getHoldingInvestmentAccount().getIssuingCompany();
		}
		if (clearingFirm != null) {
			String legalEntityIdentifier = clearingFirm.getLegalEntityIdentifier();
			if (!StringUtils.isEmpty(legalEntityIdentifier)) {
				FixAllocationDetailNestedParty nestedParty = new FixAllocationDetailNestedParty();
				nestedParty.setNestedPartyId(legalEntityIdentifier);
				nestedParty.setNestedPartyRole(PartyRoles.CLEARING_FIRM);
				nestedParty.setNestedPartyIDSource(PartyIdSources.LEGAL_ENTITY_IDENTIFIER);

				List<FixAllocationDetailNestedParty> partyList = new ArrayList<>();
				partyList.add(nestedParty);
				detail.setNestedPartyList(partyList);
			}
		}
	}


	private InvestmentAccount getAllocationAccount(TradeDestinationMapping destinationMapping, Trade trade) {
		InvestmentAccount allocationAccount = trade.getHoldingInvestmentAccount();
		// if the destination uses executing accounts, then get the execution account
		if (destinationMapping.getMappingConfiguration().isFixExecutingAccountUsed()) {
			InvestmentAccount ia = lookupExecutingAccount(trade, TradeOrderFixService.EXECUTING_PURPOSE_NAME_PREFIX + ": "
					+ trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName());

			if ((ia != null) && (ia.getNumber() != null) && !ia.getNumber().isEmpty()) {
				allocationAccount = ia;
			}
		}
		else if (destinationMapping.getMappingConfiguration().isSendClientAccountWithOrder()) {
			allocationAccount = trade.getClientInvestmentAccount();
		}

		return allocationAccount;
	}


	private void setOrderDates(FixOrder order, TradeOrder tradeOrder) {
		Trade firstTrade = tradeOrder.getTradeList().get(0);

		order.setTradeDate(tradeOrder.getTradeDate());
		if (InvestmentUtils.isSecurityOfType(firstTrade.getInvestmentSecurity(), InvestmentType.FORWARDS)) {
			if (!tradeOrder.getOriginalInvestmentSecurity().getInstrument().isDeliverable()) {
				order.setSettlementDate(tradeOrder.getOriginalInvestmentSecurity().getFirstDeliveryDate());
				order.setMaturityDate(tradeOrder.getOriginalInvestmentSecurity().getEndDate());
			}
			else {
				order.setSettlementDate(tradeOrder.getOriginalInvestmentSecurity().getEndDate());
			}
		}
		else {
			order.setSettlementDate(firstTrade.getSettlementDate());
		}
	}


	private InvestmentAccount lookupExecutingAccount(Trade trade, String purposeName) {
		InvestmentAccountRelationshipSearchForm searchForm = new InvestmentAccountRelationshipSearchForm();
		searchForm.setPurposeName(purposeName);
		searchForm.setMainAccountId(trade.getHoldingInvestmentAccount().getId());
		searchForm.setRelatedAccountIssuingCompanyId(trade.getExecutingBrokerCompany().getId());
		searchForm.setActiveOnDate(trade.getTradeDate());
		InvestmentAccountRelationship relationship = CollectionUtils.getOnlyElement(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm));
		return relationship != null ? relationship.getReferenceTwo() : null;
	}


	private String getDescription(List<Trade> tradeList, TradeDestinationMapping destinationMapping) {
		StringBuilder sb = new StringBuilder();
		boolean allAreEqual = true;
		String description = "";
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if ((trade.getDescription() != null) && !trade.getDescription().isEmpty()) {
				sb.append(trade.getDescription());
				sb.append("/n");
				if ((description == null) || description.isEmpty()) {
					description = trade.getDescription();
				}
				else {
					allAreEqual = description.equals(trade.getDescription());
				}
			}
		}
		return (allAreEqual ? description : sb.toString()) + (destinationMapping.getFixGlobalTradeNote() != null ? destinationMapping.getFixGlobalTradeNote() : "");
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public TradeOrderFixUtilHandler getTradeOrderFixUtilHandler() {
		return this.tradeOrderFixUtilHandler;
	}


	public void setTradeOrderFixUtilHandler(TradeOrderFixUtilHandler tradeOrderFixUtilHandler) {
		this.tradeOrderFixUtilHandler = tradeOrderFixUtilHandler;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public BusinessDayConventionUtilHandler getBusinessDayConventionUtilHandler() {
		return this.businessDayConventionUtilHandler;
	}


	public void setBusinessDayConventionUtilHandler(BusinessDayConventionUtilHandler businessDayConventionUtilHandler) {
		this.businessDayConventionUtilHandler = businessDayConventionUtilHandler;
	}


	public ScheduleApiService getScheduleApiService() {
		return this.scheduleApiService;
	}


	public void setScheduleApiService(ScheduleApiService scheduleApiService) {
		this.scheduleApiService = scheduleApiService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public List<TradeOrderFixSecurityEntityPopulator> getTradeOrderFixSecurityEntityPopulatorList() {
		return this.tradeOrderFixSecurityEntityPopulatorList;
	}


	public void setTradeOrderFixSecurityEntityPopulatorList(List<TradeOrderFixSecurityEntityPopulator> tradeOrderFixSecurityEntityPopulatorList) {
		this.tradeOrderFixSecurityEntityPopulatorList = tradeOrderFixSecurityEntityPopulatorList;
	}
}
