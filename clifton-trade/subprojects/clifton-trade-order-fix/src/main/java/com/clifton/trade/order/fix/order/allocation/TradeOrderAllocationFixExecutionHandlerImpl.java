package com.clifton.trade.order.fix.order.allocation;


import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.TradeOrderAllocationExecutionHandler;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>TradeOrderFixAllocationExecutionService</code> implements an allocation execution service.
 * <p/>
 * Used by TradeOrderProcessorImpl to call the FIX specific allocation execution.
 *
 * @author mwacker
 */
@Component("tradeOrderAllocationExecutionHandler")
public class TradeOrderAllocationFixExecutionHandlerImpl implements TradeOrderAllocationExecutionHandler {

	private TradeOrderAllocationFixService tradeOrderAllocationFixService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void allocationTradeOrder(List<TradeOrder> order) {
		getTradeOrderAllocationFixService().sendTradeOrderFixAllocationInstruction(order);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderAllocationFixService getTradeOrderAllocationFixService() {
		return this.tradeOrderAllocationFixService;
	}


	public void setTradeOrderAllocationFixService(TradeOrderAllocationFixService tradeOrderAllocationFixService) {
		this.tradeOrderAllocationFixService = tradeOrderAllocationFixService;
	}
}
