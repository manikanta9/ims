package com.clifton.trade.order.fix.order.allocation;


import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetail;
import com.clifton.trade.order.allocation.TradeOrderAllocationDetailProvider;
import com.clifton.trade.order.allocation.processor.TradeOrderAllocationProcessorParameters;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>TradeOrderFixAllocationDetailProvider</code> implements TradeOrderAllocationDetailProvider used by the FIX services to generate
 * the allocation details for the FixAllocationInstruction messages.
 *
 * @author mwacker
 */
@Component("tradeOrderAllocationDetailProvider")
public class TradeOrderAllocationFixDetailProvider implements TradeOrderAllocationDetailProvider {

	private TradeOrderFixOrderAllocationService tradeOrderFixOrderAllocationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<TradeOrderAllocationDetail> getTradeOrderAllocationDetails(TradeOrderAllocationProcessorParameters allocationParameters) {
		List<TradeOrderAllocationDetail> allocationDetailList = new ArrayList<>();

		// if the allocation is complete and the process doesn't use allocation reports, the create the allocation details
		if ((allocationParameters.getDestinationMapping() != null) && !allocationParameters.getDestinationMapping().getMappingConfiguration().isFixAllocationReportExpected()) {
			FixAllocationInstruction allocationInstruction = getTradeOrderFixOrderAllocationService().getTradeOrderFixAllocationInstruction(allocationParameters.getExternalAllocationIdentifier(), false);
			int id = 100;
			for (FixAllocationDetail detail : CollectionUtils.getIterable(allocationInstruction.getFixAllocationDetailList())) {
				TradeOrderAllocationDetail allocationDetail = new TradeOrderAllocationDetail();
				allocationDetail.setAllocationAccount(detail.getAllocationAccount());
				allocationDetail.setAllocationAveragePrice(allocationInstruction.getAveragePrice());
				allocationDetail.setAllocationPrice(allocationInstruction.getAveragePrice());
				allocationDetail.setAllocationShares(detail.getAllocationShares());
				allocationDetail.setAllocationNetMoney(detail.getAllocationNetMoney());
				allocationDetail.setId(allocationInstruction.getIdentifier().getId() + "_" + id);
				id++;
				allocationDetailList.add(allocationDetail);
			}
		}
		else {
			// get the allocation report
			List<FixAllocationReport> allocationReportList = allocationParameters.getAllocationReports();
			if (allocationReportList == null) {
				allocationReportList = getTradeOrderFixOrderAllocationService().getTradeOrderFixAllocationReportListByAllocation(allocationParameters.getExternalAllocationIdentifier(), false);
			}

			int id = 100;
			for (FixAllocationReport report : CollectionUtils.getIterable(allocationReportList)) {
				for (FixAllocationReportDetail detail : CollectionUtils.getIterable(report.getFixAllocationReportDetailList())) {
					TradeOrderAllocationDetail allocationDetail = new TradeOrderAllocationDetail();
					allocationDetail.setAllocationAccount(detail.getAllocationAccount());
					if (!MathUtils.isNullOrZero(detail.getAllocationNetMoney()) && MathUtils.isNullOrZero(detail.getAllocationAveragePrice()) && MathUtils.isNullOrZero(detail.getAllocationPrice())) {
						allocationDetail.setAllocationAveragePrice(report.getAveragePrice());
						allocationDetail.setAllocationPrice(report.getAveragePrice());
					}
					else {
						allocationDetail.setAllocationAveragePrice(detail.getAllocationAveragePrice());
						allocationDetail.setAllocationPrice(detail.getAllocationPrice());
					}
					allocationDetail.setAllocationShares(detail.getAllocationShares());
					allocationDetail.setAllocationNetMoney(detail.getAllocationNetMoney());
					allocationDetail.setId(report.getAllocationReportId() + "_" + id);

					id++;
					allocationDetailList.add(allocationDetail);
				}
			}
		}

		return allocationDetailList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderFixOrderAllocationService getTradeOrderFixOrderAllocationService() {
		return this.tradeOrderFixOrderAllocationService;
	}


	public void setTradeOrderFixOrderAllocationService(TradeOrderFixOrderAllocationService tradeOrderFixOrderAllocationService) {
		this.tradeOrderFixOrderAllocationService = tradeOrderFixOrderAllocationService;
	}
}
