package com.clifton.trade.order.fix.order;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>FixOrderService</code> defines methods used to retrieve order specific objects from the FixMessage table.
 *
 * @author mwacker
 */
public interface TradeOrderFixOrderService {

	/**
	 * Returns all FIX messages that pertain to a given order including the allocation messages.
	 */
	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public List<FixMessage> getTradeOrderFixMessageListForOrder(Integer externalOrderIdentifier);


	@DoNotAddRequestMapping
	public List<Integer> getTradeOrderExternalIdentityListByOrder(Integer externalOrderIdentifier);


	////////////////////////////////////////////////////////////////////////////
	////////               FixOrder Business Methods                 /////////// 
	////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	@SecureMethod(securityResource = "Trade")
	public FixOrder getTradeOrderFixOrder(Integer externalOrderIdentifier, boolean fullyPopulated);


	////////////////////////////////////////////////////////////////////////////
	////////          FixExecutionReport Business Methods            /////////// 
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(securityResource = "Trade")
	public List<FixExecutionReport> getTradeOrderFixExecutionReportList(Integer externalOrderIdentifier, boolean fullyPopulated);


	@SecureMethod(securityResource = "Trade")
	public List<FixExecutionReport> getTradeOrderFixExecutionReportListForAllocation(Integer externalOrderIdentifier, boolean fullyPopulated);


	@SecureMethod(securityResource = "Trade")
	public FixExecutionReport getTradeOrderFixExecutionReportLast(Integer externalOrderIdentifier, boolean fullyPopulated, boolean fillReportOnly);
}
