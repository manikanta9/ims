package com.clifton.trade.order.fix;


import com.clifton.fix.order.FixEntity;


/**
 * The <code>TradeOrderFixProcessor</code> defines method(s) used to process incoming FIX object.
 * <p>
 * NOTE: This interface needs to implemented by the OMS that is using this API.
 *
 * @author mwacker
 */
public interface TradeOrderFixProcessor {

	/**
	 * Processes the FIX message and returns the list of FixOrders that where updated or affected.
	 */
	public <T extends FixEntity> void process(T entity);


	/**
	 * Processes the FIX message and returns the list of FixOrders that where updated or affected without suppressing errors.
	 */
	public <T extends FixEntity> void processDoNotSuppressErrors(T entity);
}
