package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Bond trades.
 * Investment Type = Bonds
 *
 * @author davidi
 */
@Component
public class BondsTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	public BondsTradeOrderFixSecurityEntityPopulator() {
		setOrder(90);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.BONDS);
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		final InvestmentSecurity security = tradeOrder.getInvestmentSecurity();
		if ((security.getCusip() == null) || security.getCusip().isEmpty()) {
			throw new RuntimeException("Cannot send fix trade for Bond [" + security.getSymbol() + "] because the CUSIP is not available.");
		}
		securityEntity.setSecurityId(security.getCusip());
		securityEntity.setSecurityIDSource(SecurityIDSources.CUSIP);
		securityEntity.setExDestination(null);
	}
}
