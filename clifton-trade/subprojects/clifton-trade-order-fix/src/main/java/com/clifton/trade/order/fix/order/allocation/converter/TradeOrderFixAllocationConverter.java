package com.clifton.trade.order.fix.order.allocation.converter;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedParty;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.beans.AllocationCancelReplaceReasons;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.order.beans.ProcessCodes;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.util.FixUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMapping;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMappingSearchForm;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.fix.TradeOrderFixService;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import com.clifton.trade.order.fix.util.TradeOrderFixUtilHandler;
import com.clifton.trade.order.fix.util.TradeOrderFixUtils;
import com.clifton.trade.order.util.TradeOrderUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>TradeOrderFixAllocationConverter</code> converts a TradeOrderAllocation to a FixAllocationInstruction
 *
 * @author mwacker
 */
@Component("tradeOrderAllocationConverter")
public class TradeOrderFixAllocationConverter implements Converter<TradeOrderAllocation, FixAllocationInstruction> {

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private TradeOrderFixOrderService tradeOrderFixOrderService;

	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataSourceCompanyService marketDataSourceCompanyService;


	private TradeOrderFixUtilHandler tradeOrderFixUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationInstruction convert(TradeOrderAllocation tradeAllocation) {
		boolean bondTrade = tradeAllocation.getTradeOrderList().get(0).getInvestmentSecurity() != null
				&& InvestmentUtils.isSecurityOfType(tradeAllocation.getTradeOrderList().get(0).getInvestmentSecurity(), InvestmentType.BONDS);

		FixAllocationInstruction result = doConvert(tradeAllocation, bondTrade);

		if (bondTrade) {
			ValidationUtils.assertNotEmpty(result.getFixOrderList().get(0).getFixExecutionReportList(), "Cannot create [" + FixAllocationInstruction.class
					+ "] because no [FixExecutionReport](s) have been provided for order [" + result.getFixOrderList().get(0).getClientOrderStringId() + "].");

			BigDecimal totalAllocationAmount = CoreMathUtils.sumProperty(result.getFixAllocationDetailList(), FixAllocationDetail::getAllocationNetMoney);
			ValidationUtils.assertEquals(0, MathUtils.compare(result.getNetMoney(), totalAllocationAmount),
					"Cannot allocate trade because total net money allocation of the details [" + CoreMathUtils.formatNumberDecimal(totalAllocationAmount)
							+ "] does not equal the total allocation net money [" + CoreMathUtils.formatNumberDecimal(result.getNetMoney()) + "].");
		}
		return result;
	}


	private FixAllocationInstruction doConvert(TradeOrderAllocation tradeAllocation, boolean useNetMoney) {
		TradeOrder firstTradeOrder = tradeAllocation.getTradeOrderList().get(0);
		List<FixOrder> fixOrderList = getFixOrderList(tradeAllocation);
		if (TradeOrderUtils.isBticOrder(firstTradeOrder)) {
			updateFixOrderListForBtic(fixOrderList, firstTradeOrder.getReferenceSecurityPrice(), InvestmentUtils.isSecurityOfType(firstTradeOrder.getInvestmentSecurity(), InvestmentType.FUTURES));
		}

		Map<Integer, FixExecutionReport> lastExecutionReportMap = getLastExecutionReportMap(tradeAllocation);
		TradeDestinationMapping destinationMapping = firstTradeOrder.getDestinationMapping();

		FixAllocationInstruction allocation = new FixAllocationInstruction();
		// set the flag indicating if we are going to use the SecondaryClOrdID
		allocation.setSendSecondaryClientOrderId(destinationMapping.getMappingConfiguration().isFixSecondaryClientOrderIdUsedForAllocations());
		// set the flag indicating we want to include order quantities for each order in the AllocationInstruction NoOrders group
		allocation.setSendOrderQuantityInNoOrdersGrouping(destinationMapping.getMappingConfiguration().isFixAllocationOrderQuantityRequiredInNoOrdersGroup());

		// populate the unique id then update after the id is available
		allocation.setAllocationStringId(FixUtils.createUniqueId(tradeAllocation, TradeOrderFixService.FIX_UNIQUE_ALLOCATION_ID_PREFIX));

		if (destinationMapping.getMappingConfiguration().getFixAllocationType() != null) {
			allocation.setAllocationType(destinationMapping.getMappingConfiguration().getFixAllocationType());
		}
		if (destinationMapping.getMappingConfiguration().isFixSendTopAccountOnAllocationMessages()) {
			allocation.setAccount(getTradeOrderFixUtilHandler().getFixTopAccountNumber(firstTradeOrder));
		}
		allocation.setFixOrderList(fixOrderList);
		TradeOrderFixUtils.setEntityItemsForAllocations(allocation, firstTradeOrder.getDestinationMapping(), true);

		if (tradeAllocation.getOriginalOrderAllocation() == null) {
			allocation.setAllocationTransactionType(AllocationTransactionTypes.NEW);
		}
		else {
			allocation.setAllocationTransactionType(AllocationTransactionTypes.REPLACE);
			allocation.setAllocationCancelReplaceReason(AllocationCancelReplaceReasons.ORIGINAL_DETAILS_INCOMPLETE_INCORRECT);
			allocation.setReferenceAllocationStringId(FixUtils.createUniqueId(tradeAllocation.getOriginalOrderAllocation(), TradeOrderFixService.FIX_UNIQUE_ALLOCATION_ID_PREFIX));
		}
		FixOrder firstOrder = fixOrderList.get(0);
		setSecurityFields(firstTradeOrder, allocation, firstOrder);
		allocation.setOpenClose(firstOrder.getOpenClose());

		BigDecimal quantityFilled = sumQuantityFilled(tradeAllocation.getTradeOrderList());

		allocation.setShares(quantityFilled);
		allocation.setAveragePrice(averagePrice(fixOrderList));

		// set currency (Fix TAG=15)
		if (destinationMapping.getMappingConfiguration().isFixCurrencyCodeRequiredOnAllocationMessages()) {
			Trade firstTrade = CollectionUtils.getFirstElement(firstTradeOrder.getTradeList());
			if (firstTrade != null) {
				allocation.setCurrency(firstTrade.getSettlementCurrency().getSymbol());
			}
		}

		// set CFI Code for traded security -- currently only supports Funds, Options, Futures
		if (destinationMapping.getMappingConfiguration().isFixCFICodeRequiredOnAllocationMessages()) {
			String cfiCode = getInvestmentSecurityUtilHandler().getCFICode(firstTradeOrder.getInvestmentSecurity());
			if (cfiCode != null) {
				allocation.setCFICode(cfiCode);
			}
		}

		if (TradeOrderUtils.isBticOrder(firstTradeOrder)) {
			BigDecimal averageBticPrice = MathUtils.add(allocation.getAveragePrice(), firstTradeOrder.getReferenceSecurityPrice());
			if (InvestmentUtils.isSecurityOfType(firstTradeOrder.getInvestmentSecurity(), InvestmentType.FUTURES)) {
				// Some futures, such as those with RTY underlying, will provide a reference price with three decimal digits. Prices should be limited to two decimal digits for futures.
				averageBticPrice = MathUtils.round(averageBticPrice, 2);
			}
			allocation.setAveragePrice(averageBticPrice);
		}
		allocation.setSide(fixOrderList.get(0).getSide());
		allocation.setTradeDate(fixOrderList.get(0).getTradeDate());
		allocation.setSettlementDate(fixOrderList.get(0).getSettlementDate());
		if (InvestmentUtils.isSecurityOfType(firstTradeOrder.getInvestmentSecurity(), InvestmentType.FUTURES)) {
			allocation.setMaturityMonthYear(getMaturityMonthYear(firstTradeOrder.getInvestmentSecurity()));
		}

		// set the price type from the execution report
		FixExecutionReport lr = lastExecutionReportMap.get(firstTradeOrder.getId());
		if (lr.getPriceType() != null) {
			allocation.setPriceType(lr.getPriceType());
		}
		// add the production code
		if (lr.getProduct() != null) {
			allocation.setProduct(lr.getProduct());
		}
		// use net money for bonds
		if (useNetMoney) {
			if (lr.getNetMoney() != null) {
				allocation.setNetMoney(CoreMathUtils.sumProperty(new ArrayList<>(lastExecutionReportMap.values()), FixExecutionReport::getNetMoney));
			}
			if (lr.getGrossTradeAmount() != null) {
				allocation.setGrossTradeAmount(CoreMathUtils.sumProperty(new ArrayList<>(lastExecutionReportMap.values()), FixExecutionReport::getGrossTradeAmount));
			}
		}
		allocation.setAllocationNoOrdersType(AllocationNoOrdersTypes.EXPLICIT_LIST_PROVIDED);

		BigDecimal totalNetMoney = BigDecimal.ZERO;
		List<FixAllocationDetail> allocationDetailList = new ArrayList<>();
		int orderCount = 0;
		for (TradeOrder order : CollectionUtils.getIterable(tradeAllocation.getTradeOrderList())) {
			orderCount++;
			boolean lastOrder = orderCount == tradeAllocation.getTradeOrderList().size();

			FixExecutionReport lastExecutionReport = lastExecutionReportMap.get(order.getId());

			// set the trader user
			if (StringUtils.isEmpty(allocation.getSenderUserName())) {
				allocation.setSenderUserName(firstOrder.getSenderUserName());
			}

			// set the parties from the order
			allocation.setPartyList(getTradeOrderFixUtilHandler().getFixPartyList(order));
			// set the parties from the execution report
			if (allocation.getPartyList() == null) {
				allocation.setPartyList(new ArrayList<>());
			}

			String overrideAllocationBrokerCode = firstTradeOrder.getDestinationMapping().getOverrideAllocationBrokerCode();
			for (FixParty party : CollectionUtils.getIterable(lastExecutionReport.getPartyList())) {
				if (overrideAllocationBrokerCode != null && PartyRoles.EXECUTING_FIRM == party.getPartyRole()) {
					party.setPartyId(overrideAllocationBrokerCode);
				}
				if (!allocation.getPartyList().contains(party)) {
					if (PartyRoles.ORDER_ORIGINATION_TRADER == party.getPartyRole()) {
						party.setSenderUserName(order.getTraderUser().getUserName());
					}
					allocation.getPartyList().add(party);
				}
			}

			int tradeCount = 0;
			for (Trade trade : CollectionUtils.getIterable(order.getTradeList())) {
				tradeCount++;
				boolean lastTrade = tradeCount == order.getTradeList().size();

				FixAllocationDetail detail = new FixAllocationDetail();
				detail.setFixAllocationInstruction(allocation);

				InvestmentAccount allocationAccount = trade.getHoldingInvestmentAccount();
				// if the destination uses executing accounts, then get the execution account
				if (destinationMapping.getMappingConfiguration().isFixExecutingAccountUsed()) {
					InvestmentAccount ia = lookupExecutingAccount(trade, TradeOrderFixService.EXECUTING_PURPOSE_NAME_PREFIX + ": "
							+ trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName());

					if ((ia != null) && (ia.getNumber() != null) && !ia.getNumber().isEmpty()) {
						allocationAccount = ia;
					}
				}
				else if (destinationMapping.getMappingConfiguration().isSendClientAccountWithOrder()) {
					allocationAccount = trade.getClientInvestmentAccount();
				}
				detail.setAllocationAccount(getTradeOrderFixUtilHandler().getAllocationAccountNumber(allocationAccount, destinationMapping.getAllocationAccountMappingPurpose()));
				addAllocationDetailParties(destinationMapping, trade.getExecutingBrokerCompany(), allocationAccount, detail);
				if (!allocationAccount.getIssuingCompany().equals(trade.getExecutingBrokerCompany())) {
					detail.setProcessCode(ProcessCodes.STEP_OUT);
				}
				else {
					detail.setProcessCode(ProcessCodes.REGULAR);
				}
				// set the shares
				detail.setAllocationShares(TradeOrderUtils.getTradeQuantity(trade));

				// set allocation price
				if (destinationMapping.getMappingConfiguration().isFixAllocationPriceRequiredInAllocationDetails()) {
					detail.setAllocationPrice(lastExecutionReport.getAveragePrice());
				}

				// set the net money
				if (useNetMoney && allocation.getNetMoney() != null) {
					detail.setAllocationPrice(lastExecutionReport.getAveragePrice());
					if (lastOrder && lastTrade) {
						detail.setAllocationNetMoney(MathUtils.subtract(allocation.getNetMoney(), totalNetMoney));
					}
					else {
						detail.setAllocationNetMoney(MathUtils.multiply(allocation.getNetMoney(), MathUtils.divide(TradeOrderUtils.getTradeQuantity(trade), quantityFilled), 2));
						totalNetMoney = MathUtils.add(totalNetMoney, detail.getAllocationNetMoney());
					}
				}

				// add the commission data
				// TODO: add logic to implement average pricing strategies based of what's defined in the destination map
				if (destinationMapping.getMappingConfiguration().getFixCommissionType() != null) {
					detail.setAllocationPrice(lastExecutionReport.getAveragePrice().setScale(6, RoundingMode.HALF_UP));
					detail.setCommissionType(destinationMapping.getMappingConfiguration().getFixCommissionType());
					if (trade.getCommissionPerUnit() != null) {
						detail.setCommission(trade.getCommissionPerUnit().setScale(6, RoundingMode.HALF_UP));
					}
					else {
						detail.setCommission(destinationMapping.getMappingConfiguration().getFixDefaultCommissionPerUnit().setScale(6, RoundingMode.HALF_UP));
					}
					detail.setProcessCode(destinationMapping.getMappingConfiguration().getFixProcessCode());
				}
				allocationDetailList.add(detail);
			}
		}
		allocation.setFixAllocationDetailList(allocationDetailList);

		BigDecimal allocationQty = CoreMathUtils.sumProperty(allocationDetailList, FixAllocationDetail::getAllocationShares);
		ValidationUtils.assertTrue(MathUtils.compare(quantityFilled, allocationQty) == 0, "The quantity being allocated [" + CoreMathUtils.formatNumberInteger(allocationQty)
				+ "] does not match the total quantity of all orders [" + CoreMathUtils.formatNumberInteger(quantityFilled) + "].");
		return allocation;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Updates the {@link FixExecutionReport} last and average prices for each provided {@link FixOrder} for BTIC orders by adding the price spread to the provided closing price.
	 * <p>
	 * Some futures, such as those with RTY underlying, will provide a reference price with three decimal digits. Prices should be limited to two decimal digits for futures.
	 */
	private void updateFixOrderListForBtic(List<FixOrder> fixOrderList, BigDecimal priceAtMarketClose, boolean roundToTwoDecimals) {
		final String manualProcessingInstructionSuffix = "_MAN";
		for (FixOrder fixOrder : CollectionUtils.getIterable(fixOrderList)) {
			for (FixExecutionReport executionReport : CollectionUtils.getIterable(fixOrder.getFixExecutionReportList())) {
				executionReport.setExecutionId(executionReport.getExecutionId() + manualProcessingInstructionSuffix);
				executionReport.setOrderId(executionReport.getOrderId() + manualProcessingInstructionSuffix);
				BigDecimal price = CoreMathUtils.sum(priceAtMarketClose, executionReport.getLastPrice());
				executionReport.setLastPrice(roundToTwoDecimals ? MathUtils.round(price, 2) : price);
				price = CoreMathUtils.sum(priceAtMarketClose, executionReport.getAveragePrice());
				executionReport.setAveragePrice(roundToTwoDecimals ? MathUtils.round(price, 2) : price);
			}
		}
	}


	private void setSecurityFields(TradeOrder order, FixSecurityEntity entity, FixOrder fixOrder) {
		final InvestmentSecurity security = order.getInvestmentSecurity();
		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.OPTIONS)) {
			entity.setSecurityType(SecurityTypes.OPTION);

			if (InvestmentUtils.isSecurityOfType(security.getUnderlyingSecurity(), InvestmentType.FUTURES)) {
				entity.setSymbol(InvestmentUtils.getOptionOnFutureSymbolWithoutStrikePrice(security));
				String securityId = security.getSymbol() + " " + getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), null).getMarketSector().getName();
				entity.setSecurityId(securityId);
			}
			else {
				// use the bloomberg symbol for Options allocations
				entity.setSymbol(security.getSymbol());
			}

			entity.setMaturityMonthYear(DateUtils.fromDate(security.getLastDeliveryDate(), "yyyyMM"));
			entity.setMaturityDay(DateUtils.fromDate(security.getLastDeliveryDate(), "dd"));
			entity.setPutOrCall(security.isPutOption() ? PutOrCalls.PUT : PutOrCalls.CALL);
			entity.setStrikePrice(security.getOptionStrikePrice());
			entity.setCFICode(getInvestmentSecurityUtilHandler().getOptionCFICode(security));
		}
		else {
			// for Future BTIC allocation instructions or allocation instructions that flow over a separate allocation connection we need to restore the original security symbol and securityID.
			if (TradeOrderUtils.isBticOrder(order) || TradeOrderUtils.isAllocationConnectionUsed(order)) {
				String symbol = security.getSymbol() + " " + getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), null).getMarketSector().getName();
				entity.setSymbol(security.getSymbol());
				entity.setSecurityId(symbol);
			}
			else {
				entity.setSymbol(fixOrder.getSymbol());
				entity.setSecurityId(fixOrder.getSecurityId());
			}
		}
		entity.setSecurityIDSource(fixOrder.getSecurityIDSource());
		if (order.getDestinationMapping().getMappingConfiguration().isFixMICExchangeCodeRequiredOnAllocationMessages()) {
			entity.setSecurityExchange(getTradeOrderFixUtilHandler().getExchangeMICCode(order));
		}
	}


	private void addAllocationDetailParties(TradeDestinationMapping destinationMapping, BusinessCompany executingBroker, InvestmentAccount account, FixAllocationDetail detail) {
		List<FixAllocationDetailNestedParty> partyList = new ArrayList<>();
		// add the proprietary code if required
		if (destinationMapping.getMappingConfiguration().isFixSendBrokerPartyIdsOnAllocationMessages()) {
			try {
				MarketDataSource source = getMarketDataSourceService().getMarketDataSourceByCompany(executingBroker);

				MarketDataSourceCompanyMappingSearchForm searchForm = new MarketDataSourceCompanyMappingSearchForm();
				searchForm.setMarketDataSourceId(source.getId());
				searchForm.setMarketDataSourcePurposeId(destinationMapping.getTradeDestination().getMarketDataSourcePurpose().getId());
				searchForm.setBusinessCompanyId(account.getIssuingCompany().getId());

				MarketDataSourceCompanyMapping mapping = CollectionUtils.getOnlyElement(getMarketDataSourceCompanyService().getMarketDataSourceCompanyMappingList(searchForm));
				if (mapping != null) {
					partyList.add(createNestedParty(mapping.getExternalCompanyName(), PartyRoles.CLEARING_FIRM, PartyIdSources.PROPRIETARY_CUSTOM_CODE));
				}
			}
			catch (Throwable e) {
				// if the datasource is not found, then we will simply not add the nested party for the proprietary code
				LogUtils.info(getClass(), "Failed to find datasource for executing broker [" + executingBroker + "].", e);
			}
		}
		// add the LEI code if required
		if (destinationMapping.getMappingConfiguration().isFixSendClearingFirmLEIWithAllocation()) {
			String legalEntityIdentifier = account.getIssuingCompany().getLegalEntityIdentifier();
			if (!StringUtils.isEmpty(legalEntityIdentifier)) {
				partyList.add(createNestedParty(legalEntityIdentifier, PartyRoles.CLEARING_FIRM, PartyIdSources.LEGAL_ENTITY_IDENTIFIER));
			}
		}

		if (!CollectionUtils.isEmpty(partyList)) {
			detail.setNestedPartyList(partyList);
		}
	}


	private FixAllocationDetailNestedParty createNestedParty(String partyId, PartyRoles role, PartyIdSources source) {
		FixAllocationDetailNestedParty nestedParty = new FixAllocationDetailNestedParty();
		nestedParty.setNestedPartyId(partyId);
		nestedParty.setNestedPartyRole(role);
		nestedParty.setNestedPartyIDSource(source);
		return nestedParty;
	}


	private String getMaturityMonthYear(InvestmentSecurity security) {
		Integer yearCode = InvestmentUtils.getFutureYear(security);
		Integer monthCode = InvestmentUtils.getFutureMonth(security);
		if (monthCode != null) {
			return yearCode + "" + CoreMathUtils.formatNumber(new BigDecimal(monthCode), "00");
		}
		return null;
	}


	private List<FixOrder> getFixOrderList(TradeOrderAllocation tradeAllocation) {
		List<FixOrder> fixOrderList = new ArrayList<>();
		for (TradeOrder tradeOrder : CollectionUtils.getIterable(tradeAllocation.getTradeOrderList())) {
			FixOrder fixOrder = getTradeOrderFixOrderService().getTradeOrderFixOrder(tradeOrder.getExternalIdentifier(), false);
			fixOrder.setFixExecutionReportList(getTradeOrderFixOrderService().getTradeOrderFixExecutionReportListForAllocation(tradeOrder.getExternalIdentifier(), false));
			fixOrderList.add(fixOrder);
		}
		return fixOrderList;
	}


	private Map<Integer, FixExecutionReport> getLastExecutionReportMap(TradeOrderAllocation tradeAllocation) {
		Map<Integer, FixExecutionReport> lastExecutionReportMap = new HashMap<>();
		for (TradeOrder tradeOrder : CollectionUtils.getIterable(tradeAllocation.getTradeOrderList())) {
			lastExecutionReportMap.put(tradeOrder.getId(), getTradeOrderFixOrderService().getTradeOrderFixExecutionReportLast(tradeOrder.getExternalIdentifier(), false, false));
		}
		return lastExecutionReportMap;
	}


	private BigDecimal sumQuantityFilled(List<TradeOrder> orderList) {
		BigDecimal result = BigDecimal.ZERO;
		for (TradeOrder order : orderList) {
			result = result.add(order.getQuantityFilled());
		}
		return result;
	}


	private BigDecimal averagePrice(List<FixOrder> orderList) {
		BigDecimal sumShares = BigDecimal.ZERO;
		BigDecimal totalCost = BigDecimal.ZERO;
		for (FixOrder order : orderList) {
			FixExecutionReport report = getTradeOrderFixOrderService().getTradeOrderFixExecutionReportLast(order.getIdentifier().getId(), false, false);
			if (report == null) {
				throw new RuntimeException("Cannot allocate a trade that is not filled.");
			}
			sumShares = sumShares.add(report.getCumulativeQuantity());
			totalCost = totalCost.add(report.getAveragePrice().multiply(report.getCumulativeQuantity()));
		}
		return MathUtils.divide(totalCost, sumShares, 10);
	}


	private InvestmentAccount lookupExecutingAccount(Trade trade, String purposeName) {
		InvestmentAccountRelationshipSearchForm searchForm = new InvestmentAccountRelationshipSearchForm();
		searchForm.setPurposeName(purposeName);
		searchForm.setMainAccountId(trade.getHoldingInvestmentAccount().getId());
		searchForm.setRelatedAccountIssuingCompanyId(trade.getExecutingBrokerCompany().getId());
		searchForm.setActiveOnDate(trade.getTradeDate());
		InvestmentAccountRelationship relationship = CollectionUtils.getOnlyElement(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm));
		return relationship != null ? relationship.getReferenceTwo() : null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeOrderFixOrderService getTradeOrderFixOrderService() {
		return this.tradeOrderFixOrderService;
	}


	public void setTradeOrderFixOrderService(TradeOrderFixOrderService tradeOrderFixOrderService) {
		this.tradeOrderFixOrderService = tradeOrderFixOrderService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public TradeOrderFixUtilHandler getTradeOrderFixUtilHandler() {
		return this.tradeOrderFixUtilHandler;
	}


	public void setTradeOrderFixUtilHandler(TradeOrderFixUtilHandler tradeOrderFixUtilHandler) {
		this.tradeOrderFixUtilHandler = tradeOrderFixUtilHandler;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataSourceCompanyService getMarketDataSourceCompanyService() {
		return this.marketDataSourceCompanyService;
	}


	public void setMarketDataSourceCompanyService(MarketDataSourceCompanyService marketDataSourceCompanyService) {
		this.marketDataSourceCompanyService = marketDataSourceCompanyService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
