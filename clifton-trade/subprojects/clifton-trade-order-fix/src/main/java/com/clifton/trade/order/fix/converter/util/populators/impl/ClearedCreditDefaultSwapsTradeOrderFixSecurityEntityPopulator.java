package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Cleared Credit Default Swap trades.
 * InvestmentType = Swaps and TradeType = Cleared Credit Default Swaps
 *
 * @author davidi
 */
@Component
public class ClearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ClearedCreditDefaultSwapsTradeOrderFixSecurityEntityPopulator() {
		setOrder(100);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.SWAPS) && TradeType.CLEARED_CREDIT_DEFAULT_SWAPS.equals(firstTrade.getTradeType().getName());
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		final InvestmentSecurity security = tradeOrder.getInvestmentSecurity();
		final InvestmentSecurity underlying = security.getUnderlyingSecurity();

		if ((security.getUnderlyingSecurity() == null) || (security.getCusip() == null) || security.getCusip().isEmpty()) {
			throw new RuntimeException("Cannot send fix trade for Swap [" + security.getSymbol() + "] because the CUSIP (a.k.a. Red Code) of the underlying benchmark is not available.");
		}
		securityEntity.setSecurityId(security.getUnderlyingSecurity().getCusip());
		securityEntity.setSecurityIDSource(SecurityIDSources.RED_CODE);
		securityEntity.setExDestination(null);

		Object tenor = getInvestmentSecurityUtilHandler().getCustomColumnValue(underlying, InvestmentSecurity.CUSTOM_FIELD_TENOR);
		if (tenor != null) {
			securityEntity.setTenor("Y" + tenor);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}
}
