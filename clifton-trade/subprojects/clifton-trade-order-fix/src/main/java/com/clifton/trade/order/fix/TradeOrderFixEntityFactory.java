package com.clifton.trade.order.fix;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.beans.AllocationCancelReplaceReasons;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.factory.AbstractFixEntityFactory;
import com.clifton.fix.order.factory.FixEntityFactoryGenerationTypes;
import com.clifton.fix.util.FixUtils;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import com.clifton.trade.order.fix.util.TradeOrderFixUtilHandler;
import com.clifton.trade.order.fix.util.TradeOrderFixUtils;
import com.clifton.trade.order.search.TradeOrderSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>TradeOrderFixEntityFactory</code> implements the FixEntityFactory used to generate FixEntities from TradeOrder and TradeOrderAllocation objects.
 *
 * @author mwacker
 */
@Component("fixEntityFactory")
public class TradeOrderFixEntityFactory extends AbstractFixEntityFactory {

	private TradeOrderFixOrderService tradeOrderFixOrderService;
	private TradeOrderService tradeOrderService;
	private TradeOrderFixUtilHandler tradeOrderFixUtilHandler;

	private Converter<TradeOrder, FixOrder> tradeOrderFixOrderConverter;
	private Converter<TradeOrder, FixOrderCancelReplaceRequest> tradeOrderFixOrderCancelReplaceRequestConverter;
	private Converter<TradeOrderAllocation, FixAllocationInstruction> tradeOrderAllocationConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	protected <F extends FixEntity, O> F doCreate(O from, FixEntityFactoryGenerationTypes action) {
		F result = null;
		switch (action) {
			case ORDER_SINGLE:
				ValidationUtils.assertTrue(from instanceof TradeOrder, "Generation type [" + action + "] is not applicable to type [" + from.getClass() + "].");
				result = (F) generateOrderMessage((TradeOrder) from);
				break;
			case ORDER_CANCEL_REQUEST:
				ValidationUtils.assertTrue(from instanceof TradeOrder, "Generation type [" + action + "] is not applicable to type [" + from.getClass() + "].");
				result = (F) generateOrderCancelRequest((TradeOrder) from);
				break;
			case ORDER_CANCEL_REPLACE_REQUEST:
				ValidationUtils.assertTrue(from instanceof TradeOrder, "Generation type [" + action + "] is not applicable to type [" + from.getClass() + "].");
				result = (F) generateOrderCancelReplaceRequest((TradeOrder) from);
				break;
			case ALLOCATION: {
				ValidationUtils.assertTrue(from instanceof TradeOrderAllocation, "Generation type [" + action + "] is not applicable to type [" + from.getClass() + "].");
				result = (F) generateAllocationMessage((TradeOrderAllocation) from);
				break;
			}
			case ALLOCATION_CANCEL: {
				ValidationUtils.assertTrue(from instanceof TradeOrderAllocation, "Generation type [" + action + "] is not applicable to type [" + from.getClass() + "].");
				result = (F) generateAllocationCancel((TradeOrderAllocation) from);
				break;
			}
		}
		return result;
	}


	public FixOrderCancelRequest generateOrderCancelRequest(TradeOrder tradeOrder) {
		FixOrder order = getTradeOrderFixOrderService().getTradeOrderFixOrder(tradeOrder.getExternalIdentifier(), false);
		FixOrderCancelRequest result = new FixOrderCancelRequest();
		TradeOrderFixUtils.setEntityItems(result, tradeOrder.getDestinationMapping());
		result.setClientOrderStringId(FixUtils.createUniqueIdForCancel(tradeOrder, TradeOrderFixService.FIX_UNIQUE_ORDER_ID_PREFIX));
		result.setOriginalClientOrderStringId(FixUtils.createUniqueId(tradeOrder, TradeOrderFixService.FIX_UNIQUE_ORDER_ID_PREFIX));
		result.setTransactionTime(new Date());
		result.setSymbol(order.getSymbol());
		result.setSecurityId(order.getSecurityId());
		result.setSecurityIDSource(order.getSecurityIDSource());
		result.setSecurityType(order.getSecurityType());

		// Options
		result.setMaturityMonthYear(order.getMaturityMonthYear());
		result.setMaturityDay(order.getMaturityDay());
		result.setPutOrCall(order.getPutOrCall());
		result.setStrikePrice(order.getStrikePrice());
		result.setCFICode(order.getCFICode());

		result.setHandlingInstructions(order.getHandlingInstructions());
		result.setFixSenderSubId(order.getFixSenderSubId());
		result.setSenderUserName(order.getSenderUserName());
		result.setOrderQty(tradeOrder.getQuantity());
		result.setSide(order.getSide());
		return result;
	}


	public FixOrderCancelReplaceRequest generateOrderCancelReplaceRequest(TradeOrder tradeOrder) {
		return getTradeOrderFixOrderCancelReplaceRequestConverter().convert(tradeOrder);
	}


	public FixAllocationInstruction generateAllocationCancel(TradeOrderAllocation tradeAllocation) {
		FixAllocationInstruction result = new FixAllocationInstruction();
		result = TradeOrderFixUtils.setEntityItems(result, tradeAllocation.getTradeOrderList().get(0).getDestinationMapping(), true);
		result.setTradeDate(tradeAllocation.getTradeOrderList().get(0).getTradeDate());
		result.setAllocationStringId(FixUtils.createUniqueId(tradeAllocation, TradeOrderFixService.FIX_UNIQUE_ALLOCATION_ID_PREFIX) + "_CANCEL");
		result.setReferenceAllocationStringId(FixUtils.createUniqueId(tradeAllocation, TradeOrderFixService.FIX_UNIQUE_ALLOCATION_ID_PREFIX));
		result.setAllocationTransactionType(AllocationTransactionTypes.CANCEL);
		result.setAllocationCancelReplaceReason(AllocationCancelReplaceReasons.ORIGINAL_DETAILS_INCOMPLETE_INCORRECT);


		TradeOrderSearchForm searchForm = new TradeOrderSearchForm();
		searchForm.setAllocationIdForAllOrders(tradeAllocation.getId());
		TradeOrder order = CollectionUtils.getFirstElement(getTradeOrderService().getTradeOrderList(searchForm));
		if (order != null) {
			// set the parties from the order
			result.setPartyList(getTradeOrderFixUtilHandler().getFixPartyList(order));
		}
		return result;
	}


	public FixAllocationInstruction generateAllocationMessage(TradeOrderAllocation tradeAllocation) {
		return getTradeOrderAllocationConverter().convert(tradeAllocation);
	}


	public FixOrder generateOrderMessage(TradeOrder tradeOrder) {
		return getTradeOrderFixOrderConverter().convert(tradeOrder);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Converter<TradeOrder, FixOrder> getTradeOrderFixOrderConverter() {
		return this.tradeOrderFixOrderConverter;
	}


	public void setTradeOrderFixOrderConverter(Converter<TradeOrder, FixOrder> tradeOrderFixOrderConverter) {
		this.tradeOrderFixOrderConverter = tradeOrderFixOrderConverter;
	}


	public Converter<TradeOrderAllocation, FixAllocationInstruction> getTradeOrderAllocationConverter() {
		return this.tradeOrderAllocationConverter;
	}


	public void setTradeOrderAllocationConverter(Converter<TradeOrderAllocation, FixAllocationInstruction> tradeOrderAllocationConverter) {
		this.tradeOrderAllocationConverter = tradeOrderAllocationConverter;
	}


	public Converter<TradeOrder, FixOrderCancelReplaceRequest> getTradeOrderFixOrderCancelReplaceRequestConverter() {
		return this.tradeOrderFixOrderCancelReplaceRequestConverter;
	}


	public void setTradeOrderFixOrderCancelReplaceRequestConverter(Converter<TradeOrder, FixOrderCancelReplaceRequest> tradeOrderFixOrderCancelReplaceRequestConverter) {
		this.tradeOrderFixOrderCancelReplaceRequestConverter = tradeOrderFixOrderCancelReplaceRequestConverter;
	}


	public TradeOrderFixUtilHandler getTradeOrderFixUtilHandler() {
		return this.tradeOrderFixUtilHandler;
	}


	public void setTradeOrderFixUtilHandler(TradeOrderFixUtilHandler tradeOrderFixUtilHandler) {
		this.tradeOrderFixUtilHandler = tradeOrderFixUtilHandler;
	}


	public TradeOrderService getTradeOrderService() {
		return this.tradeOrderService;
	}


	public void setTradeOrderService(TradeOrderService tradeOrderService) {
		this.tradeOrderService = tradeOrderService;
	}


	public TradeOrderFixOrderService getTradeOrderFixOrderService() {
		return this.tradeOrderFixOrderService;
	}


	public void setTradeOrderFixOrderService(TradeOrderFixOrderService tradeOrderFixOrderService) {
		this.tradeOrderFixOrderService = tradeOrderFixOrderService;
	}
}
