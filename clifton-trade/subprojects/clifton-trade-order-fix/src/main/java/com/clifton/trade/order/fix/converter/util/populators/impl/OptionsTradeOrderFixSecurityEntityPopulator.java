package com.clifton.trade.order.fix.converter.util.populators.impl;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.fix.converter.util.populators.BaseTradeOrderFixSecurityEntityPopulator;
import org.springframework.stereotype.Component;


/**
 * Populates the FixSecurityEntity properties specific to Option trades.
 * InvestmentType = Options
 *
 * @author davidi
 */
@Component
public class OptionsTradeOrderFixSecurityEntityPopulator extends BaseTradeOrderFixSecurityEntityPopulator {

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	public OptionsTradeOrderFixSecurityEntityPopulator() {
		setOrder(40);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade) {
		return InvestmentUtils.isSecurityOfType(tradeOrder.getInvestmentSecurity(), InvestmentType.OPTIONS);
	}


	@Override
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade) {
		InvestmentSecurity security = tradeOrder.getInvestmentSecurity();
		ValidationUtils.assertNotNull(security.getUnderlyingSecurity(), "The option security: \"" + security.getLabel() + "\" requires an underlying security");
		securityEntity.setSecurityType(SecurityTypes.OPTION);

		if (InvestmentUtils.isSecurityOfType(security.getUnderlyingSecurity(), InvestmentType.FUTURES)) {
			MarketDataFieldMapping fieldMapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), null);
			securityEntity.setSecurityId(security.getSymbol() + " " + fieldMapping.getMarketSector().getName());
			securityEntity.setSymbol(InvestmentUtils.getOptionOnFutureSymbolWithoutStrikePrice(security));
			securityEntity.setUnderlyingSecurityType(SecurityTypes.FUTURE);
			securityEntity.setSecurityIDSource(SecurityIDSources.BID);
		}
		else {
			// Note that for listed options the option symbol is not always based on the underlying ticker symbol.
			// In such cases, use the symbol extracted from the full option ticker as the symbol for the securityEntity.
			String optionSymbol = InvestmentUtils.getOptionSymbolPrefix(security.getSymbol());
			if (!StringUtils.isEmpty(optionSymbol)) {
				securityEntity.setSymbol(optionSymbol);
			}
			else {
				securityEntity.setSymbol(security.getUnderlyingSecurity().getSymbol());
			}
		}

		securityEntity.setMaturityMonthYear(DateUtils.fromDate(security.getLastDeliveryDate(), "yyyyMM"));
		securityEntity.setMaturityDay(DateUtils.fromDate(security.getLastDeliveryDate(), "dd"));
		securityEntity.setPutOrCall(security.isPutOption() ? PutOrCalls.PUT : PutOrCalls.CALL);
		securityEntity.setStrikePrice(security.getOptionStrikePrice());
		securityEntity.setCFICode(getInvestmentSecurityUtilHandler().getOptionCFICode(security));
	}


	/////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
