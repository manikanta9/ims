package com.clifton.trade.order.fix.messaging;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.order.fix.TradeOrderFixProcessor;


/**
 * The <code>TradeOrderFixMessagingProcessor</code> processes incoming asynchronous JMS messages from the the FIX server.
 * <p>
 * NOTE:  This implementation simply converts the message to a FixEntity then calls a FixClientProcessor
 * that should be implemented in an other project (currently that is in the Trade project).
 *
 * @author mwacker
 */
public class TradeOrderFixMessagingProcessor implements AsynchronousProcessor<AsynchronousMessage> {


	private ContextHandler contextHandler;


	private FixEntityService fixEntityService;

	/**
	 * Used to processes the FixEntity.
	 * <p>
	 * NOTE: This interface is implemented by the OMS in which is currently in the trade project
	 * and implements in com.clifton.trade.order.fix.TradeOrderFixClientProcessor
	 */
	private TradeOrderFixProcessor tradeOrderFixProcessor;


	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////


	/**
	 * A default user need to run the process so there is a user for database updates and inserts.
	 */
	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(AsynchronousMessage msg, @SuppressWarnings("unused") AsynchronousMessageHandler handler) {
		if (msg != null) {
			// set the user for database
			setRunAsUser();

			if (msg instanceof BaseFixMessagingMessage) {
				FixEntity entity = null;
				try {
					if (msg instanceof FixMessageTextMessagingMessage) {
						FixMessageTextMessagingMessage message = (FixMessageTextMessagingMessage) msg;

						entity = getFixEntityService().getFixEntityForFixMessageText(message.getMessageText(), message.getSessionQualifier());
						entity.setIdentifier(message.getIdentifier());
						entity.setAllocationValidationError(message.isAllocationValidationError());
						entity.setValidationError(message.isValidationError());
						entity.setFixDestinationName(message.getDestinationName());
					}
					else if (msg instanceof FixEntityMessagingMessage) {
						FixEntityMessagingMessage message = (FixEntityMessagingMessage) msg;
						entity = message.getFixEntity();
					}
					else {
						throw new RuntimeException("Cannot process BaseFixMessagingMessage, unsupported subtype: " + msg.getClass().getName());
					}
					entity.setIncoming(true);
					getTradeOrderFixProcessor().process(entity);
				}
				catch (Throwable e) {
					throw new RuntimeException("Error processing TradeOrder FIX message: " + (entity == null ? msg : entity.getIdentifier()), e);
				}
			}
		}
	}


	private void setRunAsUser() {
		if ((getContextHandler() != null) && getContextHandler().getBean(Context.USER_BEAN_NAME) == null) {
			SecurityUser result = getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
			getContextHandler().setBean(Context.USER_BEAN_NAME, result);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}


	public TradeOrderFixProcessor getTradeOrderFixProcessor() {
		return this.tradeOrderFixProcessor;
	}


	public void setTradeOrderFixProcessor(TradeOrderFixProcessor tradeOrderFixProcessor) {
		this.tradeOrderFixProcessor = tradeOrderFixProcessor;
	}


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
