package com.clifton.trade.order.fix.converter.util.populators;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.trade.Trade;
import com.clifton.trade.order.TradeOrder;
import org.springframework.core.Ordered;


/**
 * A populator for a {@link FixSecurityEntity} that extracts relevant  data from a {@link TradeOrder} and {@link Trade}
 *
 * @author davidi
 */
public interface TradeOrderFixSecurityEntityPopulator extends Ordered {

	/**
	 * A method used to determine if the populate action is applicable to the tradeOrder based on properties
	 * within the trade order and firstTrade.  This method should be called before calling populateFixSecurityEntityFields(...)
	 * to determine if the population operation should be executed.
	 *
	 * @return True if populator is applicable to the tradeOrder, False if not.
	 */
	public boolean isApplicable(TradeOrder tradeOrder, Trade firstTrade);


	/**
	 * Populates the FixSecurityEntity fields based on data within the TradeOrder and Trade parameters. This
	 * method should only be called if the call to the isApplicable(TradeOrder tradeOrder, Trade firstTrade) method returns true.
	 */
	public void populateFixSecurityEntityFields(FixSecurityEntity securityEntity, TradeOrder tradeOrder, Trade firstTrade);
}
