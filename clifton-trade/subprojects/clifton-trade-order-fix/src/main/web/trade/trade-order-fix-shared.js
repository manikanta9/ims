Ext.ns('Clifton.trade.order.fix', 'Clifton.trade.order.fix.message', 'Clifton.trade.order.fix.message.validation', 'Clifton.trade.order.fix.order', 'Clifton.trade.order.fix.order.allocation');

Clifton.trade.order.GetTradeOrderFixWindowClassName = function() {
	return 'Clifton.trade.order.fix.TradeOrderFixWindow';
};


// Add Re-Process Button
Ext.override(Clifton.fix.FixMessageGrid, {
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Reprocess Selected',
				tooltip: 'Reprocess selected messages',
				iconCls: 'run',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one message to be processed.', 'No Message(s) Selected');
					}
					else {
						const messages = sm.getSelections();
						const messageIdList = [];
						for (let i = 0; i < messages.length; i++) {
							messageIdList[i] = messages[i].id;
						}
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Processing Messages',
							waitTarget: gridPanel,
							params: {
								messageIdList: messageIdList
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('tradeOrderFixReprocessFixMessageList.json');
					}
				}
			}, '-', {
				xtype: 'fix-download-selected-message-button'
			}, '-');
		}
	}
});


Clifton.trade.order.fix.order.FixExecutionReportGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeOrderFixExecutionReportList',
	xtype: 'gridpanel',
	instructions: 'The following execution reports have been received for selected FIX Order. These are the messages that we received from FIX provider about the order: confirm receipt, fills, cancellation, reject (duplicates), etc.',
	getLoadParams: function() {
		const win = this.getWindow();
		return {
			'externalOrderIdentifier': win.defaultData.externalIdentifier,
			'fullyPopulated': false,
			requestedMaxDepth: 4,
			readUncommittedRequested: true
		};
	},
	isPagingEnabled: function() {
		return true;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Order ID', width: 15, dataIndex: 'orderId', hidden: true},
		{header: 'Secondary Order ID', width: 15, dataIndex: 'secondaryOrderId', hidden: true},
		{header: 'Client Order ID', width: 15, dataIndex: 'clientOrderStringId', hidden: true},
		{header: 'Original Client Order ID', width: 15, dataIndex: 'originalClientOrderStringId', hidden: true},

		{header: 'Received', width: 60, dataIndex: 'messageDateAndTime', type: 'date'},
		{header: 'Type', width: 40, dataIndex: 'executionTransactionType'},
		{header: 'Exec. Type', width: 40, dataIndex: 'executionType', hidden: true},
		{header: 'Exec. ID', width: 40, dataIndex: 'executionId', hidden: true},
		{header: 'Ref Exec. ID', width: 40, dataIndex: 'executionReferenceId', hidden: true},
		{
			header: 'Status', width: 60, dataIndex: 'orderStatus', filter: {
				type: 'list', options: [
					['NEW', 'NEW'], ['PARTIALLY_FILLED', 'PARTIALLY_FILLED'],
					['FILLED', 'FILLED'], ['CANCELED', 'CANCELED'],
					['DONE_FOR_DAY', 'DONE_FOR_DAY'], ['REPLACED', 'REPLACED'],
					['PENDING_CANCEL', 'PENDING_CANCEL'], ['STOPPED', 'STOPPED'],
					['REJECTED', 'REJECTED'], ['PENDING_REPLACE', 'PENDING_REPLACE']]
			}
		},
		{header: 'Text', width: 70, dataIndex: 'text'},
		{
			header: 'Buy/Sell', width: 25, dataIndex: 'side', filter: {type: 'list', options: [['BUY', 'BUY'], ['SELL', 'SELL']]},
			renderer: function(v, metaData) {
				metaData.css = (v === 'BUY') ? 'buy-light' : 'sell-light';
				return v;
			}
		},
		{header: 'Order Type', width: 40, dataIndex: 'type', hidden: true},
		{header: 'Quantity', width: 35, dataIndex: 'orderQuantity', type: 'float'},
		{header: 'Last Qty', width: 45, dataIndex: 'lastShares', type: 'float'},
		{header: 'Cumulative Qty', width: 50, dataIndex: 'cumulativeQuantity', type: 'float'},
		{header: 'Security', width: 40, dataIndex: 'securityId', hidden: true},
		{header: 'Security ID Source', width: 40, dataIndex: 'securityIDSource', hidden: true},
		{header: 'Security Type', width: 40, dataIndex: 'securityType', hidden: true},
		{header: 'Price', width: 45, dataIndex: 'price', type: 'float'},
		{header: 'Last Price', width: 45, dataIndex: 'lastPrice', type: 'float'},
		{header: 'Avg Price', width: 50, dataIndex: 'averagePrice', type: 'float'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow'
	}
});
Ext.reg('fix-executionReportGrid', Clifton.trade.order.fix.order.FixExecutionReportGrid);

Clifton.trade.order.fix.order.allocation.FixAllocationAcknowledgementGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeOrderFixAllocationAcknowledgementListByOrder',
	xtype: 'gridpanel',
	instructions: 'The following allocation acknowledgements from FIX provider have been received for selected FIX Order. FIX provider first confirms that they received allocations from us: RECEIVED. After that, they process allocation and either accept or reject it.',
	getLoadParams: function() {
		// default position date to previous business day
		const win = this.getWindow();
		return {
			'externalOrderIdentifier': win.defaultData.externalIdentifier,
			'fullyPopulated': false,
			requestedMaxDepth: 4,
			readUncommittedRequested: true
		};
	},
	isPagingEnabled: function() {
		return true;
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Received', width: 50, dataIndex: 'messageDateAndTime', type: 'date'},
		{header: 'Trade Date', width: 50, dataIndex: 'tradeDate', type: 'date'},
		{
			header: 'Status', width: 50, dataIndex: 'allocationStatus', filter: {
				type: 'list', options: [
					['ACCEPTED', 'ACCEPTED'],
					['BLOCK_LEVEL_REJECT', 'BLOCK_LEVEL_REJECT'],
					['ACCOUNT_LEVEL_REJECT', 'ACCOUNT_LEVEL_REJECT'],
					['RECEIVED', 'RECEIVED'],
					['INCOMPLETE', 'INCOMPLETE'],
					['REJECTED_BY_INTERMEDIARY', 'REJECTED_BY_INTERMEDIARY'],
					['ALLOCATION_PENDING', 'ALLOCATION_PENDING'],
					['REVERSED', 'REVERSED'],
					['CANCELED', 'CANCELED']]
			}
		},
		{header: 'Reject Code', width: 50, dataIndex: 'allocationRejectCode'},
		{header: 'Text', width: 100, dataIndex: 'text'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow'
	}
});
Ext.reg('fix-allocationAcknowledgementGrid', Clifton.trade.order.fix.order.allocation.FixAllocationAcknowledgementGrid);

Clifton.trade.order.fix.order.allocation.FixAllocationReportDetailGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeOrderFixAllocationReportDetailListByOrder',
	xtype: 'gridpanel',
	instructions: 'Final message about the order received from FIX provider if allocation was accepted. It includes final prices used (average pricing) and how these were allocated to specific client account(s).',
	getLoadParams: function() {
		// default position date to previous business day
		const win = this.getWindow();
		return {
			'externalOrderIdentifier': win.defaultData.externalIdentifier,
			'fullyPopulated': false,
			requestedMaxDepth: 4,
			readUncommittedRequested: true
		};
	},
	columns: [
		{header: 'Account', width: 50, dataIndex: 'allocationAccount'},
		{header: 'Process Code', width: 50, dataIndex: 'ProcessCode'},
		{header: 'Shares', width: 50, dataIndex: 'allocationShares', type: 'float'},
		{header: 'Average Price', width: 50, dataIndex: 'allocationAveragePrice', type: 'float'},
		{header: 'Allocation Price', width: 50, dataIndex: 'allocationPrice', type: 'float', useNull: true},
		{header: 'Net Money', width: 50, dataIndex: 'allocationNetMoney', type: 'currency'},
		{header: 'Gross Amount', width: 50, dataIndex: 'allocationGrossTradeAmount', type: 'currency', hidden: true},
		{header: 'Accrued Interest', width: 50, dataIndex: 'allocationAccruedInterestAmount', type: 'currency', hidden: true}
	]
});
Ext.reg('fix-allocationReportDetailGrid', Clifton.trade.order.fix.order.allocation.FixAllocationReportDetailGrid);


Clifton.trade.order.fix.order.FixOrderMessageGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeOrderFixMessageListForOrder',
	xtype: 'gridpanel',
	instructions: 'FIX Messages received and processed by our system. Also includes messages that failed processing. After the underlying problem is resolved, you can reprocess failed messages.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'In/Out', width: 10, dataIndex: 'incoming', renderer: TCG.renderIncomingOrOutgoing, tooltip: Clifton.fix.IncomingAndOutgoingHeaderToolTip, align: 'center', filter: {type: 'boolean', yesText: 'Incoming', noText: 'Outgoing'}},
		{header: 'FIX Session', width: 40, dataIndex: 'identifier.session.definition.name', filter: {searchField: 'sessionDefinitionName'}},
		{header: 'Message Type', width: 40, dataIndex: 'type.name'},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text'}
	],
	getLoadParams: function(firstLoad) {
		const win = this.getWindow();
		return {
			'externalOrderIdentifier': win.defaultData.externalIdentifier,
			requestedMaxDepth: 6,
			readUncommittedRequested: true
		};
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Reprocess selected',
				tooltip: 'Reprocess selected messages',
				iconCls: 'run',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					if (sm.getCount() !== 1) {
						TCG.showError('Please select one and only one message to run.', 'No Message(s) Selected');
					}
					else {
						const messages = sm.getSelections();
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Reprocessing Message',
							waitTarget: gridPanel,
							params: {
								messageId: messages[0].id
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('tradeOrderFixReprocessFixMessage.json');
					}
				}
			}, '-');
		}
	}
});
Ext.reg('fix-orderMessageGrid', Clifton.trade.order.fix.order.FixOrderMessageGrid);


Ext.override(Clifton.trade.blotter.ActiveOrdersPanel, {
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},
		initComponent: function() {
			// Attach "items" property manually instead of on prototype; ExtJS does not properly handle prototype-scoped "items" values during panel children
			this.items = [...this.activeOrdersItems];
			Clifton.trade.blotter.ActiveOrdersPanel.superclass.initComponent.call(this);
		},
		reload: function() {
			for (let i = 0; i < this.items.length; i++) {
				const item = this.items.get(i);
				if (item.reload) {
					item.reload();
				}
			}
		},
		activeOrdersItems: [
			{
				xtype: 'tradeOrder-activeTradeOrderGrid',
				border: 1,
				flex: 1,

				getTopToolbarFilters: function(toolbar) {
					const parentFilters = Clifton.trade.order.ActiveTradeOrderGrid.prototype.getTopToolbarFilters.call(this, toolbar);
					return Clifton.trade.blotter.mergeToCommonToolbarFilters(parentFilters);
				},
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						const trader = TCG.getCurrentUser();
						this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
					}
					return Clifton.trade.blotter.applyTopToolbarFilterLoadParams(this, this.getStandardLoadParams());
				}
			},
			{
				xtype: 'tradeOrder-nonFixActiveTradesGrid',

				getTopToolbarFilters: function(toolbar) {
					const parentFilters = Clifton.trade.order.NonFixActiveTradesGrid.prototype.getTopToolbarFilters.call(this, toolbar);
					return Clifton.trade.blotter.mergeToCommonToolbarFilters(parentFilters);
				},
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						const trader = TCG.getCurrentUser();
						this.setFilterValue('traderUser.label', {value: trader.id, text: trader.label});
					}
					return Clifton.trade.blotter.applyTopToolbarFilterLoadParams(this, this.getStandardLoadParams());
				}
			}
		]
	}
);


Clifton.trade.order.ActiveTradeOrderGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'tradeOrderListFind',
	xtype: 'gridpanel',
	rowSelectionModel: 'checkbox',
	title: 'Active FIX Orders',
	instructions: 'Active orders that are being executed via FIX.',
	wikiPage: 'IT/FIX Trouble Shooting',
	additionalPropertiesToRequest: 'id|status|externalIdentifier',
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Team', name: 'teamSecurityGroup.id', width: 150, minListWidth: 150, xtype: 'combo', url: 'securityGroupTeamList.json', loadAll: true}
		];
	},
	columns: [
		{header: 'Order ID', width: 13, dataIndex: 'id'},
		{
			header: 'Executing Broker',
			width: 30,
			dataIndex: 'destinationMapping.executingBrokerCompany.name',
			filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'},
			hidden: true
		},
		{header: 'Order Type', width: 24, dataIndex: 'type.name', filter: {type: 'list', searchFieldName: 'tradeOrderTypeNameList', options: Clifton.trade.order.TradeOrderTypeNames}},
		{header: 'Order Status', width: 24, dataIndex: 'status.orderStatus', filter: {type: 'list', options: Clifton.trade.order.TradeOrderStatuses, searchFieldName: 'orderStatusList', sortFieldName: 'orderStatus'}},
		{header: 'Allocation Status', width: 24, dataIndex: 'allocationStatus.allocationStatus', filter: {type: 'list', searchFieldName: 'allocationStatusList', sortFieldName: 'allocationStatus', options: Clifton.trade.order.TradeOrderAllocationStatuses}},
		{
			header: 'Buy/Sell', width: 10, dataIndex: 'buy', type: 'boolean',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Quantity', width: 15, dataIndex: 'quantity', type: 'float', useNull: true},
		{header: 'Qty Filled', width: 15, dataIndex: 'quantityFilled', type: 'float', useNull: true},
		{
			header: 'Security',
			width: 15,
			dataIndex: 'investmentSecurity.symbol',
			filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}
		},
		{header: 'Average Price', width: 20, dataIndex: 'averagePrice', type: 'float', useNull: true},
		{header: 'Limit Price', width: 15, dataIndex: 'limitPrice', type: 'float', useNull: true},
		{
			header: 'Trader',
			width: 15,
			dataIndex: 'traderUser.label',
			filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json?groupNameLike=Portfolio Manage'}
		},
		{header: 'Destination', width: 15, dataIndex: 'destinationMapping.tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}},
		{header: 'Traded On', width: 16, dataIndex: 'tradeDate', searchFieldName: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{
			header: 'Validation', width: 11, filter: false, sortable: false, hidden: true,
			renderer: function(v, metaData, d) {
				const failedValidation = (d.data.validationError === true || d.data.allocationValidationError === true);
				metaData.css = failedValidation ? 'sell-light' : 'buy-light';
				return failedValidation ? TCG.renderActionColumn('warning', '', 'Open Fix Message Validation Window', 'ShowFixValidationWindow')
					: TCG.renderIconWithTooltip('row_checked', 'Passed Validation');
			}
		},
		{header: 'Validation Failed', width: 11, hidden: true, dataIndex: 'validationError', type: 'boolean'},
		{header: 'Allocation Validation Failed', width: 11, hidden: true, dataIndex: 'allocationValidationError', type: 'boolean'}
	],
	getStandardLoadParams: function() {
		const t = this.getTopToolbar();
		const teamSecurityGroupValue = TCG.getChildByName(t, 'teamSecurityGroup.id').getValue();

		return {
			tradeWorkflowStatusName: 'Active',
			tradeWorkflowStateName: 'Execution',
			activeOrder: true,
			teamSecurityGroupId: teamSecurityGroupValue && teamSecurityGroupValue !== '' ? teamSecurityGroupValue : undefined,
			requestedMaxDepth: 6,
			readUncommittedRequested: true
		};
	},
	reallocateSelected: function(orderList, count) {
		const gridPanel = this;
		const grid = this.grid;
		const order = orderList[count];
		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Allocating...',
			params: {tradeOrderId: order.id},
			conf: {rowId: order.id},
			onLoad: function(record, conf) {
				count++;
				if (count === orderList.length) { // refresh after all repos were transitioned
					gridPanel.reload();
				}
				else {
					gridPanel.reallocateSelected(orderList, count);
				}
			}
		});
		loader.load('tradeOrderFixAllocationInstructionResend.json');
	},
	editor: {
		detailPageClass: 'Clifton.trade.order.TradeOrderWindow',
		drillDownOnly: true,
		getDefaultData: function(gridPanel, row) {
			return {
				externalIdentifier: row.json.externalIdentifier,
				tradeOrderStatus: row.json.status
			};
		},
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Actions',
				iconCls: 'run',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Allocate Partially Filled',
							tooltip: 'Manually allocate a partially filled and canceled trade.',
							iconCls: 'run',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								const orders = sm.getSelections();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
								}
								else if ((orders[0].json.status.orderStatus !== 'PARTIALLY_FILLED_CANCELED') && (orders[0].json.status.orderStatus !== 'PARTIALLY_FILLED_DONE_FOR_DAY')) {
									TCG.showError('Orders in this state can only be edited if the fill status is \'PARTIALLY_FILLED_CANCELED\'.', 'Incorrect Selection');
								}
								else {
									const className = 'Clifton.trade.order.TradeOrderPartialFillAllocationWindow';
									const id = orders[0].id;
									const params = id ? {id: id} : undefined;
									const cmpId = TCG.getComponentId(className, id);
									TCG.createComponent(className, {
										id: cmpId,
										params: params,
										openerCt: gridPanel,
										defaultIconCls: gridPanel.getWindow().iconCls
									});
								}
							}
						},

						{
							text: 'Reallocate Selected',
							tooltip: 'Manually allocate a partially filled and canceled trade.',
							iconCls: 'run',
							handler: function() {
								const win = this.getWindow();
								const selections = gridPanel.grid.getSelectionModel().getSelections();
								// sort repos by tradeDate and then averageUnitPrice: proper booking order
								const orderList = [];
								let addedOrders = '';
								for (let i = 0; i < selections.length; i++) {
									const order = selections[i].json;
									if (order.status.partialFill === false && order.status.fillComplete) {
										if (addedOrders.indexOf(order.id)) {
											addedOrders += order.id;
											orderList.push(order);
										}
									}
									else {

										const className = 'Clifton.trade.order.TradeOrderPartialFillAllocationWindow';
										const id = order.id;
										const params = id ? {id: id} : undefined;
										const cmpId = TCG.getComponentId(className, id);
										TCG.createComponent(className, {
											id: cmpId,
											params: params,
											openerCt: gridPanel,
											defaultIconCls: win.iconCls
										});

									}
								}
								if (orderList.length > 0) {
									gridPanel.reallocateSelected(orderList, 0);
								}
							}
						},

						{
							text: 'Process Selected Orders',
							tooltip: 'Force FIX order processing to move orders to the appropriate state(s).',
							iconCls: 'run',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select at least one order to be processed.', 'No Order(s) Selected');
								}
								else {
									const orders = sm.getSelections();
									const tradeOrderList = [];
									for (let i = 0; i < orders.length; i++) {
										tradeOrderList[i] = orders[i].id;
									}
									const loader = new TCG.data.JsonLoader({
										waitMsg: 'Processing Orders',
										waitTarget: gridPanel,
										params: {
											tradeOrderList: tradeOrderList
										},
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('tradeOrderListProcess.json');
								}
							}
						},

						{
							text: 'Resend Rejected Order',
							tooltip: 'Resends the original order message if the message was rejected.',
							iconCls: 'run',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single order to be resent.', 'Incorrect Selection');
								}
								else {
									const orders = sm.getSelections();
									const loader = new TCG.data.JsonLoader({
										waitMsg: 'Resending Order',
										waitTarget: gridPanel,
										params: {tradeOrderId: orders[0].id},
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('tradeOrderFixRejectedResend.json');
								}
							}
						},

						{
							text: 'Cancel Order and/or Trades',
							tooltip: 'Cancel the underlying trades if the order is manual or does not have any executions, otherwise send the FIX cancel request.',
							iconCls: 'run',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select at least 1 order to cancel.', 'Incorrect Selection');
								}
								else {
									const orders = sm.getSelections();
									const tradeOrderList = [];
									for (let i = 0; i < orders.length; i++) {
										tradeOrderList[i] = orders[i].id;
									}
									Ext.Msg.confirm('Cancel Order and Trades', 'You are about to cancel all trades for this order which will prevent any more FIX actions.<br/>Would you like to continue?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitMsg: 'Cancelling',
												waitTarget: gridPanel,
												params: {
													tradeOrderIdList: tradeOrderList
												},
												onLoad: function(record, conf) {
													gridPanel.reload();
												}
											});
											loader.load('tradeOrderFixListCancel.json');
										}

									}, gridPanel);
								}
							}
						},

						{
							text: 'Move to Manual Process',
							tooltip: 'Move trade to [Awaiting Fills] state so fills can be entered manually.',
							iconCls: 'run',
							handler: function() {
								const win = gridPanel.getWindow();
								const nonFixGridPanel = TCG.getChildByName(win, 'activeTradeOrderListFind');
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select at least 1 order to move to manual processing.', 'Incorrect Selection');
								}
								else {
									const orders = sm.getSelections();
									const tradeOrderList = [];
									for (let i = 0; i < orders.length; i++) {
										tradeOrderList[i] = orders[i].id;
									}
									Ext.Msg.confirm('Move to Manual Process', 'You are about to move all trades for this order to the [Awaiting Fills] which will prevent any more FIX actions.<br/>Would you like to continue?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitMsg: 'Move to Manual',
												waitTarget: gridPanel,
												params: {
													tradeOrderIdList: tradeOrderList
												},
												onLoad: function(record, conf) {
													gridPanel.reload();
													nonFixGridPanel.reload();
												}
											});
											loader.load('tradeOrderFixListMoveToManualFill.json');
										}

									}, gridPanel);
								}
							}
						},
						{
							text: 'Book with Average Price',
							tooltip: 'Create fills with the average price and book the trade.',
							iconCls: 'run',
							handler: function() {
								const win = gridPanel.getWindow();
								const nonFixGridPanel = TCG.getChildByName(win, 'activeTradeOrderListFind');
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select at least 1 order to move to manual processing.', 'Incorrect Selection');
								}
								else {
									const orders = sm.getSelections();
									const tradeOrderList = [];
									for (let i = 0; i < orders.length; i++) {
										tradeOrderList[i] = orders[i].id;
									}
									Ext.Msg.confirm('Book with Average Price', 'You are about to book each trade with the average price which will prevent any more FIX actions.<br/>Would you like to continue?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitMsg: 'Manual Allocation',
												waitTarget: gridPanel,
												params: {
													tradeOrderIdList: tradeOrderList
												},
												onLoad: function(record, conf) {
													gridPanel.reload();
													nonFixGridPanel.reload();
												}
											});
											loader.load('tradeOrderFixListWithAveragePriceBook.json');
										}

									}, gridPanel);
								}
							}
						},
						{
							text: 'Process Draft Orders',
							tooltip: 'Allows execution of orders in DRAFT state as limit or market orders.',
							iconCls: 'run',
							handler: function() {
								TCG.createComponent('Clifton.trade.order.TradeOrderLimitPriceWindow', {
									openerCt: gridPanel
								});
							}
						},
						'-',

						{
							text: 'Export',
							iconCls: 'excel',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Export Options Strangle Summary',
									tooltip: '',
									iconCls: 'excel',
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select at least 1 order to move to manual processing.', 'Incorrect Selection');
										}
										else {
											const orders = sm.getSelections();

											const tradeOrderList = [];
											for (let i = 0; i < orders.length; i++) {
												tradeOrderList[i] = 'tradeOrderIds=' + orders[i].id;
											}
											const url = 'tradeOrderExportDownload.json?downloadName=ExportOptionsStrangleSummary&' + tradeOrderList.join('&');
											TCG.downloadFile(url, null, gridPanel);
											gridPanel.grid.loadMask.onLoad();
										}
									}
								}, {
									text: 'Export Options Strangle Details',
									tooltip: '',
									iconCls: 'excel',
									handler: function() {
										const sm = gridPanel.grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select at least 1 order to move to manual processing.', 'Incorrect Selection');
										}
										else {
											const orders = sm.getSelections();

											const tradeOrderList = [];
											for (let i = 0; i < orders.length; i++) {
												tradeOrderList[i] = 'tradeOrderIds=' + orders[i].id;
											}
											const url = 'tradeOrderExportDownload.json?downloadName=ExportOptionsStrangleDetail&' + tradeOrderList.join('&');
											TCG.downloadFile(url, null, gridPanel);
											gridPanel.grid.loadMask.onLoad();
										}
									}
								}]
							})
						}]
				})
			}, '-');

			t.add({
				name: 'traderName',
				xtype: 'combo',
				displayField: 'label',
				url: 'securityUserListFind.json?disabled=false',
				emptyText: '< Select Trader >',
				loadAll: true,
				width: 100,
				listWidth: 250
			});
			t.add({
				text: 'Assign',
				xtype: 'button',
				tooltip: 'Assign selected trades to new trader',
				iconCls: 'run',
				handler: function() {
					const traderNameField = TCG.getChildByName(t, 'traderName');
					const traderId = traderNameField.getValue();
					const traderName = traderNameField.lastSelectionText;
					if (traderId === '') {
						TCG.showError('You must first select a trader.');
					}
					else {
						const sm = gridPanel.grid.getSelectionModel();

						const orders = sm.getSelections();
						const tradeOrderList = [];
						for (let i = 0; i < orders.length; i++) {
							tradeOrderList[i] = orders[i].id;
						}
						Ext.Msg.confirm('Assign Orders to the Selected Trader', 'You are about reassign all selected orders to ' + traderName + ' by sending a cancel/replace FIX message.<br/>Would you like to continue?', function(a) {
							if (a === 'yes') {
								const loader = new TCG.data.JsonLoader({
									waitMsg: 'Re-assigning',
									waitTarget: gridPanel,
									params: {
										tradeOrderIdList: tradeOrderList,
										securityUserId: traderId
									},
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load('tradeOrderAssignToNewTraderExecute.json');
							}

						}, gridPanel);
					}
				}
			});
			t.add('-');
		}
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const eventName = TCG.getActionColumnEventName(evt);
					if (eventName === 'ShowFixValidationWindow') {
						const row = grid.store.data.items[rowIndex];
						const tradeOrderId = row.json.id;
						const gridPanel = grid.ownerGridPanel;

						gridPanel.editor.openDetailPage('Clifton.trade.order.fix.message.validation.FixMessageValidationWindow', gridPanel, tradeOrderId, row);
					}
				}
			}
		}
	}
});
Ext.reg('tradeOrder-activeTradeOrderGrid', Clifton.trade.order.ActiveTradeOrderGrid);
Ext.reg('trade-activeFixOrderGrid', Clifton.trade.order.ActiveTradeOrderGrid); // keep for now to be backward compatible

