Clifton.trade.order.fix.message.validation.FixMessageValidationWindow = Ext.extend(TCG.app.Window, {
	title: 'FIX Message Validations',
	width: 800,
	height: 510,
	iconCls: 'shopping-cart',

	items: [{
		name: 'tradeOrderFixMessageValidationsForTradeOrderList',
		xtype: 'gridpanel',
		groupField: 'groupingLabel',
		groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Messages" : "Message"]}',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'fixMessageId', hidden: true},
			{header: 'Grouping Label', width: 20, dataIndex: 'groupingLabel', hidden: true},
			{header: 'Message Type', width: 50, dataIndex: 'messageTypeLabel'},
			{header: 'Identifier', width: 50, dataIndex: 'fixIdentifierId'},
			{header: 'Status', width: 50, dataIndex: 'validationStatus'},
			{header: 'Message', width: 250, dataIndex: 'message'}
		],
		getLoadParams: function() {
			const win = this.getWindow();
			return {
				'externalOrderIdentifier': win.defaultData.externalIdentifier
			};
		},
		editor: {
			drillDownOnly: true,
			detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
			getDetailPageId: function(gridPanel, row) {
				// use selected row parameters for drill down filtering
				return row.json.fixMessageId;
			}
		}
	}]
});
