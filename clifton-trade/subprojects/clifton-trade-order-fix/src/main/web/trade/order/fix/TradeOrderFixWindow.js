Clifton.trade.order.fix.TradeOrderFixWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeOrderFixWindow',
	titlePrefix: 'Trade Order',
	iconCls: 'shopping-cart-blue',
	width: 1200,
	closeWindowTrail: true,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		requiredFormIndex: 0,

		items: [
			{
				title: 'Order',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},

				tbar: [{
					text: 'Actions',
					iconCls: 'run',
					menu: {
						xtype: 'menu',
						items: [{
							text: 'Process Order',
							tooltip: 'Force FIX order processing to move orders to the appropriate state(s).',
							iconCls: 'run',
							handler: function() {
								const win = TCG.getParentByClass(this, Ext.Window);
								const orderId = win.params.id;
								const grid = TCG.getChildByName(win, 'tradeListFind');

								const tradeOrderList = [];
								tradeOrderList[0] = orderId;
								const loader = new TCG.data.JsonLoader({
									waitMsg: 'Processing Order',
									waitTarget: grid,
									params: {tradeOrderList: tradeOrderList},
									onLoad: function(record, conf) {
										grid.reload();
									}
								});
								loader.load('tradeOrderListProcess.json');
							}
						}, {
							text: 'Resend Rejected Order',
							iconCls: 'run',
							tooltip: 'Resends the original order message if the message was rejected.',
							handler: function() {
								const win = TCG.getParentByClass(this, Ext.Window);
								const orderId = win.params.id;
								const grid = TCG.getChildByName(win, 'tradeListFind');

								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									params: {tradeOrderId: orderId},
									onLoad: function(record, conf) {
										grid.reload();
									}
								});
								loader.load('tradeOrderFixRejectedResend.json');
							}
						}, {
							text: 'Cancel Order',
							iconCls: 'run',
							handler: function() {
								const win = TCG.getParentByClass(this, Ext.Window);
								const orderId = win.params.id;
								const grid = TCG.getChildByName(win, 'tradeListFind');

								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									params: {tradeOrderId: orderId},
									onLoad: function(record, conf) {
										grid.reload();
									}
								});
								loader.load('tradeOrderFixCancelRequestSend.json');
							}
						}, '-', {
							text: 'Resend Order (Admin Only)',
							iconCls: 'run',
							tooltip: 'Resends the original order message.  If the original order was received, the message will be rejected and original order will stay active.  Only use when a message failed and was not received be Goldman.',
							handler: function() {
								const win = TCG.getParentByClass(this, Ext.Window);
								const orderId = win.params.id;
								const grid = TCG.getChildByName(win, 'tradeListFind');

								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									params: {tradeOrderId: orderId},
									onLoad: function(record, conf) {
										grid.reload();
									}
								});
								loader.load('tradeOrderFixResend.json');
							}
						}, {
							text: 'Send OrderCancelReplace (Admin Only)',
							iconCls: 'run',
							handler: function() {
								const win = TCG.getParentByClass(this, Ext.Window);
								const orderId = win.params.id;
								const grid = TCG.getChildByName(win, 'tradeListFind');

								const loader = new TCG.data.JsonLoader({
									waitTarget: grid,
									params: {tradeOrderId: orderId},
									onLoad: function(record, conf) {
										grid.reload();
									}
								});
								loader.load('tradeOrderCancelReplaceExecute.json');
							}
						}]
					}
				}],

				items: [
					{
						height: 140,
						xtype: 'formpanel',
						loadDefaultDataAfterRender: true,
						labelFieldName: 'id',

						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .4,
									layout: 'form',
									items: [
										{
											fieldLabel: 'Buy/Sell', name: 'buy', xtype: 'displayfield', width: 125,
											setRawValue: function(v) {
												this.el.addClass(v ? 'buy' : 'sell');
												TCG.form.DisplayField.superclass.setRawValue.call(this, v ? 'BUY' : 'SELL');
											}
										},
										{fieldLabel: 'Quantity', name: 'quantity', xtype: 'floatfield', width: 129, readOnly: true, allowBlank: false},
										{fieldLabel: 'Security', name: 'investmentSecurity.label', xtype: 'linkfield', detailIdField: 'investmentSecurity.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'Executing Broker', name: 'destinationMapping.executingBrokerCompany.name', xtype: 'linkfield', detailIdField: 'destinationMapping.executingBrokerCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
										{fieldLabel: 'Trade Destination', name: 'destinationMapping.tradeDestination.name', xtype: 'linkfield', detailPageClass: 'Clifton.trade.destination.TradeDestinationWindow', detailIdField: 'destinationMapping.tradeDestination.id'}
									]
								},
								{
									columnWidth: .3,
									layout: 'form',
									items: [
										{fieldLabel: 'Average Price', name: 'averagePrice', xtype: 'floatfield', readOnly: true},
										{fieldLabel: 'Quantity Filled', name: 'quantityFilled', xtype: 'floatfield', readOnly: true, allowBlank: false}
									]
								},
								{
									columnWidth: .3,
									layout: 'form',
									labelWidth: 125,
									items: [
										{fieldLabel: 'Order Type', name: 'type.name', xtype: 'displayfield'},
										{fieldLabel: 'Order Status', name: 'status.name', xtype: 'linkfield', detailIdField: 'status.id', detailPageClass: 'Clifton.trade.order.TradeOrderStatusWindow'},
										{fieldLabel: 'Allocation Status', name: 'allocationStatus.name', xtype: 'linkfield', detailIdField: 'allocationStatus.id', detailPageClass: 'Clifton.trade.order.TradeOrderAllocationStatusWindow'},
										{fieldLabel: 'Trader', name: 'traderUser.label', xtype: 'linkfield', detailIdField: 'traderUser.id', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Trade Date', name: 'tradeDate', xtype: 'datefield', readOnly: true}
									]
								}]
						}]
					},

					{
						title: 'Trades',
						xtype: 'gridpanel',
						name: 'tradeListFind',
						flex: 1,
						border: 1,
						additionalPropertiesToRequest: 'tradeDestination.connection',
						instructions: 'More than one trade for the same security in the same direction can be grouped for execution into a single FIX Order. The following trades are associated with selected FIX Order.',
						wikiPage: 'IT/FIX+Trouble+Shooting',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Workflow State', width: 14, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Client Account', width: 45, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, defaultSortColumn: true},
							{header: 'Holding Account', width: 45, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
							{
								header: 'Buy/Sell', width: 5, dataIndex: 'buy', type: 'boolean', hidden: true,
								renderer: function(v, metaData) {
									metaData.css = v ? 'amountPositive' : 'amountNegative';
									return v ? 'BUY' : 'SELL';
								}
							},
							{header: 'Quantity', width: 10, dataIndex: 'quantityIntended', type: 'float', useNull: true},
							{header: 'Original Face', width: 10, dataIndex: 'originalFace', type: 'float', useNull: true},
							{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
							{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
							{header: 'Avg Price', width: 15, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
							{header: 'Expected Price', width: 15, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
							{header: 'Commission Per Unit', width: 15, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
							{header: 'Fee', width: 15, dataIndex: 'feeAmount', type: 'float', hidden: true},
							{header: 'Accounting Notional', width: 30, dataIndex: 'accountingNotional', type: 'currency'},
							{header: 'Novation', width: 10, dataIndex: 'novation', type: 'boolean', hidden: true},
							{header: 'Assigned Counterparty', width: 25, dataIndex: 'assignmentCompany.name', hidden: true, useNull: true, filter: {type: 'combo', searchFieldName: 'assignmentCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
							{header: 'Trade Note', width: 20, dataIndex: 'description', renderer: TCG.renderTextNowrapWithTooltip},
							{header: 'Block', width: 10, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
							{header: 'Trader', width: 12, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
							{header: 'Traded On', width: 10, dataIndex: 'tradeDate', searchFieldName: 'tradeDate'},
							{header: 'Settled On', width: 10, dataIndex: 'settlementDate', searchFieldName: 'settlementDate'},
							{width: 0} // need to fix last column UI glitch
						],
						getLoadParams: function() {
							const win = this.getWindow();
							return {
								orderIdentifier: win.params.id,
								sessionParameters: win.params.sessionParameters,
								requestedMaxDepth: 6
							};
						},
						editor: {
							detailPageClass: 'Clifton.trade.TradeWindow',
							drillDownOnly: true
						}
					}
				]
			},


			{
				title: 'Execution Reports',
				items: [{
					xtype: 'fix-executionReportGrid'
				}]
			},


			{
				title: 'Allocation Acknowledgements',
				items: [{
					xtype: 'fix-allocationAcknowledgementGrid',
					wikiPage: 'IT/FIX+Trouble+Shooting',
					editor: {
						drillDownOnly: true,
						addEditButtons: function(t, grid) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							const win = this.getWindow();
							const orderId = win.params.id;

							t.add({
								text: 'Reallocate',
								xtype: 'button',
								iconCls: 'shopping-cart',
								scope: grid,
								handler: function() {
									const order = win.defaultData;
									if ((TCG.isFalse(order.status.partialFill) || order.tradeList.length === 1) && order.status.fillComplete) {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid,
											params: {tradeOrderId: orderId},
											onLoad: function(record, conf) {
												grid.reload();
											}
										});
										loader.load('tradeOrderFixAllocationInstructionResend.json');
									}
									else {

										const className = 'Clifton.trade.order.TradeOrderPartialFillAllocationWindow';
										const id = orderId;
										const params = id ? {id: id} : undefined;
										const cmpId = TCG.getComponentId(className, id);
										TCG.createComponent(className, {
											id: cmpId,
											params: params,
											openerCt: grid,
											defaultIconCls: win.iconCls
										});

									}
								}
							});
							t.add('-');
							t.add({
								text: 'Cancel Allocation',
								xtype: 'button',
								iconCls: 'shopping-cart',
								scope: grid,
								handler: function() {
									const loader = new TCG.data.JsonLoader({
										waitTarget: grid,
										params: {tradeOrderId: orderId},
										onLoad: function(record, conf) {
											grid.reload();
										}
									});
									loader.load('tradeOrderFixAllocationInstructionCancel.json');
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Allocation Report Details',
				items: [{
					xtype: 'fix-allocationReportDetailGrid'
				}]
			},


			{
				title: 'FIX Messages',
				items: [{
					xtype: 'fix-orderMessageGrid'
				}]
			},


			{
				title: 'Replaced Orders',
				items: [{
					name: 'tradeOrderListFind',
					xtype: 'tradeOrder-activeTradeOrderGrid',
					rowSelectionModel: 'checkbox',
					instructions: 'A list of all orders in the system. An order is a grouping of one or more trades for same security in same direction used for electronic or manual execution.',
					isPagingEnabled: function() {
						return false;
					},
					getLoadParams: function(firstLoad) {
						const win = this.getWindow();
						return {
							orderGroup: win.defaultData.orderGroup,
							activeOrder: false,
							requestedMaxDepth: 6,
							readUncommittedRequested: true
						};
					}
				}]
			}
		]
	}]
});
