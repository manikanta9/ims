Clifton.trade.order.fix.TradeOrderFixListWindow = Ext.extend(TCG.app.Window, {
	id: 'tradeOrderFixListWindow',
	title: 'Orders',
	iconCls: 'shopping-cart-blue',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,

		items: [
			{
				title: 'Orders',
				items: [{
					name: 'tradeOrderListFind',
					xtype: 'tradeOrder-activeTradeOrderGrid',
					title: void 0,
					rowSelectionModel: 'checkbox',
					instructions: 'A list of all orders in the system. An order is a grouping of one or more trades for same security in same direction used for electronic or manual execution.',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -30)});
						}

						const t = this.getTopToolbar();
						const teamSecurityGroupValue = TCG.getChildByName(t, 'teamSecurityGroup.id').getValue();

						return {
							teamSecurityGroupId: teamSecurityGroupValue && TCG.isNotBlank(teamSecurityGroupValue) ? teamSecurityGroupValue : undefined,
							requestedMaxDepth: 6,
							readUncommittedRequested: true,
							activeOrder: true
						};
					},
					editor: {
						detailPageClass: 'Clifton.trade.order.TradeOrderWindow',
						drillDownOnly: true,
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								externalIdentifier: row.json.externalIdentifier,
								tradeOrderStatus: row.json.status
							};
						},
						addEditButtons: function(t) {
							const gridPanel = this.getGridPanel();
							t.add({
								text: 'Actions',
								iconCls: 'run',
								menu: new Ext.menu.Menu({
									items: [
										{
											text: 'Allocate',
											tooltip: 'Manually allocate a partially filled and canceled trade.',
											iconCls: 'run',
											scope: gridPanel,
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												const orders = sm.getSelections();
												if (sm.getCount() !== 1) {
													TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
												}
												else if ((orders[0].json.status.orderStatus !== 'PARTIALLY_FILLED_CANCELED') && (orders[0].json.status.orderStatus !== 'PARTIALLY_FILLED_DONE_FOR_DAY')) {
													TCG.showError('Orders in this state can only be edited if the fill status is \'PARTIALLY_FILLED_CANCELED\'.', 'Incorrect Selection');
												}
												else {
													const className = 'Clifton.trade.order.TradeOrderWindow';
													const id = orders[0].id;
													const params = id ? {id: id} : undefined;
													const cmpId = TCG.getComponentId(className, id);
													TCG.createComponent(className, {
														id: cmpId,
														params: params,
														openerCt: gridPanel,
														defaultIconCls: gridPanel.getWindow().iconCls
													});
												}
											}
										},

										{
											text: 'Process Selected Orders',
											tooltip: 'Force FIX order processing to move orders to the appropriate state(s).',
											iconCls: 'run',
											scope: gridPanel,
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												if (sm.getCount() === 0) {
													TCG.showError('Please select at least one order to be processed.', 'No Order(s) Selected');
												}
												else {
													const orders = sm.getSelections();
													const tradeOrderList = [];
													for (let i = 0; i < orders.length; i++) {
														tradeOrderList[i] = orders[i].id;
													}
													const loader = new TCG.data.JsonLoader({
														waitMsg: 'Processing Orders',
														waitTarget: this,
														params: {
															tradeOrderList: tradeOrderList
														},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('tradeOrderListProcess.json');
												}
											}
										},

										{
											text: 'Resend Order',
											iconCls: 'run',
											scope: gridPanel,
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												if (sm.getCount() !== 1) {
													TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
												}
												else {
													const orders = sm.getSelections();
													const loader = new TCG.data.JsonLoader({
														waitMsg: 'Resending Order',
														waitTarget: this,
														params: {tradeOrderId: orders[0].id},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('tradeOrderFixResend.json');
												}
											}
										},

										{
											text: 'Cancel Selected Order',
											tooltip: 'Send FIX message to cancel the active order.',
											iconCls: 'run',
											scope: gridPanel,
											handler: function() {
												const gridPanel = this;
												const sm = this.grid.getSelectionModel();
												if (sm.getCount() !== 1) {
													TCG.showError('Please select a single order to allocate.', 'Incorrect Selection');
												}
												else {
													const orders = sm.getSelections();
													const loader = new TCG.data.JsonLoader({
														waitMsg: 'Canceling Order',
														waitTarget: this,
														params: {tradeOrderId: orders[0].id},
														onLoad: function(record, conf) {
															gridPanel.reload();
														}
													});
													loader.load('tradeOrderFixCancelRequestSend.json');
												}
											}
										}, '-',

										{
											text: 'Export',
											iconCls: 'excel',
											menu: new Ext.menu.Menu({
												items: [{
													text: 'Export Options Strangle Summary',
													tooltip: '',
													iconCls: 'excel',
													scope: gridPanel,
													handler: function() {
														const sm = this.grid.getSelectionModel();
														if (sm.getCount() === 0) {
															TCG.showError('Please select at least 1 order to move to manual processing.', 'Incorrect Selection');
														}
														else {
															const orders = sm.getSelections();

															const tradeOrderList = [];
															for (let i = 0; i < orders.length; i++) {
																tradeOrderList[i] = 'tradeOrderIds=' + orders[i].id;
															}
															const url = 'tradeOrderExportDownload.json?downloadName=ExportOptionsStrangleSummary&' + tradeOrderList.join('&');
															TCG.downloadFile(url, null, gridPanel);
															this.grid.loadMask.onLoad();
														}
													}
												}, {
													text: 'Export Options Strangle Details',
													tooltip: '',
													iconCls: 'excel',
													scope: gridPanel,
													handler: function() {
														const sm = this.grid.getSelectionModel();
														if (sm.getCount() === 0) {
															TCG.showError('Please select at least 1 order to move to manual processing.', 'Incorrect Selection');
														}
														else {
															const orders = sm.getSelections();

															const tradeOrderList = [];
															for (let i = 0; i < orders.length; i++) {
																tradeOrderList[i] = 'tradeOrderIds=' + orders[i].id;
															}
															const url = 'tradeOrderExportDownload.json?downloadName=ExportOptionsStrangleDetail&' + tradeOrderList.join('&');
															TCG.downloadFile(url, null, gridPanel);
															this.grid.loadMask.onLoad();
														}
													}
												}]
											})
										}
									]
								})
							}, '-');
						}
					}
				}]
			},

			{
				title: 'Order Status',
				items: [
					{
						xtype: 'gridpanel',
						name: 'tradeOrderStatusListFind',
						instructions: 'List of available order statuses for orders',
						topToolbarSearchParameter: 'searchPattern',
						columns: [
							{header: 'ID', dataIndex: 'id', width: 25, hidden: true, type: 'int', doNotFormat: true},
							{header: 'Order Status Name', dataIndex: 'name', width: 75},
							{header: 'Description', dataIndex: 'description', width: 175},
							{header: 'Order Status', dataIndex: 'orderStatus', width: 75, filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.trade.order.TradeOrderStatuses}}},
							{header: 'Cancel If Not Filled', dataIndex: 'cancelIfNotFilled', width: 50, type: 'boolean', tooltip: 'If no fills are received and the order is canceled, then cancel the underlying trades.'},
							{header: 'Partial Fill', dataIndex: 'partialFill', width: 50, type: 'boolean', tooltip: 'The order has been partially filled.'},
							{header: 'Fill Complete', dataIndex: 'fillComplete', width: 50, type: 'boolean', tooltip: 'The order fill is complete. (Filled or Partially Filled Canceled)'}
						],
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.trade.order.TradeOrderStatusWindow'
						}
					}
				]

			},

			{
				title: 'Order Allocation Status',
				items: [
					{
						xtype: 'gridpanel',
						name: 'tradeOrderAllocationStatusListFind',
						instructions: 'List of available order allocation statuses for orders.',
						topToolbarSearchParameter: 'searchPattern',
						columns: [
							{header: 'ID', dataIndex: 'id', width: 25, hidden: true, type: 'int', doNotFormat: true},
							{header: 'Allocation Status Name', dataIndex: 'name', width: 75},
							{header: 'Description', dataIndex: 'description', width: 175},
							{header: 'Allocation Status', dataIndex: 'allocationStatus', width: 75, filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.trade.order.TradeOrderAllocationStatuses}}},
							{header: 'Allocation Fill Received', dataIndex: 'allocationFillReceived', width: 50, type: 'boolean', tooltip: 'The allocation report with the actual fills has been received.'},
							{header: 'Allocation Complete', dataIndex: 'allocationComplete', width: 50, type: 'boolean', tooltip: 'The allocation data has been received.'},
							{header: 'Allocation Rejected', dataIndex: 'rejected', width: 50, type: 'boolean', tooltip: 'The allocation has been rejected'}
						],
						editor: {
							drillDownOnly: true,
							detailPageClass: 'Clifton.trade.order.TradeOrderAllocationStatusWindow'
						}
					}
				]

			},

			{
				title: 'FIX Messages',
				items: [{
					xtype: 'fix-messageGrid'
				}]
			},

			{
				title: 'Administrative Entries',
				items: [{
					xtype: 'fix-messageEntryGrid',
					topToolbarSearchParameter: 'searchPattern'
				}]
			},

			{
				title: 'Heartbeats',
				items: [{
					xtype: 'fix-messageEntryHeartbeatGrid'
				}]
			},

			{
				title: 'Non-Persistent Messages',
				items: [{
					xtype: 'fix-messageEntryNonPersistentGrid'
				}]
			},

			{
				title: 'Error Queue',
				items: [{
					xtype: 'fix-messageErrorGrid'
				}]
			},

			{
				title: 'Unknown Message Entries',
				items: [{
					xtype: 'fix-messageEntryGrid',
					instructions: 'Each FIX message is logged by our system as a Fix Message Entry. Displays all unknown message entries: FIX Sessions is not set or the entry has not been processed.',
					getLoadParams: () => ({
						fixMessageEntryTypeIdsName: 'MESSAGE_AND_NOT_ADMINISTRATIVE',
						sessionIsNullOrNotProcessed: true,
						readUncommittedRequested: true
					})
				}]
			},

			{
				title: 'Failed Message Entries',
				items: [{
					xtype: 'fix-messageEntryFailedMessageGrid'
				}]
			}
		]
	}]
});
