dependencies {

	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Spring dependencies
	api("org.springframework.boot:spring-boot")
	api("org.springframework.boot:spring-boot-autoconfigure")
	//SnakeYAML used for YAML properties files
	api("org.yaml:snakeyaml")
	api("org.apache.tomcat.embed:tomcat-embed-core")
	api("org.apache.tomcat.embed:tomcat-embed-websocket")
	runtimeOnly("org.springframework:spring-instrument")
	// Enable annotation processing for @ConfigurationProperties types
	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

	// Wrapper types for executing the application as a Windows service
	api("winrun4j:winrun4j:0.4.5")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))
	compileOnly(project(":clifton-websocket"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	// Add Spring Boot testing functionality
	testFixturesApi("org.springframework.boot:spring-boot-test-autoconfigure")
	testFixturesApi("org.springframework.boot:spring-boot-test")

	testFixturesApi(testFixtures(project(":clifton-core")))
}
