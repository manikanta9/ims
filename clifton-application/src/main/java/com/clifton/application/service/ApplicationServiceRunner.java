package com.clifton.application.service;

import org.boris.winrun4j.AbstractService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * The {@link AbstractService} WinRun4J wrapper type for executing {@link SpringApplication Spring Boot applications}.
 *
 * @author MikeH
 * @see <a href="http://winrun4j.sourceforge.net/">WinRun4J website</a>
 */
public abstract class ApplicationServiceRunner extends AbstractService {

	@Override
	public int serviceMain(String[] args) {
		Class<?> applicationClass = getClass();
		ConfigurableApplicationContext context = new SpringApplicationBuilder(applicationClass)
				.main(applicationClass) // Manually assign main class since insufficient information is available for SpringApplication#deduceMainApplicationClass inference
				.run(args);
		while (!isShutdown()) {
			try {
				//noinspection BusyWait : This is the documented and supported WinRun4J method of waiting for service shutdown
				Thread.sleep(6000);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				this.shutdown = true;
			}
		}
		context.close();
		return 0;
	}
}
