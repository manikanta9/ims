package com.clifton.application.context.filter;

import org.springframework.core.io.Resource;
import org.tuckey.web.filters.urlrewrite.Conf;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import java.io.IOException;


/**
 * The <code>ConfigurableUrlRewriteFilter</code> provides a configurable implementation of {@link UrlRewriteFilter}.
 * <p>
 * This type can be used to use runtime-customized properties or logic for the {@link UrlRewriteFilter}.
 *
 * @author MikeH
 * @see UrlRewriteFilter
 */
public class ConfigurableUrlRewriteFilter extends UrlRewriteFilter {

	/**
	 * The configuration resource. This replaces {@link UrlRewriteFilter#confPath}, which is limited in capacity and only able to load resources as filesystem paths or relative
	 * context paths.
	 */
	private Resource configResource;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void loadUrlRewriter(FilterConfig filterConfig) throws ServletException {
		try {
			Conf conf = new Conf(filterConfig.getServletContext(), getConfigResource().getInputStream(), getConfigResource().getFilename(), getConfigResource().getURI().toString());
			checkConf(conf);
		}
		catch (IOException ex) {
			throw new ServletException("Unable to load URL rewrite configuration file from resource [" + getConfigResource() + "].", ex);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Resource getConfigResource() {
		return this.configResource;
	}


	public void setConfigResource(Resource configResource) {
		this.configResource = configResource;
	}
}
