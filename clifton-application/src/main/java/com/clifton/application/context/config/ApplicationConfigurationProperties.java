package com.clifton.application.context.config;

import org.apache.catalina.valves.RemoteIpValve;
import org.springframework.boot.context.properties.ConfigurationProperties;


/**
 * The {@link ApplicationConfigurationProperties} type provides custom properties for applications.
 * <p>
 * Metadata is automatically generated for these properties during compilation via the Spring Boot configuration annotation processor. This provides IDE tooling, such as auto-
 * completion, property validation, and documentation links.
 *
 * @author MikeH
 */
@ConfigurationProperties(prefix = "clifton")
public class ApplicationConfigurationProperties {

	/**
	 * The secondary HTTP port to use (plain-text only).
	 */
	private Integer secondaryHttpPort;


	/**
	 * The trusted proxies pattern to use for {@link RemoteIpValve} valves. This will be used to populate the {@link RemoteIpValve#trustedProxies} field.
	 */
	private String trustedProxies;

	/**
	 * If set to true or left out, the application will use the <code>securityUserManagementFilterRegistration</code> bean defined in `ApplicationWebServerConfiguration`.
	 * If set to false (as in Portal Admin, because it defines its own security filter), the application will not create this bean.
	 */
	private boolean useStandardSecurityFilter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSecondaryHttpPort() {
		return this.secondaryHttpPort;
	}


	public void setSecondaryHttpPort(Integer secondaryHttpPort) {
		this.secondaryHttpPort = secondaryHttpPort;
	}


	public String getTrustedProxies() {
		return this.trustedProxies;
	}


	public void setTrustedProxies(String trustedProxies) {
		this.trustedProxies = trustedProxies;
	}


	public boolean isUseStandardSecurityFilter() {
		return this.useStandardSecurityFilter;
	}


	public void setUseStandardSecurityFilter(boolean useStandardSecurityFilter) {
		this.useStandardSecurityFilter = useStandardSecurityFilter;
	}
}
