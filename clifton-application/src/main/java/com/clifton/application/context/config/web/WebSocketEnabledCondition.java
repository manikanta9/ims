package com.clifton.application.context.config.web;


import com.clifton.websocket.config.WebSocketConfiguration;
import org.springframework.boot.autoconfigure.condition.AllNestedConditions;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.websocket.servlet.WebSocketServletAutoConfiguration;


public class WebSocketEnabledCondition extends AllNestedConditions {

	public WebSocketEnabledCondition() {
		super(ConfigurationPhase.REGISTER_BEAN);
	}


	@Override
	public ConfigurationPhase getConfigurationPhase() {
		return null;
	}


	@ConditionalOnClass({WebSocketConfiguration.class, WebSocketServletAutoConfiguration.class})
	@ConditionalOnProperty(value = "application.websocket.enabled", havingValue = "true")
	static class OnWebSocketConfigurationClasses {}
}
