package com.clifton.application.context.config.web;

import com.clifton.core.context.spring.SpringApplicationContextUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.web.stats.SystemRequestStatsStatusExcludeRule;
import com.clifton.core.web.stats.SystemRequestStatsUriRule;
import com.clifton.core.web.stats.SystemRequestStatsUriRule.RuleEffect;
import com.clifton.core.web.stats.SystemRequestStatsUriRule.RulePatternType;
import com.clifton.websocket.config.WebSocketStatsUriRuleBeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.DispatcherServlet;

import java.util.Collections;
import java.util.List;


/**
 * The {@link ApplicationWebMvcConfiguration} provides web-MVC-related context configurations.
 *
 * @author MikeH
 */
@Configuration
public class ApplicationWebMvcConfiguration {

	@Configuration
	public static class RequestStatsConfiguration {

		/**
		 * The bean name prefix used when generating {@link SystemRequestStatsUriRule} beans.
		 */
		public static final String STATIC_ASSET_STATS_RULE_BEAN_NAME_PREFIX = "staticAssetStatsUriPrefixRule-";

		/**
		 * The list of URI suffixes which shall be excluded from system statistics via {@link SystemRequestStatsUriRule} rules.
		 */
		private static final List<String> STATIC_ASSET_STATS_RULE_URI_SUFFIX_LIST = CollectionUtils.createList(
				".css",
				".gif",
				".ico",
				".jpeg",
				".jpg",
				".js",
				".map",
				".png",
				".html"
		);


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * The {@link BeanFactoryPostProcessor} which generates {@link SystemRequestStatsUriRule} rules to exclude all static assets from system statistics. This is required to
		 * reduce request statistic bloat in environments where static assets requests are handled through the {@link DispatcherServlet} and its standard filter chain.
		 * <p>
		 * @formatter:off
		 * TODO: Remove this if and when we are able to move to default servlet handling for static assets
		 *  (https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html#mvc-default-servlet-handler).
		 * @formatter:on
		 */
		@Bean
		public static BeanFactoryPostProcessor systemRequestStatsUriPrefixRuleStaticAssetBeanFactoryPostProcessor() {
			return beanFactory -> {
				BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) beanFactory;
				int index = 0;
				// Hide context root requests
				String rootContextBeanName = STATIC_ASSET_STATS_RULE_BEAN_NAME_PREFIX + index++;
				SpringApplicationContextUtils.registerBeanDefinition(beanDefinitionRegistry, rootContextBeanName, SystemRequestStatsUriRule.class, "/", RulePatternType.EXACT.name(), RuleEffect.HIDE.name());
				// Hide static asset requests
				for (String uriSuffix : STATIC_ASSET_STATS_RULE_URI_SUFFIX_LIST) {
					String beanName = STATIC_ASSET_STATS_RULE_BEAN_NAME_PREFIX + index++;
					SpringApplicationContextUtils.registerBeanDefinition(beanDefinitionRegistry, beanName, SystemRequestStatsUriRule.class, uriSuffix, RulePatternType.SUFFIX.name(), RuleEffect.HIDE.name());
				}
				SpringApplicationContextUtils.registerBeanDefinition(beanDefinitionRegistry, "webjarsStatsUriPrefixRule", SystemRequestStatsUriRule.class, "/webjars/", RulePatternType.PREFIX.name(), RuleEffect.HIDE.name());
				SpringApplicationContextUtils.registerBeanDefinition(beanDefinitionRegistry, "excludeNotFoundStatsStatusExcludeRule", SystemRequestStatsStatusExcludeRule.class, Collections.singletonList(HttpStatus.NOT_FOUND.value()));
			};
		}


		@Bean
		@Conditional(WebSocketEnabledCondition.class)
		public static BeanFactoryPostProcessor webSocketStatsUriPrefixBeanFactoryPostProcessor() {
			return new WebSocketStatsUriRuleBeanFactoryPostProcessor();
		}
	}
}
