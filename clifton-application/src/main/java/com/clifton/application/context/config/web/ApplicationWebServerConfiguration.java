package com.clifton.application.context.config.web;

import com.clifton.application.context.filter.ConfigurableUrlRewriteFilter;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.websocket.config.WebSocketConfiguration;
import org.apache.catalina.Valve;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.util.SessionConfig;
import org.apache.catalina.valves.RemoteIpValve;
import org.apache.logging.log4j.web.Log4jServletContextListener;
import org.apache.tomcat.util.http.Rfc6265CookieProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.condition.ConditionalOnResource;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.autoconfigure.web.ServerProperties.ForwardHeadersStrategy;
import org.springframework.boot.autoconfigure.web.ServerProperties.Tomcat.Remoteip;
import org.springframework.boot.autoconfigure.web.embedded.TomcatWebServerFactoryCustomizer;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.autoconfigure.websocket.servlet.WebSocketServletAutoConfiguration;
import org.springframework.boot.web.embedded.tomcat.ConfigurableTomcatWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletContextInitializerBeans;
import org.springframework.boot.web.servlet.context.ServletWebServerApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.ForwardedHeaderFilter;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * The {@link ApplicationWebServerConfiguration} configures the application servlet container, including servlets, filters, and connectors.
 *
 * @author MikeH
 */
@Configuration
public class ApplicationWebServerConfiguration {

	private static final int PORT_MIN = 0;
	private static final int PORT_MAX = 65535;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets up the secondary HTTP connector. This provides a plain-text secondary access point for the application, particularly for trusted resources behind a firewall.
	 */
	@Bean
	@ConditionalOnExpression("'${clifton.secondary-http-port:}' ne ''")
	public WebServerFactoryCustomizer<TomcatServletWebServerFactory> tomcatSecondaryPortWebServerFactoryCustomizer(@Value("${clifton.secondary-http-port}") int secondaryHttpPort) {
		AssertUtils.assertTrue(secondaryHttpPort >= PORT_MIN && secondaryHttpPort <= PORT_MAX, "Invalid secondary port: [%d]. The secondary HTTP port must be a valid number between [%d] and [%d].", secondaryHttpPort, PORT_MIN, PORT_MAX);
		return (TomcatServletWebServerFactory factory) -> {
			Connector secondaryConnector = new Connector(TomcatServletWebServerFactory.DEFAULT_PROTOCOL);
			secondaryConnector.setScheme("http");
			secondaryConnector.setPort(secondaryHttpPort);
			factory.addAdditionalTomcatConnectors(secondaryConnector);
		};
	}


	/**
	 * Sets the {@link RemoteIpValve#trustedProxies} field on the existing registered {@link RemoteIpValve} valve, if such a valve has been registered. This adds support for
	 * trusted proxies, where Spring Boot typically provides support for internal proxies only (via {@link Remoteip#internalProxies}). This must run after {@link RemoteIpValve}
	 * registration in {@link TomcatWebServerFactoryCustomizer#customizeRemoteIpValve(ConfigurableTomcatWebServerFactory)}.
	 */
	@Bean
	@ConditionalOnExpression("'${clifton.trusted-proxies:}' ne ''")
	public WebServerFactoryCustomizer<TomcatServletWebServerFactory> tomcatValveWebServerFactoryCustomizer(@Value("${clifton.trusted-proxies}") String trustedProxies) {
		return (TomcatServletWebServerFactory factory) -> {
			List<Valve> remoteIpValveList = CollectionUtils.getFiltered(factory.getEngineValves(), RemoteIpValve.class::isInstance);
			if (remoteIpValveList.isEmpty()) {
				LogUtils.warn(getClass(), String.format("No [%s] valve instance was found. Unable to set trusted proxies.", RemoteIpValve.class.getName()));
			}
			else if (remoteIpValveList.size() > 1) {
				throw new RuntimeException(String.format("Multiple [%s] valve instances were found ([%d]). No more than one instance of this valve may be active.", RemoteIpValve.class.getName(), remoteIpValveList.size()));
			}
			else {
				RemoteIpValve remoteIpValve = (RemoteIpValve) CollectionUtils.getOnlyElementStrict(remoteIpValveList);
				remoteIpValve.setTrustedProxies(trustedProxies);
				LogUtils.info(getClass(), String.format("Set RemoteIpValve trusted proxies pattern to [%s].", trustedProxies));
			}
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Applies {@code X-Forwarded-Prefix} context paths to session token cookie paths.
	 * <p>
	 * This works around a limitation in Tomcat in which the session cookie path cannot be defined on a per-request basis. Per-request cookie paths are needed when running behind
	 * reverse proxies which alter the context URI. For example if the application is registered under the URI {@code /app-clifton-ims} but is proxied through a load balancer using
	 * the URI {@code /ims}, then the cookie path must be rewritten to {@code /ims}, even if the application is not aware of the load balancer URI.
	 * <p>
	 * Reverse proxies are often able to handle cookie path rewrites to some degree, but also often have limited capabilities for doing so. For example, NGINX can rewrite the paths
	 * for all cookies, but cannot selectively rewrite paths for individual cookies. In order for us to the the Crowd SSO cookie ({@code crowd.token_key}), we must retain its
	 * mapping to the root path while rewriting session token cookies to the URI used by the client.
	 * <p>
	 * The {@code X-Forwarded-Prefix} is typically used to inform the application of the requested URI. It is used in Spring's {@link ForwardedHeaderFilter} to alter the {@link
	 * HttpServletRequest#getContextPath() context path}, but this altered context path is not used by the Tomcat session cookie generator (see {@link
	 * SessionConfig#getSessionCookiePath(org.apache.catalina.Context)}). This bean registers a custom cookie processor to consistently apply the {@code X-Forwarded-Prefix} to
	 * session cookies.
	 */
	@Bean
	public TomcatContextCustomizer tomcatForwardedHeaderCookieContextCustomizer() {
		return context -> context.setCookieProcessor(new Rfc6265CookieProcessor() {
			@Override
			public String generateHeader(Cookie cookie, HttpServletRequest request) {
				String sessionCookieName = SessionConfig.getSessionCookieName(null);
				if (sessionCookieName.equalsIgnoreCase(cookie.getName())) {
					String forwardedUriPrefix = request.getHeader("X-Forwarded-Prefix");
					if (forwardedUriPrefix != null) {
						cookie.setPath(forwardedUriPrefix);
					}
				}
				return super.generateHeader(cookie, request);
			}
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ApplicationServletConfiguration {

		@Bean
		public ServletContextInitializer applicationServletContextInitializer(@Value("${oauth.relativeAuthorizationPath},${oauth.relativeTokenPath},*.json") String[] jsonProperties) {
			return servletContext -> {
				servletContext.setInitParameter("log4jConfiguration", "META-INF/log4j/log4j2.xml");
				servletContext.addListener(new Log4jServletContextListener());
				ServletRegistration servlet = servletContext.getServletRegistration(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);
				servlet.addMapping(jsonProperties);
			};
		}
	}


	@Configuration
	@Import({WebSocketConfiguration.class, WebSocketServletAutoConfiguration.class})
	@Conditional(WebSocketEnabledCondition.class)
	public static class ApplicationWebSocketConfiguration {

		@Bean
		public ServletContextInitializer applicationWebSocketServletContextInitializer(@Value("${application.websocket.servlet.path}/*") String[] websocketProperties) {
			return servletContext -> {
				ServletRegistration servlet = servletContext.getServletRegistration(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);
				servlet.addMapping(websocketProperties);
			};
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Configuration
	public static class ApplicationFilterConfiguration {

		public static final String URL_REWRITE_CONFIG_LOCATION = "classpath:/META-INF/urlrewrite.xml";


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * Disables automatic registration for all {@link Filter} beans. This bean factory post processor requires all {@link Filter} registration to be done through manual
		 * registration of {@link FilterRegistrationBean} beans.
		 * <p>
		 * The default Spring Framework implementation performs automatic registration through the use of the {@link ServletContextInitializerBeans.FilterRegistrationBeanAdapter} type.
		 * All beans of type {@link Filter} are automatically detected and used to produce matching {@link FilterRegistrationBean} beans, but only if matching {@link
		 * FilterRegistrationBean} beans do not already exist. By declaring {@link FilterRegistrationBean} beans, we preclude automatic filter registration for all {@link Filter} beans
		 * which are declared by the framework and allow the system to use only its own filters instead.
		 * <p>
		 * Automatic filter registration takes place during the {@link ServletWebServerApplicationContext#onRefresh() onRefresh} event for the {@link
		 * ServletWebServerApplicationContext} context type, during its {@link ServletWebServerApplicationContext#selfInitialize(ServletContext) selfInitialize} call. The {@code
		 * onRefresh} event is triggered takes place after bean factory post processing.
		 */
		@Bean
		public static BeanFactoryPostProcessor disableFilterRegistrationBeanFactoryPostProcessor() {
			return beanFactory -> {
				DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) beanFactory;
				String[] filterBeanNames = defaultListableBeanFactory.getBeanNamesForType(Filter.class, true, false);
				for (String filterBeanName : filterBeanNames) {
					AbstractBeanDefinition disableRegistrationBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(FilterRegistrationBean.class)
							.addPropertyReference("filter", filterBeanName)
							.addPropertyValue("enabled", false)
							.getBeanDefinition();
					defaultListableBeanFactory.registerBeanDefinition(filterBeanName + "DisableFilterRegistration", disableRegistrationBeanDefinition);
				}
			};
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Bean
		@ConditionalOnResource(resources = URL_REWRITE_CONFIG_LOCATION)
		public FilterRegistrationBean<UrlRewriteFilter> urlRewriteFilter(@Value(URL_REWRITE_CONFIG_LOCATION) Resource configResource) {
			ConfigurableUrlRewriteFilter filter = new ConfigurableUrlRewriteFilter();
			filter.setConfigResource(configResource);
			FilterRegistrationBean<UrlRewriteFilter> registration = new FilterRegistrationBean<>(filter);
			registration.setName("UrlRewriteFilter");
			registration.setOrder(0);
			registration.setAsyncSupported(true);
			registration.addInitParameter("logLevel", "ERROR"); // Remove unnecessary warnings for duplicate header names (UrlRewriteWrappedResponse.addHeader)
			registration.addUrlPatterns("/*");
			registration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.FORWARD);
			return registration;
		}


		/**
		 * Registers the {@link ForwardedHeaderFilter} to apply request rewrites from {@code Forwarded} and {@code X-Forwarded-*} headers.
		 * <p>
		 * This is in particular necessary for handling {@code X-Forwarded-Prefix} headers, which indicate the context prefix for which cookies and redirects should be written.
		 * This intentionally ignores remote address rewrites ({@code X-Forwarded-By} and {@code X-Forwarded-For}) since the filter is not able to apply sufficient security (a
		 * trusted proxies list would be required). Instead, remote address rewrites on the request are relegated to other devices if present, such as the Tomcat {@link
		 * RemoteIpValve} (normally enabled via the {@link ServerProperties#setForwardHeadersStrategy(ForwardHeadersStrategy) server.forward-headers-strategy} property with value
		 * {@link ForwardHeadersStrategy#NATIVE NATIVE}).
		 */
		@Bean
		public FilterRegistrationBean<ForwardedHeaderFilter> forwardedHeaderFilter() {
			ForwardedHeaderFilter filter = new ForwardedHeaderFilter();
			FilterRegistrationBean<ForwardedHeaderFilter> registration = new FilterRegistrationBean<>(filter);
			registration.setOrder(50);
			registration.addUrlPatterns("/*");
			registration.setAsyncSupported(true);
			registration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.ASYNC, DispatcherType.ERROR);
			return registration;
		}


		@Bean
		public FilterRegistrationBean<DelegatingFilterProxy> springSecurityFilterChainFilter() {
			DelegatingFilterProxy filter = new DelegatingFilterProxy();
			FilterRegistrationBean<DelegatingFilterProxy> registration = new FilterRegistrationBean<>(filter);
			registration.setName("springSecurityFilterChain");
			registration.setOrder(100);
			registration.setAsyncSupported(true);
			registration.addUrlPatterns("/*");
			registration.addServletNames(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);
			return registration;
		}


		/**
		 * Stores/clears current user in/from context associated with current request
		 */
		@Bean
		@ConditionalOnProperty(value = "clifton.use-standard-security-filter", matchIfMissing = true)
		public FilterRegistrationBean<DelegatingFilterProxy> securityUserManagementFilterRegistration() {
			DelegatingFilterProxy filter = new DelegatingFilterProxy();
			FilterRegistrationBean<DelegatingFilterProxy> registration = new FilterRegistrationBean<>(filter);
			// In this case, the filter name must match bean name
			registration.setName("securityUserManagementFilter");
			registration.setOrder(200);
			registration.setAsyncSupported(true);
			registration.addInitParameter("targetFilterLifecycle", "true");
			registration.addUrlPatterns("/oauth/authorize", "/oauth/token", "*.json", "/ws/*");
			registration.addServletNames(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME);
			registration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.ASYNC);
			return registration;
		}
	}
}
