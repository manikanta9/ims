package com.clifton.application;


import com.clifton.application.context.config.web.ApplicationWebMvcConfiguration.RequestStatsConfiguration;
import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ApplicationProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "application";
	}


	@Override
	protected boolean isServiceSkipped(String beanName) {
		return "applicationContextService".equals(beanName);
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.servlet.");
		imports.add("org.apache.catalina.");
		imports.add("org.apache.logging.log4j.");
		imports.add("org.apache.tomcat.util.http.Rfc6265CookieProcessor");
		imports.add("org.boris.winrun4j.AbstractService");
		imports.add("org.springframework.beans.");
		imports.add("org.springframework.boot.");
		imports.add("org.springframework.context.");
		imports.add("org.springframework.http.HttpStatus");
		imports.add("org.springframework.instrument.classloading.LoadTimeWeaver");
		imports.add("org.springframework.web.");
		imports.add("org.tuckey.");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		return CollectionUtils.combineCollections(
				super.getAllowedContextManagedBeanSuffixNames(),
				Arrays.asList(
						"Customizer",
						"FilterRegistration"
				),
				IntStream.rangeClosed(0, 9)
						.mapToObj(i -> RequestStatsConfiguration.STATIC_ASSET_STATS_RULE_BEAN_NAME_PREFIX + i)
						.collect(Collectors.toList())
		);
	}
}
