import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant

val apiVariant = registerVariant("api", upstreamForMain = true)

dependencies {

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	apiVariant.api(project(":clifton-core"))
	apiVariant.api(project(":clifton-core:clifton-core-messaging"))
	apiVariant.api(project(":clifton-system"))

	apiVariant.api(project(":clifton-fix")) { usingVariant("api") }

	api(project(":clifton-fix"))

	javascript(project(":clifton-fix"))


	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-fix")))

}
