package com.clifton.fix.server.session;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.fix.util.FixUtils;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * The <code>FixSessionExecutorService</code> defines the methods that can be called to start/restart/stop, etc. FixSessions
 *
 * @author manderson
 */
public interface FixSessionExecutorService {


	@RequestMapping("fixStopSession")
	@UserIgnorableValidation
	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void stopFixSession(Short fixSessionId, boolean ignoreValidation);


	@RequestMapping("fixStopSessionList")
	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void stopFixSessionList(String[] sessionIds, boolean force);


	@RequestMapping("fixStartSession")
	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void startFixSession(Short fixSessionId);


	@RequestMapping("fixRestartSession")
	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void restartFixSession(Short fixSessionId);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Ability to manually override the sequence numbers on the session
	 * If the session is running: Will first stop the session
	 * Then will update the sequence numbers on the FixSession
	 * Then, if originally running will start the session back up
	 */
	@RequestMapping("fixResetSessionSequenceNumbersManual")
	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void resetFixSessionSequenceNumbersManual(Short fixSessionId, Integer incomingSequenceNumber, Integer outgoingSequenceNumber);
}
