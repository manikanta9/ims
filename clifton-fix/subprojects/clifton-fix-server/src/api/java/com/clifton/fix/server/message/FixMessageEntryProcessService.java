package com.clifton.fix.server.message;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.util.FixUtils;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author manderson
 */
public interface FixMessageEntryProcessService {

	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	@RequestMapping("fixMessageEntryReprocess")
	public FixMessage reprocessFixMessageEntry(long entryId);


	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	@RequestMapping("fixMessageEntryReprocessList")
	public void reprocessFixMessageEntryList(long[] entryIdList);


	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	@RequestMapping("fixMessageEntryFailedMessageListReprocess")
	public void reprocessFixMessageEntryFailedMessageList();
}
