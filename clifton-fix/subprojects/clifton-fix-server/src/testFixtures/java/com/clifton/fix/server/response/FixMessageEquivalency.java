package com.clifton.fix.server.response;

import com.clifton.fix.server.message.FixMessageDifference;


public interface FixMessageEquivalency<M, F> {

	public boolean areEquivalent(M one, M two);


	/**
	 * Intended to show the detailed differences in the case where {@link #areEquivalent(Object, Object)} fails.
	 */
	public FixMessageDifference symmetricDifferences(M incoming, M template);
}
