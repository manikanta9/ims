package com.clifton.fix.server.provider;

/**
 * The implementation specific sequence provider gives
 * a unique sequence number to all out-going messages.
 */
public interface FixMessageSequenceProvider {

	public String getNextSequenceNumber();
}
