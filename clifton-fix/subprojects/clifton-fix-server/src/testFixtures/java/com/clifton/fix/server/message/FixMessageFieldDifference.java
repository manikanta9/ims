package com.clifton.fix.server.message;

import java.util.function.Supplier;


public class FixMessageFieldDifference {

	private String field;
	private String label;
	private String incoming;
	private String template;


	private FixMessageFieldDifference(String field, String label, String incoming, String template) {
		this.field = field;
		this.label = label;
		this.incoming = incoming;
		this.template = template;
	}


	@Override
	public String toString() {
		return this.label +
				"(" + this.field + ")" +
				" = (" +
				this.incoming +
				", " +
				this.template +
				")";
	}


	public static FixMessageFieldDifference build(String field, String label, Supplier<String> incoming, Supplier<String> template) {
		return new FixMessageFieldDifference(field, label, incoming.get(), template.get());
	}
}
