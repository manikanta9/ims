package com.clifton.fix.server;

import com.clifton.core.messaging.jms.asynchronous.sender.MessageSender;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.util.FixUtilService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * FixTestServerMessageRouter reads messages placed on the JMS queue, delegates
 * the response determination logic and returns the test appropriate response to the
 * shared queue.
 */
public class FixTestServerMessageRouter<T> extends AbstractFixServerMessageRouter<T> {

	private MessageSender fixServerMessagingSender;
	private FixMessageEntryService fixMessageEntryService;
	private FixUtilService<T> fixUtilService;


	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public void send(T message, String sessionQualifier) {
		FixSession fixSession = getFixUtilService().getFixSession(message, sessionQualifier, false);
		String typeName = getFixUtilService().getMessageTypeCode(message.toString());
		getFixMessageEntryService().createMessageEntry(message.toString(), typeName, false, fixSession);

		FixMessageTextMessagingMessage newMessage = new FixMessageTextMessagingMessage();
		newMessage.setMessageText(message.toString());
		newMessage.setIdentifier(getFixIdentifierHandler().applyFixIdentifier(message, newMessage, true, fixSession));
		String sourceSystemTag = null;
		if (newMessage.getIdentifier() != null && !StringUtils.isEmpty(newMessage.getIdentifier().getSourceSystemTag())) {
			sourceSystemTag = newMessage.getIdentifier().getSourceSystemTag();
		}
		else if (!StringUtils.isEmpty(fixSession.getDefinition().getUnknownMessageSystemTag())) {
			sourceSystemTag = fixSession.getDefinition().getUnknownMessageSystemTag();
		}
		if (!StringUtils.isEmpty(sourceSystemTag)) {
			newMessage.getProperties().put("sourceSystemTag", sourceSystemTag);
		}

		MessageSender messageSender = getMessageSender(fixSession, sourceSystemTag);
		messageSender.send(newMessage);
	}


	/**
	 * Get the JMS message sender. This is a test router so we only have one defined.
	 */
	@Override
	protected MessageSender getMessageSender(FixSession session, String sourceSystemTag) {
		return getFixServerMessagingSender();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MessageSender getFixServerMessagingSender() {
		return this.fixServerMessagingSender;
	}


	public void setFixServerMessagingSender(MessageSender fixServerMessagingSender) {
		this.fixServerMessagingSender = fixServerMessagingSender;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixUtilService<T> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<T> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}
}
