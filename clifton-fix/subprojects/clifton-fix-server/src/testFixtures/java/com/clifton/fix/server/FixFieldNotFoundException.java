package com.clifton.fix.server;

/**
 * Exception thrown when a message does not contain a field value.
 */
public class FixFieldNotFoundException extends Exception {

	public FixFieldNotFoundException(String message) {
		super(message);
	}


	public FixFieldNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}


	public FixFieldNotFoundException(Throwable cause) {
		super(cause);
	}


	public FixFieldNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
