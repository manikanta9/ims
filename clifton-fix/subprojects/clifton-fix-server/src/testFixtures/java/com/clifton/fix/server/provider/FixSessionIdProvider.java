package com.clifton.fix.server.provider;

/**
 * Get the Implementation specific Fix Session Identifier from the provided message and session qualifier.
 */
public interface FixSessionIdProvider<S, M> {

	public S getFixSessionId(M message, String sessionQualifier);
}
