package com.clifton.fix.server.response;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.server.provider.FixFileNameProvider;
import com.clifton.fix.util.FixUtilService;
import com.google.common.base.Charsets;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Reads text files from the provided path, builds a list of expected test case
 * 'template' requests and responses.  The file name corresponds to a test
 * case characterized by SenderCompID, SenderSubID, SecurityID, SecurityIDSource
 * and quantity.  See {@link FixFileNameProvider}.
 */
public class FixServerResponseCollection<M> {

	private String directory;
	private FixFileNameProvider<M> fixFileNameProvider;
	private FixUtilService<M> fixUtilService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<M> getTestCaseMessages(M firstMessage) {
		try {
			String filename = getFixFileNameProvider().getFixFileName(firstMessage);
			return buildTestCaseTemplateList(filename);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not load test case messages", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<M> buildTestCaseTemplateList(String filename) throws IOException {
		ResourceLoader loader = new DefaultResourceLoader();
		Resource resource = loader.getResource(Paths.get(this.directory).toString());
		return CollectionUtils.createList(resource.getFile().listFiles())
				.stream()
				.filter(f -> filename.equals(f.getName()))
				.flatMap(f -> buildFileResponseList(f).stream())
				.collect(Collectors.toList());
	}


	private List<M> buildFileResponseList(File file) {
		List<String> lines;
		try {
			lines = Files.readAllLines(file.toPath(), Charsets.UTF_8);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not open test file.", e);
		}
		return lines.stream().map(getFixUtilService()::parseMessage).collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDirectory() {
		return this.directory;
	}


	public void setDirectory(String directory) {
		this.directory = directory;
	}


	public FixFileNameProvider<M> getFixFileNameProvider() {
		return this.fixFileNameProvider;
	}


	public void setFixFileNameProvider(FixFileNameProvider<M> fixFileNameProvider) {
		this.fixFileNameProvider = fixFileNameProvider;
	}


	public FixUtilService<M> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<M> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}
}
