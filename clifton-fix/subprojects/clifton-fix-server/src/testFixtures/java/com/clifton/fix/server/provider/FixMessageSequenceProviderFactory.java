package com.clifton.fix.server.provider;

/**
 * Return an implementation specific {@link FixMessageSequenceProvider} for the
 * provided implementation specific session identifier.
 */
public interface FixMessageSequenceProviderFactory<S> {

	public FixMessageSequenceProvider getFixMessageSequenceProvider(S sessionID);
}
