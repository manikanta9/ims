package com.clifton.fix.server.response.transform;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class FixFieldValueMapper {

	private Map<String, Map<String, String>> fixFieldValueMap;


	public FixFieldValueMapper() {
		this.fixFieldValueMap = new HashMap<>();
	}


	public Map<String, String> getFieldValueMapping(String uniqueIdentifier) {
		return this.fixFieldValueMap.get(uniqueIdentifier);
	}


	public Set<String> getUniqueIdentifiers() {
		return this.fixFieldValueMap.keySet();
	}


	public void addFieldValueMapping(String uniqueIdentifier, Map<String, String> fixFieldValueMap) {
		Map<String, String> replacements = this.fixFieldValueMap.computeIfAbsent(uniqueIdentifier, key -> new HashMap<>());
		replacements.putAll(fixFieldValueMap);
	}
}
