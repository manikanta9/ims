package com.clifton.fix.server.response.transform;

import com.clifton.fix.server.provider.FixMessageSequenceProvider;


/**
 * Return an implementation specific {@link FixMessageTransformer} along with its correct {@link FixFieldValueMapper} and {@link FixMessageSequenceProvider} instances.
 */
public interface FixMessageTransformerFactory<T> {

	public FixMessageTransformer<T> getFixMessageTransform(FixFieldValueMapper fixFieldValueMapper, FixMessageSequenceProvider fixMessageSequenceProvider);
}
