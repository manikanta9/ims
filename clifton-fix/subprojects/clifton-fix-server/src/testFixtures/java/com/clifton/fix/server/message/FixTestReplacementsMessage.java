package com.clifton.fix.server.message;

import java.util.Map;


public class FixTestReplacementsMessage implements FixTestMessage<Map<String, String>> {

	private Map<String, String> replacements;
	private String uniqueIdentifier;


	public FixTestReplacementsMessage(String uniqueIdentifier, Map<String, String> replacements) {
		this.uniqueIdentifier = uniqueIdentifier;
		this.replacements = replacements;
	}


	@Override
	public Map<String, String> getPayload() {
		return this.replacements;
	}


	public String getUniqueIdentifier() {
		return this.uniqueIdentifier;
	}
}
