package com.clifton.fix.server.message;

import java.util.Map;
import java.util.concurrent.BlockingQueue;


/**
 * Provides a communication mechanism between the JUnit Test and the Fix Test Server threads.
 * This allows the test to wait for status messages from the Fix Test Server thread.
 */
public class FixTestMessageConduitImpl implements FixTestMessageConduit<FixTestMessage<String>, FixTestMessage<Map<String, String>>> {

	private BlockingQueue<FixTestMessage<String>> messageQueue;
	private BlockingQueue<FixTestReplacementsMessage> replacementQueue;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public void sendFailedMessage(String message) {
		send(FixTestMessageStatus.FAILED, message);
	}


	@Override
	public void sendIntermediateMessage(String message) {
		send(FixTestMessageStatus.INTERMEDIATE_RESULT, message);
	}


	@Override
	public void sendSuccessMessage(String message) {
		send(FixTestMessageStatus.SUCCESS, message);
	}


	@Override
	public void sendWaitingMessage(String message) {
		send(FixTestMessageStatus.WAITING_FOR_REQUEST, message);
	}


	@Override
	public void sendReceivedReplacementsMessage() {
		send(FixTestMessageStatus.FIELD_VALUE_MAPPINGS, "");
	}


	@Override
	public void sendReplacements(String uniqueIdentifier, Map<String, String> replacements) {
		try {
			FixTestReplacementsMessage fixTestReplacementsMessage = new FixTestReplacementsMessage(uniqueIdentifier, replacements);
			this.replacementQueue.put(fixTestReplacementsMessage);
		}
		catch (InterruptedException e) {
			throw new RuntimeException("Error putting replacements to the queue.", e);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void send(FixTestMessageStatus status, String message) {
		send(new FixTestStatusMessage(status, message));
	}


	private void send(FixTestMessage<String> message) {
		try {
			this.messageQueue.put(message);
		}
		catch (InterruptedException e) {
			throw new RuntimeException("Error putting message to the queue.", e);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public void setMessageQueue(BlockingQueue<FixTestMessage<String>> messageQueue) {
		this.messageQueue = messageQueue;
	}


	@Override
	public void setReplacementsQueue(BlockingQueue<FixTestReplacementsMessage> replacementQueue) {
		this.replacementQueue = replacementQueue;
	}
}
