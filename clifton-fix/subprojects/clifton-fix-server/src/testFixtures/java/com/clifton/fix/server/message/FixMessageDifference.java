package com.clifton.fix.server.message;

import java.util.ArrayList;
import java.util.List;


public class FixMessageDifference {

	private String type;

	private List<FixMessageFieldDifference> header = new ArrayList<>();
	private List<FixMessageFieldDifference> body = new ArrayList<>();
	private List<FixMessageFieldDifference> group = new ArrayList<>();


	public FixMessageDifference(String type) {
		this.type = type;
	}


	public void addHeader(List<FixMessageFieldDifference> fixMessageFieldDifferences) {
		this.header.addAll(fixMessageFieldDifferences);
	}


	public void addBody(List<FixMessageFieldDifference> fixMessageFieldDifferences) {
		this.body.addAll(fixMessageFieldDifferences);
	}


	public void addGroup(List<FixMessageFieldDifference> fixMessageFieldDifferences) {
		this.group.addAll(fixMessageFieldDifferences);
	}


	@Override
	public String toString() {
		return "FixMessageDifferences(incoming, template)" +
				"{" +
				"  type=" + this.type +
				"  header=" + this.header +
				", body=" + this.body +
				", group=" + this.group +
				" }";
	}
}
