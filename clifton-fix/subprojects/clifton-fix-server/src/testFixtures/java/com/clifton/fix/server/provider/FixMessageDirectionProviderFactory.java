package com.clifton.fix.server.provider;

/**
 * Returns the {@link FixMessageDirectionProvider} implementation necessary for
 * the the provided message.
 */
public interface FixMessageDirectionProviderFactory<T> {

	public FixMessageDirectionProvider<T> getFixMessageDirectionProvider(T message);
}
