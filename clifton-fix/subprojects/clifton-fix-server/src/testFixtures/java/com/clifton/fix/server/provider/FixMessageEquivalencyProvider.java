package com.clifton.fix.server.provider;

import com.clifton.fix.server.response.FixMessageEquivalency;


public interface FixMessageEquivalencyProvider<M, F> {

	FixMessageEquivalency<M, F> getFixMessageEquivalency();
}
