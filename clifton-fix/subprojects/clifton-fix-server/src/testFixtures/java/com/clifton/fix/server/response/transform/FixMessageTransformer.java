package com.clifton.fix.server.response.transform;

/**
 * Takes a 'template' message and applies values from the source message.
 * The template is first deeply cloned.  The value providers are supplied
 * by the source and extract by the mapper.
 */
public interface FixMessageTransformer<T> {

	public T transform(T templateResponse, T source);
}
