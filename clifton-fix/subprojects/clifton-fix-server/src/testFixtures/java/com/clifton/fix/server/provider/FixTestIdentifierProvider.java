package com.clifton.fix.server.provider;

import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;

import java.util.List;


/**
 * Provides a test specific identifier from the provided message and
 * {@link FixMessageTextMessagingMessage}.
 */
public interface FixTestIdentifierProvider<T> {

	/**
	 * Get the allocation identifier from the list of responses.
	 */
	public String getAllocationSecondaryIdentifier(List<T> responses);


	/**
	 * Get the test identifier from an incoming message.
	 */
	public String getTestIdentifier(T message, BaseFixMessagingMessage fixMessagingMessage);


	/**
	 * get the Client Order ID (11) or AllocationID (70).
	 */
	public String getMessageIdentifier(T message);
}
