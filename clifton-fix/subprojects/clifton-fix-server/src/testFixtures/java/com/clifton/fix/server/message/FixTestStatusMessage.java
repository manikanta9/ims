package com.clifton.fix.server.message;

/**
 * Messages communicated via a shared queue between the Junit test and
 * the fix server.
 */
public class FixTestStatusMessage implements FixTestMessage<String> {

	private FixTestMessageStatus status;
	private String payload;


	public FixTestStatusMessage(FixTestMessageStatus status, String message) {
		this.status = status;
		this.payload = message;
	}


	@Override
	public String toString() {
		return "FixTestMessage{" +
				"status=" + this.status +
				", message='" + this.payload + '\'' +
				'}';
	}


	@Override
	public String getPayload() {
		return this.payload;
	}


	public FixTestMessageStatus getStatus() {
		return this.status;
	}
}
