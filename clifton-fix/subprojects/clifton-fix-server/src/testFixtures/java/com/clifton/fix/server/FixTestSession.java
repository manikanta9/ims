package com.clifton.fix.server;

import com.clifton.fix.server.message.FixTestMessageConduit;
import com.clifton.fix.server.message.FixTestReplacementsMessage;
import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.response.FixServerResponseState;
import com.clifton.fix.server.response.transform.FixFieldValueMapper;
import com.clifton.fix.server.response.transform.FixMessageTransformer;
import com.clifton.fix.server.response.transform.FixMessageTransformerFactory;

import java.util.List;
import java.util.Set;


/**
 * FixTestSession contains the expected request and responses for each test case and
 * the status of the communications with the IMS.
 */
public class FixTestSession<M, F, E, P> {

	private final String testIdentifier;
	private final FixServerResponseState<M, F> fixServerResponseState;
	private final FixMessageSequenceProvider fixMessageSequenceProvider;
	private final FixMessageTransformerFactory<M> fixMessageTransformerFactory;
	private final Set<String> uniqueIdentifiers;

	private FixFieldValueMapper valueMappers = new FixFieldValueMapper();


	public FixTestSession(final String testIdentifier, final FixServerResponseState<M, F> fixServerResponseState, final FixMessageSequenceProvider fixMessageSequenceProvider, final FixMessageTransformerFactory<M> fixMessageTransformerFactory, final FixTestMessageConduit<E, P> fixTestMessageConduit, final Set<String> uniqueIdentifier) {
		this.testIdentifier = testIdentifier;
		this.fixServerResponseState = fixServerResponseState;
		this.fixMessageSequenceProvider = fixMessageSequenceProvider;
		this.fixMessageTransformerFactory = fixMessageTransformerFactory;
		this.uniqueIdentifiers = uniqueIdentifier;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<M> getResponses(M incomingRequest) {
		FixMessageTransformer<M> fixMessageTransformer = getFixMessageTransformerFactory().getFixMessageTransform(this.valueMappers, getFixMessageSequenceProvider());
		return getFixServerResponseState().nextResponsesList(incomingRequest, fixMessageTransformer);
	}


	public FixServerResponseState.State getState() {
		return getFixServerResponseState().getState();
	}


	public void putReplacementsFields(String uniqueIdentifier, FixTestReplacementsMessage fixTestReplacementsMessage) {
		this.valueMappers.addFieldValueMapping(uniqueIdentifier, fixTestReplacementsMessage.getPayload());
	}


	public String getTestIdentifier() {
		return this.testIdentifier;
	}


	public FixFieldValueMapper getValueMappers() {
		return this.valueMappers;
	}


	public boolean hasUniqueIdentifierMessage(String uniqueIdentifier) {
		return this.uniqueIdentifiers.contains(uniqueIdentifier);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixServerResponseState<M, F> getFixServerResponseState() {
		return this.fixServerResponseState;
	}


	public FixMessageSequenceProvider getFixMessageSequenceProvider() {
		return this.fixMessageSequenceProvider;
	}


	public FixMessageTransformerFactory<M> getFixMessageTransformerFactory() {
		return this.fixMessageTransformerFactory;
	}
}
