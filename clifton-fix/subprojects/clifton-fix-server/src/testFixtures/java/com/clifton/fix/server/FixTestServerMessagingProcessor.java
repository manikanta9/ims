package com.clifton.fix.server;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.message.validation.FixMessageValidationHandler;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.server.message.FixTestMessageConduit;
import com.clifton.fix.server.response.FixServerResponseState.State;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.setup.FixSourceSystem;

import java.util.List;
import java.util.concurrent.BlockingQueue;


/**
 * FixTestServerMessagingProcessor intercepts Fix messages intended for external
 * services and returns the responses from a test file of expected requests and responses.
 */
public class FixTestServerMessagingProcessor<M, S, E, P, F> extends FixServerMessagingProcessor {

	private FixMessageValidationHandler<M> fixMessageValidationHandler;

	private FixTestSessionStorage<M, S, E, P, F> fixTestSessionStorage;
	private FixTestMessageConduit<E, P> fixTestMessageConduit;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public void testSetup(BlockingQueue<E> messageQueue, String testDataPath) {
		getFixTestMessageConduit().setMessageQueue(messageQueue);
		getFixTestSessionStorage().getFixServerResponseCollection().setDirectory(testDataPath);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	protected void handleFixMessagingMessage(BaseFixMessagingMessage message, AsynchronousMessageHandler handler) {
		try {
			String messageText = getFixMessageHandler().getBaseFixMessagingMessageText(message);

			M fixMessage = (M) getFixUtilService().parseMessage(messageText);
			//Set header fields if destination is present
			if (message.getDestinationName() != null && !message.getDestinationName().isEmpty()) {
				FixDestination destination = getFixDestinationService().getFixDestinationByName(message.getDestinationName());
				FixSessionDefinition definition = destination.getSessionDefinition();
				message.setSessionQualifier(definition.getSessionQualifier());
				getFixUtilService().setMessageInfoFromSessionDefinition(fixMessage, definition);
			}
			// apply tag modifiers
			getFixTagModifierHandler().applyTagModifiers(fixMessage, message.getSessionQualifier(), null);
			// get test identifier, also creates fix identifier if necessary.
			String testIdentifier = getFixTestSessionStorage().getTestIdentifier(fixMessage, message);

			// send first response
			if (!getFixTestSessionStorage().hasSession(testIdentifier)) {
				if (!FixMessageTypeTagValues.NEW_ORDER_SINGLE_TYPE_TAG.getTagValue().equals(getFixUtilService().getMessageTypeCode(messageText))) {
					throw new RuntimeException("Message should be associated with an existing test session with identifier " + testIdentifier);
				}
				getFixTestSessionStorage().setupNewTestSession(fixMessage, message, testIdentifier);
				sendNewSessionIdentifier(handler, message, fixMessage, messageText);
			}
			// send subsequent responses
			sendFixServiceResponses(handler, message, fixMessage, messageText);
		}
		catch (Exception e) {
			getFixTestMessageConduit().sendFailedMessage(e.getMessage());
			throw new RuntimeException("Error responding.", e);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void sendNewSessionIdentifier(AsynchronousMessageHandler handler, BaseFixMessagingMessage message, M fixMessage, String messageText) {
		// send the identifier message back to IMS
		FixIdentifier identifier = getFixIdentifier(fixMessage, message, false, messageText);
		identifier.setNewlyCreated(false);
		FixEntityMessagingMessage idMessage = new FixEntityMessagingMessage();
		BeanUtils.copyProperties(message, idMessage, new String[]{"machineName", "messageDelaySeconds"});
		FixEntity fixEntity = getFixEntityService().getFixEntityForFixMessageText(messageText, message.getSourceSystemTag());
		fixEntity.setIdentifier(identifier);
		idMessage.setFixEntity(fixEntity);
		idMessage.getProperties().put("sourceSystemTag", message.getSourceSystemTag());
		handler.send(idMessage);
	}


	private void sendFixServiceResponses(AsynchronousMessageHandler handler, BaseFixMessagingMessage message, M fixMessage, String messageText) {
		FixIdentifier identifier = getFixIdentifier(fixMessage, message, false, messageText);
		FixSession fixSession = identifier.getSession();
		updateMessageLogs(fixMessage, fixSession, false);
		String testIdentifier = getFixTestSessionStorage().getTestIdentifier(fixMessage, message);
		FixTestSession<M, F, E, P> fixTestSession = getFixTestSessionStorage().getFixTestSession(testIdentifier);
		if (fixTestSession.getState() == State.IN_PROGRESS) {
			List<M> responses = fixTestSession.getResponses(fixMessage);
			getFixTestMessageConduit().sendIntermediateMessage("Send Responses " + responses.size());
			responses.forEach(m -> sendFixResponse(handler, m, identifier));
		}
		switch (fixTestSession.getState()) {
			case FAILURE:
				getFixTestMessageConduit().sendFailedMessage("Test failed.");
				break;
			case DONE:
				getFixTestMessageConduit().sendSuccessMessage("Test finished successfully.");
				break;
			case IN_PROGRESS:
			case NOT_STARTED:
				getFixTestMessageConduit().sendWaitingMessage("Waiting for next request.");
				break;
		}
	}


	/**
	 * Sends an individual message response to the queue. This replicates much of the logic in {@link AbstractFixServerMessageRouter#receiveFromSession(Object, FixSession)}.
	 */
	private void sendFixResponse(AsynchronousMessageHandler handler, M message, FixIdentifier fixIdentifier) {
		FixSession fixSession = fixIdentifier.getSession();
		updateMessageLogs(message, fixSession, true);

		// Determine source system
		final String sourceSystemTag;
		if (!StringUtils.isEmpty(fixIdentifier.getSourceSystemTag())) {
			sourceSystemTag = fixIdentifier.getSourceSystemTag();
		}
		else if (!StringUtils.isEmpty(fixSession.getDefinition().getUnknownMessageSystemTag())) {
			sourceSystemTag = fixSession.getDefinition().getUnknownMessageSystemTag();
		}
		else {
			sourceSystemTag = null;
		}

		FixSourceSystem sourceSystem = getFixSetupService().getFixSourceSystemByTag(sourceSystemTag);

		// Generate message
		final AsynchronousMessage resultMessage;
		String messageText = message.toString();
		List<FixValidationResult> validationResults = getFixMessageValidationHandler().validate(message, fixIdentifier);
		if (sourceSystem != null && sourceSystem.isSerializeEntity()) {
			FixEntity fixEntity = getFixEntityService().getFixEntityForFixMessageText(messageText, sourceSystemTag);
			fixEntity.setIdentifier(fixIdentifier);
			fixEntity.setAllocationValidationError(FixValidationResult.isAllocationValidationError(validationResults));
			fixEntity.setValidationError(FixValidationResult.isValidationError(validationResults));
			FixEntityMessagingMessage entityMessage = new FixEntityMessagingMessage();
			entityMessage.setFixEntity(fixEntity);
			resultMessage = entityMessage;
		}
		else {
			FixMessageTextMessagingMessage fixTextMessage = new FixMessageTextMessagingMessage();
			fixTextMessage.setMessageText(messageText);
			fixTextMessage.setIdentifier(fixIdentifier);
			fixTextMessage.setAllocationValidationError(FixValidationResult.isAllocationValidationError(validationResults));
			fixTextMessage.setValidationError(FixValidationResult.isValidationError(validationResults));
			resultMessage = fixTextMessage;
		}
		if (sourceSystem != null) {
			resultMessage.getProperties().put("sourceSystemTag", sourceSystemTag);
			resultMessage.getProperties().put(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME, sourceSystem.getMessageFormat().getContentType());
		}

		handler.send(resultMessage);
	}


	private void updateMessageLogs(M message, FixSession fixSession, boolean incoming) {
		FixMessageEntry fixMessageEntry = getFixMessageEntryService().createMessageEntry(message.toString(), FixMessageEntryService.MESSAGE_ENTRY_TYPE_NAME, incoming, fixSession);
		getFixMessageProcessHandler().processMessage(fixMessageEntry);
	}


	@SuppressWarnings("unchecked")
	private FixIdentifier getFixIdentifier(M fixMessage, BaseFixMessagingMessage message, boolean incoming, String messageText) {
		return getFixIdentifierHandler().applyFixIdentifier(fixMessage, message, incoming, null, messageText);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public FixMessageValidationHandler<M> getFixMessageValidationHandler() {
		return this.fixMessageValidationHandler;
	}


	public void setFixMessageValidationHandler(FixMessageValidationHandler<M> fixMessageValidationHandler) {
		this.fixMessageValidationHandler = fixMessageValidationHandler;
	}


	public FixTestSessionStorage<M, S, E, P, F> getFixTestSessionStorage() {
		return this.fixTestSessionStorage;
	}


	public void setFixTestSessionStorage(FixTestSessionStorage<M, S, E, P, F> fixTestSessionStorage) {
		this.fixTestSessionStorage = fixTestSessionStorage;
	}


	public FixTestMessageConduit<E, P> getFixTestMessageConduit() {
		return this.fixTestMessageConduit;
	}


	public void setFixTestMessageConduit(FixTestMessageConduit<E, P> fixTestMessageConduit) {
		this.fixTestMessageConduit = fixTestMessageConduit;
	}
}

