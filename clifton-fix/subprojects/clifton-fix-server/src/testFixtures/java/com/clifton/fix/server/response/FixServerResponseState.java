package com.clifton.fix.server.response;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.server.provider.FixMessageDirectionProvider;
import com.clifton.fix.server.response.transform.FixMessageTransformer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Tracks progress in the test execution.
 * Contains the raw message that was received.
 */
public class FixServerResponseState<M, F> {

	private FixMessageEquivalency<M, F> fixMessageEquivalency;
	private FixMessageDirectionProvider<M> fixDirectionProvider;
	private State state;
	private List<M> messages;
	private int currentIndex = 0;


	public FixServerResponseState(FixMessageEquivalency<M, F> fixMessageEquivalency) {
		this.state = State.NOT_STARTED;
		this.fixMessageEquivalency = fixMessageEquivalency;
	}


	public void failure() {
		this.state = State.FAILURE;
		clear();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void started(List<M> messages, FixMessageDirectionProvider<M> fixDirectionProvider) {
		this.state = State.IN_PROGRESS;
		this.messages = CollectionUtils.asNonNullList(messages);
		this.fixDirectionProvider = fixDirectionProvider;
		this.currentIndex = 0;
	}


	public void done() {
		this.state = State.DONE;
		clear();
	}


	public List<M> nextResponsesList(M request, FixMessageTransformer<M> fixMessageTransformer) {
		verifyCurrent(request, fixMessageTransformer);
		List<M> responses = new ArrayList<>();
		while (hasNextResponse()) {
			responses.add(fixMessageTransformer.transform(nextResponse(), request));
		}
		while (hasNextRequest()) {
			this.currentIndex++;
		}
		if (isDone()) {
			done();
		}

		return responses;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void verifyCurrent(M message, FixMessageTransformer<M> fixMessageTransformer) {
		M templateRequest = (!this.messages.isEmpty()) ? this.messages.get(this.currentIndex) : null;
		M transformed = fixMessageTransformer.transform(templateRequest, message);
		if (!getFixMessageEquivalency().areEquivalent(message, transformed)) {
			failure();
			throw new RuntimeException("IMS request message should be functionally equivalent to the test message at index " + (this.currentIndex + 1) + ".  "
					+ getFixMessageEquivalency().symmetricDifferences(message, transformed));
		}
	}


	public M getCurrent() {
		return (!this.messages.isEmpty()) ? this.messages.get(this.currentIndex) : null;
	}


	private void clear() {
		this.messages = Collections.emptyList();
		this.currentIndex = 0;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private M nextResponse() {
		if (hasNextResponse()) {
			return this.messages.get(++this.currentIndex);
		}
		return null;
	}


	private boolean isDone() {
		return (this.currentIndex + 1) >= this.messages.size();
	}


	private boolean hasNextResponse() {
		return (this.state == State.IN_PROGRESS
				&& (this.currentIndex + 1) < this.messages.size()
				&& isResponse(this.messages.get(this.currentIndex + 1)));
	}


	private boolean hasNextRequest() {
		return (this.state == State.IN_PROGRESS
				&& (this.currentIndex + 1) < this.messages.size()
				&& isRequest(this.messages.get(this.currentIndex + 1)));
	}


	private boolean isResponse(M message) {
		return getFixDirectionProvider().isResponse(message);
	}


	private boolean isRequest(M message) {
		return getFixDirectionProvider().isRequest(message);
	}


	public State getState() {
		return this.state;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEquivalency<M, F> getFixMessageEquivalency() {
		return this.fixMessageEquivalency;
	}


	public void setFixMessageEquivalency(FixMessageEquivalency<M, F> fixMessageEquivalency) {
		this.fixMessageEquivalency = fixMessageEquivalency;
	}


	public FixMessageDirectionProvider<M> getFixDirectionProvider() {
		return this.fixDirectionProvider;
	}


	public void setFixDirectionProvider(FixMessageDirectionProvider<M> fixDirectionProvider) {
		this.fixDirectionProvider = fixDirectionProvider;
	}


	public enum State {
		NOT_STARTED,
		IN_PROGRESS,
		FAILURE,
		DONE
	}
}
