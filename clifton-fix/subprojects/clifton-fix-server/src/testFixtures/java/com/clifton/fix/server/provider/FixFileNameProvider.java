package com.clifton.fix.server.provider;

/**
 * Build the test data file name from the provided message.
 */
public interface FixFileNameProvider<M> {

	public static final String FIX_DATA_FILE_EXTENSION = "dat";


	public String getFixFileName(M message);
}
