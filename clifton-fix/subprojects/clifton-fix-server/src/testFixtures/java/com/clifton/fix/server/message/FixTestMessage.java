package com.clifton.fix.server.message;

public interface FixTestMessage<P> {

	public P getPayload();
}
