package com.clifton.fix.server.provider;

/**
 * Return whether the provided message is a request or
 * response based on implementation specific message
 * fields.
 */
public interface FixMessageDirectionProvider<M> {

	public boolean isRequest(M message);


	public boolean isResponse(M message);
}
