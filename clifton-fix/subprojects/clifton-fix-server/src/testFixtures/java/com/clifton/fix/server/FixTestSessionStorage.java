package com.clifton.fix.server;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.server.message.FixTestMessageConduit;
import com.clifton.fix.server.message.FixTestReplacementsMessage;
import com.clifton.fix.server.provider.FixMessageDirectionProvider;
import com.clifton.fix.server.provider.FixMessageDirectionProviderFactory;
import com.clifton.fix.server.provider.FixMessageEquivalencyProvider;
import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.provider.FixMessageSequenceProviderFactory;
import com.clifton.fix.server.provider.FixSessionIdProvider;
import com.clifton.fix.server.provider.FixTestIdentifierProvider;
import com.clifton.fix.server.response.FixMessageEquivalency;
import com.clifton.fix.server.response.FixServerResponseCollection;
import com.clifton.fix.server.response.FixServerResponseState;
import com.clifton.fix.server.response.transform.FixMessageTransformerFactory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;


/**
 * <code>FixTestSessionStorage</code> deals with test session specific messaging, stores a test specific set of response messages and map of replacement values.
 */
public class FixTestSessionStorage<M, S, E, P, F> {

	// test session handling
	private FixServerResponseCollection<M> fixServerResponseCollection;
	private FixTestIdentifierProvider<M> fixTestIdentifierProvider;
	private FixMessageDirectionProviderFactory<M> fixMessageDirectionProviderFactory;
	private FixMessageEquivalencyProvider<M, F> fixMessageEquivalencyProvider;
	private FixSessionIdProvider<S, M> fixSessionIdProvider;
	private FixMessageSequenceProviderFactory<S> fixMessageSequenceProviderFactory;
	private FixMessageTransformerFactory<M> fixMessageTransformerFactory;
	private FixTestMessageConduit<E, P> fixTestMessageConduit;

	// store sessions
	private Map<String, FixTestSession<M, F, E, P>> fixTestSessionMap = new LinkedHashMap<>();

	// communicate with junit test field value replacements
	private BlockingQueue<FixTestReplacementsMessage> replacementQueue = new LinkedBlockingQueue<>();

	// deferred replacements, update the session when its created
	private ConcurrentMap<String, List<FixTestReplacementsMessage>> deferredReplacements = new ConcurrentHashMap<>();

	// separate thread to monitor the replacements queue
	private ExecutorService executorService;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public void initialize() {
		getFixTestMessageConduit().setReplacementsQueue(this.replacementQueue);
		// monitor the queue and route to the specific test session.
		this.executorService = Executors.newSingleThreadExecutor();
		this.executorService.execute(() -> {
			while (!Thread.interrupted()) {
				try {
					FixTestReplacementsMessage fixTestReplacementsMessage = this.replacementQueue.take();
					String uniqueIdentifier = fixTestReplacementsMessage.getUniqueIdentifier();
					List<FixTestSession<M, F, E, P>> sessionList = CollectionUtils.getFiltered(this.fixTestSessionMap.values(), (e -> e.hasUniqueIdentifierMessage(uniqueIdentifier)));
					if (sessionList.isEmpty()) {
						List<FixTestReplacementsMessage> deferrals = this.deferredReplacements.computeIfAbsent(uniqueIdentifier, key -> new ArrayList<>());
						deferrals.add(fixTestReplacementsMessage);
					}
					else {
						sessionList.forEach(s -> s.putReplacementsFields(uniqueIdentifier, fixTestReplacementsMessage));
					}
					this.getFixTestMessageConduit().sendReceivedReplacementsMessage();
				}
				catch (InterruptedException e) {
					// swallow
				}
			}
		});
	}


	public void shutdown() {
		this.executorService.shutdownNow();
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public FixTestSession<M, F, E, P> getFixTestSession(String testIdentifier) {
		return this.fixTestSessionMap.get(testIdentifier);
	}


	public boolean hasSession(String testIdentifier) {
		return this.fixTestSessionMap.containsKey(testIdentifier);
	}


	public String getTestIdentifier(M fixMessage, BaseFixMessagingMessage message) {
		return getFixTestIdentifierProvider().getTestIdentifier(fixMessage, message);
	}


	public void setupNewTestSession(M message, BaseFixMessagingMessage fixMessagingMessage, String testIdentifier) {
		List<M> responses = getFixServerResponseCollection().getTestCaseMessages(message);
		Set<String> uniqueIdentifierList = responses.stream()
				.map(m -> getFixTestIdentifierProvider().getMessageIdentifier(m))
				.filter(Objects::nonNull)
				.collect(Collectors.toSet());
		String allocationIdentifier = getFixTestIdentifierProvider().getAllocationSecondaryIdentifier(responses);
		ValidationUtils.assertNotEmpty(responses, () -> "Could not load responses from data file '" + getFixServerResponseCollection().getFixFileNameProvider().getFixFileName(message) + "'");
		FixMessageDirectionProvider<M> fixMessageDirectionProvider = getFixMessageDirectionProviderFactory().getFixMessageDirectionProvider(message);
		FixMessageEquivalency<M, F> fixMessageEquivalency = getFixMessageEquivalencyProvider().getFixMessageEquivalency();
		FixServerResponseState<M, F> fixServerResponseState = new FixServerResponseState<>(fixMessageEquivalency);
		fixServerResponseState.started(responses, fixMessageDirectionProvider);
		S sessionId = getFixSessionIdProvider().getFixSessionId(message, fixMessagingMessage.getSessionQualifier());
		FixMessageSequenceProvider fixMessageSequenceProvider = getFixMessageSequenceProviderFactory().getFixMessageSequenceProvider(sessionId);
		FixTestSession<M, F, E, P> fixTestSession = new FixTestSession<>(testIdentifier, fixServerResponseState, fixMessageSequenceProvider, getFixMessageTransformerFactory(), this.fixTestMessageConduit, uniqueIdentifierList);
		checkDeferrals(fixTestSession);
		if (!StringUtils.isEmpty(allocationIdentifier)) {
			this.fixTestSessionMap.put(allocationIdentifier, fixTestSession);
		}
		this.fixTestSessionMap.put(testIdentifier, fixTestSession);
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void checkDeferrals(FixTestSession<M, F, E, P> fixTestSession) {
		for (Map.Entry<String, List<FixTestReplacementsMessage>> stringListEntry : this.deferredReplacements.entrySet()) {
			if (fixTestSession.hasUniqueIdentifierMessage(stringListEntry.getKey())) {
				for (FixTestReplacementsMessage fixTestReplacementsMessage : stringListEntry.getValue()) {
					fixTestSession.putReplacementsFields(stringListEntry.getKey(), fixTestReplacementsMessage);
				}
				this.deferredReplacements.remove(stringListEntry.getKey());
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public FixServerResponseCollection<M> getFixServerResponseCollection() {
		return this.fixServerResponseCollection;
	}


	public void setFixServerResponseCollection(FixServerResponseCollection<M> fixServerResponseCollection) {
		this.fixServerResponseCollection = fixServerResponseCollection;
	}


	public FixTestIdentifierProvider<M> getFixTestIdentifierProvider() {
		return this.fixTestIdentifierProvider;
	}


	public void setFixTestIdentifierProvider(FixTestIdentifierProvider<M> fixTestIdentifierProvider) {
		this.fixTestIdentifierProvider = fixTestIdentifierProvider;
	}


	public FixMessageDirectionProviderFactory<M> getFixMessageDirectionProviderFactory() {
		return this.fixMessageDirectionProviderFactory;
	}


	public void setFixMessageDirectionProviderFactory(FixMessageDirectionProviderFactory<M> fixMessageDirectionProviderFactory) {
		this.fixMessageDirectionProviderFactory = fixMessageDirectionProviderFactory;
	}


	public FixMessageEquivalencyProvider<M, F> getFixMessageEquivalencyProvider() {
		return this.fixMessageEquivalencyProvider;
	}


	public void setFixMessageEquivalencyProvider(FixMessageEquivalencyProvider<M, F> fixMessageEquivalencyProvider) {
		this.fixMessageEquivalencyProvider = fixMessageEquivalencyProvider;
	}


	public FixSessionIdProvider<S, M> getFixSessionIdProvider() {
		return this.fixSessionIdProvider;
	}


	public void setFixSessionIdProvider(FixSessionIdProvider<S, M> fixSessionIdProvider) {
		this.fixSessionIdProvider = fixSessionIdProvider;
	}


	public FixMessageSequenceProviderFactory<S> getFixMessageSequenceProviderFactory() {
		return this.fixMessageSequenceProviderFactory;
	}


	public void setFixMessageSequenceProviderFactory(FixMessageSequenceProviderFactory<S> fixMessageSequenceProviderFactory) {
		this.fixMessageSequenceProviderFactory = fixMessageSequenceProviderFactory;
	}


	public FixMessageTransformerFactory<M> getFixMessageTransformerFactory() {
		return this.fixMessageTransformerFactory;
	}


	public void setFixMessageTransformerFactory(FixMessageTransformerFactory<M> fixMessageTransformerFactory) {
		this.fixMessageTransformerFactory = fixMessageTransformerFactory;
	}


	public FixTestMessageConduit<E, P> getFixTestMessageConduit() {
		return this.fixTestMessageConduit;
	}


	public void setFixTestMessageConduit(FixTestMessageConduit<E, P> fixTestMessageConduit) {
		this.fixTestMessageConduit = fixTestMessageConduit;
	}
}
