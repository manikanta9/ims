package com.clifton.fix.server.message;

import java.util.Map;
import java.util.concurrent.BlockingQueue;


/**
 * Relays messages between the Fix Server Thread and the JUnit test runner thread.
 * This allows the JUnit test to pause and wait for the server thread to return a
 * response.
 */
public interface FixTestMessageConduit<E, P> {

	public void setMessageQueue(BlockingQueue<E> messageQueue);


	public void setReplacementsQueue(BlockingQueue<FixTestReplacementsMessage> replacementQueue);


	public void sendFailedMessage(String message);


	public void sendIntermediateMessage(String message);


	public void sendSuccessMessage(String message);


	public void sendWaitingMessage(String message);


	public void sendReceivedReplacementsMessage();


	public void sendReplacements(String uniqueIdentifier, Map<String, String> replacements);
}
