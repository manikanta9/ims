package com.clifton.fix.server.message;

/**
 * The set of states that can be communicated between the Junit
 * test and the Mock Fix Server.  These are included with
 * {@link FixTestMessage}
 */
public enum FixTestMessageStatus {
	FAILED,
	SUCCESS,
	WAITING_FOR_REQUEST,
	INTERMEDIATE_RESULT,
	FIELD_VALUE_MAPPINGS
}
