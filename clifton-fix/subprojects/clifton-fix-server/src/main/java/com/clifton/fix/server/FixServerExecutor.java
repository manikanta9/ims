package com.clifton.fix.server;


import com.clifton.fix.session.FixSessionParameters;

import java.util.List;


/**
 * The <code>FixServerExecutor</code> defines an object used to manage FIX sessions.  An
 * instance of an implementation will be in the spring context, and that instance should
 * start all the FIX sessions.
 * <p>
 * <bean id="fixExecutor" class="com.clifton.fix.quickfix.server.QuickFixExecutor" destroy-method="stop" init-method="init"/>
 * <p>
 * If the implementation uses @PreDestroy on the close method then use this:
 * <bean id="fixExecutor" class="com.clifton.fix.quickfix.server.QuickFixExecutor" destroy-method="stop" init-method="init"/>
 *
 * @author mwacker
 */
public interface FixServerExecutor {


	/**
	 * Returns true if the session is currently running
	 */
	public boolean isRunning(FixSessionParameters sessionParameters);


	/**
	 * Restart all session.
	 */
	public void restart();


	/**
	 * Restart a single session.
	 */
	public void restart(FixSessionParameters sessionParameters);


	/**
	 * Start all FIX sessions.
	 */
	public void start();


	/**
	 * Start a single FIX sessions.
	 */
	public void start(FixSessionParameters sessionParameters);


	/**
	 * Stop all FIX sessions.
	 */
	public void stop();


	/**
	 * Stop a single FIX sessions. If force is set to true, then the initiator will be forcibly stopped if any errors occur.
	 */
	public void stop(FixSessionParameters sessionParameters, boolean force);


	/**
	 * Stop a given list of FIX sessions. If force is set to true, then the initiator will be forcibly stopped if any errors occur.
	 */
	public void stopSessionList(List<String> sessionIdList, boolean force);


	/**
	 * Used to initialize the object.
	 */
	public void init();
}
