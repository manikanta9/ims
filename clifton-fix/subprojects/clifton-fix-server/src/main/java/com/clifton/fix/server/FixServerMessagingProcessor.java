package com.clifton.fix.server;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.destination.FixDestinationService;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.identifier.FixIdentifierHandler;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageProcessHandler;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityService;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.setup.FixSetupService;
import com.clifton.fix.setup.FixSourceSystem;
import com.clifton.fix.tag.FixTagModifierHandler;
import com.clifton.fix.util.FixMessageHandler;
import com.clifton.fix.util.FixUtilService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * The <code>FixServerAsynchronousProcessor</code> server side AsynchronousMessage processor for FIX.
 *
 * @author mwacker
 */
@SuppressWarnings("rawtypes")
public class FixServerMessagingProcessor implements AsynchronousProcessor<AsynchronousMessage> {

	private FixServerMessageRouter fixServerMessageRouter;
	private FixUtilService fixUtilService;
	private FixTagModifierHandler fixTagModifierHandler;
	private FixIdentifierHandler fixIdentifierHandler;
	private FixMessageHandler fixMessageHandler;

	private FixMessageEntryService fixMessageEntryService;
	private FixMessageProcessHandler<FixMessageEntry> fixMessageProcessHandler;

	private FixDestinationService fixDestinationService;
	private FixSetupService fixSetupService;
	private FixEntityService fixEntityService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(AsynchronousMessage msg, AsynchronousMessageHandler handler) {
		if (msg instanceof BaseFixMessagingMessage) {
			handleFixMessagingMessage((BaseFixMessagingMessage) msg, handler);
		}
	}


	@SuppressWarnings("unchecked")
	protected void handleFixMessagingMessage(BaseFixMessagingMessage message, AsynchronousMessageHandler handler) {
		if (getFixMessageRouter() == null) {
			throw new RuntimeException("No message router is defined.");
		}

		String messageText = getFixMessageHandler().getBaseFixMessagingMessageText(message);
		Object fixMessage = getFixUtilService().parseMessage(messageText);

		// The entire method cannot be wrapped in a transaction.
		// This specifically needs to be applied in its own transaction before sending the message out.  Otherwise, we can get a response before the transaction is committed and end up with a stale cache for the FixIdentifier which causes an attempt to create a duplicate which fails at the db level
		FixIdentifier identifier = handleFixMessagingMessageImpl(message, messageText, fixMessage);

		getFixMessageRouter().send(fixMessage, message.getSessionQualifier());

		// send the identifier message
		if (identifier != null && identifier.isNewlyCreated()) {
			FixSourceSystem sourceSystem = getFixSetupService().getFixSourceSystemByTag(message.getSourceSystemTag());
			identifier.setNewlyCreated(false);
			BaseFixMessagingMessage resultMessage;
			if (sourceSystem != null && sourceSystem.isSerializeEntity()) {
				FixEntityMessagingMessage entityMessage = new FixEntityMessagingMessage();
				FixEntity fixEntity = getFixEntityService().getFixEntityForFixMessageText(messageText, message.getSourceSystemTag());
				entityMessage.setFixEntity(fixEntity);
				resultMessage = entityMessage;
			}
			else {
				FixMessageTextMessagingMessage idMessage = new FixMessageTextMessagingMessage();
				idMessage.setMessageText(fixMessage.toString());
				resultMessage = idMessage;
			}
			BeanUtils.copyProperties(message, resultMessage, new String[]{"machineName", "messageDelaySeconds", "fixEntity", "messageText"});
			resultMessage.setIdentifier(identifier);
			resultMessage.getProperties().put("sourceSystemTag", message.getSourceSystemTag());
			handler.send(resultMessage);
		}
	}


	@SuppressWarnings("unchecked")
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected FixIdentifier handleFixMessagingMessageImpl(BaseFixMessagingMessage message, String messageText, Object fixMessage) {
		//If a destination name is present, get the session info from the associated session definition
		if (!StringUtils.isEmpty(message.getDestinationName())) {
			FixDestination destination = getFixDestinationService().getFixDestinationByName(message.getDestinationName());
			ValidationUtils.assertNotNull(destination, "The destination name " + message.getDestinationName() + " does not exist.");
			FixSessionDefinition definition = destination.getSessionDefinition();
			message.setSessionQualifier(definition.getSessionQualifier());
			getFixUtilService().setMessageInfoFromSessionDefinition(fixMessage, definition);
		}
		getFixTagModifierHandler().applyTagModifiers(fixMessage, message.getSessionQualifier(), (message instanceof FixEntityMessagingMessage) ? ((FixEntityMessagingMessage) message).getFixEntity().getAdditionalParams() : null);
		// get the new identifier if needed
		return getFixIdentifierHandler().applyFixIdentifier(fixMessage, message, false, null, messageText);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixServerMessageRouter getFixMessageRouter() {
		return this.fixServerMessageRouter;
	}


	public void setFixMessageRouter(FixServerMessageRouter messageRouter) {
		this.fixServerMessageRouter = messageRouter;
	}


	public FixTagModifierHandler getFixTagModifierHandler() {
		return this.fixTagModifierHandler;
	}


	public void setFixTagModifierHandler(FixTagModifierHandler fixTagModifierHandler) {
		this.fixTagModifierHandler = fixTagModifierHandler;
	}


	public FixMessageHandler getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixUtilService getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService fixUtilService) {
		this.fixUtilService = fixUtilService;
	}


	public FixIdentifierHandler getFixIdentifierHandler() {
		return this.fixIdentifierHandler;
	}


	public void setFixIdentifierHandler(FixIdentifierHandler fixIdentifierHandler) {
		this.fixIdentifierHandler = fixIdentifierHandler;
	}


	public FixMessageProcessHandler<FixMessageEntry> getFixMessageProcessHandler() {
		return this.fixMessageProcessHandler;
	}


	public void setFixMessageProcessHandler(FixMessageProcessHandler<FixMessageEntry> fixMessageProcessHandler) {
		this.fixMessageProcessHandler = fixMessageProcessHandler;
	}


	public FixDestinationService getFixDestinationService() {
		return this.fixDestinationService;
	}


	public void setFixDestinationService(FixDestinationService fixDestinationService) {
		this.fixDestinationService = fixDestinationService;
	}


	public FixSetupService getFixSetupService() {
		return this.fixSetupService;
	}


	public void setFixSetupService(FixSetupService fixSetupService) {
		this.fixSetupService = fixSetupService;
	}


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}
}
