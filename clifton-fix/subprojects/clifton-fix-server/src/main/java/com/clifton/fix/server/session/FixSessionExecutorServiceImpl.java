package com.clifton.fix.server.session;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.fix.server.FixServerExecutor;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionParameters;
import com.clifton.fix.session.FixSessionParametersImpl;
import com.clifton.fix.session.FixSessionService;
import org.springframework.stereotype.Service;


/**
 * @author manderson
 */
@Service
public class FixSessionExecutorServiceImpl implements FixSessionExecutorService {

	private FixSessionService fixSessionService;

	private FixServerExecutor fixServerExecutor;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void stopFixSession(Short fixSessionId, boolean ignoreValidation) {
		if (fixSessionId == null) {
			getFixServerExecutor().stop();
		}
		else {
			getFixServerExecutor().stop(getFixSessionParametersForSession(fixSessionId), ignoreValidation);
		}
	}


	@Override
	public void stopFixSessionList(String[] sessionIds, boolean force) {
		getFixServerExecutor().stopSessionList(CollectionUtils.createList(sessionIds), force);
	}


	@Override
	public void startFixSession(Short fixSessionId) {
		if (fixSessionId == null) {
			getFixServerExecutor().start();
		}
		else {
			getFixServerExecutor().start(getFixSessionParametersForSession(fixSessionId));
		}
	}


	@Override
	public void restartFixSession(Short fixSessionId) {
		if (fixSessionId == null) {
			getFixServerExecutor().restart();
		}
		else {
			getFixServerExecutor().restart(getFixSessionParametersForSession(fixSessionId));
		}
	}


	private FixSessionParameters getFixSessionParametersForSession(short fixSessionId) {
		FixSession session = getFixSessionService().getFixSession(fixSessionId);
		AssertUtils.assertNotNull(session, "Could not find FIX session for id [" + session.getId() + "].");
		return new FixSessionParametersImpl(session.getDefinition());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Ability to manually override the sequence numbers on the session
	 * If the session is running: Will first stop the session
	 * Then will update the sequence numbers on the FixSession
	 * Then, if originally running will start the session back up
	 */
	@Override
	public void resetFixSessionSequenceNumbersManual(Short fixSessionId, Integer incomingSequenceNumber, Integer outgoingSequenceNumber) {
		FixSessionParameters sessionParameters = getFixSessionParametersForSession(fixSessionId);
		boolean running = getFixServerExecutor().isRunning(sessionParameters);
		if (running) {
			getFixServerExecutor().stop(sessionParameters, false);
		}
		FixSession fixSession = getFixSessionService().getFixSessionPopulated(fixSessionId);
		getFixSessionService().storeFixSessionSequenceNumbers(fixSession, ObjectUtils.coalesce(incomingSequenceNumber, fixSession.getLiveIncomingSequenceNumber(), fixSession.getIncomingSequenceNumber()), ObjectUtils.coalesce(outgoingSequenceNumber, fixSession.getLiveOutgoingSequenceNumber(), fixSession.getOutgoingSequenceNumber()));
		if (running) {
			getFixServerExecutor().start(sessionParameters);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixServerExecutor getFixServerExecutor() {
		return this.fixServerExecutor;
	}


	public void setFixServerExecutor(FixServerExecutor fixServerExecutor) {
		this.fixServerExecutor = fixServerExecutor;
	}
}
