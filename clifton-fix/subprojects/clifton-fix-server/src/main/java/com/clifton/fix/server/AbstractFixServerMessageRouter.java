package com.clifton.fix.server;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSender;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.identifier.FixIdentifierHandler;
import com.clifton.fix.message.validation.FixMessageValidationHandler;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityService;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.setup.FixSetupService;
import com.clifton.fix.setup.FixSourceSystem;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>AbstractMessageRouter</code> defines the basic methods for routing FIX messages
 * to and from the FIX session.
 *
 * @author mwacker
 */
public abstract class AbstractFixServerMessageRouter<T> implements FixServerMessageRouter<T> {


	/**
	 * Set up by the {@link com.clifton.fix.server.messaging.FixMessagingQueueRegistratorImpl} as queue message containers are registered
	 * this map gets populated
	 */
	private final Map<String, MessageSender> sourceSystemMessageSenderMap = new ConcurrentHashMap<>();
	private FixIdentifierHandler<T> fixIdentifierHandler;
	private FixMessageValidationHandler<T> fixMessageValidationHandler;

	/**
	 * Service to convert Fix message into Fix Entity Object {@link com.clifton.fix.order.FixEntityServiceImpl}
	 */
	private FixEntityService fixEntityService;
	/**
	 * Service to extract {@link com.clifton.fix.setup.FixSourceSystem} using {@link com.clifton.fix.setup.FixSetupServiceImpl}
	 */
	private FixSetupService fixSetupService;
	private Map<String, MessageSender> sourceSystemMessageSenderOverrideMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addSourceSystemMessageSender(String sourceSystemTag, MessageSender messageSender) {
		this.sourceSystemMessageSenderMap.put(sourceSystemTag, messageSender);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void receiveFromSession(T message, FixSession session) {
		// Guard-clause: No-op for informational sessions
		if (session.getDefinition().isInformational()) {
			return;
		}

		// Set up message data
		FixMessageTextMessagingMessage fixTextMessage = new FixMessageTextMessagingMessage();
		String messageText = message.toString();
		fixTextMessage.setMessageText(messageText);
		FixIdentifier fixIdentifier = getFixIdentifierHandler().applyFixIdentifier(message, fixTextMessage, true, session);

		// Determine source system
		final String sourceSystemTag;
		if (fixIdentifier != null && !StringUtils.isEmpty(fixIdentifier.getSourceSystemTag())) {
			sourceSystemTag = fixIdentifier.getSourceSystemTag();
		}
		else if (!StringUtils.isEmpty(session.getDefinition().getUnknownMessageSystemTag())) {
			sourceSystemTag = session.getDefinition().getUnknownMessageSystemTag();
		}
		else {
			sourceSystemTag = null;
		}
		FixSourceSystem sourceSystem = getFixSetupService().getFixSourceSystemByTag(sourceSystemTag);

		// Generate message
		final AsynchronousMessage resultMessage;
		List<FixValidationResult> validationResults = getFixMessageValidationHandler().validate(message, fixIdentifier);
		if (sourceSystem != null && sourceSystem.isSerializeEntity()) {
			FixEntity fixEntity = getFixEntityService().getFixEntityForFixMessageText(messageText, sourceSystemTag);
			FixEntityMessagingMessage entityMessage = new FixEntityMessagingMessage();
			entityMessage.setFixEntity(fixEntity);
			if (fixIdentifier != null) {
				fixEntity.setIdentifier(fixIdentifier);
				entityMessage.setSourceSystemFkFieldId(fixIdentifier.getSourceSystemFkFieldId());
			}
			fixEntity.setAllocationValidationError(FixValidationResult.isAllocationValidationError(validationResults));
			fixEntity.setValidationError(FixValidationResult.isValidationError(validationResults));
			resultMessage = entityMessage;
		}
		else {
			fixTextMessage.setIdentifier(fixIdentifier);
			fixTextMessage.setAllocationValidationError(FixValidationResult.isAllocationValidationError(validationResults));
			fixTextMessage.setValidationError(FixValidationResult.isValidationError(validationResults));
			resultMessage = fixTextMessage;
		}
		if (sourceSystem != null) {
			resultMessage.getProperties().put("sourceSystemTag", sourceSystemTag);
			resultMessage.getProperties().put(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME, sourceSystem.getMessageFormat().getContentType());
		}

		MessageSender messageSender = getMessageSender(session, sourceSystemTag);
		if (messageSender == null) {
			LogUtils.warn(getClass(), String.format("No message sender found for session [%s] and source system tag [%s]. The message will not be forwarded to a source system. Please set the \"Unknown Message Source System\" field on the session definition.", session.getDefinition().getName(), sourceSystemTag));
		}
		else {
			messageSender.send(resultMessage);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Get the JMS message handler be the queue defined be the FixSessionDefinition.  If there isn't one, return the default.
	 */
	protected MessageSender getMessageSender(FixSession session, String sourceSystemTag) {
		// TODO MESSAGE QUEUE ON THE FIX SESSION DEFINITION ISN'T USED - ISN'T EVEN VISIBLE VIA UI - REMOVE IT? HOW CAN WE USE IT?
		if (!StringUtils.isEmpty(session.getDefinition().getMessageQueue()) && (getSourceSystemMessageSenderOverrideMap() != null)
				&& getSourceSystemMessageSenderOverrideMap().containsKey(session.getDefinition().getMessageQueue())) {
			return getSourceSystemMessageSenderOverrideMap().get(session.getDefinition().getMessageQueue());
		}

		return sourceSystemTag != null
				? this.sourceSystemMessageSenderMap.get(sourceSystemTag)
				: null;
	}



	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixIdentifierHandler<T> getFixIdentifierHandler() {
		return this.fixIdentifierHandler;
	}


	public void setFixIdentifierHandler(FixIdentifierHandler<T> fixIdentifierHandler) {
		this.fixIdentifierHandler = fixIdentifierHandler;
	}


	public FixMessageValidationHandler<T> getFixMessageValidationHandler() {
		return this.fixMessageValidationHandler;
	}


	public void setFixMessageValidationHandler(FixMessageValidationHandler<T> fixMessageValidationHandler) {
		this.fixMessageValidationHandler = fixMessageValidationHandler;
	}


	public FixSetupService getFixSetupService() {
		return this.fixSetupService;
	}


	public void setFixSetupService(FixSetupService fixSetupService) {
		this.fixSetupService = fixSetupService;
	}


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}


	public Map<String, MessageSender> getSourceSystemMessageSenderMap() {
		return this.sourceSystemMessageSenderMap;
	}


	public Map<String, MessageSender> getSourceSystemMessageSenderOverrideMap() {
		return this.sourceSystemMessageSenderOverrideMap;
	}


	public void setSourceSystemMessageSenderOverrideMap(Map<String, MessageSender> sourceSystemMessageSenderOverrideMap) {
		this.sourceSystemMessageSenderOverrideMap = sourceSystemMessageSenderOverrideMap;
	}
}
