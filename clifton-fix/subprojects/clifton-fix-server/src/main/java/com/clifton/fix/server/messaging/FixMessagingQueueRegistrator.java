package com.clifton.fix.server.messaging;

import com.clifton.fix.setup.FixSourceSystem;


/**
 * Creates / register the queues during start up based on what is defined in the {@link FixSourceSystem} list
 *
 * @author manderson
 */
public interface FixMessagingQueueRegistrator {

	public void registerFixSourceSystemQueues(FixSourceSystem sourceSystem);
}
