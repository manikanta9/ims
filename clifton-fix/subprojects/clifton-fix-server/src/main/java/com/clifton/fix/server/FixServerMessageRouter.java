package com.clifton.fix.server;


import com.clifton.core.messaging.jms.asynchronous.sender.MessageSender;
import com.clifton.fix.session.FixSession;


/**
 * The <code>FixServerMessageRouter</code> defines an object the will route messages to and from the FIX sessions.
 *
 * @param <T>
 * @author mwacker
 */
public interface FixServerMessageRouter<T> {


	/**
	 * Adds the message sender to the map for easy retrieval during processing
	 */
	public void addSourceSystemMessageSender(String sourceSystemTag, MessageSender messageSender);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Send a message via FIX.
	 *
	 * @param message
	 */
	public void send(T message, String sessionQualifier);


	/**
	 * Receive a message via FIX.
	 *
	 * @param message
	 */
	public void receiveFromSession(T message, FixSession session);

}
