package com.clifton.fix.server.messaging;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandlerImpl;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSender;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSenderContext;
import com.clifton.core.messaging.jms.asynchronous.sender.MessageSenderProvider;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.messaging.xml.FixMessagingXmlXStreamConfigurer;
import com.clifton.fix.server.FixServerMessageRouter;
import com.clifton.fix.server.FixServerMessagingProcessor;
import com.clifton.fix.setup.FixSetupService;
import com.clifton.fix.setup.FixSourceSystem;
import com.clifton.fix.setup.search.FixSourceSystemSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.ConnectionFactory;
import java.util.Collections;
import java.util.List;


/**
 * @author manderson
 */
@SuppressWarnings("rawtypes")
public class FixMessagingQueueRegistratorImpl implements FixMessagingQueueRegistrator, CurrentContextApplicationListener<ContextRefreshedEvent> {

	private ApplicationContext applicationContext;

	private FixSetupService fixSetupService;

	private FixServerMessageRouter fixMessageRouter;

	private ConnectionFactory fixConnectionFactory;
	private JmsTemplate fixJmsTemplate;
	private FixServerMessagingProcessor fixServerMessagingProcessor;
	private MessageConverter jmsMessageConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String fixJmsUrl;
	private String fixErrorQueueName;
	private String fixMessageGroup;
	private Integer fixQueuePrefetchSize;
	private MessageTypes fixDefaultMessageType;
	/**
	 * If true, system statistics will be recorded for {@link AsynchronousMessageHandlerImpl#processMessage}
	 */
	private boolean recordJmsFixSystemStats;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			// get the list of Fix Source Systems
			List<FixSourceSystem> sourceSystemList = getFixSetupService().getFixSourceSystemList(new FixSourceSystemSearchForm());
			for (FixSourceSystem sourceSystem : CollectionUtils.getIterable(sourceSystemList)) {
				try {
					registerFixSourceSystemQueues(sourceSystem);
				}
				catch (Exception e) {
					// can't throw errors during application startup as the application won't start
					LogUtils.error(getClass(), "Error registering FIX Message Queues for Source System: " + sourceSystem.getName() + ": " + event, e);
				}
			}
		}
		catch (Exception e) {
			// can't throw errors during application startup as the application won't start
			LogUtils.error(getClass(), "Error registering FIX Message Queues: " + event, e);
		}
	}


	@Override
	public void registerFixSourceSystemQueues(FixSourceSystem sourceSystem) {
		if (!StringUtils.isEmpty(sourceSystem.getRequestQueueName())) {
			// Generate message sender
			MessageSenderProvider messageSenderProvider = ((MessageSenderProvider) getSystemBeanService().getBeanInstance(sourceSystem.getMessageSenderFactoryProviderBean()));
			MessageSender messageSender = messageSenderProvider.getMessageSender(getMessageSenderContext(sourceSystem));
			getFixMessageRouter().addSourceSystemMessageSender(sourceSystem.getSystemTag(), messageSender);

			// Generate message handler (for message receipt)
			AsynchronousMessageHandlerImpl messageHandler = new AsynchronousMessageHandlerImpl();
			messageHandler.setJmsTemplate(getFixJmsTemplate());
			messageHandler.setOutgoingQueue(sourceSystem.getResponseQueueName());
			messageHandler.setErrorQueue(getFixErrorQueueName());
			messageHandler.setBrokerUrl(getFixJmsUrl());
			messageHandler.setProcessor(getFixServerMessagingProcessor());
			messageHandler.setXmlXStreamConfigurerList(Collections.singletonList(new FixMessagingXmlXStreamConfigurer()));
			messageHandler.setDefaultMessageProperties(MapUtils.of("JMSXGroupID", getFixMessageGroup()));
			messageHandler.setDefaultMessageType(getFixDefaultMessageType());
			if (isRecordJmsFixSystemStats()) {
				messageHandler.setRequestStatsSource("JMS-FIX-" + sourceSystem.getSystemTag());
			}
			getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(messageHandler, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

			ActiveMQQueue requestQueue = new ActiveMQQueue();
			requestQueue.setPhysicalName(sourceSystem.getRequestQueueName() + "?jms.prefetchPolicy.all=" + getFixQueuePrefetchSize());
			getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(requestQueue, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

			DefaultMessageListenerContainer messageContainer = new DefaultMessageListenerContainer();
			messageContainer.setConnectionFactory(getFixConnectionFactory());
			messageContainer.setDestination(requestQueue);
			messageContainer.setMessageListener(messageHandler);
			messageContainer.setMessageSelector("JMSXGroupID = '" + getFixMessageGroup() + "'");
			messageContainer.setSessionTransacted(false);
			getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(messageContainer, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

			// Register the message container
			ConfigurableListableBeanFactory beanFactory = (ConfigurableListableBeanFactory) getApplicationContext().getAutowireCapableBeanFactory();
			beanFactory.registerSingleton("fixJmsContainer_" + sourceSystem.getSystemTag(), messageContainer);
			messageContainer.initialize();
			messageContainer.start();
		}
	}


	private MessageSenderContext getMessageSenderContext(FixSourceSystem sourceSystem) {
		MessageSenderContext messageSenderContext = new MessageSenderContext();
		messageSenderContext.setOutgoingQueue(sourceSystem.getResponseQueueName());
		messageSenderContext.setDefaultMessageProperties(MapUtils.of("JMSXGroupID", getFixMessageGroup()));
		messageSenderContext.setDefaultMessageType(getFixDefaultMessageType());
		return messageSenderContext;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public FixSetupService getFixSetupService() {
		return this.fixSetupService;
	}


	public void setFixSetupService(FixSetupService fixSetupService) {
		this.fixSetupService = fixSetupService;
	}


	public FixServerMessageRouter getFixMessageRouter() {
		return this.fixMessageRouter;
	}


	public void setFixMessageRouter(FixServerMessageRouter fixMessageRouter) {
		this.fixMessageRouter = fixMessageRouter;
	}


	public ConnectionFactory getFixConnectionFactory() {
		return this.fixConnectionFactory;
	}


	public void setFixConnectionFactory(ConnectionFactory fixConnectionFactory) {
		this.fixConnectionFactory = fixConnectionFactory;
	}


	public JmsTemplate getFixJmsTemplate() {
		return this.fixJmsTemplate;
	}


	public void setFixJmsTemplate(JmsTemplate fixJmsTemplate) {
		this.fixJmsTemplate = fixJmsTemplate;
	}


	public FixServerMessagingProcessor getFixServerMessagingProcessor() {
		return this.fixServerMessagingProcessor;
	}


	public void setFixServerMessagingProcessor(FixServerMessagingProcessor fixServerMessagingProcessor) {
		this.fixServerMessagingProcessor = fixServerMessagingProcessor;
	}


	public MessageConverter getJmsMessageConverter() {
		return this.jmsMessageConverter;
	}


	public void setJmsMessageConverter(MessageConverter jmsMessageConverter) {
		this.jmsMessageConverter = jmsMessageConverter;
	}


	public String getFixJmsUrl() {
		return this.fixJmsUrl;
	}


	public void setFixJmsUrl(String fixJmsUrl) {
		this.fixJmsUrl = fixJmsUrl;
	}


	public String getFixErrorQueueName() {
		return this.fixErrorQueueName;
	}


	public void setFixErrorQueueName(String fixErrorQueueName) {
		this.fixErrorQueueName = fixErrorQueueName;
	}


	public String getFixMessageGroup() {
		return this.fixMessageGroup;
	}


	public void setFixMessageGroup(String fixMessageGroup) {
		this.fixMessageGroup = fixMessageGroup;
	}


	public Integer getFixQueuePrefetchSize() {
		return this.fixQueuePrefetchSize;
	}


	public void setFixQueuePrefetchSize(Integer fixQueuePrefetchSize) {
		this.fixQueuePrefetchSize = fixQueuePrefetchSize;
	}


	public MessageTypes getFixDefaultMessageType() {
		return this.fixDefaultMessageType;
	}


	public void setFixDefaultMessageType(MessageTypes fixDefaultMessageType) {
		this.fixDefaultMessageType = fixDefaultMessageType;
	}


	public boolean isRecordJmsFixSystemStats() {
		return this.recordJmsFixSystemStats;
	}


	public void setRecordJmsFixSystemStats(boolean recordJmsFixSystemStats) {
		this.recordJmsFixSystemStats = recordJmsFixSystemStats;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
