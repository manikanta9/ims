package com.clifton.fix.server.message;

import com.clifton.core.util.queue.Queue;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageProcessHandler;
import com.clifton.fix.server.FixServerMessageRouter;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionParametersImpl;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.util.FixUtilService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author manderson
 */
@SuppressWarnings("rawtypes")
@Service
public class FixMessageEntryProcessServiceImpl implements FixMessageEntryProcessService {


	private FixMessageEntryService fixMessageEntryService;

	private FixMessageProcessHandler<FixMessageEntry> fixMessageProcessHandler;

	private FixSessionService fixSessionService;

	private FixServerMessageRouter fixMessageRouter;

	private FixUtilService fixUtilService;

	private Queue<FixMessage> fixFailedMessageQueue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public FixMessage reprocessFixMessageEntry(long entryId) {
		FixMessageEntry entry = getFixMessageEntryService().getFixMessageEntry(entryId);

		// Add the message to queue to be processed.  Only add message entries that are not administrative.
		if (entry.getType().isMessage()) {
			Object message = getFixUtilService().parseMessage(entry.getText());
			entry = reprocessFixMessageEntry(entry, getFixUtilService().getSessionId(message, null));

			getFixMessageRouter().receiveFromSession(message, entry.getSession());
		}
		else {
			// Note: Historically this just always appeared to log a Null Pointer when we convert the log entry to the object message.
			throw new ValidationException("FixMessageEntry " + entryId + " is not a message and cannot be re-processed.");
		}

		return entry.toFixMessage();
	}


	@Override
	public void reprocessFixMessageEntryList(long[] entryIdList) {
		if (entryIdList != null && entryIdList.length > 0) {
			for (long entryId : entryIdList) {
				reprocessFixMessageEntry(entryId);
			}
		}
	}


	@Override
	public void reprocessFixMessageEntryFailedMessageList() {
		FixMessage e = getFixFailedMessageQueue().poll();
		while (e != null) {
			getFixMessageEntryService().createMessageEntry(e.getText(), e.getType().getName(), e.getIncoming(), e.getSession());
			e = getFixFailedMessageQueue().poll();
		}
	}


	/**
	 * Get the FixMessage entry, find the FixSession, update the entry and add it to the processing queue
	 */
	@Transactional
	protected FixMessageEntry reprocessFixMessageEntry(FixMessageEntry entry, Object sessionId) {
		if (entry.getSession() == null) {
			FixSessionParametersImpl parameters = new FixSessionParametersImpl(sessionId);
			parameters.setIncoming(entry.getIncoming());
			FixSessionDefinition def = getFixSessionService().getFixSessionDefinitionByParameters(parameters);
			ValidationUtils.assertNotNull(def, "Cannot find FIX session definition for [" + parameters + "].");
			FixSession session = getFixSessionService().getFixSessionCurrentByDefinition(def);
			ValidationUtils.assertNotNull(session, "No active FIX session for definition [" + def.getId() + "] with session parameters [" + parameters + "].");
			entry.setSession(session);
		}

		return getFixMessageProcessHandler().processMessage(entry);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixMessageProcessHandler<FixMessageEntry> getFixMessageProcessHandler() {
		return this.fixMessageProcessHandler;
	}


	public void setFixMessageProcessHandler(FixMessageProcessHandler<FixMessageEntry> fixMessageProcessHandler) {
		this.fixMessageProcessHandler = fixMessageProcessHandler;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixServerMessageRouter getFixMessageRouter() {
		return this.fixMessageRouter;
	}


	public void setFixMessageRouter(FixServerMessageRouter fixMessageRouter) {
		this.fixMessageRouter = fixMessageRouter;
	}


	public FixUtilService getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService fixUtilService) {
		this.fixUtilService = fixUtilService;
	}


	public Queue<FixMessage> getFixFailedMessageQueue() {
		return this.fixFailedMessageQueue;
	}


	public void setFixFailedMessageQueue(Queue<FixMessage> fixFailedMessageQueue) {
		this.fixFailedMessageQueue = fixFailedMessageQueue;
	}
}
