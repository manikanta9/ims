package com.clifton.fix.server;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.logging.LogUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.annotation.PostConstruct;


/**
 * The {@link FixServerExecutorStartupListener} starts FIX sessions on application start-up.
 * <p>
 * By waiting until application start-up, this ensures that all required application resources are available before FIX sessions are initiated. As an alternative to using a
 * {@link PostConstruct} or {@code init} method, this resolves issues that can occur such as null pointers when referring to Spring beans which have not yet been fully initialized.
 *
 * @author MikeH
 */
public class FixServerExecutorStartupListener implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private ApplicationContext applicationContext;
	private FixServerExecutor fixServerExecutor;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			getFixServerExecutor().init();
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "An error occurred during FIX session initialization.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public FixServerExecutor getFixServerExecutor() {
		return this.fixServerExecutor;
	}


	public void setFixServerExecutor(FixServerExecutor fixServerExecutor) {
		this.fixServerExecutor = fixServerExecutor;
	}
}
