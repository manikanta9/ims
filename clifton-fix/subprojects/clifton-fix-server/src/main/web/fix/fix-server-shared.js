Ext.ns('Clifton.fix.server');


/**
 * Applies colors and tooltips during column rendering for queue size thresholds.
 */
Clifton.fix.server.renderQueueUsage = function(value, metadata, rec) {
	const capacity = rec.queueCapacity ?? 10_000;
	if (value > Math.round(capacity * 0.10)) {
		metadata.css += ' ruleViolationBig';
		metadata.attr += ' ' + TCG.renderQtip(`The queue size exceeds 10% of capacity. Messages may be delayed and system performance may be degraded.`);
	}
	else if (value > Math.round(capacity * 0.05)) {
		metadata.css += ' ruleViolation';
		metadata.attr += ' ' + TCG.renderQtip(`The queue size exceeds 5% of capacity. If the queue size continues to grow, then messages may be delayed and system performance may be degraded.`);
	}
	else if (value > Math.round(capacity * 0.01)) {
		metadata.css += ' minRuleViolationBig';
		metadata.attr += ' ' + TCG.renderQtip(`The queue size exceeds 1% of capacity. Queue size should be monitored to ensure that it does not continue to grow.`);
	}
	return `${value} / ${capacity}`;
};


/*
 * Add FIX statistics information to the stats administrative window. This tab is very similar to the "Request Processor Stats" grid.
 */
Clifton.core.stats.StatsListTabs.push({
	title: 'FIX Session Stats',
	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			layout: 'fit',
			getLoadURL: () => 'fixSessionStatus.json',
			reload: function(params) {
				this.load({
					url: encodeURI(this.getLoadURL()),
					params: params || this.getLoadParams(this.getWindow()),
					waitMsg: 'Loading...',
					success: (form, action) => {
						this.loadJsonResult(action.result, true);
						this.fireEvent('afterload', this);
					},
					...TCG.form.submitDefaults
				});
			},
			tbar: [
				{xtype: 'sectionheaderfield', header: 'FIX Session Stats'},
				'-',
				{text: 'Reload', xtype: 'button', iconCls: 'table-refresh', handler: button => TCG.getParentFormPanel(button).reload()}
			],
			items: [
				{
					xtype: 'tabpanel',
					deferredRender: false, // Force children to be pre-rendered so that they can be properly bound to the form
					items: [
						{
							xtype: 'formgrid',
							title: '<div qtip="The details associated with each running FIX session.">Session Stats</div>',
							hideInstructions: true,
							collapsible: false,
							readOnly: true,
							storeRoot: 'sessionStatusList',
							viewConfig: {markDirty: false},
							autoHeight: false,
							bindToFormLoad: true,
							// Dynamically attach all columns from the attribute map
							useDynamicColumns: true,
							getDynamicColumns: data => data.flatMap(({attributeMap}) => Object.entries(attributeMap))
								.distinct(([fieldName]) => fieldName)
								.map(([fieldName, value]) => [fieldName, `attributeMap.${fieldName}.value`, value.description])
								.map(([fieldName, dataIndex, description]) => ({header: TCG.camelCaseToTitle(fieldName), dataIndex: dataIndex, tooltip: description, hidden: true})),
							loadData: (store, data) => {
								// Append queue size and capacity data to records before loading
								store.loadData({
									...data,
									sessionStatusList: data.sessionStatusList.map(sessionStatus => ({
										...sessionStatus,
										...data['sessionQueueStatusList']
											.filter(queueStatus => queueStatus.sessionId === TCG.getValue('attributeMap.SessionID.value', sessionStatus))
											.reduce(({queueSize = 0, queueCapacity = 0}, {queueSize: curQueueSize, queueCapacity: curQueueCapacity}) => ({
												queueSize: queueSize + curQueueSize,
												queueCapacity: queueCapacity + curQueueCapacity
											}), {})
									}))
								});
							},
							columnsConfig: [
								{header: 'Session ID', width: 300, dataIndex: 'attributeMap.SessionID.value', defaultSortColumn: true},
								{header: 'Connector ID', width: 90, dataIndex: 'attributeMap.ConnectorName.value', type: 'int', convert: value => (/id=(\d+)/.exec(value) || [])[1]},
								{
									header: 'Session Start Time', width: 150, dataIndex: 'attributeMap.StartTime.value', type: 'date', alwaysIncludeTime: true, tooltip: 'The time at which this session was originally enabled.',
									convert: value => /(\d{4})(\d{2})(\d{2})-(\d{2}):(\d{2}):(\d{2}).(\d{3})/.exec(value)
										?.let(([_, year, month, day, hour, minute, second, ms]) => new Date(`${year}-${month}-${day}T${hour}:${minute}:${second}.${ms}Z`))
								},
								{header: 'Next Inc. Seq. Num.', width: 150, dataIndex: 'attributeMap.NextTargetMsgSeqNum.value', type: 'int', tooltip: 'The next expected incoming sequence number.'},
								{header: 'Next Out. Seq. Num.', width: 150, dataIndex: 'attributeMap.NextSenderMsgSeqNum.value', type: 'int', tooltip: 'The next generated outgoing sequence number.'},
								{header: 'Enabled', width: 80, dataIndex: 'attributeMap.Enabled.value', type: 'boolean'},
								{header: 'Logged On', width: 80, dataIndex: 'attributeMap.LoggedOn.value', type: 'boolean'},
								{header: 'Queue Usage', width: 120, dataIndex: 'queueSize', type: 'int', renderer: Clifton.fix.server.renderQueueUsage, tooltip: 'The size and capacity of the queue for inbound messages to be processed. If any queue shows continual growth, then this indicates that the application is unable to process messages as quickly as they are being received and cannot support the existing message volume. Such cases will typically lead to long delays or application failures. Queues typically have a capacity of 10,000 elements and will block once this is reached.'},
								{header: 'Queue Capacity', width: 100, dataIndex: 'queueCapacity', type: 'int', hidden: true}
							]
						},
						{
							xtype: 'formgrid',
							title: '<div qtip="The details associated with each set of session settings. Session settings are typically mapped one-to-one with session instances and describe the application properties which apply to the session. See <code>https://www.quickfixj.org/usermanual/2.1.0/usage/configuration.html</code> for more detailed property descriptions.">Session Settings</div>',
							hideInstructions: true,
							collapsible: false,
							readOnly: true,
							storeRoot: 'settingsStatusList',
							viewConfig: {markDirty: false},
							autoHeight: false,
							bindToFormLoad: true,
							// Dynamically attach all columns from the attribute map
							useDynamicColumns: true,
							getDynamicColumns: data => data.flatMap(({attributeMap}) => Object.entries(attributeMap))
								.distinct(([fieldName]) => fieldName)
								.map(([fieldName, value]) => [fieldName, `attributeMap.${fieldName}.value`, value.description])
								.map(([fieldName, dataIndex, description]) => ({header: fieldName, dataIndex: dataIndex, tooltip: description})),
							sortDynamicColumns: function(columns) {
								// Explicitly sort selected columns
								const columnOrder = ['ConnectionType', 'BeginString', 'SenderCompID', 'TargetCompID', 'DefaultApplVerID', 'StartTime', 'EndTime', 'TimeZone', 'SocketConnectHost', 'SocketConnectPort'];
								return columns.sort((col1, col2) =>
									columnOrder.indexOf(col1.header).let(v => v > -1 ? v : Number.MAX_SAFE_INTEGER) -
									columnOrder.indexOf(col2.header).let(v => v > -1 ? v : Number.MAX_SAFE_INTEGER));
							},
							columnsConfig: [
								{
									header: 'Setting Identifier', width: 300, dataIndex: 'objectName', defaultSortColumn: true,
									renderer: value => value.substring(value.indexOf(':') + 1)
										.split(',')
										.map(kvPair => kvPair.split('='))
										.let(Object.fromEntries)
										.let(props => `${props.beginString}:${props.senderCompID}->${props.targetCompID}` +
											// Add all other properties to string
											Object.entries({...props, beginString: null, senderCompID: null, targetCompID: null, type: null})
												.filter(([_, value]) => value != null)
												.map(([key, value]) => `; ${key} = ${value}`)
												.join('')),
									tooltip: 'The identifier associated with this settings object. This identifier is typically very similar to the corresponding session ID.'
								},
								{header: 'Description', width: 250, dataIndex: 'attributeMap.Description.value'}
							]
						},
						{
							xtype: 'formgrid',
							title: '<div qtip="The details associated with all running connectors. Connectors are categorized as either initiators or acceptors and can host any number of sessions, each of which will have its own queues and connections. Typically, exactly one connector exists per session.">Connector Stats</div>',
							hideInstructions: true,
							collapsible: false,
							readOnly: true,
							storeRoot: 'connectorStatusList',
							viewConfig: {markDirty: false},
							autoHeight: false,
							bindToFormLoad: true,
							// Dynamically attach all columns from the attribute map
							useDynamicColumns: true,
							getDynamicColumns: data => data.flatMap(({attributeMap}) => Object.entries(attributeMap))
								.distinct(([fieldName]) => fieldName)
								.map(([fieldName, value]) => [fieldName, `attributeMap.${fieldName}.value`, value.description])
								.map(([fieldName, dataIndex, description]) => ({header: TCG.camelCaseToTitle(fieldName), dataIndex: dataIndex, tooltip: description, hidden: true})),
							columnsConfig: [
								{
									header: 'Session ID(s)', width: 300, defaultSortColumn: true,
									convert: (_, rec) => (TCG.getValue('attributeMap.Sessions.value', rec) || '')
										.split(/\n\n/)
										.map(individualValue => individualValue
											.split(/\n/)
											.map(el => el.split(/\s*=\s*/))
											.let(Object.fromEntries)['SessionID']
										)
										.map(Ext.util.Format.htmlEncode)
										.join('<br>')
								},
								{header: 'Connector ID', width: 110, dataIndex: 'keyValueMap.id', type: 'int'},
								{
									header: 'Initiator Address(es)', width: 200,
									convert: (_, rec) => (TCG.getValue('attributeMap.InitiatorAddresses.value', rec) || '')
										.split(/\n\n/)
										.map(individualValue => individualValue
											.split(/\n/)
											.map(el => el.split(/\s*=\s*/))
											.let(Object.fromEntries)['InitiatorAddresses']
										)
										.map(Ext.util.Format.htmlEncode)
										.join('<br>')
								},
								{header: 'Host Name', width: 225, dataIndex: 'attributeMap.HostName.value'},
								{header: 'Role', width: 100, dataIndex: 'attributeMap.Role.value', tooltip: 'The connector type. Initiators are clients which connect to a listening host, and acceptors are listeners which wait for client connections.'},
								{header: 'Queue Usage', width: 120, dataIndex: 'attributeMap.QueueSize.value', type: 'int', tooltip: 'The size and capacity of the queue for inbound messages to be processed. If any queue shows continual growth, then this indicates that the application is unable to process messages as quickly as they are being received and cannot support the existing message volume. Such cases will typically lead to long delays or application failures. Queues typically have a capacity of 10,000 elements and will block once this is reached.', renderer: Clifton.fix.server.renderQueueUsage},
								{header: 'Session Details', width: 350, dataIndex: 'attributeMap.Sessions.value', hidden: true, renderer: sessionText => Ext.util.Format.htmlEncode(sessionText || '').replaceAll(/\n/g, '<br>')},
								{header: 'Initiator Address Details', width: 350, dataIndex: 'attributeMap.InitiatorAddresses.value', hidden: true, renderer: initiatorAddressText => Ext.util.Format.htmlEncode(initiatorAddressText || '').replaceAll(/\n/g, '<br>')}
							]
						},
						{
							title: '<div qtip="View the list of session dispatcher threads. These is the full list of active threads used for session dispatchers. Orphaned threads here may be indicative of a memory leak and may occur when a session is not stopped properly.">Session Dispatcher Threads</div>',
							items: [
								{fieldLabel: 'Threads', name: 'sessionDispatcherThreadDump', xtype: 'textarea', readOnly: true}
							]
						}
					]
				}
			]
		}
	]
});


// Add Re-Process Button and Download button
Ext.override(Clifton.fix.FixMessageEntryFailedMessageGrid, {
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Reprocess Messages',
				tooltip: 'Log the failed messages and reprocess them.',
				iconCls: 'run',
				handler: function() {
					const loader = new TCG.data.JsonLoader({
						waitMsg: 'Loading Messages',
						waitTarget: gridPanel,
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('fixMessageEntryFailedMessageListReprocess.json');
				}
			}, '-', {
				xtype: 'fix-download-selected-message-button'
			}, '-');
		}
	}
});


// Add Re-Process Button and Download button
Ext.override(Clifton.fix.FixMessageGrid, {
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		addEditButtons: function(toolbar) {
			toolbar.add({
				text: 'Reprocess Selected',
				tooltip: 'Reprocess selected FIX Message entries.',
				iconCls: 'run',
				scope: this.getGridPanel(),
				handler: function() {
					const gridPanel = this;
					const sm = this.grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one entry to reprocess.', 'No Entries Selected');
					}
					else {
						const entities = sm.getSelections();
						const entryIdList = [];
						for (let i = 0; i < entities.length; i++) {
							entryIdList[i] = entities[i].id;
						}
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Processing Fix Message Entries',
							waitTarget: this,
							params: {
								entryIdList: entryIdList
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('fixMessageEntryReprocessList.json');
					}
				}
			}, '-', {
				xtype: 'fix-download-selected-message-button'
			}, '-', {
				xtype: 'fix-preview-message-button'
			}, '-');
		}
	}
});


// Add Session Action Buttons
Ext.override(Clifton.fix.session.FixSessionGrid, {
	executeSessionAction: function(url, fixSessionId) {
		const params = {};
		if (TCG.isNotBlank(fixSessionId)) {
			params.fixSessionId = fixSessionId;
		}
		const gridPanel = this;
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Executing Session Action',
			waitTarget: this,
			params: params,
			onLoad: function(record, conf) {
				gridPanel.reload();
			}
		});
		loader.load(url);
	},
	editor: {
		detailPageClass: 'Clifton.fix.session.FixSessionWindow',
		drillDownOnly: true,
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Actions',
				iconCls: 'run',
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Restart',
							iconCls: 'run',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Selected',
									tooltip: 'Restarts the selected FIX session.',
									iconCls: 'run',
									scope: gridPanel,
									handler: function() {
										const gridPanel = this;
										const sm = this.grid.getSelectionModel();
										if (sm.getCount() !== 1) {
											TCG.showError('Please select a single session.', 'No Session Selected');
										}
										else {
											const messages = sm.getSelections();
											gridPanel.executeSessionAction('fixRestartSession.json', messages[0].id);
										}
									}
								}, {
									text: 'All',
									tooltip: 'Restarts all FIX sessions.',
									iconCls: 'run',
									scope: gridPanel,
									handler: function() {
										const gridPanel = this;
										gridPanel.executeSessionAction('fixRestartSession.json');
									}
								}]
							})
						},

						{
							text: 'Stop',
							iconCls: 'run',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Selected',
									tooltip: 'Stop the selected FIX session.',
									iconCls: 'run',
									scope: gridPanel,
									handler: function() {
										const gridPanel = this;
										const sm = this.grid.getSelectionModel();
										if (sm.getCount() !== 1) {
											TCG.showError('Please select a single session.', 'No Session Selected');
										}
										else {
											const messages = sm.getSelections();
											gridPanel.executeSessionAction('fixStopSession.json', messages[0].id);
										}
									}
								}, {
									text: 'All',
									tooltip: 'Stops all running FIX sessions.',
									iconCls: 'run',
									scope: gridPanel,
									handler: function() {
										const gridPanel = this;
										gridPanel.executeSessionAction('fixStopSession.json');
									}
								}]
							})
						},

						{
							text: 'Start',
							iconCls: 'run',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Selected',
									tooltip: 'Start the selected FIX session.',
									iconCls: 'run',
									scope: gridPanel,
									handler: function() {
										const gridPanel = this;
										const sm = this.grid.getSelectionModel();
										if (sm.getCount() !== 1) {
											TCG.showError('Please select a single session.', 'No Session Selected');
										}
										else {
											const messages = sm.getSelections();
											gridPanel.executeSessionAction('fixStartSession.json', messages[0].id);
										}
									}
								}, {
									text: 'All',
									tooltip: 'Start all FIX sessions that are not running.',
									iconCls: 'run',
									scope: gridPanel,
									handler: function() {
										const gridPanel = this;
										gridPanel.executeSessionAction('fixStartSession.json');
									}
								}]
							})
						},
						'-',
						{
							text: 'Synchronize Sequence Numbers',
							tooltip: 'Updates the database to match the live sequence numbers',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								const gridPanel = this;
								gridPanel.executeSessionAction('fixSessionSequenceNumbersSave.json');
							}
						},
						{
							text: 'Manually Override Sequence Numbers',
							tooltip: 'Manually override sequence numbers for selected session.  The session will be stopped first if it is running and then started after the change (if it was previously running)',
							iconCls: 'numbers',
							scope: gridPanel,
							handler: function() {
								const gridPanel = this;
								const sm = this.grid.getSelectionModel();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single session.', 'No Session Selected');
								}
								else {
									const messages = sm.getSelections();
									TCG.createComponent('TCG.app.OKCancelWindow', {
										title: 'Manually Update Fix Session Sequence Numbers',
										iconCls: 'numbers',
										height: 250,
										width: 550,
										modal: true,

										items: [{
											xtype: 'formpanel',
											labelWidth: 160,
											loadValidation: false,
											instructions: 'Enter the new Incoming / Outgoing Sequence Numbers.  If the session is currently running it will be stopped first and then started again after the update.  It will not be started if the session was not already running.  Leaving a value blank will NOT overwrite that value.',
											items: [
												{fieldLabel: 'Fix Session', value: messages[0].json.definition.name, doNotSubmit: true, readOnly: true},
												{xtype: 'hidden', name: 'fixSessionId', value: messages[0].id},
												{fieldLabel: 'New Incoming Sequence #', xtype: 'integerfield', name: 'incomingSequenceNumber'},
												{fieldLabel: 'New Outgoing Sequence #', xtype: 'integerfield', name: 'outgoingSequenceNumber'}
											]
										}],
										saveWindow: function(closeOnSuccess, forceSubmit) {
											const w = this;
											const fp = this.getMainFormPanel();
											const iv = fp.getForm().findField('incomingSequenceNumber').getValue();
											const ov = fp.getForm().findField('outgoingSequenceNumber').getValue();

											if (TCG.isBlank(iv) && TCG.isBlank(ov)) {
												TCG.showError('At least a new incoming or outgoing sequence number is required.');
												return;
											}

											const params = {fixSessionId: messages[0].id, incomingSequenceNumber: iv, outgoingSequenceNumber: ov};
											const loader = new TCG.data.JsonLoader({
												waitTarget: fp,
												waitMsg: 'Updating...',
												params: params,
												onLoad: function(record, conf) {
													gridPanel.reload();
													w.closeWindow();
												}
											});
											loader.load('fixResetSessionSequenceNumbersManual.json');
										}
									});
								}
							}
						}
					]
				})
			});
			t.add('-');
		}
	}
});
