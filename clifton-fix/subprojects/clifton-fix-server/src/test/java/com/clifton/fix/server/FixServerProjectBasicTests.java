package com.clifton.fix.server;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class FixServerProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "fix-server";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.annotation.PostConstruct");
		imports.add("org.apache.activemq.command.ActiveMQQueue");
		imports.add("org.springframework.beans.factory.config.");
		imports.add("org.springframework.jms.");
		imports.add("javax.jms.ConnectionFactory");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("com.google.common.base.Charsets");
	}
}
