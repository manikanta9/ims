dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	runtimeOnly("org.apache.mina:mina-core:2.0.13")

	api("org.quickfixj:quickfixj-core:2.2.0-clifton-v6")
	implementation("org.quickfixj:quickfixj-messages-fix44:2.2.0-clifton-v6")
	implementation("org.quickfixj:quickfixj-messages-fix50sp2:2.2.0-clifton-v6")
	implementation("org.quickfixj:quickfixj-messages-fixt11:2.2.0-clifton-v6")


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-fix"))


	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-fix")))

	// Allow downstream projects to use version-specific implementations
	testFixturesApi("org.quickfixj:quickfixj-messages-fix44:2.2.0-clifton-v4")
	testFixturesApi("org.quickfixj:quickfixj-messages-fix50sp2:2.2.0-clifton-v4")
}
