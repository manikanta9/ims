package com.clifton.fix.quickfix.message.converter;

import com.clifton.fix.FixEntityEqualityUtils;
import com.clifton.fix.message.converter.FixMessageConverterService;
import com.clifton.fix.message.converter.FixMessageConverterTestExecutor;
import com.clifton.fix.order.FixDontKnowTrade;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixTestObjectFactory;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.beans.AllocationLinkTypes;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.google.common.collect.Maps;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import quickfix.Message;
import quickfix.field.SecurityType;
import quickfix.fix44.DontKnowTrade;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Map;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class QuickFixMessageConverterServiceTests {

	@Resource
	private FixMessageConverterService<Message> fixMessageConverterService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAllocationMessageParsing() {
		Message message = this.fixMessageConverterService.parse(FixTestObjectFactory.allocationInstructionWithProcessCodeString);
		Assertions.assertTrue(message.isSetField(new SecurityType()));

		// test parsing message without process code
		message = this.fixMessageConverterService.parse(FixTestObjectFactory.allocationInstructionWithOutProcessCodeString);
		Assertions.assertTrue(message.isSetField(new SecurityType()));
	}


	@Test
	public void testExecutionReportFxConnectDealerQuoteToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportFxConnectDealerQuoteString)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReportFxConnectDealerQuote())
				.execute();
	}


	@Test
	public void testExecutionReportCompDealerQuoteToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportDealerQuoteString)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReportDealerQuote())
				.execute();
	}


	@Test
	public void testExecutionReportEmsxDealerQuoteToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportEmsxDealerQuoteString)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReportEmsxDealerQuote())
				.execute();
	}


	@Test
	public void testExecutionReportWithSubPartiesConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportWithSubPartiesString)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReportWithSubParties())
				.execute();
	}


	@Test
	public void testNewOrderAllocationGroupsToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.newOrderWithAllocationGroupsString)
				.withExpectedFixEntity(FixTestObjectFactory.createNewOrderWithAllocationGroups())
				.withExpectedDifference("fixSenderSubId", "test_user_name", null)
				.withExpectedDifference("fixDestinationName", null, "fixDestination1")
				.execute();
	}


	@Test
	public void testNewOrderAllocationGroupsFromFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntity(FixTestObjectFactory.createNewOrderWithAllocationGroups())
				.withExpectedDifference("sequenceNumber", 0, 580)
				.withExpectedDifference("fixSenderSubId", "test_user_name", null)
				.withExpectedDifference("fixDestinationName", null, "fixDestination1")
				.execute();
	}


	@Test
	public void testOrderCancelRequestConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.orderCancelRequestString)
				.withExpectedFixEntity(FixTestObjectFactory.createOrderCancelRequest())
				.withExpectedDifference("fixDestinationName", null, "fixDestination3")
				.execute();
	}


	@Test
	public void testFixOrderCancelRequestConverterFromFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntity(FixTestObjectFactory.createOrderCancelRequest())
				.withExpectedDifference("sequenceNumber", 0, 2500)
				.withExpectedDifference("fixDestinationName", null, "fixDestination3")
				.execute();
	}


	@Test
	public void testOrderCancelReplaceRequestConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.orderCancelReplaceRequestString)
				.withExpectedFixEntity(FixTestObjectFactory.createOrderCancelReplaceRequest())
				.execute();
	}


	@Test
	public void testAllocationInstructionConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.allocationInstructionString)
				.withExpectedFixEntity(FixTestObjectFactory.createAllocationInstruction())
				.withExpectedDifference("fixDestinationName", null, "fixDestination3")
				.execute();
	}


	@Test
	public void testAllocationInstructionFromFixEntity() {
		FixAllocationInstruction allocationInstruction = FixTestObjectFactory.createAllocationInstructionWithExecutionReport();
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntity(allocationInstruction)
				.withExpectedDifference("sequenceNumber", 0, 2096)
				.withExpectedDifference("incoming", false, true)
				.withExpectedDifference("tenor", null, "Tenor")
				.withExpectedDifference("sendSecondaryClientOrderId", false, true)
				.withExpectedDifference("id", null, 1)
				.withExpectedDifference("allocationLinkStringId", null, "AllocationLinkStringId")
				.withExpectedDifference("allocationNoOrdersType", null, AllocationNoOrdersTypes.NOT_SPECIFIED)
				.withExpectedDifference("allocationStatus", null, AllocationStatuses.BLOCK_LEVEL_REJECT)
				.withExpectedDifference("fixSessionQualifier", null, "FixSessionQualifier")
				.withExpectedDifference(
						"fixOrderList",
						Collections.emptyList(),
						FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(allocationInstruction.getFixOrderList())
				)
				.withExpectedDifference("allocationLinkType", null, AllocationLinkTypes.F_X_NETTING)
				.withExpectedDifference("fixDestinationName", null, "fixDestination3")
				.execute();
	}


	@Test
	public void testExecutionReportConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportString)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReport())
				.execute();
	}


	@Test
	public void testExecutionReportConverterFromFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntity(FixTestObjectFactory.createExecutionReport())
				.withExpectedDifference("sequenceNumber", 0, 2096)
				.withExpectedDifference("securityId", "USU7 Comdty", null)
				.execute();
	}


	@Test
	public void testOrderCancelRejectConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.orderCancelRejectString)
				.withExpectedFixEntity(FixTestObjectFactory.createOrderCancelReject())
				.execute();
	}


	@Test
	public void testDontKnowTradeConverterFromFixEntity() {
		FixDontKnowTrade dontKnowTrade = FixTestObjectFactory.createDontKnowTrade();
		DontKnowTrade message = this.fixMessageConverterService.fromFixEntity(dontKnowTrade);

		FixDontKnowTrade fixDontKnowTrade = this.fixMessageConverterService.toFixEntity(message, null);

		Map<String, Object> executionMap = FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(dontKnowTrade, FixAllocationInstruction.class.getPackage().getName(), FixOrder.class.getPackage().getName());
		Map<String, Object> fixExecutionMap = FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(fixDontKnowTrade, FixAllocationInstruction.class.getPackage().getName(), FixOrder.class.getPackage().getName());

		String[] ignoreProperties = new String[]{
				"orderType",
				"concession",
				"executionType",
				"lastShares",
				"orderStatus",
				"CFICode",
				"transactionTime",
				"issuer",
				"orderQuantity",
				"cumulativeQuantity",
				"indexRatio",
				"executionReferenceId",
				"price",
				"yield",
				"lastCapacity",
				"netMoney",
				"currency",
				"text",
				"timeInForceType",
				"sequenceNumber",
				"product",
				"secondaryClientOrderStringId",
				"maturityMonthYear",
				"lastMarket",
				"grossTradeAmount",
				"priceType",
				"securityId",
				"orderCapacity",
				"settlementDate",
				"originalClientOrderStringId",
				"tradeDate",
				"secondaryOrderId",
				"compDealerQuoteList",
				"leavesQuantity",
				"settlementCurrency",
				"couponRate",
				"clientOrderStringId",
				"executionTransactionType",
				"accruedInterestAmount",
				"averagePrice",
				"settlementCurrencyAmount",
				"lastPrice"
		};
		FixEntityEqualityUtils.removeProperties(executionMap, ignoreProperties);
		FixEntityEqualityUtils.removeProperties(fixExecutionMap, ignoreProperties);

		MatcherAssert.assertThat("DIFFERENCES:  " + Maps.difference(executionMap, fixExecutionMap).entriesDiffering().toString(), executionMap, IsEqual.equalTo(fixExecutionMap));
	}


	@Test
	public void testAllocationInstructionAckConverterToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.allocationAcknowledgementString)
				.withExpectedFixEntity(FixTestObjectFactory.createAllocationAcknowledgement())
				.execute();
	}


	@Test
	public void testAllocationReportBloombergToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.allocationReportBloombergString)
				.withExpectedFixEntity(FixTestObjectFactory.createAllocationReportBloomberg())
				.withIgnoredPropertyPath("fixAllocationReportDetailList", "fixAllocationReport")
				.execute();
	}


	@Test
	public void testExecutionReportBloombergToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportBloombergString)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReportBloomberg())
				.execute();
	}


	@Test
	public void testOrderToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.newOrderString)
				.withExpectedFixEntity(FixTestObjectFactory.createNewOrder())
				.withExpectedDifference("senderUserName", null, "mwacker")
				.withExpectedDifference("fixSenderSubId", "mwacker", null)
				.withExpectedDifference("securityId", "ESH3 Index", null)
				.withExpectedDifference("text", "Test 42/nTest 43/nTest", "Test 42//nTest 43//nTest")
				.withExpectedDifference("fixDestinationName", null, "fixDestination1")
				.execute();
	}


	@Test
	public void testOrderFromFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntity(FixTestObjectFactory.createNewOrder2())
				.withExpectedDifference("fixSenderSubId", "test_user_name", null)
				.withExpectedDifference("securityId", "ESH3 Index", null)
				.withExpectedDifference("fixDestinationName", null, "fixDestination2")
				.execute();
	}


	@Test
	public void testIOIToFixEntity() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.newIOIMessage)
				.withExpectedFixEntity(FixTestObjectFactory.createNewFixIOIEntity())
				.execute();
	}

	@Test
	public void testIOIToFixEntity_cancelMessage() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.cancelIOIMessage)
				.withExpectedFixEntity(FixTestObjectFactory.createCancelFixIOIEntity())
				.execute();
	}

	@Test
	public void testMarketDataRefreshToFixEntity() {
		QuickFixMessageUtils.getDataDictionary(FixTestObjectFactory.marketDataRefresh).setCheckUnorderedGroupFields(false);

		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.marketDataRefresh)
				.withExpectedFixEntity(FixTestObjectFactory.createNewMarketDataRefreshEntity())
				.withIgnoredPropertyPath("data", "transactTime")
				.withIgnoredPropertyPath("data", "quantitySize")
				.execute();

		QuickFixMessageUtils.getDataDictionary(FixTestObjectFactory.marketDataRefresh).setCheckUnorderedGroupFields(true);
	}

	@Test
	public void testExecutionReportBloombergToFixEntityWithNotes() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.executionReportBloombergStringWithNotes)
				.withExpectedFixEntity(FixTestObjectFactory.createExecutionReportBloombergWithNotes())
				.execute();
	}


	@Test
	public void testTradeCapture() {
		FixMessageConverterTestExecutor.newTestForFixConverter(this.fixMessageConverterService)
				.startWithFixEntityString(FixTestObjectFactory.tradeCaptureMsg)
				.withExpectedFixEntity(FixTestObjectFactory.createNewFixTCR())
				.execute();
	}

}
