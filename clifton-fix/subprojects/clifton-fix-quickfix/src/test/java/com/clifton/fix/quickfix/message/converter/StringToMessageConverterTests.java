package com.clifton.fix.quickfix.message.converter;


import com.clifton.fix.quickfix.converter.StringToMessageConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import quickfix.FieldNotFound;
import quickfix.IntField;
import quickfix.Message;
import quickfix.field.SecurityType;


public class StringToMessageConverterTests {

	@Test
	public void testConvert() throws FieldNotFound {
		Message msg = (StringToMessageConverter.getInstance()).convert("8=FIX.4.49=19135=D49=CLIFTON50=u74315956=GSFUT57=TKTS1=C032061311=18_2013020411134921=122=A38=25.0040=148=ESH3 Index54=155=ESH3 Index59=060=20130204-18:25:47.11975=20130204167=FUT10=222");
		Assertions.assertEquals("FUT", msg.getField(new SecurityType()).getValue());
	}


	@Test
	public void testConvertTime() throws FieldNotFound {
		for (int i = 0; i < 1400; i++) {
			Message msg = (StringToMessageConverter.getInstance()).convert("8=FIX.4.49=19135=D49=CLIFTON50=u74315956=GSFUT57=TKTS1=C032061311=18_2013020411134921=122=A38=25.0040=148=ESH3 Index54=155=ESH3 Index59=060=20130204-18:25:47.11975=20130204167=FUT10=222");
			Assertions.assertEquals("FUT", msg.getField(new SecurityType()).getValue());
		}
	}


	@Test
	public void testCustomFieldParsing() throws FieldNotFound {
		// This test contains the user-defined field: DayCount(5798)=11
		final String fixTextMessage = "8=FIX.4.49=088035=849=BLP56=CLIFTONPARA34=1034128=ZERO347=UTF-8115=DCSVC_278_452=20170728-20:47:2160=20170728-20:47:20150=F31=100.78125151=0541=2053061632=850000423=164=201707316=100.7812537=VCON:20170728:51600:138=85000039=2159=801.03669=100.78125460=10223=0.0174214=85000015=USD75=2017072817=VCON:20170728:51600:1:12167=MBS48=38378XDB4198=3739:20170728:51600:1228=0.64917678470=US381=556111.222=154=15798=1155=[N/A]236=0.0227210607118=556912.23453=6448=1186447=D452=1448=CHASKAMP:6036836447=D452=11802=3523=14803=4523=CHRIS HASKAMP803=9523=5107156803=4000448=JWHITE19:1486874447=D452=12802=1523=JEFFREY WHITE803=9448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=SXT447=D452=16448=JWHITE19:1486874447=D452=36802=1523=JEFFREY WHITE803=9454=4455=US38378XDB47456=4455=38378XDB4456=1455=!!027A1X456=A455=GNR456=810=021";
		Message msg = (StringToMessageConverter.getInstance()).convert(fixTextMessage);
		Assertions.assertEquals(11, msg.getField(new IntField(5798)).getValue());
	}
}
