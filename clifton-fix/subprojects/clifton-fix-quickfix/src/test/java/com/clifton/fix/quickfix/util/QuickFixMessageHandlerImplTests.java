package com.clifton.fix.quickfix.util;

import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixMessageTextEntity;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixPartySub;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.util.FixMessageHandler;
import com.clifton.security.system.SecuritySystem;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import quickfix.Group;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.NoPartyIDs;
import quickfix.field.PartyID;
import quickfix.field.PartyIDSource;
import quickfix.field.PartyRole;
import quickfix.field.PartySubID;
import quickfix.field.PartySubIDType;
import quickfix.fix44.NewOrderSingle;

import javax.annotation.Resource;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class QuickFixMessageHandlerImplTests {

	@Resource
	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetMessageTextForFixMessageTextMessagingMessage() {
		FixMessageTextMessagingMessage message = new FixMessageTextMessagingMessage();
		message.setMessageText("MESSAGE_TEXT");

		String text = this.fixMessageHandler.getBaseFixMessagingMessageText(message);
		Assertions.assertEquals("MESSAGE_TEXT", text);
	}


	@Test
	public void testGetMessageTextForFixEntityMessagingMessage() {
		FixEntityMessagingMessage message = new FixEntityMessagingMessage();
		FixMessageTextEntity entity = new FixMessageTextEntity();
		entity.setMessageText("MESSAGE_TEXT");
		message.setFixEntity(entity);

		String text = this.fixMessageHandler.getBaseFixMessagingMessageText(message);
		Assertions.assertEquals("MESSAGE_TEXT", text);
	}


	@Test
	public void testGetAliasForUserFixDestinationFromEntity() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		String alias = this.fixMessageHandler.getAliasForUser(entity, null, "mwacker");
		Assertions.assertEquals("test_user_name", alias);
	}


	@Test
	public void testGetAliasForUserFixDestinationFromIdentifier() {
		FixEntity entity = new FixOrder();
		entity.setIdentifier(new FixIdentifier());
		entity.getIdentifier().setFixDestination(new FixDestination());
		entity.getIdentifier().getFixDestination().setSecuritySystem(new SecuritySystem());
		entity.getIdentifier().getFixDestination().getSecuritySystem().setId((short) 1);

		String alias = this.fixMessageHandler.getAliasForUser(entity, null, "lnaylor");
		Assertions.assertEquals("r146876", alias);
	}


	@Test
	public void testGetAliasForUserFixDestinationFromIdentifierLookup() {
		Message message = new Message();
		message.setField(new StringField(35, "TYPE_CODE"));
		message.setField(new StringField(1, "ID"));

		FixEntity entity = new FixOrder();
		entity.setFixBeginString("FIX.4.4");
		entity.setFixSenderCompId("CLIFTONMB44");
		entity.setFixTargetCompId("REDITKTS");

		String alias = this.fixMessageHandler.getAliasForUser(entity, message, "laurenn");
		Assertions.assertEquals("r151412", alias);
	}


	@Test
	public void testGetAliasForUserFixDestinationFromSessionLookup() {
		FixEntity entity = new FixOrder();
		entity.setFixBeginString("FIX.4.4");
		entity.setFixSenderCompId("HTGM_QA_IOI");
		entity.setFixTargetCompId("PARAMETRIC");

		String alias = this.fixMessageHandler.getAliasForUser(entity, null, "fixParty2");
		Assertions.assertEquals("0443HEAD", alias);
	}


	@Test
	public void testGetAliasForUserFixDestinationNotFound() {
		FixEntity entity = new FixOrder();
		entity.setFixBeginString("DUMMY");
		entity.setFixSenderCompId("HTGM_QA_IOI");
		entity.setFixTargetCompId("PARAMETRIC");

		try {
			this.fixMessageHandler.getAliasForUser(entity, null, "fixParty2");
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception e) {
			Assertions.assertEquals("Unable to get FixDestination for FixEntity.", e.getMessage());
		}
	}


	@Test
	public void testGetAliasForUserFixDestinationNoSecuritySystem() {
		FixEntity entity = new FixOrder();
		entity.setIdentifier(new FixIdentifier());
		entity.getIdentifier().setFixDestination(new FixDestination());

		String user = this.fixMessageHandler.getAliasForUser(entity, null, "lnaylor");
		Assertions.assertNull(user);
	}


	@Test
	public void testGetAliasForUserSecurityUserNull() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		try {
			this.fixMessageHandler.getAliasForUser(entity, null, "dummy_name");
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception e) {
			Assertions.assertEquals("Cannot send FIX message because no user exists for senderUserName [dummy_name].", e.getMessage());
		}
	}


	@Test
	public void testGetAliasForUserAliasNull() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination5");

		try {
			this.fixMessageHandler.getAliasForUser(entity, null, "lnaylor");
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception e) {
			Assertions.assertEquals("Cannot send FIX message because no user alias exists for [lnaylor] on [{id=2,label=SecuritySystem2}].", e.getMessage());
		}
	}


	@Test
	public void testGetAliasForUserAliasNullMappingNotRequired() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination4");

		String alias = this.fixMessageHandler.getAliasForUser(entity, null, "unmappedUser");
		Assertions.assertNull(alias);
	}


	@Test
	public void testGetAliasForUserAliasNullAllowAnonymousAccessUseUsername() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination6");

		String alias = this.fixMessageHandler.getAliasForUser(entity, null, "unmappedUser");
		Assertions.assertEquals("unmappedUser", alias);
	}


	@Test
	public void testGetAliasForUserSecurityUserNullAllowAnonymousAccess() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination4");

		String alias = this.fixMessageHandler.getAliasForUser(entity, null, "dummy_name");
		Assertions.assertNull(alias);
	}


	@Test
	public void testGetAliasForUserAliasReadOnly() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination5");

		try {
			this.fixMessageHandler.getAliasForUser(entity, null, "laurenn");
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception e) {
			Assertions.assertEquals("Cannot send FIX message because the trader alias [READ_ONLY] is readonly.", e.getMessage());
		}
	}


	@Test
	public void testGetAliasForUserAliasReadOnlyAllowAnonymousAccess() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination4");

		try {
			this.fixMessageHandler.getAliasForUser(entity, null, "readonly");
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception e) {
			Assertions.assertEquals("Cannot send FIX message because the trader alias [READ_ONLY_MAPPING_NR] is readonly.", e.getMessage());
		}
	}


	@Test
	public void testGetUserForAliasFixDestinationFromEntity() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "test_user_name");
		Assertions.assertEquals("mwacker", user);
	}


	@Test
	public void testGetUserForAliasFixDestinationFromIdentifier() {
		FixEntity entity = new FixOrder();
		entity.setIdentifier(new FixIdentifier());
		entity.getIdentifier().setFixDestination(new FixDestination());
		entity.getIdentifier().getFixDestination().setSecuritySystem(new SecuritySystem());
		entity.getIdentifier().getFixDestination().getSecuritySystem().setId((short) 1);

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "r146876");
		Assertions.assertEquals("lnaylor", user);
	}


	@Test
	public void testGetUserForAliasFixDestinationFromIdentifierLookup() {
		Message message = new Message();
		message.setField(new StringField(35, "TYPE_CODE"));
		message.setField(new StringField(1, "ID"));

		FixEntity entity = new FixOrder();
		entity.setFixBeginString("FIX.4.4");
		entity.setFixSenderCompId("CLIFTONMB44");
		entity.setFixTargetCompId("REDITKTS");

		String user = this.fixMessageHandler.getUserForAlias(entity, message, "r151412");
		Assertions.assertEquals("laurenn", user);
	}


	@Test
	public void testGetUserForAliasFixDestinationFromSessionLookup() {
		FixEntity entity = new FixOrder();
		entity.setFixBeginString("FIX.4.4");
		entity.setFixSenderCompId("HTGM_QA_IOI");
		entity.setFixTargetCompId("PARAMETRIC");

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "0443HEAD");
		Assertions.assertEquals("fixParty2", user);
	}


	@Test
	public void testGetUserForAliasFixDestinationNotFound() {
		FixEntity entity = new FixOrder();
		entity.setFixBeginString("DUMMY");
		entity.setFixSenderCompId("HTGM_QA_IOI");
		entity.setFixTargetCompId("PARAMETRIC");

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "JKENNEFICK:444330");
		Assertions.assertNull(user);
	}


	@Test
	public void testGetUserForAliasFixDestinationNoSecuritySystem() {
		FixEntity entity = new FixOrder();
		entity.setIdentifier(new FixIdentifier());
		entity.getIdentifier().setFixDestination(new FixDestination());

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "r146876");
		Assertions.assertNull(user);
	}


	@Test
	public void testGetUserForAliasSecurityAliasNull() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "dummy_name");
		Assertions.assertNull(user);
	}


	@Test
	public void testGetUserForAliasNoSecuritySystemUserUseUserNameTrue() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination6");

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "dummy_name");
		Assertions.assertEquals(user, "dummy_name");
	}


	@Test
	public void testGetUserForAliasNoSecurityUserUseUserNameTrue() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination6");

		String user = this.fixMessageHandler.getUserForAlias(entity, null, "NO_SECURITY_USER");
		Assertions.assertEquals(user, "NO_SECURITY_USER");
	}


	@Test
	public void testGetFixSenderSubIdForFixEntityIdPresent() {
		FixEntity entity = new FixOrder();
		entity.setFixSenderSubId("SSID");

		String senderSubId = this.fixMessageHandler.getFixSenderSubIdForFixEntity(entity, null, false);
		Assertions.assertEquals("SSID", senderSubId);
	}


	@Test
	public void testGetFixSenderSubIdForFixEntityFixOrder() {
		FixOrder entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");
		entity.setSenderUserName("mwacker");

		String senderSubId = this.fixMessageHandler.getFixSenderSubIdForFixEntity(entity, null, false);
		Assertions.assertEquals("test_user_name", senderSubId);
	}


	@Test
	public void testGetFixSenderSubIdForFixEntityFixOrderCancelRequest() {
		FixOrderCancelRequest entity = new FixOrderCancelRequest();
		entity.setFixDestinationName("fixDestination1");
		entity.setSenderUserName("mwacker");

		String senderSubId = this.fixMessageHandler.getFixSenderSubIdForFixEntity(entity, null, false);
		Assertions.assertEquals("test_user_name", senderSubId);
	}


	@Test
	public void testGetFixSenderSubIdForFixEntityFixAllocationInstruction() {
		FixAllocationInstruction entity = new FixAllocationInstruction();
		entity.setFixDestinationName("fixDestination1");
		entity.setSenderUserName("mwacker");

		String senderSubId = this.fixMessageHandler.getFixSenderSubIdForFixEntity(entity, null, false);
		Assertions.assertEquals("test_user_name", senderSubId);
	}


	@Test
	public void testGetFixSenderSubIdForFixEntityUserNameNullThrowsException() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		try {
			this.fixMessageHandler.getFixSenderSubIdForFixEntity(entity, null, true);
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception e) {
			Assertions.assertEquals("Cannot send FIX message because no user exists for senderUserName [null].", e.getMessage());
		}
	}


	@Test
	public void testGetFixSenderSubIdForFixEntityUserNameNullNoException() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		String senderSubId = this.fixMessageHandler.getFixSenderSubIdForFixEntity(entity, null, false);
		Assertions.assertNull(senderSubId);
	}


	@Test
	public void testGetSenderUserNameForFixEntity() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");
		entity.setFixSenderSubId("test_user_name");

		String user = this.fixMessageHandler.getSenderUserNameForFixEntity(entity, null);
		Assertions.assertNotNull(user);
		Assertions.assertEquals("mwacker", user);
	}


	@Test
	public void testGetSenderUserNameForFixEntitySenderSubIdNull() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		String user = this.fixMessageHandler.getSenderUserNameForFixEntity(entity, null);
		Assertions.assertNull(user);
	}


	@Test
	public void testGetFixPartyListWithGroups() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		Message message = new Message();

		Group party = new NewOrderSingle.NoPartyIDs();
		party.setField(new PartyID("test_user_name"));
		party.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		party.setField(new PartyRole(PartyRole.EXECUTING_FIRM));

		Group subParty = new NewOrderSingle.NoPartyIDs.NoPartySubIDs();
		subParty.setField(new PartySubID("SUB_ID"));
		subParty.setField(new PartySubIDType(PartySubIDType.APPLICATION));

		party.addGroup(subParty);
		message.addGroup(party);

		List<FixParty> fixPartyList = this.fixMessageHandler.getFixPartyList(message, entity);
		Assertions.assertEquals(1, fixPartyList.size());
		FixParty partyResult = fixPartyList.get(0);
		Assertions.assertEquals("test_user_name", partyResult.getPartyId());
		Assertions.assertEquals("mwacker", partyResult.getSenderUserName());
		Assertions.assertEquals(PartyIDSource.PROPRIETARY_CUSTOM_CODE, partyResult.getPartyIdSource().getCode());
		Assertions.assertEquals(PartyRole.EXECUTING_FIRM, partyResult.getPartyRole().getCode());

		Assertions.assertEquals(1, partyResult.getPartySubList().size());
		FixPartySub partySubResult = partyResult.getPartySubList().get(0);
		Assertions.assertEquals("SUB_ID", partySubResult.getPartySubId());
		Assertions.assertEquals(PartySubIDType.APPLICATION, partySubResult.getPartySubIdType().getCode());
	}


	@Test
	public void testGetFixPartyListWithoutGroups() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");

		Message message = new Message();
		message.setField(new NoPartyIDs());
		message.setField(new PartyID("test_user_name"));
		message.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		message.setField(new PartyRole(PartyRole.EXECUTING_FIRM));

		List<FixParty> fixPartyList = this.fixMessageHandler.getFixPartyList(message, entity);
		Assertions.assertEquals(1, fixPartyList.size());
		FixParty partyResult = fixPartyList.get(0);
		Assertions.assertEquals("test_user_name", partyResult.getPartyId());
		Assertions.assertEquals("mwacker", partyResult.getSenderUserName());
		Assertions.assertEquals(PartyIDSource.PROPRIETARY_CUSTOM_CODE, partyResult.getPartyIdSource().getCode());
		Assertions.assertEquals(PartyRole.EXECUTING_FIRM, partyResult.getPartyRole().getCode());
	}


	@Test
	public void testGetFixPartyListWithoutGroupsUsingEntityId() {
		FixEntity entity = new FixOrder();
		entity.setFixDestinationName("fixDestination1");
		entity.setFixSenderSubId("test_user_name");

		Message message = new Message();
		message.setField(new NoPartyIDs());
		message.setField(new PartyID("CITI"));
		message.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		message.setField(new PartyRole(PartyRole.EXECUTING_FIRM));

		List<FixParty> fixPartyList = this.fixMessageHandler.getFixPartyList(message, entity);
		Assertions.assertEquals(1, fixPartyList.size());
		FixParty partyResult = fixPartyList.get(0);
		Assertions.assertEquals("CITI", partyResult.getPartyId());
		Assertions.assertEquals("mwacker", partyResult.getSenderUserName());
		Assertions.assertEquals(PartyIDSource.PROPRIETARY_CUSTOM_CODE, partyResult.getPartyIdSource().getCode());
		Assertions.assertEquals(PartyRole.EXECUTING_FIRM, partyResult.getPartyRole().getCode());
	}
}
