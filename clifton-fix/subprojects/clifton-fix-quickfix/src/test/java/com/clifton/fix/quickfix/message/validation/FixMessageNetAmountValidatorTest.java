package com.clifton.fix.quickfix.message.validation;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.fix.message.validation.FixValidationStatuses;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import quickfix.InvalidMessage;
import quickfix.Message;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@ExtendWith(SpringExtension.class)
@ContextConfiguration("FixMessageValidationTests-context.xml")
public class FixMessageNetAmountValidatorTest {

	private static final String ALLOCATION_INSTRUCTION_RND = "8=FIX.4.4\u00019=591\u000135=J\u000134=609\u000149=CLIFTONPARA\u000152=20161018-14:09:56.196\u000156=BLP\u00016=99.856694444\u000122=1\u000148=912796JE0\u000153=1900000.00000000\u000154=2\u000155=N\\A\u000160=20161018-14:08:56.601\u000164=20161019\u000170=A_197653_20161018\u000171=0\u000175=20161018\u0001118=1897277.19000000\u0001381=1897277.19000000\u0001423=4\u0001460=6\u0001626=1\u0001857=1\u000173=1\u000111=O_203974_20161018\u000137=6923\u000178=1\u000179=052-BAC0U\u000180=1900000.00000000\u000181=3\u0001154=1897277.19000000\u0001124=1\u000132=1900000.00000000\u000117=SGUT:20161018:518:6:12\u000131=0.38500000\u0001453=5\u0001448=2586\u0001447=D\u0001452=1\u0001448=16351946\u0001447=D\u0001452=11\u0001448=PARAMETRIC PORTFOLIO ASSOCIATES LLC\u0001447=D\u0001452=13\u0001448=TSOX\u0001447=D\u0001452=16\u0001448=JLENNER:14651814\u0001447=D\u0001452=36\u000110=133\u0001";
	private static final String ALLOCATION_REPORT_RND = "8=FIX.4.4\u00019=0683\u000135=AS\u000149=BLP\u000156=CLIFTONPARA\u000134=618\u0001115=i.FIXALLOC\u000152=20161018-14:09:56\u000160=20161018-14:09:56\u0001423=4\u000164=20161019\u0001755=6277:20161018:518:6\u00016=99.856694444\u000170=A_197653_20161018\u0001460=6\u000171=0\u0001793=6277:20161018:518:6\u0001794=3\u000175=20161018\u0001916=00010101\u0001857=1\u0001917=00010101\u000148=912796JE0\u0001381=1897277.1899999999\u000122=1\u000153=1900000\u000154=2\u000155=N\\A\u000187=0\u0001118=1897277.1899999999\u000173=1\u000111=O_203974_20161018\u000178=1\u000179=052-BAC0U\u0001661=99\u000180=1900000\u0001539=1\u0001524=052-BAC0U\u0001525=D\u0001538=24\u0001154=1897277.1899999999\u00015546=1897277.1899999999\u0001742=0\u0001453=5\u0001448=2586\u0001447=D\u0001452=1\u0001448=16351946\u0001447=D\u0001452=11\u0001448=PARAMETRIC PORTFOLIO ASSOCIATES LLC\u0001447=D\u0001452=13\u0001448=TSOX\u0001447=D\u0001452=16\u0001448=JLENNER:14651814\u0001447=D\u0001452=36\u0001124=1\u000132=1900000\u000131=0.385\u000110=017\u0001";

	private static final List<FixMessageEntryType> messageTypes = new ArrayList<>();

	@Resource
	private BeanFactory beanFactory;

	@Resource
	private QuickFixMessageValidationHandlerImpl fixValidationHandler;

	@Mock
	private FixMessageService fixMessageService;

	@Mock
	private FixMessageEntryService fixMessageEntryService;


	@BeforeAll
	public static void beforeClass() {
		FixMessageEntryType messageType = new FixMessageEntryType();
		messageType.setId((short) 4);
		messageType.setName("AllocationInstruction");
		messageType.setDescription("Allocate an order of list of orders.");
		messageType.setMessageTypeTagValue("J");
		messageType.setIdFieldTag((short) 70);
		messageType.setReferencedIdFieldTag((short) 72);
		messageTypes.add(messageType);

		messageType = new FixMessageEntryType();
		messageType.setId((short) 8);
		messageType.setName("AllocationReport");
		messageType.setDescription("Allocation details received when the allocation is complete.");
		messageType.setMessageTypeTagValue("AS");
		messageType.setIdFieldTag((short) 70);
		messageType.setReferencedIdFieldTag((short) 72);
		messageTypes.add(messageType);
	}


	@BeforeEach
	public void before() {
		MockitoAnnotations.initMocks(this);

		this.fixValidationHandler.setFixMessageService(this.fixMessageService);
		this.fixValidationHandler.setFixMessageEntryService(this.fixMessageEntryService);

		Mockito.doAnswer(invocation -> {
			Object[] arguments = invocation.getArguments();
			if (!ArrayUtils.isEmpty(arguments)) {
				String type = arguments[0].toString();
				for (FixMessageEntryType messageType : messageTypes) {
					if (StringUtils.isEqual(type, messageType.getMessageTypeTagValue())) {
						return messageType;
					}
				}
			}
			return null;
		}).when(this.fixMessageEntryService).getFixMessageTypeByTagValue(ArgumentMatchers.anyString());

		FixMessageEntryType fixMessageType = new FixMessageEntryType();
		fixMessageType.setName("NewSingleOrder");
		Mockito.when(this.fixMessageEntryService.getFixMessageTypeByTagValue(ArgumentMatchers.anyString())).thenReturn(fixMessageType);
	}


	@Test
	public void TestRoundingError() throws InvalidMessage {
		QuickFixMessageValidationHandlerImpl fixMessageNetAmountValidator = (QuickFixMessageValidationHandlerImpl) this.beanFactory.getBean("quickFixMessageValidator");
		fixMessageNetAmountValidator.setFixMessageService(this.fixMessageService);

		Message allocationReport = QuickFixMessageUtils.parse(ALLOCATION_REPORT_RND);

		FixIdentifier fixIdentifier = new FixIdentifier("roundingError");
		fixIdentifier.setId(1);
		FixSessionDefinition fixSessionDefinition = new FixSessionDefinition();
		FixSession fixSession = new FixSession();
		fixSession.setDefinition(fixSessionDefinition);
		fixIdentifier.setSession(fixSession);

		Mockito.doAnswer(invocation -> {
			Object[] arguments = invocation.getArguments();
			if (!ArrayUtils.isEmpty(arguments)) {
				FixMessageEntrySearchForm searchForm = (FixMessageEntrySearchForm) arguments[0];
				if (FixMessageTypeTagValues.ALLOCATION_INSTRUCTION_TYPE_TAG.getTagValue().equals(searchForm.getMessageTypeTagValue())) {
					FixMessage fixMessage = new FixMessage();
					fixMessage.setIdentifier(fixIdentifier);
					fixMessage.setText(ALLOCATION_INSTRUCTION_RND);
					return Collections.singletonList(fixMessage);
				}
				if (FixMessageTypeTagValues.ALLOCATION_REPORT_TYPE_TAG.getTagValue().equals(searchForm.getMessageTypeTagValue())) {
					FixMessage fixMessage = new FixMessage();
					fixMessage.setIdentifier(fixIdentifier);
					fixMessage.setText(ALLOCATION_REPORT_RND);
					return Collections.singletonList(fixMessage);
				}
			}
			return Collections.emptyList();
		}).when(this.fixMessageService).getFixMessageList(ArgumentMatchers.any(FixMessageEntrySearchForm.class));

		List<FixValidationResult> fixValidationResult = this.fixValidationHandler.validate(allocationReport, fixIdentifier);
		Assertions.assertNotNull(fixValidationResult);
		List<FixValidationStatuses> statuses = fixValidationResult.stream().map(FixValidationResult::getValidationStatus).distinct().collect(Collectors.toList());
		Assertions.assertEquals(1, statuses.size());
		Assertions.assertEquals(FixValidationStatuses.FAILURE, statuses.get(0));
		List<String> messages = fixValidationResult.stream().map(FixValidationResult::getMessage).collect(Collectors.toList());
		Assertions.assertEquals(2, messages.size(), messages.toString());
		Assertions.assertTrue(messages.get(0).startsWith("Expected the same"), messages.get(0));
		Assertions.assertTrue(messages.get(1).startsWith("Expected the same"), messages.get(1));
	}
}
