package com.clifton.fix.quickfix.util;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.session.FixSessionDefinition;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.field.BeginString;
import quickfix.field.SenderCompID;
import quickfix.field.SenderLocationID;
import quickfix.field.SenderSubID;
import quickfix.field.TargetCompID;
import quickfix.field.TargetLocationID;
import quickfix.field.TargetSubID;

import java.util.List;


public class QuickFixUtilServiceImplTests {

	@Test
	public void testFixMessageFieldList() {
		String messageText = "8=FIX.4.49=23935=D34=56949=CLIFTON50=u74315952=20130515-17:41:40.43456=GSFUT57=TKTS1=C032061311=O_56685_2013051512414021=122=A38=5.000000000040=148=ESZ3 Index54=155=ESZ3 Index59=060=20130515-17:41:40.41864=2013051675=20130515167=FUT10=250";

		QuickFixUtilServiceImpl utilService = new QuickFixUtilServiceImpl();
		List<FixMessageField> fieldList = utilService.getFixMessageFieldList(messageText);

		for (FixMessageField fld : CollectionUtils.getIterable(fieldList)) {
			System.out.println(fld.toString());
		}

		MatcherAssert.assertThat(fieldList.get(0).toString(), IsEqual.equalTo("8	BeginString	FIX.4.4	"));
		MatcherAssert.assertThat(fieldList.get(1).toString(), IsEqual.equalTo("9	BodyLength	239	"));
		MatcherAssert.assertThat(fieldList.get(2).toString(), IsEqual.equalTo("34	MsgSeqNum	569	"));
		MatcherAssert.assertThat(fieldList.get(3).toString(), IsEqual.equalTo("35	MsgType	D	NewOrderSingle"));
		MatcherAssert.assertThat(fieldList.get(4).toString(), IsEqual.equalTo("49	SenderCompID	CLIFTON	"));
		MatcherAssert.assertThat(fieldList.get(5).toString(), IsEqual.equalTo("50	SenderSubID	u743159	"));
		MatcherAssert.assertThat(fieldList.get(6).toString(), IsEqual.equalTo("52	SendingTime	20130515-17:41:40.434	"));
		MatcherAssert.assertThat(fieldList.get(7).toString(), IsEqual.equalTo("56	TargetCompID	GSFUT	"));
		MatcherAssert.assertThat(fieldList.get(8).toString(), IsEqual.equalTo("57	TargetSubID	TKTS	"));
		MatcherAssert.assertThat(fieldList.get(9).toString(), IsEqual.equalTo("1	Account	C0320613	"));
		MatcherAssert.assertThat(fieldList.get(10).toString(), IsEqual.equalTo("11	ClOrdID	O_56685_20130515124140	"));
		MatcherAssert.assertThat(fieldList.get(11).toString(), IsEqual.equalTo("21	HandlInst	1	AUTOMATED_EXECUTION_ORDER_PRIVATE"));
		MatcherAssert.assertThat(fieldList.get(12).toString(), IsEqual.equalTo("22	SecurityIDSource	A	BLOOMBERG_SYMBOL"));
		MatcherAssert.assertThat(fieldList.get(13).toString(), IsEqual.equalTo("38	OrderQty	5.0000000000	"));
		MatcherAssert.assertThat(fieldList.get(14).toString(), IsEqual.equalTo("40	OrdType	1	MARKET"));
		MatcherAssert.assertThat(fieldList.get(15).toString(), IsEqual.equalTo("48	SecurityID	ESZ3 Index	"));
		MatcherAssert.assertThat(fieldList.get(16).toString(), IsEqual.equalTo("54	Side	1	BUY"));
		MatcherAssert.assertThat(fieldList.get(17).toString(), IsEqual.equalTo("55	Symbol	ESZ3 Index	"));
		MatcherAssert.assertThat(fieldList.get(18).toString(), IsEqual.equalTo("59	TimeInForce	0	DAY"));
		MatcherAssert.assertThat(fieldList.get(19).toString(), IsEqual.equalTo("60	TransactTime	20130515-17:41:40.418	"));
		MatcherAssert.assertThat(fieldList.get(20).toString(), IsEqual.equalTo("64	SettlDate	20130516	"));
		MatcherAssert.assertThat(fieldList.get(21).toString(), IsEqual.equalTo("75	TradeDate	20130515	"));
		MatcherAssert.assertThat(fieldList.get(22).toString(), IsEqual.equalTo("167	SecurityType	FUT	FUTURE"));
		MatcherAssert.assertThat(fieldList.get(23).toString(), IsEqual.equalTo("10	CheckSum	250	"));

		Assertions.assertEquals(24, fieldList.size());
	}


	@Test
	public void testFixMessageFieldListWithGroups() {
		String messageText = "8=FIX.4.49=43935=J34=1549=CLIFTONPARA52=20130509-12:27:49.96356=BLP6=99.53515625000000000000000000000022=148=N/A53=2000000.00000000000000054=155=N/A60=20130509-12:26:50.23964=2013051070=A_56474_2013050907253671=075=20130509423=1857=173=111=O_55642_2013050907235037=2378=279=SWBF090900280=1000000.000000000081=079=2460860280=1000000.000000000081=0124=132=2000000.00000000000000017=D2:20130509:145:6:1231=99.53515625000000010=232";

		QuickFixUtilServiceImpl utilService = new QuickFixUtilServiceImpl();
		List<FixMessageField> fieldList = utilService.getFixMessageFieldList(messageText);

		for (FixMessageField fld : CollectionUtils.getIterable(fieldList)) {
			System.out.println(fld.toString());
			if ((fld.getChildren() != null) && (!fld.getChildren().isEmpty())) {
				for (FixMessageField cFld : CollectionUtils.getIterable(fld.getChildren())) {
					System.out.println("\t" + cFld.toString());
				}
			}
		}

		MatcherAssert.assertThat(fieldList.get(0).toString(), IsEqual.equalTo("8	BeginString	FIX.4.4	"));
		MatcherAssert.assertThat(fieldList.get(1).toString(), IsEqual.equalTo("9	BodyLength	439	"));
		MatcherAssert.assertThat(fieldList.get(2).toString(), IsEqual.equalTo("34	MsgSeqNum	15	"));
		MatcherAssert.assertThat(fieldList.get(3).toString(), IsEqual.equalTo("35	MsgType	J	AllocationInstruction"));
		MatcherAssert.assertThat(fieldList.get(4).toString(), IsEqual.equalTo("49	SenderCompID	CLIFTONPARA	"));
		MatcherAssert.assertThat(fieldList.get(5).toString(), IsEqual.equalTo("52	SendingTime	20130509-12:27:49.963	"));
		MatcherAssert.assertThat(fieldList.get(6).toString(), IsEqual.equalTo("56	TargetCompID	BLP	"));
		MatcherAssert.assertThat(fieldList.get(7).toString(), IsEqual.equalTo("6	AvgPx	99.535156250000000000000000000000	"));
		MatcherAssert.assertThat(fieldList.get(8).toString(), IsEqual.equalTo("22	SecurityIDSource	1	CUSIP"));
		MatcherAssert.assertThat(fieldList.get(9).toString(), IsEqual.equalTo("48	SecurityID	N/A	"));
		MatcherAssert.assertThat(fieldList.get(10).toString(), IsEqual.equalTo("53	Quantity	2000000.000000000000000	"));
		MatcherAssert.assertThat(fieldList.get(11).toString(), IsEqual.equalTo("54	Side	1	BUY"));
		MatcherAssert.assertThat(fieldList.get(12).toString(), IsEqual.equalTo("55	Symbol	N/A	"));
		MatcherAssert.assertThat(fieldList.get(13).toString(), IsEqual.equalTo("60	TransactTime	20130509-12:26:50.239	"));
		MatcherAssert.assertThat(fieldList.get(14).toString(), IsEqual.equalTo("64	SettlDate	20130510	"));
		MatcherAssert.assertThat(fieldList.get(15).toString(), IsEqual.equalTo("70	AllocID	A_56474_20130509072536	"));
		MatcherAssert.assertThat(fieldList.get(16).toString(), IsEqual.equalTo("71	AllocTransType	0	NEW"));
		MatcherAssert.assertThat(fieldList.get(17).toString(), IsEqual.equalTo("73	NoOrders	1	"));
		MatcherAssert.assertThat(fieldList.get(17).getChildren().get(0).toString(), IsEqual.equalTo("11	ClOrdID	O_55642_20130509072350	"));
		MatcherAssert.assertThat(fieldList.get(17).getChildren().get(1).toString(), IsEqual.equalTo("37	OrderID	23	"));
		MatcherAssert.assertThat(fieldList.get(18).toString(), IsEqual.equalTo("75	TradeDate	20130509	"));
		MatcherAssert.assertThat(fieldList.get(19).toString(), IsEqual.equalTo("78	NoAllocs	2	"));
		MatcherAssert.assertThat(fieldList.get(19).getChildren().get(0).toString(), IsEqual.equalTo("79	AllocAccount	SWBF0909002	"));
		MatcherAssert.assertThat(fieldList.get(19).getChildren().get(1).toString(), IsEqual.equalTo("80	AllocQty	1000000.0000000000	"));
		MatcherAssert.assertThat(fieldList.get(19).getChildren().get(2).toString(), IsEqual.equalTo("81	ProcessCode	0	REGULAR"));
		MatcherAssert.assertThat(fieldList.get(19).getChildren().get(3).toString(), IsEqual.equalTo("79	AllocAccount	24608602	"));
		MatcherAssert.assertThat(fieldList.get(19).getChildren().get(4).toString(), IsEqual.equalTo("80	AllocQty	1000000.0000000000	"));
		MatcherAssert.assertThat(fieldList.get(19).getChildren().get(5).toString(), IsEqual.equalTo("81	ProcessCode	0	REGULAR"));
		MatcherAssert.assertThat(fieldList.get(20).toString(), IsEqual.equalTo("124	NoExecs	1	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(0).toString(), IsEqual.equalTo("17	ExecID	D2:20130509:145:6:12	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(1).toString(), IsEqual.equalTo("31	LastPx	99.535156250000000	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(2).toString(), IsEqual.equalTo("32	LastQty	2000000.000000000000000	"));
		MatcherAssert.assertThat(fieldList.get(21).toString(), IsEqual.equalTo("423	PriceType	1	PERCENTAGE"));
		MatcherAssert.assertThat(fieldList.get(22).toString(), IsEqual.equalTo("857	AllocNoOrdersType	1	EXPLICIT_LIST_PROVIDED"));
		MatcherAssert.assertThat(fieldList.get(23).toString(), IsEqual.equalTo("10	CheckSum	232	"));

		Assertions.assertEquals(24, fieldList.size());
	}


	@Test
	public void testFixMessageFieldListWithSubGroups() {
		String messageText = "8=FIX.4.4\u00019=315\u000135=D\u000134=538\u000149=CLIFTONPARA\u000152=20160630-13:01:13.742\u000156=BLP\u000157=6036836\u000111=O_189555_20160630\u000121=3\u000122=P\u000138=1050000.00000000\u000140=1\u000148=2I65BYDK8\u000154=2\u000155=N\\A\u000159=0\u000160=20160630-13:01:13.694\u000164=20160630\u000175=20160630\u00016215=Y5\u000178=1\u000179=PPA\u0001539=1\u0001524=1V8Y6QCX6YMJ2OELII46\u0001525=N\u0001538=4\u000180=1050000.00000000\u0001453=1\u0001448=ICE\u0001447=D\u0001452=21\u000110=075\u0001";

		QuickFixUtilServiceImpl utilService = new QuickFixUtilServiceImpl();
		List<FixMessageField> fieldList = utilService.getFixMessageFieldList(messageText);

		for (FixMessageField fld : CollectionUtils.getIterable(fieldList)) {
			System.out.println(fld.toString());
			if ((fld.getChildren() != null) && (!fld.getChildren().isEmpty())) {
				for (FixMessageField cFld : CollectionUtils.getIterable(fld.getChildren())) {
					System.out.println("\t" + cFld.toString());
				}
			}
		}

		MatcherAssert.assertThat(fieldList.get(0).toString(), IsEqual.equalTo("8	BeginString	FIX.4.4	"));
		MatcherAssert.assertThat(fieldList.get(1).toString(), IsEqual.equalTo("9	BodyLength	315	"));
		MatcherAssert.assertThat(fieldList.get(2).toString(), IsEqual.equalTo("34	MsgSeqNum	538	"));
		MatcherAssert.assertThat(fieldList.get(3).toString(), IsEqual.equalTo("35	MsgType	D	NewOrderSingle"));
		MatcherAssert.assertThat(fieldList.get(4).toString(), IsEqual.equalTo("49	SenderCompID	CLIFTONPARA	"));
		MatcherAssert.assertThat(fieldList.get(5).toString(), IsEqual.equalTo("52	SendingTime	20160630-13:01:13.742	"));
		MatcherAssert.assertThat(fieldList.get(6).toString(), IsEqual.equalTo("56	TargetCompID	BLP	"));
		MatcherAssert.assertThat(fieldList.get(7).toString(), IsEqual.equalTo("57	TargetSubID	6036836	"));
		MatcherAssert.assertThat(fieldList.get(8).toString(), IsEqual.equalTo("11	ClOrdID	O_189555_20160630	"));
		MatcherAssert.assertThat(fieldList.get(9).toString(), IsEqual.equalTo("21	HandlInst	3	MANUAL_ORDER"));
		MatcherAssert.assertThat(fieldList.get(10).toString(), IsEqual.equalTo("22	SecurityIDSource	P	null"));
		MatcherAssert.assertThat(fieldList.get(11).toString(), IsEqual.equalTo("38	OrderQty	1050000.00000000	"));
		MatcherAssert.assertThat(fieldList.get(12).toString(), IsEqual.equalTo("40	OrdType	1	MARKET"));
		MatcherAssert.assertThat(fieldList.get(13).toString(), IsEqual.equalTo("48	SecurityID	2I65BYDK8	"));
		MatcherAssert.assertThat(fieldList.get(14).toString(), IsEqual.equalTo("54	Side	2	SELL"));
		MatcherAssert.assertThat(fieldList.get(15).toString(), IsEqual.equalTo("55	Symbol	N\\A	"));
		MatcherAssert.assertThat(fieldList.get(16).toString(), IsEqual.equalTo("59	TimeInForce	0	DAY"));
		MatcherAssert.assertThat(fieldList.get(17).toString(), IsEqual.equalTo("60	TransactTime	20160630-13:01:13.694	"));
		MatcherAssert.assertThat(fieldList.get(18).toString(), IsEqual.equalTo("64	SettlDate	20160630	"));
		MatcherAssert.assertThat(fieldList.get(19).toString(), IsEqual.equalTo("75	TradeDate	20160630	"));
		MatcherAssert.assertThat(fieldList.get(20).toString(), IsEqual.equalTo("78	NoAllocs	1	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(0).toString(), IsEqual.equalTo("79	AllocAccount	PPA	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(1).toString(), IsEqual.equalTo("80	AllocQty	1050000.00000000	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(2).toString(), IsEqual.equalTo("539	NoNestedPartyIDs	1	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(2).getChildren().get(0).toString(), IsEqual.equalTo("524	NestedPartyID	1V8Y6QCX6YMJ2OELII46	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(2).getChildren().get(1).toString(), IsEqual.equalTo("525	NestedPartyIDSource	N	"));
		MatcherAssert.assertThat(fieldList.get(20).getChildren().get(2).getChildren().get(2).toString(), IsEqual.equalTo("538	NestedPartyRole	4	"));
		MatcherAssert.assertThat(fieldList.get(21).toString(), IsEqual.equalTo("453	NoPartyIDs	1	"));
		MatcherAssert.assertThat(fieldList.get(21).getChildren().get(0).toString(), IsEqual.equalTo("447	PartyIDSource	D\tPROPRIETARY_CUSTOM_CODE"));
		MatcherAssert.assertThat(fieldList.get(21).getChildren().get(1).toString(), IsEqual.equalTo("448	PartyID	ICE	"));
		MatcherAssert.assertThat(fieldList.get(21).getChildren().get(2).toString(), IsEqual.equalTo("452	PartyRole	21\tCLEARING_ORGANIZATION"));
		MatcherAssert.assertThat(fieldList.get(22).toString(), IsEqual.equalTo("6215	null	Y5	"));
		MatcherAssert.assertThat(fieldList.get(23).toString(), IsEqual.equalTo("10	CheckSum	075	"));

		Assertions.assertEquals(24, fieldList.size());
	}


	@Test
	public void testSetMessageInfoFromSessionDefinition() throws FieldNotFound {
		Message message = new Message();
		FixSessionDefinition definition = new FixSessionDefinition();

		definition.setBeginString("1");
		definition.setSenderCompId("2");
		definition.setSenderSubId("3");
		definition.setSenderLocId("4");
		definition.setTargetCompId("5");
		definition.setTargetSubId("6");
		definition.setTargetLocId("7");

		QuickFixUtilServiceImpl utilService = new QuickFixUtilServiceImpl();
		utilService.setMessageInfoFromSessionDefinition(message, definition);

		Message.Header header = message.getHeader();

		Assertions.assertEquals("1", header.getString(BeginString.FIELD));
		Assertions.assertEquals("2", header.getString(SenderCompID.FIELD));
		Assertions.assertEquals("3", header.getString(SenderSubID.FIELD));
		Assertions.assertEquals("4", header.getString(SenderLocationID.FIELD));
		Assertions.assertEquals("5", header.getString(TargetCompID.FIELD));
		Assertions.assertEquals("6", header.getString(TargetSubID.FIELD));
		Assertions.assertEquals("7", header.getString(TargetLocationID.FIELD));
	}
}
