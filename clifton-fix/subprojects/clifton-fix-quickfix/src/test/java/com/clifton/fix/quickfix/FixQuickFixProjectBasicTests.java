package com.clifton.fix.quickfix;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class FixQuickFixProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "fix-quickfix";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("quickfix");
		approvedList.add("market");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.text.MessageFormat");
		imports.add("org.springframework.beans.factory.BeanCreationException");
		imports.add("org.quickfixj.");
		imports.add("quickfix.");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("com.google.common.collect.ImmutableMap");
		imports.add("com.google.common.collect.ImmutableList");
		imports.add("com.google.common.collect.MapDifference");
		imports.add("com.google.common.collect.Maps");
		imports.add("com.google.common.collect.Lists");
		imports.add("javax.servlet.http.HttpServletRequest");
		imports.add("org.springframework.beans.factory.BeanFactory");
		imports.add("org.springframework.test.util.ReflectionTestUtils");
		imports.add("org.springframework.util.ClassUtils");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> result = super.getAllowedContextManagedBeanSuffixNames();
		result.add("FixTagModifier");
		return result;
	}
}
