package com.clifton.fix.quickfix.message.validation;

import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import quickfix.InvalidMessage;
import quickfix.Message;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.List;


/**
 * The class this is testing resides in the clifton-fix module, however, this test needs to be here in order to
 * pick up the clifton-fix-quickfix implementations of the fix validators.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class FixMessageValidationTests {

	// has 55 set to a valid symbol value.
	private static final String singleOrder1 = "8=FIX.4.4\u00019=191\u000135=D\u000134=2223\u000149=CLIFTON\u000150=u737933\u000152=20110427-15:06:56.613\u000156=GSFUT\u000157=TKTS\u000111=25_20110427100655\u000121=1\u000122=A\u000138=16\u000140=1\u000148=FVM1 Comdty\u000154=2\u000155=FVM1 Comdty\u000159=0\u000160=20110427-15:06:55.217\u0001167=FUT\u000110=079\u0001";
	private static final String executionReport1 = "8=FIX.4.4\u00019=237\u000135=8\u000149=GSFUT\u000156=CLIFTON\u000134=2630\u000150=u737933\u000152=20110427-15:06:57\u000137=KRT00002\u000111=25_20110427100655\u000117=24300239511711\u000120=0\u0001150=0\u000139=0\u000155=FVM1 Comdty\u0001167=FUT\u000154=2\u000138=16\u000140=1\u000159=0\u000132=0\u000131=0\u0001151=6\u000114=0\u00016=0.0000\u000160=20110427-15:06:57\u000121=1\u0001442=2\u000110=087\u0001";
	private static final String executionReport2 = "8=FIX.4.4\u00019=237\u000135=8\u000149=GSFUT\u000156=CLIFTON\u000134=2630\u000150=u737933\u000152=20110427-15:06:57\u000137=KRT00002\u000111=25_20110427100655\u000117=24300239511711\u000120=0\u0001150=0\u000139=0\u000155=FVM1 Comodity\u0001167=FUT\u000154=2\u000138=16\u000140=1\u000159=0\u000132=0\u000131=0\u0001151=6\u000114=0\u00016=0.0000\u000160=20110427-15:06:57\u000121=1\u0001442=2\u000110=047\u0001";

	// 55 set to N/A, use securityID instead.
	private static final String singleOrder2 = "8=FIX.4.4\u00019=210\u000135=D\u000134=236\u000149=CLIFTONPARA\u000150=\u000152=20130613-17:38:34.303\u000156=BLP\u000157=6036836\u000111=O_59609_20130613123834\u000121=3\u000122=1\u000138=1.0000000000\u000140=1\u000148=912828BW9\u000154=1\u000155=N/A\u000159=0\u000160=20130613-17:38:34.687\u000164=20130614\u000175=20130613\u000110=085\u0001";
	private static final String executionReport3 = "8=FIX.4.4\u00019=0318\u000135=8\u000149=BLP\u000156=CLIFTONPARA\u000134=228\u000150=6036836\u0001115=654857\u000152=20130613-17:38:36\u000160=20130613-17:38:34\u0001150=8\u000131=0\u0001151=0\u000132=0\u00016=0\u000137=NONREF\u000138=1\u000139=8\u0001460=12\u000111=O_59609_20130613123834\u0001192=0\u0001103=99\u0001223=0\u000114=0\u000117=51ba039c0458000a\u000148=912828BW9\u000122=1\u0001442=1\u000154=1\u000155=N/A\u0001236=0\u000158=Order quantity is too small, the minimum is 10\u000159=0\u000164=20130614\u000175=20130613\u000110=105\u0001";
	private static final String executionReport4 = "8=FIX.4.4\u00019=0318\u000135=8\u000149=BLP\u000156=CLIFTONPARA\u000134=228\u000150=6036836\u0001115=654857\u000152=20130613-17:38:36\u000160=20130613-17:38:34\u0001150=8\u000131=0\u0001151=0\u000132=0\u00016=0\u000137=NONREF\u000138=1\u000139=8\u0001460=12\u000111=O_59609_20130613123834\u0001192=0\u0001103=99\u0001223=0\u000114=0\u000117=51ba039c0458000a\u000148=912828BWW\u000122=1\u0001442=1\u000154=1\u000155=N/A\u0001236=0\u000158=Order quantity is too small, the minimum is 10\u000159=0\u000164=20130614\u000175=20130613\u000110=135\u0001";

	@Resource
	private BeanFactory beanFactory;

	@Resource
	private FixMessageService fixMessageService;

	@Resource
	private FixMessageEntryService fixMessageEntryService;


	@BeforeEach
	public void before() {
		Mockito.reset(this.fixMessageService);
		Mockito.reset(this.fixMessageEntryService);
	}


	@Test
	public void testSymbolEqualityValidations() throws InvalidMessage {
		QuickFixMessageValidationHandlerImpl fixMessageSecurityEqualityValidator = (QuickFixMessageValidationHandlerImpl) this.beanFactory.getBean("quickFixMessageValidator");
		Message message = QuickFixMessageUtils.parse(singleOrder1);
		FixIdentifier fixIdentifier = new FixIdentifier();
		fixIdentifier.setId(1);
		FixSessionDefinition fixSessionDefinition = new FixSessionDefinition();
		FixSession fixSession = new FixSession();
		fixSession.setDefinition(fixSessionDefinition);
		fixIdentifier.setSession(fixSession);
		List<FixValidationResult> fixValidationResults = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);

		Assertions.assertNotNull(fixValidationResults);
		Assertions.assertTrue(fixValidationResults.isEmpty());

		message = QuickFixMessageUtils.parse(executionReport1);
		fixValidationResults = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);

		Assertions.assertNotNull(fixValidationResults);
		Assertions.assertTrue(fixValidationResults.isEmpty());

		fixIdentifier.setId(1);
		FixMessage fixMessage = new FixMessage();
		fixMessage.setText(singleOrder1);
		fixMessage.setIdentifier(fixIdentifier);
		Mockito.when(this.fixMessageService.getFixMessageList(ArgumentMatchers.any(FixMessageEntrySearchForm.class))).thenReturn(Collections.singletonList(fixMessage));

		FixMessageEntryType fixMessageEntryType = new FixMessageEntryType();
		fixMessageEntryType.setName("NewSingleOrder");
		Mockito.when(this.fixMessageEntryService.getFixMessageTypeByTagValue(ArgumentMatchers.anyString())).thenReturn(fixMessageEntryType);

		fixValidationResults = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);
		Assertions.assertNotNull(fixValidationResults);
		Assertions.assertTrue(fixValidationResults.isEmpty());

		message = QuickFixMessageUtils.parse(executionReport2);
		fixValidationResults = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);
		Assertions.assertNotNull(fixValidationResults);
		Assertions.assertFalse(fixValidationResults.isEmpty());
		Assertions.assertEquals(1, fixValidationResults.size());
		Assertions.assertTrue(fixValidationResults.get(0).getMessage().startsWith("Expected the same"));
	}


	@Test
	public void testSecurityIdEqualityValidations() throws InvalidMessage {
		QuickFixMessageValidationHandlerImpl fixMessageSecurityEqualityValidator = (QuickFixMessageValidationHandlerImpl) this.beanFactory.getBean("quickFixMessageValidator");
		Message message = QuickFixMessageUtils.parse(singleOrder2);
		FixIdentifier fixIdentifier = new FixIdentifier();
		fixIdentifier.setId(1);
		FixSessionDefinition fixSessionDefinition = new FixSessionDefinition();
		FixSession fixSession = new FixSession();
		fixSession.setDefinition(fixSessionDefinition);
		fixIdentifier.setSession(fixSession);
		FixMessage fixMessage = new FixMessage();
		fixMessage.setText(singleOrder2);
		fixMessage.setIdentifier(fixIdentifier);
		Mockito.when(this.fixMessageService.getFixMessageList(ArgumentMatchers.any(FixMessageEntrySearchForm.class))).thenReturn(Collections.singletonList(fixMessage));

		FixMessageEntryType fixMessageType = new FixMessageEntryType();
		fixMessageType.setName("NewSingleOrder");
		Mockito.when(this.fixMessageEntryService.getFixMessageTypeByTagValue(ArgumentMatchers.anyString())).thenReturn(fixMessageType);

		List<FixValidationResult> fixValidationResult = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);

		Assertions.assertNotNull(fixValidationResult);
		Assertions.assertTrue(fixValidationResult.isEmpty());

		message = QuickFixMessageUtils.parse(executionReport3);
		fixValidationResult = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);

		Assertions.assertNotNull(fixValidationResult);
		Assertions.assertTrue(fixValidationResult.isEmpty());

		fixValidationResult = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);
		Assertions.assertNotNull(fixValidationResult);
		Assertions.assertTrue(fixValidationResult.isEmpty());

		message = QuickFixMessageUtils.parse(executionReport4);
		fixValidationResult = fixMessageSecurityEqualityValidator.validate(message, fixIdentifier);
		Assertions.assertNotNull(fixValidationResult);
		Assertions.assertFalse(fixValidationResult.isEmpty());
		Assertions.assertEquals(1, fixValidationResult.size());
		Assertions.assertTrue(fixValidationResult.get(0).getMessage().startsWith("Expected the same"));
	}
}
