package com.clifton.fix.quickfix.util;


import com.clifton.core.util.validation.ValidationException;
import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.quickfix.converter.StringToMessageConverter;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.cache.FixSessionCache;
import com.clifton.fix.util.FixUtilService;
import org.springframework.stereotype.Component;
import quickfix.DataDictionary;
import quickfix.Field;
import quickfix.FieldMap;
import quickfix.Message;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.StringField;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * The <code>QuickFixUtilServiceImpl</code> provides a QuickFIX specific implementation of FixUtilService.
 *
 * @author mwacker
 */
@Component("fixUtilService")
public class QuickFixUtilServiceImpl implements FixUtilService<Message> {

	private static final BiFunction<StringField, DataDictionary, FixMessageField> fixMessageFieldCreator = (field, dataDictionary) -> {
		FixMessageField fld = new FixMessageField();
		fld.setTag(field.getField());
		fld.setFieldName(dataDictionary.getFieldName(fld.getTag()));
		fld.setFieldValue(field.getValue());
		fld.setFieldValueName(dataDictionary.hasFieldValue(fld.getTag()) ? dataDictionary.getValueName(fld.getTag(), field.getValue()) : "");
		return fld;
	};


	private FixSessionCache<SessionID> fixSessionCache;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message parseMessage(String fixMessageString) {
		return (StringToMessageConverter.getInstance()).convert(fixMessageString);
	}


	@Override
	public Object getSessionId(Message message, String sessionQualifier) {
		SessionID sessionId = QuickFixMessageUtils.getSessionIDWithOptionalFields(message, sessionQualifier);
		Session session = Session.lookupSession(sessionId);
		// If no session is found, then use only the required fields
		if (session == null) {
			return QuickFixMessageUtils.getSessionID(message, sessionQualifier);
		}
		return sessionId;
	}


	@Override
	public FixSession getFixSession(Message message, String sessionQualifier, boolean incoming) {
		SessionID id = (SessionID) (incoming ? getReverseSessionId(message, sessionQualifier) : getSessionId(message, sessionQualifier));
		return getFixSessionCache().get(id);
	}


	@Override
	public String getMessageTypeCode(String messageText) {
		return QuickFixMessageUtils.getMessageType(messageText);
	}


	@Override
	public String getStringField(String messageString, int tag) {
		return QuickFixMessageUtils.getStringField(messageString, tag);
	}


	@Override
	public String getFieldName(String version, int tag) {
		DataDictionary dd = QuickFixMessageUtils.getDataDictionaryForVersion(version);
		return dd.getFieldName(tag);
	}


	@Override
	public void setField(Message message, int tag, String value) {
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);
		Field<?> field = new StringField(tag, value);
		if (dd.isHeaderField(tag)) {
			message.getHeader().setField(field.getTag(), field);
		}
		else {
			message.setField(field.getTag(), field);
		}
	}


	@Override
	public List<FixMessageField> getFixMessageFieldList(String fixMessageString) {
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(fixMessageString);
		if (dd == null) {
			throw new ValidationException("Unable to determine Fix Version for Message " + fixMessageString + ". Please confirm the fix version is set as the BeginField (i.e. 8=FIX4.4)");
		}
		Message message = parseMessage(fixMessageString);
		List<FixMessageField> result = new ArrayList<>();
		result.addAll(getChildFieldList(message.getHeader(), dd));
		result.addAll(getChildFieldList(message, dd));
		result.addAll(getChildFieldList(message.getTrailer(), dd));
		return result;
	}


	/**
	 * Sets session information in the message header based on the definition.
	 */
	@Override
	public void setMessageInfoFromSessionDefinition(Message message, FixSessionDefinition definition) {
		QuickFixMessageUtils.setMessageHeaderFieldsFromSessionDefinition(message, definition);
	}


	/**
	 * Indicates if a message is an administrative message.
	 */
	@Override
	public boolean isAdminMessage(String msgType) {
		return QuickFixMessageUtils.isAdminMessage(msgType);
	}


	/**
	 * Indicates if the message is a heartbeat.
	 */
	@Override
	public boolean isHeartbeat(String message) {
		return QuickFixMessageUtils.isHeartbeat(message);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<FixMessageField> getChildFieldList(FieldMap map, DataDictionary dd) {
		Iterable<Field<?>> fields = map::iterator;
		List<FixMessageField> result = StreamSupport
				.stream(fields.spliterator(), false)
				.filter(StringField.class::isInstance)
				.map(StringField.class::cast)
				.map(sf -> fixMessageFieldCreator.apply(sf, dd))
				.collect(Collectors.toList());
		result.forEach(fld -> addGroupChildren(fld, map, dd));
		return result;
	}


	private void addGroupChildren(FixMessageField fixMessageField, FieldMap map, DataDictionary dd) {
		Map<FixMessageField, FieldMap> fieldMappings = new HashMap<>();
		List<FixMessageField> childMessages = map.getGroups(fixMessageField.getTag())
				.stream()
				.flatMap(g -> {
					Iterable<Field<?>> grpFields = g::iterator;
					return StreamSupport
							.stream(grpFields.spliterator(), false)
							.sorted(Comparator.comparingInt(Field::getTag))
							.filter(StringField.class::isInstance)
							.map(StringField.class::cast)
							.map(sf -> fixMessageFieldCreator.apply(sf, dd))
							.peek(fm -> fieldMappings.put(fm, g));
				}).collect(Collectors.toList());
		fixMessageField.setChildren(childMessages);
		childMessages.forEach(fm -> addGroupChildren(fm, fieldMappings.get(fm), dd));
	}


	private Object getReverseSessionId(Message message, String sessionQualifier) {
		SessionID sessionId = QuickFixMessageUtils.getReverseSessionIDWithOptionalFields(message, sessionQualifier);
		Session session = Session.lookupSession(sessionId);
		// If no session is found, then use only the required fields
		if (session == null) {
			return QuickFixMessageUtils.getReverseSessionID(message, sessionQualifier);
		}
		return sessionId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionCache<SessionID> getFixSessionCache() {
		return this.fixSessionCache;
	}


	public void setFixSessionCache(FixSessionCache<SessionID> fixSessionCache) {
		this.fixSessionCache = fixSessionCache;
	}
}
