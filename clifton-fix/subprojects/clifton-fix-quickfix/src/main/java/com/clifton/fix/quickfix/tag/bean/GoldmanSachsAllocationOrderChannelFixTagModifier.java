package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.MsgType;
import quickfix.field.PartyID;
import quickfix.field.PartyIDSource;
import quickfix.field.PartyRole;
import quickfix.fix44.component.Parties;

import java.util.Set;


/**
 * The <code>GoldmanSachsAllocationOrderChannelTagModifier</code> sets the specified order channel in Allocation Instruction FIX messages outbound to Goldman Sachs systems.
 *
 * @author davidi
 */
public class GoldmanSachsAllocationOrderChannelFixTagModifier implements FixTagModifierBean<Message> {

	// the order channel name used by Goldman Sachs: "ALGO" for electronically processed orders, "VOICE" for high-touch orders.
	private String orderChannelName;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		String messageTypeCode = QuickFixMessageUtils.getMessageType(message.toString());
		if (!MsgType.ALLOCATION_INSTRUCTION.equals(messageTypeCode)) {
			throw new RuntimeException("GoldmanSachsAllocationOrderChannelTagModifier can only be used by messages of type 35=J (AllocationInstructions).  This message is of type 35=" + messageTypeCode + ".");
		}

		String updatedOrderChannelName = null;
		if (getOrderChannelName() != null) {
			updatedOrderChannelName = getOrderChannelName().toUpperCase();
		}
		validateInputValues(updatedOrderChannelName);

		// Add the new group to the outbound message
		Group partyIdsGroup = new Parties.NoPartyIDs();
		partyIdsGroup.setField(PartyID.FIELD, new PartyID(updatedOrderChannelName));
		partyIdsGroup.setField(PartyIDSource.FIELD, new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		partyIdsGroup.setField(PartyRole.FIELD, new PartyRole(PartyRole.EXECUTING_UNIT));
		message.addGroup(partyIdsGroup);
	}


	private void validateInputValues(String orderChannelValue) {
		ValidationUtils.assertNotEmpty(orderChannelValue, "An order channel value must be provided in the tag modifier configuration.");
		final Set<String> validOrderChannelValues = CollectionUtils.createHashSet("ALGO", "VOICE");
		ValidationUtils.assertTrue(validOrderChannelValues.contains(orderChannelValue), "The entered value \"" + orderChannelValue + "\" is not valid.  Allowed values are ALGO and VOICE.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getOrderChannelName() {
		return this.orderChannelName;
	}


	public void setOrderChannelName(String orderChannelName) {
		this.orderChannelName = orderChannelName;
	}
}
