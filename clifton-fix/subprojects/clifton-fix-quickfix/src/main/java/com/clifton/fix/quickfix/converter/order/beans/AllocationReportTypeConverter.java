package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationReportTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocReportType;


public class AllocationReportTypeConverter extends BaseQuickFixFieldConverter<AllocationReportTypes, AllocReportType> {

	private static final Lazy<AllocationReportTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationReportTypeConverter::new);


	public static AllocationReportTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationReportTypes, AllocReportType> map) {
		map.put(AllocationReportTypes.SELLSIDE_CALCULATED_USING_PRELIMINARY, new AllocReportType(AllocReportType.SELLSIDE_CALCULATED_USING_PRELIMINARY));
		map.put(AllocationReportTypes.SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY, new AllocReportType(AllocReportType.SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY));
		map.put(AllocationReportTypes.WAREHOUSE_RECAP, new AllocReportType(AllocReportType.WAREHOUSE_RECAP));
		map.put(AllocationReportTypes.REQUEST_TO_INTERMEDIARY, new AllocReportType(AllocReportType.REQUEST_TO_INTERMEDIARY));
		map.put(AllocationReportTypes.PRELIMINARY_REQUEST_TO_INTERMEDIARY, new AllocReportType(AllocReportType.PRELIMINARY_REQUEST_TO_INTERMEDIARY));
		map.put(AllocationReportTypes.ACCEPT, new AllocReportType(AllocReportType.ACCEPT));
		map.put(AllocationReportTypes.REJECT, new AllocReportType(AllocReportType.REJECT));
		map.put(AllocationReportTypes.ACCEPT_PENDING, new AllocReportType(AllocReportType.ACCEPT_PENDING));
		map.put(AllocationReportTypes.COMPLETE, new AllocReportType(AllocReportType.COMPLETE));
		map.put(AllocationReportTypes.REVERSE_PENDING, new AllocReportType(AllocReportType.REVERSE_PENDING));
	}
}
