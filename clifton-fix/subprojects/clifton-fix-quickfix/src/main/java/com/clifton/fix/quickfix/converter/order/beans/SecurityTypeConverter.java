package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.SecurityType;


public class SecurityTypeConverter extends BaseQuickFixFieldConverter<SecurityTypes, SecurityType> {


	private static final Lazy<SecurityTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(SecurityTypeConverter::new);


	public static SecurityTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<SecurityTypes, SecurityType> map) {
		SecurityTypes[] sources = SecurityTypes.values();
		for (SecurityTypes source : sources) {
			map.put(source, new SecurityType(source.getCode()));
		}
	}
}
