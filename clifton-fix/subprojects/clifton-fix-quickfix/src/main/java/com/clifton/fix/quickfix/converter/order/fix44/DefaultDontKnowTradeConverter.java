package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixDontKnowTrade;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityIDSourceConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityTypeConverter;
import quickfix.Message;
import quickfix.field.ExecID;
import quickfix.field.OrderID;
import quickfix.field.SecurityIDSource;
import quickfix.field.SecurityType;
import quickfix.field.Side;
import quickfix.field.Symbol;
import quickfix.fix44.DontKnowTrade;


public class DefaultDontKnowTradeConverter implements Converter<Message, FixDontKnowTrade> {

	@Override
	public FixDontKnowTrade convert(Message from) {
		try {
			FixDontKnowTrade result = new FixDontKnowTrade();

			result = QuickFixConverterUtils.convertToFixEntity(from, result);
			result.setOrderId(from.getField(new OrderID()).getValue());

			result.setExecutionId(from.getField(new ExecID()).getValue());
			result.setSymbol(from.getField(new Symbol()).getValue());
			result.setSide((OrderSideConverter.getInstance()).fromField((Side) from.getField(new Side())));
			result.setSecurityIDSource((SecurityIDSourceConverter.getInstance()).fromField((SecurityIDSource) from.getField(new SecurityIDSource())));
			result.setSecurityType((SecurityTypeConverter.getInstance()).fromField((SecurityType) from.getField(new SecurityType())));

			return result;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + DontKnowTrade.class + "] to [" + FixDontKnowTrade.class + "].", e);
		}
	}
}
