package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.AssertUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.Message;
import quickfix.StringField;


/**
 * The FieldAddFixTagModifier adds a new tag (field) and tag value to the FIX Message.
 */
public class FieldAddFixTagModifier implements FixTagModifierBean<Message> {

	public Integer fieldTag;
	public String newFieldValue;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);
		AssertUtils.assertNotNull(getFieldTag(), "Field Tag value cannot be null.");
		AssertUtils.assertNotEmpty(getNewFieldValue(), "New Field Value cannot be null.");

		try {
			if (dd.isHeaderField(getFieldTag())) {
				message.getHeader().setField(new StringField(getFieldTag(), getNewFieldValue()));
			}
			else {
				message.setField(new StringField(getFieldTag(), getNewFieldValue()));
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to modify message [" + message + "] in class [" + this.getClass() + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(Integer fieldTag) {
		this.fieldTag = fieldTag;
	}


	public String getNewFieldValue() {
		return this.newFieldValue;
	}


	public void setNewFieldValue(String newFieldValue) {
		this.newFieldValue = newFieldValue;
	}
}
