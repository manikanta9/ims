package com.clifton.fix.quickfix.converter;


import quickfix.Field;


/**
 * The <code>FixFieldConverter</code> defines quickfix field specific converter used to
 * convert to add from quickfix Field objects.
 *
 * @param <T>
 * @param <F>
 * @author mwacker
 */
public interface QuickFixFieldConverter<T, F extends Field<?>> {

	public F toField(T value);


	public T fromField(F field);
}
