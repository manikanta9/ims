package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.NestedPartyIDSource;


public class NestedPartyIdSourcesConverter extends BaseQuickFixFieldConverter<PartyIdSources, NestedPartyIDSource> {

	private static final Lazy<NestedPartyIdSourcesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(NestedPartyIdSourcesConverter::new);


	public static NestedPartyIdSourcesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PartyIdSources, NestedPartyIDSource> map) {
		PartyIdSources[] sources = PartyIdSources.values();
		for (PartyIdSources source : sources) {
			map.put(source, new NestedPartyIDSource(source.getCode()));
		}
	}
}
