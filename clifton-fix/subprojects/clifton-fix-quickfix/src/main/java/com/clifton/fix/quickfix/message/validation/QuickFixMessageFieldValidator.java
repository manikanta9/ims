package com.clifton.fix.quickfix.message.validation;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.message.validation.FixMessageValidator;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import org.springframework.beans.factory.InitializingBean;
import quickfix.Message;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * <code>QuickFixMessageFieldValidator</code> used by {@link QuickFixMessageValidationHandlerImpl}.
 */
public class QuickFixMessageFieldValidator implements FixMessageValidator<Message, String>, InitializingBean {

	public static final MessageFormat messageFormat = new MessageFormat("Expected the same {0} ({1}). {2} versus {3}.");
	public static final String FIELD_NOT_PRESENT = "N/A";

	// required
	private String label;
	private int field;

	// optional

	/**
	 * The tag or field number of parent group that contains the field to be validated.
	 */
	private int parentGroupField = 0;
	private boolean aggregate = false;
	private List<String> excludedFixSessionDefinitionNames = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String isValid(Message m1, Message m2) {
		Class<?> javaType = QuickFixMessageUtils.getJavaType(m1, getField());
		String results;
		if (Number.class.isAssignableFrom(javaType)) {
			results = validateAsNumeric(m1, m2);
		}
		else {
			results = validateAsString(m1, m2);
		}

		return results;
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		AssertUtils.assertTrue(getField() > 0, "Field should be greater than 0 and its value is %d.", this.field);
		AssertUtils.assertNotEmpty(getLabel(), "Label shouldn't be empty.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected String formatFailureMessage(List<String> v1, List<String> v2) {

		return messageFormat.format(new Object[]{getLabel(), getField(), v1.toString(), v2.toString()});
	}


	private String validateAsNumeric(Message m1, Message m2) {
		String results = "";
		if (isAggregate() && isGroupField()) {
			Optional<BigDecimal> b1 = QuickFixMessageUtils.getAggregatedDecimalFieldValue(m1, getParentGroupField(), getField(), BigDecimal.ZERO);
			Optional<BigDecimal> b2 = QuickFixMessageUtils.getAggregatedDecimalFieldValue(m2, getParentGroupField(), getField(), BigDecimal.ZERO);
			if (b1.isPresent() && b2.isPresent() && b1.get().compareTo(b2.get()) != 0) {
				results = formatFailureMessage(
						Collections.singletonList(b1.map(BigDecimal::toPlainString).orElse(FIELD_NOT_PRESENT)),
						Collections.singletonList(b2.map(BigDecimal::toPlainString).orElse(FIELD_NOT_PRESENT))
				);
			}
		}
		else if (isGroupField()) {
			Map<Message, List<Optional<BigDecimal>>> values = QuickFixMessageUtils.getDecimalGroupFieldValues(Arrays.asList(m1, m2), getParentGroupField(), getField(), BigDecimal.ZERO);
			if (!QuickFixMessageUtils.areEqualBigDecimalList(values.get(m1), values.get(m2))) {
				results = formatFailureMessage(
						values.get(m1)
								.stream()
								.map(Optional::get)
								.map(BigDecimal::toPlainString)
								.collect(Collectors.toList()),
						values.get(m2)
								.stream()
								.map(Optional::get)
								.map(BigDecimal::toPlainString)
								.collect(Collectors.toList())
				);
			}
		}
		else {
			Optional<BigDecimal> b1 = QuickFixMessageUtils.getDecimalFieldValue(m1, getField(), BigDecimal.ZERO);
			Optional<BigDecimal> b2 = QuickFixMessageUtils.getDecimalFieldValue(m2, getField(), BigDecimal.ZERO);
			if (b1.isPresent() && b2.isPresent() && b1.get().compareTo(b2.get()) != 0) {
				results = formatFailureMessage(
						Collections.singletonList(b1.map(BigDecimal::toPlainString).orElse(FIELD_NOT_PRESENT)),
						Collections.singletonList(b2.map(BigDecimal::toPlainString).orElse(FIELD_NOT_PRESENT))
				);
			}
		}

		return results;
	}


	private String validateAsString(Message m1, Message m2) {
		String results = "";
		if (isGroupField()) {
			Map<Message, List<Optional<String>>> values = QuickFixMessageUtils.getStringGroupFieldValues(Arrays.asList(m1, m2), getParentGroupField(), getField(), "");
			if (!QuickFixMessageUtils.areEqualStringList(values.get(m1), values.get(m2))) {
				results = formatFailureMessage(
						values.get(m1)
								.stream()
								.map(o -> o.orElse(FIELD_NOT_PRESENT))
								.collect(Collectors.toList()),
						values.get(m2)
								.stream()
								.map(o -> o.orElse(FIELD_NOT_PRESENT))
								.collect(Collectors.toList())
				);
			}
		}
		else {
			Optional<String> s1 = QuickFixMessageUtils.getStringFieldValue(m1, getField(), "");
			Optional<String> s2 = QuickFixMessageUtils.getStringFieldValue(m2, getField(), "");
			if (s1.isPresent() && s2.isPresent() && !StringUtils.isEqual(s1.get(), s2.get())) {
				results = formatFailureMessage(
						Collections.singletonList(s1.orElse(FIELD_NOT_PRESENT)),
						Collections.singletonList(s2.orElse(FIELD_NOT_PRESENT))
				);
			}
		}
		return results;
	}


	private boolean isGroupField() {
		return getParentGroupField() > 0;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public int getField() {
		return this.field;
	}


	public void setField(int field) {
		this.field = field;
	}


	public boolean isAggregate() {
		return this.aggregate;
	}


	public void setAggregate(boolean aggregate) {
		this.aggregate = aggregate;
	}


	public List<String> getExcludedFixSessionDefinitionNames() {
		return this.excludedFixSessionDefinitionNames;
	}


	public void setExcludedFixSessionDefinitionNames(List<String> excludedFixSessionDefinitionNames) {
		this.excludedFixSessionDefinitionNames = excludedFixSessionDefinitionNames;
	}


	public int getParentGroupField() {
		return this.parentGroupField;
	}


	public void setParentGroupField(int parentGroupField) {
		this.parentGroupField = parentGroupField;
	}
}
