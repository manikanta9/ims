package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixOrder;
import quickfix.Message;


public class DefaultNewOrderSingleConverter extends BaseOrderConverter implements Converter<Message, FixOrder> {

	@Override
	public FixOrder convert(Message from) {
		return toFixOrder(from, new FixOrder());
	}
}
