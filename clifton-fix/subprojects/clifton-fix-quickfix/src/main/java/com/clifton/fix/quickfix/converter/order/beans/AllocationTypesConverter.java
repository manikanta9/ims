package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocType;


public class AllocationTypesConverter extends BaseQuickFixFieldConverter<AllocationTypes, AllocType> {


	private static final Lazy<AllocationTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationTypesConverter::new);


	public static AllocationTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationTypes, AllocType> map) {
		AllocationTypes[] sources = AllocationTypes.values();
		for (AllocationTypes source : sources) {
			map.put(source, new AllocType(source.getCode()));
		}
	}
}
