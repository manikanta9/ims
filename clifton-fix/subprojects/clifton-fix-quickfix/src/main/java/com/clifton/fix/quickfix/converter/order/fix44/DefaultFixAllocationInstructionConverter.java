package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedParty;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.AllocationCancelReplaceReasonsConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationLinkTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationNoOrdersTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationTransactionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.CommissionTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.NestedPartyIdSourcesConverter;
import com.clifton.fix.quickfix.converter.order.beans.NestedPartyRolesConverter;
import com.clifton.fix.quickfix.converter.order.beans.OpenClosesConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartyIdSourcesConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartyRolesConverter;
import com.clifton.fix.quickfix.converter.order.beans.PriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProcessCodeConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProductsConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityTypeConverter;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.Message;
import quickfix.field.Account;
import quickfix.field.AllocAccount;
import quickfix.field.AllocID;
import quickfix.field.AllocLinkID;
import quickfix.field.AllocNetMoney;
import quickfix.field.AllocPrice;
import quickfix.field.AllocQty;
import quickfix.field.AvgPx;
import quickfix.field.ClOrdID;
import quickfix.field.Commission;
import quickfix.field.ExecID;
import quickfix.field.FutSettDate;
import quickfix.field.GrossTradeAmt;
import quickfix.field.LastPx;
import quickfix.field.LastShares;
import quickfix.field.NestedPartyID;
import quickfix.field.NetMoney;
import quickfix.field.OrderID;
import quickfix.field.OrderQty;
import quickfix.field.PartyID;
import quickfix.field.ProcessCode;
import quickfix.field.RefAllocID;
import quickfix.field.SenderSubID;
import quickfix.field.SettlDate;
import quickfix.field.Shares;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.fix44.AllocationInstruction;
import quickfix.fix44.component.CommissionData;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class DefaultFixAllocationInstructionConverter implements Converter<FixAllocationInstruction, AllocationInstruction> {

	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Builds the allocation message for to send via FIX.
	 * <p>
	 * NOTE: The executionReportList property for each order, must be populated with all execution reports for the order group not just for that order.
	 */
	@Override
	public AllocationInstruction convert(FixAllocationInstruction value) {
		AllocationInstruction allocation = new AllocationInstruction();
		if (value.getAllocationTransactionType() != AllocationTransactionTypes.CANCEL) {
			ValidationUtils.assertNotEmpty(value.getFixOrderList(), "Cannot convert [" + FixAllocationInstruction.class + "] to [" + AllocationInstruction.class
					+ "] because no [FixOrder](s) have been provided for the allocation.");
		}
		FixOrder firstOrder = CollectionUtils.getFirstElement(value.getFixOrderList());

		allocation = QuickFixConverterUtils.convertFromFixSecurityEntity(value, allocation);
		// custom security type lookup for allocations
		if (value.getSecurityType() != null) {
			allocation.set((SecurityTypeConverter.getInstance()).toField(value.getSecurityType()));
		}
		else if (firstOrder != null && firstOrder.getSecurityType() != null) {
			allocation.set((SecurityTypeConverter.getInstance()).toField(firstOrder.getSecurityType()));
		}
		if (value.getOpenClose() != null) {
			allocation.setField((OpenClosesConverter.getInstance()).toField(value.getOpenClose()));
		}
		if (!StringUtils.isEmpty(value.getSenderUserName()) || !StringUtils.isEmpty(value.getFixSenderSubId())) {
			String alias = getFixMessageHandler().getFixSenderSubIdForFixEntity(value, allocation, false);
			if (!StringUtils.isEmpty(alias)) {
				allocation.getHeader().setField(new SenderSubID(alias));
			}
		}
		if (!StringUtils.isEmpty(value.getAllocationStringId())) {
			allocation.set(new AllocID(value.getAllocationStringId()));
		}
		allocation.set((AllocationTransactionTypeConverter.getInstance()).toField(value.getAllocationTransactionType()));
		if (value.getAllocationCancelReplaceReason() != null) {
			allocation.set((AllocationCancelReplaceReasonsConverter.getInstance()).toField(value.getAllocationCancelReplaceReason()));
		}
		if (!StringUtils.isEmpty(value.getReferenceAllocationStringId())) {
			allocation.set(new RefAllocID(value.getReferenceAllocationStringId()));
		}
		if (value.getFixAllocationLink() != null) {
			if (value.getAllocationLinkType() == null) {
				throw new RuntimeException("AllocationLinkType required when an allocation link is defined.");
			}

			allocation.set(new AllocLinkID(value.getFixAllocationLink().getAllocationStringId()));
			allocation.set((AllocationLinkTypeConverter.getInstance()).toField(value.getAllocationLinkType()));
		}
		if (!StringUtils.isEmpty(value.getAccount())) {
			allocation.setField(new Account(value.getAccount()));
		}
		if (value.getSettlementDate() != null) {
			allocation.setField(new SettlDate(DateUtils.fromDate(value.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}

		if (value.getAllocationNoOrdersType() != null) {
			allocation.setField((AllocationNoOrdersTypeConverter.getInstance()).toField(value.getAllocationNoOrdersType()));
		}
		if (value.getAllocationType() != null) {
			allocation.setField((AllocationTypesConverter.getInstance()).toField(value.getAllocationType()));
		}
		if (value.getPriceType() != null) {
			allocation.setField((PriceTypesConverter.getInstance()).toField(value.getPriceType()));
		}

		for (FixOrder order : CollectionUtils.getIterable(value.getFixOrderList())) {
			Set<String> addedOrders = new HashSet<>();
			Set<String> addedExecutions = new HashSet<>();
			ValidationUtils.assertNotEmpty(order.getFixExecutionReportList(), "Cannot convert [" + FixAllocationInstruction.class + "] to [" + AllocationInstruction.class
					+ "] because no [FixExecutionReport](s) have been provided for order [" + order.getClientOrderStringId() + "].");
			List<String> canceledExecutionIdList = getCanceledExecutionIdList(order.getFixExecutionReportList());
			for (FixExecutionReport report : order.getFixExecutionReportList()) {
				if (canceledExecutionIdList.contains(report.getExecutionId()) || MathUtils.isNullOrZero(report.getLastShares())) {
					continue;
				}
				if (((report.getOrderStatus() == OrderStatuses.FILLED) || (report.getOrderStatus() == OrderStatuses.PARTIALLY_FILLED))
						&& !addedOrders.contains(report.getOrderId())) {
					addedOrders.add(report.getOrderId());

					quickfix.fix44.AllocationInstruction.NoOrders orders = new quickfix.fix44.AllocationInstruction.NoOrders();
					if (value.isSendSecondaryClientOrderId() && report.getSecondaryClientOrderStringId() != null) {
						orders.set(new ClOrdID(report.getSecondaryClientOrderStringId()));
					}
					else {
						orders.set(new ClOrdID(order.getClientOrderStringId()));
					}
					orders.set(new OrderID(report.getOrderId()));
					if (value.isSendOrderQuantityInNoOrdersGrouping()) {
						BigDecimal quantityForOrder = CoreMathUtils.sumProperty(order.getFixExecutionReportList(), executionReport -> executionReport.getOrderId() != null && executionReport.getOrderId().equals(report.getOrderId()) ? executionReport.getLastShares() : BigDecimal.ZERO);
						orders.set(new OrderQty(quantityForOrder));
					}
					allocation.addGroup(orders);
				}
				if (((report.getOrderStatus() == OrderStatuses.FILLED) || (report.getOrderStatus() == OrderStatuses.PARTIALLY_FILLED)) && !addedExecutions.contains(report.getExecutionId())) {
					addedExecutions.add(report.getExecutionId());

					quickfix.fix44.AllocationInstruction.NoExecs exec = new quickfix.fix44.AllocationInstruction.NoExecs();
					exec.setField(new LastShares(formatNumber(report.getLastShares())));
					exec.setField(new ExecID(report.getExecutionId()));
					exec.setField(new LastPx(formatNumber(report.getLastPrice())));
					allocation.addGroup(exec);
				}
			}
		}
		if (value.getSide() != null) {
			allocation.set((OrderSideConverter.getInstance()).toField(value.getSide()));
		}
		if (value.getShares() != null) {
			allocation.setField(new Shares(formatNumber(value.getShares())));
		}
		if (value.getAveragePrice() != null) {
			allocation.set(new AvgPx(formatNumber(value.getAveragePrice())));
		}
		if (value.getTradeDate() != null) {
			allocation.set(new TradeDate(DateUtils.fromDate(value.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}
		if (value.getTransactionTime() != null) {
			allocation.set(new TransactTime(DateUtils.asLocalDateTime(value.getTransactionTime(), ZoneOffset.UTC)));
		}
		else {
			allocation.set(new TransactTime(DateUtils.asLocalDateTime(new Date(), ZoneOffset.UTC)));
		}
		if (value.getFutureSettlementDate() != null) {
			allocation.setField(new FutSettDate(DateUtils.fromDate(value.getFutureSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}
		if (value.getNetMoney() != null) {
			allocation.set(new NetMoney(formatNumber(value.getNetMoney())));
		}
		if (value.getGrossTradeAmount() != null) {
			allocation.set(new GrossTradeAmt(formatNumber(value.getGrossTradeAmount())));
		}
		if (value.getProduct() != null) {
			allocation.setField((ProductsConverter.getInstance()).toField(value.getProduct()));
		}

		for (FixParty fixParty : CollectionUtils.getIterable(value.getPartyList())) {
			quickfix.fix44.AllocationInstruction.NoPartyIDs party = new quickfix.fix44.AllocationInstruction.NoPartyIDs();

			if (!StringUtils.isEmpty(fixParty.getPartyId())) {
				party.setField(new PartyID(fixParty.getPartyId()));
			}
			else if (!StringUtils.isEmpty(fixParty.getSenderUserName())) {
				String alias = getFixMessageHandler().getAliasForUser(value, allocation, fixParty.getSenderUserName());
				if (!StringUtils.isEmpty(alias)) {
					party.setField(new PartyID(alias));
				}
			}
			party.setField((PartyIdSourcesConverter.getInstance()).toField(fixParty.getPartyIdSource()));
			party.setField((PartyRolesConverter.getInstance()).toField(fixParty.getPartyRole()));
			allocation.addGroup(party);
		}

		addAllocations(allocation, value);

		return allocation;
	}


	private void addAllocations(AllocationInstruction allocation, FixAllocationInstruction value) {
		for (FixAllocationDetail allocationDetail : CollectionUtils.getIterable(value.getFixAllocationDetailList())) {
			quickfix.fix44.AllocationInstruction.NoAllocs alloc = new quickfix.fix44.AllocationInstruction.NoAllocs();

			alloc.set(new AllocAccount(allocationDetail.getAllocationAccount()));
			alloc.set(new AllocQty(formatNumber(allocationDetail.getAllocationShares())));
			if (allocationDetail.getAllocationPrice() != null) {
				alloc.set(new AllocPrice(formatNumber(allocationDetail.getAllocationPrice())));
			}
			if (allocationDetail.getProcessCode() != null) {
				alloc.set((ProcessCodeConverter.getInstance()).toField(allocationDetail.getProcessCode()));
			}
			else {
				alloc.set(new ProcessCode(ProcessCode.REGULAR));
			}

			if (allocationDetail.getAllocationNetMoney() != null) {
				alloc.set(new AllocNetMoney(formatNumber(allocationDetail.getAllocationNetMoney())));
			}

			if ((allocationDetail.getCommission() != null) && (allocationDetail.getCommissionType() != null)) {
				CommissionData commissionData = new CommissionData();
				commissionData.set(new Commission(formatNumber(allocationDetail.getCommission())));
				commissionData.set((CommissionTypesConverter.getInstance()).toField(allocationDetail.getCommissionType()));
				alloc.set(commissionData);
			}

			addAllocationNestedParties(allocationDetail, alloc);
			allocation.addGroup(alloc);
		}
	}


	private void addAllocationNestedParties(FixAllocationDetail allocationDetail, quickfix.fix44.AllocationInstruction.NoAllocs alloc) {
		for (FixAllocationDetailNestedParty nestedParty : CollectionUtils.getIterable(allocationDetail.getNestedPartyList())) {
			quickfix.fix44.AllocationInstruction.NoAllocs.NoNestedPartyIDs nestedPartyID = new AllocationInstruction.NoAllocs.NoNestedPartyIDs();
			nestedPartyID.set(new NestedPartyID(nestedParty.getNestedPartyId()));
			nestedPartyID.set((NestedPartyRolesConverter.getInstance()).toField(nestedParty.getNestedPartyRole()));
			nestedPartyID.set((NestedPartyIdSourcesConverter.getInstance()).toField(nestedParty.getNestedPartyIDSource()));
			alloc.addGroup(nestedPartyID);
		}
	}


	private List<String> getCanceledExecutionIdList(List<FixExecutionReport> reportList) {
		List<String> result = new ArrayList<>();
		for (FixExecutionReport report : reportList) {
			if (ExecutionTypes.TRADE_CANCEL == report.getExecutionType()) {
				result.add(report.getExecutionReferenceId());
				result.add(report.getExecutionId());
			}
		}
		return result;
	}


	private BigDecimal formatNumber(BigDecimal value) {
		return QuickFixConverterUtils.getScaledBigDecimal(value);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
