package com.clifton.fix.quickfix.converter.ioi.fix44;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.ioi.FixIOIEntity;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.PriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProductsConverter;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.Message;
import quickfix.field.IOIID;
import quickfix.field.IOIQty;
import quickfix.field.IOIRefID;
import quickfix.field.IOITransType;
import quickfix.field.NoPartyIDs;
import quickfix.field.NoStipulations;
import quickfix.field.Price;
import quickfix.field.PriceType;
import quickfix.field.Product;
import quickfix.field.SettlDate;
import quickfix.field.Side;
import quickfix.field.Text;
import quickfix.field.TransactTime;

import java.time.ZoneOffset;
import java.util.List;


/**
 * Converts an incoming IOI QuickFix Message to a FixIOIEntity.
 */
public class DefaultIOIMessageConverter implements Converter<Message, FixIOIEntity> {

	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixIOIEntity convert(Message message) {
		try {
			FixIOIEntity entity = new FixIOIEntity();

			// handle security fields
			entity = QuickFixConverterUtils.convertToFixSecurityEntity(message, entity);

			if (message.isSetField(IOIID.FIELD)) {
				entity.setIoiId(message.getField(new IOIID()).getValue());
			}

			if (message.isSetField(IOITransType.FIELD)) {
				entity.setIoiTransactionType(Character.toString(message.getField(new IOITransType()).getValue()));
			}

			if (message.isSetField(IOIRefID.FIELD)) {
				entity.setIoiRefId(message.getField(new IOIRefID()).getValue());
			}

			if (message.isSetField(Product.FIELD)) {
				entity.setProduct(ProductsConverter.getInstance().fromField((Product) message.getField(new Product())));
			}

			if (message.isSetField(SettlDate.FIELD)) {
				entity.setSettlementDate(DateUtils.toDate(message.getField(new SettlDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}

			if (message.isSetField(Side.FIELD)) {
				OrderSides orderSide = OrderSideConverter.getInstance().fromField((Side) message.getField(new Side()));
				entity.setSide(orderSide);
			}

			if (message.isSetField(IOIQty.FIELD)) {
				entity.setIoiQuantity(message.getField(new IOIQty()).getValue());
			}

			if (message.isSetField(PriceType.FIELD)) {
				entity.setPriceType(PriceTypesConverter.getInstance().fromField((PriceType) message.getField(new PriceType())));
			}

			if (message.isSetField(Price.FIELD)) {
				entity.setPrice(message.getField(new Price()).getValue());
			}

			if (message.isSetField(TransactTime.FIELD)) {
				entity.setTransactionTime(DateUtils.asUtilDate(message.getField(new TransactTime()).getValue(), ZoneOffset.UTC));
			}

			if (message.isSetField(Text.FIELD)) {
				entity.setText(message.getField(new Text()).getValue());
			}

			if (message.isSetField(NoStipulations.FIELD)) {
				entity.setStipulationList(QuickFixMessageUtils.getStipulationList(message));
			}

			if (message.isSetField(NoPartyIDs.FIELD)) {
				List<FixParty> fixPartyList = getFixMessageHandler().getFixPartyList(message, entity);
				if (!CollectionUtils.isEmpty(fixPartyList)) {
					entity.setPartyList(fixPartyList);
				}
			}

			return entity;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix IOI Message to FixIOIEntity.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
