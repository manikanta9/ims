package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.Products;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.Product;


public class ProductsConverter extends BaseQuickFixFieldConverter<Products, Product> {


	private static final Lazy<ProductsConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(ProductsConverter::new);


	public static ProductsConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<Products, Product> map) {
		Products[] sources = Products.values();
		for (Products source : sources) {
			map.put(source, new Product(source.getCode()));
		}
	}
}
