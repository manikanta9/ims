package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.PutOrCall;


public class PutOrCallsConverter extends BaseQuickFixFieldConverter<PutOrCalls, PutOrCall> {


	private static final Lazy<PutOrCallsConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(PutOrCallsConverter::new);


	public static PutOrCallsConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PutOrCalls, PutOrCall> map) {
		PutOrCalls[] sources = PutOrCalls.values();
		for (PutOrCalls source : sources) {
			map.put(source, new PutOrCall(source.getCode()));
		}
	}
}
