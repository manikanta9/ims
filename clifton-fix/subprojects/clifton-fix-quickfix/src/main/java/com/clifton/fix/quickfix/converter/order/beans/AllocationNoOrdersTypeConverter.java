package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocNoOrdersType;


public class AllocationNoOrdersTypeConverter extends BaseQuickFixFieldConverter<AllocationNoOrdersTypes, AllocNoOrdersType> {


	private static final Lazy<AllocationNoOrdersTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationNoOrdersTypeConverter::new);


	public static AllocationNoOrdersTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationNoOrdersTypes, AllocNoOrdersType> map) {
		map.put(AllocationNoOrdersTypes.NOT_SPECIFIED, new AllocNoOrdersType(AllocNoOrdersType.NOT_SPECIFIED));
		map.put(AllocationNoOrdersTypes.EXPLICIT_LIST_PROVIDED, new AllocNoOrdersType(AllocNoOrdersType.EXPLICIT_LIST_PROVIDED));
	}
}
