package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.AssertUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.Field;
import quickfix.Message;
import quickfix.StringField;


/**
 * The FieldOverrideFixTagModifier overrides the value of a specified FIX Tag (field) with a new value.
 */
public class FieldOverrideFixTagModifier implements FixTagModifierBean<Message> {

	public Integer fieldTag;
	public String newFieldValue;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);
		AssertUtils.assertNotNull(getFieldTag(), "Field Tag value cannot be null.");
		AssertUtils.assertNotEmpty(getNewFieldValue(), "New Field Value cannot be null.");

		if (dd.isHeaderField(getFieldTag()) && message.getHeader().isSetField(getFieldTag())) {
			message.getHeader().removeField(getFieldTag());
		}
		else if (message.isSetField(getFieldTag())) {
			message.removeField(getFieldTag());
		}

		Field<?> field = new StringField(getFieldTag(), getNewFieldValue());
		if (dd.isHeaderField(getFieldTag())) {
			message.getHeader().setField(field.getTag(), field);
		}
		else {
			message.setField(field.getTag(), field);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(Integer fieldTag) {
		this.fieldTag = fieldTag;
	}


	public String getNewFieldValue() {
		return this.newFieldValue;
	}


	public void setNewFieldValue(String newFieldValue) {
		this.newFieldValue = newFieldValue;
	}
}
