package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedParty;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.CommissionTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.HandlingInstructionsConverter;
import com.clifton.fix.quickfix.converter.order.beans.NestedPartyIdSourcesConverter;
import com.clifton.fix.quickfix.converter.order.beans.NestedPartyRolesConverter;
import com.clifton.fix.quickfix.converter.order.beans.OpenClosesConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProcessCodeConverter;
import com.clifton.fix.quickfix.converter.order.beans.TimeInForceConverter;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.Group;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.Account;
import quickfix.field.AllocAccount;
import quickfix.field.AllocNetMoney;
import quickfix.field.AllocQty;
import quickfix.field.ClOrdID;
import quickfix.field.CommType;
import quickfix.field.Commission;
import quickfix.field.ExDestination;
import quickfix.field.ExpireTime;
import quickfix.field.HandlInst;
import quickfix.field.MaturityDate;
import quickfix.field.NestedPartyID;
import quickfix.field.NestedPartyIDSource;
import quickfix.field.NestedPartyRole;
import quickfix.field.NoAllocs;
import quickfix.field.NoNestedPartyIDs;
import quickfix.field.OpenClose;
import quickfix.field.OrdType;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.Price;
import quickfix.field.ProcessCode;
import quickfix.field.SenderSubID;
import quickfix.field.SettlDate;
import quickfix.field.Side;
import quickfix.field.Text;
import quickfix.field.TimeInForce;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.fix44.AllocationInstruction;
import quickfix.fix44.NewOrderSingle;

import java.math.BigDecimal;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;


public class BaseOrderConverter {

	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected <T extends Message> T fromFixOrder(FixOrder value, T order, Class<?> noPartyIDsClass, Class<?> noPartySubIDsClass) {
		//NewOrderSingle newOrderSingle = new NewOrderSingle();
		order = QuickFixConverterUtils.convertFromFixSecurityEntity(value, order);

		String alias = getFixMessageHandler().getFixSenderSubIdForFixEntity(value, order, true);
		if (!StringUtils.isEmpty(alias)) {
			order.getHeader().setField(new SenderSubID(alias));
		}

		if (!StringUtils.isEmpty(value.getClientOrderStringId())) {
			order.setField(new ClOrdID(value.getClientOrderStringId()));
		}
		if (!StringUtils.isEmpty(value.getOriginalClientOrderStringId())) {
			order.setField(new OrigClOrdID(value.getOriginalClientOrderStringId()));
		}
		order.setField((HandlingInstructionsConverter.getInstance()).toField(value.getHandlingInstructions()));
		if (!StringUtils.isEmpty(value.getTenor())) {
			// NOTE: 6215 is the Bloomberg field.  If there is a more standard field, this should be changed to that, and it should be moved be the tag modifiers on the FIX server.
			order.setField(new StringField(6215, value.getTenor()));
		}
		if (!StringUtils.isEmpty(value.getExDestination())) {
			order.setField(new ExDestination(value.getExDestination()));
		}
		order.setField((OrderSideConverter.getInstance()).toField(value.getSide()));
		if (value.getOpenClose() != null) {
			order.setField((OpenClosesConverter.getInstance()).toField(value.getOpenClose()));
		}
		order.setField(new OrderQty(setBigDecimalScale(value.getOrderQty().abs())));
		order.setField((OrderTypeConverter.getInstance()).toField(value.getOrderType()));
		if (value.getPrice() != null) {
			order.setField(new Price(setBigDecimalScale(value.getPrice())));
		}
		order.setField((TimeInForceConverter.getInstance()).toField(value.getTimeInForce()));
		if (value.getTimeInForce() == com.clifton.fix.order.beans.TimeInForceTypes.GTD) {
			order.setField(new ExpireTime(DateUtils.asLocalDateTime(value.getExpireTime(), ZoneOffset.UTC)));
		}
		if (!StringUtils.isEmpty(value.getAccount())) {
			order.setField(new Account(value.getAccount()));
		}
		order.setField(new TransactTime(DateUtils.asLocalDateTime(value.getTransactionTime(), ZoneOffset.UTC)));
		if (!StringUtils.isEmpty(value.getText())) {
			order.setField(new Text(value.getText()));
		}
		if (value.getTradeDate() != null) {
			order.setField(new TradeDate(DateUtils.fromDate(value.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}
		if (value.getSettlementDate() != null) {
			order.setField(new SettlDate(DateUtils.fromDate(value.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}
		if (value.getMaturityDate() != null) {
			order.setField(new MaturityDate(DateUtils.fromDate(value.getMaturityDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}

		QuickFixMessageUtils.addFixPartyList(order, value.getPartyList(), noPartyIDsClass, noPartySubIDsClass);

		addAllocationToOrder(order, value);
		return order;
	}


	private BigDecimal setBigDecimalScale(BigDecimal value) {
		return QuickFixConverterUtils.getScaledBigDecimal(value);
	}


	private <T extends Message> void addAllocationToOrder(T order, FixOrder value) {
		if (value.getFixAllocationDetailList() != null) {
			for (FixAllocationDetail allocationDetail : CollectionUtils.getIterable(value.getFixAllocationDetailList())) {
				quickfix.fix44.NewOrderSingle.NoAllocs alloc = new quickfix.fix44.NewOrderSingle.NoAllocs();

				alloc.setField(new AllocAccount(allocationDetail.getAllocationAccount()));
				alloc.setField(new AllocQty(setBigDecimalScale(allocationDetail.getAllocationShares())));

				addAllocationNestedParties(allocationDetail, alloc);
				order.addGroup(alloc);
			}
		}
	}


	private void addAllocationNestedParties(FixAllocationDetail allocationDetail, quickfix.fix44.NewOrderSingle.NoAllocs orderAllocation) {
		for (FixAllocationDetailNestedParty nestedParty : CollectionUtils.getIterable(allocationDetail.getNestedPartyList())) {
			quickfix.fix44.AllocationInstruction.NoAllocs.NoNestedPartyIDs nestedPartyID = new AllocationInstruction.NoAllocs.NoNestedPartyIDs();
			nestedPartyID.set(new NestedPartyID(nestedParty.getNestedPartyId()));
			nestedPartyID.set((NestedPartyRolesConverter.getInstance()).toField(nestedParty.getNestedPartyRole()));
			nestedPartyID.set((NestedPartyIdSourcesConverter.getInstance()).toField(nestedParty.getNestedPartyIDSource()));
			orderAllocation.addGroup(nestedPartyID);
		}
	}


	protected <T extends Message, F extends FixOrder> F toFixOrder(T from, F order) {
		try {
			order = QuickFixConverterUtils.convertToFixSecurityEntity(from, order);
			if (from.getHeader().isSetField(new SenderSubID())) {
				order.setFixSenderSubId(from.getHeader().getField(new SenderSubID()).getValue());
				order.setSenderUserName(getFixMessageHandler().getSenderUserNameForFixEntity(order, from));
			}

			order.setClientOrderStringId(from.getField(new ClOrdID()).getValue());
			if (from.isSetField(new OrigClOrdID())) {
				order.setOriginalClientOrderStringId(from.getField(new OrigClOrdID()).getValue());
			}

			if (from.isSetField(new HandlInst())) {
				order.setHandlingInstructions((HandlingInstructionsConverter.getInstance()).fromField((HandlInst) from.getField(new HandlInst())));
			}

			order.setSide((OrderSideConverter.getInstance()).fromField((Side) from.getField(new Side())));
			if (from.isSetField(new OpenClose())) {
				order.setOpenClose((OpenClosesConverter.getInstance()).fromField((OpenClose) from.getField(new OpenClose())));
			}
			order.setOrderQty(from.getField(new OrderQty()).getValue());
			order.setOrderType((OrderTypeConverter.getInstance()).fromField((OrdType) from.getField(new OrdType())));
			if (from.isSetField(new Price())) {
				order.setPrice(from.getField(new Price()).getValue());
			}
			order.setTimeInForce((TimeInForceConverter.getInstance()).fromField((TimeInForce) from.getField(new TimeInForce())));
			if (from.isSetField(new ExpireTime())) {
				order.setExpireTime(DateUtils.asUtilDate(from.getField(new ExpireTime()).getValue(), ZoneOffset.UTC));
			}
			if (from.isSetField(new Account())) {
				order.setAccount(from.getField(new Account()).getValue());
			}
			order.setTransactionTime(DateUtils.asUtilDate(from.getField(new TransactTime()).getValue(), ZoneOffset.UTC));

			if (from.isSetField(new Text())) {
				order.setText(from.getField(new Text()).getValue());
			}

			if (from.isSetField(new TradeDate())) {
				order.setTradeDate(DateUtils.toDate(from.getField(new TradeDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (from.isSetField(new SettlDate())) {
				order.setSettlementDate(DateUtils.toDate(from.getField(new SettlDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}

			order.setPartyList(getFixMessageHandler().getFixPartyList(from, order));

			getAllocationsFromOrder(from, order);

			return order;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + NewOrderSingle.class + "] to [" + FixOrder.class + "].", e);
		}
	}


	private void getAllocationsFromOrder(Message allocation, FixOrder value) {
		try {
			List<FixAllocationDetail> detailList = new ArrayList<>();
			for (Group alloc : CollectionUtils.getIterable(allocation.getGroups(NoAllocs.FIELD))) {
				FixAllocationDetail allocationDetail = new FixAllocationDetail();
				allocationDetail.setAllocationAccount(alloc.getField(new AllocAccount()).getValue());
				allocationDetail.setAllocationShares(alloc.getField(new AllocQty()).getValue());
				if (alloc.isSetField(new ProcessCode())) {
					allocationDetail.setProcessCode((ProcessCodeConverter.getInstance()).fromField((ProcessCode) alloc.getField(new ProcessCode())));
				}
				if (alloc.isSetField(new AllocNetMoney())) {
					allocationDetail.setAllocationNetMoney(alloc.getField(new AllocNetMoney()).getValue());
				}
				if (alloc.isSetField(new Commission())) {
					allocationDetail.setCommission(alloc.getField(new Commission()).getValue());
				}
				if (alloc.isSetField(new CommType())) {
					allocationDetail.setCommissionType((CommissionTypesConverter.getInstance()).fromField((CommType) alloc.getField(new CommType())));
				}

				List<FixAllocationDetailNestedParty> nestedPartyList = new ArrayList<>();
				List<Group> nestedPartyIdGroups = alloc.getGroups(NoNestedPartyIDs.FIELD);
				for (Group nestedPartyIdGroup : CollectionUtils.getIterable(nestedPartyIdGroups)) {
					FixAllocationDetailNestedParty party = new FixAllocationDetailNestedParty();
					if (nestedPartyIdGroup.isSetField(NestedPartyID.FIELD)) {
						party.setNestedPartyId(nestedPartyIdGroup.getField(new NestedPartyID()).getValue());
					}
					if (nestedPartyIdGroup.isSetField(NestedPartyRole.FIELD)) {
						party.setNestedPartyRole((NestedPartyRolesConverter.getInstance()).fromField((NestedPartyRole) nestedPartyIdGroup.getField(new NestedPartyRole())));
					}
					if (nestedPartyIdGroup.isSetField(NestedPartyIDSource.FIELD)) {
						party.setNestedPartyIDSource((NestedPartyIdSourcesConverter.getInstance()).fromField((NestedPartyIDSource) nestedPartyIdGroup.getField(new NestedPartyIDSource())));
					}
					nestedPartyList.add(party);
				}
				allocationDetail.setNestedPartyList(nestedPartyList);

				detailList.add(allocationDetail);
			}
			value.setFixAllocationDetailList(detailList);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + NoAllocs.class + "] to a list of [" + FixAllocationDetail.class + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
