package com.clifton.fix.quickfix.converter.order.beans;

import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PartySubIDTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.PartySubIDType;


public class PartySubIDTypesConverter extends BaseQuickFixFieldConverter<PartySubIDTypes, PartySubIDType> {


	private static final Lazy<PartySubIDTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(PartySubIDTypesConverter::new);


	public static PartySubIDTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PartySubIDTypes, PartySubIDType> map) {

		PartySubIDTypes[] sources = PartySubIDTypes.values();
		for (PartySubIDTypes source : sources) {
			map.put(source, new PartySubIDType(source.getCode()));
		}
	}
}
