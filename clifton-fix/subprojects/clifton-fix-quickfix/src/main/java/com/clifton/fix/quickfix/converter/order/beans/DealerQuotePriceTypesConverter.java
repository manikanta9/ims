package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CompDealerQuotePriceTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.DealerQuotePriceType;


public class DealerQuotePriceTypesConverter extends BaseQuickFixFieldConverter<CompDealerQuotePriceTypes, DealerQuotePriceType> {

	private static final Lazy<DealerQuotePriceTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(DealerQuotePriceTypesConverter::new);


	public static DealerQuotePriceTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CompDealerQuotePriceTypes, DealerQuotePriceType> map) {

		CompDealerQuotePriceTypes[] sources = CompDealerQuotePriceTypes.values();
		for (CompDealerQuotePriceTypes source : sources) {
			map.put(source, new DealerQuotePriceType(source.getCode()));
		}
	}
}
