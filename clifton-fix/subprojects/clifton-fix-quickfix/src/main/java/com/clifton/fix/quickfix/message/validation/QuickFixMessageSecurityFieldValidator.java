package com.clifton.fix.quickfix.message.validation;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import quickfix.Message;
import quickfix.field.Symbol;

import java.util.Collections;
import java.util.Optional;
import java.util.regex.Pattern;


/**
 * <code>QuickFixServerMessageSecurityValidator</code> validates instrument ID consistency between the new order single and its
 * corresponding execution report(s).
 * <p>
 * 55 = Symbol (required) - "human understood" representation of the security, "[N/A]" for products which do not have a symbol.
 * 48 = SecurityID - value can be specified if no symbol exists
 */
public class QuickFixMessageSecurityFieldValidator extends QuickFixMessageFieldValidator {

	private static Pattern symbolNotSetPattern = Pattern.compile("^([nN][\\\\/][aA])|\\[([nN][\\\\/][aA])\\]$");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String isValid(Message m1, Message m2) {
		String results = "";
		Optional<String> o1 = QuickFixMessageUtils.getStringFieldValue(m1, getField(), "");
		Optional<String> o2 = QuickFixMessageUtils.getStringFieldValue(m2, getField(), "");
		if (getField() == Symbol.FIELD) {
			if (o1.isPresent()) {
				o1 = symbolNotSetPattern.matcher(o1.get()).matches() ? Optional.empty() : o1;
			}
			if (o2.isPresent()) {
				o2 = symbolNotSetPattern.matcher(o2.get()).matches() ? Optional.empty() : o2;
			}
		}
		if (o1.isPresent() && o2.isPresent() && !StringUtils.isEqual(o1.get(), o2.get())) {
			results = formatFailureMessage(
					Collections.singletonList(o1.isPresent() ? o1.get() : FIELD_NOT_PRESENT),
					Collections.singletonList(o2.isPresent() ? o2.get() : FIELD_NOT_PRESENT)
			);
		}
		return results;
	}
}
