package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.UnderlyingSecurityType;


public class UnderlyingSecurityTypeConverter extends BaseQuickFixFieldConverter<SecurityTypes, UnderlyingSecurityType> {


	private static final Lazy<UnderlyingSecurityTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(UnderlyingSecurityTypeConverter::new);


	public static UnderlyingSecurityTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<SecurityTypes, UnderlyingSecurityType> map) {
		SecurityTypes[] sources = SecurityTypes.values();
		for (SecurityTypes source : sources) {
			map.put(source, new UnderlyingSecurityType(source.getCode()));
		}
	}
}
