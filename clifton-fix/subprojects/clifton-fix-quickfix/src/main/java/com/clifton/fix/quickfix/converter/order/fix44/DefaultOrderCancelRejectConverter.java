package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixOrderCancelReject;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.CancelRejectReasonConverter;
import com.clifton.fix.quickfix.converter.order.beans.CancelRejectResponseToConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderStatusConverter;
import quickfix.Message;
import quickfix.field.ClOrdID;
import quickfix.field.CxlRejReason;
import quickfix.field.CxlRejResponseTo;
import quickfix.field.OrdStatus;
import quickfix.field.OrigClOrdID;
import quickfix.fix44.OrderCancelReject;


public class DefaultOrderCancelRejectConverter implements Converter<Message, FixOrderCancelReject> {

	@Override
	public FixOrderCancelReject convert(Message message) {
		try {
			FixOrderCancelReject result = new FixOrderCancelReject();

			result = QuickFixConverterUtils.convertToFixEntity(message, result);
			result.setClientOrderStringId(message.getField(new ClOrdID()).getValue());
			//result.setClientOrderId(FixUtils.parseUniqueId(result.getClientOrderStringId()));

			if (message.isSetField(OrigClOrdID.FIELD)) {
				String originalClientOrderStringId = message.getField(new OrigClOrdID()).getValue();
				if (!"UNKNOWN".equals(originalClientOrderStringId)) {
					result.setOriginalClientOrderStringId(originalClientOrderStringId);
					//result.setOriginalClientOrderId(FixUtils.parseUniqueId(originalClientOrderStringId));
				}
			}

			if (message.isSetField(CxlRejResponseTo.FIELD)) {
				result.setCancelRejectResponseTo((CancelRejectResponseToConverter.getInstance()).fromField((CxlRejResponseTo) message.getField(new CxlRejResponseTo())));
			}
			if (message.isSetField(CxlRejReason.FIELD)) {
				result.setCancelRejectReason((CancelRejectReasonConverter.getInstance()).fromField((CxlRejReason) message.getField(new CxlRejReason())));
			}
			if (message.isSetField(OrdStatus.FIELD)) {
				result.setStatus((OrderStatusConverter.getInstance()).fromField((OrdStatus) message.getField(new OrdStatus())));
			}
			return result;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + OrderCancelReject.class + "] to [" + FixOrderCancelReject.class + "].", e);
		}
	}
}
