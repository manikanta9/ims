package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.TimeInForce;


public class TimeInForceConverter extends BaseQuickFixFieldConverter<TimeInForceTypes, TimeInForce> {


	private static final Lazy<TimeInForceConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(TimeInForceConverter::new);


	public static TimeInForceConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<TimeInForceTypes, TimeInForce> map) {
		map.put(com.clifton.fix.order.beans.TimeInForceTypes.DAY, new TimeInForce(TimeInForce.DAY));
		map.put(com.clifton.fix.order.beans.TimeInForceTypes.GTC, new TimeInForce(TimeInForce.GOOD_TILL_CANCEL));
		map.put(com.clifton.fix.order.beans.TimeInForceTypes.IOC, new TimeInForce(TimeInForce.IMMEDIATE_OR_CANCEL));
		map.put(com.clifton.fix.order.beans.TimeInForceTypes.GTD, new TimeInForce(TimeInForce.GOOD_TILL_DATE));
	}
}
