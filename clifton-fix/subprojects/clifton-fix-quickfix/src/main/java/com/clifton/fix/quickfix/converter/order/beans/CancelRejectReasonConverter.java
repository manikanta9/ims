package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CancelRejectReasons;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.CxlRejReason;


public class CancelRejectReasonConverter extends BaseQuickFixFieldConverter<CancelRejectReasons, CxlRejReason> {


	private static final Lazy<CancelRejectReasonConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(CancelRejectReasonConverter::new);


	public static CancelRejectReasonConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CancelRejectReasons, CxlRejReason> map) {
		map.put(CancelRejectReasons.TOO_LATE_TO_CANCEL, new CxlRejReason(CxlRejReason.TOO_LATE_TO_CANCEL));
		map.put(CancelRejectReasons.UNKNOWN_ORDER, new CxlRejReason(CxlRejReason.UNKNOWN_ORDER));
		map.put(CancelRejectReasons.BROKER_EXCHANGE_OPTION, new CxlRejReason(CxlRejReason.BROKER_EXCHANGE_OPTION));
		map.put(CancelRejectReasons.ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS, new CxlRejReason(CxlRejReason.ORDER_ALREADY_IN_PENDING_CANCEL_OR_PENDING_REPLACE_STATUS));
		map.put(CancelRejectReasons.UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST, new CxlRejReason(CxlRejReason.UNABLE_TO_PROCESS_ORDER_MASS_CANCEL_REQUEST));
		map.put(CancelRejectReasons.ORIGORDMODTIME_DID_NOT_MATCH_LAST_TRANSACTTIME_OF_ORDER, new CxlRejReason(CxlRejReason.ORIGORDMODTIME_OF_ORDER));
		map.put(CancelRejectReasons.DUPLICATE_CLORDID_RECEIVED, new CxlRejReason(CxlRejReason.DUPLICATE_CLORDID_RECEIVED));
		map.put(CancelRejectReasons.INVALID_PRICE_INCREMENT, new CxlRejReason(CxlRejReason.INVALID_PRICE_INCREMENT));
		map.put(CancelRejectReasons.OTHER, new CxlRejReason(CxlRejReason.OTHER));
	}
}
