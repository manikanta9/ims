package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.HandlingInstructionsConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.Message;
import quickfix.field.ClOrdID;
import quickfix.field.HandlInst;
import quickfix.field.OrigClOrdID;
import quickfix.field.SenderSubID;
import quickfix.field.Side;
import quickfix.field.TransactTime;
import quickfix.fix44.OrderCancelRequest;

import java.time.ZoneOffset;


public class DefaultOrderCancelRequestConverter implements Converter<Message, FixOrderCancelRequest> {

	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderCancelRequest convert(Message from) {
		try {
			FixOrderCancelRequest result = new FixOrderCancelRequest();

			result = QuickFixConverterUtils.convertToFixSecurityEntity(from, result);
			result.setClientOrderStringId(from.getField(new ClOrdID()).getValue());

			if (from.isSetField(new OrigClOrdID())) {
				result.setOriginalClientOrderStringId(from.getField(new OrigClOrdID()).getValue());
			}

			result.setTransactionTime(DateUtils.asUtilDate(from.getField(new TransactTime()).getValue(), ZoneOffset.UTC));

			result.setHandlingInstructions((HandlingInstructionsConverter.getInstance()).fromField((HandlInst) from.getField(new HandlInst())));
			result.setFixSenderSubId(from.getHeader().getField(new SenderSubID()).getValue());
			result.setSenderUserName(getFixMessageHandler().getSenderUserNameForFixEntity(result, from));

			if (from.isSetField(new Side())) {
				result.setSide((OrderSideConverter.getInstance()).fromField((Side) from.getField(new Side())));
			}

			return result;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + OrderCancelRequest.class + "] to [" + FixOrderCancelRequest.class + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
