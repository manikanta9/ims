package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedParty;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.AllocationCancelReplaceReasonsConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationLinkTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationStatusConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationTransactionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.CommissionTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.NestedPartyIdSourcesConverter;
import com.clifton.fix.quickfix.converter.order.beans.NestedPartyRolesConverter;
import com.clifton.fix.quickfix.converter.order.beans.OpenClosesConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.PriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProcessCodeConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProductsConverter;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.Account;
import quickfix.field.AllocAccount;
import quickfix.field.AllocCancReplaceReason;
import quickfix.field.AllocID;
import quickfix.field.AllocLinkID;
import quickfix.field.AllocLinkType;
import quickfix.field.AllocNetMoney;
import quickfix.field.AllocQty;
import quickfix.field.AllocStatus;
import quickfix.field.AllocTransType;
import quickfix.field.AllocType;
import quickfix.field.AvgPx;
import quickfix.field.ClOrdID;
import quickfix.field.CommType;
import quickfix.field.Commission;
import quickfix.field.FutSettDate;
import quickfix.field.GrossTradeAmt;
import quickfix.field.NestedPartyID;
import quickfix.field.NestedPartyIDSource;
import quickfix.field.NestedPartyRole;
import quickfix.field.NetMoney;
import quickfix.field.NoAllocs;
import quickfix.field.NoNestedPartyIDs;
import quickfix.field.NoOrders;
import quickfix.field.OpenClose;
import quickfix.field.PriceType;
import quickfix.field.ProcessCode;
import quickfix.field.Product;
import quickfix.field.RefAllocID;
import quickfix.field.SenderSubID;
import quickfix.field.SettlDate;
import quickfix.field.Shares;
import quickfix.field.Side;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.fix44.AllocationInstruction;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>DefaultAllocationInstructionConverter</code> converts a AllocationInstruction object to FixAllocationInstruction.
 * <p>
 * NOTE: Not all data contained in the NoOrders and NoExecs groups is used.  The returned will have a list of FixOrder objects, but
 * only the clientOrderId and clientOrderStringId properties will be populated.  In order to get the orders and execution report that
 * where allocated, each of the returned FixOrder object must be populated by some other method.
 *
 * @author mwacker
 */
public class DefaultAllocationInstructionConverter implements Converter<Message, FixAllocationInstruction> {

	private FixMessageHandler<Message> fixMessageHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationInstruction convert(Message from) {
		try {
			FixAllocationInstruction result = new FixAllocationInstruction();

			result = QuickFixConverterUtils.convertToFixSecurityEntity(from, result);
			result.setAllocationStringId(from.getField(new AllocID()).getValue());

			result.setAllocationTransactionType((AllocationTransactionTypeConverter.getInstance()).fromField((AllocTransType) from.getField(new AllocTransType())));
			if (from.getHeader().isSetField(new SenderSubID())) {
				result.setFixSenderSubId(from.getHeader().getField(new SenderSubID()).getValue());
				result.setSenderUserName(getFixMessageHandler().getSenderUserNameForFixEntity(result, from));
			}
			if (from.isSetField(new AllocStatus())) {
				result.setAllocationStatus((AllocationStatusConverter.getInstance()).fromField((AllocStatus) from.getField(new AllocStatus())));
			}
			if (from.isSetField(new Account())) {
				result.setAccount(from.getField(new Account()).getValue());
			}
			if (from.isSetField(new RefAllocID())) {
				result.setReferenceAllocationStringId(from.getField(new RefAllocID()).getValue());
			}
			if (from.isSetField(new AllocLinkID())) {
				result.setAllocationLinkStringId(from.getField(new AllocLinkID()).getValue());
			}
			if (from.isSetField(new AllocLinkType())) {
				result.setAllocationLinkType((AllocationLinkTypeConverter.getInstance()).fromField((AllocLinkType) from.getField(new AllocLinkType())));
			}

			if (from.isSetField(new AllocCancReplaceReason())) {
				result.setAllocationCancelReplaceReason((AllocationCancelReplaceReasonsConverter.getInstance()).fromField((AllocCancReplaceReason) from.getField(new AllocCancReplaceReason())));
			}

			if (from.isSetField(new SettlDate())) {
				result.setSettlementDate(DateUtils.toDate(from.getField(new SettlDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (from.isSetField(new AllocType())) {
				result.setAllocationType((AllocationTypesConverter.getInstance()).fromField((AllocType) from.getField(new AllocType())));
			}
			if (from.isSetField(new PriceType())) {
				result.setPriceType((PriceTypesConverter.getInstance()).fromField((PriceType) from.getField(new PriceType())));
			}

			// Load the FixOrder's for the message.
			List<String> addedOrders = new ArrayList<>();
			List<Group> orderGroups = from.getGroups(NoOrders.FIELD);
			List<FixOrder> orderList = new ArrayList<>();
			for (Group orderGroup : CollectionUtils.getIterable(orderGroups)) {
				String clOrdID = orderGroup.getField(new ClOrdID()).getValue();
				if (!addedOrders.contains(clOrdID)) {
					FixOrder order = new FixOrder();
					order.setClientOrderStringId(clOrdID);
					//order.setClientOrderId(FixUtils.parseUniqueId(clOrdID));
					orderList.add(order);
					addedOrders.add(clOrdID);
				}
			}
			result.setFixOrderList(orderList);

			if (from.isSetField(Side.FIELD)) {
				result.setSide((OrderSideConverter.getInstance()).fromField((Side) from.getField(new Side())));
			}
			if (from.isSetField(new OpenClose())) {
				result.setOpenClose((OpenClosesConverter.getInstance()).fromField((OpenClose) from.getField(new OpenClose())));
			}
			if (from.isSetField(Shares.FIELD)) {
				result.setShares(from.getField(new Shares()).getValue());
			}
			if (from.isSetField(AvgPx.FIELD)) {
				result.setAveragePrice(from.getField(new AvgPx()).getValue());
			}
			if (from.isSetField(TradeDate.FIELD)) {
				result.setTradeDate(DateUtils.toDate(from.getField(new TradeDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (from.isSetField(TransactTime.FIELD)) {
				result.setTransactionTime(DateUtils.asUtilDate(from.getField(new TransactTime()).getValue(), ZoneOffset.UTC));
			}
			if (from.isSetField(new FutSettDate())) {
				result.setFutureSettlementDate(DateUtils.toDate(from.getField(new FutSettDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (from.isSetField(new NetMoney())) {
				result.setNetMoney(from.getField(new NetMoney()).getValue());
			}
			if (from.isSetField(GrossTradeAmt.FIELD)) {
				result.setGrossTradeAmount(from.getField(new GrossTradeAmt()).getValue());
			}
			if (from.isSetField(Product.FIELD)) {
				result.setProduct((ProductsConverter.getInstance()).fromField((Product) from.getField(new Product())));
			}
			if (from.isSetField(AllocCancReplaceReason.FIELD)) {
				result.setAllocationCancelReplaceReason((AllocationCancelReplaceReasonsConverter.getInstance()).fromField((AllocCancReplaceReason) from.getField(new AllocCancReplaceReason())));
			}
			result.setPartyList(getFixMessageHandler().getFixPartyList(from, result));

			addAllocations(from, result);

			return result;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + AllocationInstruction.class + "] to [" + FixAllocationInstruction.class + "].", e);
		}
	}


	private void addAllocations(Message allocation, FixAllocationInstruction value) {
		try {
			List<FixAllocationDetail> detailList = new ArrayList<>();
			List<Group> allocationGroups = allocation.getGroups(NoAllocs.FIELD);
			for (Group allocationGroup : CollectionUtils.getIterable(allocationGroups)) {
				FixAllocationDetail allocationDetail = new FixAllocationDetail();
				allocationDetail.setAllocationAccount(allocationGroup.getField(new AllocAccount()).getValue());
				allocationDetail.setAllocationShares(allocationGroup.getField(new AllocQty()).getValue());
				if (allocationGroup.isSetField(ProcessCode.FIELD)) {
					allocationDetail.setProcessCode((ProcessCodeConverter.getInstance()).fromField((ProcessCode) allocationGroup.getField(new ProcessCode())));
				}

				if (allocationGroup.isSetField(new AllocNetMoney())) {
					allocationDetail.setAllocationNetMoney(allocationGroup.getField(new AllocNetMoney()).getValue());
				}
				if (allocationGroup.isSetField(new Commission())) {
					allocationDetail.setCommission(allocationGroup.getField(new Commission()).getValue());
				}
				if (allocationGroup.isSetField(new CommType())) {
					allocationDetail.setCommissionType((CommissionTypesConverter.getInstance()).fromField((CommType) allocationGroup.getField(new CommType())));
				}

				List<FixAllocationDetailNestedParty> nestedPartyList = new ArrayList<>();
				List<Group> nestedPartyIdGroups = allocationGroup.getGroups(NoNestedPartyIDs.FIELD);
				for (Group nestedPartyIdGroup : CollectionUtils.getIterable(nestedPartyIdGroups)) {
					FixAllocationDetailNestedParty party = new FixAllocationDetailNestedParty();
					if (nestedPartyIdGroup.isSetField(NestedPartyID.FIELD)) {
						party.setNestedPartyId(nestedPartyIdGroup.getField(new NestedPartyID()).getValue());
					}
					if (nestedPartyIdGroup.isSetField(NestedPartyRole.FIELD)) {
						party.setNestedPartyRole((NestedPartyRolesConverter.getInstance()).fromField((NestedPartyRole) nestedPartyIdGroup.getField(new NestedPartyRole())));
					}
					if (nestedPartyIdGroup.isSetField(NestedPartyIDSource.FIELD)) {
						party.setNestedPartyIDSource((NestedPartyIdSourcesConverter.getInstance()).fromField((NestedPartyIDSource) nestedPartyIdGroup.getField(new NestedPartyIDSource())));
					}
					nestedPartyList.add(party);
				}
				allocationDetail.setNestedPartyList(nestedPartyList);

				detailList.add(allocationDetail);
			}
			value.setFixAllocationDetailList(detailList);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + NoAllocs.class + "] to a list of [" + FixAllocationDetail.class + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
