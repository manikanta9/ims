package com.clifton.fix.quickfix.converter.order.beans;

import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.market.data.refresh.MarketDataActions;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.MDUpdateAction;


public class MarketDataActionConverter extends BaseQuickFixFieldConverter<MarketDataActions, MDUpdateAction> {

	private static final Lazy<MarketDataActionConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(MarketDataActionConverter::new);


	public static MarketDataActionConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<MarketDataActions, MDUpdateAction> map) {

		MarketDataActions[] sources = MarketDataActions.values();
		for (MarketDataActions source : sources) {
			map.put(source, new MDUpdateAction(source.getCode()));
		}
	}
}
