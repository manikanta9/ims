package com.clifton.fix.quickfix.converter.order.beans;

import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.FixNoteTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.NoteType;


public class NoteTypeConverter extends BaseQuickFixFieldConverter<FixNoteTypes, NoteType> {

	private static final Lazy<NoteTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(NoteTypeConverter::new);


	public static NoteTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<FixNoteTypes, NoteType> map) {

		FixNoteTypes[] noteTypes = FixNoteTypes.values();
		for (FixNoteTypes noteType : noteTypes) {
			map.put(noteType, new NoteType(noteType.getCode()));
		}
	}
}
