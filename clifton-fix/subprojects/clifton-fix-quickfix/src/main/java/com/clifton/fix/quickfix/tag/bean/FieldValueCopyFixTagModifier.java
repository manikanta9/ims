package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.AssertUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.Message;
import quickfix.StringField;


/**
 * The FieldValueCopyFixTagModifier copies the value in a specified fieldTag to another field tag.
 */
public class FieldValueCopyFixTagModifier implements FixTagModifierBean<Message> {

	private Integer fromFieldTag;
	private Integer toFieldTag;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		AssertUtils.assertNotNull(getFromFieldTag(), "From Field Tag value cannot be null.");
		AssertUtils.assertNotNull(getToFieldTag(), "To Field Tag value cannot be null.");
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);

		try {

			if (dd.isHeaderField(getFromFieldTag())) {
				String fromValue = message.getHeader().getField(new StringField(getFromFieldTag())).getValue();
				message.getHeader().setField(new StringField(getToFieldTag(), fromValue));
			}
			else {
				String fromValue = message.getField(new StringField(getFromFieldTag())).getValue();
				message.setField(new StringField(getToFieldTag(), fromValue));
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to modify message [" + message + "] in class [" + this.getClass() + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFromFieldTag() {
		return this.fromFieldTag;
	}


	public void setFromFieldTag(Integer fromFieldTag) {
		this.fromFieldTag = fromFieldTag;
	}


	public Integer getToFieldTag() {
		return this.toFieldTag;
	}


	public void setToFieldTag(Integer toFieldTag) {
		this.toFieldTag = toFieldTag;
	}
}
