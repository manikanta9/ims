package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.ExecutionTransactionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.ExecutionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.LastCapacityConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderCapacityConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderStatusConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.PriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProductsConverter;
import com.clifton.fix.quickfix.converter.order.beans.TimeInForceConverter;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import quickfix.field.AccruedInterestAmt;
import quickfix.field.AvgPx;
import quickfix.field.ClOrdID;
import quickfix.field.Concession;
import quickfix.field.CouponRate;
import quickfix.field.CumQty;
import quickfix.field.ExecID;
import quickfix.field.ExecRefID;
import quickfix.field.Factor;
import quickfix.field.GrossTradeAmt;
import quickfix.field.Issuer;
import quickfix.field.LastMkt;
import quickfix.field.LastPx;
import quickfix.field.LastShares;
import quickfix.field.LeavesQty;
import quickfix.field.NetMoney;
import quickfix.field.OrderID;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.Price;
import quickfix.field.SecondaryClOrdID;
import quickfix.field.SecondaryOrderID;
import quickfix.field.SettlCurrAmt;
import quickfix.field.SettlCurrency;
import quickfix.field.SettlDate;
import quickfix.field.Text;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.field.Yield;
import quickfix.fix44.ExecutionReport;

import java.time.ZoneOffset;


public class DefaultFixExecutionReportConverter implements Converter<FixExecutionReport, ExecutionReport> {


	@Override
	public ExecutionReport convert(FixExecutionReport entity) {
		try {
			ExecutionReport message = new ExecutionReport();
			message = QuickFixConverterUtils.convertFromFixSecurityEntity(entity, message);

			if (!StringUtils.isEmpty(entity.getOrderId())) {
				message.setField(new OrderID(entity.getOrderId()));
			}
			if (!StringUtils.isEmpty(entity.getSecondaryOrderId())) {
				message.setField(new SecondaryOrderID(entity.getSecondaryOrderId()));
			}
			if (!StringUtils.isEmpty(entity.getClientOrderStringId())) {
				message.setField(new ClOrdID(entity.getClientOrderStringId()));
			}
			if (!StringUtils.isEmpty(entity.getOriginalClientOrderStringId())) {
				message.setField(new OrigClOrdID(entity.getOriginalClientOrderStringId()));
			}
			if (!StringUtils.isEmpty(entity.getSecondaryClientOrderStringId())) {
				message.setField(new SecondaryClOrdID(entity.getSecondaryClientOrderStringId()));
			}

			if (entity.getExecutionTransactionType() != null) {
				message.setField((ExecutionTransactionTypeConverter.getInstance()).toField(entity.getExecutionTransactionType()));
			}
			if (entity.getExecutionType() != null) {
				message.setField((ExecutionTypeConverter.getInstance()).toField(entity.getExecutionType()));
			}
			if (!StringUtils.isEmpty(entity.getExecutionId())) {
				message.setField(new ExecID(entity.getExecutionId()));
			}
			if (!StringUtils.isEmpty(entity.getExecutionReferenceId())) {
				message.setField(new ExecRefID(entity.getExecutionReferenceId()));
			}
			if (entity.getOrderStatus() != null) {
				message.setField((OrderStatusConverter.getInstance()).toField(entity.getOrderStatus()));
			}
			if (entity.getSide() != null) {
				message.setField((OrderSideConverter.getInstance()).toField(entity.getSide()));
			}
			if (entity.getOrderQuantity() != null) {
				message.setField(new OrderQty(entity.getOrderQuantity()));
			}
			if (entity.getOrderType() != null) {
				message.setField((OrderTypeConverter.getInstance()).toField(entity.getOrderType()));
			}
			if (entity.getPrice() != null) {
				message.setField(new Price(entity.getPrice()));
			}

			if (entity.getTimeInForceType() != null) {
				message.setField((TimeInForceConverter.getInstance()).toField(entity.getTimeInForceType()));
			}
			if (entity.getOrderCapacity() != null) {
				message.setField((OrderCapacityConverter.getInstance()).toField(entity.getOrderCapacity()));
			}
			if (entity.getLastShares() != null) {
				message.setField(new LastShares(entity.getLastShares()));
			}
			if (entity.getLastPrice() != null) {
				message.setField(new LastPx(entity.getLastPrice()));
			}
			if (!StringUtils.isEmpty(entity.getLastMarket())) {
				message.setField(new LastMkt(entity.getLastMarket()));
			}
			if (entity.getCumulativeQuantity() != null) {
				message.setField(new CumQty(entity.getCumulativeQuantity()));
			}
			if (entity.getLeavesQuantity() != null) {
				message.setField(new LeavesQty(entity.getLeavesQuantity()));
			}
			if (entity.getAveragePrice() != null) {
				message.setField(new AvgPx(entity.getAveragePrice()));
			}
			if (entity.getLastCapacity() != null) {
				message.setField((LastCapacityConverter.getInstance()).toField(entity.getLastCapacity()));
			}
			if (entity.getTransactionTime() != null) {
				message.setField(new TransactTime(DateUtils.asLocalDateTime(entity.getTransactionTime(), ZoneOffset.UTC)));
			}
			if (!StringUtils.isEmpty(entity.getText())) {
				message.setField(new Text(entity.getText()));
			}
			if (entity.getPriceType() != null) {
				message.setField((PriceTypesConverter.getInstance()).toField(entity.getPriceType()));
			}
			if (entity.getNetMoney() != null) {
				message.setField(new NetMoney(entity.getNetMoney()));
			}
			if (entity.getGrossTradeAmount() != null) {
				message.setField(new GrossTradeAmt(entity.getGrossTradeAmount()));
			}
			if (entity.getAccruedInterestAmount() != null) {
				message.setField(new AccruedInterestAmt(entity.getAccruedInterestAmount()));
			}
			if (entity.getCouponRate() != null) {
				message.setField(new CouponRate(entity.getCouponRate().doubleValue()));
			}
			if (entity.getYield() != null) {
				message.setField(new Yield(entity.getYield().doubleValue()));
			}
			if (entity.getConcession() != null) {
				message.setField(new Concession(entity.getConcession()));
			}
			if (!StringUtils.isEmpty(entity.getIssuer())) {
				message.setField(new Issuer(entity.getIssuer()));
			}
			if (entity.getProduct() != null) {
				message.setField((ProductsConverter.getInstance()).toField(entity.getProduct()));
			}
			if (entity.getTradeDate() != null) {
				message.setField(new TradeDate(DateUtils.fromDate(entity.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
			}
			if (entity.getSettlementDate() != null) {
				message.setField(new SettlDate(DateUtils.fromDate(entity.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
			}
			if (entity.getSettlementCurrencyAmount() != null) {
				message.setField(new SettlCurrAmt(entity.getSettlementCurrencyAmount()));
			}
			if (!StringUtils.isEmpty(entity.getSettlementCurrency())) {
				message.setField(new SettlCurrency(entity.getSettlementCurrency()));
			}
			if (entity.getIndexRatio() != null) {
				message.setField(new Factor(entity.getIndexRatio().doubleValue()));
			}

			QuickFixMessageUtils.addFixPartyList(message, entity.getPartyList(), ExecutionReport.NoPartyIDs.class, ExecutionReport.NoPartyIDs.NoPartySubIDs.class);
			QuickFixMessageUtils.addCompDealerQuoteList(message, entity.getCompDealerQuoteList());

			return message;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix ExecutionReport to FixExecutionReport.", e);
		}
	}
}
