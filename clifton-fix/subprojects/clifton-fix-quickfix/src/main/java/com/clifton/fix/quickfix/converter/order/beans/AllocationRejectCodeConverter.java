package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationRejectCodes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocRejCode;


public class AllocationRejectCodeConverter extends BaseQuickFixFieldConverter<AllocationRejectCodes, AllocRejCode> {


	private static final Lazy<AllocationRejectCodeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationRejectCodeConverter::new);


	public static AllocationRejectCodeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationRejectCodes, AllocRejCode> map) {
		map.put(AllocationRejectCodes.UNKNOWN_ACCOUNT, new AllocRejCode(AllocRejCode.UNKNOWN_ACCOUNT));
		map.put(AllocationRejectCodes.INCORRECT_QUANTITY, new AllocRejCode(AllocRejCode.INCORRECT_QUANTITY));
		map.put(AllocationRejectCodes.INCORRECT_AVERAGE_PRICE, new AllocRejCode(AllocRejCode.INCORRECT_AVERAGEG_PRICE));
		map.put(AllocationRejectCodes.UNKNOWN_EXECUTING_BROKER_MNEMONIC, new AllocRejCode(AllocRejCode.UNKNOWN_EXECUTING_BROKER_MNEMONIC));
		map.put(AllocationRejectCodes.COMMISSION_DIFFERENCE, new AllocRejCode(AllocRejCode.COMMISSION_DIFFERENCE));
		map.put(AllocationRejectCodes.UNKNOWN_ORDERID, new AllocRejCode(AllocRejCode.UNKNOWN_ORDERID));
		map.put(AllocationRejectCodes.UNKNOWN_LISTID, new AllocRejCode(AllocRejCode.UNKNOWN_LISTID));
		map.put(AllocationRejectCodes.OTHER, new AllocRejCode(AllocRejCode.OTHER));
		map.put(AllocationRejectCodes.INCORRECT_ALLOCATED_QUANTITY, new AllocRejCode(AllocRejCode.INCORRECT_ALLOCATED_QUANTITY));
		map.put(AllocationRejectCodes.CALCULATION_DIFFERENCE, new AllocRejCode(AllocRejCode.CALCULATION_DIFFERENCE));
		map.put(AllocationRejectCodes.UNKNOWN_OR_STALE_EXEC_ID, new AllocRejCode(AllocRejCode.UNKNOWN_OR_STALE_EXECID));
		map.put(AllocationRejectCodes.MISMATCHED_DATA_VALUE, new AllocRejCode(AllocRejCode.MISMATCHED_DATA));
		map.put(AllocationRejectCodes.UNKNOWN_CLORDID, new AllocRejCode(AllocRejCode.UNKNOWN_CLORDID));
		map.put(AllocationRejectCodes.WAREHOUSE_REQUEST_REJECTED, new AllocRejCode(AllocRejCode.WAREHOUSE_REQUEST_REJECTED));
	}
}
