package com.clifton.fix.quickfix.util;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.destination.FixDestinationService;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.identifier.FixIdentifierHandler;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixEntityService;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixPartySub;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.quickfix.converter.order.beans.PartyIdSourcesConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartyRolesConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartySubIDTypesConverter;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.util.FixMessageHandler;
import com.clifton.fix.util.FixUtilService;
import com.clifton.fix.util.FixUtils;
import com.clifton.security.system.SecuritySystemService;
import com.clifton.security.system.SecuritySystemUser;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.NoPartyIDs;
import quickfix.field.NoPartySubIDs;
import quickfix.field.PartyID;
import quickfix.field.PartyIDSource;
import quickfix.field.PartyRole;
import quickfix.field.PartySubID;
import quickfix.field.PartySubIDType;

import java.util.ArrayList;
import java.util.List;


/**
 * {@link QuickFixMessageHandlerImpl} contains non-static convenience methods for use on QuickFix Messages.
 *
 * @author lnaylor
 */
@Component("fixMessageHandler")
public class QuickFixMessageHandlerImpl implements FixMessageHandler<Message> {

	private FixEntityService fixEntityService;

	private SecuritySystemService securitySystemService;

	private SecurityUserService securityUserService;

	private FixMessageEntryService fixMessageEntryService;

	private FixSessionService fixSessionService;

	private FixIdentifierHandler<Message> fixIdentifierHandler;

	private FixDestinationService fixDestinationService;

	private FixUtilService<Message> fixUtilService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the message text associated with a {@link BaseFixMessagingMessage}.
	 */
	@Override
	public String getBaseFixMessagingMessageText(BaseFixMessagingMessage message) {
		String messageText;
		if (message instanceof FixMessageTextMessagingMessage) {
			messageText = ((FixMessageTextMessagingMessage) message).getMessageText();
		}
		else if (message instanceof FixEntityMessagingMessage) {
			messageText = getFixEntityService().getFixMessageTextForFixEntity(((FixEntityMessagingMessage) message).getFixEntity());
		}
		else {
			throw new RuntimeException("Unrecognized instance of BaseFixMessagingMessage: " + message.getClass().getName());
		}
		return messageText;
	}


	/**
	 * Returns the {@link SecuritySystemUser#getUserName()} alias for the given {@link FixEntity}, message, and senderUserName.
	 */
	@Override
	public String getAliasForUser(FixEntity entity, Message message, String senderUserName) {
		FixDestination fixDestination = getFixDestinationForFixEntityAndMessage(entity, message);

		AssertUtils.assertNotNull(fixDestination, "Unable to get FixDestination for FixEntity.");
		if (fixDestination.getSecuritySystem() == null) {
			return null;
		}

		SecurityUser securityUser = getSecurityUserService().getSecurityUserByName(senderUserName);
		if (securityUser == null && fixDestination.isAllowAnonymousAccess()) {
			return fixDestination.isUseUsernameForAnonymousAccess() ? senderUserName : null;
		}
		AssertUtils.assertNotNull(securityUser, "Cannot send FIX message because no user exists for senderUserName [" + senderUserName + "].");

		SecuritySystemUser alias = getSecuritySystemService().getSecuritySystemUserBySystemIdAndUserId(fixDestination.getSecuritySystem().getId(), securityUser.getId());
		if (!fixDestination.isAllowAnonymousAccess()) {
			AssertUtils.assertNotNull(alias, "Cannot send FIX message because no user alias exists for [" + securityUser.getLabel() + "] on ["
					+ fixDestination.getSecuritySystem() + "].");
		}
		else if (alias == null) {
			return fixDestination.isUseUsernameForAnonymousAccess() ? senderUserName : null;
		}
		if (alias.isReadOnly()) {
			throw new RuntimeException("Cannot send FIX message because the trader alias [" + alias.getUserName() + "] is readonly.");
		}

		return alias.getUserName();
	}


	/**
	 * Returns the {@link SecurityUser#getUserName()} for the given {@link FixEntity}, message, and alias.
	 */
	@Override
	public String getUserForAlias(FixEntity entity, Message message, String alias) {
		FixDestination fixDestination = getFixDestinationForFixEntityAndMessage(entity, message);
		if (fixDestination != null && fixDestination.getSecuritySystem() != null) {
			SecuritySystemUser securitySystemUser = getSecuritySystemService().getSecuritySystemUserBySystemIdAndUserAlias(fixDestination.getSecuritySystem().getId(), alias);
			if ((securitySystemUser == null || securitySystemUser.getSecurityUser() == null) && fixDestination.isAllowAnonymousAccess() && fixDestination.isUseUsernameForAnonymousAccess()) {
				return alias;
			}
			if (securitySystemUser != null && securitySystemUser.getSecurityUser() != null) {
				return securitySystemUser.getSecurityUser().getUserName();
			}
		}
		return null;
	}


	/**
	 * Returns the FixSenderSubId for a given {@link FixEntity} and message. If throwExceptionIfNoUserName is set to true,
	 * then an exception will be thrown if no username is found on the {@link FixEntity}.
	 */
	@Override
	public String getFixSenderSubIdForFixEntity(FixEntity entity, Message message, boolean throwExceptionIfNoUserName) {
		if (entity.getFixSenderSubId() != null) {
			return entity.getFixSenderSubId();
		}

		if (throwExceptionIfNoUserName || !StringUtils.isEmpty(entity.getSenderUserName())) {
			return getAliasForUser(entity, message, entity.getSenderUserName());
		}
		return null;
	}


	/**
	 * Returns the SenderUserName for a given {@link FixEntity} and message, converted from the FixSenderSubId.
	 */
	@Override
	public String getSenderUserNameForFixEntity(FixEntity entity, Message message) {
		return getUserForAlias(entity, message, entity.getFixSenderSubId());
	}


	/**
	 * Returns a list of Parties from the NoPartyIDs group.
	 */
	@Override
	public List<FixParty> getFixPartyList(Message message, FixEntity entity) {
		List<Group> groups = message.getGroups(NoPartyIDs.FIELD);
		if (CollectionUtils.isEmpty(groups) && !message.isSetField(NoPartyIDs.FIELD)) {
			return null;
		}

		List<FixParty> result = new ArrayList<>();

		try {
			if (!CollectionUtils.isEmpty(groups)) {
				for (Group party : CollectionUtils.getIterable(groups)) {

					FixParty fixParty = new FixParty();
					fixParty.setPartyId(party.getField(new PartyID()).getValue());
					if (party.isSetField(PartyIDSource.FIELD)) {
						fixParty.setPartyIdSource((PartyIdSourcesConverter.getInstance()).fromField((PartyIDSource) party.getField(new PartyIDSource())));
					}
					if (party.isSetField(PartyRole.FIELD)) {
						fixParty.setPartyRole((PartyRolesConverter.getInstance()).fromField((PartyRole) party.getField(new PartyRole())));
					}

					List<Group> subParties = party.getGroups(NoPartySubIDs.FIELD);
					if (!CollectionUtils.isEmpty(subParties)) {
						fixParty.setPartySubList(new ArrayList<>());
						for (Group subParty : subParties) {
							FixPartySub partySub = new FixPartySub();
							if (subParty.isSetField(PartySubID.FIELD)) {
								partySub.setPartySubId(subParty.getField(new PartySubID()).getValue());
							}
							if (subParty.isSetField(PartySubIDType.FIELD)) {
								partySub.setPartySubIdType((PartySubIDTypesConverter.getInstance()).fromField((PartySubIDType) subParty.getField(new PartySubIDType())));
							}
							fixParty.getPartySubList().add(partySub);
						}
					}
					fixParty.setSenderUserName(getSenderUserNameForFixParty(entity, message, fixParty));
					result.add(fixParty);
				}
			}
			else {
				// This section is executed if the message has an a NoPartyIds entry that is not automatically grouped
				FixParty fixParty = new FixParty();
				fixParty.setPartyId(message.getField(new PartyID()).getValue());
				fixParty.setPartyIdSource(PartyIdSources.fromCode(message.getField(new PartyIDSource()).getValue()));
				fixParty.setPartyRole(PartyRoles.fromCode(message.getField(new PartyRole()).getValue()));
				fixParty.setSenderUserName(getSenderUserNameForFixParty(entity, message, fixParty));
				result.add(fixParty);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixDestination getFixDestinationForFixEntityAndMessage(FixEntity entity, Message message) {
		FixDestination fixDestination = null;
		FixSessionDefinition sessionDefinition = null;

		if (!StringUtils.isEmpty(entity.getFixDestinationName())) {
			fixDestination = getFixDestinationService().getFixDestinationByName(entity.getFixDestinationName());
		}
		if (fixDestination == null && entity.getIdentifier() != null && entity.getIdentifier().getFixDestination() != null) {
			fixDestination = entity.getIdentifier().getFixDestination();
		}

		if (fixDestination == null && message != null) {
			String fixMessageString = message.toString();
			final String typeCode = getFixUtilService().getMessageTypeCode(fixMessageString);
			final FixMessageEntryType type = getFixMessageEntryService().getFixMessageTypeByTagValue(typeCode);
			if (type != null) {
				String uniqueId = getFixUtilService().getStringField(fixMessageString, type.getIdFieldTag());
				if (StringUtils.isEmpty(uniqueId) && entity.isIncoming() && (type.getDropCopyIdFieldTag() != null)) {
					uniqueId = getFixUtilService().getStringField(fixMessageString, type.getDropCopyIdFieldTag());
				}
				if (!StringUtils.isEmpty(uniqueId)) {
					uniqueId = FixUtils.cleanUniqueId(uniqueId);

					sessionDefinition = getFixSessionService().getFixSessionDefinitionByParameters(entity);
					if (sessionDefinition != null) {
						FixSession session = getFixSessionService().getFixSessionCurrentByDefinition(sessionDefinition);
						if (session != null) {
							FixIdentifier identifier = getFixIdentifierHandler().getFixIdentifierByStringId(uniqueId, session.getId());
							if (identifier != null) {
								fixDestination = identifier.getFixDestination();
							}
						}
					}
				}
			}
		}

		if (fixDestination == null) {
			if (sessionDefinition == null) {
				sessionDefinition = getFixSessionService().getFixSessionDefinitionByParameters(entity);
			}
			if (sessionDefinition != null) {
				fixDestination = getFixDestinationService().getFixDestinationBySessionDefinition(sessionDefinition.getId());
			}
		}
		return fixDestination;
	}


	private String getSenderUserNameForFixParty(FixEntity entity, Message message, FixParty party) {
		String user = getUserForAlias(entity, message, party.getPartyId());
		if (StringUtils.isEmpty(user)) {
			user = getUserForAlias(entity, message, entity.getFixSenderSubId());
		}
		return user;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixEntityService getFixEntityService() {
		return this.fixEntityService;
	}


	public void setFixEntityService(FixEntityService fixEntityService) {
		this.fixEntityService = fixEntityService;
	}


	public SecuritySystemService getSecuritySystemService() {
		return this.securitySystemService;
	}


	public void setSecuritySystemService(SecuritySystemService securitySystemService) {
		this.securitySystemService = securitySystemService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixIdentifierHandler<Message> getFixIdentifierHandler() {
		return this.fixIdentifierHandler;
	}


	public void setFixIdentifierHandler(FixIdentifierHandler<Message> fixIdentifierHandler) {
		this.fixIdentifierHandler = fixIdentifierHandler;
	}


	public FixDestinationService getFixDestinationService() {
		return this.fixDestinationService;
	}


	public void setFixDestinationService(FixDestinationService fixDestinationService) {
		this.fixDestinationService = fixDestinationService;
	}


	public FixUtilService<Message> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<Message> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}
}
