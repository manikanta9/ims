package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.PartyRole;


public class PartyRolesConverter extends BaseQuickFixFieldConverter<PartyRoles, PartyRole> {

	private static final Lazy<PartyRolesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(PartyRolesConverter::new);


	public static PartyRolesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PartyRoles, PartyRole> map) {
		PartyRoles[] sources = PartyRoles.values();
		for (PartyRoles source : sources) {
			map.put(source, new PartyRole(source.getCode()));
		}
	}
}
