package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.MathUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.ExecutionTransactionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.ExecutionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.LastCapacityConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderCapacityConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderStatusConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.PriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProductsConverter;
import com.clifton.fix.quickfix.converter.order.beans.TimeInForceConverter;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.Message;
import quickfix.field.AccruedInterestAmt;
import quickfix.field.AvgPx;
import quickfix.field.ClOrdID;
import quickfix.field.Concession;
import quickfix.field.CouponRate;
import quickfix.field.CumQty;
import quickfix.field.ExecID;
import quickfix.field.ExecRefID;
import quickfix.field.ExecTransType;
import quickfix.field.ExecType;
import quickfix.field.Factor;
import quickfix.field.GrossTradeAmt;
import quickfix.field.Issuer;
import quickfix.field.LastCapacity;
import quickfix.field.LastMkt;
import quickfix.field.LastPx;
import quickfix.field.LastShares;
import quickfix.field.LeavesQty;
import quickfix.field.NetMoney;
import quickfix.field.OrdStatus;
import quickfix.field.OrdType;
import quickfix.field.OrderCapacity;
import quickfix.field.OrderID;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.Price;
import quickfix.field.PriceType;
import quickfix.field.Product;
import quickfix.field.SecondaryClOrdID;
import quickfix.field.SecondaryOrderID;
import quickfix.field.SettlCurrAmt;
import quickfix.field.SettlCurrency;
import quickfix.field.SettlDate;
import quickfix.field.Side;
import quickfix.field.Text;
import quickfix.field.TimeInForce;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.field.Yield;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneOffset;


public class DefaultExecutionReportConverter implements Converter<Message, FixExecutionReport> {

	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixExecutionReport convert(Message message) {
		try {
			FixExecutionReport report = new FixExecutionReport();

			report = QuickFixConverterUtils.convertToFixSecurityEntity(message, report);
			if (message.isSetField(OrderID.FIELD)) {
				report.setOrderId(message.getField(new OrderID()).getValue());
			}
			if (message.isSetField(SecondaryOrderID.FIELD)) {
				report.setSecondaryOrderId(message.getField(new SecondaryOrderID()).getValue());
			}
			if (message.isSetField(ClOrdID.FIELD)) {
				report.setClientOrderStringId(message.getField(new ClOrdID()).getValue());
			}
			if (message.isSetField(OrigClOrdID.FIELD)) {
				report.setOriginalClientOrderStringId(message.getField(new OrigClOrdID()).getValue());
			}
			if (message.isSetField(SecondaryClOrdID.FIELD)) {
				report.setSecondaryClientOrderStringId(message.getField(new SecondaryClOrdID()).getValue());
			}

			if (message.isSetField(ExecTransType.FIELD)) {
				report.setExecutionTransactionType((ExecutionTransactionTypeConverter.getInstance()).fromField((ExecTransType) message.getField(new ExecTransType())));
			}
			if (message.isSetField(ExecType.FIELD)) {
				report.setExecutionType((ExecutionTypeConverter.getInstance()).fromField((ExecType) message.getField(new ExecType())));
			}
			report.setExecutionId(message.getField(new ExecID()).getValue());
			if (message.isSetField(ExecRefID.FIELD)) {
				report.setExecutionReferenceId(message.getField(new ExecRefID()).getValue());
			}
			report.setOrderStatus((OrderStatusConverter.getInstance()).fromField((OrdStatus) message.getField(new OrdStatus())));
			report.setSide((OrderSideConverter.getInstance()).fromField((Side) message.getField(new Side())));
			if (message.isSetField(OrderQty.FIELD)) {
				report.setOrderQuantity(message.getField(new OrderQty()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(OrdType.FIELD)) {
				report.setOrderType((OrderTypeConverter.getInstance()).fromField((OrdType) message.getField(new OrdType())));
			}
			if (message.isSetField(Price.FIELD)) {
				report.setPrice(message.getField(new Price()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(TimeInForce.FIELD)) {
				report.setTimeInForceType((TimeInForceConverter.getInstance()).fromField((TimeInForce) message.getField(new TimeInForce())));
			}
			else {
				report.setTimeInForceType(com.clifton.fix.order.beans.TimeInForceTypes.DAY);
			}
			if (message.isSetField(OrderCapacity.FIELD)) {
				report.setOrderCapacity((OrderCapacityConverter.getInstance()).fromField((OrderCapacity) message.getField(new OrderCapacity())));
			}
			if (message.isSetField(LastShares.FIELD)) {
				report.setLastShares(message.getField(new LastShares()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(LastPx.FIELD)) {
				report.setLastPrice(message.getField(new LastPx()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(LastMkt.FIELD)) {
				report.setLastMarket(message.getField(new LastMkt()).getValue());
			}
			if (message.isSetField(CumQty.FIELD)) {
				report.setCumulativeQuantity(message.getField(new CumQty()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(LeavesQty.FIELD)) {
				report.setLeavesQuantity(message.getField(new LeavesQty()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(AvgPx.FIELD)) {
				report.setAveragePrice(message.getField(new AvgPx()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(LastCapacity.FIELD)) {
				report.setLastCapacity((LastCapacityConverter.getInstance()).fromField((LastCapacity) message.getField(new LastCapacity())));
			}
			if (message.isSetField(TransactTime.FIELD)) {
				report.setTransactionTime(DateUtils.asUtilDate(message.getField(new TransactTime()).getValue(), ZoneOffset.UTC));
			}
			if (message.isSetField(Text.FIELD)) {
				report.setText(message.getField(new Text()).getValue());
			}
			if (message.isSetField(PriceType.FIELD)) {
				report.setPriceType((PriceTypesConverter.getInstance()).fromField((PriceType) message.getField(new PriceType())));
			}
			if (message.isSetField(NetMoney.FIELD)) {
				report.setNetMoney(message.getField(new NetMoney()).getValue());
			}
			if (message.isSetField(GrossTradeAmt.FIELD)) {
				report.setGrossTradeAmount(message.getField(new GrossTradeAmt()).getValue());
			}
			if (message.isSetField(new AccruedInterestAmt())) {
				report.setAccruedInterestAmount(message.getField(new AccruedInterestAmt()).getValue());
			}
			if (message.isSetField(new CouponRate())) {
				report.setCouponRate(BigDecimal.valueOf(message.getField(new CouponRate()).getValue()));
			}
			if (message.isSetField(new Yield())) {
				report.setYield(BigDecimal.valueOf(message.getField(new Yield()).getValue()));
			}
			if (message.isSetField(new Concession())) {
				report.setConcession(message.getField(new Concession()).getValue());
			}
			if (message.isSetField(new Issuer())) {
				report.setIssuer(message.getField(new Issuer()).getValue());
			}
			if (message.isSetField(Product.FIELD)) {
				report.setProduct((ProductsConverter.getInstance()).fromField((Product) message.getField(new Product())));
			}
			if (message.isSetField(new TradeDate())) {
				report.setTradeDate(DateUtils.toDate(message.getField(new TradeDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (message.isSetField(new SettlDate())) {
				report.setSettlementDate(DateUtils.toDate(message.getField(new SettlDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (message.isSetField(new SettlCurrAmt())) {
				report.setSettlementCurrencyAmount(message.getField(new SettlCurrAmt()).getValue());
			}
			if (message.isSetField(new SettlCurrency())) {
				report.setSettlementCurrency(message.getField(new SettlCurrency()).getValue());
			}
			if (message.isSetField(Factor.FIELD)) {
				report.setIndexRatio(MathUtils.round(BigDecimal.valueOf(message.getField(new Factor()).getValue()), 10));
			}
			report.setPartyList(getFixMessageHandler().getFixPartyList(message, report));
			report.setCompDealerQuoteList(QuickFixMessageUtils.getCompDealerQuoteList(message));
			report.setFixNotes(QuickFixMessageUtils.getFixNotesList(message));
			return report;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix ExecutionReport to FixExecutionReport.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
