package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.OrdType;


public class OrderTypeConverter extends BaseQuickFixFieldConverter<OrderTypes, OrdType> {

	private static final Lazy<OrderTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(OrderTypeConverter::new);


	public static OrderTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<OrderTypes, OrdType> map) {
		map.put(OrderTypes.MARKET, new OrdType(OrdType.MARKET));
		map.put(OrderTypes.LIMIT, new OrdType(OrdType.LIMIT));
		map.put(OrderTypes.STOP, new OrdType(OrdType.STOP_STOP_LOSS));
		map.put(OrderTypes.STOP_LIMIT, new OrdType(OrdType.STOP_LIMIT));
		map.put(OrderTypes.MARKET_ON_CLOSE, new OrdType(OrdType.MARKET_ON_CLOSE));
		map.put(OrderTypes.WITH_OR_WITHOUT, new OrdType(OrdType.WITH_OR_WITHOUT));
		map.put(OrderTypes.LIMIT_OR_BETTER, new OrdType(OrdType.LIMIT_OR_BETTER));
		map.put(OrderTypes.LIMIT_WITH_OR_WITHOUT, new OrdType(OrdType.LIMIT_WITH_OR_WITHOUT));
		map.put(OrderTypes.ON_BASIS, new OrdType(OrdType.ON_BASIS));
		map.put(OrderTypes.ON_CLOSE, new OrdType(OrdType.ON_CLOSE));
		map.put(OrderTypes.LIMIT_ON_CLOSE, new OrdType(OrdType.LIMIT_ON_CLOSE));
		map.put(OrderTypes.FOREX_MARKET, new OrdType(OrdType.FOREX_MARKET));
		map.put(OrderTypes.PREVIOUSLY_QUOTED, new OrdType(OrdType.PREVIOUSLY_QUOTED));
		map.put(OrderTypes.PREVIOUSLY_INDICATED, new OrdType(OrdType.PREVIOUSLY_INDICATED));
		map.put(OrderTypes.FOREX_LIMIT, new OrdType(OrdType.FOREX_LIMIT));
		map.put(OrderTypes.FOREX_SWAP, new OrdType(OrdType.FOREX_SWAP));
		map.put(OrderTypes.FOREX_PREVIOUSLY_QUOTED, new OrdType(OrdType.FOREX_PREVIOUSLY_QUOTED));
		map.put(OrderTypes.FUNARI, new OrdType(OrdType.FUNARI));
		map.put(OrderTypes.MARKET_IF_TOUCHED, new OrdType(OrdType.MARKET_IF_TOUCHED));
		map.put(OrderTypes.MARKET_WITH_LEFTOVER_AS_LIMIT, new OrdType(OrdType.MARKET_WITH_LEFT_OVER_AS_LIMIT));
		map.put(OrderTypes.PREVIOUS_FUND_VALUATION_POINT, new OrdType(OrdType.PREVIOUS_FUND_VALUATION_POINT));
		map.put(OrderTypes.NEXT_FUND_VALUATION_POINT, new OrdType(OrdType.NEXT_FUND_VALUATION_POINT));
		map.put(OrderTypes.PEGGED, new OrdType(OrdType.PEGGED));
		map.put(OrderTypes.COUNTER_ORDER_SELECTION, new OrdType(OrdType.COUNTER_ORDER_SELECTION));
	}
}
