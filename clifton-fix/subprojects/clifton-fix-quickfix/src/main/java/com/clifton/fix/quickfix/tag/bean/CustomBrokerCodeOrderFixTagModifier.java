package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.quickfix.field.QuickFixFieldRediBroker;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.Group;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.NoPartyIDs;
import quickfix.field.PartyID;
import quickfix.field.PartyIDSource;
import quickfix.field.PartyRole;
import quickfix.fix44.NewOrderSingle;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>RediOrderFixTagModifierBean</code> removes all Party entries for the NewOrderSingle message, and puts
 * the PartyRole.EXECUTING_FIRM PartyID in Field 8135 if it exists.
 *
 * @author mwacker
 */
public class CustomBrokerCodeOrderFixTagModifier implements FixTagModifierBean<Message> {

	// the target field tag, which, if null, defaults to Tag 8135
	Integer targetFieldTag;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		try {
			Integer targetField = getTargetFieldTag();

			String brokerCode = null;
			List<Group> groups = message.getGroups(NoPartyIDs.FIELD);
			if (CollectionUtils.isEmpty(groups) && message.isSetField(PartyID.FIELD)) {
				brokerCode = message.getField(new PartyID()).getValue();
				message.removeField(PartyRole.FIELD);
				message.removeField(PartyID.FIELD);
				message.removeField(PartyIDSource.FIELD);
				message.removeField(NoPartyIDs.FIELD);
			}
			else {
				List<Group> removeGroupList = new ArrayList<>();
				for (Group party : CollectionUtils.getIterable(groups)) {
					if (party.isSetField(PartyRole.FIELD) &&
							((PartyRole.EXECUTING_FIRM == party.getField(new PartyRole()).getValue()) || (PartyRole.ACCEPTABLE_COUNTERPARTY == party.getField(new PartyRole()).getValue()))) {
						brokerCode = party.getField(new PartyID()).getValue();
					}
					removeGroupList.add(party);
				}
				for (Group party : CollectionUtils.getIterable(removeGroupList)) {
					message.removeGroup(party);
				}
				message.removeGroup(NoPartyIDs.FIELD);
			}
			if (!StringUtils.isEmpty(brokerCode)) {
				if (targetField == null) {
					message.setField(new QuickFixFieldRediBroker(brokerCode));
				}
				else {
					DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);

					if (dd.isHeaderField(targetField)) {
						message.getHeader().setField(new StringField(targetField, brokerCode));
					}
					else {
						message.setField(new StringField(targetField, brokerCode));
					}
				}
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + NewOrderSingle.class + "] to [" + FixOrder.class + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getTargetFieldTag() {
		return this.targetFieldTag;
	}


	public void setTargetFieldTag(Integer targetFieldTag) {
		this.targetFieldTag = targetFieldTag;
	}
}
