package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CompDealerMidPriceTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.CompDealerMidPriceType;


public class CompDealerMidPriceTypesConverter extends BaseQuickFixFieldConverter<CompDealerMidPriceTypes, CompDealerMidPriceType> {

	private static final Lazy<CompDealerMidPriceTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(CompDealerMidPriceTypesConverter::new);


	public static CompDealerMidPriceTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CompDealerMidPriceTypes, CompDealerMidPriceType> map) {
		CompDealerMidPriceTypes[] sources = CompDealerMidPriceTypes.values();
		for (CompDealerMidPriceTypes source : sources) {
			map.put(source, new CompDealerMidPriceType(source.getCode()));
		}
	}
}
