package com.clifton.fix.quickfix.converter.order.beans;

import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.market.data.refresh.MarketDataEntryTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.MDEntryType;


public class MarketDataEntryTypeConverter extends BaseQuickFixFieldConverter<MarketDataEntryTypes, MDEntryType> {

	private static final Lazy<MarketDataEntryTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(MarketDataEntryTypeConverter::new);


	public static MarketDataEntryTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<MarketDataEntryTypes, MDEntryType> map) {

		MarketDataEntryTypes[] sources = MarketDataEntryTypes.values();
		for (MarketDataEntryTypes source : sources) {
			map.put(source, new MDEntryType(source.getCode()));
		}
	}
}
