package com.clifton.fix.quickfix.message.validation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.message.validation.FixMessageValidationHandler;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.fix.message.validation.FixValidationStatuses;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import quickfix.Message;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * <code>QuickFixMessageValidationHandler</code> this is defined, along with its validations in the server spring xml.
 */
public class QuickFixMessageValidationHandlerImpl implements FixMessageValidationHandler<Message> {

	private static final Map<FixMessageTypeTagValues, FixMessageTypeTagValues> targetTypeBySourceMap = new EnumMap<>(FixMessageTypeTagValues.class);


	static {
		targetTypeBySourceMap.put(FixMessageTypeTagValues.EXECUTION_REPORT_TYPE_TAG, FixMessageTypeTagValues.NEW_ORDER_SINGLE_TYPE_TAG);
		targetTypeBySourceMap.put(FixMessageTypeTagValues.ALLOCATION_REPORT_TYPE_TAG, FixMessageTypeTagValues.ALLOCATION_INSTRUCTION_TYPE_TAG);
	}


	private Map<FixMessageTypeTagValues, List<QuickFixMessageFieldValidator>> validatorsBySourceTypeMap;

	private FixMessageEntryService fixMessageEntryService;

	private FixMessageService fixMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixValidationResult> validate(Message message, FixIdentifier fixIdentifier) {
		List<FixValidationResult> results = new ArrayList<>();
		FixMessageTypeTagValues sourceMessageType = null;
		try {
			String messageType = QuickFixMessageUtils.getMessageType(message.toString());
			sourceMessageType = FixMessageTypeTagValues.findMessageTypeTagValue(messageType);
			if (getValidatorsBySourceTypeMap().containsKey(sourceMessageType)) {
				List<QuickFixMessageFieldValidator> validators = getValidatorsBySourceTypeMap().get(sourceMessageType);
				// finds by identifier and type.
				FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
				FixMessageTypeTagValues targetMessageType = targetTypeBySourceMap.get(sourceMessageType);
				searchForm.setMessageTypeTagValue(targetMessageType.getTagValue());
				searchForm.setIdentifierId(fixIdentifier.getId());
				List<FixMessage> fixMessages = getFixMessageService().getFixMessageList(searchForm);
				// does not require the corresponding target message.
				for (FixMessage fixMessage : CollectionUtils.getIterable(fixMessages)) {
					Message targetMessage = QuickFixMessageUtils.parse(fixMessage.getText());
					for (QuickFixMessageFieldValidator fixMessageFieldValidation : validators) {
						if (!fixMessageFieldValidation.getExcludedFixSessionDefinitionNames().contains(fixIdentifier.getSession().getDefinition().getName())) {
							String msg = fixMessageFieldValidation.isValid(message, targetMessage);
							// empty string indicates no validation errors.
							if (!StringUtils.isEmpty(msg)) {
								FixMessageEntryType fixMessageType = getFixMessageEntryService().getFixMessageTypeByTagValue(messageType);
								FixValidationResult result = new FixValidationResult(fixMessage.getId(), sourceMessageType, fixIdentifier.getId(), FixValidationStatuses.FAILURE, msg);
								result.setMessageTypeLabel(fixMessageType.getName());
								results.add(result);
							}
						}
					}
				}
			}
		}
		catch (Exception e) {
			results.add(new FixValidationResult(null, sourceMessageType, fixIdentifier.getId(), FixValidationStatuses.FAILURE, e.getMessage()));
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<FixMessageTypeTagValues, List<QuickFixMessageFieldValidator>> getValidatorsBySourceTypeMap() {
		return this.validatorsBySourceTypeMap;
	}


	public void setValidatorsBySourceTypeMap(Map<FixMessageTypeTagValues, List<QuickFixMessageFieldValidator>> validatorsBySourceTypeMap) {
		this.validatorsBySourceTypeMap = validatorsBySourceTypeMap;
	}


	public static Map<FixMessageTypeTagValues, FixMessageTypeTagValues> getTargetTypeBySourceMap() {
		return targetTypeBySourceMap;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixMessageService getFixMessageService() {
		return this.fixMessageService;
	}


	public void setFixMessageService(FixMessageService fixMessageService) {
		this.fixMessageService = fixMessageService;
	}
}
