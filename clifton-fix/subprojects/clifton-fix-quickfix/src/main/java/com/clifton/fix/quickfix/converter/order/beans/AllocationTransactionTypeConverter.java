package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocTransType;


public class AllocationTransactionTypeConverter extends BaseQuickFixFieldConverter<AllocationTransactionTypes, AllocTransType> {


	private static final Lazy<AllocationTransactionTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationTransactionTypeConverter::new);


	public static AllocationTransactionTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationTransactionTypes, AllocTransType> map) {
		map.put(AllocationTransactionTypes.NEW, new AllocTransType(AllocTransType.NEW));
		map.put(AllocationTransactionTypes.REPLACE, new AllocTransType(AllocTransType.REPLACE));
		map.put(AllocationTransactionTypes.CANCEL, new AllocTransType(AllocTransType.CANCEL));
		map.put(AllocationTransactionTypes.PRELIMINARY, new AllocTransType(AllocTransType.PRELIMINARY));
		map.put(AllocationTransactionTypes.CALCULATED, new AllocTransType(AllocTransType.CALCULATED));
		map.put(AllocationTransactionTypes.CALCULATED_WITHOUT_PRELIMINARY, new AllocTransType(AllocTransType.CALCULATED_WITHOUT_PRELIMINARY));
		map.put(AllocationTransactionTypes.REVERSAL, new AllocTransType(AllocTransType.REVERSAL));
	}
}
