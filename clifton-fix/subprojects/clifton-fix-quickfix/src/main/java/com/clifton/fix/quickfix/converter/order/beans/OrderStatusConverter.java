package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.OrdStatus;


public class OrderStatusConverter extends BaseQuickFixFieldConverter<OrderStatuses, OrdStatus> {


	private static final Lazy<OrderStatusConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(OrderStatusConverter::new);


	public static OrderStatusConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<OrderStatuses, OrdStatus> map) {
		map.put(OrderStatuses.NEW, new OrdStatus(OrdStatus.NEW));
		map.put(OrderStatuses.PARTIALLY_FILLED, new OrdStatus(OrdStatus.PARTIALLY_FILLED));
		map.put(OrderStatuses.FILLED, new OrdStatus(OrdStatus.FILLED));
		map.put(OrderStatuses.DONE_FOR_DAY, new OrdStatus(OrdStatus.DONE_FOR_DAY));
		map.put(OrderStatuses.CANCELED, new OrdStatus(OrdStatus.CANCELED));
		map.put(OrderStatuses.REPLACED, new OrdStatus(OrdStatus.REPLACED));
		map.put(OrderStatuses.PENDING_CANCEL, new OrdStatus(OrdStatus.PENDING_CANCEL));
		map.put(OrderStatuses.STOPPED, new OrdStatus(OrdStatus.STOPPED));
		map.put(OrderStatuses.REJECTED, new OrdStatus(OrdStatus.REJECTED));
		map.put(OrderStatuses.PENDING_REPLACE, new OrdStatus(OrdStatus.PENDING_REPLACE));
	}
}
