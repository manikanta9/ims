package com.clifton.fix.quickfix.converter.market.data.refresh.fix44;

import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.market.data.refresh.FixMarketDataRefresh;
import com.clifton.fix.market.data.refresh.FixMarketDataRefreshEntity;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.MarketDataActionConverter;
import com.clifton.fix.quickfix.converter.order.beans.MarketDataEntryTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.PriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProductsConverter;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.ConcessionSchedule;
import quickfix.field.ExpireTime;
import quickfix.field.MDEntryDate;
import quickfix.field.MDEntryID;
import quickfix.field.MDEntrySize;
import quickfix.field.MDEntryTime;
import quickfix.field.MDEntryType;
import quickfix.field.MDReqID;
import quickfix.field.MDUpdateAction;
import quickfix.field.NoMDEntries;
import quickfix.field.Price;
import quickfix.field.PriceType;
import quickfix.field.Product;
import quickfix.field.QuoteEntryID;
import quickfix.field.ResponseTime;
import quickfix.field.SettlDate;
import quickfix.field.Text;
import quickfix.field.TotalIssuedAmount;
import quickfix.field.TransactTime;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


/**
 * Converts an incoming Fix MarketDataRefresh to FixMarketDataRefreshEntity
 */
public class DefaultMarketDataRefreshConverter implements Converter<Message, FixMarketDataRefreshEntity> {

	@Override
	public FixMarketDataRefreshEntity convert(Message message) {
		try {

			FixMarketDataRefreshEntity entity = new FixMarketDataRefreshEntity();
			// handle security fields
			entity = QuickFixConverterUtils.convertToFixEntity(message, entity);

			if (message.isSetField(MDReqID.FIELD)) {
				entity.setMarketDataRequestId(message.getField(new MDReqID()).getValue());
			}

			if (message.isSetField(NoMDEntries.FIELD)) {
				entity.setNumberOfEntries(message.getField(new NoMDEntries()).getValue());
			}

			List<FixMarketDataRefresh> data = new ArrayList<>(entity.getNumberOfEntries());

			for (int entry = 1; entry <= entity.getNumberOfEntries(); entry++) {
				FixMarketDataRefresh dataRefresh = new FixMarketDataRefresh();
				Group marketDataGroup = message.getGroup(entry, NoMDEntries.FIELD);

				// handle security fields
				QuickFixConverterUtils.convertToFixSecurityEntityFromFieldMap(marketDataGroup, dataRefresh);

				if (marketDataGroup.isSetField(MDUpdateAction.FIELD)) {
					dataRefresh.setActionType(MarketDataActionConverter.getInstance().fromField((MDUpdateAction) marketDataGroup.getField(new MDUpdateAction())));
				}

				if (marketDataGroup.isSetField(MDEntryType.FIELD)) {
					dataRefresh.setEntryType(MarketDataEntryTypeConverter.getInstance().fromField((MDEntryType) marketDataGroup.getField(new MDEntryType())));
				}

				if (marketDataGroup.isSetField(MDEntryID.FIELD)) {
					dataRefresh.setEntryId(marketDataGroup.getField(new MDEntryID()).getValue());
				}

				if (marketDataGroup.isSetField(QuoteEntryID.FIELD)) {
					dataRefresh.setQuoteEntryID(marketDataGroup.getField(new QuoteEntryID()).getValue());
				}

				if (marketDataGroup.isSetField(QuoteEntryID.FIELD)) {
					dataRefresh.setQuoteEntryID(marketDataGroup.getField(new QuoteEntryID()).getValue());
				}

				if (marketDataGroup.isSetField(Product.FIELD)) {
					dataRefresh.setProduct(ProductsConverter.getInstance().fromField((Product) marketDataGroup.getField(new Product())));
				}

				if (marketDataGroup.isSetField(PriceType.FIELD)) {
					dataRefresh.setPriceType(PriceTypesConverter.getInstance().fromField((PriceType) marketDataGroup.getField(new PriceType())));
				}

				if (marketDataGroup.isSetField(Price.FIELD)) {
					dataRefresh.setPrice(marketDataGroup.getField(new Price()).getValue());
				}

				if (marketDataGroup.isSetField(MDEntrySize.FIELD)) {
					dataRefresh.setQuantitySize(marketDataGroup.getField(new MDEntrySize()).getValue());
				}

				if (marketDataGroup.isSetField(MDEntryDate.FIELD)) {
					dataRefresh.setAsOfDate(marketDataGroup.getField(new MDEntryDate()).getValue());
				}

				if (marketDataGroup.isSetField(MDEntryTime.FIELD)) {
					dataRefresh.setAsOfTime(marketDataGroup.getField(new MDEntryTime()).getValue());
				}
				if (marketDataGroup.isSetField(ExpireTime.FIELD)) {
					dataRefresh.setExpiryTime(marketDataGroup.getField(new ExpireTime()).getValue().atOffset(ZoneOffset.UTC));
				}

				if (marketDataGroup.isSetField(SettlDate.FIELD)) {
					dataRefresh.setSettleDate(LocalDate.parse(marketDataGroup.getField(new SettlDate()).getValue(), DateTimeFormatter.ofPattern(DateUtils.FIX_DATE_FORMAT_INPUT)));
				}

				if (marketDataGroup.isSetField(Text.FIELD)) {
					dataRefresh.setText(marketDataGroup.getField(new Text()).getValue());
				}

				if (marketDataGroup.isSetField(TransactTime.FIELD)) {
					dataRefresh.setTransactTime(marketDataGroup.getField(new TransactTime()).getValue().atOffset(ZoneOffset.UTC));
				}

				if (marketDataGroup.isSetField(TotalIssuedAmount.FIELD)) {
					dataRefresh.setTotalIssuedAmount(marketDataGroup.getField(new TotalIssuedAmount()).getValue());
				}

				if (marketDataGroup.isSetField(ConcessionSchedule.FIELD)) {
					dataRefresh.setConcessionSchedule(marketDataGroup.getField(new ConcessionSchedule()).getValue());
				}

				if (marketDataGroup.isSetField(ResponseTime.FIELD)) {
					dataRefresh.setResponseTime(marketDataGroup.getField(new ResponseTime()).getValue().atOffset(ZoneOffset.UTC));
				}
				if (marketDataGroup.isSetField(ExpireTime.FIELD)) {
					dataRefresh.setExpiryTime(marketDataGroup.getField(new ExpireTime()).getValue().atOffset(ZoneOffset.UTC));
				}

				data.add(dataRefresh);
			}
			entity.setData(data);
			return entity;
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to convert fix Market Data Refresh Message to FixMarketDataRefreshEntity.", e);
		}
	}
}
