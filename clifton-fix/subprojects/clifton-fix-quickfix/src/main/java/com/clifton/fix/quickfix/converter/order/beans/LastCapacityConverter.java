package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.LastCapacities;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.LastCapacity;


public class LastCapacityConverter extends BaseQuickFixFieldConverter<LastCapacities, LastCapacity> {

	private static final Lazy<LastCapacityConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(LastCapacityConverter::new);


	public static LastCapacityConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<LastCapacities, LastCapacity> map) {
		map.put(com.clifton.fix.order.beans.LastCapacities.AGENT, new LastCapacity(LastCapacity.AGENT));
		map.put(com.clifton.fix.order.beans.LastCapacities.CROSS_AS_AGENT, new LastCapacity(LastCapacity.CROSS_AS_AGENT));
		map.put(com.clifton.fix.order.beans.LastCapacities.CROSS_AS_PRINCIPAL, new LastCapacity(LastCapacity.CROSS_AS_PRINCIPAL));
		map.put(com.clifton.fix.order.beans.LastCapacities.PRINCIPAL, new LastCapacity(LastCapacity.PRINCIPAL));
	}
}
