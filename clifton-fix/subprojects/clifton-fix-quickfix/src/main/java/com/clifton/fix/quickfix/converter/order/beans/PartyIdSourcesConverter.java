package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.PartyIDSource;


public class PartyIdSourcesConverter extends BaseQuickFixFieldConverter<PartyIdSources, PartyIDSource> {

	private static final Lazy<PartyIdSourcesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(PartyIdSourcesConverter::new);


	public static PartyIdSourcesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PartyIdSources, PartyIDSource> map) {
		PartyIdSources[] sources = PartyIdSources.values();
		for (PartyIdSources source : sources) {
			map.put(source, new PartyIDSource(source.getCode()));
		}
	}
}
