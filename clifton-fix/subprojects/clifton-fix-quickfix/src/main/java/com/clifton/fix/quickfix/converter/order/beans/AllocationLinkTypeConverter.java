package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationLinkTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocLinkType;


public class AllocationLinkTypeConverter extends BaseQuickFixFieldConverter<AllocationLinkTypes, AllocLinkType> {

	private static final Lazy<AllocationLinkTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationLinkTypeConverter::new);


	public static AllocationLinkTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationLinkTypes, AllocLinkType> map) {
		map.put(AllocationLinkTypes.F_X_NETTING, new AllocLinkType(AllocLinkType.FX_NETTING));
		map.put(AllocationLinkTypes.F_X_SWAP, new AllocLinkType(AllocLinkType.FX_SWAP));
	}
}
