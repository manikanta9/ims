package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.Side;


public class OrderSideConverter extends BaseQuickFixFieldConverter<OrderSides, Side> {


	private static final Lazy<OrderSideConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(OrderSideConverter::new);


	public static OrderSideConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<OrderSides, Side> map) {
		OrderSides[] sources = OrderSides.values();
		for (OrderSides source : sources) {
			map.put(source, new Side(source.getCode()));
		}
	}
}
