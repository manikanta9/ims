package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import quickfix.Message;


public class DefaultOrderCancelReplaceRequestConverter extends BaseOrderConverter implements Converter<Message, FixOrderCancelReplaceRequest> {

	@Override
	public FixOrderCancelReplaceRequest convert(Message from) {
		return toFixOrder(from, new FixOrderCancelReplaceRequest());
	}
}
