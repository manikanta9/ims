package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.AllocAlgo;
import quickfix.field.MsgType;
import quickfix.field.SecurityExchange;


/**
 * The <code>CitiAllocationAlgorithmFixTagModifier</code> sets the correct allocation algorithm based on the exchange code for CITI allocation instruction messages.
 *
 * @author mwacker
 */

public class CitiAllocationAlgorithmFixTagModifier implements FixTagModifierBean<Message> {

	// A delimited string formatted as algoCode=exchange1,exchange2,exchange3...
	private String bestFitExchanges;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		AssertUtils.assertNotEmpty(getBestFitExchanges(), "The Best Fit Exchanges value is required and cannot be null.");
		String messageTypeCode = QuickFixMessageUtils.getMessageType(message.toString());
		if (!MsgType.ALLOCATION_INSTRUCTION.equals(messageTypeCode)) {
			throw new RuntimeException("CitiAllocationAlgorithmFixTagModifier can only be used by messages of type 35=J (AllocationInstructions).  This message is of type 35=" + messageTypeCode + ".");
		}

		String exchangeCode = getSecurityExchangeField(message);
		if (!StringUtils.isEmpty(exchangeCode)) {
				String[] propertyValue = getBestFitExchanges().split("=");
				if ((propertyValue.length == 2) && MathUtils.isNumber(propertyValue[0])) {
					int algorithmCode = Integer.parseInt(propertyValue[0]);
					String exchangeCodeString = propertyValue[1];
					if (exchangeCodeString.toUpperCase().contains(exchangeCode.toUpperCase())) {
						message.setField(new AllocAlgo(algorithmCode));
					}
				}
		}
		if (!message.isSetField(AllocAlgo.FIELD)) {
			message.setField(new AllocAlgo(AllocAlgo.AVERAGE_PRICE));
		}
	}


	private String getSecurityExchangeField(Message message) {
		try {
			return message.isSetField(new SecurityExchange()) ? message.getField(new StringField(SecurityExchange.FIELD)).getValue() : null;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to get SecurityExchange field for message.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBestFitExchanges() {
		return this.bestFitExchanges;
	}


	public void setBestFitExchanges(String bestFitExchanges) {
		this.bestFitExchanges = bestFitExchanges;
	}
}
