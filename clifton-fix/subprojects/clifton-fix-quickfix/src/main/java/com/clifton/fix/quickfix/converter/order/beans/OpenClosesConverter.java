package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.OpenCloses;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.OpenClose;


public class OpenClosesConverter extends BaseQuickFixFieldConverter<OpenCloses, OpenClose> {

	private static final Lazy<OpenClosesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(OpenClosesConverter::new);


	public static OpenClosesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<OpenCloses, OpenClose> map) {
		OpenCloses[] sources = OpenCloses.values();
		for (OpenCloses source : sources) {
			map.put(source, new OpenClose(source.getCode()));
		}
	}
}
