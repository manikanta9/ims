package com.clifton.fix.quickfix.field;


import quickfix.StringField;


/**
 * The <code>QuickFixFieldRediBroker</code> QuickFix string field that contains the broker code for multiple broker trading on REDI.
 * <p/>
 * NOTE:  This field is custom for FIX connections to REDI.
 *
 * @author mwacker
 */
public class QuickFixFieldRediBroker extends StringField {

	static final long serialVersionUID = 20120412;

	public static final int FIELD = 8135;


	public QuickFixFieldRediBroker() {
		super(QuickFixFieldRediBroker.FIELD);
	}


	public QuickFixFieldRediBroker(String data) {
		super(QuickFixFieldRediBroker.FIELD, data);
	}
}
