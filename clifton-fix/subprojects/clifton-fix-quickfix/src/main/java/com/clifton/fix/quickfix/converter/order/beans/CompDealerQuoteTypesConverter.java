package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CompDealerQuoteTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.CompDealerQuoteType;


public class CompDealerQuoteTypesConverter extends BaseQuickFixFieldConverter<CompDealerQuoteTypes, CompDealerQuoteType> {

	private static final Lazy<CompDealerQuoteTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(CompDealerQuoteTypesConverter::new);


	public static CompDealerQuoteTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CompDealerQuoteTypes, CompDealerQuoteType> map) {
		CompDealerQuoteTypes[] sources = CompDealerQuoteTypes.values();
		for (CompDealerQuoteTypes source : sources) {
			map.put(source, new CompDealerQuoteType(source.getCode()));
		}
	}
}
