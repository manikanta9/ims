package com.clifton.fix.quickfix.converter;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.quickfix.converter.order.beans.PutOrCallsConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityIDSourceConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.UnderlyingSecurityTypeConverter;
import quickfix.FieldMap;
import quickfix.Message;
import quickfix.field.BeginString;
import quickfix.field.CFICode;
import quickfix.field.Currency;
import quickfix.field.EncodedSecurityDesc;
import quickfix.field.EncodedSecurityDescLen;
import quickfix.field.ExDestination;
import quickfix.field.MaturityDate;
import quickfix.field.MaturityDay;
import quickfix.field.MaturityMonthYear;
import quickfix.field.MsgSeqNum;
import quickfix.field.OnBehalfOfCompID;
import quickfix.field.PutOrCall;
import quickfix.field.SecurityExchange;
import quickfix.field.SecurityID;
import quickfix.field.SecurityIDSource;
import quickfix.field.SecurityType;
import quickfix.field.SenderCompID;
import quickfix.field.SenderLocationID;
import quickfix.field.SenderSubID;
import quickfix.field.SettlCurrency;
import quickfix.field.StrikePrice;
import quickfix.field.Symbol;
import quickfix.field.TargetCompID;
import quickfix.field.TargetLocationID;
import quickfix.field.TargetSubID;
import quickfix.field.UnderlyingSecurityType;

import java.math.BigDecimal;
import java.util.Map;


public class QuickFixConverterUtils {

	private QuickFixConverterUtils() {
		// prevent construction
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Populates the standard header objects for a QuickFix message using a FixEntity.
	 */
	public static <T extends Message, F extends FixEntity> T convertFromFixEntity(F entity, T message) {
		if (entity == null) {
			throw new IllegalArgumentException("Required entity argument to convertToFixEntity cannot be null.");
		}

		if (!StringUtils.isEmpty(entity.getFixBeginString())) {
			message.getHeader().setField(new BeginString(entity.getFixBeginString()));
		}
		if (!StringUtils.isEmpty(entity.getFixSenderCompId())) {
			message.getHeader().setField(new SenderCompID(entity.getFixSenderCompId()));
		}
		if (!StringUtils.isEmpty(entity.getFixSenderSubId())) {
			message.getHeader().setField(new SenderSubID(entity.getFixSenderSubId()));
		}
		if (!StringUtils.isEmpty(entity.getFixSenderLocId())) {
			message.getHeader().setField(new SenderLocationID(entity.getFixSenderLocId()));
		}
		if (!StringUtils.isEmpty(entity.getFixTargetCompId())) {
			message.getHeader().setField(new TargetCompID(entity.getFixTargetCompId()));
		}
		if (!StringUtils.isEmpty(entity.getFixTargetSubId())) {
			message.getHeader().setField(new TargetSubID(entity.getFixTargetSubId()));
		}
		if (!StringUtils.isEmpty(entity.getFixTargetLocId())) {
			message.getHeader().setField(new TargetLocationID(entity.getFixTargetLocId()));
		}
		if (!StringUtils.isEmpty(entity.getFixOnBehalfOfCompID())) {
			message.getHeader().setField(new OnBehalfOfCompID(entity.getFixOnBehalfOfCompID()));
		}
		return message;
	}


	/**
	 * Retrieves the standard header fields from a quickfix messages and sets the values on the FixEntity.
	 *
	 * @param message
	 * @param entity
	 */
	public static <T extends FixEntity, F extends Message> T convertToFixEntity(F message, T entity) {
		if (entity == null) {
			throw new IllegalArgumentException("Required entity argument to convertToFixEntity cannot be null.");
		}
		try {
			entity.setFixBeginString(message.getHeader().getField(new BeginString()).getValue());
			if (message.getHeader().isSetField(new SenderCompID())) {
				entity.setFixSenderCompId(message.getHeader().getField(new SenderCompID()).getValue());
			}
			if (message.getHeader().isSetField(new SenderSubID())) {
				entity.setFixSenderSubId(message.getHeader().getField(new SenderSubID()).getValue());
			}
			if (message.getHeader().isSetField(new SenderLocationID())) {
				entity.setFixSenderLocId(message.getHeader().getField(new SenderLocationID()).getValue());
			}

			if (message.getHeader().isSetField(new TargetCompID())) {
				entity.setFixTargetCompId(message.getHeader().getField(new TargetCompID()).getValue());
			}
			if (message.getHeader().isSetField(new TargetSubID())) {
				entity.setFixTargetSubId(message.getHeader().getField(new TargetSubID()).getValue());
			}
			if (message.getHeader().isSetField(new TargetLocationID())) {
				entity.setFixTargetLocId(message.getHeader().getField(new TargetLocationID()).getValue());
			}
			if (message.getHeader().isSetField(new MsgSeqNum())) {
				entity.setSequenceNumber(message.getHeader().getField(new MsgSeqNum()).getValue());
			}
			if (message.getHeader().isSetField(new OnBehalfOfCompID())) {
				entity.setFixOnBehalfOfCompID(message.getHeader().getField(new OnBehalfOfCompID()).getValue());
			}
			return entity;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + Message.class + "] to [" + FixEntity.class + "].", e);
		}
	}


	/**
	 * Populates the standard header objects for a QuickFix message using a FixEntity.
	 *
	 * @param entity
	 * @param message
	 */
	public static <T extends Message, F extends FixSecurityEntity> T convertFromFixSecurityEntity(F entity, T message) {
		message = convertFromFixEntity(entity, message);

		if (!StringUtils.isEmpty(entity.getSymbol())) {
			message.setField(new Symbol(entity.getSymbol()));
		}
		if (StringUtils.isEmpty(entity.getSecurityId()) && !StringUtils.isEmpty(entity.getSymbol())) {
			message.setField(new SecurityID(entity.getSymbol()));
		}
		else if (!StringUtils.isEmpty(entity.getSecurityId())) {
			message.setField(new SecurityID(entity.getSecurityId()));
		}
		if (entity.getSecurityIDSource() != null) {
			message.setField((SecurityIDSourceConverter.getInstance()).toField(entity.getSecurityIDSource()));
		}
		if (entity.getSecurityType() != null) {
			message.setField((SecurityTypeConverter.getInstance()).toField(entity.getSecurityType()));
		}

		if (!StringUtils.isEmpty(entity.getSecurityExchange())) {
			message.setField(new SecurityExchange(entity.getSecurityExchange()));
		}
		if (entity.getCurrency() != null) {
			message.setField(new Currency(entity.getCurrency()));
		}
		if (entity.getSettlementCurrency() != null) {
			message.setField(new SettlCurrency(entity.getSettlementCurrency()));
		}
		if (!StringUtils.isEmpty(entity.getExDestination())) {
			message.setField(new ExDestination(entity.getExDestination()));
		}
		if (entity.getMaturityDate() != null) {
			message.setField(new MaturityDate(DateUtils.fromDate(entity.getMaturityDate(), DateUtils.FIX_DATE_FORMAT_INPUT)));
		}

		// Options
		if (!StringUtils.isEmpty(entity.getMaturityMonthYear())) {
			message.setField(new MaturityMonthYear(entity.getMaturityMonthYear()));
		}
		if (!StringUtils.isEmpty(entity.getMaturityDay())) {
			message.setField(new MaturityDay(entity.getMaturityDay()));
		}
		if (entity.getPutOrCall() != null) {
			message.setField((PutOrCallsConverter.getInstance()).toField(entity.getPutOrCall()));
		}
		if (entity.getStrikePrice() != null) {
			message.setField(new StrikePrice(getScaledBigDecimal(entity.getStrikePrice())));
		}
		if (entity.getCFICode() != null) {
			message.setField(new CFICode(entity.getCFICode()));
		}
		if (entity.getUnderlyingSecurityType() != null) {
			message.setField((UnderlyingSecurityTypeConverter.getInstance()).toField(entity.getUnderlyingSecurityType()));
		}

		// IRS
		if (!StringUtils.isEmpty(entity.getEncodeSecurityDescription())) {
			message.setField(new EncodedSecurityDesc(entity.getEncodeSecurityDescription()));
			message.setField(new EncodedSecurityDescLen(entity.getEncodeSecurityDescription().length()));
		}

		//Common - additionalParams
		if (entity.getAdditionalParams() != null) {
			for (Map.Entry<String, String> entry : entity.getAdditionalParams().entrySet()) {
				try {
					message.setString(Integer.parseInt(entry.getKey()), entry.getValue());
				}
				catch (NumberFormatException nfe) {
					LogUtils.warn(QuickFixConverterUtils.class, String.format("Fix translation error in AdditionalParams occured. Message processing will continue, but some message data may be lost. Message text: [%s].", entry.getKey()), nfe);
				}
			}
		}
		return message;
	}


	/**
	 * Retrieves the standard header fields from a quickfix messages and sets the values on the FixSecurityEntity.
	 */
	public static <T extends FixSecurityEntity, F extends Message> T convertToFixSecurityEntity(F message, T entity) {
		if (entity == null) {
			throw new IllegalArgumentException("Required entity argument to convertToFixEntity cannot be null.");
		}
		try {
			entity = convertToFixEntity(message, entity);

			convertToFixSecurityEntityFromFieldMap(message, entity);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + Message.class + "] to [" + FixEntity.class + "].", e);
		}
		return entity;
	}


	/**
	 * Retrieves the standard header fields from a quickfix FieldMap and sets the values on the FixSecurityEntity.
	 *
	 * @param message
	 * @param entity
	 *
	 */
	public static  <T extends FixSecurityEntity, F extends FieldMap> T convertToFixSecurityEntityFromFieldMap(F message, T entity){

		try {
			if (message.isSetField(new Symbol())) {
				entity.setSymbol(message.getField(new Symbol()).getValue());
			}
			if (message.isSetField(new SecurityID())) {
				entity.setSecurityId(message.getField(new SecurityID()).getValue());
			}
			if (message.isSetField(new SecurityIDSource())) {
				entity.setSecurityIDSource((SecurityIDSourceConverter.getInstance()).fromField((SecurityIDSource) message.getField(new SecurityIDSource())));
			}
			if (message.isSetField(new SecurityType())) {
				entity.setSecurityType((SecurityTypeConverter.getInstance()).fromField((SecurityType) message.getField(new SecurityType())));
			}
			if (message.isSetField(new SecurityExchange())) {
				entity.setSecurityExchange(message.getField(new SecurityExchange()).getValue());
			}
			if (message.isSetField(new Currency())) {
				entity.setCurrency(message.getField(new Currency()).getValue());
			}
			if (message.isSetField(new SettlCurrency())) {
				entity.setSettlementCurrency(message.getField(new SettlCurrency()).getValue());
			}
			if (message.isSetField(new MaturityDate())) {
				entity.setMaturityDate(DateUtils.toDate(message.getField(new MaturityDate()).getValue(), DateUtils.FIX_DATE_FORMAT_INPUT));
			}
			if (message.isSetField(new ExDestination())) {
				entity.setExDestination(message.getField(new ExDestination()).getValue());
			}

			// Options
			if (message.isSetField(new MaturityMonthYear())) {
				entity.setMaturityMonthYear(message.getField(new MaturityMonthYear()).getValue());
			}
			if (message.isSetField(new MaturityDay())) {
				entity.setMaturityDay(message.getField(new MaturityDay()).getValue());
			}
			if (message.isSetField(new PutOrCall())) {
				entity.setPutOrCall((PutOrCallsConverter.getInstance()).fromField((PutOrCall) message.getField(new PutOrCall())));
			}
			if (message.isSetField(new StrikePrice())) {
				entity.setStrikePrice(message.getField(new StrikePrice()).getValue());
			}
			if (message.isSetField(new CFICode())) {
				entity.setCFICode(message.getField(new CFICode()).getValue());
			}
			if (message.isSetField(new UnderlyingSecurityType())) {
				entity.setUnderlyingSecurityType((UnderlyingSecurityTypeConverter.getInstance()).fromField((UnderlyingSecurityType) message.getField(new UnderlyingSecurityType())));
			}

			// IRS
			if (message.isSetField(new EncodedSecurityDesc())) {
				entity.setEncodeSecurityDescription(message.getField(new EncodedSecurityDesc()).getValue());
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + FieldMap.class + "] to [" + FixEntity.class + "].", e);
		}
		return entity;
	}

	public static BigDecimal getScaledBigDecimal(BigDecimal value) {
		String decimalString = CoreMathUtils.formatNumber(value, "#.00000000######################");
		return new BigDecimal(decimalString);
	}
}
