package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.OrderCapacities;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.OrderCapacity;


public class OrderCapacityConverter extends BaseQuickFixFieldConverter<OrderCapacities, OrderCapacity> {


	private static final Lazy<OrderCapacityConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(OrderCapacityConverter::new);


	public static OrderCapacityConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<OrderCapacities, OrderCapacity> map) {
		map.put(com.clifton.fix.order.beans.OrderCapacities.AGENCY, new OrderCapacity(OrderCapacity.AGENCY));
		map.put(com.clifton.fix.order.beans.OrderCapacities.PROPRIETARY, new OrderCapacity(OrderCapacity.PROPRIETARY));
		map.put(com.clifton.fix.order.beans.OrderCapacities.INDIVIDUAL, new OrderCapacity(OrderCapacity.INDIVIDUAL));
		map.put(com.clifton.fix.order.beans.OrderCapacities.PRINCIPAL, new OrderCapacity(OrderCapacity.PRINCIPAL));
		map.put(com.clifton.fix.order.beans.OrderCapacities.RISKLESS_PRINCIPAL, new OrderCapacity(OrderCapacity.RISKLESS_PRINCIPAL));
		map.put(com.clifton.fix.order.beans.OrderCapacities.AGENT_FOR_OTHER_MEMBER, new OrderCapacity(OrderCapacity.AGENT_FOR_OTHER_MEMBER));
	}
}
