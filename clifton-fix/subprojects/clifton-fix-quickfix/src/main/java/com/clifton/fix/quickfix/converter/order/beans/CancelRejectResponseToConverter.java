package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CancelRejectResponseToItems;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.CxlRejResponseTo;


public class CancelRejectResponseToConverter extends BaseQuickFixFieldConverter<CancelRejectResponseToItems, CxlRejResponseTo> {

	private static final Lazy<CancelRejectResponseToConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(CancelRejectResponseToConverter::new);


	public static CancelRejectResponseToConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CancelRejectResponseToItems, CxlRejResponseTo> map) {
		map.put(CancelRejectResponseToItems.ORDER_CANCEL_REQUEST, new CxlRejResponseTo(CxlRejResponseTo.ORDER_CANCEL_REQUEST));
		map.put(CancelRejectResponseToItems.ORDER_CANCEL_REPLACE_REQUEST, new CxlRejResponseTo(CxlRejResponseTo.ORDER_CANCEL_REPLACE_REQUEST));
	}
}
