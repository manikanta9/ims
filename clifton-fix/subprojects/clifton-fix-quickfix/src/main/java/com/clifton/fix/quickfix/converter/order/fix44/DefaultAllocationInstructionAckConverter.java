package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.AllocationRejectCodeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationStatusConverter;
import quickfix.Message;
import quickfix.field.AllocID;
import quickfix.field.AllocRejCode;
import quickfix.field.AllocStatus;
import quickfix.field.Text;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;

import java.time.ZoneOffset;


public class DefaultAllocationInstructionAckConverter implements Converter<Message, FixAllocationAcknowledgement> {

	@Override
	public FixAllocationAcknowledgement convert(Message message) {
		try {
			FixAllocationAcknowledgement ack = new FixAllocationAcknowledgement();
			ack = QuickFixConverterUtils.convertToFixEntity(message, ack);
			if (message.isSetField(AllocID.FIELD)) {
				ack.setAllocationStringId(message.getField(new AllocID()).getValue());
				//ack.setAllocationId(FixUtils.parseUniqueId(ack.getAllocationStringId()));
			}

			ack.setAllocationStatus((AllocationStatusConverter.getInstance()).fromField((AllocStatus) message.getField(new AllocStatus())));
			if (message.isSetField(AllocRejCode.FIELD)) {
				ack.setAllocationRejectCode((AllocationRejectCodeConverter.getInstance()).fromField((AllocRejCode) message.getField(new AllocRejCode())));
			}
			if (message.isSetField(Text.FIELD)) {
				ack.setText(message.getField(new Text()).getValue());
			}
			if (message.isSetField(TradeDate.FIELD)) {
				ack.setTradeDate(message.getField(new TradeDate()).getValue());
			}
			if (message.isSetField(TransactTime.FIELD)) {
				ack.setTransactionTime(DateUtils.asUtilDate(message.getField(new TransactTime()).getValue(), ZoneOffset.UTC));
			}
			return ack;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix AllocationInstructionAck to FixFixAllocationAcknowledgement.", e);
		}
	}
}
