package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationCancelReplaceReasons;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocCancReplaceReason;


public class AllocationCancelReplaceReasonsConverter extends BaseQuickFixFieldConverter<AllocationCancelReplaceReasons, AllocCancReplaceReason> {


	private static final Lazy<AllocationCancelReplaceReasonsConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationCancelReplaceReasonsConverter::new);


	public static AllocationCancelReplaceReasonsConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationCancelReplaceReasons, AllocCancReplaceReason> map) {
		AllocationCancelReplaceReasons[] sources = AllocationCancelReplaceReasons.values();
		for (AllocationCancelReplaceReasons source : sources) {
			map.put(source, new AllocCancReplaceReason(source.getCode()));
		}
	}
}
