package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.Group;
import quickfix.Message;

import java.util.ArrayList;
import java.util.List;


/**
 * The FieldRemoveFixTagModifier removes the specified fieldTag from the Fix Message.  Supports removal of FIX Tags within a group,
 * and FIX Tags that represent a group.
 */
public class FieldRemoveFixTagModifier implements FixTagModifierBean<Message> {

	private Integer fieldTag;
	private Integer parentGroupFieldTag;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);
		AssertUtils.assertNotNull(getFieldTag(), "Field Tag value cannot be null.");

		List<Group> groups = message.getGroups(getParentGroupFieldTag() != null ? getParentGroupFieldTag() : getFieldTag());
		if (CollectionUtils.isEmpty(groups) && (message.isSetField(getFieldTag()) || message.getHeader().isSetField(getFieldTag()))) {
			if (dd.isHeaderField(getFieldTag())) {
				message.getHeader().removeField(getFieldTag());
			}
			else {
				message.removeField(getFieldTag());
			}
		}
		else {
			List<Group> removeGroupList = new ArrayList<>();
			for (Group group : CollectionUtils.getIterable(groups)) {
				if (getParentGroupFieldTag() != null) {
					group.removeField(getFieldTag());
				}
				else {
					removeGroupList.add(group);
				}
			}
			if (getParentGroupFieldTag() == null) {
				for (Group group : CollectionUtils.getIterable(removeGroupList)) {
					if (message.hasGroup(group)) {
						message.removeGroup(group);
					}
				}
			}
			if (message.hasGroup(getFieldTag())) {
				message.removeGroup(getFieldTag());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(Integer fieldTag) {
		this.fieldTag = fieldTag;
	}


	public Integer getParentGroupFieldTag() {
		return this.parentGroupFieldTag;
	}


	public void setParentGroupFieldTag(Integer parentGroupFieldTag) {
		this.parentGroupFieldTag = parentGroupFieldTag;
	}
}
