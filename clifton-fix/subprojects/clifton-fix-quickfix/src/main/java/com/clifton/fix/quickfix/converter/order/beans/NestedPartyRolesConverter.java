package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.NestedPartyRole;


public class NestedPartyRolesConverter extends BaseQuickFixFieldConverter<PartyRoles, NestedPartyRole> {


	private static final Lazy<NestedPartyRolesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(NestedPartyRolesConverter::new);


	public static NestedPartyRolesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PartyRoles, NestedPartyRole> map) {
		PartyRoles[] sources = PartyRoles.values();
		for (PartyRoles source : sources) {
			map.put(source, new NestedPartyRole(source.getCode()));
		}
	}
}
