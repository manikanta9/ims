package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.SecurityIDSource;


public class SecurityIDSourceConverter extends BaseQuickFixFieldConverter<SecurityIDSources, SecurityIDSource> {


	private static final Lazy<SecurityIDSourceConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(SecurityIDSourceConverter::new);


	public static SecurityIDSourceConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<SecurityIDSources, SecurityIDSource> map) {
		SecurityIDSources[] sources = SecurityIDSources.values();
		for (SecurityIDSources source : sources) {
			map.put(source, new SecurityIDSource(source.getCode()));
		}
	}
}
