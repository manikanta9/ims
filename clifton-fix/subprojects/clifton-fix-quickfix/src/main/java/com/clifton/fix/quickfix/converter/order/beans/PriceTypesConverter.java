package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.PriceType;


public class PriceTypesConverter extends BaseQuickFixFieldConverter<PriceTypes, PriceType> {

	private static final Lazy<PriceTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(PriceTypesConverter::new);


	public static PriceTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<PriceTypes, PriceType> map) {
		PriceTypes[] sources = PriceTypes.values();
		for (PriceTypes source : sources) {
			map.put(source, new PriceType(source.getCode()));
		}
	}
}
