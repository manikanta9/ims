package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.ExecType;


public class ExecutionTypeConverter extends BaseQuickFixFieldConverter<ExecutionTypes, ExecType> {

	private static final Lazy<ExecutionTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(ExecutionTypeConverter::new);


	public static ExecutionTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<ExecutionTypes, ExecType> map) {
		map.put(ExecutionTypes.NEW, new ExecType(ExecType.NEW));
		map.put(ExecutionTypes.PARTIAL_FILL, new ExecType(ExecType.PARTIAL_FILL));
		map.put(ExecutionTypes.FILL, new ExecType(ExecType.FILL));
		map.put(ExecutionTypes.DONE_FOR_DAY, new ExecType(ExecType.DONE_FOR_DAY));
		map.put(ExecutionTypes.CANCELED, new ExecType(ExecType.CANCELED));
		map.put(ExecutionTypes.REPLACE, new ExecType(ExecType.REPLACED));
		map.put(ExecutionTypes.PENDING_CANCEL, new ExecType(ExecType.PENDING_CANCEL));
		map.put(ExecutionTypes.STOPPED, new ExecType(ExecType.STOPPED));
		map.put(ExecutionTypes.REJECTED, new ExecType(ExecType.REJECTED));
		map.put(ExecutionTypes.SUSPENDED, new ExecType(ExecType.SUSPENDED));
		map.put(ExecutionTypes.PENDING_NEW, new ExecType(ExecType.PENDING_NEW));
		map.put(ExecutionTypes.CALCULATED, new ExecType(ExecType.CALCULATED));
		map.put(ExecutionTypes.EXPIRED, new ExecType(ExecType.EXPIRED));
		map.put(ExecutionTypes.RESTATED, new ExecType(ExecType.RESTATED));
		map.put(ExecutionTypes.PENDING_REPLACE, new ExecType(ExecType.PENDING_REPLACE));
		map.put(ExecutionTypes.TRADE, new ExecType(ExecType.TRADE));
		map.put(ExecutionTypes.TRADE_CORRECT, new ExecType(ExecType.TRADE_CORRECT));
		map.put(ExecutionTypes.TRADE_CANCEL, new ExecType(ExecType.TRADE_CANCEL));
		map.put(ExecutionTypes.ORDER_STATUS, new ExecType(ExecType.ORDER_STATUS));
		map.put(ExecutionTypes.TRADE_IN_A_CLEARING_HOLD, new ExecType(ExecType.TRADE_IN_A_CLEARING_HOLD));
		map.put(ExecutionTypes.TRADE_HAS_BEEN_RELEASED_TO_CLEARING, new ExecType(ExecType.TRADE_HAS_BEEN_RELEASED_TO_CLEARING));
		map.put(ExecutionTypes.TRIGGERED_OR_ACTIVATED_BY_SYSTEM, new ExecType(ExecType.TRIGGERED_OR_ACTIVATED_BY_SYSTEM));
	}
}
