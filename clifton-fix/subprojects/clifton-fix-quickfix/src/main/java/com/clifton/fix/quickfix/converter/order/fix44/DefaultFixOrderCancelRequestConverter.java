package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.HandlingInstructionsConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.util.FixMessageHandler;
import quickfix.field.ClOrdID;
import quickfix.field.OrderQty;
import quickfix.field.OrigClOrdID;
import quickfix.field.SenderSubID;
import quickfix.field.TransactTime;
import quickfix.fix44.Message;
import quickfix.fix44.OrderCancelRequest;

import java.time.ZoneOffset;
import java.util.Date;


public class DefaultFixOrderCancelRequestConverter implements Converter<FixOrderCancelRequest, OrderCancelRequest> {


	private FixMessageHandler<Message> fixMessageHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public OrderCancelRequest convert(FixOrderCancelRequest from) {
		OrderCancelRequest cancelRequest = new OrderCancelRequest();
		cancelRequest = QuickFixConverterUtils.convertFromFixSecurityEntity(from, cancelRequest);
		cancelRequest.set(new ClOrdID(from.getClientOrderStringId()));
		if (!StringUtils.isEmpty(from.getOriginalClientOrderStringId())) {
			cancelRequest.set(new OrigClOrdID(from.getOriginalClientOrderStringId()));
		}
		if (from.getTransactionTime() != null) {
			cancelRequest.set(new TransactTime(DateUtils.asLocalDateTime(from.getTransactionTime(), ZoneOffset.UTC)));
		}
		else {
			cancelRequest.set(new TransactTime(DateUtils.asLocalDateTime(new Date(), ZoneOffset.UTC)));
		}
		cancelRequest.setField((HandlingInstructionsConverter.getInstance()).toField(from.getHandlingInstructions()));
		if (!StringUtils.isEmpty(from.getSenderUserName()) || !StringUtils.isEmpty(from.getFixSenderSubId())) {
			String alias = getFixMessageHandler().getFixSenderSubIdForFixEntity(from, cancelRequest, false);
			if (alias != null) {
				cancelRequest.getHeader().setField(new SenderSubID(alias));
			}
		}

		if (from.getSide() != null) {
			cancelRequest.setField((OrderSideConverter.getInstance()).toField(from.getSide()));
		}

		if (from.getOrderQty() != null) {
			cancelRequest.setField(new OrderQty(from.getOrderQty()));
		}
		return cancelRequest;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageHandler<Message> getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler<Message> fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
