package com.clifton.fix.quickfix.converter;

import com.clifton.core.util.collections.TwoWayMap;
import quickfix.Field;


/**
 * @author manderson
 */
public abstract class BaseQuickFixFieldConverter<T, F extends Field<?>> implements QuickFixFieldConverter<T, F> {

	private final TwoWayMap<T, F> map;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BaseQuickFixFieldConverter() {
		this.map = new TwoWayMap<>();
		populateMap(this.map);
	}


	protected abstract void populateMap(TwoWayMap<T, F> map);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public F toField(T value) {
		return this.map.getFirst(value);
	}


	@Override
	public T fromField(F field) {
		return this.map.getSecond(field);
	}
}
