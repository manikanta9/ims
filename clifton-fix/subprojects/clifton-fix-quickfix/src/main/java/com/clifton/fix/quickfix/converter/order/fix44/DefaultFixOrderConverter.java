package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixOrder;
import quickfix.fix44.NewOrderSingle;


public class DefaultFixOrderConverter extends BaseOrderConverter implements Converter<FixOrder, NewOrderSingle> {

	@Override
	public NewOrderSingle convert(FixOrder value) {
		return fromFixOrder(value, new NewOrderSingle(), NewOrderSingle.NoPartyIDs.class, NewOrderSingle.NoPartyIDs.NoPartySubIDs.class);
	}
}
