package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.ProcessCodes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.ProcessCode;


public class ProcessCodeConverter extends BaseQuickFixFieldConverter<ProcessCodes, ProcessCode> {


	private static final Lazy<ProcessCodeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(ProcessCodeConverter::new);


	public static ProcessCodeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<ProcessCodes, ProcessCode> map) {
		ProcessCodes[] sources = ProcessCodes.values();
		for (ProcessCodes source : sources) {
			map.put(source, new ProcessCode(source.getCode()));
		}
	}
}
