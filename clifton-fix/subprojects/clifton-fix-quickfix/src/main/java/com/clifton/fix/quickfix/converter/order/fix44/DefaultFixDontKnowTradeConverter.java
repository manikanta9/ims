package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixDontKnowTrade;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityIDSourceConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityTypeConverter;
import quickfix.field.ExecID;
import quickfix.field.OrderID;
import quickfix.field.SecurityID;
import quickfix.field.Symbol;
import quickfix.fix44.DontKnowTrade;


public class DefaultFixDontKnowTradeConverter implements Converter<FixDontKnowTrade, DontKnowTrade> {

	@Override
	public DontKnowTrade convert(FixDontKnowTrade from) {
		DontKnowTrade result = new DontKnowTrade();

		result = QuickFixConverterUtils.convertFromFixEntity(from, result);
		result.setField(new OrderID(from.getOrderId()));
		result.setField(new ExecID(from.getExecutionId()));
		result.set(new Symbol(from.getSymbol()));
		result.set(new SecurityID(from.getSymbol()));
		result.setField((SecurityIDSourceConverter.getInstance()).toField(from.getSecurityIDSource()));
		result.setField((SecurityTypeConverter.getInstance()).toField(from.getSecurityType()));
		result.set((OrderSideConverter.getInstance()).toField(from.getSide()));

		return result;
	}
}
