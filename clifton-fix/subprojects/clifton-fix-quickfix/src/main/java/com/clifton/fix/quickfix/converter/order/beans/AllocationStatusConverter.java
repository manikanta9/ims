package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.AllocStatus;


public class AllocationStatusConverter extends BaseQuickFixFieldConverter<AllocationStatuses, AllocStatus> {


	private static final Lazy<AllocationStatusConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(AllocationStatusConverter::new);


	public static AllocationStatusConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<AllocationStatuses, AllocStatus> map) {

		map.put(AllocationStatuses.ACCEPTED, new AllocStatus(AllocStatus.ACCEPTED));
		map.put(AllocationStatuses.BLOCK_LEVEL_REJECT, new AllocStatus(AllocStatus.BLOCK_LEVEL_REJECT));
		map.put(AllocationStatuses.ACCOUNT_LEVEL_REJECT, new AllocStatus(AllocStatus.ACCOUNT_LEVEL_REJECT));
		map.put(AllocationStatuses.RECEIVED, new AllocStatus(AllocStatus.RECEIVED));
		map.put(AllocationStatuses.INCOMPLETE, new AllocStatus(AllocStatus.INCOMPLETE));
		map.put(AllocationStatuses.REJECTED_BY_INTERMEDIARY, new AllocStatus(AllocStatus.REJECTED_BY_INTERMEDIARY));
		map.put(AllocationStatuses.ALLOCATION_PENDING, new AllocStatus(AllocStatus.ALLOCATION_PENDING));
		map.put(AllocationStatuses.REVERSED, new AllocStatus(AllocStatus.REVERSED));
	}
}
