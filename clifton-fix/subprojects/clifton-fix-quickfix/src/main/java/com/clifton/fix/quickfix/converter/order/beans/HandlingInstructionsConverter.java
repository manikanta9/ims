package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.HandlInst;


public class HandlingInstructionsConverter extends BaseQuickFixFieldConverter<HandlingInstructions, HandlInst> {


	private static final Lazy<HandlingInstructionsConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(HandlingInstructionsConverter::new);


	public static HandlingInstructionsConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<HandlingInstructions, HandlInst> map) {
		map.put(HandlingInstructions.AUTOMATIC, new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION));
		map.put(HandlingInstructions.MANUAL, new HandlInst(HandlInst.MANUAL_ORDER_BEST_EXECUTION));
	}
}
