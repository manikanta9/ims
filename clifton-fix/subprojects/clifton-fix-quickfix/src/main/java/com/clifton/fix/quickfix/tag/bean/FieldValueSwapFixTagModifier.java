package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.AssertUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.FieldMap;
import quickfix.Message;
import quickfix.StringField;


public class FieldValueSwapFixTagModifier implements FixTagModifierBean<Message> {

	private Integer fromFieldTag;
	private Integer toFieldTag;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		AssertUtils.assertNotNull(getFromFieldTag(), "From Field Tag value cannot be null.");
		AssertUtils.assertNotNull(getToFieldTag(), "To Field Tag value cannot be null.");
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);

		try {
			FieldMap fromFldMap = dd.isHeaderField(getFromFieldTag()) ? message.getHeader() : message;
			FieldMap toFldMap = dd.isHeaderField(getToFieldTag()) ? message.getHeader() : message;

			String fromValue = null;
			if (fromFldMap.isSetField(getFromFieldTag())) {
				fromValue = fromFldMap.getField(new StringField(getFromFieldTag())).getValue();
			}
			String toValue = null;
			if (toFldMap.isSetField(getToFieldTag())) {
				toValue = toFldMap.getField(new StringField(getToFieldTag())).getValue();
			}

			if (fromValue != null) {
				toFldMap.setField(new StringField(getToFieldTag(), fromValue));
			}
			else if (toFldMap.isSetField(getToFieldTag())) {
				toFldMap.removeField(getToFieldTag());
			}
			if (toValue != null) {
				fromFldMap.setField(new StringField(getFromFieldTag(), toValue));
			}
			else if (fromFldMap.isSetField(getFromFieldTag())) {
				fromFldMap.removeField(getFromFieldTag());
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to modify message [" + message + "] in class [" + this.getClass() + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFromFieldTag() {
		return this.fromFieldTag;
	}


	public void setFromFieldTag(Integer fromFieldTag) {
		this.fromFieldTag = fromFieldTag;
	}


	public Integer getToFieldTag() {
		return this.toFieldTag;
	}


	public void setToFieldTag(Integer toFieldTag) {
		this.toFieldTag = toFieldTag;
	}
}
