package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.ExecutionTransactionTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.ExecTransType;


public class ExecutionTransactionTypeConverter extends BaseQuickFixFieldConverter<ExecutionTransactionTypes, ExecTransType> {

	private static final Lazy<ExecutionTransactionTypeConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(ExecutionTransactionTypeConverter::new);


	public static ExecutionTransactionTypeConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<ExecutionTransactionTypes, ExecTransType> map) {
		map.put(ExecutionTransactionTypes.NEW, new ExecTransType(ExecTransType.NEW));
		map.put(ExecutionTransactionTypes.CANCEL, new ExecTransType(ExecTransType.CANCEL));
		map.put(ExecutionTransactionTypes.CORRECT, new ExecTransType(ExecTransType.CORRECT));
		map.put(ExecutionTransactionTypes.STATUS, new ExecTransType(ExecTransType.STATUS));
	}
}
