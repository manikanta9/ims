package com.clifton.fix.quickfix.util;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.ioi.FixStipulation;
import com.clifton.fix.order.FixCompDealerQuote;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixNote;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixPartySub;
import com.clifton.fix.order.beans.StipulationTypes;
import com.clifton.fix.quickfix.converter.order.beans.CompDealerMidPriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.CompDealerQuotePriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.CompDealerQuoteTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.DealerQuotePriceTypesConverter;
import com.clifton.fix.quickfix.converter.order.beans.NoteTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartyIdSourcesConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartyRolesConverter;
import com.clifton.fix.quickfix.converter.order.beans.PartySubIDTypesConverter;
import com.clifton.fix.session.FixSessionDefinition;
import quickfix.DataDictionary;
import quickfix.DefaultDataDictionaryProvider;
import quickfix.DefaultMessageFactory;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.FieldType;
import quickfix.Group;
import quickfix.InvalidMessage;
import quickfix.Message;
import quickfix.MessageFactory;
import quickfix.MessageUtils;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.field.BeginString;
import quickfix.field.CompDealerID;
import quickfix.field.CompDealerMidPrice;
import quickfix.field.CompDealerMidPriceType;
import quickfix.field.CompDealerParQuote;
import quickfix.field.CompDealerQuotePrice;
import quickfix.field.CompDealerQuotePriceType;
import quickfix.field.CompDealerQuoteType;
import quickfix.field.DealerID;
import quickfix.field.DealerQuotePrice;
import quickfix.field.DealerQuotePriceType;
import quickfix.field.MsgType;
import quickfix.field.NoCompDealerQuotes;
import quickfix.field.NoDealers;
import quickfix.field.NoNotes;
import quickfix.field.NoStipulations;
import quickfix.field.NoteText;
import quickfix.field.NoteType;
import quickfix.field.PartyID;
import quickfix.field.PartySubID;
import quickfix.field.SenderCompID;
import quickfix.field.SenderLocationID;
import quickfix.field.SenderSubID;
import quickfix.field.StipulationType;
import quickfix.field.StipulationValue;
import quickfix.field.TargetCompID;
import quickfix.field.TargetLocationID;
import quickfix.field.TargetSubID;
import quickfix.fix44.ExecutionReport;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * <code>QuickFixMessageUtils</code> contains static convenience methods for use on QuickFix Messages.
 */
public class QuickFixMessageUtils {

	private static final DefaultDataDictionaryProvider DATA_DICTIONARY_PROVIDER = new DefaultDataDictionaryProvider();
	private static final MessageFactory DEFAULT_MESSAGE_FACTORY = new DefaultMessageFactory();

	/**
	 * Add fields to the message from the list of {@link FixCompDealerQuote} values.
	 * Each {@link FixCompDealerQuote} is added as its own group.
	 */
	public static void addCompDealerQuoteList(Message message, List<FixCompDealerQuote> quoteList) {
		for (FixCompDealerQuote quote : CollectionUtils.getIterable(quoteList)) {
			Group dealerQuote = new ExecutionReport.NoCompDealerQuotes();

			if (!StringUtils.isEmpty(quote.getCompDealerId())) {
				dealerQuote.setField(new CompDealerID(quote.getCompDealerId()));
			}
			if (quote.getCompDealerQuoteType() != null) {
				dealerQuote.setField((CompDealerQuoteTypesConverter.getInstance()).toField(quote.getCompDealerQuoteType()));
			}
			if (quote.getCompDealerQuotePrice() != null) {
				dealerQuote.setField(new CompDealerQuotePrice(quote.getCompDealerQuotePrice()));
			}
			if (quote.getCompDealerQuotePriceType() != null) {
				dealerQuote.setField(CompDealerQuotePriceTypesConverter.getInstance().toField(quote.getCompDealerQuotePriceType()));
			}
			if (quote.getCompDealerParQuote() != null) {
				dealerQuote.setField(new CompDealerParQuote(quote.getCompDealerParQuote()));
			}
			if (quote.getCompDealerMidPrice() != null) {
				dealerQuote.setField(new CompDealerMidPrice(quote.getCompDealerMidPrice()));
			}
			if (quote.getCompDealerMidPriceType() != null) {
				dealerQuote.setField((CompDealerMidPriceTypesConverter.getInstance()).toField(quote.getCompDealerMidPriceType()));
			}
			message.addGroup(dealerQuote);
		}
	}


	public static List<FixCompDealerQuote> getCompDealerQuoteList(Message message) throws FieldNotFound {
		List<FixCompDealerQuote> result = new ArrayList<>();

		if (message.isSetField(NoCompDealerQuotes.FIELD)) {
			List<Group> groups = message.getGroups(NoCompDealerQuotes.FIELD);
			for (Group quote : CollectionUtils.getIterable(groups)) {
				FixCompDealerQuote dealerQuote = new FixCompDealerQuote();
				dealerQuote.setCompDealerId(quote.getField(new CompDealerID()).getValue());
				if (quote.isSetField(CompDealerQuoteType.FIELD)) {
					dealerQuote.setCompDealerQuoteType((CompDealerQuoteTypesConverter.getInstance()).fromField((CompDealerQuoteType) quote.getField(new CompDealerQuoteType())));
				}
				if (quote.isSetField(CompDealerQuotePrice.FIELD)) {
					dealerQuote.setCompDealerQuotePrice(quote.getField(new CompDealerQuotePrice()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (quote.isSetField(CompDealerQuotePriceType.FIELD)) {
					dealerQuote.setCompDealerQuotePriceType(CompDealerQuotePriceTypesConverter.getInstance().fromField((CompDealerQuotePriceType) quote.getField(new CompDealerQuotePriceType())));
				}
				if (quote.isSetField(CompDealerParQuote.FIELD)) {
					dealerQuote.setCompDealerParQuote(quote.getField(new CompDealerParQuote()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (quote.isSetField(CompDealerMidPrice.FIELD)) {
					dealerQuote.setCompDealerMidPrice(quote.getField(new CompDealerMidPrice()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (quote.isSetField(CompDealerMidPriceType.FIELD)) {
					dealerQuote.setCompDealerMidPriceType((CompDealerMidPriceTypesConverter.getInstance()).fromField((CompDealerMidPriceType) quote.getField(new CompDealerQuotePriceType())));
				}
				result.add(dealerQuote);
			}
		}
		else if (message.isSetField(NoDealers.FIELD)) {
			// EMSX
			List<Group> quoteList = message.getGroups(NoDealers.FIELD);
			for (Group quote : CollectionUtils.getIterable(quoteList)) {
				FixCompDealerQuote dealerQuote = new FixCompDealerQuote();
				dealerQuote.setCompDealerId(quote.getField(new DealerID()).getValue());
				if (quote.isSetField(DealerQuotePrice.FIELD)) {
					dealerQuote.setCompDealerQuotePrice(quote.getField(new DealerQuotePrice()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (quote.isSetField(DealerQuotePriceType.FIELD)) {
					dealerQuote.setCompDealerQuotePriceType(DealerQuotePriceTypesConverter.getInstance().fromField((DealerQuotePriceType) quote.getField(new DealerQuotePriceType())));
				}
				result.add(dealerQuote);
			}
		}
		else if (message.isSetField(6230)) {
			// FX Connect
			String quoteDataString = message.getString(6230);
			String[] quotes = quoteDataString.split("[;|]");
			for (String quote : quotes) {
				FixCompDealerQuote dealerQuote = new FixCompDealerQuote();
				String[] quoteProperties = quote.split(",");
				for (String quoteProperty : quoteProperties) {
					String[] quotePropertyData = quoteProperty.split(":");
					String key = quotePropertyData[0];
					String value = quotePropertyData.length > 1 ? quotePropertyData[1] : null;

					switch (key) {
						case "bk":
							dealerQuote.setCompDealerId(value);
							break;
						case "f":
						case "s":
							// not used
							break;
						case "a":
							if (!StringUtils.isEmpty(value)) {
								dealerQuote.setCompDealerQuotePrice(new BigDecimal(value));
							}
							break;
					}
				}
				result.add(dealerQuote);
			}
		}
		return result;
	}


	public static void addFixPartyList(Message message, List<FixParty> partyList, Class<?> noPartyIDsClass, Class<?> noPartySubIDsClass) {

		for (FixParty fixParty : CollectionUtils.getIterable(partyList)) {
			Group party = (Group) BeanUtils.newInstance(noPartyIDsClass);

			party.setField(new PartyID(fixParty.getPartyId()));
			party.setField((PartyIdSourcesConverter.getInstance()).toField(fixParty.getPartyIdSource()));
			party.setField((PartyRolesConverter.getInstance()).toField(fixParty.getPartyRole()));

			for (FixPartySub sub : CollectionUtils.getIterable(fixParty.getPartySubList())) {
				Group partySub = (Group) BeanUtils.newInstance(noPartySubIDsClass);
				partySub.setField(new PartySubID(sub.getPartySubId()));
				partySub.setField((PartySubIDTypesConverter.getInstance()).toField(sub.getPartySubIdType()));

				party.addGroup(partySub);
			}
			message.addGroup(party);
		}
	}


	/**
	 * Returns a list of FixStipulation entities extracted from the Message.
	 *
	 * @return list if FixStipulation list is extracted from message, otherwise returns null
	 */
	public static List<FixStipulation> getStipulationList(Message message) throws FieldNotFound {
		List<Group> groupList = message.getGroups(NoStipulations.FIELD);
		if (CollectionUtils.isEmpty(groupList)) {
			return null;
		}
		List<FixStipulation> stipulationList = new ArrayList<>();

		for (Group stipulationsGrp : CollectionUtils.getIterable(groupList)) {
			FixStipulation fixStipulation = new FixStipulation();
			fixStipulation.setStipulationType(StipulationTypes.valueOf(stipulationsGrp.getField(new StipulationType()).getValue()));
			fixStipulation.setStipulationValue(stipulationsGrp.getField(new StipulationValue()).getValue());
			stipulationList.add(fixStipulation);
		}

		return stipulationList;
	}


	/**
	 * Get the session id using only the required fields (BeginString, SenderCompID, TargetCompID) and the session qualifier.
	 */
	public static SessionID getSessionID(Message message, String sessionQualifier) {
		try {
			final String senderCompID;
			senderCompID = message.getHeader().getString(SenderCompID.FIELD);
			final String targetCompID = message.getHeader().getString(TargetCompID.FIELD);

			return new SessionID(message.getHeader().getString(BeginString.FIELD), senderCompID, targetCompID, StringUtils.isEmpty(sessionQualifier) ? "" : sessionQualifier);
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * Get the session id using all available session id fields.
	 */
	public static SessionID getSessionIDWithOptionalFields(Message fixMessage, String sessionQualifier) {
		final Message.Header header = fixMessage.getHeader();
		return new SessionID(getFieldOrDefault(header, BeginString.FIELD, null), getFieldOrDefault(header, SenderCompID.FIELD, null), getFieldOrDefault(header, SenderSubID.FIELD, null),
				getFieldOrDefault(header, SenderLocationID.FIELD, null), getFieldOrDefault(header, TargetCompID.FIELD, null), getFieldOrDefault(header, TargetSubID.FIELD, null), getFieldOrDefault(
				header, TargetLocationID.FIELD, null), sessionQualifier);
	}


	/**
	 * Get the session id with only the required fields from an existing session id.
	 */
	public static SessionID getSessionIDRequired(SessionID sessionId) {
		return new SessionID(sessionId.getBeginString(), sessionId.getSenderCompID(), sessionId.getTargetCompID(), sessionId.getSessionQualifier());
	}


	public static SessionID getReverseSessionID(Message message, String sessionQualifier) {
		try {
			final String senderCompID;
			senderCompID = message.getHeader().getString(SenderCompID.FIELD);
			final String targetCompID = message.getHeader().getString(TargetCompID.FIELD);

			return new SessionID(message.getHeader().getString(BeginString.FIELD), targetCompID, senderCompID, StringUtils.isEmpty(sessionQualifier) ? "" : sessionQualifier);
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}


	public static SessionID getReverseSessionIDWithOptionalFields(Message fixMessage, String sessionQualifier) {
		final Message.Header header = fixMessage.getHeader();
		return new SessionID(getFieldOrDefault(header, BeginString.FIELD, null), getFieldOrDefault(header, TargetCompID.FIELD, null), getFieldOrDefault(header, TargetSubID.FIELD, null),
				getFieldOrDefault(header, TargetLocationID.FIELD, null), getFieldOrDefault(header, SenderCompID.FIELD, null), getFieldOrDefault(header, SenderSubID.FIELD, null), getFieldOrDefault(
				header, SenderLocationID.FIELD, null), sessionQualifier);
	}


	/**
	 * Get the session id using only the required fields (BeginString, SenderCompID, TargetCompID) and the session qualifier.
	 */
	public static SessionID getSessionID(FixEntity entity) {
		return new SessionID(entity.getFixBeginString(), entity.getFixSenderCompId(), entity.getFixTargetCompId(), StringUtils.isEmpty(entity.getFixSessionQualifier()) ? ""
				: entity.getFixSessionQualifier());
	}


	/**
	 * Get the session id using all available session id fields.
	 */
	public static SessionID getSessionIDWithOptionalFields(FixEntity entity) {
		return new SessionID(entity.getFixBeginString(), entity.getFixSenderCompId(), entity.getFixSenderSubId(), entity.getFixSenderLocId(), entity.getFixTargetCompId(), entity.getFixTargetSubId(),
				entity.getFixTargetLocId(), StringUtils.isEmpty(entity.getFixSessionQualifier()) ? "" : entity.getFixSessionQualifier());
	}


	/**
	 * Return the value of a field.
	 */
	public static String getStringField(String messageString, int tag) {
		return quickfix.MessageUtils.getStringField(messageString, tag);
	}



	/**
	 * Return the Java Class type for the provided field.
	 */
	public static Class<?> getJavaType(Message message, int field) {
		FieldType fieldType = getDataDictionary(message).getFieldType(field);
		return fieldType.getJavaType();
	}


	/**
	 * Indicates if a message is an administrative message.
	 */
	public static boolean isAdminMessage(String msgType) {
		return quickfix.MessageUtils.isAdminMessage(msgType);
	}


	/**
	 * Parse a FIX message - Use the SessionSpecific DataDict from File Handle if configured.
	 */
	public static Message parse(String messageString) throws InvalidMessage, SecurityException {
		DataDictionary dd = null;

		//Use QFix Internals to locate Active Session and try to get overridden Dictionary if overridden
		SessionID sessionID = MessageUtils.getSessionID(messageString);
		Session session = Session.lookupSession(sessionID);
		if (session == null) {
			// If no session is found, then use only the required fields
			session = Session.lookupSession(getSessionIDRequired(sessionID));
			if (session == null) {
				sessionID = MessageUtils.getReverseSessionID(messageString);
				session = Session.lookupSession(sessionID);
				if (session == null) {
					// If no session is found, then use only the required fields
					session = Session.lookupSession(getSessionIDRequired(sessionID));
				}
			}
		}
		if (session != null) {
			dd = session.getDataDictionary(); //Lookup DataDictionary if set at Session Level
		}

		//Fallback if Dictionary from File Load Errors ( FOR FIX SESSION SPECIFIC DICTIONARY LOAD)
		if (dd == null) {
			dd = DATA_DICTIONARY_PROVIDER.getSessionDataDictionary(getStringField(messageString, BeginString.FIELD));
		}
		//dd.setCheckUnorderedGroupFields(false);
		Message message = MessageUtils.parse(DEFAULT_MESSAGE_FACTORY, dd, messageString);
		if (message.getException() != null) {
			/*
			 * On error, QuickFIX/J halts parsing and attaches the exception directly to the message rather than failing serialization. This can lead to difficult-to-detect errors
			 * from partially-parsed messages.
			 */
			LogUtils.warn(QuickFixMessageUtils.class, String.format("An exception occurred while parsing the FIX message. Message processing will continue, but some message data may be lost. Message text: [%s].", messageString), message.getException());
		}
		return message;
	}


	/**
	 * Returns the message type string.
	 */
	public static String getMessageType(String messageString) {
		try {
			return quickfix.MessageUtils.getMessageType(messageString);
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to determine message type.", e);
		}
	}


	/**
	 * Indicates if the message is a heartbeat.
	 */
	public static boolean isHeartbeat(String message) {
		return quickfix.MessageUtils.isHeartbeat(message);
	}


	public static boolean isHeartbeat(Message message) {
		try {
			return message.getHeader().isSetField(MsgType.FIELD) && message.getHeader().getString(MsgType.FIELD).equals(MsgType.HEARTBEAT);
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to determine the message type.", e);
		}
	}


	public static DataDictionary getDataDictionaryForVersion(String version) {
		return DATA_DICTIONARY_PROVIDER.getSessionDataDictionary(version);
	}


	public static DataDictionary getDataDictionary(String message) {
		return getDataDictionaryForVersion(quickfix.MessageUtils.getStringField(message, BeginString.FIELD));
	}


	public static DataDictionary getDataDictionary(Message message) {
		try {
			return DATA_DICTIONARY_PROVIDER.getSessionDataDictionary(message.getHeader().getField(new BeginString()).getValue());
		}
		catch (FieldNotFound e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * Sets session information in the message header based on the definition.
	 */
	public static void setMessageHeaderFieldsFromSessionDefinition(Message message, FixSessionDefinition definition) {
		Message.Header header = message.getHeader();
		if (definition.getBeginString() != null) {
			header.setString(BeginString.FIELD, definition.getBeginString());
		}
		if (definition.getSenderCompId() != null) {
			header.setString(SenderCompID.FIELD, definition.getSenderCompId());
		}
		if (definition.getSenderSubId() != null) {
			header.setString(SenderSubID.FIELD, definition.getSenderSubId());
		}
		if (definition.getSenderLocId() != null) {
			header.setString(SenderLocationID.FIELD, definition.getSenderLocId());
		}
		if (definition.getTargetCompId() != null) {
			header.setString(TargetCompID.FIELD, definition.getTargetCompId());
		}
		if (definition.getTargetSubId() != null) {
			header.setString(TargetSubID.FIELD, definition.getTargetSubId());
		}
		if (definition.getTargetLocId() != null) {
			header.setString(TargetLocationID.FIELD, definition.getTargetLocId());
		}
	}


	private static String getFieldOrDefault(FieldMap fields, int tag, String defaultValue) {
		if (fields.isSetField(tag)) {
			try {
				return fields.getString(tag);
			}
			catch (final FieldNotFound e) {
				// ignore, should never happen
				return null;
			}
		}
		return defaultValue;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Get the value of the string field as an {@link Optional}.
	 * If the field is not set, Optional.empty (null value) is returned, otherwise, return the field
	 * value as a string.
	 */
	public static Optional<String> getStringFieldValue(FieldMap m, int f, String defaultValue) {
		AssertUtils.assertNotNull(defaultValue, "The default value cannot be null.");
		try {
			if (m.isSetField(f)) {
				return m.getString(f) == null ? Optional.of(defaultValue) : Optional.of(m.getString(f));
			}
			else {
				return Optional.empty();
			}
		}
		catch (FieldNotFound fieldNotFound) {
			// swallow exception
		}
		return Optional.empty();
	}


	/**
	 * Get the value of the decimal field as an {@link Optional}.
	 * If the field is not set, Optional.empty (null value) is returned, otherwise, return the field
	 * value as a decimal.  A blank field value will return the default parameter value.
	 */
	public static Optional<BigDecimal> getDecimalFieldValue(FieldMap m, int f, BigDecimal defaultValue) {
		AssertUtils.assertNotNull(defaultValue, "The default value cannot be null.");
		try {
			if (m.isSetField(f)) {
				return m.getDecimal(f) == null ? Optional.of(defaultValue) : Optional.of(m.getDecimal(f));
			}
			else {
				return Optional.empty();
			}
		}
		catch (FieldNotFound fieldNotFound) {
			// swallow exception
		}
		return Optional.empty();
	}


	/**
	 * Get a Map of a messages to group field values.  If the message's group is not set, return an empty list.
	 * If the field is not set, return Optional.empty, otherwise, return the Optional of the field's string value.
	 * The group values are returned in sorted order based on the string's natural order.
	 */
	public static Map<Message, List<Optional<String>>> getStringGroupFieldValues(List<Message> messages, int g, int f, String defaultValue) {
		Map<Message, List<Optional<String>>> results = new HashMap<>();
		for (Message msg : CollectionUtils.asNonNullList(messages)) {
			results.put(msg, Collections.emptyList());
			if (msg.hasGroup(g)) {
				results.put(msg, msg.getGroups(g)
						.stream()
						.filter(grp -> grp.isSetField(f))
						.map(grp -> getStringFieldValue(grp, f, defaultValue))
						.sorted(Comparator.comparing(Optional::get))
						.collect(Collectors.toList())
				);
			}
		}
		return results;
	}


	/**
	 * Get a Map of a messages to group field values.  If the message's group is not set, return an empty list.
	 * If the field  is not set, return Optional.empty, otherwise, return the Optional of the field's decimal value.
	 * A blank decimal value will return the default parameter value.  The group values are returned in sorted order
	 * based on the decimal's natural order.
	 */
	public static Map<Message, List<Optional<BigDecimal>>> getDecimalGroupFieldValues(List<Message> messages, int g, int f, BigDecimal defaultValue) {
		Map<Message, List<Optional<BigDecimal>>> results = new HashMap<>();
		for (Message msg : CollectionUtils.asNonNullList(messages)) {
			results.put(msg, Collections.emptyList());
			if (msg.hasGroup(g)) {
				results.put(msg, msg.getGroups(g)
						.stream()
						.filter(grp -> grp.isSetField(f))
						.map(grp -> getDecimalFieldValue(grp, f, defaultValue))
						.sorted(Comparator.comparing(Optional::get))
						.collect(Collectors.toList())
				);
			}
		}
		return results;
	}


	/**
	 * Get a Map of a messages to aggregated group field values.  If the message's group is not set, return an empty list.
	 * If the field is not set, return Optional.empty, otherwise, return the Optional of the cumulative decimal field value.
	 * A blank decimal value will use the default parameter value.
	 */
	public static Optional<BigDecimal> getAggregatedDecimalFieldValue(Message message, int g, int f, BigDecimal defaultValue) {
		if (message.hasGroup(g)) {
			return Optional.of(message.getGroups(g)
					.stream()
					.filter(grp -> grp.isSetField(f))
					.map(grp -> getDecimalFieldValue(grp, f, defaultValue))
					.filter(Optional::isPresent)
					.map(Optional::get)
					.reduce(BigDecimal.ZERO, BigDecimal::add));
		}
		return Optional.empty();
	}


	/**
	 * The first and second list are compared element by element for equality.
	 * The lists must be sorted and the same size.
	 */
	public static boolean areEqualBigDecimalList(List<Optional<BigDecimal>> first, List<Optional<BigDecimal>> second) {
		Iterator<Optional<BigDecimal>> v1Iterator = CollectionUtils.asNonNullList(first).iterator();
		Iterator<Optional<BigDecimal>> v2Iterator = CollectionUtils.asNonNullList(second).iterator();
		while (v1Iterator.hasNext() && v2Iterator.hasNext()) {
			Optional<BigDecimal> b1 = v1Iterator.next();
			Optional<BigDecimal> b2 = v2Iterator.next();
			if (b1.isPresent() && b2.isPresent() && (b1.get().compareTo(b2.get()) != 0)) {
				return false;
			}
		}
		return !(v1Iterator.hasNext() || v2Iterator.hasNext());
	}


	/**
	 * The first and second list are compared element by element for equality.
	 * The lists must be sorted and the same size.
	 */
	public static boolean areEqualStringList(List<Optional<String>> first, List<Optional<String>> second) {
		Iterator<Optional<String>> v1Iterator = CollectionUtils.asNonNullList(first).iterator();
		Iterator<Optional<String>> v2Iterator = CollectionUtils.asNonNullList(second).iterator();
		while (v1Iterator.hasNext() && v2Iterator.hasNext()) {
			Optional<String> s1 = v1Iterator.next();
			Optional<String> s2 = v2Iterator.next();
			if (s1.isPresent() && s2.isPresent() && !(StringUtils.isEqual(s1.get(), s2.get()))) {
				return false;
			}
		}
		return !(v1Iterator.hasNext() || v2Iterator.hasNext());
	}


	/**
	 * Function to Past NoNotes Fix group.
	 */
	public static List<FixNote> getFixNotesList(Message message) throws FieldNotFound {
		List<FixNote> result = new ArrayList<>();

		if (!message.isSetField(NoNotes.FIELD)) {
			return result;
		}

		List<Group> notes = message.getGroups(NoNotes.FIELD);

		for (Group note : CollectionUtils.getIterable(notes)) {
			FixNote resultNote = new FixNote();

			if (note.isSetField(NoteType.FIELD)) {
				resultNote.setNoteType(NoteTypeConverter.getInstance().fromField((NoteType) note.getField(new NoteType())));
			}

			if (note.isSetField(NoteText.FIELD)) {
				resultNote.setNote(note.getField(new NoteText()).getValue().trim());
			}
			result.add(resultNote);
		}

		return result;
	}

}
