package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.CommType;


public class CommissionTypesConverter extends BaseQuickFixFieldConverter<CommissionTypes, CommType> {

	private static final Lazy<CommissionTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(CommissionTypesConverter::new);


	public static CommissionTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CommissionTypes, CommType> map) {
		map.put(CommissionTypes.PER_UNIT, new CommType(CommType.PER_UNIT));
		map.put(CommissionTypes.PERCENTAGE, new CommType(CommType.PERCENT));
		map.put(CommissionTypes.ABSOLUTE, new CommType(CommType.ABSOLUTE));
		map.put(CommissionTypes.PERCENTAGE_WAIVED_CASH_DISCOUNT, new CommType(CommType.PERCENTAGE_WAIVED_CASH_DISCOUNT));
		map.put(CommissionTypes.PERCENTAGE_WAIVED_ENHANCED_UNITS, new CommType(CommType.PERCENTAGE_WAIVED_ENHANCED_UNITS));
		map.put(CommissionTypes.POINTS_PER_BOND_OR_OR_CONTRACT, new CommType(CommType.POINTS_PER_BOND_OR_CONTRACT));
	}
}
