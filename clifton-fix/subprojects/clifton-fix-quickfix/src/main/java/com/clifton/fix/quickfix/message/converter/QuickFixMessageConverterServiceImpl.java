package com.clifton.fix.quickfix.message.converter;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.fix.message.converter.FixMessageConverterService;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.quickfix.converter.StringToMessageConverter;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import quickfix.Message;
import quickfix.SessionID;

import java.util.Map;


/**
 * The <code>QuickFixMessageConverterService</code> implements message converter service for
 * quickfix.
 *
 * @author mwacker
 */
public class QuickFixMessageConverterServiceImpl implements FixMessageConverterService<Message> {

	/**
	 * Maps the session string to a map of classes to converters.  This allows registration of session
	 * specific converters.
	 * <p/>
	 * NOTE:  Default converters are registered with session name "default", and will be used if no converter
	 * is registered for the session and class.
	 */
	private Map<String, Map<Class<?>, Converter<?, ?>>> fixMessageConverterMap;


	@Override
	public Message parse(String text) {
		return (StringToMessageConverter.getInstance()).convert(text);
	}


	@Override
	@SuppressWarnings("unchecked")
	public <F extends FixEntity, Q extends Message> F toFixEntity(Q message, String sessionQualifier) {
		SessionID sessionId = QuickFixMessageUtils.getSessionIDWithOptionalFields(message, sessionQualifier);

		Converter<Q, F> converter = (Converter<Q, F>) getMessageConverter(sessionId, message.getClass());
		AssertUtils.assertNotNull(converter, "Unable to find a message converter for messages of type [%s].", message.getClass().getName());
		return converter.convert(message);
	}


	@Override
	@SuppressWarnings("unchecked")
	public <Q extends Message, F extends FixEntity> Q fromFixEntity(F entity) {
		SessionID sessionId = QuickFixMessageUtils.getSessionIDWithOptionalFields(entity);
		Converter<F, Q> converter = (Converter<F, Q>) getMessageConverter(sessionId, entity.getClass());
		AssertUtils.assertNotNull(converter, "Unable to find a message converter for entities of type [%s].", entity.getClass().getName());
		return converter.convert(entity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Converter<?, ?> getMessageConverter(SessionID sessionId, Class<?> fromClass) {
		Map<Class<?>, Converter<?, ?>> defaultMap = getFixMessageConverterMap().get("default");
		Map<Class<?>, Converter<?, ?>> converterMap = null;
		if (getFixMessageConverterMap().containsKey(sessionId.toString())) {
			converterMap = getFixMessageConverterMap().get(sessionId.toString());
		}
		else if (getFixMessageConverterMap().containsKey(QuickFixMessageUtils.getSessionIDRequired(sessionId).toString())) {
			converterMap = getFixMessageConverterMap().get(sessionId.toString());
		}
		Converter<?, ?> converter = null;
		if ((converterMap != null) && converterMap.containsKey(fromClass)) {
			converter = converterMap.get(fromClass);
		}
		if (converter == null) {
			converter = defaultMap.get(fromClass);
		}
		AssertUtils.assertNotNull(converter, "No suitable message converter was found for session ID [%s] and message class [%s].", sessionId, fromClass);
		return converter;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, Map<Class<?>, Converter<?, ?>>> getFixMessageConverterMap() {
		return this.fixMessageConverterMap;
	}


	public void setFixMessageConverterMap(Map<String, Map<Class<?>, Converter<?, ?>>> fixMessageConverterMap) {
		this.fixMessageConverterMap = fixMessageConverterMap;
	}
}
