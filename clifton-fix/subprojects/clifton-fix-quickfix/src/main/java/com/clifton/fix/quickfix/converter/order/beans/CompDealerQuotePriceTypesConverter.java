package com.clifton.fix.quickfix.converter.order.beans;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.collections.TwoWayMap;
import com.clifton.fix.order.beans.CompDealerQuotePriceTypes;
import com.clifton.fix.quickfix.converter.BaseQuickFixFieldConverter;
import quickfix.field.CompDealerQuotePriceType;


public class CompDealerQuotePriceTypesConverter extends BaseQuickFixFieldConverter<CompDealerQuotePriceTypes, CompDealerQuotePriceType> {


	private static final Lazy<CompDealerQuotePriceTypesConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(CompDealerQuotePriceTypesConverter::new);


	public static CompDealerQuotePriceTypesConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateMap(TwoWayMap<CompDealerQuotePriceTypes, CompDealerQuotePriceType> map) {
		CompDealerQuotePriceTypes[] sources = CompDealerQuotePriceTypes.values();
		for (CompDealerQuotePriceTypes source : sources) {
			map.put(source, new CompDealerQuotePriceType(source.getCode()));
		}
	}
}
