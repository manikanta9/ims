package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.converter.Converter;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import quickfix.fix44.OrderCancelReplaceRequest;


public class DefaultFixOrderCancelReplaceRequestConverter extends BaseOrderConverter implements Converter<FixOrderCancelReplaceRequest, OrderCancelReplaceRequest> {

	@Override
	public OrderCancelReplaceRequest convert(FixOrderCancelReplaceRequest value) {
		return fromFixOrder(value, new OrderCancelReplaceRequest(), OrderCancelReplaceRequest.NoPartyIDs.class, OrderCancelReplaceRequest.NoPartyIDs.NoPartySubIDs.class);
	}
}
