package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.DataDictionary;
import quickfix.Message;
import quickfix.StringField;


/**
 * A tag modifier to reformat an account number within a specified FIX tag to remove all "-" characters from the account number and
 * apply a "-T" suffix to that account number.
 */
public class RediPTGoldmanAccountNumberTagModifier implements FixTagModifierBean<Message> {

	// FIX tag number of the field containing the Account number.
	Integer fieldTag;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(Message message) {
		ValidationUtils.assertNotNull(getFieldTag(), "RediPTGoldmanAccountNumberTagModifier requires that the fieldTag is not null.");
		String accountNumber = "";
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);
		boolean isHeaderField = dd.isHeaderField(getFieldTag());
		try {
			if (isHeaderField && message.getHeader().isSetField(getFieldTag())) {
				accountNumber = message.getHeader().getString(getFieldTag());
			}
			else if (message.isSetField(getFieldTag())) {
				accountNumber = message.getString(getFieldTag());
			}

			if (!StringUtils.isEmpty(accountNumber) && (accountNumber.startsWith("157-") || accountNumber.startsWith("020-"))) {
				accountNumber = accountNumber.replace("-", "") + "-T";
				if (isHeaderField) {
					message.getHeader().setField(new StringField(getFieldTag(), accountNumber));
				}
				else {
					message.setField(new StringField(getFieldTag(), accountNumber));
				}
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to modify message [" + message + "] in class [" + this.getClass() + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(Integer fieldTag) {
		this.fieldTag = fieldTag;
	}
}
