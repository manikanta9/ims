package com.clifton.fix.quickfix.converter.order.fix44;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.quickfix.converter.order.beans.AllocationNoOrdersTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationReportTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.AllocationTransactionTypeConverter;
import com.clifton.fix.quickfix.converter.order.beans.OrderSideConverter;
import com.clifton.fix.quickfix.converter.order.beans.ProcessCodeConverter;
import com.clifton.fix.quickfix.converter.order.beans.SecurityIDSourceConverter;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.AllocAccount;
import quickfix.field.AllocAccruedInterestAmt;
import quickfix.field.AllocAvgPx;
import quickfix.field.AllocGrossTradeAmt;
import quickfix.field.AllocID;
import quickfix.field.AllocNetMoney;
import quickfix.field.AllocNoOrdersType;
import quickfix.field.AllocPrice;
import quickfix.field.AllocReportID;
import quickfix.field.AllocReportType;
import quickfix.field.AllocShares;
import quickfix.field.AllocText;
import quickfix.field.AllocTransType;
import quickfix.field.AvgPx;
import quickfix.field.Currency;
import quickfix.field.MaturityMonthYear;
import quickfix.field.NoAllocs;
import quickfix.field.NoExecs;
import quickfix.field.NoOrders;
import quickfix.field.ProcessCode;
import quickfix.field.SecurityID;
import quickfix.field.SecurityIDSource;
import quickfix.field.Shares;
import quickfix.field.Side;
import quickfix.field.Symbol;
import quickfix.field.Text;
import quickfix.field.TradeDate;
import quickfix.field.TransactTime;
import quickfix.fix44.AllocationReport;

import java.math.RoundingMode;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;


public class DefaultAllocationReportConverter implements Converter<Message, FixAllocationReport> {

	@Override
	public FixAllocationReport convert(Message message) {
		try {
			FixAllocationReport allocationReport = new FixAllocationReport();

			allocationReport = QuickFixConverterUtils.convertToFixEntity(message, allocationReport);
			allocationReport.setAllocationStringId(message.getField(new AllocID()).getValue());
			//allocationReport.setAllocationId(FixUtils.parseUniqueId(allocationReport.getAllocationStringId()));

			allocationReport.setAllocationReportId(message.getField(new AllocReportID()).getValue());
			allocationReport.setAllocationReportType((AllocationReportTypeConverter.getInstance()).fromField((AllocReportType) message.getField(new AllocReportType())));
			allocationReport.setAllocationTransactionType((AllocationTransactionTypeConverter.getInstance()).fromField((AllocTransType) message.getField(new AllocTransType())));
			if (message.isSetField(AllocNoOrdersType.FIELD)) {
				allocationReport.setAllocationNoOrdersType((AllocationNoOrdersTypeConverter.getInstance()).fromField((AllocNoOrdersType) message.getField(new AllocNoOrdersType())));
			}

			// TODO: Figure out what to do with these.
			int orderCount = message.getGroupCount(NoOrders.FIELD);
			for (int i = 0; i < orderCount; i++) {
				//				quickfix.fix44.AllocationReport.NoOrders order = (quickfix.fix44.AllocationReport.NoOrders) message.getGroup(i, new quickfix.fix44.AllocationReport.NoOrders());
				//				int localOrderId = Integer.parseInt(order.getField(new ClOrdID()).getValue());
				//				String orderId = order.getField(new OrderID()).getValue();
			}
			// TODO: Figure out what to do with these.
			int execCount = message.getGroupCount(NoExecs.FIELD);
			for (int i = 0; i < execCount; i++) {
				//				quickfix.fix44.AllocationReport.NoExecs exec = (quickfix.fix44.AllocationReport.NoExecs) message.getGroup(i, new quickfix.fix44.AllocationReport.NoExecs());
				//				BigDecimal lastShares = new BigDecimal(exec.getField(new LastShares()).getValue());
				//				String execId = exec.getField(new ExecID()).getValue();
				//				String secondaryExecId = exec.getField(new SecondaryExecID()).getValue();
				//				BigDecimal lastPx = new BigDecimal(exec.getField(new LastPx()).getValue());
			}
			if (message.isSetField(Side.FIELD)) {
				allocationReport.setSide((OrderSideConverter.getInstance()).fromField((Side) message.getField(new Side())));
			}
			if (message.isSetField(Symbol.FIELD)) {
				allocationReport.setSymbol(message.getField(new Symbol()).getValue());
			}
			if (message.isSetField(SecurityID.FIELD)) {
				allocationReport.setSecurityId(message.getField(new SecurityID()).getValue());
			}
			if (message.isSetField(SecurityIDSource.FIELD)) {
				allocationReport.setSecurityIDSource((SecurityIDSourceConverter.getInstance()).fromField((SecurityIDSource) message.getField(new SecurityIDSource())));
			}
			if (message.isSetField(MaturityMonthYear.FIELD)) {
				allocationReport.setMaturityMonthYear(message.getField(new MaturityMonthYear()).getValue());
			}
			allocationReport.setShares(message.getField(new Shares()).getValue().setScale(15, RoundingMode.HALF_UP));
			if (message.isSetField(AvgPx.FIELD)) {
				allocationReport.setAveragePrice(message.getField(new AvgPx()).getValue().setScale(15, RoundingMode.HALF_UP));
			}
			if (message.isSetField(Currency.FIELD)) {
				allocationReport.setCurrency(message.getField(new Currency()).getValue());
			}
			if (message.isSetField(TradeDate.FIELD)) {
				allocationReport.setTradeDate(message.getField(new TradeDate()).getValue());
			}
			if (message.isSetField(TransactTime.FIELD)) {
				allocationReport.setTransactionTime(DateUtils.asUtilDate(message.getField(new TransactTime()).getValue(), ZoneOffset.UTC));
			}
			if (message.isSetField(Text.FIELD)) {
				allocationReport.setText(message.getField(new Text()).getValue());
			}

			allocationReport.setFixAllocationReportDetailList(new ArrayList<>());
			List<Group> groups = message.getGroups(NoAllocs.FIELD);
			for (Group alloc : CollectionUtils.getIterable(groups)) {

				FixAllocationReportDetail reportDetail = new FixAllocationReportDetail();
				reportDetail.setFixAllocationReport(allocationReport);
				reportDetail.setAllocationAccount(alloc.getField(new AllocAccount()).getValue());
				reportDetail.setAllocationShares(alloc.getField(new AllocShares()).getValue().setScale(15, RoundingMode.HALF_UP));
				if (alloc.isSetField(ProcessCode.FIELD)) {
					reportDetail.setProcessCode((ProcessCodeConverter.getInstance()).fromField((ProcessCode) alloc.getField(new ProcessCode())));
				}
				if (alloc.isSetField(AllocAvgPx.FIELD)) {
					reportDetail.setAllocationAveragePrice(alloc.getField(new AllocAvgPx()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (alloc.isSetField(AllocPrice.FIELD)) {
					reportDetail.setAllocationPrice(alloc.getField(new AllocPrice()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (alloc.isSetField(AllocText.FIELD)) {
					reportDetail.setAllocationText(alloc.getField(new AllocText()).getValue());
				}
				if (alloc.isSetField(AllocNetMoney.FIELD)) {
					reportDetail.setAllocationNetMoney(alloc.getField(new AllocNetMoney()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (alloc.isSetField(AllocGrossTradeAmt.FIELD)) {
					reportDetail.setAllocationGrossTradeAmount(alloc.getField(new AllocGrossTradeAmt()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				if (alloc.isSetField(AllocAccruedInterestAmt.FIELD)) {
					reportDetail.setAllocationAccruedInterestAmount(alloc.getField(new AllocAccruedInterestAmt()).getValue().setScale(15, RoundingMode.HALF_UP));
				}
				allocationReport.getFixAllocationReportDetailList().add(reportDetail);
				// TODO: process nested party ids
			}

			return allocationReport;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert fix [" + AllocationReport.class + "] to [" + FixAllocationReport.class + "].", e);
		}
	}
}
