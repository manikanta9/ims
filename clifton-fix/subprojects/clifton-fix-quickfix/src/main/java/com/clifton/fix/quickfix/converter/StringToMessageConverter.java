package com.clifton.fix.quickfix.converter;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.converter.Converter;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import quickfix.Message;


/**
 * The <code>StringToMessageConverter</code> converts a FIX message string to a QuickFIX message.
 *
 * @author mwacker
 */
public class StringToMessageConverter implements Converter<String, Message> {


	private static final Lazy<StringToMessageConverter> SINGLETON_INSTANCE_LAZY = new Lazy<>(StringToMessageConverter::new);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static StringToMessageConverter getInstance() {
		return SINGLETON_INSTANCE_LAZY.get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message convert(String from) {
		try {
			return QuickFixMessageUtils.parse(from);
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
}
