package com.clifton.fix.quickfix.tag.bean;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.MaturityDate;
import quickfix.field.NoPartyIDs;
import quickfix.field.PartyID;
import quickfix.field.PartyIDSource;
import quickfix.field.PartyRole;
import quickfix.field.Text;
import quickfix.fix44.NewOrderSingle;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>FXConnectOrderFixTagModifier</code> FXConnect has a bug so this tag modifier is required in order to do a competitive NDF.
 * This tag modifier is used to properly set up an FX Connect trade for Competitive or Portfolio trades by inserting into the FIX Message a "NoPartyIds" FIX group
 * containing the executing broker code and a Text field indicating whether the trade is a Competitive or Portfolio trade.  If the
 * ExecutingBrokerCode string contains multiple delimited broker codes, the trade is considered Competitive, if only one exists, it is a Portfolio trade.
 *
 * @author mwacker
 */
public class FXConnectOrderFixTagModifier implements FixTagModifierBean<Message> {

	@Override
	public void execute(Message message) {
		try {
			String text = message.isSetField(Text.FIELD) ? (": " + message.getField(new Text()).getValue()) : "";
			String executingBrokerCode = getExecutingBrokerCodesAndRemoveFields(message);

			if (!StringUtils.isEmpty(executingBrokerCode)) {
				Group party = new NewOrderSingle.NoPartyIDs();

				party.setField(new PartyID(executingBrokerCode));
				party.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
				party.setField(new PartyRole(PartyRole.EXECUTING_FIRM));

				message.addGroup(party);

				if (executingBrokerCode.split(";").length > 1) {
					message.removeField(MaturityDate.FIELD);
					message.setField(new Text("COMPETITIVE" + text));
				}
				else {
					message.setField(new Text("PORTFOLIO" + text));
				}
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to execute tag modifier [" + this.getClass() + "].", e);
		}
	}


	private String getExecutingBrokerCodesAndRemoveFields(Message message) throws FieldNotFound {
		List<String> brokerCodeList = new ArrayList<>();
		List<Group> removeGroupList = new ArrayList<>();
		List<Group> groups = message.getGroups(NoPartyIDs.FIELD);
		for (Group party : CollectionUtils.getIterable(groups)) {
			if (party.isSetField(PartyID.FIELD) && party.isSetField(PartyRole.FIELD) &&
					((PartyRole.ACCEPTABLE_COUNTERPARTY == party.getField(new PartyRole()).getValue()) || (PartyRole.EXECUTING_FIRM == party.getField(new PartyRole()).getValue()))) {
				brokerCodeList.add(party.getField(new PartyID()).getValue());
				removeGroupList.add(party);
			}
		}

		for (Group party : CollectionUtils.getIterable(removeGroupList)) {
			message.removeGroup(party);
		}

		return StringUtils.join(brokerCodeList, ";");
	}
}
