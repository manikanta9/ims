package com.clifton.fix.quickfix.server.tag;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CacheStats;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCustomKeyListCache;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.destination.FixDestinationTagModifierFilter;
import com.clifton.fix.destination.FixDestinationService;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.tag.FixTagModifierFilter;
import com.clifton.fix.tag.FixTagModifierService;
import com.clifton.fix.tag.cache.FixTagModifierCache;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * A set of in-memory DB tests to test the functionality of the FixTagModifierCache and FixTagModifierFilter classes.
 *
 * @author davidi
 */

@ContextConfiguration
@Transactional
public class FixTagModifierCacheImplTests extends BaseInMemoryDatabaseTests {

	@Resource
	private DaoCustomKeyListCache<Short, SystemBean> fixTagModifierCache;

	@Resource
	private DaoCustomKeyListCache<String, FixTagModifierFilter> fixTagModifierFilterCache;

	@Resource
	private FixTagModifierService fixTagModifierService;

	@Resource
	private FixSessionService fixSessionService;

	@Resource
	private FixDestinationService fixDestinationService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private AdvancedUpdatableDAO<SystemBeanProperty, Criteria> systemBeanPropertyDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@BeforeEach
	public void beforeTest() {
		((FixTagModifierCache) this.fixTagModifierCache).getCacheHandler().clearAll();
	}

	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCacheInstantiation() {
		Assertions.assertNotNull(this.fixTagModifierCache);
		Assertions.assertNotNull(this.fixTagModifierFilterCache);
		Assertions.assertNotNull(this.systemBeanPropertyDAO);
	}


	@Test
	public void testFixTagModifierCache_InitialGetRequest() {
		FixSessionDefinition fixSessionDefinition1 = this.fixSessionService.getFixSessionDefinition((short) 16);
		CacheStats preCacheStats = getCacheStats(this.fixTagModifierCache);
		List<Integer> expectedIdList1 = CollectionUtils.createList(83, 84);

		Assertions.assertEquals(0, preCacheStats.getSize());
		List<SystemBean> fixTagModifierList = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);

		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList);
		CacheStats postCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(1, postCacheStats.getSize(), "Unexpected cache size");
	}


	@Test
	public void testFixTagModifierCache_GetRequestWithCachedData() {
		FixSessionDefinition fixSessionDefinition1 = this.fixSessionService.getFixSessionDefinition((short) 8);
		FixSessionDefinition fixSessionDefinition2 = this.fixSessionService.getFixSessionDefinition((short) 16);
		CacheStats preCacheStats = getCacheStats(this.fixTagModifierCache);
		List<Integer> expectedIdList1 = CollectionUtils.createList(42, 43, 44, 45, 46, 47, 48);
		List<Integer> expectedIdList2 = CollectionUtils.createList(83, 84);

		Assertions.assertEquals(0, preCacheStats.getSize());
		List<SystemBean> fixTagModifierList1 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);
		List<SystemBean> fixTagModifierList2 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition2);

		// non-cached data
		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList1);
		assertResultListContainsExpectedIds(expectedIdList2, fixTagModifierList2);
		CacheStats postCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// access cached data
		fixTagModifierList1 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);
		fixTagModifierList2 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition2);
		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList1);
		assertResultListContainsExpectedIds(expectedIdList2, fixTagModifierList2);
	}


	@Test
	public void testFixTagModifierCache_ClearCacheAfterDataChanged_FixTagModifier() {
		FixSessionDefinition fixSessionDefinition1 = this.fixSessionService.getFixSessionDefinition((short) 8);
		FixSessionDefinition fixSessionDefinition2 = this.fixSessionService.getFixSessionDefinition((short) 16);
		CacheStats preCacheStats = getCacheStats(this.fixTagModifierCache);
		List<Integer> expectedIdList1 = CollectionUtils.createList(42, 43, 44, 45, 46, 47, 48);
		List<Integer> expectedIdList2 = CollectionUtils.createList(83, 84);

		Assertions.assertEquals(0, preCacheStats.getSize());
		List<SystemBean> fixTagModifierList1 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);
		List<SystemBean> fixTagModifierList2 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition2);

		// non-cached data
		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList1);
		assertResultListContainsExpectedIds(expectedIdList2, fixTagModifierList2);
		CacheStats postCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// update to the SystemBean table should clear the cache
		updateFixTagModifier(82, "neo");
		CacheStats postClearCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(0, postClearCacheStats.getSize(), "Unexpected cache size after clearing cache");
	}


	@Test
	public void testFixTagModifierCache_ClearCacheAfterDataChanged_FixTagModifierFilter() {
		FixSessionDefinition fixSessionDefinition1 = this.fixSessionService.getFixSessionDefinition((short) 8);
		FixSessionDefinition fixSessionDefinition2 = this.fixSessionService.getFixSessionDefinition((short) 16);
		CacheStats preCacheStats = getCacheStats(this.fixTagModifierCache);
		List<Integer> expectedIdList1 = CollectionUtils.createList(42, 43, 44, 45, 46, 47, 48);
		List<Integer> expectedIdList2 = CollectionUtils.createList(83, 84);

		Assertions.assertEquals(0, preCacheStats.getSize());
		List<SystemBean> fixTagModifierList1 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);
		List<SystemBean> fixTagModifierList2 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition2);

		// non-cached data
		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList1);
		assertResultListContainsExpectedIds(expectedIdList2, fixTagModifierList2);
		CacheStats postCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// update to the FixTagModifierFilter table should clear the cache
		updateFixTagModifierFilterTable(57, "delta1");
		CacheStats postClearCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(0, postClearCacheStats.getSize(), "Unexpected cache size after clearing cache");
	}


	@Test
	public void testFixTagModifierCache_ClearCacheAfterDataChanged_FixSessionDefinitionTagModifierTagModifierFilter() {
		FixSessionDefinition fixSessionDefinition1 = this.fixSessionService.getFixSessionDefinition((short) 8);
		FixSessionDefinition fixSessionDefinition2 = this.fixSessionService.getFixSessionDefinition((short) 16);
		CacheStats preCacheStats = getCacheStats(this.fixTagModifierCache);
		List<Integer> expectedIdList1 = CollectionUtils.createList(42, 43, 44, 45, 46, 47, 48);
		List<Integer> expectedIdList2 = CollectionUtils.createList(83, 84);

		Assertions.assertEquals(0, preCacheStats.getSize());
		List<SystemBean> fixTagModifierList1 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);
		List<SystemBean> fixTagModifierList2 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition2);

		// non-cached data
		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList1);
		assertResultListContainsExpectedIds(expectedIdList2, fixTagModifierList2);
		CacheStats postCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// update to the FixSessionDefinitionTagModifierTagModifierFilter table should clear the cache
		updateFixSessionDefinitionTagModifierTagModifierFilter(91, 71);
		CacheStats postClearCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(0, postClearCacheStats.getSize(), "Unexpected cache size after clearing cache");
	}


	@Test
	public void testFixTagModifierCache_ClearCacheAfterDataChanged_FixTagModifierProperty() {
		FixSessionDefinition fixSessionDefinition1 = this.fixSessionService.getFixSessionDefinition((short) 8);
		FixSessionDefinition fixSessionDefinition2 = this.fixSessionService.getFixSessionDefinition((short) 16);
		CacheStats preCacheStats = getCacheStats(this.fixTagModifierCache);
		List<Integer> expectedIdList1 = CollectionUtils.createList(42, 43, 44, 45, 46, 47, 48);
		List<Integer> expectedIdList2 = CollectionUtils.createList(83, 84);

		Assertions.assertEquals(0, preCacheStats.getSize());
		List<SystemBean> fixTagModifierList1 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition1);
		List<SystemBean> fixTagModifierList2 = this.fixTagModifierService.getFixTagModifierListBySessionDefinition(fixSessionDefinition2);

		// non-cached data
		assertResultListContainsExpectedIds(expectedIdList1, fixTagModifierList1);
		assertResultListContainsExpectedIds(expectedIdList2, fixTagModifierList2);
		CacheStats postCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// update to a Tag Modifier's property should clear the cache
		SystemBean tagModifier = fixTagModifierList2.get(1);
		//ClassUtils: for statement below, Error getting Class for class name [com.clifton.fix.quickfix.tag.bean.FieldAddFixTagModifier]. ClassUtils: 236
		List<SystemBeanProperty> systemBeanPropertyList = this.systemBeanService.getSystemBeanPropertyListByBean(tagModifier);
		SystemBeanProperty property1 = systemBeanPropertyList.get(0);
		SystemBeanProperty property2 = systemBeanPropertyList.get(1);
		property1.setValue("101");
		property1.setText("101");
		property2.setValue("XRAY");
		property2.setText("XRAY");
		tagModifier.setPropertyList(systemBeanPropertyList);
		this.systemBeanService.saveSystemBean(tagModifier);
		CacheStats postClearCacheStats = getCacheStats(this.fixTagModifierCache);
		Assertions.assertEquals(0, postClearCacheStats.getSize(), "Unexpected cache size after clearing cache");
	}


	@Test
	public void testFixTagModifierFilterCache_InitialGetRequest() {
		FixSession fixSession2 = this.fixSessionService.getFixSession((short) 11);
		SystemBean modifier83 = this.systemBeanService.getSystemBean((short) 83);
		SystemBean modifier84 = this.systemBeanService.getSystemBean((short) 84);

		List<Integer> expectedFilterIdListSession2TagMod83 = CollectionUtils.createList(56);
		List<Integer> expectedFilterIdListSession2TagMod84 = CollectionUtils.createList(56);

		CacheStats preCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(0, preCacheStats.getSize());

		// non-cached data -- should be cached after accessing
		List<FixTagModifierFilter> fixTagModifierFilterList81 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier83);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod83, fixTagModifierFilterList81);
		List<FixTagModifierFilter> fixTagModifierFilterList82 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier84);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod84, fixTagModifierFilterList82);

		CacheStats postCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");
	}


	@Test
	public void testFixTagModifierFilterCache_GetRequestWithCachedData() {

		FixSession fixSession1 = this.fixSessionService.getFixSession((short) 5);
		FixSession fixSession2 = this.fixSessionService.getFixSession((short) 11);
		SystemBean modifier42 = this.systemBeanService.getSystemBean(42);
		SystemBean modifier43 = this.systemBeanService.getSystemBean(43);
		SystemBean modifier44 = this.systemBeanService.getSystemBean(44);
		SystemBean modifier45 = this.systemBeanService.getSystemBean(45);
		SystemBean modifier46 = this.systemBeanService.getSystemBean(46);
		SystemBean modifier47 = this.systemBeanService.getSystemBean(47);
		SystemBean modifier48 = this.systemBeanService.getSystemBean(48);
		SystemBean modifier83 = this.systemBeanService.getSystemBean(83);
		SystemBean modifier84 = this.systemBeanService.getSystemBean(84);

		List<Integer> expectedFilterIdListSession1TagMod42 = CollectionUtils.createList(58);
		List<Integer> expectedFilterIdListSession1TagMod43 = CollectionUtils.createList(56);
		List<Integer> expectedFilterIdListSession1TagMod44 = CollectionUtils.createList(57, 69);
		List<Integer> expectedFilterIdListSession1TagMod45 = CollectionUtils.createList(57, 79);
		List<Integer> expectedFilterIdListSession1TagMod46 = CollectionUtils.createList(56, 70);
		List<Integer> expectedFilterIdListSession1TagMod47 = CollectionUtils.createList(56, 75);
		List<Integer> expectedFilterIdListSession1TagMod48 = CollectionUtils.createList(56, 71);
		List<Integer> expectedFilterIdListSession2TagMod83 = CollectionUtils.createList(56);
		List<Integer> expectedFilterIdListSession2TagMod84 = CollectionUtils.createList(56);

		CacheStats preCacheStats = getCacheStats(this.fixTagModifierFilterCache);

		// non-cached data
		Assertions.assertEquals(0, preCacheStats.getSize());
		List<FixTagModifierFilter> fixTagModifierFilterList1 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier42);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod42, fixTagModifierFilterList1);
		List<FixTagModifierFilter> fixTagModifierFilterList2 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier43);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod43, fixTagModifierFilterList2);
		List<FixTagModifierFilter> fixTagModifierFilterList3 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier44);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod44, fixTagModifierFilterList3);
		List<FixTagModifierFilter> fixTagModifierFilterList4 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier45);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod45, fixTagModifierFilterList4);
		List<FixTagModifierFilter> fixTagModifierFilterList5 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier46);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod46, fixTagModifierFilterList5);
		List<FixTagModifierFilter> fixTagModifierFilterList6 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier47);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod47, fixTagModifierFilterList6);
		List<FixTagModifierFilter> fixTagModifierFilterList7 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier48);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod48, fixTagModifierFilterList7);
		List<FixTagModifierFilter> fixTagModifierFilterList8 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier83);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod83, fixTagModifierFilterList8);
		List<FixTagModifierFilter> fixTagModifierFilterList9 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier84);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod84, fixTagModifierFilterList9);

		CacheStats postCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(9, postCacheStats.getSize(), "Unexpected cache size");

		// access cached data
		fixTagModifierFilterList1 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier42);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod42, fixTagModifierFilterList1);
		fixTagModifierFilterList2 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier43);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod43, fixTagModifierFilterList2);
		fixTagModifierFilterList3 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier44);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod44, fixTagModifierFilterList3);
		fixTagModifierFilterList4 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier45);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod45, fixTagModifierFilterList4);
		fixTagModifierFilterList5 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier46);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod46, fixTagModifierFilterList5);
		fixTagModifierFilterList6 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier47);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod47, fixTagModifierFilterList6);
		fixTagModifierFilterList7 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession1, modifier48);
		assertResultListContainsExpectedIds(expectedFilterIdListSession1TagMod48, fixTagModifierFilterList7);
		fixTagModifierFilterList8 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier83);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod83, fixTagModifierFilterList8);
		fixTagModifierFilterList9 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier84);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod84, fixTagModifierFilterList9);
	}


	@Test
	public void testFixTagModifierFilterCache_ClearCacheAfterDataChanged_TagModifierFilter() {
		FixSession fixSession2 = this.fixSessionService.getFixSession((short) 11);
		SystemBean modifier83 = this.systemBeanService.getSystemBean(83);
		SystemBean modifier84 = this.systemBeanService.getSystemBean(84);

		List<Integer> expectedFilterIdListSession2TagMod83 = CollectionUtils.createList(56);
		List<Integer> expectedFilterIdListSession2TagMod84 = CollectionUtils.createList(56);

		CacheStats preCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(0, preCacheStats.getSize());

		// non-cached data -- should be cached after accessing
		List<FixTagModifierFilter> fixTagModifierFilterList81 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier83);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod83, fixTagModifierFilterList81);
		List<FixTagModifierFilter> fixTagModifierFilterList82 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier84);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod84, fixTagModifierFilterList82);

		CacheStats postCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// update to the filter table should clear the cache
		updateFixTagModifierFilterTable(56, "delta2");
		CacheStats postClearCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(0, postClearCacheStats.getSize(), "Unexpected cache size after clearing cache");
	}


	@Test
	public void testFixTagModifierFilterCache_ClearCacheAfterDataChanged_FixSessionDefinitionTagModifierTagModifierFilter() {
		FixSession fixSession2 = this.fixSessionService.getFixSession((short) 11);
		SystemBean modifier83 = this.systemBeanService.getSystemBean(83);
		SystemBean modifier84 = this.systemBeanService.getSystemBean(84);

		List<Integer> expectedFilterIdListSession2TagMod83 = CollectionUtils.createList(56);
		List<Integer> expectedFilterIdListSession2TagMod84 = CollectionUtils.createList(56);

		CacheStats preCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(0, preCacheStats.getSize());

		// non-cached data -- should be cached after accessing
		List<FixTagModifierFilter> fixTagModifierFilterList81 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier83);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod83, fixTagModifierFilterList81);
		List<FixTagModifierFilter> fixTagModifierFilterList82 = this.fixTagModifierService.getFixTagModifierFilterListBySessionAndModifier(fixSession2, modifier84);
		assertResultListContainsExpectedIds(expectedFilterIdListSession2TagMod84, fixTagModifierFilterList82);

		CacheStats postCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(2, postCacheStats.getSize(), "Unexpected cache size");

		// update to the filter table should clear the cache
		updateFixSessionDefinitionTagModifierTagModifierFilter(91, 57);
		CacheStats postClearCacheStats = getCacheStats(this.fixTagModifierFilterCache);
		Assertions.assertEquals(0, postClearCacheStats.getSize(), "Unexpected cache size after clearing cache");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertResultListContainsExpectedIds(List<Integer> expectedIds, List<?> entriesToVerifyList) {
		List<Integer> idsToVerifyList = new ArrayList<>();
		for (Object identityObject : entriesToVerifyList) {
			idsToVerifyList.add((Integer) ((IdentityObject) identityObject).getIdentity());
		}

		Set<Integer> expectedIdSet = new HashSet<>(expectedIds);
		Set<Integer> verifyIdSet = new HashSet<>(idsToVerifyList);
		Assertions.assertEquals(expectedIdSet, verifyIdSet);
	}


	private void updateFixTagModifierFilterTable(Integer id, String name) {
		FixTagModifierFilter filter = this.fixTagModifierService.getFixTagModifierFilter(id);
		filter.setName(name);
		this.fixTagModifierService.saveFixTagModifierFilter(filter);
	}


	private void updateFixTagModifier(Integer id, String name) {
		SystemBean tagModifier = this.systemBeanService.getSystemBean(id);
		tagModifier.setName(name);
		this.systemBeanService.saveSystemBean(tagModifier);
	}


	private void updateFixSessionDefinitionTagModifierTagModifierFilter(Integer id, Integer filterId) {
		FixDestinationTagModifierFilter entry = this.fixDestinationService.getFixDestinationTagModifierFilter(id);
		FixTagModifierFilter filter = this.fixTagModifierService.getFixTagModifierFilter(filterId);
		entry.setReferenceTwo(filter);
		this.fixDestinationService.saveFixDestinationTagModifierFilter(entry);
	}


	private CacheStats getCacheStats(Object cache) {
		// get shared cacheHandler
		CacheHandler<?, Serializable[]> cacheHandler = ((FixTagModifierCache) this.fixTagModifierCache).getCacheHandler();

		// clear specific cache
		return cacheHandler.getCacheStats(cache.getClass().getName());
	}
}
