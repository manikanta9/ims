package com.clifton.fix.quickfix.server.response;

import com.clifton.fix.quickfix.server.provider.QuickFixFileNameProvider;
import com.clifton.fix.quickfix.util.QuickFixUtilServiceImpl;
import com.clifton.fix.server.provider.FixFileNameProvider;
import com.clifton.fix.server.response.FixServerResponseCollection;
import com.clifton.fix.util.FixUtilService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.ClassUtils;
import quickfix.Message;
import quickfix.field.OrderQty;
import quickfix.field.SecurityID;
import quickfix.field.SenderCompID;
import quickfix.field.SenderSubID;

import java.util.List;


/**
 * <code>FixServerResponseCollectionTest</code> test loading test messages from a data file based on an existing message.
 * TradeOrderFixTests will break if this logic changes.
 */
public class FixServerResponseCollectionTest {

	public static final String dataPath = ClassUtils.classPackageAsResourcePath(FixServerResponseCollectionTest.class) + "/data";


	@Test
	public void testGetTestCaseMessages() {
		Assertions.assertEquals(4, QuickFixFileNameProvider.KEYS.size());

		FixServerResponseCollection<Message> collection = new FixServerResponseCollection<>();
		collection.setDirectory(dataPath);
		FixFileNameProvider<Message> fixFileNameProvider = new QuickFixFileNameProvider();
		collection.setFixFileNameProvider(fixFileNameProvider);
		FixUtilService<Message> fixUtilService = new QuickFixUtilServiceImpl();
		collection.setFixUtilService(fixUtilService);

		Message message = new Message();
		message.setString(SenderCompID.FIELD, "CLIFTON");
		message.setString(SenderSubID.FIELD, "test_user_1");
		message.setString(SecurityID.FIELD, "ESM6 Index");
		message.setString(OrderQty.FIELD, "36.00000000");
		List<Message> messages = collection.getTestCaseMessages(message);
		Assertions.assertNotNull(messages);
		Assertions.assertEquals(18, messages.size());
	}
}
