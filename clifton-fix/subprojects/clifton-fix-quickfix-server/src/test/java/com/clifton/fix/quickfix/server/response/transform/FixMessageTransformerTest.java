package com.clifton.fix.quickfix.server.response.transform;

import com.clifton.fix.quickfix.server.provider.QuickFixMessageSequenceProvider;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.server.response.transform.FixFieldValueMapper;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import quickfix.DataDictionary;
import quickfix.DefaultDataDictionaryProvider;
import quickfix.InvalidMessage;
import quickfix.MemoryStore;
import quickfix.Message;
import quickfix.field.BeginString;

import java.io.IOException;


/**
 * <code>FixMessageTransformerTest</code> Takes a 'template' message and applies values from the source message.
 * The template is first deeply cloned.  The value providers are supplied by the source and extracted by the mapper.
 * See QuickFixMessageTransformer and TradeOrderFixTests.
 */
public class FixMessageTransformerTest {

	private static final DefaultDataDictionaryProvider DATA_DICTIONARY_PROVIDER = new DefaultDataDictionaryProvider();

	private static final String firstMsgString = "8=FIX.4.4\u00019=203\u000135=D\u000134=2\u000149=CLIFTON\u000150=r147786\u000156=GSFUT\u000157=TKTZ\u000111=O_185691_20160602\u000121=1\u000122=A\u000138=36.00000000\u000140=1\u000148=ESM6 Index\u000154=1\u000155=ESM6 Index\u000159=0\u000160=20160602-18:47:39.766\u000164=20160602\u000175=20160602\u0001167=FUT\u00018135=GS\u000110=009\u0001";
	private static final String secondMsgString = "8=FIX.4.4\u00019=203\u000135=D\u000134=2\u000149=CLIFTON\u000150=r147785\u000156=GSFUT\u000157=TKTS\u000111=O_185691_20160102\u000121=1\u000122=A\u000138=36.00000000\u000140=1\u000148=ESM6 Index\u000154=1\u000155=ESM6 Index\u000159=0\u000160=20160102-17:47:39.766\u000164=20160602\u000175=20160602\u0001167=FUT\u00018135=GS\u000110=009\u0001";

	private static Message firstMessage;
	private static Message secondMessage;


	@BeforeAll
	public static void setup() throws InvalidMessage {
		firstMessage = new Message();
		secondMessage = new Message();

		DataDictionary dd = DATA_DICTIONARY_PROVIDER.getSessionDataDictionary(QuickFixMessageUtils.getStringField(firstMsgString, BeginString.FIELD));
		firstMessage.fromString(firstMsgString, dd, false);
		dd = DATA_DICTIONARY_PROVIDER.getSessionDataDictionary(QuickFixMessageUtils.getStringField(secondMsgString, BeginString.FIELD));
		secondMessage.fromString(secondMsgString, dd, false);

		QuickFixMessageUtils.parse(firstMessage.toString());
		QuickFixMessageUtils.parse(secondMessage.toString());
	}


	@Test
	public void testTransform() throws IOException {
		MatcherAssert.assertThat(firstMessage.toString(), IsNot.not(IsEqual.equalTo(secondMessage.toString())));

		QuickFixMessageTransformer transformer = new QuickFixMessageTransformer();
		QuickFixMessageSequenceProvider sequence = new QuickFixMessageSequenceProvider(new MemoryStore());
		transformer.setFixMessageSequenceProvider(sequence);
		transformer.setFixFieldValueMapper(new FixFieldValueMapper());

		Message transformed = transformer.transform(firstMessage, secondMessage);
		MatcherAssert.assertThat(transformed.toString(), IsEqual.equalTo(secondMessage.toString()));
		MatcherAssert.assertThat(transformed.toString(), IsNot.not(IsEqual.equalTo(firstMessage.toString())));
	}
}
