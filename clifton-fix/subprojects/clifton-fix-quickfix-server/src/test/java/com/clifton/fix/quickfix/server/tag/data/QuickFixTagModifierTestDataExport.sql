SELECT 'FixSession' AS entityTableName, FixSessionID AS entityId FROM FixSession
UNION
SELECT 'FixDestinationTagModifierFilter' AS entityTableName, FixDestinationTagModifierFilterID AS entityId
FROM FixDestinationTagModifierFilter
UNION
SELECT 'FixDestinationTagModifier' AS entityTableName, FixDestinationTagModifierID AS entityId
FROM FixDestinationTagModifier
UNION
SELECT 'FixDestination' AS entityTableName, FixDestinationID AS entityId
FROM FixDestination
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID AS entityId
FROM SystemBeanProperty
WHERE SystemBeanPropertyTypeID IN
	  (SELECT SystemBeanPropertyTypeID
	   FROM SystemBeanPropertyType
	   WHERE SystemBeanTypeID IN
			 (SELECT SystemBeanTypeID FROM SystemBeanType WHERE SystemBeanGroupID = (SELECT SystemBeanGroupID FROM SystemBeanGroup WHERE SystemBeanGroupName = 'FIX Tag Modifier')));
