package com.clifton.fix.quickfix.server.tag;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.quickfix.converter.StringToMessageConverter;
import com.clifton.fix.quickfix.field.QuickFixFieldRediBroker;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.Account;
import quickfix.field.AvgPx;
import quickfix.field.DeliverToLocationID;
import quickfix.field.NoAllocs;
import quickfix.field.NoPartyIDs;
import quickfix.field.PartyID;
import quickfix.field.PartyIDSource;
import quickfix.field.PartyRole;
import quickfix.field.ProcessCode;
import quickfix.field.SenderSubID;
import quickfix.field.Symbol;
import quickfix.field.TargetSubID;
import quickfix.fix44.AllocationInstruction;
import quickfix.fix44.NewOrderSingle;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


@ContextConfiguration("QuickFixTagModifierTest-context.xml")
public class QuickFixTagModifierHandlerTest extends BaseInMemoryDatabaseTests {

	@Resource
	private QuickFixTagModifierHandler quickFixTagModifierHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRemoveTag81() {
		String messageText = "8=FIX.4.49=62135=J34=137849=CLIFTON52=20150924-13:20:48.01656=GSFUT57=GSDESK6=9859.0000000022=A48=LNZ5 Comdty53=1198.0000000054=155=LNZ5 Comdty60=20150924-13:19:48.66364=2015092470=A_148002_2015092471=075=20150924167=FUT200=20151273=111=O_151753_2015092437=FUSNYB660602015092478=879=157-9715380=519.0000000081=079=157-7060080=103.0000000081=079=157-7426080=202.0000000081=079=157-9715480=142.0000000081=079=157-9716180=124.0000000081=079=157-9716580=51.0000000081=079=157-9715980=34.0000000081=079=157-9716380=23.0000000081=0124=132=1198.0000000017=FUSNYB663812015092431=9859.0000000010=024";
		AllocationInstruction message = (AllocationInstruction) (StringToMessageConverter.getInstance()).convert(messageText);

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		List<Group> groups = message.getGroups(NoAllocs.FIELD);
		for (Group group : CollectionUtils.getIterable(groups)) {
			Assertions.assertFalse(group.isSetField(ProcessCode.FIELD));
		}
	}


	@Test
	public void testAccountNumberModifier() throws FieldNotFound {
		String messageText = "8=FIX.4.49=25235=D34=151149=CLIFTONIPT50=u72744052=20140818-14:45:24.42256=REDIIPT57=IPT1=157-6150511=O_102365_2014081821=322=A38=1.000000000040=148=RTAU4 Index54=155=RTAU4 Index59=060=20140818-14:45:24.43564=2014081875=20140818167=FUT8135=CITI10=064";
		NewOrderSingle message = (NewOrderSingle) (StringToMessageConverter.getInstance()).convert(messageText);

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertFalse(message.isSetField(SenderSubID.FIELD));
		Assertions.assertEquals("15761505-T", message.getField(new Account()).getValue());
	}


	@Test
	public void testOrderTagModifiers() throws FieldNotFound {
		String messageText = "8=FIX.4.49=23135=D34=228349=CLIFTON50=u71968652=20130620-20:04:54.52156=GSFUT57=TKTS11=O_60147_2013062015045421=122=A38=4.000000000040=148=RTAU3 Index54=255=RTAU3 Index59=060=20130620-20:04:54.26264=2013062175=20130620167=FUT10=138";
		NewOrderSingle message = (NewOrderSingle) (StringToMessageConverter.getInstance()).convert(messageText);

		Group party = new NewOrderSingle.NoPartyIDs();
		party.setField(new PartyID("CITI"));
		party.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		party.setField(new PartyRole(PartyRole.EXECUTING_FIRM));
		message.addGroup(party);

		List<Group> groups = message.getGroups(NoPartyIDs.FIELD);
		Assertions.assertEquals(1, groups.size());

		message.getHeader().setField(new SenderSubID("YES"));
		message.getHeader().setField(new TargetSubID("DUDE"));

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		groups = message.getGroups(NoPartyIDs.FIELD);
		Assertions.assertEquals(0, groups.size());

		Assertions.assertEquals("CITI", message.getField(new QuickFixFieldRediBroker()).getValue());
		Assertions.assertEquals("DUDE", message.getHeader().getField(new SenderSubID()).getValue());
		Assertions.assertEquals("YES", message.getHeader().getField(new TargetSubID()).getValue());
	}


	@Test
	public void testTagModifiers_01() throws FieldNotFound {
		String messageText = "8=FIX.4.49=38135=J34=139849=CLIFTON52=20130117-14:35:37.58556=GSFUT6=1472.000000000000000000000000022=A48=ESH3 Index53=15.000000000054=255=ESH3 Index60=20130117-14:35:55.84870=47086_2013011708355571=075=20130117167=CS73=111=46129_2013011708354737=A11331CMK0002AHF_RoI78=179=260-7109980=15.000000000081=0124=132=15.000000000017=MIRAA81142013011731=1472.00000000000000010=219";
		Message message = (StringToMessageConverter.getInstance()).convert(messageText);

		Group party = new NewOrderSingle.NoPartyIDs();

		party.setField(new PartyID("CITI"));
		party.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		party.setField(new PartyRole(PartyRole.ENTERING_FIRM));
		message.addGroup(party);

		message.getHeader().setField(new SenderSubID("DUDE"));
		message.getHeader().setField(new TargetSubID("DUDE"));

		Assertions.assertEquals("DUDE", message.getHeader().getField(new SenderSubID()).getValue());
		Assertions.assertEquals("DUDE", message.getHeader().getField(new TargetSubID()).getValue());

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertEquals("CLIFTON", message.getHeader().getField(new SenderSubID()).getValue());
		Assertions.assertEquals("SLKEXE", message.getHeader().getField(new TargetSubID()).getValue());
	}


	@Test
	public void testTagModifiers_02() throws FieldNotFound {
		String messageText = "8=FIX.4.49=38135=J34=139849=CLIFTON52=20130117-14:35:37.58556=GSFUT6=1472.000000000000000000000000022=A48=ESH3 Index53=15.000000000054=255=ESH3 Index60=20130117-14:35:55.84870=47086_2013011708355571=075=20130117167=CS73=111=46129_2013011708354737=A11331CMK0002AHF_RoI78=179=260-7109980=15.000000000081=0124=132=15.000000000017=MIRAA81142013011731=1472.00000000000000010=219";
		Message message = (StringToMessageConverter.getInstance()).convert(messageText);

		Group party = new NewOrderSingle.NoPartyIDs();

		party.setField(new PartyID("CITI"));
		party.setField(new PartyIDSource(PartyIDSource.PROPRIETARY_CUSTOM_CODE));
		party.setField(new PartyRole(PartyRole.ENTERING_FIRM));
		message.addGroup(party);

		Assertions.assertEquals(0, new BigDecimal("1472.0000000000000000000000000").compareTo(message.getField(new AvgPx()).getValue()));

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertEquals(0, new BigDecimal("154").compareTo(message.getField(new AvgPx()).getValue()));
	}


	@Test
	public void testTagModifiersFilterDoNotMatch() throws FieldNotFound {
		String messageText = "8=FIX.4.49=38135=G34=139849=CLIFTON52=20130117-14:35:37.58556=GSFUT6=1472.000000000000000000000000022=A48=ESH3 Index53=15.000000000054=255=ESH3 Index60=20130117-14:35:55.84870=47086_2013011708355571=075=20130117167=C73=111=46129_2013011708354737=A11331CMK0002AHF_RoI78=179=260-7109980=15.000000000081=0124=132=15.000000000017=MIRAA81142013011731=1472.00000000000000010=136";
		Message message = (StringToMessageConverter.getInstance()).convert(messageText);

		message.getHeader().setField(new SenderSubID("DUDE"));
		message.getHeader().setField(new TargetSubID("DUDE"));

		Assertions.assertEquals("DUDE", message.getHeader().getField(new SenderSubID()).getValue());
		Assertions.assertEquals("DUDE", message.getHeader().getField(new TargetSubID()).getValue());
		Assertions.assertEquals(0, new BigDecimal("1472.0000000000000000000000000").compareTo(message.getField(new AvgPx()).getValue()));

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertEquals("DUDE", message.getHeader().getField(new SenderSubID()).getValue());
		Assertions.assertEquals("DUDE", message.getHeader().getField(new TargetSubID()).getValue());
		Assertions.assertEquals(0, new BigDecimal("1472.0000000000000000000000000").compareTo(message.getField(new AvgPx()).getValue()));
	}


	@Test
	public void testBloombergTagModifiers() throws FieldNotFound {
		String messageText = "8=FIX.4.49=23135=D34=228349=CLIFTONPARA50=u71968652=20130620-20:04:54.52156=BLP11=O_60147_2013062015045421=122=A38=4.000000000040=148=RTAU3 Index54=255=RTAU3 Index59=060=20130620-20:04:54.26264=2013062175=20130620167=FUT10=019";
		Message message = (StringToMessageConverter.getInstance()).convert(messageText);

		message.getHeader().setField(new SenderSubID("YES"));
		message.getHeader().setField(new TargetSubID("DUDE"));

		Assertions.assertEquals("YES", message.getHeader().getField(new SenderSubID()).getValue());
		Assertions.assertEquals("DUDE", message.getHeader().getField(new TargetSubID()).getValue());

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertFalse(message.isSetField(SenderSubID.FIELD));
		Assertions.assertEquals("YES", message.getHeader().getField(new TargetSubID()).getValue());
		Assertions.assertEquals("[N\\A]", message.getField(new Symbol()).getValue());
	}


	@Test
	public void testChainedModifiers() throws FieldNotFound {

		String messageText = "8=FIX.4.49=43535=J34=36449=CLIFTONMB4450=u74315952=20170804-18:34:57.58256=REDITKTS115=CLIF6=0.1320000048=CSCO53=9.0000000054=255=CSCO60=20170804-18:34:57.76664=2017080770=A_237382_2017080471=075=20170804167=OPT200=20170818201=1202=33.0205=18461=OCAXCS857=173=111=O_246222_2017080437=LYQ0053778=279=045619T2980=4.0000000081=379=033-1AA9N780=5.0000000081=0124=132=9.0000000017=12083117812161731=0.13200000453=1448=MS447=D452=110=171";
		Message message = (StringToMessageConverter.getInstance()).convert(messageText);

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertEquals("MSOPT", message.getHeader().getField(new DeliverToLocationID()).getValue());
	}


	@Test
	public void testTagModifiersRemove() {
		String messageText = "8=FIX.4.49=63635=J34=110949=CLIFTONPARA52=20131219-20:24:08.06456=BLP6=102.58984375000000000000000000000022=148=912828MR853=21839000.00000000000000054=255=N\\A60=20131219-20:23:09.26564=2013122070=A_79364_2013121914230871=075=20131219118=22563637.59381=22404595.98423=1626=1857=173=111=O_79591_2013121914221237=103878=179=22-7320580=21839000.000000000081=0154=22563637.59124=132=21839000.00000000000000017=BART:20131219:2803:6:1231=102.589843750000000453=5448=JPERSUITTI8447=D452=11448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=TSOX447=D452=16448=DVANHORNE3:5748090447=D452=36448=2164447=D452=110=040";
		Message message = (StringToMessageConverter.getInstance()).convert(messageText);

		this.quickFixTagModifierHandler.applyTagModifiers(message, null, null);

		Assertions.assertFalse(message.isSetField(NoPartyIDs.FIELD));
		Assertions.assertFalse(message.toString().contains(
				"453=5448=JPERSUITTI8447=D452=11448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=TSOX447=D452=16448=DVANHORNE3:5748090447=D452=36448=2164447=D452=1"));
	}

}
