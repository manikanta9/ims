package com.clifton.fix.quickfix.server.log;

import com.clifton.core.folder.TestFolderWatcher;
import com.clifton.core.util.FunctionUtils;
import com.clifton.fix.quickfix.server.log.file.QuickFixRolloverFileWriter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.stream.Stream;
import java.util.zip.GZIPInputStream;


public class QuickFixRolloverFileWriterTest {

	private static final String LOG_ENTRY = "8=FIX.4.4\u00019=186\u000135=D\u000134=10\u000149=CLIFTON\u000150=u737933\u000152=20110331-19:58:39.199\u000156=GSFUT\u000157=TKTS\u000111=2_20110331145838\u000121=1\u000122=A\u000138=1\u000140=1\u000148=ESM1 Index\u000154=2\u000155=ESM1 Index\u000159=0\u000160=20110331-19:58:38.537\u0001167=FUT\u000110=168\u0001";
	private static final int messageBytes = LOG_ENTRY.getBytes(StandardCharsets.UTF_8).length;

	@TempDir
	public Path rollOverConcurrentlyPath;

	@TempDir
	public Path rollOverOnMaximumSizePath;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRollOverConcurrently() throws IOException {
		Path logFile = Files.createFile(this.rollOverOnMaximumSizePath.resolve("Messages.log"));

		int maxRolledFileSizeBytes = messageBytes * 2;
		int compressedMsgCnt = 14;
		int loggedMsgCnt = 1;
		int loggedEventCount = compressedMsgCnt + loggedMsgCnt;
		int expectedFileCount = (loggedEventCount * messageBytes / maxRolledFileSizeBytes);

		List<Callable<Void>> tasks = new ArrayList<>();
		List<Path> deletedFiles;
		try (QuickFixRolloverFileWriter quickFixRolloverFileOutputStream = new QuickFixRolloverFileWriter(logFile, StandardCharsets.UTF_8, 10000000, null, true, false)) {
			ReflectionTestUtils.setField(quickFixRolloverFileOutputStream, "maxRolledFileSizeBytes", maxRolledFileSizeBytes);
			for (int i = 0; i < loggedEventCount; i++) {
				tasks.add(() -> {
					quickFixRolloverFileOutputStream.write(LOG_ENTRY);
					return null;
				});
			}

			ExecutorService pool = Executors.newFixedThreadPool(3);
			deletedFiles = TestFolderWatcher.watchForFolderEvents(
					this.rollOverConcurrentlyPath,
					FunctionUtils.uncheckedRunnable(() -> pool.invokeAll(tasks)),
					expectedFileCount,
					Duration.of(5, ChronoUnit.SECONDS),
					path -> !path.getFileName().toString().endsWith(".log"),
					StandardWatchEventKinds.ENTRY_DELETE
			);
		}
		Assertions.assertEquals(expectedFileCount, deletedFiles.size());
		Supplier<Stream<Path>> fileStreamSupplier = FunctionUtils.uncheckedSupplier(() -> Files.list(this.rollOverConcurrentlyPath));

		Assertions.assertNotNull(fileStreamSupplier.get());
		Assertions.assertEquals(expectedFileCount + 1, fileStreamSupplier.get().count());
		Assertions.assertEquals(expectedFileCount, fileStreamSupplier.get().filter(p -> p.getFileName().toString().endsWith(".gz")).count());

		@SuppressWarnings("OptionalGetWithoutIsPresent")
		Path messageLogPath = fileStreamSupplier.get().filter(p -> p.getFileName().toString().endsWith(".log")).findFirst().get();
		String contents = new String(Files.readAllBytes(messageLogPath));
		int occurrences = countOccurrences(contents);
		Assertions.assertEquals(loggedMsgCnt, occurrences);

		occurrences = fileStreamSupplier.get()
				.filter(path -> path.getFileName().toString().endsWith(".gz"))
				.map(FunctionUtils.uncheckedFunction(path -> {
					Path unzipped = Paths.get(path.getParent().toAbsolutePath().toString(), path.getFileName().toString() + ".unzipped");
					try (GZIPInputStream gzipInputStream = new GZIPInputStream(new FileInputStream(path.toFile()))) {
						Files.copy(gzipInputStream, unzipped);
					}
					return Files.readAllBytes(unzipped);
				}))
				.map(String::new)
				.mapToInt(this::countOccurrences)
				.sum();
		Assertions.assertEquals(compressedMsgCnt, occurrences);
	}


	@Test
	public void testRollOverOnMaximumSize() throws IOException {
		Path logFile = Files.createFile(this.rollOverOnMaximumSizePath.resolve("Messages.log"));

		int threeMegaBytes = 3 << 20;
		int numEntriesPerFile = threeMegaBytes / messageBytes;
		int numFilesToWrite = 5;
		// Rollover a single time and then write a select number of entries in the last file
		int finalFileEntryCount = 4;
		int numEntriesToWrite = numEntriesPerFile * (numFilesToWrite - 1) + finalFileEntryCount;

		try (QuickFixRolloverFileWriter quickFixRolloverFileOutputStream = new QuickFixRolloverFileWriter(logFile, StandardCharsets.UTF_8, threeMegaBytes, null, true, false)) {
			TestFolderWatcher.watchForFolderEvents(
					this.rollOverOnMaximumSizePath,
					FunctionUtils.uncheckedRunnable(() -> {
						for (int i = 0; i < numEntriesToWrite; i++) {
							quickFixRolloverFileOutputStream.write(LOG_ENTRY);
						}
					}),
					numFilesToWrite - 1,
					// should NEVER take this long, 2 seconds is the typical duration.
					Duration.of(10, ChronoUnit.SECONDS),
					path -> !path.getFileName().toString().endsWith(".log"),
					StandardWatchEventKinds.ENTRY_DELETE
			);
		}

		Supplier<Stream<Path>> fileStreamSupplier = FunctionUtils.uncheckedSupplier(() -> Files.list(this.rollOverOnMaximumSizePath));

		Assertions.assertNotNull(fileStreamSupplier.get());
		Assertions.assertEquals(numFilesToWrite, fileStreamSupplier.get().count(), "Found an unexpected number of files.");
		Assertions.assertEquals(numFilesToWrite - 1, fileStreamSupplier.get().filter(p -> p.getFileName().toString().endsWith(".gz")).count(), "Found an unexpected number of compressed files. All files should be compressed except for the latest.");

		@SuppressWarnings("OptionalGetWithoutIsPresent")
		Path messageLogPath = fileStreamSupplier.get().filter(p -> p.getFileName().toString().endsWith(".log")).findFirst().get();
		String contents = new String(Files.readAllBytes(messageLogPath));
		int entryCount = countOccurrences(contents);
		Assertions.assertEquals(finalFileEntryCount, entryCount);

		@SuppressWarnings("OptionalGetWithoutIsPresent")
		Path gzip = fileStreamSupplier.get().filter(p -> p.getFileName().toString().endsWith(".gz")).findFirst().get();
		Path unzipped = Paths.get(gzip.getParent().toAbsolutePath().toString(), "unzipped");
		try (GZIPInputStream gzipInputStream = new GZIPInputStream(new FileInputStream(gzip.toFile()))) {
			Files.copy(gzipInputStream, unzipped);
		}
		entryCount = 0;
		byte[] buffer = new byte[messageBytes];
		try (FileInputStream inputStream = new FileInputStream(unzipped.toFile())) {
			while (inputStream.read(buffer) > 0) {
				entryCount++;
			}
		}
		Assertions.assertEquals(numEntriesPerFile, entryCount, "Found an unexpected number of entries within the zipped file.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int countOccurrences(String contents) {
		int occurrences = 0;
		int index = contents.indexOf("8=FIX.4.4");
		while (index >= 0) {
			index = contents.indexOf("8=FIX.4.4", index + 1);
			occurrences++;
		}
		return occurrences;
	}
}
