package com.clifton.fix.quickfix.server.session;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import javax.annotation.Resource;
import java.util.HashMap;


/**
 * Tests for daily vs weekly configuration settings to ensure the proper data is set in the QuickFix SessionSettings used
 * to configure the QuickFix Session.
 *
 * @author davidi
 */
@FixInMemoryDatabaseContext
public class FixSessionDefinitionDurationSettingsTests extends BaseInMemoryDatabaseTests {

	@Resource
	private FixSessionService fixSessionService;

	@Resource
	private QuickFixSessionSettingsStandardConfigurer quickFixSessionSettingsStandardConfigurer;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFixSessionDefinitionDailySession() {
		FixSessionDefinition sessionDefinition = this.fixSessionService.getFixSessionDefinitionByLabel("CITI CARE");
		sessionDefinition.setWeeklySession(false);

		HashMap<String, String> sessionSettings = new HashMap<>();
		this.quickFixSessionSettingsStandardConfigurer.configure(sessionDefinition, sessionSettings);

		Assertions.assertEquals(6, sessionSettings.size());
		Assertions.assertEquals(sessionDefinition.getStartTime(), sessionSettings.get("StartTime"));
		Assertions.assertEquals(sessionDefinition.getEndTime(), sessionSettings.get("EndTime"));
		Assertions.assertEquals(sessionDefinition.getBeginString(), sessionSettings.get("BeginString"));
		Assertions.assertEquals(sessionDefinition.getSenderCompId(), sessionSettings.get("SenderCompID"));
		Assertions.assertEquals(sessionDefinition.getHeartbeatInterval().toString(), sessionSettings.get("HeartBtInt"));
		Assertions.assertEquals(sessionDefinition.getTargetCompId(), sessionSettings.get("TargetCompID"));
	}


	@Test
	public void testFixSessionDefinitionWeeklySession() {
		FixSessionDefinition sessionDefinition = this.fixSessionService.getFixSessionDefinitionByLabel("Newport EMS");
		sessionDefinition.setWeeklySession(true);

		HashMap<String, String> sessionSettings = new HashMap<>();
		this.quickFixSessionSettingsStandardConfigurer.configure(sessionDefinition, sessionSettings);

		Assertions.assertEquals(9, sessionSettings.size());
		Assertions.assertEquals(sessionDefinition.getStartTime(), sessionSettings.get("StartTime"));
		Assertions.assertEquals(sessionDefinition.getEndTime(), sessionSettings.get("EndTime"));
		Assertions.assertEquals(sessionDefinition.getBeginString(), sessionSettings.get("BeginString"));
		Assertions.assertEquals(sessionDefinition.getSenderCompId(), sessionSettings.get("SenderCompID"));
		Assertions.assertEquals(sessionDefinition.getHeartbeatInterval().toString(), sessionSettings.get("HeartBtInt"));
		Assertions.assertEquals(sessionDefinition.getTargetCompId(), sessionSettings.get("TargetCompID"));
		Assertions.assertEquals(sessionDefinition.getDescription(), sessionSettings.get("Description"));

		// Only present for weekly sessions
		Assertions.assertEquals("Sunday", sessionSettings.get("StartDay"));
		Assertions.assertEquals("Friday", sessionSettings.get("EndDay"));
	}
}
