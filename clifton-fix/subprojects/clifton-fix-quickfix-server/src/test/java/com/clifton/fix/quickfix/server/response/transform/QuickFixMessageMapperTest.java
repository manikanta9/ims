package com.clifton.fix.quickfix.server.response.transform;

import com.clifton.fix.quickfix.server.provider.QuickFixMessageSequenceProvider;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.response.transform.FixFieldValueMapper;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import quickfix.DataDictionary;
import quickfix.DefaultDataDictionaryProvider;
import quickfix.InvalidMessage;
import quickfix.MemoryStore;
import quickfix.Message;
import quickfix.field.BeginString;

import java.io.IOException;
import java.util.Map;


/**
 * <code>QuickFixMessageMapperTest</code> Will replace template response field values, such as CIOrdID with
 * the correct value from the JMS retrieved message.
 * See QuickFixMessageTransformer and TradeOrderFixTests.
 */
public class QuickFixMessageMapperTest {

	private static final DefaultDataDictionaryProvider DATA_DICTIONARY_PROVIDER = new DefaultDataDictionaryProvider();

	private static final String firstMsgString = "8=FIX.4.4\u00019=231\u000135=D\u000134=2\u000149=CLIFTON\u000150=r147785\u000152=20160602-17:47:39.863\u000156=GSFUT\u000157=TKTS\u000111=O_185691_20160602\u000121=1\u000122=A\u000138=36.00000000\u000140=1\u000148=ESM6 Index\u000154=1\u000155=ESM6 Index\u000159=0\u000160=20160602-17:47:39.766\u000164=20160602\u000175=20160602\u0001167=FUT\u00018135=GS\u000110=149\u0001";

	private static Message message;


	@BeforeAll
	public static void setup() throws InvalidMessage {
		message = new Message();

		DataDictionary dd = DATA_DICTIONARY_PROVIDER.getSessionDataDictionary(QuickFixMessageUtils.getStringField(firstMsgString, BeginString.FIELD));
		message.fromString(firstMsgString, dd, false);
		QuickFixMessageUtils.parse(message.toString());
	}


	@Test
	public void testGetFieldMappings() throws IOException {
		FixMessageSequenceProvider sequenceProvider = new QuickFixMessageSequenceProvider(new MemoryStore());
		QuickFixMessageMapper mapper = new QuickFixMessageMapper(message, sequenceProvider, new FixFieldValueMapper());
		String testMessageString = message.toString();

		Map<Integer, QuickFixMessageMapper.Replacement> mappings = mapper.getStaticFieldMappings();
		Assertions.assertEquals(QuickFixMessageMapper.MUTABLE_FIELDS.size(), mappings.size());
		for (Map.Entry<Integer, QuickFixMessageMapper.Replacement> integerReplacementEntry : mappings.entrySet()) {
			String oldValue = QuickFixMessageUtils.getStringField(testMessageString, integerReplacementEntry.getKey());
			MatcherAssert.assertThat("field:  " + (int) integerReplacementEntry.getKey(), oldValue, IsEqual.equalTo(integerReplacementEntry.getValue().replace(oldValue)));
		}
	}
}
