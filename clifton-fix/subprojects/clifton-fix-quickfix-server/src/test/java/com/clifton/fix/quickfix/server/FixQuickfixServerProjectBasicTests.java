package com.clifton.fix.quickfix.server;

import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.quickfix.server.log.composite.QuickFixCompositeLogFactory;
import com.clifton.fix.quickfix.server.store.QuickFixStore;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class FixQuickfixServerProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "fix-quickfix-server";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.beans.factory.BeanCreationException");
		imports.add("quickfix.");
		imports.add("org.quickfixj.jmx.JmxExporter");
		imports.add("javax.management.ObjectName");
		imports.add("org.springframework.core.annotation.Order");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("quickfix.");
		imports.add("org.springframework.util.ClassUtils");
		imports.add("com.google.common.collect.");
	}


	@Override
	protected List<Class<?>> getSkipGetSetTestClasses() {
		return CollectionUtils.createList(
				QuickFixCompositeLogFactory.class,
				QuickFixStore.class
		);
	}
}
