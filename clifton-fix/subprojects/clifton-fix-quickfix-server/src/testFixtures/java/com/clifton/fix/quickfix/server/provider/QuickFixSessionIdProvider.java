package com.clifton.fix.quickfix.server.provider;

import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.server.provider.FixSessionIdProvider;
import quickfix.Message;
import quickfix.Session;
import quickfix.SessionID;


/**
 * {@inheritDoc}
 */
public class QuickFixSessionIdProvider implements FixSessionIdProvider<SessionID, Message> {

	@Override
	public SessionID getFixSessionId(Message message, String sessionQualifier) {
		SessionID sessionId = QuickFixMessageUtils.getSessionIDWithOptionalFields(message, sessionQualifier);
		Session session = Session.lookupSession(sessionId);
		// If no session is found, then use only the required fields
		if (session == null) {
			return QuickFixMessageUtils.getSessionID(message, sessionQualifier);
		}
		return sessionId;
	}
}
