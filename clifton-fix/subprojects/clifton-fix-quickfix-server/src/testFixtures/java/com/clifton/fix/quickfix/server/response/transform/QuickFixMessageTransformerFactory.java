package com.clifton.fix.quickfix.server.response.transform;

import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.response.transform.FixFieldValueMapper;
import com.clifton.fix.server.response.transform.FixMessageTransformer;
import com.clifton.fix.server.response.transform.FixMessageTransformerFactory;
import quickfix.Message;


/**
 * {@inheritDoc}
 */
public class QuickFixMessageTransformerFactory implements FixMessageTransformerFactory<Message> {

	@Override
	public FixMessageTransformer<Message> getFixMessageTransform(FixFieldValueMapper fixFieldValueMapper, FixMessageSequenceProvider fixMessageSequenceProvider) {
		QuickFixMessageTransformer quickFixMessageTransformer = new QuickFixMessageTransformer();
		quickFixMessageTransformer.setFixMessageSequenceProvider(fixMessageSequenceProvider);
		quickFixMessageTransformer.setFixFieldValueMapper(fixFieldValueMapper);
		return quickFixMessageTransformer;
	}
}
