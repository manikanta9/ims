package com.clifton.fix.quickfix.server.provider;

import com.clifton.fix.quickfix.server.store.QuickFixStoreFactory;
import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.provider.FixMessageSequenceProviderFactory;
import quickfix.MessageStore;
import quickfix.SessionID;


public class QuickFixMessageSequenceProviderFactory implements FixMessageSequenceProviderFactory<SessionID> {

	private QuickFixStoreFactory quickFixStoreFactory;


	@Override
	public FixMessageSequenceProvider getFixMessageSequenceProvider(SessionID sessionID) {
		MessageStore quickFixStore = getQuickFixStoreFactory().create(sessionID);
		return new QuickFixMessageSequenceProvider(quickFixStore);
	}


	public QuickFixStoreFactory getQuickFixStoreFactory() {
		return this.quickFixStoreFactory;
	}


	public void setQuickFixStoreFactory(QuickFixStoreFactory quickFixStoreFactory) {
		this.quickFixStoreFactory = quickFixStoreFactory;
	}
}
