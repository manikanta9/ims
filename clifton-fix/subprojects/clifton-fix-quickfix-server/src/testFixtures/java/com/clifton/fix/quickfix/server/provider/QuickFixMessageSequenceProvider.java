package com.clifton.fix.quickfix.server.provider;

import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import quickfix.MessageStore;

import java.io.IOException;


/**
 * {@inheritDoc}
 */
public class QuickFixMessageSequenceProvider implements FixMessageSequenceProvider {

	private MessageStore quickFixStore;


	public QuickFixMessageSequenceProvider(MessageStore quickFixStore) {
		this.quickFixStore = quickFixStore;
	}


	@Override
	public String getNextSequenceNumber() {
		try {
			this.quickFixStore.incrNextSenderMsgSeqNum();
			return Integer.toString(this.quickFixStore.getNextSenderMsgSeqNum());
		}
		catch (IOException e) {
			throw new RuntimeException("Could not get next sequence.", e);
		}
	}
}
