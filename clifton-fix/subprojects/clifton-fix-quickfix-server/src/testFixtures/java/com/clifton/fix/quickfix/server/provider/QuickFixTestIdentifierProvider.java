package com.clifton.fix.quickfix.server.provider;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.identifier.FixIdentifierHandler;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.server.provider.FixTestIdentifierProvider;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.util.FixMessageHandler;
import com.clifton.fix.util.FixUtilService;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.Message;
import quickfix.field.AllocID;
import quickfix.field.ClOrdID;
import quickfix.field.MsgType;
import quickfix.field.NoOrders;
import quickfix.field.SecondaryClOrdID;

import java.util.List;
import java.util.Optional;


/**
 * Get the test identifier, this is always the ClOrdID, even for allocation instructions.  It is important, however, to send the actual
 * identifier with the response.  For allocation acknowledgements and reports this is the AllocID.  This has the side-effect of creating
 * the identifier if it doesn't exist.
 */
public class QuickFixTestIdentifierProvider implements FixTestIdentifierProvider<Message> {

	private FixIdentifierHandler<Message> fixIdentifierHandler;
	private FixUtilService<Message> fixUtilService;
	private FixMessageHandler fixMessageHandler;


	@Override
	public String getAllocationSecondaryIdentifier(List<Message> responses) {
		Optional<String> secondaryClOrdID = responses.stream().filter(m -> getMessageType(m).equals(FixMessageTypeTagValues.EXECUTION_REPORT_TYPE_TAG.getTagValue()))
				.filter(m -> m.isSetField(SecondaryClOrdID.FIELD)).map(m -> getFieldValue(m, SecondaryClOrdID.FIELD)).findFirst();
		return secondaryClOrdID.orElse(null);
	}


	@Override
	public String getTestIdentifier(Message fixMessage, BaseFixMessagingMessage fixMessagingMessage) {
		FixIdentifier fixIdentifier;
		FixMessageTypeTagValues messageType = FixMessageTypeTagValues.findMessageTypeTagValue(getMessageType(fixMessage));
		switch (messageType) {
			case NEW_ORDER_SINGLE_TYPE_TAG:
				String messageText = getFixMessageHandler().getBaseFixMessagingMessageText(fixMessagingMessage);
				fixIdentifier = getFixIdentifierHandler().applyFixIdentifier(fixMessage, fixMessagingMessage, false, null, messageText);
				break;
			case ALLOCATION_INSTRUCTION_TYPE_TAG:
				return getAllocationIdentifier(fixMessage, fixMessagingMessage);
			default:
				throw new RuntimeException("Can not get the test identifier for message type " + getMessageType(fixMessage));
		}
		if (fixIdentifier == null) {
			throw new RuntimeException("Could not get the fix identifier for the message type " + getMessageType(fixMessage));
		}
		return fixIdentifier.getUniqueStringIdentifier();
	}


	@Override
	public String getMessageIdentifier(Message message) {
		if (message.isSetField(ClOrdID.FIELD)) {
			return getFieldValue(message, ClOrdID.FIELD);
		}
		if (message.isSetField(AllocID.FIELD)) {
			return getFieldValue(message, AllocID.FIELD);
		}
		return null;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private String getMessageType(Message message) {
		return getFieldValue(message.getHeader(), MsgType.FIELD);
	}


	private String getFieldValue(FieldMap map, int field) {
		try {
			return map.getString(field);
		}
		catch (FieldNotFound e) {
			throw new RuntimeException(String.format("Message field %s could not be found", field), e);
		}
	}


	private String getAllocationIdentifier(Message fixMessage, BaseFixMessagingMessage fixMessagingMessage) {
		String identifier = null;
		List<Group> groups = fixMessage.getGroups(NoOrders.FIELD);
		Optional<Group> noOrderGrp = groups.stream().filter(g -> g.isSetField(ClOrdID.FIELD)).findFirst();
		if (noOrderGrp.isPresent() && !StringUtils.isEmpty(getFieldValue(noOrderGrp.get(), ClOrdID.FIELD))) {
			String clientID = getFieldValue(noOrderGrp.get(), ClOrdID.FIELD);
			FixSession fixSession = getFixUtilService().getFixSession(fixMessage, fixMessagingMessage.getSessionQualifier(), false);
			FixIdentifier fixIdentifier = getFixIdentifierHandler().getFixIdentifierByStringId(clientID, fixSession.getId());
			if (fixIdentifier != null) {
				identifier = fixIdentifier.getUniqueStringIdentifier();
			}
			// must be secondary client ID
			else if (!clientID.contains("O_")) {
				identifier = clientID;
			}
		}
		if (StringUtils.isEmpty(identifier)) {
			throw new RuntimeException("Expected the allocation to have an client identifier.");
		}
		return identifier;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public FixIdentifierHandler<Message> getFixIdentifierHandler() {
		return this.fixIdentifierHandler;
	}


	public void setFixIdentifierHandler(FixIdentifierHandler<Message> fixIdentifierHandler) {
		this.fixIdentifierHandler = fixIdentifierHandler;
	}


	public FixUtilService<Message> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<Message> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}


	public FixMessageHandler getFixMessageHandler() {
		return this.fixMessageHandler;
	}


	public void setFixMessageHandler(FixMessageHandler fixMessageHandler) {
		this.fixMessageHandler = fixMessageHandler;
	}
}
