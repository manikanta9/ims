package com.clifton.fix.quickfix.server.response;

import com.clifton.fix.quickfix.server.response.transform.QuickFixMessageMapper;
import com.clifton.fix.server.message.FixMessageDifference;
import com.clifton.fix.server.message.FixMessageFieldDifference;
import com.clifton.fix.server.response.FixMessageEquivalency;
import quickfix.DataDictionary;
import quickfix.DefaultDataDictionaryProvider;
import quickfix.Field;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.MsgType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.StreamSupport.stream;


/**
 * Tests two messages for equivalency by ignoring some fields, typically date dependent field values.
 * Check header, body, and group fields.
 */
public class QuickFixMessageEquivalency implements FixMessageEquivalency<Message, Field<?>> {

	// defaulting to version 4.4
	private static final DataDictionary dd = new DefaultDataDictionaryProvider().getSessionDataDictionary("FIX.4.4");

	private static final Function<FieldMap, List<Field<?>>> getComparableFields = (fieldMap) -> {
		Iterable<Field<?>> fields = fieldMap::iterator;
		return stream(fields.spliterator(), false)
				.filter(f -> !QuickFixMessageMapper.MUTABLE_FIELDS.contains(f.getField()))
				.flatMap(f ->
						Stream.of(f.getObject())
								.map(o -> o == null ? "" : o.toString())
								.flatMap(s -> Arrays.stream(s.split(";")))
								.map(v -> new StringField(f.getField(), v)))
				.collect(Collectors.toList());
	};


	private static final Function<FieldMap, List<Field<?>>> getGroupFields = (fieldMap) -> {
		Iterable<Integer> keys = fieldMap::groupKeyIterator;
		return stream(keys.spliterator(), false)
				.flatMap(k -> fieldMap.getGroups(k).stream())
				.filter(g -> !g.isEmpty())
				.flatMap(g -> getComparableFields.apply(g).stream())
				.filter(f -> f.getObject() != null)
				.collect(Collectors.toList());
	};

	private static final BiFunction<List<Field<?>>, Set<Field<?>>, Optional<String>> findFirstByKey = (values, source) ->
			source.stream().filter(s -> values.stream().map(Field::getTag).anyMatch(t -> t == s.getTag()))
					.map(f -> (f.getObject() == null) ? "" : f.getObject().toString()).findFirst();

	private static final BiFunction<Set<Field<?>>, Set<Field<?>>, List<FixMessageFieldDifference>> buildDifferenceFields = (incoming, template) -> {
		List<FixMessageFieldDifference> differences = new ArrayList<>();

		symmetricDifference(incoming, template).stream().collect(Collectors.groupingBy(Field::getTag)).forEach(
				(key, list) -> differences.add(FixMessageFieldDifference.build(
						key.toString(),
						dd.getFieldName(key),
						() -> findFirstByKey.apply(list, incoming).orElse(null),
						() -> findFirstByKey.apply(list, template).orElse(null)
				)));

		return differences;
	};
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean areEquivalent(Message one, Message two) {
		return areFieldsEquivalent(one.getHeader(), two.getHeader())
				&& areFieldsEquivalent(one, two)
				&& areGroupFieldsEquivalent(one, two);
	}


	@Override
	public FixMessageDifference symmetricDifferences(Message incoming, Message template) {
		FixMessageDifference fixMessageDifference = new FixMessageDifference(getFieldValue(incoming.getHeader(), MsgType.FIELD));

		List<FixMessageFieldDifference> body = buildDifferenceFields.apply(new HashSet<>(getComparableFields.apply(incoming)),
				new HashSet<>(getComparableFields.apply(template)));
		fixMessageDifference.addBody(body);

		List<FixMessageFieldDifference> header = buildDifferenceFields.apply(new HashSet<>(getComparableFields.apply(incoming.getHeader())),
				new HashSet<>(getComparableFields.apply(template.getHeader())));
		fixMessageDifference.addHeader(header);

		List<FixMessageFieldDifference> group = buildDifferenceFields.apply(new HashSet<>(getGroupFields.apply(incoming)),
				new HashSet<>(getGroupFields.apply(template)));
		fixMessageDifference.addGroup(group);

		return fixMessageDifference;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static List<Field<?>> symmetricDifference(FieldMap one, FieldMap two) {
		Set<Field<?>> elements1 = new HashSet<>(getComparableFields.apply(one));
		Set<Field<?>> elements2 = new HashSet<>(getComparableFields.apply(two));
		return symmetricDifference(elements1, elements2);
	}


	private static List<Field<?>> symmetricDifference(Set<Field<?>> fields1, Set<Field<?>> fields2) {
		Set<Field<?>> elements1 = new HashSet<>(fields1);
		Set<Field<?>> elements2 = new HashSet<>(fields2);

		Set<Field<?>> result = new HashSet<>(elements1);
		result.addAll(elements2);
		elements1.retainAll(elements2);
		result.removeAll(elements1);

		return new ArrayList<>(result);
	}


	private static boolean areFieldsEquivalent(FieldMap oneMap, FieldMap twoMap) {
		List<Field<?>> oneFields = getComparableFields.apply(oneMap);
		List<Field<?>> twoFields = getComparableFields.apply(twoMap);
		return Objects.equals(oneFields, twoFields);
	}


	private static boolean areGroupFieldsEquivalent(Message one, Message two) {
		Set<Field<?>> oneGrpFields = new HashSet<>(getGroupFields.apply(one));
		Set<Field<?>> twoGrpFields = new HashSet<>(getGroupFields.apply(two));
		return Objects.equals(oneGrpFields, twoGrpFields);
	}


	private static String getFieldValue(FieldMap message, int field) {
		try {
			return message.getString(field);
		}
		catch (FieldNotFound fieldNotFound) {
			// swallow
		}
		return "";
	}
}
