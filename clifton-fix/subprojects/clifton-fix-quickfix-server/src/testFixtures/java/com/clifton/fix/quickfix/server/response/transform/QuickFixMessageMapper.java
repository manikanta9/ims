package com.clifton.fix.quickfix.server.response.transform;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.response.transform.FixFieldValueMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import quickfix.Message;
import quickfix.field.AllocID;
import quickfix.field.AllocReportID;
import quickfix.field.BodyLength;
import quickfix.field.ClOrdID;
import quickfix.field.MsgSeqNum;
import quickfix.field.SenderSubID;
import quickfix.field.SendingTime;
import quickfix.field.TargetSubID;
import quickfix.field.TransactTime;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Provides value mappings from a source message to a target message.
 * Will replace template response field values, such as CIOrdID with
 * the correct value from the JMS retrieve message.  The length and
 * Checksum will be recalculated.
 * <p>
 * DEPENDS ON {@link QuickFixMessageUtils#getStringField(String, int)}
 * returning null when a field is missing, this cannot throw an exception.
 */
public class QuickFixMessageMapper {

	public static final ImmutableList<Integer> MUTABLE_FIELDS = ImmutableList.of(
			SendingTime.FIELD,
			ClOrdID.FIELD,
			TransactTime.FIELD,
			AllocID.FIELD,
			AllocReportID.FIELD,
			BodyLength.FIELD,
			SenderSubID.FIELD,
			TargetSubID.FIELD,
			MsgSeqNum.FIELD);

	private final String source;
	private final FixMessageSequenceProvider fixMessageSequenceProvider;

	// retains insertion order.
	private final ImmutableMap<Integer, Replacement> fieldMappings = ImmutableMap.<Integer, Replacement>builder()
			.put(SendingTime.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), SendingTime.FIELD))
			.put(ClOrdID.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), ClOrdID.FIELD))
			.put(TransactTime.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), TransactTime.FIELD))
			.put(AllocID.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), AllocID.FIELD))
			.put(AllocReportID.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), AllocID.FIELD))
			.put(TargetSubID.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), TargetSubID.FIELD))
			.put(SenderSubID.FIELD, (old) -> QuickFixMessageUtils.getStringField(this.getSource(), SenderSubID.FIELD))
			.put(BodyLength.FIELD, (old) -> old)
			.put(MsgSeqNum.FIELD, (old) -> this.getNextSequenceNumber())
			.build();

	private final Map<String, Map<Integer, List<Replacement>>> dynamicFieldMappings = new HashMap<>();


	public QuickFixMessageMapper(Message source, FixMessageSequenceProvider fixMessageSequenceProvider, FixFieldValueMapper fieldValueMapper) {
		AssertUtils.assertTrue(MUTABLE_FIELDS.containsAll(this.fieldMappings.keySet()), "The field list and map must be the same.");
		this.source = source.toString();
		this.fixMessageSequenceProvider = fixMessageSequenceProvider;

		buildDynamicFieldMappings(fieldValueMapper);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Integer, Replacement> getStaticFieldMappings() {
		return this.fieldMappings;
	}


	public Map<Integer, List<Replacement>> getDynamicFieldMappings(String uniqueIdentifier) {
		return this.dynamicFieldMappings.getOrDefault(uniqueIdentifier, Collections.emptyMap());
	}


	public String getSource() {
		return this.source;
	}


	public String getNextSequenceNumber() {
		return this.fixMessageSequenceProvider.getNextSequenceNumber();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void buildDynamicFieldMappings(FixFieldValueMapper fieldValueMapper) {
		for (String uniqueIdentifier : fieldValueMapper.getUniqueIdentifiers()) {
			Map<Integer, List<Replacement>> replacements = this.dynamicFieldMappings.computeIfAbsent(uniqueIdentifier, key -> new HashMap<>());
			for (Map.Entry<String, String> entry : fieldValueMapper.getFieldValueMapping(uniqueIdentifier).entrySet()) {
				String[] before = entry.getKey().split("\\=");
				AssertUtils.assertTrue(!ArrayUtils.isEmpty(before) && before.length == 2, "Invalid format");
				Integer field = Integer.parseInt(before[0].trim());
				String[] after = entry.getValue().split("\\=");
				AssertUtils.assertTrue(!ArrayUtils.isEmpty(after) && after.length == 2, "Invalid format");

				final String beforeValue = before[1].trim();
				final String afterValue = after[1].trim();
				List<Replacement> replacementList = replacements.computeIfAbsent(field, key -> new ArrayList<>());
				replacementList.add((old) -> (StringUtils.isEqual(old, beforeValue)) ? afterValue : old);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@FunctionalInterface
	public interface Replacement {

		public String replace(String old);
	}
}
