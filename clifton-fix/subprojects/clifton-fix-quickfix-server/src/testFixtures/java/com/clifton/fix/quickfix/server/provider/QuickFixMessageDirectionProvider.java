package com.clifton.fix.quickfix.server.provider;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.server.provider.FixMessageDirectionProvider;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.field.SenderCompID;
import quickfix.field.TargetCompID;


/**
 * {@inheritDoc}
 */
public class QuickFixMessageDirectionProvider implements FixMessageDirectionProvider<Message> {

	// taken from the first request, target = FIX, sender = IMS.
	private String targetCompID;
	private String senderCompID;


	public QuickFixMessageDirectionProvider(Message incoming) {
		this.targetCompID = getString(incoming, TargetCompID.FIELD);
		this.senderCompID = getString(incoming, SenderCompID.FIELD);
	}


	@Override
	public boolean isRequest(Message message) {
		return (StringUtils.isEqual(this.senderCompID, getString(message, SenderCompID.FIELD)));
	}


	@Override
	public boolean isResponse(Message message) {
		return (StringUtils.isEqual(this.senderCompID, getString(message, TargetCompID.FIELD)));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getString(Message message, int field) {
		if (message.isSetField(field)) {
			return getFieldMapString(message, field);
		}
		else if (message.getHeader().isSetField(field)) {
			return getFieldMapString(message.getHeader(), field);
		}
		else {
			throw new RuntimeException("Could not find message field " + field);
		}
	}


	private String getFieldMapString(FieldMap fieldMap, int field) {
		try {
			return fieldMap.getString(field);
		}
		catch (FieldNotFound e) {
			throw new RuntimeException("Could not find message field", e);
		}
	}
}
