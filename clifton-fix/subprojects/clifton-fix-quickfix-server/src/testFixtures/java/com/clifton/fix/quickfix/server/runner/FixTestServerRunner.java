package com.clifton.fix.quickfix.server.runner;

import com.clifton.core.context.CurrentContextApplicationListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


/**
 * FixTestServerRunner loads the fix server spring context using the provided properties.
 * The Spring context will instantiate the beans and create the threads necessary to
 * receive JMS messages.  The context is loaded within a Callable interface.
 * The callable interface allows exceptions encountered during context loading to
 * be redirected back to 'this' thread and will fail the test rather than silently
 * failing in the background.
 */
public class FixTestServerRunner {

	public static final String FIX_CONTEXT_PROPERTIES = "fix.context.properties";
	public static final String CLASSPATH_SERVICE_CLIFTON_FIX_CONTEXT_XML = "classpath:com/clifton/fix/quickfix/server/runner/service-clifton-test-fix-context.xml";

	private String fixContextProperties;
	private ClassPathXmlApplicationContext applicationContext;

	private SpringRefreshedListener springRefreshedListener = new SpringRefreshedListener();
	private SpringClosedListener springClosedListener = new SpringClosedListener();


	public FixTestServerRunner(String fixContextProperties) {
		this.fixContextProperties = fixContextProperties;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext startup() throws Exception {
		System.setProperty(FIX_CONTEXT_PROPERTIES, this.fixContextProperties);
		this.applicationContext = new ClassPathXmlApplicationContext(new String[]{CLASSPATH_SERVICE_CLIFTON_FIX_CONTEXT_XML}, false);
		this.applicationContext.addApplicationListener(this.springRefreshedListener);
		this.applicationContext.addApplicationListener(this.springClosedListener);
		this.applicationContext.registerShutdownHook();
		this.applicationContext.refresh();
		this.springRefreshedListener.awaitRefresh();
		return this.applicationContext;
	}


	public void shutdown() throws Exception {
		try {
			if (this.applicationContext != null) {
				this.applicationContext.close();
				this.springClosedListener.awaitClose();
				this.applicationContext = null;
			}
		}
		finally {
			System.clearProperty(FIX_CONTEXT_PROPERTIES);
		}
	}


	class SpringRefreshedListener implements CurrentContextApplicationListener<ContextRefreshedEvent> {

		private CountDownLatch contextInitializationLatch = new CountDownLatch(1);


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Override
		public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
			this.contextInitializationLatch.countDown();
		}


		public void awaitRefresh() throws InterruptedException {
			this.contextInitializationLatch.await(1, TimeUnit.MINUTES);
		}


		@Override
		public void setApplicationContext(ApplicationContext context) {
			//Need to override for ApplicationContextListener interface
		}


		@Override
		public ApplicationContext getApplicationContext() {
			return FixTestServerRunner.this.applicationContext;
		}
	}


	class SpringClosedListener implements CurrentContextApplicationListener<ContextClosedEvent> {

		private CountDownLatch contextClosedLatch = new CountDownLatch(1);


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Override
		public void onCurrentContextApplicationEvent(ContextClosedEvent event) {
			this.contextClosedLatch.countDown();
		}


		public void awaitClose() throws InterruptedException {
			this.contextClosedLatch.await(1, TimeUnit.MINUTES);
		}


		@Override
		public void setApplicationContext(ApplicationContext context) {
			//Need to override for ApplicationContextListener interface
		}


		@Override
		public ApplicationContext getApplicationContext() {
			return FixTestServerRunner.this.applicationContext;
		}
	}
}
