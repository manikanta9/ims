package com.clifton.fix.quickfix.server.provider;

import com.clifton.fix.server.provider.FixMessageDirectionProvider;
import com.clifton.fix.server.provider.FixMessageDirectionProviderFactory;
import quickfix.Message;


/**
 * {@inheritDoc}
 */
public class QuickFixMessageDirectionProviderFactory implements FixMessageDirectionProviderFactory<Message> {

	@Override
	public FixMessageDirectionProvider<Message> getFixMessageDirectionProvider(Message message) {
		return new QuickFixMessageDirectionProvider(message);
	}
}
