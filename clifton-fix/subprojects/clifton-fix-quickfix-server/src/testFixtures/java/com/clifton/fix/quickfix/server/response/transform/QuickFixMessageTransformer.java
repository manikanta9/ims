package com.clifton.fix.quickfix.server.response.transform;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.server.provider.FixMessageSequenceProvider;
import com.clifton.fix.server.response.transform.FixFieldValueMapper;
import com.clifton.fix.server.response.transform.FixMessageTransformer;
import com.google.common.collect.ImmutableList;
import quickfix.Field;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.Message;
import quickfix.StringField;
import quickfix.field.AllocID;
import quickfix.field.BodyLength;
import quickfix.field.CheckSum;
import quickfix.field.ClOrdID;
import quickfix.field.SendingTime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.StreamSupport;


/**
 * Takes a 'template' message and applies values from the source message.
 * The template is first deeply cloned.  The value providers are supplied
 * by the source and extract by the mapper.
 */
public class QuickFixMessageTransformer implements FixMessageTransformer<Message> {

	private static final List<Integer> SkippedFields = ImmutableList.of(CheckSum.FIELD, BodyLength.FIELD, SendingTime.FIELD);
	private static final Predicate<Field<?>> SkippedFieldsFilter = field -> !SkippedFields.contains(field.getField());

	private FixFieldValueMapper fixFieldValueMapper;
	private FixMessageSequenceProvider fixMessageSequenceProvider;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message transform(Message templateResponse, Message source) {
		return doTransform(templateResponse, source, getFixFieldValueMapper(), getFixMessageSequenceProvider());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static Message doTransform(Message templateResponse, Message source, FixFieldValueMapper fixFieldValueMapper, FixMessageSequenceProvider fixMessageSequenceProvider) {
		QuickFixMessageMapper mapper = new QuickFixMessageMapper(source, fixMessageSequenceProvider, fixFieldValueMapper);
		final Map<Integer, List<QuickFixMessageMapper.Replacement>> dynamicReplacementMap = new HashMap<>();
		if (templateResponse.isSetField(ClOrdID.FIELD)) {
			try {
				String uniqueStringIdentifier = templateResponse.getString(ClOrdID.FIELD);
				if (!mapper.getDynamicFieldMappings(uniqueStringIdentifier).isEmpty()) {
					dynamicReplacementMap.putAll(mapper.getDynamicFieldMappings(uniqueStringIdentifier));
				}
			}
			catch (FieldNotFound fieldNotFound) {
				// swallow
			}
		}
		else if (templateResponse.isSetField(AllocID.FIELD)) {
			try {
				String uniqueStringIdentifier = templateResponse.getString(AllocID.FIELD);
				if (!mapper.getDynamicFieldMappings(uniqueStringIdentifier).isEmpty()) {
					dynamicReplacementMap.putAll(mapper.getDynamicFieldMappings(uniqueStringIdentifier));
				}
			}
			catch (FieldNotFound fieldNotFound) {
				// swallow
			}
		}
		Function<Field<?>, StringField> fieldsMapperFunction = field -> {
			if (mapper.getStaticFieldMappings().containsKey(field.getTag())) {
				return new StringField(field.getTag(), mapper.getStaticFieldMappings().get(field.getTag()).replace(StringUtils.toNullableString(field.getObject())));
			}
			else if (dynamicReplacementMap.containsKey(field.getTag())) {
				List<QuickFixMessageMapper.Replacement> replacementList = dynamicReplacementMap.get(field.getTag());
				for (QuickFixMessageMapper.Replacement replacement : replacementList) {
					String value = replacement.replace(StringUtils.toNullableString(field.getObject()));
					if (!Objects.equals(value, field.getObject())) {
						return new StringField(field.getTag(), value);
					}
				}
				return new StringField(field.getTag(), field.getObject().toString());
			}
			else {
				return new StringField(field.getTag(), field.getObject().toString());
			}
		};

		Message transformed = new Message();
		// body fields
		Iterable<Field<?>> fields = templateResponse::iterator;
		StreamSupport.stream(fields.spliterator(), false)
				.filter(SkippedFieldsFilter)
				.map(fieldsMapperFunction)
				.filter(f -> !StringUtils.isEmpty(f.getValue()))
				.forEach(transformed::setField);
		// header fields
		Iterable<Field<?>> header = () -> templateResponse.getHeader().iterator();
		StreamSupport.stream(header.spliterator(), false)
				.filter(SkippedFieldsFilter)
				.map(fieldsMapperFunction)
				.filter(f -> !StringUtils.isEmpty(f.getValue()))
				.forEach(f -> transformed.getHeader().setField(f));
		// group fields
		Iterable<Integer> groupKeys = templateResponse::groupKeyIterator;
		StreamSupport.stream(groupKeys.spliterator(), false)
				.flatMap(k -> templateResponse.getGroups(k).stream())
				.forEach(g -> {
					Iterable<Field<?>> grpFields = g::iterator;
					Group group = new Group(g.getFieldTag(), g.delim(), g.getFieldOrder());
					StreamSupport.stream(grpFields.spliterator(), false)
							.filter(SkippedFieldsFilter)
							.map(fieldsMapperFunction)
							.filter(f -> !StringUtils.isEmpty(f.getValue()))
							.forEach(group::setField);
					transformed.addGroup(group);
				});
		return transformed;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixFieldValueMapper getFixFieldValueMapper() {
		return this.fixFieldValueMapper;
	}


	public void setFixFieldValueMapper(FixFieldValueMapper fixFieldValueMapper) {
		this.fixFieldValueMapper = fixFieldValueMapper;
	}


	public FixMessageSequenceProvider getFixMessageSequenceProvider() {
		return this.fixMessageSequenceProvider;
	}


	public void setFixMessageSequenceProvider(FixMessageSequenceProvider fixMessageSequenceProvider) {
		this.fixMessageSequenceProvider = fixMessageSequenceProvider;
	}
}
