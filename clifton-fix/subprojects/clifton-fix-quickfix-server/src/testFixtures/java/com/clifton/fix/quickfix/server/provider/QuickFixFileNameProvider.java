package com.clifton.fix.quickfix.server.provider;

import com.clifton.core.util.StringUtils;
import com.clifton.fix.server.provider.FixFileNameProvider;
import quickfix.Message;
import quickfix.MessageUtils;
import quickfix.field.OrderQty;
import quickfix.field.SecurityID;
import quickfix.field.SenderCompID;
import quickfix.field.SenderSubID;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * {@inheritDoc}
 */
public class QuickFixFileNameProvider implements FixFileNameProvider<Message> {


	public static final List<Integer> KEYS = Arrays.asList(
			SenderCompID.FIELD, // Assigned value used to identify firm sending message.
			SenderSubID.FIELD, // Assigned value used to identify specific message originator (desk, trader, etc.)
			SecurityID.FIELD,   // Security identifier value of SecurityIDSource (22) type (e.g. CUSIP, SEDOL, ISIN, etc). Requires SecurityIDSource.
			OrderQty.FIELD
	);


	@Override
	public String getFixFileName(Message message) {
		String messageString = message.toString();
		return KEYS.stream()
				.map(tag -> MessageUtils.getStringField(messageString, tag))
				.filter(t -> !StringUtils.isEmpty(t))
				.map(String::toUpperCase)
				.map(t -> t.replace('.', '_'))
				.map(t -> t.replace(' ', '_'))
				.map(t -> t.replace('/', '_'))
				.collect(Collectors.joining("_")) + "." + FIX_DATA_FILE_EXTENSION;
	}
}
