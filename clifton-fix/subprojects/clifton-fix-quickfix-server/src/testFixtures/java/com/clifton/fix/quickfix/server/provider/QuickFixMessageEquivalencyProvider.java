package com.clifton.fix.quickfix.server.provider;

import com.clifton.fix.quickfix.server.response.QuickFixMessageEquivalency;
import com.clifton.fix.server.provider.FixMessageEquivalencyProvider;
import com.clifton.fix.server.response.FixMessageEquivalency;
import quickfix.Field;
import quickfix.Message;


/**
 * {@inheritDoc}
 */
public class QuickFixMessageEquivalencyProvider implements FixMessageEquivalencyProvider<Message, Field<?>> {

	@Override
	public FixMessageEquivalency<Message, Field<?>> getFixMessageEquivalency() {
		return new QuickFixMessageEquivalency();
	}
}
