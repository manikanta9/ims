package com.clifton.fix.quickfix.server.session;

import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionSettingsConfigurer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import quickfix.Initiator;
import quickfix.Session;
import quickfix.SessionFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * The {@link QuickFixSessionSettingsAcceptorConfigurer} configures {@link Session} settings for {@link Initiator} sessions.
 *
 * @author MikeH
 */
@Component
@Order(100) // Execute after the standard configurer so that the connection type setting is defined
public class QuickFixSessionSettingsInitiatorConfigurer implements FixSessionSettingsConfigurer {

	@Override
	public void configure(FixSessionDefinition sessionDefinition, Map<String, String> sessionSettings) {
		if (!Objects.equals(SessionFactory.INITIATOR_CONNECTION_TYPE, sessionSettings.get(SessionFactory.SETTING_CONNECTION_TYPE))) {
			return;
		}
		sessionSettings.putAll(getInitiatorSettings(sessionDefinition));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, String> getInitiatorSettings(FixSessionDefinition sessionDefinition) {
		Map<String, String> settings = new HashMap<>();
		MapUtils.putIfNotNull(settings, Initiator.SETTING_RECONNECT_INTERVAL, StringUtils.toNullableString(sessionDefinition.getReconnectInterval()));
		MapUtils.putIfNotNull(settings, Initiator.SETTING_SOCKET_CONNECT_HOST, sessionDefinition.getSocketConnectHost());
		MapUtils.putIfNotNull(settings, Initiator.SETTING_SOCKET_CONNECT_PORT, sessionDefinition.getSocketConnectPort());

		return settings;
	}
}
