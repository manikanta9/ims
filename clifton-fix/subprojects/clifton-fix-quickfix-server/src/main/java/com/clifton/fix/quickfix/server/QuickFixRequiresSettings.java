package com.clifton.fix.quickfix.server;


import quickfix.SessionSettings;


/**
 * The <code>QuickFixRequiresSettings</code> indicates that an object requires the sessions setting before it can be used.
 *
 * @author mwacker
 */
public interface QuickFixRequiresSettings {

	public void setSessionSettings(SessionSettings settings);
}
