package com.clifton.fix.quickfix.server.util;

import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionParameters;
import quickfix.SessionID;
import quickfix.SessionSettings;


public class QuickFixSessionUtils {

	/**
	 * Get the session id from the definition.
	 */
	public static SessionID getSessionID(FixSessionDefinition sessionDefinition) {
		String beginString = sessionDefinition.getBeginString();
		String senderCompID = sessionDefinition.getSenderCompId();
		String senderSubID = sessionDefinition.getSenderSubId();
		String senderLocationID = sessionDefinition.getSenderLocId();
		String targetCompID = sessionDefinition.getTargetCompId();
		String targetSubID = sessionDefinition.getTargetSubId();
		String targetLocationID = sessionDefinition.getTargetLocId();
		String sessionQualifier = sessionDefinition.getSessionQualifier();
		return new SessionID(beginString, senderCompID, senderSubID, senderLocationID, targetCompID, targetSubID, targetLocationID, sessionQualifier);
	}


	public static SessionID getSessionID(FixSessionParameters sessionParameters) {
		String beginString = sessionParameters.getFixBeginString();
		String senderCompID = sessionParameters.getFixSenderCompId();
		String senderSubID = sessionParameters.getFixSenderSubId();
		String senderLocationID = sessionParameters.getFixSenderLocId();
		String targetCompID = sessionParameters.getFixTargetCompId();
		String targetSubID = sessionParameters.getFixTargetSubId();
		String targetLocationID = sessionParameters.getFixTargetLocId();
		String sessionQualifier = sessionParameters.getFixSessionQualifier();
		return new SessionID(beginString, senderCompID, senderSubID, senderLocationID, targetCompID, targetSubID, targetLocationID, sessionQualifier);
	}


	/**
	 * Gets the boolean value of the given {@link SessionID} and <code>key</code> within the provided {@link SessionSettings}. Returns <code>defaultValue</code> if the key was not
	 * found.
	 */
	public static Boolean getSessionBoolOrDefault(SessionSettings sessionSettings, SessionID sessionID, String key, Boolean defaultValue) {
		try {
			boolean result = defaultValue;
			if (sessionSettings.isSetting(sessionID, key)) {
				result = sessionSettings.getBool(sessionID, key);
			}
			return result;
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to retrieve the boolean value from session settings for session [%s] and key [%s].", sessionID, key), e);
		}
	}


	/**
	 * Gets the integer value of the given {@link SessionID} and <code>key</code> within the provided {@link SessionSettings}. Returns <code>defaultValue</code> if the key was not
	 * found.
	 */
	public static Integer getSessionIntOrDefault(SessionSettings sessionSettings, SessionID sessionID, String key, Integer defaultValue) {
		try {
			Integer result = defaultValue;
			if (sessionSettings.isSetting(sessionID, key)) {
				result = sessionSettings.getInt(sessionID, key);
			}
			return result;
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to retrieve the integer value from session settings for session [%s] and key [%s].", sessionID, key), e);
		}
	}
}
