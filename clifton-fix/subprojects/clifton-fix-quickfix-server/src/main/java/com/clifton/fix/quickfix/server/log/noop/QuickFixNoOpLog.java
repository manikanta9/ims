package com.clifton.fix.quickfix.server.log.noop;

import com.clifton.fix.quickfix.server.log.AbstractQuickFixLogFactory;
import quickfix.Log;
import quickfix.LogFactory;
import quickfix.SessionState;


/**
 * The {@link QuickFixNoOpLog} is a no-op logger. This is used to provide a logging implementation when needed which does nothing. This can be useful in some cases, such as when
 * logging should be entirely disabled for a session, or when a {@link LogFactory} needs to return a disabled {@link Log} implementation (see {@link
 * AbstractQuickFixLogFactory#enabled}).
 * <p>
 * This is an alternative to the no-op logger ({@link SessionState.NullLog}) substituted by QuickFIX/J when a <code>null</code> logger is provided for a session. The {@link
 * QuickFixNoOpLog} is required in cases where the built-in no-op logger would not otherwise be substituted, such as {@link quickfix.CompositeLog}.
 *
 * @author MikeH
 * @see SessionState.NullLog
 * @see AbstractQuickFixLogFactory
 */
public class QuickFixNoOpLog implements Log {

	public static final QuickFixNoOpLog INSTANCE = new QuickFixNoOpLog();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private QuickFixNoOpLog() {
		// Do nothing
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void clear() {
		// Do nothing
	}


	@Override
	public void onIncoming(String message) {
		// Do nothing
	}


	@Override
	public void onOutgoing(String message) {
		// Do nothing
	}


	@Override
	public void onEvent(String text) {
		// Do nothing
	}


	@Override
	public void onErrorEvent(String text) {
		// Do nothing
	}
}
