package com.clifton.fix.quickfix.server.log;


import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import quickfix.Log;
import quickfix.MessageUtils;

import java.io.Closeable;


/**
 * The {@link AbstractQuickFixLog} is a base implementation for QuickFIX logs. This provides the ability to enable standard filters for heartbeats and non-persistent messages.
 *
 * @author mwacker
 */
public abstract class AbstractQuickFixLog implements Log, Closeable {

	private FixMessageEntryService fixMessageEntryService;

	/**
	 * If <code>true</code>, {@link QuickFixMessageUtils#isHeartbeat(java.lang.String) heartbeat} messages will be logged. Otherwise, they will be excluded.
	 */
	private boolean logHeartbeats;
	/**
	 * If <code>true</code>, {@link FixMessageEntryType#persistent persistent} messages will be logged. Otherwise, they will be excluded.
	 */
	private boolean logNonPersistentMessages;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected abstract void logIncoming(String message);


	protected abstract void logOutgoing(String message);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final void onIncoming(String message) {
		if (evaluateMessageFilter(message)) {
			logIncoming(message);
		}
	}


	@Override
	public final void onOutgoing(String message) {
		if (evaluateMessageFilter(message)) {
			logOutgoing(message);
		}
	}


	/**
	 * Closes the log.
	 * <p>
	 * This is implemented in order to expose the implicit {@link Closeable} functionality available to all {@link Log} implementations. See the {@link quickfix.Session#close}
	 * method, which checks for all {@link Closeable} logs and executes their {@link Closeable#close()} methods.
	 */
	@Override
	public void close() {
		// Do nothing by default
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected boolean evaluateMessageFilter(String message) {
		// Guard-clause: Trivial case; no filters enabled
		if (isLogHeartbeats() && isLogNonPersistentMessages()) {
			return true;
		}

		boolean filterSatisfied = true;
		if (!isLogHeartbeats()) {
			filterSatisfied = !MessageUtils.isHeartbeat(message);
		}
		if (filterSatisfied && !isLogNonPersistentMessages()) {
			String messageTypeTagValue = QuickFixMessageUtils.getMessageType(message);
			FixMessageEntryType messageType = getFixMessageEntryService().getFixMessageTypeByTagValue(messageTypeTagValue);
			filterSatisfied = messageType == null || messageType.isPersistent();
		}
		return filterSatisfied;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public boolean isLogHeartbeats() {
		return this.logHeartbeats;
	}


	public void setLogHeartbeats(boolean logHeartbeats) {
		this.logHeartbeats = logHeartbeats;
	}


	public boolean isLogNonPersistentMessages() {
		return this.logNonPersistentMessages;
	}


	public void setLogNonPersistentMessages(boolean logNonPersistentMessages) {
		this.logNonPersistentMessages = logNonPersistentMessages;
	}
}
