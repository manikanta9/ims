package com.clifton.fix.quickfix.server.log.composite;


import com.clifton.fix.quickfix.server.QuickFixRequiresSettings;
import com.clifton.fix.quickfix.server.log.AbstractQuickFixLogFactory;
import quickfix.CompositeLogFactory;
import quickfix.Log;
import quickfix.LogFactory;
import quickfix.SessionID;
import quickfix.SessionSettings;

import java.util.List;


/**
 * The {@link QuickFixCompositeLogFactory} is a {@link LogFactory} implementation which allows multiple logs to be used simultaneously. For example, events could be logged to the
 * console and messages could be logged to a database.
 * <p>
 * This implementation adds to the functionality provided by the QuickFIX/J built-in {@link CompositeLogFactory} by incorporating {@link AbstractQuickFixLogFactory} functionality.
 *
 * @author mwacker
 * @see CompositeLogFactory
 */
public class QuickFixCompositeLogFactory extends AbstractQuickFixLogFactory {

	private final List<LogFactory> logFactoryList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public QuickFixCompositeLogFactory(List<LogFactory> logFactoryList) {
		this.logFactoryList = logFactoryList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Log doCreate(SessionID sessionID) {
		LogFactory[] logFactories = getLogFactoryList().toArray(new LogFactory[0]);
		CompositeLogFactory compositeLogFactory = new CompositeLogFactory(logFactories);
		return compositeLogFactory.create(sessionID);
	}


	@Override
	public void setSessionSettings(SessionSettings sessionSettings) {
		super.setSessionSettings(sessionSettings);
		// Propagate settings to children
		for (LogFactory factory : getLogFactoryList()) {
			if (factory instanceof QuickFixRequiresSettings) {
				((QuickFixRequiresSettings) factory).setSessionSettings(sessionSettings);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<LogFactory> getLogFactoryList() {
		return this.logFactoryList;
	}
}
