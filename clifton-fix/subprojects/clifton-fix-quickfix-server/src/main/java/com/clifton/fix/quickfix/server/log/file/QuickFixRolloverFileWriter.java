package com.clifton.fix.quickfix.server.log.file;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.GZIPOutputStream;


/**
 * The {@link QuickFixRolloverFileWriter} writes content to a file that is rolled over when it reaches a maximum size.  Rolled over files with have file names consisting of the
 * base log file name with the current time stamp and the extension .gz (message.log.2017_03_23_12345.gz) appended.
 *
 * @see QuickFixRolloverFileLog
 */
public class QuickFixRolloverFileWriter extends Writer {

	public static final String ROLLOVER_FILE_DATE_FORMAT = "yyyy_MM_dd";
	public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(ROLLOVER_FILE_DATE_FORMAT);
	public static final int DEFAULT_MAXIMUM_BYTE_SIZE = 1 << 20; // 1 MB
	public static final int DEFAULT_OUTPUT_BUFFER_SIZE = 0; // Disabled by default

	private final Charset charset;
	private final Path filePath;
	private final int maxRolledFileSizeBytes;
	/**
	 * The size of the buffer used for output streams. Set to <code>0</code> to disable buffering.
	 */
	private final int outputBufferSize;
	private final boolean compressArchive;
	private final boolean syncAfterWrite;

	/**
	 * Creates an Executor that uses a single worker thread operating off an unbounded queue.
	 */
	private final ExecutorService pool;
	/**
	 * The backing {@link FileChannel} for the {@link #outputStream}. This channel is used internally for writes and is retained as a field for monitoring file descriptor metrics,
	 * such as {@link FileChannel#position()}, which is used to evaluate rollover criteria.
	 */
	private FileChannel outputFileChannel;
	private OutputStream outputStream;
	private final Object writerLock = new Object();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Construct a rolling log file output stream.
	 *
	 * @param filePath               must point to an actual file, not just a path to the output folder
	 * @param charset                the charset to use when writing messages to files
	 * @param maxRolledFileSizeBytes the maximum size of the log file before it is rolled over
	 * @param bufferSizeBytes        the default buffer size to use; if not provided, the lesser of {@link #DEFAULT_OUTPUT_BUFFER_SIZE} and <code>maxRolledFileSizeBytes</code> will
	 *                               be used; if set to <code>0</code>, buffering will be disabled
	 * @param compressArchive        after the log file is rolled over, it can be optionally compressed using gzip
	 * @param syncAfterWrite         if <code>true</code>, the {@link StandardOpenOption#SYNC} flag will be enabled, forcing all content to be written to the device immediately
	 * @throws IOException Will throw this exception only when writing to the output stream.  Exceptions during the rollover process are suppressed.  Any error
	 *                     during rollover should not hinder the logging process, just continue to log to the same log file even it exceeds the maximum file
	 *                     size.
	 */
	public QuickFixRolloverFileWriter(Path filePath, Charset charset, int maxRolledFileSizeBytes, Integer bufferSizeBytes, boolean compressArchive, boolean syncAfterWrite) {
		AssertUtils.assertNotNull(filePath, "Rollover file path is required.");
		AssertUtils.assertTrue(maxRolledFileSizeBytes >= DEFAULT_MAXIMUM_BYTE_SIZE, "The maximum file size must be at least 1 MB.");
		AssertUtils.assertTrue(bufferSizeBytes == null || bufferSizeBytes >= 0, "The buffer size must be greater than or equal to 0.");

		this.charset = charset;
		this.filePath = filePath;
		this.maxRolledFileSizeBytes = maxRolledFileSizeBytes;
		this.outputBufferSize = Math.min(bufferSizeBytes != null ? bufferSizeBytes : DEFAULT_OUTPUT_BUFFER_SIZE, maxRolledFileSizeBytes);
		this.compressArchive = compressArchive;
		this.syncAfterWrite = syncAfterWrite;
		this.pool = Executors.newSingleThreadExecutor();

		// DO NOT erase file contents, append = true is absolutely necessary.
		updateOutputStream(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void write(char[] cbuf, int off, int len) throws IOException {
		synchronized (getWriterLock()) {
			try {
				ByteBuffer encodedByteBuffer = getCharset().encode(CharBuffer.wrap(cbuf, off, len));
				ensureCapacity(encodedByteBuffer.remaining());
				getOutputStream().write(encodedByteBuffer.array(), encodedByteBuffer.position(), encodedByteBuffer.remaining());
			}
			catch (Exception e) {
				throw new RuntimeException("An exception occurred while writing to the file.", e);
			}
		}
	}


	@Override
	public void flush() {
		synchronized (getWriterLock()) {
			try {
				getOutputStream().flush();
			}
			catch (Exception e) {
				throw new RuntimeException("An exception occurred while flushing.", e);
			}
		}
	}


	@Override
	public void close() {
		synchronized (getWriterLock()) {
			closeOutputStream();
			getPool().shutdown();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a new {@link #outputStream} and corresponding backing {@link #outputFileChannel}, assigning instance field values and closing existing resources as needed.
	 */
	private void updateOutputStream(boolean append) {
		synchronized (getWriterLock()) {
			try {
				closeOutputStream();
				setOutputFileChannel(FileChannel.open(getFilePath(), getFileOpenOptions(append)));
				OutputStream channelOutputStream = Channels.newOutputStream(getOutputFileChannel());
				if (isOutputBufferEnabled()) {
					setOutputStream(new BufferedOutputStream(channelOutputStream, getOutputBufferSize()));
				}
				else {
					setOutputStream(channelOutputStream);
				}
			}
			catch (IOException e) {
				throw new RuntimeException(String.format("An error occurred while generating the file stream for path [%s].", getFilePath()), e);
			}
		}
	}


	/**
	 * Close file stream before rolling over.
	 */
	private void closeOutputStream() {
		synchronized (getWriterLock()) {
			try {
				if (getOutputFileChannel() != null && getOutputFileChannel().isOpen()) {
					getOutputStream().flush();
					getOutputFileChannel().force(true); // Block until device write is completed
					getOutputStream().close();
				}
			}
			catch (IOException ex) {
				LogUtils.error(getClass(), "Could not close file stream.", ex);
			}
			finally {
				setOutputFileChannel(null);
				setOutputStream(null);
			}
		}
	}


	/**
	 * The Steps performed during a file rollover: <ul> <li>close the current stream pointing to 'message.log'</li> <li>rename 'message.log' to
	 * 'message.log.2017_03_23_12345'</li> <li>recreate an empty 'message.log'</li> <li>open a stream to 'message.log'</li> <li>spawn a new thread to compress
	 * 'message.log.2017_03_23_12345' to 'message.log.2017_03_23_12345.gz'</li> <li>delete 'message.log.2017_03_23_12345' when the compression succeeds</li>
	 * </ul>
	 * <p>
	 * An atomic file operation is an operation that cannot be interrupted or "partially" performed. Either the entire operation is performed or the operation
	 * fails. This is important when you have multiple processes operating on the same area of the file system, and you need to guarantee that each process
	 * accesses a complete file.
	 * <p>
	 * With Linux, the rename is atomic if and only if the source path and target path are under the same mount point (not filesystem).
	 * <p>
	 * This is synchronized with the write methods using the original log file path instance variable.  This is necessary when doing a rollover because the
	 * output stream and original log file will not exist at the moment its being moved and before its been able to recreate the log file and its corresponding
	 * stream.  This should be a very infrequent and fast operation.
	 */
	private void ensureCapacity(int len) {
		synchronized (getWriterLock()) {
			try {
				boolean requireRollover = false;
				if (!isOutputBufferEnabled()) {
					requireRollover = getOutputFileChannel().position() + len > getMaxRolledFileSizeBytes();
				}
				else {
					// The output buffer may have contents, so it must be included when computing the required capacity
					if (getOutputFileChannel().position() + getOutputBufferSize() + len > getMaxRolledFileSizeBytes()) {
						// Flush buffer to update channel position and re-check
						getOutputStream().flush();
						requireRollover = getOutputFileChannel().position() + len > getMaxRolledFileSizeBytes();
					}
				}
				// Execute rollover if needed
				if (requireRollover) {
					closeOutputStream();
					Path archivedFile = getArchivedFilePath();
					// this is a native OS call to atomically rename the file
					Files.move(getFilePath(), archivedFile, StandardCopyOption.ATOMIC_MOVE);
					Files.createFile(getFilePath());
					// this is a new file, append = false.
					updateOutputStream(false);
					// spawn a separate thread to compress the file
					compressArchivedFile(archivedFile);
				}
			}
			catch (Exception ex) {
				// if anything goes wrong, just keep writing to the same file.
				LogUtils.error(getClass(), "Could not do log file rollover ", ex);
				restoreLogFileState();
			}
		}
	}


	/**
	 * Attempt to create a unique archive file name by incrementing the file name index.
	 */
	private Path getArchivedFilePath() throws IOException {
		final String datePortion = DATE_FORMATTER.format(LocalDate.now());

		String archivedFileName;
		int index = 1;
		List<String> existing;
		do {
			final String candidate = getFilePath().getFileName().toString() + "." + datePortion + "_" + index++;
			try (Stream<Path> files = Files.list(getFilePath().getParent())) {
				existing = files.map(path -> path.getFileName().toString())
						.filter(filename -> filename.startsWith(candidate))
						.collect(Collectors.toList());
			}
			archivedFileName = candidate;
		}
		while (!existing.isEmpty());
		return getFilePath().resolveSibling(Paths.get(archivedFileName));
	}


	/**
	 * Verify that the originally named log file and its corresponding stream are open, should do nothing under normal operation.
	 */
	private void restoreLogFileState() {
		if (!getFilePath().toFile().exists()) {
			LogUtils.warn(getClass(), "Something went wrong during log file rollover. Attempting to restore logging functionality.");
			try {
				Files.createFile(getFilePath());
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Could not create log file after performing a file rollover.", e);
			}
		}
		if (getOutputStream() == null) {
			LogUtils.warn(getClass(), "Something went wrong during log file rollover. Attempting to restore logging functionality.");
			// append = true, this could be an existing file.
			updateOutputStream(true);
		}
	}


	/**
	 * Use a singled threaded pool to compress the rolled-over, archived log file in the background.
	 */
	private void compressArchivedFile(final Path archivePath) {
		if (isCompressArchive()) {
			AssertUtils.assertNotNull(archivePath, "Archived file path cannot be null.");
			getPool().execute(() -> {
				long bytes;
				// resolve sibling will create within the same parent folder.
				Path targetPath = getFilePath().resolveSibling(Paths.get(archivePath.getFileName().toString() + ".gz"));
				// targetPath is a new file, appends = false
				try (
						OutputStream fos = Files.newOutputStream(targetPath, getFileOpenOptions(false));
						GZIPOutputStream zos = new GZIPOutputStream(fos)
				) {
					bytes = Files.copy(archivePath, zos);
				}
				catch (Exception e) {
					bytes = -1;
					LogUtils.error(getClass(), "Could not compress log file " + archivePath.getFileName(), e);
				}
				// allow the resources to flush and close before attempting to delete the unzipped version.
				try {
					long archivedBytes = Files.size(archivePath);
					if (archivedBytes == bytes && bytes > 0) {
						Files.delete(archivePath);
					}
					else {
						LogUtils.error(getClass(), "Could not delete log " + archivePath.getFileName() + " after compression, the written byte count " + archivedBytes + ", does not equal the log file byte count " + bytes);
					}
				}
				catch (Exception e) {
					LogUtils.error(getClass(), "Could not delete after compressing log file " + archivePath.getFileName(), e);
				}
			});
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StandardOpenOption[] getFileOpenOptions(boolean append) {
		List<StandardOpenOption> fileOpenOptionList = new ArrayList<>();
		fileOpenOptionList.add(StandardOpenOption.CREATE);
		fileOpenOptionList.add(StandardOpenOption.WRITE);
		fileOpenOptionList.add(append ? StandardOpenOption.APPEND : StandardOpenOption.TRUNCATE_EXISTING);
		if (isSyncAfterWrite()) {
			fileOpenOptionList.add(StandardOpenOption.SYNC);
		}
		return CollectionUtils.toArray(fileOpenOptionList, StandardOpenOption.class);
	}


	private boolean isOutputBufferEnabled() {
		return getOutputBufferSize() > 0;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FileChannel getOutputFileChannel() {
		return this.outputFileChannel;
	}


	private void setOutputFileChannel(FileChannel outputFileChannel) {
		this.outputFileChannel = outputFileChannel;
	}


	private OutputStream getOutputStream() {
		return this.outputStream;
	}


	private void setOutputStream(OutputStream outputStream) {
		this.outputStream = outputStream;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Charset getCharset() {
		return this.charset;
	}


	public Path getFilePath() {
		return this.filePath;
	}


	public int getMaxRolledFileSizeBytes() {
		return this.maxRolledFileSizeBytes;
	}


	public int getOutputBufferSize() {
		return this.outputBufferSize;
	}


	public boolean isCompressArchive() {
		return this.compressArchive;
	}


	public boolean isSyncAfterWrite() {
		return this.syncAfterWrite;
	}


	public ExecutorService getPool() {
		return this.pool;
	}


	public Object getWriterLock() {
		return this.writerLock;
	}
}
