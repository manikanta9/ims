package com.clifton.fix.quickfix.server;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import com.clifton.fix.server.FixServerExecutor;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionDefinitionSetting;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.session.FixSessionSettingsConfigurer;
import com.clifton.fix.session.cache.FixSessionCache;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import quickfix.Application;
import quickfix.Dictionary;
import quickfix.Initiator;
import quickfix.LogFactory;
import quickfix.MemoryStoreFactory;
import quickfix.MessageStoreFactory;
import quickfix.SLF4JLogFactory;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.SessionSettings;
import quickfix.mina.ssl.SSLSupport;

import java.io.File;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * The <code>AbstractFixServerExecutor</code> implements a basic FixServerExecutor used
 * to create, start and stop FIX sessions.
 *
 * @author mwacker
 */
public abstract class AbstractFixServerExecutor implements FixServerExecutor {

	private FixSessionCache<SessionID> fixSessionCache;
	private FixSessionService fixSessionService;
	private CacheHandler<?, ?> cacheHandler;
	/**
	 * The list of {@link FixSessionSettingsConfigurer} instances. These will be used to configure settings during initialization for all FIX sessions.
	 */
	@Autowired
	private List<FixSessionSettingsConfigurer> sessionSettingsConfigurerList;

	/**
	 * The FIX application used to handle the messages.
	 */
	private Application application;
	private SessionSettings sessionSettings;
	private MessageStoreFactory messageStoreFactory;
	private LogFactory logFactory;

	/**
	 * Configurable environment used to filter the session that can be started.
	 */
	private String serverEnvironment;
	/**
	 * The root path to the certificate, the relative path is specified in the database.
	 */
	private String certificatePath;

	/**
	 * List of external processed started for each session.
	 */
	private final Map<SessionID, Process> externalProcessList = new ConcurrentHashMap<>();
	/**
	 * Map of SessionID's to actual QuickFIX Sessions.  Used to visit the individual sessions d
	 */
	private final Lazy<Map<SessionID, Session>> sessionBySessionIdMapLazy = new Lazy<>(this::createSessionMap);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the {@link Initiator} associated with the given {@link SessionID}.
	 */
	public abstract Initiator getInitiator(SessionID sessionID);


	/**
	 * Gets the full list of {@link Initiator} objects associated with this executor.
	 */
	public abstract Collection<Initiator> getInitiatorList();


	/**
	 * Gets the full list of {@link SessionID} objects associated with this executor.
	 */
	public abstract Collection<SessionID> getSessionIdList();


	/**
	 * Generates the map of {@link SessionID} objects to their actual associated {@link Session} instances.
	 */
	protected abstract ConcurrentMap<SessionID, Session> createSessionMap();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Start external application that need to be running for the session.
	 * <p/>
	 * For example: some FIX connections use SSH tunnel and need have stunnel started be the session.
	 */
	protected void startExternalApplications() {
		List<FixSessionDefinition> sessionList = getFixSessionService().getFixSessionDefinitionListActiveAndPopulated();
		for (FixSessionDefinition session : CollectionUtils.getIterable(sessionList)) {
			startExternalApplicationForSession(session);
		}
	}


	protected void startExternalApplicationForSession(FixSessionDefinition session) {
		ValidationUtils.assertEquals(getServerEnvironment(), session.getServerEnvironment(),
				"Cannot start FIX session(s) because session [" + session.getName() + "] has a [" + session.getServerEnvironment() + "] but the current environment is [" + getServerEnvironment()
						+ "].");
		if (!session.isCreateSession() || StringUtils.isEmpty(session.getPathToExecutable())) {
			return;
		}

		File executable = new File(session.getPathToExecutable());
		if (executable.exists()) {
			SessionID sessionId = QuickFixSessionUtils.getSessionID(session);
			if (!this.externalProcessList.containsKey(sessionId)) {
				try {
					ProcessBuilder pb = new ProcessBuilder(session.getPathToExecutable(), session.getExecutableArgument());
					Process process = pb.start();
					this.externalProcessList.put(sessionId, process);
				}
				catch (Throwable e) {
					throw new RuntimeException("Failed to start external application on path [" + session.getPathToExecutable() + "].", e);
				}
				finally {
					stopExternalApplications();
				}
			}
		}
	}


	/**
	 * Stop the external application.
	 */
	protected void stopExternalApplications() {
		for (SessionID sessionId : getSessionBySessionIdMap().keySet()) {
			stopExternalApplicationForSession(sessionId);
		}
	}


	protected void stopExternalApplicationForSession(SessionID sessionId) {
		if (this.externalProcessList.containsKey(sessionId)) {
			LogUtils.debug(getClass(), "Stopping external application for [" + sessionId + "]");
			Process p;
			try {
				p = getExternalProcessList().remove(sessionId);
				p.destroyForcibly();
			}
			catch (Exception e) {
				// we don't have control over this external process, so swallow the error if we fail to stop it
				LogUtils.error(getClass(), "Failed to stop process.", e);
			}
		}
	}


	protected void verifySessionShutdown(SessionID sessionId, Session session) {
		// bit of a hack...
		int waitCount = 1;
		while (waitCount < 5 && session.isLoggedOn()) {
			LogUtils.info(getClass(), "Waiting for " + sessionId + " to logout");
			System.out.println("Waiting for " + sessionId + " to logout"); // print to the command line
			try {
				Thread.sleep(200L * waitCount);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new RuntimeException("Thread was interrupted while wait for log out.", e);
			}
			waitCount++;
		}
		if (session.isLoggedOn()) {
			LogUtils.warn(getClass(), sessionId + " didn't log out, shutting down anyway.");
			System.out.println(sessionId + " didn't log out, shutting down anyway."); // print to the command line
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This is just to avoid duplicating the loop code.
	 * No it isn't really the Visitor pattern.  No it probably isn't necessary.
	 *
	 * @param sessionVisitor Does something to each session.
	 * @see SessionVisitor#visitSession(SessionID, Session)
	 */
	protected void visitSessions(SessionVisitor sessionVisitor) {
		for (Entry<SessionID, Session> entry : getSessionBySessionIdMap().entrySet()) {
			SessionID sessionId = entry.getKey();
			Session session = entry.getValue();
			if (sessionId != null && session != null) {
				sessionVisitor.visitSession(sessionId, session);
			}
		}
	}


	protected void validateProperties() {
		if (getApplication() == null) {
			throw new BeanCreationException("Must specify an application");
		}
		if (getLogFactory() == null) {
			setLogFactory(new SLF4JLogFactory(getAggregatedSessionSettings()));
		}
		if (getMessageStoreFactory() == null) {
			setMessageStoreFactory(new MemoryStoreFactory());
		}
	}


	protected void configureSessionSettings() {
		// set the session setting on object that need them
		if (getLogFactory() instanceof QuickFixRequiresSettings) {
			((QuickFixRequiresSettings) getLogFactory()).setSessionSettings(getAggregatedSessionSettings());
		}
	}


	/**
	 * Clear all caches including ehcache for tables that might have been updated.
	 * <p>
	 * NOTE: This only clears the hibernate 2nd level caches.  It does not clear any custom caches.
	 */
	protected void clearSessionAndDBCaches() {
		// clear the session settings map to ensure the it is reloaded
		setSessionSettings(null);

		getCacheHandler().clear(FixSession.class.getName());
		getCacheHandler().clear(FixSessionDefinition.class.getName());
		getCacheHandler().clear(FixSessionDefinitionSetting.class.getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<SessionID, Session> getSessionBySessionIdMap() {
		return this.sessionBySessionIdMapLazy.get();
	}


	protected void setSessionSettings(SessionSettings sessionSettings) {
		this.sessionSettings = sessionSettings;
	}


	/**
	 * The session setting for all sessions.
	 */
	protected SessionSettings getAggregatedSessionSettings() {
		if (this.sessionSettings == null) {
			this.sessionSettings = loadAggregatedSessionSettings();
		}
		return this.sessionSettings;
	}


	private SessionSettings loadAggregatedSessionSettings() {
		SessionSettings result = new SessionSettings();

		List<FixSessionDefinition> definitionList = getFixSessionService().getFixSessionDefinitionListActiveAndPopulated();
		for (FixSessionDefinition definition : CollectionUtils.getIterable(definitionList)) {
			ValidationUtils.assertEquals(getServerEnvironment(), definition.getServerEnvironment(),
					"Cannot start FIX session(s) because session [" + definition.getName() + "] has a [" + definition.getServerEnvironment() + "] but the current environment is ["
							+ getServerEnvironment() + "].");
			if (!definition.isCreateSession()) {
				continue;
			}
			Map<String, String> defaults = null;
			if (definition.getParent() != null) {
				defaults = buildSettings(definition.getParent(), null);
			}

			FixSession session = getFixSessionService().getFixSessionCurrentByDefinition(definition);
			// if no session exists, create one and put it in the cache
			if (session == null) {
				session = new FixSession();
				session.setDefinition(definition);
				session.setCreationDateAndTime(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
				session.setIncomingSequenceNumber(1);
				session.setOutgoingSequenceNumber(1);
				session.setActiveSession(true);
				session = getFixSessionService().saveFixSession(session);
			}
			SessionID sessionId = QuickFixSessionUtils.getSessionID(definition);
			getFixSessionCache().put(sessionId, session);
			try {
				Map<String, String> settings = buildSettings(definition, defaults);
				@SuppressWarnings("unchecked") // Address library limitations: Settings always require strings for keys and values
				Map<Object, Object> objectSettings = (Map<Object, Object>) (Map<?, ?>) settings;
				result.set(sessionId, new Dictionary(null, objectSettings));
			}
			catch (Throwable e) {
				throw new RuntimeException("Failed to create settings.", e);
			}
		}

		return result;
	}


	private Map<String, String> buildSettings(FixSessionDefinition sessionDefinition, Map<String, String> defaults) {
		// Apply defaults
		Map<String, String> result = new HashMap<>();
		if (defaults != null) {
			result.putAll(defaults);
		}

		// Apply settings from configurers
		for (FixSessionSettingsConfigurer configurer : getSessionSettingsConfigurerList()) {
			configurer.configure(sessionDefinition, result);
		}

		// Apply customized settings
		for (FixSessionDefinitionSetting setting : CollectionUtils.getIterable(sessionDefinition.getSettingList())) {
			String settingName = setting.getSettingName();
			String settingValue = setting.getSettingValue();
			if (SSLSupport.SETTING_KEY_STORE_NAME.equals(settingName)) {
				settingValue = FileUtils.combinePaths(getCertificatePath(), settingValue);
			}
			result.put(settingName, settingValue);
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected interface SessionVisitor {

		public void visitSession(SessionID sessionId, Session session);
	}


	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public FixSessionCache<SessionID> getFixSessionCache() {
		return this.fixSessionCache;
	}


	public void setFixSessionCache(FixSessionCache<SessionID> fixSessionCache) {
		this.fixSessionCache = fixSessionCache;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public List<FixSessionSettingsConfigurer> getSessionSettingsConfigurerList() {
		return this.sessionSettingsConfigurerList;
	}


	public void setSessionSettingsConfigurerList(List<FixSessionSettingsConfigurer> sessionSettingsConfigurerList) {
		this.sessionSettingsConfigurerList = sessionSettingsConfigurerList;
	}


	public Application getApplication() {
		return this.application;
	}


	public void setApplication(Application application) {
		this.application = application;
	}


	public SessionSettings getSessionSettings() {
		return this.sessionSettings;
	}


	public MessageStoreFactory getMessageStoreFactory() {
		return this.messageStoreFactory;
	}


	public void setMessageStoreFactory(MessageStoreFactory messageStoreFactory) {
		this.messageStoreFactory = messageStoreFactory;
	}


	public LogFactory getLogFactory() {
		return this.logFactory;
	}


	public void setLogFactory(LogFactory logFactory) {
		this.logFactory = logFactory;
	}


	public String getServerEnvironment() {
		return this.serverEnvironment;
	}


	public void setServerEnvironment(String serverEnvironment) {
		this.serverEnvironment = serverEnvironment;
	}


	public String getCertificatePath() {
		return this.certificatePath;
	}


	public void setCertificatePath(String certificatePath) {
		this.certificatePath = certificatePath;
	}


	public Map<SessionID, Process> getExternalProcessList() {
		return this.externalProcessList;
	}


	public Lazy<Map<SessionID, Session>> getSessionBySessionIdMapLazy() {
		return this.sessionBySessionIdMapLazy;
	}
}
