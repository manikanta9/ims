package com.clifton.fix.quickfix.server.tag;


import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.cache.FixSessionCache;
import com.clifton.fix.tag.FixTagModifierFilter;
import com.clifton.fix.tag.FixTagModifierHandler;
import com.clifton.fix.tag.FixTagModifierService;
import com.clifton.fix.tag.bean.FixTagModifierBean;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;
import quickfix.DataDictionary;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.Message;
import quickfix.SessionID;
import quickfix.StringField;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>QuickFixTagModifierHandler</code> implements message tag modifiers for QuickFIX.
 *
 * @author mwacker
 */
@Component("fixTagModifierHandler")
public class QuickFixTagModifierHandler implements FixTagModifierHandler<Message> {

	private FixTagModifierService fixTagModifierService;

	/**
	 * The FIX session cache used to find the FixSession objects based on the quickfix.SessionID.
	 */
	private FixSessionCache<SessionID> fixSessionCache;


	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The Fix tag modifier filter cache
	 */
	@SuppressWarnings({"unchecked"})
	@Override
	public void applyTagModifiers(Message message, String sessionQualifier, Map<String, String> additionalParams) {
		//TOBE USED in FUTURE
		//Map<String, String> params = Optional.ofNullable(additionalParams).orElseGet(Collections::emptyMap);
		//TOBE USED in FUTURE  PART OF  FRAMEWORK

		SessionID sessionId = QuickFixMessageUtils.getSessionID(message, sessionQualifier);
		FixSession fixSession = getFixSessionCache().get(sessionId);
		DataDictionary dd = QuickFixMessageUtils.getDataDictionary(message);
		List<SystemBean> tagModifierList = getFixTagModifierService().getFixTagModifierListBySession(fixSession);

		for (SystemBean modifier : CollectionUtils.getIterable(tagModifierList)) {
			List<FixTagModifierFilter> tagModifierFilterList = getFixTagModifierService().getFixTagModifierFilterListBySessionAndModifier(fixSession, modifier);

			boolean valueIsEqual = false;
			for (FixTagModifierFilter modifierFilter : CollectionUtils.getIterable(tagModifierFilterList)) {
				Set<String> fieldValues = new HashSet<>();
				try {
					if (dd.isHeaderField(modifierFilter.getFieldTag())) {
						fieldValues.add(message.getHeader().getField(new StringField(modifierFilter.getFieldTag())).getValue());
					}
					else if (message.isSetField(modifierFilter.getFieldTag())) {
						fieldValues.add(message.getField(new StringField(modifierFilter.getFieldTag())).getValue());
					}
					else {
						getValuesRecursive(message, modifierFilter.getFieldTag(), fieldValues);
					}
					if (fieldValues.isEmpty()) {
						throw new FieldNotFound(modifierFilter.getFieldTag());
					}
					// TODO: Implement include/exclude then this next logic will have to change
					// use pattern to maintain support for values such as [D|G]
					Pattern p = Pattern.compile(modifierFilter.getFieldValue());
					boolean found = fieldValues.stream().anyMatch(v -> {
						Matcher matcher = p.matcher(v);
						return matcher.find();
					});
					if (!found) {
						valueIsEqual = false;
						break;
					}
					valueIsEqual = true;
				}
				catch (FieldNotFound e) {
					// field not found - the value cannot be compared, so set valueIsEqual to false and break
					valueIsEqual = false;
					break;
				}
			}

			if (valueIsEqual) {
				try {
					FixTagModifierBean<Message> modifierInstance = (FixTagModifierBean<Message>) getSystemBeanService().getBeanInstance(modifier);
					modifierInstance.execute(message);
				}
				catch (Exception exc) {
					throw new RuntimeException("Error executing tag modifier action for tag modifier \"" + modifier.getLabel() + "\":" + exc.getMessage(), exc);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void getValuesRecursive(FieldMap fieldMap, int fieldNumber, Set<String> values) throws FieldNotFound {
		Iterator<Integer> groups = fieldMap.groupKeyIterator();
		while (groups.hasNext()) {
			List<Group> groupList = fieldMap.getGroups(groups.next());
			for (Group grp : groupList) {
				getValuesRecursive(grp, fieldNumber, values);
			}
		}
		if (fieldMap.isSetField(fieldNumber)) {
			values.add(fieldMap.getField(new StringField(fieldNumber)).getValue());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixTagModifierService getFixTagModifierService() {
		return this.fixTagModifierService;
	}


	public void setFixTagModifierService(FixTagModifierService fixTagModifierService) {
		this.fixTagModifierService = fixTagModifierService;
	}


	public FixSessionCache<SessionID> getFixSessionCache() {
		return this.fixSessionCache;
	}


	public void setFixSessionCache(FixSessionCache<SessionID> fixSessionCache) {
		this.fixSessionCache = fixSessionCache;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
