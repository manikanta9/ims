package com.clifton.fix.quickfix.server.log.logger;

import com.clifton.core.logging.LogLevels;
import com.clifton.fix.quickfix.server.log.AbstractQuickFixLogFactory;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import quickfix.Log;
import quickfix.LogFactory;
import quickfix.SessionID;


/**
 * The {@link QuickFixLoggerLogFactory} is a {@link LogFactory} implementation which produces {@link Log} instances which use the application logger.
 *
 * @author MikeH
 * @see QuickFixLoggerLog
 */
public class QuickFixLoggerLogFactory extends AbstractQuickFixLogFactory {

	private static final String SETTING_LOG_HEARTBEATS = "LoggerLogHeartbeats";
	private static final String SETTING_LOG_NON_PERSISTENT_MESSAGES = "LoggerLogNonPersistentMessages";

	private LogLevels defaultLogLevel;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Log doCreate(SessionID sessionID) {
		QuickFixLoggerLog loggerLog = new QuickFixLoggerLog(sessionID, getDefaultLogLevel());
		loggerLog.setLogHeartbeats(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, SETTING_LOG_HEARTBEATS, false));
		loggerLog.setLogNonPersistentMessages(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, SETTING_LOG_NON_PERSISTENT_MESSAGES, false));
		return loggerLog;
	}


	@Override
	public boolean isAutowire() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LogLevels getDefaultLogLevel() {
		return this.defaultLogLevel;
	}


	public void setDefaultLogLevel(LogLevels defaultLogLevel) {
		this.defaultLogLevel = defaultLogLevel;
	}
}
