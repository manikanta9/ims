package com.clifton.fix.quickfix.server.log.logger;

import com.clifton.core.logging.LogLevels;
import com.clifton.core.logging.LogUtils;
import com.clifton.fix.quickfix.server.log.AbstractQuickFixLog;
import quickfix.FileUtil;
import quickfix.SLF4JLog;
import quickfix.SessionID;


/**
 * The {@link QuickFixLoggerLog} logs contents by use of the application logger.
 * <p>
 * Before executing log statements, this log populates the underlying log implementation's <i>mapped diagnostic context</i> (MDC), if supported, with the session base file name
 * ({@link #MDC_FIX_SESSION_FILE_NAME_PROPERTY}) and the message type ({@link #MDC_FIX_SESSION_MESSAGE_TYPE_PROPERTY}). These can be used to manipulate logged data or to map logged
 * statements to per-session or per-message-type appenders, such as <code>MyEmsxSession.event-log.txt</code> or <code>MyHtgmSession.message-log.txt</code>. Such configurations are
 * managed within the logging configuration layer.
 * <p>
 * This adds to the functionality of the QuickFIX/J built-in {@link SLF4JLog} by incorporating {@link AbstractQuickFixLog} functionality, allowing for configurable log levels, and
 * adding MDC properties.
 *
 * @author MikeH
 * @see SLF4JLog
 */
public class QuickFixLoggerLog extends AbstractQuickFixLog {

	/**
	 * The mapped diagnostic context property name for the filesystem-friendly representation of the FIX session ID.
	 */
	private static final String MDC_FIX_SESSION_FILE_NAME_PROPERTY = "fixSessionFileName";
	/**
	 * The mapped diagnostic context property name for the logged message type ({@link #LOG_TYPE_EVENT} or {@link #LOG_TYPE_MESSAGE}).
	 */
	private static final String MDC_FIX_SESSION_MESSAGE_TYPE_PROPERTY = "fixSessionMessageType";
	private static final String LOG_TYPE_EVENT = "event";
	private static final String LOG_TYPE_MESSAGE = "message";

	private final String sessionFileName;
	private final String logPrefix;
	private final LogLevels defaultLogLevel;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public QuickFixLoggerLog(SessionID sessionID, LogLevels defaultLogLevel) {
		this.sessionFileName = FileUtil.sessionIdFileName(sessionID);
		this.logPrefix = sessionID + ": ";
		this.defaultLogLevel = defaultLogLevel;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(String text) {
		doLog("EVT: " + text, LOG_TYPE_EVENT, getDefaultLogLevel());
	}


	@Override
	public void onErrorEvent(String text) {
		doLog("ERR: " + text, LOG_TYPE_EVENT, LogLevels.ERROR);
	}


	@Override
	protected void logIncoming(String message) {
		doLog("INC: " + message, LOG_TYPE_MESSAGE, getDefaultLogLevel());
	}


	@Override
	protected void logOutgoing(String message) {
		doLog("OUT: " + message, LOG_TYPE_MESSAGE, getDefaultLogLevel());
	}


	@Override
	public void clear() {
		onEvent("The clear operation is not supported for loggers of type " + getClass().getName() + ".");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void doLog(String message, String messageType, LogLevels logLevel) {
		LogUtils.addContextProperty(MDC_FIX_SESSION_FILE_NAME_PROPERTY, getSessionFileName());
		LogUtils.addContextProperty(MDC_FIX_SESSION_MESSAGE_TYPE_PROPERTY, messageType);
		LogUtils.log(getClass(), logLevel, getLogPrefix() + message);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSessionFileName() {
		return this.sessionFileName;
	}


	public String getLogPrefix() {
		return this.logPrefix;
	}


	public LogLevels getDefaultLogLevel() {
		return this.defaultLogLevel;
	}
}
