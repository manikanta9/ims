package com.clifton.fix.quickfix.server.log.screen;


import com.clifton.fix.quickfix.server.log.AbstractQuickFixLogFactory;
import quickfix.Log;
import quickfix.ScreenLogFactory;
import quickfix.SessionID;


/**
 * The {@link QuickFixScreenLogFactory} proxies QuickFIX {@link Log}-creation to the {@link ScreenLogFactory}. This implementation adds additional features, such as the ability to
 * {@link #enabled enable or disable} the log factory.
 *
 * @author mwacker
 * @see ScreenLogFactory
 */
public class QuickFixScreenLogFactory extends AbstractQuickFixLogFactory {

	private boolean logIncoming;
	private boolean logOutgoing;
	private boolean logEvents;
	private boolean logHeartbeats;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Log doCreate(SessionID sessionID) {
		ScreenLogFactory screenLogFactory = new ScreenLogFactory(isLogIncoming(), isLogOutgoing(), isLogEvents(), isLogHeartbeats());
		return screenLogFactory.create(sessionID);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLogIncoming() {
		return this.logIncoming;
	}


	public void setLogIncoming(boolean logIncoming) {
		this.logIncoming = logIncoming;
	}


	public boolean isLogOutgoing() {
		return this.logOutgoing;
	}


	public void setLogOutgoing(boolean logOutgoing) {
		this.logOutgoing = logOutgoing;
	}


	public boolean isLogEvents() {
		return this.logEvents;
	}


	public void setLogEvents(boolean logEvents) {
		this.logEvents = logEvents;
	}


	public boolean isLogHeartbeats() {
		return this.logHeartbeats;
	}


	public void setLogHeartbeats(boolean logHeartbeats) {
		this.logHeartbeats = logHeartbeats;
	}
}
