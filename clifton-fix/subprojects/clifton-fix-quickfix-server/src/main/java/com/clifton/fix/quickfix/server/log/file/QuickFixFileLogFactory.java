package com.clifton.fix.quickfix.server.log.file;


import com.clifton.core.util.StringUtils;
import com.clifton.fix.quickfix.server.log.AbstractQuickFixLogFactory;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import quickfix.FileLogFactory;
import quickfix.Log;
import quickfix.RuntimeError;
import quickfix.SessionID;


/**
 * The <code>QuickfixFileLogFactory</code> copies functionality from FileLogFactory in but returns
 * the rollover file log instead of the quickfix version.
 *
 * @author mwacker
 * @see QuickFixRolloverFileLog
 * @see FileLogFactory
 */

public class QuickFixFileLogFactory extends AbstractQuickFixLogFactory {

	public static final String SETTING_LOG_NON_PERSISTENT_MESSAGES = "FileLogNonPersistentMessages";
	public static final String SETTING_LOG_BUFFER_SIZE_BYTES = "FileLogBufferSizeBytes";

	private String fileLogPath;
	private int maximumFileSizeBytes;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Log doCreate(SessionID sessionID) {
		// see quickfix.LogFileFactory
		try {
			QuickFixRolloverFileLogConfig logConfig = new QuickFixRolloverFileLogConfig();
			logConfig.setMaximumFileSizeBytes(getMaximumFileSizeBytes());
			logConfig.setBufferSizeBytes(QuickFixSessionUtils.getSessionIntOrDefault(getSessionSettings(), sessionID, SETTING_LOG_BUFFER_SIZE_BYTES, null));
			logConfig.setIncludeMillis(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, FileLogFactory.SETTING_INCLUDE_MILLIS_IN_TIMESTAMP, false));
			logConfig.setIncludeTimestampForMessages(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, FileLogFactory.SETTING_INCLUDE_TIMESTAMP_FOR_MESSAGES, false));
			if (!StringUtils.isEmpty(getFileLogPath())) {
				getSessionSettings().setString(FileLogFactory.SETTING_FILE_LOG_PATH, getFileLogPath());
				logConfig.setLogPath(getFileLogPath());
			}
			QuickFixRolloverFileLog log = new QuickFixRolloverFileLog(sessionID, logConfig);
			log.setLogHeartbeats(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, FileLogFactory.SETTING_LOG_HEARTBEATS, false));
			log.setLogNonPersistentMessages(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, SETTING_LOG_NON_PERSISTENT_MESSAGES, false));
			return log;
		}
		catch (Exception e) {
			throw new RuntimeError("An error occurred while generating the logger.", e);
		}
	}


	@Override
	protected boolean isAutowire() {
		// The generated logger requires injected services
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFileLogPath() {
		return this.fileLogPath;
	}


	public void setFileLogPath(String fileLogPath) {
		this.fileLogPath = fileLogPath;
	}


	public int getMaximumFileSizeBytes() {
		return this.maximumFileSizeBytes;
	}


	public void setMaximumFileSizeBytes(int maximumFileSizeBytes) {
		this.maximumFileSizeBytes = maximumFileSizeBytes;
	}
}
