package com.clifton.fix.quickfix.server.session.management;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ThreadUtils;
import com.clifton.core.util.jmx.JmxUtils;
import com.clifton.core.util.jmx.MBeanWrapper;
import com.clifton.fix.quickfix.server.AbstractFixServerExecutor;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.management.FixSessionManagementService;
import com.clifton.fix.session.management.FixSessionQueueStatus;
import com.clifton.fix.session.management.FixSessionRunStatus;
import com.clifton.fix.session.management.FixSessionStatus;
import org.springframework.stereotype.Service;
import quickfix.Initiator;
import quickfix.SessionID;
import quickfix.ThreadedSocketInitiator;
import quickfix.mina.EventHandlingStrategy;
import quickfix.mina.ThreadPerSessionEventHandlingStrategy;

import javax.management.ObjectName;
import java.lang.management.ThreadInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;


/**
 * The {@link QuickFixSessionManagementServiceImpl} provides the default {@link FixSessionManagementService} implementation for use when monitoring application state with the
 * QuickFIX/J library.
 *
 * @author MikeH
 */
@Service("fixSessionManagementService")
public class QuickFixSessionManagementServiceImpl implements FixSessionManagementService {

	private static final String QUICKFIX_MBEAN_DOMAIN = "org.quickfixj";
	private static final String QUICKFIX_MBEAN_CONNECTOR_TYPE = "Connector";
	private static final String QUICKFIX_MBEAN_SESSION_TYPE = "Session";
	private static final String QUICKFIX_MBEAN_SETTINGS_TYPE = "Settings";
	private static final String QUICKFIX_SESSION_DISPATCHER_THREAD_PREFIX = "QF/J Session dispatcher: ";

	private AbstractFixServerExecutor fixServerExecutor;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixSessionStatus getFixSessionStatus() {
		List<MBeanWrapper> connectorStatusList = getMBeanWrapperList(QUICKFIX_MBEAN_CONNECTOR_TYPE);
		List<MBeanWrapper> sessionStatusList = getMBeanWrapperList(QUICKFIX_MBEAN_SESSION_TYPE);
		List<MBeanWrapper> settingsStatusList = getMBeanWrapperList(QUICKFIX_MBEAN_SETTINGS_TYPE);
		String sessionDispatcherThreadDump = getFixSessionDispatcherThreadInfoList().stream()
				.map(ThreadUtils::getThreadDump)
				.collect(Collectors.joining("\n\n"));
		List<FixSessionQueueStatus> sessionQueueStatusList = new ArrayList<>();
		for (MBeanWrapper sessionWrapper : sessionStatusList) {
			try {
				sessionQueueStatusList.add(getFixSessionQueueStatus(sessionWrapper));
			}
			catch (Exception e) {
				LogUtils.warn(getClass(), String.format("Unable to determine FIX session queue usage statistics for MBean [%s].", sessionWrapper.getObjectName()), e);
			}
		}
		return new FixSessionStatus(sessionStatusList, settingsStatusList, connectorStatusList, sessionQueueStatusList, sessionDispatcherThreadDump);
	}


	@Override
	public FixSessionRunStatus getFixSessionRunStatus(FixSession fixSession) {
		SessionID sessionId = QuickFixSessionUtils.getSessionID(fixSession.getDefinition());
		MBeanWrapper sessionMBeanWrapper = getSessionMBeanWrapper(QUICKFIX_MBEAN_SESSION_TYPE, sessionId);
		return new FixSessionRunStatus(sessionMBeanWrapper);
	}


	@Override
	public List<ThreadInfo> getFixSessionDispatcherThreadInfoList() {
		return Arrays.stream(ThreadUtils.getAllThreadInfo(100))
				.filter(ti -> ti.getThreadName().startsWith(QUICKFIX_SESSION_DISPATCHER_THREAD_PREFIX))
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixSessionQueueStatus getFixSessionQueueStatus(MBeanWrapper sessionWrapper) {
		String sessionIdStr = (String) sessionWrapper.getAttributeMap().get("SessionID").getValue();
		SessionID sessionId = new SessionID(sessionIdStr);
		Initiator initiator = getFixServerExecutor().getInitiator(sessionId);
		if (!(initiator instanceof ThreadedSocketInitiator)) {
			return null;
		}
		EventHandlingStrategy eventHandlingStrategy = BeanUtils.getFieldValue(initiator, "eventHandlingStrategy");
		if (!(eventHandlingStrategy instanceof ThreadPerSessionEventHandlingStrategy)) {
			return null;
		}
		Map<?, ?> dispatcherMap = BeanUtils.getFieldValue(eventHandlingStrategy, "dispatchers");
		List<BlockingQueue<?>> messageQueueList = CollectionUtils.getConverted(dispatcherMap.values(), dispatcher -> BeanUtils.getFieldValue(dispatcher, "messages"));
		int size = messageQueueList.stream().mapToInt(Collection::size).sum();
		int capacity = size + messageQueueList.stream().mapToInt(BlockingQueue::remainingCapacity).sum();
		return new FixSessionQueueStatus(sessionIdStr, size, capacity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the list of all QuickFIX/J {@link MBeanWrapper} objects of the given type, such as {@value #QUICKFIX_MBEAN_SESSION_TYPE} or {@value QUICKFIX_MBEAN_CONNECTOR_TYPE}.
	 */
	private List<MBeanWrapper> getMBeanWrapperList(String type) {
		Collection<ObjectName> objectNameList = JmxUtils.queryMBeans(QUICKFIX_MBEAN_DOMAIN, MapUtils.of("type", type));
		return CollectionUtils.getConverted(objectNameList, JmxUtils::getMBeanWrapperNormalized);
	}


	/**
	 * Retrieves the QuickFIX/J {@link MBeanWrapper} object of the given type for the provided {@link SessionID}. This can be use to retrieve JMX-based information for a specific
	 * session.
	 */
	private MBeanWrapper getSessionMBeanWrapper(String type, SessionID sessionId) {
		// Object name construction is based on org.quickfixj.jmx.mbean.session.SessionJmxExporter#addSessionIdProperties(SessionID, ObjectNameFactory)
		Map<String, String> queryParams = new LinkedHashMap<>(); // Order must be retained
		queryParams.put("type", type);
		queryParams.put("beginString", sessionId.getBeginString());
		queryParams.put("senderCompID", sessionId.getSenderCompID());
		queryParams.put("targetCompID", sessionId.getTargetCompID());
		queryParams.put("senderSubID", sessionId.getSenderSubID());
		queryParams.put("senderLocationID", sessionId.getSenderLocationID());
		queryParams.put("targetSubID", sessionId.getTargetSubID());
		queryParams.put("targetLocationID", sessionId.getTargetLocationID());
		queryParams.put("qualifier", sessionId.getSessionQualifier());
		// Exclude empty params
		Map<String, String> filteredQueryParams = queryParams.entrySet().stream()
				.filter(entry -> !StringUtils.isEmpty(entry.getValue()))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, CollectionUtils.throwingMerger(), LinkedHashMap::new));
		return JmxUtils.isMBeanRegistered(QUICKFIX_MBEAN_DOMAIN, filteredQueryParams)
				? JmxUtils.getMBeanWrapperNormalized(QUICKFIX_MBEAN_DOMAIN, filteredQueryParams)
				: null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractFixServerExecutor getFixServerExecutor() {
		return this.fixServerExecutor;
	}


	public void setFixServerExecutor(AbstractFixServerExecutor fixServerExecutor) {
		this.fixServerExecutor = fixServerExecutor;
	}
}
