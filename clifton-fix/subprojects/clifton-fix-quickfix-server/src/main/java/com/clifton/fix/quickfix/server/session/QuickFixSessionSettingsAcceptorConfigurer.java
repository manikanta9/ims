package com.clifton.fix.quickfix.server.session;

import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionSettingsConfigurer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import quickfix.Acceptor;
import quickfix.Session;
import quickfix.SessionFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


/**
 * The {@link QuickFixSessionSettingsAcceptorConfigurer} configures {@link Session} settings for {@link Acceptor} sessions.
 *
 * <b>Note: Support for {@link Acceptor} sessions is incomplete.</b>
 *
 * @author MikeH
 */
@Component
@Order(100) // Execute after the standard configurer so that the connection type setting is defined
public class QuickFixSessionSettingsAcceptorConfigurer implements FixSessionSettingsConfigurer {

	@Override
	public void configure(FixSessionDefinition sessionDefinition, Map<String, String> sessionSettings) {
		if (!Objects.equals(SessionFactory.ACCEPTOR_CONNECTION_TYPE, sessionSettings.get(SessionFactory.SETTING_CONNECTION_TYPE))) {
			return;
		}
		sessionSettings.putAll(getAcceptorSettings(sessionDefinition));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, String> getAcceptorSettings(FixSessionDefinition sessionDefinition) {
		Map<String, String> settings = new HashMap<>();
		// No settings configured
		return settings;
	}
}
