package com.clifton.fix.quickfix.server.log.file;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.fix.quickfix.server.log.AbstractQuickFixLog;
import quickfix.FileUtil;
import quickfix.Log;
import quickfix.SessionID;
import quickfix.SystemTime;
import quickfix.field.converter.UtcTimestampConverter;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;


/**
 * The {@link QuickFixRolloverFileLog} is a {@link Log} implementation which writes message to files. This is based on the QuickFIX/J built-in {@link quickfix.FileLog} type, but
 * adds the base {@link AbstractQuickFixLog} functionality as well as file-rollover capabilities, similar to what is provided in many logging frameworks.
 */
public class QuickFixRolloverFileLog extends AbstractQuickFixLog {

	private static final String TIME_STAMP_DELIMITER = ": ";

	private final int maximumFileSizeBytes;
	private final Integer bufferSizeBytes;
	private final boolean includeMillis;
	private final boolean includeTimestampForMessages;

	private final String messagesFileName;
	private final String eventFileName;
	private boolean syncAfterWrite;
	private Writer messageWriter;
	private Writer eventWriter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public QuickFixRolloverFileLog(SessionID sessionID, QuickFixRolloverFileLogConfig config) {
		AssertUtils.assertNotNull(sessionID, "SessionID cannot be null.");
		AssertUtils.assertNotEmpty(config.getLogPath(), "Log path cannot be empty.");

		this.maximumFileSizeBytes = config.getMaximumFileSizeBytes();
		this.bufferSizeBytes = config.getBufferSizeBytes();
		this.includeMillis = config.isIncludeMillis();
		this.includeTimestampForMessages = config.isIncludeTimestampForMessages();

		String sessionName = FileUtil.sessionIdFileName(sessionID);
		String prefix = FileUtil.fileAppendPath(config.getLogPath(), sessionName + ".");
		this.messagesFileName = prefix + "messages.log";
		this.eventFileName = prefix + "event.log";

		File directory = new File(getMessagesFileName()).getParentFile();
		if (!directory.exists()) {
			directory.mkdirs();
		}

		openLogStreams();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void close() {
		try {
			getMessageWriter().close();
		}
		catch (Exception e) {
			LogUtils.warn(getClass(), "An exception occurred while attempting to close the log message writer.", e);
		}
		try {
			getEventWriter().close();
		}
		catch (Exception e) {
			LogUtils.warn(getClass(), "An exception occurred while attempting to close the log event writer.", e);
		}
	}


	@Override
	public void clear() {
		close();
		openLogStreams();
	}


	@Override
	public void onEvent(String message) {
		writeMessage(getEventWriter(), message, true);
	}


	@Override
	public void onErrorEvent(String message) {
		writeMessage(getEventWriter(), message, true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void openLogStreams() {
		setMessageWriter(new QuickFixRolloverFileWriter(Paths.get(getMessagesFileName()).toAbsolutePath(), StandardCharsets.UTF_8, getMaximumFileSizeBytes(), getBufferSizeBytes(), true, isSyncAfterWrite()));
		setEventWriter(new QuickFixRolloverFileWriter(Paths.get(getEventFileName()).toAbsolutePath(), StandardCharsets.UTF_8, getMaximumFileSizeBytes(), getBufferSizeBytes(), true, isSyncAfterWrite()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void logIncoming(String message) {
		writeMessage(getMessageWriter(), message, false);
	}


	@Override
	protected void logOutgoing(String message) {
		writeMessage(getEventWriter(), message, false);
	}


	private void writeMessage(Writer writer, String message, boolean forceTimestamp) {
		try {
			if (forceTimestamp || isIncludeTimestampForMessages()) {
				writeTimeStamp(writer);
			}
			writer.write(message);
			writer.write("\n");
		}
		catch (IOException e) {
			LogUtils.error(getClass(), "Error writing Fix message to log", e);
		}
	}


	private void writeTimeStamp(Writer writer) {
		String formattedTime = UtcTimestampConverter.convert(SystemTime.getDate(), isIncludeMillis());
		try {
			writer.write(formattedTime);
			writer.write(TIME_STAMP_DELIMITER);
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while writing the timestamp.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setMessageWriter(Writer messageWriter) {
		this.messageWriter = messageWriter;
	}


	private void setEventWriter(Writer eventWriter) {
		this.eventWriter = eventWriter;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getMaximumFileSizeBytes() {
		return this.maximumFileSizeBytes;
	}


	public Integer getBufferSizeBytes() {
		return this.bufferSizeBytes;
	}


	public boolean isIncludeMillis() {
		return this.includeMillis;
	}


	public boolean isIncludeTimestampForMessages() {
		return this.includeTimestampForMessages;
	}


	public String getMessagesFileName() {
		return this.messagesFileName;
	}


	public String getEventFileName() {
		return this.eventFileName;
	}


	public boolean isSyncAfterWrite() {
		return this.syncAfterWrite;
	}


	public void setSyncAfterWrite(boolean syncAfterWrite) {
		this.syncAfterWrite = syncAfterWrite;
	}


	public Writer getMessageWriter() {
		return this.messageWriter;
	}


	public Writer getEventWriter() {
		return this.eventWriter;
	}
}
