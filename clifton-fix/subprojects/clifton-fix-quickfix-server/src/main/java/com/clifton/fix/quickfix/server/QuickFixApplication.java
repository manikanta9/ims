package com.clifton.fix.quickfix.server;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.date.TimeTrackedOperation;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.server.AbstractFixServerMessageRouter;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.cache.FixSessionCache;
import quickfix.Application;
import quickfix.DefaultSessionFactory;
import quickfix.Dictionary;
import quickfix.Message;
import quickfix.Session;
import quickfix.SessionID;
import quickfix.StringField;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>QuickFixApplication</code> handle message to and from the quick fix session.  It implements quickfix.Application to send and receive messages
 * from the quickfix sessions and FixMessageRouter to route the messages to and from the FIX server.
 *
 * @author mwacker
 */
public class QuickFixApplication extends AbstractFixServerMessageRouter<Message> implements Application {

	/**
	 * Defined Pattern for tag values, for the tag=value pairs, e.g. 553=username
	 */
	private static final Pattern TAG_VALUE_PATTERN = Pattern.compile("([0-9]+)=(.*)");

	/**
	 * Session setting for custom heartbeat tags. Single entry or consecutive list of
	 * tag=value pairs, e.g. HeartbeatTag=553=username and HeartbeatTag1=554=FIXMessageGroup.
	 *
	 * @see Session#SETTING_LOGON_TAG
	 */
	public static final String SETTING_HEARTBEAT_TAG = "HeartbeatTag";

	/**
	 * The FIX session cache used to find the FixSession objects based on the quickfix.SessionID.
	 */
	private FixSessionCache<SessionID> fixSessionCache;
	private SystemRequestStatsService systemRequestStatsService;
	private ContextHandler contextHandler;

	/**
	 * Should be set to true in properties file if system stats should be recorded.
	 */
	private boolean recordSendSystemStats;
	private boolean recordAppSystemStats;
	private AbstractFixServerExecutor fixServerExecutor;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void send(Message message, String sessionQualifier) {
		SessionID sessionId = QuickFixMessageUtils.getSessionIDWithOptionalFields(message, sessionQualifier);

		if (isRecordSendSystemStats()) {
			TimeTrackedOperation<Void> sendMessageOp = TimeUtils.getElapsedTime(() -> doSend(sessionId, message, sessionQualifier));

			try {
				FixSession fixSession = getFixSessionCache().get(sessionId);
				Object user = getContextHandler().getBean(Context.USER_BEAN_NAME);
				String uri = fixSession.getDefinition().getName();
				getSystemRequestStatsService().saveSystemRequestStats("FIX-MESSAGE-SEND", uri, user, sendMessageOp.getElapsedNanoseconds(), 0, 0);
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Error occurred saving send system stats in the QuickFixApplication", e);
			}
		}
		else {
			doSend(sessionId, message, sessionQualifier);
		}
	}


	private void doSend(SessionID sessionId, Message message, String sessionQualifier) {
		// look up the session using most specific session id first.
		Session session = Session.lookupSession(sessionId);
		// If no session is found, then use only the required fields
		if (session == null) {
			session = Session.lookupSession(QuickFixMessageUtils.getSessionID(message, sessionQualifier));
		}
		if (session == null) {
			throw new RuntimeException("No session found for message [" + message + "].");
		}
		if (!session.send(message)) {
			throw new RuntimeException("Failed to send message [" + message + "].");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void fromAdmin(Message message, SessionID sessionID) {
		LogUtils.info(getClass(), String.format("SessionID= %s ,  Message=%s", sessionID.toString(), message != null ? message.toString() : ""));
	}


	@Override
	public void fromApp(Message message, SessionID sessionID) {
		try {
			FixSession fixSession = getFixSessionCache().get(sessionID);
			if (isRecordAppSystemStats()) {
				TimeTrackedOperation<Void> fromAppOp = TimeUtils.getElapsedTime(() -> receiveFromSession(message, fixSession));
				Object user = getContextHandler().getBean(Context.USER_BEAN_NAME);
				getSystemRequestStatsService().saveSystemRequestStats("FIX-MESSAGE-RECEIVE", fixSession.getDefinition().getName(), user, fromAppOp.getElapsedNanoseconds(), 0, 0);
			}
			else {
				receiveFromSession(message, fixSession);
			}
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Error occurred in the QuickFixApplication", e);
			// TODO: enable sending Business Rejects
		}
	}

	@Override
	public void onCreate(SessionID sessionID) {
		LogUtils.info(getClass(), sessionID.toString());
	}


	@Override
	public void onLogon(SessionID sessionID) {
		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> String.format("Logged on, " +
						"isFIXT=%s, " +
						"beginString=%s, " +
						"senderCompID=%s, " +
						"senderLocationID=%s, " +
						"senderSubID=%s, " +
						"sessionQualifier=%s, " +
						"targetCompID=%s, " +
						"targetLocationID=%s, " +
						"targetSubID=%s",
				sessionID.isFIXT(),
				sessionID.getBeginString(),
				sessionID.getSenderCompID(),
				sessionID.getSenderLocationID(),
				sessionID.getSenderSubID(),
				sessionID.getSessionQualifier(),
				sessionID.getTargetCompID(),
				sessionID.getTargetLocationID(),
				sessionID.getTargetSubID()))
		);
	}


	@Override
	public void onLogout(SessionID sessionID) {
		// Do nothing
	}


	@Override
	public void toAdmin(Message message, SessionID sessionID) {
		if (QuickFixMessageUtils.isHeartbeat(message)) {
			applyHeartbeatTags(message, sessionID);
		}
	}


	@Override
	public void toApp(Message message, SessionID sessionID) {
		// Do nothing
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the tags with the name HeartbeatTag and respective key value pairs
	 * for a Heartbeat message to identify the origin.
	 * Added to aid in the identification of the machine connected to the session.
	 * <p>
	 * e.g. HeartbeatTag=553=Username: username. Message group: message_group
	 *
	 * @see Session#SETTING_LOGON_TAG
	 * @see Session#setLogonTags
	 * @see DefaultSessionFactory#getLogonTags
	 */

	private void applyHeartbeatTags(Message message, SessionID sessionID) {
		try {
			Dictionary sessionSettings = getFixServerExecutor().getAggregatedSessionSettings().get(sessionID);
			for (int i = 0; i <= 10; i++) {
				String settingName = SETTING_HEARTBEAT_TAG + ((i != 0) ? i : "");
				if (!sessionSettings.has(settingName)) {
					break;
				}
				String settingValue = sessionSettings.getString(settingName);

				Matcher tagMatcher = TAG_VALUE_PATTERN.matcher(settingValue);

				if (!tagMatcher.matches()) {
					LogUtils.warn(getClass(), String.format("[%s] The session definition setting for key [%s] and value [%s] does not match the expected pattern. Values must match the pattern [%s]",
							sessionID.toString(), settingName, settingValue, TAG_VALUE_PATTERN.pattern()));
				}
				else {
					message.getHeader().setField(new StringField(Integer.parseInt(tagMatcher.group(1)), tagMatcher.group(2)));
				}
			}
		}
		catch (Exception e) {
			LogUtils.warn(getClass(), String.format("An exception occurred while applying heartbeat tags in sessionID: [%s]", sessionID.toString()), e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionCache<SessionID> getFixSessionCache() {
		return this.fixSessionCache;
	}


	public void setFixSessionCache(FixSessionCache<SessionID> fixSessionCache) {
		this.fixSessionCache = fixSessionCache;
	}


	public AbstractFixServerExecutor getFixServerExecutor() {
		return this.fixServerExecutor;
	}


	public void setFixServerExecutor(AbstractFixServerExecutor fixServerExecutor) {
		this.fixServerExecutor = fixServerExecutor;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public boolean isRecordSendSystemStats() {
		return this.recordSendSystemStats;
	}


	public void setRecordSendSystemStats(boolean recordSendSystemStats) {
		this.recordSendSystemStats = recordSendSystemStats;
	}


	public boolean isRecordAppSystemStats() {
		return this.recordAppSystemStats;
	}


	public void setRecordAppSystemStats(boolean recordAppSystemStats) {
		this.recordAppSystemStats = recordAppSystemStats;
	}


}
