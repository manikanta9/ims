package com.clifton.fix.quickfix.server.log.file;


/**
 * The {@link QuickFixRolloverFileLogConfig} is a configuration file used when generating {@link QuickFixRolloverFileLog} instances.
 *
 * @author MikeH
 * @see QuickFixRolloverFileLog
 */
public class QuickFixRolloverFileLogConfig {

	private String logPath;
	private int maximumFileSizeBytes;
	private Integer bufferSizeBytes;
	private boolean includeMillis;
	private boolean includeTimestampForMessages;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLogPath() {
		return this.logPath;
	}


	public void setLogPath(String logPath) {
		this.logPath = logPath;
	}


	public int getMaximumFileSizeBytes() {
		return this.maximumFileSizeBytes;
	}


	public void setMaximumFileSizeBytes(int maximumFileSizeBytes) {
		this.maximumFileSizeBytes = maximumFileSizeBytes;
	}


	public Integer getBufferSizeBytes() {
		return this.bufferSizeBytes;
	}


	public void setBufferSizeBytes(Integer bufferSizeBytes) {
		this.bufferSizeBytes = bufferSizeBytes;
	}


	public boolean isIncludeMillis() {
		return this.includeMillis;
	}


	public void setIncludeMillis(boolean includeMillis) {
		this.includeMillis = includeMillis;
	}


	public boolean isIncludeTimestampForMessages() {
		return this.includeTimestampForMessages;
	}


	public void setIncludeTimestampForMessages(boolean includeTimestampForMessages) {
		this.includeTimestampForMessages = includeTimestampForMessages;
	}
}
