package com.clifton.fix.quickfix.server.store;


import com.clifton.core.context.ApplicationContextService;
import quickfix.MessageStore;
import quickfix.MessageStoreFactory;
import quickfix.SessionID;


/**
 * The <code>QuickFixStoreFactory</code> message store factory used
 * to create custom message store <code>QuickFixStore</code>.
 *
 * @author mwacker
 */
public class QuickFixStoreFactory implements MessageStoreFactory {

	private ApplicationContextService applicationContextService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MessageStore create(SessionID sessionId) {
		try {
			QuickFixStore result = new QuickFixStore(sessionId);
			getApplicationContextService().autowireBean(result);
			result.init();
			return result;
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
