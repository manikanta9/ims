package com.clifton.fix.quickfix.server;


import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ThreadUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import com.clifton.fix.server.session.FixSessionExecutorService;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionParameters;
import com.clifton.fix.session.management.FixSessionManagementService;
import org.quickfixj.jmx.JmxExporter;
import quickfix.Connector;
import quickfix.DefaultSessionFactory;
import quickfix.Initiator;
import quickfix.Session;
import quickfix.SessionFactory;
import quickfix.SessionID;
import quickfix.SessionSettings;
import quickfix.ThreadedSocketInitiator;

import javax.annotation.PreDestroy;
import javax.management.ObjectName;
import java.lang.management.ThreadInfo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * The <code>QuickFixExecutor</code> handles the creating, starting and stopping the SocketInitiator
 * that runs the FIX connection.
 * <p>
 * NOTE: This implementation will create 1 Initiator for each session, that means
 * that the sessions can be started and stopped individually.
 * <p>
 * Example configuration:
 * <p>
 * <pre><code>{@literal
 * <bean id="fixExecutor" class="com.clifton.fix.quickfix.server.QuickFixExecutorSingleInitiatorPerSession" init-method="init">
 *     <property name="application" ref="fixMessageRouter" />
 *     <property name="logFactory" ref="fixLogFactory" />
 *     <property name="messageStoreFactory" ref="fixStoreFactory" />
 *     <property name="serverEnvironment" value="${fix.session.serverEnvironment}"/>
 * </bean>
 * }</code></pre>
 *
 * @author mwacker
 */
public class QuickFixExecutorSingleInitiatorPerSession extends AbstractFixServerExecutor {

	private FixSessionManagementService fixSessionManagementService;

	/**
	 * If {@code true}, management beans will be enabled. These are used to provide runtime FIX system diagnostics such as {@link Session} and {@link Connector} details.
	 */
	private boolean enableMBeans;

	/**
	 * The session settings.
	 */
	private Lazy<Map<SessionID, SessionSettings>> sessionSettingsBySessionIdMapLazy;

	/**
	 * The map of {@link SessionID} objects to corresponding {@link SessionConnectorDetail} objects, which contain associated {@link Connector} information for the connectors used
	 * to run the given session.
	 */
	private final ConcurrentMap<SessionID, SessionConnectorDetail> sessionConnectorDetailMap = new ConcurrentHashMap<>();

	/**
	 * The confirmation message that will be used with {@link UserIgnorableValidationException}
	 */
	public static final String FORCE_STOP_CONFIRMATION_MESSAGE = "Would you like to force termination for the session? Some data, such as existing queued messages, may be lost.";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isRunning(FixSessionParameters sessionParameters) {
		SessionID sessionId = QuickFixSessionUtils.getSessionID(sessionParameters);
		return getSessionConnectorDetailMap().containsKey(sessionId);
	}


	@Override
	public void restart(FixSessionParameters sessionParameters) {
		stop(sessionParameters, false);
		start(sessionParameters);
	}


	@Override
	public void restart() {
		stop();
		start();
	}


	@Override
	public void start(FixSessionParameters sessionParameters) {
		SessionID sessionId = QuickFixSessionUtils.getSessionID(sessionParameters);
		FixSession session = getFixSessionCache().get(sessionId);
		prepareForStart();
		try {
			doStart(session.getDefinition(), sessionId);
		}
		catch (ValidationException e) {
			// Rethrow non-critical errors to client
			throw e;
		}
		catch (Exception e) {
			try {
				stop(sessionParameters, false);
			}
			catch (Exception ex) {
				LogUtils.error(getClass(), "Failed to stop application after error on startup for session [" + sessionParameters + "].", ex);
			}
			throw new RuntimeException("Failed to start FIX sessions.", e);
		}
	}


	@Override
	public void start() {
		prepareForStart();

		Map<SessionID, Exception> failedSessions = new HashMap<>();
		List<FixSessionDefinition> sessionList = getFixSessionService().getFixSessionDefinitionListActiveAndPopulated();
		for (FixSessionDefinition sessionDef : CollectionUtils.getIterable(sessionList)) {
			SessionID sessionId = QuickFixSessionUtils.getSessionID(sessionDef);
			try {
				doStart(sessionDef, sessionId);
			}
			catch (ValidationException e) {
				failedSessions.put(sessionId, e);
			}
			catch (Exception e) {
				try {
					stop(sessionDef, false);
				}
				catch (Exception ex) {
					LogUtils.error(getClass(), "Failed to stop session initiator after error starting session [" + sessionDef + "].", ex);
				}
				failedSessions.put(sessionId, e);
			}
		}

		handlerStartStopErrors(failedSessions, true);
	}


	@Override
	public void stop(FixSessionParameters sessionParameters, boolean force) {
		SessionID sessionId = sessionParameters == null ? null : QuickFixSessionUtils.getSessionID(sessionParameters);
		doStop(sessionId, force);
	}


	@Override
	public void stopSessionList(List<String> sessionIdList, boolean force) {
		Map<SessionID, Exception> failedSessions = new HashMap<>();
		sessionIdList.forEach(id -> {
			SessionID sessionID = null;
			try {
				sessionID = new SessionID(id);
				doStop(sessionID, force);
			}
			catch (Exception e) {
				failedSessions.put(sessionID, e);
			}
		});
		handlerStartStopErrors(failedSessions, false);
	}


	@PreDestroy
	@Override
	public void stop() {
		Map<SessionID, Exception> failedSessions = new HashMap<>();

		// copy session ids to a list because we will be editing the map during shutdown
		List<SessionID> idList = new ArrayList<>(getSessionConnectorDetailMap().keySet());

		// stop the initiators
		idList.forEach(sessionID -> {
			try {
				doStop(sessionID, false);
			}
			catch (Exception e) {
				failedSessions.put(sessionID, e);
			}
		});

		// clear the session cache
		getFixSessionCache().clear();

		if (!CollectionUtils.isEmpty(failedSessions)) {
			Map<String, String> parameterMap = new HashMap<>();
			List<String> sessionIdList = new ArrayList<>();
			for (SessionID sessionID : failedSessions.keySet()) {
				sessionIdList.add(sessionID.toString());
			}
			parameterMap.put("force", "true");
			parameterMap.put("sessionIds", StringUtils.collectionToDelimitedString(sessionIdList, ","));
			throw new UserIgnorableValidationException("Failed to stop the following sessions: " + CollectionUtils.toString(sessionIdList, 10))
					.setIgnoreExceptionUrl(FixSessionExecutorService.class, "stopFixSessionList")
					.addParameters(parameterMap)
					.setConfirmationMessage(FORCE_STOP_CONFIRMATION_MESSAGE);
		}
	}


	@Override
	public void init() {
		// Don't fall on application start up, just log the error
		try {
			validateProperties();
			start();
		}
		catch (Exception e) {
			LogUtils.error(getClass(), ExceptionUtils.getOriginalMessage(e), e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Initiator getInitiator(SessionID sessionID) {
		return getSessionConnectorDetailMap().get(sessionID).getInitiator();
	}


	@Override
	public Collection<Initiator> getInitiatorList() {
		return CollectionUtils.getConverted(getSessionConnectorDetailMap().values(), SessionConnectorDetail::getInitiator);
	}


	@Override
	public Collection<SessionID> getSessionIdList() {
		return getSessionConnectorDetailMap().keySet();
	}

	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected ConcurrentMap<SessionID, Session> createSessionMap() {
		ConcurrentMap<SessionID, Session> result = new ConcurrentHashMap<>();
		for (SessionID sessionId : getSessionSettingsBySessionIdMap().keySet()) {
			Session session = Session.lookupSession(sessionId);
			if (session != null) {
				result.put(sessionId, session);
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Clear all caches, settings and configure the factories to make sure we have clean data.
	 */
	private void prepareForStart() {
		clearSessionSettingsBySessionIdMap();
		clearSessionAndDBCaches();
		configureFixFactories();
	}


	private void doStart(FixSessionDefinition sessionDef, SessionID sessionId) {
		ValidationUtils.assertFalse(getSessionConnectorDetailMap().containsKey(sessionId), "Session [" + sessionId + "] is already running.");
		ValidationUtils.assertFalse(sessionDef.isDisabled(), "Cannot start FIX session [" + sessionDef.getName() + "] because it is disabled");

		// get the session settings
		SessionSettings sessionSettings = getSessionSettingsBySessionIdMap().get(sessionId);

		// start the external applications
		startExternalApplicationForSession(sessionDef);

		// create the session factory
		SessionFactory sessionFactory = new DefaultSessionFactory(getApplication(), getMessageStoreFactory(), getLogFactory());

		// create, register and start the socket initiator
		try {
			SessionConnectorDetail sessionManagementDetails = new SessionConnectorDetail(sessionId);
			Initiator init = new ThreadedSocketInitiator(sessionFactory, sessionSettings);
			sessionManagementDetails.setInitiator(init);
			if (isEnableMBeans()) {
				try {
					JmxExporter jmxExporter = new JmxExporter();
					sessionManagementDetails.setJmxExporter(jmxExporter);
					sessionManagementDetails.setConnectorObjectName(jmxExporter.register(init));
				}
				catch (Exception e) {
					LogUtils.warn(LogCommand.ofThrowableAndMessage(getClass(), e, "An exception occurred while attempting to generate the JmxExporter for session [", sessionId, "]. MBeans will not be registered."));
				}
			}
			getSessionConnectorDetailMap().put(sessionId, sessionManagementDetails);
			init.start();
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An exception occurred while attempting to start the FIX session [%s].", sessionDef.getName()), e);
		}

		LogUtils.info(getClass(), "Started " + sessionId);
		System.out.println("Started " + sessionId); // print to the command line
	}


	private void doStop(SessionID sessionId, boolean force) {
		List<String> errorLogList = new ArrayList<>();
		List<String> errorMessageList = new ArrayList<>();

		boolean sessionFound = true;
		try {
			sessionFound = doSessionLogout(sessionId);
		}
		catch (Exception e) {
			if (!force) {
				throw new UserIgnorableValidationException("Error logging out.").setConfirmationMessage(FORCE_STOP_CONFIRMATION_MESSAGE);
			}
			errorLogList.add(String.format("Error logging out: %s", ExceptionUtils.getFullStackTrace(e)));
			errorMessageList.add(String.format("Error logging out: %s", ExceptionUtils.getOriginalMessage(e)));
		}

		boolean initiatorFound = true;
		try {
			initiatorFound = doStopInitiator(force, sessionId);
		}
		catch (Exception e) {
			if (!force) {
				throw new UserIgnorableValidationException("Error stopping initiator.").setConfirmationMessage(FORCE_STOP_CONFIRMATION_MESSAGE);
			}
			errorLogList.add(String.format("Error force stopping initiator: %s", ExceptionUtils.getFullStackTrace(e)));
			errorMessageList.add(String.format("Error force stopping initiator: %s", ExceptionUtils.getOriginalMessage(e)));
		}

		// look for any hanging dispatcher threads
		try {
			doStopDispatcherThreads(force, sessionId);
		}
		catch (Exception e) {
			if (!force) {
				throw new UserIgnorableValidationException("Found session dispatcher threads that were not terminated.").setConfirmationMessage(FORCE_STOP_CONFIRMATION_MESSAGE);
			}
			errorLogList.add(String.format("Error force stopping hanging dispatcher threads: %s", ExceptionUtils.getFullStackTrace(e)));
			errorMessageList.add(String.format("Error force stopping hanging dispatcher threads: %s", ExceptionUtils.getOriginalMessage(e)));
		}

		// Sync All Sequence Number Updates to the Database
		FixSession fixSession = getFixSessionCache().get(sessionId);
		try {
			getFixSessionService().saveFixSessionSequenceNumbersForSession(fixSession.getId());
			getFixSessionCache().remove(sessionId);
		}
		catch (Exception e) {
			if (!force) {
				throw new UserIgnorableValidationException("Error saving sequence numbers.").setConfirmationMessage(FORCE_STOP_CONFIRMATION_MESSAGE);
			}
			errorLogList.add(String.format("Error saving sequence numbers: %s", ExceptionUtils.getFullStackTrace(e)));
			errorMessageList.add(String.format("Error saving sequence numbers: %s", ExceptionUtils.getOriginalMessage(e)));
		}

		// Report errors, if any
		if (!sessionFound && !initiatorFound) {
			String message = String.format("No running session was found for session ID [%s].", sessionId);
			errorLogList.add(message);
			errorMessageList.add(message);
		}
		else if (!sessionFound) {
			String message = String.format("No running session was found for session ID [%s], but a session initiator was still active and has been terminated. An error likely occurred during a previous operation.", sessionId);
			errorLogList.add(message);
			errorMessageList.add(message);
		}
		else if (!initiatorFound) {
			String message = String.format("The running session for session ID [%s] was terminated, but no corresponding session initiator was found. An error likely occurred during a previous operation.", sessionId);
			errorLogList.add(message);
			errorMessageList.add(message);
		}

		if (!CollectionUtils.isEmpty(errorLogList)) {
			String message = errorLogList.stream().collect(Collectors.joining("\n\n-", "Errors were encountered while stopping session:\n\n-", ""));
			LogUtils.error(getClass(), message);
		}

		if (!CollectionUtils.isEmpty(errorMessageList)) {
			String messageShort = errorMessageList.stream().collect(Collectors.joining("\n\n-", "Errors were encountered while stopping session:\n\n-", ""));
			ValidationUtils.fail(messageShort);
		}
	}


	private boolean doSessionLogout(SessionID sessionId) {
		// get the actual fix session
		Session session = Session.lookupSession(sessionId);
		if (session != null) {
			// Log out of the session; the session is not closed here, as the close action is delegated to the initiator later on
			session.logout();
			// check that the session is shutdown
			verifySessionShutdown(sessionId, session);
		}
		return session != null;
	}


	/*
	 * Attempt to stop the initiator in order to retain consistent state, even if no session was found. Stopping the initiator closes all sessions started by the initiator.
	 * Resilience to missing sessions is necessary to handle to case in which the initiator has started but was not able to successfully initiate a session. In these cases, the
	 * initiator still needs to be stopped and removed before a new one can be created for session re-initiation. This typically occurs when an error is encountered during
	 * session configuration.
	 */
	private boolean doStopInitiator(boolean force, SessionID sessionId) {
		SessionConnectorDetail sessionDetails = getSessionConnectorDetailMap().get(sessionId);
		Initiator initiator = null;
		if (sessionDetails != null) {
			initiator = sessionDetails.getInitiator();
			if (initiator != null) {
				final ExecutorService executor = Executors.newSingleThreadExecutor();
				Initiator finalInitiator = initiator;
				final Future<?> future = executor.submit(() -> finalInitiator.stop(force));
				executor.shutdown();
				try {
					future.get(2, TimeUnit.SECONDS);
				}
				catch (Exception e) {
					if (e instanceof InterruptedException) {
						Thread.currentThread().interrupt();
					}
					future.cancel(false);
					if (!force) {
						throw new RuntimeException("An error occurred while stopping the QuickFIX initiator.", e);
					}
				}
				// stop the external applications
				stopExternalApplicationForSession(sessionId);
			}

			// Unregister the MBean connector
			if (sessionDetails.getConnectorObjectName() != null) {
				try {
					sessionDetails.getJmxExporter().getMBeanServer().unregisterMBean(sessionDetails.getConnectorObjectName());
				}
				catch (Exception e) {
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, "An error occurred while unregistering the connector [", sessionDetails.getConnectorObjectName(), "] for session ID [", sessionId, "]."));
				}
			}
			getSessionConnectorDetailMap().remove(sessionId);
		}
		return initiator != null;
	}


	@SuppressWarnings("BusyWait") // Intentionally allow waits for thread termination
	private void doStopDispatcherThreads(boolean force, SessionID sessionId) {
		List<ThreadInfo> threadInfoList = CollectionUtils.getFiltered(getFixSessionManagementService().getFixSessionDispatcherThreadInfoList(), t -> t.getThreadName().contains("Session dispatcher: " + sessionId));
		int waitCount = 1;
		while (waitCount < 6 && !CollectionUtils.isEmpty(threadInfoList)) {
			try {
				Thread.sleep(1000);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new RuntimeException("Thread was interrupted while waiting for dispatcher threads to stop.", e);
			}
			threadInfoList = CollectionUtils.getFiltered(getFixSessionManagementService().getFixSessionDispatcherThreadInfoList(), t -> t.getThreadName().contains("Session dispatcher: " + sessionId));
			waitCount++;
		}

		if (!CollectionUtils.isEmpty(threadInfoList)) {
			if (!force) {
				throw new RuntimeException("Found session dispatcher threads that were not terminated.");
			}
			else {
				for (ThreadInfo threadInfo : threadInfoList) {
					ThreadUtils.getThreadById(threadInfo.getThreadId()).interrupt();
				}
			}
		}
	}


	private void handlerStartStopErrors(Map<SessionID, Exception> failedSessions, boolean start) {
		if (!failedSessions.isEmpty()) {
			StringBuilder errors = new StringBuilder();

			errors.append("\n\n");
			for (Map.Entry<SessionID, Exception> entry : failedSessions.entrySet()) {
				errors.append("Failed to ").append(start ? "start" : "stop").append(" FIX session [").append(entry.getKey()).append("].");
				errors.append("\nCAUSED BY ");
				errors.append(ExceptionUtils.getDetailedMessage(entry.getValue())).append("\n");
			}
			throw new RuntimeException(errors.toString());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureFixFactories() {
		// set the session setting on objects that need them
		// NOTE: reconfigure for each initiator to make sure that the latest session settings are loaded,
		// also use the session settings for all sessions not just the session being started
		configureSessionSettings();

		// add the settings to the store and the factory if needed
		if (getMessageStoreFactory() instanceof QuickFixRequiresSettings) {
			((QuickFixRequiresSettings) getMessageStoreFactory()).setSessionSettings(getAggregatedSessionSettings());
		}
		if (getApplication() instanceof QuickFixRequiresSettings) {
			((QuickFixRequiresSettings) getApplication()).setSessionSettings(getAggregatedSessionSettings());
		}
	}


	private void clearSessionSettingsBySessionIdMap() {
		setSessionSettingsBySessionIdMapLazy(new Lazy<>(this::loadSessionSettingsBySessionIdMap));
	}


	private Map<SessionID, SessionSettings> loadSessionSettingsBySessionIdMap() {
		try {
			Map<SessionID, SessionSettings> result = new HashMap<>();
			SessionSettings allSessionSettings = getAggregatedSessionSettings();
			// Convert to individual session settings instances
			Iterator<SessionID> iter = allSessionSettings.sectionIterator();
			while (iter.hasNext()) {
				SessionID sessionId = iter.next();
				SessionSettings sessionSettings = new SessionSettings();
				sessionSettings.set(sessionId, allSessionSettings.get(sessionId));
				result.put(sessionId, sessionSettings);
			}
			return result;
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while loading session settings.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Custom getter
	public Map<SessionID, SessionSettings> getSessionSettingsBySessionIdMap() {
		return getSessionSettingsBySessionIdMapLazy().get();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The detail object associating a {@link SessionID} with its corresponding {@link Connector} and any other related information.
	 */
	private static class SessionConnectorDetail {

		private final SessionID sessionID;

		private Initiator initiator;
		private JmxExporter jmxExporter;
		private ObjectName connectorObjectName;

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public SessionConnectorDetail(SessionID sessionID) {
			this.sessionID = sessionID;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public SessionID getSessionID() {
			return this.sessionID;
		}


		public Initiator getInitiator() {
			return this.initiator;
		}


		public void setInitiator(Initiator initiator) {
			this.initiator = initiator;
		}


		public JmxExporter getJmxExporter() {
			return this.jmxExporter;
		}


		public void setJmxExporter(JmxExporter jmxExporter) {
			this.jmxExporter = jmxExporter;
		}


		public ObjectName getConnectorObjectName() {
			return this.connectorObjectName;
		}


		public void setConnectorObjectName(ObjectName connectorObjectName) {
			this.connectorObjectName = connectorObjectName;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionManagementService getFixSessionManagementService() {
		return this.fixSessionManagementService;
	}


	public void setFixSessionManagementService(FixSessionManagementService fixSessionManagementService) {
		this.fixSessionManagementService = fixSessionManagementService;
	}


	public boolean isEnableMBeans() {
		return this.enableMBeans;
	}


	public void setEnableMBeans(boolean enableMBeans) {
		this.enableMBeans = enableMBeans;
	}


	public Lazy<Map<SessionID, SessionSettings>> getSessionSettingsBySessionIdMapLazy() {
		return this.sessionSettingsBySessionIdMapLazy;
	}


	public void setSessionSettingsBySessionIdMapLazy(Lazy<Map<SessionID, SessionSettings>> sessionSettingsBySessionIdMapLazy) {
		this.sessionSettingsBySessionIdMapLazy = sessionSettingsBySessionIdMapLazy;
	}


	public ConcurrentMap<SessionID, SessionConnectorDetail> getSessionConnectorDetailMap() {
		return this.sessionConnectorDetailMap;
	}
}
