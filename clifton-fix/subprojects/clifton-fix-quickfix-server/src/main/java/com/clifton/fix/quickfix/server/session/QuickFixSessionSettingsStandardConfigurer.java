package com.clifton.fix.quickfix.server.session;

import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.quickfix.server.AbstractFixServerExecutor;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionSettingsConfigurer;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import quickfix.FixVersions;
import quickfix.Initiator;
import quickfix.Session;
import quickfix.SessionFactory;
import quickfix.SessionID;
import quickfix.SessionSettings;

import java.util.HashMap;
import java.util.Map;


/**
 * The {@link QuickFixSessionSettingsStandardConfigurer} configures standard {@link Session} settings which are intended to apply to all sessions. This applies settings declared by
 * the {@link Session}, {@link SessionSettings}, and {@link SessionFactory} types.
 * <p>
 * Session settings are described in further detail in {@link SessionSettings}. Setting keys are provided as constants in types linked by the {@link SessionSettings} documentation,
 * such as {@link Session}, {@link SessionFactory}, and {@link Initiator}.
 *
 * @author MikeH
 * @see AbstractFixServerExecutor#buildSettings(FixSessionDefinition, Map)
 * @see QuickFixSessionSettingsStandardConfigurer
 * @see SessionSettings
 */
@Component
@Order(0)
public class QuickFixSessionSettingsStandardConfigurer implements FixSessionSettingsConfigurer {

	@Override
	public void configure(FixSessionDefinition sessionDefinition, Map<String, String> sessionSettings) {
		sessionSettings.putAll(getSessionSettings(sessionDefinition));
		sessionSettings.putAll(getSessionSettingsSettings(sessionDefinition));
		sessionSettings.putAll(getSessionFactorySettings(sessionDefinition));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the configured settings associated with the {@link Session} class.
	 */
	private Map<String, String> getSessionSettings(FixSessionDefinition sessionDefinition) {
		Map<String, String> settings = new HashMap<>();
		MapUtils.putIfNotNull(settings, Session.SETTING_DEFAULT_APPL_VER_ID, sessionDefinition.getDefaultApplVerId());
		MapUtils.putIfNotNull(settings, Session.SETTING_DESCRIPTION, sessionDefinition.getDescription());
		MapUtils.putIfNotNull(settings, Session.SETTING_HEARTBTINT, StringUtils.toNullableString(sessionDefinition.getHeartbeatInterval()));
		MapUtils.putIfNotNull(settings, Session.SETTING_START_TIME, sessionDefinition.getStartTime());
		MapUtils.putIfNotNull(settings, Session.SETTING_END_TIME, sessionDefinition.getEndTime());
		MapUtils.putIfNotNull(settings, Session.SETTING_TIMEZONE, sessionDefinition.getTimeZone());
		MapUtils.putIfNotNull(settings, Session.SETTING_DATA_DICTIONARY, sessionDefinition.getSessionDictionaryLocation());

		if (!StringUtils.isEmpty(sessionDefinition.getBeginString()) && sessionDefinition.getBeginString().equals(FixVersions.BEGINSTRING_FIXT11)) {
			MapUtils.putIfNotNull(settings, Session.SETTING_APP_DATA_DICTIONARY, sessionDefinition.getSessionDictionaryLocation());
		}

		if (sessionDefinition.isWeeklySession()) {
			MapUtils.putIfNotNull(settings, Session.SETTING_START_DAY, DateUtils.getWeekdayName(sessionDefinition.getStartWeekday()));
			MapUtils.putIfNotNull(settings, Session.SETTING_END_DAY, DateUtils.getWeekdayName(sessionDefinition.getEndWeekday()));
		}
		return settings;
	}


	/**
	 * Gets the configured settings associated with the {@link SessionSettings} class.
	 * <p>
	 * <b>Note: None of these settings are typically used or required. Instead, these properties are used during {@link SessionID} creation and then used from the resulting {@link
	 * SessionID} directly at runtime. An example of {@link SessionID} generation can be found at {@link QuickFixSessionUtils#getSessionID(FixSessionDefinition)}. The below
	 * settings are included for completeness, only.</b>
	 */
	private Map<String, String> getSessionSettingsSettings(FixSessionDefinition sessionDefinition) {
		Map<String, String> settings = new HashMap<>();
		MapUtils.putIfNotNull(settings, SessionSettings.BEGINSTRING, sessionDefinition.getBeginString());
		MapUtils.putIfNotNull(settings, SessionSettings.SENDERCOMPID, sessionDefinition.getSenderCompId());
		MapUtils.putIfNotNull(settings, SessionSettings.SENDERSUBID, sessionDefinition.getSenderSubId());
		MapUtils.putIfNotNull(settings, SessionSettings.SENDERLOCID, sessionDefinition.getSenderLocId());
		MapUtils.putIfNotNull(settings, SessionSettings.TARGETCOMPID, sessionDefinition.getTargetCompId());
		MapUtils.putIfNotNull(settings, SessionSettings.TARGETSUBID, sessionDefinition.getTargetSubId());
		MapUtils.putIfNotNull(settings, SessionSettings.TARGETLOCID, sessionDefinition.getTargetLocId());
		MapUtils.putIfNotNull(settings, SessionSettings.SESSION_QUALIFIER, sessionDefinition.getSessionQualifier());

		return settings;
	}


	/**
	 * Gets the configured settings associated with the {@link SessionFactory} class.
	 */
	private Map<String, String> getSessionFactorySettings(FixSessionDefinition sessionDefinition) {
		Map<String, String> settings = new HashMap<>();
		MapUtils.putIfNotNull(settings, SessionFactory.SETTING_CONNECTION_TYPE, sessionDefinition.getConnectionType());
		return settings;
	}
}
