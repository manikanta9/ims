package com.clifton.fix.quickfix.server.log.database;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.TimeTrackedOperation;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageProcessHandler;
import com.clifton.fix.quickfix.server.log.AbstractQuickFixLog;
import com.clifton.fix.quickfix.util.QuickFixMessageUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.cache.FixSessionCache;
import quickfix.SessionID;
import quickfix.field.MsgType;

import java.util.Date;


/**
 * The <code>QuickFixLog</code> implements a quickfix.Log used to log messages and
 * events for a session.
 *
 * @author mwacker
 */
public class QuickFixDatabaseLog extends AbstractQuickFixLog {

	private FixMessageProcessHandler<FixMessageEntry> fixMessageProcessHandler;
	private FixSessionCache<SessionID> fixSessionCache;
	private SystemRequestStatsService systemRequestStatsService;
	private ContextHandler contextHandler;

	private SessionID sessionId;

	/**
	 * Should be set to true in properties file if system stats should be recorded.
	 */
	private boolean recordDatabaseSystemStats;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public QuickFixDatabaseLog(SessionID sessionId) {
		this.sessionId = sessionId;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void clear() {
		getFixMessageEntryService().clearFixMessageEntriesForSession(getFixSessionCache().get(getSessionId()).getId());
	}


	@Override
	public void onEvent(String text) {
		doLogEvent(text, FixMessageEntryService.EVENT_ENTRY_TYPE_NAME, null);
	}


	@Override
	public void onErrorEvent(String text) {
		doLogEvent(text, FixMessageEntryService.ERROR_EVENT_ENTRY_TYPE_NAME, null);
	}


	@Override
	protected void logIncoming(String message) {
		doLogEvent(message, FixMessageEntryService.MESSAGE_ENTRY_TYPE_NAME, true);
	}


	@Override
	protected void logOutgoing(String message) {
		doLogEvent(message, FixMessageEntryService.MESSAGE_ENTRY_TYPE_NAME, false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void doLogEvent(String text, String typeName, Boolean incoming) {
		FixSession fixSession = null;
		try {
			fixSession = getFixSessionCache().get(getSessionId());
		}
		catch (Throwable e) {
			// do nothing so avoid missing an entry in the log
		}

		if (isRecordDatabaseSystemStats()) {
			FixSession finalFixSession = fixSession;
			TimeTrackedOperation<Void> doLogEventOp = TimeUtils.getElapsedTime(() -> doLogEventLogic(text, typeName, incoming, finalFixSession));

			if (fixSession != null) {
				Object user = getContextHandler().getBean(Context.USER_BEAN_NAME);
				String uri = fixSession.getDefinition().getName();
				if (incoming != null) {
					uri += incoming ? "-INCOMING" : "-OUTGOING";
				}
				else {
					uri += "-EVENT";
				}
				getSystemRequestStatsService().saveSystemRequestStats("FIX-DATABASE-LOG", uri, user, doLogEventOp.getElapsedNanoseconds(), 0, 0);
			}
		}
		else {
			doLogEventLogic(text, typeName, incoming, fixSession);
		}
	}


	private void doLogEventLogic(String text, String typeName, Boolean incoming, FixSession fixSession) {
		// Record date nearest to time of reception, rather than time of persistence
		Date entryDate = new Date();

		// Get the message type of the message string, assuming this is message moving across the wire
		String messageType = null;
		if (incoming != null) {
			try {
				messageType = QuickFixMessageUtils.getMessageType(text);
			}
			catch (Exception e) {
				// Log exception but continue in order to ensure message is persisted in database
				LogUtils.warn(getClass(), "An exception occurred while retrieving the message type.", e);
			}
		}

		// if it's a heartbeat or admin message change the type
		if (incoming != null && !StringUtils.isEmpty(messageType)) {
			// if it's a heartbeat or admin message change the type
			if (MsgType.HEARTBEAT.equals(messageType)) {
				typeName = FixMessageEntryService.HEARTBEAT_MESSAGE_ENTRY_TYPE_NAME;
			}
			else if (QuickFixMessageUtils.isAdminMessage(messageType)) {
				typeName = FixMessageEntryService.ADMIN_MESSAGE_ENTRY_TYPE_NAME;
			}
		}

		FixMessageEntry entry = getFixMessageEntryService().createMessageEntry(text, typeName, incoming, entryDate, fixSession);

		// Add the message to queue to be processed.
		if (entry.getType().isMessage()) {
			getFixMessageProcessHandler().processMessage(entry);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageProcessHandler<FixMessageEntry> getFixMessageProcessHandler() {
		return this.fixMessageProcessHandler;
	}


	public void setFixMessageProcessHandler(FixMessageProcessHandler<FixMessageEntry> fixMessageProcessHandler) {
		this.fixMessageProcessHandler = fixMessageProcessHandler;
	}


	public FixSessionCache<SessionID> getFixSessionCache() {
		return this.fixSessionCache;
	}


	public void setFixSessionCache(FixSessionCache<SessionID> fixSessionCache) {
		this.fixSessionCache = fixSessionCache;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SessionID getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(SessionID sessionId) {
		this.sessionId = sessionId;
	}


	public boolean isRecordDatabaseSystemStats() {
		return this.recordDatabaseSystemStats;
	}


	public void setRecordDatabaseSystemStats(boolean recordDatabaseSystemStats) {
		this.recordDatabaseSystemStats = recordDatabaseSystemStats;
	}
}
