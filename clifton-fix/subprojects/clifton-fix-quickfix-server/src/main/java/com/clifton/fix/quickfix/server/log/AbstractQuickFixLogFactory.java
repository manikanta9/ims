package com.clifton.fix.quickfix.server.log;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.fix.quickfix.server.QuickFixRequiresSettings;
import com.clifton.fix.quickfix.server.log.noop.QuickFixNoOpLog;
import org.jetbrains.annotations.NotNull;
import quickfix.Log;
import quickfix.LogFactory;
import quickfix.SessionID;
import quickfix.SessionSettings;


/**
 * The {@link AbstractQuickFixLogFactory} provides a base {@link LogFactory} implementation for generating QuickFIX {@link Log} instances. This provides features such as {@link
 * #enabled enabling or disabling} the factory, automatically {@link #isAutowire() autowiring} generated instances, and injecting {@link SessionSettings} via the {@link
 * QuickFixRequiresSettings} interface.
 *
 * @author MikeH
 */
public abstract class AbstractQuickFixLogFactory implements LogFactory, QuickFixRequiresSettings {

	private ApplicationContextService applicationContextService;

	/**
	 * If <code>true</code>, the log will be generated using the standard {@link #doCreate(SessionID)} method. Otherwise, a {@link QuickFixNoOpLog} will be generated in its place.
	 */
	private boolean enabled = true;
	private SessionSettings sessionSettings;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates the {@link Log} implementation. If <code>null</code> is returned, a {@link QuickFixNoOpLog} will be generated in its place.
	 */
	protected abstract Log doCreate(SessionID sessionID);


	/**
	 * Returns the autowire flag. If <code>true</code>, the generated {@link Log} will be autowired after creation.
	 */
	protected boolean isAutowire() {
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@NotNull // Null return values are not allowed; some QuickFIX locations, such as the CompositeLog, cannot handle null loggers
	public Log create(SessionID sessionID) {
		if (!isEnabled()) {
			return QuickFixNoOpLog.INSTANCE;
		}
		Log result = doCreate(sessionID);
		if (result == null) {
			return QuickFixNoOpLog.INSTANCE;
		}
		if (isAutowire()) {
			getApplicationContextService().autowireBean(result);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public boolean isEnabled() {
		return this.enabled;
	}


	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}


	public SessionSettings getSessionSettings() {
		return this.sessionSettings;
	}


	@Override
	public void setSessionSettings(SessionSettings sessionSettings) {
		this.sessionSettings = sessionSettings;
	}
}
