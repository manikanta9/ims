package com.clifton.fix.quickfix.server;


import com.clifton.core.logging.LogUtils;
import com.clifton.fix.session.FixSessionParameters;
import quickfix.DefaultSessionFactory;
import quickfix.Initiator;
import quickfix.Session;
import quickfix.SessionFactory;
import quickfix.SessionID;
import quickfix.ThreadedSocketInitiator;

import javax.annotation.PreDestroy;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


/**
 * The <code>QuickFixExecutor</code> handles the creating, starting and stopping the SocketInitiator
 * that runs the FIX connection.
 * <p>
 * NOTE: This implementation will create 1 Initiator, that means
 * that the sessions cannot be started or stopped individually.
 * <p>
 * Example configuration:
 * <p>
 * <pre><code>{@literal
 * <bean id="fixExecutor" class="com.clifton.fix.quickfix.server.QuickFixExecutor" init-method="init">
 *     <property name="application" ref="fixMessageRouter" />
 *     <property name="logFactory" ref="fixLogFactory" />
 *     <property name="messageStoreFactory" ref="fixStoreFactory" />
 *     <property name="serverEnvironment" value="${fix.session.serverEnvironment}"/>
 * </bean>
 * }</code></pre>
 *
 * @author mwacker
 */
public class QuickFixExecutor extends AbstractFixServerExecutor {

	/**
	 * The actual SocketInitiator (or ThreadedSocketInitiator) that is created to run the sessions.
	 */
	private Initiator initiator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isRunning(FixSessionParameters sessionParameters) {
		throw new UnsupportedOperationException();
	}


	@Override
	public void restart(FixSessionParameters sessionParameters) {
		throw new UnsupportedOperationException();
	}


	@Override
	public void restart() {
		stop();
		start();
	}


	@Override
	public void start(FixSessionParameters sessionParameters) {
		throw new UnsupportedOperationException();
	}


	@Override
	public void start() {
		try {
			clearSessionAndDBCaches();

			// start the external applications
			startExternalApplications();

			// set the session setting on object that need them
			configureSessionSettings();

			if (getMessageStoreFactory() instanceof QuickFixRequiresSettings) {
				((QuickFixRequiresSettings) getMessageStoreFactory()).setSessionSettings(getAggregatedSessionSettings());
			}
			if (getApplication() instanceof QuickFixRequiresSettings) {
				((QuickFixRequiresSettings) getApplication()).setSessionSettings(getAggregatedSessionSettings());
			}

			// create the session factory
			SessionFactory sessionFactory = new DefaultSessionFactory(getApplication(), getMessageStoreFactory(), getLogFactory());

			// create and start the socket initiator
			setInitiator(new ThreadedSocketInitiator(sessionFactory, getAggregatedSessionSettings()));
			getInitiator().start();

			visitSessions(new SessionVisitor() {

				@Override
				public void visitSession(SessionID sessionId, Session session) {
					LogUtils.info(getClass(), "Started " + sessionId);
					System.out.println("Started " + sessionId); // print to the command line
				}
			});
		}
		catch (Throwable e) {
			try {
				stop();
			}
			catch (Throwable ex) {
				LogUtils.error(getClass(), "Failed to stop application after error on startup.", ex);
			}
			throw new RuntimeException("Failed to start FIX sessions.", e);
		}
	}


	@Override
	public void stop(FixSessionParameters sessionParameters, boolean force) {
		throw new UnsupportedOperationException();
	}


	@Override
	@PreDestroy
	public void stop() {
		// logout of the session and shutdown the connections
		// ask them all to logout
		visitSessions((sessionId, session) -> session.logout());

		// wait for them to do so
		visitSessions(this::verifySessionShutdown);

		// stop the initiator
		this.initiator.stop(true);

		// stop the external applications
		stopExternalApplications();
		// clear the session cache
		getFixSessionCache().clear();
		// clear the external applications
		getExternalProcessList().clear();
	}


	@Override
	public void stopSessionList(List<String> sessionIdList, boolean force) {
		throw new UnsupportedOperationException();
	}


	@Override
	public void init() {
		validateProperties();
		start();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Initiator getInitiator(SessionID sessionID) {
		return (sessionID == null || getInitiator().getSessions().contains(sessionID))
				? getInitiator()
				: null;
	}


	@Override
	public Collection<Initiator> getInitiatorList() {
		return Collections.singletonList(getInitiator());
	}


	@Override
	public Collection<SessionID> getSessionIdList() {
		return getInitiator().getSessions();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected ConcurrentMap<SessionID, Session> createSessionMap() {
		ConcurrentMap<SessionID, Session> result = new ConcurrentHashMap<>();
		for (Iterator<SessionID> sessionIterator = getAggregatedSessionSettings().sectionIterator(); sessionIterator.hasNext(); ) {
			SessionID sessionId = sessionIterator.next();
			Session session = Session.lookupSession(sessionId);
			if (session != null) {
				result.put(sessionId, session);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Private setter
	private void setInitiator(Initiator initiator) {
		this.initiator = initiator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Initiator getInitiator() {
		return this.initiator;
	}
}
