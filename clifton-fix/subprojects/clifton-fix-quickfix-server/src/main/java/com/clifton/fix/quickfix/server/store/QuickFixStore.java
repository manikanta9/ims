package com.clifton.fix.quickfix.server.store;


import com.clifton.fix.message.store.BaseMessageStoreHandler;
import com.clifton.fix.session.FixSession;
import quickfix.MemoryStore;
import quickfix.MessageStore;
import quickfix.SessionID;

import java.io.IOException;


/**
 * The <code>QuickFixStore</code> implements a message store for QuickFIX.
 *
 * @author mwacker
 */
public class QuickFixStore extends BaseMessageStoreHandler<SessionID> implements MessageStore {

	private final MemoryStore cache;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public QuickFixStore(SessionID sessionId) {
		super(sessionId);

		try {
			this.cache = new MemoryStore();
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to create the memory store.", e);
		}
	}


	public void init() {
		try {
			loadCache();
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to generate message store cache.", e);
		}
	}


	@Override
	public void reset() {
		try {
			this.cache.reset();
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
		super.reset();
	}


	@Override
	protected void loadCache() {
		try {
			FixSession session = getFixSessionCache().get(getSessionId());
			if (session == null) {
				throw new RuntimeException("No session definition exists for [" + getSessionId() + "].");
			}
			if (session.isNewBean()) {
				session.setCreationDateAndTime(this.cache.getCreationTime());
				session.setIncomingSequenceNumber(this.cache.getNextTargetMsgSeqNum());
				session.setOutgoingSequenceNumber(this.cache.getNextSenderMsgSeqNum());
				session.setActiveSession(true);
				saveFixSession(session);
			}
			else {
				this.cache.setNextTargetMsgSeqNum(session.getIncomingSequenceNumber());
				this.cache.setNextSenderMsgSeqNum(session.getOutgoingSequenceNumber());
			}
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}


	private void storeSequenceNumbers() throws IOException {
		super.storeSequenceNumbers(this.cache.getNextTargetMsgSeqNum(), this.cache.getNextSenderMsgSeqNum());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getNextSenderMsgSeqNum() throws IOException {
		return this.cache.getNextSenderMsgSeqNum();
	}


	@Override
	public int getNextTargetMsgSeqNum() throws IOException {
		return this.cache.getNextTargetMsgSeqNum();
	}


	@Override
	public void setNextSenderMsgSeqNum(int next) throws IOException {
		this.cache.setNextSenderMsgSeqNum(next);
		storeSequenceNumbers();
	}


	@Override
	public void setNextTargetMsgSeqNum(int next) throws IOException {
		this.cache.setNextTargetMsgSeqNum(next);
		storeSequenceNumbers();
	}


	@Override
	public void incrNextSenderMsgSeqNum() throws IOException {
		this.cache.incrNextSenderMsgSeqNum();
		setNextSenderMsgSeqNum(this.cache.getNextSenderMsgSeqNum());
	}


	@Override
	public void incrNextTargetMsgSeqNum() throws IOException {
		this.cache.incrNextTargetMsgSeqNum();
		setNextTargetMsgSeqNum(this.cache.getNextTargetMsgSeqNum());
	}
}
