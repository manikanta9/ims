package com.clifton.fix.quickfix.server.log.database;


import com.clifton.fix.quickfix.server.log.AbstractQuickFixLogFactory;
import com.clifton.fix.quickfix.server.util.QuickFixSessionUtils;
import quickfix.Log;
import quickfix.LogFactory;
import quickfix.RuntimeError;
import quickfix.SessionID;


/**
 * The {@link QuickFixDatabaseLogFactory} is a {@link LogFactory} implementation which produces a database-writing {@link Log} instance.
 *
 * @author mwacker
 * @see QuickFixDatabaseLog
 */
public class QuickFixDatabaseLogFactory extends AbstractQuickFixLogFactory {

	/**
	 * The setting name used to determine whether heartbeats should be logged for the session.
	 */
	public static final String SETTING_LOG_HEARTBEATS = "DatabaseLogHeartbeats";

	/**
	 * If true, system statistics will be recorded for {@link QuickFixDatabaseLog#doLogEvent}
	 */
	private boolean recordDatabaseSystemStats;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Log doCreate(SessionID sessionID) {
		try {
			QuickFixDatabaseLog databaseLog = new QuickFixDatabaseLog(sessionID);
			databaseLog.setLogHeartbeats(QuickFixSessionUtils.getSessionBoolOrDefault(getSessionSettings(), sessionID, SETTING_LOG_HEARTBEATS, true));
			databaseLog.setLogNonPersistentMessages(true); // Log implementation has internal handling for non-persistent messages
			databaseLog.setRecordDatabaseSystemStats(isRecordDatabaseSystemStats());
			return databaseLog;
		}
		catch (Exception e) {
			throw new RuntimeError(e);
		}
	}


	@Override
	protected boolean isAutowire() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRecordDatabaseSystemStats() {
		return this.recordDatabaseSystemStats;
	}


	public void setRecordDatabaseSystemStats(boolean recordDatabaseSystemStats) {
		this.recordDatabaseSystemStats = recordDatabaseSystemStats;
	}
}
