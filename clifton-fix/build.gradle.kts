import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant

val apiVariant = registerVariant("api", upstreamForMain = true)
val sharedVariant = registerVariant("shared", upstreamForMain = true)
val fixsimVariant = registerVariant("fixsim")

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	apiVariant.api(sharedVariant())

	apiVariant.api(project(":clifton-core"))
	apiVariant.api(project(":clifton-core:clifton-core-messaging")) // TODO SEE IF WE CAN REMOVE THIS (FixMessagingMessage)
	apiVariant.api(project(":clifton-system"))
	implementation(project(":clifton-fix:clifton-fix-api")) { usingVariant("java-server") }

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-system")))
	testFixturesApi("com.clifton:clifton-fix-fixsim-api-java:0.1.0")


}
