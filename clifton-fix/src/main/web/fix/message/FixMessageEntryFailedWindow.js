Clifton.fix.message.FixMessageEntryFailedWindow = Ext.extend(Clifton.fix.message.FixMessageWindowBase, {
	titlePrefix: 'FIX Message Failed Entry',
	detailUrl: 'fixMessageEntryFailedMessage.json',
	fieldListUrl: 'fixMessageEntryFailedFieldList.json',
	table: 'FixMessageEntry',

	listeners: {
		beforerender: function(win) {
			win.fieldListUrl = 'fixMessageEntryFailedFieldList.json?id=' + win.params.id;
		}
	},

	detailItems: [
		{fieldLabel: 'FIX Session', xtype: 'linkfield', name: 'session.definition.name', detailIdField: 'session.definition.id', detailPageClass: 'Clifton.fix.session.FixSessionWindow'},
		{fieldLabel: 'Message Type', name: 'type.name', xtype: 'linkfield', detailIdField: 'type.id', detailPageClass: 'Clifton.fix.message.FixMessageEntryTypeWindow'},
		{fieldLabel: 'Message Date and Time', name: 'messageEntryDateAndTime', xtype: 'displayfield'},
		{fieldLabel: '', boxLabel: 'Incoming (received by our FIX Engine from an execution venue)', xtype: 'checkbox', name: 'incoming'},
		{fieldLabel: 'Raw Message Text', name: 'text', xtype: 'textarea', height: 250}
	]
});
