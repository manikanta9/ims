Clifton.fix.message.FixMessageStoreDetailWindow = Ext.extend(Clifton.fix.message.FixMessageWindowBase, {
	titlePrefix: 'FIX Message Store Entry',
	detailUrl: 'fixMessageStore.json',
	fieldListUrl: 'fixMessageStoreFieldList.json',
	table: 'FixMessageStore',
	height: 450,

	detailItems: [
		{fieldLabel: 'FIX Session', xtype: 'linkfield', name: 'session.definition.name', detailIdField: 'session.id', detailPageClass: 'Clifton.fix.session.FixSessionWindow'},
		{fieldLabel: 'Sequence Number', name: 'sequenceNumber', xtype: 'integerfield'},
		{fieldLabel: 'Raw Message Text', name: 'text', xtype: 'textarea', height: 250}
	]
});
