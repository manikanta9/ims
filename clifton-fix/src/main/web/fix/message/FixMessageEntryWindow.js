Clifton.fix.message.FixMessageEntryWindow = Ext.extend(Clifton.fix.message.FixMessageWindowBase, {
	titlePrefix: 'FIX Message Entry',
	detailUrl: 'fixMessageEntry.json',
	fieldListUrl: 'fixMessageEntryFieldList.json',
	table: 'FixMessageEntry',
	height: 570,

	listeners: {
		beforerender: function(win) {
			const defaultData = win.defaultData;
			if (TCG.isNotBlank(defaultData) && (TCG.isTrue(defaultData.heartbeatMessages) || TCG.isTrue(defaultData.nonPersistentMessages))) {
				const nonPersistentUrlFragment = TCG.isTrue(defaultData.heartbeatMessages) ? 'heartbeatMessages=true' : 'nonPersistentMessages=true';
				const urlSuffix = '?id=' + win.params.id + '&' + nonPersistentUrlFragment + '&limit=1';
				win.detailUrl = 'fixMessageEntryNonPersistent.json' + urlSuffix;
				win.fieldListUrl = 'fixMessageEntryFieldListNonPersistentFind.json' + urlSuffix;
			}
		}
	},

	afterFormLoad: function(formPanel) {
		formPanel.hideFieldIfBlank('identifier.uniqueStringIdentifier');
	},
	detailItems: [
		{fieldLabel: 'FIX Session', xtype: 'linkfield', name: 'session.definition.name', detailIdField: 'session.id', detailPageClass: 'Clifton.fix.session.FixSessionWindow'},
		{fieldLabel: 'Message Type', name: 'type.name', xtype: 'linkfield', detailIdField: 'type.id', detailPageClass: 'Clifton.fix.message.FixMessageEntryTypeWindow'},
		{fieldLabel: 'Unique ID', xtype: 'linkfield', name: 'identifier.uniqueStringIdentifier', detailIdField: 'identifier.id', detailPageClass: 'Clifton.fix.identifier.FixIdentifierWindow'},
		{fieldLabel: 'Message Date and Time', name: 'messageEntryDateAndTime', xtype: 'displayfield'},
		{fieldLabel: '', boxLabel: 'Incoming (received by our FIX Engine from an execution venue)', xtype: 'checkbox', name: 'incoming'},
		{fieldLabel: '', boxLabel: 'Processed (first messages are stored and then processed by the system)', xtype: 'checkbox', name: 'processed'},
		{fieldLabel: 'Raw Message Text', name: 'text', xtype: 'textarea', height: 250},
		{
			xtype: 'panel',
			fbar: [{
				text: 'Download Message',
				iconCls: 'arrow-down-green',
				tooltip: 'Download the raw message text of this message. This download is most frequently used to export message sequences to be replayed in testing environments.',
				width: 150,
				handler: function() {
					const f = TCG.getParentFormPanel(this);
					const textToDownload = f.getFormValue('text');
					const messageIdentifier = f.getFormValue('identifier.uniqueStringIdentifier');
					let fileName = 'FIX Message';
					if (messageIdentifier) {
						fileName += ' - ' + messageIdentifier;
					}
					TCG.downloadDataAsFile('text/plain', textToDownload, fileName);
				}
			}]
		}
	]
});
