Clifton.fix.message.FixMessagePreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'FIX Message Preview',
	height: 570,
	width: 800,

	layout: 'border',

	items: [
		{
			xtype: 'formpanel',
			region: 'north',
			height: 200,
			instructions: 'Copy and Paste Raw Message text to preview the friendly version of the message below.',
			labelWidth: 120,
			items: [
				{fieldLabel: 'Raw Message Text', name: 'rawMessage', xtype: 'textarea', height: '100'}
			]
		},

		{
			region: 'center',
			xtype: 'treegrid',
			appendStandardColumns: false,
			instructions: 'Copy and Paste Raw Message text to preview the friendly version of the message below.',
			defaults: {
				anchor: '-35 -35' // leave room for error icon
			},
			getLoadURL: function() {
				return 'fixMessageEntryFieldListForRawMessageText.json';
			},

			getLoadParams: function() {
				const fp = this.getWindow().getMainFormPanel();
				const rawMessageValue = fp.getFormValue('rawMessage', true, true);
				if (TCG.isNotBlank(rawMessageValue)) {
					return {rawMessage: rawMessageValue};
				}
				return false;
			},
			prepareResultData: function(o) {
				if (TCG.isNull(TCG.getValue('children', o))) {
					o.leaf = true;
				}
				return o;
			},
			readOnly: true,
			columns: [
				{header: 'Field Name', width: 170, dataIndex: 'fieldName'},
				{header: 'Tag', width: 50, dataIndex: 'tag', type: 'int', doNotFormat: true},
				{header: 'Field Value', width: 300, dataIndex: 'fieldValue'},
				{header: 'Field Value Description', width: 230, dataIndex: 'fieldValueName'}
			]
		}
	]
});
