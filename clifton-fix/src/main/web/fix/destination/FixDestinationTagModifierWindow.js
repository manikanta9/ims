Clifton.fix.destination.FixDestinationTagModifierWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Fix Session Definition Tag Modifier',
	iconCls: 'shopping-cart-blue',
	width: 600,
	height: 300,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'FIX Tag Modifier',
				items: [{
					xtype: 'formpanel',
					labelWidth: 130,
					url: 'fixDestinationTagModifier.json',
					instructions: 'Provides the ability to modify the fix destination tag modifier order.',

					items: [
						{fieldLabel: 'Session Definition', name: 'referenceOne.name', xtype: 'linkfield', detailIdField: 'referenceOne.id', detailPageClass: 'Clifton.fix.session.FixSessionDefinitionWindow'},
						{fieldLabel: 'Tag Modifier', name: 'referenceTwo.name', xtype: 'linkfield', detailIdField: 'referenceTwo.id', detailPageClass: 'Clifton.fix.tag.FixTagModifierWindow'},
						{fieldLabel: 'Order', name: 'order'}
					]
				}]
			},
			{
				title: 'FIX Tag Modifier Filters',
				items: [{
					name: 'fixTagModifierTagModifierFilterListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'Filters this session-specific Fix Tag Modifier.',
					columns: [
						{header: 'ID', dataIndex: 'referenceTwo.id', width: 10, hidden: true},
						{header: 'Name', dataIndex: 'referenceTwo.name', width: 50},
						{header: 'Description', dataIndex: 'referenceTwo.description', width: 150},
						{header: 'Field Tag', dataIndex: 'referenceTwo.fieldTag', width: 50, type: 'int'},
						{header: 'Field Value', dataIndex: 'referenceTwo.fieldValue', width: 75}
					],
					getLoadParams: function() {
						return {
							fixSessionDefinitionId: this.getWindow().getMainForm().formValues.referenceOne.id,
							fixTagModifierId: this.getWindow().getMainForm().formValues.referenceTwo.id
						};
					},
					editor: {
						detailPageClass: 'Clifton.fix.tag.FixTagModifierFilterWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceTwo.id;
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'fixTagModifierFilter', url: 'fixTagModifierFilterListFind.json', emptyText: '< Select Tag Filter >', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a fix tag modifier filter to this session specific modifier.',
								iconCls: 'add',
								scope: gridPanel,
								handler: function() {
									const tagModifierFilter = TCG.getChildByName(toolBar, 'fixTagModifierFilter');
									const tagModifierFilterId = tagModifierFilter.getValue();
									const sessionDefinitionId = this.getWindow().getMainForm().formValues.referenceOne.id;
									const tagModifierId = this.getWindow().getMainForm().formValues.referenceTwo.id;

									tagModifierFilter.clearValue();

									const params = {};
									params['fixSessionDefinitionId'] = sessionDefinitionId;
									params['fixTagModifierId'] = tagModifierId;
									params['fixTagModifierFilterId'] = tagModifierFilterId;

									if (tagModifierFilterId === '') {
										TCG.showError('You must first select desired tag modifier filter from the list.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											timeout: 120000,
											waitMsg: 'Saving: Please Wait.',
											params: params,
											scope: gridPanel,
											success: function(form, action) {
												const result = Ext.decode(form.responseText);
												if (result.success) {
													if (this.isUseWaitMsg()) {
														this.getMsgTarget().mask('Saving Tag Modifier Filter... Success');
														const unmaskFunction = function() {
															this.getMsgTarget().unmask();
														};
														unmaskFunction.defer(600, this);
														this.scope.reload();
													}
												}
												else {
													this.onFailure();
													this.getMsgTarget().unmask();
													TCG.data.ErrorHandler.handleFailure(this, result);
												}
											},
											error: function(form, action) {
												Ext.Msg.alert('Failed to save tag modifier Filter to session definition', action.result.data);
											}
										});
										loader.load('fixTagModifierTagModifierFilterToFixTagModifierFilterLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
