Clifton.fix.destination.FixDestinationTagModifierFilterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'FIX Tag Modifier',
	iconCls: 'shopping-cart-blue',
	width: 800,
	height: 600,

	items: [
		{
			xtype: 'formwithdynamicfields',
			url: 'fixTagModifierEntry.json',
			loadValidation: false,
			requestedProperties: 'properties',
			labelWidth: 140,
			labelFieldName: 'fixTagModifier.name',
			instructions: 'The following field tag modifier is applied to messages for the selected FIX Destination when all of the below filters apply (AND logic).',

			getWarningMessage: function() {
				return 'Tag Modifiers are <b>global</b> entities, so changing tag modifier properties here will affect other fix destinations if they are used within them as well.  The Filters applied here are also fix destination specific.';
			},
			childTables: 'SystemBeanProperty', // used by audit trail
			dynamicTriggerFieldName: 'fixTagModifier.type.label',
			dynamicTriggerValueFieldName: 'fixTagModifier.type.id',
			dynamicFieldTypePropertyName: 'type',
			dynamicFieldListPropertyName: 'fixTagModifier.propertyList',
			dynamicFieldsUrl: 'systemBeanPropertyTypeListByBean.json',
			dynamicFieldsUrlParameterName: 'type.id',
			dynamicFieldsUrlIdFieldName: 'fixTagModifier.id',
			dynamicFieldsUrlListParameterName: 'propertyList',
			dynamicFieldFormFragment: 'fixTagModifierPropertyListFragment',

			listeners: {
				afterload: function(panel) {
					const form = panel.getForm();
					if (TCG.isNotBlank(TCG.getValue('fixTagModifier.id', form.formValues))) {
						panel.disableField('fixTagModifier.tagModifierMetaData.label');
					}
				}
			},
			items: [
				{fieldLabel: 'FIX Destination', name: 'fixDestination.name', xtype: 'linkfield', detailIdField: 'fixDestination.id', detailPageClass: 'Clifton.fix.destination.FixDestinationWindow'},
				{fieldLabel: 'Bean Group (Interface)', name: 'fixTagModifier.type.group.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.GroupWindow', detailIdField: 'fixTagModifier.type.group.id'},
				{
					fieldLabel: 'Bean Type (Class)', name: 'fixTagModifier.type.label', hiddenName: 'fixTagModifier.type.id', displayField: 'label', xtype: 'combo',
					url: 'systemBeanTypeListFind.json?',
					detailPageClass: 'Clifton.system.bean.TypeWindow',
					disableAddNewItem: true,
					listeners: {
						// reset object bean property fields on changes to bean type (triggerField)
						select: function(combo, record, index) {
							combo.ownerCt.resetDynamicFields(combo, false);
						},
						beforequery: function(queryEvent) {
							const form = queryEvent.combo.getParentForm().getForm();
							const groupId = form.findField('fixTagModifier.type.group.id').getValue();
							const groupName = form.findField('fixTagModifier.type.group.name').getValue();
							const queryParams = {};
							if (groupName) {
								queryParams.groupName = groupName;
							}
							if (groupId) {
								queryParams.groupId = groupId;
							}
							if (TCG.isBlank(queryParams.groupName) && TCG.isBlank(queryParams.groupId)) {
								queryParams.groupName = 'FIX Tag Modifier';
							}
							queryEvent.combo.store.baseParams = queryParams;
						}
					}
				},
				{fieldLabel: 'Modifier Id', name: 'fixTagModifier.id', width: 10, hidden: true},
				{fieldLabel: 'Modifier Bean Name', name: 'fixTagModifier.name', width: 50},
				{fieldLabel: 'Description', name: 'fixTagModifier.description', xtype: 'textarea'},
				{fieldLabel: 'Modifier Order', name: 'fixTagModifierOrder', xtype: 'integerfield', allowBlank: false, qtip: 'Modifiers are applied to a FIX Message in specified order, application of earlier modifiers could affect the application of later modifiers.'},
				{xtype: 'label', html: '<hr/>', submitValue: false},

				{
					xtype: 'formfragment',
					frame: false,
					name: 'fixTagModifierPropertyListFragment',
					labelWidth: 140,
					bodyStyle: 'padding: 0',
					anchor: '-15',

					items: []
				},
				{
					html: '<hr/>',
					xtype: 'label'
				},

				{
					title: 'Filters',
					xtype: 'formgrid',
					storeRoot: 'fixTagModifierFilterList',
					name: 'fixTagModifierFilterList',
					dtoClass: 'com.clifton.fix.tag.FixTagModifierFilter',
					detailPageClass: 'Clifton.fix.tag.FixTagModifierFilterWindow',
					readOnly: true,
					columnsConfig: [
						{header: 'ID', name: 'id', dataIndex: 'id', width: 50, hidden: true},
						{header: 'Filter Name', dataIndex: 'name', width: 175},
						{header: 'Description', dataIndex: 'description', width: 250},
						{header: 'Field Tag', dataIndex: 'fieldTag', width: 75, type: 'int'},
						{header: 'Field Value', dataIndex: 'fieldValue', width: 175}
					],
					reload: function(win) {
						const gridForm = this;
						const form = win.getMainForm();
						const recordId = form.formValues.id;
						const store = gridForm.getStore();
						let existingRecord = false;
						store.each(function(record) {
							if (TCG.isEquals(record.get('id'), recordId)) {
								existingRecord = true;
								record.data = form.formValues;
								record.commit();
							}
						});
						if (!existingRecord) {
							const RowClass = store.recordType;
							gridForm.stopEditing();
							const row = new RowClass(form.formValues);
							row.json = form.formValues;
							row.id = recordId;
							store.insert(0, row);
							gridForm.startEditing(0, 0);
						}
						gridForm.markModified();
					},
					addToolbarButtons: function(toolBar) {
						const formPanel = TCG.getParentFormPanel(toolBar);
						toolBar.add(new TCG.form.ComboBox({
							name: 'fixTagModifierFilter',
							url: 'fixTagModifierFilterListFind.json',
							emptyText: '< Select Filter >',
							loadAll: false,
							disableAddNewItem: true,
							width: 250,
							listWidth: 250
						}));
						toolBar.add(' ');
						toolBar.add({
							text: 'Link',
							tooltip: 'Link an existing filter.',
							iconCls: 'link',
							scope: this,
							handler: function() {
								const tagModifierFilter = TCG.getChildByName(toolBar, 'fixTagModifierFilter');
								const tagModifierFilterId = tagModifierFilter.getValue();
								if (TCG.isBlank(tagModifierFilterId)) {
									TCG.showError('You must first select desired tag modifier filter from the list.');
								}
								else {
									let containsRecord = false;
									this.store.each(function(record) {
										if (TCG.isEquals(tagModifierFilterId, record.get('id'))) {
											containsRecord = true;
										}
									});
									if (containsRecord) {
										TCG.showError('Filter ' + '[' + tagModifierFilterId + '] is already associated with this modifier.');
									}
									else {
										const params = {
											id: tagModifierFilterId
										};
										const loader = new TCG.data.JsonLoader({
											waitTarget: formPanel,
											timeout: 120000,
											waitMsg: 'Loading: Please Wait.',
											params: params,
											formgrid: TCG.getParentByClass(toolBar, TCG.grid.FormGridPanel),
											success: function(form, action) {
												const result = Ext.decode(form.responseText);
												if (result.success) {
													if (this.isUseWaitMsg()) {
														this.getMsgTarget().mask('Loading Tag Modifier... Success');
														const unmaskFunction = function() {
															this.getMsgTarget().unmask();
														};
														unmaskFunction.defer(600, this);
														const store = this.formgrid.getStore();
														const RowClass = store.recordType;
														const fg = this.formgrid;
														fg.stopEditing();
														const row = new RowClass(result.data);
														store.insert(0, row);
														fg.startEditing(0, 0);
														fg.markModified();
													}
												}
												else {
													this.onFailure();
													this.getMsgTarget().unmask();
													TCG.data.ErrorHandler.handleFailure(this, result);
												}
											},
											error: function(form, action) {
												Ext.Msg.alert('Failed to load tag modifier filter', action.result.data);
											}
										});
										loader.load('fixTagModifierFilter.json');
									}
								}
							}
						});
						toolBar.add('-');
						toolBar.add({
							text: 'Add',
							tooltip: 'Add a new filter',
							iconCls: 'add',
							scope: this,
							handler: function() {
								const className = this.getDetailPageClass();
								TCG.createComponent(className, {
									openerCt: this,
									defaultIconCls: this.getWindow().iconCls
								});
							}
						});
						toolBar.add('-');
						toolBar.add({
							text: 'Remove',
							tooltip: 'Unlink selected filter',
							iconCls: 'remove',
							scope: this,
							handler: function() {
								const index = this.getSelectionModel().getSelectedCell();
								if (index) {
									const store = this.getStore();
									const rec = store.getAt(index[0]);
									store.remove(rec);
									this.markModified();
								}
							}
						});
					}
				}
			]
		}
	]
});
