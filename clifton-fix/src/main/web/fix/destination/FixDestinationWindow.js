Clifton.fix.destination.FixDestinationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Fix Destination',
	iconCls: 'shopping-cart-blue',


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Destination',
				items: [{
					xtype: 'formpanel',
					url: 'fixDestination.json',
					instructions: 'FIX Destinations define a destination name to be associated with FIX Session Definition. If an incoming message specifies a destination name, then the system will look up the session information from the destination\'s SessionDefinition',
					items: [
						{fieldLabel: 'Destination Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Session Definition', name: 'sessionDefinition.name', hiddenName: 'sessionDefinition.id', displayField: 'name',
							xtype: 'combo', url: 'fixSessionDefinitionListFind.json', detailPageClass: 'Clifton.fix.session.FixSessionDefinitionWindow'
						},
						{fieldLabel: 'Security System', name: 'securitySystem.name', hiddenName: 'securitySystem.id', xtype: 'combo', url: 'securitySystemListFind.json', detailPageClass: 'Clifton.security.system.SystemWindow', detailIdField: 'securitySystem.id', qtip: 'Used to map our user (Trader) names to the corresponding name used by the external system/provider.'},
						{boxLabel: 'Allow Anonymous Access', name: 'allowAnonymousAccess', xtype: 'checkbox', qtip: 'True if messages sent using this destination should succeed even if the given sender user name is not mapped to an external alias.'},
						{boxLabel: 'Use Username For Anonymous Access', name: 'useUsernameForAnonymousAccess', xtype: 'checkbox', qtip: 'True if messages sent using this destination should populate sender sub ID with the sender user name if it is not mapped to an alias.', requiredFields: ['allowAnonymousAccess']},
						{boxLabel: 'Default Destination', name: 'defaultDestination', xtype: 'checkbox', qtip: 'True if this destination should be used as the default destination for messages using this session definition. Some messages (i.e. drop copies) may not be correlated with any previous outbound messages from which they could get the correct destination. In this case, the default destination for the message\'s session would be attached before it is sent to the appropriate source system.'}
					]
				}]
			},


			{
				title: 'Tag Modifiers',
				items: [{
					name: 'fixDestinationTagModifierListFind',
					xtype: 'gridpanel',
					additionalPropertiesToRequest: 'referenceTwo.id',
					appendStandardColumns: false,
					instructions: 'The following tag modifiers are applied to this fix destination, and applied in the specified order.  Each tag modifier uses tag modifier filters to determine if it applies to the specific message.',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true, width: 10},
						{header: 'Modifier Name', dataIndex: 'referenceTwo.name', width: 70},
						{header: 'Description', dataIndex: 'referenceTwo.description', width: 100, renderer: TCG.renderValueWithTooltip, hidden: true},
						{header: 'Tag Type', width: 50, dataIndex: 'referenceTwo.type.name', filter: {searchFieldName: 'typeName'}},
						{header: 'Order', dataIndex: 'order', type: 'int', defaultSortColumn: true, width: 20}
					],
					editor: {
						detailPageClass: 'Clifton.fix.destination.FixDestinationTagModifierFilterWindow',
						getDetailPageParams: function(id) {
							const gridPanel = this.getGridPanel();
							const items = gridPanel.grid.store.data.items;
							let modifierId = undefined;
							for (let i = 0; i < items.length; i++) {
								if (TCG.isEquals(id, items[i].id)) {
									modifierId = items[i].json.referenceTwo.id;
									break;
								}
							}
							return {
								fixDestinationId: this.getWindow().getMainFormId(),
								fixTagModifierId: modifierId
							};
						},
						addToolbarAddButton: function(toolBar, gridPanel) {
							toolBar.add(new TCG.form.ComboBox({name: 'fixTagModifier', url: 'systemBeanListFind.json?groupName=FIX Tag Modifier', emptyText: '< Select Tag Modifier >', loadAll: false, disableAddNewItem: true, width: 150, listWidth: 230}));
							toolBar.add(' ');
							toolBar.add({
								text: 'Link',
								tooltip: 'Link an existing fix tag modifier to this fix destination.',
								iconCls: 'link',
								scope: gridPanel,
								handler: function() {
									const tagModifier = TCG.getChildByName(toolBar, 'fixTagModifier');
									const tagModifierId = tagModifier.getValue();
									const fixDestinationId = this.getWindow().getMainFormId();

									tagModifier.clearValue();

									const params = {};
									params['fixDestinationId'] = fixDestinationId;
									params['fixTagModifierId'] = tagModifierId;

									if (tagModifierId === '') {
										TCG.showError('You must first select desired tag modifier from the list.');
									}
									else {
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											timeout: 120000,
											waitMsg: 'Saving: Please Wait.',
											params: params,
											scope: gridPanel,
											success: function(form, action) {
												const result = Ext.decode(form.responseText);
												if (result.success) {
													if (this.isUseWaitMsg()) {
														const msgTarget = this.getMsgTarget();
														msgTarget.mask('Saving Tag Modifier... Success');
														const unmaskFunction = function() {
															msgTarget.unmask();
														};
														unmaskFunction.defer(600, this);
														this.scope.reload();
													}
												}
												else {
													this.onFailure();
													this.getMsgTarget().unmask();
													TCG.data.ErrorHandler.handleFailure(this, result);
												}
											},
											error: function(form, action) {
												Ext.Msg.alert('Failed to save tag modifier to session definition', action.result.data);
											}
										});
										loader.load('fixDestinationToTagModifierLink.json');
									}
								}
							});
							toolBar.add('-');
						},
						addToolbarDeleteButton: function(toolBar, gridPanel) {
							toolBar.add({
								text: 'Delete',
								tooltip: 'Delete selected item(s)',
								iconCls: 'stop',
								scope: this,
								handler: function() {
									const editor = this;
									const sm = editor.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										if (editor.allowToDeleteMultiple) {
											Ext.Msg.confirm('Delete Selected Row(s)', 'Would you like to delete all selected rows(s)?', function(a) {
												if (a === 'yes') {
													editor.deleteSelection(sm, gridPanel, 0);
												}
											});
										}
										else {
											TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
										}
									}
									else {
										Ext.Msg.confirm('Delete Selected Row', 'Would you like to delete selected row?', function(a) {
											if (a === 'yes') {
												editor.deleteSelection(sm, gridPanel, 0);
											}
										});
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Add',
								tooltip: 'Add a new Fix Tag Modifier and link it to this Fix Destination.',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const cmpId = TCG.getComponentId('Clifton.fix.destination.FixDestinationTagModifierFilterWindow');
									const defaultData = {
										fixDestination: {
											name: this.getWindow().getMainForm().formValues.name,
											id: this.getWindow().getMainFormId()
										},
										fixTagModifier: {},
										fixTagModifierFilterList: []
									};
									TCG.createComponent('Clifton.fix.destination.FixDestinationTagModifierFilterWindow', {
										id: cmpId,
										defaultData: defaultData,
										params: undefined,
										openerCt: gridPanel,
										defaultIconCls: gridPanel.getWindow().iconCls,
										defaultActiveTabName: undefined
									});
								}
							});
							toolBar.add('-');
						}
					},
					getLoadParams: function() {
						return {fixDestinationId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
