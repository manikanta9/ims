Clifton.fix.FixDashboardWindow = Ext.extend(TCG.app.Window, {
	id: 'fixDashboardWindow',
	title: 'FIX Dashboard',
	iconCls: 'chart-bar',
	enableRefreshWindow: true,
	width: 1500,
	height: 810,

	items: [{
		xtype: 'panel',
		layout: 'table',
		layoutConfig: {
			columns: 3,
			tableAttrs: {
				style: {'table-layout': 'fixed', width: '100%'},
				cellspacing: 3
			}
		},
		defaults: {height: 368},

		listeners: {
			afterrender: function(p) {
				TCG.getChildByName(p, 'timeInterval').updateDates('0'); // 'Today'
			}
		},

		getQueryParameters: function(includeGroupBy) {
			const params = {
				fromDate: TCG.getChildByName(this, 'fromDate').getValue().format('m/d/Y'),
				toDate: TCG.getChildByName(this, 'toDate').getValue().format('m/d/Y'),
				messageScope: TCG.getChildByName(this, 'messageScope').getValue()
			};
			let o = TCG.getChildByName(this, 'sessionDefinition').getValue();
			if (TCG.isNotBlank(o)) {
				params.sessionDefinitionId = o;
			}
			o = TCG.getChildByName(this, 'messageType').getValue();
			if (TCG.isNotBlank(o)) {
				params.messageTypeId = o;
			}
			if (includeGroupBy && TCG.getChildByName(this, 'groupBy').getValue() === 'SESSION') {
				params.groupBySessionDefinition = true;
			}
			return params;
		},

		items: [
			{
				xtype: 'panel',
				layout: 'hbox',
				bodyStyle: 'padding: 3px 0px 0px 3px',
				defaults: {},
				colspan: 3,
				height: 30,
				items: [
					{xtype: 'displayfield', value: 'Time Interval:', style: 'padding: 5px 5px 0px 5px;'},
					{
						name: 'timeInterval', xtype: 'combo', width: 105, minListWidth: 105, value: 'Today', mode: 'local', store: {xtype: 'arraystore', data: [['0', 'Today'], ['1', 'Last 2 Days'], ['7', 'Last Week'], ['30', 'Last Month'], ['180', 'Last 6 Months'], ['365', 'Last Year'], ['730', 'Last 2 Years'], ['14', 'Custom...']]},
						listeners: {
							beforeselect: function(combo, record) {
								combo.updateDates(record.data.id);
							}
						},
						updateDates: function(value) {
							const fromDate = new Date().add(Date.DAY, -parseInt(value));
							const toDate = new Date();
							const p = TCG.getParentByClass(this, Ext.Panel);
							TCG.getChildByName(p, 'fromDate').setValue(fromDate.format('m/d/Y'));
							TCG.getChildByName(p, 'toDate').setValue(toDate.format('m/d/Y'));
						}
					},
					{xtype: 'displayfield', value: 'From:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'fromDate', xtype: 'datefield', width: 95, allowBlank: false},
					{xtype: 'displayfield', value: 'To:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'toDate', xtype: 'datefield', width: 95, allowBlank: false},

					{xtype: 'displayfield', value: 'Session Definition:', style: 'padding: 5px 5px 0px 40px;'},
					{name: 'sessionDefinition', xtype: 'combo', width: 170, minListWidth: 230, url: 'fixSessionDefinitionListFind.json', displayField: 'label'},
					{xtype: 'displayfield', value: 'Message Type:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'messageType', xtype: 'combo', width: 170, minListWidth: 230, url: 'fixMessageEntryTypeListFind.json'},
					{xtype: 'displayfield', value: 'Messages:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'messageScope', xtype: 'combo', width: 90, minListWidth: 90, value: 'NONADMIN', mode: 'local', store: {xtype: 'arraystore', data: [['ALL', 'All', 'Include all messages: administrative and non-administrative.'], ['ADMIN', 'Admin', 'Limit message type to Administrative messages only.'], ['NONADMIN', 'Non Admin', 'Exclude Administrative messages.']]}},
					{xtype: 'displayfield', value: 'Group By:', style: 'padding: 5px 5px 0px 10px;'},
					{name: 'groupBy', xtype: 'combo', width: 120, minListWidth: 120, value: 'SESSION', mode: 'local', store: {xtype: 'arraystore', data: [['SESSION', 'Session Definition', 'Group historical message counts by Session Definition.'], ['MSGTYPE', 'Message Type', 'Group historical message counts by Message Type.']]}},

					{
						xtype: 'button', text: 'Reload', iconCls: 'table-refresh', width: 80, style: 'margin-left: 35px;',
						handler: function(btn) {
							const items = TCG.getParentByClass(TCG.getParentByClass(btn, Ext.Panel), Ext.Panel).items;
							items.each(o => {
								if (o.reload) {
									o.reload();
								}
							});
						}
					}
				]
			},


			{
				xtype: 'system-query-doughnut-chart',
				title: 'FIX Message Counts by Session Definition',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters();
				},
				onClickEvent: function(data, label, datasetLabel, fullData) {
					const params = this.queryParams();
					TCG.createComponent('Clifton.fix.FixMessageListWindow', {
						defaultActiveTabName: 'FIX Messages',
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDate(params.fromDate, 'm/d/Y'),
							toDate: TCG.parseDate(params.toDate, 'm/d/Y'),
							sessionDefinitionLabel: label
						}
					});
				}
			},
			{
				xtype: 'system-query-doughnut-chart',
				title: 'FIX Message Counts by Message Type',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters();
				},
				onClickEvent: function(data, label, datasetLabel) {
					const params = this.queryParams();
					TCG.createComponent('Clifton.fix.FixMessageListWindow', {
						defaultActiveTabName: 'FIX Messages',
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDate(params.fromDate, 'm/d/Y'),
							toDate: TCG.parseDate(params.toDate, 'm/d/Y'),
							messageTypeName: label
						}
					});
				}
			},
			{
				xtype: 'system-query-doughnut-chart',
				title: 'FIX Message Counts by Source System',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters();
				},
				onClickEvent: function(data, label, datasetLabel) {
					const params = this.queryParams();
					TCG.createComponent('Clifton.fix.FixMessageListWindow', {
						defaultActiveTabName: 'FIX Messages',
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDate(params.fromDate, 'm/d/Y'),
							toDate: TCG.parseDate(params.toDate, 'm/d/Y'),
							sourceSystemName: label
						}
					});
				}
			},


			{
				xtype: 'system-query-stackedbar-chart',
				title: 'Historical FIX Message Counts',
				queryName: 'FIX Message Counts',
				queryParams: function() {
					return TCG.getParentByClass(this, Ext.Panel).getQueryParameters(true);
				},
				colspan: 3,
				onClickEvent: function(data, label, datasetLabel) {
					// find the next label
					let toDate;
					const labels = this.chart.data.labels;
					for (let i = 0; i < labels.length; i++) {
						if (labels[i] === label) {
							i++;
							if (i < labels.length) {
								toDate = labels[i];
							}
							break;
						}
					}
					TCG.createComponent('Clifton.fix.FixMessageListWindow', {
						defaultActiveTabName: 'FIX Messages',
						defaultData: {
							forceReload: true,
							fromDate: TCG.parseDateSmart(label),
							toDate: TCG.parseDateSmart(toDate || this.queryParams().toDate)
						}
					});
				}
			}
		]
	}]
});
