Clifton.fix.tag.FixTagModifierTagModifierFilterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'FIX Destination Tag Modifier Filter',
	iconCls: 'shopping-cart-blue',
	width: 600,
	height: 200,

	items: [{
		xtype: 'formpanel',
		instructions: 'Use an existing Tag Modifier Filter on this FIX Destination-specific modifier.',
		labelWidth: 130,

		items: [
			{
				fieldLabel: 'Tag Modifier',
				name: 'name',
				xtype: 'linkfield',
				detailIdField: 'id',
				detailPageClass: 'Clifton.fix.tag.FixTagModifierWindow'
			},
			{
				fieldLabel: 'FixDestination',
				name: 'fixDestinationId',
				hiddenName: 'fixDestinationId',
				xtype: 'combo',
				displayField: 'name',
				url: 'fixDestinationListFind.json',
				allowBlank: false,
				disableAddNewItem: true
			},
			{
				fieldLabel: 'Tag Modifier Filter',
				name: 'fixTagModifierFilter',
				hiddenName: 'fixTagModifierFilterId',
				xtype: 'combo',
				displayField: 'name',
				url: 'fixTagModifierFilterListFind.json',
				allowBlank: false,
				disableAddNewItem: true
			}
		],
		getSaveURL: function() {
			return 'fixTagModifierTagModifierFilterToTagModifierFilterLink.json';
		}
	}]
});
