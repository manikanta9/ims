Clifton.fix.tag.FixTagModifierFilterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Fix Tag Modifier Filter',
	iconCls: 'shopping-cart-blue',
	width: 700,
	height: 500,

	items: [
		{
			xtype: 'tabpanel',
			requiredFormIndex: 0,
			items: [
				{
					title: 'FIX Tag Modifier Filter',
					items: [
						{
							xtype: 'formpanel',
							url: 'fixTagModifierFilter.json',
							instructions: 'Filters are used to limit FIX messages that a particular tag modifier should apply to.  The field value is a Regular Expression.',
							getDataAfterSave: function(data) {
								return TCG.data.getData(this.url + '?id=' + data.id, this);
							},
							items: [
								{fieldLabel: 'Filter Name', name: 'name'},
								{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
								{fieldLabel: 'Field Tag', name: 'fieldTag', xtype: 'integerfield'},
								{fieldLabel: 'Field Value', name: 'fieldValue', qtip: 'Regular Expression to match for the field value'}
							]
						}]
				},


				{
					title: 'Used By',
					items: [{
						name: 'fixDestinationTagModifierFilterListFind.json',
						xtype: 'gridpanel',
						instructions: 'A list of FIX Tag Modifiers using this tag modifier filter.  To add this tag modifier filter to a new a session tag modifier, please navigate to the Fix Destination window.',
						additionalPropertiesToRequest: 'referenceOne.referenceOne.id|referenceOne.referenceTwo.id',
						columns: [
							{header: 'ID', dataIndex: 'id', hidden: true, width: 20, filter: false, sortable: false},
							{header: 'FIX Destination Name', dataIndex: 'referenceOne.referenceOne.name', width: 100, filter: {searchFieldName: 'fixDestinationId', type: 'combo', url: 'fixDestinationListFind.json'}, defaultSort: true, defaultSortAsc: true},
							{header: 'Modifier Name', dataIndex: 'referenceOne.referenceTwo.name', width: 100, filter: {searchFieldName: 'fixTagModifierId', type: 'combo', url: 'systemBeanListFind.json?groupName=FIX Tag Modifier'}},
							{header: 'Modifier Description', dataIndex: 'referenceOne.referenceTwo.description', hidden: true, width: 100, filter: false, sortable: false},
							{header: 'Order', dataIndex: 'referenceOne.order', width: 40, type: 'int', filter: {searchFieldName: 'fixSessionDefinitionTagModifierOrder'}}
						],
						editor: {
							detailPageClass: 'Clifton.fix.destination.FixDestinationTagModifierFilterWindow',
							drillDownOnly: true,
							getDetailPageParams: function(id) {
								const gridPanel = this.getGridPanel();
								const items = gridPanel.grid.store.data.items;
								let fixDestinationId = undefined;
								let tagModifierId = undefined;
								for (let i = 0; i < items.length; i++) {
									if (TCG.isEquals(id, items[i].id)) {
										fixDestinationId = items[i].json.referenceOne.referenceOne.id;
										tagModifierId = items[i].json.referenceOne.referenceTwo.id;
										break;
									}
								}
								return {
									fixDestinationId: fixDestinationId,
									fixTagModifierId: tagModifierId
								};
							}
						},
						getLoadParams: function() {
							return {fixTagModifierFilterId: this.getWindow().getMainFormId()};
						}
					}]
				}
			]
		}]
});
