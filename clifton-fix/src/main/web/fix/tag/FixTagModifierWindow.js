Clifton.fix.tag.FixTagModifierWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'FIX Tag Modifier',
	iconCls: 'shopping-cart-blue',
	width: 800,
	height: 400,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'FIX Tag Modifier',
				items: [{
					xtype: 'formwithdynamicfields',
					instructions: 'A Fix Tag Modifier is a system bean that represents a specific instance of corresponding system bean type within the Tag Modifier group.',
					getWarningMessage: function() {
						return 'Tag Modifiers are <b>global</b> entities, so changing tag modifier properties here will affect other fix destinations if they are used within them as well.';
					},
					url: 'systemBean.json',
					labelWidth: 170,
					childTables: 'SystemBeanProperty', // used by audit trail

					dynamicTriggerFieldName: 'type.label',
					dynamicTriggerValueFieldName: 'type.id',
					dynamicFieldTypePropertyName: 'type',
					dynamicFieldListPropertyName: 'propertyList',
					dynamicFieldsUrl: 'systemBeanPropertyTypeListByBean.json',
					dynamicFieldsUrlParameterName: 'type.id',

					dynamicFieldsUrlIdFieldName: 'id',
					dynamicFieldsUrlListParameterName: 'propertyList',

					items: [
						{fieldLabel: 'Bean Group (Interface)', name: 'type.group.name', submitValue: false, xtype: 'linkfield', detailPageClass: 'Clifton.system.bean.GroupWindow', detailIdField: 'type.group.id'},
						{
							fieldLabel: 'Bean Type (Class)', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'combo',
							url: 'systemBeanTypeListFind.json?groupName=FIX Tag Modifier',
							detailPageClass: 'Clifton.system.bean.TypeWindow',
							disableAddNewItem: true,
							listeners: {
								// reset object bean property fields on changes to bean type (triggerField)
								select: function(combo, record, index) {
									combo.ownerCt.resetDynamicFields(combo, false);
								},
								beforequery: function(queryEvent) {
									const form = queryEvent.combo.getParentForm().getForm();
									const groupId = form.findField('type.group.id').getValue();
									const groupName = form.findField('type.group.name').getValue();
									const queryParams = {};
									if (groupName) {
										queryParams.groupName = groupName;
									}
									if (groupId) {
										queryParams.groupId = groupId;
									}
									queryEvent.combo.store.baseParams = queryParams;
								}
							}
						},
						{fieldLabel: 'Modifier Bean Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'label', html: '<hr/>'}
					],

					listeners: {
						afterload: function(form, isClosing) {
							// for created objects replace combo with linkfield: can't edit
							const triggerField = this.getForm().findField('type.label');
							if (this.getIdFieldValue() > 0 && triggerField.xtype !== 'linkfield') {
								this.remove(triggerField, true);

								const formValues = this.getForm().formValues;
								this.add({name: 'type.id', xtype: 'hidden', value: TCG.getValue('type.id', formValues)});
								this.insert(3, {
									fieldLabel: triggerField.fieldLabel,
									name: 'type.label',
									submitValue: false,
									xtype: 'linkfield',
									detailPageClass: 'Clifton.system.bean.TypeWindow',
									detailIdField: 'type.id',
									value: TCG.getValue('type.label', formValues)
								});
								try {
									this.doLayout();
								}
								catch (e) {
									// strange error due to removal of elements with allowBlank = false
								}
							}
						}
					}
				}]
			},


			{
				title: 'Used By',
				items: [{
					name: 'fixDestinationTagModifierListFind',
					xtype: 'gridpanel',
					instructions: 'This tag modifier is used by the following FIX Session Definitions.  To add or remove this tag modifier from a session, please navigate to the Session Definition window.',
					additionalPropertiesToRequest: 'referenceOne.id',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true, width: 10},
						{header: 'Fix Destination', dataIndex: 'referenceOne.name', width: 100, filter: {searchFieldName: 'fixSessionDefinitionId', url: 'fixSessionDefinitionListFind.json', type: 'combo'}, defaultSort: true, defaultSortAsc: true},
						{header: 'Order', dataIndex: 'order', width: 20, type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.fix.destination.FixDestinationTagModifierFilterWindow',
						drillDownOnly: true,
						getDetailPageParams: function(id) {
							const gridPanel = this.getGridPanel();
							const items = gridPanel.grid.store.data.items;
							let fixDestinationId = undefined;
							for (let i = 0; i < items.length; i++) {
								if (TCG.isEquals(id, items[i].id)) {
									fixDestinationId = items[i].json.referenceOne.id;
									break;
								}
							}
							return {
								fixDestinationId: fixDestinationId,
								fixTagModifierId: this.getWindow().getMainFormId()
							};
						}
					},
					getLoadParams: function() {
						return {fixTagModifierId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
