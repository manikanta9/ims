Clifton.fix.FixSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'fixSetupWindow',
	title: 'FIX Setup',
	iconCls: 'tools',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Session Definitions',
				items: [{
					name: 'fixSessionDefinitionListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					remoteSort: true, // Otherwise UI changes sort on name column which should be hierarchy order
					instructions: 'A list of all FIX Session Definitions defined in the system. The parent session settings will be applied to the child then overridden by the child-specific session',
					wikiPage: 'IT/FIX Trouble Shooting',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{
							header: 'Definition Name', dataIndex: 'name', width: 120, filter: {sortFieldName: 'nameExpanded'},
							renderer: function(v, metaData, r) {
								if (TCG.isBlank(r.data['parent.name'])) {
									return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
								}
								return '<div style="FONT-WEIGHT: bold; COLOR: #777777; PADDING-LEFT: 15px">' + v + '</div>';
							}
						},
						{header: 'Session Label', dataIndex: 'label', width: 100},
						{header: 'Connectivity Type', dataIndex: 'connectivityType.text', width: 70, filter: {searchFieldName: 'connectivityTypeId', type: 'combo', displayField: 'text', url: 'systemListItemListFind.json?listName=FIX Connectivity Types'}},
						{header: 'Description', dataIndex: 'description', width: 100, hidden: true},
						{header: 'Sequence # Batch Size', dataIndex: 'saveSequenceNumberBatchSize', width: 40, type: 'int', useNull: true, tooltip: 'Save sequence number updates to the database after selected number of increments to either incoming or outgoing sequence numbers. Uses value as specified on parent session definition if blank.', filter: {searchFieldName: 'saveSequenceNumberBatchSize'}},
						{header: 'Non-Persistent Cache Size', dataIndex: 'nonPersistentMessageCacheSize', hidden: true, width: 40, type: 'int', useNull: true, tooltip: 'The maximum number of non-persistent messages which will be retained for this session.', filter: {searchFieldName: 'saveSequenceNumberBatchSize'}},
						{header: 'Parent Session', dataIndex: 'parent.name', hidden: true, width: 50, tooltip: 'The parent session settings will be applied to the child then overridden by the child-specific session'},

						{header: 'BeginString', dataIndex: 'beginString', hidden: true, tooltip: 'The FIX version string.  FIX.4.2,FIX.4.4, etc.'},
						{header: 'SenderCompId', dataIndex: 'senderCompId', hidden: true, tooltip: 'The sending company id.'},
						{header: 'SenderSubId', dataIndex: 'senderSubId', hidden: true, tooltip: 'For REDI+, this field is the user name of the trader'},
						{header: 'SenderLocId', dataIndex: 'senderLocId', hidden: true, tooltip: ''},
						{header: 'TargetCompId', dataIndex: 'targetCompId', hidden: true, tooltip: 'The receiving company id.'},
						{header: 'TargetSubId', dataIndex: 'targetSubId', hidden: true, tooltip: 'The target on for the counter party.  For example, for a REDI+ ticket the value in this field is "TKTS".  For Direct Market Order, this field would be "DMA".  This value is taken from the FIX Target on the trade destination.'},
						{header: 'TargetLocId', dataIndex: 'targetLocId', hidden: true, tooltip: ''},
						{header: 'SessionQualifier', dataIndex: 'sessionQualifier', hidden: true, qtip: 'FIX session qualifier used to distinguish other wise identical sessions.'},

						{header: 'Message Queue', dataIndex: 'messageQueue', width: 50, hidden: true, tooltip: 'The message queue used by this session'},
						{header: 'Unknown Msg Source System', dataIndex: 'unknownMessageFixSourceSystem.label', width: 50, hidden: true, tooltip: 'The source system to use for unknown messages received by this session.  For example, drop copy messages sent from Bloomberg session CLIFTONPARA->BLP need to be sent to IMS.', filter: {searchFieldName: 'unknownMessageFixSourceSystemId', type: 'combo', url: 'fixSourceSystemListFind.json', displayField: 'label'}},
						{header: 'Connection Type', dataIndex: 'connectionType', width: 35, tooltip: 'Either "acceptor or initiator"'},
						{header: 'Time Zone', dataIndex: 'timeZone', width: 50, tooltip: 'The time zone of this connection', hidden: true},
						{header: 'Weekly Session', dataIndex: 'weeklySession', type: 'boolean', width: 35, tooltip: 'A weekly session starts on  the starting weekday at the start time, and runs without resets until the end time on the ending weekday.'},
						{header: 'Start Time', dataIndex: 'startTime', width: 35, tooltip: 'The time of day that the session will begin. Time must be in the same time as the above time zone', hidden: true},
						{header: 'End Time', dataIndex: 'endTime', width: 35, tooltip: 'The time of day that the session will end. Time must be in the same time as the above time zone', hidden: true},
						{
							header: 'Starting Weekday', dataIndex: 'startWeekday', width: 35, tooltip: 'The day of the week that the session will begin.', hidden: true,
							renderer: value => {
								const matchingData = Clifton.fix.CalendarWeekdays.filter(data => data[0] === value);
								return matchingData.length === 1
									? matchingData[0][1]
									: value;
							}
						},
						{
							header: 'Ending Weekday', dataIndex: 'endWeekday', width: 35, tooltip: 'The day of the week that the session will end.', hidden: true,
							renderer: value => {
								const matchingData = Clifton.fix.CalendarWeekdays.filter(data => data[0] === value);
								return matchingData.length === 1
									? matchingData[0][1]
									: value;
							}
						},
						{header: 'Heartbeat Interval', dataIndex: 'heartbeatInterval', width: 40, type: 'int', useNull: true, tooltip: 'The time period in seconds between heartbeats'},
						{header: 'Reconnect Interval', dataIndex: 'reconnectInterval', width: 40, type: 'int', useNull: true, tooltip: 'The time period in seconds between connection attempts'},
						{header: 'Session Dictionary Location', dataIndex: 'sessionDictionaryLocation', width: 200, tooltip: 'Optional location of the FIX Session Dictionary file if not using the default.', hidden: true},
						{header: 'Socket Host', dataIndex: 'socketConnectHost', width: 50, tooltip: 'The host address'},
						{header: 'Socket Port', dataIndex: 'socketConnectPort', width: 35, tooltip: 'The host port'},
						{header: 'Path To Executable', dataIndex: 'pathToExecutable', width: 50, tooltip: 'Path to an executable that needs to be started before the session'},
						{header: 'Default Version', dataIndex: 'defaultApplVerId', width: 50, tooltip: 'The default FIX version', hidden: true},
						{header: 'Executable Argument', dataIndex: 'executableArgument', width: 50, tooltip: 'Arguments for starting the executable', hidden: true},
						{header: 'Create Session', dataIndex: 'createSession', width: 35, type: 'boolean', tooltip: 'Indicates if an actual session can be created from these settings or if it\'s used as a parent for default settings.'},
						{header: 'Informational', dataIndex: 'informational', width: 35, type: 'boolean', tooltip: 'Informational sessions will simply log messages and not send them to any external applications.'},
						{header: 'Disabled', dataIndex: 'disabled', width: 35, type: 'boolean', tooltip: 'If this definition is disabled, so no session will be created for it'}

					],
					editor: {
						detailPageClass: 'Clifton.fix.session.FixSessionDefinitionWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'FIX Sessions',
				items: [{
					xtype: 'fix-sessionGrid'
				}]
			},


			{
				title: 'FIX Destinations',
				items: [{
					name: 'fixDestinationListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'A list of all FIX destinations defined in the system. If an incoming message specifies a destination name, then the system will look up the session information from the destination\'s defined Session Definition',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{header: 'Destination Name', dataIndex: 'name', width: 95, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 150, hidden: true},
						{header: 'Session Definition', dataIndex: 'sessionDefinition.name', width: 95, filter: {sortFieldName: 'sessionDefinitionName'}},
						{header: 'Security System', width: 50, dataIndex: 'securitySystem.name'},
						{header: 'Allow Anonymous', dataIndex: 'allowAnonymousAccess', type: 'boolean', width: 30, tooltip: 'True if messages sent using this destination should succeed even if the given sender user name is not mapped to an alias.'},
						{header: 'Username for Anonymous', dataIndex: 'useUsernameForAnonymousAccess', type: 'boolean', width: 35, tooltip: 'True if messages sent using this destination should populate sender sub ID with the sender user name if it is not mapped to an alias.'},
						{header: 'Default', dataIndex: 'defaultDestination', type: 'boolean', width: 20, tooltip: 'True if this destination should be used as the default destination for messages using this session definition.'}
					],
					editor: {
						detailPageClass: 'Clifton.fix.destination.FixDestinationWindow'
					}
				}]
			},


			{
				title: 'Tag Modifiers',
				items: [{
					name: 'systemBeanListFind.json?groupName=FIX Tag Modifier',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					instructions: 'Tag Modifiers are used to alter FIX message fields based on the provided filters. These customizations can include adding, changing, swapping, and removing tags, as well as some more Broker Customizations.  These global tag modifiers can be assigned to a specific session definition and given an order to control the change process (if multiple were to apply).',
					wikiPage: 'IT/FIX Tag Modifiers',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 10, hidden: true},
						{header: 'Modifier Name', dataIndex: 'name', defaultSortColumn: true, width: 75},
						{header: 'Description', dataIndex: 'description', width: 150},
						{header: 'Tag Type', width: 50, dataIndex: 'type.name', filter: {searchFieldName: 'typeName'}}
					],
					editor: {
						detailPageClass: 'Clifton.fix.tag.FixTagModifierWindow'
					}
				}]
			},


			{
				title: 'Fix Destination Tag Modifiers',
				items: [{
					name: 'fixDestinationTagModifierListFind',
					xtype: 'gridpanel',
					additionalPropertiesToRequest: 'referenceOne|referenceTwo',
					appendStandardColumns: false,
					instructions: 'The following tag modifiers are applied to this fix destination, and applied in the specified order.  Each tag modifier uses tag modifier filters to determine if it applies to the specific message.',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true, width: 10},
						{header: 'Fix Destination', dataIndex: 'referenceOne.name', width: 50, filter: {searchFieldName: 'fixDestinationName'}},
						{header: 'Modifier Name', dataIndex: 'referenceTwo.name', width: 70, filter: {searchFieldName: 'fixTagModifierName'}},
						{header: 'Description', dataIndex: 'referenceTwo.description', width: 100, filter: {searchFieldName: 'fixTagModifierDescription'}, renderer: TCG.renderValueWithTooltip, hidden: true},
						{header: 'Tag Type', dataIndex: 'referenceTwo.type.name', width: 50, filter: {searchFieldName: 'fixTagModifierTypeName'}},
						{header: 'Order', dataIndex: 'order', type: 'int', defaultSortColumn: true, width: 10}
					],
					editor: {
						detailPageClass: 'Clifton.fix.destination.FixDestinationTagModifierFilterWindow',

						getDetailPageParams: function(id) {
							const gridPanel = this.getGridPanel();
							const items = gridPanel.grid.store.data.items;
							let modifierId = undefined;
							let destinationId = undefined;
							for (let i = 0; i < items.length; i++) {
								if (TCG.isEquals(id, items[i].id)) {
									modifierId = items[i].json.referenceTwo.id;
									destinationId = items[i].json.referenceOne.id;
									break;
								}
							}
							return {
								fixDestinationId: destinationId,
								fixTagModifierId: modifierId
							};
						}
					}
				}]
			},


			{
				title: 'Tag Modifier Filters',
				items: [{
					name: 'fixTagModifierFilterListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					appendStandardColumns: false,
					instructions: 'Tag modifier filters are used to filter messages based on a specific tag and value.  These filters can be associated with FIX Session Definition specific tag modifiers to determine if the tag modifier applies for a specific message for that session.',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 10, hidden: true},
						{header: 'Filter Name', dataIndex: 'name', width: 75, defaultSortColumn: true},
						{header: 'Description', dataIndex: 'description', width: 150, renderer: TCG.renderValueWithTooltip},
						{header: 'Field Tag', dataIndex: 'fieldTag', width: 25, type: 'int'},
						{header: 'Field Value', dataIndex: 'fieldValue', width: 75}
					],
					editor: {
						detailPageClass: 'Clifton.fix.tag.FixTagModifierFilterWindow'
					}
				}]
			},


			{
				title: 'FIX Source Systems',
				items: [{
					name: 'fixSourceSystemListFind',
					xtype: 'gridpanel',
					instructions: 'FIX Source Systems are internal systems the that Fix application sends and receives FIX messages to external parties on behalf of.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', dataIndex: 'id', width: 10, hidden: true},
						{header: 'Source System Name', dataIndex: 'name', width: 75, defaultSortColumn: true},
						{header: 'Source System Description', dataIndex: 'description', width: 125},
						{header: 'Source System Tag', dataIndex: 'systemTag', width: 60},
						{header: 'Request Queue Name', dataIndex: 'requestQueueName', width: 70, tooltip: 'Where the source system will place request messages for Fix to consume.'},
						{header: 'Response Queue Name', dataIndex: 'responseQueueName', width: 70, tooltip: 'Where FIX will place messages for the source system to consume.'},
						{header: 'Entity Serialization', dataIndex: 'serializeEntity', width: 40, type: 'boolean', tooltip: 'If enabled, Fix Messages are converted to Fix Entities during serialization.'},
						{header: 'Format', dataIndex: 'messageFormat', width: 20, tooltip: 'The serialization format for outgoing messages.'},
						{
							header: 'Sender Provider', dataIndex: 'messageSenderFactoryProviderBean.name', width: 60, filter: {searchFieldName: 'messageSenderFactoryProviderBeanId', type: 'combo', url: 'systemBeanListFind.json?groupName=Message Sender Provider'},
							tooltip: 'The message sender factory which will be used to send messages on the configured queues. This could be ActiveMQ, SQS, Kafka, or any other target.'
						}
					],

					editor: {
						detailPageClass: 'Clifton.fix.setup.FixSourceSystemWindow'
					}
				}]
			}
		]
	}]
});
