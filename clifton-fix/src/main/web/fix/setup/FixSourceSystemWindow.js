Clifton.fix.setup.FixSourceSystemWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Fix Source System',
	iconCls: 'shopping-cart-blue',
	width: 900,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Source System',
				items: [{
					xtype: 'formpanel',
					url: 'fixSourceSystem.json',
					instructions: 'FIX Source Systems are internal systems the that Fix application sends and receives FIX messages to external parties on behalf of.',
					labelWidth: 150,
					labelFieldName: 'label',
					items: [
						{fieldLabel: 'Source System Name', name: 'name'},
						{fieldLabel: 'Source System Tag', name: 'systemTag', qtip: 'The system tag can appear on messages so we know what system the message belongs to.', readOnly: true},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'sectionheaderfield', header: 'Queues'},
						{fieldLabel: 'Request Queue Name', name: 'requestQueueName', qtip: 'Where the source system will place request messages for Fix to consume.', readOnly: true},
						{fieldLabel: 'Response Queue Name', name: 'responseQueueName', qtip: 'Where FIX will place messages for the source system to consume.', readOnly: true},
						{fieldLabel: 'Use Entity Serialization', name: 'serializeEntity', xtype: 'checkbox', qtip: 'If enabled, Fix Messages are converted to Fix Entities during serialization. This is useful for source systems which are unable to convert Fix Messages into Fix Entities themselves. Such conversion is typically done by the Fix application, and clients such as IMS may perform conversion internally by calling Fix application APIs.'},
						{fieldLabel: 'Message Format', name: 'messageFormat', xtype: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.core.messaging.MESSAGE_TYPES}, qtip: 'The serialization format for outgoing messages.'},
						{
							fieldLabel: 'Sender Provider', beanName: 'messageSenderFactoryProviderBean', xtype: 'system-bean-combo', groupName: 'Message Sender Provider',
							qtip: 'The message sender factory which will be used to send messages on the configured queues. This could be ActiveMQ, SQS, Kafka, or any other target.'
						}
					]
				}]
			},


			{
				title: 'FIX Identifiers',
				items: [{
					name: 'fixIdentifierListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'FIX Identifier represents an identifier used by client systems to get unique IDs for their messages.',
					columns: [
						{header: 'ID', width: 35, dataIndex: 'id', defaultSortColumn: true, defaultSortDirection: 'DESC', hidden: true},
						{header: 'Unique Identifier', dataIndex: 'uniqueStringIdentifier', filter: {searchFieldName: 'uniqueStringIdentifierSearch'}, width: '70', tooltip: 'The unique id which was either generated or received for the message.  This string represents the exact value that will be in the id fields'},
						{header: 'Referenced Identifier', dataIndex: 'referencedIdentifier.uniqueStringIdentifier', filter: {searchFieldName: 'referencedUniqueStringIdentifierSearch'}, width: 70, tooltip: 'The identifier the this instance replaced.', hidden: true},
						{header: 'Source FK Field ID', dataIndex: 'sourceSystemFkFieldId', type: 'int', doNotFormat: true, width: 40, useNull: true, tooltip: 'The identity of the object on the source system, used to return the new FixIdentity object'},
						{header: 'FIX Destination', dataIndex: 'fixDestination.name', width: 70, filter: {type: 'combo', searchFieldName: 'fixDestinationId', displayField: 'name', url: 'fixDestinationListFind.json'}, tooltip: 'The FIX Destination that was present on the incoming message associated with this identifier.'},
						{header: 'Identifier Generated', dataIndex: 'identifierGenerated', type: 'boolean', width: 50, tooltip: 'True if the identifier was generated, false if it represents an identifier created by another system'},
						{header: 'Newly Created', dataIndex: 'newlyCreated', type: 'boolean', width: 45, tooltip: 'Local property used to indicate that the identifier was just created'}
					],
					getLoadParams: function() {
						return {sourceSystemId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.fix.identifier.FixIdentifierWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'FIX Messages',
				items: [{
					xtype: 'fix-messageGrid',
					columnOverrides: [
						{dataIndex: 'text', hidden: true}
					],
					applyAdditionalLoadParams: function(firstLoad, params) {
						params.sourceSystemIncludingUnknownId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			}
		]
	}]
});
