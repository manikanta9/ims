Clifton.fix.session.setting.FixSessionDefinitionSettingWindow = Ext.extend(TCG.app.DetailWindow, {

	titlePrefix: 'FIX Session Definition Setting',
	iconCls: 'tools',

	items: [{
		xtype: 'formpanel',
		url: 'fixSessionDefinitionSetting.json',
		instructions: 'FIX Session Definition Settings are a hash style (name/value) configuration to define various settings needed for the session. Each session will use the parent session definition settings and add or override with it\'s own settings.',
		labelWidth: 140,
		listeners: {
			afterrender: function() {
				this.updateWarningMessage('If the session is currently active, changes will not take effect until the session is reset.');
			}
		},

		confirmBeforeSaveMsgTitle: 'Update Setting',
		getConfirmBeforeSaveMsg: function() {
			const parentSessionId = this.getForm().findField('sessionDefinition.parent.id').getValue();
			if (!parentSessionId) {
				return 'Are you sure you want to add/update this setting for the Default Session?';
			}
			return '';
		},

		getFormLabel: function() {
			return this.getFormValue('settingName');
		},

		items: [
			{fieldLabel: 'Parent Session ID', name: 'sessionDefinition.parent.id', hidden: true},
			{fieldLabel: 'Session Definition Name', name: 'sessionDefinition.name', xtype: 'linkfield', detailIdField: 'sessionDefinition.id', detailPageClass: 'Clifton.fix.session.FixSessionDefinitionWindow'},
			{fieldLabel: 'Setting Name', name: 'settingName'},
			{fieldLabel: 'Setting Value', name: 'settingValue', xtype: 'script-textarea'}
		]
	}]
});
