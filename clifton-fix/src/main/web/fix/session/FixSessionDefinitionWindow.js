Clifton.fix.session.FixSessionDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Session Definition',
	iconCls: 'shopping-cart-blue',
	width: 800,
	height: 750,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Session Definition',
				items: [{
					xtype: 'formpanel',
					url: 'fixSessionDefinition.json',
					readOnly: false,
					labelWidth: 165,
					//instructions: '',
					items: [
						{fieldLabel: 'Parent Session', name: 'parent.name', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.fix.session.FixSessionDefinitionWindow', qtip: 'The parent session settings will be applied to the child then overridden by the child-specific session'},
						{fieldLabel: 'Definition Name', name: 'name', width: 60},
						{fieldLabel: 'Session Label', name: 'label', width: 60},
						{fieldLabel: 'Connectivity Type', name: 'connectivityType.text', hiddenName: 'connectivityType.id', xtype: 'system-list-combo', valueField: 'id', listName: 'FIX Connectivity Types'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},

						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Sequence # Batch Size', name: 'saveSequenceNumberBatchSize', xtype: 'integerfield', minValue: 1, qtip: 'Save sequence number updates to the database after selected number of increments to either incoming or outgoing sequence numbers. Uses value as specified on parent session definition if blank.'}
								]
							}, {
								rows: [
									{fieldLabel: 'Non-Pers. Cache Size', name: 'nonPersistentMessageCacheSize', xtype: 'integerfield', minValue: 1, qtip: 'The maximum number of non-persistent messages which will be retained for this session. Uses value as specified on the parent session definition if blank.'}
								]
							}]
						},

						{xtype: 'label', html: '<hr/>'},

						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'BeginString', name: 'beginString', qtip: 'The FIX version string.  FIX.4.2,FIX.4.4, etc.'},
									{fieldLabel: 'SenderCompId', name: 'senderCompId', qtip: 'The sending company id.'},
									{fieldLabel: 'SenderSubId', name: 'senderSubId', qtip: 'For REDI+, this field is the user name of the trader'},
									{fieldLabel: 'SenderLocId', name: 'senderLocId', qtip: ''}
								]
							}, {
								rows: [
									{fieldLabel: 'SessionQualifier', name: 'sessionQualifier', qtip: 'FIX session qualifier used to distinguish otherwise identical sessions.'},
									{fieldLabel: 'TargetCompId', name: 'targetCompId', qtip: 'The receiving company id.'},
									{fieldLabel: 'TargetSubId', name: 'targetSubId', qtip: 'The target on for the counter party.  For example, for a REDI+ ticket the value in this field is "TKTS".  For Direct Market Order, this field would be "DMA".  This value is taken from the FIX Target on the trade destination.'},
									{fieldLabel: 'TargetLocId', name: 'targetLocId', qtip: ''}
								]
							}]
						},


						{xtype: 'label', html: '<hr/>'},

						{fieldLabel: 'Default Version', name: 'defaultApplVerId', qtip: 'The default FIX application layer version ID. This only applies to FIXT 1.1 and newer transport versions, where the application layer version operates independently of the transport layer. See more information under the "Configuration" section of the QuickFIX/J manual at https://www.quickfixj.org/.'},
						{fieldLabel: 'Connection Type', name: 'connectionType', qtip: 'Either "acceptor or initiator".  Note: currently only initiator is used.', xtype: 'string-combo', stringArray: ['initiator', 'acceptor']},
						{fieldLabel: 'Message Queue', name: 'messageQueue', qtip: 'The message queue used by this session'},
						{fieldLabel: 'Unknown Msg Source System', name: 'unknownMessageFixSourceSystem.label', hiddenName: 'unknownMessageFixSourceSystem.id', xtype: 'combo', url: 'fixSourceSystemListFind.json', displayField: 'label', detailPageClass: 'Clifton.fix.setup.FixSourceSystemWindow', qtip: 'The source system to use for unknown messages received by this session.  For example, drop copy messages sent from Bloomberg session CLIFTONPARA->BLP need to be sent to IMS.'},

						{xtype: 'label', html: '<hr/>'},

						{
							fieldLabel: 'Time Zone', xtype: 'combo', name: 'timeZone', hiddenName: 'timeZone', displayName: 'name', valueField: 'value', allowBlank: true, mode: 'local', qtip: 'The time zone of this connection',
							store: new Ext.data.ArrayStore({
								fields: ['value', 'name'],
								data: Clifton.fix.AmericanTimeZones
							})
						},
						{
							boxLabel: 'Weekly Session (starts and ends on the weekday specified below; uncheck to restart Daily)', xtype: 'checkbox', name: 'weeklySession', qtip: 'If checked, this session becomes a weekly session that starts on the selected "Starting Weekday" at the specified start time and ends at the selected "Ending Weekday" at the specified end time.  If unchecked, the session is considered a daily session that is active beteeen the specified start and end times.'
						},
						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Starting Weekday', xtype: 'combo', name: 'startWeekdayName', hiddenName: 'startWeekday', allowBlank: true, mode: 'local', qtip: 'For weekly sessions this is the day of the week the session will begin. For daily sessions this value is used by health checks to establish a day range to determine if the session should be active on a given day.', store: {xtype: 'arraystore', data: Clifton.fix.CalendarWeekdays}},
									{fieldLabel: 'Ending Weekday', xtype: 'combo', name: 'endWeekdayName', hiddenName: 'endWeekday', allowBlank: true, mode: 'local', qtip: '\'For weekly sessions this is the day of the week the session will end. For daily sessions this value is used by health checks to establish a day range to determine if the session should be active on a given day.', store: {xtype: 'arraystore', data: Clifton.fix.CalendarWeekdays}},
									{fieldLabel: 'Heartbeat Interval', name: 'heartbeatInterval', qtip: 'The time period in seconds between heartbeats'}
								]
							}, {
								rows: [
									{fieldLabel: 'Start Time', name: 'startTime', emptyText: 'hh:mm:ss America/Chicago', regex: Clifton.fix.TimeWithAmericanTimeZonesRegex, sourceregexText: 'Expected input format:  hh:mm:ss America/Chicago', qtip: 'The time of day the session will begin. Time must be in the same time zone as the above Time Zone'},
									{fieldLabel: 'End Time', name: 'endTime', emptyText: 'hh:mm:ss America/Chicago', regex: Clifton.fix.TimeWithAmericanTimeZonesRegex, sourceregexText: 'Expected input format:  hh:mm:ss America/Chicago', qtip: 'The time of day the session will end. Time must be in the same time zone as the above Time Zone'},
									{fieldLabel: 'Reconnect Interval', name: 'reconnectInterval', qtip: 'The time period in seconds between connection attempts'}
								]
							}]
						},

						{xtype: 'label', html: '<hr/>'},


						{
							xtype: 'columnpanel',
							defaults: {xtype: 'textfield'},
							columns: [{
								rows: [
									{fieldLabel: 'Socket Host', name: 'socketConnectHost', qtip: 'The host address'}
								]
							}, {
								rows: [
									{fieldLabel: 'Socket Port', name: 'socketConnectPort', qtip: 'The host port'}
								]
							}]
						},
						{fieldLabel: 'Path To Executable', name: 'pathToExecutable', qtip: 'Path to an executable that needs to be started before the session'},
						{fieldLabel: 'Executable Argument', name: 'executableArgument', qtip: 'Arguments for starting the executable'},
						{fieldLabel: 'Session Dictionary Location', name: 'sessionDictionaryLocation', qtip: 'Optional location of the FIX Session Dictionary file if not using the default.'},


						{xtype: 'label', html: '<hr/>'},

						{boxLabel: 'Create Session (A session can be created. Leave unchecked if it\'s a parent used for default settings.)', name: 'createSession', xtype: 'checkbox', qtip: 'Indicates if an actual session can be created from these settings or if it\'s used as a parent for default settings.'},
						{boxLabel: 'Informational (Log Messages only, not sent to external applications)', name: 'informational', xtype: 'checkbox'},
						{boxLabel: 'Disabled (No session will be created)', name: 'disabled', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Session Definition Settings',
				items: [{
					name: 'fixSessionDefinitionSettingBySession.json',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'FIX Session Definition Settings are a hash style (name/value) configuration to define various settings needed for the session.  Each session will use the parent session definition settings and add or override with it\'s own settings. When including parent settings, they will only be included IF there is not an override by this specific session.',

					getTopToolbarFilters: function(toolbar) {
						return [
							'-',
							{fieldLabel: '', boxLabel: 'Include Parent Session Settings', xtype: 'toolbar-checkbox', name: 'includeParent', checked: true, qtip: 'Parent Session Settings are only included if the session does not have a setting overridden.'}
						];
					},
					columns: [
						{header: 'Session Definition Name', dataIndex: 'sessionDefinition.name', hidden: true},
						{header: 'Setting Name', dataIndex: 'settingName', defaultSortColumn: true},
						{header: 'Setting Value', dataIndex: 'settingValue'}
					],
					getLoadParams: function() {
						const includeParent = TCG.getChildByName(this.getTopToolbar(), 'includeParent');
						return {
							sessionId: this.getWindow().getMainFormId(),
							includeParent: includeParent.checked
						};
					},
					editor: {
						detailPageClass: 'Clifton.fix.session.setting.FixSessionDefinitionSettingWindow',
						deleteURL: 'fixSessionDefinitionSettingDelete.json',
						deleteConfirmMessage: 'Would you like to delete the selected setting? This change will not take effect until the session is reset.',
						getDefaultData: function(gridPanel) {
							return {
								sessionDefinition: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'FIX Sessions',
				items: [{
					name: 'fixSessionListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'A list of actual FIX Sessions for this session definition.  Most definitions have only 1 session.',
					columns: [
						{header: 'Session Definition', dataIndex: 'definition.name', width: 50, hidden: true},
						{header: 'Creation Time', dataIndex: 'creationDateAndTime', type: 'date', width: 50},
						{header: 'Incoming Sequence #', type: 'int', dataIndex: 'incomingSequenceNumber', width: 50},
						{header: 'Outgoing Sequence #', type: 'int', dataIndex: 'outgoingSequenceNumber', width: 50},
						{header: 'Active Session', dataIndex: 'activeSession', width: 35, type: 'boolean', tooltip: 'Indicates that this is the current session. Note: only one session can be active at a time'}
					],
					editor: {
						detailPageClass: 'Clifton.fix.session.FixSessionWindow',
						drillDownOnly: true
					},
					getLoadParams: function() {
						return {definitionId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'FIX Destinations',
				items: [{
					name: 'fixDestinationListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'A list of all FIX destinations defined in the system. If an incoming message specifies a destination name, then the system will look up the session information from the destination\'s defined Session Definition',
					columns: [
						{header: 'ID', dataIndex: 'id', hidden: true},
						{header: 'Destination Name', dataIndex: 'name', width: 50},
						{header: 'Description', dataIndex: 'description', width: 100, hidden: true},
						{header: 'Session Definition', dataIndex: 'sessionDefinition.name', width: 50, filter: {sortFieldName: 'sessionDefinitionName'}, hidden: true},
						{header: 'Security System', width: 50, dataIndex: 'securitySystem.name'},
						{header: 'Allow Anonymous', dataIndex: 'allowAnonymousAccess', type: 'boolean', width: 30, tooltip: 'True if messages sent using this destination should succeed even if the given sender user name is not mapped to an alias.'},
						{header: 'Username for Anonymous', dataIndex: 'useUsernameForAnonymousAccess', type: 'boolean', width: 35, tooltip: 'True if messages sent using this destination should populate sender sub ID with the sender user name if it is not mapped to an alias.'},
						{header: 'Default', dataIndex: 'defaultDestination', type: 'boolean', width: 15, tooltip: 'True if this destination should be used as the default destination for messages using this session definition.'}
					],
					editor: {
						detailPageClass: 'Clifton.fix.destination.FixDestinationWindow',
						getDefaultData: function(gridPanel) {
							return {
								sessionDefinition: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {sessionDefinitionId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]

	}]
});
