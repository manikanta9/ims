Clifton.fix.session.FixSessionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'FIX Session',
	iconCls: 'shopping-cart-blue',
	width: 1000,
	height: 700,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'FIX Session',
				items: [{
					xtype: 'formpanel',
					url: 'fixSessionPopulated.json',
					table: 'FixSession',
					labelFieldName: 'definition.name',
					instructions: 'A FIX Session is an instance of a FIX Session Definition.  At this time, only one session for each definition can be created/active at one time.  Sequence numbers are reset when a new session is created.',
					readOnly: true,
					labelWidth: 150,
					items: [
						{fieldLabel: 'Session Definition', name: 'definition.name', xtype: 'linkfield', detailIdField: 'definition.id', detailPageClass: 'Clifton.fix.session.FixSessionDefinitionWindow'},
						{fieldLabel: 'Creation Date/Time', name: 'creationDateAndTime'},
						{boxLabel: 'Active Session', name: 'activeSession', xtype: 'checkbox', qtip: 'Indicates that this is the current session. Note: only one session can be active at a time'},
						{xtype: 'sectionheaderfield', header: 'Sequence Numbers'},
						{xtype: 'label', html: 'If the session batches sequence number saves to the database, the Live (in memory) values might be higher until that data is synchronized back to the database.'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Incoming (Saved)', name: 'incomingSequenceNumber', xtype: 'integerfield'},
										{fieldLabel: 'Incoming (Live)', name: 'liveIncomingSequenceNumber', xtype: 'integerfield'}
									]
								},
								{
									rows: [
										{fieldLabel: 'Outgoing (Saved)', name: 'outgoingSequenceNumber', xtype: 'integerfield'},
										{fieldLabel: 'Outgoing (Live)', name: 'liveOutgoingSequenceNumber', xtype: 'integerfield'}
									]

								}
							]
						}
					]
				}]
			},


			{
				title: 'FIX Message Store',
				items: [{
					name: 'fixMessageStoreListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'FIX Message store is used to persist outgoing messages for a session.  When a session is reset these messages will be cleared.',
					columns: [
						{header: 'Sequence #', dataIndex: 'sequenceNumber', type: 'int', width: 10},
						{header: 'Text', dataIndex: 'text', width: 50}
					],
					getLoadParams: function() {
						return {sessionId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.fix.message.FixMessageStoreDetailWindow'
					}
				}]
			},


			{
				title: 'FIX Identifiers',
				items: [{
					name: 'fixIdentifierListFind',
					xtype: 'gridpanel',
					appendStandardColumns: false,
					instructions: 'FIX Identifier represents an identifier used by client systems to get unique IDs for their messages.',
					columns: [
						{header: 'ID', width: 35, dataIndex: 'id', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Unique Identifier', dataIndex: 'uniqueStringIdentifier', filter: {searchFieldName: 'uniqueStringIdentifierSearch'}, width: '70', tooltip: 'The unique id which was either generated or received for the message.  This string represents the exact value that will be in the id fields'},
						{header: 'Referenced Identifier', dataIndex: 'referencedIdentifier.uniqueStringIdentifier', filter: {searchFieldName: 'referencedUniqueStringIdentifierSearch'}, width: 70, tooltip: 'The identifier the this instance replaced.', hidden: true},
						{header: 'Source System', dataIndex: 'sourceSystem.label', width: 70, tooltip: 'The source system that created the identifier'},
						{header: 'Source FK Field ID', dataIndex: 'sourceSystemFkFieldId', type: 'int', doNotFormat: true, width: 40, useNull: true, tooltip: 'The identity of the object on the source system, used to return the new FixIdentity object'},
						{header: 'FIX Destination', dataIndex: 'fixDestination.name', width: 70, filter: {type: 'combo', searchFieldName: 'fixDestinationId', displayField: 'name', url: 'fixDestinationListFind.json'}, tooltip: 'The FIX Destination that was present on the incoming message associated with this identifier.'},
						{header: 'Identifier Generated', dataIndex: 'identifierGenerated', type: 'boolean', width: 50, tooltip: 'True if the identifier was generated, false if it represents an identifier created by another system'},
						{header: 'Newly Created', dataIndex: 'newlyCreated', type: 'boolean', width: 45, tooltip: 'Local property used to indicate that the identifier was just created'}
					],
					getLoadParams: function() {
						return {sessionId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.fix.identifier.FixIdentifierWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'FIX Messages',
				items: [{
					xtype: 'fix-messageGrid',
					columnOverrides: [
						{dataIndex: 'session.definition.name', hidden: true},
						{dataIndex: 'text', hidden: true}
					],
					applyAdditionalLoadParams: function(firstLoad, params) {
						params.sessionId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			},


			{
				title: 'Administrative Entries',
				items: [{
					xtype: 'fix-messageEntryGrid',
					columnOverrides: [
						{dataIndex: 'session.definition.name', hidden: true}
					],

					applyAdditionalLoadParams: function(firstLoad, params) {
						params.sessionId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			},


			{
				title: 'Heartbeats',
				items: [{
					xtype: 'fix-messageEntryHeartbeatGrid',
					columnOverrides: [
						{dataIndex: 'session.definition.name', hidden: true}
					],
					applyAdditionalLoadParams: function(firstLoad, params) {
						params.sessionId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			},


			{
				title: 'Non-Persistent Messages',
				items: [{
					xtype: 'fix-messageEntryNonPersistentGrid',
					columnOverrides: [
						{dataIndex: 'session.definition.name', hidden: true}
					],
					applyAdditionalLoadParams: function(firstLoad, params) {
						params.sessionId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			}
		]
	}]
});
