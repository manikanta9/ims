Ext.ns('Clifton.fix', 'Clifton.fix.identifier', 'Clifton.fix.message', 'Clifton.fix.session', 'Clifton.fix.session.setting', 'Clifton.fix.setup', 'Clifton.fix.tag', 'Clifton.fix.tag.bean', 'Clifton.fix.validation', 'Clifton.fix.order', 'Clifton.fix.destination');

Clifton.fix.IncomingAndOutgoingHeaderToolTip = Ext.util.Format.htmlEncode(`${TCG.renderIncomingOrOutgoing(true)} is incoming | ${TCG.renderIncomingOrOutgoing(false)} is outgoing | Empty is an event`);

Clifton.fix.CalendarWeekdays = [
	[1, 'Sunday'],
	[2, 'Monday'],
	[3, 'Tuesday'],
	[4, 'Wednesday'],
	[5, 'Thursday'],
	[6, 'Friday'],
	[7, 'Saturday']
];

Clifton.fix.AmericanTimeZones = [
	['America/New_York', 'America/New York'],
	['America/Chicago', 'America/Chicago'],
	['America/Denver', 'America/Denver'],
	['America/Los_Angeles', 'America/Los Angeles'],
	['America/Anchorage', 'America/Anchorage'],
	['Pacific/Honolulu', 'Pacific/Honolulu'],
	['America/Adak', 'America/Adak']
];

Clifton.fix.TimeWithAmericanTimeZonesRegex = new RegExp('^(([0|1][0-9])|([2][0-3])):([0-5][0-9])(:([0-5][0-9])) ((America|Pacific|US)\/(New_York|Chicago|Denver|Los_Angeles|Anchorage|Honolulu|Adak|Central))$');

//Download raw data from fix messages
Clifton.fix.DownloadMessagesButton = Ext.extend(Ext.Button, {
	text: 'Download Selected',
	tooltip: 'Download the raw message text from each selected entry into a single file. This download is most frequently used to export message sequences to be replayed in testing environments.',
	iconCls: 'arrow-down-green',
	handler: function() {
		const sm = TCG.getParentByClass(this, TCG.grid.GridPanel).grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one entry.', 'No Entries Selected');
		}
		else {
			const textToDownload = [...sm.getSelections()]
					.sort((entity1, entity2) => Math.abs(entity1.data.id) - Math.abs(entity2.data.id))
					.map(entity => entity.data.text)
					.join('\n');
			TCG.downloadDataAsFile('text/plain', textToDownload, 'FIX Messages');
		}
	}
});
Ext.reg('fix-download-selected-message-button', Clifton.fix.DownloadMessagesButton);


Clifton.fix.FixMessagePreviewWindowButton = Ext.extend(Ext.Button, {
	text: 'Preview Message',
	tooltip: 'Preview the message fields for raw text input',
	iconCls: 'preview',
	handler: function() {
		return TCG.createComponent('Clifton.fix.message.FixMessagePreviewWindow');
	}
});
Ext.reg('fix-preview-message-button', Clifton.fix.FixMessagePreviewWindowButton);


Clifton.fix.message.FixMessageWindowBase = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'FIX Message',
	width: 800,
	height: 550,
	iconCls: 'shopping-cart-blue',

	// Override for Entity Specific Data (see FixMessageWindow and FixLogEntryWindow)
	detailUrl: undefined,
	fieldListUrl: undefined,
	detailItems: [],

	afterFormLoad: function(formPanel) {
		// can override to add configuration here (hide/remove fields, etc.)
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					labelFieldName: 'id',
					loadValidation: false,
					listeners: {
						afterload: function(formPanel) {
							formPanel.getWindow().afterFormLoad(formPanel);
						}
					},
					getLoadURL: function() {
						const w = this.getWindow();
						return w.detailUrl;
					},
					readOnly: true,
					labelWidth: 150,
					instructions: 'Use the <i>Message</i> tab to see the properties of the Raw Message Text parsed into a readable format.',
					initComponent: function() {
						const w = this.getWindow();
						this.table = w.table;
						const currentItems = [];
						Ext.each(w.detailItems, function(f) {
							currentItems.push(f);
						});

						this.items = currentItems;
						TCG.form.FormPanel.superclass.initComponent.call(this);
					}
				}]
			},


			{
				title: 'Message',
				items: [{
					xtype: 'treegrid',
					appendStandardColumns: false,
					defaults: {
						anchor: '-35 -35' // leave room for error icon
					},
					getLoadParams: function() {
						const win = this.getWindow();
						return {messageId: win.params.id};
					},
					getLoadURL: function() {
						const win = this.getWindow();
						return win.fieldListUrl;
					},
					prepareResultData: function(o) {
						if (TCG.isNull(TCG.getValue('children', o))) {
							o.leaf = true;
						}
						return o;
					},
					readOnly: true,
					columns: [
						{header: 'Field Name', width: 170, dataIndex: 'fieldName'},
						{header: 'Tag', width: 50, dataIndex: 'tag', type: 'int', doNotFormat: true},
						{header: 'Field Value', width: 300, dataIndex: 'fieldValue'},
						{header: 'Field Value Description', width: 230, dataIndex: 'fieldValueName'}
					]
				}]
			}
		]
	}]
})
;
Ext.reg('fix-messageWindow', Clifton.fix.message.FixMessageWindowBase);


Clifton.fix.FixMessageGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixMessageEntryListFind',
	xtype: 'gridpanel',
	rowSelectionModel: 'checkbox',
	instructions: 'FIX Messages received and processed by our system (excludes Administrative messages). Also includes messages that failed processing. After the underlying problem is resolved, you can reprocess failed messages.',
	appendStandardColumns: false,
	topToolbarSearchParameter: 'searchPattern',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'In/Out', width: 10, dataIndex: 'incoming', renderer: TCG.renderIncomingOrOutgoing, tooltip: Clifton.fix.IncomingAndOutgoingHeaderToolTip, align: 'center', filter: {type: 'boolean', yesText: 'Incoming', noText: 'Outgoing'}},
		{header: 'FIX Session', width: 40, dataIndex: 'session.definition.name', filter: {searchFieldName: 'sessionDefinitionId', type: 'combo', url: 'fixSessionDefinitionListFind.json'}},
		{header: 'Source System', width: 25, dataIndex: 'sourceSystemIncludingUnknown.name', filter: {searchFieldName: 'sourceSystemIncludingUnknownId', type: 'combo', url: 'fixSourceSystemListFind.json'}, hidden: true},
		{header: 'Message Type', width: 40, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'fixMessageEntryTypeListFind.json?fixMessageEntryTypeIdsName=MESSAGE_AND_NOT_ADMINISTRATIVE'}},
		{header: 'Unique ID', width: 40, dataIndex: 'identifier.uniqueStringIdentifier', filter: {searchFieldName: 'uniqueStringIdentifier'}},
		{header: 'Drop Copy', width: 15, dataIndex: 'identifier.dropCopyIdentifier', type: 'boolean'},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Processed', width: 20, dataIndex: 'processed', type: 'boolean'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text'}
	],
	getTopToolbarInitialLoadParams: function(firstLoad) {
		let params = {
			fixMessageEntryTypeIdsName: 'MESSAGE_AND_NOT_ADMINISTRATIVE',
			readUncommittedRequested: true
		};
		params = this.applyAdditionalLoadParams(firstLoad, params);
		if (firstLoad) {
			const grid = this;
			const dd = this.getWindow().defaultData;
			if (dd && dd.forceReload) {
				this.clearFilters(true);
			}

			const entryDate = {'after': new Date().add(Date.DAY, -1)};
			if (dd) {
				if (dd.fromDate) {
					entryDate.after = dd.fromDate;
				}
				if (dd.toDate) {
					entryDate.before = dd.toDate.add(Date.DAY, 1); // include messages on toDate
				}
			}
			this.setFilterValue('messageEntryDateAndTime', entryDate, false, true);

			const filterPromisses = [];
			if (dd?.sessionDefinitionLabel) {
				filterPromisses.push(TCG.data.getDataPromiseUsingCaching('fixSessionDefinitionByLabel.json?labelName=' + dd.sessionDefinitionLabel, grid, 'fix.session.definition.label.' + dd.sessionDefinitionLabel)
						.then(function(l) {
							if (l && l.id) {
								grid.setFilterValue('session.definition.name', {value: l.id, text: l.name});
							}
						}));
			}
			if (dd?.messageTypeName) {
				filterPromisses.push(TCG.data.getDataPromiseUsingCaching('fixMessageEntryTypeByName.json?typeName=' + dd.messageTypeName, grid, 'fix.message.type.' + dd.messageTypeName)
						.then(function(t) {
							if (t && t.id) {
								grid.setFilterValue('type.name', {value: t.id, text: t.name});
							}
						}));
			}
			if (dd?.sourceSystemName) {
				filterPromisses.push(TCG.data.getDataPromiseUsingCaching('fixSourceSystemByName.json?name=' + dd.sourceSystemName, grid, 'fix.source.system.name.' + dd.sourceSystemName)
						.then(function(l) {
							if (l && l.id) {
								grid.setFilterValue('sourceSystemIncludingUnknown.name', {value: l.id, text: l.name});
							}
						}));
			}
			return Promise.all(filterPromisses).then(() => params);
		}

		return params;
	},
	applyAdditionalLoadParams: function(firstLoad, params) {
		return params;
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow'
	}
});
Ext.reg('fix-messageGrid', Clifton.fix.FixMessageGrid);


Clifton.fix.FixMessageEntryGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixMessageEntryListFind',
	xtype: 'gridpanel',
	rowSelectionModel: 'checkbox',
	instructions: 'Displays all administrative entries that are not a regular message: Administrative messages, Events and Error Events.',
	appendStandardColumns: false,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'In/Out', width: 10, dataIndex: 'incoming', renderer: TCG.renderIncomingOrOutgoing, tooltip: Clifton.fix.IncomingAndOutgoingHeaderToolTip, align: 'center', filter: {type: 'boolean', yesText: 'Incoming', noText: 'Outgoing'}},
		{header: 'FIX Session', width: 40, dataIndex: 'session.definition.name', filter: {searchFieldName: 'sessionDefinitionId', type: 'combo', url: 'fixSessionDefinitionListFind.json'}},
		{header: 'Message Type', width: 40, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'fixMessageEntryTypeListFind.json?fixMessageEntryTypeIdsName=ADMINISTRATIVE'}},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text'}
	],
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('messageEntryDateAndTime', {'after': new Date().add(Date.DAY, -1)});
		}
		return {
			readUncommittedRequested: true
		};
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('messageEntryDateAndTime', {'after': new Date().add(Date.DAY, -1)});
		}

		const searchPatternField = TCG.getChildByName(this.topToolbar, 'searchPattern', false);
		let searchPatternValue = null;
		if (TCG.isNotBlank(searchPatternField)) {
			searchPatternValue = searchPatternField.getValue();
		}

		let params = {
			searchPattern: searchPatternValue,
			fixMessageEntryTypeIdsName: 'ADMINISTRATIVE',
			readUncommittedRequested: true
		};
		params = this.applyAdditionalLoadParams(firstLoad, params);
		return params;
	},
	applyAdditionalLoadParams: function(firstLoad, params) {

		return params;
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryWindow',
		addEditButtons: function(toolbar) {
			toolbar.add({
				xtype: 'fix-download-selected-message-button'
			}, '-');
			toolbar.add({
				xtype: 'fix-preview-message-button'
			}, '-');
		}
	}
});
Ext.reg('fix-messageEntryGrid', Clifton.fix.FixMessageEntryGrid);


Clifton.fix.FixMessageEntryHeartbeatGrid = Ext.extend(Clifton.fix.FixMessageEntryGrid, {
	instructions: 'Displays recent heartbeat messages associated with active sessions.',
	appendStandardColumns: false,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: false, sortable: false},
		{header: 'In/Out', width: 10, dataIndex: 'incoming', renderer: TCG.renderIncomingOrOutgoing, tooltip: Clifton.fix.IncomingAndOutgoingHeaderToolTip, align: 'center', filter: {type: 'boolean', yesText: 'Incoming', noText: 'Outgoing'}},
		{header: 'FIX Session', width: 40, dataIndex: 'session.definition.name', filter: {searchFieldName: 'sessionDefinitionId', type: 'combo', url: 'fixSessionDefinitionListFind.json'}},
		{header: 'Entry Type', width: 40, dataIndex: 'type.name', filter: false, sortable: false},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text', filter: false, sortable: false}
	],
	listeners: {
		'afterrender': function() {
			this.grid.store.on('load', async () => {
				const capacity = await TCG.data.getDataValuePromise('fixHeartbeatMessageCapacity.json', this);
				TCG.getChildByName(this.getTopToolbar(), 'capacity').setValue(capacity);
			});
		}
	},
	editor: {
		...Clifton.fix.FixMessageEntryGrid.prototype.editor,
		getDefaultData: () => ({heartbeatMessages: true})
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('messageEntryDateAndTime', {'after': new Date().add(Date.DAY, -0)});
		}
		let params = {
			heartbeatMessages: true,
			readUncommittedRequested: true
		};
		params = this.applyAdditionalLoadParams(firstLoad, params);
		return params;
	},
	applyAdditionalLoadParams: function(firstLoad, params) {
		return params;
	},
	addToolbarButtons: function(toolbar) {
		toolbar.insert(0, {
			text: 'Clear',
			iconCls: 'remove',
			handler: async () => {
				const response = await TCG.showConfirm('This will remove all cached heartbeats. <b>Are you sure?</b>', 'Clear All Heartbeats');
				if (response === 'yes') {
					const loader = new TCG.data.JsonLoader({
						waitTarget: this,
						waitMsg: 'Clearing...',
						onLoad: () => this.reload()
					});
					loader.load('fixHeartbeatListClear.json');
				}
			}
		}, '-');
	},
	addToolbarButtonsAfter: function(toolbar, gridPanel) {
		toolbar.add(
				{xtype: 'label', html: '&nbsp;Per-Session Message Capacity:&nbsp;', qtip: 'The number of messages of this type which will be retained in the non-persistent message store for each session. This is a resource utilization configuration. While increasing the capacity will cause additional information to be retained, it will also cause the system to consume additional resources. This value is only active for the current application run and will be reset on application restart.'},
				{xtype: 'integerfield', name: 'capacity', listeners: {specialkey: (field, e) => e.getKey() === e.ENTER && gridPanel.saveCapacity()}},
				{text: 'Apply', tooltip: 'Save the specified message capacity parameters.', iconCls: 'run', handler: () => gridPanel.saveCapacity()}
		);
	},
	saveCapacity: function() {
		const loader = new TCG.data.JsonLoader({
			waitTarget: this,
			params: {capacity: TCG.getChildByName(this.getTopToolbar(), 'capacity').getValue()},
			onLoad: () => this.reload()
		});
		loader.load('fixHeartbeatMessageCapacityUpdate.json');
	}
});
Ext.reg('fix-messageEntryHeartbeatGrid', Clifton.fix.FixMessageEntryHeartbeatGrid);


Clifton.fix.FixMessageEntryNonPersistentGrid = Ext.extend(Clifton.fix.FixMessageEntryGrid, {
	instructions: 'Displays all messages received by our system that are not persisted in the database.',
	appendStandardColumns: false,
	additionalPropertiesToRequest: 'session.id', // Used for clearing messages for the session associated with the selected row
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true, filter: false, sortable: false},
		{header: 'In/Out', width: 10, dataIndex: 'incoming', renderer: TCG.renderIncomingOrOutgoing, tooltip: Clifton.fix.IncomingAndOutgoingHeaderToolTip, align: 'center', filter: {type: 'boolean', yesText: 'Incoming', noText: 'Outgoing'}},
		{header: 'FIX Session', width: 40, dataIndex: 'session.definition.name', filter: {searchFieldName: 'sessionDefinitionId', type: 'combo', url: 'fixSessionDefinitionListFind.json'}},
		{header: 'Entry Type', width: 40, dataIndex: 'type.name', filter: false, sortable: false},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text', filter: false, sortable: false}
	],
	editor: {
		...Clifton.fix.FixMessageEntryGrid.prototype.editor,
		getDefaultData: () => ({nonPersistentMessages: true})
	},
	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('messageEntryDateAndTime', {'after': new Date().add(Date.DAY, -0)});
		}
		let params = {
			nonPersistentMessages: true
		};
		params = this.applyAdditionalLoadParams(firstLoad, params);

		return params;
	},
	applyAdditionalLoadParams: function(firstLoad, params) {
		return params;
	},
	addToolbarButtons: function(toolbar) {
		toolbar.insert(0, {
			text: 'Clear',
			iconCls: 'remove',
			menu: [
				{
					text: 'Clear Selected Session',
					tooltip: 'Remove all non-persistent messages for the selected session',
					iconCls: 'remove',
					handler: async () => {
						const selections = this.grid.getSelectionModel().getSelections();
						if (selections.length !== 1) {
							TCG.showError('A single row must be selected in order to delete non-persistent messages for a specific session.', 'Clear Selected Session Messages Error');
							return;
						}
						const selected = selections[0];
						const response = await TCG.showConfirm(`This will remove all cached non-persistent messages for the ${selected.get('session.definition.name')} session. <b>Are you sure?</b>`, 'Clear All Non-Persistent Messages');
						if (response === 'yes') {
							const loader = new TCG.data.JsonLoader({
								waitTarget: this,
								waitMsg: 'Clearing...',
								params: {id: selected.json.session.id},
								onLoad: () => this.reload()
							});
							loader.load('fixNonPersistentMessageListForSessionClear.json');
						}
					}
				},
				{
					text: 'Clear All',
					tooltip: 'Remove all non-persistent messages for all sessions',
					iconCls: 'remove',
					handler: async () => {
						const response = await TCG.showConfirm('This will remove all cached non-persistent messages. <b>Are you sure?</b>', 'Clear All Non-Persistent Messages');
						if (response === 'yes') {
							const loader = new TCG.data.JsonLoader({
								waitTarget: this,
								waitMsg: 'Clearing...',
								onLoad: () => this.reload()
							});
							loader.load('fixNonPersistentMessageListClear.json');
						}
					}
				}
			]
		}, '-');
	}
});
Ext.reg('fix-messageEntryNonPersistentGrid', Clifton.fix.FixMessageEntryNonPersistentGrid);


Clifton.fix.FixMessageEntryFailedMessageGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixMessageEntryFailedMessageList',
	xtype: 'gridpanel',
	instructions: 'FIX Messages the failed to be processed after being received.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'In/Out', width: 10, dataIndex: 'incoming', renderer: TCG.renderIncomingOrOutgoing, tooltip: Clifton.fix.IncomingAndOutgoingHeaderToolTip, align: 'center', filter: {type: 'boolean', yesText: 'Incoming', noText: 'Outgoing'}},
		{header: 'FIX Session', width: 40, dataIndex: 'session.definition.name', filter: {searchFieldName: 'sessionDefinitionId', type: 'combo', url: 'fixSessionDefinitionListFind.json'}},
		{header: 'Entry Type', width: 40, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'fixMessageEntryTypeListFind.json'}},
		{header: 'Received', width: 40, dataIndex: 'messageEntryDateAndTime', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Raw Message Text', width: 80, dataIndex: 'text'}
	],
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.fix.message.FixMessageEntryFailedWindow',
		addEditButtons: function(t, gridPanel) {
			t.add({
				xtype: 'fix-download-selected-message-button'
			}, '-');
		}
	}
});
Ext.reg('fix-messageEntryFailedMessageGrid', Clifton.fix.FixMessageEntryFailedMessageGrid);


Clifton.fix.FixMessageErrorGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixMessagingErrorList',
	xtype: 'gridpanel',
	instructions: 'JMS queue that keeps Quick FIX messages that were received but failed initial processing: did not get to our message log.',
	rowSelectionModel: 'checkbox',
	appendStandardColumns: false,
	columns: [
		{header: 'Error Message ID', width: 45, dataIndex: 'errorMessageId'},
		{header: 'Message ID', width: 45, dataIndex: 'messageId'},
		{header: 'Error', width: 45, dataIndex: 'error.message'},
		{header: 'Cause', width: 145, dataIndex: 'error.cause.message'},
		{header: 'Session Name', width: 45, dataIndex: 'message.object.identifier.referencedIdentifier.session.definition.label'},
		{header: 'Identifier', width: 45, dataIndex: 'message.object.identifier.uniqueStringIdentifier'},
		{header: 'Identity', width: 45, dataIndex: 'message.object.identifier.identity'},
		{header: 'Server', width: 45, dataIndex: 'server'},
		{header: 'Target Queue', width: 45, dataIndex: 'targetQueue'},
		{header: 'Timestamp', width: 45, dataIndex: 'messageTimestamp'}
	],
	editor: {
		init: function(grid) {
			this.grid = grid;
			this.grid.addListener('rowdblclick', function(grid, rowIndex, evt) {
				const row = grid.store.data.items[rowIndex];
				const url = 'http://' + row.json.server.replace('61616', '8161') + '/admin/message.jsp?id=' + row.json.errorMessageId + '&JMSDestination=fix.error';
				window.open(url, '_blank');
			}, this);
		},
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Resend Message',
				tooltip: 'Move message to the original target queue for processing.',
				iconCls: 'run',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();

					const messages = sm.getSelections();
					gridPanel.loaderFunction = function(messagesList, currentCount, grid) {
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Moving Messages',
							waitTarget: grid,
							params: {
								messageId: messagesList[currentCount].json.messageId,
								targetQueue: messagesList[currentCount].json.targetQueue
							},
							onLoad: function(record, conf) {
								if (currentCount < messages.length - 1) {
									currentCount = currentCount + 1;
									gridPanel.loaderFunction(messagesList, currentCount, grid);
								}
								else {
									grid.reload();
								}
							}
						});
						loader.load('fixMessagingErrorMoveToTarget.json');
					};
					gridPanel.loaderFunction(messages, 0, gridPanel);
				}
			}, '-');
			t.add({
				text: 'Delete Message',
				tooltip: 'Delete error message (message will NOT be sent to the original target queue for processing).',
				iconCls: 'remove',
				handler: function() {

					Ext.Msg.confirm('Delete Selected Message(s)', 'Are you sure you would like to delete all selected message(s)? This action cannot be undone.', function(a) {
						if (a === 'yes') {

							const sm = gridPanel.grid.getSelectionModel();

							const messages = sm.getSelections();
							gridPanel.loaderFunction = function(messagesList, currentCount, grid) {
								const loader = new TCG.data.JsonLoader({
									waitMsg: 'Deleting Messages',
									waitTarget: grid,
									params: {
										messageId: messagesList[currentCount].json.messageId,
										targetQueue: messagesList[currentCount].json.targetQueue
									},
									onLoad: function(record, conf) {
										if (currentCount < messages.length - 1) {
											currentCount = currentCount + 1;
											gridPanel.loaderFunction(messagesList, currentCount, grid);
										}
										else {
											grid.reload();
										}
									}
								});
								loader.load('fixMessagingErrorDeleteFromTarget.json');
							};
							gridPanel.loaderFunction(messages, 0, gridPanel);
						}
					});
				}
			}, '-');
		}
	}
});
Ext.reg('fix-messageErrorGrid', Clifton.fix.FixMessageErrorGrid);


Clifton.fix.session.FixSessionGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'fixSessionListFind',
	xtype: 'gridpanel',
	topToolbarSearchParameter: 'definitionName',
	appendStandardColumns: false,
	wikiPage: 'IT/FIX Trouble Shooting',
	additionalPropertiesToRequest: 'liveIncomingSequenceNumber|liveOutgoingSequenceNumber',
	columns: [
		{header: 'ID', dataIndex: 'id', hidden: true},
		{header: 'Session Definition', dataIndex: 'definition.name', width: 100, filter: {searchFieldName: 'definitionName'}, defaultSortColumn: true},
		{header: 'Creation Time', dataIndex: 'creationDateAndTime', type: 'date', alwaysIncludeTime: true, width: 50},
		{
			header: 'Incoming Sequence Number', type: 'int', dataIndex: 'incomingSequenceNumber', width: 50,
			renderer: function(v, metaData, r) {
				const liveValue = r.json.liveIncomingSequenceNumber;
				if (TCG.isNotBlank(liveValue) && TCG.isNotEquals(liveValue, v)) {
					return '<div class="amountAdjusted" qtip=\'<table><tr><td>Saved Value: </td><td align="right">' + Ext.util.Format.number(v, '0,000') + '</td></tr><tr><td>Live Value: </td><td align="right">' + TCG.renderAmount(liveValue, false, '0,000') + '</td></tr></table>\'>' + liveValue + '</div>';
				}
				return v;
			}
		},
		{
			header: 'Outgoing Sequence Number', type: 'int', dataIndex: 'outgoingSequenceNumber', width: 50,
			renderer: function(v, metaData, r) {
				const liveValue = r.json.liveOutgoingSequenceNumber;
				if (TCG.isNotBlank(liveValue) && TCG.isNotEquals(liveValue, v)) {
					return '<div class="amountAdjusted" qtip=\'<table><tr><td>Saved Value: </td><td align="right">' + Ext.util.Format.number(v, '0,000') + '</td></tr><tr><td>Live Value: </td><td align="right">' + TCG.renderAmount(liveValue, false, '0,000') + '</td></tr></table>\'>' + liveValue + '</div>';
				}
				return v;
			}

		},
		{header: 'Active Session', dataIndex: 'activeSession', width: 20, type: 'boolean', tooltip: 'Indicates that this is the current session. Note: only one session per definition can be active at a time'},
		{header: 'Disabled', dataIndex: 'definition.disabled', width: 20, type: 'boolean', filter: {searchFieldName: 'disabled'}, tooltip: 'If this definition is disabled, so no session will be created for it'},
		{header: 'Running', dataIndex: 'running', width: 20, type: 'boolean', filter: false, sortable: false, tooltip: 'Indicates whether this session is running. Running sessions may or may not be connected, depending on the session schedule and the success of the latest connection attempt.'},
		{header: 'Logged On', dataIndex: 'loggedOn', width: 20, type: 'boolean', filter: false, sortable: false, tooltip: 'Indicates whether this session is connected and logged on.'}
	],
	getTopToolbarInitialLoadParams: function() {
		return {populateLiveSequenceNumbers: true, populateRunStatus: true};
	},
	editor: {
		detailPageClass: 'Clifton.fix.session.FixSessionWindow',
		drillDownOnly: true
	}
});
Ext.reg('fix-sessionGrid', Clifton.fix.session.FixSessionGrid);

Clifton.fix.order.CommissionTypes = [
	['PER_UNIT', 'PER_UNIT'],
	['PERCENTAGE', 'PERCENTAGE'],
	['ABSOLUTE', 'ABSOLUTE'],
	['PERCENTAGE_WAIVED_CASH_DISCOUNT', 'PERCENTAGE_WAIVED_CASH_DISCOUNT'],
	['PERCENTAGE_WAIVED_ENHANCED_UNITS', 'PERCENTAGE_WAIVED_ENHANCED_UNITS'],
	['POINTS_PER_BOND_OR_OR_CONTRACT', 'POINTS_PER_BOND_OR_OR_CONTRACT']
];


Clifton.fix.order.ProcessCodes = [
	['REGULAR', 'REGULAR'],
	['SOFT_DOLLAR', 'SOFT_DOLLAR'],
	['STEP_IN', 'STEP_IN'],
	['STEP_OUT', 'STEP_OUT'],
	['SOFT_DOLLAR_STEP_IN', 'SOFT_DOLLAR_STEP_IN'],
	['SOFT_DOLLAR_STEP_OUT', 'SOFT_DOLLAR_STEP_OUT'],
	['PLAN_SPONSOR', 'PLAN_SPONSOR']
];

Clifton.fix.order.AllocationTypes = [
	['CALCULATED', 'CALCULATED'],
	['PRELIMINARY', 'PRELIMINARY']
	/* EXCLUDING UNSUPPORTED OPTIONS */
	//['SELLSIDE_CALCULATED_USING_PRELIMINARY','SELLSIDE_CALCULATED_USING_PRELIMINARY'],
	//['SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY','SELLSIDE_CALCULATED_WITHOUT_PRELIMINARY'],
	//['READY_TO_BOOK_SINGLE_ORDER','READY_TO_BOOK_SINGLE_ORDER'],
	//['BUYSIDE_READY_TO_BOOK','BUYSIDE_READY_TO_BOOK'],
	//['WAREHOUSE_INSTRUCTION','WAREHOUSE_INSTRUCTION'],
	//['REQUEST_TO_INTERMEDIARY','REQUEST_TO_INTERMEDIARY'],
	//['ACCEPT','ACCEPT'],
	//['REJECT','REJECT'],
	//['ACCEPT_PENDING','ACCEPT_PENDING'],
	//['INCOMPLETE_GROUP','INCOMPLETE_GROUP'],
	//['COMPLETE_GROUP','COMPLETE_GROUP'],
	//['REVERSAL_PENDING','REVERSAL_PENDING']
];
