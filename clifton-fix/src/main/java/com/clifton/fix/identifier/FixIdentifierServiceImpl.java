package com.clifton.fix.identifier;


import com.clifton.core.util.StringUtils;
import com.clifton.fix.identifier.search.FixIdentifierSearchForm;
import com.clifton.fix.setup.FixSetupService;
import com.clifton.fix.setup.FixSourceSystem;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FixIdentifierServiceImpl<T> implements FixIdentifierService {

	private FixIdentifierHandler<T> fixIdentifierHandler;

	private FixSetupService fixSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////             FixIdentifier Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixIdentifier getFixIdentifier(int id) {
		return getFixIdentifierHandler().getFixIdentifier(id);
	}


	@Override
	public List<FixIdentifier> getFixIdentifierList(FixIdentifierSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getSourceSystemTag())) {
			FixSourceSystem sourceSystem = getFixSetupService().getFixSourceSystemByTag(searchForm.getSourceSystemTag());
			if (sourceSystem != null) {
				searchForm.setSourceSystemTag(null);
				searchForm.setSourceSystemId(sourceSystem.getId());
			}
		}
		return getFixIdentifierHandler().getFixIdentifierList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixIdentifierHandler<T> getFixIdentifierHandler() {
		return this.fixIdentifierHandler;
	}


	public void setFixIdentifierHandler(FixIdentifierHandler<T> fixIdentifierHandler) {
		this.fixIdentifierHandler = fixIdentifierHandler;
	}


	public FixSetupService getFixSetupService() {
		return this.fixSetupService;
	}


	public void setFixSetupService(FixSetupService fixSetupService) {
		this.fixSetupService = fixSetupService;
	}
}
