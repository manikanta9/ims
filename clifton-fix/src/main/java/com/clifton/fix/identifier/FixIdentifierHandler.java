package com.clifton.fix.identifier;


import com.clifton.fix.identifier.search.FixIdentifierSearchForm;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.session.FixSession;

import java.util.List;


public interface FixIdentifierHandler<T> {


	////////////////////////////////////////////////////////////////////////////
	////////             FixIdentifier Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixIdentifier getFixIdentifier(int id);


	public FixIdentifier getFixIdentifierByStringId(String identifier, Short sessionId);


	public List<FixIdentifier> getFixIdentifierList(FixIdentifierSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Finds the FIX identifier for the message, creates one if none exists and applies the new id to the message.
	 * <p/>
	 * NOTE:  If a FixIdentifier exists, the method should return null because a return from this message
	 * will generate a message back to the original system with the new id.
	 */
	public FixIdentifier applyFixIdentifier(T message, FixMessageTextMessagingMessage messagingMessage, boolean incoming, FixSession session);


	public FixIdentifier applyFixIdentifier(T message, BaseFixMessagingMessage messagingMessage, boolean incoming, FixSession session, final String fixMessageString);
}
