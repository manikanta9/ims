package com.clifton.fix.identifier;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.destination.FixDestinationService;
import com.clifton.fix.identifier.search.FixIdentifierSearchForm;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.setup.FixSetupService;
import com.clifton.fix.util.FixUtilService;
import com.clifton.fix.util.FixUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;


/**
 * The <code>FixIdentifierHandlerImpl</code> contains the basic FixIdentifier CRUD methods as well as additional methods to find the FIX identifier for the message, creates one if none exists and applies the new id to the message.
 * <p/>
 * NOTE:  If a FixIdentifier exists, the applyFixIdentifier method should return null because a return from this message
 * will generate a message back to the original system with the new id.
 *
 * @author mwacker
 */
@Component
public class FixIdentifierHandlerImpl<T> implements FixIdentifierHandler<T> {

	private AdvancedUpdatableDAO<FixIdentifier, Criteria> fixIdentifierDAO;

	private DaoCompositeKeyCache<FixIdentifier, Short, String> fixIdentifierByUniqueStringIdentifierCache;

	private FixMessageEntryService fixMessageEntryService;

	private FixSetupService fixSetupService;

	private FixUtilService<T> fixUtilService;

	private FixDestinationService fixDestinationService;

	////////////////////////////////////////////////////////////////////////////
	////////             FixIdentifier Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixIdentifier getFixIdentifier(int id) {
		return getFixIdentifierDAO().findByPrimaryKey(id);
	}


	@Override
	public FixIdentifier getFixIdentifierByStringId(String identifier, Short sessionId) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(identifier), "Cannot look up [FixIdentifier] for unique id [" + identifier + "].");
		return getFixIdentifierByUniqueStringIdentifierCache().getBeanForKeyValues(getFixIdentifierDAO(), sessionId, identifier);
	}


	@Override
	public List<FixIdentifier> getFixIdentifierList(FixIdentifierSearchForm searchForm) {
		return getFixIdentifierDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private FixIdentifier saveFixIdentifier(FixIdentifier bean) {
		return bean.isPersistent() ? getFixIdentifierDAO().save(bean) : bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixIdentifier applyFixIdentifier(T message, FixMessageTextMessagingMessage messagingMessage, boolean incoming, FixSession session) {
		final String fixMessageString = messagingMessage.getMessageText();
		return applyFixIdentifier(message, messagingMessage, incoming, session, fixMessageString);
	}


	@Override
	@Transactional
	public FixIdentifier applyFixIdentifier(T message, BaseFixMessagingMessage messagingMessage, boolean incoming, FixSession session, final String fixMessageString) {
		final String sessionQualifier = messagingMessage.getSessionQualifier();

		final String typeCode = getFixUtilService().getMessageTypeCode(fixMessageString);

		if (session == null) {
			session = getFixUtilService().getFixSession(message, sessionQualifier, incoming);
		}
		ValidationUtils.assertNotNull(session, "No FIX session for message [" + message + "].");
		final FixMessageEntryType type = getFixMessageEntryService().getFixMessageTypeByTagValue(typeCode);

		if (type != null) {
			boolean dropCopy = false;
			String uniqueId = getFixUtilService().getStringField(fixMessageString, type.getIdFieldTag());
			if (StringUtils.isEmpty(uniqueId) && incoming && (type.getDropCopyIdFieldTag() != null)) {
				uniqueId = getFixUtilService().getStringField(fixMessageString, type.getDropCopyIdFieldTag());
				dropCopy = true;
			}
			if (!StringUtils.isEmpty(uniqueId)) {
				uniqueId = FixUtils.cleanUniqueId(uniqueId);
			}

			String referencedUniqueId = null;
			if (type.getReferencedIdFieldTag() != null) {
				referencedUniqueId = getFixUtilService().getStringField(fixMessageString, type.getReferencedIdFieldTag());
				if (!StringUtils.isEmpty(referencedUniqueId)) {
					referencedUniqueId = FixUtils.cleanUniqueId(referencedUniqueId);
				}
			}

			// find the identifier
			FixIdentifier identifier = null;
			if (!StringUtils.isEmpty(uniqueId)) {
				identifier = getFixIdentifierByStringId(uniqueId, session.getId());
			}
			// Only retrieved if we'll need it to get the original source system and fkfield id
			FixIdentifier refIdentifier = null;
			if (incoming && identifier == null) {
				refIdentifier = (StringUtils.isEmpty(referencedUniqueId) ? null : getFixIdentifierByStringId(referencedUniqueId, session.getId()));
				if (refIdentifier == null || refIdentifier.getSourceSystemFkFieldId() == null) {
					dropCopy = true;
				}
			}
			// if there is no identifier, create one
			if (identifier == null) {
				identifier = new FixIdentifier();
				identifier.setUniqueStringIdentifier(uniqueId);
				identifier.setSession(session);
				identifier.setSourceSystem(getFixSetupService().getFixSourceSystemByTag(messagingMessage.getSourceSystemTag()));
				if (messagingMessage.getSourceSystemFkFieldId() != null) {
					identifier.setSourceSystemFkFieldId(messagingMessage.getSourceSystemFkFieldId());
				}
				// TODO temporary for backwards compatibility
				else if (messagingMessage.getSourceSystemId() != null) {
					identifier.setSourceSystemFkFieldId(messagingMessage.getSourceSystemId());
				}
				else if (!dropCopy && refIdentifier != null && refIdentifier.getSourceSystemFkFieldId() != null) {
					// Note: Current case when this happens is an order is sent to FX Connect - stays open for a day and the following day is canceled from FX Connect
					// The identifier comes back with a new string with the updated trade date and references the original one as a cancel
					identifier.setSourceSystemFkFieldId(refIdentifier.getSourceSystemFkFieldId());
					identifier.setSourceSystem(refIdentifier.getSourceSystem());
				}
				identifier.setDropCopyIdentifier(dropCopy);
				identifier.setPersistent(type.isPersistent());
				identifier = createFixIdentifier(identifier, referencedUniqueId);
			}
			// set the id fields on the message
			if (identifier.isNewlyCreated() && identifier.isIdentifierGenerated()) {
				getFixUtilService().setField(message, type.getIdFieldTag(), identifier.getUniqueStringIdentifier());
				if (identifier.getReferencedIdentifier() != null) {
					getFixUtilService().setField(message, type.getReferencedIdFieldTag(), identifier.getReferencedIdentifier().getUniqueStringIdentifier());
				}
				else if (!identifier.isPersistent() && referencedUniqueId != null) {
					getFixUtilService().setField(message, type.getReferencedIdFieldTag(), referencedUniqueId);
				}
			}

			if (identifier.getFixDestination() == null) {
				identifier.setFixDestination(getDestinationForIdentifierAndMessage(identifier, messagingMessage));
				identifier = saveFixIdentifier(identifier);
			}
			return identifier;
		}
		return null;
	}


	/**
	 * Creates or returns the FixIdentifier.  This input is FixIdentifier used to hold the properties
	 * received on the message.
	 */
	@Transactional
	protected FixIdentifier createFixIdentifier(FixIdentifier identifier, String referencedUniqueId) {
		ValidationUtils.assertTrue(identifier.isNewBean(), "Cannot generate a new identifier from an existing one.");

		// set a UUID for now, will be reset once the is an ID available.
		if (StringUtils.isEmpty(identifier.getUniqueStringIdentifier())) {
			identifier.setUniqueStringIdentifier(UUID.randomUUID().toString());
			identifier.setIdentifierGenerated(true);
		}
		else {
			identifier.setIdentifierGenerated(false);
		}
		// get the reference identifier for persistent identifiers
		if (identifier.isPersistent() && !StringUtils.isEmpty(referencedUniqueId) && !identifier.getUniqueStringIdentifier().equals(referencedUniqueId)) {
			FixIdentifier ref = getFixIdentifierByStringId(referencedUniqueId, identifier.getSession().getId());
			ValidationUtils.assertNotNull(ref, "Cannot replace FixIdentifier [" + referencedUniqueId + "] because it does not exist.");
			identifier.setReferencedIdentifier(ref);
		}

		// save the identifier
		FixIdentifier result = saveFixIdentifier(identifier);
		// use the populated id to create the final unique id
		if (result.isIdentifierGenerated()) {
			result.setUniqueStringIdentifier(FixUtils.createUniqueIdFromId(result.getSourceSystemFkFieldId(), null, result.getSourceSystemTag()));
			result = saveFixIdentifier(result);
		}
		result.setNewlyCreated(true);
		return result;
	}


	private FixDestination getDestinationForIdentifierAndMessage(FixIdentifier identifier, BaseFixMessagingMessage messagingMessage) {
		if (!StringUtils.isEmpty(messagingMessage.getDestinationName())) {
			return getFixDestinationService().getFixDestinationByName(messagingMessage.getDestinationName());
		}
		else {
			return getFixDestinationService().getFixDestinationBySessionDefinition(identifier.getSession().getDefinition().getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixIdentifier, Criteria> getFixIdentifierDAO() {
		return this.fixIdentifierDAO;
	}


	public void setFixIdentifierDAO(AdvancedUpdatableDAO<FixIdentifier, Criteria> fixIdentifierDAO) {
		this.fixIdentifierDAO = fixIdentifierDAO;
	}


	public DaoCompositeKeyCache<FixIdentifier, Short, String> getFixIdentifierByUniqueStringIdentifierCache() {
		return this.fixIdentifierByUniqueStringIdentifierCache;
	}


	public void setFixIdentifierByUniqueStringIdentifierCache(DaoCompositeKeyCache<FixIdentifier, Short, String> fixIdentifierByUniqueStringIdentifierCache) {
		this.fixIdentifierByUniqueStringIdentifierCache = fixIdentifierByUniqueStringIdentifierCache;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixSetupService getFixSetupService() {
		return this.fixSetupService;
	}


	public void setFixSetupService(FixSetupService fixSetupService) {
		this.fixSetupService = fixSetupService;
	}


	public FixUtilService<T> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<T> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}


	public FixDestinationService getFixDestinationService() {
		return this.fixDestinationService;
	}


	public void setFixDestinationService(FixDestinationService fixDestinationService) {
		this.fixDestinationService = fixDestinationService;
	}
}
