package com.clifton.fix.identifier.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.fix.identifier.FixIdentifier;
import org.springframework.stereotype.Component;


/**
 * The <code>FixIdentifierByUniqueStringIdentifierCache</code> caches a single FixIdentifier by UX properties: Session ID and UniqueStringIdentifier
 *
 * @author manderson
 */
@Component
public class FixIdentifierByUniqueStringIdentifierCache extends SelfRegisteringCompositeKeyDaoCache<FixIdentifier, Short, String> {


	@Override
	protected String getBeanKey1Property() {
		return "session.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "uniqueStringIdentifier";
	}


	@Override
	protected Short getBeanKey1Value(FixIdentifier bean) {
		if (bean.getSession() != null) {
			return bean.getSession().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(FixIdentifier bean) {
		return bean.getUniqueStringIdentifier();
	}
}
