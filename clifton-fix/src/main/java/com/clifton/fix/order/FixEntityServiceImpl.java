package com.clifton.fix.order;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.FixMessageTypeTagValues;
import com.clifton.fix.message.converter.FixMessageConverterService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author manderson
 */
@SuppressWarnings("rawtypes")
@Service
public class FixEntityServiceImpl implements FixEntityService {

	private FixMessageService fixMessageService;
	private FixMessageConverterService fixMessageConverterService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends FixEntity> List<T> getFixEntityListForIdentifierAndType(Integer identifier, String typeTag) {
		return getFixEntityListForIdentifierListAndType(Collections.singletonList(identifier), typeTag);
	}


	@Override
	public <T extends FixEntity> List<T> getFixEntityListForIdentifierAndTypeList(Integer identifier, List<String> typeTagList) {
		return getFixEntityListForIdentifierListAndTypeListImpl(Collections.singletonList(identifier), typeTagList);
	}


	@Override
	public <T extends FixEntity> List<T> getFixEntityListForIdentifierListAndType(List<Integer> identifierList, String typeTag) {
		return getFixEntityListForIdentifierListAndTypeListImpl(identifierList, CollectionUtils.createList(typeTag));
	}


	@SuppressWarnings("unchecked")
	private <T extends FixEntity> List<T> getFixEntityListForIdentifierListAndTypeListImpl(List<Integer> identifierList, List<String> typeTagList) {
		ValidationUtils.assertNotNull(getFixMessageConverterService(), "No message converter service is defined.");
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(typeTagList), "Type tag is required to find and convert message.");

		List<FixMessage> messageList = doGetFixMessageListByFkFieldId(identifierList, typeTagList);

		List<T> result = new ArrayList<>();
		for (FixMessage message : CollectionUtils.getIterable(messageList)) {
			result.add(getFixEntityForFixMessage(message, false));
		}
		// EXTERNAL PROXY CALLS DON'T WORK WITH ARRAY LISTS THAT SHOULD BE FIXED BUT FOUND IT'S NOT THAT SIMPLE TO FIX SO FOR NOW WILL RETURN THIS AS A PAGING ARRAY LIST
		// LOOKS LIKE PORTAL HAS SIMILAR CASES AND THEY JUST RETURN PAGING ARRAY LIST
		return new PagingArrayList(result, 0, CollectionUtils.getSize(result));
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends FixEntity> T getFixEntityForFixMessage(FixMessage message, boolean includeSessionQualifier) {
		T entity = (T) getFixMessageConverterService().toFixEntity(getFixMessageConverterService().parse(message.getText()), (includeSessionQualifier ? message.getIdentifier().getSession().getDefinition().getFixSessionQualifier() : null));
		entity.setMessageDateAndTime(message.getMessageEntryDateAndTime());
		entity.setId(message.getId());
		entity.setIncoming(message.getIncoming());
		entity.setIdentifier(message.getIdentifier());

		return entity;
	}


	@SuppressWarnings("unchecked")
	@Override
	public <T extends FixEntity> T getFixEntityForFixMessageText(String messageText, String sessionQualifier) {
		// Spring form based deserialization strips the SOH character at the end of the messageText - re-add if necessary
		if (!messageText.endsWith("\u0001")) {
			messageText += "\u0001";
		}

		return (T) getFixMessageConverterService().toFixEntity(getFixMessageConverterService().parse(messageText), sessionQualifier);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixExecutionReport> getFixExecutionReportListForSourceEntity(String sourceSystemName, Long sourceSystemFkFieldId) {
		return getFixEntityListForSourceEntityAndType(sourceSystemName, sourceSystemFkFieldId, FixMessageTypeTagValues.EXECUTION_REPORT_TYPE_TAG.getTagValue());
	}


	@Override
	public List<FixAllocationAcknowledgement> getFixAllocationAcknowledgementListForSourceEntity(String sourceSystemName, Long sourceSystemFkFieldId) {
		return getFixEntityListForSourceEntityAndType(sourceSystemName, sourceSystemFkFieldId, FixMessageTypeTagValues.ALLOCATION_INSTRUCTION_ACK_TYPE_TAG.getTagValue());
	}


	@Override
	public List<FixAllocationReportDetail> getFixAllocationReportDetailListForSourceEntity(String sourceSystemName, Long sourceSystemFkFieldId) {
		// NOTE: THIS MAY NEED TO TAKE IN AN ARRAY OF SOURCE FK FIELD IDS (PLACEMENT ALLOCATIONS FROM OMS? VS JUST THE PLACEMENT ID?)
		List<FixAllocationReport> allocationReportList = getFixEntityListForSourceEntityAndType(sourceSystemName, sourceSystemFkFieldId, FixMessageTypeTagValues.ALLOCATION_REPORT_TYPE_TAG.getTagValue());

		List<FixAllocationReportDetail> result = new ArrayList<>();
		for (FixAllocationReport report : CollectionUtils.getIterable(allocationReportList)) {
			result.addAll(report.getFixAllocationReportDetailList());
		}
		return result;
	}


	@SuppressWarnings("unchecked")
	private <T extends FixEntity> List<T> getFixEntityListForSourceEntityAndType(String sourceSystemName, Long sourceSystemFkFieldId, String typeTag) {
		List<T> result = new ArrayList<>();
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setSourceSystemName(sourceSystemName);
		searchForm.setSourceSystemFkFieldId(sourceSystemFkFieldId);
		searchForm.setMessageTypeTagValue(typeTag);
		List<FixMessage> messageList = getFixMessageService().getFixMessageList(searchForm);
		for (FixMessage message : CollectionUtils.getIterable(messageList)) {
			result.add(getFixEntityForFixMessage(message, false));
		}
		// EXTERNAL PROXY CALLS DON'T WORK WITH ARRAY LISTS THAT SHOULD BE FIXED BUT FOUND IT'S NOT THAT SIMPLE TO FIX SO FOR NOW WILL RETURN THIS AS A PAGING ARRAY LIST
		// LOOKS LIKE PORTAL HAS SIMILAR CASES AND THEY JUST RETURN PAGING ARRAY LIST
		return new PagingArrayList(result, 0, CollectionUtils.getSize(result));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends FixEntity> String getFixMessageTextForFixEntity(T fixEntity) {
		if (fixEntity instanceof FixMessageTextEntity) {
			return ((FixMessageTextEntity) fixEntity).getMessageText();
		}
		return getFixMessageConverterService().fromFixEntity(fixEntity).toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<FixMessage> doGetFixMessageListByFkFieldId(List<Integer> identifierList, List<String> typeTagList) {
		ValidationUtils.assertTrue(!identifierList.isEmpty(), "At least one [fkFieldId] is required.");

		FixMessageEntrySearchForm searchFrom = new FixMessageEntrySearchForm();
		if (typeTagList.size() == 1) {
			searchFrom.setMessageTypeTagValue(typeTagList.get(0));
		}
		else {
			String[] typeTags = new String[typeTagList.size()];
			typeTags = typeTagList.toArray(typeTags);
			searchFrom.setMessageTypeTagValueList(typeTags);
		}
		if (identifierList.size() == 1) {
			searchFrom.setIdentifierId(identifierList.get(0));
		}
		else {
			Integer[] identifierIds = new Integer[identifierList.size()];
			identifierIds = identifierList.toArray(identifierIds);
			searchFrom.setIdentifierIdList(identifierIds);
		}
		return getFixMessageService().getFixMessageList(searchFrom);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageService getFixMessageService() {
		return this.fixMessageService;
	}


	public void setFixMessageService(FixMessageService fixMessageService) {
		this.fixMessageService = fixMessageService;
	}


	public FixMessageConverterService getFixMessageConverterService() {
		return this.fixMessageConverterService;
	}


	public void setFixMessageConverterService(FixMessageConverterService fixMessageConverterService) {
		this.fixMessageConverterService = fixMessageConverterService;
	}
}
