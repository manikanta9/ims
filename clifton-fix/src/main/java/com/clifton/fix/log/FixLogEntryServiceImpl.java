package com.clifton.fix.log;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.BaseFixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class FixLogEntryServiceImpl implements FixLogEntryService, BaseFixMessageEntryService<FixLogEntry> {

	FixMessageEntryService fixMessageEntryService;


	////////////////////////////////////////////////////////////////////////////
	////////               FixLogEntry Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixLogEntry getFixLogEntry(long id) {
		return createEntity(getFixMessageEntryService().getFixMessageEntry(id));
	}


	@Override
	public List<FixLogEntry> getFixLogEntryList(FixMessageEntrySearchForm searchForm) {
		// update or override SearchForm settings to get only Administrative entries and / or the Administrative message
		if (searchForm.getFixMessageEntryTypeIdsName() == null) {
			// filter search results to administrative entries (may be non-messages or admin message)
			searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.ADMINISTRATIVE);
		}
		else if (searchForm.getFixMessageEntryTypeIdsName() == FixMessageEntryTypeIds.MESSAGE) {
			// filter to administrative messages
			searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_ADMINISTRATIVE);
		}
		else if (searchForm.getFixMessageEntryTypeIdsName() == FixMessageEntryTypeIds.NOT_ADMINISTRATIVE || searchForm.getFixMessageEntryTypeIdsName() == FixMessageEntryTypeIds.MESSAGE_AND_NOT_ADMINISTRATIVE) {
			// override to non messages that are not administrative  -- MESSAGE_AND_NOT_ADMINISTRATIVE is not supported for querying via the FixlogEntryService
			searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_NOT_ADMINISTRATIVE);
		}

		return createEntityList(getFixMessageEntryService().getFixMessageEntryList(searchForm));
	}


	@Override
	public List<FixMessageField> getFixLogEntryFieldList(long id) {
		return getFixMessageEntryService().getFixMessageEntryFieldList(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixLogEntry createEntity(FixMessageEntry entry) {
		if (entry != null && (entry.getType().isAdministrative())) {
			return entry.toFixLogEntry();
		}
		return null;
	}


	@Override
	public List<FixLogEntry> createEntityList(List<FixMessageEntry> entryList) {
		return CollectionUtils.getStream(entryList)
				.filter(Objects::nonNull)
				.filter(entry -> !entry.getType().isMessage() || entry.getType().isAdministrative())
				.map(FixMessageEntry::toFixLogEntry)
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}
}
