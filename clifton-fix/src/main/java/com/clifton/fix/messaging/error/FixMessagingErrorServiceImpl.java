package com.clifton.fix.messaging.error;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.AbstractXStreamMessageHandler;
import com.clifton.core.messaging.jms.JmsObjectMessage;
import com.clifton.core.messaging.jms.JmsQueueService;
import com.clifton.core.messaging.jms.JmsUtils;
import com.clifton.core.messaging.jms.asynchronous.JmsAsynchronousErrorMessage;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.xml.FixMessagingXmlXStreamConfigurer;
import com.thoughtworks.xstream.XStream;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FixMessagingErrorServiceImpl extends AbstractXStreamMessageHandler implements FixMessagingErrorService {

	private JmsTemplate jmsTemplate;
	private JmsQueueService<JmsAsynchronousErrorMessage> fixJmsQueueService;

	private String errorQueue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<JmsAsynchronousErrorMessage> getMessageErrorList() {
		List<JmsAsynchronousErrorMessage> messageList = getFixJmsQueueService().getMessages(getErrorQueue());
		XStream xStream = getXStreamInstance();
		for (JmsAsynchronousErrorMessage jmsAsynchronousErrorMessage : messageList) {
			try {
				BaseFixMessagingMessage fixMessagingMessage = (BaseFixMessagingMessage) JmsUtils.deserializeMessage(jmsAsynchronousErrorMessage.getMessage(), xStream, getJmsTemplate().getMessageConverter());
				if (fixMessagingMessage != null) {
					JmsObjectMessage jmsObjectMessage = JmsUtils.convertTextMessage(jmsAsynchronousErrorMessage.getMessage(), fixMessagingMessage);
					jmsAsynchronousErrorMessage.setMessage(jmsObjectMessage);
				}
			}
			catch (Exception e) {
				LogUtils.error(FixMessagingErrorServiceImpl.class, "Failed to convert Fix Error Queue Text Message.", e);
			}
		}
		return messageList;
	}


	@Override
	public void moveMessageErrorToTarget(String messageId, String targetQueue) {
		getFixJmsQueueService().moveErrorMessageToTarget(messageId, getErrorQueue(), targetQueue);
	}


	@Override
	public void deleteMessageErrorFromTarget(String messageId, String targetQueue) {
		getFixJmsQueueService().deleteErrorMessageFromQueue(messageId, getErrorQueue(), targetQueue);
	}


	@Override
	protected XStream createXStreamInstance() {
		XStream stream = new XStream();
		(new FixMessagingXmlXStreamConfigurer()).configure(stream);
		return stream;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public JmsQueueService<JmsAsynchronousErrorMessage> getFixJmsQueueService() {
		return this.fixJmsQueueService;
	}


	public void setFixJmsQueueService(JmsQueueService<JmsAsynchronousErrorMessage> fixJmsQueueService) {
		this.fixJmsQueueService = fixJmsQueueService;
	}


	public String getErrorQueue() {
		return this.errorQueue;
	}


	public void setErrorQueue(String errorQueue) {
		this.errorQueue = errorQueue;
	}
}
