package com.clifton.fix.tag.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCustomKeyDaoListCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.destination.FixDestinationTagModifierFilter;
import com.clifton.fix.tag.FixTagModifierFilter;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * A cache implementation that is designed to cache FIX Tag Modifier Filter lists by using a compound key
 * consisting of the FIX Destination ID and the SystemBeanID of the tag modifier to which the filters pertain.
 *
 * @author davidi
 */
@Component
public class FixTagModifierFilterCache extends SelfRegisteringCustomKeyDaoListCache<String, FixTagModifierFilter> {

	@Override
	public List<Class<? extends IdentityObject>> getAdditionalDtoTypeList() {
		return CollectionUtils.createList(FixTagModifierFilter.class, FixDestinationTagModifierFilter.class);
	}


	@Override
	protected Class<FixTagModifierFilter> getDtoClass() {
		return FixTagModifierFilter.class;
	}
}

