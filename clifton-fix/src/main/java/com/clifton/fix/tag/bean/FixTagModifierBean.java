package com.clifton.fix.tag.bean;


/**
 * The <code>FixTagModifierBean</code> defines a class the implements a code based tag modifier.
 *
 * @param <T>
 * @author mwacker
 */
public interface FixTagModifierBean<T> {

	public void execute(T message);
}
