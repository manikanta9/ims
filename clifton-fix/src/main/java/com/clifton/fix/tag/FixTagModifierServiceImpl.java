package com.clifton.fix.tag;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCustomKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.destination.FixDestinationService;
import com.clifton.fix.destination.FixDestinationTagModifier;
import com.clifton.fix.destination.FixDestinationTagModifierFilter;
import com.clifton.fix.destination.search.FixDestinationTagModifierFilterSearchForm;
import com.clifton.fix.destination.search.FixDestinationTagModifierSearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.tag.search.FixTagModifierFilterSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * A service to support operations on Fix Tag Modifiers.  The original FixTagModifier class has been replaced
 * with a SystemBean, thus methods have been adapted to return tag modifiers as SystemBeans.
 */
@Service
public class FixTagModifierServiceImpl implements FixTagModifierService {

	private AdvancedUpdatableDAO<FixTagModifierFilter, Criteria> fixTagModifierFilterDAO;
	private DaoCustomKeyListCache<Short, SystemBean> fixTagModifierCache;
	private DaoCustomKeyListCache<String, FixTagModifierFilter> fixTagModifierFilterCache;

	private FixDestinationService fixDestinationService;
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////            FixTagModifier Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<SystemBean> getFixTagModifierListBySession(final FixSession session) {
		return getFixTagModifierListBySessionDefinition(session.getDefinition());
	}


	@Override
	public List<SystemBean> getFixTagModifierListBySessionDefinition(FixSessionDefinition fixSessionDefinition) {
		AssertUtils.assertNotNull(fixSessionDefinition, "Fix Session Definition is required.");
		AssertUtils.assertNotNull(fixSessionDefinition.getId(), "Fix Session Definition is required.");

		final FixDestination fixDestination = getFixDestinationService().getFixDestinationBySessionDefinition(fixSessionDefinition.getId());
		AssertUtils.assertNotNull(fixDestination, "No FIX Destination defined for FixSession: " + fixSessionDefinition.getLabel());

		return getFixTagModifierCache().computeIfAbsent(fixDestination.getId(), fixDestinationId -> {
			FixDestinationTagModifierSearchForm searchForm = new FixDestinationTagModifierSearchForm();
			searchForm.setFixDestinationId(fixDestinationId);
			searchForm.setOrderBy("order:ASC");

			return getFixDestinationService().getFixDestinationTagModifierList(searchForm).stream()
					.map(FixDestinationTagModifier::getReferenceTwo)
					.collect(Collectors.toList());
		});
	}

	////////////////////////////////////////////////////////////////////////////
	/////               FixTagModifierFilter Business Methods              /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixTagModifierFilter getFixTagModifierFilter(int id) {
		return getFixTagModifierFilterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<FixTagModifierFilter> getFixTagModifierFilterListBySession(final FixSession session) {
		AssertUtils.assertNotNull(session, "Fix Session cannot be null.");

		final FixDestination fixDestination = getFixDestinationService().getFixDestinationBySessionDefinition(session.getDefinition().getId());

		FixTagModifierFilterSearchForm searchForm = new FixTagModifierFilterSearchForm();
		searchForm.setDestinationId(fixDestination.getId());
		return getFixTagModifierFilterList(searchForm);
	}


	@Override
	public List<FixTagModifierFilter> getFixTagModifierFilterListByModifier(int fixTagModifierId) {
		FixTagModifierFilterSearchForm searchForm = new FixTagModifierFilterSearchForm();
		searchForm.setModifierId(fixTagModifierId);
		return getFixTagModifierFilterList(searchForm);
	}


	@Override
	public List<FixTagModifierFilter> getFixTagModifierFilterListBySessionAndModifier(FixSession fixSession, SystemBean fixTagModifier) {
		AssertUtils.assertNotNull(fixSession, "Fix Session cannot be null.");
		AssertUtils.assertNotNull(fixTagModifier, "Fix Tag Modifier cannot be null.");

		final FixDestination fixDestination = getFixDestinationService().getFixDestinationBySessionDefinition(fixSession.getDefinition().getId());

		return getFixTagModifierFilterCache().computeIfAbsent(fixSession.getDefinition().getId() + "_" + fixTagModifier.getId(), key -> {
			FixTagModifierFilterSearchForm searchForm = new FixTagModifierFilterSearchForm();
			searchForm.setDestinationId(fixDestination.getId());
			searchForm.setModifierId(fixTagModifier.getId());
			return getFixTagModifierFilterList(searchForm);
		});
	}


	@Override
	public List<FixTagModifierFilter> getFixTagModifierFilterList(final FixTagModifierFilterSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public boolean configureOrderBy(Criteria criteria) {
				if (searchForm.getDestinationId() != null) {
					return true;
				}
				return super.configureOrderBy(criteria);
			}
		};
		return getFixTagModifierFilterDAO().findBySearchCriteria(config);
	}


	@Override
	public FixTagModifierFilter saveFixTagModifierFilter(FixTagModifierFilter bean) {
		return getFixTagModifierFilterDAO().save(bean);
	}


	@Override
	public void deleteFixTagModifierFilter(int id) {
		getFixTagModifierFilterDAO().delete(id);
	}


	@Override
	public FixDestinationTagModifierFilter linkFixTagModifierTagModifierFilterToTagModifierFilter(short fixDestinationId, int fixTagModifierId, int fixTagModifierFilterId) {
		FixDestination fixDestination = getFixDestinationService().getFixDestination(fixDestinationId);
		ValidationUtils.assertFalse(Objects.isNull(fixDestination), () -> String.format("The Fix Session Definition with ID '%s' cannot be found.", fixDestinationId));

		SystemBean fixTagModifier = getSystemBeanService().getSystemBean(fixTagModifierId);
		ValidationUtils.assertFalse(Objects.isNull(fixTagModifier), () -> String.format("The Fix Session Modifier with ID '%s' cannot be found.", fixTagModifierId));

		FixTagModifierFilter fixTagModifierFilter = getFixTagModifierFilter(fixTagModifierFilterId);
		ValidationUtils.assertFalse(Objects.isNull(fixTagModifierFilter), () -> String.format("The Fix Session Modifier Filter with ID '%s' cannot be found.", fixTagModifierFilterId));

		FixDestinationTagModifierSearchForm searchForm = new FixDestinationTagModifierSearchForm();
		searchForm.setFixDestinationId(fixDestinationId);
		searchForm.setFixTagModifierId(fixTagModifierId);
		List<FixDestinationTagModifier> definitionTagModifierList = getFixDestinationService().getFixDestinationTagModifierList(searchForm);
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(definitionTagModifierList),
				() -> String.format("Cannot associate the Filter with the Modifier '%s' because a FIX Tag Modifier is not associated with the FIX Destination '%s'",
						fixTagModifier.getName(), fixDestination.getName()));

		FixDestinationTagModifier fixDestinationTagModifier = CollectionUtils.getFirstElementStrict(definitionTagModifierList);
		FixDestinationTagModifierFilter fixTagModifierTagModifierFilter = new FixDestinationTagModifierFilter();
		fixTagModifierTagModifierFilter.setReferenceOne(fixDestinationTagModifier);
		fixTagModifierTagModifierFilter.setReferenceTwo(getFixTagModifierFilter(fixTagModifierFilterId));
		return getFixDestinationService().saveFixDestinationTagModifierFilter(fixTagModifierTagModifierFilter);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              TagModifierEntry Business Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixTagModifierEntry getFixTagModifierEntry(short fixDestinationId, int fixTagModifierId) {
		SystemBean fixTagModifier = getSystemBeanService().getSystemBean(fixTagModifierId);
		FixDestination fixDestination = getFixDestinationService().getFixDestination(fixDestinationId);
		ValidationUtils.assertNotNull(fixTagModifier, String.format("Fix Tag Modifier [%s] not found.", fixTagModifierId));
		FixDestinationTagModifierFilterSearchForm searchForm = new FixDestinationTagModifierFilterSearchForm();
		searchForm.setFixDestinationId(fixDestinationId);
		searchForm.setFixTagModifierId(fixTagModifierId);
		List<FixTagModifierFilter> filters = CollectionUtils.getStream(getFixDestinationService().getFixDestinationTagModifierFilterList(searchForm))
				.map(FixDestinationTagModifierFilter::getReferenceTwo)
				.collect(Collectors.toList());
		FixDestinationTagModifierSearchForm fixDestinationTagModifierSearchForm = new FixDestinationTagModifierSearchForm();
		fixDestinationTagModifierSearchForm.setFixTagModifierId(fixTagModifierId);
		fixDestinationTagModifierSearchForm.setFixDestinationId(fixDestinationId);
		List<FixDestinationTagModifier> fixSessionDefinitionTagModifierList = getFixDestinationService().getFixDestinationTagModifierList(fixDestinationTagModifierSearchForm);
		FixDestinationTagModifier fixSessionDefinitionTagModifier = CollectionUtils.getOnlyElement(fixSessionDefinitionTagModifierList);
		ValidationUtils.assertNotNull(fixSessionDefinitionTagModifier, String.format("Expected a link table entry for session definition [%s] and modifier [%s]",
				fixDestinationId, fixTagModifierId));
		FixTagModifierEntry fixTagModifierEntry = new FixTagModifierEntry();
		fixTagModifierEntry.setFixDestination(fixDestination);
		fixTagModifierEntry.setFixTagModifier(fixTagModifier);
		fixTagModifierEntry.setFixTagModifierFilterList(filters);
		int order = fixSessionDefinitionTagModifier.getOrder();
		fixTagModifierEntry.setFixTagModifierOrder(order);
		return fixTagModifierEntry;
	}


	@Override
	@Transactional
	public void saveFixTagModifierEntry(FixTagModifierEntry fixTagModifierEntry) {
		ValidationUtils.assertNotNull(fixTagModifierEntry.getFixDestination(), "Fix Destination is required.");
		ValidationUtils.assertNotNull(fixTagModifierEntry.getFixTagModifier(), "Fix Tag Modifier is required.");

		// make sure FixDestination is found
		FixDestination fixDestination = getFixDestinationService().getFixDestination(fixTagModifierEntry.getFixDestination().getId());
		ValidationUtils.assertNotNull(fixDestination, String.format("Fix Session Definition [%s] cannot be found.", fixTagModifierEntry.getFixDestination().getId()));

		// save / update modifier
		SystemBean fixTagModifier = fixTagModifierEntry.getFixTagModifier();
		getSystemBeanService().saveSystemBean(fixTagModifier);

		// get existing filter links.
		FixDestinationTagModifierFilterSearchForm destinationTagModifierFilterSearch = new FixDestinationTagModifierFilterSearchForm();
		destinationTagModifierFilterSearch.setFixDestinationId(fixDestination.getId());
		destinationTagModifierFilterSearch.setFixTagModifierId(fixTagModifier.getId());
		List<FixDestinationTagModifierFilter> existingLinks = CollectionUtils.getStream(getFixDestinationService().getFixDestinationTagModifierFilterList(destinationTagModifierFilterSearch))
				.filter(f -> Objects.equals(f.getReferenceOne().getReferenceOne().getId(), fixDestination.getId()))
				.filter(f -> Objects.equals(f.getReferenceOne().getReferenceTwo().getId(), fixTagModifier.getId()))
				.collect(Collectors.toList());

		// save / update link from session to modifier.
		FixDestinationTagModifierSearchForm destinationModifierSearch = new FixDestinationTagModifierSearchForm();
		destinationModifierSearch.setFixDestinationId(fixDestination.getId());
		destinationModifierSearch.setFixTagModifierId(fixTagModifier.getId());
		List<FixDestinationTagModifier> fixDestinationTagModifierList = getFixDestinationService().getFixDestinationTagModifierList(destinationModifierSearch);
		FixDestinationTagModifier fixDestinationTagModifier;
		if (fixDestinationTagModifierList.isEmpty()) {
			fixDestinationTagModifier = new FixDestinationTagModifier();
			fixDestinationTagModifier.setReferenceOne(fixDestination);
			fixDestinationTagModifier.setReferenceTwo(fixTagModifier);
		}
		else {
			fixDestinationTagModifier = fixDestinationTagModifierList.get(0);
		}
		fixDestinationTagModifier.setOrder(fixTagModifierEntry.getFixTagModifierOrder());
		getFixDestinationService().saveFixDestinationTagModifier(fixDestinationTagModifier);

		ValidationUtils.assertNotEmpty(fixTagModifierEntry.getFixTagModifierFilterList(), "The Fix Tag Modifier must include at least one filter.");

		// refresh the list of intended filters.
		List<FixTagModifierFilter> newFilterList = CollectionUtils.getStream(fixTagModifierEntry.getFixTagModifierFilterList())
				.map(f -> getFixTagModifierFilter(f.getId()))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		// unlink not intended
		CollectionUtils.getStream(existingLinks)
				.filter(f -> !newFilterList.contains(f.getReferenceTwo()))
				.forEach(l -> getFixDestinationService().deleteFixDestinationTagModifierFilter(l.getId()));

		// existing linked filters.
		List<FixTagModifierFilter> existingLinkFilters = CollectionUtils.getStream(existingLinks)
				.map(FixDestinationTagModifierFilter::getReferenceTwo)
				.collect(Collectors.toList());

		// link intended
		CollectionUtils.getStream(newFilterList)
				.filter(l -> !existingLinkFilters.contains(l))
				.forEach(f -> {
					FixDestinationTagModifierFilter link = new FixDestinationTagModifierFilter();
					link.setReferenceOne(fixDestinationTagModifier);
					link.setReferenceTwo(f);
					getFixDestinationService().saveFixDestinationTagModifierFilter(link);
				});
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixTagModifierFilter, Criteria> getFixTagModifierFilterDAO() {
		return this.fixTagModifierFilterDAO;
	}


	public void setFixTagModifierFilterDAO(AdvancedUpdatableDAO<FixTagModifierFilter, Criteria> fixTagModifierFilterDAO) {
		this.fixTagModifierFilterDAO = fixTagModifierFilterDAO;
	}


	public DaoCustomKeyListCache<Short, SystemBean> getFixTagModifierCache() {
		return this.fixTagModifierCache;
	}


	public void setFixTagModifierCache(DaoCustomKeyListCache<Short, SystemBean> fixTagModifierCache) {
		this.fixTagModifierCache = fixTagModifierCache;
	}


	public DaoCustomKeyListCache<String, FixTagModifierFilter> getFixTagModifierFilterCache() {
		return this.fixTagModifierFilterCache;
	}


	public void setFixTagModifierFilterCache(DaoCustomKeyListCache<String, FixTagModifierFilter> fixTagModifierFilterCache) {
		this.fixTagModifierFilterCache = fixTagModifierFilterCache;
	}


	public FixDestinationService getFixDestinationService() {
		return this.fixDestinationService;
	}


	public void setFixDestinationService(FixDestinationService fixDestinationService) {
		this.fixDestinationService = fixDestinationService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
