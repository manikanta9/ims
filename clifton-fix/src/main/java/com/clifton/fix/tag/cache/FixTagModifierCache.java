package com.clifton.fix.tag.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCustomKeyDaoListCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.destination.FixDestinationTagModifier;
import com.clifton.fix.destination.FixDestinationTagModifierFilter;
import com.clifton.fix.tag.FixTagModifierFilter;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * A cache implementation that is designed to cache FIX Tag Modifier lists using the FIX Destination ID as a key.
 *
 * @author davidi
 */
@Component
public class FixTagModifierCache extends SelfRegisteringCustomKeyDaoListCache<Short, SystemBean> {

	@Override
	public List<Class<? extends IdentityObject>> getAdditionalDtoTypeList() {
		return CollectionUtils.createList(FixDestinationTagModifier.class, FixTagModifierFilter.class, FixDestinationTagModifierFilter.class, SystemBeanProperty.class);
	}


	@Override
	protected Class<SystemBean> getDtoClass() {
		return SystemBean.class;
	}
}
