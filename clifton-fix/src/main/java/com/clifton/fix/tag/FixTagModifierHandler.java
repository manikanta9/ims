package com.clifton.fix.tag;


import java.util.Map;


/**
 * The <code>FixTagModifierHandler</code> defines an interface the will apply tag modifiers to a message.
 *
 * @author mwacker
 */
public interface FixTagModifierHandler<T> {

	public void applyTagModifiers(T message, String sessionQualifier, Map<String, String> additionalParams);
}
