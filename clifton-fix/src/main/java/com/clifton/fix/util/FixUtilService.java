package com.clifton.fix.util;


import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;

import java.util.List;


/**
 * The <code>FixUtilService</code> defines a utility service used by the fix project
 * to convert messages to and from a specific API.  Needs to be implemented in the project
 * the connect to the API, for example clifton-fix-quickfix.
 *
 * @param <T>
 * @author mwacker
 */
public interface FixUtilService<T> {

	/**
	 * Parse the message
	 *
	 * @param fixMessageString
	 */
	public T parseMessage(String fixMessageString);


	/**
	 * Returns the session id used by implemented api.
	 *
	 * @param message
	 * @param sessionQualifier
	 */
	public Object getSessionId(T message, String sessionQualifier);


	/**
	 * Returns the FixSession from the cache implemented by the API.
	 *
	 * @param message
	 * @param sessionQualifier
	 */
	public FixSession getFixSession(T message, String sessionQualifier, boolean incoming);


	/**
	 * Returns the string that represents the message type.
	 *
	 * @param messageText
	 */
	public String getMessageTypeCode(String messageText);


	/**
	 * Get the string value of a specific field.
	 *
	 * @param messageString
	 * @param tag
	 */
	public String getStringField(String messageString, int tag);


	/**
	 * Get the string field name of a specific field integer value.
	 */
	public String getFieldName(String version, int tag);


	public void setField(T message, int tag, String value);


	/**
	 * Set the message header info based on the definition info.
	 */
	public void setMessageInfoFromSessionDefinition(T message, FixSessionDefinition definition);


	/**
	 * Returns a list of message fields.
	 *
	 * @param fixMessageString
	 */
	public List<FixMessageField> getFixMessageFieldList(String fixMessageString);


	/**
	 * Indicates if a message is an administrative message.
	 *
	 * @param msgType
	 */
	public boolean isAdminMessage(String msgType);


	/**
	 * Indicates if the message is a heartbeat.
	 *
	 * @param message
	 */
	public boolean isHeartbeat(String message);
}
