package com.clifton.fix.util;

import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixParty;
import com.clifton.security.system.SecuritySystemUser;
import com.clifton.security.user.SecurityUser;

import java.util.List;


/**
 * The {@link FixMessageHandler} defines a utility service used by the fix project for convenience methods.
 * Needs to be implemented in the project the connect to the API, for example clifton-fix-quickfix.
 *
 * @author lnaylor
 */
public interface FixMessageHandler<T> {

	/**
	 * Returns the message text associated with a {@link BaseFixMessagingMessage}.
	 */
	public String getBaseFixMessagingMessageText(BaseFixMessagingMessage message);


	/**
	 * Returns the {@link SecuritySystemUser#getUserName()} alias for the given {@link FixEntity}, message, and senderUserName.
	 */
	public String getAliasForUser(FixEntity entity, T message, String senderUserName);


	/**
	 * Returns the {@link SecurityUser#getUserName()} for the given {@link FixEntity}, message, and alias.
	 */
	public String getUserForAlias(FixEntity entity, T message, String alias);


	/**
	 * Returns the FixSenderSubId for a given {@link FixEntity} and message.
	 */
	public String getFixSenderSubIdForFixEntity(FixEntity entity, T message, boolean throwExceptionIfNoUserName);


	/**
	 * Returns the SenderUserName for a given {@link FixEntity} and message.
	 */
	public String getSenderUserNameForFixEntity(FixEntity entity, T message);


	/**
	 * Returns a list of Parties from the NoPartyIDs group.
	 */
	public List<FixParty> getFixPartyList(T message, FixEntity entity);
}
