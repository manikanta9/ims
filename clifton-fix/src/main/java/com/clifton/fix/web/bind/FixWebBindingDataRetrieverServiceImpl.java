package com.clifton.fix.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.CoreClassUtils;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class FixWebBindingDataRetrieverServiceImpl implements FixWebBindingDataRetrieverService {

	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public <T extends IdentityObject> T getFixWebBindingObject(String objectClassName, Number objectId) {
		Class<T> dtoClass = (Class<T>) CoreClassUtils.getClass(objectClassName);
		ReadOnlyDAO<T> dao = getDaoLocator().locate(dtoClass);
		return dao.findByPrimaryKey(objectId);
	}


	@Override
	public List<Column> getFixWebBindingObjectColumnList(String objectClassName) {
		Class<? extends IdentityObject> dtoClass = CoreClassUtils.getClass(objectClassName);
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(dtoClass);
		return dao.getConfiguration().getColumnList();
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
