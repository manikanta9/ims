package com.clifton.fix.destination;

import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.fix.destination.search.FixDestinationTagModifierFilterSearchForm;
import com.clifton.fix.destination.search.FixDestinationTagModifierSearchForm;
import com.clifton.fix.destination.search.FixDestinationSearchForm;
import com.clifton.system.bean.SystemBeanService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>FixDestinationServiceImpl</code> class is a basic implementation of {@link FixDestinationService} interface.
 *
 * @author lnaylor
 */
@Service
public class FixDestinationServiceImpl implements FixDestinationService {

	private AdvancedUpdatableDAO<FixDestination, Criteria> fixDestinationDAO;
	private DaoNamedEntityCache<FixDestination> fixDestinationCache;
	private DaoSingleKeyCache<FixDestination, Short> fixDestinationByFixSessionDefinitionCache;

	private AdvancedUpdatableDAO<FixDestinationTagModifier, Criteria> fixDestinationTagModifierDAO;
	private AdvancedUpdatableDAO<FixDestinationTagModifierFilter, Criteria> fixDestinationTagModifierFilterDAO;
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	////////              FixDestination Business Methods               ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixDestination getFixDestination(short id) {
		return getFixDestinationDAO().findByPrimaryKey(id);
	}


	@Override
	public FixDestination getFixDestinationByName(String name) {
		return getFixDestinationCache().getBeanForKeyValue(getFixDestinationDAO(), name);
	}


	@Override
	public FixDestination getFixDestinationBySessionDefinition(short sessionDefinitionId) {
		return getFixDestinationByFixSessionDefinitionCache().getBeanForKeyValue(getFixDestinationDAO(), sessionDefinitionId);
	}


	@Override
	public List<FixDestination> getFixDestinationList(FixDestinationSearchForm searchForm) {
		return getFixDestinationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public FixDestination saveFixDestination(FixDestination destination, boolean ignoreValidation) {
		List<String> validationExceptionMessageList = new ArrayList<>();

		if (destination.isDefaultDestination()) {
			List<FixDestination> destinationList = getFixDestinationDAO().findByField("sessionDefinition.id", destination.getSessionDefinition().getId());
			if (destinationList != null) {
				for (FixDestination destinationOther : destinationList) {
					if (!destinationOther.equals(destination) && destinationOther.isDefaultDestination()) {
						if (!ignoreValidation) {
							validationExceptionMessageList.add("Are you sure you want to set this FIX Destination as the default destination for " + destination.getSessionDefinition().getName() + "? The destination " + destinationOther.getName() + " will be removed as the default destination for this definition.");
						}
						else {
							destinationOther.setDefaultDestination(false);
							//Need to flush here so that we don't get a constraint error (we can't have two destinations with the same session definition both marked as default)
							//Without the flush, hibernate tries to save the new default destination first
							DaoUtils.executeWithPostUpdateFlushEnabled(() -> getFixDestinationDAO().save(destinationOther));
						}
					}
				}
			}
		}

		if (destination.getId() != null) {
			FixDestination oldValue = getFixDestination(destination.getId());
			if (!oldValue.getName().equals(destination.getName()) && !ignoreValidation) {
				validationExceptionMessageList.add("Are you sure you want to update the FIX Destination name? Any clients using the old name will break until they're updated.");
			}
		}

		if (!validationExceptionMessageList.isEmpty()) {
			throw new UserIgnorableValidationException(StringUtils.collectionToDelimitedString(validationExceptionMessageList, "<br/><br/>"));
		}

		return getFixDestinationDAO().save(destination);
	}


	@Override
	public void deleteFixDestination(short id, boolean ignoreValidation) {
		if (!ignoreValidation) {
			throw new UserIgnorableValidationException("Are you sure you want to delete this FIX Destination? Any clients using this destination will break until they're updated.");
		}
		getFixDestinationDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////        FixDestinationTagModifier Business Methods        //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixDestinationTagModifier getFixDestinationTagModifier(int id) {
		return getFixDestinationTagModifierDAO().findByPrimaryKey(id);
	}


	@Override
	public FixDestinationTagModifier saveFixDestinationTagModifier(FixDestinationTagModifier bean) {
		return getFixDestinationTagModifierDAO().save(bean);
	}


	@Override
	public void deleteFixDestinationTagModifier(int id) {
		FixDestinationTagModifierFilterSearchForm searchForm = new FixDestinationTagModifierFilterSearchForm();
		searchForm.setFixDestinationTagModifierId(id);
		List<FixDestinationTagModifierFilter> filterLinks = getFixDestinationTagModifierFilterList(searchForm);
		getFixDestinationTagModifierFilterDAO().deleteList(filterLinks);
		getFixDestinationTagModifierDAO().delete(id);
	}


	@Override
	public void deleteFixDestinationTagModifierByFixDestination(short fixDestinationId) {
		FixDestinationTagModifierSearchForm searchForm = new FixDestinationTagModifierSearchForm();
		searchForm.setFixDestinationId(fixDestinationId);
		getFixDestinationTagModifierDAO().deleteList(getFixDestinationTagModifierList(searchForm));
	}


	@Override
	public FixDestinationTagModifier linkFixDestinationToTagModifier(short fixDestinationId, int fixTagModifierId) {
		FixDestinationTagModifier fixDestinationTagModifier = new FixDestinationTagModifier();
		fixDestinationTagModifier.setReferenceOne(getFixDestination(fixDestinationId));
		fixDestinationTagModifier.setReferenceTwo(getSystemBeanService().getSystemBean(fixTagModifierId));
		return getFixDestinationTagModifierDAO().save(fixDestinationTagModifier);
	}


	@Override
	public List<FixDestinationTagModifier> getFixDestinationTagModifierList(final FixDestinationTagModifierSearchForm searchForm) {
		return getFixDestinationTagModifierDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	//           FixDestinationTagModifierFilter Business Methods             //
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixDestinationTagModifierFilter getFixDestinationTagModifierFilter(int id) {
		return getFixDestinationTagModifierFilterDAO().findByPrimaryKey(id);
	}


	@Override
	public FixDestinationTagModifierFilter saveFixDestinationTagModifierFilter(FixDestinationTagModifierFilter bean) {
		return getFixDestinationTagModifierFilterDAO().save(bean);
	}


	@Override
	public void deleteFixDestinationTagModifierFilter(int id) {
		getFixDestinationTagModifierFilterDAO().delete(id);
	}


	@Override
	public void deleteFixDestinationTagModifierFilterByFixDestination(short fixDestinationId) {
		FixDestinationTagModifierFilterSearchForm searchForm = new FixDestinationTagModifierFilterSearchForm();
		searchForm.setFixDestinationId(fixDestinationId);
		getFixDestinationTagModifierFilterDAO().deleteList(getFixDestinationTagModifierFilterList(searchForm));
	}


	@Override
	public List<FixDestinationTagModifierFilter> getFixDestinationTagModifierFilterList(FixDestinationTagModifierFilterSearchForm searchForm) {
		return getFixDestinationTagModifierFilterDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixDestination, Criteria> getFixDestinationDAO() {
		return this.fixDestinationDAO;
	}


	public void setFixDestinationDAO(AdvancedUpdatableDAO<FixDestination, Criteria> fixDestinationDAO) {
		this.fixDestinationDAO = fixDestinationDAO;
	}


	public DaoNamedEntityCache<FixDestination> getFixDestinationCache() {
		return this.fixDestinationCache;
	}


	public void setFixDestinationCache(DaoNamedEntityCache<FixDestination> fixDestinationCache) {
		this.fixDestinationCache = fixDestinationCache;
	}


	public DaoSingleKeyCache<FixDestination, Short> getFixDestinationByFixSessionDefinitionCache() {
		return this.fixDestinationByFixSessionDefinitionCache;
	}


	public void setFixDestinationByFixSessionDefinitionCache(DaoSingleKeyCache<FixDestination, Short> fixDestinationByFixSessionDefinitionCache) {
		this.fixDestinationByFixSessionDefinitionCache = fixDestinationByFixSessionDefinitionCache;
	}


	public AdvancedUpdatableDAO<FixDestinationTagModifier, Criteria> getFixDestinationTagModifierDAO() {
		return this.fixDestinationTagModifierDAO;
	}


	public void setFixDestinationTagModifierDAO(AdvancedUpdatableDAO<FixDestinationTagModifier, Criteria> fixDestinationTagModifierDAO) {
		this.fixDestinationTagModifierDAO = fixDestinationTagModifierDAO;
	}


	public AdvancedUpdatableDAO<FixDestinationTagModifierFilter, Criteria> getFixDestinationTagModifierFilterDAO() {
		return this.fixDestinationTagModifierFilterDAO;
	}


	public void setFixDestinationTagModifierFilterDAO(AdvancedUpdatableDAO<FixDestinationTagModifierFilter, Criteria> fixDestinationTagModifierFilterDAO) {
		this.fixDestinationTagModifierFilterDAO = fixDestinationTagModifierFilterDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
