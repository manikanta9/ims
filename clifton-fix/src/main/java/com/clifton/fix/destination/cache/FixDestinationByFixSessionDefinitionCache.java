package com.clifton.fix.destination.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.fix.destination.FixDestination;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>FixDestinationByFixSessionDefinitionCache</code> class provides caching and retrieval from cache of
 * FixDestination objects by the FixSessionDefinitionID. A Destination must have defaultDestination set to true
 * in order to be returned; if all destinations associated with a definition have defaultDestination set to false,
 * this cache will return null.
 *
 * @author lnaylor
 */
@Component
public class FixDestinationByFixSessionDefinitionCache extends SelfRegisteringSingleKeyDaoCache<FixDestination, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "sessionDefinition.id";
	}


	@Override
	protected Short getBeanKeyValue(FixDestination bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getSessionDefinition());
		}
		return null;
	}


	@Override
	protected FixDestination lookupBean(ReadOnlyDAO<FixDestination> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		List<FixDestination> beans = dao.findByFields(getBeanKeyProperties(), keyProperties);
		List<FixDestination> defaultDestinationList = new ArrayList<>();
		if (beans != null) {
			for (FixDestination bean : beans) {
				if (bean.isDefaultDestination()) {
					defaultDestinationList.add(bean);
				}
			}
		}

		if (defaultDestinationList.size() > 1) {
			String defaultDestinations = CollectionUtils.toString(CollectionUtils.getConverted(defaultDestinationList, FixDestination::getName), defaultDestinationList.size());
			throw new ValidationException("Multiple Fix Destinations are marked as default for Fix Session Definition ID(s) [" + ArrayUtils.toString(keyProperties) + "]: " + defaultDestinations);
		}
		if (defaultDestinationList.isEmpty() && throwExceptionIfNotFound) {
			throw new ValidationException("Unable to find Fix Destination with Fix Session Definition ID(s) [" + ArrayUtils.toString(keyProperties) + "] that was set as a default destination.");
		}
		return CollectionUtils.getOnlyElement(defaultDestinationList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<FixDestination> dao, DaoEventTypes event, FixDestination bean, Throwable e) {
		if (e == null) {
			if (event.isUpdate()) {
				FixDestination originalBean = getOriginalBean(dao, bean);
				if (bean.isDefaultDestination() != originalBean.isDefaultDestination()) {
					getCacheHandler().remove(getCacheName(), getBeanKey(originalBean));
					getCacheHandler().remove(getCacheName(), getBeanKey(bean));
				}
			}
			else {
				getCacheHandler().remove(getCacheName(), getBeanKey(bean));
			}
		}
	}
}
