package com.clifton.fix.message.converter;


import com.clifton.fix.order.FixEntity;


/**
 * The <code>FixMessageConverterService</code> defines a service component that
 * will be used to parse incoming messages to the defined format and convert them to and from FixEntities.
 *
 * @param <M>
 * @author mwacker
 */
public interface FixMessageConverterService<M> {

	/**
	 * Parse a message to the implemented format.  For quickfix this is quickfix.Message.
	 *
	 * @param text
	 */
	public M parse(String text);


	/**
	 * Convert a native message to an internal FIX entity.
	 *
	 * @param message
	 */
	public <F extends FixEntity, Q extends M> F toFixEntity(Q message, String sessionQualifier);


	/**
	 * Convert an internal FIX entity to the native format.
	 */
	public <Q extends M, F extends FixEntity> Q fromFixEntity(F entity);
}
