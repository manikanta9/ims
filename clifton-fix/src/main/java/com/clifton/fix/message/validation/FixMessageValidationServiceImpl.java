package com.clifton.fix.message.validation;

import com.clifton.core.util.AssertUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.util.FixUtilService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
@Service
public class FixMessageValidationServiceImpl<M> implements FixMessageValidationService {

	private FixMessageService fixMessageService;
	private FixUtilService<M> fixUtilService;
	private FixMessageValidationHandler<M> fixMessageValidationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixValidationResult> validateFixMessageForOrderList(List<Integer> externalOrderIdentifierList) {
		AssertUtils.assertNotNull(externalOrderIdentifierList, "Identifier list cannot be null.");
		List<FixValidationResult> results = new ArrayList<>();
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		Integer[] identifierIds = new Integer[externalOrderIdentifierList.size()];
		identifierIds = externalOrderIdentifierList.toArray(identifierIds);
		searchForm.setIdentifierIdList(identifierIds);

		List<FixMessage> messages = getFixMessageService().getFixMessageList(searchForm);
		for (FixMessage fixMessage : messages) {
			M message = getFixUtilService().parseMessage(fixMessage.getText());
			results.addAll(getFixMessageValidationHandler().validate(message, fixMessage.getIdentifier()));
		}
		return results.stream().distinct().collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageService getFixMessageService() {
		return this.fixMessageService;
	}


	public void setFixMessageService(FixMessageService fixMessageService) {
		this.fixMessageService = fixMessageService;
	}


	public FixMessageValidationHandler<M> getFixMessageValidationHandler() {
		return this.fixMessageValidationHandler;
	}


	public void setFixMessageValidationHandler(FixMessageValidationHandler<M> fixMessageValidationHandler) {
		this.fixMessageValidationHandler = fixMessageValidationHandler;
	}


	public FixUtilService<M> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<M> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}
}
