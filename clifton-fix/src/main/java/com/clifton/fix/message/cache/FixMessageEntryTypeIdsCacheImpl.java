package com.clifton.fix.message.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.search.FixMessageEntryTypeSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Supplier;


/**
 * The <code>FixLogEntryTypeIdsCacheImpl</code> class is a cache of FixLogEntryType id's of specific types:
 * MESSAGE_NOT_ADMINISTRATIVE is the most common use case
 *
 * <p>
 * It can be used to improve system performance by avoiding joins on large tables: FixLogEntry, etc.
 * Instead, get corresponding FixLogEntryType id's and use them in "IN" clause.
 *
 * @author manderson
 */
@Component
public class FixMessageEntryTypeIdsCacheImpl extends SelfRegisteringSimpleDaoCache<FixMessageEntryType, String, Short[]> implements FixMessageEntryTypeIdsCache {

	@Override
	public Short[] getFixMessageEntryTypes(FixMessageEntryTypeIds entryTypeIdsKey, FixMessageEntryService fixMessageEntryService) {
		return getFromCacheOrUpdate(entryTypeIdsKey.name(), fixMessageEntryService, () -> {
			FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
			searchForm.setMessage(entryTypeIdsKey.getMessage());
			searchForm.setAdministrative(entryTypeIdsKey.getAdministrative());
			return searchForm;
		});
	}


	private Short[] getFromCacheOrUpdate(String cacheKey, FixMessageEntryService fixMessageEntryService, Supplier<FixMessageEntryTypeSearchForm> searchFormSupplierIfMissing) {
		Short[] result = getCacheHandler().get(getCacheName(), cacheKey);
		if (result == null) {
			// not in cache: retrieve from service and store in cache for future calls
			FixMessageEntryTypeSearchForm searchForm = searchFormSupplierIfMissing.get();
			List<FixMessageEntryType> list = fixMessageEntryService.getFixMessageEntryTypeList(searchForm);

			int size = CollectionUtils.getSize(list);
			result = new Short[size];
			for (int i = 0; i < size; i++) {
				result[i] = list.get(i).getId();
			}
			getCacheHandler().put(getCacheName(), cacheKey, result);
		}
		return result;
	}
}
