package com.clifton.fix.message;

import java.io.Serializable;


/**
 * A command object to pass parameters to the FixMessageEntryService::getFixMessageEntryLastHeartBeatBySession method.
 *
 * @author davidi
 */
public class FixMessageEntryHeartbeatStatusCommand implements Serializable {

	/**
	 * If true, will only check active sessions for heartbeat status.
	 */
	private boolean activeSessionsOnly;


	/**
	 * If heartbeat is null or older than value in maximumMinutesSinceLastHeartbeatWarn, the condition raises a warning.  Time is in minutes.
	 */
	private short maximumMinutesSinceLastHeartbeatWarn;


	/**
	 * If set to true, the call will return only sessions that have warnings associated with them.
	 */
	private boolean returnWarningsOnly;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isActiveSessionsOnly() {
		return this.activeSessionsOnly;
	}


	public void setActiveSessionsOnly(boolean activeSessionsOnly) {
		this.activeSessionsOnly = activeSessionsOnly;
	}


	public short getMaximumMinutesSinceLastHeartbeatWarn() {
		return this.maximumMinutesSinceLastHeartbeatWarn;
	}


	public void setMaximumMinutesSinceLastHeartbeatWarn(short maximumMinutesSinceLastHeartbeatWarn) {
		this.maximumMinutesSinceLastHeartbeatWarn = maximumMinutesSinceLastHeartbeatWarn;
	}


	public boolean isReturnWarningsOnly() {
		return this.returnWarningsOnly;
	}


	public void setReturnWarningsOnly(boolean returnWarningsOnly) {
		this.returnWarningsOnly = returnWarningsOnly;
	}
}
