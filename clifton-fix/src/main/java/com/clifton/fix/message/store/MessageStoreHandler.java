package com.clifton.fix.message.store;


import java.io.IOException;
import java.util.Collection;
import java.util.Date;


/**
 * The <code>MessageStoreHandler</code> defines an object used to store messages for resending
 * and manage the session sequence numbers.
 *
 * @author mwacker
 */
public interface MessageStoreHandler {

	/**
	 * Adds a raw fix messages to the store with the given sequence number.
	 * (Most implementations just append the message data to the store so be
	 * careful about assuming random access behavior.)
	 *
	 * @param sequence the sequence number
	 * @param message  the raw FIX message string
	 * @return true is successful, false otherwise
	 * @throws IOException IO error
	 */
	public boolean set(int sequence, String message);


	/**
	 * Get messages within sequence number range (inclusive). Used for message
	 * resend requests.
	 *
	 * @param startSequence the starting message sequence number.
	 * @param endSequence   the ending message sequence number.
	 * @param messages      the retrieved messages (out parameter)
	 * @throws IOException IO error
	 */
	public void get(int startSequence, int endSequence, Collection<String> messages);


	public int getNextOutgoingMsgSeqNum();


	public int getNextIncomingMsgSeqNum();


	public void setNextOutgoingMsgSeqNum(int next);


	public void setNextIncomingMsgSeqNum(int next);


	public void incrNextOutgoingMsgSeqNum();


	public void incrNextIncomingMsgSeqNum();


	/**
	 * Get the session creation time.
	 *
	 * @return the session creation time.
	 * @throws IOException IO error
	 */
	public Date getCreationTime();


	/**
	 * Reset the message store. Sequence numbers are set back to 1 and stored
	 * messages are erased. The session creation time is also set to the time of
	 * the reset.
	 *
	 * @throws IOException IO error
	 */
	public void reset();


	/**
	 * Refresh session state from a <em>shared</em> state storage (e.g. database,
	 * file, ...). Refresh will not work for message stores without shared state
	 * (e.g., MemoryStore). These stores should log an session error, at a minimum,
	 * or throw an exception.
	 */
	public void refresh();
}
