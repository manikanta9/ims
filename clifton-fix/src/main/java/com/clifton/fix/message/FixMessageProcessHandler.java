package com.clifton.fix.message;


/**
 * The <code>FixMessageHandler</code> defines methods used to update FixMessageEntry entities
 * that are non-administrative FIX messages with a FixIdentifier.
 *
 * @param <T>
 * @author mwacker
 */
public interface FixMessageProcessHandler<T> {

	/**
	 * Process FixMessageEntry, assigning it a FixIdentifier.
	 *
	 * @param fixMessageEntry
	 */
	public T processMessage(T fixMessageEntry);
}
