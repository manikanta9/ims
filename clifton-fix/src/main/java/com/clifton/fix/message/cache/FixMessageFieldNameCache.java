package com.clifton.fix.message.cache;

import java.util.List;


/**
 * The <code>FixMessageFieldNameCache</code> is a lazily built cache that allows for the efficient
 * retrieval of field tag and name map for a particular fix version.
 */
public interface FixMessageFieldNameCache {

	public List<FixMessageFieldName> getFixMessageFieldNameMap(String fixVersion);
}
