package com.clifton.fix.message.validation;

import com.clifton.fix.identifier.FixIdentifier;

import java.util.List;


/**
 * <code>FixMessageValidationHandler</code> validates the provided fix message and returns the validation results.
 * <p>
 * It is implemented in Quick Fix Project
 */
public interface FixMessageValidationHandler<T> {

	public List<FixValidationResult> validate(T message, FixIdentifier fixIdentifier);
}
