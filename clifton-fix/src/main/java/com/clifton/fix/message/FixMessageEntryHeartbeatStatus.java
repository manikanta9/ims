package com.clifton.fix.message;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.model.ModelAware;
import com.clifton.core.model.ModelAwareUtils;
import com.clifton.fix.api.server.model.FixMessageEntryHeartbeatStatusModel;
import com.clifton.fix.session.FixSessionDefinition;

import java.util.Date;


/**
 * The <code>FixMessageEntryHeartbeatStatus</code> is a custom object that includes a {@link com.clifton.fix.session.FixSessionDefinition} and LastHeartbeatDateAndTime property for the last heartbeat for the session definition.
 * <p>
 * Used by health check pages to confirm sessions that are up are exchanging heartbeats.
 *
 * @author manderson
 */
@NonPersistentObject
public class FixMessageEntryHeartbeatStatus implements ModelAware<FixMessageEntryHeartbeatStatusModel> {

	private int sessionDefinitionId;

	private String sessionDefinitionLabel;

	private Date lastHeartbeatDateAndTime;

	private boolean failed;

	private String statusMessage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public FixMessageEntryHeartbeatStatusModel toModelObject() {
		FixMessageEntryHeartbeatStatusModel model = new FixMessageEntryHeartbeatStatusModel();
		ModelAwareUtils.copyPropertiesToModelObject(this, model);
		return model;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryHeartbeatStatus() {
		// Empty Constructor for Serialization/Deserialization
	}


	public FixMessageEntryHeartbeatStatus(FixSessionDefinition fixSessionDefinition, Date lastHeartbeatDateAndTime) {
		setSessionDefinitionId(fixSessionDefinition.getId());
		setSessionDefinitionLabel(fixSessionDefinition.getLabel());
		setLastHeartbeatDateAndTime(lastHeartbeatDateAndTime);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getSessionDefinitionId() {
		return this.sessionDefinitionId;
	}


	public void setSessionDefinitionId(int sessionDefinitionId) {
		this.sessionDefinitionId = sessionDefinitionId;
	}


	public String getSessionDefinitionLabel() {
		return this.sessionDefinitionLabel;
	}


	public void setSessionDefinitionLabel(String sessionDefinitionLabel) {
		this.sessionDefinitionLabel = sessionDefinitionLabel;
	}


	public Date getLastHeartbeatDateAndTime() {
		return this.lastHeartbeatDateAndTime;
	}


	public void setLastHeartbeatDateAndTime(Date lastHeartbeatDateAndTime) {
		this.lastHeartbeatDateAndTime = lastHeartbeatDateAndTime;
	}


	public boolean isFailed() {
		return this.failed;
	}


	public void setFailed(boolean failed) {
		this.failed = failed;
	}


	public String getStatusMessage() {
		return this.statusMessage;
	}


	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
}
