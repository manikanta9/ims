package com.clifton.fix.message.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.HashSet;


/**
 * A search form configurer for FixMessage queries, optimized for specific queries involving FixMessageTypes.
 *
 * @author davidi
 */
public class FixMessageEntrySearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final FixMessageEntryService fixMessageEntryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntrySearchFormConfigurer(BaseEntitySearchForm searchForm, FixMessageEntryService fixMessageEntryService) {
		super(searchForm);
		this.fixMessageEntryService = fixMessageEntryService;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		FixMessageEntrySearchForm searchForm = (FixMessageEntrySearchForm) getSortableSearchForm();
		configureMessageTypeTagValue(criteria, searchForm);

		if (searchForm.getSessionIsNullOrNotProcessed() != null) {
			criteria.add(Restrictions.or(Restrictions.isNull("session.id"), Restrictions.eq("processed", false)));
		}
	}


	/**
	 * Configures criteria for a search form containing messageTypeTagValue or messageTypeTagValueList search field values.  Customized to avoid
	 * creation of an SQL INNER JOIN between the FixMessage and FixMessageType tables.
	 */
	private void configureMessageTypeTagValue(Criteria criteria, FixMessageEntrySearchForm searchForm) {
		if (searchForm.getMessageTypeTagValue() == null && searchForm.getMessageTypeTagValueList() == null) {
			return;
		}

		if (searchForm.getMessageTypeTagValue() != null) {
			FixMessageEntryType messageType = this.fixMessageEntryService.getFixMessageTypeByTagValue(searchForm.getMessageTypeTagValue());
			ValidationUtils.assertNotNull(messageType, "MessageType value cannot be null");
			criteria.add(Restrictions.eq("type.id", messageType.getId()));
		}

		if (searchForm.getMessageTypeTagValueList() != null) {
			ValidationUtils.assertTrue(searchForm.getMessageTypeTagValueList().length > 0, "MessageTypeTagValueList cannot be empty.");

			HashSet<Short> messageTypeIdHashSet = new HashSet<>();
			for (String tagValue : CollectionUtils.createList(searchForm.getMessageTypeTagValueList())) {
				FixMessageEntryType messageType = this.fixMessageEntryService.getFixMessageTypeByTagValue(tagValue);
				ValidationUtils.assertNotNull(messageType, String.format("MessageType value not found for tag value '%s'", tagValue));
				messageTypeIdHashSet.add(messageType.getId());
			}

			if (messageTypeIdHashSet.size() == 1) {
				criteria.add(Restrictions.eq("type.id", CollectionUtils.getFirstElement(messageTypeIdHashSet)));
			}
			else if (messageTypeIdHashSet.size() > 1) {
				criteria.add(Restrictions.in("type.id", messageTypeIdHashSet));
			}
		}
	}
}
