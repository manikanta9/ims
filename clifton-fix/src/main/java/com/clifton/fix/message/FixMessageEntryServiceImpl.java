package com.clifton.fix.message;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.xml.XmlBasedCriteria;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlUpdateCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.RollingList;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.queue.PersistentBlockingQueue;
import com.clifton.core.util.queue.Queue;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.cache.FixMessageEntryTypeIdsCache;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.message.search.FixMessageEntrySearchFormConfigurer;
import com.clifton.fix.message.search.FixMessageEntryTypeSearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.util.FixUtilService;
import com.clifton.fix.util.FixUtils;
import org.hibernate.Criteria;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;


/**
 * The <code>FixMessageEntryServiceImpl</code> is the server side implementation of the FixMessageEntryService.  This implementation
 * uses our typical DAO objects to retrieve message from the FixMessageEntry table.  Note that this service contains the methods from the
 * former FixLogService and FixMessageService.
 *
 * @author davidi
 */
@SuppressWarnings("rawtypes")
@Service
public class FixMessageEntryServiceImpl implements FixMessageEntryService, ApplicationContextAware {

	private static final String CACHE_DEFAULT_FIX_LOG_ENTRY_TYPE = "FIX_LOG_ENTRY_TYPE_DEFAULT";

	private AdvancedUpdatableDAO<FixMessageEntry, Criteria> FixMessageEntryDAO;
	private AdvancedReadOnlyDAO<FixMessageEntryType, Criteria> FixMessageEntryTypeDAO;

	private CacheHandler<Object, FixMessageEntryType> cacheHandler;
	private DaoNamedEntityCache<FixMessageEntryType> FixMessageEntryTypeCache;
	private FixMessageEntryTypeIdsCache FixMessageEntryTypeIdsCache;
	private DaoSingleKeyCache<FixMessageEntryType, String> fixMessageEntryTypeByTagValueCache;

	private FixSessionService fixSessionService;
	private FixUtilService fixUtilService;

	private ApplicationContext applicationContext;
	private SqlHandler sqlHandler;

	private final AtomicLong heartbeatId = new AtomicLong(-1);
	private final AtomicLong nonPersistentMessageId = new AtomicLong(-1);
	private final AtomicLong failedMessageId = new AtomicLong(-1);
	private final Map<Short, List<FixMessageEntry>> heartbeatLogMap = new ConcurrentHashMap<>();
	private final Map<Short, List<FixMessageEntry>> nonPersistentMessageLogMap = new ConcurrentHashMap<>();
	/*
	 * Read/write locks are used to prevent message caches from being generated or mutated while capacities are in the process of changing. This prevents race conditions which may
	 * produce unexpected application behaviors, such as messages written to orphaned lists or lists created with old capacity values.
	 */
	private final ReadWriteLock heartbeatLogMapLock = new ReentrantReadWriteLock();
	private final ReadWriteLock nonPersistentMessageLogMapLock = new ReentrantReadWriteLock();

	@Value("${fix.message.heartbeat.capacity:50}")
	private int heartbeatMessageCapacity;

	private Queue<FixMessageEntry> fixFailedMessageQueue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@PostConstruct
	public void init() {
		try {
			String filePath = ((PersistentBlockingQueue) getFixFailedMessageQueue()).getFileNameAndPath();
			FileContainer fileContainer = FileContainerFactory.getFileContainer(filePath);
			String directory = fileContainer.getParent();
			if (!FileUtils.fileExists(directory)) {
				FileUtils.createDirectoryForFile(new File(filePath));
			}
		}
		catch (Exception ex) {
			LogUtils.error(getClass(), "Failed to create log directory.", ex);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////          FixMessageEntry Business Methods               ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixMessageEntry getFixMessageEntry(long id) {
		return getFixMessageEntryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<FixMessageField> getFixMessageEntryFieldList(long messageId) {
		FixMessageEntry msg = getFixMessageEntry(messageId);
		return getFixMessageEntryFieldList(msg);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<FixMessageField> getFixMessageEntryFieldListForRawMessageText(String rawMessage) {
		if (StringUtils.isEmpty(rawMessage)) {
			return null;
		}
		try {
			// Spring form based deserialization strips the SOH character at the end of the messageText - re-add if necessary
			if (!rawMessage.endsWith("\u0001")) {
				rawMessage += "\u0001";
			}
			return getFixUtilService().getFixMessageFieldList(rawMessage);
		}
		catch (Throwable e) {
			// Don't log these as real errors since it's just a preview for testing
			throw new ValidationException("Error generating preview for raw message text: " + ExceptionUtils.getOriginalMessage(e));
		}
	}


	@SuppressWarnings("unchecked")
	private List<FixMessageField> getFixMessageEntryFieldList(FixMessageEntry msg) {
		if (msg.getType().isMessage()) {
			return getFixUtilService().getFixMessageFieldList(msg.getText());
		}
		// Prevent Null Pointer - If we log entry is not a message - fields unavailable to parse it so just return it with one field as the the message
		List<FixMessageField> result = new ArrayList<>();
		FixMessageField messageField = new FixMessageField();
		messageField.setFieldName(msg.getType().getName());
		messageField.setFieldValue(msg.getText());
		result.add(messageField);
		return result;
	}


	@Override
	public List<FixMessageEntry> getFixMessageEntryList(final FixMessageEntrySearchForm searchForm) {
		if (((searchForm.getHeartbeatMessages() != null) && searchForm.getHeartbeatMessages()) || ((searchForm.getLatestHeartbeatsOnly() != null) && searchForm.getLatestHeartbeatsOnly())) {
			return getHeartbeatList(searchForm);
		}

		if ((searchForm.getNonPersistentMessages() != null) && searchForm.getNonPersistentMessages()) {
			return getNonPersistentMessageList(searchForm);
		}

		if (searchForm.getFixMessageEntryTypeIdsName() != null) {
			searchForm.setTypeIds(getFixMessageEntryTypeIdsCache().getFixMessageEntryTypes(searchForm.getFixMessageEntryTypeIdsName(), this));
			// If the result is empty, return empty list - no results
			if (ArrayUtils.getLength(searchForm.getTypeIds()) == 0) {
				return new PagingArrayList<>();
			}
		}

		return getFixMessageEntryDAO().findBySearchCriteria(new FixMessageEntrySearchFormConfigurer(searchForm, this));
	}


	@Override
	@Transactional
	public FixMessageEntry saveFixMessageEntry(FixMessageEntry bean) {
		ValidationUtils.assertNotNull(bean.getType(), "The FixMessageEntry must have a type value.");
		if (!bean.getType().isPersistent()) {
			return bean.getType().isCacheNonPersistentMessage() ? this.saveNonPersistentMessage(bean) : bean;
		}

		return getFixMessageEntryDAO().save(bean);
	}


	@Override
	public FixMessageEntry createMessageEntry(String text, String typeName, Boolean incoming, FixSession fixSession) {
		return createMessageEntry(text, typeName, incoming, new Date(), fixSession);
	}


	@Override
	@SuppressWarnings("unchecked")
	public FixMessageEntry createMessageEntry(String text, String typeName, Boolean incoming, Date entryDate, FixSession fixSession) {
		FixMessageEntry entry = null;
		FixMessageEntryType type = null;
		try {
			// if its a message type, resolve it further
			if (MESSAGE_ENTRY_TYPE_NAME.equals(typeName)) {
				String typeCode = getFixUtilService().getMessageTypeCode(text);
				type = getFixMessageTypeByTagValue(typeCode);
			}
			else {
				type = getFixMessageEntryTypeByName(typeName);
			}

			// get the entry type, if none is found use the default to avoid losing messages.
			if (type == null) {
				type = getFixMessageEntryTypeDefault();
			}

			if (fixSession == null) {
				Object message = getFixUtilService().parseMessage(text);
				fixSession = getFixUtilService().getFixSession(message, null, incoming);
			}

			// create and save the new message
			entry = new FixMessageEntry();
			entry.setText(text);
			entry.setMessageEntryDateAndTime(entryDate);
			entry.setSession(fixSession);
			entry.setType(type);
			if (entry.getType().isMessage()) {
				entry.setProcessed(false);
			}
			entry.setIncoming(incoming);
			return saveFixMessageEntry(entry);
		}
		catch (Throwable e) {
			try {
				if (((type != null) && type.isMessage()) || (!ERROR_EVENT_ENTRY_TYPE_NAME.equals(typeName) && !EVENT_ENTRY_TYPE_NAME.equals(typeName))) {
					FixMessageEntry errorQueueEntry = entry;
					if (errorQueueEntry == null) {
						errorQueueEntry = new FixMessageEntry();
						errorQueueEntry.setText(text);
						errorQueueEntry.setMessageEntryDateAndTime(entryDate);
						FixMessageEntryType errorQueueEntryType = new FixMessageEntryType();
						errorQueueEntryType.setName(typeName);
						errorQueueEntry.setType(errorQueueEntryType);
					}
					if (errorQueueEntry.getId() == null) {
						errorQueueEntry.setId(getFailedMessageId().getAndDecrement());
					}
					getFixFailedMessageQueue().add(errorQueueEntry);
				}
			}
			catch (Throwable ex) {
				LogUtils.error(getClass(), "Failed to put message in failed message queue: [" + text + "]", ex);
			}
			throw new RuntimeException("Failed to save message: [" + text + "]", e);
		}
	}


	@Override
	public int deleteFixMessageEntryHistoricalList(Date maximumEntryDate, Short[] fixMessageEntryTypeIds) {
		// Direct SQL here is a significant performance improvement.  About 100K rows to delete, direct SQL < 4 seconds, Using search form a and delete List (which deletes one at a time < 1 Minute)
		String params = Arrays.stream(fixMessageEntryTypeIds)
				.map(String::valueOf)
				.collect(Collectors.joining(", "));

		return getSqlHandler().executeUpdate(SqlUpdateCommand.forSql(
				"DELETE FROM FixMessageEntry WHERE FixMessageEntryTypeID IN (" + params + ") AND MessageEntryDateAndTime <= ?")
				.addDateParameterValue(maximumEntryDate)
		);
	}


	/**
	 * Use JDBC statement to clear the administrative entries because the table could be very large.
	 */
	@Override
	@Transactional
	public void clearFixMessageEntriesForSession(int fixSessionId) {
		getSqlHandler().executeUpdate(SqlUpdateCommand.forSql(
				"DELETE FROM FixMessageEntry WHERE FixMessageEntryTypeID IN (SELECT FixMessageEntryTypeID FROM FixMessageEntryType WHERE isMessage = 0) AND FixSessionID = ?")
				.addIntegerParameterValue(fixSessionId)
		);
		getCacheHandler().clear(FixMessageEntry.class.getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Non-Persistent FixMessageEntry Business Methods ////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public List<FixMessageField> getFixMessageEntryFieldListNonPersistent(FixMessageEntrySearchForm searchForm) {
		FixMessageEntry msg = getFixMessageEntryNonPersistent(searchForm);
		return getFixUtilService().getFixMessageFieldList(msg.getText());
	}


	@Override
	public FixMessageEntry getFixMessageEntryNonPersistent(final FixMessageEntrySearchForm searchForm) {
		List<FixMessageEntry> resultList = getFixMessageEntryList(searchForm);
		ValidationUtils.assertNotNull(searchForm.getId(), "An entry ID is required.");
		ValidationUtils.assertTrue((searchForm.getHeartbeatMessages() != null && searchForm.getHeartbeatMessages()) || (searchForm.getNonPersistentMessages() != null && searchForm.getNonPersistentMessages()), "Either the heartbeatMessages or the nonPersistentMessages property must be enabled.");
		return CollectionUtils.getOnlyElement(resultList, "Expected a single entity, but found multiple.");
	}


	@Override
	public List<FixMessageEntry> getFixMessageEntryFailedMessageList() {
		return getFixFailedMessageQueue().getElementList();
	}


	@Override
	public FixMessageEntry getFixMessageEntryFailedMessage(Long id) {
		List<FixMessageEntry> filteredList = CollectionUtils.getFiltered(getFixMessageEntryFailedMessageList(), entry -> id.equals(entry.getId()));
		return CollectionUtils.getFirstElement(filteredList);
	}


	@Override
	public List<FixMessageField> getFixMessageEntryFailedFieldList(Long id) {
		FixMessageEntry fixMessageEntry = getFixMessageEntryFailedMessage(id);
		if (fixMessageEntry == null) {
			return new ArrayList<>();
		}
		return getFixMessageEntryFieldList(fixMessageEntry);
	}


	@Override
	public List<FixMessageEntryHeartbeatStatus> getFixMessageEntryLastHeartbeatBySession(FixMessageEntryHeartbeatStatusCommand command) {
		List<FixMessageEntryHeartbeatStatus> result = new ArrayList<>();
		List<FixSessionDefinition> sessionDefList = getFixSessionService().getFixSessionDefinitionListActiveAndPopulated();
		for (FixSessionDefinition def : CollectionUtils.getIterable(sessionDefList)) {
			FixSession session = getFixSessionService().getFixSessionCurrentByDefinition(def);
			FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
			searchForm.setHeartbeatMessages(true);
			searchForm.setLatestHeartbeatsOnly(true);
			searchForm.setSessionId(session.getId());
			List<FixMessageEntry> entryList = getFixMessageEntryList(searchForm);
			FixMessageEntry lastHeartbeatMessage = CollectionUtils.getFirstElement(entryList);

			FixMessageEntryHeartbeatStatus heartbeatStatus = createFixMessageHeartbeatStatus(command, def, lastHeartbeatMessage);
			if (heartbeatStatus != null && (!command.isReturnWarningsOnly() || heartbeatStatus.isFailed())) {
				result.add(heartbeatStatus);
			}
		}
		return result;
	}


	/**
	 * Creates and populates a FixMessageHeartbeatStatus.
	 *
	 * @return a populated FixMessageHeartbeatStatus entity for active sessions (or inactive sessions with command.isActiveSessionsOnly == false).
	 * null if session is not active and command.isActiveSessionsOnly == true.
	 */
	private FixMessageEntryHeartbeatStatus createFixMessageHeartbeatStatus(FixMessageEntryHeartbeatStatusCommand command, FixSessionDefinition sessionDefinition, FixMessageEntry lastHeartBeatMessage) {
		LocalDateTime localStartTime = LocalDateTime.now(FixUtils.parseTimeZone(sessionDefinition.getStartTime()));
		LocalDateTime localEndTime = LocalDateTime.now(FixUtils.parseTimeZone(sessionDefinition.getEndTime()));

		// populate if command.isActiveSessionsOnly == false OR if (command.isActiveSessionsOnly == true AND isSessionActive == true)
		if (!command.isActiveSessionsOnly() || FixUtils.isSessionActive(sessionDefinition, localStartTime, localEndTime)) {
			FixMessageEntryHeartbeatStatus fixMessageHeartbeatStatus = new FixMessageEntryHeartbeatStatus(sessionDefinition, lastHeartBeatMessage == null ? null : lastHeartBeatMessage.getMessageEntryDateAndTime());
			int maxHeartBeatTimeInterval = command.getMaximumMinutesSinceLastHeartbeatWarn();
			StringBuilder msg = new StringBuilder(sessionDefinition.getLabel());
			if (fixMessageHeartbeatStatus.getLastHeartbeatDateAndTime() == null || DateUtils.compare(DateUtils.addMinutes(new Date(), -maxHeartBeatTimeInterval), fixMessageHeartbeatStatus.getLastHeartbeatDateAndTime(), true) > 0) {
				msg.append(" did not have a heartbeat in the last ").append(maxHeartBeatTimeInterval).append("  minutes.");
				if (fixMessageHeartbeatStatus.getLastHeartbeatDateAndTime() != null) {
					msg.append("Last heartbeat was at [").append(DateUtils.fromDate(fixMessageHeartbeatStatus.getLastHeartbeatDateAndTime())).append("]");
				}
				if (FixUtils.isSessionWithinStartUpOrShutDownMinutes(sessionDefinition, 10, localStartTime, localEndTime)) {
					msg.append(" - WARNING ONLY (within start up / shut down grace period of 10 minutes)");
				}
				else {
					fixMessageHeartbeatStatus.setFailed(true);
				}
			}
			else {
				msg.append(" - last heartbeat:  [").append(DateUtils.fromDate(fixMessageHeartbeatStatus.getLastHeartbeatDateAndTime())).append("]");
				fixMessageHeartbeatStatus.setFailed(false);
			}
			fixMessageHeartbeatStatus.setStatusMessage(msg.toString());
			return fixMessageHeartbeatStatus;
		}

		return null;
	}


	@Override
	public int getFixHeartbeatMessageCapacity() {
		return getHeartbeatMessageCapacity();
	}


	@Override
	@SuppressWarnings("UseBulkOperation")
	public void updateFixHeartbeatMessageCapacity(int capacity) {
		Lock writeLock = getHeartbeatLogMapLock().writeLock();
		try {
			writeLock.lock();
			setHeartbeatMessageCapacity(capacity);
			getHeartbeatLogMap().replaceAll((sessionId, existingList) -> {
				List<FixMessageEntry> newList = new RollingList<>(capacity);
				// Call #add instead of #addAll to retain RollingList capacity constraint
				existingList.forEach(newList::add);
				return newList;
			});
		}
		finally {
			writeLock.unlock();
		}
	}


	@Override
	@SuppressWarnings("UseBulkOperation")
	public void updateFixNonPersistentMessageCapacityForSession(FixSession fixSession, int capacity) {
		Lock writeLock = getNonPersistentMessageLogMapLock().writeLock();
		try {
			writeLock.lock();
			getNonPersistentMessageLogMap().computeIfPresent(fixSession.getId(), (sessionId, existingList) -> {
				List<FixMessageEntry> newList = new RollingList<>(capacity);
				// Call #add instead of #addAll to retain RollingList capacity constraint
				existingList.forEach(newList::add);
				return newList;
			});
		}
		finally {
			writeLock.unlock();
		}
	}


	@Override
	public void clearFixHeartbeatList() {
		Lock writeLock = getHeartbeatLogMapLock().writeLock();
		try {
			writeLock.lock();
			getHeartbeatLogMap().clear();
		}
		finally {
			writeLock.unlock();
		}
	}


	@Override
	public void clearFixNonPersistentMessageList() {
		Lock writeLock = getNonPersistentMessageLogMapLock().writeLock();
		try {
			writeLock.lock();
			getNonPersistentMessageLogMap().clear();
		}
		finally {
			writeLock.unlock();
		}
	}


	@Override
	public void clearFixNonPersistentMessageListForSession(FixSession fixSession) {
		Lock writeLock = getNonPersistentMessageLogMapLock().writeLock();
		try {
			writeLock.lock();
			getNonPersistentMessageLogMap().remove(fixSession.getId());
		}
		finally {
			writeLock.unlock();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////           FixMessageEntryType Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixMessageEntryType getFixMessageEntryType(short id) {
		return getFixMessageEntryTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public FixMessageEntryType getFixMessageEntryTypeByName(String typeName) {
		return getFixMessageEntryTypeCache().getBeanForKeyValue(getFixMessageEntryTypeDAO(), typeName);
	}


	@Override
	public FixMessageEntryType getFixMessageEntryTypeDefault() {
		FixMessageEntryType result = getFixMessageEntryTypeDefaultFromCache();
		if (result == null) {
			result = getFixMessageEntryTypeDAO().findOneByField("defaultType", true);
			ValidationUtils.assertNotNull(result, "Cannot find default fix log entry type. One fix log entry type must be marked as default.");
			setFixMessageEntryTypeDefaultInCache(result);
		}
		return result;
	}


	@Override
	public List<FixMessageEntryType> getFixMessageEntryTypeList(FixMessageEntryTypeSearchForm searchForm) {
		if (searchForm.getFixMessageEntryTypeIdsName() != null) {
			searchForm.setIds(getFixMessageEntryTypeIdsCache().getFixMessageEntryTypes(searchForm.getFixMessageEntryTypeIdsName(), this));
			// If the result is empty, return empty list - no results
			if (ArrayUtils.getLength(searchForm.getIds()) == 0) {
				return new PagingArrayList<>();
			}
		}

		return getFixMessageEntryTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public FixMessageEntryType getFixMessageTypeByTagValue(String value) {
		return getFixMessageEntryTypeByTagValueCache().getBeanForKeyValue(getFixMessageEntryTypeDAO(), value);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<FixMessageEntry> getHeartbeatList(FixMessageEntrySearchForm searchForm) {
		return getNonPersistentMessageList(searchForm, getHeartbeatLogMap(), searchForm.getLatestHeartbeatsOnly());
	}


	private List<FixMessageEntry> getNonPersistentMessageList(FixMessageEntrySearchForm searchForm) {
		return getNonPersistentMessageList(searchForm, getNonPersistentMessageLogMap(), searchForm.getLatestNonPersistentMessagesOnly());
	}


	@SuppressWarnings("unchecked")
	private List<FixMessageEntry> getNonPersistentMessageList(FixMessageEntrySearchForm searchForm, Map<Short, List<FixMessageEntry>> logMap, Boolean latestEntriesOnly) {
		ReadOnlyDAO<FixMessageEntry> dao = getFixMessageEntryDAO();
		SearchConfigurer<Criteria> searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public boolean configureOrderBy(Criteria criteria) {
				if (BooleanUtils.isTrue(latestEntriesOnly)) {
					searchForm.setOrderBy("MessageEntryDateAndTime:desc");
					if (!StringUtils.isEmpty(searchForm.getSessionDefinitionName()) || searchForm.getSessionDefinitionId() != null || searchForm.getSessionId() != null) {
						searchForm.setLimit(1);
					}
				}

				return super.configureOrderBy(criteria);
			}
		};

		Criteria criteria = new XmlBasedCriteria<>(dao.getConfiguration(), getApplicationContext(), true, true);
		searchConfigurer.configureCriteria(criteria);
		searchConfigurer.configureOrderBy(criteria);
		List<FixMessageEntry> allRecords = logMap.values().stream()
				// #toArray is used here to avoid concurrent modification exceptions while iterating over lists which are likely to be mutated during high volumes
				.flatMap(entryList -> Arrays.stream(entryList.toArray(new FixMessageEntry[0])))
				.collect(Collectors.toList());
		List<FixMessageEntry> filtered = ((XmlBasedCriteria) criteria).list(allRecords);
		return filtered == null ? new PagingArrayList<>(Collections.emptyList(), searchForm.getStart(), 0, searchForm.getLimit())
				: CollectionUtils.toPagingArrayList(filtered, searchForm.getStart(), searchForm.getLimit());
	}


	private FixMessageEntry saveNonPersistentMessage(FixMessageEntry bean) {
		final Map<Short, List<FixMessageEntry>> logMap;
		final Lock readLock;
		final int capacity;
		final long entryId;

		// place select the map for either heartbeats or other non-persistent messages
		if (HEARTBEAT_MESSAGE_ENTRY_TYPE_NAME.equals(bean.getType().getName())) {
			logMap = getHeartbeatLogMap();
			readLock = getHeartbeatLogMapLock().readLock();
			capacity = getFixHeartbeatMessageCapacity();
			entryId = getHeartbeatId().getAndDecrement();
		}
		else {
			logMap = getNonPersistentMessageLogMap();
			readLock = getNonPersistentMessageLogMapLock().readLock();
			FixSessionDefinition definition = bean.getSession().getDefinition();
			capacity = AssertUtils.assertNotNull(definition.getCoalesceNonPersistentMessageCacheSize(), () -> String.format("No non-persistent message cache size was found for session [%s].", definition.getName()));
			entryId = getNonPersistentMessageId().getAndDecrement();
		}

		try {
			// Acquire read lock only since the map itself is not mutated in a non-thread-safe manner
			readLock.lock();
			List<FixMessageEntry> messageLog = logMap.computeIfAbsent(bean.getSession().getId(), key -> new RollingList<>(capacity));
			bean.setId(entryId);
			messageLog.add(bean);
			return bean;
		}
		finally {
			readLock.unlock();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                  Cache Helper Methods                   //////////
	////////////////////////////////////////////////////////////////////////////


	private FixMessageEntryType getFixMessageEntryTypeDefaultFromCache() {
		return getCacheHandler().get(CACHE_DEFAULT_FIX_LOG_ENTRY_TYPE, true);
	}


	private void setFixMessageEntryTypeDefaultInCache(FixMessageEntryType entryType) {
		getCacheHandler().put(CACHE_DEFAULT_FIX_LOG_ENTRY_TYPE, true, entryType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixMessageEntry, Criteria> getFixMessageEntryDAO() {
		return this.FixMessageEntryDAO;
	}


	public void setFixMessageEntryDAO(AdvancedUpdatableDAO<FixMessageEntry, Criteria> fixMessageEntryDAO) {
		this.FixMessageEntryDAO = fixMessageEntryDAO;
	}


	public AdvancedReadOnlyDAO<FixMessageEntryType, Criteria> getFixMessageEntryTypeDAO() {
		return this.FixMessageEntryTypeDAO;
	}


	public void setFixMessageEntryTypeDAO(AdvancedReadOnlyDAO<FixMessageEntryType, Criteria> fixMessageEntryTypeDAO) {
		this.FixMessageEntryTypeDAO = fixMessageEntryTypeDAO;
	}


	public CacheHandler<Object, FixMessageEntryType> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Object, FixMessageEntryType> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public DaoNamedEntityCache<FixMessageEntryType> getFixMessageEntryTypeCache() {
		return this.FixMessageEntryTypeCache;
	}


	public void setFixMessageEntryTypeCache(DaoNamedEntityCache<FixMessageEntryType> fixMessageEntryTypeCache) {
		this.FixMessageEntryTypeCache = fixMessageEntryTypeCache;
	}


	public FixMessageEntryTypeIdsCache getFixMessageEntryTypeIdsCache() {
		return this.FixMessageEntryTypeIdsCache;
	}


	public void setFixMessageEntryTypeIdsCache(FixMessageEntryTypeIdsCache fixMessageEntryTypeIdsCache) {
		this.FixMessageEntryTypeIdsCache = fixMessageEntryTypeIdsCache;
	}


	public DaoSingleKeyCache<FixMessageEntryType, String> getFixMessageEntryTypeByTagValueCache() {
		return this.fixMessageEntryTypeByTagValueCache;
	}


	public void setFixMessageEntryTypeByTagValueCache(DaoSingleKeyCache<FixMessageEntryType, String> fixMessageEntryTypeByTagValueCache) {
		this.fixMessageEntryTypeByTagValueCache = fixMessageEntryTypeByTagValueCache;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixUtilService getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService fixUtilService) {
		this.fixUtilService = fixUtilService;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public AtomicLong getHeartbeatId() {
		return this.heartbeatId;
	}


	public AtomicLong getNonPersistentMessageId() {
		return this.nonPersistentMessageId;
	}


	public AtomicLong getFailedMessageId() {
		return this.failedMessageId;
	}


	public Map<Short, List<FixMessageEntry>> getHeartbeatLogMap() {
		return this.heartbeatLogMap;
	}


	public Map<Short, List<FixMessageEntry>> getNonPersistentMessageLogMap() {
		return this.nonPersistentMessageLogMap;
	}


	public ReadWriteLock getHeartbeatLogMapLock() {
		return this.heartbeatLogMapLock;
	}


	public ReadWriteLock getNonPersistentMessageLogMapLock() {
		return this.nonPersistentMessageLogMapLock;
	}


	public int getHeartbeatMessageCapacity() {
		return this.heartbeatMessageCapacity;
	}


	public void setHeartbeatMessageCapacity(int heartbeatMessageCapacity) {
		this.heartbeatMessageCapacity = heartbeatMessageCapacity;
	}


	public Queue<FixMessageEntry> getFixFailedMessageQueue() {
		return this.fixFailedMessageQueue;
	}


	public void setFixFailedMessageQueue(Queue<FixMessageEntry> fixFailedMessageQueue) {
		this.fixFailedMessageQueue = fixFailedMessageQueue;
	}
}
