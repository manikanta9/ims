package com.clifton.fix.message.validation;

/**
 * <code>FixServerMessageValidator</code> performs message field validations.
 */
public interface FixMessageValidator<M, R> {

	public R isValid(M m1, M m2);
}
