package com.clifton.fix.message;


import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * An interface for the front-end services (such as FixMessageService and FixLogService) that specifies the conversion methods which must be overridden to convert the FixMessageEntry to
 * the designated type T (such as FixMessage or FixLogEntry).
 *
 * @author davidi
 */
public interface BaseFixMessageEntryService<T> {

	@DoNotAddRequestMapping
	public T createEntity(FixMessageEntry entry);


	@DoNotAddRequestMapping
	public List<T> createEntityList(List<FixMessageEntry> entryList);
}

