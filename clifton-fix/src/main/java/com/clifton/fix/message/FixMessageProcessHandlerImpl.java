package com.clifton.fix.message;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.identifier.FixIdentifierHandler;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.session.FixSessionParametersImpl;
import com.clifton.fix.util.FixUtilService;
import com.clifton.fix.util.FixUtils;

import java.util.Date;
import java.util.List;


@SuppressWarnings("rawtypes")
public class FixMessageProcessHandlerImpl implements FixMessageProcessHandler<FixMessageEntry> {

	private FixIdentifierHandler fixIdentifierHandler;
	private FixMessageEntryService fixMessageEntryService;
	private FixUtilService fixUtilService;


	@Override
	public FixMessageEntry processMessage(FixMessageEntry fixMessageEntry) {
		return convertMessageEntry(fixMessageEntry);
	}


	/**
	 * This method fill in additional fields pertinent to a FIX Message on the unprocessed incoming messages.
	 * Only non-administrative persistent messages require processing.
	 *
	 * @param from
	 */
	@SuppressWarnings("unchecked")
	private FixMessageEntry convertMessageEntry(FixMessageEntry from) {
		String uniqueId = null;

		if (from.getType().isPersistent() && (from.getProcessed() == null || BooleanUtils.isFalse(from.getProcessed()))) {
			try {
				if (!from.getType().isAdministrative()) {
					// try to resolve type to an actual message type for messages of generic MESSAGE type.
					if (FixMessageEntryService.MESSAGE_ENTRY_TYPE_NAME.equals(from.getType().getName())) {
						FixMessageEntryType entryType = getFixMessageEntryService().getFixMessageTypeByTagValue(getFixUtilService().getMessageTypeCode(from.getText()));
						if (entryType != null) {
							from.setType(entryType);
						}
					}
					if (from.getType().getIdFieldTag() != null) {
						uniqueId = getFixUtilService().getStringField(from.getText(), from.getType().getIdFieldTag());
					}
					if (StringUtils.isEmpty(uniqueId) && (BooleanUtils.isTrue(from.getIncoming())) && (from.getType().getDropCopyIdFieldTag() != null)) {
						uniqueId = getFixUtilService().getStringField(from.getText(), from.getType().getDropCopyIdFieldTag());
					}
					if (!StringUtils.isEmpty(uniqueId)) {
						uniqueId = FixUtils.cleanUniqueId(uniqueId);
					}
					if (uniqueId == null) {
						//TODO Handle Missing Fix MsgTypes after reviewing fix logs ie- MassCxl types etc
						LogUtils.warn(getClass(), String.format("Null uniqueId for Msg: [%s].", from.getText()));
					}

					FixIdentifier identifier = getFixIdentifierHandler().getFixIdentifierByStringId(uniqueId, from.getSession().getId());

					if (identifier == null) {
						// Note:  parseMessage is generally needed when reprocessing an existing message where identifier was not originally determined.
						Object message = getFixUtilService().parseMessage(from.getText());
						identifier = getFixIdentifierHandler().applyFixIdentifier(message, getFixMessagingMessageFromMessage(from), from.getIncoming(), from.getSession());
					}
					ValidationUtils.assertNotNull(identifier, "Cannot find FixIdentifier for [" + uniqueId + "] and session [" + (new FixSessionParametersImpl(from.getSession())) + "].");

					from.setIdentifier(identifier);
				}
				from.setProcessed(true);
				return getFixMessageEntryService().saveFixMessageEntry(from);
			}

			catch (Throwable e) {
				throw new RuntimeException("Failed to convert FixEntryMessage [" + from.getId() + "] to FixMessage with unique id [" + uniqueId + "].", e);
			}
		}
		return from;
	}


	private FixMessageTextMessagingMessage getFixMessagingMessageFromMessage(FixMessageEntry from) {
		FixMessageTextMessagingMessage result = new FixMessageTextMessagingMessage();
		result.setMessageText(from.getText());
		result.setSessionQualifier(from.getSession().getDefinition().getSessionQualifier());
		return result;
	}


	public void init() {
		loadUnprocessedItem();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void loadUnprocessedItem() {
		final int maxHistoricalDays = 60;
		Date minimumDate = DateUtils.addDays(new Date(), -maxHistoricalDays);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setProcessed(false);
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_NOT_ADMINISTRATIVE);
		searchForm.addSearchRestriction("messageEntryDateAndTime", ComparisonConditions.GREATER_THAN_OR_EQUALS, minimumDate);

		List<FixMessageEntry> entryList = getFixMessageEntryService().getFixMessageEntryList(searchForm);
		for (FixMessageEntry entry : CollectionUtils.getIterable(entryList)) {
			try {
				processMessage(entry);
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), "Failed to load unprocessed message.", e);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}


	public FixUtilService getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService fixUtilService) {
		this.fixUtilService = fixUtilService;
	}


	public FixIdentifierHandler getFixIdentifierHandler() {
		return this.fixIdentifierHandler;
	}


	public void setFixIdentifierHandler(FixIdentifierHandler fixIdentifierHandler) {
		this.fixIdentifierHandler = fixIdentifierHandler;
	}
}
