package com.clifton.fix.message;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.message.search.FixMessageEntryTypeSearchForm;
import com.clifton.fix.session.FixSession;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.List;


public interface FixMessageEntryService {

	public static final String MESSAGE_ENTRY_TYPE_NAME = "Message";

	public static final String EVENT_ENTRY_TYPE_NAME = "Event";

	public static final String ERROR_EVENT_ENTRY_TYPE_NAME = "Error Event";

	public static final String HEARTBEAT_MESSAGE_ENTRY_TYPE_NAME = "Heartbeat Message";

	public static final String ADMIN_MESSAGE_ENTRY_TYPE_NAME = "Admin Message";

	////////////////////////////////////////////////////////////////////////////
	////////           FixMessageEntry Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntry getFixMessageEntry(long id);


	@SecureMethod(dtoClass = FixMessageEntry.class)
	public List<FixMessageField> getFixMessageEntryFieldList(long messageId);


	/**
	 * Useful when getting a raw message from an external party and converting it into a readable format
	 */
	@SecureMethod(dtoClass = FixMessageEntry.class)
	public List<FixMessageField> getFixMessageEntryFieldListForRawMessageText(String rawMessage);


	public List<FixMessageEntry> getFixMessageEntryList(FixMessageEntrySearchForm searchForm);


	@SecureMethod(dtoClass = FixMessageEntry.class)
	public List<FixMessageEntryHeartbeatStatus> getFixMessageEntryLastHeartbeatBySession(FixMessageEntryHeartbeatStatusCommand command);


	@DoNotAddRequestMapping
	public FixMessageEntry saveFixMessageEntry(FixMessageEntry bean);


	/**
	 * @see #createMessageEntry(String, String, Boolean, Date, FixSession)
	 */
	@DoNotAddRequestMapping
	public FixMessageEntry createMessageEntry(String text, String typeName, Boolean incoming, FixSession fixSession);


	/**
	 * Creates a FixMessageEntry that is either a an entity of class FixMessageEntry or subclass FixMessage,
	 * depending on the FixMessageEntryType derived from the typeName parameter.  Any message type will return a FixMessage.
	 * Messages representing events or have a type that is not considered a FixMessage are returned as FixMessageEntry entities.
	 */
	@DoNotAddRequestMapping
	public FixMessageEntry createMessageEntry(String text, String typeName, Boolean incoming, Date entryDate, FixSession fixSession);


	// Only used through the batch job - returns the # of rows affected, uses direct sql
	@DoNotAddRequestMapping
	public int deleteFixMessageEntryHistoricalList(Date maximumEntryDate, Short[] fixMessageEntryTypeIds);


	@DoNotAddRequestMapping
	public void clearFixMessageEntriesForSession(int fixSessionId);

	////////////////////////////////////////////////////////////////////////////
	////////            Non-Persistent FixMessageEntry Business Methods ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A special method to get the field list from a non-persistent FixMessage from in-memory storage.  The
	 * searchForm should set the ID of the entity as well as heartbeatMessages=true or nonPersistentMessages=true
	 */
	@SecureMethod(dtoClass = FixMessageEntry.class)
	public List<FixMessageField> getFixMessageEntryFieldListNonPersistent(FixMessageEntrySearchForm searchForm);


	/**
	 * A special method to get a non-persistent FixMessage from in-memory storage.  The
	 * searchForm should set the ID of the entity as well as heartbeatMessages=true or nonPersistentMessages=true
	 */
	public FixMessageEntry getFixMessageEntryNonPersistent(FixMessageEntrySearchForm searchForm);


	/**
	 * A special method to get a failed FixMessageEntry from in-memory storage.
	 */
	public List<FixMessageEntry> getFixMessageEntryFailedMessageList();


	public FixMessageEntry getFixMessageEntryFailedMessage(Long id);


	@SecureMethod(dtoClass = FixMessageEntry.class)
	public List<FixMessageField> getFixMessageEntryFailedFieldList(Long id);


	@ResponseBody
	@SecureMethod(dtoClass = FixMessageEntry.class)
	public int getFixHeartbeatMessageCapacity();


	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void updateFixHeartbeatMessageCapacity(int capacity);


	@DoNotAddRequestMapping
	public void updateFixNonPersistentMessageCapacityForSession(FixSession fixSession, int capacity);


	/**
	 * Clears the heartbeat messages cache. This should be used for debugging by administrators only.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void clearFixHeartbeatList();


	/**
	 * Clears the full non-persistent message cache. This should be used for debugging by administrators only.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void clearFixNonPersistentMessageList();


	/**
	 * Clears the non-persistent message cache for the given session. This should be used for debugging by administrators only.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void clearFixNonPersistentMessageListForSession(FixSession fixSession);

	////////////////////////////////////////////////////////////////////////////
	////////         FixMessageEntryType Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryType getFixMessageEntryType(short id);


	public FixMessageEntryType getFixMessageEntryTypeByName(String typeName);


	public FixMessageEntryType getFixMessageEntryTypeDefault();


	public List<FixMessageEntryType> getFixMessageEntryTypeList(FixMessageEntryTypeSearchForm searchForm);


	public FixMessageEntryType getFixMessageTypeByTagValue(String value);
}
