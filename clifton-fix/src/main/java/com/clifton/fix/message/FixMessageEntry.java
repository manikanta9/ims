package com.clifton.fix.message;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.log.FixLogEntry;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.setup.FixSourceSystem;

import java.util.Date;


/**
 * The <code>FixMessageEntry</code> represents a FixEntry which represents the shared properties for FIX Message, Administrative FIX Message or event.
 * The <code>FixMessage</code> class is a subclass of FixMessageEntry that has additional fields used by FIX Messages.
 *
 * @author davidi
 */
public class FixMessageEntry extends BaseSimpleEntity<Long> {

	/**
	 * Versioning support
	 */
	private byte[] rv;

	private FixSession session;
	private FixMessageEntryType type;
	private Date messageEntryDateAndTime;
	private String text;
	/**
	 * Indicates that this is an incoming message.  This field will be null for all types other non administrative FIX messages.
	 */
	private Boolean incoming;

	/**
	 * Indicates the message entry has been processed.  This flag applies to both the MessageEntry and FixMessage subclass.
	 */
	private Boolean processed;

	private FixIdentifier identifier;

	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntry() {
	}


	public FixMessageEntry(Boolean incoming) {
		if (incoming != null) {
			setIncoming(incoming);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a non-persistent FixMessage entity from this FixMessageEntry entity.
	 */
	public FixMessage toFixMessage() {
		FixMessage fixMessage = new FixMessage();
		fixMessage.setId(getId());
		fixMessage.setSession(getSession());
		fixMessage.setType(getType());
		fixMessage.setMessageEntryDateAndTime(getMessageEntryDateAndTime());
		fixMessage.setText(getText());
		fixMessage.setIncoming(getIncoming());
		fixMessage.setProcessed(getProcessed());
		fixMessage.setIdentifier(getIdentifier());
		return fixMessage;
	}


	/**
	 * Generates a non-persistent FixLogEntry entity from this FixMessageEntry entity.
	 */
	public FixLogEntry toFixLogEntry() {
		FixLogEntry fixLogEntry = new FixLogEntry();
		fixLogEntry.setId(getId());
		fixLogEntry.setSession(getSession());
		fixLogEntry.setType(getType());
		fixLogEntry.setMessageEntryDateAndTime(getMessageEntryDateAndTime());
		fixLogEntry.setText(getText());
		fixLogEntry.setIncoming(getIncoming());
		return fixLogEntry;
	}


	/**
	 * Returns {@link FixSourceSystem} from entry's identifier if one is defined.
	 * If not defined, returns the {@link FixSourceSystem} for the Unknown Messages from the entry's Session Definition.
	 */
	public FixSourceSystem getSourceSystemIncludingUnknown() {
		if (getIdentifier() != null && getIdentifier().getSourceSystem() != null) {
			return getIdentifier().getSourceSystem();
		}
		if (getSession() != null && getSession().getDefinition() != null) {
			return getSession().getDefinition().getUnknownMessageFixSourceSystem();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSession getSession() {
		return this.session;
	}


	public void setSession(FixSession session) {
		this.session = session;
	}


	public FixMessageEntryType getType() {
		return this.type;
	}


	public void setType(FixMessageEntryType entryType) {
		this.type = entryType;
	}


	public Date getMessageEntryDateAndTime() {
		return this.messageEntryDateAndTime;
	}


	public void setMessageEntryDateAndTime(Date messageEntryDateAndTime) {
		this.messageEntryDateAndTime = messageEntryDateAndTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String entryText) {
		this.text = entryText;
	}


	public Boolean getIncoming() {
		return this.incoming;
	}


	public void setIncoming(Boolean incoming) {
		this.incoming = incoming;
	}


	public Boolean getProcessed() {
		return this.processed;
	}


	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}


	public FixIdentifier getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(FixIdentifier identifier) {
		this.identifier = identifier;
	}


	public byte[] getRv() {
		return this.rv;
	}


	public void setRv(byte[] rv) {
		this.rv = rv;
	}
}
