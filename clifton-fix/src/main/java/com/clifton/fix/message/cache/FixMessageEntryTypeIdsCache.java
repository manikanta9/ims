package com.clifton.fix.message.cache;

import com.clifton.fix.message.FixMessageEntryService;


/**
 * The <code>FixLogEntryTypeIdsCache</code> caches the list of FixLogEntryTypeIds for given attributes
 *
 * @author manderson
 */
public interface FixMessageEntryTypeIdsCache {

	/**
	 * Returns an array of FixLogEntryType id's for the specified key. Use constants on this class for available key names.
	 * NOTE: use to improve performance by avoiding extra joins on large tables (FixLogEntry, etc.)
	 */
	public Short[] getFixMessageEntryTypes(FixMessageEntryTypeIds entryTypeIdsKey, FixMessageEntryService fixMessageEntryService);
}
