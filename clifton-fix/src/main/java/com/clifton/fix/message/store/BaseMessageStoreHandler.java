package com.clifton.fix.message.store;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.message.store.search.FixMessageStoreSearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.session.cache.FixSessionCache;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


/**
 * The <code>BaseMessageStoreHandler</code> base implementation of message store.
 * <p>
 * For an example implementation see com.clifton.fix.quickfix.server.store.QuickFixStore
 *
 * @param <T>
 * @author mwacker
 */
public abstract class BaseMessageStoreHandler<T> implements MessageStoreHandler {

	private FixSessionCache<T> fixSessionCache;
	private FixSessionService fixSessionService;
	private FixMessageStoreService fixMessageStoreService;

	private final T sessionId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseMessageStoreHandler(T sessionId) {
		this.sessionId = sessionId;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void afterPropertiesSet() {
		try {
			loadCache();
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to generate message store cache.", e);
		}
	}


	@Override
	public boolean set(int sequence, String message) {
		FixSession session = getFixSessionCache().get(this.sessionId);

		FixMessageStore store = new FixMessageStore();
		store.setSession(session);
		store.setSequenceNumber(sequence);
		store.setText(message);
		return getFixMessageStoreService().saveFixMessageStore(store);
	}


	@Override
	public void get(int startSequence, int endSequence, Collection<String> messages) {
		FixSession fixSession = getFixSessionCache().get(this.sessionId);

		FixMessageStoreSearchForm searchForm = new FixMessageStoreSearchForm();
		searchForm.setSessionId(fixSession.getId());
		searchForm.setStartSequenceNumber(startSequence);
		searchForm.setEndSequenceNumber(endSequence);
		searchForm.setOrderBy("sequenceNumber:asc");

		List<FixMessageStore> storeList = getFixMessageStoreService().getFixMessageStoreList(searchForm);
		for (FixMessageStore store : CollectionUtils.getIterable(storeList)) {
			messages.add(store.getText());
		}
	}


	@Override
	public int getNextOutgoingMsgSeqNum() {
		FixSession session = getFixSessionCache().get(this.sessionId);
		return session.getOutgoingSequenceNumber();
	}


	@Override
	public int getNextIncomingMsgSeqNum() {
		FixSession session = getFixSessionCache().get(this.sessionId);
		return session.getIncomingSequenceNumber();
	}


	@Override
	@ValueChangingSetter
	public void setNextOutgoingMsgSeqNum(int next) {
		storeSequenceNumbers(getNextIncomingMsgSeqNum(), next);
	}


	@Override
	@ValueChangingSetter
	public void setNextIncomingMsgSeqNum(int next) {
		storeSequenceNumbers(next, getNextOutgoingMsgSeqNum());
	}


	@Override
	public void incrNextOutgoingMsgSeqNum() {
		setNextOutgoingMsgSeqNum(getNextOutgoingMsgSeqNum() + 1);
	}


	@Override
	public void incrNextIncomingMsgSeqNum() {
		setNextIncomingMsgSeqNum(getNextIncomingMsgSeqNum() + 1);
	}


	@Override
	public Date getCreationTime() {
		FixSession session = getFixSessionCache().get(this.sessionId);
		return session.getCreationDateAndTime();
	}


	@Override
	public void reset() {
		FixSession session = getFixSessionCache().get(this.sessionId);

		getFixMessageStoreService().deleteFixMessageStoreForSession(session);

		session.setCreationDateAndTime(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
		session.setIncomingSequenceNumber(1);
		session.setOutgoingSequenceNumber(1);

		saveFixSession(session);
	}


	@Override
	public void refresh() {
		loadCache();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void loadCache() {
		FixSession session = getFixSessionCache().get(this.sessionId);
		if (session == null) {
			throw new RuntimeException("No session definition exists for [" + this.sessionId + "].");
		}
		if (session.isNewBean()) {
			session.setCreationDateAndTime(new Date());
			session.setIncomingSequenceNumber(1);
			session.setOutgoingSequenceNumber(1);
			session.setActiveSession(true);
			saveFixSession(session);
		}
	}


	protected synchronized void storeSequenceNumbers(int incoming, int outgoing) {
		FixSession session = getFixSessionCache().get(this.sessionId);
		getFixSessionService().storeFixSessionSequenceNumbers(session, incoming, outgoing);
	}


	/**
	 * Save the session and update the cached object.
	 */
	protected void saveFixSession(FixSession session) {
		FixSession fixSession = getFixSessionService().saveFixSessionInternal(session);
		getFixSessionCache().remove(this.sessionId);
		getFixSessionCache().put(this.sessionId, fixSession);
		storeSequenceNumbers(fixSession.getIncomingSequenceNumber(), fixSession.getOutgoingSequenceNumber());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionCache<T> getFixSessionCache() {
		return this.fixSessionCache;
	}


	public void setFixSessionCache(FixSessionCache<T> fixSessionCache) {
		this.fixSessionCache = fixSessionCache;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixMessageStoreService getFixMessageStoreService() {
		return this.fixMessageStoreService;
	}


	public void setFixMessageStoreService(FixMessageStoreService fixMessageStoreService) {
		this.fixMessageStoreService = fixMessageStoreService;
	}


	public T getSessionId() {
		return this.sessionId;
	}
}
