package com.clifton.fix.message.jobs;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;

import java.util.Date;
import java.util.Map;


/**
 * This job can be set up as a system bean to delete historical FixMessageEntry rows in the FixMessageEntry table that represent non-message entries
 * such as login events or administrative messages.
 *
 * @author manderson
 */
public class FixMessageEntryDeleteHistoryJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	/**
	 * The minimum number of days back the log entry date must be in order to be deleted
	 * entered as a positive number
	 */
	private short minimumDaysBack;

	/**
	 * FixLogEntryTypes to delete.  Cannot be a message
	 */
	private Short[] fixMessageEntryTypeIds;


	private StatusHolderObject<Status> statusHolderObject;


	private FixMessageEntryService fixMessageEntryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(getMinimumDaysBack() > 0, "Minimum Days Back must be entered as a positive number");
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(getFixMessageEntryTypeIds()), "Fix Message Entry Type selection(s) are required.");
		for (Short fixMessageEntryTypeId : getFixMessageEntryTypeIds()) {
			FixMessageEntryType fixMessageEntryType = getFixMessageEntryService().getFixMessageEntryType(fixMessageEntryTypeId);
			ValidationUtils.assertFalse(fixMessageEntryType.isMessage(), "Cannot delete historical message log events from FixMessageEntry table.");
		}
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolderObject.getStatus();
		int count = getFixMessageEntryService().deleteFixMessageEntryHistoricalList(DateUtils.addDays(DateUtils.clearTime(new Date()), -getMinimumDaysBack()), getFixMessageEntryTypeIds());
		status.setMessage("Deleted [" + count + "] historical entries from FixMessageEntry table.");
		status.setActionPerformed(count > 0);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public short getMinimumDaysBack() {
		return this.minimumDaysBack;
	}


	public void setMinimumDaysBack(short minimumDaysBack) {
		this.minimumDaysBack = minimumDaysBack;
	}


	public Short[] getFixMessageEntryTypeIds() {
		return this.fixMessageEntryTypeIds;
	}


	public void setFixMessageEntryTypeIds(Short[] fixMessageEntryTypeIds) {
		this.fixMessageEntryTypeIds = fixMessageEntryTypeIds;
	}


	public StatusHolderObject<Status> getStatusHolderObject() {
		return this.statusHolderObject;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}
}
