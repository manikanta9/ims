package com.clifton.fix.message.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.fix.message.FixMessageEntryType;
import org.springframework.stereotype.Component;


/**
 * This cache stores FixMessageTypes using the MessageTypeTagValue field as a key.
 *
 * @author davidi
 */
@Component
public class FixMessageEntryTypeByTagValueCache extends SelfRegisteringSingleKeyDaoCache<FixMessageEntryType, String> {

	@Override
	protected String getBeanKeyProperty() {
		return "messageTypeTagValue";
	}


	@Override
	protected String getBeanKeyValue(FixMessageEntryType bean) {
		return bean.getMessageTypeTagValue();
	}
}




