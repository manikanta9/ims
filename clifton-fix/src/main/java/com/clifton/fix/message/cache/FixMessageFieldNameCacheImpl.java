package com.clifton.fix.message.cache;

import com.clifton.core.cache.LazilyBuiltCache;
import com.clifton.core.cache.SimpleCacheHandler;
import com.clifton.fix.util.FixUtilService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * See META-INF/schema/quickfix/FIX44.xml for further information on the complete list of Fix message fields.
 */
@SuppressWarnings("rawtypes")
@Component
public class FixMessageFieldNameCacheImpl extends LazilyBuiltCache<Map<String, List<FixMessageFieldName>>> implements FixMessageFieldNameCache, InitializingBean {

	public static final String MIN_FIX_VERSION = "FIX.4.4";

	public static final int MIN_FIELD_TAG = 1;
	public static final int MAX_FIELD_TAG = 10626;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixUtilService fixUtilService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() throws Exception {
		setCacheHandler(new SimpleCacheHandler<>());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FixMessageFieldName> getFixMessageFieldNameMap(String fixVersion) {
		List<FixMessageFieldName> fieldNameCache = getOrBuildCache().get(fixVersion);

		if (fieldNameCache == null) {
			buildFieldNameCache(fixVersion);
			return getOrBuildCache().get(fixVersion);
		}

		return fieldNameCache;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private synchronized void buildFieldNameCache(final String fixVersion) {
		List<FixMessageFieldName> fieldNameCache = getOrBuildCache().get(fixVersion);

		if (fieldNameCache == null) {
			List<FixMessageFieldName> fieldNameList = Collections.synchronizedList(new ArrayList<>());

			for (int tag = MIN_FIELD_TAG; tag <= MAX_FIELD_TAG; tag++) {
				String fieldName = getFixUtilService().getFieldName(fixVersion, tag);
				if (fieldName != null) {
					fieldNameList.add(new FixMessageFieldName(Integer.toString(tag), fieldName));
				}
			}
			getOrBuildCache().put(fixVersion, fieldNameList);
		}
	}


	@Override
	protected Map<String, List<FixMessageFieldName>> buildCache() {
		return new ConcurrentHashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixUtilService getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService fixUtilService) {
		this.fixUtilService = fixUtilService;
	}
}
