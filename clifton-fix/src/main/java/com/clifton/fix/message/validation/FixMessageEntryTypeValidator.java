package com.clifton.fix.message.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;


/**
 * A validator for FixMessageType entities.  Prohibits adding new entries or removing existing entries,
 * as these changes would also require an implementation change to the code.
 *
 * @author davidi
 */
@Component
public class FixMessageEntryTypeValidator extends SelfRegisteringDaoValidator<FixMessageEntryType> {

	private SecurityAuthorizationService securityAuthorizationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(FixMessageEntryType bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isDelete()) {
			throw new ValidationException("Cannot " + (config.isInsert() ? "create" : "delete") + " Fix Message Entry Types.");
		}
		else if (config.isUpdate()) {
			FixMessageEntryType originalBean = getOriginalBean(bean);
			if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
				throw new ValidationException("Only Administrators can update a FixMessageEntryType.");
			}
			if (originalBean != null && !originalBean.getName().equals(bean.getName())) {
				throw new ValidationException("Cannot change the name of a FixMessageEntryType.");
			}
			if (bean.isMessage() && !bean.isDefaultType()) {
				ValidationUtils.assertNotNull(bean.getMessageTypeTagValue(), "A message requires a Message Type Tag Value.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
