package com.clifton.fix.message.store;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlUpdateCommand;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.message.store.search.FixMessageStoreSearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.util.FixUtilService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class FixMessageStoreServiceImpl implements FixMessageStoreService {

	private AdvancedUpdatableDAO<FixMessageStore, Criteria> fixMessageStoreDAO;

	private SqlHandler sqlHandler;

	private FixUtilService<FixMessage> fixUtilService;

	////////////////////////////////////////////////////////////////////////////
	////////           FixMessageStore Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixMessageStore getFixMessageStore(int id) {
		return getFixMessageStoreDAO().findByPrimaryKey(id);
	}


	@Override
	public List<FixMessageStore> getFixMessageStoreList(FixMessageStoreSearchForm searchForm) {
		return getFixMessageStoreDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public boolean saveFixMessageStore(FixMessageStore bean) {
		try {
			getFixMessageStoreDAO().save(bean);
		}
		catch (Exception e) {
			throw new ValidationException("Error " + (bean.isNewBean() ? "inserting" : "updating") + " FixMessageStore entry for Session " + bean.getSession().getDefinition().getName() + " and sequence # " + bean.getSequenceNumber() + ". Please confirm outgoing sequence numbers on the session and reset manually if necessary.", e);
			// Note: If this happens often enough it might be useful to raise an event and let the system auto manage this?
		}
		return true;
	}


	/**
	 * Using JDBC here to minimize the number of database calls.  Don't want to look up a list and then call delete on potentially thousands of rows.
	 */
	@Override
	@Transactional
	public void deleteFixMessageStoreForSession(FixSession session) {
		getSqlHandler().executeUpdate(SqlUpdateCommand.forSql(
				"DELETE FROM FixMessageStore WHERE FixSessionID = ?")
				.addShortParameterValue(session.getId())
		);
	}


	@Override
	public List<FixMessageField> getFixMessageStoreFieldList(Integer messageId) {
		FixMessageStore fixMessageStore = getFixMessageStore(messageId);
		return getFixUtilService().getFixMessageFieldList(fixMessageStore.getText());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixMessageStore, Criteria> getFixMessageStoreDAO() {
		return this.fixMessageStoreDAO;
	}


	public void setFixMessageStoreDAO(AdvancedUpdatableDAO<FixMessageStore, Criteria> fixMessageStoreDAO) {
		this.fixMessageStoreDAO = fixMessageStoreDAO;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public FixUtilService<FixMessage> getFixUtilService() {
		return this.fixUtilService;
	}


	public void setFixUtilService(FixUtilService<FixMessage> fixUtilService) {
		this.fixUtilService = fixUtilService;
	}
}
