package com.clifton.fix.message;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.session.FixSession;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class FixMessageServiceImpl implements FixMessageService, BaseFixMessageEntryService<FixMessage> {

	FixMessageEntryService fixMessageEntryService;

	////////////////////////////////////////////////////////////////////////////
	////////               FixMessage Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixMessage getFixMessage(long id) {
		return createEntity(getFixMessageEntryService().getFixMessageEntry(id));
	}


	@Override
	public List<FixMessageField> getFixMessageFieldList(long messageId) {
		return getFixMessageEntryService().getFixMessageEntryFieldList(messageId);
	}


	@Override
	public List<FixMessageField> getFixMessageFieldListNonPersistent(FixMessageEntrySearchForm searchForm) {
		return getFixMessageEntryService().getFixMessageEntryFieldListNonPersistent(searchForm);
	}


	@Override
	public FixMessage getFixMessageNonPersistent(FixMessageEntrySearchForm searchForm) {
		return createEntity(getFixMessageEntryService().getFixMessageEntryNonPersistent(searchForm));
	}


	@Override
	public List<FixMessage> getFixMessageList(FixMessageEntrySearchForm searchForm) {
		if (ArrayUtils.isEmpty(searchForm.getTypeIds()) && searchForm.getFixMessageEntryTypeIdsName() == null) {
			searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE);
		}
		else {
			searchForm.setMessage(true);
		}
		return createEntityList(getFixMessageEntryService().getFixMessageEntryList(searchForm));
	}


	@Override
	public List<FixMessage> getFixMessageFailedMessageList() {
		return createEntityList(getFixMessageEntryService().getFixMessageEntryFailedMessageList());
	}


	@Override
	public FixMessage getFixMessageFailedMessage(Long id) {
		return createEntity(getFixMessageEntryService().getFixMessageEntryFailedMessage(id));
	}


	@Override
	public List<FixMessageField> getFixMessageFailedFieldList(Long id) {
		return getFixMessageEntryService().getFixMessageEntryFailedFieldList(id);
	}


	@Override
	public void updateFixNonPersistentMessageCapacityForSession(FixSession fixSession, int capacity) {
		getFixMessageEntryService().updateFixNonPersistentMessageCapacityForSession(fixSession, capacity);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixMessage createEntity(FixMessageEntry entry) {
		if (entry != null && entry.getType().isMessage()) {
			return entry.toFixMessage();
		}
		return null;
	}


	@Override
	public List<FixMessage> createEntityList(List<FixMessageEntry> entryList) {
		return CollectionUtils.getStream(entryList)
				.filter(Objects::nonNull)
				.filter(entry -> entry.getType().isMessage())
				.map(FixMessageEntry::toFixMessage).collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryService getFixMessageEntryService() {
		return this.fixMessageEntryService;
	}


	public void setFixMessageEntryService(FixMessageEntryService fixMessageEntryService) {
		this.fixMessageEntryService = fixMessageEntryService;
	}
}
