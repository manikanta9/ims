package com.clifton.fix.message.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessageEntry;
import org.springframework.stereotype.Component;


/**
 * A validator for FixMessageEntry entities. These require additional fields defined that are
 * nullable in the schema due to the FixMessage and FixMessageEntry entities being mapped to the same table: FixMessageEntry.
 *
 * @author davidi
 */
@Component
public class FixMessageEntryValidator extends SelfRegisteringDaoValidator<FixMessageEntry> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(FixMessageEntry bean, DaoEventTypes config) throws ValidationException {

		if ((config.isInsert() || config.isUpdate()) && (bean.getType().isMessage())) {
			ValidationUtils.assertNotNull(bean.getIncoming(), "The incoming field of a fix message cannot be null.");

			// A processed message must have an identifier.
			if (BooleanUtils.isTrue(bean.getProcessed()) && !bean.getType().isAdministrative()) {
				ValidationUtils.assertNotNull(bean.getIdentifier(), "A successfully processed message must have an identifier value.");
			}
		}

		if (config.isDelete()) {
			ValidationUtils.assertFalse(bean.getType().isMessage(), "Entries of type FixMessage may not be deleted.");
		}
	}
}
