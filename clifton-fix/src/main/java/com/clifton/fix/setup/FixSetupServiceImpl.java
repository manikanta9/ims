package com.clifton.fix.setup;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.setup.search.FixSourceSystemSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class FixSetupServiceImpl implements FixSetupService {

	private AdvancedUpdatableDAO<FixSourceSystem, Criteria> fixSourceSystemDAO;

	private DaoNamedEntityCache<FixSourceSystem> fixSourceSystemCache;
	private DaoSingleKeyCache<FixSourceSystem, String> fixSourceSystemBySystemTagCache;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixSourceSystem getFixSourceSystem(short id) {
		return getFixSourceSystemDAO().findByPrimaryKey(id);
	}


	@Override
	public FixSourceSystem getFixSourceSystemByName(String name) {
		return getFixSourceSystemCache().getBeanForKeyValue(getFixSourceSystemDAO(), name);
	}


	@Override
	public FixSourceSystem getFixSourceSystemByTag(String sourceSystemTag) {
		if (StringUtils.isEmpty(sourceSystemTag)) {
			return null;
		}
		return getFixSourceSystemBySystemTagCache().getBeanForKeyValue(getFixSourceSystemDAO(), sourceSystemTag);
	}


	@Override
	public List<FixSourceSystem> getFixSourceSystemList(FixSourceSystemSearchForm searchForm) {
		return getFixSourceSystemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public FixSourceSystem saveFixSourceSystem(FixSourceSystem bean) {
		return getFixSourceSystemDAO().save(bean);
	}


	@Override
	public void deleteFixSourceSystem(short id) {
		getFixSourceSystemDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods                   ////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixSourceSystem, Criteria> getFixSourceSystemDAO() {
		return this.fixSourceSystemDAO;
	}


	public void setFixSourceSystemDAO(AdvancedUpdatableDAO<FixSourceSystem, Criteria> fixSourceSystemDAO) {
		this.fixSourceSystemDAO = fixSourceSystemDAO;
	}


	public DaoNamedEntityCache<FixSourceSystem> getFixSourceSystemCache() {
		return this.fixSourceSystemCache;
	}


	public void setFixSourceSystemCache(DaoNamedEntityCache<FixSourceSystem> fixSourceSystemCache) {
		this.fixSourceSystemCache = fixSourceSystemCache;
	}


	public DaoSingleKeyCache<FixSourceSystem, String> getFixSourceSystemBySystemTagCache() {
		return this.fixSourceSystemBySystemTagCache;
	}


	public void setFixSourceSystemBySystemTagCache(DaoSingleKeyCache<FixSourceSystem, String> fixSourceSystemBySystemTagCache) {
		this.fixSourceSystemBySystemTagCache = fixSourceSystemBySystemTagCache;
	}
}
