package com.clifton.fix.setup.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.fix.setup.FixSourceSystem;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class FixSourceSystemBySystemTagCache extends SelfRegisteringSingleKeyDaoCache<FixSourceSystem, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "systemTag";
	}


	@Override
	protected String getBeanKeyValue(FixSourceSystem bean) {
		return (bean != null ? bean.getSystemTag() : null);
	}
}
