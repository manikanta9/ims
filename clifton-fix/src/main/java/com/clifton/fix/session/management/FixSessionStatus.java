package com.clifton.fix.session.management;

import com.clifton.core.util.jmx.MBeanWrapper;

import java.util.List;


/**
 * The {@link FixSessionStatus} object is a DTO which provides information about the current state of the FIX system.
 *
 * @author MikeH
 */
public class FixSessionStatus {

	private final List<MBeanWrapper> sessionStatusList;
	private final List<MBeanWrapper> settingsStatusList;
	private final List<MBeanWrapper> connectorStatusList;
	private final List<FixSessionQueueStatus> sessionQueueStatusList;
	private final String sessionDispatcherThreadDump;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionStatus(List<MBeanWrapper> sessionStatusList, List<MBeanWrapper> settingsStatusList, List<MBeanWrapper> connectorStatusList, List<FixSessionQueueStatus> sessionQueueStatusList, String sessionDispatcherThreadDump) {
		this.connectorStatusList = connectorStatusList;
		this.sessionStatusList = sessionStatusList;
		this.settingsStatusList = settingsStatusList;
		this.sessionQueueStatusList = sessionQueueStatusList;
		this.sessionDispatcherThreadDump = sessionDispatcherThreadDump;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<MBeanWrapper> getSessionStatusList() {
		return this.sessionStatusList;
	}


	public List<MBeanWrapper> getSettingsStatusList() {
		return this.settingsStatusList;
	}


	public List<MBeanWrapper> getConnectorStatusList() {
		return this.connectorStatusList;
	}


	public List<FixSessionQueueStatus> getSessionQueueStatusList() {
		return this.sessionQueueStatusList;
	}


	public String getSessionDispatcherThreadDump() {
		return this.sessionDispatcherThreadDump;
	}
}
