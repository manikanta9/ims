package com.clifton.fix.session;

import com.clifton.core.util.functionalinterface.ShortFunction;

import java.util.List;
import java.util.function.Consumer;


/**
 * @author manderson
 */
public interface FixSessionSequenceNumberHandler {


	/**
	 * Stores the current sequence numbers in memory, will call the synchronize function if the FixSession needs to be saved.
	 * If not saving, will add a runner to trigger the save within 5 minutes, else will clear existing runner (if set) so it can be rescheduled on the next update
	 *
	 * @param fixSessionRetrievalFunction: retrieve the fix session rom the database by Id.  This is when scheduling the runner that we always get the most recent copy of the session before
	 * @param fixSessionSaveFunction:      save the fix session.  Either if batch size is hit, we call this directly, OR used by the runner to save.  Calling save will update the sequence numbers in the database from the {@link FixSessionObserver}
	 */
	public void storeFixSessionSequenceNumbers(FixSession fixSession, int incomingSequenceNumber, int outgoingSequenceNumber, ShortFunction<FixSession> fixSessionRetrievalFunction, Consumer<FixSession> fixSessionSaveFunction);


	/**
	 * Synchronizes the live incoming and outgoing sequences numbers on the object and cancels any runners if scheduled
	 * Called by the {@link FixSessionObserver} just prior to saving FixSessions
	 */
	public void synchronizeFixSessionSequenceNumbers(FixSession fixSession);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the "Live" versions of the sequence numbers for display in UI
	 */
	public void populateFixSessionLiveSequenceNumbers(FixSession fixSession);


	/**
	 * Sets the "Live" versions of the sequence numbers for display in UI
	 */
	public void populateFixSessionLiveSequenceNumbers(List<FixSession> fixSessionList);
}
