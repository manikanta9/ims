package com.clifton.fix.session;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import org.springframework.stereotype.Component;


/**
 * The <code>FixSessionObserver</code> observes updates to FixSessions and prior to saving, synchronizes the sequence numbers with the live memory values.
 *
 * @author manderson
 */
@Component
public class FixSessionObserver extends SelfRegisteringDaoObserver<FixSession> {


	private FixSessionSequenceNumberHandler fixSessionSequenceNumberHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<FixSession> dao, DaoEventTypes event, FixSession bean) {
		// Before any saves sync the live sequence numbers
		getFixSessionSequenceNumberHandler().synchronizeFixSessionSequenceNumbers(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionSequenceNumberHandler getFixSessionSequenceNumberHandler() {
		return this.fixSessionSequenceNumberHandler;
	}


	public void setFixSessionSequenceNumberHandler(FixSessionSequenceNumberHandler fixSessionSequenceNumberHandler) {
		this.fixSessionSequenceNumberHandler = fixSessionSequenceNumberHandler;
	}
}
