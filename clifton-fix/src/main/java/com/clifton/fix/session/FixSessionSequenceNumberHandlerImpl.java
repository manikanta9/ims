package com.clifton.fix.session;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.functionalinterface.ShortFunction;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;
import java.util.function.IntConsumer;


/**
 * @author manderson
 */
@Component
public class FixSessionSequenceNumberHandlerImpl implements FixSessionSequenceNumberHandler {

	private RunnerHandler runnerHandler;

	/**
	 * Number of seconds after the first time a sequence number is updated in the sequence number maps that isn't propagated to the database
	 * until it is saved.  i.e. The longest amount of time the database is delayed (5 minutes)
	 */
	private static final int FIX_SESSION_SYNC_SECONDS_DELAY = (60 * 5);

	private static final String FIX_SESSION_SYNC_RUNNER_TYPE = "FIX-SESSION-SEQUENCE-NUMBERS";


	/**
	 * Internal caches for the sequence numbers so we know what number we are on without actually having to save until
	 * we hit the batch size since the last save
	 */
	private final ConcurrentMap<Short, FixSessionSequenceNumberHolder> fixSessionSequenceNumberMap = new ConcurrentHashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public synchronized void storeFixSessionSequenceNumbers(FixSession fixSession, int incomingSequenceNumber, int outgoingSequenceNumber, ShortFunction<FixSession> fixSessionRetrievalFunction, Consumer<FixSession> fixSessionSaveFunction) {
		this.fixSessionSequenceNumberMap.compute(fixSession.getId(), (key, currentValue) -> {
			if (currentValue == null) {
				return new FixSessionSequenceNumberHolder(incomingSequenceNumber, outgoingSequenceNumber);
			}
			currentValue.setIncomingSequenceNumber(incomingSequenceNumber);
			currentValue.setOutgoingSequenceNumber(outgoingSequenceNumber);
			return currentValue;
		});

		if (isSaveSequenceNumbers(fixSession, incomingSequenceNumber, outgoingSequenceNumber)) {
			fixSessionSaveFunction.accept(fixSession);
		}
		else {
			scheduleFixSessionSequenceNumberRunner(fixSession.getId(), fixSessionRetrievalFunction, fixSessionSaveFunction);
		}
	}


	@Override
	public synchronized void synchronizeFixSessionSequenceNumbers(FixSession fixSession) {
		populateFixSessionLiveSequenceNumbersImpl(fixSession, fixSession::setIncomingSequenceNumber, fixSession::setOutgoingSequenceNumber);
		cancelFixSessionSequenceNumberRunner(fixSession);
	}


	private boolean isSaveSequenceNumbers(FixSession fixSession, int incomingSequenceNumber, int outgoingSequenceNumber) {
		Integer batchSize = fixSession.getDefinition().getCoalesceSaveSequenceNumberBatchSize();
		if (batchSize == null || batchSize <= 1) {
			return true;
		}
		// If incoming or outgoing is greater than what is on the store - save it - handles session resets
		if (fixSession.getIncomingSequenceNumber() > incomingSequenceNumber || fixSession.getOutgoingSequenceNumber() > outgoingSequenceNumber) {
			return true;
		}
		int difference = incomingSequenceNumber - fixSession.getIncomingSequenceNumber();
		if (difference >= batchSize) {
			return true;
		}
		difference = outgoingSequenceNumber - fixSession.getOutgoingSequenceNumber();
		return (difference >= batchSize);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void scheduleFixSessionSequenceNumberRunner(short fixSessionId, ShortFunction<FixSession> fixSessionRetrievalFunction, Consumer<FixSession> fixSessionSaveFunction) {
		String runId = fixSessionId + "";
		if (CollectionUtils.isEmpty(getRunnerHandler().getScheduledRunnerList(FIX_SESSION_SYNC_RUNNER_TYPE, runId))) {
			final Date scheduledDate = DateUtils.addSeconds(new Date(), FIX_SESSION_SYNC_SECONDS_DELAY);

			Runner runner = new AbstractStatusAwareRunner(FIX_SESSION_SYNC_RUNNER_TYPE, runId, scheduledDate) {

				@Override
				public void run() {
					FixSession fixSession = fixSessionRetrievalFunction.apply(fixSessionId);
					fixSessionSaveFunction.accept(fixSession);
					getStatus().setMessage("Fix Session Sequence Numbers Synchronized to the Database.");
				}
			};

			getRunnerHandler().rescheduleRunner(runner);
		}
	}


	private void cancelFixSessionSequenceNumberRunner(FixSession fixSession) {
		getRunnerHandler().cancelRunners(FIX_SESSION_SYNC_RUNNER_TYPE, fixSession.getId() + "");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void populateFixSessionLiveSequenceNumbers(FixSession fixSession) {
		populateFixSessionLiveSequenceNumbersImpl(fixSession, fixSession::setLiveIncomingSequenceNumber, fixSession::setLiveOutgoingSequenceNumber);
	}


	private void populateFixSessionLiveSequenceNumbersImpl(FixSession fixSession, IntConsumer incomingConsumer, IntConsumer outgoingConsumer) {
		if (this.fixSessionSequenceNumberMap.containsKey(fixSession.getId())) {
			incomingConsumer.accept(ObjectUtils.coalesce(this.fixSessionSequenceNumberMap.get(fixSession.getId()).getIncomingSequenceNumber(), fixSession.getIncomingSequenceNumber()));
			outgoingConsumer.accept(ObjectUtils.coalesce(this.fixSessionSequenceNumberMap.get(fixSession.getId()).getOutgoingSequenceNumber(), fixSession.getOutgoingSequenceNumber()));
		}
	}


	@Override
	public void populateFixSessionLiveSequenceNumbers(List<FixSession> fixSessionList) {
		CollectionUtils.getStream(fixSessionList).forEach(this::populateFixSessionLiveSequenceNumbers);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class FixSessionSequenceNumberHolder {

		private int incomingSequenceNumber;
		private int outgoingSequenceNumber;


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		FixSessionSequenceNumberHolder(int incomingSequenceNumber, int outgoingSequenceNumber) {
			this.incomingSequenceNumber = incomingSequenceNumber;
			this.outgoingSequenceNumber = outgoingSequenceNumber;
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public void applySequenceNumberUpdates(int incomingSequenceNumber, int outgoingSequenceNumber) {
			if (incomingSequenceNumber > getIncomingSequenceNumber()) {
				setIncomingSequenceNumber(incomingSequenceNumber);
			}
			if (outgoingSequenceNumber > getOutgoingSequenceNumber()) {
				setOutgoingSequenceNumber(outgoingSequenceNumber);
			}
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public int getIncomingSequenceNumber() {
			return this.incomingSequenceNumber;
		}


		public void setIncomingSequenceNumber(int incomingSequenceNumber) {
			this.incomingSequenceNumber = incomingSequenceNumber;
		}


		public int getOutgoingSequenceNumber() {
			return this.outgoingSequenceNumber;
		}


		public void setOutgoingSequenceNumber(int outgoingSequenceNumber) {
			this.outgoingSequenceNumber = outgoingSequenceNumber;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
