package com.clifton.fix.session.management;

import com.clifton.fix.session.FixSession;


/**
 * The {@link FixSessionQueueStatus} object is a DTO which provides information about the current queue status for a single {@link FixSession}.
 * <p>
 * FIX engine libraries typically use queues for message ingestion in order to improve throughput and prevent blocking operations. This DTO provides information about the queue
 * for a single session to be used during diagnostics.
 *
 * @author MikeH
 */
public class FixSessionQueueStatus {

	private final String sessionId;
	private final int queueSize;
	private final int queueCapacity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionQueueStatus(String sessionId, int queueSize, int queueCapacity) {
		this.sessionId = sessionId;
		this.queueSize = queueSize;
		this.queueCapacity = queueCapacity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSessionId() {
		return this.sessionId;
	}


	public int getQueueSize() {
		return this.queueSize;
	}


	public int getQueueCapacity() {
		return this.queueCapacity;
	}
}
