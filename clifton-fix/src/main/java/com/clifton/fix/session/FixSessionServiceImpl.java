package com.clifton.fix.session;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.session.management.FixSessionManagementService;
import com.clifton.fix.session.management.FixSessionRunStatus;
import com.clifton.fix.session.search.FixSessionDefinitionSearchForm;
import com.clifton.fix.session.search.FixSessionSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
public class FixSessionServiceImpl implements FixSessionService {

	private AdvancedUpdatableDAO<FixSession, Criteria> fixSessionDAO;
	private AdvancedUpdatableDAO<FixSessionDefinition, Criteria> fixSessionDefinitionDAO;
	private AdvancedUpdatableDAO<FixSessionDefinitionSetting, Criteria> fixSessionDefinitionSettingDAO;

	private DaoSingleKeyListCache<FixSessionDefinitionSetting, Short> fixSessionDefinitionSettingListBySessionDefinitionCache;

	private FixSessionManagementService fixSessionManagementService;
	private FixSessionSequenceNumberHandler fixSessionSequenceNumberHandler;

	private DaoCompositeKeyListCache<FixSessionDefinition, Boolean, Boolean> fixSessionDefinitionListByDisabledCreateSessionStatesCache;
	private DaoCompositeKeyCache<FixSession, Short, Boolean> fixSessionBySessionDefinitionActiveCache;
	private DaoSingleKeyListCache<FixSessionDefinition, String> fixSessionDefinitionByTargetCompIdCache;
	////////////////////////////////////////////////////////////////////////////
	////////              FixSession Business Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixSession getFixSession(short id) {
		return getFixSessionDAO().findByPrimaryKey(id);
	}


	@Override
	public FixSession getFixSessionPopulated(short id) {
		FixSession fixSession = getFixSession(id);
		if (fixSession != null) {
			getFixSessionSequenceNumberHandler().populateFixSessionLiveSequenceNumbers(fixSession);
		}
		return fixSession;
	}



	@Override
	public List<FixSession> getFixSessionList(FixSessionSearchForm searchForm, boolean populateLiveSequenceNumbers, boolean populateRunStatus) {
		List<FixSession> sessionList = getFixSessionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
		if (populateLiveSequenceNumbers) {
			getFixSessionSequenceNumberHandler().populateFixSessionLiveSequenceNumbers(sessionList);
		}
		if (populateRunStatus) {
			for (FixSession session : sessionList) {
				try {
					FixSessionRunStatus sessionRunStatus = getFixSessionManagementService().getFixSessionRunStatus(session);
					session.setRunning(sessionRunStatus.isRunning());
					session.setLoggedOn(sessionRunStatus.isLoggedOn());
				}
				catch (Exception e) {
					LogUtils.warn(getClass(), String.format("An exception occurred while obtaining session runtime information for session ID [%d].", session.getId()));
				}
			}
		}
		return sessionList;
	}


	@Override
	public FixSession getFixSessionCurrentByDefinition(FixSessionDefinition definition) {
		return getFixSessionBySessionDefinitionActiveCache().getBeanForKeyValues(getFixSessionDAO(), definition.getId(), true);
	}


	@Override
	public FixSession saveFixSession(FixSession bean) {
		if ((bean != null) && bean.isActiveSession()) {
			FixSession activeSession = getFixSessionCurrentByDefinition(bean.getDefinition());
			if (!bean.equals(activeSession)) {
				ValidationUtils.assertNull(activeSession, "Only one session can be active at a time.  The FixSession with id [" + (activeSession != null ? activeSession.getId() : -10)
						+ "] must be deactivated before this session can be activated.");
			}
		}
		return saveFixSessionInternal(bean);
	}

	////////////////////////////////////////////////////////////////////////////
	////////        FixSession Sequence Number Business Methods      ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void storeFixSessionSequenceNumbers(FixSession fixSession, int incomingSequenceNumber, int outgoingSequenceNumber) {
		getFixSessionSequenceNumberHandler().storeFixSessionSequenceNumbers(fixSession, incomingSequenceNumber, outgoingSequenceNumber, this::getFixSession, this::saveFixSessionInternal);
	}


	/**
	 * Forces save of the sequence numbers in the database to match the live versions.
	 * Called on application shutdown
	 */
	@Override
	@PreDestroy
	public void saveFixSessionSequenceNumbers() {
		// Trigger a Save on the FixSession if mismatched with live data
		for (FixSession fixSession : getFixSessionList(new FixSessionSearchForm(), true, false)) {
			saveFixSessionSequenceNumbersForSessionImpl(fixSession);
		}
	}


	@Override
	public void saveFixSessionSequenceNumbersForSession(short fixSessionId) {
		saveFixSessionSequenceNumbersForSessionImpl(getFixSessionPopulated(fixSessionId));
	}


	private void saveFixSessionSequenceNumbersForSessionImpl(FixSession fixSession) {
		if (fixSession != null) {
			boolean save = false;
			if (fixSession.getLiveIncomingSequenceNumber() != null && fixSession.getLiveIncomingSequenceNumber() != fixSession.getIncomingSequenceNumber()) {
				save = true;
			}
			if (fixSession.getLiveOutgoingSequenceNumber() != null && fixSession.getLiveOutgoingSequenceNumber() != fixSession.getOutgoingSequenceNumber()) {
				save = true;
			}
			if (save) {
				saveFixSessionInternal(fixSession);
			}
		}
	}


	@Override
	public FixSession saveFixSessionInternal(FixSession bean) {
		return getFixSessionDAO().save(bean);
	}


	@Override
	public void deleteFixSession(short id) {
		getFixSessionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////          FixSessionDefinition Business Methods          ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixSessionDefinition getFixSessionDefinition(short id) {
		FixSessionDefinition result = getFixSessionDefinitionDAO().findByPrimaryKey(id);
		if (result != null) {
			result.setSettingList(getFixSessionDefinitionSettingBySession(result.getId(), false));
		}

		return result;
	}


	@Override
	public FixSessionDefinition getFixSessionDefinitionByLabel(String labelName) {
		return getFixSessionDefinitionDAO().findOneByField("label", labelName);
	}


	@Override
	public FixSessionDefinition getFixSessionDefinitionByParameters(FixSessionParameters sessionParameters) {
		if (sessionParameters == null) {
			return null;
		}
		// if it's an incoming FixEntity then the session parameters will be reversed.
		Boolean reverseSessionId = (Boolean) BeanUtils.getPropertyValue(sessionParameters, "incoming", true);
		if (reverseSessionId == null) {
			reverseSessionId = false;
		}

		 //If going out from Parametric FAN OUT -
		if (!reverseSessionId) {
			return getFixSessionDefinition(sessionParameters.getFixBeginString(), sessionParameters.getFixSessionQualifier(), sessionParameters.getFixSenderCompId(), sessionParameters.getFixSenderSubId(), sessionParameters.getFixTargetCompId(), sessionParameters.getFixTargetSubId());
		}
		// Else Coming into PARAMETRIC from EXCHNG/VENDOR
		else {
			return getFixSessionDefinition(sessionParameters.getFixBeginString(), sessionParameters.getFixSessionQualifier(), sessionParameters.getFixTargetCompId(), sessionParameters.getFixTargetSubId(), sessionParameters.getFixSenderCompId(), sessionParameters.getFixSenderSubId());
		}
	}


	private FixSessionDefinition getFixSessionDefinitionByRequiredParameters(String beginString, String sessionQualifier, String senderComp, String targetCompId) {
		List<FixSessionDefinition> fixSessionDefinitions = getFixSessionDefinitionListByTargetCompId(targetCompId);
		fixSessionDefinitions = CollectionUtils.getFiltered(fixSessionDefinitions, fixSession ->
				(fixSession != null && StringUtils.isEqual(fixSession.getBeginString(), beginString))
						&& (StringUtils.isEqual(fixSession.getSessionQualifier(), sessionQualifier))
						&& (StringUtils.isEqual(fixSession.getFixSenderCompId(), senderComp))
						&& (StringUtils.isEqual(fixSession.getFixTargetCompId(), targetCompId))

		);

		if (!fixSessionDefinitions.isEmpty()) {
			return CollectionUtils.getOnlyElement(fixSessionDefinitions);
		}
		return null;
	}


	private FixSessionDefinition getFixSessionDefinition(String beginString, String sessionQualifier, String senderComp, String senderSub, String targetCompId, String targetSub) {
		List<FixSessionDefinition> fixSessionDefinitions = getFixSessionDefinitionListByTargetCompId(targetCompId);
		fixSessionDefinitions = CollectionUtils.getFiltered(fixSessionDefinitions, fixSession ->
				(fixSession != null && StringUtils.isEqual(fixSession.getBeginString(), beginString))
						&& (StringUtils.isEqual(fixSession.getSessionQualifier(), sessionQualifier))
						&& (StringUtils.isEqual(fixSession.getFixSenderCompId(), senderComp))
						&& (StringUtils.isEqual(fixSession.getFixSenderSubId(), senderSub))
						&& (StringUtils.isEqual(fixSession.getFixTargetCompId(), targetCompId))

		);

		if (!fixSessionDefinitions.isEmpty()) {
			return CollectionUtils.getOnlyElement(fixSessionDefinitions);
		}

		// if no session was found, use only the required parameters.
		return getFixSessionDefinitionByRequiredParameters(beginString, sessionQualifier, senderComp, targetCompId);
	}


	@Override
	public List<FixSessionDefinition> getFixSessionDefinitionListByTargetCompId(String targetCompId) {
		return getFixSessionDefinitionByTargetCompIdCache().getBeanListForKeyValue(getFixSessionDefinitionDAO(), targetCompId);
	}


	@Override
	public List<FixSessionDefinition> getFixSessionDefinitionList(FixSessionDefinitionSearchForm searchForm) {
		// Default Sorting if none defined
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("nameExpanded");
		}
		return getFixSessionDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<FixSessionDefinition> getFixSessionDefinitionListActiveAndPopulated() {
		List<FixSessionDefinition> result = getFixSessionDefinitionListByDisabledCreateSessionStatesCache().getBeanListForKeyValues(getFixSessionDefinitionDAO(), false, true);
		for (FixSessionDefinition fixSessionDefinition : CollectionUtils.getIterable(result)) {
			fixSessionDefinition.setSettingList(getFixSessionDefinitionSettingBySession(fixSessionDefinition.getId(), false));
			if (fixSessionDefinition.getParent() != null) {
				fixSessionDefinition.getParent().setSettingList(getFixSessionDefinitionSettingBySession(fixSessionDefinition.getParent().getId(), false));
			}
		}
		return result;
	}


	@Override
	public FixSessionDefinition saveFixSessionDefinition(FixSessionDefinition bean) {
		if (bean.getParent() != null) {
			ValidationUtils.assertNull(bean.getParent().getParent(), "Only one parent level is allowed.");
		}
		validateWeekdayPeriod(bean);
		// If it's a real session where we establish a connection, require connectivity type.  Although used for informational purposes only, will keep all session definitions accurately updated
		if (!bean.isDisabled() && bean.isCreateSession()) {
			ValidationUtils.assertNotNull(bean.getConnectivityType(), "Active Session Definitions that will create Sessions are required to supply a connectivity type.  This is for informational purposes only.");
		}

		bean = getFixSessionDefinitionDAO().save(bean);
		return bean;
	}


	@Override
	public void deleteFixSessionDefinition(short id) {
		getFixSessionDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////           FixSessionSetting Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixSessionDefinitionSetting getFixSessionDefinitionSetting(int id) {
		return getFixSessionDefinitionSettingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<FixSessionDefinitionSetting> getFixSessionDefinitionSettingBySession(short sessionId, boolean includeParent) {
		List<FixSessionDefinitionSetting> list = getFixSessionDefinitionSettingListBySessionDefinitionCache().getBeanListForKeyValue(getFixSessionDefinitionSettingDAO(), sessionId);
		if (includeParent) {
			FixSessionDefinition sessionDefinition = getFixSessionDefinition(sessionId);
			if (sessionDefinition.getParent() != null) {
				List<FixSessionDefinitionSetting> parentList = getFixSessionDefinitionSettingListBySessionDefinitionCache().getBeanListForKeyValue(getFixSessionDefinitionSettingDAO(), sessionDefinition.getParent().getId());
				if (CollectionUtils.isEmpty(list)) {
					return parentList;
				}
				// include parent only when not set on the child
				list.addAll(parentList.stream()
						.filter(parentSetting -> list.stream().noneMatch(sessionSetting -> StringUtils.isEqual(sessionSetting.getSettingName(), parentSetting.getSettingName())))
						.collect(Collectors.toList()));
			}
		}
		return list;
	}


	@Override
	public FixSessionDefinitionSetting saveFixSessionDefinitionSetting(FixSessionDefinitionSetting bean) {
		return getFixSessionDefinitionSettingDAO().save(bean);
	}


	@Override
	public void deleteFixSessionDefinitionSetting(int id) {
		getFixSessionDefinitionSettingDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////                         Health Check Methods                      /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFixSessionServiceAlive() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateWeekdayPeriod(FixSessionDefinition bean) {
		ValidationUtils.assertTrue((bean.getStartWeekday() != null && bean.getEndWeekday() != null) || Objects.equals(bean.getStartWeekday(), bean.getEndWeekday()),
				"Day of week cannot be empty.", (bean.getStartWeekday() == null ? "Starting " : "Ending ") + "Weekday");
		if (bean.getStartWeekday() != null && bean.getEndWeekday() != null) {
			if (!ChronoField.DAY_OF_WEEK.range().isValidValue(bean.getStartWeekday()) || !ChronoField.DAY_OF_WEEK.range().isValidValue(bean.getEndWeekday())) {
				throw new FieldValidationException("Day of week must be in the range [1-7].", "Starting Weekday");
			}
			if (bean.getEndWeekday() < bean.getStartWeekday()) {
				throw new FieldValidationException("Ending Weekday must be before Starting Weekday", "Ending Weekday");
			}
		}
		if (bean.isWeeklySession()) {
			ValidationUtils.assertTrue(bean.getStartWeekday() != null && bean.getEndWeekday() != null && bean.getStartTime() != null && bean.getEndTime() != null,
					"Weekly Sessions require that a starting weekday and an ending weekday be specified as well as the session start time and end time.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<FixSession, Criteria> getFixSessionDAO() {
		return this.fixSessionDAO;
	}


	public void setFixSessionDAO(AdvancedUpdatableDAO<FixSession, Criteria> fixSessionDAO) {
		this.fixSessionDAO = fixSessionDAO;
	}


	public AdvancedUpdatableDAO<FixSessionDefinition, Criteria> getFixSessionDefinitionDAO() {
		return this.fixSessionDefinitionDAO;
	}


	public void setFixSessionDefinitionDAO(AdvancedUpdatableDAO<FixSessionDefinition, Criteria> fixSessionDefinitionDAO) {
		this.fixSessionDefinitionDAO = fixSessionDefinitionDAO;
	}


	public AdvancedUpdatableDAO<FixSessionDefinitionSetting, Criteria> getFixSessionDefinitionSettingDAO() {
		return this.fixSessionDefinitionSettingDAO;
	}


	public void setFixSessionDefinitionSettingDAO(AdvancedUpdatableDAO<FixSessionDefinitionSetting, Criteria> fixSessionDefinitionSettingDAO) {
		this.fixSessionDefinitionSettingDAO = fixSessionDefinitionSettingDAO;
	}


	public DaoSingleKeyListCache<FixSessionDefinitionSetting, Short> getFixSessionDefinitionSettingListBySessionDefinitionCache() {
		return this.fixSessionDefinitionSettingListBySessionDefinitionCache;
	}


	public void setFixSessionDefinitionSettingListBySessionDefinitionCache(DaoSingleKeyListCache<FixSessionDefinitionSetting, Short> fixSessionDefinitionSettingListBySessionDefinitionCache) {
		this.fixSessionDefinitionSettingListBySessionDefinitionCache = fixSessionDefinitionSettingListBySessionDefinitionCache;
	}


	public FixSessionManagementService getFixSessionManagementService() {
		return this.fixSessionManagementService;
	}


	public void setFixSessionManagementService(FixSessionManagementService fixSessionManagementService) {
		this.fixSessionManagementService = fixSessionManagementService;
	}


	public FixSessionSequenceNumberHandler getFixSessionSequenceNumberHandler() {
		return this.fixSessionSequenceNumberHandler;
	}


	public void setFixSessionSequenceNumberHandler(FixSessionSequenceNumberHandler fixSessionSequenceNumberHandler) {
		this.fixSessionSequenceNumberHandler = fixSessionSequenceNumberHandler;
	}


	public DaoCompositeKeyListCache<FixSessionDefinition, Boolean, Boolean> getFixSessionDefinitionListByDisabledCreateSessionStatesCache() {
		return this.fixSessionDefinitionListByDisabledCreateSessionStatesCache;
	}


	public void setFixSessionDefinitionListByDisabledCreateSessionStatesCache(DaoCompositeKeyListCache<FixSessionDefinition, Boolean, Boolean> fixSessionDefinitionListByDisabledCreateSessionStatesCache) {
		this.fixSessionDefinitionListByDisabledCreateSessionStatesCache = fixSessionDefinitionListByDisabledCreateSessionStatesCache;
	}


	public DaoCompositeKeyCache<FixSession, Short, Boolean> getFixSessionBySessionDefinitionActiveCache() {
		return this.fixSessionBySessionDefinitionActiveCache;
	}


	public void setFixSessionBySessionDefinitionActiveCache(DaoCompositeKeyCache<FixSession, Short, Boolean> fixSessionBySessionDefinitionActiveCache) {
		this.fixSessionBySessionDefinitionActiveCache = fixSessionBySessionDefinitionActiveCache;
	}



	public void setFixSessionDefinitionByTargetCompIdCache(DaoSingleKeyListCache<FixSessionDefinition, String> fixSessionDefinitionByTargetCompIdCache) {
		this.fixSessionDefinitionByTargetCompIdCache = fixSessionDefinitionByTargetCompIdCache;
	}


	public DaoSingleKeyListCache<FixSessionDefinition, String> getFixSessionDefinitionByTargetCompIdCache() {
		return this.fixSessionDefinitionByTargetCompIdCache;
	}
}
