package com.clifton.fix.session.management;

import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.jmx.MBeanWrapper;
import com.clifton.fix.session.FixSession;


/**
 * The {@link FixSessionRunStatus} object is a DTO which provides information about the current running state of a single {@link FixSession}.
 *
 * @author MikeH
 */
public class FixSessionRunStatus {

	/**
	 * {@code true} if the session is <i>running</i>. A session is considered running if the backing FIX engine is aware of the session and its associated schedule-based triggers
	 * or other listeners are currently active.
	 */
	private final boolean running;
	/**
	 * {@code true} if the session is <i>logged on</i>. For a session to be logged on, a connection with the counterparty must be established and the authentication process must be
	 * successfully completed. In addition, the session must not be inactive for longer than the allowed time.
	 */
	private final boolean loggedOn;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionRunStatus(MBeanWrapper sessionMBeanWrapper) {
		this.running = sessionMBeanWrapper != null;
		this.loggedOn = ObjectUtils.map(sessionMBeanWrapper, MBeanWrapper::getAttributeMap)
				.thenMap(attributeMap -> attributeMap.get("LoggedOn"))
				.thenMap(MBeanWrapper.AttributeWrapper::getValue)
				.thenMap(BooleanUtils::isTrue)
				.getOrElse(false);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRunning() {
		return this.running;
	}


	public boolean isLoggedOn() {
		return this.loggedOn;
	}
}
