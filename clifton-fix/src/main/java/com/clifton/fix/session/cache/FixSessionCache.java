package com.clifton.fix.session.cache;


import com.clifton.fix.session.FixSession;


/**
 * The <code>FixSessionCacheImpl</code> cache to handle storing FixSessions based a key value.
 * <p/>
 * For example, in the QuickFix implementation there is a SessionID that is always available, so
 * the implementation uses this cache to correlate the QuickFIX SessionID to a FixSession object.
 *
 * @param <T>
 * @author mwacker
 */
public interface FixSessionCache<T> {

	public final static String FIX_SESSION_CACHE_NAME = "FIX_SESSION_CACHE";


	public FixSession get(T sessionIdentifier);


	public void put(T sessionIdentifier, FixSession session);


	public void remove(T sessionIdentifier);


	public void clear();
}
