package com.clifton.fix.session.cache;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionParametersImpl;
import com.clifton.fix.session.FixSessionService;
import org.springframework.stereotype.Component;


/**
 * The <code>FixSessionCacheImpl</code> cache to handle storing FixSessions based a key value.
 * <p/>
 * For example, in the QuickFix implementation there is a SessionID that is always available, so
 * the implementation uses this cache to correlate the QuickFIX SessionID to a FixSession object.
 *
 * @param <T>
 * @author mwacker
 */
@Component
public class FixSessionCacheImpl<T> implements FixSessionCache<T>, CustomCache<T, Short> {

	private FixSessionService fixSessionService;
	/**
	 * Cache handler to store FixSession objects by SessionID.  Cached by FixSession.ID
	 * because the FixSession table is cached by hibernate.
	 */
	private CacheHandler<T, Short> cacheHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return FIX_SESSION_CACHE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Only caching the ID of the FIX session.  The actual FixSession object's are in the hibernate cache, this is
	 * simply a cache to look up the FixSessionID based on the provided session identifier.
	 */
	@Override
	public FixSession get(T sessionIdentifier) {
		Short id = getCacheHandler().get(getCacheName(), sessionIdentifier);
		FixSession session = null;
		if (id != null) {
			session = getFixSessionService().getFixSession(id);
		}
		if (session == null) {
			FixSessionParametersImpl parameters = new FixSessionParametersImpl(sessionIdentifier);
			FixSessionDefinition def = getFixSessionService().getFixSessionDefinitionByParameters(parameters);
			ValidationUtils.assertNotNull(def, "Cannot find FIX session definition for [" + parameters + "].");
			session = getFixSessionService().getFixSessionCurrentByDefinition(def);
			ValidationUtils.assertNotNull(session, "No active FIX session for definition [" + def.getId() + "] with session parameters [" + parameters + "].");
			put(sessionIdentifier, session);
		}
		return session;
	}


	@Override
	public void put(T sessionIdentifier, FixSession session) {
		getCacheHandler().put(getCacheName(), sessionIdentifier, session.getId());
	}


	@Override
	public void remove(T sessionIdentifier) {
		getCacheHandler().remove(getCacheName(), sessionIdentifier);
	}


	@Override
	public void clear() {
		getCacheHandler().clear(getCacheName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<T, Short> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<T, Short> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}
}
