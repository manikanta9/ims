package com.clifton.fix.session.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.fix.session.FixSessionDefinition;
import org.springframework.stereotype.Component;


/**
 * A cache implementation that is designed to cache a FixSessionDefinitionList the boolean composite keys: disabled and createSession.
 *
 * @author davidi
 */
@Component
public class FixSessionDefinitionListByDisabledCreateSessionStatesCache extends SelfRegisteringCompositeKeyDaoListCache<FixSessionDefinition, Boolean, Boolean> {


	@Override
	protected String getBeanKey1Property() {
		return "disabled";
	}


	@Override
	protected String getBeanKey2Property() {
		return "createSession";
	}


	@Override
	protected Boolean getBeanKey1Value(FixSessionDefinition bean) {
		return bean.isDisabled();
	}


	@Override
	protected Boolean getBeanKey2Value(FixSessionDefinition bean) {
		return bean.isCreateSession();
	}
}
