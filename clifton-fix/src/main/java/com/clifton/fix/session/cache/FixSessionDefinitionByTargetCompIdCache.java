package com.clifton.fix.session.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.fix.session.FixSessionDefinition;
import org.springframework.stereotype.Component;


/**
 * @author DipinC
 * <p>
 * cache implementation that is designed to cache a FixSessionDefinition with targetCompId lookup
 */
@Component
public class FixSessionDefinitionByTargetCompIdCache extends SelfRegisteringSingleKeyDaoListCache<FixSessionDefinition, String> {

	@Override
	protected String getBeanKeyProperty() {
		return "targetCompId";
	}


	@Override
	protected String getBeanKeyValue(FixSessionDefinition bean) {
		return bean.getTargetCompId();
	}
}
