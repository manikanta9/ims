package com.clifton.fix.session.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.fix.session.FixSessionDefinitionSetting;
import org.springframework.stereotype.Component;


/**
 * The <code>FixSessionDefinitionSettingListBySessionDefinitionCache</code> caches the list of session definition settings for a session definition
 *
 * @author manderson
 */
@Component
public class FixSessionDefinitionSettingListBySessionDefinitionCache extends SelfRegisteringSingleKeyDaoListCache<FixSessionDefinitionSetting, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "sessionDefinition.id";
	}


	@Override
	protected Short getBeanKeyValue(FixSessionDefinitionSetting bean) {
		if (bean.getSessionDefinition() != null) {
			return bean.getSessionDefinition().getId();
		}
		return null;
	}
}
