package com.clifton.fix.session.management;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.session.FixSession;

import java.lang.management.ThreadInfo;
import java.util.List;


/**
 * The {@link FixSessionManagementService} provides methods for monitoring the state of the FIX system.
 *
 * @author MikeH
 */
public interface FixSessionManagementService {

	/**
	 * Gets the system-wide {@link FixSessionStatus} object, which describes the current state of the FIX system.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public FixSessionStatus getFixSessionStatus();


	/**
	 * Gets the {@link FixSessionRunStatus} for the given {@link FixSession}.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public FixSessionRunStatus getFixSessionRunStatus(FixSession fixSession);


	/**
	 * Gets the full list of {@link ThreadInfo} objects for currently active FIX session dispatcher threads.
	 */
	@DoNotAddRequestMapping
	public List<ThreadInfo> getFixSessionDispatcherThreadInfoList();
}
