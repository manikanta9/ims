package com.clifton.fix.session.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.fix.session.FixSession;
import org.springframework.stereotype.Component;


/**
 * A cache implementation that is designed to cache a FixSession based in its FixSessionDefinitionId and ActiveSession state (true/false).
 *
 * @author davidi
 */
@Component
public class FixSessionBySessionDefinitionActiveCache extends SelfRegisteringCompositeKeyDaoCache<FixSession, Short, Boolean> {


	@Override
	protected String getBeanKey1Property() {
		return "definition.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "activeSession";
	}


	@Override
	protected Short getBeanKey1Value(FixSession bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getDefinition());
		}
		return null;
	}


	@Override
	protected Boolean getBeanKey2Value(FixSession bean) {
		return bean.isActiveSession();
	}
}
