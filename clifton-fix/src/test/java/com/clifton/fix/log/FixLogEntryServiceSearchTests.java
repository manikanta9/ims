package com.clifton.fix.log;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * FixLogEntry (the subset FixMessageEntries that are administrative messages or non-messages) search tests via the FixLogEntryService.
 *
 * @author davidi
 */
@FixInMemoryDatabaseContext
public class FixLogEntryServiceSearchTests extends BaseInMemoryDatabaseTests {

	@Resource
	FixLogEntryService fixLogEntryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFixMessageEntryServiceAvailable() {
		Assertions.assertNotNull(this.fixLogEntryService);
	}


	@Test
	public void testSearchBy_EntryID() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(251087);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setId((long) 251087);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_IsMessage_False() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(251095, 251087, 251078, 251040, 248467);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_IsMessage_True() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(251081, 251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		// note:  will be overridden by service to be MESSAGE_AND_ADMINISTRATIVE
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Administrative_False() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		// note:  the FixLogEntry service will override this to NOT_MESSAGE_AND_NOT_ADMINISTRATIVE
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_ADMINISTRATIVE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchByFixLogEntryType_Administrative_True() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(251095, 251088, 251087, 251081, 251078, 251040, 248467);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.ADMINISTRATIVE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_True_Administrative_True() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(251081, 251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_ADMINISTRATIVE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_True_Administrative_False() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		// note:  the FixLogEntryService will override this to NOT_MESSAGE_AND_NOT_ADMINISTRATIVE as it should not return non-administrative fix messages.
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_NOT_ADMINISTRATIVE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchByFixLogEntryType_Message_False_Administrative_True() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(248467, 251040, 251078, 251087, 251095);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_ADMINISTRATIVE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_False_Administrative_False() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_NOT_ADMINISTRATIVE);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchByFixLogEntryTypeName_Error_Event() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(248467, 251040);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeName("Error Event");

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryTypeName_Message() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(251088, 251081);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeName("Message");

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryTypeNameEquals_Message() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeNameEquals("Message");

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchBy_heartbeatMessages_True() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchBy_heartbeatMessages_False() {
		List<Integer> expectedLogEntryIdList = CollectionUtils.createList(248467, 251040, 251078, 251081, 251087, 251088, 251095);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(false);

		List<FixLogEntry> searchResults = this.fixLogEntryService.getFixLogEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedLogEntryIdList, searchResults);
	}


	@Test
	public void testGetFixLogEntry() {
		long fixLogEntryId = 251040;
		FixLogEntry fixLogEntry = this.fixLogEntryService.getFixLogEntry(fixLogEntryId);
		Assertions.assertNotNull(fixLogEntry);
		Assertions.assertEquals(fixLogEntryId, fixLogEntry.getId());
	}


	@Test
	public void testGetFixLogEntry_ID_NOT_EXIST() {
		long fixLogEntryId = 555;
		FixLogEntry fixLogEntry = this.fixLogEntryService.getFixLogEntry(fixLogEntryId);
		Assertions.assertNull(fixLogEntry);
	}


	@Test
	public void testGetFixLogEntry_ID_IS_FOR_MESSAGE() {
		long fixLogEntryId = 249068; // This is a message entry instead of a log entry
		FixLogEntry fixLogEntry = this.fixLogEntryService.getFixLogEntry(fixLogEntryId);
		Assertions.assertNull(fixLogEntry);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertResultListContainsExpectedIds(List<Integer> expectedIds, List<FixLogEntry> entriesToVerifyList) {
		Set<Long> idsToVerifySet = new HashSet<>();
		Set<Long> expectedIdsLong = new HashSet<>(convertToLong(expectedIds));
		for (FixLogEntry logEntry : entriesToVerifyList) {
			idsToVerifySet.add(logEntry.getId());
		}

		Assertions.assertEquals(expectedIdsLong, idsToVerifySet);
	}


	private List<Long> convertToLong(List<Integer> integerList) {
		List<Long> listOfLong = new ArrayList<>();
		for (Integer intNumber : integerList) {
			listOfLong.add(intNumber.longValue());
		}
		return listOfLong;
	}
}
