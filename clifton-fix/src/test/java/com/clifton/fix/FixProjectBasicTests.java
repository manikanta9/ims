package com.clifton.fix;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.fix.identifier.FixIdentifier;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class FixProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "fix";
	}


	@Override
	public boolean isMethodSkipped(Method method) {
		if ("deleteFixSessionDefinition".equals(method.getName())) {
			return true;
		}
		if ("saveFixMessageEntry".equals(method.getName())) {
			return true;
		}
		if ("saveFixTagModifier".equals(method.getName())) {
			return true;
		}
		return false;
	}


	@Override
	public boolean isClassSkipped(String className) {
		//Fix purposely violates the shadowed field rule in order to make XML serialization easier - ignore.
		if (FixIdentifier.class.getName().equals(className)) {
			return true;
		}
		return false;
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("ioi");
		approvedList.add("market");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("com.thoughtworks.");
		imports.add("org.springframework.beans.");
		imports.add("org.springframework.jms.");
		imports.add("javax.annotation.PostConstruct");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.core.io.DefaultResourceLoader");
		imports.add("org.springframework.core.io.Resource");
		imports.add("org.springframework.core.io.ResourceLoader");
		imports.add("javax.sql.DataSource");
		imports.add("com.google.common.base.Charsets");
		imports.add("com.google.common.base.Equivalence");
		imports.add("com.google.common.collect.MapDifference");
		imports.add("com.google.common.collect.Maps");
		imports.add("javax.annotation.Nullable");
		imports.add("java.text.SimpleDateFormat");
		imports.add("org.springframework.test.util.ReflectionTestUtils");
		imports.add("com.google.common.collect.Lists");
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveFixTagModifierEntry");
		ignoredVoidSaveMethodSet.add("saveFixSessionSequenceNumbers");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
