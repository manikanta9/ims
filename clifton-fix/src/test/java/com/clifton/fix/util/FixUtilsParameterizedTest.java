package com.clifton.fix.util;

import com.clifton.fix.session.FixSessionDefinition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class FixUtilsParameterizedTest {

	private static FixSessionDefinition fixSessionDefinition;
	private static FixSessionDefinition fixSessionDefinitionWeeklySession;

	private static final LocalDate firstSunday = LocalDate.of(2016, 8, 14);
	private static final LocalDate secondSunday = LocalDate.of(2016, 8, 21);
	private static final LocalDate saturday = LocalDate.of(2016, 8, 20);
	private static final LocalDate friday = LocalDate.of(2016, 8, 19);

	private static final List<LocalTime> maintenanceWindow = Arrays.asList(
			LocalTime.of(20, 30),
			LocalTime.of(20, 35),
			LocalTime.of(20, 40)
	);
	private static final LocalTime sessionStart = LocalTime.of(20, 40);
	private static final LocalTime sessionEnd = LocalTime.of(20, 30);

	private static final LocalTime weeklySessionStart = LocalTime.of(22, 00);
	private static final LocalTime weeklySessionEnd = LocalTime.of(19, 00);

	private static List<LocalDateTime> activeTimes;
	private static List<LocalDateTime> withinStartStopTimes;
	private static List<LocalDateTime> downTimes;

	private static List<LocalDateTime> weeklyActiveTimes;
	private static List<LocalDateTime> weeklyWithinStartStopTimes;
	private static List<LocalDateTime> weeklyDownTimes;


	public static List<LocalDateTime> data() {
		activeTimes = new ArrayList<>();
		withinStartStopTimes = new ArrayList<>();
		downTimes = new ArrayList<>();

		List<LocalDateTime> data = new ArrayList<>();
		LocalDateTime running = LocalDateTime.of(2016, 8, 14, 0, 0);
		LocalDateTime ending = LocalDateTime.of(2016, 8, 21, 0, 0);
		while (running.isBefore(ending)) {
			data.add(running);

			if (saturday.equals(running.toLocalDate())) {
				downTimes.add(running);
			}
			else if (maintenanceWindow.contains(running.toLocalTime())) {
				downTimes.add(running);
			}
			else if (friday.equals(running.toLocalDate()) && running.toLocalTime().isAfter(sessionEnd)) {
				downTimes.add(running);
			}
			else if ((firstSunday.equals(running.toLocalDate()) || secondSunday.equals(running.toLocalDate())) && running.toLocalTime().isBefore(sessionStart)) {
				downTimes.add(running);
			}
			else {
				activeTimes.add(running);
				// If running and between 20:20 and 20:50 then within start / stop time grace period check
				if (running.getHour() == 20 && running.getMinute() >= 20 && running.getMinute() <= 50) {
					withinStartStopTimes.add(running);
				}
			}

			running = running.plusMinutes(5);
		}
		return data;
	}


	public static List<LocalDateTime> weeklyData() {
		weeklyActiveTimes = new ArrayList<>();
		weeklyWithinStartStopTimes = new ArrayList<>();
		weeklyDownTimes = new ArrayList<>();

		// Sunday up at 22, Friday down at 19

		List<LocalDateTime> weeklyData = new ArrayList<>();
		LocalDateTime running = LocalDateTime.of(2016, 8, 14, 0, 0, 1);
		LocalDateTime ending = LocalDateTime.of(2016, 8, 21, 0, 0);
		while (running.isBefore(ending)) {
			weeklyData.add(running);

			if (saturday.equals(running.toLocalDate())) {
				weeklyDownTimes.add(running);
			}
			else if (friday.equals(running.toLocalDate())) {
				if (running.toLocalTime().isAfter(weeklySessionEnd)) {
					weeklyDownTimes.add(running);
				}
				else {
					weeklyActiveTimes.add(running);
				}
				// on friday, if running and between 18:50 and 19:10 then within start / stop time grace period check
				if (running.toLocalTime().isAfter(LocalTime.of(18, 49, 59)) && running.toLocalTime().isBefore(LocalTime.of(19, 10, 01))) {
					weeklyWithinStartStopTimes.add(running);
				}
			}
			else if ((firstSunday.equals(running.toLocalDate()) || secondSunday.equals(running.toLocalDate()))) {
				if (running.toLocalTime().isBefore(weeklySessionStart)) {
					weeklyDownTimes.add(running);
				}
				else {
					weeklyActiveTimes.add(running);
				}
				// on sunday, if running and between 21:50 and 22:10 then within start / stop time grace period check
				if (running.toLocalTime().isAfter(LocalTime.of(21, 49, 59)) && running.toLocalTime().isBefore(LocalTime.of(22, 10, 01))) {
					weeklyWithinStartStopTimes.add(running);
				}
			}
			else {
				weeklyActiveTimes.add(running);
			}

			running = running.plusMinutes(5);
		}
		return weeklyData;
	}


	@BeforeAll
	public static void beforeClass() {
		// Run using local time zone so no conflicts when checking hours
		fixSessionDefinition = new FixSessionDefinition();
		fixSessionDefinition.setStartTime("20:40:00 " + ZoneId.systemDefault());
		fixSessionDefinition.setEndTime("20:30:00 " + ZoneId.systemDefault());
		fixSessionDefinition.setStartWeekday(1);
		fixSessionDefinition.setEndWeekday(6);
		fixSessionDefinition.setName("CITI CARE");

		fixSessionDefinitionWeeklySession = new FixSessionDefinition();
		fixSessionDefinitionWeeklySession.setWeeklySession(true);
		fixSessionDefinitionWeeklySession.setStartTime("22:00:00 " + ZoneId.systemDefault());
		fixSessionDefinitionWeeklySession.setEndTime("19:00:00 " + ZoneId.systemDefault());

		fixSessionDefinitionWeeklySession.setStartWeekday(1);  // Sunday
		fixSessionDefinitionWeeklySession.setEndWeekday(6);    // Friday
		fixSessionDefinitionWeeklySession.setName("Newport EMS");

		Assertions.assertEquals(data().size(), activeTimes.size() + downTimes.size());
	}


	@ParameterizedTest
	@MethodSource("data")
	public void testIsSessionActive(LocalDateTime currentDateTime) {
		boolean isActive = FixUtils.isSessionActive(fixSessionDefinition, currentDateTime, currentDateTime);
		if (isActive) {
			Assertions.assertTrue(activeTimes.contains(currentDateTime), "Expected Active Time " + currentDateTime.toString());
			Assertions.assertEquals(withinStartStopTimes.contains(currentDateTime), FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, currentDateTime, currentDateTime), "Expected " + (!withinStartStopTimes.contains(currentDateTime) ? "NOT" : "") + " to be within start/stop grace period " + currentDateTime.toString());
		}
		else {
			Assertions.assertTrue(downTimes.contains(currentDateTime), "Expected Down Time " + currentDateTime.toString());
		}
	}


	@ParameterizedTest
	@MethodSource("weeklyData")
	public void testIsWeeklySessionActive(LocalDateTime currentDateTime) {
		boolean isActive = FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, currentDateTime, currentDateTime);
		if (isActive) {
			Assertions.assertTrue(weeklyActiveTimes.contains(currentDateTime), "Expected Active Time " + currentDateTime.toString());
			Assertions.assertEquals(weeklyWithinStartStopTimes.contains(currentDateTime), FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinitionWeeklySession, 10, currentDateTime, currentDateTime), "Expected " + (!withinStartStopTimes.contains(currentDateTime) ? "NOT" : "") + " to be within start/stop grace period " + currentDateTime.toString());
		}
		else {
			Assertions.assertTrue(weeklyDownTimes.contains(currentDateTime), "Expected Down Time " + currentDateTime.toString());
		}
	}


	@Test
	public void testIsSessionActive_SpecificTimes() {
		LocalDateTime saturday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 9), LocalTime.NOON);
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinition, saturday_noon, saturday_noon), "Expected to be inactive Saturday at noon");

		LocalDateTime sunday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 10), LocalTime.NOON);
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinition, sunday_noon, sunday_noon), "Expected to be inactive Sunday at noon");

		LocalDateTime sunday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 10), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinition, sunday_11_pm, sunday_11_pm), "Expected to be active Sunday at 11 pm");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, sunday_11_pm, sunday_11_pm), "Expected not to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime monday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.NOON);
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinition, monday_noon, monday_noon), "Expected to be active Monday at noon");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_noon, monday_noon), "Expected not to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime monday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinition, monday_11_pm, monday_11_pm), "Expected to be active Monday at 11 pm");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_11_pm, monday_11_pm), "Expected not to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime monday_829_pm = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.of(20, 29));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinition, monday_829_pm, monday_829_pm), "Expected to be active Monday at 8:29 pm");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_829_pm, monday_829_pm), "Expected to be within first/last 10 minutes of session start up / shut down");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 5, monday_829_pm, monday_829_pm), "Expected to be within first/last 10 minutes of session start up / shut down");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 2, monday_829_pm, monday_829_pm), "Expected to be within first/last 2 minutes of session start up / shut down");

		LocalDateTime monday_845_pm = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.of(20, 45));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinition, monday_845_pm, monday_845_pm), "Expected to be active Monday at 8:45 pm");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_845_pm, monday_845_pm), "Expected to be within first/last 10 minutes of session start up / shut down");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 5, monday_845_pm, monday_845_pm), "Expected to be within first/last 10 minutes of session start up / shut down");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 2, monday_845_pm, monday_845_pm), "Expected NOT to be within first/last 2 minutes of session start up / shut down");
	}


	@Test
	public void testIsSessionActive_WeeklySession_SpecificTimes() {
		LocalDateTime saturday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 9), LocalTime.NOON);
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, saturday_noon, saturday_noon), "Expected to be inactive Saturday at noon");

		LocalDateTime sunday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 10), LocalTime.NOON);
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, sunday_noon, sunday_noon), "Expected to be inactive Sunday at noon");

		LocalDateTime sunday_6_pm = LocalDateTime.of(LocalDate.of(2020, 5, 10), LocalTime.of(18, 0));
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, sunday_6_pm, sunday_6_pm), "Expected to be inactive Sunday at 6 pm");

		LocalDateTime sunday_701_pm = LocalDateTime.of(LocalDate.of(2020, 5, 10), LocalTime.of(19, 1));
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, sunday_701_pm, sunday_701_pm), "Expected to be inactive Sunday at 7:01 pm");

		LocalDateTime sunday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 10), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, sunday_11_pm, sunday_11_pm), "Expected to be active Sunday at 11 pm");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, sunday_11_pm, sunday_11_pm), "Expected NOT to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime monday_8_am = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.of(8, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, monday_8_am, monday_8_am), "Expected to be active Monday at 8 am");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_8_am, monday_8_am), "Expected NOT to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime monday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.NOON);
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, monday_noon, monday_noon), "Expected to be active Monday at noon");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_noon, monday_noon), "Expected NOT to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime monday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 11), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, monday_11_pm, monday_11_pm), "Expected to be active Monday at 11 pm");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, monday_11_pm, monday_11_pm), "Expected NOT to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime tuesday_8_am = LocalDateTime.of(LocalDate.of(2020, 5, 12), LocalTime.of(8, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, tuesday_8_am, tuesday_8_am), "Expected to be active Tuesday at 8 am");
		Assertions.assertFalse(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinition, 10, tuesday_8_am, tuesday_8_am), "Expected NOT to be within first/last 10 minutes of session start up / shut down");

		LocalDateTime tuesday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 12), LocalTime.NOON);
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, tuesday_noon, tuesday_noon), "Expected to be active Tuesday at noon");

		LocalDateTime tuesday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 12), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, tuesday_11_pm, tuesday_11_pm), "Expected to be active Tuesday at 11 pm");

		LocalDateTime wednesday_8_am = LocalDateTime.of(LocalDate.of(2020, 5, 13), LocalTime.of(8, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, wednesday_8_am, wednesday_8_am), "Expected to be active Wednesday at 8 am");

		LocalDateTime wednesday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 13), LocalTime.NOON);
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, wednesday_noon, wednesday_noon), "Expected to be active Wednesday at noon");

		LocalDateTime wednesday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 13), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, wednesday_11_pm, wednesday_11_pm), "Expected to be active Wednesday at 11 pm");

		LocalDateTime thursday_8_am = LocalDateTime.of(LocalDate.of(2020, 5, 14), LocalTime.of(8, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, thursday_8_am, thursday_8_am), "Expected to be active Thursday at 8 am");

		LocalDateTime thursday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 14), LocalTime.NOON);
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, thursday_noon, thursday_noon), "Expected to be active Thursday at noon");

		LocalDateTime thursday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 14), LocalTime.of(23, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, thursday_11_pm, thursday_11_pm), "Expected to be active Thursday at 11 pm");

		LocalDateTime friday_8_am = LocalDateTime.of(LocalDate.of(2020, 5, 15), LocalTime.of(8, 0));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, friday_8_am, friday_8_am), "Expected to be active Friday at 8 am");

		LocalDateTime friday_noon = LocalDateTime.of(LocalDate.of(2020, 5, 15), LocalTime.NOON);
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, friday_noon, friday_noon), "Expected to be active Friday at noon");

		LocalDateTime friday_659_pm = LocalDateTime.of(LocalDate.of(2020, 5, 15), LocalTime.of(18, 59));
		Assertions.assertTrue(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, friday_659_pm, friday_659_pm), "Expected to be active Friday at 659 pm");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinitionWeeklySession, 10, friday_659_pm, friday_659_pm), "Expected to be within first/last 10 minutes of session start up / shut down");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinitionWeeklySession, 5, friday_659_pm, friday_659_pm), "Expected to be within first/last 10 minutes of session start up / shut down");
		Assertions.assertTrue(FixUtils.isSessionWithinStartUpOrShutDownMinutes(fixSessionDefinitionWeeklySession, 2, friday_659_pm, friday_659_pm), "Expected to be within first/last 2 minutes of session start up / shut down");

		LocalDateTime friday_11_pm = LocalDateTime.of(LocalDate.of(2020, 5, 15), LocalTime.of(23, 0));
		Assertions.assertFalse(FixUtils.isSessionActive(fixSessionDefinitionWeeklySession, friday_11_pm, friday_11_pm), "Expected to be inactive friday at 11 pm");
	}
}
