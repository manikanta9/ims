package com.clifton.fix.util;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.util.date.DateUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;


public class FixUtilsTests {

	@Test
	public void testParseUniqueId() {
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("O_124_ABCDEFG"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("124_ABCDEFG"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("O_124"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("O_124_CANCEL"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("124_ABCDEFG_CANCEL"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("O_124_ABCDEFG_CANCEL"));

		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("A_124_ABCDEFG"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("124_ABCDEFG"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("A_124"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("A_124_CANCEL"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("124_ABCDEFG_CANCEL"));
		Assertions.assertEquals(Integer.valueOf(124), FixUtils.parseUniqueId("A_124_ABCDEFG_CANCEL"));
	}


	@Test
	public void testCreateUniqueIdForCancel() {
		NamedEntityTestObject entity = new NamedEntityTestObject();
		entity.setId(124);
		entity.setCreateDate(DateUtils.toDate("2015-09-25 03:15:00", DateUtils.DATE_FORMAT_FULL));

		Date date = entity.getCreateDate();
		Assertions.assertEquals("124_" + DateUtils.fromDate(date, "yyyyMMddHHmmss") + "_CANCEL", FixUtils.createUniqueIdForCancel(entity, null, date));
		Assertions.assertEquals("O_124_" + DateUtils.fromDate(date, "yyyyMMddHHmmss") + "_CANCEL", FixUtils.createUniqueIdForCancel(entity, "O", date));
	}


	@Test
	public void testCleanUniqueId() {
		Assertions.assertEquals("O_124_20150925", FixUtils.cleanUniqueId("O_124_20150925031500_CANCEL"));
		Assertions.assertEquals("O_124_20150925", FixUtils.cleanUniqueId("O_124_20150925_CANCEL"));
		Assertions.assertEquals("O_124_20150925", FixUtils.cleanUniqueId("O_124_20150925"));
	}


	@Test
	public void testCreateUniqueIdFromId() {
		long id = 124;
		Date date = new Date();
		Assertions.assertEquals("124_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdFromId(id, null, null, date));
		Assertions.assertEquals("124_TEST_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdFromId(id, null, "TEST", date));
		Assertions.assertEquals("O_124_TEST_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdFromId(id, "O", "TEST", date));
		Assertions.assertEquals("O_124_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdFromId(id, "O", null, date));
	}


	@Test
	public void testCreateUniqueId() {
		NamedEntityTestObject entity = new NamedEntityTestObject();
		entity.setId(124);
		entity.setCreateDate(new Date());

		Date date = entity.getCreateDate();
		Assertions.assertEquals("124_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdWithTag(entity, null, null));
		Assertions.assertEquals("124_TEST_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdWithTag(entity, null, "TEST"));
		Assertions.assertEquals("O_124_TEST_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdWithTag(entity, "O", "TEST"));
		Assertions.assertEquals("O_124_" + DateUtils.fromDate(date, "yyyyMMdd"), FixUtils.createUniqueIdWithTag(entity, "O", null));
	}


	/**
	 * According to the documentation 'quickfixj/core/src/main/doc/usermanual/usage/configuration.html':
	 * <p>
	 * "The TimeZone setting will be used, if set, or UTC will be used by default. The timezone string should
	 * be one that the Java TimeZone class can resolve. For example, "15:00:00 US/Central".'
	 * <p>
	 * This will check all the UI timezones to make sure java TimeZone can parse it correctly.
	 * <p>
	 * Clifton.fix.AmericanTimeZones = [
	 * ['America/New_York', 'America/New_York'],            UTC-5
	 * ['America/Chicago', 'America/Chicago'],              UTC-6
	 * ['America/Denver', 'America/Denver'],                UTC-7
	 * ['America/Los_Angeles', 'America/Los_Angeles'],      UTC-8
	 * ['America/Anchorage', 'America/Anchorage'],          UTC-9
	 * ['Pacific/Honolulu', 'Pacific/Honolulu']             UTC-10
	 * ['America/Adak', 'America/Adak'],                    UTC-10
	 * ];
	 * <p>
	 * NOTE:  UTC time is not always the same as DST.
	 * For example, America/New_York is UTC-5 but UTC-4 DST.
	 * <p>
	 * Honolulu and Adak are listed twice with the same UTC-10, however, one observes DST and the other doesn't.
	 */
	@Test
	public void testParsingTimes() {
		// some existing settings use 'US/Central'.
		ZoneId zoneId = FixUtils.parseTimeZone("16:33:00 US/Central");
		MatcherAssert.assertThat(ZoneId.of("America/Chicago").getRules(), IsEqual.equalTo(zoneId.getRules()));
		MatcherAssert.assertThat(ZoneId.of("America/Los_Angeles").getRules(), IsNot.not(IsEqual.equalTo(zoneId.getRules())));
		String[] available = TimeZone.getAvailableIDs();
		Assertions.assertTrue(Arrays.asList(available).contains("US/Central"));
		Assertions.assertTrue(Arrays.asList(available).contains("America/Chicago"));
		Map<String, Integer> zones = new HashMap<String, Integer>() {
			{
				put("America/New_York", -5);
				put("America/Chicago", -6);
				put("America/Denver", -7);
				put("America/Los_Angeles", -8);
				put("America/Anchorage", -9);
				put("Pacific/Honolulu", -10);
				put("America/Adak", -10);
			}
		};
		for (Map.Entry<String, Integer> stringIntegerEntry : zones.entrySet()) {
			TimeZone timeZone = TimeZone.getTimeZone(stringIntegerEntry.getKey());
			int offset = (timeZone.getRawOffset() / (1000 * 60 * 60)) % 24;
			Assertions.assertEquals(stringIntegerEntry.getValue().intValue(), offset);
			zoneId = FixUtils.parseTimeZone("16:33:00 " + stringIntegerEntry.getKey());
			Assertions.assertNotNull(zoneId);
		}
	}
}
