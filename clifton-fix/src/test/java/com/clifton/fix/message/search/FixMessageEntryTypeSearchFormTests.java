package com.clifton.fix.message.search;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * FixMessageEntryType search tests via the FixMessageEntryService.
 *
 * @author davidi
 */
@FixInMemoryDatabaseContext
public class FixMessageEntryTypeSearchFormTests extends BaseInMemoryDatabaseTests {

	@Resource
	FixMessageEntryService fixMessageEntryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFixMessageEntryServiceAvailable() {
		Assertions.assertNotNull(this.fixMessageEntryService);
	}


	////////////////////////////////////////////////////////////////////////////
	//                         FixMessage Tests                               //
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSearchById() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(4);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setId((short) 4);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByIds() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(5, 6);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setIds(ArrayUtils.createArray((short) 5, (short) 6));

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByPattern_Name() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(6);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setSearchPattern("NewOrderSingle");

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByPattern_Description() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(10);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setSearchPattern("Order execution detail");

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByName() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(13);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setName("AllocationReport");

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByDescription() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(2);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setDescription("The entry is an error event.");

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByDefaultType_True() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(3);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setDefaultType(true);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByDefaultType_False() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setDefaultType(false);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByMessageTypeTagValue() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(9);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setMessageTypeTagValue("J");

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByIdFieldTag() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(6, 7, 8, 10, 11);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setIdFieldTag((short) 11);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByReferencedIdFieldTag() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(9, 12, 13);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setReferencedIdFieldTag((short) 72);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByDropCOpyIdFieldTag() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(10);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setDropCopyIdFieldTag((short) 37);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByMessageFlag_True() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setMessage(true);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByMessageFlag_False() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setMessage(false);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByIsAdministrativeFlag_True() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2, 4, 5);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setAdministrative(true);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByIsAdministrativeFlag_False() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setAdministrative(false);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByisPersistentFlag_True() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setPersistent(true);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByisPersistentFlag_False() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(4, 14, 15);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setPersistent(false);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByIsCacheNonPersistentFlagTrue() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(4, 14, 15);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setCacheNonPersistentMessage(true);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByIsCacheNonPersistentFlag_False() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setCacheNonPersistentMessage(false);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Administrative() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2, 4, 5);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.ADMINISTRATIVE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Not_Administrative() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_ADMINISTRATIVE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Message() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Not_Message() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Message_And_Administrative() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(4, 5);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_ADMINISTRATIVE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Message_And_Not_Administrative() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(3, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15,16);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_NOT_ADMINISTRATIVE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Not_Message_And_Administrative() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList(1, 2);
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_ADMINISTRATIVE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}


	@Test
	public void testSearchByFixMessageEntryTypeIdsName_Not_Message_And_Not_Administrative() {
		List<Integer> expectedMessageTypeIdList = CollectionUtils.createList();
		FixMessageEntryTypeSearchForm searchForm = new FixMessageEntryTypeSearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_NOT_ADMINISTRATIVE);

		List<FixMessageEntryType> searchResults = this.fixMessageEntryService.getFixMessageEntryTypeList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageTypeIdList, searchResults);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertResultListContainsExpectedIds(List<Integer> expectedIds, List<?> entriesToVerifyList) {
		Assertions.assertEquals(expectedIds.size(), entriesToVerifyList.size());

		Set<Short> idsToVerifySet = new HashSet<>();
		Set<Short> expectedIdsShort = new HashSet<>(convertToShort(expectedIds));
		for (Object identityObject : entriesToVerifyList) {
			idsToVerifySet.add((Short) ((IdentityObject) identityObject).getIdentity());
		}

		Assertions.assertEquals(expectedIdsShort, idsToVerifySet);
	}


	private List<Short> convertToShort(List<Integer> integerList) {
		List<Short> listOfShort = new ArrayList<>();
		for (Integer intNumber : integerList) {
			listOfShort.add(intNumber.shortValue());
		}
		return listOfShort;
	}
}
