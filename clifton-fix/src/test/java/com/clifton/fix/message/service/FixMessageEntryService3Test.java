package com.clifton.fix.message.service;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * Additional tests for the FixMessageEntryService. Test is separate due to test data setup requirements.
 */

@FixInMemoryDatabaseContext
public class FixMessageEntryService3Test extends BaseInMemoryDatabaseTests {

	@Resource
	private FixMessageEntryService fixMessageEntryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testClearFixMessageEntriesForSession() {
		FixMessageEntrySearchForm nonMessageEntriesToDeleteSearchForm = new FixMessageEntrySearchForm();
		nonMessageEntriesToDeleteSearchForm.setMessage(false);
		nonMessageEntriesToDeleteSearchForm.setSessionId((short) 5);
		List<FixMessageEntry> initialListDeletion = this.fixMessageEntryService.getFixMessageEntryList(nonMessageEntriesToDeleteSearchForm);
		Assertions.assertFalse(initialListDeletion.isEmpty());

		FixMessageEntrySearchForm expectedPreservedMessagesSearchForm = new FixMessageEntrySearchForm();
		expectedPreservedMessagesSearchForm.setMessage(true);
		List<FixMessageEntry> expectedPreservedMessages = this.fixMessageEntryService.getFixMessageEntryList(expectedPreservedMessagesSearchForm);
		int expectedPreservedMessagesSize = expectedPreservedMessages.size();
		Assertions.assertTrue(expectedPreservedMessagesSize > 0);

		this.fixMessageEntryService.clearFixMessageEntriesForSession((short) 5);

		// Make sure non-message entries were deleted
		FixMessageEntrySearchForm deletedMessagesSearchForm = new FixMessageEntrySearchForm();
		deletedMessagesSearchForm.setMessage(false);
		deletedMessagesSearchForm.setSessionId((short) 5);
		List<FixMessageEntry> finalList = this.fixMessageEntryService.getFixMessageEntryList(deletedMessagesSearchForm);
		Assertions.assertTrue(finalList.isEmpty());

		// Make sure preserved messages remain (not deleted)
		FixMessageEntrySearchForm preservedMessagesSearchForm = new FixMessageEntrySearchForm();
		preservedMessagesSearchForm.setMessage(true);
		List<FixMessageEntry> preservedMessages = this.fixMessageEntryService.getFixMessageEntryList(preservedMessagesSearchForm);
		int preservedMessageSize = preservedMessages.size();
		Assertions.assertEquals(expectedPreservedMessagesSize, preservedMessageSize);
		Assertions.assertEquals(expectedPreservedMessages, preservedMessages);
	}
}
