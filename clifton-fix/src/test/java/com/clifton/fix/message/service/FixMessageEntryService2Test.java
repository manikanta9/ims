package com.clifton.fix.message.service;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


/**
 * Additional tests for the FixMessageEntryService.  Some tests here cannot be run within the
 * original FixMessageEntryService1Test due to that test suite setting up pre-configured heartbeat entries that
 * conflict with the newer tests.
 */

@FixInMemoryDatabaseContext
public class FixMessageEntryService2Test extends BaseInMemoryDatabaseTests {

	@Resource
	private FixMessageEntryService fixMessageEntryService;

	@Resource
	private FixSessionService fixSessionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@AfterEach
	public void afterEach() {
		this.fixMessageEntryService.clearFixHeartbeatList();
		this.fixMessageEntryService.clearFixNonPersistentMessageList();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveFixMessageEntry() {
		FixMessageEntryType entryType = this.fixMessageEntryService.getFixMessageEntryTypeByName("NewOrderSingle");
		FixSession fixSession = this.fixSessionService.getFixSession((short) 8);
		Date timeStamp = DateUtils.toDate("11/12/2020 10:16:00");

		FixMessageEntry message = new FixMessageEntry();
		message.setType(entryType);
		message.setSession(fixSession);
		message.setMessageEntryDateAndTime(timeStamp);
		message.setText("8=FIX.4.4\u00019=253\u000135=D\u000134=2368\u000149=CLIFTONMB44\u000150=r154200\u000152=20201103-20:44:53.980\u000156=REDITKTS\u000157=TKTS\u00011=C0320613-T\u000111=O_517012_20201103\u000121=1\u000122=A\u000138=15.00000000\u000140=1\u000148=MFSZ0 Index\u000154=1\u000155=MFSZ0 Index\u000159=0\u000160=20201103-20:44:53.965\u000164=20201103\u000175=20201103\u0001167=FUT\u00018135=GS\u000110=086\u0001");
		message.setIncoming(true);
		message.setProcessed(false);

		FixMessageEntry savedMessage = this.fixMessageEntryService.saveFixMessageEntry(message);
		Assertions.assertNotNull(savedMessage);
		Assertions.assertNotNull(savedMessage.getId());

		FixMessageEntry queriedMessage = this.fixMessageEntryService.getFixMessageEntry(savedMessage.getId());

		Assertions.assertEquals(savedMessage, queriedMessage);
	}


	@Test
	public void testSaveFixMessageEntry_heartbeatMsg() {
		FixMessageEntryType entryType = this.fixMessageEntryService.getFixMessageEntryTypeByName("Heartbeat Message");
		FixSession fixSession = this.fixSessionService.getFixSession((short) 6);
		Date timeStamp = DateUtils.toDate("11/12/2020 10:16:00");

		FixMessageEntry message = new FixMessageEntry();
		message.setType(entryType);
		message.setSession(fixSession);
		message.setMessageEntryDateAndTime(timeStamp);
		message.setText("8=FIX.4.4\u00019=68\u000135=0\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49307\u000152=20201112-21:57:27.357\u000110=249\u0001");
		message.setIncoming(true);
		message.setProcessed(false);

		FixMessageEntry savedMessage = this.fixMessageEntryService.saveFixMessageEntry(message);
		Assertions.assertNotNull(savedMessage);
		Assertions.assertNotNull(savedMessage.getId());

		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setId(savedMessage.getId());
		searchForm.setHeartbeatMessages(true);
		FixMessageEntry queriedMessage = this.fixMessageEntryService.getFixMessageEntryNonPersistent(searchForm);

		Assertions.assertEquals(savedMessage, queriedMessage);
	}


	@Test
	public void testSaveFixMessageEntry_nonPersistentMsg() {
		FixMessageEntryType entryType = this.fixMessageEntryService.getFixMessageEntryTypeByName("IndicationOfInterest");
		FixSession fixSession = this.fixSessionService.getFixSession((short) 14);
		Date timeStamp = DateUtils.toDate("11/12/2020 11:38:00");

		FixMessageEntry message = new FixMessageEntry();
		message.setType(entryType);
		message.setSession(fixSession);
		message.setMessageEntryDateAndTime(timeStamp);
		message.setText("8=FIX.4.4\u00019=313\u000135=6\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49127\u000152=20201112-20:38:35.336\u000123=HTGM_IOI_20201112_0000010796\u000128=N\u000155=[N/A]\u000127=10000\u0001423=1\u000144=117.86000000\u000148=790697DC7\u000122=1\u0001460=11\u000164=20201116\u000154=2\u000160=20201112-20:38:35.336\u0001232=3\u0001233=MINQTY\u0001234=5000\u0001233=MININCR\u0001234=5000\u0001233=MINTOKEEP\u0001234=5000\u0001453=1\u0001448=0443HEAD\u0001447=C\u0001452=1\u000110=164\u0001");
		message.setIncoming(true);
		message.setProcessed(false);

		FixMessageEntry savedMessage = this.fixMessageEntryService.saveFixMessageEntry(message);
		Assertions.assertNotNull(savedMessage);
		Assertions.assertNotNull(savedMessage.getId());

		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setId(savedMessage.getId());
		searchForm.setNonPersistentMessages(true);
		FixMessageEntry queriedMessage = this.fixMessageEntryService.getFixMessageEntryNonPersistent(searchForm);

		Assertions.assertEquals(savedMessage, queriedMessage);
	}


	@Test
	public void testGetFixMessageEntryList_heartbeatMsg() {
		HashSet<FixMessageEntry> expectedEntriesSet = new HashSet<>();
		expectedEntriesSet.add(this.createNonPersistentMessage((short) 2, "Heartbeat Message", "2020-11-13 10:10:00", true, "8=FIX.4.4\u00019=68\u000135=0\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49300\u000152=20201113-10:10:00.000\u000110=229\u0001"));
		expectedEntriesSet.add(this.createNonPersistentMessage((short) 2, "Heartbeat Message", "2020-11-13 10:11:00", true, "8=FIX.4.4\u00019=68\u000135=0\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49300\u000152=20201113-10:11:00.000\u000110=229\u0001"));
		FixMessageEntry latestHeartbeat = this.createNonPersistentMessage((short) 2, "Heartbeat Message", "2020-11-13 10:12:00", true, "8=FIX.4.4\u00019=68\u000135=0\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49300\u000152=20201113-10:12:00.000\u000110=229\u0001");
		expectedEntriesSet.add(latestHeartbeat);

		// get the list
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);
		List<FixMessageEntry> queriedMessageList = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		HashSet<FixMessageEntry> resultSet = new HashSet<>(queriedMessageList);
		Assertions.assertTrue(resultSet.containsAll(expectedEntriesSet));

		// get latest entry
		searchForm.setHeartbeatMessages(null);
		searchForm.setLatestHeartbeatsOnly(true);
		searchForm.setSessionDefinitionName("Goldman Sachs");

		List<FixMessageEntry> resultList = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		Assertions.assertEquals(1, resultList.size());
		Assertions.assertEquals(latestHeartbeat, resultList.get(0));
	}


	@Test
	public void testGetFixMessageEntryList_nonPersistentMsg() {
		HashSet<FixMessageEntry> expectedEntriesSet = new HashSet<>();
		expectedEntriesSet.add(this.createNonPersistentMessage((short) 7, "IndicationOfInterest", "2020-11-12 13:38:00", true, "8=FIX.4.4\u00019=313\u000135=6\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49130\u000152=20201112-13:38:00.000\u000123=HTGM_IOI_20201112_0000010799\u000128=N\u000155=[N/A]\u000127=25000\u0001423=1\u000144=104.44000000\u000148=13063BRJ3\u000122=1\u0001460=11\u000164=20201116\u000154=2\u000160=20201112-13:38:00.000\u0001232=3\u0001233=MINQTY\u0001234=5000\u0001233=MININCR\u0001234=5000\u0001233=MINTOKEEP\u0001234=5000\u0001453=1\u0001448=0443HEAD\u0001447=C\u0001452=1\u000110=167\u0001"));
		expectedEntriesSet.add(this.createNonPersistentMessage((short) 7, "IndicationOfInterest", "2020-11-14 13:45:00", true, "8=FIX.4.4\u00019=313\u000135=6\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49125\u000152=20201114-13:45:00.000\u000123=HTGM_IOI_20201114_0000010794\u000128=N\u000155=[N/A]\u000127=50000\u0001423=1\u000144=112.78000000\u000148=73358WRQ9\u000122=1\u0001460=11\u000164=20201116\u000154=2\u000160=20201114-13:45:00.000\u0001232=3\u0001233=MINQTY\u0001234=5000\u0001233=MININCR\u0001234=5000\u0001233=MINTOKEEP\u0001234=5000\u0001453=1\u0001448=0443HEAD\u0001447=C\u0001452=1\u000110=215\u0001"));
		FixMessageEntry latestMessage = this.createNonPersistentMessage((short) 7, "IndicationOfInterest", "2020-11-14 14:00:05", true, "8=FIX.4.4\u00019=313\u000135=6\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=49127\u000152=20201114-14:00:05.000\u000123=HTGM_IOI_20201114_0000010796\u000128=N\u000155=[N/A]\u000127=10000\u0001423=1\u000144=117.86000000\u000148=790697DC7\u000122=1\u0001460=11\u000164=20201116\u000154=2\u000160=20201114-14:00:05.000\u0001232=3\u0001233=MINQTY\u0001234=5000\u0001233=MININCR\u0001234=5000\u0001233=MINTOKEEP\u0001234=5000\u0001453=1\u0001448=0443HEAD\u0001447=C\u0001452=1\u000110=164\u0001");
		expectedEntriesSet.add(latestMessage);

		// get the list
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setNonPersistentMessages(true);
		List<FixMessageEntry> queriedMessageList = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		HashSet<FixMessageEntry> resultSet = new HashSet<>(queriedMessageList);
		Assertions.assertEquals(expectedEntriesSet, resultSet);

		// get latest entry
		searchForm.setLatestNonPersistentMessagesOnly(true);
		searchForm.setSessionDefinitionName("REDI Multi-Broker");

		List<FixMessageEntry> resultList = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		Assertions.assertEquals(1, resultList.size());
		Assertions.assertEquals(latestMessage, resultList.get(0));
	}


	@Test
	public void testGetFixMessageEntry() {
		long msgId = 251016;
		FixMessageEntry queriedMessage = this.fixMessageEntryService.getFixMessageEntry(msgId);
		Assertions.assertNotNull(queriedMessage);
		Assertions.assertEquals(251016, queriedMessage.getId());
	}


	@Test
	public void testGetFixMessageEntryType() {
		short typeId = 5;
		FixMessageEntryType queriedMessageType = this.fixMessageEntryService.getFixMessageEntryType(typeId);
		Assertions.assertNotNull(queriedMessageType);
		Assertions.assertEquals(5, (int) queriedMessageType.getId());
	}


	@Test
	public void testGetFixMessageEntryTypeByName() {
		String typeName = "NewOrderSingle";
		FixMessageEntryType queriedMessageType = this.fixMessageEntryService.getFixMessageEntryTypeByName(typeName);
		Assertions.assertNotNull(queriedMessageType);
		Assertions.assertEquals(typeName, queriedMessageType.getName());
	}


	@Test
	public void testGetFixMessageEntryTypeDefault() {
		String typeName = "Message";
		FixMessageEntryType queriedMessageType = this.fixMessageEntryService.getFixMessageEntryTypeDefault();
		Assertions.assertNotNull(queriedMessageType);
		Assertions.assertEquals(typeName, queriedMessageType.getName());
	}


	@Test
	public void testGetFixMessageEntryTypeByTagValue() {
		HashMap<String, String> expectedTypesMap = new HashMap<>();
		expectedTypesMap.put("D", "NewOrderSingle");
		expectedTypesMap.put("F", "OrderCancelRequest");
		expectedTypesMap.put("G", "OrderCancelReplaceRequest");
		expectedTypesMap.put("J", "AllocationInstruction");
		expectedTypesMap.put("8", "ExecutionReport");
		expectedTypesMap.put("9", "OrderCancelReject");
		expectedTypesMap.put("P", "AllocationInstructionAck");
		expectedTypesMap.put("AS", "AllocationReport");
		expectedTypesMap.put("6", "IndicationOfInterest");
		expectedTypesMap.put("0", "Heartbeat Message");

		for (Map.Entry<String, String> entry : expectedTypesMap.entrySet()) {
			FixMessageEntryType queriedMessageType = this.fixMessageEntryService.getFixMessageTypeByTagValue(entry.getKey());
			Assertions.assertNotNull(queriedMessageType);
			Assertions.assertEquals(entry.getValue(), queriedMessageType.getName());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private FixMessageEntry createNonPersistentMessage(short sessionId, String typeName, String dateString, boolean incoming, String text) {
		final FixMessageEntryType entryType = this.fixMessageEntryService.getFixMessageEntryTypeByName(typeName);
		final FixSession fixSession = this.fixSessionService.getFixSession(sessionId);
		final Date timeStamp = DateUtils.toDate(dateString, DateUtils.DATE_FORMAT_FULL);
		final FixMessageEntry message = new FixMessageEntry();

		message.setType(entryType);
		message.setSession(fixSession);
		message.setMessageEntryDateAndTime(timeStamp);
		message.setText(text);
		message.setIncoming(incoming);
		message.setProcessed(false);

		return this.fixMessageEntryService.saveFixMessageEntry(message);
	}
}
