package com.clifton.fix.message.service;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.RollingList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryBuilder;
import com.clifton.fix.message.FixMessageEntryHeartbeatStatus;
import com.clifton.fix.message.FixMessageEntryHeartbeatStatusCommand;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionBuilder;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionDefinitionBuilder;
import com.clifton.fix.session.FixSessionService;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;


@FixInMemoryDatabaseContext
public class FixMessageEntryService1Test extends BaseInMemoryDatabaseTests {

	private static final Function<List<FixMessageEntry>, RollingList<FixMessageEntry>> RollingListSupplier = fixMessageEntry -> {
		RollingList<FixMessageEntry> rollingList = new RollingList<>(250);
		rollingList.addAll(fixMessageEntry);
		return rollingList;
	};

	private static final Map<String, RollingList<FixMessageEntry>> heartbeatLogMap = new ConcurrentHashMap<>();


	@Resource
	private ContextHandler contextHandler;

	@Resource
	private FixMessageEntryService fixMessageEntryService;

	@Resource
	private FixSessionService fixSessionService;

	@Resource
	private SystemListService<SystemList> systemListService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void beforeClass() {
		heartbeatLogMap.put("Goldman Sachs", RollingListSupplier.apply(
				Arrays.asList(
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry()
				)
				)
		);
		heartbeatLogMap.put("CITI CARE", RollingListSupplier.apply(
				Arrays.asList(
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry()
				)
				)
		);
		heartbeatLogMap.put("REDI Multi-Broker", RollingListSupplier.apply(
				Arrays.asList(
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry()
				)
				)
		);
		heartbeatLogMap.put("Bloomberg", RollingListSupplier.apply(
				Arrays.asList(
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
						FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry()
				)
				)
		);


		heartbeatLogMap.get("Goldman Sachs").get(2).setMessageEntryDateAndTime(DateUtils.toDate("2220-11-13 13:08:00", DateUtils.DATE_FORMAT_FULL));
		heartbeatLogMap.get("Bloomberg").get(2).setMessageEntryDateAndTime(DateUtils.toDate("2220-11-13 13:08:00", DateUtils.DATE_FORMAT_FULL));
	}


	@BeforeEach
	public void before() {
		setCurrentUser();
		ReflectionTestUtils.setField(this.fixMessageEntryService, "heartbeatLogMap", heartbeatLogMap);
	}


	@AfterEach
	public void after() {
		// MUST CLEAR THIS OR ELSE IT AFFECTS OTHER TESTS (FixLogEntrySearchFormTests)
		ReflectionTestUtils.setField(this.fixMessageEntryService, "heartbeatLogMap", new HashMap<>());
	}


	@Test
	public void testHeartbeatIncomingFilter() {
		// build search form
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);
		searchForm.setRestrictionList(Collections.singletonList(
				new SearchRestriction("incoming", ComparisonConditions.EQUALS, Boolean.TRUE)
		));
		// do search
		List<FixMessageEntry> filteredByIncoming = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		List<Boolean> filteredIncoming = filteredByIncoming.stream().map(FixMessageEntry::getIncoming).collect(Collectors.toList());
		List<Boolean> expectedIncoming = heartbeatLogMap.values()
				.stream()
				.flatMap(List::stream)
				.map(FixMessageEntry::getIncoming)
				.filter(Boolean.TRUE::equals)
				.collect(Collectors.toList());
		MatcherAssert.assertThat(filteredIncoming, IsEqual.equalTo(expectedIncoming));
	}


	@Test
	public void testHeartbeatSessionNameFilter() {
		// save default session definition
		FixSessionDefinition rediFixSessionDefinition = heartbeatLogMap.get("REDI Multi-Broker").get(0).getSession().getDefinition();
		FixSessionDefinition defaultSessionDefinition = rediFixSessionDefinition.getParent();
		defaultSessionDefinition.setId(null);
		this.fixSessionService.saveFixSessionDefinition(defaultSessionDefinition);

		// save redi session definition
		rediFixSessionDefinition.setId(null);
		rediFixSessionDefinition.setParent(defaultSessionDefinition);
		rediFixSessionDefinition.setConnectivityType(generateTestConnectivityType());
		this.fixSessionService.saveFixSessionDefinition(rediFixSessionDefinition);
		// save redi session
		FixSession rediFixSession = heartbeatLogMap.get("REDI Multi-Broker").get(0).getSession();
		rediFixSession.setId(null);
		this.fixSessionService.saveFixSession(rediFixSession);
		// set the id for redi session and definition log entry values.
		// 'path' searches actually search the database and match based on primary key.
		heartbeatLogMap.get("REDI Multi-Broker").forEach(l -> {
			l.getSession().setId(rediFixSession.getId());
			l.getSession().getDefinition().setId(rediFixSessionDefinition.getId());
		});
		// build search form like UI does
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);
		searchForm.setRestrictionList(Collections.singletonList(
				new SearchRestriction("sessionDefinitionName", ComparisonConditions.LIKE, "REDI")
		));
		// perform search.
		List<FixMessageEntry> filteredByIncomingName = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		List<String> filteredIncomingName = filteredByIncomingName.stream().map(l -> l.getSession().getDefinition().getName()).collect(Collectors.toList());
		List<String> expectedIncomingName = heartbeatLogMap.values()
				.stream()
				.flatMap(List::stream)
				.filter(l -> l.getSession().getDefinition().getName().startsWith("REDI"))
				.map(l -> l.getSession().getDefinition().getName())
				.collect(Collectors.toList());
		MatcherAssert.assertThat(filteredIncomingName, IsEqual.equalTo(expectedIncomingName));
	}


	@Test
	public void testGetFixMessageEntryLastHeartbeatBySession_warningsOnly_false() {
		final FixSession gsSession = this.fixSessionService.getFixSession((short) 2);
		gsSession.getDefinition().setDisabled(false);
		final FixSession bbSession = this.fixSessionService.getFixSession((short) 4);
		bbSession.getDefinition().setDisabled(false);
		this.fixSessionService.saveFixSessionDefinition(gsSession.getDefinition());
		this.fixSessionService.saveFixSessionDefinition(bbSession.getDefinition());

		// Backdate heartbeat timestamps for testing
		final Date expectedDate = DateUtils.toDate("2021-01-31 13:08:00", DateUtils.DATE_FORMAT_FULL);
		int minutesOffset = 0;
		for (FixMessageEntry heartBeatMsg : CollectionUtils.getIterable(heartbeatLogMap.get("Bloomberg"))) {
			heartBeatMsg.setMessageEntryDateAndTime(DateUtils.addMinutes(expectedDate, minutesOffset));
			--minutesOffset;
		}

		final Date expectedDate2 = DateUtils.toDate("2220-11-13 13:08:00", DateUtils.DATE_FORMAT_FULL);
		FixMessageEntryHeartbeatStatusCommand command = new FixMessageEntryHeartbeatStatusCommand();
		command.setActiveSessionsOnly(false);
		command.setReturnWarningsOnly(false);
		command.setMaximumMinutesSinceLastHeartbeatWarn((short) 5);
		List<FixMessageEntryHeartbeatStatus> heartbeatStatusList = this.fixMessageEntryService.getFixMessageEntryLastHeartbeatBySession(command);
		Assertions.assertFalse(heartbeatStatusList.isEmpty());

		List<String> labelList = CollectionUtils.getConverted(heartbeatStatusList, FixMessageEntryHeartbeatStatus::getSessionDefinitionLabel);
		Assertions.assertTrue(labelList.contains("Bloomberg (Parametric Minneapolis Fixed Income)"));
		Assertions.assertTrue(labelList.contains("Goldman Sachs"));

		for (FixMessageEntryHeartbeatStatus heartbeatStatus : heartbeatStatusList) {
			switch (heartbeatStatus.getSessionDefinitionLabel()) {
				case "Bloomberg (Parametric Minneapolis Fixed Income)":
					checkHeartbeatStatus(heartbeatStatus, "Bloomberg (Parametric Minneapolis Fixed Income)", true, expectedDate, "Bloomberg (Parametric Minneapolis Fixed Income) did not have a heartbeat in the last 5  minutes. Last heartbeat was at [2021-01-31 13:08:00]");
					break;
				case "Goldman Sachs":
					checkHeartbeatStatus(heartbeatStatus, "Goldman Sachs", false, expectedDate2, "Goldman Sachs - last heartbeat:  [2220-11-13 13:08:00]");
					break;
				default:
					Assertions.fail("Unexpected session found in test: " + heartbeatStatus.getSessionDefinitionLabel());
			}
		}
	}


	@Test
	public void testGetFixMessageEntryLastHeartbeatBySession_warningsOnly_true() {
		final FixSession gsSession = this.fixSessionService.getFixSession((short) 2);
		gsSession.getDefinition().setDisabled(false);
		final FixSession bbSession = this.fixSessionService.getFixSession((short) 4);
		bbSession.getDefinition().setDisabled(false);

		// Backdate heartbeat timestamps for testing
		final Date expectedDate = DateUtils.toDate("2021-01-31 13:08:00", DateUtils.DATE_FORMAT_FULL);
		int minutesOffset = 0;
		for (FixMessageEntry heartBeatMsg : CollectionUtils.getIterable(heartbeatLogMap.get("Bloomberg"))) {
			heartBeatMsg.setMessageEntryDateAndTime(DateUtils.addMinutes(expectedDate, minutesOffset));
			--minutesOffset;
		}

		this.fixSessionService.saveFixSessionDefinition(gsSession.getDefinition());
		this.fixSessionService.saveFixSessionDefinition(bbSession.getDefinition());

		FixMessageEntryHeartbeatStatusCommand command = new FixMessageEntryHeartbeatStatusCommand();
		command.setActiveSessionsOnly(false);
		command.setReturnWarningsOnly(true);
		command.setMaximumMinutesSinceLastHeartbeatWarn((short) 5);
		List<FixMessageEntryHeartbeatStatus> heartbeatStatusList = this.fixMessageEntryService.getFixMessageEntryLastHeartbeatBySession(command);
		Assertions.assertEquals(1, heartbeatStatusList.size());
		checkHeartbeatStatus(heartbeatStatusList.get(0), "Bloomberg (Parametric Minneapolis Fixed Income)", true, expectedDate, "Bloomberg (Parametric Minneapolis Fixed Income) did not have a heartbeat in the last 5  minutes. Last heartbeat was at [2021-01-31 13:08:00]");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void checkHeartbeatStatus(FixMessageEntryHeartbeatStatus status, String expectedSessionDefinitionLabel, boolean expectedFailStatus, Date expectedLastHeartbeatDateAndTime, String expectedStatusMessage) {
		Assertions.assertNotNull(status);
		Assertions.assertEquals(expectedSessionDefinitionLabel, status.getSessionDefinitionLabel());
		Assertions.assertEquals(expectedFailStatus, status.isFailed());
		Assertions.assertTrue(DateUtils.isEqual(expectedLastHeartbeatDateAndTime, status.getLastHeartbeatDateAndTime()));
	}


	protected void setCurrentUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 0);
		user.setUserName("testUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	private SystemListItem generateTestConnectivityType() {
		return this.systemListService.getSystemListItemByListAndValue("FIX Connectivity Types", "N/A");
	}
}
