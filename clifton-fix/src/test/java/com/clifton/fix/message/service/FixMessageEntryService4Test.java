package com.clifton.fix.message.service;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Additional tests for the FixMessageEntryService.  Some tests here cannot be run within the
 * original FixMessageEntryService1Test due to that test suite setting up pre-configured heartbeat entries that
 * conflict with the newer tests.
 */

@FixInMemoryDatabaseContext
public class FixMessageEntryService4Test extends BaseInMemoryDatabaseTests {

	@Resource
	private FixMessageEntryService fixMessageEntryService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDeleteFixMessageEntryHistoricalList() {
		Date maximumEntryDate = DateUtils.toDate("10/20/2020");
		Short[] fixMessageEntryTypeIds = new Short[] {1,2};

		FixMessageEntrySearchForm expectedDeletedEntriesSearchForm = new FixMessageEntrySearchForm();
		expectedDeletedEntriesSearchForm.setTypeIds(fixMessageEntryTypeIds);
		expectedDeletedEntriesSearchForm.addSearchRestriction(new SearchRestriction("messageEntryDateAndTime", ComparisonConditions.LESS_THAN_OR_EQUALS, maximumEntryDate));
		int expectedDeleteEntriesCount= this.fixMessageEntryService.getFixMessageEntryList(expectedDeletedEntriesSearchForm).size();

		// entries that should not be deleted in specified date range.
		FixMessageEntrySearchForm expectedPreservedEntriesInRangeSearchForm = new FixMessageEntrySearchForm();
		expectedPreservedEntriesInRangeSearchForm.addSearchRestriction(new SearchRestriction("typeIds", ComparisonConditions.NOT_IN, fixMessageEntryTypeIds));
		expectedPreservedEntriesInRangeSearchForm.addSearchRestriction(new SearchRestriction("messageEntryDateAndTime", ComparisonConditions.LESS_THAN_OR_EQUALS, maximumEntryDate));
		List<FixMessageEntry> expectedPreservedEntryInRangeList = this.fixMessageEntryService.getFixMessageEntryList(expectedPreservedEntriesInRangeSearchForm);
		Assertions.assertFalse(expectedPreservedEntryInRangeList.isEmpty());

		//  entries that should not be deleted outside of date range
		FixMessageEntrySearchForm expectedPreservedEntriesOutOfRangeSearchForm = new FixMessageEntrySearchForm();
		expectedPreservedEntriesOutOfRangeSearchForm.addSearchRestriction(new SearchRestriction("messageEntryDateAndTime", ComparisonConditions.GREATER_THAN, maximumEntryDate));
		List<FixMessageEntry> expectedPreservedEntryOutOfRangeList = this.fixMessageEntryService.getFixMessageEntryList(expectedPreservedEntriesOutOfRangeSearchForm);
		Assertions.assertFalse(expectedPreservedEntryOutOfRangeList.isEmpty());


		// The method under test
		int deletedMessageEntriesCount = this.fixMessageEntryService.deleteFixMessageEntryHistoricalList(maximumEntryDate, fixMessageEntryTypeIds);
		Assertions.assertEquals(expectedDeleteEntriesCount, deletedMessageEntriesCount);


		// Verify preserved entries exist after operation
		// entries that should not be deleted in specified date range.
		FixMessageEntrySearchForm preservedEntriesInRangeSearchForm = new FixMessageEntrySearchForm();
		preservedEntriesInRangeSearchForm.addSearchRestriction(new SearchRestriction("typeIds", ComparisonConditions.NOT_IN, fixMessageEntryTypeIds));
		preservedEntriesInRangeSearchForm.addSearchRestriction(new SearchRestriction("messageEntryDateAndTime", ComparisonConditions.LESS_THAN_OR_EQUALS, maximumEntryDate));
		List<FixMessageEntry> preservedEntryInRangeList = this.fixMessageEntryService.getFixMessageEntryList(preservedEntriesInRangeSearchForm);
		Assertions.assertEquals(expectedPreservedEntryInRangeList, preservedEntryInRangeList);

		//  entries that should not be deleted outside of date range
		FixMessageEntrySearchForm preservedEntriesOutOfRangeSearchForm = new FixMessageEntrySearchForm();
		preservedEntriesOutOfRangeSearchForm.addSearchRestriction(new SearchRestriction("messageEntryDateAndTime", ComparisonConditions.GREATER_THAN, maximumEntryDate));
		List<FixMessageEntry> preservedEntryOutOfRangeList = this.fixMessageEntryService.getFixMessageEntryList(preservedEntriesOutOfRangeSearchForm);
		Assertions.assertEquals(expectedPreservedEntryOutOfRangeList, preservedEntryOutOfRangeList);
	}

}
