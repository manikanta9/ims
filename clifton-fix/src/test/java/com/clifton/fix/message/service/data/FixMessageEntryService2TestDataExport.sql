SELECT 'SystemListItem' AS "EntityTableName", SystemListItemID AS "EntityID"
FROM SystemListItem li
	 INNER JOIN SystemList l ON li.SystemListID = l.SystemListID
WHERE l.SystemListName = 'FIX Connectivity Types'
UNION
SELECT 'FixMessageEntryType' AS "EntityTableName", FixMessageEntryTypeID AS "EntityID"
FROM FixMessageEntryType
UNION
SELECT 'FixSession' AS "EntityTableName", FixSessionID AS "EntityID"
FROM FixSession
WHERE FixSessionDefinitionID NOT IN (5, 10);
