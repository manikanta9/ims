package com.clifton.fix.message.search;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * FixMessage search tests via the FixMessage service.
 *
 * @author davidi
 */
@FixInMemoryDatabaseContext
public class FixMessageServiceSearchTests extends BaseInMemoryDatabaseTests {

	@Resource
	FixMessageService fixMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFixMessageEntryServiceAvailable() {
		Assertions.assertNotNull(this.fixMessageService);
	}


	////////////////////////////////////////////////////////////////////////////
	//                         FixMessage Tests                               //
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSearchByMessageType_Type_D_using_getFixMessageList() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(249068, 249529);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeTagValue("D");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_Type_D() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(249068, 249529);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeTagValue("D");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_Type_J() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(250075, 251016);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeTagValue("J");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_Type_8() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251008, 251031);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeTagValue("8");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_Type_P() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251018, 251024);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeTagValue("P");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_Type_P_or_AS() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(250096, 251035, 251018, 251024);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeTagValueList(new String[]{"P", "AS"});

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_Type_UNKNOWN() {
		TestUtils.expectException(RuntimeException.class, () -> {
			FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
			searchForm.setMessageTypeTagValue("UNKNOWN");
			this.fixMessageService.getFixMessageList(searchForm);
		});
	}


	@Test
	public void testSearchByMessageType_By_TypeIdFieldTag_70() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(250075, 250096, 251016, 251018, 251024, 251035);

		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeIdFieldTag((short) 70);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_TypeIdFieldTag_11() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(249068, 249529, 250085, 251007, 251031, 251008);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeIdFieldTag((short) 11);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_TypeIdFieldTag_888() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setMessageTypeIdFieldTag((short) 888);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchByMessageType_By_EntryID() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251031);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setId((long) 251031);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_SessionID_8() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251035, 251031, 251024, 251018, 251016, 251008, 251007, 250096, 250085, 250075, 249529, 249068);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setSessionId((short) 8);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_SessionID_4() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251081, 251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setSessionId((short) 4);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_SessionDefinitionID() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251035, 251031, 251024, 251018, 251016, 251008, 251007, 250096, 250085, 250075, 249529, 249068);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setSessionDefinitionId((short) 18);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_SessionDefinitionName() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251081, 251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setSessionDefinitionName("Bloomberg EMSX");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_FixIdentifierID() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251024);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setIdentifierId(10001);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_FixIdentifierID_List() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251024, 251031, 251035);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setIdentifierIdList(new Integer[]{10001, 10002, 10003});

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_UniqueStringIdentifier() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251024);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setIdentifier("423724_20201019");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_UniqueStringIdentifier_List() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251024, 251035);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setIdentifierList(new String[]{"423724_20201019", "423726_20201019"});

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_UniqueStringIdentifier_Like() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251024);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setUniqueStringIdentifier("423724");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_MessageText() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setText("8=FIX.4.4\u00019=0082\u000135=1\u000149=BLP\u000156=CLFTPARAEMSX\u000134=3\u000152=20201020-04:05:01\u0001112=A.0003.0003.0003.040501\u000110=002\u0001");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_MessageText_Unknown() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setText("Unknown");

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchBy_IsIncoming_True() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(250096, 251008, 251018, 251024, 251031, 251035, 251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setIncoming(true);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchBy_IsIncoming_False() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251081, 251016, 251007, 250085, 250075, 249529, 249068);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setIncoming(false);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_TypeId() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(250075, 251016);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeId((short) 9);

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByMessageType_By_TypeIds() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251008, 251031, 250075, 251016);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeIds(new Short[]{9, 10});

		List<FixMessage> searchResults = this.fixMessageService.getFixMessageList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testGetFixMessage() {
		long fixMessageId = 251008;
		FixMessage fixMessage = this.fixMessageService.getFixMessage(fixMessageId);
		Assertions.assertNotNull(fixMessage);
		Assertions.assertEquals(fixMessageId, fixMessage.getId());
	}


	@Test
	public void testGetFixMessage_ID_NOT_EXIST() {
		long fixMessageId = 555;
		FixMessage fixMessage = this.fixMessageService.getFixMessage(fixMessageId);
		Assertions.assertNull(fixMessage);
	}


	@Test
	public void testGetFixMessage_ID_NOT_FOR_MESSAGE() {
		long fixMessageId = 248467;  // This is a log entry but not a message
		FixMessage fixMessage = this.fixMessageService.getFixMessage(fixMessageId);
		Assertions.assertNull(fixMessage);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertResultListContainsExpectedIds(List<Integer> expectedIds, List<FixMessage> entriesToVerifyList) {
		Set<Long> idsToVerifySet = new HashSet<>();
		Set<Long> expectedIdsLong = new HashSet<>(convertToLong(expectedIds));
		for (FixMessage message : entriesToVerifyList) {
			idsToVerifySet.add(message.getId());
		}

		Assertions.assertEquals(expectedIdsLong, idsToVerifySet);
	}


	private List<Long> convertToLong(List<Integer> integerList) {
		List<Long> listOfLong = new ArrayList<>();
		for (Integer intNumber : integerList) {
			listOfLong.add(intNumber.longValue());
		}
		return listOfLong;
	}
}
