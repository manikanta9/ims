package com.clifton.fix.message.service;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.RollingList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryBuilder;
import com.clifton.fix.message.FixMessageEntryHeartbeatStatus;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.FixMessageService;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixTestObjectFactory;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionBuilder;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.session.FixSessionDefinitionBuilder;
import com.clifton.fix.session.FixSessionService;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * This test is the save as the FixMessageEntryService1Test, but exercises the FixMessageService's query methods for Heartbeat messages.
 *
 * @author davidi
 */
@FixInMemoryDatabaseContext
public class FixMessageServiceTest extends BaseInMemoryDatabaseTests {

	private static final Function<List<FixMessageEntry>, RollingList<FixMessageEntry>> RollingListSupplier = fixMessageEntry -> {
		RollingList<FixMessageEntry> rollingList = new RollingList<>(250);
		rollingList.addAll(fixMessageEntry);
		return rollingList;
	};

	private static final Map<String, RollingList<FixMessageEntry>> heartbeatLogMap = new ConcurrentHashMap<>();


	@Resource
	private ContextHandler contextHandler;

	@Resource
	private FixMessageEntryService fixMessageEntryService;

	@Resource
	private FixMessageService fixMessageService;

	@Resource
	private FixSessionService fixSessionService;

	@Resource
	private SystemListService<SystemList> systemListService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void beforeClass() {
		heartbeatLogMap.put("Goldman Sachs", RollingListSupplier.apply(
						Arrays.asList(
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 2, FixSessionDefinitionBuilder.createOther("Goldman Sachs", "CLIFTON", "GSFUT").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry()
						)
				)
		);
		heartbeatLogMap.put("CITI CARE", RollingListSupplier.apply(
						Arrays.asList(
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 8, FixSessionDefinitionBuilder.createOther("CITI CARE", "PPAMPLSU", "CITICFOXU").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry()
						)
				)
		);
		heartbeatLogMap.put("REDI Multi-Broker", RollingListSupplier.apply(
						Arrays.asList(
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 7, FixSessionDefinitionBuilder.createOther("REDI Multi-Broker", "CLIFTONMB44", "REDITKTS").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry()
						)
				)
		);
		heartbeatLogMap.put("Bloomberg", RollingListSupplier.apply(
						Arrays.asList(
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), false).toFixMessageEntry(),
								FixMessageEntryBuilder.createHeartbeat(FixSessionBuilder.createDefined((short) 4, FixSessionDefinitionBuilder.createOther("Bloomberg", "CLIFTONPARA", "BLP").toFixSessionDefinition()).toFixSession(), true).toFixMessageEntry()
						)
				)
		);

		heartbeatLogMap.get("Goldman Sachs").get(2).setMessageEntryDateAndTime(DateUtils.toDate("2220-11-13 13:08:00", DateUtils.DATE_FORMAT_FULL));
		heartbeatLogMap.get("Bloomberg").get(2).setMessageEntryDateAndTime(DateUtils.toDate("2220-11-13 13:08:00", DateUtils.DATE_FORMAT_FULL));
	}


	@BeforeEach
	public void before() {
		setCurrentUser();
		ReflectionTestUtils.setField(this.fixMessageEntryService, "heartbeatLogMap", heartbeatLogMap);
	}


	@AfterEach
	public void after() {
		// MUST CLEAR THIS OR ELSE IT AFFECTS OTHER TESTS (FixLogEntrySearchFormTests)
		ReflectionTestUtils.setField(this.fixMessageEntryService, "heartbeatLogMap", new HashMap<>());
	}


	@Test
	public void testHeartbeatIncomingFilter() {
		// build search form
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);
		searchForm.setRestrictionList(Collections.singletonList(
				new SearchRestriction("incoming", ComparisonConditions.EQUALS, Boolean.TRUE)
		));
		// do search
		List<FixMessage> filteredByIncoming = this.fixMessageService.getFixMessageList(searchForm);
		List<Boolean> filteredIncoming = filteredByIncoming.stream().map(FixMessage::getIncoming).collect(Collectors.toList());
		List<Boolean> expectedIncoming = heartbeatLogMap.values()
				.stream()
				.flatMap(List::stream)
				.map(FixMessageEntry::getIncoming)
				.filter(Boolean.TRUE::equals)
				.collect(Collectors.toList());
		MatcherAssert.assertThat(filteredIncoming, IsEqual.equalTo(expectedIncoming));
	}


	@Test
	public void testGetFixSessionDefinitionByParameters() {
		//Incoming ER
		FixExecutionReport executionReport = FixTestObjectFactory.createExecutionReportBloomberg();
		executionReport.setIncoming(true);
		FixSessionDefinition fixSessionDefinitionBBG = this.fixSessionService.getFixSessionDefinitionByParameters(executionReport);
		assert fixSessionDefinitionBBG.getName().startsWith("Bloomberg");
		MatcherAssert.assertThat(fixSessionDefinitionBBG.getFixTargetCompId(), IsEqual.equalTo(executionReport.getFixSenderCompId()));
		MatcherAssert.assertThat(fixSessionDefinitionBBG.getFixSenderCompId(), IsEqual.equalTo(executionReport.getFixTargetCompId()));
		MatcherAssert.assertThat(fixSessionDefinitionBBG.getBeginString(), IsEqual.equalTo(executionReport.getFixBeginString()));
	}


	@Test
	public void testGetFixSessionDefinitionByParameters2() {
		//Outgoing NOS
		FixOrder newOrder = FixTestObjectFactory.createNewOrder();
		FixSessionDefinition fixSessionDefinitionGS = this.fixSessionService.getFixSessionDefinitionByParameters(newOrder);
		assert fixSessionDefinitionGS.getName().startsWith("Goldman");
		MatcherAssert.assertThat(fixSessionDefinitionGS.getFixSenderCompId(), IsEqual.equalTo(newOrder.getFixSenderCompId()));
		MatcherAssert.assertThat(fixSessionDefinitionGS.getFixTargetCompId(), IsEqual.equalTo(newOrder.getFixTargetCompId()));
		MatcherAssert.assertThat(fixSessionDefinitionGS.getBeginString(), IsEqual.equalTo(newOrder.getFixBeginString()));
	}


	@Test
	public void testGetFixSessionDefinitionByParameters3() {
		//Incoming ExecReport testing Only Required Params in FIxSessionService
		FixExecutionReport executionReport = FixTestObjectFactory.createDontKnowTrade();
		executionReport.setIncoming(true);
		FixSessionDefinition fixSessionDefinitionGS = this.fixSessionService.getFixSessionDefinitionByParameters(executionReport);
		MatcherAssert.assertThat(fixSessionDefinitionGS.getFixSenderCompId(), IsEqual.equalTo(executionReport.getFixTargetCompId()));
		MatcherAssert.assertThat(fixSessionDefinitionGS.getFixTargetCompId(), IsEqual.equalTo(executionReport.getFixSenderCompId()));
		MatcherAssert.assertThat(fixSessionDefinitionGS.getBeginString(), IsEqual.equalTo(executionReport.getFixBeginString()));
	}


	@Test
	public void testHeartbeatSessionNameFilter() {
		// save default session definition
		FixSessionDefinition rediFixSessionDefinition = heartbeatLogMap.get("REDI Multi-Broker").get(0).getSession().getDefinition();
		FixSessionDefinition defaultSessionDefinition = rediFixSessionDefinition.getParent();
		defaultSessionDefinition.setId(null);
		this.fixSessionService.saveFixSessionDefinition(defaultSessionDefinition);

		// save redi session definition
		rediFixSessionDefinition.setId(null);
		rediFixSessionDefinition.setParent(defaultSessionDefinition);
		rediFixSessionDefinition.setConnectivityType(generateTestConnectivityType());
		this.fixSessionService.saveFixSessionDefinition(rediFixSessionDefinition);
		// save redi session
		FixSession rediFixSession = heartbeatLogMap.get("REDI Multi-Broker").get(0).getSession();
		rediFixSession.setId(null);
		this.fixSessionService.saveFixSession(rediFixSession);
		// set the id for redi session and definition log entry values.
		// 'path' searches actually search the database and match based on primary key.
		heartbeatLogMap.get("REDI Multi-Broker").forEach(l -> {
			l.getSession().setId(rediFixSession.getId());
			l.getSession().getDefinition().setId(rediFixSessionDefinition.getId());
		});
		// build search form like UI does
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);
		searchForm.setRestrictionList(Collections.singletonList(
				new SearchRestriction("sessionDefinitionName", ComparisonConditions.LIKE, "REDI")
		));
		// perform search.
		List<FixMessage> filteredByIncomingName = this.fixMessageService.getFixMessageList(searchForm);
		List<String> filteredIncomingName = filteredByIncomingName.stream().map(l -> l.getSession().getDefinition().getName()).collect(Collectors.toList());
		List<String> expectedIncomingName = heartbeatLogMap.values()
				.stream()
				.flatMap(List::stream)
				.filter(l -> l.getSession().getDefinition().getName().startsWith("REDI"))
				.map(l -> l.getSession().getDefinition().getName())
				.collect(Collectors.toList());
		MatcherAssert.assertThat(filteredIncomingName, IsEqual.equalTo(expectedIncomingName));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void checkHeartbeatStatus(FixMessageEntryHeartbeatStatus status, String expectedSessionDefinitionLabel, boolean expectedFailStatus, Date expectedLastHeartbeatDateAndTime, String expectedStatusMessage) {
		Assertions.assertNotNull(status);
		Assertions.assertEquals(expectedSessionDefinitionLabel, status.getSessionDefinitionLabel());
		Assertions.assertEquals(expectedFailStatus, status.isFailed());
		Assertions.assertTrue(DateUtils.isEqual(expectedLastHeartbeatDateAndTime, status.getLastHeartbeatDateAndTime()));
	}


	protected void setCurrentUser() {
		SecurityUser user = new SecurityUser();
		user.setId((short) 0);
		user.setUserName("testUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	private SystemListItem generateTestConnectivityType() {
		return this.systemListService.getSystemListItemByListAndValue("FIX Connectivity Types", "N/A");
	}
}
