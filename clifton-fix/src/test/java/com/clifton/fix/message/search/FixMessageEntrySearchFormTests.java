package com.clifton.fix.message.search;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.FixMessageEntry;
import com.clifton.fix.message.FixMessageEntryService;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * FixLogEntry (the subset FixMessageEntries that are administrative messages or non-messages) search tests via the FixLogEntryService.
 *
 * @author davidi
 */
@FixInMemoryDatabaseContext
public class FixMessageEntrySearchFormTests extends BaseInMemoryDatabaseTests {

	@Resource
	FixMessageEntryService fixMessageEntryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFixMessageEntryServiceAvailable() {
		Assertions.assertNotNull(this.fixMessageEntryService);
	}


	@Test
	public void testSearchBy_EntryID() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251035);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setId((long) 251035);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_IsMessage_False() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251095, 251087, 251078, 251040, 248467);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_IsMessage_True() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(249068, 249529, 250075, 250085, 250096, 251007, 251008, 251016, 251018, 251024, 251031,
				251035, 251081, 251088, 260000, 260001);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Administrative_False() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(249068, 249529, 250075, 250085, 250096, 251007, 251008, 251016, 251018, 251024, 251031,
				251035, 260000, 260001);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_ADMINISTRATIVE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Administrative_True() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251095, 251088, 251087, 251081, 251078, 251040, 248467);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.ADMINISTRATIVE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_True_Administrative_True() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251081, 251088);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_ADMINISTRATIVE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_True_Administrative_False() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(249068, 249529, 250075, 250085, 250096, 251007, 251008, 251016, 251018, 251024, 251031, 251035, 260000, 260001);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.MESSAGE_AND_NOT_ADMINISTRATIVE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_False_Administrative_True() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(248467, 251040, 251078, 251087, 251095);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_ADMINISTRATIVE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryType_Message_False_Administrative_False() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds.NOT_MESSAGE_AND_NOT_ADMINISTRATIVE);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchByFixLogEntryTypeName_Error_Event() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(248467, 251040);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeName("Error Event");

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryTypeName_Message() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(251088, 251081, 260000, 260001);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeName("Message");

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchByFixLogEntryTypeNameEquals_Message() {
		List<Integer> expectedMessageIDList = CollectionUtils.createList(260000, 260001);
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setTypeNameEquals("Message");

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		assertResultListContainsExpectedIds(expectedMessageIDList, searchResults);
	}


	@Test
	public void testSearchBy_heartbeatMessages_True() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(true);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		Assertions.assertEquals(0, searchResults.size());
	}


	@Test
	public void testSearchBy_heartbeatMessages_False() {
		FixMessageEntrySearchForm searchForm = new FixMessageEntrySearchForm();
		searchForm.setHeartbeatMessages(false);

		List<FixMessageEntry> searchResults = this.fixMessageEntryService.getFixMessageEntryList(searchForm);
		Assertions.assertEquals(21, searchResults.size());
	}


	@Test
	public void testGetFixLogEntry() {
		long fixLogEntryId = 251040;
		FixMessageEntry fixMessageEntry = this.fixMessageEntryService.getFixMessageEntry(fixLogEntryId);
		Assertions.assertNotNull(fixMessageEntry);
		Assertions.assertEquals(fixLogEntryId, fixMessageEntry.getId());
	}


	@Test
	public void testGetFixMessage_ID_NOT_EXIST() {
		long fixLogEntryId = 555;
		FixMessageEntry fixMessageEntry = this.fixMessageEntryService.getFixMessageEntry(fixLogEntryId);
		Assertions.assertNull(fixMessageEntry);
	}


	@Test
	public void testGetFixMessage_ID_IS_FOR_MESSAGE() {
		long fixLogEntryId = 249068; // This is a message entry instead of a log entry
		FixMessageEntry fixMessageEntry = this.fixMessageEntryService.getFixMessageEntry(fixLogEntryId);
		Assertions.assertEquals(fixMessageEntry.getId(), fixLogEntryId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertResultListContainsExpectedIds(List<Integer> expectedIds, List<?> entriesToVerifyList) {
		//Assertions.assertEquals(expectedIds.size(), entriesToVerifyList.size());

		Set<Long> idsToVerifySet = new HashSet<>();
		Set<Long> expectedIdsLong = new HashSet<>(convertToLong(expectedIds));
		for (Object identityObject : entriesToVerifyList) {
			idsToVerifySet.add((Long) ((IdentityObject) identityObject).getIdentity());
		}

		Assertions.assertEquals(expectedIdsLong, idsToVerifySet);
	}


	private List<Long> convertToLong(List<Integer> integerList) {
		List<Long> listOfLong = new ArrayList<>();
		for (Integer intNumber : integerList) {
			listOfLong.add(intNumber.longValue());
		}
		return listOfLong;
	}
}
