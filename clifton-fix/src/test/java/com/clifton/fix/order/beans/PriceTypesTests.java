package com.clifton.fix.order.beans;

import com.clifton.fix.order.FixExecutionReport;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author mwacker
 */
public class PriceTypesTests {

	@Test
	public void testSpreadPrice() {

		FixExecutionReport report = new FixExecutionReport();
		report.setPriceType(PriceTypes.SPREAD);
		report.setSide(OrderSides.SELL);
		report.setCumulativeQuantity(new BigDecimal("2500000"));
		report.setGrossTradeAmount(new BigDecimal("124636.2"));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("95.014552"), report.getPriceType().calculateExecutionPrice(report)));


		report = new FixExecutionReport();
		report.setPriceType(PriceTypes.SPREAD);
		report.setSide(OrderSides.BUY);
		report.setCumulativeQuantity(new BigDecimal("2500000"));
		report.setGrossTradeAmount(new BigDecimal("-3551.18"));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("99.8579528"), report.getPriceType().calculateExecutionPrice(report)));


		report = new FixExecutionReport();
		report.setPriceType(PriceTypes.SPREAD);
		report.setSide(OrderSides.BUY);
		report.setCumulativeQuantity(new BigDecimal("2150000"));
		report.setGrossTradeAmount(new BigDecimal("95223.83"));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("104.4290153"), report.getPriceType().calculateExecutionPrice(report).setScale(7, BigDecimal.ROUND_HALF_UP)));


		report = new FixExecutionReport();
		report.setPriceType(PriceTypes.SPREAD);
		report.setSide(OrderSides.BUY);
		report.setCumulativeQuantity(new BigDecimal("2150000"));
		report.setAveragePrice(new BigDecimal("10"));

		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("10"), report.getPriceType().calculateExecutionPrice(report)));
	}
}
