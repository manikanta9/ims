package com.clifton.fix.store;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.fix.FixInMemoryDatabaseContext;
import com.clifton.fix.message.store.FixMessageStore;
import com.clifton.fix.message.store.FixMessageStoreService;
import com.clifton.fix.message.store.search.FixMessageStoreSearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * A set of tests to test the functionality of the FixMessageStoreService methods.
 *
 * @author davidi
 */

@FixInMemoryDatabaseContext
public class FixMessageStoreServiceTests extends BaseInMemoryDatabaseTests {

	@Resource
	FixMessageStoreService fixMessageStoreService;

	@Resource
	FixSessionService fixSessionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetFixMessageStore() {
		final int expectedId = 36887952;
		final short expectedSessionId = (short) 15;
		final int expectedSequenceNumber = 4;

		final FixMessageStore messageStore = this.fixMessageStoreService.getFixMessageStore(expectedId);

		Assertions.assertEquals(expectedId, messageStore.getId());
		Assertions.assertEquals(expectedSessionId, messageStore.getSession().getId());
		Assertions.assertEquals(expectedSequenceNumber, messageStore.getSequenceNumber());
	}


	@Test
	public void testSaveFixMessageStore() {
		final FixMessageStore messageStore = new FixMessageStore();
		messageStore.setSession(this.fixSessionService.getFixSession((short) 15));
		messageStore.setSequenceNumber(34);
		messageStore.setText("8=FIX.4.2\u00019=64\u000135=0\u000134=34\u000149=PARAMETRICUAT\u000152=20201123-20:23:12.721\u000156=INCAUAT\u000110=204\u0001");
		boolean isSaved = this.fixMessageStoreService.saveFixMessageStore(messageStore);

		Assertions.assertTrue(isSaved);

		final FixMessageStoreSearchForm searchForm = new FixMessageStoreSearchForm();
		searchForm.setSessionId(messageStore.getSession().getId());
		searchForm.setSequenceNumber(34);
		final List<FixMessageStore> retrievedMessageList = this.fixMessageStoreService.getFixMessageStoreList(searchForm);

		Assertions.assertEquals(1, retrievedMessageList.size());
		Assertions.assertEquals(messageStore.getSession(), retrievedMessageList.get(0).getSession());
		Assertions.assertEquals(34, retrievedMessageList.get(0).getSequenceNumber());
		Assertions.assertEquals(messageStore.getText(), retrievedMessageList.get(0).getText());
	}


	@Test
	public void testDeleteFixMessageStoreForSession() {
		final FixSession session8 = this.fixSessionService.getFixSession((short) 8);

		final List<FixMessageStore> messageStoreSession8List = getMessageStoreListBySession((short) 8);
		final List<FixMessageStore> messageStoreSession15List = getMessageStoreListBySession((short) 15);
		Assertions.assertFalse(messageStoreSession8List.isEmpty());
		Assertions.assertFalse(messageStoreSession15List.isEmpty());

		this.fixMessageStoreService.deleteFixMessageStoreForSession(session8);

		final List<FixMessageStore> messageStoreSession8ListAfter = getMessageStoreListBySession((short) 8);
		final List<FixMessageStore> messageStoreSession15ListAfter = getMessageStoreListBySession((short) 15);
		Assertions.assertTrue(messageStoreSession8ListAfter.isEmpty());
		Assertions.assertFalse(messageStoreSession15ListAfter.isEmpty());
	}


	@Test
	public void testGetFixMessageStoreList_By_SequenceNumber() {
		final FixMessageStoreSearchForm searchForm = new FixMessageStoreSearchForm();
		searchForm.setSessionId((short) 15);
		searchForm.setSequenceNumber(30);
		final List<FixMessageStore> messageStoreResultList = this.fixMessageStoreService.getFixMessageStoreList(searchForm);
		Assertions.assertEquals(1, messageStoreResultList.size());
		Assertions.assertEquals(30, messageStoreResultList.get(0).getSequenceNumber());
	}


	@Test
	public void testGetFixMessageStoreList_By_SequenceNumber_Range() {
		final FixMessageStoreSearchForm searchForm = new FixMessageStoreSearchForm();
		searchForm.setSessionId((short) 15);
		searchForm.setStartSequenceNumber(25);
		searchForm.setEndSequenceNumber(28);

		final List<FixMessageStore> messageStoreResultList = this.fixMessageStoreService.getFixMessageStoreList(searchForm);

		Assertions.assertEquals(4, messageStoreResultList.size());
		Assertions.assertEquals(25, messageStoreResultList.get(0).getSequenceNumber());
		Assertions.assertEquals(26, messageStoreResultList.get(1).getSequenceNumber());
		Assertions.assertEquals(27, messageStoreResultList.get(2).getSequenceNumber());
		Assertions.assertEquals(28, messageStoreResultList.get(3).getSequenceNumber());
	}


	@Test
	public void testGetFixMessageStoreList_By_Text() {
		final FixMessageStoreSearchForm searchForm = new FixMessageStoreSearchForm();
		searchForm.setText("8=FIX.4.2\u00019=64\u000135=0\u000134=20\u000149=PARAMETRICUAT\u000152=20201123-20:15:41.617\u000156=INCAUAT\u000110=210\u0001");
		final List<FixMessageStore> messageStoreResultList = this.fixMessageStoreService.getFixMessageStoreList(searchForm);
		Assertions.assertEquals(1, messageStoreResultList.size());
		Assertions.assertEquals(20, messageStoreResultList.get(0).getSequenceNumber());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<FixMessageStore> getMessageStoreListBySession(short sessionId) {
		FixMessageStoreSearchForm searchForm = new FixMessageStoreSearchForm();
		searchForm.setSessionId(sessionId);
		return this.fixMessageStoreService.getFixMessageStoreList(searchForm);
	}
}
