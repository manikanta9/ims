package com.clifton.fix.destination;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.fix.session.FixSessionDefinition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class FixDestinationServiceImplTests {

	@Resource
	FixDestinationService fixDestinationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testUserIgnoredValidationNewDestination() {
		FixDestination destination = new FixDestination();
		destination.setName("New Destination");

		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 1);
		destination.setSessionDefinition(definition);

		this.fixDestinationService.saveFixDestination(destination, false);
	}


	@Test
	public void testUserIgnoredValidationNameNotChanged() {
		FixDestination destination = new FixDestination();
		destination.setId((short) 1);
		destination.setName("DefinitionOneNotDefault");

		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 1);
		destination.setSessionDefinition(definition);

		this.fixDestinationService.saveFixDestination(destination, false);
	}


	@Test
	public void testUserIgnoredValidationNameChanged() {
		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			FixDestination destination = new FixDestination();
			destination.setId((short) 1);
			destination.setName("New Name");

			this.fixDestinationService.saveFixDestination(destination, false);
		}, "Are you sure you want to update the FIX Destination name? Any clients using the old name will break until they're updated.");
	}


	@Test
	public void testUserIgnoredValidationDelete() {
		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			this.fixDestinationService.deleteFixDestination((short) 1, false);
		}, "Are you sure you want to delete this FIX Destination? Any clients using this destination will break until they're updated.");
	}


	@Test
	public void testUserIgnoredValidationNotIgnoredCreateNewDefault() {
		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 3);
		definition.setName("SessionDefinitionThree");
		FixDestination newDestination = new FixDestination();
		newDestination.setSessionDefinition(definition);
		newDestination.setDefaultDestination(true);
		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			this.fixDestinationService.saveFixDestination(newDestination, false);
		}, "Are you sure you want to set this FIX Destination as the default destination for SessionDefinitionThree? The destination DefinitionThreeDefault will be removed as the default destination for this definition.");
	}


	@Test
	public void testUserIgnoredValidationIgnoredCreateNewDefault() {
		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 3);
		definition.setName("SessionDefinitionThree");
		FixDestination newDestination = new FixDestination();
		newDestination.setSessionDefinition(definition);
		newDestination.setDefaultDestination(true);

		this.fixDestinationService.saveFixDestination(newDestination, true);
		FixDestination oldDefaultDestination = this.fixDestinationService.getFixDestination((short) 4);
		Assertions.assertFalse(oldDefaultDestination.isDefaultDestination());
	}


	@Test
	public void testUserIgnoredValidationNotIgnoredModifyDefault() {
		FixDestination newDestination = this.fixDestinationService.getFixDestination((short) 6);
		newDestination.setDefaultDestination(true);

		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			this.fixDestinationService.saveFixDestination(newDestination, false);
		}, "Are you sure you want to set this FIX Destination as the default destination for SessionDefinitionFour? The destination DefinitionFourDefault will be removed as the default destination for this definition.");
	}


	@Test
	public void testUserIgnoredValidationNotIgnoredNameChangedAndModifyDefault() {
		FixDestination newDestination = this.fixDestinationService.getFixDestination((short) 6);
		newDestination.setName("New Name");
		newDestination.setDefaultDestination(true);

		TestUtils.expectException(UserIgnorableValidationException.class, () -> {
			this.fixDestinationService.saveFixDestination(newDestination, false);
		}, "Are you sure you want to set this FIX Destination as the default destination for SessionDefinitionFour? The destination DefinitionFourDefault will be removed as the default destination for this definition.<br/><br/>Are you sure you want to update the FIX Destination name? Any clients using the old name will break until they're updated.");
	}


	@Test
	public void testUserIgnoredValidationIgnoredModifyDefault() {
		FixDestination newDestination = this.fixDestinationService.getFixDestination((short) 6);
		newDestination.setDefaultDestination(true);

		this.fixDestinationService.saveFixDestination(newDestination, true);
		FixDestination oldDefaultDestination = this.fixDestinationService.getFixDestination((short) 5);
		Assertions.assertFalse(oldDefaultDestination.isDefaultDestination());
	}


	@Test
	public void testGetDestinationByDefinitionDefaultExists() {
		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 1);
		FixDestination destination = this.fixDestinationService.getFixDestinationBySessionDefinition(definition.getId());
		Assertions.assertEquals("DefinitionOneDefault", destination.getName());
	}


	@Test
	public void testGetDestinationByDefinitionNoDefaultSpecified() {
		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 2);
		FixDestination destination = this.fixDestinationService.getFixDestinationBySessionDefinition(definition.getId());
		Assertions.assertNull(destination);
	}


	@Test
	public void testGetDestinationByDefinitionDefinitionNotFound() {
		FixSessionDefinition definition = new FixSessionDefinition();
		definition.setId((short) 5);
		FixDestination destination = this.fixDestinationService.getFixDestinationBySessionDefinition(definition.getId());
		Assertions.assertNull(destination);
	}
}
