package com.clifton.fix.session;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.validation.ValidationUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class FixSessionSequenceNumberHandlerTests {

	@Resource
	private FixSessionService fixSessionService;

	@Resource
	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIncrementSequenceNumbers() {
		FixSession session = setupFixSession((short) 1);
		FixSession session2 = setupFixSession((short) 2);

		// Live Values not set
		validateSequenceNumbers(session, true, null, 1);
		validateSequenceNumbers(session, false, null, 1);

		validateSequenceNumbers(session2, true, null, 1);
		validateSequenceNumbers(session2, false, null, 1);


		// Increment Incoming Sequence Number to 25
		this.fixSessionService.storeFixSessionSequenceNumbers(session, 25, 5);

		// Confirm Runner is scheduled for session 1, but not session 2
		AssertUtils.assertNotEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session.getId() + ""), "Expected runner to be scheduled for the sequence number update.");
		AssertUtils.assertEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session2.getId() + ""), "Expected runner to not be running for session 2 with no updates.");

		// Live Values Updated - Saved Values Not updated
		validateSequenceNumbers(session, true, 25, 1);
		validateSequenceNumbers(session, false, 5, 1);


		// Increment to over the 50 batch size for session 1, but not session 2
		this.fixSessionService.storeFixSessionSequenceNumbers(session, 26, 55);
		this.fixSessionService.storeFixSessionSequenceNumbers(session2, 23, 33);

		// Live & Saved Values Updated for session 1
		validateSequenceNumbers(session, true, 26, 26);
		validateSequenceNumbers(session, false, 55, 55);

		// Live Values Only Updated - Saved Values Not updated for session 2
		validateSequenceNumbers(session2, true, 23, 1);
		validateSequenceNumbers(session2, false, 33, 1);

		// Confirm Runner is no longer scheduled for session 1, but scheduled for session 2
		AssertUtils.assertEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session.getId() + ""), "Expected runner to be canceled after sequence # update.");
		AssertUtils.assertNotEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session2.getId() + ""), "Expected runner to be scheduled for the sequence number update.");

		// Reset Sequence Numbers
		this.fixSessionService.storeFixSessionSequenceNumbers(session, 1, 1);

		validateSequenceNumbers(session, true, 1, 1);
		validateSequenceNumbers(session, false, 1, 1);

		// Confirm Runner is no longer scheduled
		AssertUtils.assertEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session.getId() + ""), "Expected runner to be canceled after sequence # update.");


		// Update sequence #'s
		this.fixSessionService.storeFixSessionSequenceNumbers(session, 3, 5);

		validateSequenceNumbers(session, true, 3, 1);
		validateSequenceNumbers(session, false, 5, 1);


		// Confirm Runner is scheduled
		AssertUtils.assertNotEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session.getId() + ""), "Expected runner to be scheduled for the sequence number update.");

		// Manually trigger a save....
		this.fixSessionService.saveFixSession(session);

		// Confirm Runner is canceled
		AssertUtils.assertEmpty(this.runnerHandler.getScheduledRunnerList("FIX-SESSION-SEQUENCE-NUMBERS", session.getId() + ""), "Expected runner to be canceled after session save was triggered.");

		validateSequenceNumbers(session, true, 3, 3);
		validateSequenceNumbers(session, false, 5, 5);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixSession setupFixSession(short definitionId) {
		FixSession session = new FixSession();
		session.setDefinition(this.fixSessionService.getFixSessionDefinition(definitionId));
		session.setCreationDateAndTime(Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime());
		session.setIncomingSequenceNumber(1);
		session.setOutgoingSequenceNumber(1);
		session.setActiveSession(true);
		return this.fixSessionService.saveFixSession(session);
	}


	private void validateSequenceNumbers(FixSession session, boolean incoming, Integer expectedLiveValue, int expectedSavedValue) {
		session = this.fixSessionService.getFixSessionPopulated(session.getId());
		Integer liveValue = (incoming ? session.getLiveIncomingSequenceNumber() : session.getLiveOutgoingSequenceNumber());
		ValidationUtils.assertEquals(expectedLiveValue, liveValue, "Expected Live " + (incoming ? "incoming" : "outgoing") + " Sequence Number to be " + expectedLiveValue + " but instead was " + liveValue);

		Integer savedValue = (incoming ? session.getIncomingSequenceNumber() : session.getOutgoingSequenceNumber());
		ValidationUtils.assertEquals(expectedSavedValue, savedValue, "Expected Saved " + (incoming ? "incoming" : "outgoing") + " Sequence Number to be " + expectedSavedValue + " but instead was " + savedValue);
	}
}
