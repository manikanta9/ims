package com.clifton.fix.messaging.configurer;


import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;


public class FixMessagingMessageConfigurerDelayedTests {

	private FixMessagingMessageConfigurerDelayed configurer;


	@BeforeEach
	public void setup() {
		this.configurer = new FixMessagingMessageConfigurerDelayed();
		this.configurer.setDelay(1000);
		this.configurer.setMessageClassList(new ArrayList<>());
		this.configurer.getMessageClassList().add(FixAllocationInstruction.class);
	}


	@Test
	public void testBaseFixMessagingMessageConfigurerDelayed() {
		FixAllocationInstruction ai = new FixAllocationInstruction();
		FixMessageTextMessagingMessage msg = new FixMessageTextMessagingMessage();

		Assertions.assertTrue(this.configurer.applyTo(ai));
		this.configurer.configure(msg);
		Assertions.assertEquals((Integer) 1000, msg.getMessageDelaySeconds());

		ai.setReferenceAllocationStringId("hello");
		Assertions.assertFalse(this.configurer.applyTo(ai));
		Assertions.assertFalse(this.configurer.applyTo(new FixExecutionReport()));
	}
}
