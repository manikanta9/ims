package com.clifton.fix.messaging;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.core.messaging.converter.xml.StandardMessagingXStreamConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.messaging.xml.FixMessagingXStreamConfigurer;
import com.thoughtworks.xstream.XStream;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.InputStream;
import java.util.List;


public class XmlMessagingTestHelper {

	private static final List<MessagingXStreamConfigurer> xStreamConfigurerList = CollectionUtils.createList(
			new StandardMessagingXStreamConfigurer(),
			new FixMessagingXStreamConfigurer()
	);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	public static <T> T loadFromFile(String fileName) {
		ResourceLoader loader = new DefaultResourceLoader();
		return (T) loadFromXml(loader.getResource(fileName));
	}


	public static <T> T loadFromXml(String xml) {
		XStream xStream = generateXStream();
		@SuppressWarnings("unchecked")
		T ojb = (T) xStream.fromXML(xml);
		return ojb;
	}


	public static String toXml(Object obj) {
		XStream xStream = generateXStream();
		return xStream.toXML(obj);
	}


	@SuppressWarnings("unchecked")
	public static <T> T loadFromXml(Resource resource) {
		InputStream in = null;
		T schema;
		try {
			XStream xStream = generateXStream();
			in = resource.getInputStream();
			schema = (T) xStream.fromXML(in);
		}
		catch (Exception e) {
			throw new RuntimeException("Error loading xml from file: " + resource.getFilename(), e);
		}
		finally {
			FileUtils.close(in);
		}
		return schema;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static XStream generateXStream() {
		XStream xStream = new XStream();
		for (MessagingXStreamConfigurer configurer : xStreamConfigurerList) {
			configurer.configure(xStream);
		}
		return xStream;
	}
}
