package com.clifton.fix.messaging;


import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.FixMessageTextEntity;
import com.clifton.fix.setup.FixSourceSystem;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class XmlMessagingTest {

	private static final String FIX_TEST_XML_FILE_OLD_NAME = "com/clifton/fix/messaging/FixMessagingMessage-message.xml";

	private static final String FIX_TEST_XML_FILE_NEW_NAME = "com/clifton/fix/messaging/FixMessageTextMessagingMessage-message.xml";

	private static final String FIX_TEST_XML_FILE_ENTITY = "com/clifton/fix/messaging/FixEntityMessagingMessage-message.xml";

	private static final String FIX_TEST_XML_FILE_ENTITY_FULLY_QUALIFIED = "com/clifton/fix/messaging/FixEntityMessagingMessage-message.xml";


	@Test
	public void testXmlLoadListResponseFixMessagingMessage() {
		FixMessageTextMessagingMessage message = XmlMessagingTestHelper.loadFromFile(FIX_TEST_XML_FILE_OLD_NAME);
		Assertions.assertNotNull(message);

		Assertions.assertEquals("y", message.getProperties().get("x"));
		Assertions.assertEquals("x", message.getProperties().get("y"));

		Assertions.assertEquals("DUDE", message.getMessageText());
		Assertions.assertEquals("MW", message.getMachineName());
		Assertions.assertEquals("xyz", message.getSessionQualifier());

		Assertions.assertEquals(25, (int) message.getIdentifier().getId());
		Assertions.assertEquals("12_ABC", message.getIdentifier().getUniqueStringIdentifier());
		Assertions.assertEquals("MARK", message.getIdentifier().getSourceSystemTag());
		Assertions.assertEquals(12, (int) MathUtils.getNumberAsInteger(message.getIdentifier().getSourceSystemFkFieldId()));
		Assertions.assertFalse(message.getIdentifier().isIdentifierGenerated());

		Assertions.assertTrue(message.isValidationError());
		Assertions.assertFalse(message.isAllocationValidationError());
	}


	@Test
	public void testXmlLoadListResponseFixMessageTextMessagingMessage() {
		FixMessageTextMessagingMessage message = XmlMessagingTestHelper.loadFromFile(FIX_TEST_XML_FILE_NEW_NAME);
		Assertions.assertNotNull(message);

		Assertions.assertEquals("y", message.getProperties().get("x"));
		Assertions.assertEquals("x", message.getProperties().get("y"));

		Assertions.assertEquals("DUDE", message.getMessageText());
		Assertions.assertEquals("MW", message.getMachineName());
		Assertions.assertEquals("xyz", message.getSessionQualifier());

		Assertions.assertEquals(25, (int) message.getIdentifier().getId());
		Assertions.assertEquals("12_ABC", message.getIdentifier().getUniqueStringIdentifier());
		Assertions.assertEquals("MARK", message.getIdentifier().getSourceSystemTag());
		Assertions.assertEquals(12, (int) MathUtils.getNumberAsInteger(message.getIdentifier().getSourceSystemFkFieldId()));
		Assertions.assertFalse(message.getIdentifier().isIdentifierGenerated());

		Assertions.assertTrue(message.isValidationError());
		Assertions.assertFalse(message.isAllocationValidationError());
	}


	@Test
	public void testXmlLoadListResponseFixEntityMessagingMessage() {
		FixEntityMessagingMessage message = XmlMessagingTestHelper.loadFromFile(FIX_TEST_XML_FILE_ENTITY);
		Assertions.assertNotNull(message);

		Assertions.assertEquals("y", message.getProperties().get("x"));
		Assertions.assertEquals("x", message.getProperties().get("y"));


		Assertions.assertEquals("MW", message.getMachineName());

		Assertions.assertTrue(message.getFixEntity() instanceof FixMessageTextEntity);
		FixMessageTextEntity entity = (FixMessageTextEntity) message.getFixEntity();


		Assertions.assertEquals(25, (int) entity.getIdentifier().getId());
		Assertions.assertEquals("12_ABC", entity.getIdentifier().getUniqueStringIdentifier());
		Assertions.assertEquals("MARK", entity.getIdentifier().getSourceSystemTag());
		Assertions.assertEquals(12, (int) MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
		Assertions.assertFalse(entity.getIdentifier().isIdentifierGenerated());

		Assertions.assertEquals("MESSAGE", entity.getMessageText());
		Assertions.assertEquals("123", entity.getFixBeginString());
		Assertions.assertEquals("456", entity.getFixSenderCompId());
		Assertions.assertEquals("789", entity.getFixSenderSubId());
		Assertions.assertEquals("ABC", entity.getFixSenderLocId());
		Assertions.assertEquals("DEF", entity.getFixTargetCompId());
		Assertions.assertEquals("GHI", entity.getFixTargetSubId());
		Assertions.assertEquals("JKL", entity.getFixTargetLocId());
		Assertions.assertEquals("xyz", entity.getFixSessionQualifier());
		Assertions.assertEquals("MNO", entity.getFixOnBehalfOfCompID());
		Assertions.assertEquals("PQR", entity.getFixDestinationName());
		Assertions.assertTrue(entity.isIncoming());
		Assertions.assertTrue(entity.isValidationError());
		Assertions.assertTrue(entity.isAllocationValidationError());
	}


	@Test
	public void testXmlLoadListResponseFixEntityMessagingMessageFullyQualified() {
		FixEntityMessagingMessage message = XmlMessagingTestHelper.loadFromFile(FIX_TEST_XML_FILE_ENTITY_FULLY_QUALIFIED);
		Assertions.assertNotNull(message);

		Assertions.assertEquals("y", message.getProperties().get("x"));
		Assertions.assertEquals("x", message.getProperties().get("y"));


		Assertions.assertEquals("MW", message.getMachineName());

		Assertions.assertTrue(message.getFixEntity() instanceof FixMessageTextEntity);
		FixMessageTextEntity entity = (FixMessageTextEntity) message.getFixEntity();


		Assertions.assertEquals(25, (int) entity.getIdentifier().getId());
		Assertions.assertEquals("12_ABC", entity.getIdentifier().getUniqueStringIdentifier());
		Assertions.assertEquals("MARK", entity.getIdentifier().getSourceSystemTag());
		Assertions.assertEquals(12, (int) MathUtils.getNumberAsInteger(entity.getIdentifier().getSourceSystemFkFieldId()));
		Assertions.assertFalse(entity.getIdentifier().isIdentifierGenerated());

		Assertions.assertEquals("MESSAGE", entity.getMessageText());
		Assertions.assertEquals("123", entity.getFixBeginString());
		Assertions.assertEquals("456", entity.getFixSenderCompId());
		Assertions.assertEquals("789", entity.getFixSenderSubId());
		Assertions.assertEquals("ABC", entity.getFixSenderLocId());
		Assertions.assertEquals("DEF", entity.getFixTargetCompId());
		Assertions.assertEquals("GHI", entity.getFixTargetSubId());
		Assertions.assertEquals("JKL", entity.getFixTargetLocId());
		Assertions.assertEquals("xyz", entity.getFixSessionQualifier());
		Assertions.assertEquals("MNO", entity.getFixOnBehalfOfCompID());
		Assertions.assertEquals("PQR", entity.getFixDestinationName());
		Assertions.assertTrue(entity.isIncoming());
		Assertions.assertTrue(entity.isValidationError());
		Assertions.assertTrue(entity.isAllocationValidationError());
	}


	@Test
	public void testToXml_1() {
		String expected = "<fixMessageTextMessagingMessage machineName=\"MW\" sourceSystemTag=\"TEST\" sessionQualifier=\"xxx\" validationError=\"false\" allocationValidationError=\"false\">\n" +
				"  <properties>\n" +
				"    <entry key=\"x\" value=\"x\"/>\n" +
				"  </properties>\n" +
				"  <messageText>ABC</messageText>\n" +
				"  <identifier sourceSystemTag=\"TEST\" identifierGenerated=\"false\" dropCopyIdentifier=\"false\" persistent=\"false\"/>\n" +
				"</fixMessageTextMessagingMessage>";
		FixMessageTextMessagingMessage msg = new FixMessageTextMessagingMessage();
		msg.setMachineName("MW");
		msg.setMessageText("ABC");
		msg.getProperties().put("x", "x");
		msg.setSessionQualifier("xxx");
		msg.setSourceSystemTag("TEST");
		msg.setIdentifier(new FixIdentifier());
		msg.getIdentifier().setSourceSystem(new FixSourceSystem("TEST"));
		String xml = XmlMessagingTestHelper.toXml(msg);
		Assertions.assertEquals(expected, xml);
	}


	@Test
	public void testToXml_2() {
		String expected = "<fixMessageTextMessagingMessage machineName=\"MW\" sourceSystemTag=\"TEST\" sessionQualifier=\"xxx\" validationError=\"false\" allocationValidationError=\"false\">\n" +
				"  <properties>\n" +
				"    <entry key=\"x\" value=\"x\"/>\n" +
				"  </properties>\n" +
				"  <messageText>ABC</messageText>\n" +
				"  <identifier identifierGenerated=\"false\" dropCopyIdentifier=\"false\" persistent=\"false\"/>\n" +
				"</fixMessageTextMessagingMessage>";
		FixMessageTextMessagingMessage msg = new FixMessageTextMessagingMessage();
		msg.setMachineName("MW");
		msg.setMessageText("ABC");
		msg.getProperties().put("x", "x");
		msg.setSessionQualifier("xxx");
		msg.setSourceSystemTag("TEST");
		msg.setIdentifier(new FixIdentifier());
		String xml = XmlMessagingTestHelper.toXml(msg);
		Assertions.assertEquals(expected, xml);
	}


	@Test
	public void testToXml_3() {
		String expected = "<fixEntityMessagingMessage machineName=\"MW\" sourceSystemTag=\"TEST\">\n" +
				"  <properties>\n" +
				"    <entry key=\"x\" value=\"x\"/>\n" +
				"  </properties>\n" +
				"  <fixEntity class=\"com.clifton.fix.order.FixMessageTextEntity\" sequenceNumber=\"0\" incoming=\"true\" validationError=\"true\" allocationValidationError=\"true\">\n" +
				"    <identifier identifierGenerated=\"false\" dropCopyIdentifier=\"false\" persistent=\"false\"/>\n" +
				"    <fixBeginString>123</fixBeginString>\n" +
				"    <fixSenderCompId>456</fixSenderCompId>\n" +
				"    <fixSenderSubId>789</fixSenderSubId>\n" +
				"    <fixSenderLocId>ABC</fixSenderLocId>\n" +
				"    <fixTargetCompId>DEF</fixTargetCompId>\n" +
				"    <fixTargetSubId>GHI</fixTargetSubId>\n" +
				"    <fixTargetLocId>JKL</fixTargetLocId>\n" +
				"    <fixSessionQualifier>xyz</fixSessionQualifier>\n" +
				"    <fixOnBehalfOfCompID>MNO</fixOnBehalfOfCompID>\n" +
				"    <fixDestinationName>PQR</fixDestinationName>\n" +
				"  </fixEntity>\n" +
				"</fixEntityMessagingMessage>";
		FixEntityMessagingMessage msg = new FixEntityMessagingMessage();
		msg.setMachineName("MW");
		msg.getProperties().put("x", "x");
		msg.setSessionQualifier("xxx");
		msg.setSourceSystemTag("TEST");
		msg.setFixEntity(new FixMessageTextEntity());
		msg.getFixEntity().setIdentifier(new FixIdentifier());
		msg.getFixEntity().setFixBeginString("123");
		msg.getFixEntity().setFixSenderCompId("456");
		msg.getFixEntity().setFixSenderSubId("789");
		msg.getFixEntity().setFixSenderLocId("ABC");
		msg.getFixEntity().setFixTargetCompId("DEF");
		msg.getFixEntity().setFixTargetSubId("GHI");
		msg.getFixEntity().setFixTargetLocId("JKL");
		msg.getFixEntity().setFixSessionQualifier("xyz");
		msg.getFixEntity().setFixOnBehalfOfCompID("MNO");
		msg.getFixEntity().setFixDestinationName("PQR");
		msg.getFixEntity().setIncoming(true);
		msg.getFixEntity().setValidationError(true);
		msg.getFixEntity().setAllocationValidationError(true);
		String xml = XmlMessagingTestHelper.toXml(msg);
		Assertions.assertEquals(expected, xml);
	}
}
