package com.clifton.fix.lifecycle;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.FixMessageEntryBuilder;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.message.FixMessageEntryTypeBuilder;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionBuilder;
import com.clifton.fix.session.FixSessionDefinitionBuilder;
import com.clifton.system.lifecycle.retriever.BaseSystemLifeCycleRetrieverTests;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleTestExecutor;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public abstract class BaseFixMessageLifeCycleRetrieverTests extends BaseSystemLifeCycleRetrieverTests {


	protected BaseFixMessageLifeCycleRetriever getFixMessageSystemLifeCycleRetriever() {
		return (BaseFixMessageLifeCycleRetriever) getSystemLifeCycleRetriever();
	}


	@Test
	public void testGetSystemLifeCycleEventListForEntity_ErrorConnectingToFix() {
		BaseFixMessageLifeCycleRetriever retriever = Mockito.spy(getFixMessageSystemLifeCycleRetriever());
		Mockito.doThrow(new ValidationException("Test Error thrown from retrieving FIX Messages")).when(retriever).getFixMessageList(ArgumentMatchers.any());

		SystemLifeCycleTestExecutor.forTableAndEntityIdAndRetriever("Test Table", 1, retriever)
				.withEventDateIsCurrentDate(true)
				.withDoNotUseCurrentProcessingEntity(true)
				.withExpectedResults(new String[]{
						"Source: N/A\tLink: N/A\tEvent Source: Fix Message\tAction: Unknown\tDate: " + SystemLifeCycleTestExecutor.CURRENT_DATE_REPLACEMENT_STRING + "\tUser: Unknown\tDetails: Unable to retrieve FIX Message Details: Test Error thrown from retrieving FIX Messages"
				})
				.execute();
	}


	protected List<FixMessage> getTestFixMessageList() {
		FixSession fixSession = FixSessionBuilder.createDefined((short) 5, FixSessionDefinitionBuilder.createOther("Test Session", "PPA", "EXT").toFixSessionDefinition()).toFixSession();
		FixMessageEntryType newOrder = FixMessageEntryTypeBuilder.createNewOrderSingle().toFixMessageEntryType();
		FixMessageEntryType executionReport = FixMessageEntryTypeBuilder.createExecutionRetport().toFixMessageEntryType();

		Date firstDate = DateUtils.toDate("2021-07-29 10:00:00", DateUtils.DATE_FORMAT_FULL);

		List<FixMessage> messageList = new ArrayList<>();
		messageList.add(FixMessageEntryBuilder.createEmptyMessageForType(fixSession, false, newOrder, firstDate).toFixMessageEntry().toFixMessage());
		messageList.add(FixMessageEntryBuilder.createEmptyMessageForType(fixSession, true, executionReport, DateUtils.addMilliseconds(firstDate, 50)).toFixMessageEntry().toFixMessage());
		messageList.add(FixMessageEntryBuilder.createEmptyMessageForType(fixSession, false, newOrder, DateUtils.addSeconds(firstDate, 150)).toFixMessageEntry().toFixMessage());
		return messageList;
	}
}
