package com.clifton.fix;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * <code>FixEntityEqualityUtils</code> utility methods for testing the equality of two Fix entities.
 */
public class FixEntityEqualityUtils {

	/**
	 * Returns the flattened map of all fields in an object. If object is complex object, that it hold collection of another of simply another object, it's
	 * important to add package for that object to expanded packages for this function to flatten the composite object.
	 *
	 * @param fixObject
	 * @param expandedPackages
	 * @return
	 */
	public static Map<String, Object> getDescribedFixEntityWithRequiredSetterMap(Object fixObject, String... expandedPackages) {
		List<String> expandedPackageList = CollectionUtils.createList(expandedPackages);
		Set<Integer> discovered = new HashSet<>();
		Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(fixObject);
		describeWithRequiredSetterRecursive(valueMap, discovered, expandedPackageList);
		return valueMap;
	}


	@SuppressWarnings("unchecked")
	public static void removeMilliseconds(Map<String, Object> valueMap, String propertyName) {
		for (Map.Entry<String, Object> stringObjectEntry : valueMap.entrySet()) {
			Object value = stringObjectEntry.getValue();
			if (value != null) {
				if (propertyName.equals(stringObjectEntry.getKey()) && value instanceof Date) {
					valueMap.put(stringObjectEntry.getKey(), DateUtils.clearMilliseconds((Date) value));
				}
				else if (Collection.class.isAssignableFrom(value.getClass())) {
					removeMillisecondsRecursiveList((Collection<Object>) value, propertyName);
				}
			}
		}
	}


	@SuppressWarnings("unchecked")
	public static void setBigDecimalScale(Map<String, Object> valueMap, String propertyName, int scale) {
		for (Map.Entry<String, Object> stringObjectEntry : valueMap.entrySet()) {
			Object value = stringObjectEntry.getValue();
			if (value != null) {
				if (propertyName.equals(stringObjectEntry.getKey()) && value instanceof BigDecimal) {
					valueMap.put(stringObjectEntry.getKey(), ((BigDecimal) value).setScale(scale, RoundingMode.DOWN));
				}
				else if (Collection.class.isAssignableFrom(value.getClass())) {
					setBigDecimalScaleRecursiveList((Collection<Object>) value, propertyName, scale);
				}
			}
		}
	}


	public static void removeProperties(Map<String, Object> valueMap, String... propertyNames) {
		List<String> propertyNameList = CollectionUtils.createList(propertyNames);
		List<Supplier<Object>> removals = new ArrayList<>();
		for (String propertyName : propertyNameList) {
			removePropertiesRecursive(valueMap, propertyName, removals);
		}
		for (Supplier<Object> removal : removals) {
			removal.get();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	public static void removePropertiesRecursive(Map<String, Object> valueMap, String propertyName, List<Supplier<Object>> removals) {
		for (Map.Entry<String, Object> stringObjectEntry : valueMap.entrySet()) {
			Object value = stringObjectEntry.getValue();
			if (propertyName.equals(stringObjectEntry.getKey())) {
				removals.add(() -> valueMap.remove(stringObjectEntry.getKey()));
			}
			else if (value != null && Collection.class.isAssignableFrom(value.getClass())) {
				removePropertyRecursiveList((Collection<Object>) value, propertyName, removals);
			}
		}
	}


	@SuppressWarnings("unchecked")
	private static void removePropertyRecursiveList(Collection<Object> collection, String propertyName, List<Supplier<Object>> removals) {
		if (!CollectionUtils.isEmpty(collection)) {
			for (Object value : collection) {
				if (value != null && Map.class.isAssignableFrom(value.getClass())) {
					removePropertiesRecursive((Map<String, Object>) value, propertyName, removals);
				}
			}
		}
	}


	@SuppressWarnings("unchecked")
	private static void setBigDecimalScaleRecursiveList(Collection<Object> collection, String propertyName, int scale) {
		if (!CollectionUtils.isEmpty(collection)) {
			Object value = collection.iterator().next();
			if (value != null && Map.class.isAssignableFrom(value.getClass())) {
				setBigDecimalScale((Map<String, Object>) value, propertyName, scale);
			}
		}
	}


	@SuppressWarnings("unchecked")
	private static void removeMillisecondsRecursiveList(Collection<Object> collection, String propertyName) {
		if (!CollectionUtils.isEmpty(collection)) {
			Object value = collection.iterator().next();
			if (value != null && Map.class.isAssignableFrom(value.getClass())) {
				removeMilliseconds((Map<String, Object>) value, propertyName);
			}
		}
	}


	private static void describeWithRequiredSetterRecursive(Map<String, Object> valueMap, Set<Integer> discovered, List<String> expandedPackageList) {
		for (Map.Entry<String, Object> stringObjectEntry : valueMap.entrySet()) {
			Object value = stringObjectEntry.getValue();
			Object newValue = describeFixEntityWithRequiredSetterRecursive(value, discovered, expandedPackageList);
			if (value != newValue) {
				valueMap.put(stringObjectEntry.getKey(), newValue);
			}
		}
	}


	private static Object describeFixEntityWithRequiredSetterRecursive(Object value, Set<Integer> discovered, List<String> expandedPackageList) {
		if (value == null || discovered.contains(System.identityHashCode(value))) {
			return value;
		}
		discovered.add(System.identityHashCode(value));
		if (expandedPackageList.contains(value.getClass().getPackage().getName())) {
			Map<String, Object> valueMap = BeanUtils.describeWithRequiredSetter(value);
			describeWithRequiredSetterRecursive(valueMap, discovered, expandedPackageList);
			return valueMap;
		}
		else if (Collection.class.isAssignableFrom(value.getClass())) {
			@SuppressWarnings({"rawtypes", "unchecked"})
			Collection<Object> list = (Collection) value;
			Collector<Object, ?, ? extends Collection<Object>> collector;
			if (Set.class.isAssignableFrom(value.getClass())) {
				collector = Collectors.toSet();
			}
			else if (List.class.isAssignableFrom(value.getClass())) {
				collector = Collectors.toList();
			}
			else {
				throw new RuntimeException("Unsupported Collection Type " + value.getClass().getName());
			}
			return list.stream().map(o -> describeFixEntityWithRequiredSetterRecursive(o, discovered, expandedPackageList)).collect(collector);
		}

		return value;
	}
}
