package com.clifton.fix.message;


/**
 * <code>FixMessageEntryTypeBuilder</code> used to build FixMessageEntryType entities for testing purposes.
 */
public class FixMessageEntryTypeBuilder {

	private final FixMessageEntryType fixMessageEntryType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixMessageEntryTypeBuilder(FixMessageEntryType fixMessageEntryType) {
		this.fixMessageEntryType = fixMessageEntryType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixMessageEntryTypeBuilder createHeartbeat() {
		FixMessageEntryType fixMessageEntryType = new FixMessageEntryType();
		fixMessageEntryType.setId((short) 4);
		fixMessageEntryType.setName("Heartbeat Message");
		fixMessageEntryType.setDescription("The entry is a heartbeat message.");
		fixMessageEntryType.setDefaultType(false);
		fixMessageEntryType.setMessage(true);
		fixMessageEntryType.setAdministrative(true);

		return new FixMessageEntryTypeBuilder(fixMessageEntryType);
	}


	public static FixMessageEntryTypeBuilder createNewOrderSingle() {
		FixMessageEntryType fixMessageEntryType = new FixMessageEntryType();
		fixMessageEntryType.setId((short) 5);
		fixMessageEntryType.setName("New Order Single");
		fixMessageEntryType.setDescription("The entry is a new order.");
		fixMessageEntryType.setDefaultType(false);
		fixMessageEntryType.setMessage(true);
		fixMessageEntryType.setAdministrative(false);

		return new FixMessageEntryTypeBuilder(fixMessageEntryType);
	}


	public static FixMessageEntryTypeBuilder createExecutionRetport() {
		FixMessageEntryType fixMessageEntryType = new FixMessageEntryType();
		fixMessageEntryType.setId((short) 6);
		fixMessageEntryType.setName("Execution Report");
		fixMessageEntryType.setDescription("The entry is an execution report.");
		fixMessageEntryType.setDefaultType(false);
		fixMessageEntryType.setMessage(true);
		fixMessageEntryType.setAdministrative(false);

		return new FixMessageEntryTypeBuilder(fixMessageEntryType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryTypeBuilder withDefaultType(boolean defaultType) {
		this.fixMessageEntryType.setDefaultType(defaultType);
		return this;
	}


	public FixMessageEntryTypeBuilder setMessage(boolean message) {
		this.fixMessageEntryType.setMessage(message);
		return this;
	}


	public FixMessageEntryTypeBuilder setAdministrative(boolean administrative) {
		this.fixMessageEntryType.setAdministrative(administrative);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryType toFixMessageEntryType() {
		return this.fixMessageEntryType;
	}
}
