package com.clifton.fix.message;

import com.clifton.fix.session.FixSession;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;


/**
 * <code>FixLogEntryBuilder</code> used for building FixMessageEntry entities for testing purposes.
 */
public class FixMessageEntryBuilder {

	private static final AtomicLong nextId = new AtomicLong(1);

	private final FixMessageEntry fixMessageEntry;


	private FixMessageEntryBuilder(FixMessageEntry fixMessageEntry) {
		this.fixMessageEntry = fixMessageEntry;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixMessageEntryBuilder createHeartbeat(FixSession session, boolean incoming) {
		FixMessageEntry fixMessageEntry = new FixMessageEntry();

		fixMessageEntry.setSession(session);
		fixMessageEntry.setIncoming(incoming);

		fixMessageEntry.setId(nextId.getAndIncrement());
		fixMessageEntry.setText("8=FIX.4.4\u00019=186\u000135=D\u000134=10\u000149=CLIFTON\u000150=u737933\u000152=20110331-19:58:39.199\u000156=GSFUT\u000157=TKTS\u000111=2_20110331145838\u000121=1\u000122=A\u000138=1\u000140=1\u000148=ESM1 Index\u000154=2\u000155=ESM1 Index\u000159=0\u000160=20110331-19:58:38.537\u0001167=FUT\u000110=168\u0001");
		fixMessageEntry.setType(FixMessageEntryTypeBuilder.createHeartbeat().toFixMessageEntryType());
		fixMessageEntry.setProcessed(false);
		fixMessageEntry.setMessageEntryDateAndTime(new Date());

		return new FixMessageEntryBuilder(fixMessageEntry);
	}


	public static FixMessageEntryBuilder createEmptyMessageForType(FixSession session, boolean incoming, FixMessageEntryType entryType, Date messageEntryDateAndTime) {
		FixMessageEntry fixMessageEntry = new FixMessageEntry();

		fixMessageEntry.setSession(session);
		fixMessageEntry.setIncoming(incoming);

		fixMessageEntry.setId(nextId.getAndIncrement());
		fixMessageEntry.setType(entryType);
		fixMessageEntry.setProcessed(true);
		fixMessageEntry.setMessageEntryDateAndTime(messageEntryDateAndTime);

		return new FixMessageEntryBuilder(fixMessageEntry);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntryBuilder withSession(FixSession session) {
		this.fixMessageEntry.setSession(session);
		return this;
	}


	public FixMessageEntryBuilder withType(FixMessageEntryType entryType) {
		this.fixMessageEntry.setType(entryType);
		return this;
	}


	public FixMessageEntryBuilder withEntryDateAndTime(Date entryDateAndTime) {
		this.fixMessageEntry.setMessageEntryDateAndTime(entryDateAndTime);
		return this;
	}


	public FixMessageEntryBuilder withText(String entryText) {
		this.fixMessageEntry.setText(entryText);
		return this;
	}


	public FixMessageEntryBuilder withIncoming(Boolean incoming) {
		this.fixMessageEntry.setIncoming(incoming);
		return this;
	}


	public FixMessageEntryBuilder withProcessed(boolean processed) {
		this.fixMessageEntry.setProcessed(processed);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageEntry toFixMessageEntry() {
		return this.fixMessageEntry;
	}
}
