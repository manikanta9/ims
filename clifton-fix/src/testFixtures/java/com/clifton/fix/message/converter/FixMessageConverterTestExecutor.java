package com.clifton.fix.message.converter;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.FixEntityEqualityUtils;
import com.clifton.fix.market.data.refresh.FixMarketDataRefresh;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.google.common.base.Equivalence;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.Assertions;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class FixMessageConverterTestExecutor<E extends FixEntity, M> {

	private E fixEntity;
	private String fixEntityString;

	private final FixMessageConverterService<M> fixMessageConverterService;

	private final Map<String, PropertyValueDifference<Object>> expectedDifferences = new HashMap<>();

	private final List<List<String>> ignoredPropertyPaths = new ArrayList<>();

	private E expectedFixEntity;


	private final static Function<Object, String> formatter = (o) -> {
		if (o instanceof Date) {
			SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss.SSS");
			return sdf.format(o);
		}
		return o == null ? null : o.toString();
	};

	private final static Equivalence<Object> equivalence = new Equivalence<Object>() {
		@Override
		protected boolean doEquivalent(Object a, Object b) {
			boolean equivalent = Objects.equals(a, b);
			if (equivalent) {
				return equivalent;
			}
			if (a instanceof BigDecimal) {
				return ((BigDecimal) a).compareTo((BigDecimal) b) == 0;
			}
			return equivalent;
		}


		@Override
		protected int doHash(Object o) {
			return Objects.hash(o);
		}
	};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixMessageConverterTestExecutor(FixMessageConverterService<M> fixMessageConverterService) {
		this.fixMessageConverterService = fixMessageConverterService;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static <E extends FixEntity, M> FixMessageConverterTestExecutor<E, M> newTestForFixConverter(FixMessageConverterService<M> fixMessageConverterService) {
		return new FixMessageConverterTestExecutor<>(fixMessageConverterService);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageConverterTestExecutor<E, M> startWithFixEntityString(String fixEntityString) {
		this.fixEntityString = fixEntityString;
		return this;
	}


	public FixMessageConverterTestExecutor<E, M> startWithFixEntity(E fixEntity) {
		this.fixEntity = fixEntity;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageConverterTestExecutor<E, M> withExpectedFixEntity(E expectedFixEntity) {
		this.expectedFixEntity = expectedFixEntity;
		return this;
	}


	public FixMessageConverterTestExecutor<E, M> withExpectedDifference(String property, Object left, Object right) {
		this.expectedDifferences.put(property, new PropertyValueDifference<>(left, right));
		return this;
	}


	public FixMessageConverterTestExecutor<E, M> withIgnoredPropertyPath(String... propertyPath) {
		this.ignoredPropertyPaths.add(CollectionUtils.createList(propertyPath));
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void execute() {
		if (this.fixMessageConverterService == null) {
			throw new IllegalStateException("Must set the converter service");
		}
		MapDifference<String, Object> difference;
		if (this.fixEntity != null) {
			difference = doExecuteWithFixEntity();
		}
		else if (!StringUtils.isEmpty(this.fixEntityString)) {
			difference = doExecuteWithString();
		}
		else {
			throw new IllegalStateException("Must start the executor with either the FixEntity or a FixEntity String");
		}
		Assertions.assertTrue(areEqual(difference), getDifferencesToString(difference));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MapDifference<String, Object> doExecuteWithString() {
		M message = this.fixMessageConverterService.parse(this.fixEntityString);
		E entity = this.fixMessageConverterService.toFixEntity(message, null);

		if (this.expectedFixEntity == null) {
			throw new IllegalStateException("Must supply the expected Fix Entity.");
		}

		// any internal complex object need to be added to list of expandedPackages for this to flatten and compare object properly.
		// for e.g for QuickFixMessageConverterServiceTests.testMarketDataRefreshToFixEntity to work, added FixMarketDataRefresh.class.getPackage().getName()
		Map<String, Object> orderValueMap = FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(entity, FixAllocationInstruction.class.getPackage().getName(), FixOrder.class.getPackage().getName()
		, FixMarketDataRefresh.class.getPackage().getName());
		Map<String, Object> fixOrderValueMap = FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(this.expectedFixEntity, FixAllocationInstruction.class.getPackage().getName(),
				FixOrder.class.getPackage().getName(), FixMarketDataRefresh.class.getPackage().getName());

		this.ignoredPropertyPaths.forEach(p -> removePropertyPaths(orderValueMap, fixOrderValueMap, p));

		return Maps.difference(orderValueMap, fixOrderValueMap, equivalence);
	}


	private MapDifference<String, Object> doExecuteWithFixEntity() {
		M message = this.fixMessageConverterService.fromFixEntity(this.fixEntity);
		E entity = this.fixMessageConverterService.toFixEntity(message, null);

		Map<String, Object> orderValueMap = FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(entity, FixAllocationInstruction.class.getPackage().getName(), FixOrder.class.getPackage().getName());
		Map<String, Object> fixOrderValueMap = FixEntityEqualityUtils.getDescribedFixEntityWithRequiredSetterMap(this.fixEntity, FixAllocationInstruction.class.getPackage().getName(), FixOrder.class.getPackage().getName());

		return Maps.difference(orderValueMap, fixOrderValueMap, equivalence);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getDifferencesToString(MapDifference<String, Object> difference) {
		if (areEqual(difference)) {
			return "Entities are Equal";
		}

		StringBuilder result = new StringBuilder("Entities are Not Equal");
		if (!difference.entriesOnlyOnLeft().isEmpty()) {
			result.append(": only on left=").append(difference.entriesOnlyOnLeft());
		}
		if (!difference.entriesOnlyOnRight().isEmpty()) {
			result.append(": only on right=").append(difference.entriesOnlyOnRight());
		}
		if (!difference.entriesDiffering().isEmpty() || !this.expectedDifferences.isEmpty()) {
			List<String> removedKeys = new ArrayList<>();
			Map<String, MapDifference.ValueDifference<Object>> values = new HashMap<>(difference.entriesDiffering());
			this.expectedDifferences.keySet().forEach(k -> {
				if (values.containsKey(k)) {
					values.remove(k);
					removedKeys.add(k);
				}
			});
			result.append(": value differences=").append(stringify(values));
			if (removedKeys.size() != this.expectedDifferences.size()) {
				Map<String, MapDifference.ValueDifference<Object>> expected = new HashMap<>(this.expectedDifferences);
				removedKeys.forEach(expected::remove);
				result.append(": unrealized expected differences=").append(expected);
			}
		}
		return result.toString();
	}


	private Map<String, MapDifference.ValueDifference<String>> stringify(Map<String, MapDifference.ValueDifference<Object>> values) {
		return values.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, (Map.Entry<String, MapDifference.ValueDifference<Object>> v)
				-> new PropertyValueDifference<>(formatter.apply(v.getValue().leftValue()), formatter.apply(v.getValue().rightValue()))));
	}


	private boolean areEqual(MapDifference<String, Object> difference) {
		boolean equal = true;
		if (!difference.entriesOnlyOnLeft().isEmpty()) {
			equal = false;
		}
		if (!difference.entriesOnlyOnRight().isEmpty()) {
			equal = false;
		}
		if (!difference.entriesDiffering().isEmpty() || !this.expectedDifferences.isEmpty()) {
			AtomicInteger removed = new AtomicInteger(0);
			Map<String, MapDifference.ValueDifference<Object>> values = new HashMap<>(difference.entriesDiffering());
			this.expectedDifferences.keySet().forEach(k -> {
				if (values.containsKey(k)) {
					values.remove(k);
					removed.incrementAndGet();
				}
			});
			equal = values.isEmpty() && removed.get() == this.expectedDifferences.size();
		}
		return equal;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void removePropertyPaths(Map<String, Object> orderValueMap, Map<String, Object> fixOrderValueMap, List<String> propertyPath) {
		List<Supplier<Object>> removals = new ArrayList<>();

		removePropertiesRecursive(orderValueMap, propertyPath, 0, removals);
		removePropertiesRecursive(fixOrderValueMap, propertyPath, 0, removals);

		removals.forEach(Supplier::get);
	}


	private void removePropertiesRecursive(Map<String, Object> valueMap, List<String> propertyPath, int currentSegment, List<Supplier<Object>> removals) {
		String key = propertyPath.get(currentSegment);
		if (valueMap.containsKey(key)) {
			if (currentSegment == propertyPath.size() - 1) {
				removals.add(() -> valueMap.remove(key));
			}
			else {
				Object value = valueMap.get(key);
				if (value != null) {
					processPropertyRemovalRecursive(value, propertyPath, ++currentSegment, removals);
				}
			}
		}
	}


	@SuppressWarnings("unchecked")
	private void processPropertyRemovalRecursive(Object value, List<String> propertyPath, int currentSegment, List<Supplier<Object>> removals) {
		if (Collection.class.isAssignableFrom(value.getClass())) {
			Collection<Object> values = (Collection<Object>) value;
			for (Object v : values) {
				processPropertyRemovalRecursive(v, propertyPath, currentSegment, removals);
			}
		}
		else if (Map.class.isAssignableFrom(value.getClass())) {
			removePropertiesRecursive((Map<String, Object>) value, propertyPath, currentSegment, removals);
		}
		else {
			throw new RuntimeException("Could not process property path " + propertyPath);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class PropertyValueDifference<V> implements com.google.common.collect.MapDifference.ValueDifference<V> {

		private final V leftValue;
		private final V rightValue;


		public PropertyValueDifference(V leftValue, V rightValue) {
			this.leftValue = leftValue;
			this.rightValue = rightValue;
		}


		@Override
		public boolean equals(@Nullable Object object) {
			if (object instanceof MapDifference.ValueDifference) {
				MapDifference.ValueDifference<?> that =
						(MapDifference.ValueDifference<?>) object;
				return com.google.common.base.Objects.equal(this.leftValue, that.leftValue())
						&& com.google.common.base.Objects.equal(this.rightValue, that.rightValue());
			}
			return false;
		}


		@Override
		public int hashCode() {
			return com.google.common.base.Objects.hashCode(this.leftValue, this.rightValue);
		}


		@Override
		public String toString() {
			return "(" + this.leftValue + ", " + this.rightValue + ")";
		}


		@Override
		public V leftValue() {
			return this.leftValue;
		}


		@Override
		public V rightValue() {
			return this.rightValue;
		}
	}
}
