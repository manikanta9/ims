package com.clifton.fix.util;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.fix.fixsim.api.OrdersInApi;
import com.clifton.fix.fixsim.api.OrdersOutApi;
import com.clifton.fix.fixsim.api.RawMessagesApi;
import com.clifton.fix.fixsim.api.SessionsApi;
import com.clifton.fix.fixsim.invoker.ApiClient;
import com.clifton.fix.fixsim.model.OrderSingleIn;
import com.clifton.fix.fixsim.model.OrderSingleOut;
import com.clifton.fix.fixsim.model.RawMessage;
import com.clifton.fix.fixsim.model.SessionInfo;
import com.clifton.fix.fixsim.model.UpdateSessionInfo;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.order.FixEntity;
import org.junit.jupiter.api.Assertions;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import java.util.Arrays;
import java.util.List;


/**
 * This class contains methods which aid in the testing through the FIXSIM API along with sending FixEntity objects on to a queue.
 *
 * @author syedz
 */

public class FixSimApiHandler {

	private String apiKey;

	private final Lazy<ApiClient> apiClient = new Lazy<>(() -> {
		ApiClient client = new ApiClient();
		client.setApiKey(getApiKey());
		return client;
	});
	private final Lazy<RawMessagesApi> messagesApi = new Lazy<>(() -> new RawMessagesApi(this.apiClient.get()));
	private final Lazy<SessionsApi> sessionApi = new Lazy<>(() -> new SessionsApi(this.apiClient.get()));
	private final Lazy<OrdersInApi> ordersInApi = new Lazy<>(() -> new OrdersInApi(this.apiClient.get()));
	private final Lazy<OrdersOutApi> ordersOutApi = new Lazy<>(() -> new OrdersOutApi(this.apiClient.get()));

	////////////////////////////////////////////////////////////////////////////
	/////                      Raw Message Methods                         /////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sends a list of raw messages (stored in a file) to a FixSim Session.
	 */
	public void sendRawMessages(String filePath, String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);
		ResourceLoader loader = new DefaultResourceLoader();

		List<String> rawMessages;
		try {
			rawMessages = Arrays.asList(FileUtils.readFileToString(loader.getResource(filePath).getFile()).split("\r?\n"));
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		sendRawMessages(rawMessages, fixSimInstance, fixSimSessionName);
	}


	/**
	 * Sends a list of raw message strings to a FixSim Session
	 */
	public void sendRawMessages(List<String> rawMessageList, String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);
		RawMessage rawMessage = new RawMessage();

		rawMessage.fiXMessages(rawMessageList);
		this.messagesApi.get().rawMessagesPostRawMessage(fixSimInstance, fixSimSessionName, rawMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	/////                        FixEntity Methods                         /////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Wraps a FixEntity in a FixEntityMessagingMessage and sends it to the Fix Application via the
	 * AsynchronousMessageHandler.
	 */
	public <T extends FixEntity> void sendFixEntity(T entity, AsynchronousMessageHandler messageHandler) {
		Assertions.assertNotNull(entity, "Expected an Entity to send.");
		Assertions.assertNotNull(messageHandler, "Expected a message handler.");
		FixEntityMessagingMessage message = new FixEntityMessagingMessage();
		message.setFixEntity(entity);
		messageHandler.send(message);
	}

	////////////////////////////////////////////////////////////////////////////
	/////                    FixSim Session Methods                        /////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Deletes a FixSim Session and re-creates a session with identical properties, and clears / resets the session's previously
	 * associated Orders and sequence numbers.
	 */
	public SessionInfo reCreateFixSimSession(String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		SessionInfo sessionInfo = this.sessionApi.get().sessionsGetByInstanceSession(fixSimInstance, fixSimSessionName);

		Assertions.assertNotNull(sessionInfo.getSessionName(), "Cannot find session: " + fixSimSessionName + " in FixSim Instance: " + fixSimInstance + ".");
		deleteFixSimSession(fixSimInstance, fixSimSessionName);
		return createFixSimSession(fixSimInstance, sessionInfo);
	}


	/**
	 * Creates a new FixSimSession based on the SessionInfo passed as the second parameter
	 */
	public SessionInfo createFixSimSession(String fixSimInstance, SessionInfo sessionInfo) {
		Assertions.assertFalse(StringUtils.isEmpty(fixSimInstance), "FixSimInstance requires a value.");
		Assertions.assertNotNull(sessionInfo, "SessionInfo must be defined.");

		this.sessionApi.get().sessionsPutSession(fixSimInstance, sessionInfo);
		return updateFixSimSessionSequenceNumbers(fixSimInstance, sessionInfo.getSessionName(), 1, 1);
	}


	/**
	 * Returns the FixSim SessionInfo within the specified instance with the specified name.
	 */
	public SessionInfo getFixSimSession(String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		return this.sessionApi.get().sessionsGetByInstanceSession(fixSimInstance, fixSimSessionName);
	}


	/**
	 * Returns a list of SessionInfo for all sessions within an instance.
	 */
	public List<SessionInfo> getFixSimSessionList(String fixSimInstance) {
		Assertions.assertFalse(StringUtils.isEmpty(fixSimInstance), "FixSimInstance requires a value.");

		return this.sessionApi.get().sessionsGetByInstance(fixSimInstance);
	}


	/**
	 * Updates an existing session's properties with the data provided.
	 */
	public SessionInfo updateFixSimSession(String fixSimInstance, String fixSimSessionName, UpdateSessionInfo data) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		return this.sessionApi.get().sessionsPatchSession(fixSimInstance, fixSimSessionName, data);
	}


	/**
	 * Updates an existing session's sequence numbers.
	 */
	public SessionInfo updateFixSimSessionSequenceNumbers(String fixSimInstance, String fixSimSessionName, Integer sendingSeqNumber, Integer recSeqNumber) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		return this.sessionApi.get().sessionsPatchSessionSeq(fixSimInstance, fixSimSessionName, sendingSeqNumber, recSeqNumber);
	}


	/**
	 * Deletes the FixSim Session within the specified instance with the specified name.
	 */
	public void deleteFixSimSession(String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		deleteFixSimOrdersInForSession(fixSimInstance, fixSimSessionName);
		deleteFixSimOrdersOutForSession(fixSimInstance, fixSimSessionName);
		updateFixSimSessionSequenceNumbers(fixSimInstance, fixSimSessionName, 1, 1);

		this.sessionApi.get().sessionsDeleteSession(fixSimInstance, fixSimSessionName);
	}

	////////////////////////////////////////////////////////////////////////////
	/////                    FixSim Order Methods                         /////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves orders from OrdersIn blotter in the specified FixSimSession with the specified OrderStatus.
	 * OrderStatus is an optional string of comma separated values for one or more OrderStatus codes (shown below).
	 * Note: The string can optionally be NULL, which will return all  orders in the OrdersIn Blotter.
	 *
	 * <p>
	 * '0'	New
	 * '1'	Partially filled
	 * '2'	Filled
	 * '3'	Done for day
	 * '4'	Canceled
	 * '6'	Pending Cancel (e.g. result of Order Cancel Request)
	 * '7'	Stopped
	 * '8'	Rejected
	 * '9'	Suspended
	 * 'A'	Pending New
	 * 'B'	Calculated
	 * 'C'	Expired
	 * 'D'	Accepted for bidding
	 * 'E'	Pending Replace (e.g. result of Order Cancel/Replace Request)
	 */
	public List<OrderSingleIn> getFixSimOrdersInForSession(String fixSimInstance, String fixSimSessionName, String orderStatus) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		return this.ordersInApi.get().ordersInGetByInstanceSession(fixSimInstance, fixSimSessionName, orderStatus);
	}


	/**
	 * Retrieves orders from OrdersOut blotter the specified FixSimSession.
	 */
	public List<OrderSingleOut> getFixSimOrdersOutForSession(String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		return this.ordersOutApi.get().ordersOutGetByInstanceSession(fixSimInstance, fixSimSessionName);
	}


	/**
	 * Deletes all orders from the OrdersIn blotter for the specified FixSimSession.
	 */
	public void deleteFixSimOrdersInForSession(String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		this.ordersInApi.get().ordersInDeleteAll(fixSimInstance, fixSimSessionName);
	}


	/**
	 * Deletes all orders from the OrdersOut blotter for the specified FixSimSession.
	 */
	public void deleteFixSimOrdersOutForSession(String fixSimInstance, String fixSimSessionName) {
		validateFixInstanceAndSession(fixSimInstance, fixSimSessionName);

		this.ordersOutApi.get().ordersOutDeleteAll(fixSimInstance, fixSimSessionName);
	}

	////////////////////////////////////////////////////////////////////////////
	/////                  Private Helper Methods                          /////
	////////////////////////////////////////////////////////////////////////////


	private void validateFixInstanceAndSession(String fixSimInstance, String fixSimSessionName) {
		Assertions.assertFalse(StringUtils.isEmpty(fixSimInstance), "FixSimInstance requires a value.");
		Assertions.assertFalse(StringUtils.isEmpty(fixSimSessionName), "FixSimSessionName requires a value.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getApiKey() {
		return this.apiKey;
	}


	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
}
