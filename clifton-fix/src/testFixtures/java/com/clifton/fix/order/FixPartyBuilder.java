package com.clifton.fix.order;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;


public class FixPartyBuilder {

	private FixParty fixParty;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixPartyBuilder(FixParty fixParty) {
		this.fixParty = fixParty;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixPartyBuilder createEmptyFixParty() {
		return new FixPartyBuilder(new FixParty());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixPartyBuilder withPartyId(String partyId) {
		getFixParty().setPartyId(partyId);
		return this;
	}


	public FixPartyBuilder withSenderUserName(String senderUserName) {
		getFixParty().setSenderUserName(senderUserName);
		return this;
	}


	public FixPartyBuilder withPartyIdSource(PartyIdSources partyIdSource) {
		getFixParty().setPartyIdSource(partyIdSource);
		return this;
	}


	public FixPartyBuilder withPartyRole(PartyRoles partyRole) {
		getFixParty().setPartyRole(partyRole);
		return this;
	}


	public FixPartyBuilder withPartySubList(FixPartySub... fixPartySubs) {
		getFixParty().setPartySubList(CollectionUtils.createList(fixPartySubs));
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixParty getFixParty() {
		return this.fixParty;
	}


	public FixParty toFixParty() {
		return this.fixParty;
	}
}
