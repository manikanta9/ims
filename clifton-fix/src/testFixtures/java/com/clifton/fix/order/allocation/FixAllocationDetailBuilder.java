package com.clifton.fix.order.allocation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


public class FixAllocationDetailBuilder {

	private FixAllocationDetail fixAllocationDetail;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationDetailBuilder(FixAllocationDetail fixAllocationDetail) {
		this.fixAllocationDetail = fixAllocationDetail;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixAllocationDetailBuilder createEmptyAllocationDetail() {
		return new FixAllocationDetailBuilder(new FixAllocationDetail());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationDetailBuilder withFixAllocationInstruction(FixAllocationInstruction fixAllocationInstruction) {
		getFixAllocationDetail().setFixAllocationInstruction(fixAllocationInstruction);
		return this;
	}


	public FixAllocationDetailBuilder withAllocationAccount(String allocationAccount) {
		getFixAllocationDetail().setAllocationAccount(allocationAccount);
		return this;
	}


	public FixAllocationDetailBuilder withNestedPartyList(FixAllocationDetailNestedParty... fixAllocationDetailNestedParties) {
		getFixAllocationDetail().setNestedPartyList(CollectionUtils.createList(fixAllocationDetailNestedParties));
		return this;
	}


	public FixAllocationDetailBuilder withAllocationPrice(BigDecimal allocationPrice) {
		getFixAllocationDetail().setAllocationPrice(allocationPrice);
		return this;
	}


	public FixAllocationDetailBuilder withAllocationShares(BigDecimal allocationShares) {
		getFixAllocationDetail().setAllocationShares(allocationShares);
		return this;
	}


	public FixAllocationDetailBuilder withAllocationNetMoney(BigDecimal allocationNetMoney) {
		getFixAllocationDetail().setAllocationNetMoney(allocationNetMoney);
		return this;
	}


	public FixAllocationDetailBuilder withProcessCode(ProcessCodes processCode) {
		getFixAllocationDetail().setProcessCode(processCode);
		return this;
	}


	public FixAllocationDetailBuilder withCommission(BigDecimal commission) {
		getFixAllocationDetail().setCommission(commission);
		return this;
	}


	public FixAllocationDetailBuilder withCommissionType(CommissionTypes commissionType) {
		getFixAllocationDetail().setCommissionType(commissionType);
		return this;
	}


	public FixAllocationDetailBuilder withIndividualAllocID(String individualAllocID) {
		getFixAllocationDetail().setIndividualAllocID(individualAllocID);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationDetail getFixAllocationDetail() {
		return this.fixAllocationDetail;
	}


	public FixAllocationDetail toFixAllocationDetail() {
		return this.fixAllocationDetail;
	}
}
