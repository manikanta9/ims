package com.clifton.fix.order.allocation;

import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


public class FixAllocationReportDetailBuilder {

	private FixAllocationReportDetail fixAllocationReportDetail;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationReportDetailBuilder(FixAllocationReportDetail fixAllocationReportDetail) {
		this.fixAllocationReportDetail = fixAllocationReportDetail;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixAllocationReportDetailBuilder createEmptyFixAllocationReportDetail() {
		return new FixAllocationReportDetailBuilder(new FixAllocationReportDetail());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationReportDetailBuilder withFixAllocationReport(FixAllocationReport fixAllocationReport) {
		getFixAllocationReportDetail().setFixAllocationReport(fixAllocationReport);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationAccount(String allocationAccount) {
		getFixAllocationReportDetail().setAllocationAccount(allocationAccount);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationText(String allocationText) {
		getFixAllocationReportDetail().setAllocationText(allocationText);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationAveragePrice(BigDecimal allocationAveragePrice) {
		getFixAllocationReportDetail().setAllocationAveragePrice(allocationAveragePrice);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationPrice(BigDecimal allocationPrice) {
		getFixAllocationReportDetail().setAllocationPrice(allocationPrice);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationShares(BigDecimal allocationShares) {
		getFixAllocationReportDetail().setAllocationShares(allocationShares);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationNetMoney(BigDecimal allocationNetMoney) {
		getFixAllocationReportDetail().setAllocationNetMoney(allocationNetMoney);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationGrossTradeAmount(BigDecimal allocationGrossTradeAmount) {
		getFixAllocationReportDetail().setAllocationGrossTradeAmount(allocationGrossTradeAmount);
		return this;
	}


	public FixAllocationReportDetailBuilder withAllocationAccruedInterestAmount(BigDecimal allocationAccruedInterestAmount) {
		getFixAllocationReportDetail().setAllocationAccruedInterestAmount(allocationAccruedInterestAmount);
		return this;
	}


	public FixAllocationReportDetailBuilder withProcessCode(ProcessCodes processCode) {
		getFixAllocationReportDetail().setProcessCode(processCode);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationReportDetail getFixAllocationReportDetail() {
		return this.fixAllocationReportDetail;
	}


	public FixAllocationReportDetail toFixAllocationReportDetail() {
		return this.fixAllocationReportDetail;
	}
}
