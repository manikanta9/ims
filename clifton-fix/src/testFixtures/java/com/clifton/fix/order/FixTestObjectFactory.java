package com.clifton.fix.order;

import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.ioi.FixIOIEntity;
import com.clifton.fix.ioi.FixStipulation;
import com.clifton.fix.market.data.refresh.FixMarketDataRefresh;
import com.clifton.fix.market.data.refresh.FixMarketDataRefreshEntity;
import com.clifton.fix.market.data.refresh.MarketDataActions;
import com.clifton.fix.market.data.refresh.MarketDataEntryTypes;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgementBuilder;
import com.clifton.fix.order.allocation.FixAllocationDetailBuilder;
import com.clifton.fix.order.allocation.FixAllocationDetailNestedPartyBuilder;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.allocation.FixAllocationInstructionBuilder;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.clifton.fix.order.allocation.FixAllocationReportBuilder;
import com.clifton.fix.order.allocation.FixAllocationReportDetailBuilder;
import com.clifton.fix.order.beans.AllocationCancelReplaceReasons;
import com.clifton.fix.order.beans.AllocationLinkTypes;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationReportTypes;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.CancelRejectReasons;
import com.clifton.fix.order.beans.CancelRejectResponseToItems;
import com.clifton.fix.order.beans.CompDealerQuotePriceTypes;
import com.clifton.fix.order.beans.ExecutionTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.LastCapacities;
import com.clifton.fix.order.beans.OrderCapacities;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;
import com.clifton.fix.order.beans.PartySubIDTypes;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.ProcessCodes;
import com.clifton.fix.order.beans.Products;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.order.beans.StipulationTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;
import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;


/**
 * <code>FixTestObjectFactory</code> returns FixEntity objects along with their corresponding FIX formatted message string.
 */
public class FixTestObjectFactory {

	public static final String newOrderWithAllocationGroupsString = "8=FIX.4.49=58135=D34=58049=CLIFTONPARA50=test_user_name52=20170601-13:44:22.72656=BLP57=1596787811=O_236670_2017060121=322=P38=5870000.0000000040=148=2I65BRNZ954=255=N\\A59=060=20170601-13:44:22.70364=2017060175=201706016215=Y578=179=PPA539=1524=1V8Y6QCX6YMJ2OELII46525=N538=480=5870000.00000000453=13448=2586447=D452=56448=1184447=D452=56448=3292447=D452=56448=3063447=D452=56448=2164447=D452=56448=1189447=D452=56448=3788447=D452=56448=1690447=D452=56448=1726447=D452=56448=3729447=D452=56448=5172447=D452=56448=1014447=D452=56448=ICE447=D452=2110=083";
	public static final String orderCancelRequestString = "8=FIX.4.49=23135=F34=250049=CLIFTONMB4450=r14687652=20170606-20:08:37.41656=REDITKTS57=TKTS11=O_237591_20170606150837_CANCEL21=122=138=1675.000000000041=O_237591_2017060648=78464A67254=155=78464A67260=20170606-20:08:37.396167=CS10=040";
	public static final String allocationInstructionString = "8=FIX.4.49=92735=J34=240449=CLIFTONMB4450=r15141252=20170627-20:00:48.20256=REDITKTS115=CLIF145=CITI6=1403.0562500022=A48=RTAU7 Index53=16.0000000054=255=RTAU7 Index60=20170627-19:59:48.34964=2017062770=A_232350_2017062771=075=20170627167=FUT200=201709207=IFUS857=110626=273=111=20170627Y51077637=PNA0003278=179=276-9182D80=16.0000000081=0539=1524=CG525=D538=4124=1132=1.0000000017=9164214281781731=1403.5000000032=1.0000000017=9164214191781731=1403.4000000032=2.0000000017=9164213981781731=1403.3000000032=1.0000000017=9164213911781731=1403.4000000032=1.0000000017=9164213581781731=1403.0000000032=2.0000000017=9164213491781731=1403.0000000032=1.0000000017=9164212831781731=1402.9000000032=3.0000000017=9164212811781731=1402.8000000032=2.0000000017=9164212221781731=1402.9000000032=1.0000000017=9164212211781731=1402.9000000032=1.0000000017=9164211821781731=1403.0000000010=179";
	public static final String allocationInstructionWithProcessCodeString = "8=FIX.4.49=44635=J34=34349=CLIFTONMB4452=20150109-18:33:51.00356=REDITKTS115=CLIF145=CITI6=2010.25000000000000000000000000000022=A48=ESH5 Index53=13.000000000054=155=ESH5 Index60=20150109-18:32:44.01964=2015010970=A_116291_2015010971=075=20150109167=FUT857=073=111=O_118089_2015010937=IFF0002778=279=260-7112580=6.000000000081=079=157-7288780=7.000000000081=0124=132=13.00000000000000017=11025101560091531=2010.25000000000000010=020";
	public static final String allocationInstructionWithOutProcessCodeString = "8=FIX.4.49=36635=J34=157149=CLIFTON52=20150925-14:57:23.01656=GSFUT57=TKTS6=362.2500000022=A48=GIV5 Index53=8.0000000054=255=GIV5 Index60=20150925-14:56:23.18964=2015092570=A_148182_2015092571=075=20150925167=FUT200=20151073=111=O_151921_2015092537=pQ30pIAO0007IPH_RoI78=179=003-1750280=8.00000000124=132=8.0000000017=MIRAB148702015092531=362.2500000010=001";
	public static final String orderCancelReplaceRequestString = "8=FIX.4.49=27535=G34=257149=CLIFTONMB4450=r14890752=20161110-17:20:50.68956=REDITKTS57=DESK1=052G0932611=O_207284_2016111021=322=A38=62.0000000040=141=O_207254_2016111048=COF7 Comdty54=155=COF759=060=20161110-17:20:50.66864=2016111075=20161110167=FUT207=IFEU8135=MS10=056";
	public static final String allocationAcknowledgementString = "8=FIX.4.49=16235=P49=REDITKTS56=CLIFTONMB44128=CLIF34=411952=20170628-23:05:5770=A_232471_2017062875=2017062960=20170628-23:05:5787=010007=MSALLOC10008=REDIFUTALLOC10=010";
	public static final String orderCancelRejectString = "8=FIX.4.49=22235=949=REDITKTS56=CLIFTONMB4434=363752=20170602-18:04:4237=O_237057_20170602130442_CANCEL11=O_237057_20170602130442_CANCEL41=O_237057_2017060239=8434=1102=258=Destination REDI.2988_E is OFF, rejecting msgType F10=164";
	public static final String executionReportString = "8=FIX.4.49=33735=849=GSFUT56=CLIFTON142=FUSNYB34=209652=20170628-18:59:5537=FUSNYB664582017062811=O_240893_2017062817=FUSNYB6957220170628150=F39=11=C032061363=055=USU7 Comdty167=FUT200=20170954=138=313540=115=USD59=032=25931=155.187530=IMME151=20214=29336=155.022939475=2017062860=20170628-18:59:55120=USD21=3461=FXXXXX10=079";
	public static final String executionReportWithSubPartiesString = "8=FIX.4.49=092435=849=BLP56=CLIFTONPARA34=708128=ZERO115=i.stpdca452=20130617-14:55:5660=20130617-14:55:56150=F31=104.875151=0541=2038122032=3228000423=164=201306206=104.8759876=137=VCON:20130617:50298:138=322800039=2159=4309.16460=10223=0.0414=322800015=USD75=2013061717=VCON:20130617:50298:1:12167=MBS48=38376XRB1198=3739:20130617:50298:1228=0.6323357470=US381=2140687.1522=154=17014=2155=GNR236=0.017389382118=2144996.31453=8448=JKENNEFICK:444330447=D452=12802=1523=JACK KENNEFICK803=9448=CLIFTON2:27359447=D452=11802=3523=27359803=4000523=14803=4523=THOMAS LEE803=9448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=JKENNEFICK:444330447=D452=36802=1523=JACK KENNEFICK803=9448=SXT447=D452=16448=1883447=D452=1448=27359447=D452=11802=2523=27359803=4000523=14803=4448=444330447=D452=12454=3455=US38376XRB19456=4455=38376XRB1456=1455=!!01DQXP456=A10=019";
	public static final String allocationInstructionWithExecutionReportString = "8=FIX.4.49=48635=J49=CLIFTONMB4450=r15141256=REDITKTS115=CLIF1=Account6=1403.0562500015=USD22=A48=RTAU7 Index53=16.0000000054=255=RTAU7 Index60=20170627-19:59:48.00064=2017062770=A_232350_2017062771=075=20170627100=ExDestination118=1403.77777000120=DDD167=FUT200=201709201=0202=7423.05625000205=DAY207=IFUS381=16.00007854423=13460=13461=CFICode541=20170627626=5796=1857=078=179=276-9182D80=16.0000000081=0539=1524=CG525=D538=4453=1448=343rdlas447=D452=410=056";
	public static final String dontKnowTradeString = "8=FIX.4.49=13035=Q49=GSFUT56=CLIFTON142=FUSNYB17=FUSNYB695722017062822=137=FUSNYB664582017062848=USU7 Comdty54=155=USU7 Comdty167=FUT10=085";
	public static final String executionReportFxConnectDealerQuoteString = "8=FIX.4.49=50335=849=FXCCX34=22550=bos.clifton.gliebl56=glfixhub.bos.clifton52=20140731-14:26:36.0881=3854006=0.9083311=O_100710_2014073114=175250015=USD17=APP.e7ec043d-b763-4127-b986-c6bbb686765231=0.9083332=175250037=FIX-103977671-27849464138=175250039=240=154=255=USD/CHF58=COMPETITIVE60=20140731-14:26:36.08464=2014091775=20140731119=1591848.33120=CHF150=F151=0194=0.9086195=-0.00027229=20140731460=46230=bk:barcap|bk:bnym,s:0.90848,f:-0.000282,a:0.908198453=1448=gs447=D452=110=077";
	public static final String executionReportDealerQuoteString = "8=FIX.4.49=081935=849=BLP56=CLIFTONPARA34=69450=603683657=6036836128=ZERO52=20140731-13:56:3360=20140731-13:56:33150=F31=103.973151=0541=2023041532=550000423=164=201408056=103.97337=1839157=11238=55000039=2159=175.06460=611=O_100762_20140731192=0223=0.00114=55000015=EUR75=20140731106=DEUTSCHLAND I/L BOND17=GS:20140731:1036:5:12167=EUSOV48=EJ0993182198=50:20140731:1036:5228=1.03728470=DE381=593170.1222=16622=06623=054=26624=-157.355=[N/A]6625=0236=-0.003493195118=593345.18238=010009=210010=372910011=010012=110010=368410011=010012=1453=5448=CHASKAMP447=D452=11448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=TSOX447=D452=16448=JBRAIK2:4825030447=D452=36448=3788447=D452=19690=29691=37299692=D9693=19694=09695=09691=36849692=D9693=19694=09695=010=148";
	public static final String executionReportDealerQuoteString2 = "8=FIX.4.49=082935=849=BLP56=CLIFTONPARA34=68350=603683657=6036836128=ZERO52=20140731-13:50:2560=20140731-13:50:25150=F31=147.667151=0541=2032072532=47300423=164=201408056=147.66737=1846157=1138=4730039=2159=55.61460=611=O_100764_20140731192=0223=0.031514=4730015=EUR75=20140731106=FRANCE (GOVT OF)17=MS:20140731:1311:5:12167=EUSOV48=EC7356149198=12:20140731:1311:5228=1.23856470=FR381=86509.0722=16622=06623=054=16624=-152.955=[N/A]6625=0236=0.00397118=86564.68238=010009=210010=258610011=147.7210012=110010=372910011=148.1610012=1453=5448=CHASKAMP447=D452=11448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=TSOX447=D452=16448=IVANA:416119447=D452=36448=3063447=D452=19690=29691=25869692=D9693=19694=147.729695=473009691=37299692=D9693=19694=148.169695=4730010=089";
	public static final String executionReportEmsxDealerQuoteString = "8=FIX.4.49=058935=849=BLP56=CLFTPARAEMSX34=77450=LSTM57=542202052=20200228-15:48:5360=20200228-15:48:53150=F31=35.19151=032=91364=202002286=35.1937=1638120138=91339=240=111=O_448263_20200228461=OPEXCS14=91315=USD645=47.2646=-5617=22818725677=O167=OPT48=MXEA200=202003201=0202=165054=255=MXEA205=27207=XCBO58=SOLD(MXEA US) 913 at 35.19(USD) for ORDER ID # 1638120159=0453=1448=CSUS447=I452=19690=49691=BNPP9692=D9693=29694=47.39695=9139691=CITI9692=D9693=29694=469695=9139691=CSUS9692=D9693=29694=539695=9139691=MSEO9692=D9693=29694=489695=91310=087";
	public static final String allocationReportBloombergString = "8=FIX.4.49=081435=AS49=BLP56=CLIFTONPARA34=68115=i.FIXALLOC52=20130510-14:40:1960=20130510-14:40:19423=164=20130513755=A_56488_201305100939176=99.40625159=070=A_56488_20130510093917460=671=0793=3788:20130510:215:6794=315=USD75=20130510857=048=N/A381=198812522=153=200000054=155=[N/A]236=2.893404971787=0118=2001163.6773=111=MANUAL37=D3:20130510:215:6198=3788:20130510:215:638=200000078=279=SWBF0909002661=9980=1000000539=1524=SWBF0909002525=D538=24804=1545=000012869805=4003153=0154=1000581.835546=0742=079=MAC33661=9980=1000000539=1524=MAC33525=D538=24804=1545=000012901805=4003153=0154=1000581.835546=0742=0453=4448=CHASKAMP447=D452=11448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=TSOX447=D452=16448=9001447=D452=1124=132=200000031=99.4062510=157";
	public static final String executionReportBloombergString = "8=FIX.4.49=083035=849=BLP56=CLIFTONPARA34=100850=8948089128=ZERO52=20130620-19:11:4460=20130620-19:11:44150=F31=0.0075151=0541=2013062732=20000000423=464=201306216=99.99987537=625157=038=2000000039=2159=0669=99.999875460=611=O_60142_20130620141002192=0223=014=2000000044=015=USD75=20130620106=TREASURY BILL17=JFF:20130620:751:6:12167=TBILL48=9127956W6198=5590:20130620:751:6470=US381=1999997522=16623=054=26624=055=[N/A]6625=0236=0.0000760418118=19999975238=010009=210010=306310011=0.007510012=410010=172610011=0.00810012=4453=5448=GLIEBL447=D452=11448=PARAMETRIC PORTFOLIO ASSOCIATES LLC447=D452=13448=TSOX447=D452=16448=TKERESTES3:10031033447=D452=36448=3684447=D452=19690=29691=30639692=D9693=49694=0.00759695=200000009691=17269692=D9693=49694=0.0089695=2000000010=236";
	public static final String newOrderString = "8=FIX.4.49=21135=D49=CLIFTON50=mwacker56=GSFUT57=TKTS167=FUT59=060=20121017-05:00:00.00064=201210171=12345611=142_1231201221=122=A75=2012101738=1040=158=Test 42\nTest 43\nTest48=ESH3 Index54=155=ESH3 Index10=206";
	public static final String newOrderString2 = "8=FIX.4.49=22035=D49=CLIFTON50=mwacker56=GSFUT57=TKTS1=12345611=142_1231201221=122=A38=10.0000000040=148=ESH3 Index54=155=ESH3 Index58=Test 42/nTest 43/nTest59=060=20121017-05:00:00.00064=2012101775=20121017167=FUT10=162";
	public static final String newIOIMessage = "8=FIX.4.4\u00019=313\u000135=6\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=86780\u000152=20200819-19:15:55.966\u000123=HTGM_IOI_20200819_0000000000\u000128=N\u000155=[N/A]\u000127=15000\u0001423=1\u000144=106.41000000\u000148=00037CSG6\u000122=1\u0001460=11\u000164=20200821\u000154=2\u000160=20200819-19:15:55.966\u0001232=3\u0001233=MINQTY\u0001234=5000\u0001233=MININCR\u0001234=5000\u0001233=MINTOKEEP\u0001234=5000\u0001453=1\u0001448=0443HEAD\u0001447=C\u0001452=1\u000110=219\u0001";
	public static final String cancelIOIMessage = "8=FIX.4.4\u00019=186\u000135=6\u000149=HTGM_QA_IOI\u000156=PARAMETRIC\u000134=78486\u000152=20200819-19:09:41.934\u000123=HTGM_IOI_20200819_0000009042\u000128=A\u000155=[N/A]\u000127=0\u0001423=1\u000144=0\u000160=20200819-19:09:41.934\u0001453=1\u0001448=0443HEAD\u0001447=C\u0001452=1\u000110=043\u0001";
	public static final String marketDataRefresh = "8=FIX.4.4\u00019=0303\u000135=X\u000134=0000081\u000152=20210514-21:28:03.255\u000149=JPM_CONTRI_UAT\u000156=PARAPORT_CONTRI_UAT\u0001268=1\u0001279=0\u0001269=1\u0001278=mAMUNIBWIC716235\u000148=59261AQK6\u000122=1\u0001460=11\u0001167=BWIC\u0001423=1\u0001271=85000.00000000\u0001272=20210514\u0001273=21:28:03\u0001126=20210514-21:33:00\u00011914=20210514-21:30:00\u000126995=B\u000164=20210518\u000115=USD\u000160=20210514-21:28:03.241\u000110=006\u0001";
	public static final String executionReportBloombergStringWithNotes = "8=FIX.4.4\u00019=0885\u000135=8\u000149=MAP_BLP_BETA\u000156=MAP_PARA_BETA\u000134=228\u0001128=ZERO\u0001115=DCSVC_12516_1\u000143=Y\u0001122=20210630-15:08:50\u000152=20210630-15:08:50\u000160=20210630-14:55:49\u0001150=F\u000131=114.95\u0001151=0\u0001541=20270701\u000132=15000\u0001423=1\u000164=20210701\u00016=114.95\u000137=VCON:20210630:50068:21\u000138=15000\u000139=2\u0001159=0\u0001669=114.95\u0001460=11\u0001223=0.03\u000114=15000\u000115=USD\u000175=20210630\u000117=VCON:20210630:50068:21:12\u0001167=REV\u000148=544525ZL5\u0001198=3739:20210630:50068:21\u0001470=US\u0001381=17242.5\u000122=1\u000154=1\u00017014=21\u000155=[N/A]\u0001236=0.0047\u0001118=17242.5\u0001453=6\u0001448=EAVA\u0001447=D\u0001452=1\u0001448=SMCELREATH2:19728435\u0001447=D\u0001452=11\u0001802=2\u0001523=14\u0001803=4\u0001523=18798741\u0001803=4000\u0001448=SMCELREATH2:19728435\u0001447=D\u0001452=12\u0001448=PARAMETRIC PORTFOLIO ASSOCIATES LLC\u0001447=D\u0001452=13\u0001448=SXT\u0001447=D\u0001452=16\u0001448=SMCELREATH2:19728435\u0001447=D\u0001452=36\u00019610=1\u00019611=D\u00019613=                                      * LOS ANGELES CA DEPT O * \u0001454=4\u0001455=US544525ZL58\u0001456=4\u0001455=544525ZL5\u0001456=1\u0001455=544525ZL5\u0001456=A\u0001455=LOSUTL\u0001456=8\u000110=046\u0001";
	public static final String tradeCaptureMsg = "8=FIX.4.4\u00019=1289\u000135=AE\u000149=MAP_BLP_BETA\u000156=MAP_PARA_BETA\u000134=10\u000157=19728435\u0001115=i.VCONSRV\u000152=20210112-14:36:42\u000160=20210112-14:36:42\u0001570=N\u000131=109\u0001571=00005023:3bac:684c:5ffcdeca\u000132=15000\u0001423=1\u0001573=0\u000164=20210114\u0001487=2\u0001818=6916882446426570760\u0001460=11\u0001880=3739:20210112:50105:21\u0001854=1\u000175=20210112\u0001856=0\u0001167=REV\u000148=US97712DXR24\u0001228=1\u0001231=1\u000122=4\u000155=[N/A]\u0001235=WORST\u0001236=2.008\u0001454=3\u0001455=97712DXR2\u0001456=1\u0001455=97712DXR\u0001456=A\u0001455=US97712DXR24\u0001456=4\u0001552=1\u000154=2\u000137=MANUAL198=3739:20210112:50105:21\u0001453=3\u0001448=STEPHAN MCELREATH @ PARAMETRIC PORTFOLIO ASSOCIATES LLC : 19728435 @ 2166\u0001447=D\u0001452=1\u0001802=8\u0001523=EAVA\u0001803=1\u0001523=2166\u0001803=4014\u0001523=PARAMETRIC PORTFOLIO ASSOCIATES LLC\u0001803=4015\u0001523=30449848\u0001803=4016\u0001523=NEW YORK\u0001803=34\u0001523=US\u0001803=38\u0001523=EAVA\u0001803=4010\u0001523=EAVA\u0001803=4013\u0001448=STEPHAN MCELREATH @ PARAMETRIC PORTFOLIO ASSOCIATES LLC : 19728435 @ 2166\u0001447=D\u0001452=11\u0001802=7\u0001523=2166\u0001803=4005\u0001523=PARAMETRIC PORTFOLIO ASSOCIATES LLC\u0001803=1\u0001523=2166\u0001803=4014\u0001523=PARAMETRIC PORTFOLIO ASSOCIATES LLC\u0001803=4015\u0001523=30449848\u0001803=4016\u0001523=NEW YORK\u0001803=34\u0001523=US\u0001803=38\u0001448=BXT\u0001447=D\u0001452=16\u000115=USD\u0001381=16350159=18.96118=16368.9658=* WISCONSIN ST HLTH & E * \u00019503=N10=108";


	public static FixOrder createNewOrderWithAllocationGroups() {
		return FixOrderBuilder.createEmptyOrder()
				.withClientOrderStringId("O_236670_20170601")
				.withSenderUserName("mwacker")
				.withHandlingInstructions(HandlingInstructions.MANUAL)
				.withSide(OrderSides.SELL)
				.withOrderQty(new BigDecimal("5870000.00000000"))
				.withOrderType(OrderTypes.MARKET)
				.withTimeInForce(TimeInForceTypes.DAY)
				.withSymbol("N\\A")
				.withSecurityId("2I65BRNZ9")
				.withSecurityIDSource(SecurityIDSources.RED_CODE)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 1, 8, 44, 22).plus(703, ChronoField.MILLI_OF_DAY.getBaseUnit())))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 1)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 1)))
				.withFixPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("2586")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("1184")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("3292")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("3063")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("2164")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("1189")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("3788")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("1690")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("1726")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("3729")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("5172")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("1014")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ACCEPTABLE_COUNTERPARTY)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("ICE")
								.withSenderUserName("mwacker")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.CLEARING_ORGANIZATION)
								.toFixParty()
				)
				.withFixAllocationDetailList(
						FixAllocationDetailBuilder.createEmptyAllocationDetail()
								.withAllocationAccount("PPA")
								.withAllocationShares(new BigDecimal("5870000.00000000"))
								.withNestedPartyList(
										FixAllocationDetailNestedPartyBuilder.createEmptyFixAllocationDetailNestedParty()
												.withNestedPartyIDSource(PartyIdSources.LEGAL_ENTITY_IDENTIFIER)
												.withNestedPartyRole(PartyRoles.CLEARING_FIRM)
												.withNestedPartyId("1V8Y6QCX6YMJ2OELII46")
												.toFixAllocationDetailNestedParty()
								)
								.toFixAllocationDetail()
				)
				.withSequenceNumber(580)
				.withFixBeginString("FIX.4.4")
				.withSenderCompId("CLIFTONPARA")
				.withFixTargetCompId("BLP")
				.withFixTargetSubId("15967878")
				.withFixDestinationName("fixDestination1")
				.withIncoming(false)
				.toOrder();
	}


	public static FixOrderCancelRequest createOrderCancelRequest() {
		return FixOrderCancelRequestBuilder.createEmptyOrderCancelRequest()
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTONMB44")
				.withFixTargetCompId("REDITKTS")
				.withFixSenderSubId("r146876")
				.withFixTargetSubId("TKTS")
				.withSenderUserName("lnaylor")
				.withSequenceNumber(2500)
				.withClientOrderStringId("O_240893_20170628")
				.withSide(OrderSides.BUY)
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.withSecurityId("78464A672")
				.withSecurityType(SecurityTypes.COMMON_STOCK)
				.withClientOrderStringId("O_237591_20170606150837_CANCEL")
				.withOriginalClientOrderStringId("O_237591_20170606")
				.withHandlingInstructions(HandlingInstructions.AUTOMATIC)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 6, 15, 8, 37).plus(396, ChronoField.MILLI_OF_DAY.getBaseUnit())))
				.withSymbol("78464A672")
				.withFixDestinationName("fixDestination3")
				.toOrderCancelRequest();
	}


	public static FixAllocationInstruction createAllocationInstruction() {
		return FixAllocationInstructionBuilder.createEmptyFixAllocationInstructionBuilder()
				.withSendSecondaryClientOrderId(false)
				.withFixOrderList(
						FixOrderBuilder.createEmptyOrder()
								.withClientOrderStringId("20170627Y510776")
								.toOrder()
				)
				.withFixAllocationDetailList(
						FixAllocationDetailBuilder.createEmptyAllocationDetail()
								.withAllocationAccount("276-9182D")
								.withAllocationShares(new BigDecimal("16.00000000"))
								.withNestedPartyList(
										FixAllocationDetailNestedPartyBuilder.createEmptyFixAllocationDetailNestedParty()
												.withNestedPartyId("CG")
												.withNestedPartyRole(PartyRoles.CLEARING_FIRM)
												.withNestedPartyIDSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
												.toFixAllocationDetailNestedParty()
								)
								.withProcessCode(ProcessCodes.REGULAR)
								.toFixAllocationDetail()
				)
				.withAllocationStringId("A_232350_20170627")
				.withAllocationTransactionType(AllocationTransactionTypes.NEW)
				.withSide(OrderSides.SELL)
				.withShares(new BigDecimal("16.00000000"))
				.withAveragePrice(new BigDecimal("1403.05625000"))
				.withSymbol("RTAU7 Index")
				.withSecurityId("RTAU7 Index")
				.withSecurityExchange("IFUS")
				.withMaturityMonthYear("201709")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withSecurityType(SecurityTypes.FUTURE)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 27, 14, 59, 48).plus(349, ChronoField.MILLI_OF_DAY.getBaseUnit())))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withFutureSettlementDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withSequenceNumber(2404)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTONMB44")
				.withFixSenderSubId("r151412")
				.withSenderUserName("laurenn")
				.withFixTargetCompId("REDITKTS")
				.withFixOnBehalfOfCompID("CLIF")
				.withIncoming(false)
				.withFixDestinationName("fixDestination3")
				.toFixAllocationInstruction();
	}


	public static FixOrderCancelReplaceRequest createOrderCancelReplaceRequest() {
		return FixOrderCancelReplaceRequestBuilder.createEmptyOrderCancelReplaceRequest()
				.withClientOrderStringId("O_207284_20161110")
				.withOriginalClientOrderStringId("O_207254_20161110")
				.withFixAllocationDetailList()
				.withHandlingInstructions(HandlingInstructions.MANUAL)
				.withSide(OrderSides.BUY)
				.withOrderQty(new BigDecimal("62.00000000"))
				.withOrderType(OrderTypes.MARKET)
				.withTimeInForce(TimeInForceTypes.DAY)
				.withAccount("052G09326")
				.withSymbol("COF7")
				.withSecurityId("COF7 Comdty")
				.withSecurityExchange("IFEU")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withSecurityType(SecurityTypes.FUTURE)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2016, 11, 10, 11, 20, 50).plus(668, ChronoField.MILLI_OF_DAY.getBaseUnit())))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2016, 11, 10)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2016, 11, 10)))
				.withSequenceNumber(2571)
				.withBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTONMB44")
				.withSenderSubId("r148907")
				.withFixTargetCompId("REDITKTS")
				.withTargetSubId("DESK")
				.withIncoming(false)
				.toOrderCancelReplacementRequest();
	}


	public static FixAllocationAcknowledgement createAllocationAcknowledgement() {
		return FixAllocationAcknowledgementBuilder.createEmptyFixAllocationAcknowledgement()
				.withFixBeginString("FIX.4.4")
				.withSequenceNumber(4119)
				.withFixSenderCompId("REDITKTS")
				.withFixTargetCompId("CLIFTONMB44")
				.withAllocationStringId("A_232471_20170628")
				.withTradeDate("20170629")
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 28, 18, 5, 57)))
				.withAllocationStatus(AllocationStatuses.ACCEPTED)
				.toFixAllocationAcknowledgement();
	}


	public static FixOrderCancelReject createOrderCancelReject() {
		return FixOrderCancelRejectBuilder.createEmptyFixOrderCancelReject()
				.withSequenceNumber(3637)
				.withClientOrderStringId("O_237057_20170602130442_CANCEL")
				.withOriginalClientOrderStringId("O_237057_20170602")
				.withCancelRejectResponseTo(CancelRejectResponseToItems.ORDER_CANCEL_REQUEST)
				.withCancelRejectReason(CancelRejectReasons.BROKER_EXCHANGE_OPTION)
				.withStatus(OrderStatuses.REJECTED)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("REDITKTS")
				.withFixTargetCompId("CLIFTONMB44")
				.toFixOrderCancelReject();
	}


	public static FixExecutionReport createExecutionReport() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("FUSNYB6645820170628")
				.withClientOrderStringId("O_240893_20170628")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("FUSNYB6957220170628")
				.withOrderStatus(OrderStatuses.PARTIALLY_FILLED)
				.withSide(OrderSides.BUY)
				.withOrderQuantity(new BigDecimal("3135.000000000000000"))
				.withOrderType(OrderTypes.MARKET)
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("259.000000000000000"))
				.withLastPrice(new BigDecimal("155.187500000000000"))
				.withLastMarket("IMME")
				.withCumulativeQuantity(new BigDecimal("2933.000000000000000"))
				.withLeavesQuantity(new BigDecimal("202.000000000000000"))
				.withAveragePrice(new BigDecimal("155.022939400000000"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 28, 13, 59, 55)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 28)))
				.withSymbol("USU7 Comdty")
				.withCFICode("FXXXXX")
				.withMaturityMonthYear("201709")
				.withSecurityType(SecurityTypes.FUTURE)
				.withCurrency("USD")
				.withSettlementCurrency("USD")
				.withSequenceNumber(2096)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("GSFUT")
				.withFixSenderLocId("FUSNYB")
				.withFixTargetCompId("CLIFTON")
				.withCompDealerQuoteList()
				.withFixNoteList()
				.toFixExecutionReport();
	}


	public static FixExecutionReport createExecutionReportWithSubParties() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("BLP")
				.withFixTargetCompId("CLIFTONPARA")
				.withSequenceNumber(708)
				.withOrderId("VCON:20130617:50298:1")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("VCON:20130617:50298:1:12")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.BUY)
				.withOrderQuantity(new BigDecimal("3228000.000000000000000"))
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("3228000.000000000000000"))
				.withLastPrice(new BigDecimal("104.875000000000000"))
				.withIndexRatio(new BigDecimal("0.6323357000"))
				.withCumulativeQuantity(new BigDecimal("3228000.000000000000000"))
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withYield(new BigDecimal("0.017389382"))
				.withAveragePrice(new BigDecimal("104.875000000000000"))
				.withAccruedInterestAmount(new BigDecimal("4309.16"))
				.withNetMoney(new BigDecimal("2144996.31"))
				.withGrossTradeAmount(new BigDecimal("2140687.15"))
				.withCouponRate(new BigDecimal("0.04"))
				.withPriceType(PriceTypes.PERCENTAGE)
				.withSecondaryOrderId("3739:20130617:50298:1")
				.withProduct(Products.MORTGAGE)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2013, 6, 17, 9, 55, 56)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2013, 6, 17)))
				.withMaturityDate(DateUtils.asUtilDate(LocalDate.of(2038, 12, 20)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2013, 6, 20)))
				.withSymbol("GNR")
				.withSecurityId("38376XRB1")
				.withFixOnBehalfOfCompID("i.stpdca4")
				.withSecurityType(SecurityTypes.MORTGAGE_BACKED_SECURITIES)
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.withCurrency("USD")
				.withCompDealerQuoteList()
				.withFixNoteList()
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("JKENNEFICK:444330")
								.withSenderUserName("fixParty2")
								.withPartyRole(PartyRoles.EXECUTING_TRADER)
								.withPartySubList(FixPartySubBuilder.createEmptyFixPartySub()
										.withPartySubId("JACK KENNEFICK")
										.withPartySubIdType(PartySubIDTypes.CONTACT_NAME)
										.toFixPartySub()
								)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("CLIFTON2:27359")
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_TRADER)
								.withPartySubList(FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("27359")
												.withPartySubIdType(PartySubIDTypes.BLOOMBERG_UUID)
												.toFixPartySub(),
										FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("14")
												.withPartySubIdType(PartySubIDTypes.APPLICATION)
												.toFixPartySub(),
										FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("THOMAS LEE")
												.withPartySubIdType(PartySubIDTypes.CONTACT_NAME)
												.toFixPartySub()
								)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("PARAMETRIC PORTFOLIO ASSOCIATES LLC")
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_FIRM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("JKENNEFICK:444330")
								.withSenderUserName("fixParty2")
								.withPartyRole(PartyRoles.ENTERING_TRADER)
								.withPartySubList(FixPartySubBuilder.createEmptyFixPartySub()
										.withPartySubId("JACK KENNEFICK")
										.withPartySubIdType(PartySubIDTypes.CONTACT_NAME)
										.toFixPartySub()
								)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("SXT")
								.withPartyRole(PartyRoles.EXECUTING_SYSTEM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("1883")
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("27359")
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_TRADER)
								.withPartySubList(FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("27359")
												.withPartySubIdType(PartySubIDTypes.BLOOMBERG_UUID)
												.toFixPartySub(),
										FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("14")
												.withPartySubIdType(PartySubIDTypes.APPLICATION)
												.toFixPartySub()
								)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("444330")
								.withPartyRole(PartyRoles.EXECUTING_TRADER)
								.toFixParty()
				)
				.toFixExecutionReport();
	}


	public static FixAllocationInstruction createAllocationInstructionWithExecutionReport() {
		return FixAllocationInstructionBuilder.createEmptyFixAllocationInstructionBuilder()
				.withIncoming(true)
				.withSequenceNumber(2096)
				.withFixOnBehalfOfCompID("CLIF")
				.withFixTargetCompId("REDITKTS")
				.withFixSenderSubId("r151412")
				.withSenderUserName("laurenn")
				.withFixBeginString("FIX.4.4")
				.withSecurityType(SecurityTypes.FUTURE)
				.withFixSenderCompId("CLIFTONMB44")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withMaturityMonthYear("201709")
				.withSecurityExchange("IFUS")
				.withSymbol("RTAU7 Index")
				.withAveragePrice(new BigDecimal("1403.05625000"))
				.withShares(new BigDecimal("16.00000000"))
				.withSide(OrderSides.SELL)
				.withSecurityId("RTAU7 Index")
				.withAllocationTransactionType(AllocationTransactionTypes.NEW)
				.withAllocationStringId("A_232350_20170627")
				.withSendSecondaryClientOrderId(true)
				.withProduct(Products.FINANCING)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 27, 14, 59, 48)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withFutureSettlementDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withMaturityDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 27)))
				.withAllocationNoOrdersType(AllocationNoOrdersTypes.NOT_SPECIFIED)
				.withAllocationType(AllocationTypes.READY_TO_BOOK_SINGLE_ORDER)
				.withAccount("Account")
				.withAllocationCancelReplaceReason(AllocationCancelReplaceReasons.ORIGINAL_DETAILS_INCOMPLETE_INCORRECT)
				.withAllocationLinkStringId("AllocationLinkStringId")
				.withAllocationLinkType(AllocationLinkTypes.F_X_NETTING)
				.withAllocationStatus(AllocationStatuses.BLOCK_LEVEL_REJECT)
				.withAllocationValidationError(false)
				.withCFICode("CFICode")
				.withCurrency("USD")
				.withExDestination("ExDestination")
				.withTenor("Tenor")
				.withFixOrderList(
						FixOrderBuilder.createEmptyOrder()
								.withClientOrderStringId("20170627Y510776")
								.withFixExecutionReport(
										FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
												.withExecutionType(ExecutionTypes.TRADE_CANCEL)
												.withLastShares(new BigDecimal("123.456"))
												.toFixExecutionReport()
								)
								.toOrder()
				)
				.withFixAllocationDetailList(
						FixAllocationDetailBuilder.createEmptyAllocationDetail()
								.withAllocationAccount("276-9182D")
								.withAllocationShares(new BigDecimal("16.00000000"))
								.withNestedPartyList(
										FixAllocationDetailNestedPartyBuilder.createEmptyFixAllocationDetailNestedParty()
												.withNestedPartyId("CG")
												.withNestedPartyRole(PartyRoles.CLEARING_FIRM)
												.withNestedPartyIDSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
												.toFixAllocationDetailNestedParty()
								)
								.withProcessCode(ProcessCodes.REGULAR)
								.toFixAllocationDetail()
				)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyRole(PartyRoles.CLEARING_FIRM)
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyId("343rdlas")
								.withSenderUserName("fixParty")
								.toFixParty()
				)
				.withId(1L)
				.withGrossTradeAmount(new BigDecimal("16.00007854"))
				.withFixSessionQualifier("FixSessionQualifier")
				.withMaturityDay("DAY")
				.withNetMoney(new BigDecimal("1403.77777"))
				.withPriceType(PriceTypes.PRODUCT_TICKS_IN_HALFS)
				.withPutOrCall(PutOrCalls.PUT)
				.withSettlementCurrency("DDD")
				.withStrikePrice(new BigDecimal("7423.05625000"))
				.withFixDestinationName("fixDestination3")
				.toFixAllocationInstruction();
	}


	public static FixDontKnowTrade createDontKnowTrade() {
		return FixDontKnowTradeBuilder.createEmptyFixDontKnowTradeBuilder()
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("GSFUT")
				.withFixTargetCompId("CLIFTON")
				.withFixSenderLocId("FUSNYB")
				.withOrderId("FUSNYB6645820170628")
				.withClientOrderStringId("O_240893_20170628")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("FUSNYB6957220170628")
				.withOrderStatus(OrderStatuses.PARTIALLY_FILLED)
				.withSide(OrderSides.BUY)
				.withOrderQuantity(new BigDecimal("3135.000000000000000"))
				.withOrderType(OrderTypes.MARKET)
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("259.000000000000000"))
				.withLastPrice(new BigDecimal("155.187500000000000"))
				.withLastMarket("IMME")
				.withCumulativeQuantity(new BigDecimal("2933.000000000000000"))
				.withLeavesQuantity(new BigDecimal("202.000000000000000"))
				.withAveragePrice(new BigDecimal("155.022939400000000"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2017, 6, 28, 13, 59, 55)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 28)))
				.withSymbol("USU7 Comdty")
				.withCFICode("FXXXXX")
				.withMaturityMonthYear("201709")
				.withSecurityType(SecurityTypes.FUTURE)
				.withCurrency("USD")
				.withSettlementCurrency("USD")
				.withSequenceNumber(2096)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("GSFUT")
				.withFixSenderLocId("FUSNYB")
				.withFixTargetCompId("CLIFTON")
				.withCompDealerQuoteList()
				.withSecurityId("USU7 Comdty")
				.withSecondaryOrderId("SECONDARY_ORDER_ID")
				.withOriginalClientOrderStringId("ORIGINAL_CLIENT_ORDER_STRING_ID")
				.withSecondaryClientOrderStringId("SECONDARY_CLIENT_ORDER_STRING_ID")
				.withExecutionTransactionType(ExecutionTransactionTypes.CORRECT)
				.withExecutionType(ExecutionTypes.ORDER_STATUS)
				.withExecutionReferenceId("TRANSACTION_REFERENCE")
				.withPrice(new BigDecimal("2222.333"))
				.withOrderCapacity(OrderCapacities.AGENCY)
				.withLastCapacity(LastCapacities.AGENT)
				.withText("TEXT")
				.withPriceType(PriceTypes.FIXED_CABINET_TRADE_PRICE)
				.withNetMoney(new BigDecimal("741.251"))
				.withGrossTradeAmount(new BigDecimal("77.22222"))
				.withAccruedInterestAmount(new BigDecimal("7421.22"))
				.withCouponRate(new BigDecimal("44.55"))
				.withYield(new BigDecimal("6.55"))
				.withConcession(new BigDecimal("7.11"))
				.withIssuer("ISSUER")
				.withProduct(Products.FINANCING)
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2017, 6, 29)))
				.withSettlementCurrency("SCR")
				.withSettlementCurrencyAmount(new BigDecimal("8314.21547"))
				.withIndexRatio(new BigDecimal("1.88"))
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.toFixDontKnowTrade();
	}


	public static FixAllocationInstruction createAllocationInstructionWithProcessCode() {
		return FixAllocationInstructionBuilder.createEmptyFixAllocationInstructionBuilder()
				.withSymbol("ESH5 Index")
				.withSequenceNumber(343)
				.withSide(OrderSides.BUY)
				.withFutureSettlementDate(DateUtils.asUtilDate(LocalDate.of(2015, 1, 9)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2015, 1, 9)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2015, 1, 9)))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2015, 1, 9, 12, 32, 44)))
				.withAllocationStringId("A_116291_20150109")
				.withAllocationTransactionType(AllocationTransactionTypes.NEW)
				.withSecurityId("ESH5 Index")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withFixOrderList(
						FixOrderBuilder.createEmptyOrder()
								.withClientOrderStringId("O_118089_20150109")
								.toOrder()
				)
				.withShares(new BigDecimal("13.0000000000"))
				.withSecurityType(SecurityTypes.FUTURE)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTONMB44")
				.withFixOnBehalfOfCompID("CLIF")
				.withAveragePrice(new BigDecimal("2010.250000000000000000000000000000"))
				.withFixTargetCompId("REDITKTS")
				.withFixAllocationDetailList(
						FixAllocationDetailBuilder.createEmptyAllocationDetail()
								.withProcessCode(ProcessCodes.REGULAR)
								.withAllocationShares(new BigDecimal("6.0000000000"))
								.withAllocationAccount("260-71125")
								.withNestedPartyList()
								.toFixAllocationDetail(),
						FixAllocationDetailBuilder.createEmptyAllocationDetail()
								.withProcessCode(ProcessCodes.REGULAR)
								.withAllocationShares(new BigDecimal("7.0000000000"))
								.withAllocationAccount("157-72887")
								.withNestedPartyList()
								.toFixAllocationDetail()
				)
				.toFixAllocationInstruction();
	}


	public static FixAllocationInstruction createAllocationInstructionWithOutProcessCode() {
		return FixAllocationInstructionBuilder.createEmptyFixAllocationInstructionBuilder()
				.withSymbol("GIV5 Index")
				.withSequenceNumber(1571)
				.withSide(OrderSides.SELL)
				.withFutureSettlementDate(DateUtils.asUtilDate(LocalDate.of(2015, 9, 25)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2015, 9, 25)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2015, 9, 25)))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2015, 9, 25, 9, 56, 23)))
				.withMaturityMonthYear("201510")
				.withAllocationStringId("A_148182_20150925")
				.withAllocationTransactionType(AllocationTransactionTypes.NEW)
				.withSecurityId("GIV5 Index")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withFixOrderList(
						FixOrderBuilder.createEmptyOrder()
								.withClientOrderStringId("O_151921_20150925")
								.toOrder()
				)
				.withShares(new BigDecimal("8.00000000"))
				.withSecurityType(SecurityTypes.FUTURE)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTON")
				.withAveragePrice(new BigDecimal("362.25000000"))
				.withFixTargetCompId("GSFUT")
				.withFixTargetSubId("TKTS")
				.withFixAllocationDetailList(
						FixAllocationDetailBuilder.createEmptyAllocationDetail()
								.withAllocationShares(new BigDecimal("8.00000000"))
								.withAllocationAccount("003-17502")
								.withNestedPartyList()
								.toFixAllocationDetail()
				)
				.toFixAllocationInstruction();
	}


	public static FixExecutionReport createExecutionReportFxConnectDealerQuote() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("FIX-103977671-278494641")
				.withClientOrderStringId("O_100710_20140731")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("APP.e7ec043d-b763-4127-b986-c6bbb6867652")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.SELL)
				.withOrderQuantity(new BigDecimal("1752500.000000000000000"))
				.withOrderType(OrderTypes.MARKET)
				.withProduct(Products.CURRENCY)
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("1752500.000000000000000"))
				.withLastPrice(new BigDecimal("0.908330000000000"))
				.withCumulativeQuantity(new BigDecimal("1752500.000000000000000"))
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withAveragePrice(new BigDecimal("0.908330000000000"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2014, 7, 31, 9, 26, 36).plus(84, ChronoField.MILLI_OF_DAY.getBaseUnit())))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2014, 7, 31)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2014, 9, 17)))
				.withSettlementCurrencyAmount(new BigDecimal("1591848.33"))
				.withSymbol("USD/CHF")
				.withCurrency("USD")
				.withText("COMPETITIVE")
				.withSettlementCurrency("CHF")
				.withSequenceNumber(225)
				.withFixBeginString("FIX.4.4")
				.withFixSenderSubId("bos.clifton.gliebl")
				.withFixSenderCompId("FXCCX")
				.withFixTargetCompId("glfixhub.bos.clifton")
				.withFixNoteList()
				.withCompDealerQuoteList(
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerId("barcap")
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("0.908198"))
								.withCompDealerId("bnym")
								.toFixCompDealerQuote()
				)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("gs")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty()
				)
				.toFixExecutionReport();
	}


	public static FixExecutionReport createExecutionReportDealerQuote() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("1839")
				.withClientOrderStringId("O_100762_20140731")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("GS:20140731:1036:5:12")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.SELL)
				.withOrderQuantity(new BigDecimal("550000.000000000000000"))
				.withProduct(Products.GOVERNMENT)
				.withConcession(BigDecimal.ZERO)
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("550000.000000000000000"))
				.withLastPrice(new BigDecimal("103.973000000000000"))
				.withCumulativeQuantity(new BigDecimal("550000.000000000000000"))
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withAveragePrice(new BigDecimal("103.973000000000000"))
				.withCouponRate(new BigDecimal("0.001"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2014, 7, 31, 8, 56, 33)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2014, 8, 5)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2014, 7, 31)))
				.withMaturityDate(DateUtils.asUtilDate(LocalDate.of(2023, 4, 15)))
				.withGrossTradeAmount(new BigDecimal("593170.12"))
				.withAccruedInterestAmount(new BigDecimal("175.06"))
				.withIndexRatio(new BigDecimal("1.0372800000"))
				.withYield(new BigDecimal("-0.003493195"))
				.withNetMoney(new BigDecimal("593345.18"))
				.withPriceType(PriceTypes.PERCENTAGE)
				.withSymbol("[N/A]")
				.withSecurityType(SecurityTypes.EURO_SOVEREIGNS)
				.withSecurityId("EJ0993182")
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.withCurrency("EUR")
				.withIssuer("DEUTSCHLAND I/L BOND")
				.withSequenceNumber(694)
				.withFixBeginString("FIX.4.4")
				.withFixSenderSubId("6036836")
				.withFixSenderCompId("BLP")
				.withFixTargetCompId("CLIFTONPARA")
				.withFixTargetSubId("6036836")
				.withSecondaryOrderId("50:20140731:1036:5")
				.withFixNoteList()
				.withCompDealerQuoteList(
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("0E-15"))
								.withCompDealerId("3729")
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.PERCENTAGE_OF_PAR)
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("0E-15"))
								.withCompDealerId("3684")
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.PERCENTAGE_OF_PAR)
								.toFixCompDealerQuote()
				)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("CHASKAMP")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_TRADER)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("PARAMETRIC PORTFOLIO ASSOCIATES LLC")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_FIRM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("TSOX")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_SYSTEM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("JBRAIK2:4825030")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ENTERING_TRADER)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("3788")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty()
				)
				.toFixExecutionReport();
	}


	public static FixExecutionReport createExecutionReportDealerQuote2() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("FIX-103977671-278494641")
				.withClientOrderStringId("O_100710_20140731")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("APP.e7ec043d-b763-4127-b986-c6bbb6867652")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.SELL)
				.withOrderQuantity(new BigDecimal("1752500.000000000000000"))
				.withOrderType(OrderTypes.MARKET)
				.withProduct(Products.CURRENCY)
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("1752500.000000000000000"))
				.withLastPrice(new BigDecimal("0.908330000000000"))
				.withCumulativeQuantity(new BigDecimal("1752500.000000000000000"))
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withAveragePrice(new BigDecimal("0.908330000000000"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2014, 7, 31, 9, 26, 36)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2014, 7, 31)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2014, 9, 17)))
				.withSettlementCurrencyAmount(new BigDecimal("1591848.33"))
				.withSymbol("USD/CHF")
				.withCurrency("USD")
				.withText("COMPETITIVE")
				.withSettlementCurrency("CHF")
				.withSequenceNumber(225)
				.withFixBeginString("FIX.4.4")
				.withFixSenderSubId("bos.clifton.gliebl")
				.withFixSenderCompId("FXCCX")
				.withFixTargetCompId("glfixhub.bos.clifton")
				.withCompDealerQuoteList(
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("0.908198"))
								.withCompDealerId("barcap")
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("0.908198"))
								.withCompDealerId("bnym")
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("0.908198"))
								.withCompDealerId("barcap")
								.toFixCompDealerQuote()
				)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("gs")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty()
				)
				.toFixExecutionReport();
	}


	public static FixExecutionReport createExecutionReportEmsxDealerQuote() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("16381201")
				.withOrderType(OrderTypes.MARKET)
				.withClientOrderStringId("O_448263_20200228")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("228187256")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.SELL)
				.withOrderQuantity(new BigDecimal("913"))
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("913.000000000000000"))
				.withLastPrice(new BigDecimal("35.190000000000000"))
				.withCumulativeQuantity(new BigDecimal("913.000000000000000"))
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withAveragePrice(new BigDecimal("35.190000000000000"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2020, 2, 28, 9, 48, 53)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2020, 2, 28)))
				.withSymbol("MXEA")
				.withSecurityType(SecurityTypes.OPTION)
				.withMaturityMonthYear("202003")
				.withMaturityDay("27")
				.withSecurityId("MXEA")
				.withPutOrCall(PutOrCalls.PUT)
				.withStrikePrice(new BigDecimal("1650"))
				.withSecurityExchange("XCBO")
				.withCFICode("OPEXCS")
				.withCurrency("USD")
				.withSequenceNumber(774)
				.withFixBeginString("FIX.4.4")
				.withFixSenderSubId("LSTM")
				.withFixSenderCompId("BLP")
				.withFixTargetCompId("CLFTPARAEMSX")
				.withFixTargetSubId("5422020")
				.withText("SOLD(MXEA US) 913 at 35.19(USD) for ORDER ID # 16381201")
				.withFixNoteList()
				.withCompDealerQuoteList(
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("47.300000000000000"))
								.withCompDealerId("BNPP")
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.PER_UNIT)
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("46.000000000000000"))
								.withCompDealerId("CITI")
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.PER_UNIT)
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("53.000000000000000"))
								.withCompDealerId("CSUS")
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.PER_UNIT)
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePrice(new BigDecimal("48.000000000000000"))
								.withCompDealerId("MSEO")
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.PER_UNIT)
								.toFixCompDealerQuote()
				)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("CSUS")
								.withPartyIdSource(PartyIdSources.DIRECTED_BROKER)
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty()
				)
				.toFixExecutionReport();
	}


	public static FixAllocationReport createAllocationReportBloomberg() {
		return FixAllocationReportBuilder.createEmptyFixAllocationReport()
				.withSequenceNumber(68)
				.withSymbol("[N/A]")
				.withSide(OrderSides.BUY)
				.withAllocationReportType(AllocationReportTypes.SELLSIDE_CALCULATED_USING_PRELIMINARY)
				.withAllocationStringId("A_56488_20130510093917")
				.withAllocationTransactionType(AllocationTransactionTypes.NEW)
				.withSecurityId("N/A")
				.withTradeDate("20130510")
				.withShares(new BigDecimal("2000000.000000000000000"))
				.withAllocationReportId("A_56488_20130510093917")
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("BLP")
				.withFixOnBehalfOfCompID("i.FIXALLOC")
				.withCurrency("USD")
				.withFixTargetCompId("CLIFTONPARA")
				.withAveragePrice(new BigDecimal("99.406250000000000"))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2013, 5, 10, 9, 40, 19)))
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.withAllocationNoOrdersType(AllocationNoOrdersTypes.NOT_SPECIFIED)
				.withFixAllocationReportDetailList(
						FixAllocationReportDetailBuilder.createEmptyFixAllocationReportDetail()
								.withAllocationAccount("SWBF0909002")
								.withAllocationAveragePrice(new BigDecimal("0E-15"))
								.withAllocationShares(new BigDecimal("1000000.000000000000000"))
								.withAllocationNetMoney(new BigDecimal("1000581.830000000000000"))
								.withAllocationGrossTradeAmount(new BigDecimal("0E-15"))
								.withAllocationAccruedInterestAmount(new BigDecimal("0E-15"))
								.toFixAllocationReportDetail(),
						FixAllocationReportDetailBuilder.createEmptyFixAllocationReportDetail()
								.withAllocationAccount("MAC33")
								.withAllocationAveragePrice(new BigDecimal("0E-15"))
								.withAllocationShares(new BigDecimal("1000000.000000000000000"))
								.withAllocationNetMoney(new BigDecimal("1000581.830000000000000"))
								.withAllocationGrossTradeAmount(new BigDecimal("0E-15"))
								.withAllocationAccruedInterestAmount(new BigDecimal("0E-15"))
								.toFixAllocationReportDetail()
				)
				.toFixAllocationReport();
	}


	public static FixExecutionReport createExecutionReportBloomberg() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("625")
				.withClientOrderStringId("O_60142_20130620141002")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("JFF:20130620:751:6:12")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.SELL)
				.withOrderQuantity(new BigDecimal("20000000.000000000000000"))
				.withProduct(Products.GOVERNMENT)
				.withPriceType(PriceTypes.DISCOUNT)
				.withGrossTradeAmount(new BigDecimal("19999975"))
				.withIssuer("TREASURY BILL")
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withLastShares(new BigDecimal("20000000.000000000000000"))
				.withLastPrice(new BigDecimal("0.007500000000000"))
				.withCumulativeQuantity(new BigDecimal("20000000.000000000000000"))
				.withPrice(BigDecimal.ZERO)
				.withConcession(BigDecimal.ZERO)
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withAveragePrice(new BigDecimal("99.999875000000000"))
				.withMaturityDate(DateUtils.asUtilDate(LocalDate.of(2013, 6, 27)))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2013, 6, 20, 14, 11, 44)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2013, 6, 20)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2013, 6, 21)))
				.withAccruedInterestAmount(BigDecimal.ZERO)
				.withSymbol("[N/A]")
				.withCouponRate(BigDecimal.ZERO)
				.withSecurityId("9127956W6")
				.withSecurityType(SecurityTypes.US_TREASURY_BILL)
				.withYield(new BigDecimal("0.0000760418"))
				.withNetMoney(new BigDecimal("19999975"))
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.withCurrency("USD")
				.withSecondaryOrderId("5590:20130620:751:6")
				.withSequenceNumber(1008)
				.withFixBeginString("FIX.4.4")
				.withFixSenderSubId("8948089")
				.withFixSenderCompId("BLP")
				.withFixTargetCompId("CLIFTONPARA")
				.withFixNoteList()
				.withCompDealerQuoteList(
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.DISCOUNT)
								.withCompDealerQuotePrice(new BigDecimal("0.007500000000000"))
								.withCompDealerId("3063")
								.toFixCompDealerQuote(),
						FixCompDealerQuoteBuilder.createEmptyFixCompDealerQuote()
								.withCompDealerQuotePriceType(CompDealerQuotePriceTypes.DISCOUNT)
								.withCompDealerQuotePrice(new BigDecimal("0.008000000000000"))
								.withCompDealerId("1726")
								.toFixCompDealerQuote()
				)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("GLIEBL")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_TRADER)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("PARAMETRIC PORTFOLIO ASSOCIATES LLC")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_FIRM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("TSOX")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_SYSTEM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("TKERESTES3:10031033")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ENTERING_TRADER)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("3684")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty()
				)
				.toFixExecutionReport();
	}


	public static FixExecutionReport createExecutionReportBloombergWithNotes() {
		return FixExecutionReportBuilder.createEmptyFixExecutionReportBuilder()
				.withOrderId("VCON:20210630:50068:21")
				.withExecutionType(ExecutionTypes.TRADE)
				.withExecutionId("VCON:20210630:50068:21:12")
				.withOrderStatus(OrderStatuses.FILLED)
				.withSide(OrderSides.BUY)
				.withOrderQuantity(new BigDecimal("15000.000000000000000"))
				.withProduct(Products.MUNICIPAL)
				.withPriceType(PriceTypes.PERCENTAGE)
				.withGrossTradeAmount(new BigDecimal("17242.5"))
				.withLastPrice(new BigDecimal("114.95"))
				.withLastShares(new BigDecimal("15000.000000000000000"))
				.withLeavesQuantity(new BigDecimal("0E-15"))
				.withAveragePrice(new BigDecimal("114.950000000000000"))
				.withMaturityDate(DateUtils.asUtilDate(LocalDate.of(2027, 7, 1)))
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2021, 6, 30, 9, 55, 49)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2021, 6, 30)))
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2021, 7, 1)))
				.withAccruedInterestAmount(BigDecimal.ZERO)
				.withSymbol("[N/A]")
				.withCouponRate(new BigDecimal("0.03"))
				.withSecurityId("544525ZL5")
				.withSecurityType(SecurityTypes.REVENUE_BONDS)
				.withYield(new BigDecimal("0.0047"))
				.withNetMoney(new BigDecimal("17242.5"))
				.withSecurityIDSource(SecurityIDSources.CUSIP)
				.withCurrency("USD")
				.withSecondaryOrderId("3739:20210630:50068:21")
				.withSequenceNumber(228)
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("MAP_BLP_BETA")
				.withFixTargetCompId("MAP_PARA_BETA")
				.withFixOnBehalfOfCompID("DCSVC_12516_1")
				.withCumulativeQuantity(new BigDecimal("15000.000000000000000"))
				.withCompDealerQuoteList((FixCompDealerQuote[]) null)
				.withTimeInForceType(TimeInForceTypes.DAY)
				.withPartyList(
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("EAVA")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_FIRM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("SMCELREATH2:19728435")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_TRADER)
								.withPartySubList(FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("14")
												.withPartySubIdType(PartySubIDTypes.APPLICATION)
												.toFixPartySub(),
										FixPartySubBuilder.createEmptyFixPartySub()
												.withPartySubId("18798741")
												.withPartySubIdType(PartySubIDTypes.BLOOMBERG_UUID)
												.toFixPartySub())
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("SMCELREATH2:19728435")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_TRADER)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("PARAMETRIC PORTFOLIO ASSOCIATES LLC")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ORDER_ORIGINATION_FIRM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("SXT")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.EXECUTING_SYSTEM)
								.toFixParty(),
						FixPartyBuilder.createEmptyFixParty()
								.withPartyId("SMCELREATH2:19728435")
								.withPartyIdSource(PartyIdSources.PROPRIETARY_CUSTOM_CODE)
								.withPartyRole(PartyRoles.ENTERING_TRADER)
								.toFixParty()
				)
				.withFixNoteList(FixNotesBuilder.createEmptyFixNote()
						.withNoteType(FixNoteTypes.DEALER_NOTES)
						.withNote("* LOS ANGELES CA DEPT O *")
						.toFixNote())
				.toFixExecutionReport();
	}


	public static FixOrder createNewOrder() {
		return FixOrderBuilder.createOrder()
				.withSide(OrderSides.BUY)
				.withHandlingInstructions(HandlingInstructions.AUTOMATIC)
				.withFixTargetSubId("TKTS")
				.withSymbol("ESH3 Index")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withSecurityType(SecurityTypes.FUTURE)
				.withOrderQty(new BigDecimal("10.00000000"))
				.withText("Test 42/nTest 43/nTest")
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTON")
				.withFixTargetCompId("GSFUT")
				.withFixDestinationName("fixDestination1")
				.withSenderUserName("mwacker")
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2012, 10, 17)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2012, 10, 17)))
				.withAccount("123456")
				.withTimeInForce(TimeInForceTypes.DAY)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2012, 10, 17, 0, 0, 0)))
				.withOrderType(OrderTypes.MARKET)
				.withFixAllocationDetailList()
				.toOrder();
	}


	public static FixOrder createNewOrder2() {
		return FixOrderBuilder.createOrder()
				.withSide(OrderSides.BUY)
				.withHandlingInstructions(HandlingInstructions.AUTOMATIC)
				.withFixTargetSubId("TKTS")
				.withSymbol("ESH3 Index")
				.withSecurityIDSource(SecurityIDSources.BID)
				.withSecurityType(SecurityTypes.FUTURE)
				.withOrderQty(new BigDecimal("10.00000000"))
				.withText("Test 42/nTest 43/nTest")
				.withFixBeginString("FIX.4.4")
				.withFixSenderCompId("CLIFTON")
				.withFixTargetCompId("GSFUT")
				.withSenderUserName("mwacker")
				.withFixDestinationName("fixDestination2")
				.withSettlementDate(DateUtils.asUtilDate(LocalDate.of(2012, 10, 17)))
				.withTradeDate(DateUtils.asUtilDate(LocalDate.of(2012, 10, 17)))
				.withAccount("123456")
				.withTimeInForce(TimeInForceTypes.DAY)
				.withTransactionTime(DateUtils.asUtilDate(LocalDateTime.of(2012, 10, 17, 0, 0, 0)))
				.withOrderType(OrderTypes.MARKET)
				.withFixAllocationDetailList()
				.toOrder();
	}


	public static FixIOIEntity createNewFixIOIEntity() {
		FixIOIEntity fixIOIEntity = new FixIOIEntity();
		fixIOIEntity.setSequenceNumber(86780);
		fixIOIEntity.setFixBeginString("FIX.4.4");
		fixIOIEntity.setFixSenderCompId("HTGM_QA_IOI");
		fixIOIEntity.setFixTargetCompId("PARAMETRIC");
		fixIOIEntity.setIoiId("HTGM_IOI_20200819_0000000000");
		fixIOIEntity.setIoiTransactionType("N");
		fixIOIEntity.setSymbol("[N/A]");
		fixIOIEntity.setIoiQuantity("15000");
		fixIOIEntity.setPriceType(PriceTypes.PERCENTAGE);
		fixIOIEntity.setPrice(new BigDecimal("106.41000000"));
		fixIOIEntity.setSecurityId("00037CSG6");
		fixIOIEntity.setSecurityIDSource(SecurityIDSources.CUSIP);
		fixIOIEntity.setProduct(Products.MUNICIPAL);
		fixIOIEntity.setSettlementDate(DateUtils.toDate("20200821", DateUtils.FIX_DATE_FORMAT_INPUT));
		fixIOIEntity.setSide(OrderSides.SELL);
		fixIOIEntity.setTransactionTime(DateUtils.toDate("20200819-14:15:55.966", "yyyyMMdd-HH:mm:ss.SSS"));

		List<FixStipulation> stipulationList = new ArrayList<>();

		FixStipulation stipulation1 = new FixStipulation();
		stipulation1.setStipulationType(StipulationTypes.MINQTY);
		stipulation1.setStipulationValue("5000");
		stipulationList.add(stipulation1);

		FixStipulation stipulation2 = new FixStipulation();
		stipulation2.setStipulationType(StipulationTypes.MININCR);
		stipulation2.setStipulationValue("5000");
		stipulationList.add(stipulation2);

		FixStipulation stipulation3 = new FixStipulation();
		stipulation3.setStipulationType(StipulationTypes.MINTOKEEP);
		stipulation3.setStipulationValue("5000");
		stipulationList.add(stipulation3);

		fixIOIEntity.setStipulationList(stipulationList);

		List<FixParty> fixPartyList = new ArrayList<>();

		FixParty fixParty = new FixParty();
		fixParty.setPartyId("0443HEAD");
		fixParty.setSenderUserName("fixParty2");
		fixParty.setPartyIdSource(PartyIdSources.GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER);
		fixParty.setPartyRole(PartyRoles.EXECUTING_FIRM);
		fixPartyList.add(fixParty);

		fixIOIEntity.setPartyList(fixPartyList);

		return fixIOIEntity;
	}


	public static FixTradeCaptureReport createNewFixTCR() {
		FixTradeCaptureReport fixTradeCaptureReport = new FixTradeCaptureReport();
		fixTradeCaptureReport.setSequenceNumber(10);
		fixTradeCaptureReport.setFixBeginString("FIX.4.4");
		fixTradeCaptureReport.setFixSenderCompId("MAP_BLP_BETA");
		fixTradeCaptureReport.setFixTargetCompId("MAP_PARA_BETA");
		fixTradeCaptureReport.setSymbol("[N/A]");
		fixTradeCaptureReport.setFixTargetSubId("19728435");
		fixTradeCaptureReport.setFixOnBehalfOfCompID("i.VCONSRV");
		fixTradeCaptureReport.setPriceType(PriceTypes.PERCENTAGE);
		fixTradeCaptureReport.setLastShares(new BigDecimal("15000"));
		fixTradeCaptureReport.setLastPrice(new BigDecimal("109"));
		fixTradeCaptureReport.setSecurityId("US97712DXR24");
		fixTradeCaptureReport.setIndexRatio(new BigDecimal("1"));
		fixTradeCaptureReport.setSecurityIDSource(SecurityIDSources.ISIN_NUMBER);
		fixTradeCaptureReport.setProduct(Products.MUNICIPAL);
		fixTradeCaptureReport.setTimeInForceType(TimeInForceTypes.DAY);
		fixTradeCaptureReport.setSecurityType(SecurityTypes.REVENUE_BONDS);
		fixTradeCaptureReport.setSettlementDate(DateUtils.toDate("20210114", DateUtils.FIX_DATE_FORMAT_INPUT));
		fixTradeCaptureReport.setTransactionTime(DateUtils.toDate("20210112-08:36:42", "yyyyMMdd-HH:mm:ss"));
		fixTradeCaptureReport.setYield(new BigDecimal("2.008"));
		fixTradeCaptureReport.setTradeDate(DateUtils.toDate("20210112", DateUtils.FIX_DATE_FORMAT_INPUT));
		fixTradeCaptureReport.setTradeReportId("00005023:3bac:684c:5ffcdeca");
		return fixTradeCaptureReport;
	}


	public static FixIOIEntity createCancelFixIOIEntity() {
		FixIOIEntity fixIOIEntity = new FixIOIEntity();
		fixIOIEntity.setSequenceNumber(78486);
		fixIOIEntity.setFixBeginString("FIX.4.4");
		fixIOIEntity.setFixSenderCompId("HTGM_QA_IOI");
		fixIOIEntity.setFixTargetCompId("PARAMETRIC");
		fixIOIEntity.setIoiId("HTGM_IOI_20200819_0000009042");
		fixIOIEntity.setIoiTransactionType("A");
		fixIOIEntity.setSymbol("[N/A]");
		fixIOIEntity.setIoiQuantity("0");
		fixIOIEntity.setPriceType(PriceTypes.PERCENTAGE);
		fixIOIEntity.setPrice(new BigDecimal("0"));
		fixIOIEntity.setTransactionTime(DateUtils.toDate("20200819-14:09:41.934", "yyyyMMdd-HH:mm:ss.SSS"));

		List<FixParty> fixPartyList = new ArrayList<>();
		FixParty fixParty = new FixParty();
		fixParty.setPartyId("0443HEAD");
		fixParty.setSenderUserName("fixParty2");
		fixParty.setPartyIdSource(PartyIdSources.GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER);
		fixParty.setPartyRole(PartyRoles.EXECUTING_FIRM);
		fixPartyList.add(fixParty);

		fixIOIEntity.setPartyList(fixPartyList);

		return fixIOIEntity;
	}


	public static FixMarketDataRefreshEntity createNewMarketDataRefreshEntity() {
		FixMarketDataRefreshEntity fixMarketDataRefreshEntity = new FixMarketDataRefreshEntity();
		fixMarketDataRefreshEntity.setNumberOfEntries(1);

		fixMarketDataRefreshEntity.setSequenceNumber(81);
		fixMarketDataRefreshEntity.setFixBeginString("FIX.4.4");
		fixMarketDataRefreshEntity.setFixSenderCompId("JPM_CONTRI_UAT");
		fixMarketDataRefreshEntity.setFixTargetCompId("PARAPORT_CONTRI_UAT");

		FixMarketDataRefresh dataRefresh = new FixMarketDataRefresh();

		dataRefresh.setActionType(MarketDataActions.NEW);
		dataRefresh.setEntryType(MarketDataEntryTypes.OFFER);
		dataRefresh.setEntryId("mAMUNIBWIC716235");
		dataRefresh.setSecurityId("59261AQK6");
		dataRefresh.setSecurityIDSource(SecurityIDSources.CUSIP);
		dataRefresh.setProduct(Products.MUNICIPAL);
		dataRefresh.setSecurityType(SecurityTypes.BID_WANTED);
		dataRefresh.setPriceType(PriceTypes.PERCENTAGE);

		dataRefresh.setQuantitySize(new BigDecimal("85000.00"));
		dataRefresh.setAsOfDate(LocalDate.of(2021, 5, 14));
		dataRefresh.setAsOfTime(LocalTime.of(21, 28, 3));
		dataRefresh.setConcessionSchedule("B");
		dataRefresh.setExpiryTime(OffsetDateTime.parse("2021-05-14T21:33Z"));
		dataRefresh.setResponseTime(OffsetDateTime.parse("2021-05-14T21:30Z"));
		dataRefresh.setSettleDate(LocalDate.parse("20210518", DateTimeFormatter.ofPattern(DateUtils.FIX_DATE_FORMAT_INPUT)));
		dataRefresh.setCurrency("USD");
		dataRefresh.setTransactTime(OffsetDateTime.parse("2021-05-14T21:28:03.000000241Z"));

		fixMarketDataRefreshEntity.setData(Lists.newArrayList(dataRefresh));

		return fixMarketDataRefreshEntity;
	}
}
