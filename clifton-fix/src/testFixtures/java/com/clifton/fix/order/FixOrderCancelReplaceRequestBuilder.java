package com.clifton.fix.order;

import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OpenCloses;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;

import java.math.BigDecimal;
import java.util.Date;


public class FixOrderCancelReplaceRequestBuilder extends FixOrderBuilder {


	private FixOrderCancelReplaceRequestBuilder(FixOrderCancelReplaceRequest fixOrderCancelReplaceRequest) {
		super(fixOrderCancelReplaceRequest);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixOrderCancelReplaceRequestBuilder createEmptyOrderCancelReplaceRequest() {
		return new FixOrderCancelReplaceRequestBuilder(new FixOrderCancelReplaceRequest());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderCancelReplaceRequestBuilder withId(Long id) {
		return (FixOrderCancelReplaceRequestBuilder) super.withId(id);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSequenceNumber(int sequenceNumber) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withIdentifier(FixIdentifier identifier) {
		return (FixOrderCancelReplaceRequestBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixBeginString(String fixBeginString) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixOrderCancelReplaceRequestBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withIncoming(boolean incoming) {
		return (FixOrderCancelReplaceRequestBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withValidationError(boolean validationError) {
		return (FixOrderCancelReplaceRequestBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixOrderCancelReplaceRequestBuilder) super.withAllocationValidationError(allocationValidationError);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSymbol(String symbol) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSymbol(symbol);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSecurityId(String securityId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSecurityId(securityId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSecurityExchange(String securityExchange) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSecurityExchange(securityExchange);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withPutOrCall(PutOrCalls putOrCall) {
		return (FixOrderCancelReplaceRequestBuilder) super.withPutOrCall(putOrCall);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withStrikePrice(BigDecimal strikePrice) {
		return (FixOrderCancelReplaceRequestBuilder) super.withStrikePrice(strikePrice);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withCFICode(String CFICode) {
		return (FixOrderCancelReplaceRequestBuilder) super.withCFICode(CFICode);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withMaturityMonthYear(String maturityMonthYear) {
		return (FixOrderCancelReplaceRequestBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withMaturityDay(String maturityDay) {
		return (FixOrderCancelReplaceRequestBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSecurityIDSource(securityIDSource);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSecurityType(SecurityTypes securityType) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSecurityType(securityType);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withCurrency(String currency) {
		return (FixOrderCancelReplaceRequestBuilder) super.withCurrency(currency);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSettlementCurrency(String settlementCurrency) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSettlementCurrency(settlementCurrency);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withMaturityDate(Date maturityDate) {
		return (FixOrderCancelReplaceRequestBuilder) super.withMaturityDate(maturityDate);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withExDestination(String exDestination) {
		return (FixOrderCancelReplaceRequestBuilder) super.withExDestination(exDestination);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTenor(String tenor) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTenor(tenor);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withBeginString(String beginString) {
		return (FixOrderCancelReplaceRequestBuilder) super.withBeginString(beginString);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSenderCompId(String senderCompId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSenderCompId(senderCompId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSenderSubId(String senderSubId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSenderSubId(senderSubId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSenderLocId(String senderLocId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSenderLocId(senderLocId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTargetCompId(String targetCompId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTargetCompId(targetCompId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTargetSubId(String targetSubId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTargetSubId(targetSubId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTargetLocId(String targetLocId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTargetLocId(targetLocId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSessionQualifier(String sessionQualifier) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSessionQualifier(sessionQualifier);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSettlementDate(Date settlementDate) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSettlementDate(settlementDate);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withText(String text) {
		return (FixOrderCancelReplaceRequestBuilder) super.withText(text);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSenderUserName(String senderUserName) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSenderUserName(senderUserName);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withAccount(String account) {
		return (FixOrderCancelReplaceRequestBuilder) super.withAccount(account);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixAllocationDetailList(FixAllocationDetail... fixAllocationDetailList) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixAllocationDetailList(fixAllocationDetailList);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withFixPartyList(FixParty... fixPartyList) {
		return (FixOrderCancelReplaceRequestBuilder) super.withFixPartyList(fixPartyList);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withHandlingInstructions(HandlingInstructions handlingInstructions) {
		return (FixOrderCancelReplaceRequestBuilder) super.withHandlingInstructions(handlingInstructions);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withSide(OrderSides orderSide) {
		return (FixOrderCancelReplaceRequestBuilder) super.withSide(orderSide);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withOpenClose(OpenCloses openClose) {
		return (FixOrderCancelReplaceRequestBuilder) super.withOpenClose(openClose);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withOrderQty(BigDecimal orderQty) {
		return (FixOrderCancelReplaceRequestBuilder) super.withOrderQty(orderQty);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withOrderType(OrderTypes orderType) {
		return (FixOrderCancelReplaceRequestBuilder) super.withOrderType(orderType);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTimeInForce(TimeInForceTypes timeInForce) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTimeInForce(timeInForce);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTransactionTime(Date transactionTime) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTransactionTime(transactionTime);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withMaturityMonthYear(Date maturityMonthYear) {
		return (FixOrderCancelReplaceRequestBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withMaturityDay(Date maturityDay) {
		return (FixOrderCancelReplaceRequestBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withClientOrderStringId(String clientOrderStringId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withClientOrderStringId(clientOrderStringId);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withTradeDate(Date tradeDate) {
		return (FixOrderCancelReplaceRequestBuilder) super.withTradeDate(tradeDate);
	}


	@Override
	public FixOrderCancelReplaceRequestBuilder withOriginalClientOrderStringId(String originalClientOrderStringId) {
		return (FixOrderCancelReplaceRequestBuilder) super.withOriginalClientOrderStringId(originalClientOrderStringId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrderCancelReplaceRequest toOrderCancelReplacementRequest() {
		return (FixOrderCancelReplaceRequest) super.toOrder();
	}
}
