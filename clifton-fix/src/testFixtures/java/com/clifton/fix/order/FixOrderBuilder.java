package com.clifton.fix.order;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OpenCloses;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;

import java.math.BigDecimal;
import java.util.Date;


public class FixOrderBuilder extends FixSecurityEntityBuilder {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixOrderBuilder(FixOrder order) {
		super(order);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixOrderBuilder createEmptyOrder() {
		return new FixOrderBuilder(new FixOrder());
	}


	public static FixOrderBuilder createOrder() {
		return createOrder(142);
	}


	/**
	 * Creates a new Trade: trade.isNewBean() == true
	 */
	public static FixOrderBuilder createNewOrder() {
		return createOrder(null);
	}


	public static FixOrderBuilder createOrder(Integer id) {
		FixOrder innerOrder = new FixOrder();
		if (id != null) {
			innerOrder.setClientOrderStringId(id + "_12312012");
		}
		innerOrder.setFixBeginString("FIX.4.4");
		innerOrder.setFixSenderCompId("CLIFTON");
		innerOrder.setFixTargetCompId("GSFUT");

		innerOrder.setSenderUserName("mwacker");
		innerOrder.setSettlementDate(DateUtils.toDate("10/17/2012"));
		innerOrder.setTradeDate(DateUtils.toDate("10/17/2012"));
		innerOrder.setAccount("123456");
		innerOrder.setTimeInForce(TimeInForceTypes.DAY);
		innerOrder.setTransactionTime(DateUtils.toDate("2012-10-17 00:00:00", DateUtils.DATE_FORMAT_FULL));
		innerOrder.setOrderType(OrderTypes.MARKET);

		return new FixOrderBuilder(innerOrder);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderBuilder withId(Long id) {
		return (FixOrderBuilder) super.withId(id);
	}


	@Override
	public FixOrderBuilder withSequenceNumber(int sequenceNumber) {
		return (FixOrderBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixOrderBuilder withIdentifier(FixIdentifier identifier) {
		return (FixOrderBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixOrderBuilder withFixBeginString(String fixBeginString) {
		return (FixOrderBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixOrderBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixOrderBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixOrderBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixOrderBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixOrderBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixOrderBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixOrderBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixOrderBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixOrderBuilder withFixDestinationName(String destinationName) {
		return (FixOrderBuilder) super.withFixDestinationName(destinationName);
	}


	@Override
	public FixOrderBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixOrderBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixOrderBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixOrderBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixOrderBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixOrderBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixOrderBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixOrderBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixOrderBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixOrderBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixOrderBuilder withIncoming(boolean incoming) {
		return (FixOrderBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixOrderBuilder withValidationError(boolean validationError) {
		return (FixOrderBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixOrderBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixOrderBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderBuilder withSymbol(String symbol) {
		return (FixOrderBuilder) super.withSymbol(symbol);
	}


	@Override
	public FixOrderBuilder withSecurityId(String securityId) {
		return (FixOrderBuilder) super.withSecurityId(securityId);
	}


	@Override
	public FixOrderBuilder withSecurityExchange(String securityExchange) {
		return (FixOrderBuilder) super.withSecurityExchange(securityExchange);
	}


	@Override
	public FixOrderBuilder withPutOrCall(PutOrCalls putOrCall) {
		return (FixOrderBuilder) super.withPutOrCall(putOrCall);
	}


	@Override
	public FixOrderBuilder withStrikePrice(BigDecimal strikePrice) {
		return (FixOrderBuilder) super.withStrikePrice(strikePrice);
	}


	@Override
	public FixOrderBuilder withCFICode(String CFICode) {
		return (FixOrderBuilder) super.withCFICode(CFICode);
	}


	@Override
	public FixOrderBuilder withMaturityMonthYear(String maturityMonthYear) {
		return (FixOrderBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixOrderBuilder withMaturityDay(String maturityDay) {
		return (FixOrderBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixOrderBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		return (FixOrderBuilder) super.withSecurityIDSource(securityIDSource);
	}


	@Override
	public FixOrderBuilder withSecurityType(SecurityTypes securityType) {
		return (FixOrderBuilder) super.withSecurityType(securityType);
	}


	@Override
	public FixOrderBuilder withCurrency(String currency) {
		return (FixOrderBuilder) super.withCurrency(currency);
	}


	@Override
	public FixOrderBuilder withSettlementCurrency(String settlementCurrency) {
		return (FixOrderBuilder) super.withSettlementCurrency(settlementCurrency);
	}


	@Override
	public FixOrderBuilder withMaturityDate(Date maturityDate) {
		return (FixOrderBuilder) super.withMaturityDate(maturityDate);
	}


	@Override
	public FixOrderBuilder withExDestination(String exDestination) {
		return (FixOrderBuilder) super.withExDestination(exDestination);
	}


	@Override
	public FixOrderBuilder withTenor(String tenor) {
		return (FixOrderBuilder) super.withTenor(tenor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrderBuilder buy() {
		getOrder().setSide(OrderSides.BUY);
		return this;
	}


	public FixOrderBuilder sell() {
		getOrder().setSide(OrderSides.SELL);
		return this;
	}


	public FixOrderBuilder addQuantity(BigDecimal quantity) {
		getOrder().setOrderQty(quantity);
		return this;
	}


	public FixOrderBuilder addText(String text) {
		getOrder().setText(text);
		return this;
	}


	public FixOrderBuilder forSecurity(String symbol, SecurityIDSources securityIDSource, SecurityTypes securityType) {
		getOrder().setSymbol(symbol);
		getOrder().setSecurityIDSource(securityIDSource);
		getOrder().setSecurityType(securityType);
		return this;
	}


	public FixOrderBuilder forDestination(HandlingInstructions handlingInstructions, String targetSubId) {
		getOrder().setHandlingInstructions(handlingInstructions);
		getOrder().setFixTargetSubId(targetSubId);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrderBuilder withBeginString(String beginString) {
		getOrder().setFixBeginString(beginString);
		return this;
	}


	public FixOrderBuilder withSenderCompId(String senderCompId) {
		getOrder().setFixSenderCompId(senderCompId);
		return this;
	}


	public FixOrderBuilder withSenderSubId(String senderSubId) {
		getOrder().setFixSenderSubId(senderSubId);
		return this;
	}


	public FixOrderBuilder withSenderLocId(String senderLocId) {
		getOrder().setFixSenderLocId(senderLocId);
		return this;
	}


	public FixOrderBuilder withTargetCompId(String targetCompId) {
		getOrder().setFixTargetCompId(targetCompId);
		return this;
	}


	public FixOrderBuilder withTargetSubId(String targetSubId) {
		getOrder().setFixTargetSubId(targetSubId);
		return this;
	}


	public FixOrderBuilder withTargetLocId(String targetLocId) {
		getOrder().setFixTargetLocId(targetLocId);
		return this;
	}


	public FixOrderBuilder withSessionQualifier(String sessionQualifier) {
		getOrder().setFixSessionQualifier(sessionQualifier);
		return this;
	}


	public FixOrderBuilder withSettlementDate(Date settlementDate) {
		getOrder().setSettlementDate(settlementDate);
		return this;
	}


	public FixOrderBuilder withText(String text) {
		getOrder().setText(text);
		return this;
	}


	public FixOrderBuilder withSenderUserName(String senderUserName) {
		getOrder().setSenderUserName(senderUserName);
		return this;
	}


	public FixOrderBuilder withAccount(String account) {
		getOrder().setAccount(account);
		return this;
	}


	public FixOrderBuilder withFixExecutionReport(FixExecutionReport... fixExecutionReportList) {
		getOrder().setFixExecutionReportList(CollectionUtils.createList(fixExecutionReportList));
		return this;
	}


	public FixOrderBuilder withFixAllocationDetailList(FixAllocationDetail... fixAllocationDetailList) {
		getOrder().setFixAllocationDetailList(CollectionUtils.createList(fixAllocationDetailList));
		return this;
	}


	public FixOrderBuilder withFixPartyList(FixParty... fixPartyList) {
		getOrder().setPartyList(CollectionUtils.createList(fixPartyList));
		return this;
	}


	public FixOrderBuilder withHandlingInstructions(HandlingInstructions handlingInstructions) {
		getOrder().setHandlingInstructions(handlingInstructions);
		return this;
	}


	public FixOrderBuilder withSide(OrderSides orderSide) {
		getOrder().setSide(orderSide);
		return this;
	}


	public FixOrderBuilder withOpenClose(OpenCloses openClose) {
		getOrder().setOpenClose(openClose);
		return this;
	}


	public FixOrderBuilder withOrderQty(BigDecimal orderQty) {
		getOrder().setOrderQty(orderQty);
		return this;
	}


	public FixOrderBuilder withOrderType(OrderTypes orderType) {
		getOrder().setOrderType(orderType);
		return this;
	}


	public FixOrderBuilder withTimeInForce(TimeInForceTypes timeInForce) {
		getOrder().setTimeInForce(timeInForce);
		return this;
	}


	public FixOrderBuilder withTransactionTime(Date transactionTime) {
		getOrder().setTransactionTime(transactionTime);
		return this;
	}


	public FixOrderBuilder withMaturityMonthYear(Date maturityMonthYear) {
		getOrder().setMaturityMonthYear(DateUtils.fromDate(maturityMonthYear, DateUtils.FIX_DATE_FORMAT_INPUT));
		return this;
	}


	public FixOrderBuilder withMaturityDay(Date maturityDay) {
		getOrder().setMaturityDay(DateUtils.fromDate(maturityDay, "dd"));
		return this;
	}


	public FixOrderBuilder withClientOrderStringId(String clientOrderStringId) {
		getOrder().setClientOrderStringId(clientOrderStringId);
		return this;
	}


	public FixOrderBuilder withTradeDate(Date tradeDate) {
		getOrder().setTradeDate(tradeDate);
		return this;
	}


	public FixOrderBuilder withOriginalClientOrderStringId(String originalClientOrderStringId) {
		getOrder().setOriginalClientOrderStringId(originalClientOrderStringId);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrder toOrder() {
		return (FixOrder) super.toFixSecurityEntity();
	}


	private FixOrder getOrder() {
		return (FixOrder) super.toFixSecurityEntity();
	}
}
