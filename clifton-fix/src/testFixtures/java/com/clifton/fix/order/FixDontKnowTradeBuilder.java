package com.clifton.fix.order;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.ExecutionTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.LastCapacities;
import com.clifton.fix.order.beans.OrderCapacities;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;

import java.math.BigDecimal;
import java.util.Date;


public class FixDontKnowTradeBuilder extends FixExecutionReportBuilder {


	private FixDontKnowTradeBuilder(FixDontKnowTrade fixDontKnowTrade) {
		super(fixDontKnowTrade);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixDontKnowTradeBuilder createEmptyFixDontKnowTradeBuilder() {
		return new FixDontKnowTradeBuilder(new FixDontKnowTrade());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixDontKnowTradeBuilder withId(Long id) {
		return (FixDontKnowTradeBuilder) super.withId(id);
	}


	@Override
	public FixDontKnowTradeBuilder withSequenceNumber(int sequenceNumber) {
		return (FixDontKnowTradeBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixDontKnowTradeBuilder withIdentifier(FixIdentifier identifier) {
		return (FixDontKnowTradeBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixDontKnowTradeBuilder withFixBeginString(String fixBeginString) {
		return (FixDontKnowTradeBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixDontKnowTradeBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixDontKnowTradeBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixDontKnowTradeBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixDontKnowTradeBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixDontKnowTradeBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixDontKnowTradeBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixDontKnowTradeBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixDontKnowTradeBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixDontKnowTradeBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixDontKnowTradeBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixDontKnowTradeBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixDontKnowTradeBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixDontKnowTradeBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixDontKnowTradeBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixDontKnowTradeBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixDontKnowTradeBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixDontKnowTradeBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixDontKnowTradeBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixDontKnowTradeBuilder withIncoming(boolean incoming) {
		return (FixDontKnowTradeBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixDontKnowTradeBuilder withValidationError(boolean validationError) {
		return (FixDontKnowTradeBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixDontKnowTradeBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixDontKnowTradeBuilder) super.withAllocationValidationError(allocationValidationError);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixDontKnowTradeBuilder withSymbol(String symbol) {
		return (FixDontKnowTradeBuilder) super.withSymbol(symbol);
	}


	@Override
	public FixDontKnowTradeBuilder withSecurityId(String securityId) {
		return (FixDontKnowTradeBuilder) super.withSecurityId(securityId);
	}


	@Override
	public FixDontKnowTradeBuilder withSecurityExchange(String securityExchange) {
		return (FixDontKnowTradeBuilder) super.withSecurityExchange(securityExchange);
	}


	@Override
	public FixDontKnowTradeBuilder withPutOrCall(PutOrCalls putOrCall) {
		return (FixDontKnowTradeBuilder) super.withPutOrCall(putOrCall);
	}


	@Override
	public FixDontKnowTradeBuilder withStrikePrice(BigDecimal strikePrice) {
		return (FixDontKnowTradeBuilder) super.withStrikePrice(strikePrice);
	}


	@Override
	public FixDontKnowTradeBuilder withCFICode(String CFICode) {
		return (FixDontKnowTradeBuilder) super.withCFICode(CFICode);
	}


	@Override
	public FixDontKnowTradeBuilder withMaturityMonthYear(String maturityMonthYear) {
		return (FixDontKnowTradeBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixDontKnowTradeBuilder withMaturityDay(String maturityDay) {
		return (FixDontKnowTradeBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixDontKnowTradeBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		return (FixDontKnowTradeBuilder) super.withSecurityIDSource(securityIDSource);
	}


	@Override
	public FixDontKnowTradeBuilder withSecurityType(SecurityTypes securityType) {
		return (FixDontKnowTradeBuilder) super.withSecurityType(securityType);
	}


	@Override
	public FixDontKnowTradeBuilder withCurrency(String currency) {
		return (FixDontKnowTradeBuilder) super.withCurrency(currency);
	}


	@Override
	public FixDontKnowTradeBuilder withSettlementCurrency(String settlementCurrency) {
		return (FixDontKnowTradeBuilder) super.withSettlementCurrency(settlementCurrency);
	}


	@Override
	public FixDontKnowTradeBuilder withMaturityDate(Date maturityDate) {
		return (FixDontKnowTradeBuilder) super.withMaturityDate(maturityDate);
	}


	@Override
	public FixDontKnowTradeBuilder withExDestination(String exDestination) {
		return (FixDontKnowTradeBuilder) super.withExDestination(exDestination);
	}


	@Override
	public FixDontKnowTradeBuilder withTenor(String tenor) {
		return (FixDontKnowTradeBuilder) super.withTenor(tenor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixDontKnowTradeBuilder withFixOrder(FixOrder fixOrder) {
		getFixDontKnowTrade().setFixOrder(fixOrder);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withFixOriginalOrder(FixOrder fixOriginalOrder) {
		getFixDontKnowTrade().setFixOriginalOrder(fixOriginalOrder);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withOrderId(String orderId) {
		getFixDontKnowTrade().setOrderId(orderId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withSecondaryOrderId(String secondaryOrderId) {
		getFixDontKnowTrade().setSecondaryOrderId(secondaryOrderId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withClientOrderStringId(String clientOrderStringId) {
		getFixDontKnowTrade().setClientOrderStringId(clientOrderStringId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withSecondaryClientOrderStringId(String secondaryClientOrderStringId) {
		getFixDontKnowTrade().setSecondaryClientOrderStringId(secondaryClientOrderStringId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withOriginalClientOrderStringId(String originalClientOrderStringId) {
		getFixDontKnowTrade().setOriginalClientOrderStringId(originalClientOrderStringId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withExecutionTransactionType(ExecutionTransactionTypes executionTransactionType) {
		getFixDontKnowTrade().setExecutionTransactionType(executionTransactionType);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withExecutionType(ExecutionTypes executionType) {
		getFixDontKnowTrade().setExecutionType(executionType);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withExecutionId(String executionId) {
		getFixDontKnowTrade().setExecutionId(executionId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withExecutionReferenceId(String executionReferenceId) {
		getFixDontKnowTrade().setExecutionReferenceId(executionReferenceId);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withOrderStatus(OrderStatuses orderStatus) {
		getFixDontKnowTrade().setOrderStatus(orderStatus);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withProduct(Products product) {
		getFixDontKnowTrade().setProduct(product);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withSide(OrderSides side) {
		getFixDontKnowTrade().setSide(side);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withOrderQuantity(BigDecimal orderQuantity) {
		getFixDontKnowTrade().setOrderQuantity(orderQuantity);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withOrderType(OrderTypes orderType) {
		getFixDontKnowTrade().setOrderType(orderType);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withPrice(BigDecimal price) {
		getFixDontKnowTrade().setPrice(price);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withTimeInForceType(TimeInForceTypes timeInForceType) {
		getFixDontKnowTrade().setTimeInForceType(timeInForceType);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withOrderCapacity(OrderCapacities orderCapacity) {
		getFixDontKnowTrade().setOrderCapacity(orderCapacity);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withLastShares(BigDecimal lastShares) {
		getFixDontKnowTrade().setLastShares(lastShares);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withLastPrice(BigDecimal lastPrice) {
		getFixDontKnowTrade().setLastPrice(lastPrice);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withLastMarket(String lastMarket) {
		getFixDontKnowTrade().setLastMarket(lastMarket);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withCumulativeQuantity(BigDecimal cumulativeQuantity) {
		getFixDontKnowTrade().setCumulativeQuantity(cumulativeQuantity);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withLeavesQuantity(BigDecimal leavesQuantity) {
		getFixDontKnowTrade().setLeavesQuantity(leavesQuantity);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withAveragePrice(BigDecimal averagePrice) {
		getFixDontKnowTrade().setAveragePrice(averagePrice);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withLastCapacity(LastCapacities lastCapacity) {
		getFixDontKnowTrade().setLastCapacity(lastCapacity);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withTransactionTime(Date transactionTime) {
		getFixDontKnowTrade().setTransactionTime(transactionTime);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withTradeDate(Date tradeDate) {
		getFixDontKnowTrade().setTradeDate(tradeDate);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withSettlementDate(Date settlementDate) {
		getFixDontKnowTrade().setSettlementDate(settlementDate);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withSettlementCurrencyAmount(BigDecimal settlementCurrencyAmount) {
		getFixDontKnowTrade().setSettlementCurrencyAmount(settlementCurrencyAmount);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withText(String text) {
		getFixDontKnowTrade().setText(text);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withAccruedInterestAmount(BigDecimal accruedInterestAmount) {
		getFixDontKnowTrade().setAccruedInterestAmount(accruedInterestAmount);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withNetMoney(BigDecimal netMoney) {
		getFixDontKnowTrade().setNetMoney(netMoney);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withGrossTradeAmount(BigDecimal grossTradeAmount) {
		getFixDontKnowTrade().setGrossTradeAmount(grossTradeAmount);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withCouponRate(BigDecimal couponRate) {
		getFixDontKnowTrade().setCouponRate(couponRate);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withYield(BigDecimal yield) {
		getFixDontKnowTrade().setYield(yield);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withConcession(BigDecimal concession) {
		getFixDontKnowTrade().setConcession(concession);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withIndexRatio(BigDecimal indexRatio) {
		getFixDontKnowTrade().setIndexRatio(indexRatio);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withIssuer(String issuer) {
		getFixDontKnowTrade().setIssuer(issuer);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withPriceType(PriceTypes priceType) {
		getFixDontKnowTrade().setPriceType(priceType);
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withPartyList(FixParty... partyList) {
		getFixDontKnowTrade().setPartyList(CollectionUtils.createList(partyList));
		return this;
	}


	@Override
	public FixDontKnowTradeBuilder withCompDealerQuoteList(FixCompDealerQuote... compDealerQuotes) {
		getFixDontKnowTrade().setCompDealerQuoteList(CollectionUtils.createList(compDealerQuotes));
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixDontKnowTrade getFixDontKnowTrade() {
		return (FixDontKnowTrade) super.toFixExecutionReport();
	}


	public FixDontKnowTrade toFixDontKnowTrade() {
		return (FixDontKnowTrade) super.toFixExecutionReport();
	}
}
