package com.clifton.fix.order;

import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.CancelRejectReasons;
import com.clifton.fix.order.beans.CancelRejectResponseToItems;
import com.clifton.fix.order.beans.OrderStatuses;

import java.util.Date;


public class FixOrderCancelRejectBuilder extends FixEntityBuilder {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixOrderCancelRejectBuilder(FixOrderCancelReject fixOrderCancelReject) {
		super(fixOrderCancelReject);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixOrderCancelRejectBuilder createEmptyFixOrderCancelReject() {
		return new FixOrderCancelRejectBuilder(new FixOrderCancelReject());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderCancelRejectBuilder withId(Long id) {
		return (FixOrderCancelRejectBuilder) super.withId(id);
	}


	@Override
	public FixOrderCancelRejectBuilder withSequenceNumber(int sequenceNumber) {
		return (FixOrderCancelRejectBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixOrderCancelRejectBuilder withIdentifier(FixIdentifier identifier) {
		return (FixOrderCancelRejectBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixBeginString(String fixBeginString) {
		return (FixOrderCancelRejectBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixOrderCancelRejectBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixOrderCancelRejectBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixOrderCancelRejectBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixOrderCancelRejectBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixOrderCancelRejectBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixOrderCancelRejectBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixOrderCancelRejectBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixOrderCancelRejectBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixOrderCancelRejectBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixOrderCancelRejectBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixOrderCancelRejectBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixOrderCancelRejectBuilder withIncoming(boolean incoming) {
		return (FixOrderCancelRejectBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixOrderCancelRejectBuilder withValidationError(boolean validationError) {
		return (FixOrderCancelRejectBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixOrderCancelRejectBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixOrderCancelRejectBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrderCancelRejectBuilder withFixOrder(FixOrder fixOrder) {
		getFixOrderCancelReject().setFixOrder(fixOrder);
		return this;
	}


	public FixOrderCancelRejectBuilder withClientOrderStringId(String clientOrderStringId) {
		getFixOrderCancelReject().setClientOrderStringId(clientOrderStringId);
		return this;
	}


	public FixOrderCancelRejectBuilder withOriginalClientOrderStringId(String originalClientOrderStringId) {
		getFixOrderCancelReject().setOriginalClientOrderStringId(originalClientOrderStringId);
		return this;
	}


	public FixOrderCancelRejectBuilder withCancelRejectResponseTo(CancelRejectResponseToItems cancelRejectResponseTo) {
		getFixOrderCancelReject().setCancelRejectResponseTo(cancelRejectResponseTo);
		return this;
	}


	public FixOrderCancelRejectBuilder withCancelRejectReason(CancelRejectReasons cancelRejectReason) {
		getFixOrderCancelReject().setCancelRejectReason(cancelRejectReason);
		return this;
	}


	public FixOrderCancelRejectBuilder withStatus(OrderStatuses status) {
		getFixOrderCancelReject().setStatus(status);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixOrderCancelReject getFixOrderCancelReject() {
		return (FixOrderCancelReject) super.toFixEntity();
	}


	public FixOrderCancelReject toFixOrderCancelReject() {
		return (FixOrderCancelReject) super.toFixEntity();
	}
}
