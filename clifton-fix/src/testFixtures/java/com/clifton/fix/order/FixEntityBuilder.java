package com.clifton.fix.order;

import com.clifton.fix.identifier.FixIdentifier;

import java.util.Date;


public class FixEntityBuilder {

	private FixEntity fixEntity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixEntityBuilder(FixEntity fixEntity) {
		this.fixEntity = fixEntity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixEntityBuilder createEmptyFixEntity() {
		return new FixEntityBuilder(new FixEntityImpl());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixEntityBuilder withId(Long id) {
		getFixEntity().setId(id);
		return this;
	}


	public FixEntityBuilder withSequenceNumber(int sequenceNumber) {
		getFixEntity().setSequenceNumber(sequenceNumber);
		return this;
	}


	public FixEntityBuilder withIdentifier(FixIdentifier identifier) {
		getFixEntity().setIdentifier(identifier);
		return this;
	}


	public FixEntityBuilder withFixBeginString(String fixBeginString) {
		getFixEntity().setFixBeginString(fixBeginString);
		return this;
	}


	public FixEntityBuilder withFixSenderCompId(String fixSenderCompId) {
		getFixEntity().setFixSenderCompId(fixSenderCompId);
		return this;
	}


	public FixEntityBuilder withFixSenderSubId(String fixSenderSubId) {
		getFixEntity().setFixSenderSubId(fixSenderSubId);
		return this;
	}


	public FixEntityBuilder withFixSenderLocId(String fixSenderLocId) {
		getFixEntity().setFixSenderLocId(fixSenderLocId);
		return this;
	}


	public FixEntityBuilder withFixTargetCompId(String fixTargetCompId) {
		getFixEntity().setFixTargetCompId(fixTargetCompId);
		return this;
	}


	public FixEntityBuilder withFixTargetSubId(String fixTargetSubId) {
		getFixEntity().setFixTargetSubId(fixTargetSubId);
		return this;
	}


	public FixEntityBuilder withFixTargetLocId(String fixTargetLocId) {
		getFixEntity().setFixTargetLocId(fixTargetLocId);
		return this;
	}


	public FixEntityBuilder withFixSessionQualifier(String fixSessionQualifier) {
		getFixEntity().setFixSessionQualifier(fixSessionQualifier);
		return this;
	}


	public FixEntityBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		getFixEntity().setFixOnBehalfOfCompID(fixOnBehalfOfCompID);
		return this;
	}


	public FixEntityBuilder withFixDestinationName(String destinationName) {
		getFixEntity().setFixDestinationName(destinationName);
		return this;
	}


	public FixEntityBuilder withMessageDateAndTime(Date messageDateAndTime) {
		getFixEntity().setMessageDateAndTime(messageDateAndTime);
		return this;
	}


	public FixEntityBuilder withIncoming(boolean incoming) {
		getFixEntity().setIncoming(incoming);
		return this;
	}


	public FixEntityBuilder withValidationError(boolean validationError) {
		getFixEntity().setValidationError(validationError);
		return this;
	}


	public FixEntityBuilder withAllocationValidationError(boolean allocationValidationError) {
		getFixEntity().setAllocationValidationError(allocationValidationError);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixEntity getFixEntity() {
		return this.fixEntity;
	}


	public FixEntity toFixEntity() {
		return this.fixEntity;
	}
}
