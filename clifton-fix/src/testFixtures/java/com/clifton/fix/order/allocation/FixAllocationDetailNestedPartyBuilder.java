package com.clifton.fix.order.allocation;

import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;


public class FixAllocationDetailNestedPartyBuilder {

	private FixAllocationDetailNestedParty fixAllocationDetailNestedParty;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationDetailNestedPartyBuilder(FixAllocationDetailNestedParty fixAllocationDetailNestedParty) {
		this.fixAllocationDetailNestedParty = fixAllocationDetailNestedParty;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixAllocationDetailNestedPartyBuilder createEmptyFixAllocationDetailNestedParty() {
		return new FixAllocationDetailNestedPartyBuilder(new FixAllocationDetailNestedParty());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationDetailNestedPartyBuilder withNestedPartyId(String nestedPartyId) {
		getFixAllocationDetailNestedParty().setNestedPartyId(nestedPartyId);
		return this;
	}


	public FixAllocationDetailNestedPartyBuilder withNestedPartyRole(PartyRoles partyRole) {
		getFixAllocationDetailNestedParty().setNestedPartyRole(partyRole);
		return this;
	}


	public FixAllocationDetailNestedPartyBuilder withNestedPartyIDSource(PartyIdSources partyIDSource) {
		getFixAllocationDetailNestedParty().setNestedPartyIDSource(partyIDSource);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationDetailNestedParty getFixAllocationDetailNestedParty() {
		return this.fixAllocationDetailNestedParty;
	}


	public FixAllocationDetailNestedParty toFixAllocationDetailNestedParty() {
		return this.fixAllocationDetailNestedParty;
	}
}
