package com.clifton.fix.order;

import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;

import java.math.BigDecimal;
import java.util.Date;


public class FixSecurityEntityBuilder extends FixEntityBuilder {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixSecurityEntityBuilder(FixSecurityEntity fixSecurityEntity) {
		super(fixSecurityEntity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixSecurityEntityBuilder createEmptyFixSecurityEntity() {
		return new FixSecurityEntityBuilder(new FixSecurityEntity());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixSecurityEntityBuilder withId(Long id) {
		return (FixSecurityEntityBuilder) super.withId(id);
	}


	@Override
	public FixSecurityEntityBuilder withSequenceNumber(int sequenceNumber) {
		return (FixSecurityEntityBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixSecurityEntityBuilder withIdentifier(FixIdentifier identifier) {
		return (FixSecurityEntityBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixSecurityEntityBuilder withFixBeginString(String fixBeginString) {
		return (FixSecurityEntityBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixSecurityEntityBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixSecurityEntityBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixSecurityEntityBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixSecurityEntityBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixSecurityEntityBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixSecurityEntityBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixSecurityEntityBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixSecurityEntityBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixSecurityEntityBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixSecurityEntityBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixSecurityEntityBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixSecurityEntityBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixSecurityEntityBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixSecurityEntityBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixSecurityEntityBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixSecurityEntityBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixSecurityEntityBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixSecurityEntityBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixSecurityEntityBuilder withIncoming(boolean incoming) {
		return (FixSecurityEntityBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixSecurityEntityBuilder withValidationError(boolean validationError) {
		return (FixSecurityEntityBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixSecurityEntityBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixSecurityEntityBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSecurityEntityBuilder withSymbol(String symbol) {
		getFixSecurityEntity().setSymbol(symbol);
		return this;
	}


	public FixSecurityEntityBuilder withSecurityId(String securityId) {
		getFixSecurityEntity().setSecurityId(securityId);
		return this;
	}


	public FixSecurityEntityBuilder withSecurityExchange(String securityExchange) {
		getFixSecurityEntity().setSecurityExchange(securityExchange);
		return this;
	}


	public FixSecurityEntityBuilder withPutOrCall(PutOrCalls putOrCall) {
		getFixSecurityEntity().setPutOrCall(putOrCall);
		return this;
	}


	public FixSecurityEntityBuilder withStrikePrice(BigDecimal strikePrice) {
		getFixSecurityEntity().setStrikePrice(strikePrice);
		return this;
	}


	public FixSecurityEntityBuilder withCFICode(String CFICode) {
		getFixSecurityEntity().setCFICode(CFICode);
		return this;
	}


	public FixSecurityEntityBuilder withMaturityMonthYear(String maturityMonthYear) {
		getFixSecurityEntity().setMaturityMonthYear(maturityMonthYear);
		return this;
	}


	public FixSecurityEntityBuilder withMaturityDay(String maturityDay) {
		getFixSecurityEntity().setMaturityDay(maturityDay);
		return this;
	}


	public FixSecurityEntityBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		getFixSecurityEntity().setSecurityIDSource(securityIDSource);
		return this;
	}


	public FixSecurityEntityBuilder withSecurityType(SecurityTypes securityType) {
		getFixSecurityEntity().setSecurityType(securityType);
		return this;
	}


	public FixSecurityEntityBuilder withCurrency(String currency) {
		getFixSecurityEntity().setCurrency(currency);
		return this;
	}


	public FixSecurityEntityBuilder withSettlementCurrency(String settlementCurrency) {
		getFixSecurityEntity().setSettlementCurrency(settlementCurrency);
		return this;
	}


	public FixSecurityEntityBuilder withMaturityDate(Date maturityDate) {
		getFixSecurityEntity().setMaturityDate(maturityDate);
		return this;
	}


	public FixSecurityEntityBuilder withExDestination(String exDestination) {
		getFixSecurityEntity().setExDestination(exDestination);
		return this;
	}


	public FixSecurityEntityBuilder withTenor(String tenor) {
		getFixSecurityEntity().setTenor(tenor);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixSecurityEntity getFixSecurityEntity() {
		return (FixSecurityEntity) super.toFixEntity();
	}


	public FixSecurityEntity toFixSecurityEntity() {
		return (FixSecurityEntity) super.toFixEntity();
	}
}
