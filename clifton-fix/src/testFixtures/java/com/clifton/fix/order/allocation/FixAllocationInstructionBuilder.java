package com.clifton.fix.order.allocation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.FixSecurityEntityBuilder;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.beans.AllocationCancelReplaceReasons;
import com.clifton.fix.order.beans.AllocationLinkTypes;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class FixAllocationInstructionBuilder extends FixSecurityEntityBuilder {


	private FixAllocationInstructionBuilder(FixAllocationInstruction fixAllocationInstruction) {
		super(fixAllocationInstruction);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixAllocationInstructionBuilder createEmptyFixAllocationInstructionBuilder() {
		return new FixAllocationInstructionBuilder(new FixAllocationInstruction());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationInstructionBuilder withId(Long id) {
		return (FixAllocationInstructionBuilder) super.withId(id);
	}


	@Override
	public FixAllocationInstructionBuilder withSequenceNumber(int sequenceNumber) {
		return (FixAllocationInstructionBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixAllocationInstructionBuilder withIdentifier(FixIdentifier identifier) {
		return (FixAllocationInstructionBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixAllocationInstructionBuilder withFixBeginString(String fixBeginString) {
		return (FixAllocationInstructionBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixAllocationInstructionBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixAllocationInstructionBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixAllocationInstructionBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixAllocationInstructionBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixAllocationInstructionBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixAllocationInstructionBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixAllocationInstructionBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixAllocationInstructionBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixAllocationInstructionBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixAllocationInstructionBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixAllocationInstructionBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixAllocationInstructionBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixAllocationInstructionBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixAllocationInstructionBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixAllocationInstructionBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixAllocationInstructionBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixAllocationInstructionBuilder withFixDestinationName(String fixDestinationName) {
		return (FixAllocationInstructionBuilder) super.withFixDestinationName(fixDestinationName);
	}


	@Override
	public FixAllocationInstructionBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixAllocationInstructionBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixAllocationInstructionBuilder withIncoming(boolean incoming) {
		return (FixAllocationInstructionBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixAllocationInstructionBuilder withValidationError(boolean validationError) {
		return (FixAllocationInstructionBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixAllocationInstructionBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixAllocationInstructionBuilder) super.withAllocationValidationError(allocationValidationError);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationInstructionBuilder withSymbol(String symbol) {
		return (FixAllocationInstructionBuilder) super.withSymbol(symbol);
	}


	@Override
	public FixAllocationInstructionBuilder withSecurityId(String securityId) {
		return (FixAllocationInstructionBuilder) super.withSecurityId(securityId);
	}


	@Override
	public FixAllocationInstructionBuilder withSecurityExchange(String securityExchange) {
		return (FixAllocationInstructionBuilder) super.withSecurityExchange(securityExchange);
	}


	@Override
	public FixAllocationInstructionBuilder withPutOrCall(PutOrCalls putOrCall) {
		return (FixAllocationInstructionBuilder) super.withPutOrCall(putOrCall);
	}


	@Override
	public FixAllocationInstructionBuilder withStrikePrice(BigDecimal strikePrice) {
		return (FixAllocationInstructionBuilder) super.withStrikePrice(strikePrice);
	}


	@Override
	public FixAllocationInstructionBuilder withCFICode(String CFICode) {
		return (FixAllocationInstructionBuilder) super.withCFICode(CFICode);
	}


	@Override
	public FixAllocationInstructionBuilder withMaturityMonthYear(String maturityMonthYear) {
		return (FixAllocationInstructionBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixAllocationInstructionBuilder withMaturityDay(String maturityDay) {
		return (FixAllocationInstructionBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixAllocationInstructionBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		return (FixAllocationInstructionBuilder) super.withSecurityIDSource(securityIDSource);
	}


	@Override
	public FixAllocationInstructionBuilder withSecurityType(SecurityTypes securityType) {
		return (FixAllocationInstructionBuilder) super.withSecurityType(securityType);
	}


	@Override
	public FixAllocationInstructionBuilder withCurrency(String currency) {
		return (FixAllocationInstructionBuilder) super.withCurrency(currency);
	}


	@Override
	public FixAllocationInstructionBuilder withSettlementCurrency(String settlementCurrency) {
		return (FixAllocationInstructionBuilder) super.withSettlementCurrency(settlementCurrency);
	}


	@Override
	public FixAllocationInstructionBuilder withMaturityDate(Date maturityDate) {
		return (FixAllocationInstructionBuilder) super.withMaturityDate(maturityDate);
	}


	@Override
	public FixAllocationInstructionBuilder withExDestination(String exDestination) {
		return (FixAllocationInstructionBuilder) super.withExDestination(exDestination);
	}


	@Override
	public FixAllocationInstructionBuilder withTenor(String tenor) {
		return (FixAllocationInstructionBuilder) super.withTenor(tenor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationInstructionBuilder withSendSecondaryClientOrderId(boolean sendSecondaryClientOrderId) {
		getFixAllocationInstruction().setSendSecondaryClientOrderId(sendSecondaryClientOrderId);
		return this;
	}


	public FixAllocationInstructionBuilder withFixOrderList(FixOrder... fixOrderList) {
		List<FixOrder> list = CollectionUtils.createList(fixOrderList);
		getFixAllocationInstruction().setFixOrderList(list.isEmpty() ? null : list);
		return this;
	}


	public FixAllocationInstructionBuilder withFixReferenceAllocation(FixAllocationInstruction fixReferenceAllocation) {
		getFixAllocationInstruction().setFixReferenceAllocation(fixReferenceAllocation);
		return this;
	}


	public FixAllocationInstructionBuilder withFixAllocationLink(FixAllocationInstruction fixAllocationLink) {
		getFixAllocationInstruction().setFixAllocationLink(fixAllocationLink);
		return this;
	}


	public FixAllocationInstructionBuilder withFixAllocationDetailList(FixAllocationDetail... fixAllocationDetailList) {
		getFixAllocationInstruction().setFixAllocationDetailList(CollectionUtils.createList(fixAllocationDetailList));
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationStringId(String allocationStringId) {
		getFixAllocationInstruction().setAllocationStringId(allocationStringId);
		return this;
	}


	public FixAllocationInstructionBuilder withReferenceAllocationStringId(String referenceAllocationStringId) {
		getFixAllocationInstruction().setReferenceAllocationStringId(referenceAllocationStringId);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationLinkStringId(String allocationLinkStringId) {
		getFixAllocationInstruction().setAllocationLinkStringId(allocationLinkStringId);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationTransactionType(AllocationTransactionTypes allocationTransactionType) {
		getFixAllocationInstruction().setAllocationTransactionType(allocationTransactionType);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationLinkType(AllocationLinkTypes allocationLinkType) {
		getFixAllocationInstruction().setAllocationLinkType(allocationLinkType);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationType(AllocationTypes allocationType) {
		getFixAllocationInstruction().setAllocationType(allocationType);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationNoOrdersType(AllocationNoOrdersTypes allocationNoOrdersType) {
		getFixAllocationInstruction().setAllocationNoOrdersType(allocationNoOrdersType);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationCancelReplaceReason(AllocationCancelReplaceReasons allocationCancelReplaceReason) {
		getFixAllocationInstruction().setAllocationCancelReplaceReason(allocationCancelReplaceReason);
		return this;
	}


	public FixAllocationInstructionBuilder withAllocationStatus(AllocationStatuses allocationStatus) {
		getFixAllocationInstruction().setAllocationStatus(allocationStatus);
		return this;
	}


	public FixAllocationInstructionBuilder withSenderUserName(String senderUserName) {
		getFixAllocationInstruction().setSenderUserName(senderUserName);
		return this;
	}


	public FixAllocationInstructionBuilder withAccount(String account) {
		getFixAllocationInstruction().setAccount(account);
		return this;
	}


	public FixAllocationInstructionBuilder withSide(OrderSides side) {
		getFixAllocationInstruction().setSide(side);
		return this;
	}


	public FixAllocationInstructionBuilder withShares(BigDecimal shares) {
		getFixAllocationInstruction().setShares(shares);
		return this;
	}


	public FixAllocationInstructionBuilder withAveragePrice(BigDecimal averagePrice) {
		getFixAllocationInstruction().setAveragePrice(averagePrice);
		return this;
	}


	public FixAllocationInstructionBuilder withPriceType(PriceTypes priceType) {
		getFixAllocationInstruction().setPriceType(priceType);
		return this;
	}


	public FixAllocationInstructionBuilder withNetMoney(BigDecimal netMoney) {
		getFixAllocationInstruction().setNetMoney(netMoney);
		return this;
	}


	public FixAllocationInstructionBuilder withGrossTradeAmount(BigDecimal grossTradeAmount) {
		getFixAllocationInstruction().setGrossTradeAmount(grossTradeAmount);
		return this;
	}


	public FixAllocationInstructionBuilder withTradeDate(Date tradeDate) {
		getFixAllocationInstruction().setTradeDate(tradeDate);
		return this;
	}


	public FixAllocationInstructionBuilder withSettlementDate(Date settlementDate) {
		getFixAllocationInstruction().setSettlementDate(settlementDate);
		return this;
	}


	public FixAllocationInstructionBuilder withTransactionTime(Date transactionTime) {
		getFixAllocationInstruction().setTransactionTime(transactionTime);
		return this;
	}


	public FixAllocationInstructionBuilder withFutureSettlementDate(Date futureSettlementDate) {
		getFixAllocationInstruction().setFutureSettlementDate(futureSettlementDate);
		return this;
	}


	public FixAllocationInstructionBuilder withProduct(Products product) {
		getFixAllocationInstruction().setProduct(product);
		return this;
	}


	public FixAllocationInstructionBuilder withPartyList(FixParty... partyList) {
		getFixAllocationInstruction().setPartyList(CollectionUtils.createList(partyList));
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationInstruction getFixAllocationInstruction() {
		return (FixAllocationInstruction) super.toFixSecurityEntity();
	}


	public FixAllocationInstruction toFixAllocationInstruction() {
		return (FixAllocationInstruction) super.toFixSecurityEntity();
	}
}
