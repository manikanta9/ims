package com.clifton.fix.order;

import com.clifton.fix.order.beans.CompDealerMidPriceTypes;
import com.clifton.fix.order.beans.CompDealerQuotePriceTypes;
import com.clifton.fix.order.beans.CompDealerQuoteTypes;

import java.math.BigDecimal;


public class FixCompDealerQuoteBuilder {

	private FixCompDealerQuote fixCompDealerQuote;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixCompDealerQuoteBuilder(FixCompDealerQuote fixCompDealerQuote) {
		this.fixCompDealerQuote = fixCompDealerQuote;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixCompDealerQuoteBuilder createEmptyFixCompDealerQuote() {
		return new FixCompDealerQuoteBuilder(new FixCompDealerQuote());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixCompDealerQuoteBuilder withCompDealerId(String compDealerId) {
		getFixCompDealerQuote().setCompDealerId(compDealerId);
		return this;
	}


	public FixCompDealerQuoteBuilder withCompDealerQuoteTypes(CompDealerQuoteTypes compDealerQuoteType) {
		getFixCompDealerQuote().setCompDealerQuoteType(compDealerQuoteType);
		return this;
	}


	public FixCompDealerQuoteBuilder withCompDealerQuotePrice(BigDecimal compDealerQuotePrice) {
		getFixCompDealerQuote().setCompDealerQuotePrice(compDealerQuotePrice);
		return this;
	}


	public FixCompDealerQuoteBuilder withCompDealerQuotePriceType(CompDealerQuotePriceTypes compDealerQuotePriceType) {
		getFixCompDealerQuote().setCompDealerQuotePriceType(compDealerQuotePriceType);
		return this;
	}


	public FixCompDealerQuoteBuilder withCompDealerParQuote(BigDecimal compDealerParQuote) {
		getFixCompDealerQuote().setCompDealerParQuote(compDealerParQuote);
		return this;
	}


	public FixCompDealerQuoteBuilder withCompDealerMidQuote(BigDecimal compDealerMidQuote) {
		getFixCompDealerQuote().setCompDealerMidPrice(compDealerMidQuote);
		return this;
	}


	public FixCompDealerQuoteBuilder withCompDealerMidPriceType(CompDealerMidPriceTypes compDealerParQuote) {
		getFixCompDealerQuote().setCompDealerMidPriceType(compDealerParQuote);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixCompDealerQuote getFixCompDealerQuote() {
		return this.fixCompDealerQuote;
	}


	public FixCompDealerQuote toFixCompDealerQuote() {
		return this.fixCompDealerQuote;
	}
}
