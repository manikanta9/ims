package com.clifton.fix.order.allocation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.FixEntityBuilder;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationReportTypes;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.SecurityIDSources;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class FixAllocationReportBuilder extends FixEntityBuilder {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixAllocationReportBuilder(FixAllocationReport fixAllocationReport) {
		super(fixAllocationReport);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixAllocationReportBuilder createEmptyFixAllocationReport() {
		return new FixAllocationReportBuilder(new FixAllocationReport());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationReportBuilder withId(Long id) {
		return (FixAllocationReportBuilder) super.withId(id);
	}


	@Override
	public FixAllocationReportBuilder withSequenceNumber(int sequenceNumber) {
		return (FixAllocationReportBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixAllocationReportBuilder withIdentifier(FixIdentifier identifier) {
		return (FixAllocationReportBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixAllocationReportBuilder withFixBeginString(String fixBeginString) {
		return (FixAllocationReportBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixAllocationReportBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixAllocationReportBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixAllocationReportBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixAllocationReportBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixAllocationReportBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixAllocationReportBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixAllocationReportBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixAllocationReportBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixAllocationReportBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixAllocationReportBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixAllocationReportBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixAllocationReportBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixAllocationReportBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixAllocationReportBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixAllocationReportBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixAllocationReportBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixAllocationReportBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixAllocationReportBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixAllocationReportBuilder withIncoming(boolean incoming) {
		return (FixAllocationReportBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixAllocationReportBuilder withValidationError(boolean validationError) {
		return (FixAllocationReportBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixAllocationReportBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixAllocationReportBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationReportBuilder withFixAllocationInstruction(FixAllocationInstruction fixAllocationInstruction) {
		getFixAllocationReport().setFixAllocationInstruction(fixAllocationInstruction);
		return this;
	}


	public FixAllocationReportBuilder withAllocationStringId(String allocationStringId) {
		getFixAllocationReport().setAllocationStringId(allocationStringId);
		return this;
	}


	public FixAllocationReportBuilder withAllocationReportId(String allocationReportId) {
		getFixAllocationReport().setAllocationReportId(allocationReportId);
		return this;
	}


	public FixAllocationReportBuilder withAllocationStatus(AllocationStatuses allocationStatus) {
		getFixAllocationReport().setAllocationStatus(allocationStatus);
		return this;
	}


	public FixAllocationReportBuilder withAllocationReportType(AllocationReportTypes allocationReportType) {
		getFixAllocationReport().setAllocationReportType(allocationReportType);
		return this;
	}


	public FixAllocationReportBuilder withAllocationTransactionType(AllocationTransactionTypes allocationTransactionType) {
		getFixAllocationReport().setAllocationTransactionType(allocationTransactionType);
		return this;
	}


	public FixAllocationReportBuilder withAllocationNoOrdersType(AllocationNoOrdersTypes allocationNoOrdersType) {
		getFixAllocationReport().setAllocationNoOrdersType(allocationNoOrdersType);
		return this;
	}


	public FixAllocationReportBuilder withSide(OrderSides side) {
		getFixAllocationReport().setSide(side);
		return this;
	}


	public FixAllocationReportBuilder withSymbol(String symbol) {
		getFixAllocationReport().setSymbol(symbol);
		return this;
	}


	public FixAllocationReportBuilder withSecurityId(String securityId) {
		getFixAllocationReport().setSecurityId(securityId);
		return this;
	}


	public FixAllocationReportBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		getFixAllocationReport().setSecurityIDSource(securityIDSource);
		return this;
	}


	public FixAllocationReportBuilder withMaturityMonthYear(String maturityMonthYear) {
		getFixAllocationReport().setMaturityMonthYear(maturityMonthYear);
		return this;
	}


	public FixAllocationReportBuilder withShares(BigDecimal shares) {
		getFixAllocationReport().setShares(shares);
		return this;
	}


	public FixAllocationReportBuilder withAveragePrice(BigDecimal averagePrice) {
		getFixAllocationReport().setAveragePrice(averagePrice);
		return this;
	}


	public FixAllocationReportBuilder withCurrency(String currency) {
		getFixAllocationReport().setCurrency(currency);
		return this;
	}


	public FixAllocationReportBuilder withTradeDate(String tradeDate) {
		getFixAllocationReport().setTradeDate(tradeDate);
		return this;
	}


	public FixAllocationReportBuilder withTransactionTime(Date transactionTime) {
		getFixAllocationReport().setTransactionTime(transactionTime);
		return this;
	}


	public FixAllocationReportBuilder withText(String text) {
		getFixAllocationReport().setText(text);
		return this;
	}


	public FixAllocationReportBuilder withFixAllocationReportDetailList(FixAllocationReportDetail... fixAllocationReportDetailList) {
		List<FixAllocationReportDetail> values = CollectionUtils.createList(fixAllocationReportDetailList);
		getFixAllocationReport().setFixAllocationReportDetailList(values);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationReport getFixAllocationReport() {
		return (FixAllocationReport) super.toFixEntity();
	}


	public FixAllocationReport toFixAllocationReport() {
		return (FixAllocationReport) super.toFixEntity();
	}
}
