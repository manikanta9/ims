package com.clifton.fix.order;

import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.ExecutionTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.LastCapacities;
import com.clifton.fix.order.beans.OrderCapacities;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;

import java.math.BigDecimal;
import java.util.Date;


public class FixExecutionReportBuilder extends FixSecurityEntityBuilder {


	protected FixExecutionReportBuilder(FixExecutionReport fixExecutionReport) {
		super(fixExecutionReport);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixExecutionReportBuilder createEmptyFixExecutionReportBuilder() {
		return new FixExecutionReportBuilder(new FixExecutionReport());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixExecutionReportBuilder withId(Long id) {
		return (FixExecutionReportBuilder) super.withId(id);
	}


	@Override
	public FixExecutionReportBuilder withSequenceNumber(int sequenceNumber) {
		return (FixExecutionReportBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixExecutionReportBuilder withIdentifier(FixIdentifier identifier) {
		return (FixExecutionReportBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixExecutionReportBuilder withFixBeginString(String fixBeginString) {
		return (FixExecutionReportBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixExecutionReportBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixExecutionReportBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixExecutionReportBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixExecutionReportBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixExecutionReportBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixExecutionReportBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixExecutionReportBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixExecutionReportBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixExecutionReportBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixExecutionReportBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixExecutionReportBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixExecutionReportBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixExecutionReportBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixExecutionReportBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixExecutionReportBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixExecutionReportBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixExecutionReportBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixExecutionReportBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixExecutionReportBuilder withIncoming(boolean incoming) {
		return (FixExecutionReportBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixExecutionReportBuilder withValidationError(boolean validationError) {
		return (FixExecutionReportBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixExecutionReportBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixExecutionReportBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixExecutionReportBuilder withSymbol(String symbol) {
		return (FixExecutionReportBuilder) super.withSymbol(symbol);
	}


	@Override
	public FixExecutionReportBuilder withSecurityId(String securityId) {
		return (FixExecutionReportBuilder) super.withSecurityId(securityId);
	}


	@Override
	public FixExecutionReportBuilder withSecurityExchange(String securityExchange) {
		return (FixExecutionReportBuilder) super.withSecurityExchange(securityExchange);
	}


	@Override
	public FixExecutionReportBuilder withPutOrCall(PutOrCalls putOrCall) {
		return (FixExecutionReportBuilder) super.withPutOrCall(putOrCall);
	}


	@Override
	public FixExecutionReportBuilder withStrikePrice(BigDecimal strikePrice) {
		return (FixExecutionReportBuilder) super.withStrikePrice(strikePrice);
	}


	@Override
	public FixExecutionReportBuilder withCFICode(String CFICode) {
		return (FixExecutionReportBuilder) super.withCFICode(CFICode);
	}


	@Override
	public FixExecutionReportBuilder withMaturityMonthYear(String maturityMonthYear) {
		return (FixExecutionReportBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixExecutionReportBuilder withMaturityDay(String maturityDay) {
		return (FixExecutionReportBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixExecutionReportBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		return (FixExecutionReportBuilder) super.withSecurityIDSource(securityIDSource);
	}


	@Override
	public FixExecutionReportBuilder withSecurityType(SecurityTypes securityType) {
		return (FixExecutionReportBuilder) super.withSecurityType(securityType);
	}


	@Override
	public FixExecutionReportBuilder withCurrency(String currency) {
		return (FixExecutionReportBuilder) super.withCurrency(currency);
	}


	@Override
	public FixExecutionReportBuilder withSettlementCurrency(String settlementCurrency) {
		return (FixExecutionReportBuilder) super.withSettlementCurrency(settlementCurrency);
	}


	@Override
	public FixExecutionReportBuilder withMaturityDate(Date maturityDate) {
		return (FixExecutionReportBuilder) super.withMaturityDate(maturityDate);
	}


	@Override
	public FixExecutionReportBuilder withExDestination(String exDestination) {
		return (FixExecutionReportBuilder) super.withExDestination(exDestination);
	}


	@Override
	public FixExecutionReportBuilder withTenor(String tenor) {
		return (FixExecutionReportBuilder) super.withTenor(tenor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixExecutionReportBuilder withFixOrder(FixOrder fixOrder) {
		getFixExecutionReport().setFixOrder(fixOrder);
		return this;
	}


	public FixExecutionReportBuilder withFixOriginalOrder(FixOrder fixOriginalOrder) {
		getFixExecutionReport().setFixOriginalOrder(fixOriginalOrder);
		return this;
	}


	public FixExecutionReportBuilder withOrderId(String orderId) {
		getFixExecutionReport().setOrderId(orderId);
		return this;
	}


	public FixExecutionReportBuilder withSecondaryOrderId(String secondaryOrderId) {
		getFixExecutionReport().setSecondaryOrderId(secondaryOrderId);
		return this;
	}


	public FixExecutionReportBuilder withClientOrderStringId(String clientOrderStringId) {
		getFixExecutionReport().setClientOrderStringId(clientOrderStringId);
		return this;
	}


	public FixExecutionReportBuilder withSecondaryClientOrderStringId(String secondaryClientOrderStringId) {
		getFixExecutionReport().setSecondaryClientOrderStringId(secondaryClientOrderStringId);
		return this;
	}


	public FixExecutionReportBuilder withOriginalClientOrderStringId(String originalClientOrderStringId) {
		getFixExecutionReport().setOriginalClientOrderStringId(originalClientOrderStringId);
		return this;
	}


	public FixExecutionReportBuilder withExecutionTransactionType(ExecutionTransactionTypes executionTransactionType) {
		getFixExecutionReport().setExecutionTransactionType(executionTransactionType);
		return this;
	}


	public FixExecutionReportBuilder withExecutionType(ExecutionTypes executionType) {
		getFixExecutionReport().setExecutionType(executionType);
		return this;
	}


	public FixExecutionReportBuilder withExecutionId(String executionId) {
		getFixExecutionReport().setExecutionId(executionId);
		return this;
	}


	public FixExecutionReportBuilder withExecutionReferenceId(String executionReferenceId) {
		getFixExecutionReport().setExecutionReferenceId(executionReferenceId);
		return this;
	}


	public FixExecutionReportBuilder withOrderStatus(OrderStatuses orderStatus) {
		getFixExecutionReport().setOrderStatus(orderStatus);
		return this;
	}


	public FixExecutionReportBuilder withProduct(Products product) {
		getFixExecutionReport().setProduct(product);
		return this;
	}


	public FixExecutionReportBuilder withSide(OrderSides side) {
		getFixExecutionReport().setSide(side);
		return this;
	}


	public FixExecutionReportBuilder withOrderQuantity(BigDecimal orderQuantity) {
		getFixExecutionReport().setOrderQuantity(orderQuantity);
		return this;
	}


	public FixExecutionReportBuilder withOrderType(OrderTypes orderType) {
		getFixExecutionReport().setOrderType(orderType);
		return this;
	}


	public FixExecutionReportBuilder withPrice(BigDecimal price) {
		getFixExecutionReport().setPrice(price);
		return this;
	}


	public FixExecutionReportBuilder withTimeInForceType(TimeInForceTypes timeInForceType) {
		getFixExecutionReport().setTimeInForceType(timeInForceType);
		return this;
	}


	public FixExecutionReportBuilder withOrderCapacity(OrderCapacities orderCapacity) {
		getFixExecutionReport().setOrderCapacity(orderCapacity);
		return this;
	}


	public FixExecutionReportBuilder withLastShares(BigDecimal lastShares) {
		getFixExecutionReport().setLastShares(lastShares);
		return this;
	}


	public FixExecutionReportBuilder withLastPrice(BigDecimal lastPrice) {
		getFixExecutionReport().setLastPrice(lastPrice);
		return this;
	}


	public FixExecutionReportBuilder withLastMarket(String lastMarket) {
		getFixExecutionReport().setLastMarket(lastMarket);
		return this;
	}


	public FixExecutionReportBuilder withCumulativeQuantity(BigDecimal cumulativeQuantity) {
		getFixExecutionReport().setCumulativeQuantity(cumulativeQuantity);
		return this;
	}


	public FixExecutionReportBuilder withLeavesQuantity(BigDecimal leavesQuantity) {
		getFixExecutionReport().setLeavesQuantity(leavesQuantity);
		return this;
	}


	public FixExecutionReportBuilder withAveragePrice(BigDecimal averagePrice) {
		getFixExecutionReport().setAveragePrice(averagePrice);
		return this;
	}


	public FixExecutionReportBuilder withLastCapacity(LastCapacities lastCapacity) {
		getFixExecutionReport().setLastCapacity(lastCapacity);
		return this;
	}


	public FixExecutionReportBuilder withTransactionTime(Date transactionTime) {
		getFixExecutionReport().setTransactionTime(transactionTime);
		return this;
	}


	public FixExecutionReportBuilder withTradeDate(Date tradeDate) {
		getFixExecutionReport().setTradeDate(tradeDate);
		return this;
	}


	public FixExecutionReportBuilder withSettlementDate(Date settlementDate) {
		getFixExecutionReport().setSettlementDate(settlementDate);
		return this;
	}


	public FixExecutionReportBuilder withSettlementCurrencyAmount(BigDecimal settlementCurrencyAmount) {
		getFixExecutionReport().setSettlementCurrencyAmount(settlementCurrencyAmount);
		return this;
	}


	public FixExecutionReportBuilder withText(String text) {
		getFixExecutionReport().setText(text);
		return this;
	}


	public FixExecutionReportBuilder withAccruedInterestAmount(BigDecimal accruedInterestAmount) {
		getFixExecutionReport().setAccruedInterestAmount(accruedInterestAmount);
		return this;
	}


	public FixExecutionReportBuilder withNetMoney(BigDecimal netMoney) {
		getFixExecutionReport().setNetMoney(netMoney);
		return this;
	}


	public FixExecutionReportBuilder withGrossTradeAmount(BigDecimal grossTradeAmount) {
		getFixExecutionReport().setGrossTradeAmount(grossTradeAmount);
		return this;
	}


	public FixExecutionReportBuilder withCouponRate(BigDecimal couponRate) {
		getFixExecutionReport().setCouponRate(couponRate);
		return this;
	}


	public FixExecutionReportBuilder withYield(BigDecimal yield) {
		getFixExecutionReport().setYield(yield);
		return this;
	}


	public FixExecutionReportBuilder withConcession(BigDecimal concession) {
		getFixExecutionReport().setConcession(concession);
		return this;
	}


	public FixExecutionReportBuilder withIndexRatio(BigDecimal indexRatio) {
		getFixExecutionReport().setIndexRatio(indexRatio);
		return this;
	}


	public FixExecutionReportBuilder withIssuer(String issuer) {
		getFixExecutionReport().setIssuer(issuer);
		return this;
	}


	public FixExecutionReportBuilder withPriceType(PriceTypes priceType) {
		getFixExecutionReport().setPriceType(priceType);
		return this;
	}


	public FixExecutionReportBuilder withPartyList(FixParty... partyList) {
		getFixExecutionReport().setPartyList(CollectionUtils.createList(partyList));
		return this;
	}


	public FixExecutionReportBuilder withCompDealerQuoteList(FixCompDealerQuote... compDealerQuotes) {
		getFixExecutionReport().setCompDealerQuoteList(CollectionUtils.createList(compDealerQuotes));
		return this;
	}


	public FixExecutionReportBuilder withFixNoteList(FixNote... fixNotes) {
		getFixExecutionReport().setFixNotes(CollectionUtils.createList(fixNotes));
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixExecutionReport getFixExecutionReport() {
		return (FixExecutionReport) super.toFixSecurityEntity();
	}


	public FixExecutionReport toFixExecutionReport() {
		return (FixExecutionReport) super.toFixSecurityEntity();
	}
}
