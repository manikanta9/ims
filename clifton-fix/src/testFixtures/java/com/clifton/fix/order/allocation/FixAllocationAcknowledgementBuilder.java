package com.clifton.fix.order.allocation;

import com.clifton.fix.order.FixEntityBuilder;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.AllocationRejectCodes;
import com.clifton.fix.order.beans.AllocationStatuses;

import java.util.Date;


public class FixAllocationAcknowledgementBuilder extends FixEntityBuilder {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixAllocationAcknowledgementBuilder(FixAllocationAcknowledgement fixAllocationAcknowledgement) {
		super(fixAllocationAcknowledgement);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixAllocationAcknowledgementBuilder createEmptyFixAllocationAcknowledgement() {
		return new FixAllocationAcknowledgementBuilder(new FixAllocationAcknowledgement());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixAllocationAcknowledgementBuilder withId(Long id) {
		return (FixAllocationAcknowledgementBuilder) super.withId(id);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withSequenceNumber(int sequenceNumber) {
		return (FixAllocationAcknowledgementBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withIdentifier(FixIdentifier identifier) {
		return (FixAllocationAcknowledgementBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixBeginString(String fixBeginString) {
		return (FixAllocationAcknowledgementBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixAllocationAcknowledgementBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixAllocationAcknowledgementBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixAllocationAcknowledgementBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixAllocationAcknowledgementBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixAllocationAcknowledgementBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixAllocationAcknowledgementBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixAllocationAcknowledgementBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixAllocationAcknowledgementBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixAllocationAcknowledgementBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withIncoming(boolean incoming) {
		return (FixAllocationAcknowledgementBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withValidationError(boolean validationError) {
		return (FixAllocationAcknowledgementBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixAllocationAcknowledgementBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixAllocationAcknowledgementBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationAcknowledgementBuilder withFixAllocationInstruction(FixAllocationInstruction fixAllocationInstruction) {
		getFixAllocationAcknowledgement().setFixAllocationInstruction(fixAllocationInstruction);
		return this;
	}


	public FixAllocationAcknowledgementBuilder withAllocationStringId(String allocationStringId) {
		getFixAllocationAcknowledgement().setAllocationStringId(allocationStringId);
		return this;
	}


	public FixAllocationAcknowledgementBuilder withTradeDate(String tradeDate) {
		getFixAllocationAcknowledgement().setTradeDate(tradeDate);
		return this;
	}


	public FixAllocationAcknowledgementBuilder withTransactionTime(Date transactionTime) {
		getFixAllocationAcknowledgement().setTransactionTime(transactionTime);
		return this;
	}


	public FixAllocationAcknowledgementBuilder withAllocationStatus(AllocationStatuses allocationStatus) {
		getFixAllocationAcknowledgement().setAllocationStatus(allocationStatus);
		return this;
	}


	public FixAllocationAcknowledgementBuilder withAllocationRejectCode(AllocationRejectCodes allocationRejectCode) {
		getFixAllocationAcknowledgement().setAllocationRejectCode(allocationRejectCode);
		return this;
	}


	public FixAllocationAcknowledgementBuilder withText(String text) {
		getFixAllocationAcknowledgement().setText(text);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FixAllocationAcknowledgement getFixAllocationAcknowledgement() {
		return (FixAllocationAcknowledgement) super.toFixEntity();
	}


	public FixAllocationAcknowledgement toFixAllocationAcknowledgement() {
		return (FixAllocationAcknowledgement) super.toFixEntity();
	}
}
