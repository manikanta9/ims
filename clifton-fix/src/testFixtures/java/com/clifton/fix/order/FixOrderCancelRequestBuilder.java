package com.clifton.fix.order;

import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;

import java.math.BigDecimal;
import java.util.Date;


public class FixOrderCancelRequestBuilder extends FixSecurityEntityBuilder {


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected FixOrderCancelRequestBuilder(FixOrderCancelRequest orderCancelRequest) {
		super(orderCancelRequest);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixOrderCancelRequestBuilder createEmptyOrderCancelRequest() {
		return new FixOrderCancelRequestBuilder(new FixOrderCancelRequest());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderCancelRequestBuilder withId(Long id) {
		return (FixOrderCancelRequestBuilder) super.withId(id);
	}


	@Override
	public FixOrderCancelRequestBuilder withSequenceNumber(int sequenceNumber) {
		return (FixOrderCancelRequestBuilder) super.withSequenceNumber(sequenceNumber);
	}


	@Override
	public FixOrderCancelRequestBuilder withIdentifier(FixIdentifier identifier) {
		return (FixOrderCancelRequestBuilder) super.withIdentifier(identifier);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixBeginString(String fixBeginString) {
		return (FixOrderCancelRequestBuilder) super.withFixBeginString(fixBeginString);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixSenderCompId(String fixSenderCompId) {
		return (FixOrderCancelRequestBuilder) super.withFixSenderCompId(fixSenderCompId);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixSenderSubId(String fixSenderSubId) {
		return (FixOrderCancelRequestBuilder) super.withFixSenderSubId(fixSenderSubId);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixSenderLocId(String fixSenderLocId) {
		return (FixOrderCancelRequestBuilder) super.withFixSenderLocId(fixSenderLocId);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixTargetCompId(String fixTargetCompId) {
		return (FixOrderCancelRequestBuilder) super.withFixTargetCompId(fixTargetCompId);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixTargetSubId(String fixTargetSubId) {
		return (FixOrderCancelRequestBuilder) super.withFixTargetSubId(fixTargetSubId);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixTargetLocId(String fixTargetLocId) {
		return (FixOrderCancelRequestBuilder) super.withFixTargetLocId(fixTargetLocId);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixSessionQualifier(String fixSessionQualifier) {
		return (FixOrderCancelRequestBuilder) super.withFixSessionQualifier(fixSessionQualifier);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		return (FixOrderCancelRequestBuilder) super.withFixOnBehalfOfCompID(fixOnBehalfOfCompID);
	}


	@Override
	public FixOrderCancelRequestBuilder withFixDestinationName(String fixDestinationName) {
		return (FixOrderCancelRequestBuilder) super.withFixDestinationName(fixDestinationName);
	}


	@Override
	public FixOrderCancelRequestBuilder withMessageDateAndTime(Date messageDateAndTime) {
		return (FixOrderCancelRequestBuilder) super.withMessageDateAndTime(messageDateAndTime);
	}


	@Override
	public FixOrderCancelRequestBuilder withIncoming(boolean incoming) {
		return (FixOrderCancelRequestBuilder) super.withIncoming(incoming);
	}


	@Override
	public FixOrderCancelRequestBuilder withValidationError(boolean validationError) {
		return (FixOrderCancelRequestBuilder) super.withValidationError(validationError);
	}


	@Override
	public FixOrderCancelRequestBuilder withAllocationValidationError(boolean allocationValidationError) {
		return (FixOrderCancelRequestBuilder) super.withAllocationValidationError(allocationValidationError);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FixOrderCancelRequestBuilder withSymbol(String symbol) {
		return (FixOrderCancelRequestBuilder) super.withSymbol(symbol);
	}


	@Override
	public FixOrderCancelRequestBuilder withSecurityId(String securityId) {
		return (FixOrderCancelRequestBuilder) super.withSecurityId(securityId);
	}


	@Override
	public FixOrderCancelRequestBuilder withSecurityExchange(String securityExchange) {
		return (FixOrderCancelRequestBuilder) super.withSecurityExchange(securityExchange);
	}


	@Override
	public FixOrderCancelRequestBuilder withPutOrCall(PutOrCalls putOrCall) {
		return (FixOrderCancelRequestBuilder) super.withPutOrCall(putOrCall);
	}


	@Override
	public FixOrderCancelRequestBuilder withStrikePrice(BigDecimal strikePrice) {
		return (FixOrderCancelRequestBuilder) super.withStrikePrice(strikePrice);
	}


	@Override
	public FixOrderCancelRequestBuilder withCFICode(String CFICode) {
		return (FixOrderCancelRequestBuilder) super.withCFICode(CFICode);
	}


	@Override
	public FixOrderCancelRequestBuilder withMaturityMonthYear(String maturityMonthYear) {
		return (FixOrderCancelRequestBuilder) super.withMaturityMonthYear(maturityMonthYear);
	}


	@Override
	public FixOrderCancelRequestBuilder withMaturityDay(String maturityDay) {
		return (FixOrderCancelRequestBuilder) super.withMaturityDay(maturityDay);
	}


	@Override
	public FixOrderCancelRequestBuilder withSecurityIDSource(SecurityIDSources securityIDSource) {
		return (FixOrderCancelRequestBuilder) super.withSecurityIDSource(securityIDSource);
	}


	@Override
	public FixOrderCancelRequestBuilder withSecurityType(SecurityTypes securityType) {
		return (FixOrderCancelRequestBuilder) super.withSecurityType(securityType);
	}


	@Override
	public FixOrderCancelRequestBuilder withCurrency(String currency) {
		return (FixOrderCancelRequestBuilder) super.withCurrency(currency);
	}


	@Override
	public FixOrderCancelRequestBuilder withSettlementCurrency(String settlementCurrency) {
		return (FixOrderCancelRequestBuilder) super.withSettlementCurrency(settlementCurrency);
	}


	@Override
	public FixOrderCancelRequestBuilder withMaturityDate(Date maturityDate) {
		return (FixOrderCancelRequestBuilder) super.withMaturityDate(maturityDate);
	}


	@Override
	public FixOrderCancelRequestBuilder withExDestination(String exDestination) {
		return (FixOrderCancelRequestBuilder) super.withExDestination(exDestination);
	}


	@Override
	public FixOrderCancelRequestBuilder withTenor(String tenor) {
		return (FixOrderCancelRequestBuilder) super.withTenor(tenor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrderCancelRequestBuilder withFixOriginalOrder(FixOrder fixOriginalOrder) {
		getOrderCancelRequestCancelRequest().setFixOriginalOrder(fixOriginalOrder);
		return this;
	}


	public FixOrderCancelRequestBuilder withClientOrderStringId(String clientOrderStringId) {
		getOrderCancelRequestCancelRequest().setClientOrderStringId(clientOrderStringId);
		return this;
	}


	public FixOrderCancelRequestBuilder withOriginalClientOrderStringId(String originalClientOrderStringId) {
		getOrderCancelRequestCancelRequest().setOriginalClientOrderStringId(originalClientOrderStringId);
		return this;
	}


	public FixOrderCancelRequestBuilder withSenderUserName(String senderUserName) {
		getOrderCancelRequestCancelRequest().setSenderUserName(senderUserName);
		return this;
	}


	public FixOrderCancelRequestBuilder withHandlingInstructions(HandlingInstructions handlingInstructions) {
		getOrderCancelRequestCancelRequest().setHandlingInstructions(handlingInstructions);
		return this;
	}


	public FixOrderCancelRequestBuilder withTransactionTime(Date transactionTime) {
		getOrderCancelRequestCancelRequest().setTransactionTime(transactionTime);
		return this;
	}


	public FixOrderCancelRequestBuilder withOrderQty(BigDecimal orderQty) {
		getOrderCancelRequestCancelRequest().setOrderQty(orderQty);
		return this;
	}


	public FixOrderCancelRequestBuilder withSide(OrderSides side) {
		getOrderCancelRequestCancelRequest().setSide(side);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrderCancelRequest toOrderCancelRequest() {
		return (FixOrderCancelRequest) super.toFixSecurityEntity();
	}


	private FixOrderCancelRequest getOrderCancelRequestCancelRequest() {
		return (FixOrderCancelRequest) super.toFixSecurityEntity();
	}
}
