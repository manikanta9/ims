package com.clifton.fix.session;

import java.util.Date;


/**
 * <code>FixSessionBuilder</code> used for building FixSession entities for testing purposes.
 */
public class FixSessionBuilder {

	private FixSession fixSession;


	private FixSessionBuilder(FixSession fixSession) {
		this.fixSession = fixSession;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixSessionBuilder createDefined(short id, FixSessionDefinition fixSessionDefinition) {
		FixSession fixSession = new FixSession();

		fixSession.setId(id);
		fixSession.setDefinition(fixSessionDefinition);
		fixSession.setCreationDateAndTime(new Date());
		fixSession.setIncomingSequenceNumber(1);
		fixSession.setOutgoingSequenceNumber(1);
		fixSession.setActiveSession(true);

		return new FixSessionBuilder(fixSession);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionBuilder withDefinition(FixSessionDefinition definition) {
		this.fixSession.setDefinition(definition);
		return this;
	}


	public FixSessionBuilder withCreationDateAndTime(Date creationDateAndTime) {
		this.fixSession.setCreationDateAndTime(creationDateAndTime);
		return this;
	}


	public FixSessionBuilder withIncomingSequenceNumber(int incomingSequenceNumber) {
		this.fixSession.setIncomingSequenceNumber(incomingSequenceNumber);
		return this;
	}


	public FixSessionBuilder withOutgoingSequenceNumber(int outgoingSequenceNumber) {
		this.fixSession.setOutgoingSequenceNumber(outgoingSequenceNumber);
		return this;
	}


	public FixSessionBuilder withActiveSession(boolean active) {
		this.fixSession.setActiveSession(active);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSession toFixSession() {
		return this.fixSession;
	}
}
