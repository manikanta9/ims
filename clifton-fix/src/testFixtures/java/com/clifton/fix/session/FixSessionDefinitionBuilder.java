package com.clifton.fix.session;

import java.util.List;


/**
 * <code>FixSessionDefinitionBuilder</code> used for building FixSessionDefinition entities for testing purposes.
 */
public class FixSessionDefinitionBuilder {

	private final FixSessionDefinition fixSessionDefinition;


	private FixSessionDefinitionBuilder(FixSessionDefinition fixSessionDefinition) {
		this.fixSessionDefinition = fixSessionDefinition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixSessionDefinitionBuilder createDefault() {
		FixSessionDefinition fixSessionDefinition = new FixSessionDefinition();

		fixSessionDefinition.setId((short) 4);
		fixSessionDefinition.setName("Default Session");
		fixSessionDefinition.setDisabled(false);
		fixSessionDefinition.setCreateSession(false);
		fixSessionDefinition.setServerEnvironment("LOCAL");
		fixSessionDefinition.setConnectionType("initiator");
		fixSessionDefinition.setTimeZone("America/Chicago");
		fixSessionDefinition.setHeartbeatInterval(30);
		fixSessionDefinition.setReconnectInterval(5);
		fixSessionDefinition.setDefaultApplVerId("FIX.4.4");
		fixSessionDefinition.setWeeklySession(false);

		return new FixSessionDefinitionBuilder(fixSessionDefinition);
	}


	public static FixSessionDefinitionBuilder createOther(String name, String senderCompId, String targetCompId) {
		FixSessionDefinition fixSessionDefinition = new FixSessionDefinition();

		fixSessionDefinition.setId((short) 5);
		fixSessionDefinition.setParent(FixSessionDefinitionBuilder.createDefault().toFixSessionDefinition());
		fixSessionDefinition.setName(name);
		fixSessionDefinition.setDisabled(false);
		fixSessionDefinition.setCreateSession(true);
		fixSessionDefinition.setServerEnvironment("LOCAL");
		fixSessionDefinition.setStartTime("16:33:00 US/Central");
		fixSessionDefinition.setEndTime("16:30:00 US/Central");
		fixSessionDefinition.setBeginString("FIX.4.4");
		fixSessionDefinition.setSenderCompId(senderCompId);
		fixSessionDefinition.setTargetCompId(targetCompId);
		fixSessionDefinition.setSocketConnectHost("192.168.1.1");
		fixSessionDefinition.setSocketConnectPort("1111");
		fixSessionDefinition.setStartWeekday(1);
		fixSessionDefinition.setEndWeekday(6);
		fixSessionDefinition.setWeeklySession(false);

		return new FixSessionDefinitionBuilder(fixSessionDefinition);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinitionBuilder withServerEnvironment(String serverEnvironment) {
		this.fixSessionDefinition.setServerEnvironment(serverEnvironment);
		return this;
	}


	public FixSessionDefinitionBuilder withMessageQueue(String messageQueue) {
		this.fixSessionDefinition.setMessageQueue(messageQueue);
		return this;
	}


	public FixSessionDefinitionBuilder withConnectionType(String connectionType) {
		this.fixSessionDefinition.setConnectionType(connectionType);
		return this;
	}


	public FixSessionDefinitionBuilder withTimeZone(String timeZone) {
		this.fixSessionDefinition.setTimeZone(timeZone);
		return this;
	}


	public FixSessionDefinitionBuilder withStartTime(String startTime) {
		this.fixSessionDefinition.setStartTime(startTime);
		return this;
	}


	public FixSessionDefinitionBuilder withEndTime(String endTime) {
		this.fixSessionDefinition.setEndTime(endTime);
		return this;
	}


	public FixSessionDefinitionBuilder withHeartbeatInterval(Integer heartbeatInterval) {
		this.fixSessionDefinition.setHeartbeatInterval(heartbeatInterval);
		return this;
	}


	public FixSessionDefinitionBuilder withReconnectInterval(Integer reconnectInterval) {
		this.fixSessionDefinition.setReconnectInterval(reconnectInterval);
		return this;
	}


	public FixSessionDefinitionBuilder withBeginString(String beginString) {
		this.fixSessionDefinition.setBeginString(beginString);
		return this;
	}


	public FixSessionDefinitionBuilder withSenderCompId(String senderCompId) {
		this.fixSessionDefinition.setSenderCompId(senderCompId);
		return this;
	}


	public FixSessionDefinitionBuilder withSenderSubId(String senderSubId) {
		this.fixSessionDefinition.setSenderSubId(senderSubId);
		return this;
	}


	public FixSessionDefinitionBuilder withSenderLocId(String senderLocId) {
		this.fixSessionDefinition.setSenderLocId(senderLocId);
		return this;
	}


	public FixSessionDefinitionBuilder withTargetCompId(String targetCompId) {
		this.fixSessionDefinition.setTargetCompId(targetCompId);
		return this;
	}


	public FixSessionDefinitionBuilder withTargetSubId(String targetSubId) {
		this.fixSessionDefinition.setTargetSubId(targetSubId);
		return this;
	}


	public FixSessionDefinitionBuilder withTargetLocId(String targetLocId) {
		this.fixSessionDefinition.setTargetLocId(targetLocId);
		return this;
	}


	public FixSessionDefinitionBuilder withSessionQualifier(String sessionQualifier) {
		this.fixSessionDefinition.setSessionQualifier(sessionQualifier);
		return this;
	}


	public FixSessionDefinitionBuilder withSocketConnectHost(String socketConnectHost) {
		this.fixSessionDefinition.setSocketConnectHost(socketConnectHost);
		return this;
	}


	public FixSessionDefinitionBuilder withSocketConnectPort(String socketConnectPort) {
		this.fixSessionDefinition.setSocketConnectPort(socketConnectPort);
		return this;
	}


	public FixSessionDefinitionBuilder withSettingList(List<FixSessionDefinitionSetting> settingList) {
		this.fixSessionDefinition.setSettingList(settingList);
		return this;
	}


	public FixSessionDefinitionBuilder withPathToExecutable(String pathToExecutable) {
		this.fixSessionDefinition.setPathToExecutable(pathToExecutable);
		return this;
	}


	public FixSessionDefinitionBuilder withDefaultApplVerId(String defaultApplVerID) {
		this.fixSessionDefinition.setDefaultApplVerId(defaultApplVerID);
		return this;
	}


	public FixSessionDefinitionBuilder withExecutableArgument(String executableArgument) {
		this.fixSessionDefinition.setExecutableArgument(executableArgument);
		return this;
	}


	public FixSessionDefinitionBuilder withDisabled(boolean disabled) {
		this.fixSessionDefinition.setDisabled(disabled);
		return this;
	}


	public FixSessionDefinitionBuilder withParent(FixSessionDefinition parent) {
		this.fixSessionDefinition.setParent(parent);
		return this;
	}


	public FixSessionDefinitionBuilder withCreateSession(boolean createSession) {
		this.fixSessionDefinition.setCreateSession(createSession);
		return this;
	}


	public FixSessionDefinitionBuilder withStartWeekday(Integer startWeekday) {
		this.fixSessionDefinition.setStartWeekday(startWeekday);
		return this;
	}


	public FixSessionDefinitionBuilder withEndWeekday(Integer endWeekday) {
		this.fixSessionDefinition.setEndWeekday(endWeekday);
		return this;
	}


	public FixSessionDefinitionBuilder withWeeklySession(boolean isWeeklySession) {
		this.fixSessionDefinition.setWeeklySession(isWeeklySession);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinition toFixSessionDefinition() {
		return this.fixSessionDefinition;
	}
}
