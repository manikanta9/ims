package com.clifton.fix.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.messaging.MessageTypes;


/**
 * @author manderson
 */
public class FixSourceSystemSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "systemTag,name", sortField = "name")
	private String searchPattern;

	@SearchField
	private String systemTag;

	@SearchField
	private String name;

	@SearchField
	private String description;


	@SearchField
	private String requestQueueName;

	@SearchField
	private String responseQueueName;

	@SearchField
	private Boolean serializeEntity;

	@SearchField
	private MessageTypes messageFormat;

	@SearchField(searchField = "messageSenderFactoryProviderBean.id")
	private Integer messageSenderFactoryProviderBeanId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getSystemTag() {
		return this.systemTag;
	}


	public void setSystemTag(String systemTag) {
		this.systemTag = systemTag;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getRequestQueueName() {
		return this.requestQueueName;
	}


	public void setRequestQueueName(String requestQueueName) {
		this.requestQueueName = requestQueueName;
	}


	public String getResponseQueueName() {
		return this.responseQueueName;
	}


	public void setResponseQueueName(String responseQueueName) {
		this.responseQueueName = responseQueueName;
	}


	public Boolean getSerializeEntity() {
		return this.serializeEntity;
	}


	public void setSerializeEntity(Boolean serializeEntity) {
		this.serializeEntity = serializeEntity;
	}


	public MessageTypes getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(MessageTypes messageFormat) {
		this.messageFormat = messageFormat;
	}


	public Integer getMessageSenderFactoryProviderBeanId() {
		return this.messageSenderFactoryProviderBeanId;
	}


	public void setMessageSenderFactoryProviderBeanId(Integer messageSenderFactoryProviderBeanId) {
		this.messageSenderFactoryProviderBeanId = messageSenderFactoryProviderBeanId;
	}
}
