package com.clifton.fix.setup;

import com.clifton.fix.setup.search.FixSourceSystemSearchForm;

import java.util.List;


/**
 * @author manderson
 */
public interface FixSetupService {


	public FixSourceSystem getFixSourceSystem(short id);


	public FixSourceSystem getFixSourceSystemByName(String name);


	public FixSourceSystem getFixSourceSystemByTag(String sourceSystemTag);


	public List<FixSourceSystem> getFixSourceSystemList(FixSourceSystemSearchForm searchForm);


	public FixSourceSystem saveFixSourceSystem(FixSourceSystem bean);


	public void deleteFixSourceSystem(short id);
}
