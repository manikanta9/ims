package com.clifton.fix.setup;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.system.bean.SystemBean;


/**
 * @author manderson
 */
@CacheByName
public class FixSourceSystem extends NamedEntityWithoutLabel<Short> implements LabeledObject {

	/**
	 * Source system tag that can be found on messages that link them to the system
	 */
	private String systemTag;

	/**
	 * Incoming queue where external source system sends to FIX application to process
	 */
	private String requestQueueName;

	/**
	 * Outgoing queue where FIX application sends responses to external source system to process
	 */
	private String responseQueueName;


	/**
	 * If <code>true</code>, messages produced for this source system will be serialized {@link FixEntity} objects. Otherwise, messages will be serialized {@link
	 * FixMessageTextMessagingMessage} objects.
	 * <p>
	 * This flag is used to control the location of {@link FixMessageTextMessagingMessage#getMessageText() Fix message text} to {@link FixEntity} conversion. Historic source systems, such as
	 * IMS, are designed to convert the Fix message text into {@link FixEntity} objects internally, using Fix APIs, but new services typically do not have this functionality.
	 * Enabling this flag provides support for these new services which are unable to perform this conversion.
	 */
	private boolean serializeEntity;

	/**
	 * The format which should be used by default for messages sent to this source system.
	 */
	private MessageTypes messageFormat;

	/**
	 * Message Sender Type where FIX application will be the sending the message to. It could be SQS or Kafka or anything
	 */
	private SystemBean messageSenderFactoryProviderBean;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSourceSystem() {
		// NOTHING HERE
	}


	public FixSourceSystem(String systemTag) {
		this();
		this.systemTag = systemTag;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getName() + " (System Tag: " + getSystemTag() + ")";
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSystemTag() {
		return this.systemTag;
	}


	public void setSystemTag(String systemTag) {
		this.systemTag = systemTag;
	}


	public String getRequestQueueName() {
		return this.requestQueueName;
	}


	public void setRequestQueueName(String requestQueueName) {
		this.requestQueueName = requestQueueName;
	}


	public String getResponseQueueName() {
		return this.responseQueueName;
	}


	public void setResponseQueueName(String responseQueueName) {
		this.responseQueueName = responseQueueName;
	}


	public boolean isSerializeEntity() {
		return this.serializeEntity;
	}


	public void setSerializeEntity(boolean serializeEntity) {
		this.serializeEntity = serializeEntity;
	}


	public MessageTypes getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(MessageTypes messageFormat) {
		this.messageFormat = messageFormat;
	}


	public SystemBean getMessageSenderFactoryProviderBean() {
		return this.messageSenderFactoryProviderBean;
	}


	public void setMessageSenderFactoryProviderBean(SystemBean messageSenderFactoryProviderBean) {
		this.messageSenderFactoryProviderBean = messageSenderFactoryProviderBean;
	}
}
