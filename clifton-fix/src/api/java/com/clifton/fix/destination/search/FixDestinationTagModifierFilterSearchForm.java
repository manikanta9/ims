package com.clifton.fix.destination.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * A search form for searching for FixDestinationTagModifierFilter entities.
 *
 * @author davidi
 */

public class FixDestinationTagModifierFilterSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer fixDestinationTagModifierId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer fixTagModifierFilterId;

	@SearchField(searchField = "referenceOne.referenceOne.id")
	private Short fixDestinationId;

	@SearchField(searchField = "referenceOne.referenceTwo.id")
	private Integer fixTagModifierId;

	@SearchField(searchField = "referenceOne.referenceTwo.name")
	private String fixTagModifierBeanName;

	@SearchField(searchField = "referenceTwo.fieldTag")
	private Integer filterFieldTag;

	@SearchField(searchField = "referenceTwo.fieldValue")
	private String filterFieldValue;

	@SearchField(searchField = "referenceOne.order")
	private Integer fixDestinationTagModifierOrder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getFixDestinationTagModifierId() {
		return this.fixDestinationTagModifierId;
	}


	public void setFixDestinationTagModifierId(Integer fixDestinationTagModifierId) {
		this.fixDestinationTagModifierId = fixDestinationTagModifierId;
	}


	public Integer getFixTagModifierFilterId() {
		return this.fixTagModifierFilterId;
	}


	public void setFixTagModifierFilterId(Integer fixTagModifierFilterId) {
		this.fixTagModifierFilterId = fixTagModifierFilterId;
	}


	public Short getFixDestinationId() {
		return this.fixDestinationId;
	}


	public void setFixDestinationId(Short fixDestinationId) {
		this.fixDestinationId = fixDestinationId;
	}


	public Integer getFixTagModifierId() {
		return this.fixTagModifierId;
	}


	public void setFixTagModifierId(Integer fixTagModifierId) {
		this.fixTagModifierId = fixTagModifierId;
	}


	public String getFixTagModifierBeanName() {
		return this.fixTagModifierBeanName;
	}


	public void setFixTagModifierBeanName(String fixTagModifierBeanName) {
		this.fixTagModifierBeanName = fixTagModifierBeanName;
	}


	public Integer getFilterFieldTag() {
		return this.filterFieldTag;
	}


	public void setFilterFieldTag(Integer filterFieldTag) {
		this.filterFieldTag = filterFieldTag;
	}


	public String getFilterFieldValue() {
		return this.filterFieldValue;
	}


	public void setFilterFieldValue(String filterFieldValue) {
		this.filterFieldValue = filterFieldValue;
	}


	public Integer getFixDestinationTagModifierOrder() {
		return this.fixDestinationTagModifierOrder;
	}


	public void setFixDestinationTagModifierOrder(Integer fixDestinationTagModifierOrder) {
		this.fixDestinationTagModifierOrder = fixDestinationTagModifierOrder;
	}
}
