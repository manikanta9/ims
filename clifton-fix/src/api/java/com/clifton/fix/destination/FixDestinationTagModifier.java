package com.clifton.fix.destination;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.comparison.Ordered;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>FixDestinationTagModifier</code> links FixTagModifiers to specific FixDestinations and
 * defines the order that the modifiers run in.
 *
 * @author davidi
 */
public class FixDestinationTagModifier extends ManyToManyEntity<FixDestination, SystemBean, Integer> implements Ordered {

	//many to many
	/**
	 * Defines the order that the tag modifier will be run in.
	 */
	private int order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
