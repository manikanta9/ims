package com.clifton.fix.destination;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.security.system.SecuritySystem;


/**
 * The <code>FixDestination</code> defines a FIX destination and the associated <code>FixSessionDefinition</code>
 * for that destination. If an incoming FIX message specifies a destination name, the system will look up the associated
 * <code>FixSessionDefinition</code> in order to get the correct session information.
 *
 * @author lnaylor
 */
@CacheByName
public class FixDestination extends NamedEntity<Short> {

	private FixSessionDefinition sessionDefinition;

	/**
	 * When true, this destination may be used as the destination of a FixIdentifier, if the identifier of a message has no destination and the same
	 * session definition as this destination. Some messages (i.e. drop copies) may not be correlated with any previous outbound messages from which
	 * they could get the correct destination. In this case, the default destination for the message's session would be attached before it is sent to
	 * the appropriate source system.
	 */
	private boolean defaultDestination;

	/**
	 * Used to map our user (Trader) names to the corresponding name used by the external system/provider.
	 */
	private SecuritySystem securitySystem;

	/**
	 * When false, FIX messages will fail to send if there is no user alias mapped to the given {@link FixEntity#getSenderUserName()}
	 */
	private boolean allowAnonymousAccess;

	/**
	 * Used when allowAnonymousAccess is set to true. If true, FIX messages will be sent populating the {@link FixEntity#getFixSenderSubId()} with the
	 * {@link FixEntity#getSenderUserName()} if there is no user alias mapped to the given {@link FixEntity#getSenderUserName()}. Otherwise the FIX message
	 * will be sent with a null {@link FixEntity#getFixSenderSubId()}
	 */
	private boolean useUsernameForAnonymousAccess;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinition getSessionDefinition() {
		return this.sessionDefinition;
	}


	public void setSessionDefinition(FixSessionDefinition sessionDefinition) {
		this.sessionDefinition = sessionDefinition;
	}


	public boolean isDefaultDestination() {
		return this.defaultDestination;
	}


	public void setDefaultDestination(boolean defaultDestination) {
		this.defaultDestination = defaultDestination;
	}


	public SecuritySystem getSecuritySystem() {
		return this.securitySystem;
	}


	public void setSecuritySystem(SecuritySystem securitySystem) {
		this.securitySystem = securitySystem;
	}


	public boolean isAllowAnonymousAccess() {
		return this.allowAnonymousAccess;
	}


	public void setAllowAnonymousAccess(boolean allowAnonymousAccess) {
		this.allowAnonymousAccess = allowAnonymousAccess;
	}


	public boolean isUseUsernameForAnonymousAccess() {
		return this.useUsernameForAnonymousAccess;
	}


	public void setUseUsernameForAnonymousAccess(boolean useUsernameForAnonymousAccess) {
		this.useUsernameForAnonymousAccess = useUsernameForAnonymousAccess;
	}
}
