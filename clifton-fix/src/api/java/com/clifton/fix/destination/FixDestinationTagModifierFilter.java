package com.clifton.fix.destination;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.fix.tag.FixTagModifierFilter;


/**
 * The <code>FixDestinationTagModifierFilter</code> links FixTagModifierFilters to the tag modifiers associated with
 * a FixDestination.
 *
 * @author davidi
 */
public class FixDestinationTagModifierFilter extends ManyToManyEntity<FixDestinationTagModifier, FixTagModifierFilter, Integer> {
}


