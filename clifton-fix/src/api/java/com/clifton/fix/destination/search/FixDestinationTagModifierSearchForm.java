package com.clifton.fix.destination.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * A SearchForm for searching for FixDestinationTagModifier entities.
 *
 * @author davidi
 */

public class FixDestinationTagModifierSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "referenceOne.name,referenceTwo.name,referenceOne.description,referenceTwo.description,referenceTwo.type.name")
	private String searchPattern;

	@SearchField(searchField = "referenceOne.id")
	private Short fixDestinationId;

	@SearchField(searchField = "referenceOne.name", comparisonConditions = ComparisonConditions.LIKE)
	private String fixDestinationName;

	@SearchField(searchField = "referenceOne.description", comparisonConditions = ComparisonConditions.LIKE)
	private String fixDestinationDescription;

	@SearchField(searchField = "referenceTwo.id")
	private Integer fixTagModifierId;

	@SearchField(searchField = "referenceTwo.name", comparisonConditions = ComparisonConditions.LIKE)
	private String fixTagModifierName;

	@SearchField(searchField = "referenceTwo.description", comparisonConditions = ComparisonConditions.LIKE)
	private String fixTagModifierDescription;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo.type", comparisonConditions = ComparisonConditions.LIKE)
	private String fixTagModifierTypeName;

	@SearchField
	private Integer order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getFixDestinationId() {
		return this.fixDestinationId;
	}


	public void setFixDestinationId(Short fixDestinationId) {
		this.fixDestinationId = fixDestinationId;
	}


	public String getFixDestinationName() {
		return this.fixDestinationName;
	}


	public void setFixDestinationName(String fixDestinationName) {
		this.fixDestinationName = fixDestinationName;
	}


	public String getFixDestinationDescription() {
		return this.fixDestinationDescription;
	}


	public void setFixDestinationDescription(String fixDestinationDescription) {
		this.fixDestinationDescription = fixDestinationDescription;
	}


	public Integer getFixTagModifierId() {
		return this.fixTagModifierId;
	}


	public void setFixTagModifierId(Integer fixTagModifierId) {
		this.fixTagModifierId = fixTagModifierId;
	}


	public String getFixTagModifierName() {
		return this.fixTagModifierName;
	}


	public void setFixTagModifierName(String fixTagModifierName) {
		this.fixTagModifierName = fixTagModifierName;
	}


	public String getFixTagModifierDescription() {
		return this.fixTagModifierDescription;
	}


	public void setFixTagModifierDescription(String fixTagModifierDescription) {
		this.fixTagModifierDescription = fixTagModifierDescription;
	}


	public String getFixTagModifierTypeName() {
		return this.fixTagModifierTypeName;
	}


	public void setFixTagModifierTypeName(String fixTagModifierTypeName) {
		this.fixTagModifierTypeName = fixTagModifierTypeName;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}
}
