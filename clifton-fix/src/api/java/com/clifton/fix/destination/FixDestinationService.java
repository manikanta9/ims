package com.clifton.fix.destination;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.fix.destination.search.FixDestinationTagModifierFilterSearchForm;
import com.clifton.fix.destination.search.FixDestinationTagModifierSearchForm;
import com.clifton.fix.destination.search.FixDestinationSearchForm;

import java.util.List;


/**
 * The <code>FixDestinationService</code> defines methods to retrieve FIX destinations.
 *
 * @author lnaylor
 */
public interface FixDestinationService {

	////////////////////////////////////////////////////////////////////////////
	////////           FixDestination Business Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	public FixDestination getFixDestination(short id);


	public FixDestination getFixDestinationByName(String name);


	public FixDestination getFixDestinationBySessionDefinition(short sessionDefinitionId);


	public List<FixDestination> getFixDestinationList(FixDestinationSearchForm searchForm);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	@UserIgnorableValidation
	public FixDestination saveFixDestination(FixDestination destination, boolean ignoreValidation);


	@SecureMethod(permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	@UserIgnorableValidation
	public void deleteFixDestination(short id, boolean ignoreValidation);

	////////////////////////////////////////////////////////////////////////////
	////////        FixDestinationTagModifier Business Methods        //////////
	////////////////////////////////////////////////////////////////////////////


	public FixDestinationTagModifier getFixDestinationTagModifier(int id);


	public FixDestinationTagModifier saveFixDestinationTagModifier(FixDestinationTagModifier bean);


	public void deleteFixDestinationTagModifier(int id);


	public void deleteFixDestinationTagModifierByFixDestination(short fixDestinationId);


	public FixDestinationTagModifier linkFixDestinationToTagModifier(short fixDestinationId, int fixTagModifierId);


	public List<FixDestinationTagModifier> getFixDestinationTagModifierList(final FixDestinationTagModifierSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	//           FixDestinationTagModifierFilter Business Methods             //
	////////////////////////////////////////////////////////////////////////////


	public FixDestinationTagModifierFilter getFixDestinationTagModifierFilter(int id);


	 public FixDestinationTagModifierFilter saveFixDestinationTagModifierFilter(FixDestinationTagModifierFilter bean);


	public void deleteFixDestinationTagModifierFilter(int id);


	public void deleteFixDestinationTagModifierFilterByFixDestination(short fixDestinationId);


	public List<FixDestinationTagModifierFilter> getFixDestinationTagModifierFilterList(FixDestinationTagModifierFilterSearchForm searchForm);
}
