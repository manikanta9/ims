package com.clifton.fix.destination.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


/**
 * The <code>FixDestinationSearchForm</code> class defines search configuration for <code>FixDestination</code> objects.
 *
 * @author lnaylor
 */
public class FixDestinationSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "sessionDefinition.name")
	private String sessionDefinitionName;

	@SearchField(searchField="sessionDefinition.id")
	private Short sessionDefinitionId;

	@SearchField
	private Boolean defaultDestination;

	@SearchField(searchField = "name", searchFieldPath = "securitySystem")
	private String securitySystemName;

	@SearchField
	private Boolean allowAnonymousAccess;

	@SearchField
	private Boolean useUsernameForAnonymousAccess;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public String getSessionDefinitionName() {
		return this.sessionDefinitionName;
	}


	public void setSessionDefinitionName(String sessionDefinitionName) {
		this.sessionDefinitionName = sessionDefinitionName;
	}


	public Short getSessionDefinitionId() {
		return this.sessionDefinitionId;
	}


	public void setSessionDefinitionId(Short sessionDefinitionId) {
		this.sessionDefinitionId = sessionDefinitionId;
	}


	public Boolean getDefaultDestination() {
		return this.defaultDestination;
	}


	public void setDefaultDestination(Boolean defaultDestination) {
		this.defaultDestination = defaultDestination;
	}


	public String getSecuritySystemName() {
		return this.securitySystemName;
	}


	public void setSecuritySystemName(String securitySystemName) {
		this.securitySystemName = securitySystemName;
	}


	public Boolean getAllowAnonymousAccess() {
		return this.allowAnonymousAccess;
	}


	public void setAllowAnonymousAccess(Boolean allowAnonymousAccess) {
		this.allowAnonymousAccess = allowAnonymousAccess;
	}


	public Boolean getUseUsernameForAnonymousAccess() {
		return this.useUsernameForAnonymousAccess;
	}


	public void setUseUsernameForAnonymousAccess(Boolean useUsernameForAnonymousAccess) {
		this.useUsernameForAnonymousAccess = useUsernameForAnonymousAccess;
	}
}
