package com.clifton.fix.order;


import com.clifton.core.util.StringUtils;
import com.clifton.fix.order.beans.CancelRejectReasons;
import com.clifton.fix.order.beans.CancelRejectResponseToItems;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.util.FixUtils;


/**
 * The <code>FixOrderCancelReject</code> represents a FIX OrderCancelReject message.
 *
 * @author mwacker
 */
public class FixOrderCancelReject extends FixEntityImpl {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private FixOrder fixOrder;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	private String clientOrderStringId;
	private String originalClientOrderStringId;

	private CancelRejectResponseToItems cancelRejectResponseTo;
	private CancelRejectReasons cancelRejectReason;
	private OrderStatuses status;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isOrderCancelReplaceReject() {
		if (!StringUtils.isEmpty(getOriginalClientOrderStringId())) {
			return !FixUtils.cleanUniqueId(getClientOrderStringId()).equals(FixUtils.cleanUniqueId(getOriginalClientOrderStringId()));
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrder getFixOrder() {
		return this.fixOrder;
	}


	public void setFixOrder(FixOrder fixOrder) {
		this.fixOrder = fixOrder;
	}


	public String getClientOrderStringId() {
		return this.clientOrderStringId;
	}


	public void setClientOrderStringId(String clientOrderId) {
		this.clientOrderStringId = clientOrderId;
	}


	public CancelRejectResponseToItems getCancelRejectResponseTo() {
		return this.cancelRejectResponseTo;
	}


	public void setCancelRejectResponseTo(CancelRejectResponseToItems cancelRejectResponseTo) {
		this.cancelRejectResponseTo = cancelRejectResponseTo;
	}


	public CancelRejectReasons getCancelRejectReason() {
		return this.cancelRejectReason;
	}


	public void setCancelRejectReason(CancelRejectReasons cancelRejectReason) {
		this.cancelRejectReason = cancelRejectReason;
	}


	public OrderStatuses getStatus() {
		return this.status;
	}


	public void setStatus(OrderStatuses status) {
		this.status = status;
	}


	public String getOriginalClientOrderStringId() {
		return this.originalClientOrderStringId;
	}


	public void setOriginalClientOrderStringId(String originalClientOrderStringId) {
		this.originalClientOrderStringId = originalClientOrderStringId;
	}
}
