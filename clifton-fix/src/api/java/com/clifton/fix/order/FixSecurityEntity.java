package com.clifton.fix.order;


import com.clifton.fix.order.beans.OpenCloses;
import com.clifton.fix.order.beans.PutOrCalls;
import com.clifton.fix.order.beans.SecurityIDSources;
import com.clifton.fix.order.beans.SecurityTypes;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>FixSecurityEntity</code> contains the common fields for FixEntities that send security data.
 *
 * @author mwacker
 */
public class FixSecurityEntity extends FixEntityImpl {

	private String symbol;
	private String securityId;
	private String securityExchange;

	/**
	 * Options fields
	 */
	private PutOrCalls putOrCall;
	private BigDecimal strikePrice;
	private String CFICode;
	private String maturityMonthYear;
	private String maturityDay;

	/**
	 * The source of the security.  For example, BID is Bloomberg Code.
	 */
	private SecurityIDSources securityIDSource;
	private SecurityTypes securityType;
	private SecurityTypes underlyingSecurityType;

	private String currency;
	private String settlementCurrency;
	private Date maturityDate;
	/**
	 * The MIC code of the exchange.
	 */
	private String exDestination;
	/**
	 * The tenor string which represents the number of years to maturity for the benchmark index. Usually 1Y, 2Y, 3Y, 4Y, 5Y, 7Y or 10Y.
	 */
	private String tenor;

	/**
	 * Used for sending Open/Close for Options orders.
	 */
	private OpenCloses openClose;

	/**
	 * The encode security data typical FpML.
	 * <p>
	 * This is currently used for sending IRS to Bloomberg.
	 */
	private String encodeSecurityDescription;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public SecurityIDSources getSecurityIDSource() {
		return this.securityIDSource;
	}


	public void setSecurityIDSource(SecurityIDSources securityIDSource) {
		this.securityIDSource = securityIDSource;
	}


	public SecurityTypes getSecurityType() {
		return this.securityType;
	}


	public void setSecurityType(SecurityTypes securityType) {
		this.securityType = securityType;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getSettlementCurrency() {
		return this.settlementCurrency;
	}


	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public String getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}


	public String getExDestination() {
		return this.exDestination;
	}


	public void setExDestination(String exDestination) {
		this.exDestination = exDestination;
	}


	public String getSecurityExchange() {
		return this.securityExchange;
	}


	public void setSecurityExchange(String securityExchange) {
		this.securityExchange = securityExchange;
	}


	public String getTenor() {
		return this.tenor;
	}


	public void setTenor(String tenor) {
		this.tenor = tenor;
	}


	public PutOrCalls getPutOrCall() {
		return this.putOrCall;
	}


	public void setPutOrCall(PutOrCalls putOrCall) {
		this.putOrCall = putOrCall;
	}


	public BigDecimal getStrikePrice() {
		return this.strikePrice;
	}


	public void setStrikePrice(BigDecimal strikePrice) {
		this.strikePrice = strikePrice;
	}


	public String getCFICode() {
		return this.CFICode;
	}


	public void setCFICode(String CFICode) {
		this.CFICode = CFICode;
	}


	public String getMaturityMonthYear() {
		return this.maturityMonthYear;
	}


	public void setMaturityMonthYear(String maturityMonthYear) {
		this.maturityMonthYear = maturityMonthYear;
	}


	public String getMaturityDay() {
		return this.maturityDay;
	}


	public void setMaturityDay(String maturityDay) {
		this.maturityDay = maturityDay;
	}


	public OpenCloses getOpenClose() {
		return this.openClose;
	}


	public void setOpenClose(OpenCloses openClose) {
		this.openClose = openClose;
	}


	public SecurityTypes getUnderlyingSecurityType() {
		return this.underlyingSecurityType;
	}


	public void setUnderlyingSecurityType(SecurityTypes underlyingSecurityType) {
		this.underlyingSecurityType = underlyingSecurityType;
	}


	public String getEncodeSecurityDescription() {
		return this.encodeSecurityDescription;
	}


	public void setEncodeSecurityDescription(String encodeSecurityDescription) {
		this.encodeSecurityDescription = encodeSecurityDescription;
	}
}
