package com.clifton.fix.order;


import com.clifton.core.util.StringUtils;
import com.clifton.fix.identifier.FixIdentifier;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>FixEntity</code> defines the common fields for all fix objects.  These fields
 * define the FIX session that will be used and some common metadata fields.
 *
 * @author mwacker
 */
public class FixEntityImpl implements FixEntity {

	/**
	 * The id on the FixMessage object that this entity was created from.  This will only be populated
	 * when entities are read for the FixMessage table.
	 */
	private Long id;

	/**
	 * The message sequence number.
	 */
	private int sequenceNumber;

	/**
	 * The identifier object for the message.
	 */
	private FixIdentifier identifier;

	/**
	 * The FIX version string.
	 * <p/>
	 * FIX.4.2,FIX.4.4, etc.
	 */
	private String fixBeginString;

	/**
	 * The sending company id.
	 */
	private String fixSenderCompId;

	/**
	 * For REDI+, this field is the user name of the trader
	 */
	private String fixSenderSubId;
	private String fixSenderLocId;

	/**
	 * The target company id.
	 */
	private String fixTargetCompId;

	/**
	 * The target on for the counter party.  For example, for a REDI+ ticket the value in this field is "TKTS".  For Direct Market Order, this
	 * field would be "DMA".  This value is taken from the FIX Target on the trade destination.
	 */
	private String fixTargetSubId;
	private String fixTargetLocId;
	private String fixSessionQualifier;

	private String fixOnBehalfOfCompID;

	private String fixDestinationName;

	/**
	 * The AD username of the sender. This is typically mapped to/from the {@link FixEntityImpl#fixSenderSubId} field.
	 */
	private String senderUserName;

	/**
	 * The date and time that the message was logged after being received.
	 */
	private Date messageDateAndTime;

	/**
	 * Indicates the entity was generated from an incoming message and to look up the session the Sender and Target values should be reversed.
	 */
	private boolean incoming;

	// Service-side validation results for non-allocation messages.
	private boolean validationError;

	// Service-side validation results for allocation messages.
	private boolean allocationValidationError;

	/**
	 * Key-Value pair of other Attributes, Key=FixTag(Numeric/Integer) , Value=(String)
	 * The Key-Value (s) from the Map are placed directly  onto the outbound FIX Message
	 */

	private Map<String, String> additionalParams;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getFixSessionString() {
		return getFixBeginString() + ":" + getFixSenderCompId() + ":" + getFixSenderSubId() + ":" + getFixSenderLocId() + ":" + getFixTargetCompId() + ":" + getFixTargetSubId() + ":"
				+ getFixTargetLocId() + ":" + getFixSessionQualifier();
	}


	@Override
	public void setAdditionalParam(String prop, String propVal) {
		if (!StringUtils.isEmpty((prop)) && !StringUtils.isEmpty(propVal)) {
			//Lazy Init
			if (this.additionalParams == null) {
				this.additionalParams = new ConcurrentHashMap<>();
			}
			this.additionalParams.put(prop, propVal);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getFixBeginString() {
		return this.fixBeginString;
	}


	@Override
	public void setFixBeginString(String beginString) {
		this.fixBeginString = beginString;
	}


	@Override
	public String getFixSenderCompId() {
		return this.fixSenderCompId;
	}


	@Override
	public void setFixSenderCompId(String senderCompId) {
		this.fixSenderCompId = senderCompId;
	}


	@Override
	public String getFixTargetCompId() {
		return this.fixTargetCompId;
	}


	@Override
	public void setFixTargetCompId(String targetCompId) {
		this.fixTargetCompId = targetCompId;
	}


	@Override
	public String getFixSessionQualifier() {
		return this.fixSessionQualifier;
	}


	@Override
	public void setFixSessionQualifier(String sessionQualifier) {
		this.fixSessionQualifier = sessionQualifier;
	}


	@Override
	public String getFixSenderSubId() {
		return this.fixSenderSubId;
	}


	@Override
	public void setFixSenderSubId(String senderSubId) {
		this.fixSenderSubId = senderSubId;
	}


	@Override
	public String getFixSenderLocId() {
		return this.fixSenderLocId;
	}


	@Override
	public void setFixSenderLocId(String senderLocId) {
		this.fixSenderLocId = senderLocId;
	}


	@Override
	public String getFixTargetSubId() {
		return this.fixTargetSubId;
	}


	@Override
	public void setFixTargetSubId(String targetSubId) {
		this.fixTargetSubId = targetSubId;
	}


	@Override
	public String getFixTargetLocId() {
		return this.fixTargetLocId;
	}


	@Override
	public void setFixTargetLocId(String targetLocId) {
		this.fixTargetLocId = targetLocId;
	}


	@Override
	public Date getMessageDateAndTime() {
		return this.messageDateAndTime;
	}


	@Override
	public void setMessageDateAndTime(Date messageDateAndTime) {
		this.messageDateAndTime = messageDateAndTime;
	}


	@Override
	public boolean isIncoming() {
		return this.incoming;
	}


	@Override
	public void setIncoming(boolean incoming) {
		this.incoming = incoming;
	}


	@Override
	public FixIdentifier getIdentifier() {
		return this.identifier;
	}


	@Override
	public void setIdentifier(FixIdentifier identifier) {
		this.identifier = identifier;
	}


	@Override
	public int getSequenceNumber() {
		return this.sequenceNumber;
	}


	@Override
	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}


	@Override
	public Long getId() {
		return this.id;
	}


	@Override
	public void setId(Long id) {
		this.id = id;
	}


	@Override
	public String getFixOnBehalfOfCompID() {
		return this.fixOnBehalfOfCompID;
	}


	@Override
	public void setFixOnBehalfOfCompID(String fixOnBehalfOfCompID) {
		this.fixOnBehalfOfCompID = fixOnBehalfOfCompID;
	}


	@Override
	public boolean isValidationError() {
		return this.validationError;
	}


	@Override
	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}


	@Override
	public boolean isAllocationValidationError() {
		return this.allocationValidationError;
	}


	@Override
	public void setAllocationValidationError(boolean allocationValidationError) {
		this.allocationValidationError = allocationValidationError;
	}


	@Override
	public String getFixDestinationName() {
		return this.fixDestinationName;
	}


	@Override
	public void setFixDestinationName(String fixDestinationName) {
		this.fixDestinationName = fixDestinationName;
	}


	@Override
	public String getSenderUserName() {
		return this.senderUserName;
	}


	@Override
	public void setSenderUserName(String senderUserName) {
		this.senderUserName = senderUserName;
	}


	@Override
	public Map<String, String> getAdditionalParams() {
		return this.additionalParams;
	}
}
