package com.clifton.fix.order;


/**
 * The <code>FixOrderCancelReplaceRequest</code> is wrapper class to indicate a cancel replace message.
 *
 * @author mwacker
 */
public class FixOrderCancelReplaceRequest extends FixOrder {
	// nothing
}
