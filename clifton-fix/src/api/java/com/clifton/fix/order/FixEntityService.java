package com.clifton.fix.order;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.allocation.FixAllocationReportDetail;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>FixEntityService</code> interface can be used to convert messages to their corresponding FixEntity (i.e. FixAllocationReport, FixExecutionReport)
 *
 * @author manderson
 */
public interface FixEntityService {


	@SecureMethod(table = "FixMessageEntry")
	public <T extends FixEntity> List<T> getFixEntityListForIdentifierAndType(Integer identifier, String typeTag);


	@SecureMethod(table = "FixMessageEntry")
	public <T extends FixEntity> List<T> getFixEntityListForIdentifierAndTypeList(Integer identifier, List<String> typeTagList);


	@SecureMethod(table = "FixMessageEntry")
	public <T extends FixEntity> List<T> getFixEntityListForIdentifierListAndType(List<Integer> identifierList, String typeTag);


	@SecureMethod(table = "FixMessageEntry")
	public <T extends FixEntity> T getFixEntityForFixMessage(FixMessage message, boolean includeSessionQualifier);


	// TODO TRY TO MERGE THIS WITH ABOVE METHOD?
	@SecureMethod(table = "FixMessageEntry")
	public <T extends FixEntity> T getFixEntityForFixMessageText(String messageText, String sessionQualifier);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(table = "FixMessageEntry")
	public List<FixExecutionReport> getFixExecutionReportListForSourceEntity(String sourceSystemName, Long sourceSystemFkFieldId);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixAllocationAcknowledgement> getFixAllocationAcknowledgementListForSourceEntity(String sourceSystemName, Long sourceSystemFkFieldId);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixAllocationReportDetail> getFixAllocationReportDetailListForSourceEntity(String sourceSystemName, Long sourceSystemFkFieldId);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@ResponseBody
	@ModelAttribute("data")
	@RequestMapping("fixMessageTextForFixEntity")
	@SecureMethod(table = "FixMessageEntry")
	public <T extends FixEntity> String getFixMessageTextForFixEntity(T fixEntity);
}
