package com.clifton.fix.order.factory;

import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.allocation.FixAllocationInstruction;


/**
 * The <code>FixEntityFactoryGenerationTypes</code> defines a list of FixEntity types that can be generated.
 *
 * @author mwacker
 */
public enum FixEntityFactoryGenerationTypes {

	ORDER_SINGLE(FixOrder.class),
	ORDER_CANCEL_REQUEST(FixOrderCancelRequest.class),
	ORDER_CANCEL_REPLACE_REQUEST(FixOrderCancelReplaceRequest.class),
	ALLOCATION(FixAllocationInstruction.class),
	ALLOCATION_CANCEL(FixAllocationInstruction.class);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final Class<?> generatedClass;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	FixEntityFactoryGenerationTypes(Class<?> generatedClass) {
		this.generatedClass = generatedClass;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Class<?> getGeneratedClass() {
		return this.generatedClass;
	}
}
