package com.clifton.fix.order.beans;


public enum CancelRejectResponseToItems {

	ORDER_CANCEL_REQUEST,
	ORDER_CANCEL_REPLACE_REQUEST
}
