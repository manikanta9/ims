package com.clifton.fix.order;

import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.session.FixSessionParameters;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;
import java.util.Map;


/**
 * @author manderson
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class)
public interface FixEntity extends FixSessionParameters {


	public String getFixSessionString();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setFixBeginString(String beginString);


	public void setFixSenderCompId(String senderCompId);


	public void setFixTargetCompId(String targetCompId);


	public void setFixSessionQualifier(String sessionQualifier);


	public void setFixSenderSubId(String senderSubId);


	public void setFixSenderLocId(String senderLocId);


	public void setFixTargetSubId(String targetSubId);


	public void setFixTargetLocId(String targetLocId);


	public Date getMessageDateAndTime();


	public void setMessageDateAndTime(Date messageDateAndTime);


	public boolean isIncoming();


	public void setIncoming(boolean incoming);


	public FixIdentifier getIdentifier();


	public void setIdentifier(FixIdentifier identifier);


	public int getSequenceNumber();


	public void setSequenceNumber(int sequenceNumber);


	public Long getId();


	public void setId(Long id);


	public String getFixOnBehalfOfCompID();


	public void setFixOnBehalfOfCompID(String fixOnBehalfOfCompID);


	public boolean isValidationError();


	public void setValidationError(boolean validationError);


	public boolean isAllocationValidationError();


	public void setAllocationValidationError(boolean allocationValidationError);


	public String getFixDestinationName();


	public void setFixDestinationName(String fixDestinationName);


	public String getSenderUserName();


	public void setSenderUserName(String senderUserName);


	public void setAdditionalParam(String prop, String propVal);


	public Map<String, String> getAdditionalParams();
}
