package com.clifton.fix.order.beans;


public enum OrderFillStatuses {

	PARTIALLY_FILLED,
	FILLED,
	PARTIALLY_FILLED_CANCELED
}
