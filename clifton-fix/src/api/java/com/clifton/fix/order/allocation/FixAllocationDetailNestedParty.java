package com.clifton.fix.order.allocation;

import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;


/**
 * @author mwacker
 */
public class FixAllocationDetailNestedParty {

	private String nestedPartyId;
	private PartyRoles nestedPartyRole;
	private PartyIdSources nestedPartyIDSource;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getNestedPartyId() {
		return this.nestedPartyId;
	}


	public void setNestedPartyId(String nestedPartyId) {
		this.nestedPartyId = nestedPartyId;
	}


	public PartyRoles getNestedPartyRole() {
		return this.nestedPartyRole;
	}


	public void setNestedPartyRole(PartyRoles nestedPartyRole) {
		this.nestedPartyRole = nestedPartyRole;
	}


	public PartyIdSources getNestedPartyIDSource() {
		return this.nestedPartyIDSource;
	}


	public void setNestedPartyIDSource(PartyIdSources nestedPartyIDSource) {
		this.nestedPartyIDSource = nestedPartyIDSource;
	}
}
