package com.clifton.fix.order.beans;


public enum PartyIdSources {

	KOREAN_INVESTOR_ID('1'),
	TAIWANESE_QUALIFIED_FOREIGN_INVESTOR_ID_QFII_FID('2'),
	TAIWANESE_TRADING_ACCOUNT('3'),
	MALAYSIAN_CENTRAL_DEPOSITORY_NUMBER('4'),
	CHINESE_B_SHARE('5'),
	UK_NATIONAL_INSURANCE_OR_PENSION_NUMBER('6'),
	US_SOCIAL_SECURITY_NUMBER('7'),
	US_EMPLOYER_IDENTIFICATION_NUMBER('8'),
	AUSTRALIAN_BUSINESS_NUMBER('9'),
	AUSTRALIAN_TAX_FILE_NUMBER('A'),
	BIC('B'),
	GENERALLY_ACCEPTED_MARKET_PARTICIPANT_IDENTIFIER('C'),
	PROPRIETARY_CUSTOM_CODE('D'),
	ISO_COUNTRY_CODE('E'),
	SETTLEMENT_ENTITY_LOCATION('F'),
	MIC('G'),
	CSD_PARTICIPANT_MEMBER_CODE('H'),
	DIRECTED_BROKER('I'),
	LEGAL_ENTITY_IDENTIFIER('N');

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final char code;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	PartyIdSources(char code) {
		this.code = code;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static PartyIdSources fromCode(char codeValue) {
		PartyIdSources[] roles = PartyIdSources.values();
		for (PartyIdSources role : roles) {
			if (codeValue == role.getCode()) {
				return role;
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public char getCode() {
		return this.code;
	}
}
