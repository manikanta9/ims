package com.clifton.fix.order.beans;


public enum LastCapacities {

	AGENT,
	CROSS_AS_AGENT,
	CROSS_AS_PRINCIPAL,
	PRINCIPAL
}
