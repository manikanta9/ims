package com.clifton.fix.order;


/**
 * The <code>FixDontKnowTrade</code> marker class for DontKnowTrade message.
 *
 * @author mwacker
 */
public class FixDontKnowTrade extends FixExecutionReport {
	//
}
