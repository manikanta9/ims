package com.clifton.fix.order;


/**
 * The <code>FixMessageTextEntity</code> defines a <code>FixEntity</code> that contains the FIX message string.
 *
 * @author lnaylor
 */
public class FixMessageTextEntity extends FixEntityImpl {

	/**
	 * The FIX message string.  NOTE: the message string is in FIX format.
	 */
	private String messageText;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMessageText() {
		return this.messageText;
	}


	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}
}
