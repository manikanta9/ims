package com.clifton.fix.order;


import com.clifton.fix.order.beans.ExecutionTransactionTypes;
import com.clifton.fix.order.beans.ExecutionTypes;
import com.clifton.fix.order.beans.LastCapacities;
import com.clifton.fix.order.beans.OrderCapacities;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderStatuses;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;
import com.clifton.fix.order.beans.TimeInForceTypes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>FixExecutionReport</code> represents a FIX ExecutionReport message.
 *
 * @author mwacker
 */
public class FixExecutionReport extends FixSecurityEntity {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private FixOrder fixOrder;
	private FixOrder fixOriginalOrder;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Unique id received from the broker for the execution that the report represents.
	 */
	private String orderId;
	private String secondaryOrderId;

	/**
	 * Unique client order of the order that execution report is for.
	 */
	private String clientOrderStringId;
	private String secondaryClientOrderStringId;
	private String originalClientOrderStringId;

	private ExecutionTransactionTypes executionTransactionType;
	private ExecutionTypes executionType;

	/**
	 * Unique execution id received from the broker which is unique for a given day
	 */
	private String executionId;
	private String executionReferenceId;
	private OrderStatuses orderStatus;
	private Products product;
	private OrderSides side;
	private BigDecimal orderQuantity;
	private OrderTypes orderType;
	private BigDecimal price;
	private TimeInForceTypes timeInForceType;
	private OrderCapacities orderCapacity;
	private BigDecimal lastShares;
	private BigDecimal lastPrice;
	private String lastMarket;
	private BigDecimal cumulativeQuantity;
	private BigDecimal leavesQuantity;
	private BigDecimal averagePrice;
	private LastCapacities lastCapacity;
	private Date transactionTime;
	private Date tradeDate;
	private Date settlementDate;
	private BigDecimal settlementCurrencyAmount;
	private String text;

	// BOND fields
	private BigDecimal accruedInterestAmount;
	private BigDecimal netMoney;
	private BigDecimal grossTradeAmount;
	private BigDecimal couponRate;
	private BigDecimal yield;
	private BigDecimal concession;
	private BigDecimal indexRatio;
	private String issuer;

	private PriceTypes priceType;

	private List<FixParty> partyList;

	private List<FixCompDealerQuote> compDealerQuoteList;


	private List<FixNote> fixNotes;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public FixOrder getFixOrder() {
		return this.fixOrder;
	}


	public void setFixOrder(FixOrder fixOrder) {
		this.fixOrder = fixOrder;
	}


	public String getOrderId() {
		return this.orderId;
	}


	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}


	public String getClientOrderStringId() {
		return this.clientOrderStringId;
	}


	public void setClientOrderStringId(String clientOrderStringId) {
		this.clientOrderStringId = clientOrderStringId;
	}


	public String getOriginalClientOrderStringId() {
		return this.originalClientOrderStringId;
	}


	public void setOriginalClientOrderStringId(String originalClientOrderStringId) {
		this.originalClientOrderStringId = originalClientOrderStringId;
	}


	public ExecutionTransactionTypes getExecutionTransactionType() {
		return this.executionTransactionType;
	}


	public void setExecutionTransactionType(ExecutionTransactionTypes executionTransactionType) {
		this.executionTransactionType = executionTransactionType;
	}


	public String getExecutionId() {
		return this.executionId;
	}


	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}


	public String getExecutionReferenceId() {
		return this.executionReferenceId;
	}


	public void setExecutionReferenceId(String executionReferenceId) {
		this.executionReferenceId = executionReferenceId;
	}


	public OrderStatuses getOrderStatus() {
		return this.orderStatus;
	}


	public void setOrderStatus(OrderStatuses orderStatus) {
		this.orderStatus = orderStatus;
	}


	public OrderSides getSide() {
		return this.side;
	}


	public void setSide(OrderSides side) {
		this.side = side;
	}


	public BigDecimal getOrderQuantity() {
		return this.orderQuantity;
	}


	public void setOrderQuantity(BigDecimal orderQuantity) {
		this.orderQuantity = orderQuantity;
	}


	public OrderTypes getOrderType() {
		return this.orderType;
	}


	public void setOrderType(OrderTypes orderType) {
		this.orderType = orderType;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public TimeInForceTypes getTimeInForceType() {
		return this.timeInForceType;
	}


	public void setTimeInForceType(TimeInForceTypes timeInForceType) {
		this.timeInForceType = timeInForceType;
	}


	public OrderCapacities getOrderCapacity() {
		return this.orderCapacity;
	}


	public void setOrderCapacity(OrderCapacities orderCapacity) {
		this.orderCapacity = orderCapacity;
	}


	public BigDecimal getLastShares() {
		return this.lastShares;
	}


	public void setLastShares(BigDecimal lastShares) {
		this.lastShares = lastShares;
	}


	public BigDecimal getLastPrice() {
		return this.lastPrice;
	}


	public void setLastPrice(BigDecimal lastPrice) {
		this.lastPrice = lastPrice;
	}


	public String getLastMarket() {
		return this.lastMarket;
	}


	public void setLastMarket(String lastMarket) {
		this.lastMarket = lastMarket;
	}


	public BigDecimal getCumulativeQuantity() {
		return this.cumulativeQuantity;
	}


	public void setCumulativeQuantity(BigDecimal cumulativeQuantity) {
		this.cumulativeQuantity = cumulativeQuantity;
	}


	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}


	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}


	public LastCapacities getLastCapacity() {
		return this.lastCapacity;
	}


	public void setLastCapacity(LastCapacities lastCapacity) {
		this.lastCapacity = lastCapacity;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public FixOrder getFixOriginalOrder() {
		return this.fixOriginalOrder;
	}


	public void setFixOriginalOrder(FixOrder originalFixOrder) {
		this.fixOriginalOrder = originalFixOrder;
	}


	public ExecutionTypes getExecutionType() {
		return this.executionType;
	}


	public void setExecutionType(ExecutionTypes executionType) {
		this.executionType = executionType;
	}


	public BigDecimal getLeavesQuantity() {
		return this.leavesQuantity;
	}


	public void setLeavesQuantity(BigDecimal leavesQuantity) {
		this.leavesQuantity = leavesQuantity;
	}


	public List<FixParty> getPartyList() {
		return this.partyList;
	}


	public void setPartyList(List<FixParty> partyList) {
		this.partyList = partyList;
	}


	public PriceTypes getPriceType() {
		return this.priceType;
	}


	public void setPriceType(PriceTypes priceType) {
		this.priceType = priceType;
	}


	public BigDecimal getNetMoney() {
		return this.netMoney;
	}


	public void setNetMoney(BigDecimal netMoney) {
		this.netMoney = netMoney;
	}


	public BigDecimal getGrossTradeAmount() {
		return this.grossTradeAmount;
	}


	public void setGrossTradeAmount(BigDecimal grossTradeAmount) {
		this.grossTradeAmount = grossTradeAmount;
	}


	public BigDecimal getAccruedInterestAmount() {
		return this.accruedInterestAmount;
	}


	public void setAccruedInterestAmount(BigDecimal accruedInterestAmount) {
		this.accruedInterestAmount = accruedInterestAmount;
	}


	public BigDecimal getCouponRate() {
		return this.couponRate;
	}


	public void setCouponRate(BigDecimal couponRate) {
		this.couponRate = couponRate;
	}


	public BigDecimal getYield() {
		return this.yield;
	}


	public void setYield(BigDecimal yield) {
		this.yield = yield;
	}


	public BigDecimal getConcession() {
		return this.concession;
	}


	public void setConcession(BigDecimal concession) {
		this.concession = concession;
	}


	public BigDecimal getIndexRatio() {
		return this.indexRatio;
	}


	public void setIndexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
	}


	public String getIssuer() {
		return this.issuer;
	}


	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}


	public String getSecondaryOrderId() {
		return this.secondaryOrderId;
	}


	public void setSecondaryOrderId(String secondaryOrderId) {
		this.secondaryOrderId = secondaryOrderId;
	}


	public Products getProduct() {
		return this.product;
	}


	public void setProduct(Products product) {
		this.product = product;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public List<FixCompDealerQuote> getCompDealerQuoteList() {
		return this.compDealerQuoteList;
	}


	public void setCompDealerQuoteList(List<FixCompDealerQuote> compDealerQuoteList) {
		this.compDealerQuoteList = compDealerQuoteList;
	}


	public BigDecimal getSettlementCurrencyAmount() {
		return this.settlementCurrencyAmount;
	}


	public void setSettlementCurrencyAmount(BigDecimal settlementCurrencyAmount) {
		this.settlementCurrencyAmount = settlementCurrencyAmount;
	}


	public String getSecondaryClientOrderStringId() {
		return this.secondaryClientOrderStringId;
	}


	public void setSecondaryClientOrderStringId(String secondaryClientOrderStringId) {
		this.secondaryClientOrderStringId = secondaryClientOrderStringId;
	}


	public List<FixNote> getFixNotes() {
		return this.fixNotes;
	}


	public void setFixNotes(List<FixNote> fixNotes) {
		this.fixNotes = fixNotes;
	}
}
