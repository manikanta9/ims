package com.clifton.fix.order;


import com.clifton.fix.order.beans.PartyIdSources;
import com.clifton.fix.order.beans.PartyRoles;

import java.util.List;


/**
 * The <code>FixParty</code> represents an entry in the NoParties group used in ExecutionReports and AllocationInstruction message.  The
 * NoParties group in contains all the data needed to identify a counter party.
 *
 * @author mwacker
 */
public class FixParty {

	/**
	 * The id of the party.
	 */
	private String partyId;
	/**
	 * The trader user AD username. This is typically populated from the {@link FixParty#partyId} field.
	 */
	private String senderUserName;
	/**
	 * The source the generated the id.
	 */
	private PartyIdSources partyIdSource;
	/**
	 * The role of the party as it pertains to the current order or allocation.
	 */
	private PartyRoles partyRole;

	private List<FixPartySub> partySubList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FixParty) {
			FixParty object = (FixParty) obj;
			return object.getPartyId().equals(getPartyId()) && object.getPartyIdSource() == getPartyIdSource() && object.getPartyRole() == getPartyRole();
		}
		return super.equals(obj);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getPartyId() {
		return this.partyId;
	}


	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}


	public String getSenderUserName() {
		return this.senderUserName;
	}


	public void setSenderUserName(String senderUserName) {
		this.senderUserName = senderUserName;
	}


	public PartyIdSources getPartyIdSource() {
		return this.partyIdSource;
	}


	public void setPartyIdSource(PartyIdSources partySource) {
		this.partyIdSource = partySource;
	}


	public PartyRoles getPartyRole() {
		return this.partyRole;
	}


	public void setPartyRole(PartyRoles partyRole) {
		this.partyRole = partyRole;
	}


	public List<FixPartySub> getPartySubList() {
		return this.partySubList;
	}


	public void setPartySubList(List<FixPartySub> partySubList) {
		this.partySubList = partySubList;
	}
}
