package com.clifton.fix.order.allocation;


import com.clifton.fix.order.FixEntityImpl;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationReportTypes;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.SecurityIDSources;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>FixAllocationReport</code> represents an incoming AllocationReport.
 *
 * @author mwacker
 */
public class FixAllocationReport extends FixEntityImpl {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private FixAllocationInstruction fixAllocationInstruction;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	private String allocationStringId;
	private String allocationReportId;
	private AllocationStatuses allocationStatus;
	private AllocationReportTypes allocationReportType;
	private AllocationTransactionTypes allocationTransactionType;
	private AllocationNoOrdersTypes allocationNoOrdersType;

	private OrderSides side;
	private String symbol;
	private String securityId;
	private SecurityIDSources securityIDSource;
	private String maturityMonthYear;
	private BigDecimal shares;
	private BigDecimal averagePrice;
	private String currency;
	private String tradeDate;
	private Date transactionTime;
	private String text;
	private List<FixAllocationReportDetail> fixAllocationReportDetailList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationInstruction getFixAllocationInstruction() {
		return this.fixAllocationInstruction;
	}


	public void setFixAllocationInstruction(FixAllocationInstruction fixAllocationInstruction) {
		this.fixAllocationInstruction = fixAllocationInstruction;
	}


	public String getAllocationStringId() {
		return this.allocationStringId;
	}


	public void setAllocationStringId(String allocationStringId) {
		this.allocationStringId = allocationStringId;
	}


	public String getAllocationReportId() {
		return this.allocationReportId;
	}


	public void setAllocationReportId(String allocationReportId) {
		this.allocationReportId = allocationReportId;
	}


	public AllocationReportTypes getAllocationReportType() {
		return this.allocationReportType;
	}


	public void setAllocationReportType(AllocationReportTypes allocationReportType) {
		this.allocationReportType = allocationReportType;
	}


	public AllocationTransactionTypes getAllocationTransactionType() {
		return this.allocationTransactionType;
	}


	public void setAllocationTransactionType(AllocationTransactionTypes allocationTransactionType) {
		this.allocationTransactionType = allocationTransactionType;
	}


	public AllocationNoOrdersTypes getAllocationNoOrdersType() {
		return this.allocationNoOrdersType;
	}


	public void setAllocationNoOrdersType(AllocationNoOrdersTypes allocationNoOrdersType) {
		this.allocationNoOrdersType = allocationNoOrdersType;
	}


	public OrderSides getSide() {
		return this.side;
	}


	public void setSide(OrderSides side) {
		this.side = side;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(String securityId) {
		this.securityId = securityId;
	}


	public SecurityIDSources getSecurityIDSource() {
		return this.securityIDSource;
	}


	public void setSecurityIDSource(SecurityIDSources securityIDSource) {
		this.securityIDSource = securityIDSource;
	}


	public String getMaturityMonthYear() {
		return this.maturityMonthYear;
	}


	public void setMaturityMonthYear(String maturityMonthYear) {
		this.maturityMonthYear = maturityMonthYear;
	}


	public BigDecimal getShares() {
		return this.shares;
	}


	public void setShares(BigDecimal shares) {
		this.shares = shares;
	}


	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}


	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public List<FixAllocationReportDetail> getFixAllocationReportDetailList() {
		return this.fixAllocationReportDetailList;
	}


	public void setFixAllocationReportDetailList(List<FixAllocationReportDetail> fixAllocationReportDetailList) {
		this.fixAllocationReportDetailList = fixAllocationReportDetailList;
	}


	public AllocationStatuses getAllocationStatus() {
		return this.allocationStatus;
	}


	public void setAllocationStatus(AllocationStatuses allocationStatus) {
		this.allocationStatus = allocationStatus;
	}
}
