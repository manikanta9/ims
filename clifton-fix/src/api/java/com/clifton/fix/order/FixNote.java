package com.clifton.fix.order;

/**
 * Fix note is used to holds notes information received in fix message
 */
public class FixNote {

	/**
	 * Fix note type, note sent by dealer or client
	 */
	private FixNoteTypes noteType;

	/**
	 * Note in the message.
	 */
	private String note;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixNoteTypes getNoteType() {
		return this.noteType;
	}


	public void setNoteType(FixNoteTypes noteType) {
		this.noteType = noteType;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
