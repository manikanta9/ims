package com.clifton.fix.order.beans;


public enum OrderCapacities {

	AGENCY,
	PROPRIETARY,
	INDIVIDUAL,
	PRINCIPAL,
	RISKLESS_PRINCIPAL,
	AGENT_FOR_OTHER_MEMBER
}
