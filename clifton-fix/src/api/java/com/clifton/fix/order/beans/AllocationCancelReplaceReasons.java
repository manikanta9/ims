package com.clifton.fix.order.beans;


public enum AllocationCancelReplaceReasons {

	ORIGINAL_DETAILS_INCOMPLETE_INCORRECT(1),
	CHANGE_IN_UNDERLYING_ORDER_DETAILS(2),
	OTHER(99);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final int code;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	AllocationCancelReplaceReasons(int code) {
		this.code = code;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getCode() {
		return this.code;
	}
}
