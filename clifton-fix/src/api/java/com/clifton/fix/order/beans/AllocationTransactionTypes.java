package com.clifton.fix.order.beans;


public enum AllocationTransactionTypes {
	NEW, REPLACE, CANCEL, PRELIMINARY, CALCULATED, CALCULATED_WITHOUT_PRELIMINARY, REVERSAL
}
