package com.clifton.fix.order.beans;


import com.clifton.fix.order.FixExecutionReport;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


public enum PriceTypes {

	PERCENTAGE(1),
	FIXED_CABINET_TRADE_PRICE(10),
	VARIABLE_CABINET_TRADE_PRICE(11),
	PER_UNIT(2),
	FIXED_AMOUNT(3),
	DISCOUNT(4),
	PREMIUM(5),
	SPREAD(6) {
		@Override
		public BigDecimal calculateExecutionPrice(FixExecutionReport executionReport) {
			BigDecimal result = executionReport.getAveragePrice();
			if (executionReport.getGrossTradeAmount() != null && !MathUtils.isNullOrZero(executionReport.getCumulativeQuantity())) {
				BigDecimal grossTradeAmount = executionReport.getGrossTradeAmount();
				BigDecimal cumulativeQuantity = executionReport.getCumulativeQuantity();

				BigDecimal numerator;
				if (executionReport.getSide() == OrderSides.BUY) {
					numerator = MathUtils.add(cumulativeQuantity, grossTradeAmount);
				}
				else {
					numerator = MathUtils.subtract(cumulativeQuantity, grossTradeAmount);
				}

				result = MathUtils.multiply(MathUtils.divide(numerator, cumulativeQuantity), MathUtils.BIG_DECIMAL_ONE_HUNDRED);
			}
			return result;
		}
	},
	TED_PRICE(7),
	TED_YIELD(8),
	YIELD(9),
	PRODUCT_TICKS_IN_HALFS(13),
	PRODUCT_TICKS_IN_FOURTHS(14),
	PRODUCT_TICKS_IN_EIGHTS(15),
	PRODUCT_TICKS_IN_SIXTEENTHS(16),
	PRODUCT_TICKS_IN_THIRTY_SECONDS(17),
	PRODUCT_TICKS_IN_SIXTY_FORTHS(18),
	PRODUCT_TICKS_IN_ONE_TWENTY_EIGHTS(19);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final int code;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	PriceTypes(int code) {
		this.code = code;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal calculateExecutionPrice(FixExecutionReport executionReport) {
		return executionReport.getAveragePrice();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getCode() {
		return this.code;
	}
}
