package com.clifton.fix.order.beans;


public enum ExecutionTransactionTypes {

	NEW,
	CANCEL,
	CORRECT,
	STATUS
}
