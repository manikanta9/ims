package com.clifton.fix.order.beans;


public enum OrderStatuses {

	NEW,
	PARTIALLY_FILLED,
	FILLED,
	DONE_FOR_DAY,
	CANCELED,
	REPLACED,
	PENDING_CANCEL,
	STOPPED,
	REJECTED,
	PENDING_REPLACE
}
