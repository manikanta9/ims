package com.clifton.fix.order.beans;


public enum SecurityIDSources {

	CUSIP("1"),
	SEDOL("2"),
	QUIK("3"),
	ISIN_NUMBER("4"),
	RIC_CODE("5"),
	ISO_CURRENCY_CODE("6"),
	ISO_COUNTRY_CODE("7"),
	EXCHANGE_SYMBOL("8"),
	CONSOLIDATED_TAPE_ASSOCIATION("9"),
	BID("A"),
	WERTPAPIER("B"),
	DUTCH("C"),
	VALOREN("D"),
	SICOVAM("E"),
	BELGIAN("F"),
	COMMON("G"),
	CLEARING_HOUSE_CLEARING_ORGANIZATION("H"),
	ISDA_FPML_PRODUCT_SPECIFICATION("I"),
	OPTIONS_PRICE_REPORTING_AUTHORITY("J"),
	ISDA_FPML_PRODUCT_URL("K"),
	LETTER_OF_CREDIT("L"),
	RED_CODE("P");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final String code;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	SecurityIDSources(String code) {
		this.code = code;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static PartyRoles fromCode(int codeValue) {
		PartyRoles[] roles = PartyRoles.values();
		for (PartyRoles role : roles) {
			if (codeValue == role.getCode()) {
				return role;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCode() {
		return this.code;
	}
}
