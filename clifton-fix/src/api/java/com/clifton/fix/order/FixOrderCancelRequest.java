package com.clifton.fix.order;


import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>FixOrderCancelRequest</code> represents a FIX OrderCancelRequest message.
 *
 * @author mwacker
 */
public class FixOrderCancelRequest extends FixSecurityEntity {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private FixOrder fixOriginalOrder;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	private String clientOrderStringId;
	private String originalClientOrderStringId;

	private HandlingInstructions handlingInstructions;
	private Date transactionTime;

	private BigDecimal orderQty;
	private OrderSides side;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixOrder getFixOriginalOrder() {
		return this.fixOriginalOrder;
	}


	public void setFixOriginalOrder(FixOrder fixOriginalOrder) {
		this.fixOriginalOrder = fixOriginalOrder;
	}


	public String getClientOrderStringId() {
		return this.clientOrderStringId;
	}


	public void setClientOrderStringId(String clientOrderStringId) {
		this.clientOrderStringId = clientOrderStringId;
	}


	public String getOriginalClientOrderStringId() {
		return this.originalClientOrderStringId;
	}


	public void setOriginalClientOrderStringId(String originalClientOrderStringId) {
		this.originalClientOrderStringId = originalClientOrderStringId;
	}


	public HandlingInstructions getHandlingInstructions() {
		return this.handlingInstructions;
	}


	public void setHandlingInstructions(HandlingInstructions handlingInstructions) {
		this.handlingInstructions = handlingInstructions;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public BigDecimal getOrderQty() {
		return this.orderQty;
	}


	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}


	public OrderSides getSide() {
		return this.side;
	}


	public void setSide(OrderSides side) {
		this.side = side;
	}
}
