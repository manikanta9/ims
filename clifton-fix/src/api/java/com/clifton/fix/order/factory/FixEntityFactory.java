package com.clifton.fix.order.factory;


import com.clifton.fix.order.FixEntity;


/**
 * The <code>FixEntityFactory</code> defines methods used to generate FixEntities.
 *
 * @author mwacker
 */
public interface FixEntityFactory {

	/**
	 * Generate a FixEntity from the provided object.
	 */
	public <T extends FixEntity, F> T create(F from, FixEntityFactoryGenerationTypes action);
}
