package com.clifton.fix.order.allocation;


import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.AllocationCancelReplaceReasons;
import com.clifton.fix.order.beans.AllocationLinkTypes;
import com.clifton.fix.order.beans.AllocationNoOrdersTypes;
import com.clifton.fix.order.beans.AllocationStatuses;
import com.clifton.fix.order.beans.AllocationTransactionTypes;
import com.clifton.fix.order.beans.AllocationTypes;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>FixAllocationInstruction</code> represents a AllocationInstruction message.
 *
 * @author mwacker
 */
public class FixAllocationInstruction extends FixSecurityEntity {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private boolean sendSecondaryClientOrderId;
	private boolean sendOrderQuantityInNoOrdersGrouping;

	private List<FixOrder> fixOrderList;
	private FixAllocationInstruction fixReferenceAllocation;
	private FixAllocationInstruction fixAllocationLink;
	private List<FixAllocationDetail> fixAllocationDetailList;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	private String allocationStringId;
	private String referenceAllocationStringId;
	private String allocationLinkStringId;

	private AllocationTransactionTypes allocationTransactionType;
	private AllocationLinkTypes allocationLinkType;

	private AllocationTypes allocationType;
	private AllocationNoOrdersTypes allocationNoOrdersType;

	private AllocationCancelReplaceReasons allocationCancelReplaceReason;

	private AllocationStatuses allocationStatus;

	private String account;

	private OrderSides side;
	private BigDecimal shares;
	private BigDecimal averagePrice;
	private PriceTypes priceType;

	private BigDecimal netMoney;
	private BigDecimal grossTradeAmount;

	private Date tradeDate;
	private Date settlementDate;
	private Date transactionTime;
	private Date futureSettlementDate;

	private Products product;

	private List<FixParty> partyList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<FixOrder> getFixOrderList() {
		return this.fixOrderList;
	}


	public void setFixOrderList(List<FixOrder> fixOrderList) {
		this.fixOrderList = fixOrderList;
	}


	public FixAllocationInstruction getFixReferenceAllocation() {
		return this.fixReferenceAllocation;
	}


	public void setFixReferenceAllocation(FixAllocationInstruction fixReferenceAllocation) {
		this.fixReferenceAllocation = fixReferenceAllocation;
	}


	public FixAllocationInstruction getFixAllocationLink() {
		return this.fixAllocationLink;
	}


	public void setFixAllocationLink(FixAllocationInstruction fixAllocationLink) {
		this.fixAllocationLink = fixAllocationLink;
	}


	public List<FixAllocationDetail> getFixAllocationDetailList() {
		return this.fixAllocationDetailList;
	}


	public void setFixAllocationDetailList(List<FixAllocationDetail> fixAllocationDetailList) {
		this.fixAllocationDetailList = fixAllocationDetailList;
	}


	public String getAllocationStringId() {
		return this.allocationStringId;
	}


	public void setAllocationStringId(String allocationId) {
		this.allocationStringId = allocationId;
	}


	public String getReferenceAllocationStringId() {
		return this.referenceAllocationStringId;
	}


	public void setReferenceAllocationStringId(String referenceAllocationId) {
		this.referenceAllocationStringId = referenceAllocationId;
	}


	public String getAllocationLinkStringId() {
		return this.allocationLinkStringId;
	}


	public void setAllocationLinkStringId(String allocationLinkId) {
		this.allocationLinkStringId = allocationLinkId;
	}


	public AllocationTransactionTypes getAllocationTransactionType() {
		return this.allocationTransactionType;
	}


	public void setAllocationTransactionType(AllocationTransactionTypes allocationTransactionType) {
		this.allocationTransactionType = allocationTransactionType;
	}


	public AllocationLinkTypes getAllocationLinkType() {
		return this.allocationLinkType;
	}


	public void setAllocationLinkType(AllocationLinkTypes allocationLinkType) {
		this.allocationLinkType = allocationLinkType;
	}


	public OrderSides getSide() {
		return this.side;
	}


	public void setSide(OrderSides side) {
		this.side = side;
	}


	public BigDecimal getShares() {
		return this.shares;
	}


	public void setShares(BigDecimal shares) {
		this.shares = shares;
	}


	public BigDecimal getAveragePrice() {
		return this.averagePrice;
	}


	public void setAveragePrice(BigDecimal averagePrice) {
		this.averagePrice = averagePrice;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public Date getFutureSettlementDate() {
		return this.futureSettlementDate;
	}


	public void setFutureSettlementDate(Date futureSettlementDate) {
		this.futureSettlementDate = futureSettlementDate;
	}


	public BigDecimal getNetMoney() {
		return this.netMoney;
	}


	public void setNetMoney(BigDecimal netMoney) {
		this.netMoney = netMoney;
	}


	public AllocationStatuses getAllocationStatus() {
		return this.allocationStatus;
	}


	public void setAllocationStatus(AllocationStatuses allocationStatus) {
		this.allocationStatus = allocationStatus;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public AllocationTypes getAllocationType() {
		return this.allocationType;
	}


	public void setAllocationType(AllocationTypes allocationType) {
		this.allocationType = allocationType;
	}


	public AllocationNoOrdersTypes getAllocationNoOrdersType() {
		return this.allocationNoOrdersType;
	}


	public void setAllocationNoOrdersType(AllocationNoOrdersTypes allocationNoOrdersType) {
		this.allocationNoOrdersType = allocationNoOrdersType;
	}


	public PriceTypes getPriceType() {
		return this.priceType;
	}


	public void setPriceType(PriceTypes priceType) {
		this.priceType = priceType;
	}


	public List<FixParty> getPartyList() {
		return this.partyList;
	}


	public void setPartyList(List<FixParty> partyList) {
		this.partyList = partyList;
	}


	public BigDecimal getGrossTradeAmount() {
		return this.grossTradeAmount;
	}


	public void setGrossTradeAmount(BigDecimal grossTradeAmount) {
		this.grossTradeAmount = grossTradeAmount;
	}


	public Products getProduct() {
		return this.product;
	}


	public void setProduct(Products product) {
		this.product = product;
	}


	public boolean isSendSecondaryClientOrderId() {
		return this.sendSecondaryClientOrderId;
	}


	public void setSendSecondaryClientOrderId(boolean sendSecondaryClientOrderId) {
		this.sendSecondaryClientOrderId = sendSecondaryClientOrderId;
	}


	public boolean isSendOrderQuantityInNoOrdersGrouping() {
		return this.sendOrderQuantityInNoOrdersGrouping;
	}


	public void setSendOrderQuantityInNoOrdersGrouping(boolean sendOrderQuantityInNoOrdersGrouping) {
		this.sendOrderQuantityInNoOrdersGrouping = sendOrderQuantityInNoOrdersGrouping;
	}


	public AllocationCancelReplaceReasons getAllocationCancelReplaceReason() {
		return this.allocationCancelReplaceReason;
	}


	public void setAllocationCancelReplaceReason(AllocationCancelReplaceReasons allocationCancelReplaceReason) {
		this.allocationCancelReplaceReason = allocationCancelReplaceReason;
	}


	public String getAccount() {
		return this.account;
	}


	public void setAccount(String account) {
		this.account = account;
	}
}
