package com.clifton.fix.order.allocation;


import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;


/**
 * The <code>FixAllocationReportDetail</code> contains the allocation details received in the incoming AllocationReport.
 *
 * @author mwacker
 */
public class FixAllocationReportDetail {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private FixAllocationReport fixAllocationReport;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	private String allocationAccount;
	private String allocationText;

	private BigDecimal allocationAveragePrice;
	private BigDecimal allocationPrice;
	private BigDecimal allocationShares;
	private BigDecimal allocationNetMoney;
	private BigDecimal allocationGrossTradeAmount;
	private BigDecimal allocationAccruedInterestAmount;

	private ProcessCodes processCode;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationReport getFixAllocationReport() {
		return this.fixAllocationReport;
	}


	public void setFixAllocationReport(FixAllocationReport fixAllocationReport) {
		this.fixAllocationReport = fixAllocationReport;
	}


	public String getAllocationAccount() {
		return this.allocationAccount;
	}


	public void setAllocationAccount(String allocationAccount) {
		this.allocationAccount = allocationAccount;
	}


	public BigDecimal getAllocationAveragePrice() {
		return this.allocationAveragePrice;
	}


	public void setAllocationAveragePrice(BigDecimal allocationAveragePrice) {
		this.allocationAveragePrice = allocationAveragePrice;
	}


	public BigDecimal getAllocationPrice() {
		return this.allocationPrice;
	}


	public void setAllocationPrice(BigDecimal allocationPrice) {
		this.allocationPrice = allocationPrice;
	}


	public BigDecimal getAllocationShares() {
		return this.allocationShares;
	}


	public void setAllocationShares(BigDecimal allocationShares) {
		this.allocationShares = allocationShares;
	}


	public ProcessCodes getProcessCode() {
		return this.processCode;
	}


	public void setProcessCode(ProcessCodes processCode) {
		this.processCode = processCode;
	}


	public String getAllocationText() {
		return this.allocationText;
	}


	public void setAllocationText(String allocationText) {
		this.allocationText = allocationText;
	}


	public BigDecimal getAllocationNetMoney() {
		return this.allocationNetMoney;
	}


	public void setAllocationNetMoney(BigDecimal allocationNetMoney) {
		this.allocationNetMoney = allocationNetMoney;
	}


	public BigDecimal getAllocationGrossTradeAmount() {
		return this.allocationGrossTradeAmount;
	}


	public void setAllocationGrossTradeAmount(BigDecimal allocationGrossTradeAmount) {
		this.allocationGrossTradeAmount = allocationGrossTradeAmount;
	}


	public BigDecimal getAllocationAccruedInterestAmount() {
		return this.allocationAccruedInterestAmount;
	}


	public void setAllocationAccruedInterestAmount(BigDecimal allocationAccruedInterestAmount) {
		this.allocationAccruedInterestAmount = allocationAccruedInterestAmount;
	}
}
