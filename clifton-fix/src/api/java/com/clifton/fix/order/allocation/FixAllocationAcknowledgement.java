package com.clifton.fix.order.allocation;


import com.clifton.fix.order.FixEntityImpl;
import com.clifton.fix.order.beans.AllocationRejectCodes;
import com.clifton.fix.order.beans.AllocationStatuses;

import java.util.Date;


/**
 * The <code>FixAllocationAcknowledgement</code> represents a FIX AllocationAcknowledgement message.
 *
 * @author mwacker
 */
public class FixAllocationAcknowledgement extends FixEntityImpl {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private FixAllocationInstruction fixAllocationInstruction;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	private String allocationStringId;
	private String tradeDate;
	private Date transactionTime;
	private AllocationStatuses allocationStatus;
	private AllocationRejectCodes allocationRejectCode;
	private String text;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationInstruction getFixAllocationInstruction() {
		return this.fixAllocationInstruction;
	}


	public void setFixAllocationInstruction(FixAllocationInstruction fixAllocationInstruction) {
		this.fixAllocationInstruction = fixAllocationInstruction;
	}


	public String getAllocationStringId() {
		return this.allocationStringId;
	}


	public void setAllocationStringId(String allocationStringId) {
		this.allocationStringId = allocationStringId;
	}


	public String getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(String tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public AllocationStatuses getAllocationStatus() {
		return this.allocationStatus;
	}


	public void setAllocationStatus(AllocationStatuses allocationStatus) {
		this.allocationStatus = allocationStatus;
	}


	public AllocationRejectCodes getAllocationRejectCode() {
		return this.allocationRejectCode;
	}


	public void setAllocationRejectCode(AllocationRejectCodes allocationRejectCode) {
		this.allocationRejectCode = allocationRejectCode;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}
}
