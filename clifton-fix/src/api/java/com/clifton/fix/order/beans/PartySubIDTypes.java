package com.clifton.fix.order.beans;


public enum PartySubIDTypes {

	FIRM(1),
	PERSON(2),
	SYSTEM(3),
	APPLICATION(4),
	FULL_LEGAL_NAME_OF_FIRM(5),
	POSTAL_ADDRESS(6),
	PHONE_NUMBER(7),
	EMAIL_ADDRESS(8),
	CONTACT_NAME(9),
	SECURITIES_ACCOUNT_NUMBER(10),
	REGISTRATION_NUMBER(11),
	REGISTERED_ADDRESS(12),
	REGULATORY_STATUS(13),
	REGISTRATION_NAME(14),
	CASH_ACCOUNT_NUMBER(15),
	BIC(16),
	CSD_PARTICIPANT_MEMBER_CODE(17),
	REGISTERED_ADDRESS2(18),
	FUND_ACCOUNT_NAME(19),
	TELEX_NUMBER(20),
	FAX_NUMBER(21),
	SECURITIES_ACCOUNT_NAME(22),
	CASH_ACCOUNT_NAME(23),
	DEPARTMENT(24),
	LOCATION_DESK(25),
	POSITION_ACCOUNT_TYPE(26),
	SECURITY_LOCATE_ID(27),
	MARKET_MAKER(28),
	ELIGIBLE_COUNTERPARTY(29),
	PROFESSIONAL_CLIENT(30),
	LOCATION(31),
	EXECUTION_VENUE(32),
	BLOOMBERG_UUID(4000),
	SALES_PERSON(4007),
	OMS_POMS_COUNTERPARTY_CODE(4011),
	TRADER(4011),
	FIRM_NUMBER(4014),
	BLOOMBERG_CUSTOMER_NAME(4015),
	BLOOMBERG_CUSTOMER_NUMBER(4016);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final int code;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	PartySubIDTypes(int code) {
		this.code = code;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static PartySubIDTypes fromCode(int codeValue) {
		PartySubIDTypes[] roles = PartySubIDTypes.values();
		for (PartySubIDTypes role : roles) {
			if (codeValue == role.getCode()) {
				return role;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getCode() {
		return this.code;
	}
}
