package com.clifton.fix.order.beans;


public enum TimeInForceTypes {

	DAY,
	GTC,
	IOC,
	GTD
}
