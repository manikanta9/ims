package com.clifton.fix.order;

/**
 * Enum to represent different fix note types.
 */
public enum FixNoteTypes {

	CUSTOMER_NOTES('C'),
	DEALER_NOTES('D');

	private final char code;


	FixNoteTypes(char code) {
		this.code = code;
	}


	public char getCode() {
		return this.code;
	}
}
