package com.clifton.fix.order.allocation;


import com.clifton.fix.order.beans.CommissionTypes;
import com.clifton.fix.order.beans.ProcessCodes;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>FixAllocationDetail</code> contains the allocation details used to generate the AllocationInstructions.
 *
 * @author mwacker
 */
public class FixAllocationDetail {

	private FixAllocationInstruction fixAllocationInstruction;
	private String allocationAccount;
	private List<FixAllocationDetailNestedParty> nestedPartyList;
	private BigDecimal allocationPrice;
	private BigDecimal allocationShares;
	private BigDecimal allocationNetMoney;
	private ProcessCodes processCode;

	private BigDecimal commission;
	private CommissionTypes commissionType;

	private String individualAllocID;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixAllocationInstruction getFixAllocationInstruction() {
		return this.fixAllocationInstruction;
	}


	public void setFixAllocationInstruction(FixAllocationInstruction fixAllocationInstruction) {
		this.fixAllocationInstruction = fixAllocationInstruction;
	}


	public String getAllocationAccount() {
		return this.allocationAccount;
	}


	public void setAllocationAccount(String allocationAccount) {
		this.allocationAccount = allocationAccount;
	}


	public BigDecimal getAllocationPrice() {
		return this.allocationPrice;
	}


	public void setAllocationPrice(BigDecimal allocationPrice) {
		this.allocationPrice = allocationPrice;
	}


	public BigDecimal getAllocationShares() {
		return this.allocationShares;
	}


	public void setAllocationShares(BigDecimal allocationShares) {
		this.allocationShares = allocationShares;
	}


	public BigDecimal getCommission() {
		return this.commission;
	}


	public void setCommission(BigDecimal commission) {
		this.commission = commission;
	}


	public CommissionTypes getCommissionType() {
		return this.commissionType;
	}


	public void setCommissionType(CommissionTypes commissionType) {
		this.commissionType = commissionType;
	}


	public ProcessCodes getProcessCode() {
		return this.processCode;
	}


	public void setProcessCode(ProcessCodes processCode) {
		this.processCode = processCode;
	}


	public BigDecimal getAllocationNetMoney() {
		return this.allocationNetMoney;
	}


	public void setAllocationNetMoney(BigDecimal allocationNetMoney) {
		this.allocationNetMoney = allocationNetMoney;
	}


	public String getIndividualAllocID() {
		return this.individualAllocID;
	}


	public void setIndividualAllocID(String individualAllocID) {
		this.individualAllocID = individualAllocID;
	}


	public List<FixAllocationDetailNestedParty> getNestedPartyList() {
		return this.nestedPartyList;
	}


	public void setNestedPartyList(List<FixAllocationDetailNestedParty> nestedPartyList) {
		this.nestedPartyList = nestedPartyList;
	}
}
