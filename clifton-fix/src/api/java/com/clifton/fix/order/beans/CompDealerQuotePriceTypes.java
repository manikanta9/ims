package com.clifton.fix.order.beans;


/**
 * <code>CompDealerQuotePriceTypes</code> represents quote price types for both CompDealerQuotePriceType and DealerQuotePriceType.
 * CompDealerQuotePriceType is a subset of DealerQuotePriceType.
 */
public enum CompDealerQuotePriceTypes {

	PERCENTAGE_OF_PAR(1),
	PER_UNIT(2),
	FIXED_AMOUNT(3),
	DISCOUNT(4),
	PREMIUM(5),
	SPREAD(6),
	TED_PRICE(7),
	TED_YIELD(8),
	YIELD(9),
	FIXED_CABINET_TRADE_PRICE(10),
	VARIABLE_CABINET_TRADE_PRICE(11);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final int code;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	CompDealerQuotePriceTypes(int code) {
		this.code = code;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CompDealerQuotePriceTypes fromCode(int codeValue) {
		CompDealerQuotePriceTypes[] roles = CompDealerQuotePriceTypes.values();
		for (CompDealerQuotePriceTypes role : roles) {
			if (codeValue == role.getCode()) {
				return role;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getCode() {
		return this.code;
	}
}
