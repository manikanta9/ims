package com.clifton.fix.order;


import com.clifton.fix.order.beans.CompDealerMidPriceTypes;
import com.clifton.fix.order.beans.CompDealerQuotePriceTypes;
import com.clifton.fix.order.beans.CompDealerQuoteTypes;

import java.math.BigDecimal;


/**
 * The <code>FixCompDealerQuote</code> represents an entry in the NoCompDealerQuote group used in ExecutionReport messages.  The
 * NoCompDealerQuote group in contains the competitive dealer quote data.
 * <p/>
 * NOTE:  Currently only Bloomberg sends dealer quote data in this group.  FX Connect sends forward quote data in tag 6230.
 * The quickfix converter
 *
 * @author mwacker
 */
public class FixCompDealerQuote {

	/**
	 * The dealer id.
	 */
	private String compDealerId;

	private CompDealerQuoteTypes compDealerQuoteType;
	private BigDecimal compDealerQuotePrice;
	private CompDealerQuotePriceTypes compDealerQuotePriceType;
	private BigDecimal compDealerParQuote;
	private BigDecimal compDealerMidPrice;
	private CompDealerMidPriceTypes compDealerMidPriceType;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getCompDealerId() {
		return this.compDealerId;
	}


	public void setCompDealerId(String compDealerId) {
		this.compDealerId = compDealerId;
	}


	public CompDealerQuoteTypes getCompDealerQuoteType() {
		return this.compDealerQuoteType;
	}


	public void setCompDealerQuoteType(CompDealerQuoteTypes compDealerQuoteType) {
		this.compDealerQuoteType = compDealerQuoteType;
	}


	public BigDecimal getCompDealerQuotePrice() {
		return this.compDealerQuotePrice;
	}


	public void setCompDealerQuotePrice(BigDecimal compDealerQuotePrice) {
		this.compDealerQuotePrice = compDealerQuotePrice;
	}


	public CompDealerQuotePriceTypes getCompDealerQuotePriceType() {
		return this.compDealerQuotePriceType;
	}


	public void setCompDealerQuotePriceType(CompDealerQuotePriceTypes compDealerQuotePriceType) {
		this.compDealerQuotePriceType = compDealerQuotePriceType;
	}


	public BigDecimal getCompDealerParQuote() {
		return this.compDealerParQuote;
	}


	public void setCompDealerParQuote(BigDecimal compDealerParQuote) {
		this.compDealerParQuote = compDealerParQuote;
	}


	public BigDecimal getCompDealerMidPrice() {
		return this.compDealerMidPrice;
	}


	public void setCompDealerMidPrice(BigDecimal compDealerMidPrice) {
		this.compDealerMidPrice = compDealerMidPrice;
	}


	public CompDealerMidPriceTypes getCompDealerMidPriceType() {
		return this.compDealerMidPriceType;
	}


	public void setCompDealerMidPriceType(CompDealerMidPriceTypes compDealerMidPriceType) {
		this.compDealerMidPriceType = compDealerMidPriceType;
	}
}
