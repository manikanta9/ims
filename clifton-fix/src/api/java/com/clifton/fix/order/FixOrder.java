package com.clifton.fix.order;


import com.clifton.fix.order.allocation.FixAllocationDetail;
import com.clifton.fix.order.beans.HandlingInstructions;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.OrderTypes;
import com.clifton.fix.order.beans.TimeInForceTypes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>FixOrder</code> represents an order the will be converted to a NewOrderSingle message.
 *
 * @author mwacker
 */
public class FixOrder extends FixSecurityEntity {

	////////////////////////////////////////////////////////////////////////////
	////////                     Non FIX Fields                      ///////////
	////////////////////////////////////////////////////////////////////////////

	private List<FixExecutionReport> fixExecutionReportList;
	private FixOrder fixOriginalOrder;

	private List<FixAllocationDetail> fixAllocationDetailList;

	////////////////////////////////////////////////////////////////////////////
	////////                       FIX Fields                        ///////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The unique id sent to the broker.
	 */
	private String clientOrderStringId;
	private String originalClientOrderStringId;

	private HandlingInstructions handlingInstructions;
	/**
	 * A list of broker codes for the order.
	 * <p/>
	 * For REDI+, this can only be 1, but for FxConnect this can be a list of brokers that are allowed to execute the order.
	 */
	//private List<String> brokerCodeList = new ArrayList<>();

	private OrderSides side;

	private BigDecimal orderQty;
	private OrderTypes orderType;
	private BigDecimal price;
	private BigDecimal commissionPerUnit;
	private TimeInForceTypes timeInForce;
	private Date expireTime;
	private String account;
	private Date transactionTime;
	private String text;

	private Date tradeDate;
	private Date settlementDate;

	/**
	 * The parties involved in the order.
	 */
	private List<FixParty> partyList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<FixExecutionReport> getFixExecutionReportList() {
		return this.fixExecutionReportList;
	}


	public void setFixExecutionReportList(List<FixExecutionReport> fixExecutionReportList) {
		this.fixExecutionReportList = fixExecutionReportList;
	}


	public FixOrder getFixOriginalOrder() {
		return this.fixOriginalOrder;
	}


	public void setFixOriginalOrder(FixOrder fixOriginalOrder) {
		this.fixOriginalOrder = fixOriginalOrder;
	}


	public String getClientOrderStringId() {
		return this.clientOrderStringId;
	}


	public void setClientOrderStringId(String clientOrderStringId) {
		this.clientOrderStringId = clientOrderStringId;
	}


	public String getOriginalClientOrderStringId() {
		return this.originalClientOrderStringId;
	}


	public void setOriginalClientOrderStringId(String originalClientOrderStringId) {
		this.originalClientOrderStringId = originalClientOrderStringId;
	}


	public HandlingInstructions getHandlingInstructions() {
		return this.handlingInstructions;
	}


	public void setHandlingInstructions(HandlingInstructions handlingInstructions) {
		this.handlingInstructions = handlingInstructions;
	}


	public OrderSides getSide() {
		return this.side;
	}


	public void setSide(OrderSides side) {
		this.side = side;
	}


	public BigDecimal getOrderQty() {
		return this.orderQty;
	}


	public void setOrderQty(BigDecimal orderQty) {
		this.orderQty = orderQty;
	}


	public OrderTypes getOrderType() {
		return this.orderType;
	}


	public void setOrderType(OrderTypes orderType) {
		this.orderType = orderType;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getCommissionPerUnit() {
		return this.commissionPerUnit;
	}


	public void setCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.commissionPerUnit = commissionPerUnit;
	}


	public TimeInForceTypes getTimeInForce() {
		return this.timeInForce;
	}


	public void setTimeInForce(TimeInForceTypes timeInForce) {
		this.timeInForce = timeInForce;
	}


	public Date getExpireTime() {
		return this.expireTime;
	}


	public void setExpireTime(Date expireTime) {
		this.expireTime = expireTime;
	}


	public String getAccount() {
		return this.account;
	}


	public void setAccount(String account) {
		this.account = account;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public List<FixParty> getPartyList() {
		return this.partyList;
	}


	public void setPartyList(List<FixParty> partyList) {
		this.partyList = partyList;
	}


	public List<FixAllocationDetail> getFixAllocationDetailList() {
		return this.fixAllocationDetailList;
	}


	public void setFixAllocationDetailList(List<FixAllocationDetail> fixAllocationDetailList) {
		this.fixAllocationDetailList = fixAllocationDetailList;
	}
}
