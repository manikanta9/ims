package com.clifton.fix.order.factory;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.order.FixEntity;


/**
 * The <code>AbstractFixEntityFactory</code> implements the basic create method of the FixEntityFactory and adds validation to
 * check that the generate object is of the expected type.
 *
 * @author mwacker
 */
public abstract class AbstractFixEntityFactory implements FixEntityFactory {

	private String fixSessionQualifier;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected abstract <T extends FixEntity, F> T doCreate(F from, FixEntityFactoryGenerationTypes action);


	@Override
	public <T extends FixEntity, F> T create(F from, FixEntityFactoryGenerationTypes action) {
		T result = doCreate(from, action);
		if (result != null) {
			if (!StringUtils.isEmpty(getFixSessionQualifier())) {
				result.setFixSessionQualifier(getFixSessionQualifier());
			}
			ValidationUtils.assertEquals(action.getGeneratedClass(), result.getClass(),
					"[" + action + "] generate a instance of [" + result.getClass() + "] but an instance of [" + action.getGeneratedClass() + "] was expected.");
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String getFixSessionQualifier() {
		return this.fixSessionQualifier;
	}


	public void setFixSessionQualifier(String fixSessionQualifier) {
		this.fixSessionQualifier = fixSessionQualifier;
	}
}
