package com.clifton.fix.order.beans;


public enum AllocationNoOrdersTypes {

	NOT_SPECIFIED,
	EXPLICIT_LIST_PROVIDED
}
