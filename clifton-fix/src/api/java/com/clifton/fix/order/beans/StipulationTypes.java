package com.clifton.fix.order.beans;

/**
 * An enumeration representing FIX Stipulation types that occur in FIX messages related fixed-income instruments.
 * <p>
 * Within IMS these types are currently only used to represent {@link com.clifton.fix.ioi.FixStipulation} to specify the
 * type of stipulation the FixStipulation represents.  MINQTY (Minimum Quantity), MININCR (Minimum Quantity Increment), and
 * MINTOKEEP (Minimum quantity to keep) are currently used in IMS for IOI Stipulations.  The remaining types were added for completeness.
 *
 * @author: davidi
 */
public enum StipulationTypes {
	AMT,
	AUTOREINV,
	BANKQUAL,
	BGNCON,
	COUPON,
	CURRENCY,
	GEOG,
	HAIRCUT,
	INSURED,
	ISSUE,
	ISSUER,
	ISSUESIZE,
	LOOKBACK,
	LOT,
	LOTVAR,
	MAT,
	MATURITY,
	MAXSUBS,
	MINQTY,
	MININCR,
	MINDNOM,
	MINTOKEEP,
	PAYFREQ,
	PIECES,
	PPM,
	PPL,
	PPT,
	PRICE,
	PRICEFREQ,
	PROD,
	PROTECT,
	PURPOSE,
	PXSOURCE,
	RATING,
	REDEMPTION,
	RESTRICTED,
	SECTOR,
	SECTYPE,
	STRUCT,
	SUBSFREQ,
	SUBSLEFT,
	TEXT,
	TRDVAR,
	WAC,
	WAL,
	WALA,
	WAM,
	WHOLE,
	YIELD,
	SMM,
	CPR,
	CPY,
	CPP,
	ABS,
	MPR,
	PSA,
	PPC,
	MHP,
	HEP
}
