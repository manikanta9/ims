package com.clifton.fix.order;


import com.clifton.fix.order.beans.PartySubIDTypes;


/**
 * The <code>FixParty</code> represents an entry in the NoParties group used in ExecutionReports and AllocationInstruction message.  The
 * NoParties group in contains all the data needed to identify a counter party.
 *
 * @author mwacker
 */
public class FixPartySub {

	/**
	 * The id of the party.
	 */
	private String partySubId;

	/**
	 * The type of sub party
	 */
	private PartySubIDTypes partySubIdType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof FixPartySub) {
			FixPartySub object = (FixPartySub) obj;
			return object.getPartySubId().equals(getPartySubId()) && object.getPartySubIdType() == getPartySubIdType();
		}
		return super.equals(obj);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getPartySubId() {
		return this.partySubId;
	}


	public void setPartySubId(String partyId) {
		this.partySubId = partyId;
	}


	public PartySubIDTypes getPartySubIdType() {
		return this.partySubIdType;
	}


	public void setPartySubIdType(PartySubIDTypes partySubIdType) {
		this.partySubIdType = partySubIdType;
	}
}
