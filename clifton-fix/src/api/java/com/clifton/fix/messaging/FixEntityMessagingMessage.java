package com.clifton.fix.messaging;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.FixExecutionReport;


/**
 * <Code>{@link FixEntityMessagingMessage}</Code> is a wrapper to send {@link FixEntity} instances over JMS.
 * <p>
 * This is to simplify consumption of messages on clients in different languages. This wraps the {@link FixEntity} object to facilitate sending the object itself, rather than the
 * Fix message text as is done in {@link FixMessageTextMessagingMessage}.
 *
 * @see FixMessageTextMessagingMessage
 */
public class FixEntityMessagingMessage extends BaseFixMessagingMessage {

	/**
	 * FixEntity Object
	 *
	 * @see FixEntity
	 * @see FixExecutionReport
	 */
	private FixEntity fixEntity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSessionQualifier() {
		if (getFixEntity() == null) {
			return null;
		}
		return getFixEntity().getFixSessionQualifier();
	}


	@Override
	@ValueChangingSetter
	public void setSessionQualifier(String sessionQualifier) {
		if (getFixEntity() != null) {
			getFixEntity().setFixSessionQualifier(sessionQualifier);
		}
	}


	@Override
	public String getDestinationName() {
		if (getFixEntity() == null) {
			return null;
		}
		return getFixEntity().getFixDestinationName();
	}


	@Override
	public void setIdentifier(FixIdentifier identifier) {
		if (getFixEntity() != null) {
			getFixEntity().setIdentifier(identifier);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixEntity getFixEntity() {
		return this.fixEntity;
	}


	public void setFixEntity(FixEntity fixEntity) {
		this.fixEntity = fixEntity;
	}
}
