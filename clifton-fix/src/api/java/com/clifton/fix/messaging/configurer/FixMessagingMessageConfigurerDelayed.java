package com.clifton.fix.messaging.configurer;


import com.clifton.core.util.StringUtils;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.order.FixEntity;
import com.clifton.fix.order.allocation.FixAllocationInstruction;


/**
 * The {@link FixMessagingMessageConfigurerDelayed} adds a message delay to
 * AllocationInstruction messages.
 * <p/>
 * NOTE:  It only adds messages to the initial allocation message which is determined
 * be whether or not referenceAllocationStringId message is populated.  If referenceAllocationStringId
 * then the message is a reallocation and does not need to be delayed.
 *
 * @author mwacker
 */
public class FixMessagingMessageConfigurerDelayed extends AbstractFixMessagingMessageConfigurer {

	/**
	 * The number of seconds to delay the message.
	 */
	private Integer delay;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void configure(BaseFixMessagingMessage message) {
		message.setMessageDelaySeconds(getDelay());
	}


	@Override
	public <T extends FixEntity> boolean applyTo(T entity) {
		// only delay the initial allocation messages
		if (entity instanceof FixAllocationInstruction) {
			FixAllocationInstruction e = (FixAllocationInstruction) entity;
			if (!StringUtils.isEmpty(e.getReferenceAllocationStringId())) {
				return false;
			}
		}
		return super.applyTo(entity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDelay() {
		return this.delay;
	}


	public void setDelay(Integer delay) {
		this.delay = delay;
	}
}
