package com.clifton.fix.messaging;

import com.clifton.core.messaging.AbstractMessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.fix.identifier.FixIdentifier;


/**
 * Base class for <code>FixMessagingMessage</code> and <code>FixEntityMessagingMessage</code> so that both can be processed
 * by <code>FixServerMessagingProcessor</code>
 *
 * @author lnaylor
 */
public abstract class BaseFixMessagingMessage extends AbstractMessagingMessage implements AsynchronousMessage {

	/**
	 * A 10 char tag value indicating the source of the message.
	 */
	private String sourceSystemTag;


	/**
	 * The identity of the object on the source system, used to return the new FixIdentity object.
	 * Replacement of sourceSystemId
	 */
	private Long sourceSystemFkFieldId;

	/**
	 * The identity of the object on the source system, used to return the new FixIdentity object.
	 * TODO: THIS IS NOW sourceSystemFkFieldId - supporting both temporarily
	 */
	private Long sourceSystemId;


	/**
	 * The number of seconds to delay message delivery.
	 */
	private Integer messageDelaySeconds;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "SourceID: " + getSourceSystemFkFieldId() + ", SourceSystemTag :" + getSourceSystemTag();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract String getSessionQualifier();


	public abstract void setSessionQualifier(String sessionQualifier);


	public abstract String getDestinationName();


	public abstract void setIdentifier(FixIdentifier identifier);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSourceSystemTag() {
		return this.sourceSystemTag;
	}


	public void setSourceSystemTag(String sourceSystemTag) {
		this.sourceSystemTag = sourceSystemTag;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public Long getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Long sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	@Override
	public Integer getMessageDelaySeconds() {
		return this.messageDelaySeconds;
	}


	public void setMessageDelaySeconds(Integer messageDelaySeconds) {
		this.messageDelaySeconds = messageDelaySeconds;
	}
}
