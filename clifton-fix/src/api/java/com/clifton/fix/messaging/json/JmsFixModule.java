package com.clifton.fix.messaging.json;

import com.clifton.fix.order.FixEntity;
import com.fasterxml.jackson.databind.module.SimpleModule;


/**
 * The {@link JmsFixModule} applies configurations for the JMS serialization and deserialization of fix objects.
 *
 * @author lnaylor
 */
public class JmsFixModule extends SimpleModule {

	@Override
	public void setupModule(SetupContext context) {
		context.setMixInAnnotations(FixEntity.class, JacksonFixEntityMixin.class);
	}
}
