package com.clifton.fix.messaging.xml;

import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.message.validation.FixValidationResult;
import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.messaging.FixEntityMessagingMessage;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.session.FixSessionParameters;
import com.clifton.fix.session.FixSessionParametersImpl;
import com.thoughtworks.xstream.XStream;
import org.springframework.stereotype.Component;


/**
 * The {@link FixMessagingXStreamConfigurer} provides {@link XStream} customizations to support serialization and deserialization for Fix messaging DTOs. Customizations may include
 * functionality to reduce the length of serialized content or support serialization and deserialization of otherwise-problematic types.
 *
 * @author MikeH
 */
@Component
public class FixMessagingXStreamConfigurer implements MessagingXStreamConfigurer {

	@Override
	public void configure(XStream xStream) {
		// FixIdentifier customization
		xStream.useAttributeFor(FixIdentifier.class, "uniqueStringIdentifier");
		xStream.useAttributeFor(FixIdentifier.class, "sourceSystem");
		xStream.aliasField("sourceSystemTag", FixIdentifier.class, "sourceSystem");
		xStream.omitField(FixIdentifier.class, "newlyCreated");

		// FixMessagingMessage customization
		xStream.useAttributeFor(FixMessageTextMessagingMessage.class, "sessionQualifier");
		xStream.useAttributeFor(BaseFixMessagingMessage.class, "sourceSystemTag");
		xStream.alias("com.clifton.fix.messaging.FixEntityMessagingMessage", FixEntityMessagingMessage.class);
		xStream.alias("fixEntityMessagingMessage", FixEntityMessagingMessage.class);
		xStream.alias("com.clifton.fix.messaging.FixMessagingMessage", FixMessageTextMessagingMessage.class); //TODO: Used for legacy support, remove when no longer necessary
		xStream.alias("fixMessageTextMessagingMessage", FixMessageTextMessagingMessage.class);

		// FixValidationResult customization
		xStream.useAttributeFor(FixValidationResult.class, "validationStatus");

		// FixSessionParametersImpl customization
		xStream.addDefaultImplementation(FixSessionParametersImpl.class, FixSessionParameters.class);
		xStream.aliasField("beginString", FixSessionParametersImpl.class, "fixBeginString");
		xStream.aliasField("senderCompID", FixSessionParametersImpl.class, "fixSenderCompId");
		xStream.aliasField("senderLocID", FixSessionParametersImpl.class, "fixSenderLocId");
		xStream.aliasField("senderSubID", FixSessionParametersImpl.class, "fixSenderSubId");
		xStream.aliasField("sessionQualifier", FixSessionParametersImpl.class, "fixSessionQualifier");
		xStream.aliasField("targetCompID", FixSessionParametersImpl.class, "fixTargetCompId");
		xStream.aliasField("targetLocID", FixSessionParametersImpl.class, "fixTargetLocId");
		xStream.aliasField("targetSubID", FixSessionParametersImpl.class, "fixTargetSubId");

		// Other customization
		xStream.registerConverter(new FixSessionXStreamConverter());
		xStream.registerConverter(new FixSourceSystemXStreamConverter());
	}
}
