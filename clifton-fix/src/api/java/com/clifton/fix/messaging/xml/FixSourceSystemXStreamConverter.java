package com.clifton.fix.messaging.xml;

import com.clifton.fix.setup.FixSourceSystem;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.SingleValueConverter;


/**
 * The {@link FixSourceSystemXStreamConverter} simplifies XML conversion for {@link FixSourceSystem} entities when registered on an {@link XStream} instance.
 *
 * @author MikeH
 */
public class FixSourceSystemXStreamConverter implements SingleValueConverter {

	@Override
	public String toString(Object obj) {
		return ((FixSourceSystem) obj).getSystemTag();
	}


	@Override
	public Object fromString(String str) {
		return new FixSourceSystem(str);
	}


	@Override
	public boolean canConvert(Class type) {
		return FixSourceSystem.class == type;
	}
}
