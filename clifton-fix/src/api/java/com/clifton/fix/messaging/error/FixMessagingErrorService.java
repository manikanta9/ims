package com.clifton.fix.messaging.error;


import com.clifton.core.messaging.jms.asynchronous.JmsAsynchronousErrorMessage;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.util.FixUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface FixMessagingErrorService {

	@SecureMethod(table = "FixMessageEntry")
	@RequestMapping("fixMessagingErrorList")
	public List<JmsAsynchronousErrorMessage> getMessageErrorList();


	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	@RequestMapping("fixMessagingErrorMoveToTarget")
	public void moveMessageErrorToTarget(String messageId, String targetQueue);


	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	@RequestMapping("fixMessagingErrorDeleteFromTarget")
	public void deleteMessageErrorFromTarget(String messageId, String targetQueue);
}
