package com.clifton.fix.messaging.configurer;


import com.clifton.core.util.CollectionUtils;
import com.clifton.fix.order.FixEntity;

import java.util.List;


/**
 * The <code>AbstractFixMessagingMessageConfigurer</code> defines a base message configurer.
 *
 * @author mwacker
 */
public abstract class AbstractFixMessagingMessageConfigurer implements FixMessagingMessageConfigurer {

	/**
	 * List of message classes to apply the configurer to.
	 */
	private List<Class<?>> messageClassList;
	/**
	 * A list of all session strings to apply this configurer to.  If this is null or empty, it will be applied to all messages
	 * in the type list.
	 */
	private List<String> sessionStringList;
	/**
	 * A list of all session strings to exclude from the configurer.
	 */
	private List<String> excludeSessionStringList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends FixEntity> boolean applyTo(T entity) {
		if (!getMessageClassList().contains(entity.getClass())) {
			return false;
		}
		if (!CollectionUtils.isEmpty(getSessionStringList()) && getSessionStringList().contains(entity.getFixSessionString())) {
			return true;
		}
		if (!CollectionUtils.isEmpty(getExcludeSessionStringList()) && getExcludeSessionStringList().contains(entity.getFixSessionString())) {
			return false;
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public List<Class<?>> getMessageClassList() {
		return this.messageClassList;
	}


	public void setMessageClassList(List<Class<?>> messageClassList) {
		this.messageClassList = messageClassList;
	}


	public List<String> getSessionStringList() {
		return this.sessionStringList;
	}


	public void setSessionStringList(List<String> sessionStringList) {
		this.sessionStringList = sessionStringList;
	}


	public List<String> getExcludeSessionStringList() {
		return this.excludeSessionStringList;
	}


	public void setExcludeSessionStringList(List<String> excludeSessionStringList) {
		this.excludeSessionStringList = excludeSessionStringList;
	}
}
