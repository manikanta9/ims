package com.clifton.fix.messaging.json;

import com.clifton.fix.ioi.FixIOIEntity;
import com.clifton.fix.market.data.refresh.FixMarketDataRefreshEntity;
import com.clifton.fix.order.FixDontKnowTrade;
import com.clifton.fix.order.FixEntityImpl;
import com.clifton.fix.order.FixExecutionReport;
import com.clifton.fix.order.FixMessageTextEntity;
import com.clifton.fix.order.FixOrder;
import com.clifton.fix.order.FixOrderCancelReject;
import com.clifton.fix.order.FixOrderCancelReplaceRequest;
import com.clifton.fix.order.FixOrderCancelRequest;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.FixTradeCaptureReport;
import com.clifton.fix.order.allocation.FixAllocationAcknowledgement;
import com.clifton.fix.order.allocation.FixAllocationInstruction;
import com.clifton.fix.order.allocation.FixAllocationReport;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


/**
 * The {@link JacksonFixEntityMixin} provides annotations for serialization and deserialization of <code>FixEntity</code> objects.
 * This mixin adds type information to fields which are sub-types of <code>FixEntity</code> and allows for deserialization to the provided sub-type.
 *
 * @author lnaylor
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
		@JsonSubTypes.Type(value = FixMessageTextEntity.class, name = "TEXT"),
		@JsonSubTypes.Type(value = FixEntityImpl.class, name = "ENTITY_IMPL"),
		@JsonSubTypes.Type(value = FixOrder.class, name = "ORDER"),
		@JsonSubTypes.Type(value = FixSecurityEntity.class, name = "SECURITY_ENTITY"),
		@JsonSubTypes.Type(value = FixOrderCancelRequest.class, name = "ORDER_CANCEL_REQUEST"),
		@JsonSubTypes.Type(value = FixOrderCancelReject.class, name = "ORDER_CANCEL_REJECT"),
		@JsonSubTypes.Type(value = FixAllocationReport.class, name = "ALLOCATION_REPORT"),
		@JsonSubTypes.Type(value = FixIOIEntity.class, name = "IOI_ENTITY"),
		@JsonSubTypes.Type(value = FixExecutionReport.class, name = "EXECUTION_REPORT"),
		@JsonSubTypes.Type(value = FixAllocationInstruction.class, name = "ALLOCATION_INSTRUCTION"),
		@JsonSubTypes.Type(value = FixOrderCancelReplaceRequest.class, name = "ORDER_CANCEL_REPLACE_REQUEST"),
		@JsonSubTypes.Type(value = FixDontKnowTrade.class, name = "DONT_KNOW_TRADE"),
		@JsonSubTypes.Type(value = FixAllocationAcknowledgement.class, name = "ALLOCATION_ACKNOWLEDGEMENT"),
		@JsonSubTypes.Type(value = FixMarketDataRefreshEntity.class, name = "MARKET_DATA_REFRESH"),
		@JsonSubTypes.Type(value = FixTradeCaptureReport.class, name = "TRADE_CAPTURE")
})
interface JacksonFixEntityMixin {
}
