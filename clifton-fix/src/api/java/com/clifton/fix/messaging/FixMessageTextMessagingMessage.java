package com.clifton.fix.messaging;


import com.clifton.fix.identifier.FixIdentifier;


/**
 * The {@link FixMessageTextMessagingMessage} is an asynchronous FIX message used to
 * send FIX message string to and from a FIX server.
 *
 * @author mwacker
 */
public class FixMessageTextMessagingMessage extends BaseFixMessagingMessage {

	/**
	 * The FIX message string.  NOTE: the message string is in FIX format.
	 */
	private String messageText;

	/**
	 * The identifier from the fix message server.
	 * <p/>
	 * NOTE: this is only used on messages coming from the server to the client.
	 */
	private FixIdentifier identifier;

	/**
	 * The destination name associated with a FIX session.
	 */
	private String destinationName;

	/**
	 * Extra session variable used to create multiple FIX connections with the same session settings.
	 */
	private String sessionQualifier;


	// Service-side validation results for non-allocation messages.
	private boolean validationError;

	// Service-side validation results for allocation messages.
	private boolean allocationValidationError;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMessageText() {
		return this.messageText;
	}


	public void setMessageText(String fixMessage) {
		this.messageText = fixMessage;
	}


	public FixIdentifier getIdentifier() {
		return this.identifier;
	}


	@Override
	public void setIdentifier(FixIdentifier identifier) {
		this.identifier = identifier;
	}


	@Override
	public String getDestinationName() {
		return this.destinationName;
	}


	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}


	@Override
	public String getSessionQualifier() {
		return this.sessionQualifier;
	}


	@Override
	public void setSessionQualifier(String sessionQualifier) {
		this.sessionQualifier = sessionQualifier;
	}


	public boolean isValidationError() {
		return this.validationError;
	}


	public void setValidationError(boolean validationError) {
		this.validationError = validationError;
	}


	public boolean isAllocationValidationError() {
		return this.allocationValidationError;
	}


	public void setAllocationValidationError(boolean allocationValidationError) {
		this.allocationValidationError = allocationValidationError;
	}
}
