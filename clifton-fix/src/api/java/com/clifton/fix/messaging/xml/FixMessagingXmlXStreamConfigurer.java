package com.clifton.fix.messaging.xml;


import com.clifton.core.messaging.converter.ContentTypeDelegatingMessageConverter;
import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.core.messaging.converter.xml.MessagingXStreamStringMapConverter;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.message.validation.FixValidationStatuses;
import com.clifton.fix.messaging.FixMessageTextMessagingMessage;
import com.clifton.fix.session.FixSessionParameters;
import com.clifton.fix.session.FixSessionParametersImpl;
import com.clifton.fix.setup.FixSourceSystem;
import com.thoughtworks.xstream.XStream;

import java.util.HashMap;


/**
 * The <code>FixMessagingXmlXStreamConfigurer</code> defines the custom XStream for FIX xml messages.
 * <p>
 * <b>Note: This is a legacy {@link MessagingXStreamConfigurer} used to support messages created without the use of {@link ContentTypeDelegatingMessageConverter} features. This
 * modifies global {@link XStream} configurations and should not be applied to shared {@link XStream} instances.</b>
 *
 * @author mwacker
 */
public class FixMessagingXmlXStreamConfigurer implements MessagingXStreamConfigurer {

	@Override
	public void configure(XStream xStream) {
		//xml.setMode(XStream.ID_REFERENCES);

		xStream.useAttributeFor(Boolean.class);
		xStream.useAttributeFor(boolean.class);
		xStream.useAttributeFor("machineName", String.class);
		xStream.useAttributeFor("uniqueStringIdentifier", String.class);
		xStream.useAttributeFor("sourceSystemTag", String.class);

		xStream.useAttributeFor(FixIdentifier.class, "sourceSystem");
		xStream.aliasField("sourceSystemTag", FixIdentifier.class, "sourceSystem");

		xStream.useAttributeFor("sourceSystemFkFieldId", String.class);
		xStream.useAttributeFor("sourceSystemId", String.class); // TODO REMOVE REPLACED BY sourceSystemFkFieldId
		xStream.useAttributeFor("sessionQualifier", String.class);
		xStream.useAttributeFor(int.class);
		xStream.useAttributeFor(long.class);
		xStream.useAttributeFor(short.class);
		xStream.useAttributeFor(Integer.class);
		xStream.useAttributeFor(Long.class);
		xStream.useAttributeFor(Short.class);

		xStream.useAttributeFor(FixValidationStatuses.class);
		xStream.useAttributeFor(FixSourceSystem.class);

		xStream.alias("message", FixMessageTextMessagingMessage.class);

		xStream.alias("properties", HashMap.class);
		xStream.alias("identifier", FixIdentifier.class);
		xStream.alias("result-message", FixMessage.class);
		xStream.alias("session", FixSessionParameters.class, FixSessionParametersImpl.class);

		xStream.aliasField("messageText", FixMessage.class, "text");

		xStream.aliasField("beginString", FixSessionParametersImpl.class, "fixBeginString");
		xStream.aliasField("senderCompID", FixSessionParametersImpl.class, "fixSenderCompId");
		xStream.aliasField("senderSubID", FixSessionParametersImpl.class, "fixSenderSubId");
		xStream.aliasField("senderLocID", FixSessionParametersImpl.class, "fixSenderLocId");
		xStream.aliasField("targetCompID", FixSessionParametersImpl.class, "fixTargetCompId");
		xStream.aliasField("targetSubID", FixSessionParametersImpl.class, "fixTargetSubId");
		xStream.aliasField("targetLocID", FixSessionParametersImpl.class, "fixTargetLocId");
		xStream.aliasField("sessionQualifier", FixSessionParametersImpl.class, "fixSessionQualifier");

		xStream.registerConverter(new MessagingXStreamStringMapConverter());
		xStream.registerConverter(new FixSessionXStreamConverter());
		xStream.registerConverter(new FixSourceSystemXStreamConverter());

		xStream.omitField(FixIdentifier.class, "newlyCreated");
	}
}
