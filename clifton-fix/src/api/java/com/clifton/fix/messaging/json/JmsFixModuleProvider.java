package com.clifton.fix.messaging.json;

import com.clifton.core.messaging.converter.json.JmsObjectMapperModuleProvider;
import com.fasterxml.jackson.databind.Module;
import org.springframework.stereotype.Component;


/**
 * The {@link JmsFixModuleProvider} is used to inject the {@link JmsFixModule} into the jmsObjectMapper bean
 * during configuration in <code>MessagingJmsConfiguration</code>.
 *
 * @author lnaylor
 */
@Component
public class JmsFixModuleProvider implements JmsObjectMapperModuleProvider {

	@Override
	public Module getModule() {
		return new JmsFixModule();
	}
}
