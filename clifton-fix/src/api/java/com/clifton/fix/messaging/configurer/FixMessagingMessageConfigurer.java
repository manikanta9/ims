package com.clifton.fix.messaging.configurer;


import com.clifton.fix.messaging.BaseFixMessagingMessage;
import com.clifton.fix.order.FixEntity;


/**
 * The {@link FixMessagingMessageConfigurer} defines an object that can be used to
 * configure a BaseFixMessagingMessage before it is sent.
 *
 * @author mwacker
 */
public interface FixMessagingMessageConfigurer {

	/**
	 * Configure the message.
	 *
	 * @param message
	 */
	public void configure(BaseFixMessagingMessage message);


	/**
	 * Determines if this configurer applies for the given entity.
	 */
	public <T extends FixEntity> boolean applyTo(T entity);
}
