package com.clifton.fix.messaging.xml;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


/**
 * The {@link FixSessionXStreamConverter} simplifies XML conversion for {@link FixSession} entities when registered on an {@link XStream} instance.
 *
 * @author MikeH
 */
public class FixSessionXStreamConverter implements Converter {

	@Override
	public boolean canConvert(Class type) {
		return FixSession.class == type;
	}


	@Override
	public void marshal(Object source, HierarchicalStreamWriter writer, @SuppressWarnings("unused") MarshallingContext context) {
		FixSession session = (FixSession) source;
		//writer.startNode("session-identifier");
		addNode("name", session.getDefinition().getName(), writer);
		addNode("description", session.getDefinition().getDescription(), writer);

		addNode("beginString", session.getDefinition().getFixBeginString(), writer);
		addNode("senderCompID", session.getDefinition().getFixSenderCompId(), writer);
		addNode("senderSubID", session.getDefinition().getFixSenderSubId(), writer);
		addNode("senderLocID", session.getDefinition().getFixSenderLocId(), writer);
		addNode("targetCompID", session.getDefinition().getFixTargetCompId(), writer);
		addNode("targetSubID", session.getDefinition().getFixTargetSubId(), writer);
		addNode("targetLocID", session.getDefinition().getFixTargetLocId(), writer);
		addNode("sessionQualifier", session.getDefinition().getFixSessionQualifier(), writer);
		//writer.endNode();
	}


	private void addNode(String name, String value, HierarchicalStreamWriter writer) {
		if (!StringUtils.isEmpty(value)) {
			writer.startNode(name);
			writer.setValue(value);
			writer.endNode();
		}
	}


	@Override
	public Object unmarshal(HierarchicalStreamReader reader, @SuppressWarnings("unused") UnmarshallingContext context) {
		FixSession session = new FixSession();
		FixSessionDefinition def = new FixSessionDefinition();
		session.setDefinition(def);

		while (reader.hasMoreChildren()) {
			reader.moveDown();
			BeanUtils.setPropertyValue(def, reader.getNodeName(), reader.getValue(), true);
			reader.moveUp();
		}

		return session;
	}
}
