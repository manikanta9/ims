package com.clifton.fix.tag;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.destination.FixDestinationTagModifierFilter;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.tag.search.FixTagModifierFilterSearchForm;
import com.clifton.system.bean.SystemBean;

import java.util.List;


public interface FixTagModifierService {

	////////////////////////////////////////////////////////////////////////////
	////////            FixTagModifier Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	public List<SystemBean> getFixTagModifierListBySession(FixSession session);


	public List<SystemBean> getFixTagModifierListBySessionDefinition(FixSessionDefinition fixSessionDefinition);

	////////////////////////////////////////////////////////////////////////////
	/////               FixTagModifierFilter Business Methods              /////
	////////////////////////////////////////////////////////////////////////////


	public FixTagModifierFilter getFixTagModifierFilter(int id);


	public List<FixTagModifierFilter> getFixTagModifierFilterListBySession(FixSession session);


	public List<FixTagModifierFilter> getFixTagModifierFilterListBySessionAndModifier(FixSession fixSession, SystemBean fixTagModifier);


	public List<FixTagModifierFilter> getFixTagModifierFilterListByModifier(int fixTagModifierId);


	public List<FixTagModifierFilter> getFixTagModifierFilterList(final FixTagModifierFilterSearchForm searchForm);


	public FixTagModifierFilter saveFixTagModifierFilter(FixTagModifierFilter bean);


	public void deleteFixTagModifierFilter(int id);


	/**
	 * Adds a new entry to the FixDestinationTagModifierFilter table.
	 */
	@SecureMethod(dtoClass = FixDestination.class)
	public FixDestinationTagModifierFilter linkFixTagModifierTagModifierFilterToTagModifierFilter(short fixDestinationId, int fixTagModifierId, int fixTagModifierFilterId);

	////////////////////////////////////////////////////////////////////////////
	////////            FixTagModifierEntry Business Methods            ////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = FixDestination.class)
	public FixTagModifierEntry getFixTagModifierEntry(short fixDestinationId, int fixTagModifierId);


	@SecureMethod(dtoClass = FixDestination.class, namingConventionViolation = true)
	public void saveFixTagModifierEntry(FixTagModifierEntry fixTagModifierEntry);
}
