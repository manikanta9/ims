package com.clifton.fix.tag;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.fix.destination.FixDestination;
import com.clifton.system.bean.SystemBean;

import java.util.List;


/**
 * <code>FixTagModifierEntry</code> DTO class used to save a specific Fix Destination's tag modifiers and filters in a single
 * operation.
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class FixTagModifierEntry {

	private FixDestination fixDestination;
	private int fixTagModifierOrder;
	private SystemBean fixTagModifier;
	private List<FixTagModifierFilter> fixTagModifierFilterList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixDestination getFixDestination() {
		return this.fixDestination;
	}


	public void setFixDestination(FixDestination fixDestination) {
		this.fixDestination = fixDestination;
	}


	public void setFixTagModifier(SystemBean fixTagModifier) {
		this.fixTagModifier = fixTagModifier;
	}


	public int getFixTagModifierOrder() {
		return this.fixTagModifierOrder;
	}


	public void setFixTagModifierOrder(int fixTagModifierOrder) {
		this.fixTagModifierOrder = fixTagModifierOrder;
	}


	public SystemBean getFixTagModifier() {
		return this.fixTagModifier;
	}


	public List<FixTagModifierFilter> getFixTagModifierFilterList() {
		return this.fixTagModifierFilterList;
	}


	public void setFixTagModifierFilterList(List<FixTagModifierFilter> fixTagModifierFilterList) {
		this.fixTagModifierFilterList = fixTagModifierFilterList;
	}
}
