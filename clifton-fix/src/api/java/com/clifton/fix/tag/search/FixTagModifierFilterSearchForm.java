package com.clifton.fix.tag.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class FixTagModifierFilterSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "fixDestinationTagModifierList.referenceOne.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short destinationId;

	@SearchField(searchField = "fixDestinationTagModifierList.referenceTwo.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer modifierId;

	@SearchField
	private Integer fieldTag;

	@SearchField
	private String fieldValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDestinationId() {
		return this.destinationId;
	}


	public void setDestinationId(Short destinationId) {
		this.destinationId = destinationId;
	}


	public Integer getModifierId() {
		return this.modifierId;
	}


	public void setModifierId(Integer modifierId) {
		this.modifierId = modifierId;
	}


	public Integer getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(Integer fieldTag) {
		this.fieldTag = fieldTag;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}
}
