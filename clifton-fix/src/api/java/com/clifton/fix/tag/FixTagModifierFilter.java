package com.clifton.fix.tag;


import com.clifton.core.beans.BaseEntity;


/**
 * The <code>FixSessionTagModifierFilter</code> used to filter messages for a tag modifier.
 *
 * @author mwacker
 */
public class FixTagModifierFilter extends BaseEntity<Integer> {

	/**
	 * The field tag to filter the message on.
	 */
	private int fieldTag;
	/**
	 * The value in the field tag to filter the message on.
	 */
	private String fieldValue;
	/**
	 * The name of the field tag modifier, unique and required.
	 */
	private String name;
	/**
	 * The description of the field tag modifier
	 */
	private String description;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return getFieldTag() + "=" + getFieldValue();
	}


	public int getFieldTag() {
		return this.fieldTag;
	}


	public void setFieldTag(int fieldTag) {
		this.fieldTag = fieldTag;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
