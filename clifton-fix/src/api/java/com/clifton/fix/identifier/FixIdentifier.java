package com.clifton.fix.identifier;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.fix.destination.FixDestination;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.setup.FixSourceSystem;


/**
 * The <code>FixIdentifier</code> represents an identifier used client systems to get
 * unique id's for their messages.
 *
 * @author mwacker
 */
public class FixIdentifier extends BaseSimpleEntity<Integer> {

	/**
	 * The identifier the this instance replaced. For example, we can resend order allocations: new identifier.
	 */
	private FixIdentifier referencedIdentifier;
	/**
	 * Tag the identifier with a session for fast look ups.
	 */
	private FixSession session;
	/**
	 * The unique id which was either generated or received for the message.  This string represents
	 * the exact value that will be in the id fields.
	 */
	private String uniqueStringIdentifier;


	/**
	 * The source system used to identify the internal system the created the identifier. For example, IMS.
	 */
	private FixSourceSystem sourceSystem;

	/**
	 * The identity of the object on the source system, used to return the new FixIdentity object.
	 */
	private Long sourceSystemFkFieldId;
	/**
	 * True if the identifier was generated, false if it represents and identifier created by another system.
	 */
	private boolean identifierGenerated;
	/**
	 * True if the identifier was created from an incoming message with no unique id in the main id field (FixMessageType.idFieldTag) causing the drop copy id field to be used.
	 */
	private boolean dropCopyIdentifier;
	/**
	 * The FIX Destination that should be associated with all messages using this identifier.
	 */
	private FixDestination fixDestination;
	/**
	 * Local property used to indicate that the identifier was just created.
	 */
	@NonPersistentField
	private boolean newlyCreated;


	/**
	 * Local property used to indicate that the identifier corresponds ta message that is persistent, and should therefore be persistent itself.
	 * If this property value is false, the identifier is created, but is not saved.
	 */
	@NonPersistentField
	private boolean persistent;


	/**
	 * Overridden to make XML serialization easier.
	 * <p>
	 * TODO: Find a better way to do this.
	 */
	private Integer id;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixIdentifier() {
		//
	}


	public FixIdentifier(String uniqueStringId) {
		this.uniqueStringIdentifier = uniqueStringId;
	}


	@Override
	public String toString() {
		return "SourceID: " + getSourceSystemFkFieldId() + ", SourceSystemTag :" + getSourceSystemTag() + ", UniqueStringID :" + getUniqueStringIdentifier();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSourceSystemTag() {
		return getSourceSystem() != null ? getSourceSystem().getSystemTag() : null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String generatedStringId) {
		this.uniqueStringIdentifier = generatedStringId;
	}


	public FixIdentifier getReferencedIdentifier() {
		return this.referencedIdentifier;
	}


	public void setReferencedIdentifier(FixIdentifier referencedIdentifier) {
		this.referencedIdentifier = referencedIdentifier;
	}


	public FixSession getSession() {
		return this.session;
	}


	public void setSession(FixSession session) {
		this.session = session;
	}


	public boolean isNewlyCreated() {
		return this.newlyCreated;
	}


	public void setNewlyCreated(boolean newlyCreated) {
		this.newlyCreated = newlyCreated;
	}


	public boolean isPersistent() {
		return this.persistent;
	}


	public void setPersistent(boolean persistent) {
		this.persistent = persistent;
	}


	public FixSourceSystem getSourceSystem() {
		return this.sourceSystem;
	}


	public void setSourceSystem(FixSourceSystem sourceSystem) {
		this.sourceSystem = sourceSystem;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public boolean isIdentifierGenerated() {
		return this.identifierGenerated;
	}


	public void setIdentifierGenerated(boolean identifierGenerated) {
		this.identifierGenerated = identifierGenerated;
	}


	@Override
	public Integer getIdentity() {
		return this.id;
	}


	@Override
	public Integer getId() {
		return this.id;
	}


	@Override
	public void setId(Integer id) {
		this.id = id;
	}


	public boolean isDropCopyIdentifier() {
		return this.dropCopyIdentifier;
	}


	public void setDropCopyIdentifier(boolean dropCopyIdentifier) {
		this.dropCopyIdentifier = dropCopyIdentifier;
	}


	public FixDestination getFixDestination() {
		return this.fixDestination;
	}


	public void setFixDestination(FixDestination fixDestination) {
		this.fixDestination = fixDestination;
	}
}
