package com.clifton.fix.identifier.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class FixIdentifierSearchForm extends BaseEntitySearchForm {

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String uniqueStringIdentifier;

	@SearchField(searchField = "uniqueStringIdentifier", comparisonConditions = ComparisonConditions.LIKE)
	private String uniqueStringIdentifierSearch;

	@SearchField(searchField = "uniqueStringIdentifier", searchFieldPath = "referencedIdentifier", comparisonConditions = ComparisonConditions.EQUALS)
	private String referencedUniqueStringIdentifier;

	@SearchField(searchField = "uniqueStringIdentifier", searchFieldPath = "referencedIdentifier", comparisonConditions = ComparisonConditions.LIKE)
	private String referencedUniqueStringIdentifierSearch;

	@SearchField(searchField = "session.id")
	private Short sessionId;

	@SearchField
	private Boolean dropCopyIdentifier;

	// Note: Where source system can be found with this tag by equals search, will replace this search field with the sourceSystemId
	@SearchField(searchFieldPath = "sourceSystem", searchField = "systemTag")
	private String sourceSystemTag;

	@SearchField(searchField = "sourceSystem.id")
	private Short sourceSystemId;

	@SearchField
	private Long sourceSystemFkFieldId;

	@SearchField
	private Boolean identifierGenerated;

	@SearchField(searchField = "fixDestination.id")
	private Short fixDestinationId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public String getUniqueStringIdentifierSearch() {
		return this.uniqueStringIdentifierSearch;
	}


	public void setUniqueStringIdentifierSearch(String uniqueStringIdentifierSearch) {
		this.uniqueStringIdentifierSearch = uniqueStringIdentifierSearch;
	}


	public String getReferencedUniqueStringIdentifier() {
		return this.referencedUniqueStringIdentifier;
	}


	public void setReferencedUniqueStringIdentifier(String referencedUniqueStringIdentifier) {
		this.referencedUniqueStringIdentifier = referencedUniqueStringIdentifier;
	}


	public String getReferencedUniqueStringIdentifierSearch() {
		return this.referencedUniqueStringIdentifierSearch;
	}


	public void setReferencedUniqueStringIdentifierSearch(String referencedUniqueStringIdentifierSearch) {
		this.referencedUniqueStringIdentifierSearch = referencedUniqueStringIdentifierSearch;
	}


	public Short getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(Short sessionId) {
		this.sessionId = sessionId;
	}


	public Boolean getDropCopyIdentifier() {
		return this.dropCopyIdentifier;
	}


	public void setDropCopyIdentifier(Boolean dropCopyIdentifier) {
		this.dropCopyIdentifier = dropCopyIdentifier;
	}


	public String getSourceSystemTag() {
		return this.sourceSystemTag;
	}


	public void setSourceSystemTag(String sourceSystemTag) {
		this.sourceSystemTag = sourceSystemTag;
	}


	public Short getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Short sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}


	public Boolean getIdentifierGenerated() {
		return this.identifierGenerated;
	}


	public void setIdentifierGenerated(Boolean identifierGenerated) {
		this.identifierGenerated = identifierGenerated;
	}


	public Short getFixDestinationId() {
		return this.fixDestinationId;
	}


	public void setFixDestinationId(Short fixDestinationId) {
		this.fixDestinationId = fixDestinationId;
	}
}
