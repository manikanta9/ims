package com.clifton.fix.identifier;


import com.clifton.fix.identifier.search.FixIdentifierSearchForm;

import java.util.List;


public interface FixIdentifierService {

	////////////////////////////////////////////////////////////////////////////
	////////             FixIdentifier Business Methods              /////////// 
	////////////////////////////////////////////////////////////////////////////


	public FixIdentifier getFixIdentifier(int id);


	public List<FixIdentifier> getFixIdentifierList(FixIdentifierSearchForm searchForm);
}
