package com.clifton.fix.ioi;

import com.clifton.fix.order.beans.StipulationTypes;

import java.util.Objects;


/**
 * A class specifying a FIX Stipulation entry which consists of a FixStipulationType (tag 233) and FixStipulationValue (tag 234).
 * This is a repeating element. The only usage of this class within IMS in the {@link FixIOIEntity} class.
 * <p>
 * For an HTGM IOI Message, A Stipulation is basically a condition that applies to an order / trade that is executed based on the Indication of Interest message.
 * In HTGM IOI "N" (new) type messages, there may be multiple stipulations:
 * <p>
 * • “MINQTY”: the minimum allowed quantity for an order.
 * • “MININCR”: the minimum quantity increment for an order.
 * • “MINTOKEEP”: the minimum quantity HTGM may be left with.
 * <p>
 * All of these quantity values are  in the units of par value.
 * <p>
 * The Stipulation Value then depends on the Stipulation Type, in this case its a numeric string value, for example:
 * StipulationValue “5000” for MINQTY would indicate the subsequent order related to this IOI Message would be require a minimum quantity of 5000 units.
 *
 * @author davidi
 */
public class FixStipulation {

	StipulationTypes stipulationType;
	String stipulationValue;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FixStipulation that = (FixStipulation) o;
		return this.stipulationType == that.stipulationType &&
				Objects.equals(this.stipulationValue, that.stipulationValue);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.stipulationType, this.stipulationValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public StipulationTypes getStipulationType() {
		return this.stipulationType;
	}


	public void setStipulationType(StipulationTypes stipulationType) {
		this.stipulationType = stipulationType;
	}


	public String getStipulationValue() {
		return this.stipulationValue;
	}


	public void setStipulationValue(String stipulationValue) {
		this.stipulationValue = stipulationValue;
	}
}
