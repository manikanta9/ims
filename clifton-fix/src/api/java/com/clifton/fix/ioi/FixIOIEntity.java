package com.clifton.fix.ioi;

import com.clifton.fix.order.FixParty;
import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.OrderSides;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * This class represents an Indication of Interest (IOI) Fix Message. Its purpose in IMS is to store IOI Fix Messages received from
 * a FIX Session.  The original data is received as a QuickFixMessage, and is converted to this class via the {@link com.clifton.fix.quickfix.converter.ioi.fix44.DefaultIOIMessageConverter}.
 * <p>
 * IOI Messages provide information regarding offers for financial instruments from a brokerage.  Currently it is used to
 * to capture HTGM’s offerings in municipal and corporate bonds.
 *
 * @author davidi
 */
public class FixIOIEntity extends FixSecurityEntity {

	/**
	 * A unique string identifier for this IOI message.
	 */
	private String ioiId;

	/**
	 * IOI Transaction Type: N = New, R = Replace, C = Cancel, A = Cancel All
	 */
	private String ioiTransactionType;

	/**
	 * IOI Reference ID.  Not present when 28=N or 28=A. Required when 28=C or 28=R
	 */
	private String ioiRefId;

	/**
	 * Product code values: 3=CORPORATE, 11=MUNICIPAL
	 */
	private Products product;

	/**
	 * Trade settlement date.
	 */
	private Date settlementDate;

	private OrderSides side;

	/**
	 * Quantity (e.g. number of shares) in numeric form or relative size (string value).
	 * Valid values:
	 * '0' ... '1000000000'
	 * 'S'	Small
	 * 'M'	Medium
	 * 'L'	Large
	 */
	private String ioiQuantity;

	/**
	 * Type of Price (tag 423) specified in Price field (tag 44)
	 */
	private PriceTypes priceType;

	private BigDecimal price;

	private Date transactionTime;

	private String text;

	private List<FixStipulation> stipulationList;

	private List<FixParty> partyList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getIoiId() {
		return this.ioiId;
	}


	public void setIoiId(String ioiId) {
		this.ioiId = ioiId;
	}


	public String getIoiTransactionType() {
		return this.ioiTransactionType;
	}


	public void setIoiTransactionType(String ioiTransactionType) {
		this.ioiTransactionType = ioiTransactionType;
	}


	public String getIoiRefId() {
		return this.ioiRefId;
	}


	public void setIoiRefId(String ioiRefId) {
		this.ioiRefId = ioiRefId;
	}


	public Products getProduct() {
		return this.product;
	}


	public void setProduct(Products product) {
		this.product = product;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public OrderSides getSide() {
		return this.side;
	}


	public void setSide(OrderSides side) {
		this.side = side;
	}


	public String getIoiQuantity() {
		return this.ioiQuantity;
	}


	public void setIoiQuantity(String ioiQuantity) {
		this.ioiQuantity = ioiQuantity;
	}


	public PriceTypes getPriceType() {
		return this.priceType;
	}


	public void setPriceType(PriceTypes priceType) {
		this.priceType = priceType;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public Date getTransactionTime() {
		return this.transactionTime;
	}


	public void setTransactionTime(Date transactionTime) {
		this.transactionTime = transactionTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public List<FixStipulation> getStipulationList() {
		return this.stipulationList;
	}


	public void setStipulationList(List<FixStipulation> stipulationList) {
		this.stipulationList = stipulationList;
	}


	public List<FixParty> getPartyList() {
		return this.partyList;
	}


	public void setPartyList(List<FixParty> partyList) {
		this.partyList = partyList;
	}
}
