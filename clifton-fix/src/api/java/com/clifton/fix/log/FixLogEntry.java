package com.clifton.fix.log;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.session.FixSession;

import java.util.Date;


/**
 * The <code>FixLogEntry</code> represents a FixEntry which represents an Administrative FIX Message or event.  This class is used with the FixLogEntryService to
 * avoid directly calling methods in the FixMessageEntry service. This class is non-persistent.
 *
 * @author davidi
 */
@NonPersistentObject
public class FixLogEntry {

	private Long id;
	private FixSession session;
	private FixMessageEntryType type;
	private Date messageEntryDateAndTime;
	private String text;
	/**
	 * Indicates that this is an incoming administrative Log entry.
	 */
	private Boolean incoming;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public FixSession getSession() {
		return this.session;
	}


	public void setSession(FixSession session) {
		this.session = session;
	}


	public FixMessageEntryType getType() {
		return this.type;
	}


	public void setType(FixMessageEntryType entryType) {
		this.type = entryType;
	}


	public Date getMessageEntryDateAndTime() {
		return this.messageEntryDateAndTime;
	}


	public void setMessageEntryDateAndTime(Date messageEntryDateAndTime) {
		this.messageEntryDateAndTime = messageEntryDateAndTime;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String entryText) {
		this.text = entryText;
	}


	public Boolean getIncoming() {
		return this.incoming;
	}


	public void setIncoming(Boolean incoming) {
		this.incoming = incoming;
	}
}
