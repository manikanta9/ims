package com.clifton.fix.log;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;

import java.util.List;


/**
 * A front-end service for to query FixLogEntry entities or administrative messages.  This service uses the FixMessageEntry service to perform
 * the actual queries and converts the results to FixLogEntry entities.
 *
 * @author davidi
 */
public interface FixLogEntryService {

	////////////////////////////////////////////////////////////////////////////
	////////               FixLogEntry Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////

	@SecureMethod(table = "FixMessageEntry")
	public FixLogEntry getFixLogEntry(long id);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixLogEntry> getFixLogEntryList(FixMessageEntrySearchForm searchForm);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixMessageField> getFixLogEntryFieldList(long id);
}
