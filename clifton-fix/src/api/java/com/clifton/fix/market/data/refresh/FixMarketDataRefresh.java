package com.clifton.fix.market.data.refresh;

import com.clifton.fix.order.FixSecurityEntity;
import com.clifton.fix.order.beans.PriceTypes;
import com.clifton.fix.order.beans.Products;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.OffsetDateTime;


/**
 * MarketDataRefresh represents market data entry for different market data types.
 */
public class FixMarketDataRefresh extends FixSecurityEntity {

	/**
	 * @see MarketDataActions
	 */
	private MarketDataActions actionType;

	/**
	 * @see MarketDataEntryTypes
	 */
	private MarketDataEntryTypes entryType;

	/**
	 * Usage dependent on MDUpdateAction:
	 * If New(0) will be a unique identifier for the entry;
	 * If Change(1) or Delete(2) will be a reference to the previous identifier for the entry.
	 */
	private String entryId;

	/**
	 * Unique Identifier for the quote. Represents BWIC-ID from upstream venues. Enabled per client request
	 */
	private String quoteEntryID;

	/**
	 * Product code values: 3=CORPORATE, 11=MUNICIPAL
	 */
	private Products product;

	/**
	 * Type of Price (tag 423) specified in Price field (tag 44)
	 */
	private PriceTypes priceType;

	/**
	 * Price of the market data entry.
	 * Presence dependent on MDUpdateAction: If New(0) or Change(1) always present;
	 * If Delete(2) not present.
	 */
	private BigDecimal price;

	/**
	 * Quantity available at the given MDEntryPx Presence dependent on MDUpdateAction:
	 * If New(0) or Change(1), always present;
	 * If Delete(2), not present.
	 */
	private BigDecimal quantitySize;

	/**
	 * Market Data entry date
	 */
	private LocalDate asOfDate;

	/**
	 * Market Data entry time
	 */
	private LocalTime asOfTime;

	/**
	 * Specifies the Current Balance
	 */
	private BigDecimal totalIssuedAmount;

	/**
	 * Concession Schedule Supported values: “A” or “B”
	 */
	private String concessionSchedule;

	/**
	 * Use when Bid or Offer represents an order (firm quote) Defined “Firm Until” timer
	 * Expressed in UTC
	 */
	private OffsetDateTime expiryTime;

	/**
	 * The time by which a meaningful response should arrive. “DUE- AT” time
	 * Format: UTCTimestamp
	 */
	private OffsetDateTime responseTime;

	/**
	 * Settlement date
	 * Format: YYYYMMDD
	 */
	private LocalDate settleDate;

	/**
	 * Book Name
	 */
	private String text;

	/**
	 * Time order was created. Format: UTC timestamp
	 */
	private OffsetDateTime transactTime;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataActions getActionType() {
		return this.actionType;
	}


	public void setActionType(MarketDataActions actionType) {
		this.actionType = actionType;
	}


	public MarketDataEntryTypes getEntryType() {
		return this.entryType;
	}


	public void setEntryType(MarketDataEntryTypes entryType) {
		this.entryType = entryType;
	}


	public String getEntryId() {
		return this.entryId;
	}


	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}


	public String getQuoteEntryID() {
		return this.quoteEntryID;
	}


	public void setQuoteEntryID(String quoteEntryID) {
		this.quoteEntryID = quoteEntryID;
	}


	public Products getProduct() {
		return this.product;
	}


	public void setProduct(Products product) {
		this.product = product;
	}


	public PriceTypes getPriceType() {
		return this.priceType;
	}


	public void setPriceType(PriceTypes priceType) {
		this.priceType = priceType;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getQuantitySize() {
		return this.quantitySize;
	}


	public void setQuantitySize(BigDecimal quantitySize) {
		this.quantitySize = quantitySize;
	}


	public LocalDate getAsOfDate() {
		return this.asOfDate;
	}


	public void setAsOfDate(LocalDate asOfDate) {
		this.asOfDate = asOfDate;
	}


	public LocalTime getAsOfTime() {
		return this.asOfTime;
	}


	public void setAsOfTime(LocalTime asOfTime) {
		this.asOfTime = asOfTime;
	}


	public BigDecimal getTotalIssuedAmount() {
		return this.totalIssuedAmount;
	}


	public void setTotalIssuedAmount(BigDecimal totalIssuedAmount) {
		this.totalIssuedAmount = totalIssuedAmount;
	}


	public String getConcessionSchedule() {
		return this.concessionSchedule;
	}


	public void setConcessionSchedule(String concessionSchedule) {
		this.concessionSchedule = concessionSchedule;
	}


	public OffsetDateTime getExpiryTime() {
		return this.expiryTime;
	}


	public void setExpiryTime(OffsetDateTime expiryTime) {
		this.expiryTime = expiryTime;
	}


	public OffsetDateTime getResponseTime() {
		return this.responseTime;
	}


	public void setResponseTime(OffsetDateTime responseTime) {
		this.responseTime = responseTime;
	}


	public LocalDate getSettleDate() {
		return this.settleDate;
	}


	public void setSettleDate(LocalDate settleDate) {
		this.settleDate = settleDate;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public OffsetDateTime getTransactTime() {
		return this.transactTime;
	}


	public void setTransactTime(OffsetDateTime transactTime) {
		this.transactTime = transactTime;
	}
}
