package com.clifton.fix.market.data.refresh;

import com.clifton.fix.order.FixEntityImpl;

import java.util.List;


/**
 * Fix entity for MarketData incremental refresh
 */
public class FixMarketDataRefreshEntity extends FixEntityImpl {

	/**
	 * Unique identifier allocated by the client for the subscription. When unsubscribing (SubscriptionRequestType = 2), the same ID must be supplied.
	 */
	private String marketDataRequestId;

	/**
	 * Number of market date entries in this message.
	 */
	private Integer numberOfEntries;

	/**
	 * Array of Market Data objects.
	 */
	private List<FixMarketDataRefresh> data;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMarketDataRequestId() {
		return this.marketDataRequestId;
	}


	public void setMarketDataRequestId(String marketDataRequestId) {
		this.marketDataRequestId = marketDataRequestId;
	}


	public Integer getNumberOfEntries() {
		return this.numberOfEntries;
	}


	public void setNumberOfEntries(Integer numberOfEntries) {
		this.numberOfEntries = numberOfEntries;
	}


	public List<FixMarketDataRefresh> getData() {
		return this.data;
	}


	public void setData(List<FixMarketDataRefresh> data) {
		this.data = data;
	}
}
