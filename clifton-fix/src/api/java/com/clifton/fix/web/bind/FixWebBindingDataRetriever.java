package com.clifton.fix.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.web.bind.WebBindingDataRetriever;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;
import java.util.regex.Pattern;


/**
 * @author manderson
 */
public class FixWebBindingDataRetriever implements WebBindingDataRetriever {

	private FixWebBindingDataRetrieverService fixWebBindingDataRetrieverService;

	private int order;
	private Pattern requestUriPattern;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	@Override
	public boolean isApplicableForRequest(WebRequest request) {
		if (this.requestUriPattern == null) {
			return true;
		}
		String uri = request.getDescription(false);
		return this.requestUriPattern.matcher(uri).matches();
	}


	@Override
	public <T extends IdentityObject> T getEntity(Class<T> entityType, Serializable entityId) {
		return getFixWebBindingDataRetrieverService().getFixWebBindingObject(entityType.getName(), (Number) entityId);
	}


	@Override
	public List<Column> getEntityColumnList(Class<? extends IdentityObject> entityType) {
		return getFixWebBindingDataRetrieverService().getFixWebBindingObjectColumnList(entityType.getName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public FixWebBindingDataRetrieverService getFixWebBindingDataRetrieverService() {
		return this.fixWebBindingDataRetrieverService;
	}


	public void setFixWebBindingDataRetrieverService(FixWebBindingDataRetrieverService fixWebBindingDataRetrieverService) {
		this.fixWebBindingDataRetrieverService = fixWebBindingDataRetrieverService;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public Pattern getRequestUriPattern() {
		return this.requestUriPattern;
	}


	public void setRequestUriPattern(Pattern requestUriPattern) {
		this.requestUriPattern = requestUriPattern;
	}
}
