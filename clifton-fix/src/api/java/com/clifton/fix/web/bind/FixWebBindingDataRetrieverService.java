package com.clifton.fix.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * The FixWebBindingDataRetrieverService interface defines methods that external callers
 * need to do proper binding and validation of fix objects.
 *
 * @author manderson
 */
public interface FixWebBindingDataRetrieverService {

	/**
	 * Returns the DTO of the specified type for the specified primary key.
	 */
	@SecureMethod(disableSecurity = true)
	public <T extends IdentityObject> T getFixWebBindingObject(String objectClassName, Number objectId);


	/**
	 * Returns Column meta-data for each column of the specified DTO type.
	 */
	@SecureMethod(disableSecurity = true)
	public List<Column> getFixWebBindingObjectColumnList(String objectClassName);
}
