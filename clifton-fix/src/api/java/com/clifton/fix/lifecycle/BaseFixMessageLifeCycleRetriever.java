package com.clifton.fix.lifecycle;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.system.lifecycle.SystemLifeCycleEvent;
import com.clifton.system.lifecycle.SystemLifeCycleEventContext;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleEventRetriever;
import com.clifton.system.schema.SystemSchemaService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>FixMessageLifeCycleRetriever</code> can be extended and implemented for objects that use Fix Messages and want to include them in the life cycle events
 * This class handles converting the messages to life cycle events.
 *
 * @author manderson
 */
public abstract class BaseFixMessageLifeCycleRetriever implements SystemLifeCycleEventRetriever {

	private SystemSchemaService systemSchemaService;


	public abstract List<FixMessage> getFixMessageList(SystemLifeCycleEventContext lifeCycleEventContext);


	@Override
	public List<SystemLifeCycleEvent> getSystemLifeCycleEventListForEntity(SystemLifeCycleEventContext lifeCycleEventContext) {
		List<SystemLifeCycleEvent> lifeCycleEventList = new ArrayList<>();
		try {
			List<FixMessage> fixMessageList = getFixMessageList(lifeCycleEventContext);
			if (!CollectionUtils.isEmpty(fixMessageList)) {
				fixMessageList.forEach(fixMessage -> lifeCycleEventList.add(convertToSystemLifeCycleEvent(fixMessage, lifeCycleEventContext)));
			}
		}
		catch (Exception e) {
			// Add record for unable to retrieve FIX messages for Placement
			SystemLifeCycleEvent event = new SystemLifeCycleEvent();
			event.setLinkedSystemTable(lifeCycleEventContext.getSystemTableByName(getSystemSchemaService(), lifeCycleEventContext.getCurrentProcessingEntityTableName()));
			event.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(lifeCycleEventContext.getCurrentProcessingEntity()));
			event.setEventDate(new Date());
			event.setEventSource("Fix Message");
			event.setEventAction("Unknown");
			event.setEventDetails("Unable to retrieve FIX Message Details: " + ExceptionUtils.getOriginalMessage(e));
			lifeCycleEventList.add(event);
			// Log error or just ignore?
		}
		return lifeCycleEventList;
	}


	private SystemLifeCycleEvent convertToSystemLifeCycleEvent(FixMessage fixMessage, SystemLifeCycleEventContext lifeCycleEventContext) {
		SystemLifeCycleEvent event = new SystemLifeCycleEvent();
		event.setLinkedSystemTable(lifeCycleEventContext.getSystemTableByName(getSystemSchemaService(), lifeCycleEventContext.getCurrentProcessingEntityTableName()));
		event.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(lifeCycleEventContext.getCurrentProcessingEntity()));

		event.setEventDate(fixMessage.getMessageEntryDateAndTime());
		event.setEventSource("Fix Message");
		event.setEventAction(fixMessage.getType().getName());
		event.setEventDetails(getEventDetailsForFixMessage(fixMessage));
		return event;
	}


	protected String getEventDetailsForFixMessage(FixMessage fixMessage) {
		StringBuilder details = new StringBuilder(20);
		details.append(BooleanUtils.isTrue(fixMessage.getIncoming()) ? "Received " : "Sent ");
		details.append(fixMessage.getType().getName());
		details.append(BooleanUtils.isTrue(fixMessage.getIncoming()) ? " from " : " to ");
		details.append(fixMessage.getSession().getDefinition().getLabel());
		return details.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
