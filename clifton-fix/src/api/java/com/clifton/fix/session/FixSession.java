package com.clifton.fix.session;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;

import java.util.Date;


/**
 * The <code>FixSession</code> defines an instance of FixSessionDefinition.
 * <p/>
 * NOTE: This class can support a new session each time the server is restarted or the
 * sequence numbers are reset.  However, it is currently does not use the active flag
 * and there is a 1-1 relationship to the FixSessionDefinition.
 *
 * @author mwacker
 */
public class FixSession extends BaseSimpleEntity<Short> {

	private FixSessionDefinition definition;
	private Date creationDateAndTime;

	/**
	 * Sequence Number Values saved in the database
	 * If we don't save after every increment, then it may not match the actual sequence number
	 * <p>
	 * Incoming = QuickFix Store Target Message Sequence Number
	 * Outgoing = QuickFix Store Sender Message Sequence Number
	 */
	private int incomingSequenceNumber;
	private int outgoingSequenceNumber;

	/**
	 * Live sequence number values (see FixSessionSequenceNumberHandlerImpl)
	 */
	@NonPersistentField
	private Integer liveIncomingSequenceNumber;
	@NonPersistentField
	private Integer liveOutgoingSequenceNumber;

	/**
	 * Indicates that this is the current session, only one session can be active at a time.
	 */
	private boolean activeSession;
	@NonPersistentField
	private boolean running;
	@NonPersistentField
	private boolean loggedOn;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(FixSessionDefinition definition) {
		this.definition = definition;
	}


	public Date getCreationDateAndTime() {
		return this.creationDateAndTime;
	}


	public void setCreationDateAndTime(Date creationDateAndTime) {
		this.creationDateAndTime = creationDateAndTime;
	}


	public int getIncomingSequenceNumber() {
		return this.incomingSequenceNumber;
	}


	public void setIncomingSequenceNumber(int incomingSequenceNumber) {
		this.incomingSequenceNumber = incomingSequenceNumber;
	}


	public int getOutgoingSequenceNumber() {
		return this.outgoingSequenceNumber;
	}


	public void setOutgoingSequenceNumber(int outgoingSequenceNumber) {
		this.outgoingSequenceNumber = outgoingSequenceNumber;
	}


	public Integer getLiveIncomingSequenceNumber() {
		return this.liveIncomingSequenceNumber;
	}


	public void setLiveIncomingSequenceNumber(Integer liveIncomingSequenceNumber) {
		this.liveIncomingSequenceNumber = liveIncomingSequenceNumber;
	}


	public Integer getLiveOutgoingSequenceNumber() {
		return this.liveOutgoingSequenceNumber;
	}


	public void setLiveOutgoingSequenceNumber(Integer liveOutgoingSequenceNumber) {
		this.liveOutgoingSequenceNumber = liveOutgoingSequenceNumber;
	}


	public boolean isActiveSession() {
		return this.activeSession;
	}


	public void setActiveSession(boolean activeSession) {
		this.activeSession = activeSession;
	}


	public boolean isRunning() {
		return this.running;
	}


	public void setRunning(boolean running) {
		this.running = running;
	}


	public boolean isLoggedOn() {
		return this.loggedOn;
	}


	public void setLoggedOn(boolean loggedOn) {
		this.loggedOn = loggedOn;
	}
}
