package com.clifton.fix.session;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;


/**
 * The <code>FixSessionParametersImpl</code> and implementation of FixSessionParameters that can read
 * the properties of a provided object to extract the session variables.
 *
 * @author mwacker
 */
public class FixSessionParametersImpl implements FixSessionParameters {

	/**
	 * The FIX version string.
	 * <p/>
	 * FIX.4.2,FIX.4.4, etc.
	 */
	private String fixBeginString;
	/**
	 * The sending company id.
	 */
	private String fixSenderCompId;
	private String fixSenderSubId;
	private String fixSenderLocId;
	/**
	 * The target company id.
	 */
	private String fixTargetCompId;
	private String fixTargetSubId; // TKTS, GSDESK, etc.
	private String fixTargetLocId;
	/**
	 * FIX session qualifier used to distinguish
	 * other wise identical sessions.
	 */
	private String fixSessionQualifier;

	private Boolean incoming;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionParametersImpl() {
		//
	}


	public <T> FixSessionParametersImpl(T fromObject) {
		this(fromObject, null);
	}


	public <T> FixSessionParametersImpl(T fromObject, Boolean incoming) {
		this.fixBeginString = getPropertyValue(fromObject, "fixBeginString", "beginString");
		this.fixSenderCompId = getPropertyValue(fromObject, "fixSenderCompId", "senderCompID", "senderCompId");
		this.fixSenderSubId = getPropertyValue(fromObject, "fixSenderSubId", "senderSubID", "senderSubId");
		this.fixSenderLocId = getPropertyValue(fromObject, "fixSenderLocId", "senderLocID", "senderLocId");

		this.fixTargetCompId = getPropertyValue(fromObject, "fixTargetCompId", "targetCompID", "targetCompId");
		this.fixTargetSubId = getPropertyValue(fromObject, "fixTargetSubId", "targetSubID", "targetSubId");
		this.fixTargetLocId = getPropertyValue(fromObject, "fixTargetLocId", "targetLocID", "targetLocId");
		this.fixSessionQualifier = getPropertyValue(fromObject, "fixSessionQualifier", "sessionQualifier");

		this.incoming = incoming;
	}


	public FixSessionParametersImpl(String fixBeginString, String fixSenderCompId, String fixTargetCompId) {
		this(fixBeginString, fixSenderCompId, null, null, fixTargetCompId, null, null, null);
	}


	public FixSessionParametersImpl(String fixBeginString, String fixSenderCompId, String fixSenderSubId, String fixSenderLocId, String fixTargetCompId, String fixTargetSubId, String fixTargetLocId, String fixSessionQualifier) {
		this.fixBeginString = fixBeginString;
		this.fixSenderCompId = fixSenderCompId;
		this.fixSenderSubId = fixSenderSubId;
		this.fixSenderLocId = fixSenderLocId;

		this.fixTargetCompId = fixTargetCompId;
		this.fixTargetSubId = fixTargetSubId;
		this.fixTargetLocId = fixTargetLocId;
		this.fixSessionQualifier = fixSessionQualifier;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getPropertyValue(Object fromObject, String... propertyNames) {
		for (String propertyName : propertyNames) {
			Object value = BeanUtils.getPropertyValue(fromObject, propertyName);
			if ((value instanceof String) && !StringUtils.isEmpty((String) value)) {
				return (String) value;
			}
		}
		return null;
	}


	public String[] toArray() {
		return new String[]{getFixBeginString(), getFixSenderCompId(), getFixSenderSubId(), getFixSenderLocId(), getFixTargetCompId(), getFixTargetSubId(), getFixTargetLocId(),
				getFixSessionQualifier()};
	}


	@Override
	public String toString() {
		return getFixBeginString() + ":" + getFixSenderCompId() + ":" + getFixSenderSubId() + ":" + getFixSenderLocId() + ":" + getFixTargetCompId() + ":" + getFixTargetSubId() + ":"
				+ getFixTargetLocId() + ":" + getFixSessionQualifier();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getFixBeginString() {
		return this.fixBeginString;
	}


	public void setFixBeginString(String fixBeginString) {
		this.fixBeginString = fixBeginString;
	}


	@Override
	public String getFixSenderCompId() {
		return this.fixSenderCompId;
	}


	public void setFixSenderCompId(String fixSenderCompId) {
		this.fixSenderCompId = fixSenderCompId;
	}


	@Override
	public String getFixSenderSubId() {
		return this.fixSenderSubId;
	}


	public void setFixSenderSubId(String fixSenderSubId) {
		this.fixSenderSubId = fixSenderSubId;
	}


	@Override
	public String getFixSenderLocId() {
		return this.fixSenderLocId;
	}


	public void setFixSenderLocId(String fixSenderLocId) {
		this.fixSenderLocId = fixSenderLocId;
	}


	@Override
	public String getFixTargetCompId() {
		return this.fixTargetCompId;
	}


	public void setFixTargetCompId(String fixTargetCompId) {
		this.fixTargetCompId = fixTargetCompId;
	}


	@Override
	public String getFixTargetSubId() {
		return this.fixTargetSubId;
	}


	public void setFixTargetSubId(String fixTargetSubId) {
		this.fixTargetSubId = fixTargetSubId;
	}


	@Override
	public String getFixTargetLocId() {
		return this.fixTargetLocId;
	}


	public void setFixTargetLocId(String fixTargetLocId) {
		this.fixTargetLocId = fixTargetLocId;
	}


	@Override
	public String getFixSessionQualifier() {
		return this.fixSessionQualifier;
	}


	public void setFixSessionQualifier(String fixSessionQualifier) {
		this.fixSessionQualifier = fixSessionQualifier;
	}


	public Boolean getIncoming() {
		return this.incoming;
	}


	public void setIncoming(Boolean incoming) {
		this.incoming = incoming;
	}
}
