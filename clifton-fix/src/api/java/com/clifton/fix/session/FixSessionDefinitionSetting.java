package com.clifton.fix.session;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;


/**
 * The <code>FixSessionDefinitionSetting</code> defines FIX session setting.
 * <p/>
 * NOTE:  This is hash style configuration and the user is required to know how to configure a FIX session. A configuration guide can be found at
 * <a href="https://www.quickfixj.org/usermanual/2.1.0/usage/configuration.html">https://www.quickfixj.org/usermanual/2.1.0/usage/configuration.html</a>.
 *
 * @author mwacker
 */
public class FixSessionDefinitionSetting extends BaseEntity<Integer> implements LabeledObject {

	/**
	 * The FixSession this setting is associated with
	 */
	private FixSessionDefinition sessionDefinition;

	/**
	 * The setting name, i.e. LogHeartbeats, EnabledProtocols, SocketUseSSL
	 */
	private String settingName;

	/**
	 * The value of the setting, i.e. Y or N (for Yes/No options), TLSv1.2 (Enabled Protocols)
	 */
	private String settingValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return getSettingName();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinition getSessionDefinition() {
		return this.sessionDefinition;
	}


	public void setSessionDefinition(FixSessionDefinition sessionDefinition) {
		this.sessionDefinition = sessionDefinition;
	}


	public String getSettingName() {
		return this.settingName;
	}


	public void setSettingName(String settingName) {
		this.settingName = settingName;
	}


	public String getSettingValue() {
		return this.settingValue;
	}


	public void setSettingValue(String settingValue) {
		this.settingValue = settingValue;
	}
}
