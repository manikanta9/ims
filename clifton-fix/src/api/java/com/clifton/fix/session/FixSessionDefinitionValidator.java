package com.clifton.fix.session;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The {@link FixSessionDefinitionValidator} validates {@link FixSessionDefinition} entity modifications.
 *
 * @author MikeH
 */
@Component
public class FixSessionDefinitionValidator extends SelfRegisteringDaoValidator<FixSessionDefinition> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(FixSessionDefinition bean, DaoEventTypes config) throws ValidationException {
		// Validate weekday selection (for reference, runtime conversion is performed by quickfix.DayConverter)
		if (bean.getStartWeekday() != null) {
			ValidationUtils.assertNotNull(bean.getEndWeekday(), "An ending weekday must be provided if a starting weekday is given.", "endWeekday");
			ValidationUtils.assertNotNull(DateUtils.getWeekdayNameOrNull(bean.getStartWeekday()), String.format("The start weekday for the given value ([%d]) could not be found.", bean.getStartWeekday()), "startWeekday");
		}
		if (bean.getEndWeekday() != null) {
			ValidationUtils.assertNotNull(bean.getStartWeekday(), "A starting weekday must be provided if an ending weekday is given.", "startWeekday");
			ValidationUtils.assertNotNull(DateUtils.getWeekdayNameOrNull(bean.getEndWeekday()), String.format("The end weekday for the given value ([%d]) could not be found.", bean.getEndWeekday()), "endWeekday");
		}
	}
}
