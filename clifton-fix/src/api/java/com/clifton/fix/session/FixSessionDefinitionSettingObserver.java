package com.clifton.fix.session;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;


/**
 * The {@link FixSessionDefinitionSettingObserver} is the default observer for {@link FixSessionDefinitionSetting} entities.
 *
 * @author MikeH
 */
@Component
public class FixSessionDefinitionSettingObserver extends SelfRegisteringDaoObserver<FixSessionDefinitionSetting> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<FixSessionDefinitionSetting> dao, DaoEventTypes event, FixSessionDefinitionSetting bean) {
		// Trim setting name and value; settings should not have leading or trailing spaces, and such whitespace is typically in error
		bean.setSettingName(StringUtils.trim(bean.getSettingName()));
		bean.setSettingValue(StringUtils.trim(bean.getSettingValue()));
	}
}
