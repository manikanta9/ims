package com.clifton.fix.session;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.fix.message.FixMessageService;
import org.springframework.stereotype.Component;

import java.util.Objects;


/**
 * The {@link FixSessionDefinitionObserver} is the default observer for {@link FixSessionDefinition} entities.
 *
 * @author MikeH
 */
@Component
public class FixSessionDefinitionObserver extends SelfRegisteringDaoObserver<FixSessionDefinition> {

	private FixSessionService fixSessionService;
	private FixMessageService fixMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<FixSessionDefinition> dao, DaoEventTypes event, FixSessionDefinition bean) {
		if (event.isUpdate()) {
			// Cache original bean for after-transaction callbacks
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<FixSessionDefinition> dao, DaoEventTypes event, FixSessionDefinition bean, Throwable e) {
		if (e == null) {
			// Update non-persistent message capacity size if modified
			FixSessionDefinition originalBean = getOriginalBean(dao, bean);
			if (!Objects.equals(originalBean.getCoalesceNonPersistentMessageCacheSize(), bean.getCoalesceNonPersistentMessageCacheSize())) {
				FixSession fixSession = getFixSessionService().getFixSessionCurrentByDefinition(bean);
				if (fixSession != null) {
					getFixMessageService().updateFixNonPersistentMessageCapacityForSession(fixSession, bean.getCoalesceNonPersistentMessageCacheSize());
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionService getFixSessionService() {
		return this.fixSessionService;
	}


	public void setFixSessionService(FixSessionService fixSessionService) {
		this.fixSessionService = fixSessionService;
	}


	public FixMessageService getFixMessageService() {
		return this.fixMessageService;
	}


	public void setFixMessageService(FixMessageService fixMessageService) {
		this.fixMessageService = fixMessageService;
	}
}
