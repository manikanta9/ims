package com.clifton.fix.session;


/**
 * The <code>FixSessionParameters</code> defines an object used to pass the parameters
 * need to find the FixSessionDefinition for the FIX object services.
 *
 * @author mwacker
 */
public interface FixSessionParameters {

	/**
	 * The FIX version string.
	 * <p/>
	 * FIX.4.2,FIX.4.4, etc.
	 */
	public String getFixBeginString();


	/**
	 * The sending company id.
	 */
	public String getFixSenderCompId();


	public String getFixSenderSubId();


	public String getFixSenderLocId();


	/**
	 * The target company id.
	 */
	public String getFixTargetCompId();


	public String getFixTargetSubId();


	public String getFixTargetLocId();


	/**
	 * FIX session qualifier used to distinguish
	 * other wise identical sessions.
	 */
	public String getFixSessionQualifier();
}
