package com.clifton.fix.session;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.ObjectUtils;
import com.clifton.fix.message.FixMessageEntryType;
import com.clifton.fix.setup.FixSourceSystem;
import com.clifton.system.list.SystemListItem;

import java.util.List;


/**
 * The {@link FixSessionDefinition} defines a FIX session and the common properties for a session.
 * <p>
 * Many of the fields in this type are used to determine FIX session settings. These are mapped to session settings via {@link FixSessionSettingsConfigurer} implementations.
 *
 * @author mwacker
 */
public class FixSessionDefinition extends NamedEntity<Short> implements FixSessionParameters {


	/**
	 * Used for Informational Purposes Only
	 * Defines how the connection is established. i.e. VPN, Physical Circuit, Internet
	 */
	private SystemListItem connectivityType;

	/**
	 * This definition is disabled, so no session will be created for it.
	 */
	private boolean disabled;

	/**
	 * Indicates that this session if for information purposes only and the messages should not be sent other applications.
	 */
	private boolean informational;

	/**
	 * Determines the parent session definition.  The parent session settings will be applied to each new session then overridden
	 * by the specific session settings.
	 * <p>
	 * NOTE: No parent session is created, it is only used to apply properties to other sessions.
	 */
	private FixSessionDefinition parent;

	/**
	 * Indicates if an actual session can be created from these settings or if it's used as a parent for default settings.
	 */
	private boolean createSession;

	/**
	 * The environment that the server is running in.  This allows us to control the connection that each
	 * server instance is allowed to run.
	 */
	private String serverEnvironment = "DEV";

	/**
	 * The message queue used by the session.
	 * Note: Not currently used
	 */
	private String messageQueue;

	/**
	 * acceptor or initiator
	 * Initiator is the FIX term for client - we use an Initiator when we are connecting to another party.
	 * Acceptor is the FIX term for server - we use an Acceptor when other parties are connecting to us.
	 * Note: All of our existing session definitions are all initiators
	 */
	private String connectionType;

	/**
	 * The time zone of this connection.
	 * i.e. America/Chicago
	 */
	private String timeZone;

	/**
	 * The first day of week (1-7) & Null in which the underlying Fix Service is available.
	 * <p>
	 * <b>Note: For daily Fix Sessions, this is used for health checks only, indicating the starting weekday range in which the session should be running.
	 * If the session is configured as a weekly session (weeklySession == true), this represents the weekday on which the session will be started.</b>
	 */
	private Integer startWeekday;

	/**
	 * The last day of week (1-7) & Null in which the underlying Fix Service is available.
	 * <p>
	 * <b>Note: For daily Fix Sessions, this is used for health checks only, indicating the ending weekday range in which we expect the session to be running.
	 * If the session is configured as a weekly session (weeklySession == true), this represents the weekday on which the session will be stopped.</b>
	 */
	private Integer endWeekday;

	/**
	 * String representation of the time of day that the session will begin.  Time must
	 * be in the same time as the above time zone.
	 * i.e. 06:00:00 US/Central
	 */
	private String startTime;

	/**
	 * String representation of the time of day that the session will end.  Time must
	 * be in the same time as the above time zone.
	 * i.e. 21:00:00 US/Central
	 */
	private String endTime;

	/**
	 *  If this property is set to True, the session is considered to be a weekly session, and will be restarted on a weekly interval instead of a daily interval.
	 *  if this property is set to False (the default value), the session is considered to be a daily session, and is stopped and restarted daily.
	 */
	private boolean weeklySession;

	/**
	 * The time period (seconds) between heartbeats.
	 * usually 30 or 60 seconds
	 * only used for initiators
	 */
	private Integer heartbeatInterval;

	/**
	 * The time period (seconds) between connection attempts.
	 * 5 seconds
	 * only used for initiators
	 */
	private Integer reconnectInterval;

	/**
	 * The FIX version string.
	 * <p>
	 * FIX.4.2,FIX.4.4, etc.
	 */
	private String beginString;

	/**
	 * PPA (Internal) compID as associated with this FIX session
	 */
	private String senderCompId;

	/**
	 * (Optional) PPA (Internal) subID as associated with this FIX session
	 */
	private String senderSubId;


	/**
	 * (Optional) PPA (Internal) locationID as associated with this FIX session
	 */
	private String senderLocId;


	/**
	 * Counterparty's compID as associated with this FIX session
	 */
	private String targetCompId;

	/**
	 * (Optional) Counterparty's subID as associated with this FIX session
	 */
	private String targetSubId;


	/**
	 * (Optional) Counterparty's locationID as associated with this FIX session
	 */
	private String targetLocId;

	/**
	 * Additional qualifier to disambiguate otherwise identical sessions. This can only be used with initiator sessions.
	 */
	private String sessionQualifier;


	/**
	 * Path of the Fix SessionDictionary File. If config xml is provided this
	 * Dictionary will be used. The default Dict will be picked up if this is not provided
	 */
	private String sessionDictionaryLocation;

	/**
	 * The host address.
	 */
	private String socketConnectHost;

	/**
	 * The host port.
	 */
	private String socketConnectPort;


	/**
	 * Path to an executable that needs to be started before the session.
	 * i.e. stunnel.exe is used for FX Connect
	 */
	private String pathToExecutable;


	/**
	 * File to pass to the executable that may contain some config properties
	 * i.e. stunnel.conf
	 */
	private String executableArgument;


	/**
	 * The default version.
	 * <p>
	 * FIX.4.2,FIX.4.4, etc.
	 */
	private String defaultApplVerId;


	/**
	 * The source system to use for unknown message received by this session.  For example, drop copy messages sent from
	 * Bloomberg session CLIFTONPARA->BLP need to sent to IMS.
	 */
	private FixSourceSystem unknownMessageFixSourceSystem;


	/**
	 * Incrementing sequence counts are stored in memory.
	 * When missing or 1 will save the value to the database after each increment
	 * Can be set to 50 or 100 to rely on memory and only save to the database after x increments since the last save
	 */
	private Integer saveSequenceNumberBatchSize;

	/**
	 * The number of {@link FixMessageEntryType#persistent non-persistent} messages which will be stored in memory for this session. This can be used to retain a greater or lesser
	 * number of non-persistent messages in memory, depending on the needs for any given session.
	 */
	private Integer nonPersistentMessageCacheSize;

	/**
	 * The list of session specific settings.
	 * Examples: AllowUnknownMsgFields, FileLogPath, RejectInvalidMessage, SocketUseSSL, SocketKeyStore
	 */
	private List<FixSessionDefinitionSetting> settingList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return getName() + ": " + getSenderCompId() + "->" + getTargetCompId();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUnknownMessageSystemTag() {
		return (getUnknownMessageFixSourceSystem() != null ? getUnknownMessageFixSourceSystem().getSystemTag() : null);
	}


	public Integer getCoalesceSaveSequenceNumberBatchSize() {
		if (getParent() != null) {
			return ObjectUtils.coalesce(getSaveSequenceNumberBatchSize(), getParent().getSaveSequenceNumberBatchSize());
		}
		return getSaveSequenceNumberBatchSize();
	}


	public Integer getCoalesceNonPersistentMessageCacheSize() {
		if (getParent() != null) {
			return ObjectUtils.coalesce(getNonPersistentMessageCacheSize(), getParent().getNonPersistentMessageCacheSize());
		}
		return getNonPersistentMessageCacheSize();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getFixBeginString() {
		return getBeginString();
	}


	@Override
	public String getFixSenderCompId() {
		return getSenderCompId();
	}


	@Override
	public String getFixSenderSubId() {
		return getSenderSubId();
	}


	@Override
	public String getFixSenderLocId() {
		return getSenderLocId();
	}


	@Override
	public String getFixTargetCompId() {
		return getTargetCompId();
	}


	@Override
	public String getFixTargetSubId() {
		return getTargetSubId();
	}


	@Override
	public String getFixTargetLocId() {
		return getTargetLocId();
	}


	@Override
	public String getFixSessionQualifier() {
		return getSessionQualifier();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemListItem getConnectivityType() {
		return this.connectivityType;
	}


	public void setConnectivityType(SystemListItem connectivityType) {
		this.connectivityType = connectivityType;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public boolean isInformational() {
		return this.informational;
	}


	public void setInformational(boolean informational) {
		this.informational = informational;
	}


	public FixSessionDefinition getParent() {
		return this.parent;
	}


	public void setParent(FixSessionDefinition parent) {
		this.parent = parent;
	}


	public boolean isCreateSession() {
		return this.createSession;
	}


	public void setCreateSession(boolean createSession) {
		this.createSession = createSession;
	}


	public String getServerEnvironment() {
		return this.serverEnvironment;
	}


	public void setServerEnvironment(String serverEnvironment) {
		this.serverEnvironment = serverEnvironment;
	}


	public String getMessageQueue() {
		return this.messageQueue;
	}


	public void setMessageQueue(String messageQueue) {
		this.messageQueue = messageQueue;
	}


	public String getConnectionType() {
		return this.connectionType;
	}


	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}


	public String getTimeZone() {
		return this.timeZone;
	}


	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}


	public Integer getStartWeekday() {
		return this.startWeekday;
	}


	public void setStartWeekday(Integer startWeekday) {
		this.startWeekday = startWeekday;
	}


	public Integer getEndWeekday() {
		return this.endWeekday;
	}


	public void setEndWeekday(Integer endWeekday) {
		this.endWeekday = endWeekday;
	}


	public String getStartTime() {
		return this.startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public String getEndTime() {
		return this.endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public boolean isWeeklySession() {
		return this.weeklySession;
	}


	public void setWeeklySession(boolean weeklySession) {
		this.weeklySession = weeklySession;
	}


	public Integer getHeartbeatInterval() {
		return this.heartbeatInterval;
	}


	public void setHeartbeatInterval(Integer heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}


	public Integer getReconnectInterval() {
		return this.reconnectInterval;
	}


	public void setReconnectInterval(Integer reconnectInterval) {
		this.reconnectInterval = reconnectInterval;
	}


	public String getBeginString() {
		return this.beginString;
	}


	public void setBeginString(String beginString) {
		this.beginString = beginString;
	}


	public String getSenderCompId() {
		return this.senderCompId;
	}


	public void setSenderCompId(String senderCompId) {
		this.senderCompId = senderCompId;
	}


	public String getSenderSubId() {
		return this.senderSubId;
	}


	public void setSenderSubId(String senderSubId) {
		this.senderSubId = senderSubId;
	}


	public String getSenderLocId() {
		return this.senderLocId;
	}


	public void setSenderLocId(String senderLocId) {
		this.senderLocId = senderLocId;
	}


	public String getTargetCompId() {
		return this.targetCompId;
	}


	public void setTargetCompId(String targetCompId) {
		this.targetCompId = targetCompId;
	}


	public String getTargetSubId() {
		return this.targetSubId;
	}


	public void setTargetSubId(String targetSubId) {
		this.targetSubId = targetSubId;
	}


	public String getTargetLocId() {
		return this.targetLocId;
	}


	public void setTargetLocId(String targetLocId) {
		this.targetLocId = targetLocId;
	}


	public String getSessionQualifier() {
		return this.sessionQualifier;
	}


	public void setSessionQualifier(String sessionQualifier) {
		this.sessionQualifier = sessionQualifier;
	}


	public String getSocketConnectHost() {
		return this.socketConnectHost;
	}


	public void setSocketConnectHost(String socketConnectHost) {
		this.socketConnectHost = socketConnectHost;
	}


	public String getSocketConnectPort() {
		return this.socketConnectPort;
	}


	public void setSocketConnectPort(String socketConnectPort) {
		this.socketConnectPort = socketConnectPort;
	}


	public String getPathToExecutable() {
		return this.pathToExecutable;
	}


	public void setPathToExecutable(String pathToExecutable) {
		this.pathToExecutable = pathToExecutable;
	}


	public String getExecutableArgument() {
		return this.executableArgument;
	}


	public void setExecutableArgument(String executableArgument) {
		this.executableArgument = executableArgument;
	}


	public String getDefaultApplVerId() {
		return this.defaultApplVerId;
	}


	public void setDefaultApplVerId(String defaultApplVerId) {
		this.defaultApplVerId = defaultApplVerId;
	}


	public FixSourceSystem getUnknownMessageFixSourceSystem() {
		return this.unknownMessageFixSourceSystem;
	}


	public void setUnknownMessageFixSourceSystem(FixSourceSystem unknownMessageFixSourceSystem) {
		this.unknownMessageFixSourceSystem = unknownMessageFixSourceSystem;
	}


	public Integer getSaveSequenceNumberBatchSize() {
		return this.saveSequenceNumberBatchSize;
	}


	public void setSaveSequenceNumberBatchSize(Integer saveSequenceNumberBatchSize) {
		this.saveSequenceNumberBatchSize = saveSequenceNumberBatchSize;
	}


	public Integer getNonPersistentMessageCacheSize() {
		return this.nonPersistentMessageCacheSize;
	}


	public void setNonPersistentMessageCacheSize(Integer nonPersistentMessageCacheSize) {
		this.nonPersistentMessageCacheSize = nonPersistentMessageCacheSize;
	}


	public List<FixSessionDefinitionSetting> getSettingList() {
		return this.settingList;
	}


	public void setSettingList(List<FixSessionDefinitionSetting> settingList) {
		this.settingList = settingList;
	}


	public String getSessionDictionaryLocation() {
		return this.sessionDictionaryLocation;
	}


	public void setSessionDictionaryLocation(String sessionDictionaryLocation) {
		this.sessionDictionaryLocation = sessionDictionaryLocation;
	}

}
