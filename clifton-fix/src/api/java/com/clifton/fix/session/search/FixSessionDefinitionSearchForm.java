package com.clifton.fix.session.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class FixSessionDefinitionSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "id")
	private Short id;

	@SearchField
	private Boolean disabled;

	@SearchField(searchField = "parent.id")
	private Short parentId;

	// Max Levels Deep is Currently 2
	@SearchField(searchField = "parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String nameExpanded;

	@SearchField(searchField = "connectivityType.id")
	private Integer connectivityTypeId;

	@SearchField(searchField = "saveSequenceNumberBatchSize,parent.saveSequenceNumberBatchSize", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer saveSequenceNumberBatchSize;

	@SearchField(searchField = "nonPersistentMessageCacheSize,parent.nonPersistentMessageCacheSize", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer nonPersistentMessageCacheSize;

	@SearchField
	private Boolean createSession;

	@SearchField
	private String serverEnvironment;

	@SearchField
	private String messageQueue;

	@SearchField(searchField = "unknownMessageFixSourceSystem.id")
	private Short unknownMessageFixSourceSystemId;

	@SearchField
	private String connectionType;

	@SearchField
	private String timeZone;

	@SearchField
	private String startTime;

	@SearchField
	private String endTime;

	@SearchField
	private Integer heartbeatInterval;

	@SearchField
	private Integer reconnectInterval;

	@SearchField
	private String beginString;

	@SearchField
	private String senderCompId;

	@SearchField
	private String senderSubId;

	@SearchField
	private String senderLocId;

	@SearchField
	private String targetCompId;

	@SearchField
	private String targetSubId;

	@SearchField
	private String targetLocId;

	@SearchField
	private String sessionQualifier;

	@SearchField
	private String socketConnectHost;

	@SearchField
	private String socketConnectPort;

	@SearchField
	private String pathToExecutable;

	@SearchField
	private String defaultApplVerId;

	@SearchField
	private String executableArgument;

	@SearchField
	private Boolean informational;


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}


	public String getNameExpanded() {
		return this.nameExpanded;
	}


	public void setNameExpanded(String nameExpanded) {
		this.nameExpanded = nameExpanded;
	}


	public Integer getConnectivityTypeId() {
		return this.connectivityTypeId;
	}


	public void setConnectivityTypeId(Integer connectivityTypeId) {
		this.connectivityTypeId = connectivityTypeId;
	}


	public Integer getSaveSequenceNumberBatchSize() {
		return this.saveSequenceNumberBatchSize;
	}


	public void setSaveSequenceNumberBatchSize(Integer saveSequenceNumberBatchSize) {
		this.saveSequenceNumberBatchSize = saveSequenceNumberBatchSize;
	}


	public Integer getNonPersistentMessageCacheSize() {
		return this.nonPersistentMessageCacheSize;
	}


	public void setNonPersistentMessageCacheSize(Integer nonPersistentMessageCacheSize) {
		this.nonPersistentMessageCacheSize = nonPersistentMessageCacheSize;
	}


	public Boolean getCreateSession() {
		return this.createSession;
	}


	public void setCreateSession(Boolean createSession) {
		this.createSession = createSession;
	}


	public String getServerEnvironment() {
		return this.serverEnvironment;
	}


	public void setServerEnvironment(String serverEnvironment) {
		this.serverEnvironment = serverEnvironment;
	}


	public String getMessageQueue() {
		return this.messageQueue;
	}


	public void setMessageQueue(String messageQueue) {
		this.messageQueue = messageQueue;
	}


	public Short getUnknownMessageFixSourceSystemId() {
		return this.unknownMessageFixSourceSystemId;
	}


	public void setUnknownMessageFixSourceSystemId(Short unknownMessageFixSourceSystemId) {
		this.unknownMessageFixSourceSystemId = unknownMessageFixSourceSystemId;
	}


	public String getConnectionType() {
		return this.connectionType;
	}


	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}


	public String getTimeZone() {
		return this.timeZone;
	}


	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}


	public String getStartTime() {
		return this.startTime;
	}


	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}


	public String getEndTime() {
		return this.endTime;
	}


	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}


	public Integer getHeartbeatInterval() {
		return this.heartbeatInterval;
	}


	public void setHeartbeatInterval(Integer heartbeatInterval) {
		this.heartbeatInterval = heartbeatInterval;
	}


	public Integer getReconnectInterval() {
		return this.reconnectInterval;
	}


	public void setReconnectInterval(Integer reconnectInterval) {
		this.reconnectInterval = reconnectInterval;
	}


	public String getBeginString() {
		return this.beginString;
	}


	public void setBeginString(String beginString) {
		this.beginString = beginString;
	}


	public String getSenderCompId() {
		return this.senderCompId;
	}


	public void setSenderCompId(String senderCompId) {
		this.senderCompId = senderCompId;
	}


	public String getSenderSubId() {
		return this.senderSubId;
	}


	public void setSenderSubId(String senderSubId) {
		this.senderSubId = senderSubId;
	}


	public String getSenderLocId() {
		return this.senderLocId;
	}


	public void setSenderLocId(String senderLocId) {
		this.senderLocId = senderLocId;
	}


	public String getTargetCompId() {
		return this.targetCompId;
	}


	public void setTargetCompId(String targetCompId) {
		this.targetCompId = targetCompId;
	}


	public String getTargetSubId() {
		return this.targetSubId;
	}


	public void setTargetSubId(String targetSubId) {
		this.targetSubId = targetSubId;
	}


	public String getTargetLocId() {
		return this.targetLocId;
	}


	public void setTargetLocId(String targetLocId) {
		this.targetLocId = targetLocId;
	}


	public String getSessionQualifier() {
		return this.sessionQualifier;
	}


	public void setSessionQualifier(String sessionQualifier) {
		this.sessionQualifier = sessionQualifier;
	}


	public String getSocketConnectHost() {
		return this.socketConnectHost;
	}


	public void setSocketConnectHost(String socketConnectHost) {
		this.socketConnectHost = socketConnectHost;
	}


	public String getSocketConnectPort() {
		return this.socketConnectPort;
	}


	public void setSocketConnectPort(String socketConnectPort) {
		this.socketConnectPort = socketConnectPort;
	}


	public String getPathToExecutable() {
		return this.pathToExecutable;
	}


	public void setPathToExecutable(String pathToExecutable) {
		this.pathToExecutable = pathToExecutable;
	}


	public String getDefaultApplVerId() {
		return this.defaultApplVerId;
	}


	public void setDefaultApplVerId(String defaultApplVerId) {
		this.defaultApplVerId = defaultApplVerId;
	}


	public String getExecutableArgument() {
		return this.executableArgument;
	}


	public void setExecutableArgument(String executableArgument) {
		this.executableArgument = executableArgument;
	}


	public Boolean getInformational() {
		return this.informational;
	}


	public void setInformational(Boolean informational) {
		this.informational = informational;
	}
}
