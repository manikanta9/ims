package com.clifton.fix.session.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


public class FixSessionSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "id")
	private Short id;

	@SearchField(searchField = "definition.id")
	private Short definitionId;

	@SearchField(searchField = "definition.name")
	private String definitionName;

	@SearchField
	private Date creationDateAndTime;

	@SearchField
	private Integer incomingSequenceNumber;

	@SearchField
	private Integer outgoingSequenceNumber;

	@SearchField
	private Boolean activeSession;

	@SearchField(searchField = "definition.disabled")
	private Boolean disabled;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Short definitionId) {
		this.definitionId = definitionId;
	}


	public Date getCreationDateAndTime() {
		return this.creationDateAndTime;
	}


	public void setCreationDateAndTime(Date creationDateAndTime) {
		this.creationDateAndTime = creationDateAndTime;
	}


	public Integer getIncomingSequenceNumber() {
		return this.incomingSequenceNumber;
	}


	public void setIncomingSequenceNumber(Integer incomingSequenceNumber) {
		this.incomingSequenceNumber = incomingSequenceNumber;
	}


	public Integer getOutgoingSequenceNumber() {
		return this.outgoingSequenceNumber;
	}


	public void setOutgoingSequenceNumber(Integer outgoingSequenceNumber) {
		this.outgoingSequenceNumber = outgoingSequenceNumber;
	}


	public Boolean getActiveSession() {
		return this.activeSession;
	}


	public void setActiveSession(Boolean activeSession) {
		this.activeSession = activeSession;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}
}
