package com.clifton.fix.session;

import java.util.Map;


/**
 * The {@link FixSessionSettingsConfigurer} type declares methods for configuring FIX session settings during session initialization. Beans implementing this interface should be
 * used to modify session settings as needed.
 * <p>
 * Additional session settings modifications can be implemented at runtime using {@link FixSessionDefinitionSetting} entities.
 *
 * @author MikeH
 */
public interface FixSessionSettingsConfigurer {

	/**
	 * Configures the provided <code>sessionSettings</code>.
	 *
	 * @param sessionDefinition the {@link FixSessionDefinition} describing the session being initialized
	 * @param sessionSettings   the <i>mutable</i> map of session settings
	 */
	public void configure(FixSessionDefinition sessionDefinition, Map<String, String> sessionSettings);
}
