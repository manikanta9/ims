package com.clifton.fix.session;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.fix.session.search.FixSessionDefinitionSearchForm;
import com.clifton.fix.session.search.FixSessionSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>FixSessionService</code> defines methods to retrieve FIX session definitions.
 *
 * @author mwacker
 */
public interface FixSessionService {

	////////////////////////////////////////////////////////////////////////////
	////////              FixSession Business Methods                ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixSession getFixSession(short id);


	/**
	 * Also populates the live sequence numbers
	 */
	public FixSession getFixSessionPopulated(short id);


	public List<FixSession> getFixSessionList(FixSessionSearchForm searchForm, boolean populateLiveSequenceNumbers, boolean populateRunStatus);


	public FixSession getFixSessionCurrentByDefinition(FixSessionDefinition definition);


	public FixSession saveFixSession(FixSession bean);


	@DoNotAddRequestMapping
	public FixSession saveFixSessionInternal(FixSession bean);


	public void deleteFixSession(short id);

	////////////////////////////////////////////////////////////////////////////
	////////        FixSession Sequence Number Business Methods      ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Internal method to set the sequence numbers, but will only save if we cross the batch count or time limit
	 */
	@DoNotAddRequestMapping
	public void storeFixSessionSequenceNumbers(FixSession fixSession, int incomingSequenceNumber, int outgoingSequenceNumber);


	/**
	 * Forces save of the sequence numbers in the database to match the live versions.
	 * Called on application shutdown
	 */
	@SecureMethod(dtoClass = FixSession.class, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void saveFixSessionSequenceNumbers();


	/**
	 * Called just prior to a session restart or after session stops to ensure seq #s are updated
	 */
	@DoNotAddRequestMapping
	public void saveFixSessionSequenceNumbersForSession(short fixSessionId);

	////////////////////////////////////////////////////////////////////////////
	////////          FixSessionDefinition Business Methods          ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinition getFixSessionDefinition(short id);


	public FixSessionDefinition getFixSessionDefinitionByLabel(String labelName);


	public List<FixSessionDefinition> getFixSessionDefinitionList(FixSessionDefinitionSearchForm searchForm);

	public FixSessionDefinition getFixSessionDefinitionByParameters(FixSessionParameters sessionParameters);


	public List<FixSessionDefinition> getFixSessionDefinitionListByTargetCompId(String targetCompId);


	public List<FixSessionDefinition> getFixSessionDefinitionListActiveAndPopulated();


	public FixSessionDefinition saveFixSessionDefinition(FixSessionDefinition bean);


	public void deleteFixSessionDefinition(short id);

	////////////////////////////////////////////////////////////////////////////
	////////           FixSessionSetting Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixSessionDefinitionSetting getFixSessionDefinitionSetting(int id);


	public List<FixSessionDefinitionSetting> getFixSessionDefinitionSettingBySession(short sessionId, boolean includeParent);


	public FixSessionDefinitionSetting saveFixSessionDefinitionSetting(FixSessionDefinitionSetting bean);


	public void deleteFixSessionDefinitionSetting(int id);

	////////////////////////////////////////////////////////////////////////////
	////                         Health Check Methods                      /////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method is used by the periodic health check to allow this application to be probed for a response.
	 * This method will always return true when called.  The failure to receive a response by the caller, would indicate
	 * the service or its connections are down.
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = FixSessionDefinition.class)
	public boolean isFixSessionServiceAlive();
}
