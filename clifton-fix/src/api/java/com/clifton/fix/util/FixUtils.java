package com.clifton.fix.util;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.fix.session.FixSessionDefinition;
import com.clifton.fix.tag.FixTagModifierFilter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ValueRange;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class FixUtils {

	public static final char FIX_FIELD_SEPARATOR = '\u0001';

	public static final String FIX_IDENTIFIER_DATE_FORMAT = "yyyyMMdd";
	public static final String FIX_IDENTIFIER_FULL_DATE_FORMAT = "yyyyMMddHHmmss";
	public static final String FIX_IDENTIFIER_CANCEL_SUFFIX = "CANCEL";
	private static final String[] SUFFIXES_TO_REMOVE = new String[]{"_CANCEL"};
	private static final String[] PREFIXES_TO_REMOVE = new String[]{"O_", "A_"};

	private static final Pattern DATE_TIME_REGEX_PATTERN = Pattern.compile("(([0-9]{4})([0-1][0-9])([0-3][0-9])([0-1][0-9]|[2][0-3])([0-5][0-9])([0-5][0-9]))");

	public static final String FIX_MANAGEMENT_SECURITY_RESOURCE_NAME = "FIX Management";


	/**
	 * Removes any suffixes for the unique id.
	 * <p>
	 * For example O_12_ABCDEFG and O_12_ABCDEFG_CANCEL represent the same FixIdentifier so to look it up we need to remove '_CANCEL'.
	 */
	public static String cleanUniqueId(String uniqueId) {
		uniqueId = cleanDateTimeStamp(uniqueId);
		for (String suffixToRemove : SUFFIXES_TO_REMOVE) {
			if (uniqueId.endsWith(suffixToRemove)) {
				return uniqueId.substring(0, uniqueId.length() - suffixToRemove.length());
			}
		}
		return uniqueId;
	}


	/**
	 * Build a Value Range from the session start and end days of the week. Returns null
	 * if either the starting or ending day of the week is null or the ending day of
	 * the week is before the starting.
	 */
	public static ValueRange getSessionDaysOfWeekRange(FixSessionDefinition definition) {
		AssertUtils.assertNotNull(definition, "Session Definition cannot be null.");

		Integer start = definition.getStartWeekday();
		Integer end = definition.getEndWeekday();
		if (start != null && end != null && start < end) {
			return ValueRange.of(start, end);
		}

		return null;
	}


	/**
	 * Determine if the current time zone adjusted LocalDateTimes are within the active period for a session
	 * based on the session's available days of week and its start and stopping times.
	 */
	public static boolean isSessionActive(FixSessionDefinition definition, LocalDateTime localStartTime, LocalDateTime localEndTime) {
		AssertUtils.assertNotNull(definition, "Session definition cannot be null");

		boolean isActiveDayTime = isActiveSessionTime(definition, localStartTime.toLocalTime(), localEndTime.toLocalTime()) && isActiveSessionDay(definition, localStartTime);

		LocalTime sessionStartTime = parseSessionTime(definition.getStartTime());
		LocalTime sessionEndTime = parseSessionTime(definition.getEndTime());
		ValueRange activeDaysOfWeekRange = getSessionDaysOfWeekRange(definition);
		int currentDayOfWeek = DateUtils.getDayOfWeek(localStartTime);
		boolean isFirstDayActiveTime = (activeDaysOfWeekRange == null || activeDaysOfWeekRange.getMinimum() != currentDayOfWeek || localStartTime.toLocalTime().isAfter(sessionStartTime));
		boolean isLastDayActiveTime = (activeDaysOfWeekRange == null || activeDaysOfWeekRange.getMaximum() != currentDayOfWeek || localEndTime.toLocalTime().isBefore(sessionEndTime));

		// for weekly sessions ignore time ranges except for those on the starting weekday and ending weekday
		if (definition.isWeeklySession()) {
			return isActiveSessionDay(definition, localStartTime) && isLastDayActiveTime && isFirstDayActiveTime;
		}
		return isActiveDayTime && isLastDayActiveTime && isFirstDayActiveTime;
	}


	/**
	 * When sessions are active but within startStopMinutes of start up or shut down time there could be a period where we don't have a heart beat yet.
	 * This check is used by the health check to not generate an error if we are within that period of time
	 * Note: THIS METHOD ALREADY ASSUMES THAT THE SESSION IS SUPPOSED TO BE ACTIVE AND JUST CHECKING IF WITHIN THAT GRACE PERIOD
	 */
	public static boolean isSessionWithinStartUpOrShutDownMinutes(FixSessionDefinition definition, int startStopMinutes, LocalDateTime localStartTime, LocalDateTime localEndTime) {
		LocalTime sessionStartTime = parseSessionTime(definition.getStartTime());
		LocalTime sessionEndTime = parseSessionTime(definition.getEndTime());

		ValueRange activeDaysOfWeekRange = getSessionDaysOfWeekRange(definition);
		int currentDayOfWeek = DateUtils.getDayOfWeek(localStartTime);
		int startStopSeconds = startStopMinutes * 60;

		// If it's a weekly session...
		if (definition.isWeeklySession()) {
			if (activeDaysOfWeekRange != null) {
				// We are on the first day - confirm within start time
				if (activeDaysOfWeekRange.getMinimum() == currentDayOfWeek) {
					Duration duration = Duration.between(sessionStartTime, localStartTime).abs();
					if (duration.getSeconds() <= startStopSeconds) {
						return true;
					}
				}
				// We are on the last day - confirm within end time
				if (activeDaysOfWeekRange.getMaximum() == currentDayOfWeek) {
					Duration duration = Duration.between(sessionEndTime, localEndTime).abs();
					if (duration.getSeconds() <= startStopSeconds) {
						return true;
					}
				}
			}
			return false;
		}

		// Daily session so just check against start and end time
		Duration duration = Duration.between(sessionStartTime, localStartTime).abs();
		if (duration.getSeconds() <= startStopSeconds) {
			return true;
		}
		duration = Duration.between(sessionEndTime, localEndTime).abs();
		if (duration.getSeconds() <= startStopSeconds) {
			return true;
		}
		return false;
	}


	/**
	 * Return whether the day of the week represented by the provide date is within the session's start and
	 * stopping day of week.
	 */
	public static boolean isActiveSessionDay(FixSessionDefinition definition, LocalDateTime startingDateTime) {
		AssertUtils.assertNotNull(definition, "Session definition cannot be null");

		ValueRange activeDaysOfWeekRange = getSessionDaysOfWeekRange(definition);
		int currentDayOfWeek = DateUtils.getDayOfWeek(startingDateTime);
		return (activeDaysOfWeekRange == null || activeDaysOfWeekRange.isValidValue(currentDayOfWeek));
	}


	/**
	 * Check if the session is active, by checking the start and stop times only.
	 * Session shutdowns occur for a few minutes each day, in which case, the start time is after the end time.
	 * For example, the REDI+ sessions have StartTime = 21:05:00 US/Central and EndTime = 20:59:00 US/Central
	 * <p>
	 * Other sessions start in the morning and stop in the evening
	 * For example, the FXConnect session has StartTime = 06:00:00 US/Central and EndTime = 21:00:00 US/Central
	 */
	public static boolean isActiveSessionTime(FixSessionDefinition definition, LocalTime localStartTime, LocalTime localEndTime) {
		AssertUtils.assertNotNull(definition, "Session definition cannot be null");

		LocalTime sessionStartTime = parseSessionTime(definition.getStartTime());
		LocalTime sessionEndTime = parseSessionTime(definition.getEndTime());

		// || localStartTime.isAfter(sessionStartTime.plusMinutes(10))
		return sessionStartTime.isAfter(sessionEndTime) ?
				localEndTime.isBefore(sessionEndTime) || localStartTime.isAfter(sessionStartTime)
				: localEndTime.isBefore(sessionEndTime) && localStartTime.isAfter(sessionStartTime);
	}


	/**
	 * Parse the time taken form the sessions start or stopping time.
	 */
	public static LocalTime parseSessionTime(String sessionTime) {
		String[] timeStringParts = sessionTime.split(" ");
		return LocalTime.parse(timeStringParts[0]);
	}


	/**
	 * Parse the time zone taken form the sessions start or stopping time.
	 */
	public static ZoneId parseTimeZone(String sessionTime) {
		String[] timeStringParts = sessionTime.split(" ");
		ZoneId timeZone = null;
		if (timeStringParts.length > 1) {
			timeZone = ZoneId.of(timeStringParts[1]);
		}
		return timeZone;
	}


	/**
	 * Removes any prefixes from the unique string.
	 * <p>
	 * O_12_ABCDEFG would return 12_ABCDEFG
	 */
	public static String cleanUniqueIdPrefixes(String uniqueId) {
		for (String prefixToRemove : PREFIXES_TO_REMOVE) {
			if (uniqueId.startsWith(prefixToRemove)) {
				return uniqueId.substring(prefixToRemove.length());
			}
		}
		return uniqueId;
	}


	/**
	 * Return the integer id from uniqueId string.
	 * <p>
	 * O_12_ABCDEFG would return 12
	 */
	public static Integer parseUniqueId(String uniqueId) {
		if (!StringUtils.isEmpty(uniqueId)) {
			uniqueId = cleanUniqueIdPrefixes(cleanUniqueId(uniqueId));
			String[] parts = uniqueId.split("_");
			if (parts.length > 0) {
				return Integer.parseInt(parts[0]);
			}
		}
		return null;
	}


	/**
	 * Create a unique string id.
	 * <p>
	 * For inputs 12, "A", "DUDE" the result would be A_DUDE_12_[CURRENT_TIMESTAMP]
	 */
	public static String createUniqueIdFromId(Long id, String prefix, String tag) {
		return createUniqueIdFromId(id, prefix, tag, new Date());
	}


	/**
	 * Create a unique string id.
	 * <p>
	 * For inputs 12, "A", "DUDE", 12/31/2013 12:31:00  the result would be A_DUDE_12_20131231
	 */
	public static String createUniqueIdFromId(Long id, String prefix, String tag, Date date) {
		return (!StringUtils.isEmpty(prefix) ? prefix + "_" : "") + id + "_" + (!StringUtils.isEmpty(tag) ? tag + "_" : "") + DateUtils.fromDate(date, FIX_IDENTIFIER_DATE_FORMAT);
	}


	/**
	 * Create a unique string id for a cancel message.
	 * <p>
	 * For inputs [SOME_ENTITY], "A" the result would be A_[SOME_ENTITY_ID]_[CURRENT_TIMESTAMP_WITH_HHmmss]_CANCEL
	 */
	public static <T extends BaseEntity<?>> String createUniqueIdForCancel(T entity, String prefix) {
		return createUniqueIdForCancel(entity, prefix, new Date());
	}


	/**
	 * Create a unique string id for a cancel message with a specific date.
	 * <p>
	 * For inputs [SOME_ENTITY], "A" the result would be A_[SOME_ENTITY_ID]_[CURRENT_TIMESTAMP_WITH_HHmmss]_CANCEL
	 */
	public static <T extends BaseEntity<?>> String createUniqueIdForCancel(T entity, String prefix, Date date) {
		return createUniqueIdWithFullDateTime(entity, prefix, null, date) + "_" + FIX_IDENTIFIER_CANCEL_SUFFIX;
	}


	/**
	 * Create a unique string id.
	 * <p>
	 * For inputs [SOME_ENTITY], "A" the result would be A_[SOME_ENTITY_ID]_[CURRENT_TIMESTAMP]
	 */
	public static <T extends BaseEntity<?>> String createUniqueId(T entity, String prefix) {
		return doCreateUniqueId(entity, prefix, null, null, false).toString();
	}


	public static <T extends BaseEntity<?>> String createUniqueIdWithTag(T entity, String prefix, String tag) {
		return doCreateUniqueId(entity, prefix, tag, null, false).toString();
	}


	/**
	 * Return a regex that will match a message if all filters are in the message.
	 * <p/>
	 * For example, the below regex will match AllocationInstruction messages with security type CS
	 * (?=.*35=J)(?=.*167=CS)
	 * Use lookahead '(?=' so filter order does not matter.
	 */
	public static String buildTagModifierFilterExpression(List<FixTagModifierFilter> filterList) {
		String regex = "";
		if (!CollectionUtils.isEmpty(filterList)) {
			regex = filterList.stream()
					.map(filter -> "(?=" + ".*" + FixUtils.FIX_FIELD_SEPARATOR + filter.toString() + FixUtils.FIX_FIELD_SEPARATOR + ")")
					.collect(Collectors.joining());
		}
		return regex;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static String cleanDateTimeStamp(String uniqueId) {
		String dateString = getDateString(uniqueId);
		if (!StringUtils.isEmpty(dateString)) {
			Date date = DateUtils.toDate(dateString, FIX_IDENTIFIER_FULL_DATE_FORMAT);
			return StringUtils.replace(uniqueId, dateString, DateUtils.fromDate(date, FIX_IDENTIFIER_DATE_FORMAT));
		}
		return uniqueId;
	}


	private static String getDateString(String uniqueId) {
		Matcher m = DATE_TIME_REGEX_PATTERN.matcher(uniqueId);
		if (m.find()) {
			return m.group();
		}
		return null;
	}


	private static <T extends BaseEntity<?>> CharSequence createUniqueIdWithFullDateTime(T entity, String prefix, String tag, Date date) {
		return doCreateUniqueId(entity, prefix, tag, date, true);
	}


	/**
	 * Create a unique string id.
	 * <p>
	 * For inputs [SOME_ENTITY], "A", "DUDE" the result would be A_DUDE_[SOME_ENTITY_ID]_[CURRENT_TIMESTAMP]
	 */
	private static <T extends BaseEntity<?>> CharSequence doCreateUniqueId(T entity, String prefix, String tag, Date date, boolean useFullDateFormat) {
		if (date == null) {
			date = entity.getCreateDate();
		}
		return (!StringUtils.isEmpty(prefix) ? prefix + "_" : "") + entity.getId() + "_" + (!StringUtils.isEmpty(tag) ? tag + "_" : "")
				+ DateUtils.fromDate(date, useFullDateFormat ? FIX_IDENTIFIER_FULL_DATE_FORMAT : FIX_IDENTIFIER_DATE_FORMAT);
	}
}
