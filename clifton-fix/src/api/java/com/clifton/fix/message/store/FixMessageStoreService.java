package com.clifton.fix.message.store;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.FixMessageField;
import com.clifton.fix.message.store.search.FixMessageStoreSearchForm;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.util.FixUtils;

import java.util.List;


public interface FixMessageStoreService {

	////////////////////////////////////////////////////////////////////////////
	////////           FixMessageStore Business Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageStore getFixMessageStore(int id);


	public List<FixMessageStore> getFixMessageStoreList(FixMessageStoreSearchForm searchForm);


	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	public boolean saveFixMessageStore(FixMessageStore bean);


	@SecureMethod(securityResource = FixUtils.FIX_MANAGEMENT_SECURITY_RESOURCE_NAME)
	public void deleteFixMessageStoreForSession(FixSession session);


	@SecureMethod(dtoClass = FixMessageStore.class)
	public List<FixMessageField> getFixMessageStoreFieldList(Integer messageId);
}
