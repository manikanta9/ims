package com.clifton.fix.message.validation;

/**
 * The ordinal of these determines severity.
 * When accumulating results from validators, the cumulative status for SUCCESS and FAILURE would be FAILURE since it has a higher ordinal value.
 * This is a very trivial case, however, this can become more relevant as we add more statuses.
 */
public enum FixValidationStatuses {

	SUCCESS,
	FAILURE
}
