package com.clifton.fix.message.store.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class FixMessageStoreSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "session.id")
	private Short sessionId;

	@SearchField
	private Integer sequenceNumber;

	@SearchField(searchField = "sequenceNumber", comparisonConditions = {ComparisonConditions.GREATER_THAN_OR_EQUALS})
	private Integer startSequenceNumber;

	@SearchField(searchField = "sequenceNumber", comparisonConditions = {ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Integer endSequenceNumber;

	@SearchField
	private String text;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(Short sessionId) {
		this.sessionId = sessionId;
	}


	public Integer getSequenceNumber() {
		return this.sequenceNumber;
	}


	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Integer getStartSequenceNumber() {
		return this.startSequenceNumber;
	}


	public void setStartSequenceNumber(Integer startSequenceNumber) {
		this.startSequenceNumber = startSequenceNumber;
	}


	public Integer getEndSequenceNumber() {
		return this.endSequenceNumber;
	}


	public void setEndSequenceNumber(Integer endSequenceNumber) {
		this.endSequenceNumber = endSequenceNumber;
	}
}
