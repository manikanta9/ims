package com.clifton.fix.message.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;


/**
 * @author manderson
 */
public class FixMessageEntryTypeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "id")
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField
	private Boolean defaultType;

	@SearchField
	private String messageTypeTagValue;

	@SearchField
	private Short idFieldTag;

	@SearchField
	private Short referencedIdFieldTag;

	@SearchField
	private Short dropCopyIdFieldTag;

	@SearchField
	private Boolean message;

	@SearchField
	private Boolean administrative;

	@SearchField
	private Boolean persistent;

	@SearchField
	private Boolean cacheNonPersistentMessage;

	// Custom Search Filter - Convert to Short[] for ids filter
	private FixMessageEntryTypeIds fixMessageEntryTypeIdsName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Boolean getDefaultType() {
		return this.defaultType;
	}


	public void setDefaultType(Boolean defaultType) {
		this.defaultType = defaultType;
	}


	public Boolean getMessage() {
		return this.message;
	}


	public void setMessage(Boolean message) {
		this.message = message;
	}


	public Boolean getAdministrative() {
		return this.administrative;
	}


	public void setAdministrative(Boolean administrative) {
		this.administrative = administrative;
	}


	public String getMessageTypeTagValue() {
		return this.messageTypeTagValue;
	}


	public void setMessageTypeTagValue(String messageTypeTagValue) {
		this.messageTypeTagValue = messageTypeTagValue;
	}


	public Short getIdFieldTag() {
		return this.idFieldTag;
	}


	public void setIdFieldTag(Short idFieldTag) {
		this.idFieldTag = idFieldTag;
	}


	public Short getReferencedIdFieldTag() {
		return this.referencedIdFieldTag;
	}


	public void setReferencedIdFieldTag(Short referencedIdFieldTag) {
		this.referencedIdFieldTag = referencedIdFieldTag;
	}


	public Short getDropCopyIdFieldTag() {
		return this.dropCopyIdFieldTag;
	}


	public void setDropCopyIdFieldTag(Short dropCopyIdFieldTag) {
		this.dropCopyIdFieldTag = dropCopyIdFieldTag;
	}


	public Boolean getPersistent() {
		return this.persistent;
	}


	public void setPersistent(Boolean persistent) {
		this.persistent = persistent;
	}


	public Boolean getCacheNonPersistentMessage() {
		return this.cacheNonPersistentMessage;
	}


	public void setCacheNonPersistentMessage(Boolean cacheNonPersistentMessage) {
		this.cacheNonPersistentMessage = cacheNonPersistentMessage;
	}


	public FixMessageEntryTypeIds getFixMessageEntryTypeIdsName() {
		return this.fixMessageEntryTypeIdsName;
	}


	public void setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds fixMessageEntryTypeIdsName) {
		this.fixMessageEntryTypeIdsName = fixMessageEntryTypeIdsName;
	}
}
