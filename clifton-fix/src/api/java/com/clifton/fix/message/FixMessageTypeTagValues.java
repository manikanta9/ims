package com.clifton.fix.message;

import com.clifton.core.util.AssertUtils;


/**
 * <tagValue>FixMessageTypeTagValues</tagValue> enumeration of fix messages types and their corresponding tagValue string.
 */
public enum FixMessageTypeTagValues {

	NEW_ORDER_SINGLE_TYPE_TAG("D", false),
	EXECUTION_REPORT_TYPE_TAG("8", false),
	IOI_TYPE_TAG("6", false),
	MARKET_DATA_REFRESH("X", false),

	ALLOCATION_REPORT_TYPE_TAG("AS", true),
	ALLOCATION_INSTRUCTION_TYPE_TAG("J", true),
	ALLOCATION_INSTRUCTION_ACK_TYPE_TAG("P", true),

	ORDER_CANCEL_REQUEST_TYPE_TAG("F", false),
	ORDER_CANCEL_REPLACE_REQUEST_TYPE_TAG("G", false),
	ORDER_CANCEL_REJECT_TYPE_TAG("9", false),
	TRADE_CAPTURE("AE", false);

	private String tagValue;
	private boolean allocationType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	FixMessageTypeTagValues(String tagValue, boolean allocationType) {
		this.tagValue = tagValue;
		this.allocationType = allocationType;
	}


	@Override
	public String toString() {
		return getTagValue();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FixMessageTypeTagValues findMessageTypeTagValue(String tagValue) {
		AssertUtils.assertNotEmpty(tagValue, "Tag value cannot be empty");
		for (FixMessageTypeTagValues tagValueEnum : FixMessageTypeTagValues.values()) {
			if (tagValueEnum.getTagValue().equals(tagValue)) {
				return tagValueEnum;
			}
		}
		throw new RuntimeException("Tag value enumeration not found for " + tagValue);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTagValue() {
		return this.tagValue;
	}


	public boolean isAllocationType() {
		return this.allocationType;
	}
}
