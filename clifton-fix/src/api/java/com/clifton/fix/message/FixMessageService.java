package com.clifton.fix.message;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.fix.message.search.FixMessageEntrySearchForm;
import com.clifton.fix.session.FixSession;

import java.util.List;


/**
 * A front-end service for to query FixMessage entities.  This service uses the FixMessageEntry service to perform
 * the actual queries and converts the results to FixMessage entities.
 *
 * @author davidi
 */
public interface FixMessageService {

	////////////////////////////////////////////////////////////////////////////
	////////               FixMessage Business Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(table = "FixMessageEntry")
	public FixMessage getFixMessage(long id);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixMessageField> getFixMessageFieldList(long messageId);


	/**
	 * A special method to get the field list from a non-persistent FixMessage from in-memory storage.  The
	 * searchForm should set the entryId of the entity as well as heartbeatMessages=true or nonPersistentMessages=true
	 */
	@SecureMethod(table = "FixMessageEntry")
	public List<FixMessageField> getFixMessageFieldListNonPersistent(FixMessageEntrySearchForm searchForm);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixMessage> getFixMessageList(FixMessageEntrySearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////          Non-Persistent FixMessage Business Methods        ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A special method to get a non-persistent FixMessage from in-memory storage.  The
	 * searchForm should set the entryId of the entity as well as heartbeatMessages=true or nonPersistentMessages=true
	 */
	@SecureMethod(table = "FixMessageEntry")
	public FixMessage getFixMessageNonPersistent(FixMessageEntrySearchForm searchForm);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixMessage> getFixMessageFailedMessageList();


	@SecureMethod(table = "FixMessageEntry")
	public FixMessage getFixMessageFailedMessage(Long id);


	@SecureMethod(table = "FixMessageEntry")
	public List<FixMessageField> getFixMessageFailedFieldList(Long id);


	@DoNotAddRequestMapping
	public void updateFixNonPersistentMessageCapacityForSession(FixSession fixSession, int capacity);
}
