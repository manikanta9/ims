package com.clifton.fix.message.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.fix.message.cache.FixMessageEntryTypeIds;
import com.clifton.fix.session.FixSessionParameters;

import java.util.Date;


public class FixMessageEntrySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "session.definition.name,type.name,text")
	private String searchPattern;

	@SearchField
	private Long id;

	@SearchField(searchField = "type.id")
	private Short typeId;

	@SearchField(searchField = "type.id")
	private Short[] typeIds;

	// NOTE: AVOID WHERE POSSIBLE USING MESSAGE AND ADMINISTRATIVE FILTERS AND TRY USING THE FixMessageEntryTypeIds filter instead
	// This will improve performance by excluding the join onto the type table.

	// Custom Search Filter - Convert to Short[] for typeIds filter
	private FixMessageEntryTypeIds fixMessageEntryTypeIdsName;

	@SearchField(searchField = "message", searchFieldPath = "type")
	private Boolean message;

	@SearchField(searchField = "administrative", searchFieldPath = "type")
	private Boolean administrative;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String typeName;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EQUALS)
	private String typeNameEquals;

	/**
	 * Filters the log entries on entries with no session or not processed.
	 */
	// Custom Search Filter
	private Boolean sessionIsNullOrNotProcessed;

	@SearchField(comparisonConditions = ComparisonConditions.LIKE)
	private String text;

	@SearchField
	private Date messageEntryDateAndTime;

	// Custom Search Filter
	private Date heartbeatSinceDateAndTime;

	@SearchField(searchField = "name", searchFieldPath = "session.definition")
	private String sessionDefinitionName;

	@SearchField(searchField = "session.id")
	private Short sessionId;

	@SearchField(searchField = "definition.id", searchFieldPath = "session")
	private Short sessionDefinitionId;

	// custom configured search field: searchField = "messageTypeTagValue", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EQUALS)
	private String messageTypeTagValue;

	// custom configured search field: searchField = "messageTypeTagValue", searchFieldPath = "type", comparisonConditions = ComparisonConditions.IN
	private String[] messageTypeTagValueList;

	@SearchField(searchField = "idFieldTag", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EQUALS)
	private Short messageTypeIdFieldTag;

	/**
	 * Used to look up the FixSessionDefinition before executing the query.
	 */
	// Custom Search Filter
	private FixSessionParameters sessionParameters;

	@SearchField(searchField = "identifier.id")
	private Integer identifierId;

	@SearchField(searchField = "dropCopyIdentifier", searchFieldPath = "identifier")
	private Boolean dropCopyIdentifier;

	@SearchField(searchField = "identifier.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] identifierIdList;

	@SearchField(searchField = "uniqueStringIdentifier", searchFieldPath = "identifier")
	private String identifier;

	@SearchField(searchField = "uniqueStringIdentifier", searchFieldPath = "identifier", comparisonConditions = ComparisonConditions.IN)
	private String[] identifierList;

	@SearchField(searchField = "uniqueStringIdentifier", searchFieldPath = "identifier", comparisonConditions = ComparisonConditions.LIKE)
	private String uniqueStringIdentifier;

	@SearchField
	private Boolean incoming;

	@SearchField
	private Boolean processed;

	@SearchField(searchField = "identifier.sourceSystem.id")
	private Short sourceSystemId;

	@SearchField(searchField = "identifier.sourceSystem.id,session.definition.unknownMessageFixSourceSystem.id", leftJoin = true)
	private Short sourceSystemIncludingUnknownId;

	@SearchField(searchFieldPath = "identifier.sourceSystem", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String sourceSystemName;

	@SearchField(searchFieldPath = "identifier", searchField = "sourceSystemFkFieldId", comparisonConditions = ComparisonConditions.EQUALS)
	private Long sourceSystemFkFieldId;


	// Custom Search Filter
	private Boolean heartbeatMessages;
	private Boolean latestHeartbeatsOnly;
	private Boolean nonPersistentMessages;
	private Boolean latestNonPersistentMessagesOnly;


	public FixMessageEntrySearchForm() {
		setOrderBy("id:desc#messageEntryDateAndTime:desc");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Short[] getTypeIds() {
		return this.typeIds;
	}


	public void setTypeIds(Short[] typeIds) {
		this.typeIds = typeIds;
	}


	public FixMessageEntryTypeIds getFixMessageEntryTypeIdsName() {
		return this.fixMessageEntryTypeIdsName;
	}


	public void setFixMessageEntryTypeIdsName(FixMessageEntryTypeIds fixMessageEntryTypeIdsName) {
		this.fixMessageEntryTypeIdsName = fixMessageEntryTypeIdsName;
	}


	public Boolean getMessage() {
		return this.message;
	}


	public void setMessage(Boolean message) {
		this.message = message;
	}


	public Boolean getAdministrative() {
		return this.administrative;
	}


	public void setAdministrative(Boolean administrative) {
		this.administrative = administrative;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public String getTypeNameEquals() {
		return this.typeNameEquals;
	}


	public void setTypeNameEquals(String typeNameEquals) {
		this.typeNameEquals = typeNameEquals;
	}


	public Boolean getSessionIsNullOrNotProcessed() {
		return this.sessionIsNullOrNotProcessed;
	}


	public void setSessionIsNullOrNotProcessed(Boolean sessionIsNullOrNotProcessed) {
		this.sessionIsNullOrNotProcessed = sessionIsNullOrNotProcessed;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public Date getMessageEntryDateAndTime() {
		return this.messageEntryDateAndTime;
	}


	public void setMessageEntryDateAndTime(Date messageEntryDateAndTime) {
		this.messageEntryDateAndTime = messageEntryDateAndTime;
	}


	public Date getHeartbeatSinceDateAndTime() {
		return this.heartbeatSinceDateAndTime;
	}


	public void setHeartbeatSinceDateAndTime(Date heartbeatSinceDateAndTime) {
		this.heartbeatSinceDateAndTime = heartbeatSinceDateAndTime;
	}


	public String getSessionDefinitionName() {
		return this.sessionDefinitionName;
	}


	public void setSessionDefinitionName(String sessionDefinitionName) {
		this.sessionDefinitionName = sessionDefinitionName;
	}


	public Short getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(Short sessionId) {
		this.sessionId = sessionId;
	}


	public Short getSessionDefinitionId() {
		return this.sessionDefinitionId;
	}


	public void setSessionDefinitionId(Short sessionDefinitionId) {
		this.sessionDefinitionId = sessionDefinitionId;
	}


	public String getMessageTypeTagValue() {
		return this.messageTypeTagValue;
	}


	public void setMessageTypeTagValue(String messageTypeTagValue) {
		this.messageTypeTagValue = messageTypeTagValue;
	}


	public String[] getMessageTypeTagValueList() {
		return this.messageTypeTagValueList;
	}


	public void setMessageTypeTagValueList(String[] messageTypeTagValueList) {
		this.messageTypeTagValueList = messageTypeTagValueList;
	}


	public Short getMessageTypeIdFieldTag() {
		return this.messageTypeIdFieldTag;
	}


	public void setMessageTypeIdFieldTag(Short messageTypeIdFieldTag) {
		this.messageTypeIdFieldTag = messageTypeIdFieldTag;
	}


	public FixSessionParameters getSessionParameters() {
		return this.sessionParameters;
	}


	public void setSessionParameters(FixSessionParameters sessionParameters) {
		this.sessionParameters = sessionParameters;
	}


	public Integer getIdentifierId() {
		return this.identifierId;
	}


	public void setIdentifierId(Integer identifierId) {
		this.identifierId = identifierId;
	}


	public Boolean getDropCopyIdentifier() {
		return this.dropCopyIdentifier;
	}


	public void setDropCopyIdentifier(Boolean dropCopyIdentifier) {
		this.dropCopyIdentifier = dropCopyIdentifier;
	}


	public Integer[] getIdentifierIdList() {
		return this.identifierIdList;
	}


	public void setIdentifierIdList(Integer[] identifierIdList) {
		this.identifierIdList = identifierIdList;
	}


	public String getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	public String[] getIdentifierList() {
		return this.identifierList;
	}


	public void setIdentifierList(String[] identifierList) {
		this.identifierList = identifierList;
	}


	public String getUniqueStringIdentifier() {
		return this.uniqueStringIdentifier;
	}


	public void setUniqueStringIdentifier(String uniqueStringIdentifier) {
		this.uniqueStringIdentifier = uniqueStringIdentifier;
	}


	public Boolean getIncoming() {
		return this.incoming;
	}


	public void setIncoming(Boolean incoming) {
		this.incoming = incoming;
	}


	public Boolean getProcessed() {
		return this.processed;
	}


	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}


	public Boolean getHeartbeatMessages() {
		return this.heartbeatMessages;
	}


	public void setHeartbeatMessages(Boolean heartbeatMessages) {
		this.heartbeatMessages = heartbeatMessages;
	}


	public Boolean getLatestHeartbeatsOnly() {
		return this.latestHeartbeatsOnly;
	}


	public void setLatestHeartbeatsOnly(Boolean latestHeartbeatsOnly) {
		this.latestHeartbeatsOnly = latestHeartbeatsOnly;
	}


	public Boolean getNonPersistentMessages() {
		return this.nonPersistentMessages;
	}


	public void setNonPersistentMessages(Boolean nonPersistentMessages) {
		this.nonPersistentMessages = nonPersistentMessages;
	}


	public Boolean getLatestNonPersistentMessagesOnly() {
		return this.latestNonPersistentMessagesOnly;
	}


	public void setLatestNonPersistentMessagesOnly(Boolean latestNonPersistentMessagesOnly) {
		this.latestNonPersistentMessagesOnly = latestNonPersistentMessagesOnly;
	}


	public Short getSourceSystemId() {
		return this.sourceSystemId;
	}


	public void setSourceSystemId(Short sourceSystemId) {
		this.sourceSystemId = sourceSystemId;
	}


	public Short getSourceSystemIncludingUnknownId() {
		return this.sourceSystemIncludingUnknownId;
	}


	public void setSourceSystemIncludingUnknownId(Short sourceSystemIncludingUnknownId) {
		this.sourceSystemIncludingUnknownId = sourceSystemIncludingUnknownId;
	}


	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public Long getSourceSystemFkFieldId() {
		return this.sourceSystemFkFieldId;
	}


	public void setSourceSystemFkFieldId(Long sourceSystemFkFieldId) {
		this.sourceSystemFkFieldId = sourceSystemFkFieldId;
	}
}
