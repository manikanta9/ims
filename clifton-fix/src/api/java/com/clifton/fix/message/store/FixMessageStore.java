package com.clifton.fix.message.store;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.fix.session.FixSession;


/**
 * The <code>FixMessageStore</code> is used to persist outgoing messages for a session.  When a session is reset these
 * messages will be cleared.
 *
 * @author mwacker
 */
public class FixMessageStore extends BaseSimpleEntity<Integer> {

	private FixSession session;
	private int sequenceNumber;
	private String text;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSession getSession() {
		return this.session;
	}


	public void setSession(FixSession session) {
		this.session = session;
	}


	public int getSequenceNumber() {
		return this.sequenceNumber;
	}


	public void setSequenceNumber(int sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}
}
