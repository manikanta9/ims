package com.clifton.fix.message.validation;

import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * <code>FixValidationService</code> allows a fix client to perform validations
 * on a message with the provided trace order ID.
 */
public interface FixMessageValidationService {

	// PUT THIS BACK IN AFTER CLIENT SUBPROJECT IS GONE AND WE CALL THIS INTERNALLY @DoNotAddRequestMapping
	@SecureMethod(table = "FixMessageEntry")
	public List<FixValidationResult> validateFixMessageForOrderList(List<Integer> externalOrderIdentifierList);
}
