package com.clifton.fix.message.validation;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessageTypeTagValues;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>FixValidationResult</code> is an value holder for the cumulative results of all validations
 * performed on a single message.
 */
public class FixValidationResult implements Serializable {

	private final Long fixMessageId;
	private final FixMessageTypeTagValues messageType;
	private final Integer fixIdentifierId;
	private final FixValidationStatuses validationStatus;
	private final String message;

	private String messageTypeLabel;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixValidationResult(Long fixMessageId, FixMessageTypeTagValues messageType, Integer fixIdentifierId, FixValidationStatuses validationStatus, String message) {
		ValidationUtils.assertNotNull(messageType, "Fix message type is required.");
		ValidationUtils.assertNotNull(fixIdentifierId, "Fix Identifier cannot be null");

		this.fixMessageId = fixMessageId;
		this.messageType = messageType;
		this.fixIdentifierId = fixIdentifierId;
		this.validationStatus = validationStatus;
		this.message = message;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAllocationValidationError() {
		return this.getValidationStatus() == FixValidationStatuses.FAILURE && getMessageType().isAllocationType();
	}


	public boolean isValidationError() {
		return this.getValidationStatus() == FixValidationStatuses.FAILURE && !getMessageType().isAllocationType();
	}


	public String getGroupingLabel() {
		return getMessageTypeLabel() + ' ' + getFixIdentifierId();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static boolean isAllocationValidationError(List<FixValidationResult> fixValidationResults) {
		List<FixValidationResult> results = fixValidationResults
				.stream()
				.filter(FixValidationResult::isAllocationValidationError)
				.collect(Collectors.toList());
		return results.isEmpty();
	}


	public static boolean isValidationError(List<FixValidationResult> fixValidationResults) {
		List<FixValidationResult> results = fixValidationResults
				.stream()
				.filter(r -> !r.isAllocationValidationError())
				.collect(Collectors.toList());
		return results.isEmpty();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FixValidationResult that = (FixValidationResult) o;
		return Objects.equals(this.fixMessageId, that.fixMessageId) &&
				this.messageType == that.messageType &&
				Objects.equals(this.fixIdentifierId, that.fixIdentifierId) &&
				this.validationStatus == that.validationStatus &&
				Objects.equals(this.message, that.message) &&
				Objects.equals(this.messageTypeLabel, that.messageTypeLabel);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.fixMessageId, this.messageType, this.fixIdentifierId, this.validationStatus, this.message, this.messageTypeLabel);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageTypeTagValues getMessageType() {
		return this.messageType;
	}


	public String getMessageTypeLabel() {
		return this.messageTypeLabel;
	}


	public void setMessageTypeLabel(String messageTypeLabel) {
		this.messageTypeLabel = messageTypeLabel;
	}


	public Integer getFixIdentifierId() {
		return this.fixIdentifierId;
	}


	public FixValidationStatuses getValidationStatus() {
		return this.validationStatus;
	}


	public String getMessage() {
		return this.message;
	}


	public Long getFixMessageId() {
		return this.fixMessageId;
	}
}
