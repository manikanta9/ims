package com.clifton.fix.message.cache;

/**
 * The <code>FixLogEntryTypeIds</code> enum defines various options for FixMessageEntryType searches.
 * Null can be used for No filtering
 *
 * @author manderson
 */
public enum FixMessageEntryTypeIds {

	ADMINISTRATIVE(null, true),
	MESSAGE_AND_ADMINISTRATIVE(true, true),
	MESSAGE_AND_NOT_ADMINISTRATIVE(true, false),
	MESSAGE(true, null),
	NOT_MESSAGE(false, null),
	NOT_MESSAGE_AND_ADMINISTRATIVE(false, true),
	NOT_MESSAGE_AND_NOT_ADMINISTRATIVE(false, false),
	NOT_ADMINISTRATIVE(null, false);



	private final Boolean message;
	private final Boolean administrative;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	FixMessageEntryTypeIds(Boolean message, Boolean administrative) {
		this.message = message;
		this.administrative = administrative;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getMessage() {
		return this.message;
	}


	public Boolean getAdministrative() {
		return this.administrative;
	}
}
