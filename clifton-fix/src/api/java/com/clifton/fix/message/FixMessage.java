package com.clifton.fix.message;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.fix.log.FixLogEntry;


/**
 * The <code>FixMessage</code> is a class that represents a Fix Message.  This class is used with the FixMessageService to
 * avoid directly calling methods in the FixMessageEntry service.  This class is non-persistent.
 *
 * @author davidi
 */
@NonPersistentObject
public class FixMessage extends FixLogEntry {

	/**
	 * Indicates the message entry has been processed.
	 */
	private Boolean processed;

	private FixIdentifier identifier;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getProcessed() {
		return this.processed;
	}


	public void setProcessed(Boolean processed) {
		this.processed = processed;
	}


	public FixIdentifier getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(FixIdentifier identifier) {
		this.identifier = identifier;
	}
}
