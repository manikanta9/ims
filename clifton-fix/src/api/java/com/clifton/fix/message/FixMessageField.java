package com.clifton.fix.message;


import java.util.List;


/**
 * The <code>FixMessageField</code> defines a FixMessage field used to display a fix message is a readable fashion on the UI.
 *
 * @author mwacker
 */
public class FixMessageField {

	private int tag;
	private String fieldName;
	private String fieldValue;
	private String fieldValueName;

	private List<FixMessageField> children;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLeaf() {
		return getChildren() == null || getChildren().isEmpty();
	}


	@Override
	public String toString() {
		return this.tag + "\t" + this.fieldName + "\t" + this.fieldValue + "\t" + this.fieldValueName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getTag() {
		return this.tag;
	}


	public void setTag(int tag) {
		this.tag = tag;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public String getFieldValueName() {
		return this.fieldValueName;
	}


	public void setFieldValueName(String fieldEnumValue) {
		this.fieldValueName = fieldEnumValue;
	}


	public List<FixMessageField> getChildren() {
		return this.children;
	}


	public void setChildren(List<FixMessageField> childFieldList) {
		this.children = childFieldList;
	}
}
