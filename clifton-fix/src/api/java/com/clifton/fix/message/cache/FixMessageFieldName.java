package com.clifton.fix.message.cache;

import java.util.Objects;


/**
 * <code>FixMessageFieldName</code> represents the mapping between a fix message tag number and its label.
 * The set of <code>FixMessageFieldName</code> objects are distinct for each fix version.  Example field values:
 * {tag = 35, name = MsgType}.  This is used by IMS side, is built from the fix dictionary and is cached.
 */
public class FixMessageFieldName {

	private String tag;
	private String name;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixMessageFieldName() {
		// empty
	}


	public FixMessageFieldName(String tag, String name) {
		this.tag = tag;
		this.name = name;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		FixMessageFieldName that = (FixMessageFieldName) o;
		return Objects.equals(this.tag, that.tag) &&
				Objects.equals(this.name, that.name);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.tag, this.name);
	}


	public String getTag() {
		return this.tag;
	}


	public void setTag(String tag) {
		this.tag = tag;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
