package com.clifton.business;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;


/**
 * The <code>BusinessProjectBasicTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "business";
	}


	/**
	 * Can be overridden to skip automated testing for certain methods.
	 */
	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("saveBusinessService");
		return skipMethods.contains(method.getName());
	}
}
