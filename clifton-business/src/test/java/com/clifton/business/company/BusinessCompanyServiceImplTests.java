package com.clifton.business.company;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.hierarchy.ParentIsChildException;
import com.clifton.core.validation.hierarchy.SameHierarchyParentException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>BusinessCompanyServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BusinessCompanyServiceImplTests {

	private static final short TYPE_CLIENT = 1;
	private static final short TYPE_TRADING = 2;
	private static final short TYPE_BROKER = 3;

	@Resource
	private BusinessCompanyService businessCompanyService;
	@Resource
	private SecurityAuthorizationService securityAuthorizationService;
	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	@Resource
	private SystemConditionService systemConditionService;


	@Test
	public void testInsertSystemDefinedBusinessCompanyType() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompanyType type = new BusinessCompanyType();
			type.setSystemDefined(true);
			type.setName("Test");
			type.setDescription("Test");
			this.businessCompanyService.saveBusinessCompanyType(type);
		}, "Cannot create a system defined company type.");
	}


	@Test
	public void testUpdateSystemDefinedBusinessCompanyType_NotAdmin() {
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);

			BusinessCompanyType type = this.businessCompanyService.getBusinessCompanyType(TYPE_CLIENT);
			type.setSystemDefined(false);
			this.businessCompanyService.saveBusinessCompanyType(type);
		}, "Administrators only can update a system defined company type.");
	}


	@Test
	public void testUpdateSystemDefinedBusinessCompanyType_Admin() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		BusinessCompanyType type = this.businessCompanyService.getBusinessCompanyType(TYPE_CLIENT);
		type.setDescription("Test Updating description!");
		this.businessCompanyService.saveBusinessCompanyType(type);
	}


	@Test
	public void testUpdateSystemDefinedBusinessCompanyType_Admin_ChangeName() {
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

			BusinessCompanyType type = this.businessCompanyService.getBusinessCompanyType(TYPE_CLIENT);
			type.setName(type.getName() + " - Update");
			this.businessCompanyService.saveBusinessCompanyType(type);
		}, "Cannot change the name of a system defined company type.");
	}


	@Test
	public void testUpdateSystemDefinedBusinessCompanyType_Admin_ChangeSystemDefined() {
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

			BusinessCompanyType type = this.businessCompanyService.getBusinessCompanyType(TYPE_CLIENT);
			type.setSystemDefined(false);
			this.businessCompanyService.saveBusinessCompanyType(type);
		}, "Cannot change a system defined company type to not system defined.");
	}


	@Test
	public void testDeleteSystemDefinedBusinessCompanyType() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.businessCompanyService.deleteBusinessCompanyType(TYPE_CLIENT);
		}, "Cannot delete a system defined company type.");
	}


	@Test
	public void testUpdateNonSystemDefinedBusinessCompanyType() {
		BusinessCompanyType type = new BusinessCompanyType();
		type.setName("Test");
		this.businessCompanyService.saveBusinessCompanyType(type);

		type.setName("Test Updated");
		type.setDescription("Test");
		this.businessCompanyService.saveBusinessCompanyType(type);

		this.businessCompanyService.deleteBusinessCompanyType(type.getId());
	}


	@Test
	public void testAbbreviationRequiredBusinessCompanyType() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompany c1 = new BusinessCompany();
			c1.setName("Test Company 1");
			c1.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER));
			this.businessCompanyService.saveBusinessCompany(c1);
		}, "Abbreviations are required for company type [Broker].  Please enter a unique abbreviation for this company.");
	}


	@Test
	public void testAbbreviationMaxLengthBusinessCompanyType() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompany c1 = new BusinessCompany();
			c1.setName("Test Company 1");
			c1.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER));
			c1.setAbbreviation("TEST_CO_1");
			this.businessCompanyService.saveBusinessCompany(c1);
		}, "Abbreviations cannot be more than 4 characters.");
	}


	@Test
	public void testBusinessCompanyAbbreviations() {
		BusinessCompany c1 = new BusinessCompany();
		c1.setAbbreviation("T1");
		c1.setName("Test Company 1");
		c1.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING));
		this.businessCompanyService.saveBusinessCompany(c1);

		BusinessCompany c2 = new BusinessCompany();
		c2.setAbbreviation("T1");
		c2.setName("Test Company 2");
		c2.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING));
		try {
			this.businessCompanyService.saveBusinessCompany(c2);
		}
		catch (Exception e) {
			Assertions.assertEquals("Abbreviation [T1] is already used for company [Test Company 1].  Company abbreviations must be unique.", ExceptionUtils.getOriginalMessage(e));

			c2.setAbbreviation("T2");
			this.businessCompanyService.saveBusinessCompany(c2);
			return;
		}
		Assertions.fail("Saving second company with same abbreviation should have thrown an exception");
	}


	@Test
	public void testSameCompanyParent() {
		TestUtils.expectException(SameHierarchyParentException.class, () -> {
			BusinessCompany company = this.businessCompanyService.getBusinessCompany(201);
			company.setParent(company);
			this.businessCompanyService.saveBusinessCompany(company);
		}, "Entity [ABC Client] cannot have itself as its parent.");
	}


	@Test
	public void testParentCompanyIsChild() {
		TestUtils.expectException(ParentIsChildException.class, () -> {
			BusinessCompany company = this.businessCompanyService.getBusinessCompany(201);
			BusinessCompany company2 = this.businessCompanyService.getBusinessCompany(202);
			company.setParent(company2);
			this.businessCompanyService.saveBusinessCompany(company);
			company2.setParent(company);
			this.businessCompanyService.saveBusinessCompany(company2);
		}, "Child [ABC Client] entity is also referenced as a parent.");
	}


	@Test
	public void testGloballyUnique_Both() {

		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompany brokerComp = new BusinessCompany();
			brokerComp.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER));
			brokerComp.setName("Test Broker - Unique Name");
			brokerComp.setAbbreviation("TBUN");
			this.businessCompanyService.saveBusinessCompany(brokerComp);

			BusinessCompany tradComp = new BusinessCompany();
			tradComp.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING));
			tradComp.setName("Test Broker - Unique Name");
			this.businessCompanyService.saveBusinessCompany(tradComp);
		}, "Company Name [Test Broker - Unique Name] is invalid because it is required to be globally unique and [Broker] company [Test Broker - Unique Name] is already using that name.");
	}


	@Test
	public void testGloballyUnique_AddUniqueFirst() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompany brokerComp = new BusinessCompany();
			brokerComp.setAbbreviation("TBUN");
			brokerComp.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER));
			brokerComp.setName("Test Broker - Unique Name");
			this.businessCompanyService.saveBusinessCompany(brokerComp);

			BusinessCompanyType tradingType = this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING);
			tradingType.setGloballyUnique(false);
			this.businessCompanyService.saveBusinessCompanyType(tradingType);

			BusinessCompany tradComp = new BusinessCompany();
			tradComp.setType(tradingType);
			tradComp.setName("Test Broker - Unique Name");
			this.businessCompanyService.saveBusinessCompany(tradComp);
		}, "Company Name [Test Broker - Unique Name] is invalid because [Broker] company [Test Broker - Unique Name] is already using that name and requires name to be globally unique.");
	}


	@Test
	public void testGloballyUnique_AddUniqueSecond() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompanyType tradingType = this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING);
			tradingType.setGloballyUnique(false);
			this.businessCompanyService.saveBusinessCompanyType(tradingType);

			BusinessCompany tradComp = new BusinessCompany();
			tradComp.setType(tradingType);
			tradComp.setName("Test Broker - Unique Name");
			this.businessCompanyService.saveBusinessCompany(tradComp);

			BusinessCompany brokerComp = new BusinessCompany();
			brokerComp.setAbbreviation("TBUN");
			brokerComp.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER));
			brokerComp.setName("Test Broker - Unique Name");
			this.businessCompanyService.saveBusinessCompany(brokerComp);
		}, "Company Name [Test Broker - Unique Name] is invalid because it is required to be globally unique and [Trading] company [Test Broker - Unique Name] is already using that name.");
	}


	@Test
	public void testNotGloballyUnique_SameType() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessCompanyType tradingType = this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING);
			tradingType.setGloballyUnique(false);
			this.businessCompanyService.saveBusinessCompanyType(tradingType);

			BusinessCompany tradComp = new BusinessCompany();
			tradComp.setType(tradingType);
			tradComp.setName("Test Broker - Same Name");
			this.businessCompanyService.saveBusinessCompany(tradComp);

			BusinessCompany tradComp_Dupe = new BusinessCompany();
			tradComp_Dupe.setType(tradingType);
			tradComp_Dupe.setName("Test Broker - Same Name");
			this.businessCompanyService.saveBusinessCompany(tradComp_Dupe);
		}, "Company Name [Test Broker - Same Name] is invalid because company [Test Broker - Same Name] is already using that name for the same company type.");
	}


	@Test
	public void testNotGloballyUnique_DifferentTypes() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		// NO EXCEPTION _ SHOULD BE FINE
		BusinessCompanyType tradingType = this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING);
		tradingType.setGloballyUnique(false);
		this.businessCompanyService.saveBusinessCompanyType(tradingType);

		BusinessCompanyType brokerType = this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER);
		brokerType.setGloballyUnique(false);
		this.businessCompanyService.saveBusinessCompanyType(brokerType);

		BusinessCompany tradComp = new BusinessCompany();
		tradComp.setType(tradingType);
		tradComp.setName("Test Broker - Same Name");
		this.businessCompanyService.saveBusinessCompany(tradComp);

		BusinessCompany brokerComp = new BusinessCompany();
		brokerComp.setType(brokerType);
		brokerComp.setAbbreviation("TBSN");
		brokerComp.setName("Test Broker - Same Name");
		this.businessCompanyService.saveBusinessCompany(brokerComp);
	}


	@Test
	//tests the default case on BusinessCompanyType where no System Condition is specified
	public void testSaveBusinessCompanyType_ModifyCondition_Default() {
		//setup BusinessCompany with no System Condition for this entire test case
		BusinessCompanyType businessCompanyType = getBusinessCompanyWithSystemCondition(Boolean.FALSE).getType();

		//make sure no SystemCondition is setup
		Assertions.assertNull(businessCompanyType.getEntityModifyCondition(), "SystemCondition is populated and shouldn't be");

		//UPDATE the BusinessCompany - only using one type of modification (UPDATE) as other cases are covered by other tests
		businessCompanyType.setName("Some Test Name");

		//prepare expected error messages
		String expectedError = "You must be an admin user to UPDATE this BusinessCompanyType.  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.";

		////////////////////////////////////////////////////////////////////////////

		//Allow save - tests for System Condition not required for non-admins using a non-admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
		saveBusinessCompanyType(businessCompanyType, null);

		//Allow save - tests for System Condition not required for non-admins using an admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		saveBusinessCompanyType(businessCompanyType, null);
	}


	@Test
	//tests the non-default case on BusinessCompanyType where a System Condition is specified
	public void testSaveBusinessCompanyType_ModifyCondition() {
		//setup BusinessCompany with a System Condition for this entire test case
		BusinessCompanyType businessCompanyType = getBusinessCompanyWithSystemCondition(Boolean.TRUE).getType();

		//make sure a SystemCondition is setup
		Assertions.assertNotNull(businessCompanyType.getEntityModifyCondition(), "SystemCondition is not populated and should be");

		//UPDATE the BusinessCompany - only using one type of modification (UPDATE) as other cases are covered by other tests
		businessCompanyType.setName("Some Test Name");

		//prepare expected error messages
		String noAccessMessage = "You don't have access!";
		String errorMessage = String.format("You do not have permission to UPDATE this BusinessCompanyType because: %s", noAccessMessage);

		//////////////////////////////////////////////////////////////////

		//use a non-admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);

		//allow update for non-admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompanyType.class))).thenReturn(null);
		saveBusinessCompanyType(businessCompanyType, null);

		//do not allow update for non-admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompanyType.class))).thenReturn(noAccessMessage);
		saveBusinessCompanyType(businessCompanyType, errorMessage);

		//////////////////////////////////////////////////////////////////

		//use an admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		//allow update for admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompanyType.class))).thenReturn(null);
		saveBusinessCompanyType(businessCompanyType, null);

		//allow update for admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompanyType.class))).thenReturn(noAccessMessage);
		saveBusinessCompanyType(businessCompanyType, null);

		//////////////////////////////////////////////////////////////////
	}


	@Test
	//tests the default case on BusinessCompany where no System Condition is specified
	public void testSaveBusinessCompany_ModifyCondition_Default() {
		//setup BusinessCompany with no System Condition for this entire test case
		BusinessCompany businessCompany = getBusinessCompanyWithSystemCondition(Boolean.FALSE);

		//make sure no SystemCondition is setup
		Assertions.assertNull(businessCompany.getType().getEntityModifyCondition(), "SystemCondition is populated and shouldn't be");

		//UPDATE the BusinessCompany - only using one type of modification (UPDATE) as other cases are covered by other tests
		businessCompany.setName("Some Test Name");

		//prepare expected error messages
		String expectedError = "You must be an admin user to UPDATE this BusinessCompany.  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.";

		////////////////////////////////////////////////////////////////////////////

		//Allow save - tests for System Condition not required for non-admins using a non-admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
		saveBusinessCompany(businessCompany, null);

		//Allow save - tests for System Condition not required for non-admins using an admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		saveBusinessCompany(businessCompany, null);

		//////////////////////////////////////////////////////////////////

		BusinessCompanyType businessCompanyType = businessCompany.getType();
		businessCompanyType.setName("Some Test Name");

		//Allow save - tests for System Condition not required for non-admins using a non-admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
		saveBusinessCompanyType(businessCompanyType, null);

		//Allow save - tests for System Condition not required for non-admins using an admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		saveBusinessCompanyType(businessCompanyType, null);
	}


	@Test
	//tests the non-default case on BusinessCompany where a System Condition is specified
	public void testSaveBusinessCompany_ModifyCondition() {
		//setup BusinessCompany with a System Condition for this entire test case
		BusinessCompany businessCompany = getBusinessCompanyWithSystemCondition(Boolean.TRUE);

		//make sure a SystemCondition is setup
		Assertions.assertNotNull(businessCompany.getType().getEntityModifyCondition(), "SystemCondition is not populated and should be");

		//UPDATE the BusinessCompany - only using one type of modification (UPDATE) as other cases are covered by other tests
		businessCompany.setName("Some Test Name");

		//prepare expected error messages
		String noAccessMessage = "You don't have access!";
		String errorMessage = String.format("You do not have permission to UPDATE this BusinessCompany because: %s", noAccessMessage);

		//////////////////////////////////////////////////////////////////

		//use a non-admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);

		//allow update for non-admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompany.class))).thenReturn(null);
		saveBusinessCompany(businessCompany, null);

		//do not allow update for non-admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompany.class))).thenReturn(noAccessMessage);
		saveBusinessCompany(businessCompany, errorMessage);

		//////////////////////////////////////////////////////////////////

		//use an admin user
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);

		//allow update for admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompany.class))).thenReturn(null);
		saveBusinessCompany(businessCompany, null);

		//allow update for admin user
		Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessCompany.class))).thenReturn(noAccessMessage);
		saveBusinessCompany(businessCompany, null);

		//////////////////////////////////////////////////////////////////
	}


	private BusinessCompany getBusinessCompanyWithSystemCondition(boolean populateSystemCondition) {
		//setup BusinessCompanyType to make sure no System Condition is present for this entire test case
		BusinessCompanyType businessCompanyType = this.businessCompanyService.getBusinessCompanyType(TYPE_TRADING);

		if (populateSystemCondition) {
			SystemCondition systemCondition = this.systemConditionService.getSystemCondition(1);
			Assertions.assertNotNull(systemCondition);
			businessCompanyType.setEntityModifyCondition(systemCondition);
		}
		else {
			businessCompanyType.setEntityModifyCondition(null);
		}
		this.businessCompanyService.saveBusinessCompanyType(businessCompanyType);

		return this.businessCompanyService.getBusinessCompany(202);
	}


	private void saveBusinessCompany(BusinessCompany businessCompany, String expectedError) {
		boolean isExpectError = !StringUtils.isEmpty(expectedError);
		boolean errorThrown = false;
		try {
			this.businessCompanyService.saveBusinessCompany(businessCompany);
		}
		catch (Throwable e) {
			errorThrown = true;
			if (isExpectError) {
				//expected error - check to see if it is the error message we expect
				Assertions.assertEquals(expectedError, e.getMessage());
			}
			else {
				// did not expect error but we got one - fail the test
				Assertions.fail(String.format("Did not expect error on save of business company, but got one.  Message: %s", ExceptionUtils.getOriginalMessage(e)));
			}
		}

		//expected error but we did not get one - fail the test
		if (!errorThrown && isExpectError) {
			Assertions.fail(String.format("Expected error but did not get one: %s", expectedError));
		}
	}


	private void saveBusinessCompanyType(BusinessCompanyType businessCompanyType, String expectedError) {
		boolean isExpectError = !StringUtils.isEmpty(expectedError);
		boolean errorThrown = false;
		try {
			this.businessCompanyService.saveBusinessCompanyType(businessCompanyType);
		}
		catch (Throwable e) {
			errorThrown = true;
			if (isExpectError) {
				//expected error - check to see if it is the error message we expect
				Assertions.assertEquals(expectedError, e.getMessage());
			}
			else {
				// did not expect error but we got one - fail the test
				Assertions.fail(String.format("Did not expect error on save of business company, but got one.  Message: %s", ExceptionUtils.getOriginalMessage(e)));
			}
		}

		//expected error but we did not get one - fail the test
		if (!errorThrown && isExpectError) {
			Assertions.fail(String.format("Expected error but did not get one: %s", expectedError));
		}
	}
}
