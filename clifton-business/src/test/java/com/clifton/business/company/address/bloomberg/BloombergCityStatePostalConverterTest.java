package com.clifton.business.company.address.bloomberg;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.common.AddressEntity;
import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BloombergCityStatePostalConverterTest {

	private static final ClassPathResource inputFile = new ClassPathResource("com/clifton/business/company/address/bloomberg/integrationAddresses.csv");

	@Resource
	private BloombergCityStatePostalConverter companyAddressUnitedStatesAdapter;

	@Resource
	private BloombergParserHandler bloombergBloombergParserHandler;

	private ObjectMapper mapper = new ObjectMapper();


	@SuppressWarnings("unchecked")
	@Test
	public void variousAddressFormatParsing() throws Exception {
		Assertions.assertNotNull(inputFile);
		Assertions.assertNotNull(this.companyAddressUnitedStatesAdapter);
		Assertions.assertNotNull(this.bloombergBloombergParserHandler);

		List<String> lines = Files.readAllLines(inputFile.getFile().toPath(), StandardCharsets.UTF_16);
		AddressObject results;
		int lineNumber = 0;
		for (String line : lines) {
			lineNumber++;
			if (line.startsWith("bloombergId")) {
				continue;
			}
			String[] parts = line.split("\t");
			List<String> components = this.bloombergBloombergParserHandler.splitIntoAddressComponents(parts[1]);
			Map<String, String> expected = this.mapper.readValue(parts[2], Map.class);
			results = this.companyAddressUnitedStatesAdapter.convertAddress(parts[1]);
			condenseAddressLines(results);
			TestAddressEntity resultsAddress = new TestAddressEntity(results);
			TestAddressEntity expectedAddress = new TestAddressEntity(expected);
			resultsAddress.setId(lineNumber);
			expectedAddress.setId(lineNumber);
			MatcherAssert.assertThat(resultsAddress.toString().toLowerCase(), IsEqual.equalTo(expectedAddress.toString().toLowerCase()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void condenseAddressLines(AddressObject addressEntity) {
		List<String> addressList = new ArrayList<>();
		addressList.add(addressEntity.getAddressLine1());
		addressList.add(addressEntity.getAddressLine2());
		addressList.add(addressEntity.getAddressLine3());
		addressList = addressList.stream().filter(l -> !StringUtils.isEmpty(l)).collect(Collectors.toList());
		addressEntity.setAddressLine1(null);
		addressEntity.setAddressLine2(null);
		addressEntity.setAddressLine3(null);
		addressEntity.setAddressLine1(String.join(" ", addressList));
	}


	private static class TestAddressEntity extends AddressEntity {

		private static final List<String> shown = Arrays.asList(
				"id",
				"addressLine1",
				"addressLine2",
				"addressLine3",
				"city",
				"state",
				"postalCode",
				"country"
		);


		public TestAddressEntity(AddressObject addressEntity) {
			BeanUtils.copyProperties(addressEntity, this);
		}


		public TestAddressEntity(Map<String, String> values) {
			for (Map.Entry<String, String> stringStringEntry : values.entrySet()) {
				BeanUtils.setPropertyValue(this, stringStringEntry.getKey(), stringStringEntry.getValue());
			}
		}


		@Override
		public String toString() {
			Map<String, String> results = new HashMap<>();
			Map<String, Object> values = BeanUtils.describe(this);
			for (Map.Entry<String, Object> stringObjectEntry : values.entrySet()) {
				if (shown.contains(stringObjectEntry.getKey())) {
					String value = stringObjectEntry.getValue() == null ? "" : stringObjectEntry.getValue().toString();
					results.put(stringObjectEntry.getKey(), value);
				}
			}
			return results.toString();
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			if (!super.equals(o)) {
				return false;
			}
			AddressEntity that = (AddressEntity) o;
			String thisAddress1 = StringUtils.isEmpty(getAddressLine1()) ? "" : getAddressLine1();
			String thatAddress1 = StringUtils.isEmpty(that.getAddressLine1()) ? "" : that.getAddressLine1();
			if (!thisAddress1.replaceAll("\\s+|,+", "").equalsIgnoreCase(thatAddress1.replaceAll("\\s+|,+", ""))) {
				return false;
			}
			return Objects.equals(getCity(), that.getCity()) &&
					Objects.equals(getState(), that.getState()) &&
					Objects.equals(getPostalCode(), that.getPostalCode()) &&
					Objects.equals(getCountry(), that.getCountry());
		}


		@Override
		public int hashCode() {
			return Objects.hash(super.hashCode(), getAddressLine1(), getAddressLine2(), getAddressLine3(), getCity(), getState(), getPostalCode(), getCountry());
		}
	}
}
