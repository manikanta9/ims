package com.clifton.business.service;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


/**
 * The <code>BusinessServiceServiceImplTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessServiceServiceImplTests {

	@Resource
	private BusinessServiceService businessServiceService;


	@Test
	public void testSetupServiceStructure_Successful() {
		BusinessService franchise = populateBusinessServiceObject(BusinessServiceLevelTypes.FRANCHISE, "Customized Exposure Management", null);
		saveBusinessService(franchise, null);
		validateServiceLevelStructure(franchise, franchise.getName(), null, null, null);

		BusinessService superStrategy = populateBusinessServiceObject(BusinessServiceLevelTypes.SUPER_STRATEGY, "Custom Beta", franchise);
		saveBusinessService(superStrategy, null);
		validateServiceLevelStructure(superStrategy, franchise.getName(), superStrategy.getName(), null, null);

		BusinessService service1 = populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE, "Enhanced U.S. Large Cap", superStrategy);
		service1.setStartDate(DateUtils.toDate("01/01/2016"));
		service1.setApplication(this.businessServiceService.getBusinessServiceApplication((short) 4));
		saveBusinessService(service1, null);
		validateServiceLevelStructure(service1, franchise.getName(), superStrategy.getName(), null, service1.getName());

		BusinessService serviceGroup = populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_GROUP, "Passive Index Management", superStrategy);
		saveBusinessService(serviceGroup, null);
		validateServiceLevelStructure(serviceGroup, franchise.getName(), superStrategy.getName(), serviceGroup.getName(), null);

		BusinessService service2 = populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE, "Passive Index Management ETFs", serviceGroup);
		service2.setApplication(this.businessServiceService.getBusinessServiceApplication((short) 4));
		saveBusinessService(service2, null);
		validateServiceLevelStructure(service2, franchise.getName(), superStrategy.getName(), serviceGroup.getName(), service2.getName());

		BusinessService serviceComponent = populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_COMPONENT, "Test Service Component", service2);
		serviceComponent.setStartDate(DateUtils.toDate("05/01/2016"));
		saveBusinessService(serviceComponent, null);
		validateServiceLevelStructure(serviceComponent, franchise.getName(), superStrategy.getName(), serviceGroup.getName(), service2.getName());
	}


	@Test
	public void testSetupService_InvalidParent() {
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SUPER_STRATEGY, "Test", null), "Business Service Level Type [SUPER_STRATEGY] requires parent selection.");
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_GROUP, "Test", null), "Business Service Level Type [SERVICE_GROUP] requires parent selection.");
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE, "Test", null), "Business Service Level Type [SERVICE] requires parent selection.");
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_COMPONENT, "Test", null), "Business Service Level Type [SERVICE_COMPONENT] requires parent selection.");

		BusinessService franchise = populateBusinessServiceObject(BusinessServiceLevelTypes.FRANCHISE, "Customized Exposure Management", null);
		saveBusinessService(franchise, null);
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.FRANCHISE, "Test", franchise), "Business Service Level Type [FRANCHISE] does not allow parent selections.");

		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_GROUP, "Test", franchise), "Parent Service [Customized Exposure Management] is not a valid parent. Business Service Level Type [SERVICE_GROUP] only allows parents of type(s) SUPER_STRATEGY");
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE, "Test", franchise), "Parent Service [Customized Exposure Management] is not a valid parent. Business Service Level Type [SERVICE] only allows parents of type(s) SUPER_STRATEGY, SERVICE_GROUP");
		saveBusinessService(populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_COMPONENT, "Test", franchise), "Parent Service [Customized Exposure Management] is not a valid parent. Business Service Level Type [SERVICE_COMPONENT] only allows parents of type(s) SERVICE");
	}


	@Test
	public void testSetupService_InvalidDates() {
		Date startDate = DateUtils.toDate("01/01/2016");
		Date endDate = DateUtils.toDate("12/31/2016");

		BusinessService franchise = populateBusinessServiceObject(BusinessServiceLevelTypes.FRANCHISE, "Customized Exposure Management", null);
		franchise.setStartDate(startDate);
		saveBusinessService(franchise, "Business Service Level Type [FRANCHISE] does not support start date.");
		franchise.setStartDate(null);
		saveBusinessService(franchise, null);

		BusinessService superStrategy = populateBusinessServiceObject(BusinessServiceLevelTypes.SUPER_STRATEGY, "Custom Beta", franchise);
		superStrategy.setEndDate(endDate);
		saveBusinessService(superStrategy, "Business Service Level Type [SUPER_STRATEGY] does not support end date.");
		superStrategy.setEndDate(null);

		BusinessService serviceGroup = populateBusinessServiceObject(BusinessServiceLevelTypes.SERVICE_GROUP, "Passive Index Management", superStrategy);
		serviceGroup.setStartDate(startDate);
		serviceGroup.setEndDate(endDate);
		saveBusinessService(serviceGroup, "Business Service Level Type [SERVICE_GROUP] does not support start date.");
	}


	@Test
	public void testSetupServiceStructure_NotAssignableService() {
		BusinessService franchise = populateBusinessServiceObject(BusinessServiceLevelTypes.FRANCHISE, "Customized Exposure Management", null);
		franchise.setApplication(this.businessServiceService.getBusinessServiceApplication((short) 4));
		saveBusinessService(franchise, "Investment Vehicle selection is only supported for assignable services.");
		franchise.setApplication(null);
		saveBusinessService(franchise, null);

		BusinessService superStrategy = populateBusinessServiceObject(BusinessServiceLevelTypes.SUPER_STRATEGY, "Custom Beta", franchise);
		superStrategy.setInvestmentStyle("Test");
		saveBusinessService(superStrategy, "Investment Style selection is only supported for assignable services.");
		superStrategy.setInvestmentStyle(null);
		saveBusinessService(superStrategy, null);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BusinessService populateBusinessServiceObject(BusinessServiceLevelTypes serviceLevelType, String name, BusinessService parent) {
		BusinessService service = new BusinessService();
		service.setServiceLevelType(serviceLevelType);
		service.setName(name);
		service.setParent(parent);
		return service;
	}


	private void saveBusinessService(BusinessService service, String expectedErrorMessage) {
		boolean error = false;
		try {
			this.businessServiceService.saveBusinessService(service);
		}
		catch (Exception e) {
			error = true;
			if (expectedErrorMessage == null) {
				Assertions.fail("Did not expect error on save of business service, but did.  Message: " + e.getMessage());
			}
			Assertions.assertEquals(expectedErrorMessage, e.getMessage());
		}
		if (!error && !StringUtils.isEmpty(expectedErrorMessage)) {
			Assertions.fail("Expected error on save with message " + expectedErrorMessage + ", but no error encountered");
		}
	}


	private void validateServiceLevelStructure(BusinessService service, String franchiseName, String superStrategyName, String serviceGroupName, String serviceName) {
		Assertions.assertEquals(franchiseName, service.getFranchiseName());
		Assertions.assertEquals(superStrategyName, service.getSuperStrategyName());
		Assertions.assertEquals(serviceGroupName, service.getServiceGroupName());
		Assertions.assertEquals(serviceName, service.getServiceName());
		Assertions.assertEquals(StringUtils.coalesce(false, serviceGroupName, serviceName), service.getCoalesceBusinessServiceGroupName());
	}
}
