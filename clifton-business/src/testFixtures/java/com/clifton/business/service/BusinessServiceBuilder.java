package com.clifton.business.service;

public class BusinessServiceBuilder {

	private BusinessService businessService;


	private BusinessServiceBuilder(BusinessService businessService) {
		this.businessService = businessService;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessServiceBuilder createCustomizedExposureManagement() {
		BusinessService businessService = new BusinessService();

		businessService.setId((short) 37);
		businessService.setName("Customized Exposure Management");
		businessService.setDescription("Strategies designed to increase efficiency, enhance return and manage risk");
		businessService.setServiceLevelType(BusinessServiceLevelTypes.FRANCHISE);

		return new BusinessServiceBuilder(businessService);
	}


	public static BusinessServiceBuilder createCustomOverlay() {
		BusinessService businessService = new BusinessService();

		businessService.setId((short) 100);
		businessService.setParent(BusinessServiceBuilder.createCustomizedExposureManagement().toBusinessService());
		businessService.setName("Custom Overlay");
		businessService.setDescription("Custom Overlay");
		businessService.setServiceLevelType(BusinessServiceLevelTypes.SUPER_STRATEGY);

		return new BusinessServiceBuilder(businessService);
	}


	public static BusinessServiceBuilder createOverlay() {
		BusinessService businessService = new BusinessService();

		businessService.setId((short) 3);
		businessService.setParent(BusinessServiceBuilder.createCustomOverlay().toBusinessService());
		businessService.setApplication(BusinessServiceApplicationBuilder.createSeparateAccount().toBusinessServiceApplication());
		businessService.setName("PIOS (Overlay)");
		businessService.setDescription("A comprehensive overlay solution designed to help investors overcome persistent portfolio inefficiencies and risks affecting policy objectives.\n" +
				"\n" +
				"Policy Implementation Overlay Service");
		businessService.setInvestmentStyle("Passive");
		businessService.setServiceLevelType(BusinessServiceLevelTypes.SERVICE);

		return new BusinessServiceBuilder(businessService);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceBuilder withId(short id) {
		getBusinessService().setId(id);
		return this;
	}


	public BusinessServiceBuilder withName(String name) {
		getBusinessService().setName(name);
		return this;
	}


	public BusinessServiceBuilder withDescription(String description) {
		getBusinessService().setDescription(description);
		return this;
	}


	public BusinessService toBusinessService() {
		return this.businessService;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessService getBusinessService() {
		return this.businessService;
	}
}
