package com.clifton.business.service;

public class BusinessServiceApplicationBuilder {

	private BusinessServiceApplication businessServiceApplication;


	private BusinessServiceApplicationBuilder(BusinessServiceApplication businessServiceApplication) {
		this.businessServiceApplication = businessServiceApplication;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessServiceApplicationBuilder createSeparateAccount() {
		BusinessServiceApplication businessServiceApplication = new BusinessServiceApplication();

		businessServiceApplication.setId((short) 22);
		businessServiceApplication.setName("Separate Account");
		businessServiceApplication.setDescription("A privately managed investment account that uses pooled money to buy individual assets.");

		return new BusinessServiceApplicationBuilder(businessServiceApplication);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceApplicationBuilder withId(short id) {
		getBusinessServiceApplication().setId(id);
		return this;
	}


	public BusinessServiceApplicationBuilder withName(String name) {
		getBusinessServiceApplication().setName(name);
		return this;
	}


	public BusinessServiceApplicationBuilder withDescription(String description) {
		getBusinessServiceApplication().setDescription(description);
		return this;
	}


	public BusinessServiceApplication toBusinessServiceApplication() {
		return this.businessServiceApplication;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessServiceApplication getBusinessServiceApplication() {
		return this.businessServiceApplication;
	}
}
