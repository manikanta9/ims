package com.clifton.business.service;

public class BusinessServiceProcessingTypeBuilder {

	private BusinessServiceProcessingType businessServiceProcessingType;


	private BusinessServiceProcessingTypeBuilder(BusinessServiceProcessingType businessServiceProcessingType) {
		this.businessServiceProcessingType = businessServiceProcessingType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessServiceProcessingTypeBuilder createOverlay() {
		BusinessServiceProcessingType processingType = new BusinessServiceProcessingType();

		processingType.setId((short) 1);
		processingType.setName("Overlay");
		processingType.setDescription("\"PIOS\" Services");
		processingType.setProcessingType(ServiceProcessingTypes.PORTFOLIO_RUNS);

		return new BusinessServiceProcessingTypeBuilder(processingType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceProcessingTypeBuilder withId(short id) {
		getBusinessServiceProcessingType().setId(id);
		return this;
	}


	public BusinessServiceProcessingTypeBuilder withName(String name) {
		getBusinessServiceProcessingType().setName(name);
		return this;
	}


	public BusinessServiceProcessingTypeBuilder withDescription(String description) {
		getBusinessServiceProcessingType().setDescription(description);
		return this;
	}


	public BusinessServiceProcessingType toBusinessServiceProcessingType() {
		return this.businessServiceProcessingType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessServiceProcessingType getBusinessServiceProcessingType() {
		return this.businessServiceProcessingType;
	}
}
