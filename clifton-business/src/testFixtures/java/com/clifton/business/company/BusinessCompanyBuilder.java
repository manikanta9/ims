package com.clifton.business.company;

public class BusinessCompanyBuilder {

	private BusinessCompany businessCompany;


	private BusinessCompanyBuilder(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessCompanyBuilder createEmptyBusinessCompany() {
		return new BusinessCompanyBuilder(new BusinessCompany());
	}


	public static BusinessCompanyBuilder createParametricPortfolio() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(10035);
		businessCompany.setType(BusinessCompanyTypeBuilder.createOther().toBusinessCompanyType());
		businessCompany.setName("Parametric Portfolio Associates LLC");
		businessCompany.setDescription("Umbrella relationship for all of the Parametric Divisions (Seattle, Minneapolis, Westport, etc).");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createParametric() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(1);
		businessCompany.setParent(BusinessCompanyBuilder.createParametricPortfolio().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createOurCompanies().toBusinessCompanyType());
		businessCompany.setName("Test Company 4");
		businessCompany.setAlias("PPA-MN");
		businessCompany.setDescription("Test Company 4");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBloomberg() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(2);
		businessCompany.setType(BusinessCompanyTypeBuilder.createVendor().toBusinessCompanyType());
		businessCompany.setName("Bloomberg");
		businessCompany.setBloombergCompanyIdentifier(143028);

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createMorganStanley() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Morgan Stanley & Co. Inc.");
		businessCompany.setAbbreviation("MSCO");
		businessCompany.setDescription("International documents need to be placed under MSIL for example:\n" +
				"EMIR International document is housed under MSIL");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createRedi() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(8624);
		businessCompany.setType(BusinessCompanyTypeBuilder.createVendor().toBusinessCompanyType());
		businessCompany.setName("REDI");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createNuveen() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(10235);
		businessCompany.setType(BusinessCompanyTypeBuilder.createRegisteredInvestmentAccount().toBusinessCompanyType());
		businessCompany.setName("Nuveen Dow 30sm Dynamic Overwr");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createGoldmanHolding() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4468);
		businessCompany.setType(BusinessCompanyTypeBuilder.createHoldingAccount().toBusinessCompanyType());
		businessCompany.setName("Goldman Sachs");
		businessCompany.setDescription("Parent company for DTC 005 and DTC 501");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createGoldmanSachs() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(7);
		businessCompany.setParent(BusinessCompanyBuilder.createGoldmanHolding().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Goldman Sachs & Co.");
		businessCompany.setDescription("Goldman, Sachs & Co. Depository Trust Company used for everything except for stocks (DTC 005) Tax ID for 5500 reporting 13-5108880; LEI FOR8UP27PHTHYVLBNG30");
		businessCompany.setAbbreviation("GSCO");
		businessCompany.setAlias("GS - Futures/Options & DESK Stock Orders");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createSocieteGenerale() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4576);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Societe Generale");
		businessCompany.setAbbreviation("SCGN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBankOfAmericaMerrillLynch() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(5042);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setParent(BusinessCompanyBuilder.createBankOfAmericaCorp().toBusinessCompany());
		businessCompany.setName("Bank of America Merrill Lynch");
		businessCompany.setAbbreviation("BAML");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createStifelNicolaus() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(5043);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Stifel, Nicolaus");
		businessCompany.setAbbreviation("STFN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBankOfAmericaCorp() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(6833);
		businessCompany.setType(BusinessCompanyTypeBuilder.createInvestmentManager().toBusinessCompanyType());
		businessCompany.setName("Bank of America Corp");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBankOfAmericaNa() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4574);
		businessCompany.setParent(BusinessCompanyBuilder.createBankOfAmericaCorp().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createCustodian().toBusinessCompanyType());
		businessCompany.setName("Bank of America, N.A.");
		businessCompany.setAbbreviation("BANA");
		businessCompany.setBusinessIdentifierCode("BOFAUS3N");
		businessCompany.setBloombergCompanyIdentifier(1134752);

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCreditSuisse() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(1307);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Credit Suisse Securities (USA) LLC");
		businessCompany.setAbbreviation("CSSC");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createJPMorganChaseCompany() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(6753);
		businessCompany.setType(BusinessCompanyTypeBuilder.createInvestmentManager().toBusinessCompanyType());
		businessCompany.setName("JPMorgan Chase & Co");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createJPMorganChaseBankNA() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(9931);
		businessCompany.setParent(BusinessCompanyBuilder.createJPMorganChaseCompany().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("JPMorgan Chase Bank NA");
		businessCompany.setDescription("Counter Party to ISDA");
		businessCompany.setAbbreviation("JPM");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createJPMorganChase() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4079);
		businessCompany.setParent(BusinessCompanyBuilder.createJPMorganChaseBankNA().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createCustodian().toBusinessCompanyType());
		businessCompany.setName("JP Morgan Chase");
		businessCompany.setAbbreviation("JPMC");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBarclaysCapitolInc() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4085);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Barclays Capital Inc.");
		businessCompany.setAlias("Barclays - Options/USTreasuries");
		businessCompany.setAbbreviation("BCAP");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBNPParibasSecCorp() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4086);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("BNP Paribas Securities Corp.");
		businessCompany.setAbbreviation("BNPP");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCitigroupGlobalMarkets() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4087);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setParent(BusinessCompanyBuilder.createCitigroup().toBusinessCompany());
		businessCompany.setName("Citigroup Global Markets Inc./Salomon Brothers");
		businessCompany.setAlias("Citigroup - Bonds Only");
		businessCompany.setAbbreviation("CTSB");
		businessCompany.setDescription("Citigroup Global Markets, Inc./Salomon Brothers Depository Trust Company used for trading bonds.");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createDeutscheBankSecurities() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4088);
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Deutsche Bank Securities Inc.");
		businessCompany.setAbbreviation("DBSI");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCLPHoldingsThreeLLC() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(14333);
		businessCompany.setType(BusinessCompanyTypeBuilder.createOtherIssuer().toBusinessCompanyType());
		businessCompany.setName("CLP Holdings Three LLC");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCitadelSecuritiesLLC() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(14332);
		businessCompany.setParent(BusinessCompanyBuilder.createCLPHoldingsThreeLLC().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Citadel Securities LLC");
		businessCompany.setAbbreviation("CDEL");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCitadelSecurities() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(10558);
		businessCompany.setParent(BusinessCompanyBuilder.createCitadelSecuritiesLLC().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Citadel Securities Institutional LLC");
		businessCompany.setAbbreviation("CDIL");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createGoldmanExecuteClearing() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4469);
		businessCompany.setParent(BusinessCompanyBuilder.createGoldmanHolding().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createBroker().toBusinessCompanyType());
		businessCompany.setName("Goldman Sachs Execution & Clearing");
		businessCompany.setAlias("GS - REDI Stocks Only");
		businessCompany.setAbbreviation("GSEC");
		businessCompany.setDescription("Goldman Sachs Execution & Clearing Depository Trust Company used for trading stocks. DTC 0005 (changed from DTC # 0501 starting Trade Date 9/9/2014 due to GSCO /GSET merger integration) -- not yet implemented as of 9/30.");


		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCitigroup() {
		return new BusinessCompanyBuilder(new BusinessCompany())
				.withId(4467)
				.withName("Citigroup")
				.withBusinessCompanyType(BusinessCompanyTypeBuilder.createOtherIssuer().toBusinessCompanyType())
				.withDescription("Parent company for DTC 247 and DTC 418");
	}


	public static BusinessCompanyBuilder createTestingCompany1() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(6206);
		businessCompany.setType(BusinessCompanyTypeBuilder.createClientRelationship().toBusinessCompanyType());
		businessCompany.setName("Testing Company 1");
		businessCompany.setAlias("TEST 1");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createTestingCompany3() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(6207);
		businessCompany.setType(BusinessCompanyTypeBuilder.createClientRelationship().toBusinessCompanyType());
		businessCompany.setName("Testing Company 3");
		businessCompany.setAlias("TEST 3");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createTestingCompany2() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4120);
		businessCompany.setType(BusinessCompanyTypeBuilder.createClient().toBusinessCompanyType());
		businessCompany.setName("Testing Company Client");
		businessCompany.setAlias("TEST Client");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createMellonBankNa() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(9);
		businessCompany.setType(BusinessCompanyTypeBuilder.createCustodian().toBusinessCompanyType());
		businessCompany.setName("Mellon Bank, N.A.");
		businessCompany.setAbbreviation("MELL");
		businessCompany.setBusinessIdentifierCode("MELNUS3PGLB");
		businessCompany.setBloombergCompanyIdentifier(216520);

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createBankOfNewYorkMellon() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(4073);
		businessCompany.setParent(BusinessCompanyBuilder.createMellonBankNa().toBusinessCompany());
		businessCompany.setType(BusinessCompanyTypeBuilder.createCustodian().toBusinessCompanyType());
		businessCompany.setName("Bank of New York Mellon");
		businessCompany.setAbbreviation("BNYM");
		businessCompany.setDescription("FED Mnemonic listed below is used for REPO instructions only at this time");
		businessCompany.setBusinessIdentifierCode("IRVTUS3NIBK");
		businessCompany.setDtcNumber("901");
		businessCompany.setFedMnemonic("BK OF NYC/SEC LEND");
		businessCompany.setBloombergCompanyIdentifier(225016);


		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createCustodian1() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(209);
		businessCompany.setType(BusinessCompanyTypeBuilder.createCustodian().toBusinessCompanyType());
		businessCompany.setName("Test Six");
		businessCompany.setAbbreviation("TN4");
		businessCompany.setDescription("Northern Six");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createClient5() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(122);
		businessCompany.setType(BusinessCompanyTypeBuilder.createClient().toBusinessCompanyType());
		businessCompany.setName("Test Company 5");
		businessCompany.setDescription("Test Company 5");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}


	public static BusinessCompanyBuilder createClient1() {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setId(123);
		businessCompany.setType(BusinessCompanyTypeBuilder.createClient().toBusinessCompanyType());
		businessCompany.setName("Test Company 4");
		businessCompany.setDescription("Test Company 4");
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");

		return new BusinessCompanyBuilder(businessCompany);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyBuilder withId(int id) {
		getBusinessCompany().setId(id);
		return this;
	}


	public BusinessCompanyBuilder withBusinessCompanyType(BusinessCompanyType businessCompanyType) {
		getBusinessCompany().setType(businessCompanyType);
		return this;
	}


	public BusinessCompanyBuilder withName(String name) {
		getBusinessCompany().setName(name);
		return this;
	}


	public BusinessCompanyBuilder withDescription(String description) {
		getBusinessCompany().setDescription(description);
		return this;
	}


	public BusinessCompanyBuilder withParent(BusinessCompany parent) {
		getBusinessCompany().setParent(parent);
		return this;
	}


	public BusinessCompanyBuilder withBusinessIdentifierCode(String businessIdentifierCode) {
		getBusinessCompany().setBusinessIdentifierCode(businessIdentifierCode);
		return this;
	}


	public BusinessCompany toBusinessCompany() {
		return this.businessCompany;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}
}
