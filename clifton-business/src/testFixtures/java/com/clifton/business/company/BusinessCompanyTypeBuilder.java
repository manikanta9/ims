package com.clifton.business.company;

public class BusinessCompanyTypeBuilder {

	private BusinessCompanyType businessCompanyType;


	private BusinessCompanyTypeBuilder(BusinessCompanyType businessCompanyType) {
		this.businessCompanyType = businessCompanyType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessCompanyTypeBuilder createEmptyBusinessCompanyType() {
		return new BusinessCompanyTypeBuilder(new BusinessCompanyType());
	}


	public static BusinessCompanyTypeBuilder createVendor() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 3);
		businessCompanyType.setName("Vendor");
		businessCompanyType.setDescription("Provider of products, data and/or services.");
		businessCompanyType.setGloballyUnique(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createInvestmentManager() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 4);
		businessCompanyType.setName("Investment Manager");
		businessCompanyType.setDescription("Money Managers that invests funds of our clients. We may need to overlay these funds and usually get their positions daily from the custodian.");
		businessCompanyType.setMoneyManager(true);
		businessCompanyType.setGloballyUnique(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createHoldingCompany() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 23);
		businessCompanyType.setName("Holding Company");
		businessCompanyType.setDescription("Top level holding company for other entities (brokers, custodians) that we work with.");
		businessCompanyType.setGloballyUnique(true);
		businessCompanyType.setSystemDefined(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createBroker() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 2);
		businessCompanyType.setName("Broker");
		businessCompanyType.setDescription("A broker firm provides trading services for various security instruments: stocks, bonds, futures, options, OTC, etc. The broker safeguards certain investment instruments (futures, options) while transfers other types (stocks, bonds) to the custodian.");
		businessCompanyType.setMoneyManager(true);
		businessCompanyType.setIssuer(true);
		businessCompanyType.setContractAllowed(true);
		businessCompanyType.setTradingAllowed(true);
		businessCompanyType.setAbbreviationRequired(true);
		businessCompanyType.setGloballyUnique(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createRegisteredInvestmentAccount() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 21);
		businessCompanyType.setName("Registered Investment Company");
		businessCompanyType.setDescription("Registered Investment Company - RIC\n" +
				"\n" +
				"A mutual fund or other investment company that is registered with the SEC. Registered investment companies are required to report their policies and financial conditions, much like a publicly-traded company. Most investment companies in the United States must register with the SEC; they are regulated by the Investment Company Act of 1940.");
		businessCompanyType.setMoneyManager(true);
		businessCompanyType.setIssuer(true);
		businessCompanyType.setGloballyUnique(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createHoldingAccount() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 23);
		businessCompanyType.setName("Holding Company");
		businessCompanyType.setDescription("Top level holding company for other entities (brokers, custodians) that we work with.");
		businessCompanyType.setGloballyUnique(true);
		businessCompanyType.setSystemDefined(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createClientRelationship() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 20);
		businessCompanyType.setName("Client Relationship");
		businessCompanyType.setDescription("Client Relationships of our company.");
		businessCompanyType.setContractAllowed(true);
		businessCompanyType.setSystemDefined(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createOurCompanies() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 5);
		businessCompanyType.setName("Our Companies");
		businessCompanyType.setDescription("Parametric Clifton. We are a special type of investment manager.");
		businessCompanyType.setOurCompany(true);
		businessCompanyType.setMoneyManager(true);
		businessCompanyType.setIssuer(true);
		businessCompanyType.setContractAllowed(true);
		businessCompanyType.setGloballyUnique(true);
		businessCompanyType.setSystemDefined(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createClient() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 6);
		businessCompanyType.setName("Client");
		businessCompanyType.setDescription("Clients of our company.");
		businessCompanyType.setContractAllowed(true);
		businessCompanyType.setSystemDefined(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createCustodian() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 1);
		businessCompanyType.setName("Custodian");
		businessCompanyType.setDescription("A custodial bank safeguards client's assets and reports on the assets held at other places (brokers, investment managers, etc).\n" +
				"\n" +
				"NOTE: The name should be globally unique but it doesn't have to be. In rare cases a Custodian can also be our Client.");
		businessCompanyType.setIssuer(true);
		businessCompanyType.setContractAllowed(true);
		businessCompanyType.setTradingAllowed(true);
		businessCompanyType.setAbbreviationRequired(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createOther() {
		BusinessCompanyType businessCompanyType = new BusinessCompanyType();

		businessCompanyType.setId((short) 9);
		businessCompanyType.setName("Other");
		businessCompanyType.setDescription("Other");
		businessCompanyType.setGloballyUnique(true);

		return new BusinessCompanyTypeBuilder(businessCompanyType);
	}


	public static BusinessCompanyTypeBuilder createOtherIssuer() {
		return new BusinessCompanyTypeBuilder(new BusinessCompanyType())
				.withId((short) 10)
				.withName("Other Issuer")
				.withDescription("Can be a law firm that issues its own internal accounts.  But the assets are actually held somewhere else (custodian, etc.)")
				.withIssuer(true)
				.withGloballyUnique(true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyTypeBuilder withId(short id) {
		getBusinessCompanyType().setId(id);
		return this;
	}


	public BusinessCompanyTypeBuilder withName(String name) {
		getBusinessCompanyType().setName(name);
		return this;
	}


	public BusinessCompanyTypeBuilder withDescription(String description) {
		getBusinessCompanyType().setDescription(description);
		return this;
	}


	public BusinessCompanyTypeBuilder withGloballyUnique(boolean globallyUnique) {
		getBusinessCompanyType().setGloballyUnique(globallyUnique);
		return this;
	}


	public BusinessCompanyTypeBuilder withIssuer(boolean issuer) {
		getBusinessCompanyType().setIssuer(issuer);
		return this;
	}


	public BusinessCompanyTypeBuilder withTradeAllowed(boolean tradeAllowed) {
		getBusinessCompanyType().setTradingAllowed(tradeAllowed);
		return this;
	}


	public BusinessCompanyTypeBuilder withContractAllowed(boolean contractAllowed) {
		getBusinessCompanyType().setContractAllowed(contractAllowed);
		return this;
	}


	public BusinessCompanyTypeBuilder withMoneyManager(boolean moneyManager) {
		getBusinessCompanyType().setMoneyManager(moneyManager);
		return this;
	}


	public BusinessCompanyType toBusinessCompanyType() {
		return this.businessCompanyType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessCompanyType getBusinessCompanyType() {
		return this.businessCompanyType;
	}
}
