package com.clifton.business.company.address.bloomberg;

import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListEntity;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListQuery;
import com.clifton.system.list.SystemListService;
import com.clifton.system.list.search.SystemListItemSearchForm;
import com.clifton.system.list.search.SystemListSearchForm;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Provides a {@link SystemListService} instance for use in test classes, it does not require
 * a database connection to provide {@link SystemListItem} values like 'States' and 'Canadian Provinces'.
 */
public class SystemListServiceTestProvider<L extends SystemList> implements SystemListService<L> {

	private Map<String, String> stateCodeMap = new HashMap<>();
	private Map<String, String> provinceCodeMap = new HashMap<>();

	private SystemList statesList = new SystemList() {
		@Override
		public boolean isSqlList() {
			return false;
		}
	};
	private SystemList provinceList = new SystemList() {
		@Override
		public boolean isSqlList() {
			return false;
		}
	};

	private List<SystemListItem> stateItemsList = new ArrayList<>();
	private List<SystemListItem> provinceItemsList = new ArrayList<>();


	public SystemListServiceTestProvider() {
		stateCodeMap.put("AL", "Alabama");
		stateCodeMap.put("AK", "Alaska");
		stateCodeMap.put("AZ", "Arizona");
		stateCodeMap.put("AR", "Arkansas");
		stateCodeMap.put("CA", "California");
		stateCodeMap.put("CO", "Colorado");
		stateCodeMap.put("CT", "Connecticut");
		stateCodeMap.put("DE", "Delaware");
		stateCodeMap.put("FL", "Florida");
		stateCodeMap.put("GA", "Georgia");
		stateCodeMap.put("HI", "Hawaii");
		stateCodeMap.put("ID", "Idaho");
		stateCodeMap.put("IL", "Illinois");
		stateCodeMap.put("IN", "Indiana");
		stateCodeMap.put("IA", "Iowa");
		stateCodeMap.put("KS", "Kansas");
		stateCodeMap.put("KY", "Kentucky");
		stateCodeMap.put("LA", "Louisiana");
		stateCodeMap.put("ME", "Maine");
		stateCodeMap.put("MD", "Maryland");
		stateCodeMap.put("MA", "Massachusetts");
		stateCodeMap.put("MI", "Michigan");
		stateCodeMap.put("MN", "Minnesota");
		stateCodeMap.put("MS", "Mississippi");
		stateCodeMap.put("MO", "Missouri");
		stateCodeMap.put("MT", "Montana");
		stateCodeMap.put("NE", "Nebraska");
		stateCodeMap.put("NV", "Nevada");
		stateCodeMap.put("NH", "New Hampshire");
		stateCodeMap.put("NJ", "New Jersey");
		stateCodeMap.put("NM", "New Mexico");
		stateCodeMap.put("NY", "New York");
		stateCodeMap.put("NC", "North Carolina");
		stateCodeMap.put("ND", "North Dakota");
		stateCodeMap.put("OH", "Ohio");
		stateCodeMap.put("OK", "Oklahoma");
		stateCodeMap.put("OR", "Oregon");
		stateCodeMap.put("PA", "Pennsylvania");
		stateCodeMap.put("RI", "Rhode Island");
		stateCodeMap.put("SC", "South Carolina");
		stateCodeMap.put("SD", "South Dakota");
		stateCodeMap.put("TN", "Tennessee");
		stateCodeMap.put("TX", "Texas");
		stateCodeMap.put("UT", "Utah");
		stateCodeMap.put("VT", "Vermont");
		stateCodeMap.put("VA", "Virginia");
		stateCodeMap.put("WA", "Washington");
		stateCodeMap.put("WV", "West Virginia");
		stateCodeMap.put("WI", "Wisconsin");
		stateCodeMap.put("WY", "Wyoming");

		statesList.setId(2);
		statesList.setName("States");

		Map<String, String> stateCodeMap = this.stateCodeMap;
		for (Map.Entry<String, String> stringStringEntry : stateCodeMap.entrySet()) {
			SystemListItem systemListItem = new SystemListItem();
			systemListItem.setSystemList(statesList);
			systemListItem.setActive(true);
			systemListItem.setValue(stringStringEntry.getKey());
			systemListItem.setText(stringStringEntry.getValue());
			this.stateItemsList.add(systemListItem);
		}

		provinceCodeMap.put("AB", "Alberta");
		provinceCodeMap.put("BC", "British Columbia");
		provinceCodeMap.put("MB", "Manitoba");
		provinceCodeMap.put("NB", "New Brunswick");
		provinceCodeMap.put("NL", "Newfoundland and Labrador");
		provinceCodeMap.put("NT", "Northwest Territories");
		provinceCodeMap.put("NS", "Nova Scotia");
		provinceCodeMap.put("NU", "Nunavut");
		provinceCodeMap.put("ON", "Ontario");
		provinceCodeMap.put("PE", "Prince Edward Island");
		provinceCodeMap.put("QC", "Quebec");
		provinceCodeMap.put("SK", "Saskatchewan");
		provinceCodeMap.put("YT", "Yukon");

		provinceList.setId(3);
		provinceList.setName("Canadian Provinces");

		for (Map.Entry<String, String> stringStringEntry : this.provinceCodeMap.entrySet()) {
			SystemListItem systemListItem = new SystemListItem();
			systemListItem.setSystemList(provinceList);
			systemListItem.setActive(true);
			systemListItem.setValue(stringStringEntry.getKey());
			systemListItem.setText(stringStringEntry.getValue());
			this.provinceItemsList.add(systemListItem);
		}
	}


	@Override
	public L getSystemList(int id) {
		Assertions.fail();
		return null;
	}


	@SuppressWarnings("unchecked")
	@Override
	public L getSystemListByName(String name) {
		switch (name) {
			case "States":
				return (L) this.statesList;
			case "Canadian Provinces":
				return (L) this.provinceList;
			default:
				Assertions.fail();
		}
		return null;
	}


	@Override
	public List<L> getSystemListList(SystemListSearchForm searchForm) {
		Assertions.fail();
		return null;
	}


	@Override
	public void deleteSystemList(int id) {
		Assertions.fail();
	}


	@Override
	public void saveSystemListEntity(SystemListEntity bean) {
		Assertions.fail();
	}


	@Override
	public void saveSystemListQuery(SystemListQuery bean) {
		Assertions.fail();
	}


	@Override
	public SystemListItem getSystemListItem(int id) {
		Assertions.fail();
		return null;
	}


	@Override
	public SystemListItem getSystemListItemByListAndText(String listName, String text) {
		Assertions.fail();
		return null;
	}


	@Override
	public SystemListItem getSystemListItemByListAndValue(String listName, String value) {
		Assertions.fail();
		return null;
	}


	@Override
	public List<SystemListItem> getSystemListItemListByList(int systemListId) {
		switch (systemListId) {
			case 2:
				return this.stateItemsList;
			case 3:
				return this.provinceItemsList;
			default:
				Assertions.fail();
		}
		return null;
	}


	@Override
	public List<SystemListItem> getSystemListItemList(SystemListItemSearchForm searchForm) {
		Assertions.fail();
		return null;
	}


	@Override
	public List<SystemListItem> getSystemListItemListByListBean(SystemList listEntity) {
		Assertions.fail();
		return null;
	}


	@Override
	public SystemListItem saveSystemListItem(SystemListItem bean) {
		Assertions.fail();
		return null;
	}


	@Override
	public void deleteSystemListItem(int id) {
		Assertions.fail();
	}
}
