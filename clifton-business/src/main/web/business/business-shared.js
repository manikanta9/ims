Ext.ns('Clifton.business', 'Clifton.business.company', 'Clifton.business.service');


// use to override detail class based on company type
Clifton.business.company.CompanyWindowOverrides = {};

Clifton.business.company.CompanyPlatformAdditionalTabs = [];

Clifton.business.BusinessSetupWindowAdditionalTabs = [];

Clifton.business.company.BasicCompanyWindowAdditionalTabs = [];

Clifton.business.service.ServiceWindowAdditionalTabs = [];
Clifton.business.service.ServiceProcessingTypes = [
	['PORTFOLIO_RUNS', 'PORTFOLIO_RUNS', 'Uses daily Portfolio Runs. Used most commonly for "PIOS" or other Overlay services, Fully Funded (GBR), and Structured Options'],
	['PORTFOLIO_TARGET', 'PORTFOLIO_TARGET', 'Uses daily Portfolio Runs to recalculate replications assigned target balance values and exposures. Used with Currency Hedging and Defensive Equity services.'],
	['DURATION_MANAGEMENT', 'DURATION_MANAGEMENT', 'Duration Management'],
	['LDI', 'LDI', 'Liability Driven Investments'],
	['HEDGING', 'HEDGING', '"Protection Plus" Services'],
	['SECURITY_TARGET', 'SECURITY_TARGET', 'Uses a fixed underlying security quantity or notional target to hold positions against']
];


Clifton.business.service.AccountingClosingMethods = [
	['HIFO', 'HIFO', 'Highest In First Out then Original Transaction Date, then lowest transaction id. Highest Cost (cost per unit) will appear before positions with lower cost per unit: minimize realized gains.'],
	['FIFO', 'FIFO', 'First In First Out by Original Transaction Date, then lowest price and then lowest transaction id.'],
	['LIFO', 'LIFO', 'Last In First Out by Original Transaction Date, then highest price and then highest transaction id.'],
	['LOFO', 'LOFO', 'Lowest Cost, First Out, then Original Transaction Date, then lowest transaction id. Lowest Cost (cost per unit) will appear before positions with higher cost per unit: maximize realized gains.']
];


Clifton.business.CompanyRelationshipGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'businessCompanyRelationshipListFind',
	xtype: 'gridpanel',
	instructions: 'The following companies are associated with this company.',
	additionalPropertiesToRequest: 'id|referenceOne.id',

	getDeleteURL: function() {
		return 'businessCompanyRelationshipDelete.json';
	},

	isIncludeRelatedCompanyCheckbox: function() {
		return false;
	},
	getRelatedCompanyCheckboxLabel: function() {
		return 'Include Parent/Child Company Relationships';
	},


	getCompanyId: function() {
		return this.getCompany().id;
	},

	// Override if Company is not the Main Form's Entity
	getCompany: function() {
		return this.getWindow().getMainForm().formValues;
	},

	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'First Company', width: 100, dataIndex: 'referenceOne.label', filter: {searchFieldName: 'referenceOneName'}, defaultSortColumn: true},
		{header: 'First Company Type', width: 60, hidden: true, dataIndex: 'referenceOne.type.name', filter: {type: 'combo', url: 'businessCompanyTypeListFind.json', searchFieldName: 'referenceOneTypeId'}},
		{header: 'Second Company', width: 100, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'referenceTwoName'}},
		{header: 'Second Company Type', width: 60, hidden: true, dataIndex: 'referenceTwo.type.name', filter: {type: 'combo', url: 'businessCompanyTypeListFind.json', searchFieldName: 'referenceTwoTypeId'}},
		{header: 'Relationship', width: 100, dataIndex: 'name'},

		{header: 'Identification Code Type', width: 100, dataIndex: 'identificationCodeType'},
		{header: 'Identification Code', width: 100, dataIndex: 'identificationCode'},

		{header: 'Description', width: 200, dataIndex: 'description', hidden: true},

		{
			header: 'Private', width: 50, dataIndex: 'privateRelationship', type: 'boolean',
			tooltip: 'Private relationships are excluded from Client facing reports.'
		},
		{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false},
		{
			width: 18, dataIndex: 'referenceTwo.id', renderer: function(url, args) {
				return TCG.renderActionColumn('business', '', 'Open Related Company');
			}
		}
	],
	editor: {
		detailPageClass: 'Clifton.business.company.CompanyRelationshipWindow',
		getDefaultData: function(gridPanel) { // defaults company id for the detail page
			return {
				referenceOne: gridPanel.getCompany()
			};
		}
	},

	getTopToolbarFilters: function(toolbar) {
		const filters = [];

		const menu = new Ext.menu.Menu();
		this.appendIncludeOptions(menu);
		if (menu.items && menu.items.length > 0) {
			toolbar.add({
				text: 'Include',
				iconCls: 'find',
				name: 'includeOptions',
				menu: menu
			});
			toolbar.add('-');
		}
		return filters;
	},

	appendIncludeOptions: function(menu) {
		const gridPanel = this;
		if (TCG.isTrue(this.isIncludeRelatedCompanyCheckbox())) {
			menu.add(
				{
					text: this.getRelatedCompanyCheckboxLabel()
					, xtype: 'menucheckitem'
					, name: 'includeRelatedCompanies'
					, checkHandler: function() {
						gridPanel.reload();
					}
				});
		}
		this.appendAdditionalIncludeOptions(menu);
	},
	appendAdditionalIncludeOptions: function(menu) {
		// OVERRIDE ME TO ADD MORE OPTIONS
	},

	getLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('active', true);
		}

		const params = {companyId: this.getCompanyId()};

		const includeOptions = TCG.getChildByName(this.getTopToolbar(), 'includeOptions');
		if (includeOptions && includeOptions.menu) {
			this.appendIncludeOptionsLoadParams(includeOptions.menu, params);
		}
		return params;
	},


	appendIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
		if (TCG.isTrue(this.isIncludeRelatedCompanyCheckbox())) {
			const inclRelated = TCG.getChildByName(includeOptionsMenu, 'includeRelatedCompanies').checked;

			if (inclRelated) {
				delete params.companyId;
				params.companyIdOrRelatedCompany = this.getCompanyId();
			}
		}
		this.appendAdditionalIncludeOptionsLoadParams(includeOptionsMenu, params);
	},

	appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
		// OVERRIDE ME IF ADDITIONAL OPTIONS ARE ADDED TO MENU
	},

	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					let id = row.json.referenceTwo.id;
					const gridPanel = grid.ownerGridPanel;
					const cId = gridPanel.getCompany().id;
					if (cId === id) {
						id = row.json.referenceOne.id;
					}
					gridPanel.editor.openDetailPage('Clifton.business.company.CompanyWindow', gridPanel, id, row);
				}
			}
		}
	}
});
Ext.reg('business-companyRelationshipGrid', Clifton.business.CompanyRelationshipGrid);


Clifton.business.company.BloombergIdentifier = Ext.extend(Ext.form.CompositeField, {
	fieldLabel: 'Bloomberg Company ID',
	qtip: 'Bloomberg Company Identifier that uniquely identifies each company on Bloomberg and can be used to link our companies to Bloomberg data.',
	items: [
		{xtype: 'textfield', name: 'bloombergCompanyIdentifier', flex: 1, height: 22},
		{
			xtype: 'button', iconCls: 'bloomberg', width: 25, tooltip: 'Click to load values from Bloomberg for this Business Company',
			handler: function(btn) {
				return btn.findParentByType('compositefield').lookupBloombergData();
			}
		}
	],
	populateCompanyData: function(record) {
		const compositefield = this;
		const fp = TCG.getParentFormPanel(compositefield);
		try {
			const businessCompany = record.businessCompany;
			fp.getForm().trackResetOnLoad = false;
			compositefield.clearFormValues(fp.getForm());
			fp.getForm().setValues(businessCompany, false);
			compositefield.populateBicHierarchy(businessCompany);
			fp.setDynamicFieldsValues();
			compositefield.showValidationMessages(record);
		}
		finally {
			fp.getForm().trackResetOnLoad = true;
		}
	},
	clearFormValues: function(form) {
		form.items.each(function(f) {
			if (f.isFormField && f.hasOwnProperty('name') && f.name !== 'systemHierarchyList') {
				if (f.hasOwnProperty('xtype') && f.xtype === 'combo') {
					f.clearValue();
					this.lastSelectionText = undefined;
				}
				else {
					f.setValue();
				}
			}
		});
	},
	populateBicHierarchy: function(record) {
		const fp = TCG.getParentFormPanel(this);
		const hierarchies = TCG.getChildrenByClass(fp, Clifton.system.hierarchy.HierarchyCombo);
		TCG.each(hierarchies, function(h) {
			if (record.integrationBicsIndustrySubgroup && h.name === 'systemHierarchyList' && h.tableName === 'BusinessCompany' && h.hierarchyCategoryName === 'BICS Hierarchy') {
				h.setValue(record.integrationBicsIndustrySubgroup);
			}
		});
	},
	showValidationMessages: function(record) {
		if (record.hasOwnProperty('validationList') && record.validationList.hasOwnProperty('length') && record.validationList.length > 0) {
			const message = record.validationList.join('<br>');
			TCG.showInfo(message, 'Bloomberg Integration Data Validations');
		}
	},
	lookupBloombergData: function() {
		const fp = TCG.getParentFormPanel(this);
		const w = fp.getWindow();
		const businessCompanyId = w.getMainFormId();
		if (!businessCompanyId || businessCompanyId === '' || fp.getForm().isDirty()) {
			TCG.showInfo('The Business Company must be saved.', 'Bloomberg Company Lookup');
		}
		else {
			const identifier = fp.getForm().findField('bloombergCompanyIdentifier').getValue();
			if (identifier && identifier !== '') {
				const compositefield = this;
				const params = {
					businessCompanyId: w.getMainFormId()
				};
				const loader = new TCG.data.JsonLoader({
					params: params,
					waitTarget: fp,
					waitMsg: 'Loading fields...',
					onLoad: function(record, conf) {
						compositefield.populateCompanyData(record);
					}
				});
				loader.load('businessCompanyFromIntegrationBusinessCompany.json');
			}
			else {
				TCG.showInfo('Please enter a \'Bloomberg Company ID\'.', 'Bloomberg Company Lookup');
			}
		}
	}
});
Ext.reg('business-company-bloomberg-identifier', Clifton.business.company.BloombergIdentifier);


/**
 * Returns the "Tranche Count" custom column value from the specified BusinessServiceProcessingType.
 */
Clifton.business.service.getTrancheCountFromBusinessServiceProcessingType = function(businessServiceProcessingTypeId) {
	return Clifton.system.GetCustomColumnValue(businessServiceProcessingTypeId, 'BusinessServiceProcessingType', 'Business Service Processing Type Custom Fields', 'Tranche Count', false);
};


/**
 * Returns the "Cycle Duration Weeks" custom column value from the specified BusinessServiceProcessingType.
 */
Clifton.business.service.getCycleDurationWeeksFromBusinessServiceProcessingType = function(businessServiceProcessingTypeId) {
	return Clifton.system.GetCustomColumnValue(businessServiceProcessingTypeId, 'BusinessServiceProcessingType', 'Business Service Processing Type Custom Fields', 'Cycle Duration Weeks', false);
};


/**
 * Returns the "Trade Option Combination Type" custom column value from the specified BusinessServiceProcessingType.
 */
Clifton.business.service.getOptionCombinationTypeFromBusinessServiceProcessingType = function(businessServiceProcessingTypeId) {
	return Clifton.system.GetCustomColumnValue(businessServiceProcessingTypeId, 'BusinessServiceProcessingType', 'Business Service Processing Type Custom Fields', 'Option Combination Type', false);
};
