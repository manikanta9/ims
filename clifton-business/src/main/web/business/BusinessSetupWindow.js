Clifton.business.BusinessSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'businessSetupWindow',
	title: 'Company/Contact Setup',
	iconCls: 'group',
	width: 1500,
	height: 700,


	windowOnShow: function(w) {
		this.addAdditionalTabs(w);
	},

	addAdditionalTabs: function(w) {
		const arr = Clifton.business.BusinessSetupWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			arr[i].addAdditionalTabs(w);
		}
	},
	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Companies',
				items: [{
					name: 'businessCompanyListFind',
					xtype: 'gridpanel',
					instructions: 'The following companies are in the system.  Each company is tied to a type and address.  For companies with multiple locations or types, parent companies can be used and each child company can be tied to a specific address or type.',
					reloadOnRender: false,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},

						{header: 'Bloomberg Company ID', width: 60, dataIndex: 'bloombergCompanyIdentifier', type: 'int', doNotFormat: true, useNull: true},

						{header: 'Relationship to Parent', width: 80, dataIndex: 'parentRelationshipType.label', filter: {type: 'combo', loadAll: true, searchFieldName: 'parentRelationshipTypeId', url: 'businessCompanyParentRelationshipTypeListFind.json'}, hidden: true},

						{header: 'Is Ultimate Parent', width: 70, dataIndex: 'ultimateParent', type: 'boolean', hidden: true},
						{header: 'Is Private Company', width: 70, dataIndex: 'privateCompany', type: 'boolean', hidden: true},
						{header: 'Acquired By Parent', width: 70, dataIndex: 'acquiredByParent', type: 'boolean', hidden: true},

						{header: 'Parent Company', width: 70, dataIndex: 'parent.nameWithType', filter: {type: 'combo', searchFieldName: 'parentId', url: 'businessCompanyListFind.json'}, hidden: true},
						{header: 'Obligor Company', width: 70, dataIndex: 'obligorBusinessCompany.nameWithType', filter: {type: 'combo', searchFieldName: 'obligorBusinessCompanyId', url: 'businessCompanyListFind.json'}, hidden: true},
						{header: 'Parent Obligor Company', width: 100, dataIndex: 'parentObligorBusinessCompany.nameWithType', filter: {type: 'combo', searchFieldName: 'parentObligorBusinessCompanyId', url: 'businessCompanyListFind.json'}, hidden: true},

						{header: 'Company Name', width: 100, dataIndex: 'name'},  // Uses Toolbar Filter
						{header: 'Company Legal Name', width: 100, dataIndex: 'companyLegalName', hidden: true},
						{header: 'Alternate Company Name', width: 100, dataIndex: 'alternateCompanyName', hidden: true},
						{header: 'Expanded Name', width: 200, dataIndex: 'nameExpanded', hidden: true, filter: {searchFieldName: 'searchPattern'}},
						{header: 'Company Type', width: 70, dataIndex: 'type.label', filter: {type: 'combo', searchFieldName: 'companyTypeId', url: 'businessCompanyTypeListFind.json'}},
						{header: 'Alias', width: 100, dataIndex: 'alias', filter: {searchFieldName: 'alias'}},
						{header: 'Abbreviation', width: 40, dataIndex: 'abbreviation', filter: {searchFieldName: 'abbreviation'}},
						{header: 'FED Mnemonic', width: 50, dataIndex: 'fedMnemonic', hidden: true},
						{header: 'Business Identifier Code', width: 75, dataIndex: 'businessIdentifierCode', hidden: true},
						{header: 'DTC Number', width: 50, dataIndex: 'dtcNumber', hidden: true},
						{header: 'Description', width: 150, dataIndex: 'description'},

						{header: 'Legal Entity Identifier', width: 75, dataIndex: 'legalEntityIdentifier', hidden: true},
						{header: 'LEI Entity Status', width: 60, dataIndex: 'leiEntityStatus', hidden: true},
						{header: 'LEI Entity Start Date', width: 75, dataIndex: 'leiEntityStartDate', hidden: true},
						{header: 'LEI Entity End Date', width: 75, dataIndex: 'leiEntityEndDate', hidden: true},

						{header: 'Company Corporate Ticker', width: 80, dataIndex: 'companyCorporateTicker', hidden: true},
						{header: 'Ultimate Parent Equity Ticker', width: 80, dataIndex: 'ultimateParentEquityTicker', hidden: true},

						{header: 'State Of Incorporation', width: 75, dataIndex: 'stateOfIncorporation', hidden: true},
						{header: 'Country Of Incorporation', width: 75, dataIndex: 'countryOfIncorporation', hidden: true},
						{header: 'Country Of Risk', width: 75, dataIndex: 'countryOfRisk', hidden: true},

						{header: 'Phone', width: 100, dataIndex: 'phoneNumber', hidden: true},
						{header: 'Fax', width: 100, dataIndex: 'faxNumber', hidden: true},
						{header: 'E-Fax', width: 100, dataIndex: 'efaxNumber', hidden: true},
						{header: 'Email', width: 100, dataIndex: 'emailAddress', renderer: TCG.renderEmailAddress, hidden: true},
						{header: 'Web Address', width: 100, dataIndex: 'webAddress', hidden: true},

						{header: 'Domicile Address 1', width: 75, dataIndex: 'addressLine1', hidden: true},
						{header: 'Domicile Address 2', width: 75, dataIndex: 'addressLine2', hidden: true},
						{header: 'Domicile Address 3', width: 75, dataIndex: 'addressLine3', hidden: true},
						{header: 'Domicile City/State', width: 75, dataIndex: 'cityState'},
						{header: 'Domicile City', width: 75, dataIndex: 'city', hidden: true},
						{header: 'Domicile State', width: 75, dataIndex: 'state', hidden: true},
						{header: 'Domicile Postal Code', width: 75, dataIndex: 'postalCode', hidden: true},
						{header: 'Domicile Country', width: 75, dataIndex: 'country', hidden: true},

						{header: 'Registration Address 1', width: 80, dataIndex: 'registrationAddressLine1', hidden: true},
						{header: 'Registration Address 2', width: 80, dataIndex: 'registrationAddressLine2', hidden: true},
						{header: 'Registration Address 3', width: 80, dataIndex: 'registrationAddressLine3', hidden: true},
						{header: 'Registration City', width: 75, dataIndex: 'registrationCity', hidden: true},
						{header: 'Registration State', width: 75, dataIndex: 'registrationState', hidden: true},
						{header: 'Registration Postal Code', width: 80, dataIndex: 'registrationPostalCode', hidden: true},
						{header: 'Registration Country', width: 75, dataIndex: 'registrationCountry', hidden: true}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Company Name', width: 150, xtype: 'toolbar-textfield', name: 'companyName'},
							{fieldLabel: 'Company Type', width: 150, xtype: 'toolbar-combo', url: 'businessCompanyTypeListFind.json', linkedFilter: 'type.label'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Business Company Tags'}
						];
					},
					getLoadParams: function(firstLoad) {
						const companyName = TCG.getChildByName(this.getTopToolbar(), 'companyName');
						const params = {};
						if (TCG.isNotBlank(companyName.getValue())) {
							params.searchPattern = companyName.getValue();
						}
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.categoryName = 'Business Company Tags';
							params.categoryHierarchyId = tag.getValue();
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.business.company.CompanyWindow',
						deleteURL: 'businessCompanyDelete.json'
					},

					addToolbarButtons: function(t, gridPanel) {
						t.add({
							text: 'Move/Copy Relationships',
							tooltip: 'Move/Copy Company Relationships from one Company to another.',
							iconCls: 'run',
							handler: function() {
								TCG.createComponent('Clifton.business.company.CompanyCopyRelationshipsWindow', {
									openerCt: gridPanel
								});
							}
						});
						t.add('-');
					}
				}]
			},


			{
				title: 'Company Relationships',
				items: [{
					name: 'businessCompanyRelationshipListFind',
					xtype: 'gridpanel',
					instructions: 'The following company - company relationships have been defined.  Select a company in the toolbar to search for their relationships in either direction.',
					reloadOnRender: false,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'First Company Type', width: 100, dataIndex: 'referenceOne.type.name', filter: {searchFieldName: 'referenceOneTypeId', type: 'combo', url: 'businessCompanyTypeListFind.json'}},
						{header: 'First Company', width: 150, dataIndex: 'referenceOne.label', filter: {searchFieldName: 'referenceOneName'}},
						{header: 'Second Company Type', width: 100, dataIndex: 'referenceTwo.type.name', filter: {searchFieldName: 'referenceTwoTypeId', type: 'combo', url: 'businessCompanyTypeListFind.json'}},
						{header: 'Second Company', width: 150, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'referenceTwoName'}},
						{header: 'Relationship', width: 100, dataIndex: 'name'},

						{header: 'Identification Code Type', width: 100, dataIndex: 'identificationCodeType'},
						{header: 'Identification Code', width: 100, dataIndex: 'identificationCode'},

						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},

						{
							header: 'Private', width: 50, dataIndex: 'privateRelationship', type: 'boolean',
							tooltip: 'Private relationships are excluded from Client facing reports.'
						},

						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Company', xtype: 'combo', name: 'companyId', width: 180, url: 'businessCompanyListFind.json', displayField: 'label',
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					getLoadParams: function(firstLoad) {
						const params = {};
						const t = this.getTopToolbar();
						const cn = TCG.getChildByName(t, 'companyId');
						if (TCG.isNotEquals(cn.getValue(), '')) {
							params.contactId = cn.getValue();
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.business.company.CompanyRelationshipWindow'
					}
				}]
			},


			{
				title: 'Company Types',
				items: [{
					name: 'businessCompanyTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Business Company Types allow classification of companies and flag companies as internal (our company).',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 80, dataIndex: 'name'},
						{header: 'Description', width: 125, dataIndex: 'description', hidden: true},
						{header: 'Modify Condition', width: 175, dataIndex: 'entityModifyCondition.name', hidden: true},
						{header: 'Our Company', width: 50, dataIndex: 'ourCompany', type: 'boolean'},
						{header: 'Money Manager', width: 50, dataIndex: 'moneyManager', type: 'boolean'},
						{header: 'Issuing Company', width: 50, dataIndex: 'issuer', type: 'boolean'},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'},
						{header: 'Require Parent', width: 50, dataIndex: 'parentCompanyRequired', type: 'boolean'},
						{header: 'Trading Allowed', width: 50, dataIndex: 'tradingAllowed', type: 'boolean'},
						{header: 'Contracts Allowed', width: 55, dataIndex: 'contractAllowed', type: 'boolean', tooltip: 'Indicates that the company type allows trading. Usually Broker and Custodian.'},
						{header: 'Require Abbreviation', width: 60, dataIndex: 'abbreviationRequired', type: 'boolean'},
						{
							header: 'Globally Unique', width: 50, dataIndex: 'globallyUnique', type: 'boolean',
							tooltip: 'When true, the combination of parent/name must be unique across all companies, otherwise must be unique by company type.'
						}
					],
					editor: {
						detailPageClass: 'Clifton.business.company.CompanyTypeWindow'
					}
				}]
			},


			{
				title: 'Delivery Instructions',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentInstructionDeliveryListFind',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Instruction Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{header: 'Bank', width: 70, dataIndex: 'deliveryCompany.name', filter: {type: 'combo', searchFieldName: 'deliveryCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Account Number', width: 70, dataIndex: 'deliveryAccountNumber'},
						{header: 'Account Name', width: 90, dataIndex: 'deliveryAccountName'},
						{header: 'ABA', width: 40, dataIndex: 'deliveryABA'},
						{header: 'SWIFT Code', width: 40, dataIndex: 'deliverySwiftCode'},
						{header: 'Further Credit', width: 50, dataIndex: 'deliveryForFurtherCredit', hidden: true},
						{header: 'Separate Collateral Wire Transfer', width: 50, dataIndex: 'separateWireForCollateral', type: 'boolean', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instruction.delivery.InstructionDeliveryWindow'
					}
				}]
			},


			{
				title: 'Company Platforms',
				items: [{
					name: 'businessCompanyPlatformListFind',
					xtype: 'gridpanel',
					instructions: 'A company platform is a Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies. Each platform is unique to a particular company.  A holding account issued by a company can also be designated to a platform associated with that company.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Company Name', width: 130, dataIndex: 'businessCompany.name', filter: {type: 'combo', searchFieldName: 'businessCompanyId', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false'}},
						{header: 'Company Type', width: 50, dataIndex: 'businessCompany.type.label', filter: {type: 'combo', searchFieldName: 'businessCompanyTypeId', url: 'businessCompanyTypeListFind.json'}},
						{header: 'Platform Name', width: 130, dataIndex: 'name'},
						{header: 'Platform Description', width: 200, dataIndex: 'description'},
						{header: 'Start Date', width: 40, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 40, dataIndex: 'endDate', hidden: true},
						{header: 'Active', width: 30, type: 'boolean', dataIndex: 'active'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
					},
					editor: {
						detailPageClass: 'Clifton.business.company.CompanyPlatformWindow'
					}
				}]
			}
		]
	}]
});

