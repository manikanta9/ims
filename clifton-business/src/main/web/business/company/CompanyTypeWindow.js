Clifton.business.company.CompanyTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company Type',
	height: 530,
	iconCls: 'business',

	items: [{
		xtype: 'formpanel',
		instructions: 'Business Company Types are used for classification purposes to group companies into categories and flag them as internal or not.',
		url: 'businessCompanyType.json',
		items: [
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{xtype: 'system-entity-modify-condition-combo'},
			{boxLabel: 'Money Manager (track account assets in order to provide client services)', name: 'moneyManager', xtype: 'checkbox'},
			{boxLabel: 'Issuing Company (bank/broker company that can issue accounts)', name: 'issuer', xtype: 'checkbox'},
			{boxLabel: 'Our Company (Parametric Clifton)', name: 'ourCompany', xtype: 'checkbox', disabled: true},
			{boxLabel: 'System Defined (company name and type cannot be changed)', name: 'systemDefined', xtype: 'checkbox', disabled: true},
			{boxLabel: 'Companies of this type require a parent company selected', name: 'parentCompanyRequired', xtype: 'checkbox'},
			{boxLabel: 'Companies of this type require abbreviations', name: 'abbreviationRequired', xtype: 'checkbox'},
			{boxLabel: 'Companies of this type allow trading', name: 'tradingAllowed', xtype: 'checkbox'},
			{boxLabel: 'Companies of this type support contracts', name: 'contractAllowed', xtype: 'checkbox'},
			{
				boxLabel: 'Companies of this type are required to be globally unique', name: 'globallyUnique', xtype: 'checkbox',
				qtip: 'When globally unique, the parent/name combination must be unique across ALL companies, regardless of type.  When false, they must only be unique within its own type.'
			}
		]
	}]
});
