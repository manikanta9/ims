Clifton.business.company.CompanyPlatformWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company Platform',
	width: 900,
	height: 500,
	iconCls: 'business',

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.business.company.CompanyPlatformAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Platform',
				items: [{
					xtype: 'formpanel',
					instructions: 'A company platform is a Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies. Each platform is unique to a particular company.  A holding account issued by a company can also be designated to a platform associated with that company.',
					url: 'businessCompanyPlatform.json',
					labelWidth: 160,
					items: [
						{fieldLabel: 'Company', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true},
						{fieldLabel: 'Platform Name', name: 'name'},
						{fieldLabel: 'Platform Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
					]
				}]
			}
		]
	}]
});
