TCG.use('Clifton.system.common.AddressFormFragment');

Clifton.business.company.BasicCompanyWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company',
	height: 700,
	width: 1000,
	iconCls: 'business',

	windowOnShow: function(w) {
		if (w.defaultData && w.defaultData.type) {
			const tabs = w.items.get(0).items.get(0);
			if (w.defaultData.type.issuer) {
				tabs.add(this.creditRatingsTab);
				if (TCG.isFalse(w.defaultData.type.ourCompany)) {
					tabs.add(this.platformsTab);
				}
			}
		}
		this.addAdditionalTabs(w);
	},

	addAdditionalTabs: function(w) {
		const arr = Clifton.business.company.BasicCompanyWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			arr[i].addAdditionalTabs(w);
		}
	},


	creditRatingsTab: {
		title: 'Credit Ratings',
		items: [{
			xtype: 'compliance-credit-rating-value-grid',
			forIssuer: true,
			getLoadParams: function(firstLoad) {
				const params = this.getTopToolbarInitialLoadParams(firstLoad);
				params.issuerCompanyId = this.getWindow().getMainFormId();
				return params;
			},
			getToolbarAddButtonElements: function() {
				return [{
					text: 'Import Latest Ratings',
					tooltip: 'Import latest credit ratings from Bloomberg.<br><br><b>Importing issuer credit ratings using the company symbol is not supported. Issuer credit ratings may only be imported by security symbol.</b>',
					iconCls: 'run',
					disabled: true
				}, '-'
				].concat(this.constructor.prototype.getToolbarAddButtonElements.apply(this, arguments));
			},
			getNewItemDefaultData: function(editor, row, detailPageClass, itemText) {
				return Ext.apply({},
					this.constructor.prototype.getNewItemDefaultData.apply(this, arguments),
					{issuerCompany: this.getWindow().getMainForm().formValues});
			}
		}]
	},


	platformsTab: {
		title: 'Platforms',
		items: [{
			xtype: 'gridpanel',
			name: 'businessCompanyPlatformListFind',
			instructions: 'A company platform is a Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies. Each platform is unique to a particular company.  A holding account issued by a company can also be designated to a platform associated with that company.',
			getLoadParams: function() {
				return {'businessCompanyId': this.getWindow().getMainFormId()};
			},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Platform Name', width: 100, dataIndex: 'name'},
				{header: 'Platform Description', width: 200, dataIndex: 'description'},
				{header: 'Start Date', width: 50, dataIndex: 'startDate', hidden: true},
				{header: 'End Date', width: 50, dataIndex: 'endDate', hidden: true},
				{header: 'Active', width: 50, type: 'boolean', dataIndex: 'active'}
			],
			editor: {
				detailPageClass: 'Clifton.business.company.CompanyPlatformWindow'
			}
		}]
	},

	items: [{
		xtype: 'formwithtabs-custom-fields',
		columnGroupName: 'Company Custom Fields',
		dynamicFieldFormFragment: 'companyCustomFields',
		url: 'businessCompany.json',
		listeners: {
			afterload: function(panel) {
				// If company type is system defined disable name & type fields
				const f = panel.getForm();
				const sd = TCG.getValue('type.systemDefined', f.formValues);
				if (TCG.isTrue(sd)) {
					// Disable the name field.
					const field = f.findField('name');
					field.setDisabled(true);
					// Disable and hide the combo boxes for type and parent.
					let matches = panel.findByTypeAndName(panel, 'combo', ['parent.nameWithType', 'type.label']);
					matches.forEach(function(fld) {
						fld.setVisible(false);
						fld.setDisabled(true);
					});
					// Duplicate link fields are initially disabled and hidden to enable record add and non system company edits.
					// show and enable the link fields (combo boxes are hidden and disabled.)
					matches = panel.findByTypeAndName(panel, 'linkfield', ['parent.nameWithType', 'type.label']);
					matches.forEach(function(fld) {
						fld.show();
						fld.setDisabled(false);
					});
				}
			}
		},
		findByTypeAndName: function(parent, xtype, name) {
			return parent.findBy(function(c) {
				return (c && c.hasOwnProperty('xtype') && TCG.isEquals(xtype, c.xtype) && c.hasOwnProperty('name') && name.indexOf(c.name) > -1);
			});
		},
		items: [{
			xtype: 'tabpanel',
			requiredFormIndex: [0, 1],
			items: [
				{
					title: 'Company',
					items: {
						xtype: 'formfragment',
						instructions: 'Each company is tied to a type and address.  For companies with multiple locations or types, parent companies can be used and each child company can be tied to a specific address or type.',
						labelWidth: 165,
						items: [
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [{
									rows: [
										{fieldLabel: 'Company Type', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessCompanyTypeListFind.json?systemDefined=false', detailPageClass: 'Clifton.business.company.CompanyTypeWindow', disableAddNewItem: true},
										{fieldLabel: 'Company Type', name: 'type.label', hiddenName: 'type.id', displayField: 'label', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyTypeWindow', submitDetailField: false, disabled: true, hidden: true}
									]
								}, {
									rows: [
										{xtype: 'business-company-bloomberg-identifier', anchor: '-19'}
									]
								}]
							},

							{fieldLabel: 'Expanded Name', name: 'nameExpanded', disabled: true, qtip: 'Full corporate structure all the way up to the ultimate parent.'},
							{fieldLabel: 'Parent Company', name: 'parent.nameWithType', hiddenName: 'parent.id', displayField: 'nameExpandedWithType', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
							{
								fieldLabel: 'Parent Company',
								name: 'parent.nameWithType',
								xtype: 'linkfield',
								detailPageClass: 'Clifton.business.company.CompanyWindow',
								getDetailIdFieldValue: function(formPanel) {
									const f = formPanel.getForm().findField('parent.id', true);
									return f ? f.getValue() : null;
								},
								submitDetailField: false,
								disabled: true,
								hidden: true
							},
							{fieldLabel: 'Relationship to Parent', name: 'parentRelationshipType.label', hiddenName: 'parentRelationshipType.id', displayField: 'label', xtype: 'combo', loadAll: true, url: 'businessCompanyParentRelationshipTypeListFind.json'},
							{fieldLabel: 'Obligor Company', name: 'obligorBusinessCompany.nameWithType', hiddenName: 'obligorBusinessCompany.id', displayField: 'nameExpandedWithType', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', qtip: 'Bloomberg number that identifies the obligor, which is the immediate resource of payment for a company.'},
							{xtype: 'hidden', name: 'parentObligorBusinessCompany.id', submitValue: false},
							{
								fieldLabel: 'Parent Obligor Company',
								name: 'parentObligorBusinessCompany.nameWithType',
								xtype: 'linkfield',
								detailPageClass: 'Clifton.business.company.CompanyWindow',
								submitValue: false,
								getDetailIdFieldValue: function(formPanel) {
									const f = formPanel.getForm().findField('parentObligorBusinessCompany.id', true);
									return f ? f.getValue() : null;
								}
							},
							{fieldLabel: 'Company Name', name: 'name', qtip: 'The official long name of the company, index, or other entity.'},
							{fieldLabel: 'Company Legal Name', name: 'companyLegalName', qtip: 'Name of the company according to its official legal filings. May not be the same as the brand name or "doing business as" name.'},
							{fieldLabel: 'Alternate Company Name', name: 'alternateCompanyName', qtip: 'The Alternate name a company may use, including doing business as (dba) name, also known as (aka) name, name that could be associated with an entity, and common spelling variations typically as a result of translations.'},
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [{
									rows: [
										{fieldLabel: 'Alias', name: 'alias', qtip: '"Friendly" company name instead of legal name. For example, "GS - REDI Stocks Only" for "Goldman Sachs Execution & Clearing".'},
										{fieldLabel: 'Abbreviation', name: 'abbreviation', qtip: 'Abbreviations are used to standardize account names, security names (Swaps), etc.', maxLength: 4}
									]
								}, {
									rows: [
										{fieldLabel: 'Company Corporate Ticker', name: 'companyCorporateTicker', qtip: '\'Corp\' ticker (fixed income) for publicly traded companies.', anchor: '-19'},
										{fieldLabel: 'Ultimate Parent Equity Ticker', name: 'ultimateParentEquityTicker', qtip: '\'Equity\' Ticker and exchange of the ultimate parent company (highest level in organization chain).', anchor: '-19'}
									]
								}]
							},
							{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 55},

							{boxLabel: 'Ultimate Parent (top most) for all child companies/subsidiaries', name: 'ultimateParent', xtype: 'checkbox'},
							{boxLabel: 'Private Company (not traded on a public stock exchange)', name: 'privateCompany', xtype: 'checkbox'},
							{boxLabel: 'Acquired By Parent Company', name: 'acquiredByParent', xtype: 'checkbox'},

							{xtype: 'label', html: '<hr />'},
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [{
									rows: [
										{fieldLabel: 'Legal Entity Identifier (LEI)', name: 'legalEntityIdentifier', qtip: 'A 20-character, alpha-numeric code that uniquely identifies legally distinct entities that engage in financial transactions.'},
										{fieldLabel: 'LEI Entity Start Date', name: 'leiEntityStartDate', xtype: 'datefield', allowBlank: true, qtip: 'Date on which the legal entity identifier (LEI) was first assigned by the registration authority.'}
									]
								}, {
									rows: [
										{fieldLabel: 'LEI Entity Status', name: 'leiEntityStatus', qtip: 'Current status of the entity for the legal entity identifier (LEI), either \'ACTIVE\' or \'INACTIVE\'.', anchor: '-19'},
										{fieldLabel: 'LEI Entity End Date', name: 'leiEntityEndDate', xtype: 'datefield', allowBlank: true, qtip: 'Date on which the legal entity identifier (LEI) entered a disabled status.', anchor: '-19'}
									]
								}]
							},

							{xtype: 'label', html: '<hr />'},
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [{
									rows: [
										{
											fieldLabel: 'State Of Incorporation', name: 'stateOfIncorporation', xtype: 'combo', valueField: 'value', displayField: 'value', tooltipField: 'text', url: 'systemListItemListFind.json', qtip: 'The State Code of the company\'s state of incorporation for Australia, Canada, Japan, and the United States.',
											beforequery: function(queryEvent) {
												const f = queryEvent.combo.getParentForm().getWindow().getMainForm();
												const country = f.findField('countryOfIncorporation').value;
												if (country === 'CAN' || country === 'Canada' || country === 'CA') {
													queryEvent.combo.store.baseParams = {listName: 'Canadian Provinces'};
												}
												else {
													queryEvent.combo.store.baseParams = {listName: 'States'};
												}
											},
											listeners: {
												select: function(field, value) {
													if (TCG.isNotBlank(value)) {
														const f = field.getParentForm().getWindow().getMainForm();
														const country = f.findField('countryOfIncorporation');
														if (TCG.isBlank(country.getValue())) {
															country.setValue('US');
															country.setRawValue('United States');
														}
													}
												}
											}
										},
										{fieldLabel: 'Country Of Incorporation', name: 'countryOfIncorporation', xtype: 'combo', displayField: 'text', valueField: 'value', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries', qtip: 'Specifies the ISO (International Organization for Standardization) country code of where a company is incorporated. Supranational (SNAT) will be returned for entities formed by multiple governments and without a single country of incorporation. Multinational (MULT) will be returned for companies incorporated in multiple jurisdictions (countries). Multinational companies that are physically incorporated in a single country will return the ISO country code of that country. Multi-issuers will return \'MULT\' when the individual entities that make up the Multi-issuer are incorporated in different countries.'},
										{fieldLabel: 'Country Of Risk', name: 'countryOfRisk', xtype: 'combo', displayField: 'text', valueField: 'value', tooltipField: 'tooltip', url: 'systemListItemListFind.json?listName=Countries', qtip: 'International Organization for Standardization (ISO) country code of the issuer\'s country of risk. Methodology consists of four factors listed in order of importance: management location, country of primary listing, country of revenue and reporting currency of the issuer. Management location is defined by country of domicile unless location of such key players as Chief Executive Officer (CEO), Chief Financial Officer (CFO), Chief Operating Officer (COO), and/or General Counsel is proven to be otherwise.'}
									]
								}, {
									rows: [
										{fieldLabel: 'Business Identifier Code', name: 'businessIdentifierCode', qtip: 'A BIC is composed of a 4-character bank code, 2-character country code 2-character location code and optional 3 character branch code.  It is used for addressing messages, routing business transactions, and identifying business parties.', anchor: '-19'},
										{fieldLabel: 'DTC Number', name: 'dtcNumber', qtip: 'The DTC number is a number that is issued in connection with Depository Trust Company for settlement services.', anchor: '-19'},
										{fieldLabel: 'FED Mnemonic', name: 'fedMnemonic', qtip: 'A company\'s standardized account at the Fed.', anchor: '-19'}
									]
								}]
							},
							{fieldLabel: 'BICS Industry Subgroup', xtype: 'system-hierarchy-combo', tableName: 'BusinessCompany', hierarchyCategoryName: 'BICS Hierarchy', qtip: 'Bloomberg Industry Classification System (BICS)'},
							{fieldLabel: 'GICS Sub-industry', xtype: 'system-hierarchy-combo', tableName: 'BusinessCompany', hierarchyCategoryName: 'GICS Hierarchy', qtip: 'Global Industry Classification Standard (GICS)'},

							{xtype: 'label', html: '<hr />'},
							{
								xtype: 'formfragment',
								labelWidth: 165,
								bodyStyle: 'padding: 0',
								defaults: {anchor: '-0'},
								frame: false,
								name: 'companyCustomFields',
								items: []
							},
							{
								xtype: 'system-tags-fieldset',
								title: 'Tags',
								tableName: 'BusinessCompany',
								hierarchyCategoryName: 'Business Company Tags',
								labelWidth: 150
							}
						]
					}
				},


				{
					title: 'Address',
					items: {
						xtype: 'address-formfragment',
						useFieldSet: true,
						fieldSetTitle: 'Domicile Address',
						itemsBefore: [{
							xtype: 'fieldset',
							collapsible: false,
							title: 'Contact Info',
							items: [
								{fieldLabel: 'Phone', name: 'phoneNumber'},
								{fieldLabel: 'Fax', name: 'faxNumber'},
								{fieldLabel: 'E-Fax', name: 'efaxNumber'},
								{fieldLabel: 'Email', name: 'emailAddress', vtype: 'email'},
								{fieldLabel: 'Web Address', name: 'webAddress'}
							]
						}],
						itemsAfter: [{
							xtype: 'address-formfragment',
							useFieldSet: true,
							frame: false,
							fieldSetTitle: 'Registration Address',
							addressFieldsPrefix: 'registration'
						}]
					}
				},


				{
					title: 'Subsidiaries',
					items: [{
						xtype: 'treegrid',
						name: 'businessCompanyListFind',
						instructions: 'The following companies are subsidiaries of this company.',
						appendStandardColumns: false, // to avoid UI glitch
						columns: [
							{header: 'Company Name', width: 450, dataIndex: 'nameWithType'},
							{header: 'Bloomberg Company ID', width: 80, dataIndex: 'bloombergCompanyIdentifier', type: 'int', doNotFormat: true, useNull: true},
							{header: 'Domicile City/State', width: 110, dataIndex: 'cityState'},
							{header: 'Domicile Country', width: 75, dataIndex: 'country'},
							{header: 'Ultimate Parent', width: 75, dataIndex: 'ultimateParent', type: 'boolean'},
							{header: 'Private Company', width: 75, dataIndex: 'privateCompany', type: 'boolean'},
							{header: 'Acquired By Parent', width: 75, dataIndex: 'acquiredByParent', type: 'boolean'},

							{header: 'Business Identifier Code', width: 75, dataIndex: 'businessIdentifierCode', hidden: true},
							{header: 'DTC Number', width: 50, dataIndex: 'dtcNumber', hidden: true},
							{header: 'FED Mnemonic', width: 150, dataIndex: 'fedMnemonic', hidden: true},

							{header: 'Company Legal Name', width: 200, dataIndex: 'companyLegalName', hidden: true},
							{header: 'Alternate Company Name', width: 100, dataIndex: 'alternateCompanyName', hidden: true}
						],
						editor: {
							detailPageClass: 'Clifton.business.company.CompanyWindow',
							drillDownOnly: true,
							getDefaultData: function(treePanel) {
								return {
									parent: treePanel.getWindow().getMainForm().formValues
								};
							}
						},
						requestedProperties: function() {
							const treePanel = this;
							const loader = treePanel.loader;
							let props = loader.idNode + '|' + loader.textNode + '|' + loader.leafNode + '|' + 'level' + '|' + 'identity';
							for (let i = 0; i < treePanel.columns.length; i++) {
								const c = treePanel.columns[i];
								if (c.dataIndex) {
									if (props.length > 0) {
										props += '|';
									}
									props += c.dataIndex;
								}
							}
							return props;
						},
						pathToRoot: function() {
							const treePanel = this;
							if (treePanel.getWindow().getMainFormId()) {
								const businessCompany = treePanel.getWindow().getMainForm().formValues;
								const path = [];
								path.push(businessCompany.id);
								let parent = (businessCompany.hasOwnProperty('parent')) ? businessCompany.parent : undefined;
								while (parent) {
									path.push(parent.id);
									parent = (parent.hasOwnProperty('parent')) ? parent.parent : undefined;
								}
								return path.reverse();
							}
						},
						expandTree: function() {
							const treePanel = this;
							const selectionPath = treePanel.getRootNode().getPath() + '/' + treePanel.pathToRoot().join('/');
							treePanel.animate = true;
							// expand the tree until 'this' company's node is encountered.
							treePanel.expandPath(selectionPath);
							treePanel.animate = false;
						},
						onRender: function() {
							TCG.tree.TreeGrid.prototype.onRender.apply(this, arguments);

							const treePanel = this;

							// watch for node expansion and select the node for 'this' company.
							treePanel.on('expandnode', function(node) {
								const targetId = treePanel.getWindow().getMainFormId();
								if (TCG.isEquals(node.id, targetId)) {
									node.select();
								}
							});

							// watch for node load, expand tree when triggered on root.
							treePanel.loader.on('load', function(This, node, response) {
								const tree = treePanel;
								if (node.isRoot) {
									tree.expandTree();
								}
							});

							// on first load: remove parentId, include id for root node (not necessarily 'this' company)
							// subsequent loads:  remove the id parameter, use only parentId.
							// all loads:  never include both parentId and id.
							treePanel.loader.getParams = function(node) {
								const tree = treePanel;
								let params = TCG.tree.JsonTreeLoader.superclass.getParams.apply(this, arguments);
								if (node.isRoot) {
									const rootPath = tree.pathToRoot();
									params = {
										'id': rootPath[0]
									};
								}
								else {
									delete params[this.idNode];
								}
								if (!params.hasOwnProperty('requestedProperties')) {
									params.requestedProperties = treePanel.requestedProperties();
									params.requestedPropertiesRoot = 'data';
								}
								return params;
							};
						}
					}]
				},


				{
					title: 'Relationships',
					items: [{
						xtype: 'business-companyRelationshipGrid',
						isIncludeRelatedCompanyCheckbox: function() {
							return true;
						},
						getRelatedCompanyCheckboxLabel: function() {
							return 'Include Parent/Subsidiary Relationships';
						}
					}]
				},


				{
					title: 'Notes',
					items: [{
						xtype: 'document-system-note-grid',
						tableName: 'BusinessCompany'
					}]
				}
			]
		}]
	}
	]
});
