Clifton.business.company.CompanyRelationshipWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Company Relationship',
	height: 500,
	iconCls: 'business',
	items: [{
		xtype: 'formpanel',
		instructions: 'A Company Relationship associates a company with another company.',
		url: 'businessCompanyRelationship.json',
		labelWidth: 150,

		items: [
			{fieldLabel: 'Main Company', name: 'referenceOne.nameWithType', hiddenName: 'referenceOne.id', displayField: 'nameWithType', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{fieldLabel: 'Relationship Type', name: 'name', xtype: 'system-list-combo', listName: 'Company Relationship Types'},
			{fieldLabel: 'Related Company', name: 'referenceTwo.nameWithType', hiddenName: 'referenceTwo.id', displayField: 'nameWithType', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{boxLabel: 'Private Relationship (excluded from Client facing reports)', name: 'privateRelationship', xtype: 'checkbox'},

			{fieldLabel: 'Identification Code Type', xtype: 'system-list-combo', name: 'identificationCodeType', listName: 'Company Identification Code Types'},
			{fieldLabel: 'Identification Code', name: 'identificationCode', requiredFields: ['identificationCodeType'], doNotClearIfRequiredChanges: true},

			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
			{fieldLabel: 'Description', xtype: 'textarea', name: 'description'}
		]
	}]
});
