Clifton.business.company.CompanyCopyRelationshipsWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Company - Move/Copy Relationships',
	iconCls: 'group',
	items: [{
		xtype: 'formpanel',
		instructions: 'Moving/Copying Company relationships allows moving or copying company-contact and company-company relationships from one company to another and optionally deleting the original company.  This can be useful for handling company mergers or cleaning up duplicates in the system.  Deleting the from company is supported only if it is not a system defined company type.',
		getSaveURL: function() {
			return 'businessCompanyRelationshipsCopy.json';
		},
		items: [
			{fieldLabel: 'From Company', name: 'fromCompanyName', hiddenName: 'fromCompanyId', displayField: 'labelExpanded', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json?systemDefined=false', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{fieldLabel: 'To Company', name: 'toCompanyName', hiddenName: 'toCompanyId', displayField: 'labelExpanded', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json?systemDefined=false', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{boxLabel: 'Move Company Relationships (If unchecked will just copy relationships, otherwise will move them to the new company and delete all relationships from the from company)', name: 'moveRelationships', xtype: 'checkbox'},
			{boxLabel: 'DELETE FROM Company', name: 'deleteFrom', xtype: 'checkbox'}
		],
		saveForm: function(closeOnSuccess, forms, form, panel, params) {
			const win = this;
			win.closeOnSuccess = closeOnSuccess;
			win.modifiedFormCount = forms.length;
			// TODO: data binding for partial field updates
			// TODO: concurrent updates validation
			form.trackResetOnLoad = true;
			form.submit(Ext.applyIf({
				url: encodeURI(panel.getSaveURL()),
				params: params,
				waitMsg: 'Saving...',
				success: function(form, action) {
					TCG.showInfo(action.result.result, 'Company Move/Copy Status');
					win.savedSinceOpen = true;
					if (win.closeOnSuccess) {
						win.closeWindow();
					}
				}
			}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
		}
	}]
});
