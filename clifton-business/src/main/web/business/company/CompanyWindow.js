// not the actual window but a window selector based on company type
Clifton.business.company.CompanyWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'businessCompany.json',

	getClassName: function(config, entity) {
		return (entity && Clifton.business.company.CompanyWindowOverrides[entity.type.name]) || 'Clifton.business.company.BasicCompanyWindow';
	}
});
