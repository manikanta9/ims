Clifton.business.service.ServiceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Service',
	iconCls: 'services',
	width: 800,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Service',
				items: [{
					xtype: 'formpanel',
					url: 'businessService.json',
					labelWidth: 160,
					listeners: {
						afterload: function(panel) {
							const w = panel.getWindow();
							const f = panel.getForm();
							const tabs = w.items.get(0);

							if (TCG.getValue('serviceLevelType', f.formValues) === 'SERVICE') {
								tabs.add(w.processingTypesTab);
								tabs.add(w.objectivesTab);
							}
							tabs.add([...Clifton.business.service.ServiceWindowAdditionalTabs]);

							panel.updateDisplay(panel);
						}
					},
					afterRenderPromise: function(panel) {
						panel.updateDisplay(panel);
					},
					updateDisplay: function(panel) {
						// Re-configure Display based on service level type
						const form = panel.getForm();
						const serviceLevelType = TCG.getValue('serviceLevelType', form.formValues);
						if (TCG.isBlank(serviceLevelType)) {
							TCG.showError('Unknown service level type');
						}
						else {
							TCG.data.getDataPromiseUsingCaching('systemListItemByListAndValue.json?listName=Business Service Level Types&value=' + serviceLevelType, panel, 'business.service.level.type.' + serviceLevelType)
								.then(function(serviceLevelTypeValue) {
									if (!serviceLevelTypeValue) {
										TCG.showError('Missing system list item for service level type ' + serviceLevelType);
									}
									else {
										panel.updateDisplayForServiceLevelType(serviceLevelType, serviceLevelTypeValue);
									}
								});
						}
					},
					instructionsLoaded: false,
					updateDisplayForServiceLevelType: function(serviceLevelType, systemListItem) {
						this.getWindow().titlePrefix = systemListItem.text;
						this.updateTitle();
						if (this.instructionsLoaded === false) {
							this.insert(0, {xtype: 'label', html: systemListItem.tooltip + '<br /><br />'});
							this.instructionsLoaded = true;
						}

						this.setFieldLabel('name', systemListItem.text + ' Name:');
						let parentName;
						switch (serviceLevelType) {
							case 'FRANCHISE':
								parentName = null;
								break;
							case 'SUPER_STRATEGY':
								parentName = 'Franchise';
								break;
							case 'SERVICE_GROUP':
								parentName = 'Super Strategy';
								break;
							case 'SERVICE':
								parentName = 'Super Strategy/Group';
								break;
							default:
								parentName = 'Service';
						}
						if (TCG.isNotNull(parentName)) {
							this.setFieldLabel('parent.nameExpanded', parentName + ':');
						}
						else {
							this.hideField('parent.nameExpanded');
						}

						if (serviceLevelType === 'SERVICE') {
							this.showField('servicePropertiesHeader', serviceLevelType === 'SERVICE');
							this.showField('fullyFunded');
							this.showField('application.name');
							this.showField('investmentStyle');
							this.showField('aumCalculatorBean.labelShort');
							this.showField('accountingClosingMethod');
						}
						if (serviceLevelType === 'SERVICE' || serviceLevelType === 'SERVICE_COMPONENT') {
							this.showField('startDate');
							this.showField('endDate');
						}

						const tabs = this.getWindow().items.get(0);
						const processingTypesTabPosition = this.getWindow().getTabPosition('Processing Types');
						if (processingTypesTabPosition === 0 && serviceLevelType === 'SERVICE') {
							tabs.add(this.getWindow().processingTypesTab);
						}
						else if (processingTypesTabPosition !== 0 && serviceLevelType !== 'SERVICE') {
							tabs.remove(processingTypesTabPosition);
						}

						const objectivesTabPosition = this.getWindow().getTabPosition('Objectives');
						if (objectivesTabPosition === 0 && serviceLevelType === 'SERVICE') {
							tabs.add(this.getWindow().objectivesTab);
						}
						else if (objectivesTabPosition !== 0 && serviceLevelType !== 'SERVICE') {
							tabs.remove(objectivesTab);
						}
						this.doLayout();
					},

					items: [
						{name: 'serviceLevelType', hidden: true},
						{
							fieldLabel: 'Parent Service', name: 'parent.nameExpanded', hiddenName: 'parent.id', xtype: 'combo', displayField: 'nameExpanded', url: 'businessServiceListFind.json', detailPageClass: 'Clifton.business.service.ServiceWindow',
							listeners: {
								beforeQuery: function(qEvent) {
									const f = qEvent.combo.getParentForm().getForm();
									const serviceLevelType = f.findField('serviceLevelType').getValue();
									if (serviceLevelType === 'SUPER_STRATEGY') {
										qEvent.combo.store.setBaseParam('serviceLevelType', 'FRANCHISE');
									}
									else if (serviceLevelType === 'SERVICE_GROUP') {
										qEvent.combo.store.setBaseParam('serviceLevelType', 'SUPER_STRATEGY');
									}
									else if (serviceLevelType === 'SERVICE') {
										qEvent.combo.store.setBaseParam('serviceLevelTypes', ['SUPER_STRATEGY', 'SERVICE_GROUP']);
									}
									else if (serviceLevelType === 'SERVICE_COMPONENT') {
										qEvent.combo.store.setBaseParam('serviceLevelType', 'SERVICE');
									}
								}
							}
						},
						{fieldLabel: 'Service Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', hidden: true},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', hidden: true},
						{xtype: 'sectionheaderfield', name: 'servicePropertiesHeader', header: 'Service Properties', hidden: true},
						{boxLabel: 'Fully Funded', name: 'fullyFunded', xtype: 'checkbox', fieldLabel: '', hidden: true},
						{fieldLabel: 'Investment Vehicle', name: 'application.name', hiddenName: 'application.id', xtype: 'combo', url: 'businessServiceApplicationListFind.json', detailPageClass: 'Clifton.business.service.ApplicationWindow', hidden: true},
						{fieldLabel: 'Investment Style', name: 'investmentStyle', xtype: 'system-list-combo', listName: 'Business Service Investment Style', hidden: true},
						{fieldLabel: 'AUM Calculator', xtype: 'system-bean-combo', beanName: 'aumCalculatorBean', groupName: 'Investment Account Calculation Snapshot Calculator', hidden: true},
						{
							fieldLabel: 'Accounting Closing Method', name: 'accountingClosingMethod', hiddenName: 'accountingClosingMethod', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', hidden: true,
							store: {xtype: 'arraystore', fields: ['name', 'value', 'description'], data: Clifton.business.service.AccountingClosingMethods},
							qtip: 'Specifies the default Accounting Closing Method (FIFO, LIFO, etc.) to be used for all client accounts under this service. This method can be overridden for each Client Account as well as for individual transactions.'
						},
						{fieldLabel: 'Report Title', name: 'reportTitle', qtip: 'Enter the report title to display.  A value of "BLANK" will be no title, and an empty value will be "Parametric Overlay Solutions".'}
					]
				}]
			}
		]
	}],


	processingTypesTab: {
		title: 'Processing Types',
		items: [{
			name: 'businessServiceBusinessServiceProcessingTypeListFind',
			xtype: 'gridpanel',
			instructions: 'The following business service processing types are supported for this service.',
			additionalPropertiesToRequest: 'businessServiceProcessingType.id|businessService.id',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Processing Type Name', width: 70, dataIndex: 'businessServiceProcessingType.name'},
				{header: 'Processing Type', width: 75, dataIndex: 'businessServiceProcessingType.processingType'},
				{header: 'Description', width: 200, dataIndex: 'businessServiceProcessingType.description'}
			],
			getLoadParams: function(firstLoad) {
				return {businessServiceId: this.getWindow().getMainFormId()};
			},
			editor: {
				detailPageClass: 'Clifton.business.service.ProcessingTypeWindow',
				getDetailPageId: function(gridPanel, row) {
					return row.json.businessServiceProcessingType.id;
				},
				deleteURL: 'businessServiceFromProcessingTypeUnlink.json',
				getDeleteParams: function(selectionModel) {
					return {
						businessServiceProcessingTypeId: selectionModel.getSelected().json.businessServiceProcessingType.id,
						businessServiceId: this.getWindow().getMainFormId()
					};
				},
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					toolBar.add(new TCG.form.ComboBox({
						name: 'name', url: 'businessServiceProcessingTypeListFind.json', hiddenName: 'id', width: 150, listWidth: 230
					}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Associate a processing type to this service.',
						iconCls: 'add',
						handler: function() {
							const businessServiceProcessingTypeId = TCG.getChildByName(toolBar, 'name').getValue();
							if (TCG.isBlank(businessServiceProcessingTypeId)) {
								TCG.showError('Please select a business service processing type from the list.');
							}
							else {
								const businessServiceId = gridPanel.getWindow().getMainFormId();
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Linking...',
									params: {businessServiceProcessingTypeId: businessServiceProcessingTypeId, businessServiceId: businessServiceId},
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.getChildByName(toolBar, 'name').reset();
									}
								});
								loader.load('businessServiceToProcessingTypeLink.json');
							}
						}
					});
					toolBar.add('-');
				}
			},
			getTopToolbarFilters: function(toolBar) {
				const gridPanel = this;
				const menu = new Ext.menu.Menu();
				menu.add({
					text: 'Performance Summary Setup',
					handler: function() {
						gridPanel.openCustomFieldWindow('Service Performance Summary Fields');
					}
				});

				toolBar.add({
					text: 'Setup',
					tooltip: 'Service Setup',
					iconCls: 'services',
					menu: menu
				});
				toolBar.add('-');
			},
			openCustomFieldWindow: function(groupName) {
				const id = this.getWindow().getMainFormId();
				const customDetailClass = 'Clifton.business.service.ServiceCustomColumnWindow';
				const cmpId = TCG.getComponentId(customDetailClass, groupName + id);
				TCG.createComponent(customDetailClass, {
					id: cmpId,
					params: {id: id, columnGroupName: groupName}
				});
			}
		}]
	},


	objectivesTab: {
		title: 'Objectives',
		items: [{
			name: 'businessServiceObjectiveListFind',
			xtype: 'gridpanel',
			instructions: 'A business service objective',
			columns: [
				{header: 'Objective Name', width: 100, dataIndex: 'name'},
				{header: 'Description', width: 200, dataIndex: 'description'},
				{header: 'Order', width: 35, dataIndex: 'order', type: 'int'}
			],
			editor: {
				detailPageClass: 'Clifton.business.service.ObjectiveWindow'
			},
			getLoadParams: function(firstLoad) {
				return {businessServiceId: this.getWindow().getMainFormId()};
			}
		}]
	}
});
