Clifton.business.service.ServiceCustomColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Service',
	iconCls: 'services',
	width: 650,
	height: 450,

	title: 'Service Setup',
	items: [{
		xtype: 'formpanel-custom-fields',
		labelFieldName: 'label',
		url: 'businessService.json',
		getSaveURL: function() {
			return 'systemColumnValueListSave.json';
		},
		labelWidth: 250,
		loadValidation: false,

		items: [
			{name: 'columnGroupName', xtype: 'hidden'},
			{fieldLabel: 'Service', name: 'name', xtype: 'linkfield', detailPageClass: 'Clifton.business.service.ServiceWindow', detailIdField: 'id', submitDetailField: false},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
