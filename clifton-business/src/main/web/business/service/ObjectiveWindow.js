Clifton.business.service.ObjectiveWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Service Objective',
	iconCls: 'services',
	width: 800,
	height: 500,


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Service Objective defines standard objectives of a business service.',
					url: 'businessServiceObjective.json',

					items: [
						{fieldLabel: 'Service', hiddenName: 'businessService.id', name: 'businessService.name', xtype: 'combo', url: 'businessServiceListFind.json?serviceLevelType=SERVICE', detailPageClass: 'Clifton.business.service.ServiceWindow'},
						{fieldLabel: 'Objective Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'}
					]
				}]
			},


			{
				title: 'Investment Accounts',
				items: [{
					name: 'investmentAccountServiceObjectiveListFind',
					xtype: 'gridpanel',
					instructions: 'Accounts that utilize this service objective.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Account', width: 150, dataIndex: 'referenceOne.label', filter: {searchFieldName: 'investmentAccountLabel'}},
						{header: 'Objective Name', width: 125, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'objectiveName'}},
						{
							header: 'Description', width: 250, dataIndex: 'objectiveDescription', filter: {searchFieldName: 'coalesceObjectiveDescription'},
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.objectiveDescriptionOverridden)) {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Overridden', width: 50, dataIndex: 'objectiveDescriptionOverridden', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							businessServiceObjectiveId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountServiceObjectiveWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
