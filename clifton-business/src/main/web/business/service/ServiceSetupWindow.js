Clifton.business.service.ServiceSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'businessServiceSetupWindow',
	title: 'Business Services',
	iconCls: 'services',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Services',
				items: [{
					name: 'businessServiceListFind',
					xtype: 'gridpanel',
					remoteSort: true,
					instructions: 'A business service represents a product or a service that we are offering to our clients.  Each level of the service hierarchy has a specific meaning.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'parent.serviceLevelType|nameWithParent',
					viewNames: ['Default', 'Service Details'],
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Full Name', width: 300, dataIndex: 'nameExpanded', hidden: true},
						{header: 'Franchise', width: 100, dataIndex: 'franchiseName', hidden: true, viewNames: ['Service Details'], filter: {searchFieldName: 'nameExpanded', sortFieldName: 'nameExpanded'}},
						{header: 'Super Strategy', width: 100, dataIndex: 'superStrategyName', hidden: true, viewNames: ['Service Details'], filter: {searchFieldName: 'nameExpanded', sortFieldName: 'nameExpanded'}},
						{
							header: 'Service', width: 100, dataIndex: 'name', hidden: true, viewNames: ['Service Details'], filter: {searchFieldName: 'nameExpanded', sortFieldName: 'nameExpanded'},
							renderer: function(v, metaData, r) {
								if (TCG.getValue('parent.serviceLevelType', r.json) === 'SERVICE_GROUP') {
									return TCG.getValue('nameWithParent', r.json);
								}
								return v;
							}
						},
						{
							header: 'Service Name', width: 200, dataIndex: 'name', filter: {searchFieldName: 'nameExpanded', sortFieldName: 'nameExpanded'}, viewNames: ['Default'],
							renderer: function(v, metaData, r) {
								if (r.data.serviceLevelType === 'FRANCHISE') {
									return '<div style="FONT-WEIGHT: bold; COLOR: #000000;">' + v + '</div>';
								}
								if (r.data.serviceLevelType === 'SUPER_STRATEGY') {
									return '<div style="FONT-WEIGHT: bold; COLOR: #777777; PADDING-LEFT: 15px">' + v + '</div>';
								}
								if (r.data.serviceLevelType === 'SERVICE_GROUP') {
									return '<div style="FONT-WEIGHT: bold; PADDING-LEFT: 30px">' + v + '</div>';
								}
								if (r.data.serviceLevelType === 'SERVICE') {
									const padding = (TCG.getValue('parent.serviceLevelType', r.json) !== 'SERVICE_GROUP') ? 30 : 35;
									return '<div style="PADDING-LEFT: ' + padding + 'px">' + v + '</div>';
								}
								return '<div style="COLOR: #330027; FONT-STYLE: italic; PADDING-LEFT: 45px">' + v + '</div>';
							}
						},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Fully Funded', width: 35, dataIndex: 'fullyFunded', type: 'boolean'},
						{header: 'Investment Vehicle', width: 80, dataIndex: 'application.name', filter: {searchFieldName: 'applicationName'}, viewNames: ['Default', 'Service Details']},
						{header: 'Investment Style', width: 50, dataIndex: 'investmentStyle', viewNames: ['Default', 'Service Details']},
						{header: 'AUM Calculator', width: 100, dataIndex: 'aumCalculatorBean.labelShort', filter: {searchFieldName: 'aumCalculatorBeanId', type: 'combo', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Snapshot Calculator', displayField: 'labelShort'}, tooltip: 'Default calculator for the system to use to calculate AUM for accounts that use this service.  Can be overridden on the account.', viewNames: ['Default', 'Service Details']},
						{header: 'Closing Method', width: 50, dataIndex: 'accountingClosingMethod', filter: {type: 'list', options: Clifton.business.service.AccountingClosingMethods}, tooltip: 'Specifies the default Accounting Closing Method (FIFO, LIFO, etc.) to be used for all client accounts under this service. This method can be overridden for each Client Account as well as for individual transactions.', viewNames: ['Default', 'Service Details']},
						{header: 'Report Title', width: 80, dataIndex: 'reportTitle'},
						{header: 'Active', width: 35, dataIndex: 'active', type: 'boolean', viewNames: ['Default', 'Service Details']},
						{header: 'Start Date', width: 60, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 60, dataIndex: 'endDate', hidden: true},
						{header: 'Level Type', width: 40, dataIndex: 'serviceLevelType', hidden: true, filter: {searchFieldName: 'serviceLevelTypes', type: 'list', options: [['FRANCHISE', 'FRANCHISE'], ['SUPER_STRATEGY', 'SUPER_STRATEGY'], ['SERVICE_GROUP', 'SERVICE GROUP'], ['SERVICE', 'SERVICE'], ['SERVICE_COMPONENT', 'SERVICE_COMPONENT']]}}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'},
							{fieldLabel: 'Active On Date', name: 'activeOnDate', xtype: 'toolbar-datefield'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						let dateValue;
						const t = this.getTopToolbar();
						const bd = TCG.getChildByName(t, 'activeOnDate');
						if (firstLoad) {
							// default to today
							if (TCG.isBlank(bd.getValue())) {
								dateValue = (new Date()).format('m/d/Y');
								bd.setValue(dateValue);
							}
						}
						const result = {};
						if (TCG.isNotBlank(bd.getValue())) {
							dateValue = (bd.getValue()).format('m/d/Y');
							result.activeOnDate = dateValue;
						}
						return result;
					},
					switchToViewBeforeReload: function(viewName) {
						if (viewName === 'Service Details') {
							this.setFilterValue('serviceLevelType', ['SERVICE'], false, true);
						}
						else {
							this.clearFilter('serviceLevelType', true);
						}
					},

					editor: {
						detailPageClass: 'Clifton.business.service.ServiceWindow',

						addToolbarAddButton: function(toolBar) {
							const gp = this.getGridPanel();
							const editor = this;

							TCG.data.getDataPromiseUsingCaching('systemListItemListFind.json?listName=Business Service Level Types', gp, 'business.service.level.types')
								.then(function(serviceLevelTypes) {
										const menu = new Ext.menu.Menu();
										gp.instructions = '<ul class="c-list">';
										for (let i = 0; i < serviceLevelTypes.length; i++) {
											const serviceLevelType = serviceLevelTypes[i];
											gp.instructions += '<li><b>' + serviceLevelType.text + '</b>: ' + serviceLevelType.tooltip + '</li>';
											menu.add({
												scope: this,
												text: serviceLevelType.text,
												serviceLevelType: serviceLevelType.value,
												tooltip: serviceLevelType.tooltip,
												handler: function(b, e) {
													editor.openNewDetailPage(editor.detailPageClass, gp, b.serviceLevelType);
												}
											});
										}
										gp.instructions += '</ul>';
										toolBar.insert(0, {
											text: 'Add',
											tooltip: 'Add a new service',
											iconCls: 'add',
											xtype: 'splitbutton',
											menu: menu,
											scope: this
										});
										toolBar.insert(1, '-');
										toolBar.doLayout();
									}
								);
						}

						,

						openNewDetailPage: function(className, gridPanel, serviceLevelType) {
							const defaultData = {serviceLevelType: serviceLevelType};
							const cmpId = TCG.getComponentId(className, id);
							TCG.createComponent(className, {
								id: cmpId,
								defaultData: defaultData,
								defaultDataIsReal: true,
								params: undefined,
								openerCt: gridPanel,
								defaultIconCls: gridPanel.getWindow().iconCls
							});
						}
					}
				}]
			},


			{
				title: 'Service Processing Types',
				items: [{
					name: 'businessServiceProcessingTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Service Types are used to classify services and drive screen layout and options for accounts and portfolio processing.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Processing Type Name', width: 70, dataIndex: 'name'},
						{header: 'Processing Type', width: 75, dataIndex: 'processingType'},
						{header: 'Description', width: 250, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.service.ProcessingTypeWindow'
					}
				}]
			},


			{
				title: 'Investment Vehicles',
				items: [{
					name: 'businessServiceApplicationListFind',
					xtype: 'gridpanel',
					instructions: 'The term "investment vehicle" refers to any method by which businesses/individuals can invest. There is a wide variety of investment vehicles (such as Separate Account, Commingled Account, Mutual Fund, etc) and many investors choose to hold at least several types in their portfolios.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Vehicle Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.service.ApplicationWindow'
					}
				}]
			},


			{
				title: 'Service Objectives',
				items: [{
					name: 'businessServiceObjectiveListFind',
					xtype: 'gridpanel',
					instructions: 'Service Objective defines standard objectives of a business service.',
					topToolbarSearchParameter: 'searchPattern',

					groupField: 'businessService.name',
					groupTextTpl: '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Objectives" : "Objective"]})',
					remoteSort: true,
					columns: [
						{header: 'Service', width: 100, dataIndex: 'businessService.name', filter: {type: 'combo', searchFieldName: 'businessServiceId', url: 'businessServiceListFind.json?serviceLevelType=SERVICE'}, hidden: true},
						{header: 'Objective Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Order', width: 25, dataIndex: 'order', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.business.service.ObjectiveWindow'
					}
				}]
			}
		]
	}]
});

