Clifton.business.service.ProcessingTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Service Processing Type',
	iconCls: 'services',
	width: 700,
	height: 540,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel-custom-fields',
					columnGroupName: 'Business Service Processing Type Custom Fields',
					instructions: 'Service Types are used to classify services and drive screen layout and options for accounts and portfolio processing.',
					url: 'businessServiceProcessingType.json',
					labelWidth: 200,
					items: [
						{fieldLabel: 'Processing Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50},
						{
							fieldLabel: 'Processing Type', name: 'processingType', hiddenName: 'processingType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
							store: new Ext.data.ArrayStore({
								fields: ['name', 'value', 'description'],
								data: Clifton.business.service.ServiceProcessingTypes
							})
						},
						{xtype: 'label', html: '<hr/>'}
					],
					addToolbarButtons: function(toolbar, formPanel) {
						toolbar.add({
							text: 'Setup',
							tooltip: 'Processing Type Setup',
							iconCls: 'services',
							menu: new Ext.menu.Menu({
								items: [
									{
										text: 'Run Setup',
										iconCls: 'services',
										tooltip: 'Open a window for run processing configuration',
										handler: function() {
											formPanel.openCustomColumnWindow('Business Service Processing Type Run Custom Fields');
										}
									}
								]
							})
						});
					},
					openCustomColumnWindow: function(columnGroupName) {
						const customDetailClass = 'Clifton.business.service.ProcessingTypeCustomColumnWindow';
						const processingTypeId = this.getWindow().getMainFormId();
						const cmpId = TCG.getComponentId(customDetailClass, columnGroupName + processingTypeId);
						TCG.createComponent(customDetailClass, {
							id: cmpId,
							params: {id: processingTypeId, columnGroupName: columnGroupName}
						});
					}
				}]
			},


			{
				title: 'Services',
				items: [{
					name: 'businessServiceBusinessServiceProcessingTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following business services belong to this service type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'businessService.id', hidden: true},
						{header: 'Full Name', width: 300, dataIndex: 'businessService.nameExpanded', hidden: true},
						{header: 'Service', width: 200, dataIndex: 'businessService.name'},
						{header: 'Service Description', width: 400, dataIndex: 'businessService.description', hidden: true},
						{header: 'Investment Vehicle', width: 100, dataIndex: 'businessService.application.name', filter: {searchFieldName: 'applicationName'}},
						{header: 'Investment Style', width: 80, dataIndex: 'businessService.investmentStyle'},
						{header: 'AUM Calculator', width: 80, dataIndex: 'businessService.aumCalculatorBean.labelShort', filter: {searchFieldName: 'aumCalculatorBeanId', type: 'combo', url: 'systemBeanListFind.json?groupName=Investment Account Calculation Snapshot Calculator', displayField: 'labelShort'}, tooltip: 'Default calculator for the system to use to calculate AUM for accounts that use this service.  Can be overridden on the account.'},
						{header: 'Active', width: 35, dataIndex: 'businessService.active', type: 'boolean'},
						{header: 'Start Date', width: 60, dataIndex: 'businessService.startDate', hidden: true},
						{header: 'End Date', width: 60, dataIndex: 'businessService.endDate', hidden: true},
						{header: 'Level Type', width: 40, dataIndex: 'businessService.serviceLevelType', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						return {businessServiceProcessingTypeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.business.service.ServiceWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.businessService.id;
						},
						deleteURL: 'businessServiceFromProcessingTypeUnlink.json',
						getDeleteParams: function(selectionModel) {
							return {
								businessServiceProcessingTypeId: this.getWindow().getMainFormId(),
								businessServiceId: selectionModel.getSelected().json.businessService.id
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({
								name: 'name', url: 'businessServiceListFind.json?serviceLevelType=SERVICE&orderBy=name', hiddenName: 'id', width: 150, listWidth: 230
							}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Associate a business service with this business service processing type.',
								iconCls: 'add',
								handler: function() {
									const businessServiceId = TCG.getChildByName(toolBar, 'name').getValue();
									if (TCG.isBlank(businessServiceId)) {
										TCG.showError('Please select a business service from the list.');
									}
									else {
										const businessServiceProcessingTypeId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {businessServiceProcessingTypeId: businessServiceProcessingTypeId, businessServiceId: businessServiceId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'name').reset();
											}
										});
										loader.load('businessServiceToProcessingTypeLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
