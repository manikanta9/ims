Clifton.business.service.ProcessingTypeCustomColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Service Processing Type Setup',
	iconCls: 'services',
	width: 700,
	height: 400,

	items: [{
		xtype: 'formpanel-custom-fields',
		labelFieldName: 'label',
		url: 'businessServiceProcessingType.json',
		getSaveURL: function() {
			return 'systemColumnValueListSave.json';
		},
		labelWidth: 250,
		loadValidation: false,
		instructions: 'Service Processing Type custom column configuration',

		items: [
			{fieldLabel: 'Processing Type Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Processing Type', name: 'processingType', xtype: 'displayfield'},
			{fieldLabel: 'Custom Column Group', name: 'columnGroupName', xtype: 'displayfield'},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
