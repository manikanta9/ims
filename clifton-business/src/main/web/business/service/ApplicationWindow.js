Clifton.business.service.ApplicationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Service Vehicle',
	iconCls: 'services',

	items: [{
		xtype: 'formpanel',
		url: 'businessServiceApplication.json',
		instructions: 'The term "investment vehicle" refers to any method by which businesses/individuals can invest. There is a wide variety of investment vehicles (such as Separate Account, Commingled Account, Mutual Fund, etc) and many investors choose to hold at least several types in their portfolios.',
		items: [
			{fieldLabel: 'Vehicle Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
