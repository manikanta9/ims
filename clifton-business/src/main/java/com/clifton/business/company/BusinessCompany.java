package com.clifton.business.company;


import com.clifton.business.shared.Company;
import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.AddressUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessCompany</code> represents a company that can either be a client, a bank or other financial institution, money manager, broker, etc.
 * <p>
 * If a company has no parent, then it is the main company.  If it has a parent then the company itself is actually a subsidiary of the parent company.
 * Subsidiaries can be location based or could be used to distinguish between company subsidiaries that cover different company types.
 *
 * @author manderson
 */

public class BusinessCompany extends NamedHierarchicalEntity<BusinessCompany, Integer> implements AddressObject, SystemColumnCustomValueAware, SystemEntityModifyConditionAware {

	public static final String BUSINESS_COMPANY_TABLE_NAME = "BusinessCompany";

	public static final String BUSINESS_COMPANY_CUSTOM_FIELDS_GROUP_NAME = "Company Custom Fields";

	////////////////////////////////////////////////////////////////////////////////

	private BusinessCompanyType type;
	private Integer bloombergCompanyIdentifier;

	private BusinessCompanyParentRelationshipType parentRelationshipType;

	/**
	 * Explicitly designated Obligor.  Bloomberg number that identifies the obligor, which is the immediate resource of payment for a company.
	 */
	private BusinessCompany obligorBusinessCompany;

	/**
	 * Name of the company according to its official legal filings. May not be the same as the brand name or "doing business as" name.
	 */
	private String companyLegalName;

	/**
	 * The Alternate name a company may use, including doing business as (dba) name, also known as (aka) name, name that could be associated with an
	 * entity, and common spelling variations typically as a result of translations.  The alternate name may come from various sources such as registries,
	 * government lists, corporate documents, news stories, or websites.
	 */
	private String alternateCompanyName;

	/**
	 * "Friendly" company name instead of legal name. For example, "GS - REDI Stocks Only" for "Goldman Sachs Execution & Clearing".
	 */
	private String alias;

	/**
	 * Abbreviation for the company, i.e. BAR for Barclays
	 * These abbreviations can be used to standardize account names, security names (swaps) etc.
	 */
	private String abbreviation;

	/**
	 * A BIC is composed of a 4-character bank code, 2-character country code 2-character location code and optional 3 character branch code.
	 */
	private String businessIdentifierCode;

	/**
	 * The DTC number is a number that is issued in connection with Depository Trust Company for settlement services.
	 */
	private String dtcNumber;

	/**
	 * A company's standardized account at the Fed.
	 */
	private String fedMnemonic;


	/**
	 * Indicates if the company has a parent.  Returns 'Y' if the company is the ultimate parent, 'N' if the company has a parent or 'N.A.' if the parent
	 * information has not been verified.
	 */
	private boolean ultimateParent;

	/**
	 * Indicates if the company is presently a private entity, not publicly traded.
	 */
	private boolean privateCompany;

	private boolean acquiredByParent;


	/**
	 * We currently use it to identify our SEF sponsor when sending Cleared CDS orders via FIX.
	 * Legal entity identifier (LEI) displayed as a 20-character, alpha-numeric code that uniquely identifies legally distinct entities that engage in financial transactions.
	 * Used for reporting and other regulatory purposes. This was first mandated by the Dodd-Frank Act.
	 */
	private String legalEntityIdentifier;

	/**
	 * Current status of the entity for the legal entity identifier (LEI), either 'ACTIVE' or 'INACTIVE'.
	 * An active status represents an entity with an independent legal status which includes in administration, in receivership, bankruptcy, and other legal statuses.
	 * An inactive status represents an entity which no longer has an independent legal status as a result of dissolution.
	 */
	private String leiEntityStatus;

	/**
	 * Date on which the legal entity identifier (LEI) was first assigned by the registration authority.
	 */
	private Date leiEntityStartDate;

	/**
	 * Date on which the legal entity identifier (LEI) entered a disabled status.
	 */
	private Date leiEntityEndDate;

	/**
	 * Entity's address as listed with the registration authority; the address will be returned in a single column with a variable number of rows to match the address format provided.
	 */
	private String leiRegistrationAddress;


	private String stateOfIncorporation;
	private String countryOfIncorporation;
	private String countryOfRisk;


	/**
	 * 'Corp' ticker (fixed income) for publicly traded companies.
	 */
	private String companyCorporateTicker;
	/**
	 * Ticker and exchange of the ultimate parent company (highest level in organization chain).
	 */
	private String ultimateParentEquityTicker;


	private String phoneNumber;
	private String faxNumber;
	private String efaxNumber;
	private String emailAddress;
	private String webAddress;

	/**
	 * Company address associated with the company's domicile which is determined by the location of senior management.
	 */
	private String addressLine1;
	private String addressLine2;
	private String addressLine3;

	private String city;
	private String state;
	private String postalCode;

	/**
	 * Returns the ISO (International Organization for Standardization) code of the country where the company's senior management is located.
	 */
	private String country;

	private String registrationAddressLine1;
	private String registrationAddressLine2;
	private String registrationAddressLine3;

	private String registrationCity;
	private String registrationState;
	private String registrationPostalCode;
	private String registrationCountry;

	/**
	 * Only populated when fetching from Integration Business Company.
	 */
	@NonPersistentField
	private String integrationBicsIndustrySubgroup;

	/**
	 * A List of custom column values that are specific to the type of this company.
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Company toCompany() {
		Company result = new Company();
		if (getId() != null) {
			result.setId(getId());
		}
		if (getType() != null) {
			result.setType(getType().getName());
		}
		if (getParent() != null) {
			result.setParentCompany(getParent().toCompany());
		}
		result.setBloombergCompanyIdentifier(getBloombergCompanyIdentifier());
		result.setName(getName());
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return StringUtils.coalesce(false, getAlias(), getName());
	}


	public String getLabelWithOneParent() {
		if (getParent() != null) {
			return getParent().getLabel() + HIERARCHY_DELIMITER + getLabel();
		}
		return getLabel();
	}


	public String getNameWithType() {
		if (getType() != null) {
			return getName() + " (" + getType().getName() + ")";
		}
		return getName();
	}


	public String getNameExpandedWithType() {
		if (getType() != null) {
			return getNameExpanded() + " (" + getType().getName() + ")";
		}
		return getNameExpanded();
	}


	@Override
	public String getAddressLabel() {
		return AddressUtils.getAddressLabel(this);
	}


	@Override
	public String getCityState() {
		return AddressUtils.getCityState(this);
	}


	@Override
	public String getCityStatePostalCode() {
		return AddressUtils.getCityStatePostalCode(this);
	}


	/**
	 * Calculated-Inherited Obligor; the obligor of the nearest ancestor.
	 */
	public BusinessCompany getParentObligorBusinessCompany() {
		if (getObligorBusinessCompany() != null) {
			return getObligorBusinessCompany();
		}
		return doRecurseParentObligor(getParent());
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getType() != null) {
			return getType().getEntityModifyCondition();
		}
		return null;
	}




	public boolean isDTCParticipant() {
		return !StringUtils.isEmpty(getDtcNumber());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private BusinessCompany doRecurseParentObligor(BusinessCompany businessCompany) {
		if (businessCompany != null) {
			if (businessCompany.getObligorBusinessCompany() != null) {
				return businessCompany.getObligorBusinessCompany();
			}
			else {
				doRecurseParentObligor(businessCompany.getParent());
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyType getType() {
		return this.type;
	}


	public void setType(BusinessCompanyType type) {
		this.type = type;
	}


	public Integer getBloombergCompanyIdentifier() {
		return this.bloombergCompanyIdentifier;
	}


	public void setBloombergCompanyIdentifier(Integer bloombergCompanyIdentifier) {
		this.bloombergCompanyIdentifier = bloombergCompanyIdentifier;
	}


	public BusinessCompanyParentRelationshipType getParentRelationshipType() {
		return this.parentRelationshipType;
	}


	public void setParentRelationshipType(BusinessCompanyParentRelationshipType parentRelationshipType) {
		this.parentRelationshipType = parentRelationshipType;
	}


	public boolean isUltimateParent() {
		return this.ultimateParent;
	}


	public void setUltimateParent(boolean ultimateParent) {
		this.ultimateParent = ultimateParent;
	}


	public boolean isPrivateCompany() {
		return this.privateCompany;
	}


	public void setPrivateCompany(boolean privateCompany) {
		this.privateCompany = privateCompany;
	}


	public boolean isAcquiredByParent() {
		return this.acquiredByParent;
	}


	public void setAcquiredByParent(boolean acquiredByParent) {
		this.acquiredByParent = acquiredByParent;
	}


	public BusinessCompany getObligorBusinessCompany() {
		return this.obligorBusinessCompany;
	}


	public void setObligorBusinessCompany(BusinessCompany obligorBusinessCompany) {
		this.obligorBusinessCompany = obligorBusinessCompany;
	}


	public String getCompanyLegalName() {
		return this.companyLegalName;
	}


	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}


	public String getAlternateCompanyName() {
		return this.alternateCompanyName;
	}


	public void setAlternateCompanyName(String alternateCompanyName) {
		this.alternateCompanyName = alternateCompanyName;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getAbbreviation() {
		return this.abbreviation;
	}


	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}


	public String getFedMnemonic() {
		return this.fedMnemonic;
	}


	public void setFedMnemonic(String fedMnemonic) {
		this.fedMnemonic = fedMnemonic;
	}


	public String getBusinessIdentifierCode() {
		return this.businessIdentifierCode;
	}


	public void setBusinessIdentifierCode(String businessIdentifierCode) {
		this.businessIdentifierCode = businessIdentifierCode;
	}


	public String getDtcNumber() {
		return this.dtcNumber;
	}


	public void setDtcNumber(String dtcNumber) {
		this.dtcNumber = dtcNumber;
	}


	public String getLegalEntityIdentifier() {
		return this.legalEntityIdentifier;
	}


	public void setLegalEntityIdentifier(String legalEntityIdentifier) {
		this.legalEntityIdentifier = legalEntityIdentifier;
	}


	public String getLeiEntityStatus() {
		return this.leiEntityStatus;
	}


	public void setLeiEntityStatus(String leiEntityStatus) {
		this.leiEntityStatus = leiEntityStatus;
	}


	public Date getLeiEntityStartDate() {
		return this.leiEntityStartDate;
	}


	public void setLeiEntityStartDate(Date leiEntityStartDate) {
		this.leiEntityStartDate = leiEntityStartDate;
	}


	public Date getLeiEntityEndDate() {
		return this.leiEntityEndDate;
	}


	public void setLeiEntityEndDate(Date leiEntityEndDate) {
		this.leiEntityEndDate = leiEntityEndDate;
	}


	public String getLeiRegistrationAddress() {
		return this.leiRegistrationAddress;
	}


	public void setLeiRegistrationAddress(String leiRegistrationAddress) {
		this.leiRegistrationAddress = leiRegistrationAddress;
	}


	public String getCompanyCorporateTicker() {
		return this.companyCorporateTicker;
	}


	public void setCompanyCorporateTicker(String companyCorporateTicker) {
		this.companyCorporateTicker = companyCorporateTicker;
	}


	public String getUltimateParentEquityTicker() {
		return this.ultimateParentEquityTicker;
	}


	public void setUltimateParentEquityTicker(String ultimateParentEquityTicker) {
		this.ultimateParentEquityTicker = ultimateParentEquityTicker;
	}


	public String getCountryOfIncorporation() {
		return this.countryOfIncorporation;
	}


	public void setCountryOfIncorporation(String countryOfIncorporation) {
		this.countryOfIncorporation = countryOfIncorporation;
	}


	public String getCountryOfRisk() {
		return this.countryOfRisk;
	}


	public void setCountryOfRisk(String countryOfRisk) {
		this.countryOfRisk = countryOfRisk;
	}


	public String getStateOfIncorporation() {
		return this.stateOfIncorporation;
	}


	public void setStateOfIncorporation(String stateOfIncorporation) {
		this.stateOfIncorporation = stateOfIncorporation;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getFaxNumber() {
		return this.faxNumber;
	}


	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}


	public String getEfaxNumber() {
		return this.efaxNumber;
	}


	public void setEfaxNumber(String efaxNumber) {
		this.efaxNumber = efaxNumber;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getWebAddress() {
		return this.webAddress;
	}


	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}


	@Override
	public String getAddressLine1() {
		return this.addressLine1;
	}


	@Override
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	@Override
	public String getAddressLine2() {
		return this.addressLine2;
	}


	@Override
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	@Override
	public String getAddressLine3() {
		return this.addressLine3;
	}


	@Override
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}


	@Override
	public String getCity() {
		return this.city;
	}


	@Override
	public void setCity(String city) {
		this.city = city;
	}


	@Override
	public String getState() {
		return this.state;
	}


	@Override
	public void setState(String state) {
		this.state = state;
	}


	@Override
	public String getPostalCode() {
		return this.postalCode;
	}


	@Override
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	@Override
	public String getCountry() {
		return this.country;
	}


	@Override
	public void setCountry(String country) {
		this.country = country;
	}


	public String getRegistrationAddressLine1() {
		return this.registrationAddressLine1;
	}


	public void setRegistrationAddressLine1(String registrationAddressLine1) {
		this.registrationAddressLine1 = registrationAddressLine1;
	}


	public String getRegistrationAddressLine2() {
		return this.registrationAddressLine2;
	}


	public void setRegistrationAddressLine2(String registrationAddressLine2) {
		this.registrationAddressLine2 = registrationAddressLine2;
	}


	public String getRegistrationAddressLine3() {
		return this.registrationAddressLine3;
	}


	public void setRegistrationAddressLine3(String registrationAddressLine3) {
		this.registrationAddressLine3 = registrationAddressLine3;
	}


	public String getRegistrationCity() {
		return this.registrationCity;
	}


	public void setRegistrationCity(String registrationCity) {
		this.registrationCity = registrationCity;
	}


	public String getRegistrationState() {
		return this.registrationState;
	}


	public void setRegistrationState(String registrationState) {
		this.registrationState = registrationState;
	}


	public String getRegistrationPostalCode() {
		return this.registrationPostalCode;
	}


	public void setRegistrationPostalCode(String registrationPostalCode) {
		this.registrationPostalCode = registrationPostalCode;
	}


	public String getRegistrationCountry() {
		return this.registrationCountry;
	}


	public void setRegistrationCountry(String registrationCountry) {
		this.registrationCountry = registrationCountry;
	}


	public String getIntegrationBicsIndustrySubgroup() {
		return this.integrationBicsIndustrySubgroup;
	}


	public void setIntegrationBicsIndustrySubgroup(String integrationBicsIndustrySubgroup) {
		this.integrationBicsIndustrySubgroup = integrationBicsIndustrySubgroup;
	}
}
