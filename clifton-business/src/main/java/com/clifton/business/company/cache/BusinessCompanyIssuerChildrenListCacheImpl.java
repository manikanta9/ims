package com.clifton.business.company.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


/**
 * @author JasonS
 */
@Component
public class BusinessCompanyIssuerChildrenListCacheImpl extends BaseBusinessCompanyChildrenListCache implements BusinessCompanyIssuerChildrenListCache {

	protected static final String BEAN_FIELD_NAME = "parent.id";


	@Override
	public Integer[] getBusinessCompanyIssuerChildrenList(int businessCompanyId, ReadOnlyDAO<BusinessCompany> businessCompanyDAO) {
		Integer[] result = getCacheHandler().get(getCacheName(), businessCompanyId);
		if (result == null) {
			Set<Integer> childrenSet = new HashSet<>();
			addAllBusinessCompanyChildrenByField(businessCompanyId, childrenSet, businessCompanyDAO);
			if (CollectionUtils.isEmpty(childrenSet)) {
				result = new Integer[0];
			}
			else {
				result = childrenSet.toArray(new Integer[0]);
			}
			getCacheHandler().put(getCacheName(), businessCompanyId, result);
		}
		return result;
	}


	@Override
	String getBeanFieldName() {
		return BEAN_FIELD_NAME;
	}
}
