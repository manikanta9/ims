package com.clifton.business.company;


import com.clifton.business.company.cache.BusinessCompanyIssuerChildrenListCache;
import com.clifton.business.company.cache.BusinessCompanyObligorChildrenListCache;
import com.clifton.business.company.search.BusinessCompanyParentRelationshipTypeSearchForm;
import com.clifton.business.company.search.BusinessCompanyPlatformSearchForm;
import com.clifton.business.company.search.BusinessCompanyRelationshipSearchForm;
import com.clifton.business.company.search.BusinessCompanyTypeSearchForm;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.definition.WorkflowStatus;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.type.ShortType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>BusinessCompanyServiceImpl</code> provides a basic implementation of the
 * {@link BusinessCompanyService} interface
 *
 * @author manderson
 */
@Service
public class BusinessCompanyServiceImpl implements BusinessCompanyService {

	private AdvancedUpdatableDAO<BusinessCompany, Criteria> businessCompanyDAO;
	private DaoCompositeKeyCache<BusinessCompany, Short, String> businessCompanyByTypeIdAndNameCache;

	private AdvancedUpdatableDAO<BusinessCompanyType, Criteria> businessCompanyTypeDAO;
	private DaoNamedEntityCache<BusinessCompanyType> businessCompanyTypeCache;

	private AdvancedUpdatableDAO<BusinessCompanyRelationship, Criteria> businessCompanyRelationshipDAO;

	private AdvancedUpdatableDAO<BusinessCompanyParentRelationshipType, Criteria> businessCompanyParentRelationshipTypeDAO;
	private DaoNamedEntityCache<BusinessCompanyParentRelationshipType> businessCompanyParentRelationshipTypeCache;

	private AdvancedUpdatableDAO<BusinessCompanyPlatform, Criteria> businessCompanyPlatformDAO;

	private BusinessCompanyIssuerChildrenListCache businessCompanyIssuerChildrenListCache;
	private BusinessCompanyObligorChildrenListCache businessCompanyObligorChildrenListCache;
	////////////////////////////////////////////////////////////////////////////
	///////////            Business Company Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompany getBusinessCompany(int id) {
		return getBusinessCompanyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessCompany> getBusinessCompanyByBloombergCompanyIdentifier(int bloombergCompanyIdentifier) {
		CompanySearchForm searchForm = new CompanySearchForm();
		searchForm.setBloombergCompanyIdentifier(bloombergCompanyIdentifier);
		return getBusinessCompanyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessCompany getBusinessCompanyByTypeAndName(final String type, final String name) {
		BusinessCompanyType businessCompanyType = getBusinessCompanyTypeByName(type);
		ValidationUtils.assertNotNull(businessCompanyType, "Could not find Business Company Type with name " + type);
		return getBusinessCompanyByTypeIdAndNameCache().getBeanForKeyValues(getBusinessCompanyDAO(), businessCompanyType.getId(), name);
	}


	@Override
	public List<BusinessCompany> getBusinessCompanyList(final CompanySearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveIssuerInvestmentAccountTypeId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS (SELECT act.IssuingCompanyID FROM InvestmentAccount act INNER JOIN WorkflowStatus ws ON act.WorkflowStatusID = ws.WorkflowStatusID WHERE ws.WorkflowStatusName = '"
							+ WorkflowStatus.STATUS_ACTIVE
							+ "' AND act.InvestmentAccountTypeID = ? "
							+ " AND act.IssuingCompanyID = {alias}.BusinessCompanyID)", searchForm.getActiveIssuerInvestmentAccountTypeId(), ShortType.INSTANCE));
				}

				// Where Company exists as a main or related company with given relationship name
				if (!StringUtils.isEmpty(searchForm.getCompanyRelationshipExists())) {
					DetachedCriteria sub = DetachedCriteria.forClass(BusinessCompanyRelationship.class, "bcr");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eq("name", searchForm.getCompanyRelationshipExists()));
					sub.add(Restrictions.or(Restrictions.eqProperty("bcr.referenceOne.id", criteria.getAlias() + ".id"), Restrictions.eqProperty("bcr.referenceTwo.id", criteria.getAlias() + ".id")));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};
		return getBusinessCompanyDAO().findBySearchCriteria(config);
	}


	@Override
	public List<BusinessCompany> getBusinessCompanyRelatedCompanyList(int id) {
		return getBusinessCompanyDAO().findByNamedQuery("business-company-hierarchy", id);
	}


	@Override
	public BusinessCompany saveBusinessCompany(BusinessCompany bean) {
		return getBusinessCompanyDAO().save(bean);
	}


	@Override
	@Transactional
	public void deleteBusinessCompany(int id) {
		// Company Contacts are deleted in the observer
		deleteBusinessCompanyRelationshipListByCompany(id);
		getBusinessCompanyDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Company Type Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompanyType getBusinessCompanyType(short id) {
		return getBusinessCompanyTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessCompanyType getBusinessCompanyTypeByName(String name) {
		return getBusinessCompanyTypeCache().getBeanForKeyValue(getBusinessCompanyTypeDAO(), name);
	}


	@Override
	public List<BusinessCompanyType> getBusinessCompanyTypeList(BusinessCompanyTypeSearchForm searchForm) {
		return getBusinessCompanyTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	// See {@link BusinessCompanyTypeValidator} for validation
	@Override
	public BusinessCompanyType saveBusinessCompanyType(BusinessCompanyType bean) {
		return getBusinessCompanyTypeDAO().save(bean);
	}


	@Override
	public void deleteBusinessCompanyType(short id) {
		getBusinessCompanyTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////           Business Company Relationship Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompanyRelationship getBusinessCompanyRelationship(int id) {
		return getBusinessCompanyRelationshipDAO().findByPrimaryKey(id);
	}


	private List<BusinessCompanyRelationship> getBusinessCompanyRelationshipListByCompany(int companyId) {
		BusinessCompanyRelationshipSearchForm searchForm = new BusinessCompanyRelationshipSearchForm();
		searchForm.setCompanyId(companyId);
		return getBusinessCompanyRelationshipList(searchForm);
	}


	@Override
	public List<BusinessCompanyRelationship> getBusinessCompanyRelationshipList(final BusinessCompanyRelationshipSearchForm searchForm) {
		return getBusinessCompanyRelationshipDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessCompanyRelationship saveBusinessCompanyRelationship(BusinessCompanyRelationship bean) {
		if (!StringUtils.isEmpty(bean.getIdentificationCode())) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getIdentificationCodeType()), "Identification Code Type is required when an identification code is entered.");
		}
		return getBusinessCompanyRelationshipDAO().save(bean);
	}


	@Override
	@Transactional
	public void copyBusinessCompanyRelationships(BusinessCompany from, BusinessCompany to, boolean moveRelationships, StringBuilder result) {
		// Copy BusinessCompanyRelationship(s)
		int count = 0;
		List<BusinessCompanyRelationship> fromList = getBusinessCompanyRelationshipListByCompany(from.getId());
		if (!CollectionUtils.isEmpty(fromList)) {
			List<BusinessCompanyRelationship> updateList = new ArrayList<>();
			List<BusinessCompanyRelationship> deleteList = new ArrayList<>();

			List<BusinessCompanyRelationship> toList = getBusinessCompanyRelationshipListByCompany(to.getId());
			for (BusinessCompanyRelationship fromCC : CollectionUtils.getIterable(fromList)) {
				boolean found = false;
				BusinessCompany fromRelatedCompany = (fromCC.getReferenceOne().equals(from) ? fromCC.getReferenceTwo() : fromCC.getReferenceOne());
				for (BusinessCompanyRelationship toCC : CollectionUtils.getIterable(toList)) {
					BusinessCompany toRelatedCompany = (toCC.getReferenceOne().equals(to) ? toCC.getReferenceTwo() : toCC.getReferenceOne());
					if (fromRelatedCompany.equals(toRelatedCompany) && StringUtils.isEqual(fromCC.getName(), toCC.getName())) {
						found = true;
						break;
					}
				}
				if (!found) {
					if (fromCC.getReferenceOne().equals(from)) {
						if (moveRelationships) {
							fromCC.setReferenceOne(to);
							updateList.add(fromCC);
						}
						else {
							BusinessCompanyRelationship copy = BeanUtils.cloneBean(fromCC, false, false);
							copy.setReferenceOne(to);
							updateList.add(copy);
						}
					}
					else {
						if (moveRelationships) {
							fromCC.setReferenceTwo(to);
							updateList.add(fromCC);
						}
						else {
							BusinessCompanyRelationship copy = BeanUtils.cloneBean(fromCC, false, false);
							copy.setReferenceTwo(to);
							updateList.add(copy);
						}
					}
					count++;
				}
				else if (moveRelationships) {
					// Already there - just delete the old reference
					deleteList.add(fromCC);
				}
			}
			getBusinessCompanyRelationshipDAO().saveList(updateList);
			getBusinessCompanyRelationshipDAO().deleteList(deleteList);
		}
		result.append(StringUtils.NEW_LINE + "[").append(count).append("] company-company relationships ").append(moveRelationships ? "moved" : "copied").append(".");
	}


	@Override
	public void deleteBusinessCompanyRelationship(int id) {
		getBusinessCompanyRelationshipDAO().delete(id);
	}


	@Override
	public void deleteBusinessCompanyRelationshipListByCompany(int companyId) {
		BusinessCompanyRelationshipSearchForm searchForm = new BusinessCompanyRelationshipSearchForm();
		searchForm.setCompanyId(companyId);
		getBusinessCompanyRelationshipDAO().deleteList(getBusinessCompanyRelationshipList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	///////      Business Company Parent Relationship Type Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompanyParentRelationshipType getBusinessCompanyParentRelationshipType(short id) {
		return getBusinessCompanyParentRelationshipTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessCompanyParentRelationshipType getBusinessCompanyParentRelationshipTypeByName(String name) {
		return getBusinessCompanyParentRelationshipTypeCache().getBeanForKeyValue(getBusinessCompanyParentRelationshipTypeDAO(), name);
	}


	@Override
	public List<BusinessCompanyParentRelationshipType> getBusinessCompanyParentRelationshipTypeList(BusinessCompanyParentRelationshipTypeSearchForm searchForm) {
		return getBusinessCompanyParentRelationshipTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessCompanyParentRelationshipType saveBusinessCompanyParentRelationshipType(BusinessCompanyParentRelationshipType bean) {
		return getBusinessCompanyParentRelationshipTypeDAO().save(bean);
	}


	@Override
	public void deleteBusinessCompanyParentRelationshipType(short id) {
		getBusinessCompanyParentRelationshipTypeDAO().delete(id);
	}



	////////////////////////////////////////////////////////////////////////////
	//////////        Business Company Issuer List Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessCompany> getBusinessCompanyChildrenListForParent(int parentCompanyId) {
		Integer[] childCompanyIds = getBusinessCompanyIssuerChildrenListCache().getBusinessCompanyIssuerChildrenList(parentCompanyId, getBusinessCompanyDAO());
		if (ArrayUtils.isEmpty(childCompanyIds)) {
			return Collections.emptyList();
		}
		return getBusinessCompanyDAO().findByPrimaryKeys(childCompanyIds);
	}


	@Override
	public List<BusinessCompany> getBusinessCompanyChildrenListForObligor(int obligorCompanyId) {
		Integer[] childObligorIds = getBusinessCompanyObligorChildrenListCache().getBusinessCompanyObligorChildrenList(obligorCompanyId, getBusinessCompanyDAO());
		if (ArrayUtils.isEmpty(childObligorIds)) {
			return Collections.emptyList();
		}
		return getBusinessCompanyDAO().findByPrimaryKeys(childObligorIds);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////         Business Company Platform Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompanyPlatform getBusinessCompanyPlatform(int id) {
		return getBusinessCompanyPlatformDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessCompanyPlatform> getBusinessCompanyPlatformList(BusinessCompanyPlatformSearchForm searchForm) {
		return getBusinessCompanyPlatformDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * See {@link com.clifton.business.company.observers.BusinessCompanyPlatformValidator} for validation
	 */
	@Override
	public BusinessCompanyPlatform saveBusinessCompanyPlatform(BusinessCompanyPlatform bean) {
		return getBusinessCompanyPlatformDAO().save(bean);
	}


	@Override
	public void deleteBusinessCompanyPlatform(int id) {
		getBusinessCompanyPlatformDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessCompany, Criteria> getBusinessCompanyDAO() {
		return this.businessCompanyDAO;
	}


	public void setBusinessCompanyDAO(AdvancedUpdatableDAO<BusinessCompany, Criteria> businessCompanyDAO) {
		this.businessCompanyDAO = businessCompanyDAO;
	}


	public DaoCompositeKeyCache<BusinessCompany, Short, String> getBusinessCompanyByTypeIdAndNameCache() {
		return this.businessCompanyByTypeIdAndNameCache;
	}


	public void setBusinessCompanyByTypeIdAndNameCache(DaoCompositeKeyCache<BusinessCompany, Short, String> businessCompanyByTypeIdAndNameCache) {
		this.businessCompanyByTypeIdAndNameCache = businessCompanyByTypeIdAndNameCache;
	}


	public AdvancedUpdatableDAO<BusinessCompanyType, Criteria> getBusinessCompanyTypeDAO() {
		return this.businessCompanyTypeDAO;
	}


	public void setBusinessCompanyTypeDAO(AdvancedUpdatableDAO<BusinessCompanyType, Criteria> businessCompanyTypeDAO) {
		this.businessCompanyTypeDAO = businessCompanyTypeDAO;
	}


	public DaoNamedEntityCache<BusinessCompanyType> getBusinessCompanyTypeCache() {
		return this.businessCompanyTypeCache;
	}


	public void setBusinessCompanyTypeCache(DaoNamedEntityCache<BusinessCompanyType> businessCompanyTypeCache) {
		this.businessCompanyTypeCache = businessCompanyTypeCache;
	}


	public AdvancedUpdatableDAO<BusinessCompanyRelationship, Criteria> getBusinessCompanyRelationshipDAO() {
		return this.businessCompanyRelationshipDAO;
	}


	public void setBusinessCompanyRelationshipDAO(AdvancedUpdatableDAO<BusinessCompanyRelationship, Criteria> businessCompanyRelationshipDAO) {
		this.businessCompanyRelationshipDAO = businessCompanyRelationshipDAO;
	}


	public AdvancedUpdatableDAO<BusinessCompanyParentRelationshipType, Criteria> getBusinessCompanyParentRelationshipTypeDAO() {
		return this.businessCompanyParentRelationshipTypeDAO;
	}


	public void setBusinessCompanyParentRelationshipTypeDAO(AdvancedUpdatableDAO<BusinessCompanyParentRelationshipType, Criteria> businessCompanyParentRelationshipTypeDAO) {
		this.businessCompanyParentRelationshipTypeDAO = businessCompanyParentRelationshipTypeDAO;
	}


	public DaoNamedEntityCache<BusinessCompanyParentRelationshipType> getBusinessCompanyParentRelationshipTypeCache() {
		return this.businessCompanyParentRelationshipTypeCache;
	}


	public void setBusinessCompanyParentRelationshipTypeCache(DaoNamedEntityCache<BusinessCompanyParentRelationshipType> businessCompanyParentRelationshipTypeCache) {
		this.businessCompanyParentRelationshipTypeCache = businessCompanyParentRelationshipTypeCache;
	}


	public AdvancedUpdatableDAO<BusinessCompanyPlatform, Criteria> getBusinessCompanyPlatformDAO() {
		return this.businessCompanyPlatformDAO;
	}


	public void setBusinessCompanyPlatformDAO(AdvancedUpdatableDAO<BusinessCompanyPlatform, Criteria> businessCompanyPlatformDAO) {
		this.businessCompanyPlatformDAO = businessCompanyPlatformDAO;
	}


	public BusinessCompanyIssuerChildrenListCache getBusinessCompanyIssuerChildrenListCache() {
		return this.businessCompanyIssuerChildrenListCache;
	}


	public void setBusinessCompanyIssuerChildrenListCache(BusinessCompanyIssuerChildrenListCache businessCompanyIssuerChildrenListCache) {
		this.businessCompanyIssuerChildrenListCache = businessCompanyIssuerChildrenListCache;
	}


	public BusinessCompanyObligorChildrenListCache getBusinessCompanyObligorChildrenListCache() {
		return this.businessCompanyObligorChildrenListCache;
	}


	public void setBusinessCompanyObligorChildrenListCache(BusinessCompanyObligorChildrenListCache businessCompanyObligorChildrenListCache) {
		this.businessCompanyObligorChildrenListCache = businessCompanyObligorChildrenListCache;
	}
}
