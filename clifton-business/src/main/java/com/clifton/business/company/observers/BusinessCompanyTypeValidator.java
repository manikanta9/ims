package com.clifton.business.company.observers;


import com.clifton.business.company.BusinessCompanyType;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessCompanyTypeValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class BusinessCompanyTypeValidator extends SelfRegisteringDaoValidator<BusinessCompanyType> {

	private SecurityAuthorizationService securityAuthorizationService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BusinessCompanyType bean, DaoEventTypes config) throws ValidationException {
		BusinessCompanyType originalBean = null;
		if (!config.isInsert()) {
			originalBean = getOriginalBean(bean);
		}
		if (config.isDelete()) {
			if (originalBean != null && originalBean.isSystemDefined()) {
				throw new ValidationException("Cannot delete a system defined company type.");
			}
		}
		else if (config.isUpdate()) {
			if (originalBean != null && originalBean.isSystemDefined()) {
				if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
					throw new ValidationException("Administrators only can update a system defined company type.");
				}
				if (!originalBean.getName().equals(bean.getName())) {
					throw new ValidationException("Cannot change the name of a system defined company type.");
				}
				if (!bean.isSystemDefined()) {
					throw new ValidationException("Cannot change a system defined company type to not system defined.");
				}
			}
			else if (bean.isSystemDefined()) {
				throw new ValidationException("Cannot change a non system defined company type to system defined.");
			}
		}
		else if (config.isInsert()) {
			if (bean.isSystemDefined()) {
				throw new ValidationException("Cannot create a system defined company type.");
			}
		}
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
