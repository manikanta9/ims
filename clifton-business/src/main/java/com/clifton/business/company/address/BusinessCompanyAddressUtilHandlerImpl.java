package com.clifton.business.company.address;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;


/**
 * @see BusinessCompanyAddressUtilHandler
 */
@Component
public class BusinessCompanyAddressUtilHandlerImpl<L extends SystemList> implements BusinessCompanyAddressUtilHandler {

	private SystemListService<L> systemListService;

	private final Lazy<Map<String, String>> stateMap = new Lazy<>(this::generateStateMap);
	private final Lazy<Map<String, String>> provinceMap = new Lazy<>(this::generateProvinceMap);

	private final Map<String, String> territoryMap;
	private final Map<String, String> countriesMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	BusinessCompanyAddressUtilHandlerImpl() {
		this.territoryMap = new HashMap<>(8);
		this.territoryMap.put("australian capital territory", "ACT");
		this.territoryMap.put("new south wales", "NSW");
		this.territoryMap.put("northern territory", "NT");
		this.territoryMap.put("queensland", "QLD");
		this.territoryMap.put("south australia", "SA");
		this.territoryMap.put("tasmania", "TAS");
		this.territoryMap.put("victoria", "VIC");
		this.territoryMap.put("western australia", "WA");

		this.countriesMap = new HashMap<>(300);
		for (String iso : Locale.getISOCountries()) {
			Locale locale = new Locale("en", iso);
			this.countriesMap.put(locale.getDisplayCountry().toLowerCase(), iso);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<String> getStateCodes() {
		return Collections.unmodifiableCollection(getStateMap().values());
	}


	@Override
	public String getStateCode(String state) {
		String stateCode = !StringUtils.isEmpty(state) ? getStateMap().get(state.toLowerCase()) : null;
		if (stateCode == null && !StringUtils.isEmpty(state) && getStateMap().containsValue(state.toUpperCase())) {
			return state.toUpperCase();
		}
		return stateCode;
	}


	@Override
	public Collection<String> getProvinceCodes() {
		return Collections.unmodifiableCollection(getProvinceMap().values());
	}


	@Override
	public String getProvinceCode(String province) {
		String provinceCode = !StringUtils.isEmpty(province) ? getProvinceMap().get(province.toLowerCase()) : null;
		if (provinceCode == null && !StringUtils.isEmpty(province) && getProvinceMap().containsValue(province.toUpperCase())) {
			return province.toUpperCase();
		}
		return provinceCode;
	}


	@Override
	public Collection<String> getTerritoryCodes() {
		return Collections.unmodifiableCollection(this.territoryMap.values());
	}


	@Override
	public String getTerritoryCode(String territory) {
		String territoryCode = !StringUtils.isEmpty(territory) ? this.territoryMap.get(territory.toLowerCase()) : null;
		if (territoryCode == null && !StringUtils.isEmpty(territory) && this.territoryMap.containsValue(territory.toUpperCase())) {
			return territory.toUpperCase();
		}
		return territoryCode;
	}


	@Override
	public String getCountryCode(String country) {
		String countryCode = !StringUtils.isEmpty(country) ? this.countriesMap.get(country.toLowerCase()) : null;
		if (countryCode == null && !StringUtils.isEmpty(country) && this.countriesMap.containsValue(country.toUpperCase())) {
			return country.toUpperCase();
		}
		return countryCode;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Map of state code to names:
	 * [ {alabama, AL}, {alaska, AL}, ...]
	 */
	private Map<String, String> getStateMap() {
		return this.stateMap.get();
	}


	private Map<String, String> generateStateMap() {
		List<SystemListItem> systemListItems = getSystemListService().getSystemListItemListByList(getSystemListService().getSystemListByName("States").getId());
		Map<String, String> results = new HashMap<>(systemListItems.size());
		for (SystemListItem item : systemListItems) {
			results.put(item.getText().toLowerCase(), item.getValue().toUpperCase());
		}
		return results;
	}


	/**
	 * Map of province code to names:
	 * [ {alberta, AB}, {british columbia, BC}, ...]
	 */
	private Map<String, String> getProvinceMap() {
		return this.provinceMap.get();
	}


	private Map<String, String> generateProvinceMap() {
		List<SystemListItem> systemListItems = getSystemListService().getSystemListItemListByList(getSystemListService().getSystemListByName("Canadian Provinces").getId());
		Map<String, String> results = new HashMap<>(systemListItems.size());
		for (SystemListItem item : systemListItems) {
			results.put(item.getText().toLowerCase(), item.getValue().toUpperCase());
		}
		return results;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemListService<L> getSystemListService() {
		return this.systemListService;
	}


	public void setSystemListService(SystemListService<L> systemListService) {
		this.systemListService = systemListService;
	}
}
