package com.clifton.business.company.address;

import java.util.Collection;


/**
 * <code>BusinessCompanyAddressUtilHandler</code> utility methods for loading and retrieving information needed to
 * load and identify address components.  For instance, US State, Canadian Provinces, Australian Territories and
 * Japanese Prefectures.
 */
public interface BusinessCompanyAddressUtilHandler {

	/**
	 * Provided the Country Name or code, lookup and return its 2 character ISO country code.
	 */
	public String getCountryCode(String country);


	/**
	 * Provided the USA state name or code, lookup and return its 2 character state code.
	 */
	public String getStateCode(String state);


	/**
	 * Get all the USA 2 character state codes.
	 */
	public Collection<String> getStateCodes();


	/**
	 * Get all the Canadian 2 character province codes.
	 */
	public Collection<String> getProvinceCodes();


	/**
	 * Provided the Canadian province name or code, lookup and return its 2 character province code.
	 */
	public String getProvinceCode(String province);


	/**
	 * Get all the Australian 2-3 character territory codes.
	 */
	public Collection<String> getTerritoryCodes();


	/**
	 * Provided the Australian territory name or code, lookup and return its 2-3 character territory code.
	 */
	public String getTerritoryCode(String territory);
}
