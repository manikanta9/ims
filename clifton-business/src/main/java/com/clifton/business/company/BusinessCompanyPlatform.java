package com.clifton.business.company;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>BusinessCompanyPlatform</code> is the Broker Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers' strategies.
 * Each platform is unique to a company and can be applied to a holding account issued by that company
 *
 * @author manderson
 */
public class BusinessCompanyPlatform extends NamedEntity<Integer> {

	private BusinessCompany businessCompany;

	private Date startDate;

	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
