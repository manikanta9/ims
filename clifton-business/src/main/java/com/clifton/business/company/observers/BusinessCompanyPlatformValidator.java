package com.clifton.business.company.observers;

import com.clifton.business.company.BusinessCompanyPlatform;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class BusinessCompanyPlatformValidator extends SelfRegisteringDaoValidator<BusinessCompanyPlatform> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BusinessCompanyPlatform bean, DaoEventTypes config) throws ValidationException {
		// Updates - cannot change the company the platform is associated with
		if (config.isUpdate()) {
			BusinessCompanyPlatform originalBean = getOriginalBean(bean);
			if (!originalBean.getBusinessCompany().equals(bean.getBusinessCompany())) {
				throw new ValidationException("Cannot change the company for an existing platform.");
			}
		}
		// Inserts/Updates - Validates the company is an issuer and not our company
		ValidationUtils.assertTrue(bean.getBusinessCompany().getType().isIssuer(), "Platforms can only be associated with companies that are issuers.");
		ValidationUtils.assertFalse(bean.getBusinessCompany().getType().isOurCompany(), "Platforms cannot be associated with our companies.");
	}
}
