package com.clifton.business.company.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeSearchForm;


/**
 * @author manderson
 */
public class BusinessCompanyPlatformSearchForm extends BaseAuditableEntityDateRangeSearchForm {

	@SearchField(searchField = "name,description,businessCompany.name")
	private String searchPattern;

	@SearchField(searchField = "businessCompany.id", sortField = "businessCompany.name")
	private Integer businessCompanyId;

	@SearchField(searchFieldPath = "businessCompany", searchField = "type.id", sortField = "type.name")
	private Short businessCompanyTypeId;

	@SearchField
	private String name;

	@SearchField
	private String description;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isIncludeTime() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public Short getBusinessCompanyTypeId() {
		return this.businessCompanyTypeId;
	}


	public void setBusinessCompanyTypeId(Short businessCompanyTypeId) {
		this.businessCompanyTypeId = businessCompanyTypeId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
