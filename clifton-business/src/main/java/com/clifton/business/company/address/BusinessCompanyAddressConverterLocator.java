package com.clifton.business.company.address;

/**
 * <code>BusinessCompanyAddressConverterLocator</code> Loads {@link BusinessCompanyAddressConverter} implementations, stores them in a map based on supported country codes,
 * and fetches the adapter singleton instance when queried by country code.
 */
public interface BusinessCompanyAddressConverterLocator {

	public BusinessCompanyAddressConverter locate(String formattedAddress);
}
