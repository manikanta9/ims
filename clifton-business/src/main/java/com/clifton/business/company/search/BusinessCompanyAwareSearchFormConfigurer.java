package com.clifton.business.company.search;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>BusinessCompanyAwareSearchFormConfigurer</code> provides a custom filtering used by search forms that implement
 * {@link BusinessCompanyAwareSearchForm} to append proper company id filters.  These are used for cases like clients and client relationships where we need to
 * include related companies and/or related client/client relationship companies
 * <p>
 * NOTE: This search form configurer is AUTOMATICALLY registered for any search form that implements {@link BusinessCompanyAwareSearchForm}
 *
 * @author manderson
 */
@Component
public class BusinessCompanyAwareSearchFormConfigurer implements SearchFormCustomConfigurer<BusinessCompanyAwareSearchForm> {

	private BusinessCompanyService businessCompanyService;


	@Override
	public Class<BusinessCompanyAwareSearchForm> getSearchFormInterfaceClassName() {
		return BusinessCompanyAwareSearchForm.class;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria, BusinessCompanyAwareSearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(searchForm.getCompanyPropertyNames()), "Company Property Names must contain at least one property name");

		List<Criterion> orCompanyFilters = new ArrayList<>();

		if (searchForm.getCompanyIdOrRelatedCompany() != null) {
			// Look up the Company to See if it is a Child or a Parent
			BusinessCompany company = getBusinessCompanyService().getBusinessCompany(searchForm.getCompanyIdOrRelatedCompany());
			if (company != null) {
				// It has a parent company, include where company.parent.id = id
				// No way to tell by company type, so just use OR statement
				if (company.getParent() == null) {
					for (String companyPropertyName : searchForm.getCompanyPropertyNames()) {
						orCompanyFilters.add(Restrictions.eq(HibernateUtils.getPathAlias(companyPropertyName + ".parent", criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.LEFT_OUTER_JOIN, processedPathToCriteria) + ".id", company.getId()));
					}
					searchForm.applyAdditionalParentCompanyFilter(criteria, orCompanyFilters, company.getId());
				}
			}
		}
		List<Integer> companyIds = getCompanyIds(searchForm);

		if (!companyIds.isEmpty()) {
			if (companyIds.size() == 1) {
				for (String companyPropertyName : searchForm.getCompanyPropertyNames()) {
					orCompanyFilters.add(Restrictions.eq(companyPropertyName + ".id", companyIds.get(0)));
				}
			}
			else {
				for (String companyPropertyName : searchForm.getCompanyPropertyNames()) {
					orCompanyFilters.add(Restrictions.in(companyPropertyName + ".id", companyIds));
				}
			}
			searchForm.applyAdditionalCompanyFilter(criteria, orCompanyFilters, companyIds);
		}
		if (!CollectionUtils.isEmpty(orCompanyFilters)) {
			if (orCompanyFilters.size() == 1) {
				criteria.add(orCompanyFilters.get(0));
			}
			else {
				Disjunction disjunction = Restrictions.disjunction();
				for (Criterion criterion : orCompanyFilters) {
					disjunction.add(criterion);
				}
				criteria.add(disjunction);
			}
		}
	}


	public List<Integer> getCompanyIds(BusinessCompanyAwareSearchForm searchForm) {
		List<Integer> companyIds = new ArrayList<>();
		if (searchForm.getCompanyId() != null) {
			companyIds.add(searchForm.getCompanyId());
		}
		else if (searchForm.getCompanyIdOrRelatedCompany() != null) {
			// Look up the Company to See if it is a Child or a Parent
			BusinessCompany company = getBusinessCompanyService().getBusinessCompany(searchForm.getCompanyIdOrRelatedCompany());
			if (company != null) {
				// Exact Company Match
				companyIds.add(company.getId());

				// If Company has a parent, then include where company id = company's parent id
				if (company.getParent() != null) {
					companyIds.add(company.getParent().getId());
				}
			}
		}
		return companyIds;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}
}
