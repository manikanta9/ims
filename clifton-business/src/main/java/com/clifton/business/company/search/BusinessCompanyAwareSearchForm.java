package com.clifton.business.company.search;

import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import java.util.List;


/**
 * The <code>BusinessCompanyAwareSearchForm</code> defines methods necessary to use the {@link BusinessCompanyAwareSearchFormConfigurer}
 * NOTE: The Search Form Configurer is AUTOMATICALLY registered for any search form that implements this interface
 * <p>
 * companyId looks for only that company
 * companyIdOrRelatedCompany will look for that company and any parents or children
 * includeClientOrClientRelationship - if the company is a client relationship, will include companies for the clients under that client relationship.  If the company is a client, will look up the client's client relationship company and include it. This field can be implemented by consumers.
 *
 * @author manderson
 */
public interface BusinessCompanyAwareSearchForm {

	/**
	 * Defines the property or properties on the bean to look for the company id values in
	 * i.e. businessCompany.  Can have multiple for cases where the there are 2 company fields (i.e. BusinessCompanyRelationship) and we check or on both fields
	 */
	public String[] getCompanyPropertyNames();


	/**
	 * Allows for some additional custom filters.
	 * Example: Contracts where we want to find where the company is a related party on the contract
	 */
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds);


	/**
	 * Allows for some additional custom filters.
	 * Example: Contracts where we want to find where the company is a related party on the contract
	 */
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getCompanyId();


	public void setCompanyId(Integer companyId);


	public Integer getCompanyIdOrRelatedCompany();


	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany);


	public Boolean getIncludeClientOrClientRelationship();


	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship);
}
