package com.clifton.business.company.search;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


public class CompanySearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] idList;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "name,alias")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String abbreviation;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField
	private String companyLegalName;

	@SearchField
	private String alternateCompanyName;

	@SearchField
	private String alias;

	@SearchField(searchField = "type.id")
	private Short companyTypeId;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.EQUALS)
	private String companyType;

	@SearchField(searchField = "name", searchFieldPath = "type")
	private String[] companyTypeNames;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = ComparisonConditions.NOT_IN)
	private String[] excludeCompanyTypeNames;

	@SearchField(searchField = "tradingAllowed", searchFieldPath = "type")
	private Boolean tradingAllowed;

	@SearchField(searchField = "contractAllowed", searchFieldPath = "type")
	private Boolean contractAllowed;

	@SearchField(searchField = "moneyManager", searchFieldPath = "type")
	private Boolean moneyManager;

	@SearchField(searchField = "issuer", searchFieldPath = "type")
	private Boolean issuer;

	@SearchField(searchField = "abbreviationRequired", searchFieldPath = "type")
	private Boolean abbreviationRequired;

	@SearchField(searchField = "systemDefined", searchFieldPath = "type")
	private Boolean systemDefined;

	@SearchField(searchField = "ourCompany", searchFieldPath = "type")
	private Boolean ourCompany;

	@SearchField(searchField = "ourCompany", searchFieldPath = "parent.type")
	private Boolean parentOurCompany;

	@SearchField(searchField = "city,state", sortField = "city")
	private String cityState;

	@SearchField
	private String phoneNumber;

	@SearchField
	private String faxNumber;

	@SearchField
	private String efaxNumber;

	@SearchField
	private String emailAddress;

	@SearchField
	private String addressLine1;
	@SearchField
	private String addressLine2;
	@SearchField
	private String addressLine3;
	@SearchField
	private String city;
	@SearchField
	private String state;
	@SearchField
	private String postalCode;
	@SearchField
	private String country;

	@SearchField
	private String businessIdentifierCode;

	@SearchField(searchField = "businessIdentifierCode", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean businessIdentifierCodeNotNull;

	@SearchField
	private String dtcNumber;

	@SearchField(searchField = "dtcNumber", comparisonConditions = ComparisonConditions.EQUALS)
	private String dtcNumberEquals;

	@SearchField
	private String fedMnemonic;

	@SearchField
	private String legalEntityIdentifier;

	@SearchField(searchField = "bloombergCompanyIdentifier", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean bloombergCompanyIdentifierNotNull;

	@SearchField
	private Integer bloombergCompanyIdentifier;

	// The contract fields below are Implemented by the consumers of the search form
	// Custom Search Filter
	private String contractTypeName;

	// Custom Search Filter
	private String contractCategoryName;

	// Custom Search Filter
	private String contractPartyRoleName;

	// Custom Search Filter
	private Integer contractInvestmentAccountId;

	// Custom search filter - Inactive contracts are those that have WorkflowStatus.Name as Non-Operational
	// Must be used with one of the other contract filters
	private Boolean contractActive;

	// Custom Search Filter - usually the client's company used only if contractInvestmentAccountId is not used
	private Integer contractCompanyId;

	// Custom Search Filter - WHERE EXISTS - Company is an issuing company of an account of selected type and that account is currently "Active"
	private Short activeIssuerInvestmentAccountTypeId;

	// Custom Search field that does a where exists in the BusinessCompanyRelationship table where relationship name = given name and this company = related company or main company
	private String companyRelationshipExists;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return BusinessCompany.BUSINESS_COMPANY_TABLE_NAME;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer[] getIdList() {
		return this.idList;
	}


	public void setIdList(Integer[] idList) {
		this.idList = idList;
	}


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public String getCompanyType() {
		return this.companyType;
	}


	public void setCompanyType(String companyType) {
		this.companyType = companyType;
	}


	public String[] getCompanyTypeNames() {
		return this.companyTypeNames;
	}


	public void setCompanyTypeNames(String[] companyTypeNames) {
		this.companyTypeNames = companyTypeNames;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getMoneyManager() {
		return this.moneyManager;
	}


	public void setMoneyManager(Boolean moneyManager) {
		this.moneyManager = moneyManager;
	}


	public String getCityState() {
		return this.cityState;
	}


	public void setCityState(String cityState) {
		this.cityState = cityState;
	}


	public Short getCompanyTypeId() {
		return this.companyTypeId;
	}


	public void setCompanyTypeId(Short companyTypeId) {
		this.companyTypeId = companyTypeId;
	}


	public String getAddressLine1() {
		return this.addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return this.addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getAddressLine3() {
		return this.addressLine3;
	}


	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}


	public String getCity() {
		return this.city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return this.state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getPostalCode() {
		return this.postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public Boolean getIssuer() {
		return this.issuer;
	}


	public void setIssuer(Boolean issuer) {
		this.issuer = issuer;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getContractTypeName() {
		return this.contractTypeName;
	}


	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}


	public String getContractPartyRoleName() {
		return this.contractPartyRoleName;
	}


	public void setContractPartyRoleName(String contractPartyRoleName) {
		this.contractPartyRoleName = contractPartyRoleName;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getFaxNumber() {
		return this.faxNumber;
	}


	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}


	public String getEfaxNumber() {
		return this.efaxNumber;
	}


	public void setEfaxNumber(String efaxNumber) {
		this.efaxNumber = efaxNumber;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	public String getCompanyLegalName() {
		return this.companyLegalName;
	}


	public void setCompanyLegalName(String companyLegalName) {
		this.companyLegalName = companyLegalName;
	}


	public String getAlternateCompanyName() {
		return this.alternateCompanyName;
	}


	public void setAlternateCompanyName(String alternateCompanyName) {
		this.alternateCompanyName = alternateCompanyName;
	}


	public Integer getContractInvestmentAccountId() {
		return this.contractInvestmentAccountId;
	}


	public void setContractInvestmentAccountId(Integer contractInvestmentAccountId) {
		this.contractInvestmentAccountId = contractInvestmentAccountId;
	}


	public Boolean getContractAllowed() {
		return this.contractAllowed;
	}


	public void setContractAllowed(Boolean contractAllowed) {
		this.contractAllowed = contractAllowed;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getOurCompany() {
		return this.ourCompany;
	}


	public void setOurCompany(Boolean ourCompany) {
		this.ourCompany = ourCompany;
	}


	public String getContractCategoryName() {
		return this.contractCategoryName;
	}


	public void setContractCategoryName(String contractCategoryName) {
		this.contractCategoryName = contractCategoryName;
	}


	public Integer getContractCompanyId() {
		return this.contractCompanyId;
	}


	public void setContractCompanyId(Integer contractCompanyId) {
		this.contractCompanyId = contractCompanyId;
	}


	public Boolean getContractActive() {
		return this.contractActive;
	}


	public void setContractActive(Boolean contractActive) {
		this.contractActive = contractActive;
	}


	public String getAbbreviation() {
		return this.abbreviation;
	}


	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}


	public Boolean getTradingAllowed() {
		return this.tradingAllowed;
	}


	public void setTradingAllowed(Boolean tradingAllowed) {
		this.tradingAllowed = tradingAllowed;
	}


	public Short getActiveIssuerInvestmentAccountTypeId() {
		return this.activeIssuerInvestmentAccountTypeId;
	}


	public void setActiveIssuerInvestmentAccountTypeId(Short activeIssuerInvestmentAccountTypeId) {
		this.activeIssuerInvestmentAccountTypeId = activeIssuerInvestmentAccountTypeId;
	}


	public Boolean getAbbreviationRequired() {
		return this.abbreviationRequired;
	}


	public void setAbbreviationRequired(Boolean abbreviationRequired) {
		this.abbreviationRequired = abbreviationRequired;
	}


	public String getBusinessIdentifierCode() {
		return this.businessIdentifierCode;
	}


	public void setBusinessIdentifierCode(String businessIdentifierCode) {
		this.businessIdentifierCode = businessIdentifierCode;
	}


	public Boolean getBusinessIdentifierCodeNotNull() {
		return this.businessIdentifierCodeNotNull;
	}


	public void setBusinessIdentifierCodeNotNull(Boolean businessIdentifierCodeNotNull) {
		this.businessIdentifierCodeNotNull = businessIdentifierCodeNotNull;
	}


	public String getDtcNumber() {
		return this.dtcNumber;
	}


	public void setDtcNumber(String dtcNumber) {
		this.dtcNumber = dtcNumber;
	}


	public String getDtcNumberEquals() {
		return this.dtcNumberEquals;
	}


	public void setDtcNumberEquals(String dtcNumberEquals) {
		this.dtcNumberEquals = dtcNumberEquals;
	}


	public String getFedMnemonic() {
		return this.fedMnemonic;
	}


	public void setFedMnemonic(String fedMnemonic) {
		this.fedMnemonic = fedMnemonic;
	}


	public String getLegalEntityIdentifier() {
		return this.legalEntityIdentifier;
	}


	public void setLegalEntityIdentifier(String legalEntityIdentifier) {
		this.legalEntityIdentifier = legalEntityIdentifier;
	}


	public Boolean getBloombergCompanyIdentifierNotNull() {
		return this.bloombergCompanyIdentifierNotNull;
	}


	public void setBloombergCompanyIdentifierNotNull(Boolean bloombergCompanyIdentifierNotNull) {
		this.bloombergCompanyIdentifierNotNull = bloombergCompanyIdentifierNotNull;
	}


	public Integer getBloombergCompanyIdentifier() {
		return this.bloombergCompanyIdentifier;
	}


	public void setBloombergCompanyIdentifier(Integer bloombergCompanyIdentifier) {
		this.bloombergCompanyIdentifier = bloombergCompanyIdentifier;
	}


	public Boolean getParentOurCompany() {
		return this.parentOurCompany;
	}


	public void setParentOurCompany(Boolean parentOurCompany) {
		this.parentOurCompany = parentOurCompany;
	}


	public String getCompanyRelationshipExists() {
		return this.companyRelationshipExists;
	}


	public void setCompanyRelationshipExists(String companyRelationshipExists) {
		this.companyRelationshipExists = companyRelationshipExists;
	}


	public String[] getExcludeCompanyTypeNames() {
		return this.excludeCompanyTypeNames;
	}


	public void setExcludeCompanyTypeNames(String[] excludeCompanyTypeNames) {
		this.excludeCompanyTypeNames = excludeCompanyTypeNames;
	}
}
