package com.clifton.business.company;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>BusinessCompanyRelationship</code> is used to maintain representative,
 * referral, consultants, etc. on a company - company basis.  Similar to the
 * <code>BusinessContactRelationship</code> table, however this table connects {@link BusinessCompany}s.
 *
 * @author manderson
 */
public class BusinessCompanyRelationship extends ManyToManyEntity<BusinessCompany, BusinessCompany, Integer> {

	/**
	 * Relationship used to create a parent entity on the portal.  The most common use case is an OCIO relationship, where the OCIO has control over
	 * its child relationships.  Users can be assigned to the parent, and would automatically inherit the child relationships, clients, and accounts.
	 * These should be added with caution as they can affect users file access to their children.
	 * The parent can apply to Client Relationships and Sister Client Groups or Sister Clients (no current parent applied).
	 * Where the Main Company is the Client Relationship, etc. and the Related company is the Parent/OCIO
	 */
	public static final String RELATIONSHIP_NAME_PORTAL_RELATIONSHIP = "Portal Relationship";


	private String name;

	/**
	 * Used to exclude displaying companies on reports
	 * i.e. CFS - may have company relationships that we know about but we don't want them displayed on the CFS report
	 */
	private boolean privateRelationship;

	/**
	 * Used to track identification codes issued
	 * Examples: Private Fund, NFA, Large Trader, LEI, SEC File Number
	 * So, if the SEC issues an identification code for a client, a relationship to the SEC company would be added for that client
	 * with the corresponding code type and code.
	 */
	private String identificationCodeType;
	private String identificationCode;

	private Date startDate;
	private Date endDate;

	private String description;


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public boolean isPrivateRelationship() {
		return this.privateRelationship;
	}


	public void setPrivateRelationship(boolean privateRelationship) {
		this.privateRelationship = privateRelationship;
	}


	public String getIdentificationCodeType() {
		return this.identificationCodeType;
	}


	public void setIdentificationCodeType(String identificationCodeType) {
		this.identificationCodeType = identificationCodeType;
	}


	public String getIdentificationCode() {
		return this.identificationCode;
	}


	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
