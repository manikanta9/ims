package com.clifton.business.company.address;

import com.clifton.core.beans.common.AddressObject;


/**
 * <code>BusinessCompanyAddressConverter</code> Accepts a specially formatted bloomberg address (;2;6;1;1;One South Street;1; ;1;Baltimore;1;MD;1;21202;1;US;),
 * attempts to identify address components, with an emphasis on United States, Canada, and Austria addresses with city, state, and postal code elements,
 * and loads the address into an {@link AddressObject}.  Any errors or warnings will be included in the provided status object.
 */
public interface BusinessCompanyAddressConverter {

	public AddressObject convertAddress(String formattedAddress);


	public boolean supports(String formattedAddress);


	public int order();
}
