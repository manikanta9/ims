package com.clifton.business.company.address.bloomberg;

import com.clifton.business.company.address.BusinessCompanyAddressConverter;
import com.clifton.business.company.address.BusinessCompanyAddressUtilHandler;
import com.clifton.core.beans.common.AddressEntity;
import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * <code>CompanyAddressCityStatePostalAdapter</code> United States, Canada and Australia, will typically use all six components.
 * 1: address line 1
 * 2: address line 2
 * 3: city
 * 4: state
 * 5: postal code
 * 6: country
 * The emphasis for this adapter is parsing and identifying the postal code component, it gives hints to
 * what is in the first components.  Another hint is the number of components used, typically when six
 * components are used it has the format given above, however, this is not always the case since the
 * address line components could use city and state components and force the actual city and state to
 * be packed into the postal code.
 */
@Component
public class BloombergCityStatePostalConverter implements BusinessCompanyAddressConverter {

	private static final List<String> supportedCountryCodes = Arrays.asList("US", "CA", "AU");

	private static final List<String> unitedStatesRegularExpressions = Arrays.asList(
			"^(?<zip>\\d{5}(?:[-\\s]\\d{4})?)$",
			"^US\\-(?<state>REPLACEMENT)\\s+(?<zip>\\d{5}(?:[-\\s]\\d{4})?)$",
			"^(?<city>[\\w\\s\\-]+),\\s+US\\-(?<state>REPLACEMENT)\\s+(?<zip>\\d{5}(?:[-\\s]\\d{4})?)$"
	);
	private static final List<String> canadaRegularExpressions = Arrays.asList(
			"^(?<zip>[A-Y&&[^DFIOQUWZ]][0-9][A-Z&&[^DFIOQU]] ?[0-9][A-Z&&[^DFIOQU]][0-9])$",
			"^(?<city>[\\w\\s\\-']+),\\s+(?<zip>[A-Z&&[^DFIOQUWZ]][0-9][A-Z&&[^DFIOQU]] ?[0-9][A-Z&&[^DFIOQU]][0-9])$",
			"^(?<city>[\\w\\s\\-']+),\\s+CA\\-(?<state>REPLACEMENT)\\s+(?<zip>[A-Z&&[^DFIOQUWZ]][0-9][A-Z&&[^DFIOQU]] ?[0-9][A-Z&&[^DFIOQU]][0-9])$"
	);
	private static final List<String> australiaRegularExpressions = Arrays.asList(
			"^(?<zip>[0-9]{4})$",
			"^(?<city>[\\w\\s\\-']+),\\s+(?<zip>[0-9]{4})$",
			"^(?<city>[\\w\\s\\-']+),\\s+AU\\-(?<state>REPLACEMENT)\\s+(?<zip>[0-9]{4})$",
			"^(?<state>REPLACEMENT)\\s+(?<zip>[0-9]{4})$"
	);
	private static AtomicReference<Map<String, List<Pattern>>> postalComponentPatternsMap = new AtomicReference<>(null);

	private BusinessCompanyAddressUtilHandler businessCompanyAddressUtilHandler;
	private BloombergParserHandler bloombergParserHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AddressObject convertAddress(String formattedAddress) {
		if (!StringUtils.isEmpty(formattedAddress)) {
			Deque<String> addressComponents = new LinkedList<>(getBloombergParserHandler().splitIntoAddressComponents(formattedAddress));
			if (!addressComponents.isEmpty()) {
				final AddressObject addressObject = new AddressEntity();

				final String country = addressComponents.peekLast();
				if (!populateFromCountryComponent(country, addressObject)) {
					populateAddressLines(addressComponents, addressObject);
					return addressObject;
				}

				addressComponents.removeLast();
				final String postal = addressComponents.peekLast();
				if (populateFromPostalComponent(postal, addressObject)) {
					addressComponents.removeLast();
				}

				final String state = addressComponents.peekLast();
				if (populateFromStateComponent(state, addressObject)) {
					addressComponents.removeLast();
				}

				final String city = addressComponents.peekLast();
				if (!StringUtils.isEmpty(city) && StringUtils.isEmpty(addressObject.getCity()) && !addressComponents.isEmpty()) {
					if (populateFromCityComponent(city, addressObject)) {
						addressComponents.removeLast();
					}
				}
				populateAddressLines(addressComponents, addressObject);
				return addressObject;
			}
		}
		return null;
	}


	@Override
	public boolean supports(String formattedAddress) {
		if (getBloombergParserHandler().recognizesAddressFormat(formattedAddress)) {
			List<String> components = getBloombergParserHandler().splitIntoAddressComponents(formattedAddress);
			String countryCode = getBloombergParserHandler().getCountryCode(components);
			return supportedCountryCodes.contains(countryCode);
		}
		return false;
	}


	@Override
	public int order() {
		return 10;
	}


	public boolean populateFromCountryComponent(String country, AddressObject addressObject) {
		if (!StringUtils.isEmpty(country)) {
			String countryCode = getBusinessCompanyAddressUtilHandler().getCountryCode(country);
			if (!StringUtils.isEmpty(countryCode)) {
				addressObject.setCountry(countryCode);
				return true;
			}
		}
		return false;
	}


	/**
	 * Go through all the postal code parses, the first one that recognizes the pattern will populate the address object
	 * and will break.
	 */
	public boolean populateFromPostalComponent(String postal, AddressObject addressObject) {
		if (!StringUtils.isEmpty(postal)) {
			List<Pattern> patterns = getPostalComponentPatterns(addressObject.getCountry());
			String trimmed = StringUtils.trim(postal).toUpperCase();
			Optional<Pattern> matched = patterns
					.stream()
					.filter(p -> {
						Matcher m = p.matcher(trimmed);
						return m.matches();
					})
					.findFirst();
			if (matched.isPresent()) {
				Matcher matcher = matched.get().matcher(trimmed);
				if (matcher.matches()) {
					Optional<String> optionalCity = getGroup(matcher, "city");
					if (optionalCity.isPresent()) {
						String city = StringUtils.isEmpty(optionalCity.get()) ? "" : optionalCity.get().toLowerCase();
						city = Arrays.stream(city.split("\\s"))
								.map(StringUtils::trim)
								.map(StringUtils::capitalize)
								.collect(Collectors.joining(" "));
						addressObject.setCity(city);
					}
					Optional<String> optionalState = getGroup(matcher, "state");
					optionalState.ifPresent(addressObject::setState);
					Optional<String> optionalZipCode = getGroup(matcher, "zip");
					if (optionalZipCode.isPresent()) {
						String zipCode = StringUtils.isEmpty(optionalZipCode.get()) ? "" : optionalZipCode.get().toLowerCase();
						addressObject.setPostalCode(zipCode);
					}
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Populate the address state if the String in the state component matches known states.
	 */
	public boolean populateFromStateComponent(String state, AddressObject addressObject) {
		if (!StringUtils.isEmpty(state)) {
			String stateCode = getBusinessCompanyAddressUtilHandler().getStateCode(state);
			if (!StringUtils.isEmpty(stateCode)) {
				addressObject.setState(stateCode);
				return true;
			}
			String provinceCode = getBusinessCompanyAddressUtilHandler().getProvinceCode(state);
			if (!StringUtils.isEmpty(provinceCode)) {
				addressObject.setState(provinceCode);
				return true;
			}
			String territoryCode = getBusinessCompanyAddressUtilHandler().getTerritoryCode(state);
			if (!StringUtils.isEmpty(territoryCode)) {
				addressObject.setState(territoryCode);
				return true;
			}
		}
		return false;
	}


	/**
	 * If this was not 'US-DE 19808' and the postal code and state are filled in, then is is most likely
	 * the city.
	 */
	public boolean populateFromCityComponent(String city, AddressObject addressObject) {
		if (!StringUtils.isEmpty(city)) {
			if (!(StringUtils.isEmpty(addressObject.getPostalCode()) || StringUtils.isEmpty(addressObject.getState()))) {
				addressObject.setCity(city);
				return true;
			}
		}
		return false;
	}


	/**
	 * Pack the address components the address lines, shouldn't have more than six components to put into address lines 1..3.
	 * Address lines 1..2 correspond to components 1..2, the remains components are concatenated together and put in address
	 * line 3.
	 */
	public void populateAddressLines(Deque<String> addressComponents, AddressObject addressObject) {
		addressObject.setAddressLine1(addressComponents.poll());
		addressObject.setAddressLine2(addressComponents.poll());
		addressObject.setAddressLine3(String.join(" ", addressComponents));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Pattern> getPostalComponentPatterns(String countryCode) {
		postalComponentPatternsMap.compareAndSet(null, createPostalComponentMap());
		return postalComponentPatternsMap.get().get(countryCode);
	}


	private Map<String, List<Pattern>> createPostalComponentMap() {
		Map<String, List<Pattern>> results = new HashMap<>();
		List<Pattern> usaPatterns = new ArrayList<>();
		String replacement = String.join("|", getBusinessCompanyAddressUtilHandler().getStateCodes());
		for (String pattern : unitedStatesRegularExpressions) {
			usaPatterns.add(Pattern.compile(pattern.replaceFirst("REPLACEMENT", replacement)));
		}
		results.put("US", usaPatterns);
		List<Pattern> canadaPatterns = new ArrayList<>();
		replacement = String.join("|", getBusinessCompanyAddressUtilHandler().getProvinceCodes());
		for (String pattern : canadaRegularExpressions) {
			canadaPatterns.add(Pattern.compile(pattern.replaceFirst("REPLACEMENT", replacement)));
		}
		results.put("CA", canadaPatterns);
		List<Pattern> australiaPatterns = new ArrayList<>();
		replacement = String.join("|", getBusinessCompanyAddressUtilHandler().getTerritoryCodes());
		for (String pattern : australiaRegularExpressions) {
			australiaPatterns.add(Pattern.compile(pattern.replaceFirst("REPLACEMENT", replacement)));
		}
		results.put("AU", australiaPatterns);
		return results;
	}


	private Optional<String> getGroup(Matcher matcher, String group) {
		Optional<String> value = Optional.empty();
		try {
			value = Optional.of(matcher.group(group));
		}
		catch (IllegalArgumentException e) {
			// swallow this
		}
		return value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyAddressUtilHandler getBusinessCompanyAddressUtilHandler() {
		return this.businessCompanyAddressUtilHandler;
	}


	public void setBusinessCompanyAddressUtilHandler(BusinessCompanyAddressUtilHandler businessCompanyAddressUtilHandler) {
		this.businessCompanyAddressUtilHandler = businessCompanyAddressUtilHandler;
	}


	public BloombergParserHandler getBloombergParserHandler() {
		return this.bloombergParserHandler;
	}


	public void setBloombergParserHandler(BloombergParserHandler bloombergParserHandler) {
		this.bloombergParserHandler = bloombergParserHandler;
	}
}
