package com.clifton.business.company.address.bloomberg;

import com.clifton.business.company.address.BusinessCompanyAddressConverter;
import com.clifton.core.beans.common.AddressEntity;
import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Format Six addresses always start with ';2;6;1;'.  This position within the address string is always the format designator and is in the range 1..6.
 * <p>
 * <code>CompanyAddressFormatSixAdapter</code> Makes the assumption that the address component to address object fields are one-to-one:
 * <p>
 * {@link AddressObject#setAddressLine1(String)} <- component 1
 * {@link AddressObject#setAddressLine2(String)} <- component 2
 * {@link AddressObject#setCity(String)} <- component 3
 * {@link AddressObject#setState(String)} <- component 4
 * {@link AddressObject#setPostalCode(String)} <- component 5
 * {@link AddressObject#setCountry(String)} <- component 6
 * <p>
 * The postal component will be checked for the presence of a comma, when present, the comma is assumed to separate city and postal code, however,
 * no assumption can be made about the remainder of components, they will be placed in address lines 1..3.
 */
@Component
public class BloombergFormatSixConverter implements BusinessCompanyAddressConverter {

	private BloombergParserHandler bloombergParserHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AddressObject convertAddress(String formattedAddress) {
		AddressObject addressObject = new AddressEntity();
		Deque<String> addressComponents = new LinkedList<>(getBloombergParserHandler().splitIntoAddressComponents(formattedAddress));

		populateFromCountryComponent(addressComponents.pollLast(), addressObject);
		populateFromPostalComponent(addressComponents.pollLast(), addressObject);
		if (StringUtils.isEmpty(addressObject.getCity())) {
			populateFromCityComponent(addressComponents.pollLast(), addressObject);
			populateFromStateComponent(addressComponents.pollLast(), addressObject);
		}
		populateAddressLines(addressComponents, addressObject);

		return addressObject;
	}


	@Override
	public boolean supports(String formattedAddress) {
		if (getBloombergParserHandler().recognizesAddressFormat(formattedAddress)) {
			List<String> components = getBloombergParserHandler().splitIntoAddressComponents(formattedAddress);
			return components.size() == 6;
		}
		return false;
	}


	@Override
	public int order() {
		return 20;
	}


	public boolean populateFromCountryComponent(String country, AddressObject addressObject) {
		addressObject.setCountry(country);
		return true;
	}


	public boolean populateFromPostalComponent(String postal, AddressObject addressObject) {
		if (!StringUtils.isEmpty(postal)) {
			if (postal.indexOf(',') > -1) {
				Deque<String> parts = Arrays.stream(postal.split(","))
						.map(StringUtils::trim)
						.filter(l -> !StringUtils.isEmpty(l))
						.collect(Collectors.toCollection(LinkedList::new));
				addressObject.setCity(parts.pop());
				addressObject.setPostalCode(parts.pop());
			}
			else {
				addressObject.setPostalCode(postal);
			}
		}
		return true;
	}


	public boolean populateFromStateComponent(String state, AddressObject addressObject) {
		addressObject.setState(state);
		return true;
	}


	public boolean populateFromCityComponent(String city, AddressObject addressObject) {
		addressObject.setCity(city);
		return true;
	}


	public void populateAddressLines(Deque<String> addressComponents, AddressObject addressObject) {
		addressObject.setAddressLine1(addressComponents.poll());
		addressObject.setAddressLine2(addressComponents.poll());
		addressObject.setAddressLine3(String.join(" ", addressComponents));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergParserHandler getBloombergParserHandler() {
		return this.bloombergParserHandler;
	}


	public void setBloombergParserHandler(BloombergParserHandler bloombergParserHandler) {
		this.bloombergParserHandler = bloombergParserHandler;
	}
}
