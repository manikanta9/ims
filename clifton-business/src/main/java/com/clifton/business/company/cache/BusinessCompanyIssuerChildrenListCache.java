package com.clifton.business.company.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;


/**
 * The <code>BusinessCompanyChildrenListCache</code> caches list of child business companies for a given business company.  The list includes grand children, great grand children, etc.
 *
 * @author JasonS
 */
public interface BusinessCompanyIssuerChildrenListCache {

	public Integer[] getBusinessCompanyIssuerChildrenList(int businessCompanyId, ReadOnlyDAO<BusinessCompany> businessCompanyDAO);

}
