package com.clifton.business.company.address.bloomberg;

import java.util.List;


/**
 * <code>BloombergParserHandler</code> take a (proprietary) formatted single line address string and parse it into individual address parts.
 * A typical Bloomberg address has a proprietary format similiar to the following:
 * ;2;6;1;1;300 Madison Avenue;1; ;1;Toledo;1;OH;1;43604;1;United States;
 */
public interface BloombergParserHandler {

	/**
	 * Accept a single-line address string and determine if it is in the proprietary Bloomberg address format similiar to:
	 * ;2;6;1;1;300 Madison Avenue;1; ;1;Toledo;1;OH;1;43604;1;United States;
	 */
	public boolean recognizesAddressFormat(String formattedAddress);


	/**
	 * Given a Bloomberg formatted address remove delimiters (1;) and return the individual parsed-out address components.
	 */
	public List<String> splitIntoAddressComponents(String formattedAddress);


	/**
	 * The address component count can number from 1 - 6, this can be parsed from the beginning of the address string ';2;6;...'.
	 * 6 components can parse to individual address portions:
	 * 1: 6600 North Military Trail
	 * 2:
	 * 3: Boca Raton
	 * 4: FL
	 * 5: 33496-2434
	 * 6: US
	 * or:
	 * 1: C/O Corporate Creations Network Inc.
	 * 2: Rodney Building
	 * 3: 3411 Silverside Road
	 * 4: Suite 104
	 * 5: Wilmington, US-DE 19810
	 * 6: United States
	 */
	public int getComponentCount(String addressString);


	/**
	 * The last component is always assumed to be the country code, it is formatted using the ISO 3166 two-letter country code or the corresponding display label.
	 */
	public String getCountryCode(List<String> components);
}
