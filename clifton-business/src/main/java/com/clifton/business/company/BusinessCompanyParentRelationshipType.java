package com.clifton.business.company;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * <code>BusinessCompanyParentRelationshipType</code> used by {@link BusinessCompany} to indicate what type of relationship it has to its parent.
 * Some Business Company Parent Relationship Types include:
 * <ul>
 * <li>Subsidiary - Used to link a company that is directly owned by another company by greater than 50%.</li>
 * <li>Affiliated Entity - Companies that had a definite relationship with another but did not meet the requirement for subsidiary, and assets involved in M&A deals.</li>
 * <li>Portfolio Company - Links portfolio companies to their private equity firm.</li>
 * <li>Synthetic Parent - Used when a legal entity is linked to a synthetic (non-legal entity) to group it together under one corporate tree.  Non-legal entities include generic mortgage shelf bids and groups.</li>
 * <li>Fund Entity - Links to a fund to its fund manager.</li>
 * </ul>
 */
@CacheByName
public class BusinessCompanyParentRelationshipType extends NamedEntity<Short> {

	// NOTHING HERE FOR NOW
}
