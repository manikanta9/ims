package com.clifton.business.company.address;

import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * @see BusinessCompanyAddressConverterLocator
 */
@Component
public class BusinessCompanyAddressConverterLocatorImpl implements BusinessCompanyAddressConverterLocator, InitializingBean, ApplicationContextAware {

	private List<BusinessCompanyAddressConverter> parserList = new ArrayList<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, BusinessCompanyAddressConverter> beanMap = getApplicationContext().getBeansOfType(BusinessCompanyAddressConverter.class);
		if (!CollectionUtils.isEmpty(beanMap)) {
			List<BusinessCompanyAddressConverter> results = new ArrayList<>(beanMap.values());
			setParserList(results);
			getParserList().sort(Comparator.comparingInt(BusinessCompanyAddressConverter::order));
		}
	}


	@Override
	public BusinessCompanyAddressConverter locate(String formattedAddress) {
		Optional<BusinessCompanyAddressConverter> optionalMatch = getParserList().stream().filter(p -> p.supports(formattedAddress)).findFirst();
		return optionalMatch.orElse(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public List<BusinessCompanyAddressConverter> getParserList() {
		return this.parserList;
	}


	public void setParserList(List<BusinessCompanyAddressConverter> parserList) {
		this.parserList = parserList;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
