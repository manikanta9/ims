package com.clifton.business.company;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class BusinessCompanyUtilHandlerImpl implements BusinessCompanyUtilHandler {

	private BusinessCompanyService businessCompanyService;
	private SystemColumnValueService systemColumnValueService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getParametricBusinessIdentifierCode() {
		return getBusinessCompanyService().getBusinessCompanyByTypeAndName(PPA_MINNEAPOLIS_COMPANY_TYPE, PPA_MINNEAPOLIS_COMPANY_NAME).getBusinessIdentifierCode();
	}


	@Override
	public BusinessCompany getIntermediaryCustodian(BusinessCompany company) {
		try {
			SystemColumnValue bicValue = getSystemColumnValue(company.getId(), BUSINESS_INTERMEDIARY_CUSTODIAN_FIELD);

			if (!StringUtils.isEmpty(bicValue.getValue()) && SystemDataType.INTEGER.equals(bicValue.getColumn().getDataType().getName())) {
				return getBusinessCompanyService().getBusinessCompany(Integer.valueOf(bicValue.getValue()));
			}
			return null;
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to find Intermediary Custodian for [" + company.getLabel() + "].", e);
		}
	}


	@Override
	public String getIntermediaryCustodianAccountNumber(BusinessCompany company) {
		try {
			SystemColumnValue bicValue = getSystemColumnValue(company.getId(), BUSINESS_INTERMEDIARY_CUSTODIAN_ACCOUNT_NUMBER_FIELD);
			return bicValue.getValue();
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to find Intermediary Custodian Account Number for [" + company.getLabel() + "].", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemColumnValue getSystemColumnValue(int companyId, String fieldName) {
		SystemColumnValueSearchForm columnValueSearchForm = new SystemColumnValueSearchForm();
		columnValueSearchForm.setColumnGroupName(COMPANY_CUSTOM_FIELDS);
		columnValueSearchForm.setColumnName(fieldName);
		columnValueSearchForm.setFkFieldId(companyId);
		return CollectionUtils.getOnlyElementStrict(getSystemColumnValueService().getSystemColumnValueList(columnValueSearchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}
}
