package com.clifton.business.company.address.bloomberg;

import com.clifton.business.company.address.BusinessCompanyAddressUtilHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * @see BloombergParserHandler
 */
@Component
public class BloombergParserHandlerImpl implements BloombergParserHandler {

	// ;2;6;1;1;300 Madison Avenue;1; ;1;Toledo;1;OH;1;43604;1;United States;
	private static final Pattern entire = Pattern.compile("^;2;(?<format>[1-6]);1;(?<components>(1;.*;)*)$");
	private static final Pattern component = Pattern.compile("1;(?<component>.*?);");

	private BusinessCompanyAddressUtilHandler businessCompanyAddressUtilHandler;


	@Override
	public boolean recognizesAddressFormat(String formattedAddress) {
		if (!StringUtils.isEmpty(formattedAddress)) {
			Matcher matcher = entire.matcher(formattedAddress);
			return matcher.matches();
		}
		return false;
	}


	@Override
	public List<String> splitIntoAddressComponents(String formattedAddress) {
		List<String> components = new ArrayList<>();
		if (!StringUtils.isEmpty(formattedAddress)) {
			Matcher matcher = entire.matcher(formattedAddress);
			ValidationUtils.assertTrue(matcher.matches(), "Input Address does not match known format.");
			String componentPortion = matcher.group("components");
			matcher = component.matcher(componentPortion);
			ValidationUtils.assertTrue(matcher.matches(), "Input Address cannot be split into its component parts.");
			matcher.reset();
			while (matcher.find()) {
				components.add(matcher.group("component"));
			}
			components = components
					.stream()
					.peek(StringUtils::trim)
					.filter(a -> !StringUtils.isEmpty(a))
					.collect(Collectors.toList());
		}
		return components;
	}


	@Override
	public int getComponentCount(String addressString) {
		if (!StringUtils.isEmpty(addressString)) {
			Matcher matcher = entire.matcher(addressString);
			if (matcher.matches()) {
				String componentCount = matcher.group("format");
				if (MathUtils.isNumber(componentCount)) {
					try {
						return Integer.parseInt(componentCount);
					}
					catch (NumberFormatException e) {
						// suppress.
					}
				}
			}
		}
		return -1;
	}


	/**
	 * This is the only thing that is 'guaranteed' to exist is the country code.
	 * However, there are addresses that are non-null but parse to nothing:
	 * ;2;6;1;1; ;1; ;1; ;1; ;1; ;1; ;
	 */
	@Override
	public String getCountryCode(List<String> components) {
		if (!CollectionUtils.isEmpty(components)) {
			String countryCode = components.get(components.size() - 1);
			return getBusinessCompanyAddressUtilHandler().getCountryCode(countryCode);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyAddressUtilHandler getBusinessCompanyAddressUtilHandler() {
		return this.businessCompanyAddressUtilHandler;
	}


	public void setBusinessCompanyAddressUtilHandler(BusinessCompanyAddressUtilHandler businessCompanyAddressUtilHandler) {
		this.businessCompanyAddressUtilHandler = businessCompanyAddressUtilHandler;
	}
}
