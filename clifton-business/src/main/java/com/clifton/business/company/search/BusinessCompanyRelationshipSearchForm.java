package com.clifton.business.company.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import java.util.List;


/**
 * The <code>BusinessCompanyRelationshipSearchForm</code> ...
 *
 * @author manderson
 */
public class BusinessCompanyRelationshipSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm implements BusinessCompanyAwareSearchForm {

	@SearchField(searchFieldPath = "referenceOne", searchField = "type.id", sortField = "type.name")
	private Short referenceOneTypeId;

	@SearchField(searchField = "referenceOne.id", sortField = "referenceOne.name")
	private Integer referenceOneId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "name")
	private String referenceOneName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "type.id", sortField = "type.name")
	private Short referenceTwoTypeId;

	@SearchField(searchField = "referenceTwo.id", sortField = "referenceTwo.name")
	private Integer referenceTwoId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String referenceTwoName;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyId;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyIdOrRelatedCompany;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Boolean includeClientOrClientRelationship;


	@SearchField
	private String name;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String nameEquals;

	@SearchField(searchField = "name", comparisonConditions = ComparisonConditions.IN)
	private String[] names;

	@SearchField
	private Boolean privateRelationship;

	@SearchField
	private String identificationCodeType;

	@SearchField
	private String identificationCode;

	@SearchField
	private String description;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getCompanyPropertyNames() {
		return new String[]{"referenceOne", "referenceTwo"};
	}


	@Override
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds) {
		// DO NOTHING
	}


	@Override
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId) {
		// DO NOTHING
	}

	////////////////////////////////////////////////////////////////////////////////


	public Integer getReferenceOneId() {
		return this.referenceOneId;
	}


	public void setReferenceOneId(Integer referenceOneId) {
		this.referenceOneId = referenceOneId;
	}


	public Integer getReferenceTwoId() {
		return this.referenceTwoId;
	}


	public void setReferenceTwoId(Integer referenceTwoId) {
		this.referenceTwoId = referenceTwoId;
	}


	@Override
	public Integer getCompanyId() {
		return this.companyId;
	}


	@Override
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public Boolean getPrivateRelationship() {
		return this.privateRelationship;
	}


	public void setPrivateRelationship(Boolean privateRelationship) {
		this.privateRelationship = privateRelationship;
	}


	@Override
	public Integer getCompanyIdOrRelatedCompany() {
		return this.companyIdOrRelatedCompany;
	}


	@Override
	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany) {
		this.companyIdOrRelatedCompany = companyIdOrRelatedCompany;
	}


	public String getReferenceOneName() {
		return this.referenceOneName;
	}


	public void setReferenceOneName(String referenceOneName) {
		this.referenceOneName = referenceOneName;
	}


	public String getReferenceTwoName() {
		return this.referenceTwoName;
	}


	public void setReferenceTwoName(String referenceTwoName) {
		this.referenceTwoName = referenceTwoName;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getIdentificationCodeType() {
		return this.identificationCodeType;
	}


	public void setIdentificationCodeType(String identificationCodeType) {
		this.identificationCodeType = identificationCodeType;
	}


	public String getIdentificationCode() {
		return this.identificationCode;
	}


	public void setIdentificationCode(String identificationCode) {
		this.identificationCode = identificationCode;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getReferenceOneTypeId() {
		return this.referenceOneTypeId;
	}


	public void setReferenceOneTypeId(Short referenceOneTypeId) {
		this.referenceOneTypeId = referenceOneTypeId;
	}


	public Short getReferenceTwoTypeId() {
		return this.referenceTwoTypeId;
	}


	public void setReferenceTwoTypeId(Short referenceTwoTypeId) {
		this.referenceTwoTypeId = referenceTwoTypeId;
	}


	public String[] getNames() {
		return this.names;
	}


	public void setNames(String[] names) {
		this.names = names;
	}


	public String getNameEquals() {
		return this.nameEquals;
	}


	public void setNameEquals(String nameEquals) {
		this.nameEquals = nameEquals;
	}


	@Override
	public Boolean getIncludeClientOrClientRelationship() {
		return this.includeClientOrClientRelationship;
	}


	@Override
	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship) {
		this.includeClientOrClientRelationship = includeClientOrClientRelationship;
	}
}
