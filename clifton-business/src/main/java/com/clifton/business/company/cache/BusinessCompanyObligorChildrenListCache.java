package com.clifton.business.company.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;


/**
 * @author JasonS
 */
public interface BusinessCompanyObligorChildrenListCache {

	public Integer[] getBusinessCompanyObligorChildrenList(int obligorCompanyId, ReadOnlyDAO<BusinessCompany> businessCompanyDAO);

}
