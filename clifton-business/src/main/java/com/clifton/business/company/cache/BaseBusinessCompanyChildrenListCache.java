package com.clifton.business.company.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;

import java.util.List;
import java.util.Set;


/**
 * @author JasonS
 */
public abstract class BaseBusinessCompanyChildrenListCache extends SelfRegisteringSimpleDaoCache<BusinessCompany, Integer, Integer[]> {

	abstract String getBeanFieldName();

	protected void addAllBusinessCompanyChildrenByField(Integer businessCompanyId, Set<Integer> childrenSet, ReadOnlyDAO<BusinessCompany> businessCompanyDAO) {
		List<BusinessCompany> childrenList = businessCompanyDAO.findByField(getBeanFieldName(), businessCompanyId);
		for (BusinessCompany childBusinessCompany : CollectionUtils.getIterable(childrenList)) {
			childrenSet.add(childBusinessCompany.getId());
			addAllBusinessCompanyChildrenByField(childBusinessCompany.getId(), childrenSet, businessCompanyDAO);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                   Observer Methods                    //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<BusinessCompany> dao, DaoEventTypes event, BusinessCompany bean, Throwable e) {
		if (e == null) {
			boolean clear = false;
			if (event.isUpdate()) {
				BusinessCompany originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !CompareUtils.isEqual(originalBean.getParent(), bean.getParent())) {
					clear = true;
				}
			}
			else if (bean.getParent() != null) {
				clear = true;
			}
			if (clear) {
				getCacheHandler().clear(getCacheName());
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<BusinessCompany> dao, DaoEventTypes event, BusinessCompany bean) {
		if (!event.isInsert()) {
			getOriginalBean(dao, bean);
		}
	}

}
