package com.clifton.business.company;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>BusinessCompanyType</code> defines the type of the company.
 * Examples: Broker, Bank, Money Manager, Our Company (Clifton)
 *
 * @author manderson
 */
@CacheByName
public class BusinessCompanyType extends NamedEntity<Short> implements SystemEntityModifyConditionAware {

	private boolean systemDefined;
	private boolean ourCompany;
	private boolean moneyManager;
	private boolean issuer;

	/**
	 * When true, companies of this type require an abbreviation
	 * which is a 4 letter unique code that can be used by templates to
	 * create account names, investment security names, etc.
	 */
	private boolean abbreviationRequired;

	/**
	 * Used for "departments" where it's really a part of a company
	 * i.e. Trading Department at Clifton.  They have their own fax # so we need it set up separately
	 */
	private boolean parentCompanyRequired;

	/**
	 * Drives whether or not contracts are supported for the company type.
	 * <p/>
	 * Only Client Company Types and Our Company support contracts at this time.
	 */
	private boolean contractAllowed;

	/**
	 * Indicates that the company type allows trading.
	 */
	private boolean tradingAllowed;

	/**
	 * When true, company names must be globally unique
	 * across all company types (UX is by Parent & Name)
	 * <p/>
	 * Otherwise, company names are required to be unique for their own company type only
	 */
	private boolean globallyUnique;

	/**
	 * Prohibits anyone except for admins to edit when a modify condition is not present.
	 */
	private SystemCondition entityModifyCondition;


	@Override
	public String getLabel() {
		return super.getLabel() + (isOurCompany() ? " (Our Company)" : "");
	}


	public boolean isOurCompany() {
		return this.ourCompany;
	}


	public void setOurCompany(boolean ourCompany) {
		this.ourCompany = ourCompany;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isMoneyManager() {
		return this.moneyManager;
	}


	public void setMoneyManager(boolean moneyManager) {
		this.moneyManager = moneyManager;
	}


	public boolean isIssuer() {
		return this.issuer;
	}


	public void setIssuer(boolean issuer) {
		this.issuer = issuer;
	}


	public boolean isParentCompanyRequired() {
		return this.parentCompanyRequired;
	}


	public void setParentCompanyRequired(boolean parentCompanyRequired) {
		this.parentCompanyRequired = parentCompanyRequired;
	}


	public boolean isContractAllowed() {
		return this.contractAllowed;
	}


	public void setContractAllowed(boolean contractAllowed) {
		this.contractAllowed = contractAllowed;
	}


	public boolean isTradingAllowed() {
		return this.tradingAllowed;
	}


	public void setTradingAllowed(boolean tradingAllowed) {
		this.tradingAllowed = tradingAllowed;
	}


	public boolean isAbbreviationRequired() {
		return this.abbreviationRequired;
	}


	public void setAbbreviationRequired(boolean abbreviationRequired) {
		this.abbreviationRequired = abbreviationRequired;
	}


	public boolean isGloballyUnique() {
		return this.globallyUnique;
	}


	public void setGloballyUnique(boolean globallyUnique) {
		this.globallyUnique = globallyUnique;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
