package com.clifton.business.company.observers;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author danielh
 */
@Component
public class BusinessCompanyValidator extends SelfRegisteringDaoValidator<BusinessCompany> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BusinessCompany bean, DaoEventTypes config) throws ValidationException {
		// Uses DAO method
	}


	@Override
	public void validate(BusinessCompany bean, DaoEventTypes event, ReadOnlyDAO<BusinessCompany> dao) throws ValidationException {
		// Get original bean for updates - name comparisons and used in after call
		BusinessCompany original = null;
		if (event.isUpdate()) {
			original = getOriginalBean(bean);
		}
		// Validation

		if (bean.getType().isParentCompanyRequired()) {
			ValidationUtils.assertNotNull(bean.getParent(), "Companies of type [" + bean.getType().getName() + "] required a parent company to be selected.", "parent");
		}

		if (!StringUtils.isEmpty(bean.getAbbreviation())) {
			String abr = bean.getAbbreviation().trim();
			if (abr.length() > 4) {
				throw new ValidationException("Abbreviations cannot be more than 4 characters.");
			}
			BusinessCompany abbreviationCompany = dao.findOneByField("abbreviation", bean.getAbbreviation());
			if (abbreviationCompany != null && (event.isInsert() || !abbreviationCompany.equals(bean))) {
				throw new ValidationException("Abbreviation [" + bean.getAbbreviation() + "] is already used for company [" + abbreviationCompany.getName() + "].  Company abbreviations must be unique.");
			}
		}
		else if (bean.getType().isAbbreviationRequired()) {
			throw new ValidationException("Abbreviations are required for company type [" + bean.getType().getName() + "].  Please enter a unique abbreviation for this company.");
		}

		// Validate Name UX for Inserts or Updates to the Parent or Name
		if (original == null || !CoreCompareUtils.isEqual(bean, original, new String[]{"parent", "name"})) {
			List<BusinessCompany> companyList = dao.findByFields(new String[]{"parent.id", "name"},
					new Object[]{(bean.getParent() == null ? null : bean.getParent().getId()), bean.getName()});
			if (!CollectionUtils.isEmpty(companyList)) {
				for (BusinessCompany company : companyList) {
					if (company.equals(bean)) {
						continue;
					}
					if (bean.getType().isGloballyUnique()) {
						throw new ValidationException("Company Name [" + bean.getName() + "] is invalid because it is required to be globally unique and [" + company.getType().getName()
								+ "] company [" + company.getName() + "] is already using that name.");
					}
					if (company.getType().isGloballyUnique() || bean.getType().isGloballyUnique()) {
						throw new ValidationException("Company Name [" + bean.getName() + "] is invalid because [" + company.getType().getName() + "] company [" + company.getName()
								+ "] is already using that name and requires name to be globally unique.");
					}
					else if (company.getType().equals(bean.getType())) {
						throw new ValidationException("Company Name [" + bean.getName() + "] is invalid because company [" + company.getName()
								+ "] is already using that name for the same company type.");
					}
				}
			}
		}
	}
}
