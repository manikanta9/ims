package com.clifton.business.company;

/**
 * @author mwacker
 */
public interface BusinessCompanyUtilHandler {

	public static final String PPA_MINNEAPOLIS_COMPANY_NAME = "Parametric Minneapolis";
	public static final String PPA_MINNEAPOLIS_COMPANY_TYPE = "Our Companies";

	public static final String COMPANY_CUSTOM_FIELDS = "Company Custom Fields";

	public static final String BUSINESS_INTERMEDIARY_CUSTODIAN_FIELD = "IntermediaryCustodian";
	public static final String BUSINESS_INTERMEDIARY_CUSTODIAN_ACCOUNT_NUMBER_FIELD = "IntermediaryCustodianAccountNumber";


	/**
	 * Will return the Business Identifier Code (BIC) for 'Parametric Minneapolis'
	 */
	public String getParametricBusinessIdentifierCode();


	/**
	 * Will return the "IntermediaryCustodian" custom column.
	 */
	public BusinessCompany getIntermediaryCustodian(BusinessCompany company);


	/**
	 * Will return the "IntermediaryCustodianAccountNumber" custom column.
	 */
	public String getIntermediaryCustodianAccountNumber(BusinessCompany company);
}
