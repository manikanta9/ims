package com.clifton.business.company.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


/**
 * @author JasonS
 */
@Component
public class BusinessCompanyObligorChildrenListCacheImpl extends BaseBusinessCompanyChildrenListCache implements BusinessCompanyObligorChildrenListCache {


	protected static final String BEAN_FIELD_NAME = "obligorBusinessCompany.id";

	@Override
	public Integer[] getBusinessCompanyObligorChildrenList(int businessCompanyId, ReadOnlyDAO<BusinessCompany> businessCompanyDAO) {
		Integer[] result = getCacheHandler().get(getCacheName(), businessCompanyId);
		if (result == null) {
			Set<Integer> obligorSet = new HashSet<>();
			addAllBusinessCompanyChildrenByField(businessCompanyId, obligorSet, businessCompanyDAO);
			if (CollectionUtils.isEmpty(obligorSet)) {
				result = new Integer[0];
			}
			else {
				result = obligorSet.toArray(new Integer[0]);
			}
			getCacheHandler().put(getCacheName(), businessCompanyId, result);
		}
		return result;
	}


	@Override
	String getBeanFieldName() {
		return BEAN_FIELD_NAME;
	}
}
