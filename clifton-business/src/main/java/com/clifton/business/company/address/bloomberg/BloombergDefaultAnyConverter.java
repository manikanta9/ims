package com.clifton.business.company.address.bloomberg;

import com.clifton.business.company.address.BusinessCompanyAddressConverter;
import com.clifton.core.beans.common.AddressEntity;
import com.clifton.core.beans.common.AddressObject;
import org.springframework.stereotype.Component;

import java.util.Deque;
import java.util.LinkedList;


/**
 * <code>CompanyAddressDefaultAdapter</code> when not composed of six address components and not one of the special country-specific
 * adapters, load the country code and the rest into address lines 1..3.
 */
@Component
public class BloombergDefaultAnyConverter implements BusinessCompanyAddressConverter {

	private BloombergParserHandler bloombergParserHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public AddressObject convertAddress(String formattedAddress) {
		AddressObject addressObject = new AddressEntity();
		Deque<String> addressComponents = new LinkedList<>(getBloombergParserHandler().splitIntoAddressComponents(formattedAddress));

		addressObject.setCountry(addressComponents.pollLast());
		addressObject.setAddressLine1(addressComponents.poll());
		addressObject.setAddressLine2(addressComponents.poll());
		addressObject.setAddressLine3(String.join(" ", addressComponents));

		return addressObject;
	}


	@Override
	public boolean supports(String formattedAddress) {
		return getBloombergParserHandler().recognizesAddressFormat(formattedAddress);
	}


	@Override
	public int order() {
		return 100;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BloombergParserHandler getBloombergParserHandler() {
		return this.bloombergParserHandler;
	}


	public void setBloombergParserHandler(BloombergParserHandler bloombergParserHandler) {
		this.bloombergParserHandler = bloombergParserHandler;
	}
}
