package com.clifton.business.company.cache;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import org.springframework.stereotype.Component;


/**
 * <code>BusinessCompanyByTypeIdAndNameCache</code> cache for {@link BusinessCompany} entities, allows fetching by
 * name and type.
 */
@Component
public class BusinessCompanyByTypeIdAndNameCache extends SelfRegisteringCompositeKeyDaoCache<BusinessCompany, Short, String> {

	@Override
	protected String getBeanKey1Property() {
		return "type.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "name";
	}


	@Override
	protected Short getBeanKey1Value(BusinessCompany bean) {
		if (bean.getType() != null) {
			return bean.getType().getId();
		}
		return null;
	}


	@Override
	protected String getBeanKey2Value(BusinessCompany bean) {
		return bean.getName();
	}
}
