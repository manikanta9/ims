package com.clifton.business.company;


import com.clifton.business.company.search.BusinessCompanyParentRelationshipTypeSearchForm;
import com.clifton.business.company.search.BusinessCompanyPlatformSearchForm;
import com.clifton.business.company.search.BusinessCompanyRelationshipSearchForm;
import com.clifton.business.company.search.BusinessCompanyTypeSearchForm;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * The <code>BusinessCompanyService</code> defines methods for working with {@link BusinessCompany}
 * and related company objects
 *
 * @author manderson
 */
public interface BusinessCompanyService {

	////////////////////////////////////////////////////////////////////////////
	///////////            Business Company Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getBusinessCompany(int id);


	public BusinessCompany getBusinessCompanyByTypeAndName(final String type, final String name);


	/**
	 * The Business Company Table Column [BloombergCompanyIdentifier] is NOT unique, must return a list of matching records.
	 */
	public List<BusinessCompany> getBusinessCompanyByBloombergCompanyIdentifier(int bloombergCompanyIdentifier);


	public List<BusinessCompany> getBusinessCompanyList(CompanySearchForm searchForm);


	public List<BusinessCompany> getBusinessCompanyRelatedCompanyList(int id);


	public BusinessCompany saveBusinessCompany(BusinessCompany bean);


	public void deleteBusinessCompany(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Company Type Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyType getBusinessCompanyType(short id);


	public BusinessCompanyType getBusinessCompanyTypeByName(String name);


	/**
	 * If systemDefined is not null, will also filter on systemDefined field
	 */
	public List<BusinessCompanyType> getBusinessCompanyTypeList(BusinessCompanyTypeSearchForm searchForm);


	public BusinessCompanyType saveBusinessCompanyType(BusinessCompanyType bean);


	public void deleteBusinessCompanyType(short id);


	////////////////////////////////////////////////////////////////////////////
	///////           Business Company Relationship Methods              ///////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyRelationship getBusinessCompanyRelationship(int id);


	public List<BusinessCompanyRelationship> getBusinessCompanyRelationshipList(BusinessCompanyRelationshipSearchForm searchForm);


	public BusinessCompanyRelationship saveBusinessCompanyRelationship(BusinessCompanyRelationship bean);


	@DoNotAddRequestMapping
	public void copyBusinessCompanyRelationships(BusinessCompany from, BusinessCompany to, boolean moveRelationships, StringBuilder result);


	public void deleteBusinessCompanyRelationship(int id);


	public void deleteBusinessCompanyRelationshipListByCompany(int companyId);


	////////////////////////////////////////////////////////////////////////////
	///////      Business Company Parent Relationship Type Methods       ///////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyParentRelationshipType getBusinessCompanyParentRelationshipType(short id);


	public BusinessCompanyParentRelationshipType getBusinessCompanyParentRelationshipTypeByName(String name);


	public List<BusinessCompanyParentRelationshipType> getBusinessCompanyParentRelationshipTypeList(BusinessCompanyParentRelationshipTypeSearchForm searchForm);


	public BusinessCompanyParentRelationshipType saveBusinessCompanyParentRelationshipType(BusinessCompanyParentRelationshipType bean);


	public void deleteBusinessCompanyParentRelationshipType(short id);


	////////////////////////////////////////////////////////////////////////////
	//////////        Business Company Issuer List Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	public List<BusinessCompany> getBusinessCompanyChildrenListForParent(int parentCompanyId);


	public List<BusinessCompany> getBusinessCompanyChildrenListForObligor(int obligorCompanyId);


	////////////////////////////////////////////////////////////////////////////
	//////////         Business Company Platform Methods               /////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyPlatform getBusinessCompanyPlatform(int id);


	public List<BusinessCompanyPlatform> getBusinessCompanyPlatformList(BusinessCompanyPlatformSearchForm searchForm);


	public BusinessCompanyPlatform saveBusinessCompanyPlatform(BusinessCompanyPlatform bean);


	public void deleteBusinessCompanyPlatform(int id);
}
