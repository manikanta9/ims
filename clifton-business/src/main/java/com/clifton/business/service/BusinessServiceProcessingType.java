package com.clifton.business.service;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.util.List;


/**
 * A BusinessServiceProcessingType entity defines the processing type that should be used by this Client Account for its portfolio management.
 * Different processing types may use additional data (tables) specific to them and will drive corresponding UI (usually by adding tabs to Client Account).
 *
 * @author davidi
 */
@CacheByName
public class BusinessServiceProcessingType extends NamedEntity<Short> implements SystemColumnCustomValueAware {


	// Column Group Names and Properties
	public static final String BUSINESS_SERVICE_PROCESSING_TYPE_COLUMN_GROUP_NAME = "Business Service Processing Type Custom Fields";
	// column group with admin security for run processing
	public static final String BUSINESS_SERVICE_PROCESSING_TYPE_RUN_COLUMN_GROUP_NAME = "Business Service Processing Type Run Custom Fields";

	// For link value "SECURITY_TARGET"
	public static final String TRANCHE_POSITION_COUNT = "Tranche Position Count";
	public static final String TRADE_CYCLE_UNEXPIRED_CONTRACT_FILTER_TYPE = "Trade Cycle Unexpired Contract Filter Type";
	public static final String CYCLE_DURATION_WEEKS = "Cycle Duration Weeks";
	public static final String ALLOWED_OPTION_EXPIRATION_CYCLE_DEVIATION_DAYS = "Allowed Option Expiration Cycle Deviation Days";
	public static final String TRANCHE_COUNT = "Tranche Count";
	public static final String USES_VALUE_AT_RISK = "Uses Value At Risk";
	public static final String OPTION_COMBINATION_TYPE = "Option Combination Type";
	public static final String MAX_OPTION_SPREAD_PERCENT = "Max Option Spread Percent";

	// For link value "SECURITY_TARGET" - Performance Summaries
	public static final String PERFORMANCE_DO_NOT_CALCULATE_BENCHMARK_RETURN = "Performance Summary: Do Not Calculate Benchmark Return";

	// For link value "SECURITY_TARGET", (Delta Mid-Range Values and allowed deviation)
	public static final String SHORT_PUT_DELTA = "Short Put Option Delta";
	public static final String LONG_PUT_DELTA = "Long Put Option Delta";
	public static final String SHORT_CALL_DELTA = "Short Call Option Delta";
	public static final String LONG_CALL_DELTA = "Long Call Option Delta";
	public static final String ALLOWED_DELTA_DEVIATION = "Allowed Delta Deviation";

	// Field for report account masking; the value indicates the purpose to use for finding a related account to use for a client account
	public static final String ACCOUNT_PURPOSE_REPORTING_ACCOUNT_NUMBER = "Account Purpose Reporting Account Number";

	// For link value "PORTFOLIO_RUNS"
	public static final String TRADE_CREATION_VIEW = "Trade Creation View";

	////////////////////////////////////////////////////////////////////////////


	private ServiceProcessingTypes processingType;

	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ServiceProcessingTypes getProcessingType() {
		return this.processingType;
	}


	public void setProcessingType(ServiceProcessingTypes processingType) {
		this.processingType = processingType;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
