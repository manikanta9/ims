package com.clifton.business.service.search;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BusinessServiceProcessingTypeSearchForm</code> is used to search for
 * BusinessServiceProcessingType data located in the BusinessServiceProcessingType table.
 *
 * @author davidi
 */
public class BusinessServiceProcessingTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description,processingType")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private ServiceProcessingTypes processingType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public ServiceProcessingTypes getProcessingType() {
		return this.processingType;
	}


	public void setProcessingType(ServiceProcessingTypes processingType) {
		this.processingType = processingType;
	}
}
