package com.clifton.business.service;


import com.clifton.business.service.search.BusinessServiceApplicationSearchForm;
import com.clifton.business.service.search.BusinessServiceBusinessServiceProcessingTypeSearchForm;
import com.clifton.business.service.search.BusinessServiceObjectiveSearchForm;
import com.clifton.business.service.search.BusinessServiceProcessingTypeSearchForm;
import com.clifton.business.service.search.BusinessServiceSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>BusinessServiceServiceImpl</code> class provides basic implementation of the BusinessServiceService interface.
 *
 * @author vgomelsky
 */
@Service
public class BusinessServiceServiceImpl implements BusinessServiceService {

	private AdvancedUpdatableDAO<BusinessService, Criteria> businessServiceDAO;
	private AdvancedUpdatableDAO<BusinessServiceBusinessServiceProcessingType, Criteria> businessServiceBusinessServiceProcessingTypeDAO;
	private AdvancedUpdatableDAO<BusinessServiceProcessingType, Criteria> businessServiceProcessingTypeDAO;
	private AdvancedUpdatableDAO<BusinessServiceApplication, Criteria> businessServiceApplicationDAO;
	private AdvancedUpdatableDAO<BusinessServiceObjective, Criteria> businessServiceObjectiveDAO;

	private DaoNamedEntityCache<BusinessServiceProcessingType> businessServiceProcessingTypeCache;

	////////////////////////////////////////////////////////////////////////////
	////////      Business Service Processing Type Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessServiceProcessingType getBusinessServiceProcessingType(short id) {
		return getBusinessServiceProcessingTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessServiceProcessingType getBusinessServiceProcessingTypeByName(String name) {
		return getBusinessServiceProcessingTypeCache().getBeanForKeyValue(getBusinessServiceProcessingTypeDAO(), name);
	}


	@Override
	public List<BusinessServiceProcessingType> getBusinessServiceProcessingTypeList(BusinessServiceProcessingTypeSearchForm searchForm) {
		return getBusinessServiceProcessingTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessServiceProcessingType saveBusinessServiceProcessingType(BusinessServiceProcessingType bean) {
		return getBusinessServiceProcessingTypeDAO().save(bean);
	}


	@Override
	public void deleteBusinessServiceProcessingType(short id) {
		getBusinessServiceProcessingTypeDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////  Business Service - Business Service Processing Type Methods   //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessServiceBusinessServiceProcessingType getBusinessServiceBusinessServiceProcessingType(int id) {
		return getBusinessServiceBusinessServiceProcessingTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessServiceBusinessServiceProcessingType> getBusinessServiceBusinessServiceProcessingTypeList(BusinessServiceBusinessServiceProcessingTypeSearchForm searchForm) {
		return getBusinessServiceBusinessServiceProcessingTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessServiceBusinessServiceProcessingType saveBusinessServiceBusinessServiceProcessingType(BusinessServiceBusinessServiceProcessingType bean) {
		return getBusinessServiceBusinessServiceProcessingTypeDAO().save(bean);
	}


	@Override
	public void deleteBusinessServiceBusinessServiceProcessingType(int id) {
		getBusinessServiceBusinessServiceProcessingTypeDAO().delete(id);
	}


	@Override
	public void linkBusinessServiceToProcessingType(short businessServiceId, short businessServiceProcessingTypeId) {
		BusinessServiceBusinessServiceProcessingType businessServiceBusinessServiceProcessingType = new BusinessServiceBusinessServiceProcessingType();
		BusinessService businessService = getBusinessServiceDAO().findByPrimaryKey(businessServiceId);
		BusinessServiceProcessingType businessServiceProcessingType = getBusinessServiceProcessingTypeDAO().findByPrimaryKey(businessServiceProcessingTypeId);
		AssertUtils.assertNotNull(businessService, "Cannot find BusinessService entity with id: " + businessServiceId);
		AssertUtils.assertNotNull(businessServiceProcessingType, "Cannot find BusinessServiceProcessingType entity with id: " + businessServiceProcessingTypeId);
		businessServiceBusinessServiceProcessingType.setBusinessService(businessService);
		businessServiceBusinessServiceProcessingType.setBusinessServiceProcessingType(businessServiceProcessingType);
		saveBusinessServiceBusinessServiceProcessingType(businessServiceBusinessServiceProcessingType);
	}


	@Override
	public void unlinkBusinessServiceFromProcessingType(short businessServiceId, short businessServiceProcessingTypeId) {
		BusinessServiceBusinessServiceProcessingTypeSearchForm searchForm = new BusinessServiceBusinessServiceProcessingTypeSearchForm();
		searchForm.setBusinessServiceId(businessServiceId);
		searchForm.setBusinessServiceProcessingTypeId(businessServiceProcessingTypeId);
		List<BusinessServiceBusinessServiceProcessingType> businessServiceBusinessServiceProcessingTypeList = getBusinessServiceBusinessServiceProcessingTypeList(searchForm);
		if (businessServiceBusinessServiceProcessingTypeList.isEmpty()) {
			return;
		}
		BusinessServiceBusinessServiceProcessingType businessServiceBusinessServiceProcessingType = businessServiceBusinessServiceProcessingTypeList.get(0);
		deleteBusinessServiceBusinessServiceProcessingType(businessServiceBusinessServiceProcessingType.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////         Business Service Application Methods            ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessServiceApplication getBusinessServiceApplication(short id) {
		return getBusinessServiceApplicationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessServiceApplication> getBusinessServiceApplicationList(BusinessServiceApplicationSearchForm searchForm) {
		return getBusinessServiceApplicationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessServiceApplication saveBusinessServiceApplication(BusinessServiceApplication bean) {
		return getBusinessServiceApplicationDAO().save(bean);
	}


	@Override
	public void deleteBusinessServiceApplication(short id) {
		getBusinessServiceApplicationDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                Business Service Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessService getBusinessService(short id) {
		return getBusinessServiceDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessService> getBusinessServiceList(final BusinessServiceSearchForm searchForm) {
		// Default Sorting if none defined
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("nameExpanded");
		}

		return getBusinessServiceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessService saveBusinessService(BusinessService bean) {
		// Note: User Interface should prevent these violations by hiding fields that don't apply, and restricting parent selections
		ValidationUtils.assertNotNull(bean.getServiceLevelType(), "Business Service Level Type is required");
		String messagePrefix = "Business Service Level Type [" + bean.getServiceLevelType().name() + "]";
		if (CollectionUtils.isEmpty(bean.getServiceLevelType().getParentLevelTypeList())) {
			ValidationUtils.assertNull(bean.getParent(), messagePrefix + " does not allow parent selections.");
		}
		else {
			ValidationUtils.assertNotNull(bean.getParent(), messagePrefix + " requires parent selection.");
			ValidationUtils.assertTrue(bean.getServiceLevelType().getParentLevelTypeList().contains(bean.getParent().getServiceLevelType()), "Parent Service [" + bean.getParent().getNameExpanded() + "] is not a valid parent. " + messagePrefix + " only allows parents of type(s) " + StringUtils.collectionToCommaDelimitedString(bean.getServiceLevelType().getParentLevelTypeList()));
		}
		if (!bean.getServiceLevelType().isActiveDatesSupported()) {
			ValidationUtils.assertNull(bean.getStartDate(), messagePrefix + " does not support start date.");
			ValidationUtils.assertNull(bean.getEndDate(), messagePrefix + " does not support end date.");
		}
		if (!bean.getServiceLevelType().isAssignableService()) {
			ValidationUtils.assertFalse(bean.isFullyFunded(), "Fully Funded selection is only supported for assignable services.");
			ValidationUtils.assertNull(bean.getApplication(), "Investment Vehicle selection is only supported for assignable services.");
			ValidationUtils.assertNull(bean.getAumCalculatorBean(), "AUM Calculator selection is only supported for assignable services.");
			ValidationUtils.assertNull(bean.getInvestmentStyle(), "Investment Style selection is only supported for assignable services.");
		}
		return getBusinessServiceDAO().save(bean);
	}


	@Override
	public void deleteBusinessService(short id) {
		getBusinessServiceDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////         Business Service Type Objective Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessServiceObjective getBusinessServiceObjective(int id) {
		return getBusinessServiceObjectiveDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessServiceObjective> getBusinessServiceObjectiveList(BusinessServiceObjectiveSearchForm searchForm) {
		return getBusinessServiceObjectiveDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessServiceObjective saveBusinessServiceObjective(BusinessServiceObjective bean) {
		return getBusinessServiceObjectiveDAO().save(bean);
	}


	@Override
	public void deleteBusinessServiceObjective(int id) {
		getBusinessServiceObjectiveDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////                 Getters and Setters                     ///////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessService, Criteria> getBusinessServiceDAO() {
		return this.businessServiceDAO;
	}


	public void setBusinessServiceDAO(AdvancedUpdatableDAO<BusinessService, Criteria> businessServiceDAO) {
		this.businessServiceDAO = businessServiceDAO;
	}


	public AdvancedUpdatableDAO<BusinessServiceBusinessServiceProcessingType, Criteria> getBusinessServiceBusinessServiceProcessingTypeDAO() {
		return this.businessServiceBusinessServiceProcessingTypeDAO;
	}


	public void setBusinessServiceBusinessServiceProcessingTypeDAO(AdvancedUpdatableDAO<BusinessServiceBusinessServiceProcessingType, Criteria> businessServiceBusinessServiceProcessingTypeDAO) {
		this.businessServiceBusinessServiceProcessingTypeDAO = businessServiceBusinessServiceProcessingTypeDAO;
	}


	public AdvancedUpdatableDAO<BusinessServiceProcessingType, Criteria> getBusinessServiceProcessingTypeDAO() {
		return this.businessServiceProcessingTypeDAO;
	}


	public void setBusinessServiceProcessingTypeDAO(AdvancedUpdatableDAO<BusinessServiceProcessingType, Criteria> businessServiceProcessingTypeDAO) {
		this.businessServiceProcessingTypeDAO = businessServiceProcessingTypeDAO;
	}


	public AdvancedUpdatableDAO<BusinessServiceApplication, Criteria> getBusinessServiceApplicationDAO() {
		return this.businessServiceApplicationDAO;
	}


	public void setBusinessServiceApplicationDAO(AdvancedUpdatableDAO<BusinessServiceApplication, Criteria> businessServiceApplicationDAO) {
		this.businessServiceApplicationDAO = businessServiceApplicationDAO;
	}


	public AdvancedUpdatableDAO<BusinessServiceObjective, Criteria> getBusinessServiceObjectiveDAO() {
		return this.businessServiceObjectiveDAO;
	}


	public void setBusinessServiceObjectiveDAO(AdvancedUpdatableDAO<BusinessServiceObjective, Criteria> businessServiceObjectiveDAO) {
		this.businessServiceObjectiveDAO = businessServiceObjectiveDAO;
	}


	public DaoNamedEntityCache<BusinessServiceProcessingType> getBusinessServiceProcessingTypeCache() {
		return this.businessServiceProcessingTypeCache;
	}


	public void setBusinessServiceProcessingTypeCache(DaoNamedEntityCache<BusinessServiceProcessingType> businessServiceProcessingTypeCache) {
		this.businessServiceProcessingTypeCache = businessServiceProcessingTypeCache;
	}
}
