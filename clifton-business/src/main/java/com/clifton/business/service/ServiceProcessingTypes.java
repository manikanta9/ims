package com.clifton.business.service;


/**
 * The <code>ServiceProcessingTypes</code> defines enum which drive Account processing, tabs available on the account
 * and views
 *
 * @author manderson
 */
public enum ServiceProcessingTypes {

	PORTFOLIO_RUNS(true, true, true, true), // GENERIC - USED FOR OVERLAY, FULLY FUNDED, STRUCTURED OPTIONS
	PORTFOLIO_TARGET(true, true, false, false), //
	HEDGING(false, false, false, false), //
	LDI(true, true, true, false), //
	DURATION_MANAGEMENT(false, true, false, false), //
	SECURITY_TARGET(false, true, false, false);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	ServiceProcessingTypes(boolean portfolioRunsSupported, boolean managerAssignmentsSupported, boolean managerAssignmentCashTypeRequired, boolean assetClassesSupported) {
		this.portfolioRunsSupported = portfolioRunsSupported;
		this.managerAssignmentsSupported = managerAssignmentsSupported;
		this.managerAssignmentCashTypeRequired = managerAssignmentCashTypeRequired;
		this.assetClassesSupported = assetClassesSupported;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Validated on creation of a new portfolio run the account doesn't support runs you can't create the new run
	 */
	private final boolean portfolioRunsSupported;

	/**
	 * Validated on Save of Manager Assignments to ensure it's supported for the account
	 */
	private final boolean managerAssignmentsSupported;

	/**
	 * Validated on Save of Manager Assignments to validate if manager assignment cash type is required/used
	 */
	private final boolean managerAssignmentCashTypeRequired;

	/**
	 * Validated on Save of Manager Assignments to determine of the Manager Assignment is used
	 * to allocate to asset classes, or just assigns to a benchmark (required in this case).
	 */
	private final boolean assetClassesSupported;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isPortfolioRunsSupported() {
		return this.portfolioRunsSupported;
	}


	public boolean isManagerAssignmentsSupported() {
		return this.managerAssignmentsSupported;
	}


	public boolean isAssetClassesSupported() {
		return this.assetClassesSupported;
	}


	public boolean isManagerAssignmentCashTypeRequired() {
		return this.managerAssignmentCashTypeRequired;
	}
}
