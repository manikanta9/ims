package com.clifton.business.service;

/**
 * Defines available accounting closing methods for positions. Enables sorting positions according to the specified method.
 * <p>
 * Other cost basis accounting methods to consider: LOFO (Lowest Cost, First Out), LGUT (Loss Gain Utilization), SLID (Specific Lot Identification), Average Cost.
 *
 * @author vgomelsky
 */
public enum AccountingClosingMethods {

	/**
	 * Highest In First Out then Original Transaction Date, then lowest transaction id.
	 * Highest Cost (cost per unit) will appear before positions with lower cost per unit: minimize realized gains.
	 */
	HIFO,


	/**
	 * First In First Out by Original Transaction Date, then lowest price and then lowest transaction id.
	 */
	FIFO,


	/**
	 * Last In First Out by Original Transaction Date, then highest price and then highest transaction id.
	 */
	LIFO,


	/**
	 * Lowest Cost, First Out, then Original Transaction Date, then lowest transaction id.
	 * Lowest Cost (cost per unit) will appear before positions with higher cost per unit: maximize realized gains.
	 */
	LOFO;
}
