package com.clifton.business.service;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>BusinessServiceObjective</code> defines an objective that is tied to a BusinessService
 * i.e. PIOS "Overlay" Service Types have Securitize Manager Cash as an objective
 *
 * @author Mary Anderson
 */
public class BusinessServiceObjective extends NamedEntity<Integer> {

	private BusinessService businessService;

	private int order;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessService getBusinessService() {
		return this.businessService;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
