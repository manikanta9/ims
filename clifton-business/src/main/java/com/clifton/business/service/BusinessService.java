package com.clifton.business.service;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessService</code> class represents a business service: "PIOS \ Cash Securitization", etc.
 * More than one service can have the same business application.
 * Usually leaf services have applications and are used.
 *
 * @author vgomelsky
 */
public class BusinessService extends NamedHierarchicalEntity<BusinessService, Short> implements SystemColumnCustomValueAware {

	private BusinessServiceLevelTypes serviceLevelType;

	private BusinessServiceApplication application;


	private boolean fullyFunded;

	private Date startDate;

	private Date endDate;

	/**
	 * Uses system list: Business Service Investment Style
	 * Current Options Passive & Active
	 */
	private String investmentStyle;

	/**
	 * Default AUM Calculator to use for accounts that use this service
	 * Note: Accounts can have their own override
	 * SystemBean must implement com.clifton.investment.account.calculation.calculator.InvestmentAccountCalculationSnapshotCalculator
	 */
	private SystemBean aumCalculatorBean;

	/**
	 * Specifies the default Accounting Closing Method (FIFO, LIFO, etc.) to be used for all client accounts under this service.
	 * This method can be overridden for each Client Account as well as for individual transactions.
	 */
	private AccountingClosingMethods accountingClosingMethod;

	/**
	 * The report title to show on client reports.
	 */
	private String reportTitle;


	/**
	 * A List of custom column values for the accounts
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	/**
	 * Returns the parent name + / + name
	 * only goes one level up
	 */
	public String getNameWithParent() {
		if (getParent() != null) {
			return getParent().getName() + " / " + getName();
		}
		return getName();
	}


	public String getFranchiseName() {
		return BusinessServiceLevelTypes.getServiceNameAtLevel(this, BusinessServiceLevelTypes.FRANCHISE);
	}


	public String getSuperStrategyName() {
		return BusinessServiceLevelTypes.getServiceNameAtLevel(this, BusinessServiceLevelTypes.SUPER_STRATEGY);
	}


	public String getServiceName() {
		return BusinessServiceLevelTypes.getServiceNameAtLevel(this, BusinessServiceLevelTypes.SERVICE);
	}


	public String getServiceGroupName() {
		return BusinessServiceLevelTypes.getServiceNameAtLevel(this, BusinessServiceLevelTypes.SERVICE_GROUP);
	}


	public String getCoalesceBusinessServiceGroupName() {
		return StringUtils.coalesce(false, getServiceGroupName(), getServiceName());
	}


	////////////////////////////////////////////////////////////////////////////////


	public BusinessServiceLevelTypes getServiceLevelType() {
		return this.serviceLevelType;
	}


	public void setServiceLevelType(BusinessServiceLevelTypes serviceLevelType) {
		this.serviceLevelType = serviceLevelType;
	}


	public BusinessServiceApplication getApplication() {
		return this.application;
	}


	public void setApplication(BusinessServiceApplication application) {
		this.application = application;
	}


	public boolean isFullyFunded() {
		return this.fullyFunded;
	}


	public void setFullyFunded(boolean fullyFunded) {
		this.fullyFunded = fullyFunded;
	}


	public String getInvestmentStyle() {
		return this.investmentStyle;
	}


	public void setInvestmentStyle(String investmentStyle) {
		this.investmentStyle = investmentStyle;
	}


	public SystemBean getAumCalculatorBean() {
		return this.aumCalculatorBean;
	}


	public void setAumCalculatorBean(SystemBean aumCalculatorBean) {
		this.aumCalculatorBean = aumCalculatorBean;
	}


	public AccountingClosingMethods getAccountingClosingMethod() {
		return this.accountingClosingMethod;
	}


	public void setAccountingClosingMethod(AccountingClosingMethods accountingClosingMethod) {
		this.accountingClosingMethod = accountingClosingMethod;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getReportTitle() {
		return this.reportTitle;
	}


	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
