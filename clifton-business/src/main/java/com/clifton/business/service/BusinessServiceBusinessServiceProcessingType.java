package com.clifton.business.service;

import com.clifton.core.beans.BaseEntity;


/**
 * An entity used to relate a BusinessService to BusinessServiceProcessingType.
 *
 * @author davidi
 */
public class BusinessServiceBusinessServiceProcessingType extends BaseEntity<Integer> {

	private BusinessService businessService;
	private BusinessServiceProcessingType businessServiceProcessingType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessService getBusinessService() {
		return this.businessService;
	}


	public void setBusinessService(BusinessService businessService) {
		this.businessService = businessService;
	}


	public BusinessServiceProcessingType getBusinessServiceProcessingType() {
		return this.businessServiceProcessingType;
	}


	public void setBusinessServiceProcessingType(BusinessServiceProcessingType businessServiceProcessingType) {
		this.businessServiceProcessingType = businessServiceProcessingType;
	}
}
