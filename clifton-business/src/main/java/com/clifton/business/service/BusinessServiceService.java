package com.clifton.business.service;


import com.clifton.business.service.search.BusinessServiceApplicationSearchForm;
import com.clifton.business.service.search.BusinessServiceBusinessServiceProcessingTypeSearchForm;
import com.clifton.business.service.search.BusinessServiceObjectiveSearchForm;
import com.clifton.business.service.search.BusinessServiceProcessingTypeSearchForm;
import com.clifton.business.service.search.BusinessServiceSearchForm;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * The <code>BusinessServiceService</code> interface defines methods for working with business services.
 *
 * @author vgomelsky
 */
public interface BusinessServiceService {

	////////////////////////////////////////////////////////////////////////////
	////////      Business Service Processing Type Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceProcessingType getBusinessServiceProcessingType(short id);


	public BusinessServiceProcessingType getBusinessServiceProcessingTypeByName(String name);


	public List<BusinessServiceProcessingType> getBusinessServiceProcessingTypeList(BusinessServiceProcessingTypeSearchForm searchForm);


	public BusinessServiceProcessingType saveBusinessServiceProcessingType(BusinessServiceProcessingType bean);


	public void deleteBusinessServiceProcessingType(short id);

	////////////////////////////////////////////////////////////////////////////
	//////  Business Service - Business Service Processing Type Methods   //////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceBusinessServiceProcessingType getBusinessServiceBusinessServiceProcessingType(int id);


	public List<BusinessServiceBusinessServiceProcessingType> getBusinessServiceBusinessServiceProcessingTypeList(BusinessServiceBusinessServiceProcessingTypeSearchForm searchForm);


	public BusinessServiceBusinessServiceProcessingType saveBusinessServiceBusinessServiceProcessingType(BusinessServiceBusinessServiceProcessingType bean);


	public void deleteBusinessServiceBusinessServiceProcessingType(int id);


	@SecureMethod(dtoClass = BusinessServiceBusinessServiceProcessingType.class)
	public void linkBusinessServiceToProcessingType(short businessServiceId, short businessServiceProcessingTypeId);


	@SecureMethod(dtoClass = BusinessServiceBusinessServiceProcessingType.class)
	public void unlinkBusinessServiceFromProcessingType(short businessServiceId, short businessServiceProcessingTypeId);

	////////////////////////////////////////////////////////////////////////////
	////////         Business Service Application Methods            ///////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceApplication getBusinessServiceApplication(short id);


	public List<BusinessServiceApplication> getBusinessServiceApplicationList(BusinessServiceApplicationSearchForm searchForm);


	public BusinessServiceApplication saveBusinessServiceApplication(BusinessServiceApplication bean);


	public void deleteBusinessServiceApplication(short id);

	////////////////////////////////////////////////////////////////////////////
	////////                Business Service Methods                 ///////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessService getBusinessService(short id);


	public List<BusinessService> getBusinessServiceList(BusinessServiceSearchForm searchForm);


	public BusinessService saveBusinessService(BusinessService bean);


	public void deleteBusinessService(short id);

	////////////////////////////////////////////////////////////////////////////
	////////           Business Service Objective Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessServiceObjective getBusinessServiceObjective(int id);


	public List<BusinessServiceObjective> getBusinessServiceObjectiveList(BusinessServiceObjectiveSearchForm searchForm);


	public BusinessServiceObjective saveBusinessServiceObjective(BusinessServiceObjective bean);


	public void deleteBusinessServiceObjective(int id);
}
