package com.clifton.business.service;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>BusinessServiceApplication</code> class represent "VEHICLES"
 * <p/>
 * Note: Term "application" has been reconfigured to represent vehicles.  All code is left to
 * use application as DB field and DTO objects, however UI representation has changed so labels
 * and instructions refer to these as Vehicles
 * <p/>
 * The term "investment vehicle" refers to any method by which businesses/individuals can invest. There is a wide variety of investment vehicles (such as Separate Account, Commingled Account, Mutual Fund, etc) and many investors choose to hold at least several types in their portfolios.
 *
 * @author vgomelsky
 */
public class BusinessServiceApplication extends NamedEntity<Short> {

	// empty for now
}
