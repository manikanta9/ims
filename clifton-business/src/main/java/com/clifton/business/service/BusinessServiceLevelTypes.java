package com.clifton.business.service;

import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * The <code>BusinessServiceLevelTypes</code> enum defines the characteristics of each level of the {@link BusinessService} hierarchy
 *
 * @author manderson
 */
public enum BusinessServiceLevelTypes {

	FRANCHISE(false, false), //
	SUPER_STRATEGY(CollectionUtils.createList(FRANCHISE), false, false), //
	SERVICE_GROUP(CollectionUtils.createList(SUPER_STRATEGY), false, false), //
	SERVICE(CollectionUtils.createList(SUPER_STRATEGY, SERVICE_GROUP), true, true), //
	SERVICE_COMPONENT(CollectionUtils.createList(SERVICE), true, false);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The parent level for this level
	 */
	private List<BusinessServiceLevelTypes> parentLevelTypeList;

	/**
	 * If start/end dates are allowed
	 */
	private boolean activeDatesSupported;


	/**
	 * Used for SERVICE level to indicate it's an actual service that supports the
	 * additional field entries for service type, aum type, performance type, investment style, etc.
	 */
	private boolean assignableService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	BusinessServiceLevelTypes(boolean activeDatesSupported, boolean assignableService) {
		this(null, activeDatesSupported, assignableService);
	}


	BusinessServiceLevelTypes(List<BusinessServiceLevelTypes> parentLevelTypeList, boolean activeDatesSupported, boolean assignableService) {
		this.parentLevelTypeList = parentLevelTypeList;
		this.activeDatesSupported = activeDatesSupported;
		this.assignableService = assignableService;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static String getServiceNameAtLevel(BusinessService businessService, BusinessServiceLevelTypes serviceLevelType) {
		if (businessService == null) {
			return null;
		}
		if (businessService.getServiceLevelType() == serviceLevelType) {
			return businessService.getName();
		}
		return getServiceNameAtLevel(businessService.getParent(), serviceLevelType);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<BusinessServiceLevelTypes> getParentLevelTypeList() {
		return this.parentLevelTypeList;
	}


	public boolean isActiveDatesSupported() {
		return this.activeDatesSupported;
	}


	public boolean isAssignableService() {
		return this.assignableService;
	}
}
