package com.clifton.business.service.search;


import com.clifton.business.service.AccountingClosingMethods;
import com.clifton.business.service.BusinessServiceLevelTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class BusinessServiceSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "name")
	private String name;

	// Current max is 5 levels deep
	@SearchField(searchField = "parent.parent.parent.parent.name,parent.parent.parent.name,parent.parent.name,parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String nameExpanded;

	@SearchField
	private Boolean fullyFunded;


	@SearchField
	private BusinessServiceLevelTypes serviceLevelType;

	@SearchField(searchField = "serviceLevelType", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private BusinessServiceLevelTypes excludeServiceLevelType;

	@SearchField(searchField = "serviceLevelType")
	private BusinessServiceLevelTypes[] serviceLevelTypes;

	@SearchField(searchField = "parent.id")
	private Short parentId;

	// Current limit is four levels deep
	@SearchField(searchField = "parent.id,parent.parent.id,parent.parent.parent.id", leftJoin = true)
	private Short parentIdExpanded;

	@SearchField(searchField = "application.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean applicationPresent;

	@SearchField(searchField = "name", searchFieldPath = "application")
	private String applicationName;

	// Option to exclude all root levels
	@SearchField(searchField = "parent.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean childrenOnly;

	@SearchField
	private String investmentStyle;

	@SearchField(searchField = "aumCalculatorBean.id")
	private Integer aumCalculatorBeanId;

	@SearchField
	private AccountingClosingMethods accountingClosingMethod;

	@SearchField
	private String reportTitle;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getNameExpanded() {
		return this.nameExpanded;
	}


	public void setNameExpanded(String nameExpanded) {
		this.nameExpanded = nameExpanded;
	}


	public Boolean getFullyFunded() {
		return this.fullyFunded;
	}


	public void setFullyFunded(Boolean fullyFunded) {
		this.fullyFunded = fullyFunded;
	}


	public BusinessServiceLevelTypes getServiceLevelType() {
		return this.serviceLevelType;
	}


	public void setServiceLevelType(BusinessServiceLevelTypes serviceLevelType) {
		this.serviceLevelType = serviceLevelType;
	}


	public BusinessServiceLevelTypes getExcludeServiceLevelType() {
		return this.excludeServiceLevelType;
	}


	public void setExcludeServiceLevelType(BusinessServiceLevelTypes excludeServiceLevelType) {
		this.excludeServiceLevelType = excludeServiceLevelType;
	}


	public BusinessServiceLevelTypes[] getServiceLevelTypes() {
		return this.serviceLevelTypes;
	}


	public void setServiceLevelTypes(BusinessServiceLevelTypes[] serviceLevelTypes) {
		this.serviceLevelTypes = serviceLevelTypes;
	}


	public Short getParentId() {
		return this.parentId;
	}


	public void setParentId(Short parentId) {
		this.parentId = parentId;
	}


	public Short getParentIdExpanded() {
		return this.parentIdExpanded;
	}


	public void setParentIdExpanded(Short parentIdExpanded) {
		this.parentIdExpanded = parentIdExpanded;
	}


	public Boolean getApplicationPresent() {
		return this.applicationPresent;
	}


	public void setApplicationPresent(Boolean applicationPresent) {
		this.applicationPresent = applicationPresent;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}


	public Boolean getChildrenOnly() {
		return this.childrenOnly;
	}


	public void setChildrenOnly(Boolean childrenOnly) {
		this.childrenOnly = childrenOnly;
	}


	public String getInvestmentStyle() {
		return this.investmentStyle;
	}


	public void setInvestmentStyle(String investmentStyle) {
		this.investmentStyle = investmentStyle;
	}


	public Integer getAumCalculatorBeanId() {
		return this.aumCalculatorBeanId;
	}


	public void setAumCalculatorBeanId(Integer aumCalculatorBeanId) {
		this.aumCalculatorBeanId = aumCalculatorBeanId;
	}


	public AccountingClosingMethods getAccountingClosingMethod() {
		return this.accountingClosingMethod;
	}


	public void setAccountingClosingMethod(AccountingClosingMethods accountingClosingMethod) {
		this.accountingClosingMethod = accountingClosingMethod;
	}


	public String getReportTitle() {
		return this.reportTitle;
	}


	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}
}
