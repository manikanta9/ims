package com.clifton.business.service.search;


import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BusinessServiceBusinessServiceProcessingTypeSearchForm</code> is used to search for
 * BusinessServiceBusinessServiceProcessingType relational data located in the BusinessServiceBusinessServiceProcessingType table.
 *
 * @author davidi
 */
public class BusinessServiceBusinessServiceProcessingTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "businessService.name,businessService.description,businessServiceProcessingType.name,businessServiceProcessingType.description")
	private String searchPattern;

	@SearchField(searchField = "id")
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] idList;

	@SearchField(searchField = "businessService.id")
	private Short businessServiceId;

	@SearchField(searchField = "businessService.id")
	private Short[] businessServiceIdList;

	@SearchField(searchField = "businessServiceProcessingType.id")
	private Short businessServiceProcessingTypeId;

	@SearchField(searchField = "businessServiceProcessingType.id")
	private Short[] businessServiceProcessingTypeIdList;

	@SearchField(searchField = "processingType", searchFieldPath = "businessServiceProcessingType")
	private ServiceProcessingTypes processingType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIdList() {
		return this.idList;
	}


	public void setIdList(Integer[] idList) {
		this.idList = idList;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Short[] getBusinessServiceIdList() {
		return this.businessServiceIdList;
	}


	public void setBusinessServiceIdList(Short[] businessServiceIdList) {
		this.businessServiceIdList = businessServiceIdList;
	}


	public Short getBusinessServiceProcessingTypeId() {
		return this.businessServiceProcessingTypeId;
	}


	public void setBusinessServiceProcessingTypeId(Short businessServiceProcessingTypeId) {
		this.businessServiceProcessingTypeId = businessServiceProcessingTypeId;
	}


	public Short[] getBusinessServiceProcessingTypeIdList() {
		return this.businessServiceProcessingTypeIdList;
	}


	public void setBusinessServiceProcessingTypeIdList(Short[] businessServiceProcessingTypeIdList) {
		this.businessServiceProcessingTypeIdList = businessServiceProcessingTypeIdList;
	}


	public ServiceProcessingTypes getProcessingType() {
		return this.processingType;
	}


	public void setProcessingType(ServiceProcessingTypes processingType) {
		this.processingType = processingType;
	}
}

