package com.clifton.business.shared;

import java.io.Serializable;


/**
 * Represents a company (a legal entity) that can either be a client, a bank or other financial institution, money manager, broker, etc.
 * <p>
 * If a company has no parent, then it is the main company.  If it has a parent then the company itself is actually a subsidiary of the parent company.
 * Subsidiaries can be location based or could be used to distinguish between company subsidiaries that cover different company types.
 *
 * @author vgomelsky
 */
public class Company implements Serializable {

	private int id;
	private Integer bloombergCompanyIdentifier;

	private String type;
	private String name;

	/**
	 * If a company has no parent, then it is the main company.  If it has a parent then the company itself is actually a subsidiary of the parent company.
	 * Subsidiaries can be location based or could be used to distinguish between company subsidiaries that cover different company types.
	 */
	private Company parentCompany;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Integer getBloombergCompanyIdentifier() {
		return this.bloombergCompanyIdentifier;
	}


	public void setBloombergCompanyIdentifier(Integer bloombergCompanyIdentifier) {
		this.bloombergCompanyIdentifier = bloombergCompanyIdentifier;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Company getParentCompany() {
		return this.parentCompany;
	}


	public void setParentCompany(Company parentCompany) {
		this.parentCompany = parentCompany;
	}
}
