package com.clifton.business.shared;

import java.io.Serializable;


/**
 * Represents a person.
 *
 * @author vgomelsky
 */
public class Contact implements Serializable {

	private int id;

	private String firstName;
	private String lastName;
	private String middleName;
	private String namePrefix;
	private String nameSuffix;
	private String saluation;


	private String emailAddress;
	private String phoneNumber;
	private String faxNumber;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getNameLabel() {
		StringBuilder sb = new StringBuilder();
		if (!isEmpty(getNamePrefix())) {
			sb.append(getNamePrefix()).append(" ");
		}
		sb.append(getLabelShort());
		if (!isEmpty(getNameSuffix())) {
			sb.append(" ").append(getNameSuffix());
		}
		return sb.toString();
	}


	public String getLabelShort() {
		StringBuilder sb = new StringBuilder();
		if (!isEmpty(getFirstName())) {
			sb.append(getFirstName()).append(" ");
		}
		if (!isEmpty(getMiddleName())) {
			sb.append(getMiddleName()).append(" ");
		}
		sb.append(getLastName());
		return sb.toString();
	}


	private boolean isEmpty(CharSequence str) {
		return str == null || str.codePoints().allMatch(Character::isWhitespace);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return this.firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return this.lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getMiddleName() {
		return this.middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getNamePrefix() {
		return this.namePrefix;
	}


	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}


	public String getNameSuffix() {
		return this.nameSuffix;
	}


	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}


	public String getSaluation() {
		return this.saluation;
	}


	public void setSaluation(String saluation) {
		this.saluation = saluation;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getFaxNumber() {
		return this.faxNumber;
	}


	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
}
