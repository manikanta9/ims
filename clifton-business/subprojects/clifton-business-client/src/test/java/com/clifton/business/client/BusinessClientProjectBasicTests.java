package com.clifton.business.client;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;


/**
 * @author danielh
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessClientProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "business-client";
	}


	/**
	 * Can be overridden to skip automated testing for certain methods.
	 */
	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("getBusinessClientRelationshipExtendedList");
		skipMethods.add("getBusinessClientCompanyRelationshipExtendedList");
		return skipMethods.contains(method.getName());
	}
}
