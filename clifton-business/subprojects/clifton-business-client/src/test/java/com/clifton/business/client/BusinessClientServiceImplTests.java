package com.clifton.business.client;


import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.client.setup.BusinessClientSetupService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.list.SystemListItemBuilder;
import com.clifton.system.list.SystemListService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>BusinessClientServiceTests</code> ...
 *
 * @author Mary Anderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessClientServiceImplTests {

	@Resource
	private BusinessClientService businessClientService;

	@Resource
	private BusinessClientSetupService businessClientSetupService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private SystemListService systemListService;


	@Test
	public void testCreateUpdateClientCompany() {
		// Verify creating a client will also create it's company with the same name & Client type
		BusinessClient client = new BusinessClient();
		client.setName("Test Client");
		client.setClientType(SystemListItemBuilder.createInvestmentPool().toSystemListItem());
		client.setCategory(this.businessClientSetupService.getBusinessClientCategoryByName("Client"));
		this.businessClientService.saveBusinessClient(client);

		Assertions.assertNotNull(client.getCompany());
		Assertions.assertEquals(client.getName(), client.getCompany().getName());
		// No Legal Name, so Company Name = Short Name and Company shouldn't have an alias set
		Assertions.assertNull(client.getCompany().getAlias());
		Assertions.assertNotNull(client.getCompany().getType());
		Assertions.assertEquals(client.getCategory().getCompanyType(), client.getCompany().getType());

		// Edit an address field through the client company and update the name
		client.getCompany().setAddressLine1("1234 Main Street");
		client.setName("Test Client Updated");
		// Add a Legal Name
		client.setLegalName("Test Client Updated - Legal");

		this.businessClientService.saveBusinessClient(client);

		Assertions.assertNotNull(client.getCompany());
		// Name should now match Legal Name
		Assertions.assertEquals(client.getLegalName(), client.getCompany().getName());
		// Alias should now match Short Name
		Assertions.assertEquals(client.getName(), client.getCompany().getAlias());

		Assertions.assertEquals("1234 Main Street", client.getCompany().getAddressLine1());

		// Pull company directly and verify fields are set
		BusinessCompany company = this.businessCompanyService.getBusinessCompany(client.getCompany().getId());
		Assertions.assertEquals(client.getLegalName(), company.getName());
		Assertions.assertEquals(client.getCategory().getCompanyType(), client.getCompany().getType());
		Assertions.assertEquals("1234 Main Street", company.getAddressLine1());
	}


	@Test
	public void testCreateFund() {
		BusinessClient client = new BusinessClient();
		client.setName("Test Fund");
		client.setCategory(this.businessClientSetupService.getBusinessClientCategoryByName("Commingled Vehicle"));
		this.businessClientService.saveBusinessClient(client);
	}


	@Test
	public void testCreateUpdateFundCompany() {
		// Verify creating a "fund" as a client will also create it's company with the same name & Client type
		BusinessClient client = new BusinessClient();
		client.setName("Test New Fund");
		client.setCategory(this.businessClientSetupService.getBusinessClientCategoryByName("Commingled Vehicle"));
		this.businessClientService.saveBusinessClient(client);

		Assertions.assertNotNull(client.getCompany());
		Assertions.assertEquals(client.getName(), client.getCompany().getName());
		Assertions.assertNotNull(client.getCompany().getType());
		Assertions.assertEquals(client.getCategory().getCompanyType(), client.getCompany().getType());

		// Edit an address field through the client company and update the name
		client.getCompany().setAddressLine1("1234 Main Street");
		client.setName("Test Client Updated");
		this.businessClientService.saveBusinessClient(client);

		Assertions.assertNotNull(client.getCompany());
		Assertions.assertEquals(client.getName(), client.getCompany().getName());
		Assertions.assertEquals("1234 Main Street", client.getCompany().getAddressLine1());

		// Pull company directly and verify fields are set
		BusinessCompany company = this.businessCompanyService.getBusinessCompany(client.getCompany().getId());
		Assertions.assertEquals(client.getName(), company.getName());
		Assertions.assertEquals(client.getCategory().getCompanyType(), client.getCompany().getType());
		Assertions.assertEquals("1234 Main Street", company.getAddressLine1());
	}


	@Test
	public void testLinkBusinessClientToParent() {
		BusinessClientRelationship rel1 = new BusinessClientRelationship();
		rel1.setName("Client Relationship 1");
		rel1 = this.businessClientService.saveBusinessClientRelationship(rel1);

		BusinessClientRelationship rel2 = new BusinessClientRelationship();
		rel2.setName("Client Relationship 2");
		rel2 = this.businessClientService.saveBusinessClientRelationship(rel2);

		BusinessClientCategory fundGroupCategory = this.businessClientSetupService.getBusinessClientCategoryByName("Commingled Vehicle Group");
		BusinessClientCategory fundCategory = this.businessClientSetupService.getBusinessClientCategoryByName("Commingled Vehicle");
		BusinessClientCategory clientGroupCategory = this.businessClientSetupService.getBusinessClientCategoryByName("Client Group");
		BusinessClientCategory clientCategory = this.businessClientSetupService.getBusinessClientCategoryByName("Client");

		BusinessClient clientGroup = createBusinessClient(clientGroupCategory, "Test Parent Client", rel1);
		BusinessClient fundGroup = createBusinessClient(fundGroupCategory, "Test Fund Group", null);

		// Valid Parent/Child Relationship
		BusinessClient childClient = createBusinessClient(clientCategory, "Child Client", rel1);
		childClient = linkBusinessClientToParent(clientGroup, childClient);

		BusinessClient childFund = createBusinessClient(fundCategory, "Fund Client", null);
		childFund = linkBusinessClientToParent(fundGroup, childFund);

		// Try to assign a child with a different relationship
		BusinessClient childClient2 = createBusinessClient(clientCategory, "Child Client - 2", rel2);
		validateExceptionOnClientLink(clientGroup, childClient2, "Unable to link client [Child Client - 2] to parent client [Test Parent Client] because they belong to different client relationships.");

		// Remove Relationship From Parent and Then add the client of a different relationship
		clientGroup.setClientRelationship(null);
		clientGroup = this.businessClientService.saveBusinessClient(clientGroup);
		childClient2 = linkBusinessClientToParent(clientGroup, childClient2);

		// Try to Set the Relationship Back on the Parent Client Group
		clientGroup.setClientRelationship(rel1);
		validateExceptionOnClientSave(clientGroup, "This client [Test Parent Client] has a child Child Client - 2 whose client relationship is set to [Client Relationship 2]. Cannot have clients that belong to a different relationship than its children.You can clear the client relationship from the parent if you would like to have a cross client relationship children.");

		// Remove the Link to the second child client
		childClient2 = deleteBusinessClientLinkToParent(childClient2);

		// Save the Client Group Again
		clientGroup = this.businessClientService.saveBusinessClient(clientGroup);

		// Now try to change the relationship on the existing child client
		childClient.setClientRelationship(rel2);
		validateExceptionOnClientSave(childClient, "This client [Child Client] is a member of Client Group whose client relationship is set to [Client Relationship 1]. Cannot have clients that belong to a different relationship than its parent.");
		// Put the relationship back for future saves
		childClient.setClientRelationship(rel1);

		// Try to assign a child as a parent of the parent
		validateExceptionOnClientLink(childClient, clientGroup, "Parent Client [Child Client] is in category [Client] which does not allow children.");

		// Try to add the Fund as a child of the client group
		BusinessClient childFund2 = createBusinessClient(fundCategory, "Fund Client - 2", null);
		validateExceptionOnClientLink(clientGroup, childFund2, "Parent Client [Test Parent Client] only allows children in category [Client]. Child client is in category [Commingled Vehicle]");

		// Try to add Client as a child of the client group
		validateExceptionOnClientLink(fundGroup, childClient2, "Parent Client [Test Fund Group] only allows children in category [Commingled Vehicle]. Child client is in category [Client]");

		// Try to change the parent to not the Client Group type
		clientGroup.setCategory(clientCategory);
		validateExceptionOnClientSave(clientGroup, "Cannot change category from [Client Group] to [Client].");

		// Try to change the child to a Client Group type
		childClient.setCategory(clientGroupCategory);
		validateExceptionOnClientSave(childClient, "Cannot change category from [Client] to [Client Group].");
	}


	@Test
	public void testAddBusinessClientToTerminatedRelationship() {
		BusinessClientRelationship relationship = new BusinessClientRelationship();
		relationship.setName("Client Relationship Terminated");
		relationship.setTerminateDate(DateUtils.toDate("01/01/2019"));
		relationship = this.businessClientService.saveBusinessClientRelationship(relationship);

		BusinessClient client = new BusinessClient();
		client.setCategory(this.businessClientSetupService.getBusinessClientCategoryByName("Client"));
		client.setName("Test Invalid Client");
		client.setClientRelationship(relationship);
		validateExceptionOnClientSave(client, "Cannot add a client to a terminated client relationship.");
	}


	private BusinessClient createBusinessClient(BusinessClientCategory category, String name, BusinessClientRelationship relationship) {
		BusinessClient client = new BusinessClient();
		client.setCategory(category);
		client.setName(name);
		client.setClientRelationship(relationship);

		client.setClientType(this.systemListService.getSystemListItem(1281));
		return this.businessClientService.saveBusinessClient(client);
	}


	private void validateExceptionOnClientSave(BusinessClient client, String message) {
		try {
			this.businessClientService.saveBusinessClient(client);
		}
		catch (Exception e) {
			if (message.equals(e.getMessage())) {
				return;
			}
			throw new RuntimeException("Expected exception with message [" + message + "] but instead go error with message [" + e.getMessage() + "]", e);
		}
		Assertions.fail("Expected exception with message [" + message + "] to be thrown during the save, but no Validation Exception was thrown.");
	}


	/**
	 * Returns the child client which has Parent populated
	 */
	private BusinessClient linkBusinessClientToParent(BusinessClient parent, BusinessClient child) {
		this.businessClientService.linkBusinessClientToParent(parent.getId(), child.getId());
		return this.businessClientService.getBusinessClient(child.getId());
	}


	/**
	 * Returns the child client which has Parent cleared
	 */
	private BusinessClient deleteBusinessClientLinkToParent(BusinessClient child) {
		this.businessClientService.deleteBusinessClientLinkToParent(child.getId());
		return this.businessClientService.getBusinessClient(child.getId());
	}


	private void validateExceptionOnClientLink(BusinessClient parent, BusinessClient child, String message) {
		try {
			this.businessClientService.linkBusinessClientToParent(parent.getId(), child.getId());
		}
		catch (Exception e) {
			if (message.equals(e.getMessage())) {
				return;
			}
			throw new RuntimeException("Expected exception with message [" + message + "] but instead go error with message [" + e.getMessage() + "]", e);
		}
		Assertions.fail("Expected exception with message [" + message + "] to be thrown during the save, but no Validation Exception was thrown.");
	}
}
