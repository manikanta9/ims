package com.clifton.business.client.setup;

import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.company.BusinessCompanyTypeBuilder;


public class BusinessClientCategoryBuilder {

	private BusinessClientCategory businessClientCategory;


	private BusinessClientCategoryBuilder(BusinessClientCategory businessClientCategory) {
		this.businessClientCategory = businessClientCategory;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessClientCategoryBuilder createClientInvestingEntity() {
		BusinessClientCategory businessClientCategory = new BusinessClientCategory();

		businessClientCategory.setId((short) 1);
		businessClientCategory.setName("Client (Investing Entity)");
		businessClientCategory.setDescription("A Client (Investing Entity) is defined as the legal entity that is also the beneficial owner of the assets. A Client (Investing Entity) is assigned a unique Tax Identification Number (E.I.N. or Social Security number). The Client (Investing Entity) is the legal entity for which Bank and Brokerage accounts are established for portfolio management. An exception may be made when the Client (Investing Entity) operates multiple pools under a single Tax ID, and the pools must be distinguished for marketing purposes. In contrast to a Sister Client, these entities are utilizing products sponsored by the Mpls office, where the composite is also managed and the AUM is claimed.  The Client Account is generally 'issued by' Parametric Minneapolis.");
		businessClientCategory.setCompanyType(BusinessCompanyTypeBuilder.createClient().toBusinessCompanyType());
		businessClientCategory.setCategoryType(BusinessClientCategoryTypes.CLIENT);
		businessClientCategory.setDetailScreenClass("Clifton.business.client.BasicClientWindow");
		businessClientCategory.setClientRelationshipUsed(true);

		return new BusinessClientCategoryBuilder(businessClientCategory);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessClientCategoryBuilder withId(short id) {
		getBusinessClientCategory().setId(id);
		return this;
	}


	public BusinessClientCategoryBuilder withName(String name) {
		getBusinessClientCategory().setName(name);
		return this;
	}


	public BusinessClientCategoryBuilder withDescription(String description) {
		getBusinessClientCategory().setDescription(description);
		return this;
	}


	public BusinessClientCategoryBuilder withCompanyType(BusinessCompanyType businessCompanyType) {
		getBusinessClientCategory().setCompanyType(businessCompanyType);
		return this;
	}


	public BusinessClientCategoryBuilder withCategoryType(BusinessClientCategoryTypes businessClientCategoryTypes) {
		getBusinessClientCategory().setCategoryType(businessClientCategoryTypes);
		return this;
	}


	public BusinessClientCategoryBuilder withDetailScreenClass(String detailScreenClass) {
		getBusinessClientCategory().setDetailScreenClass(detailScreenClass);
		return this;
	}


	public BusinessClientCategoryBuilder withClientRelationshipUsed(boolean clientRelationshipUsed) {
		getBusinessClientCategory().setClientRelationshipUsed(clientRelationshipUsed);
		return this;
	}


	public BusinessClientCategory toBusinessClientCategory() {
		return this.businessClientCategory;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessClientCategory getBusinessClientCategory() {
		return this.businessClientCategory;
	}
}
