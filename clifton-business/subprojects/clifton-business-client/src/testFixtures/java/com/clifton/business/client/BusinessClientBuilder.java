package com.clifton.business.client;

import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.client.setup.BusinessClientCategoryBuilder;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListItemBuilder;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.definition.WorkflowStatusBuilder;


public class BusinessClientBuilder {

	private BusinessClient businessClient;


	private BusinessClientBuilder(BusinessClient businessClient) {
		this.businessClient = businessClient;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessClientBuilder createTestingClient1() {
		BusinessClient businessClient = new BusinessClient();

		businessClient.setId(108);
		businessClient.setName("test 1");
		businessClient.setDescription("test 1");
		businessClient.setClientRelationship(BusinessClientRelationshipBuilder.createTestGovernment().toBusinessClientRelationship());
		businessClient.setCompany(BusinessCompanyBuilder.createClient5().toBusinessCompany());
		businessClient.setCategory(BusinessClientCategoryBuilder.createClientInvestingEntity().toBusinessClientCategory());
		businessClient.setClientType(SystemListItemBuilder.createInvestmentPool().toSystemListItem());
		businessClient.setLegalName("test 1");
		businessClient.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		businessClient.setWorkflowState(WorkflowStateBuilder.createActiveClient().toWorkflowState());
		businessClient.setOrigin("State Government");
		businessClient.setFederalTaxNumber("89-1234567");

		return new BusinessClientBuilder(businessClient);
	}


	public static BusinessClientBuilder createTestingClient2() {
		BusinessClient businessClient = new BusinessClient();

		businessClient.setId(109);
		businessClient.setName("test 2");
		businessClient.setDescription("test 2");
		businessClient.setClientRelationship(BusinessClientRelationshipBuilder.createConsultant().toBusinessClientRelationship());

		businessClient.setCompany(BusinessCompanyBuilder.createClient1().toBusinessCompany());
		businessClient.setCategory(BusinessClientCategoryBuilder.createClientInvestingEntity().toBusinessClientCategory());

		businessClient.setClientType(SystemListItemBuilder.createInvestmentPool().toSystemListItem());
		businessClient.setLegalName("test 2");
		businessClient.setWorkflowState(WorkflowStateBuilder.createActiveClient().toWorkflowState());
		businessClient.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		businessClient.setInceptionDate(DateUtils.toDate("04/04/2004"));
		businessClient.setFederalTaxNumber("88-874757");

		return new BusinessClientBuilder(businessClient);
	}


	public static BusinessClientBuilder createTestingClient3() {
		BusinessClient businessClient = new BusinessClient();

		businessClient.setId(110);
		businessClient.setName("test 3");
		businessClient.setDescription("test 3");
		businessClient.setClientRelationship(BusinessClientRelationshipBuilder.createConsultant().toBusinessClientRelationship());

		businessClient.setCompany(BusinessCompanyBuilder.createClient1().toBusinessCompany());
		businessClient.setCategory(BusinessClientCategoryBuilder.createClientInvestingEntity().toBusinessClientCategory());

		businessClient.setClientType(SystemListItemBuilder.createInvestmentPool().toSystemListItem());
		businessClient.setLegalName("test 3");
		businessClient.setWorkflowState(WorkflowStateBuilder.createActiveClient().toWorkflowState());
		businessClient.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		businessClient.setInceptionDate(DateUtils.toDate("04/04/2004"));
		businessClient.setFederalTaxNumber("88-8747453");

		return new BusinessClientBuilder(businessClient);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessClientBuilder withId(int id) {
		getBusinessClient().setId(id);
		return this;
	}


	public BusinessClientBuilder withName(String name) {
		getBusinessClient().setName(name);
		return this;
	}


	public BusinessClientBuilder withDescription(String description) {
		getBusinessClient().setDescription(description);
		return this;
	}


	public BusinessClientBuilder withClientRelationship(BusinessClientRelationship businessClientRelationship) {
		getBusinessClient().setClientRelationship(businessClientRelationship);
		return this;
	}


	public BusinessClientBuilder withCompany(BusinessCompany company) {
		getBusinessClient().setCompany(company);
		return this;
	}


	public BusinessClientBuilder withCategory(BusinessClientCategory businessClientCategory) {
		getBusinessClient().setCategory(businessClientCategory);
		return this;
	}


	public BusinessClientBuilder withClientType(SystemListItem clientType) {
		getBusinessClient().setClientType(clientType);
		return this;
	}


	public BusinessClientBuilder withLegalName(String legalName) {
		getBusinessClient().setLegalName(legalName);
		return this;
	}


	public BusinessClientBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		getBusinessClient().setWorkflowStatus(workflowStatus);
		return this;
	}


	public BusinessClientBuilder withWorkflowState(WorkflowState workflowState) {
		getBusinessClient().setWorkflowState(workflowState);
		return this;
	}


	public BusinessClientBuilder withOrigin(String origin) {
		getBusinessClient().setOrigin(origin);
		return this;
	}


	public BusinessClientBuilder withFederalTaxNumber(String federalTaxNumber) {
		getBusinessClient().setFederalTaxNumber(federalTaxNumber);
		return this;
	}


	public BusinessClient toBusinessClient() {
		return this.businessClient;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessClient getBusinessClient() {
		return this.businessClient;
	}
}
