package com.clifton.business.client;

import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.list.SystemListItemBuilder;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.definition.WorkflowStatusBuilder;


public class BusinessClientRelationshipBuilder {

	private BusinessClientRelationship businessClientRelationship;


	private BusinessClientRelationshipBuilder(BusinessClientRelationship businessClientRelationship) {
		this.businessClientRelationship = businessClientRelationship;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessClientRelationshipBuilder createTestGovernment() {
		BusinessClientRelationship businessClientRelationship = new BusinessClientRelationship();

		businessClientRelationship.setId(85);
		businessClientRelationship.setCompany(BusinessCompanyBuilder.createTestingCompany1().toBusinessCompany());
		businessClientRelationship.setName("Test");
		businessClientRelationship.setLegalName("Test");
		businessClientRelationship.setDescription("Test");
		businessClientRelationship.setRelationshipType(SystemListItemBuilder.createGovernmentPublic().toSystemListItem());
		businessClientRelationship.setWorkflowState(WorkflowStateBuilder.createActive().toWorkflowState());
		businessClientRelationship.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());

		return new BusinessClientRelationshipBuilder(businessClientRelationship);
	}


	public static BusinessClientRelationshipBuilder createConsultant() {
		BusinessClientRelationship businessClientRelationship = new BusinessClientRelationship();

		businessClientRelationship.setId(86);
		businessClientRelationship.setCompany(BusinessCompanyBuilder.createTestingCompany3().toBusinessCompany());
		businessClientRelationship.setName("Test 2");
		businessClientRelationship.setLegalName("Test 2");
		businessClientRelationship.setDescription("nonprofit");
		businessClientRelationship.setRelationshipType(SystemListItemBuilder.createHealthServicesOrg().toSystemListItem());
		businessClientRelationship.setRelationshipSubType(SystemListItemBuilder.createNonProfit().toSystemListItem());
		businessClientRelationship.setWorkflowState(WorkflowStateBuilder.createActive().toWorkflowState());
		businessClientRelationship.setWorkflowStatus(WorkflowStatusBuilder.createActive().toWorkflowStatus());
		businessClientRelationship.setWorkflowStateEffectiveStartDate(DateUtils.toDate("04/04/2005"));
		businessClientRelationship.setInceptionDate(DateUtils.toDate("04/04/2005"));

		return new BusinessClientRelationshipBuilder(businessClientRelationship);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessClientRelationshipBuilder withId(int id) {
		getBusinessClientRelationship().setId(id);
		return this;
	}


	public BusinessClientRelationshipBuilder withName(String name) {
		getBusinessClientRelationship().setName(name);
		return this;
	}


	public BusinessClientRelationshipBuilder withDescription(String description) {
		getBusinessClientRelationship().setDescription(description);
		return this;
	}


	public BusinessClientRelationship toBusinessClientRelationship() {
		return this.businessClientRelationship;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessClientRelationship getBusinessClientRelationship() {
		return this.businessClientRelationship;
	}
}
