Ext.ns('Clifton.business.client', 'Clifton.business.client.relationship');

Clifton.business.client.ClientWindowBaseAdditionalTabs = [];
Clifton.business.client.ClientFundWindowAdditionalTabs = [];

Clifton.business.client.ClientRelationshipWindowAdditionalTabs = [];

Clifton.business.company.CompanyWindowOverrides['Client'] = {
	className: 'Clifton.business.client.ClientWindow',
	getEntity: function(config, entity) {
		if (entity) {
			const data = TCG.data.getData('businessClientByCompany.json?companyId=' + entity.id, this);
			config.params.id = data.id;
			return data;
		}
	}
};
Clifton.business.company.CompanyWindowOverrides['Client Relationship'] = {
	className: 'Clifton.business.client.relationship.ClientRelationshipWindow',
	getEntity: function(config, entity) {
		if (entity) {
			const data = TCG.data.getData('businessClientRelationshipByCompany.json?companyId=' + entity.id, this);
			config.params.id = data.id;
			return data;
		}
	}
};


Clifton.business.client.ClientGrid = Ext.extend(TCG.grid.GridPanel, {
	categoryType: 'REQUIRED', // CLIENT, VIRTUAL_CLIENT, FUND

	showClientRelationship: true,
	showClientType: true,
	showFirmCategory: false,

	name: 'businessClientListFind',
	xtype: 'gridpanel',
	instructions: 'A client (investing entity) is a legal entity with its own tax id and IMA that is governed by a client relationship.',
	topToolbarSearchParameter: 'searchPattern',
	rowSelectionModel: 'multiple',


	initComponent: function() {
		Clifton.business.client.ClientGrid.superclass.initComponent.call(this);
		// hide ClientRelationship and retail column if specified
		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('clientRelationship.name'), TCG.isFalse(this.showClientRelationship));
		cm.setHidden(cm.findColumnIndex('clientRelationship.relationshipCategory.retail'), TCG.isFalse(this.showClientRelationship));

		// hide client type column if specified
		cm.setHidden(cm.findColumnIndex('clientType.text'), TCG.isFalse(this.showClientType));

		// show Firm Category/Sub-Category if requested
		cm.setHidden(cm.findColumnIndex('clientRelationship.relationshipType.text'), TCG.isFalse(this.showFirmCategory));
		cm.setHidden(cm.findColumnIndex('clientRelationship.relationshipSubType.text'), TCG.isFalse(this.showFirmCategory));
	},


	columns: [
		{header: 'Category', width: 40, dataIndex: 'category.name', filter: {searchFieldName: 'categoryName'}, hidden: true},
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Group', width: 75, dataIndex: 'parent.name', hidden: true, filter: {searchFieldName: 'parentName'}},
		{header: 'Short Client Name', width: 75, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Legal Client Name', width: 75, hidden: true, dataIndex: 'legalLabel', filter: {searchFieldName: 'searchPattern'}},
		{header: 'Client Type', width: 40, dataIndex: 'clientType.text', filter: {searchFieldName: 'clientTypeName'}},
		{header: 'Short Relationship Name', width: 75, dataIndex: 'clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}},
		{header: 'Legal Relationship Name', width: 75, hidden: true, dataIndex: 'clientRelationship.legalLabel', filter: {searchFieldName: 'clientRelationshipName'}},
		{header: 'Relationship Category', width: 40, hidden: true, dataIndex: 'clientRelationship.relationshipCategory.name', filter: {searchFieldName: 'clientRelationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
		{header: 'Retail', width: 20, dataIndex: 'clientRelationship.relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'clientRelationshipCategoryRetail'}},
		{header: 'Firm Category', width: 60, hidden: true, dataIndex: 'clientRelationship.relationshipType.text', filter: {searchFieldName: 'clientRelationshipTypeName'}},
		{header: 'Firm Sub-Category', width: 60, hidden: true, dataIndex: 'clientRelationship.relationshipSubType.text', filter: {searchFieldName: 'clientRelationshipSubTypeName'}},
		{header: 'Inception Date', width: 30, dataIndex: 'inceptionDate'},
		{header: 'Terminate Date', width: 30, dataIndex: 'terminateDate'},
		{
			header: 'Active', width: 20, dataIndex: 'active', type: 'boolean', sortable: false,
			tooltip: 'Active flag driven by Inception/Terminate Dates'
		},

		{header: 'Relationship Workflow State', width: 45, hidden: true, dataIndex: 'clientRelationship.workflowState.name', filter: {searchFieldName: 'clientRelationshipWorkflowStateName', comparison: 'BEGINS_WITH'}},
		{
			header: 'Relationship Workflow State Effective Start', width: 30, hidden: true, dataIndex: 'clientRelationship.workflowStateEffectiveStartDate', filter: {searchFieldName: 'clientRelationshipWorkflowStateEffectiveStart'},
			tooltip: 'Date indicating when the Client Relationship Workflow State was changed.'
		},

		{
			header: 'Account Workflow State', width: 45, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName', comparison: 'BEGINS_WITH'},
			tooltip: 'Account workflow state is populated automatically based on its Client Accounts Workflow States.'
		},
		{header: 'Account Workflow Status', width: 45, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName', comparison: 'BEGINS_WITH'}, hidden: true},

		{header: 'Taxable', width: 20, dataIndex: 'taxable', type: 'boolean', hidden: true},
		{header: 'Federal Tax #', width: 40, dataIndex: 'federalTaxNumber', hidden: true},
		{header: 'State Tax #', width: 40, dataIndex: 'stateTaxNumber', hidden: true},

		{header: 'Origin', width: 40, dataIndex: 'origin', hidden: true},
		{header: 'Domicile', width: 40, dataIndex: 'originDomicile', hidden: true},

		{header: 'Address 1', width: 75, dataIndex: 'company.addressLine1', hidden: true, filter: {searchFieldName: 'addressLine1'}},
		{header: 'Address 2', width: 75, dataIndex: 'company.addressLine2', hidden: true, filter: {searchFieldName: 'addressLine2'}},
		{header: 'Address 3', width: 75, dataIndex: 'company.addressLine3', hidden: true, filter: {searchFieldName: 'addressLine3'}},
		{header: 'City', width: 75, dataIndex: 'company.city', hidden: true, filter: {searchFieldName: 'city'}},
		{header: 'State', width: 50, dataIndex: 'company.state', hidden: true, filter: {searchFieldName: 'state'}},
		{header: 'Postal Code', width: 50, dataIndex: 'company.postalCode', hidden: true, filter: {searchFieldName: 'postalCode'}},
		{header: 'Country', width: 75, dataIndex: 'company.country', hidden: true, filter: {searchFieldName: 'country'}}
	],
	editor: {
		detailPageClass: 'Clifton.business.client.ClientWindow',
		addEditButtons: function(toolBar) {
			TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
			toolBar.add({
				text: 'Add Note',
				tooltip: 'Add a note linked to selected client(s).',
				iconCls: 'pencil',
				scope: this,
				handler: function() {
					const gridPanel = this;
					const grid = this.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one row to add a note to.', 'No Row(s) Selected');
					}
					else {
						const cIds = [];
						const ut = sm.getSelections();
						for (let i = 0; i < ut.length; i++) {
							cIds.push(ut[i].json.id);
						}

						let className = 'Clifton.system.note.SingleNoteWindow';

						const tbl = TCG.data.getData('systemTableByName.json?tableName=BusinessClient', 'system.table.BusinessClient');
						let dd = {
							noteType: {table: tbl}
						};
						if (cIds.length === 1) {
							dd = Ext.apply(dd, {linkedToMultiple: false, fkFieldId: cIds[0]});
						}
						else {
							className = 'Clifton.system.note.LinkedNoteWindow';
							dd = Ext.apply(dd, {linkedToMultiple: true, fkFieldIds: cIds});
						}
						const cmpId = TCG.getComponentId(className, undefined);
						TCG.createComponent(className, {
							id: cmpId,
							defaultData: dd,
							openerCt: gridPanel
						});
					}
				}
			});
		},
		addToolbarAddButton: function(toolBar) {
			const gp = this.getGridPanel();
			const editor = this;
			// Loop through Categories for Category Type
			const loader = new TCG.data.JsonLoader({
				waitTarget: toolBar,
				waitMsg: 'Loading categories...',
				params: {categoryType: gp.categoryType},
				onLoad: function(records, conf) {
					if (records && records.length > 0) {
						if (records.length === 1) {
							const record = records[0];
							gp.instructions = record.description;
							toolBar.insert(0, {
								text: 'Add',
								tooltip: 'Add a new ' + record.name,
								iconCls: 'add',
								category: record,
								scope: this,
								handler: function(b, e) {
									editor.openNewDetailPage(editor.detailPageClass, gp, b.category);
								}
							});
						}
						else {
							const menu = new Ext.menu.Menu();
							gp.instructions = '<ul class="c-list">';
							for (let i = 0; i < records.length; i++) {
								const record = records[i];
								gp.instructions += '<li><b>' + record.name + '</b>: ' + record.description + '</li>';
								menu.add({
									scope: this,
									text: record.name,
									category: record,
									tooltip: 'Add a new ' + record.name,
									handler: function(b, e) {
										editor.openNewDetailPage(editor.detailPageClass, gp, b.category);
									}
								});
							}
							gp.instructions += '</ul>';
							toolBar.insert(0, {
								text: 'Add',
								tooltip: 'Add a new item',
								iconCls: 'add',
								xtype: 'splitbutton',
								menu: menu,
								scope: this
							});
						}
						toolBar.insert(1, '-');
						toolBar.doLayout();
					}
				}
			});
			loader.load('businessClientCategoryListFind.json');
		},

		openNewDetailPage: function(className, gridPanel, category) {
			const defaultData = {category: category};
			const cmpId = TCG.getComponentId(className, id);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: defaultData,
				params: undefined,
				openerCt: gridPanel,
				defaultIconCls: gridPanel.getWindow().iconCls
			});
		}
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			// default to active
			this.setFilterValue('active', true);
		}
		return {
			categoryType: this.categoryType
		};

	},
	onBeforeReturnFilterData: function(filters) {
		const toolbar = this.gridPanel.topToolbar;
		const accountGroupId = TCG.getChildByName(toolbar, 'accountGroupId').getValue();
		if (TCG.isNotBlank(accountGroupId)) {
			filters.push({
				field: 'investmentAccountGroupId',
				data: {comparison: 'EQUALS', value: accountGroupId}
			});
		}
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Account Group', name: 'accountGroupId', xtype: 'toolbar-combo', width: 200, url: 'investmentAccountGroupListFind.json'},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}];
	}
});
Ext.reg('business-client-grid', Clifton.business.client.ClientGrid);
