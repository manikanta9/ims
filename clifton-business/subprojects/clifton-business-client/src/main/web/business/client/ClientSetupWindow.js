Clifton.business.client.ClientSetupWindow = Ext.extend(TCG.app.Window, {
	title: 'Clients & Relationships',
	id: 'clientSetupWindow',
	width: 1500,
	height: 700,
	iconCls: 'client',

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Clients',
				items: [{
					xtype: 'business-client-grid',
					categoryType: 'CLIENT',
					showFirmCategory: true
				}]
			},


			{
				title: 'Sister Clients',
				items: [{
					xtype: 'business-client-grid',
					categoryType: 'SISTER_CLIENT',
					showClientRelationship: false,
					showClientType: false
				}]
			},


			{
				title: 'Virtual Clients',
				items: [{
					xtype: 'business-client-grid',
					categoryType: 'VIRTUAL_CLIENT',
					showClientRelationship: false,
					showClientType: false,
					showFirmCategory: true,
					groupField: 'category.name'
				}]
			},


			{
				title: 'Commingled Vehicles',
				items: [{
					xtype: 'business-client-grid',
					categoryType: 'FUND',
					showClientRelationship: false,
					showClientType: false
				}]
			},


			{
				title: 'Client Relationships',
				items: [{
					name: 'businessClientRelationshipExtendedListFind',
					xtype: 'gridpanel',
					instructions: 'A client relationship (organization) is an umbrella relationship that encompasses all subsidiary related client accounts.' +
						'A general rule of thumb is that if there is a single decision making authority for multiple related clients they are grouped as a client relationship.' +
						'<ul class="c-list">' +
						'<li>Are the clients (investment entities)/accounts related?</li>' +
						'<li>Is there one decision maker?</li></ul>' +
						'If the answer is yes to both of the above – they are considered one client relationship (organization).',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Short Relationship Name', width: 120, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}, defaultSortColumn: true, defaultSortDirection: 'ASC'},
						{header: 'Legal Relationship Name', width: 120, dataIndex: 'legalLabel', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Category', width: 70, hidden: true, dataIndex: 'relationshipCategory.name', filter: {searchFieldName: 'relationshipCategoryId', type: 'combo', url: 'businessClientRelationshipCategoryListFind.json'}},
						{header: 'Retail', width: 35, dataIndex: 'relationshipCategory.retail', type: 'boolean', filter: {searchFieldName: 'relationshipCategoryRetail'}},
						{header: 'Firm Category', width: 70, dataIndex: 'relationshipType.text', filter: {searchFieldName: 'relationshipTypeName'}},
						{header: 'Firm Sub-Category', width: 70, dataIndex: 'relationshipSubType.text', filter: {searchFieldName: 'relationshipSubTypeName'}},
						{header: 'Relationship Mgr', tooltip: 'Portfolio Manager', width: 60, dataIndex: 'portfolioManagerContact.nameLabel', filter: {searchFieldName: 'portfolioManagerContactName'}},
						{header: 'NBD Client Rep', width: 60, dataIndex: 'newBusinessContact.nameLabel', filter: {searchFieldName: 'newBusinessContactName'}},
						{header: 'Workflow State', width: 50, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateNames', type: 'list', options: [['Prospect', 'Prospect'], ['Client-To-Be', 'Client-To-Be'], ['Active', 'Active'], ['Inactive', 'Inactive'], ['Terminated', 'Terminated']]}},
						{header: 'Workflow Status', width: 50, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
						{header: 'Effective Start', width: 40, dataIndex: 'workflowStateEffectiveStartDate', filter: {searchFieldName: 'workflowStateEffectiveStartDate'}, hidden: true},
						{header: 'Inception Date', width: 40, dataIndex: 'inceptionDate'},
						{header: 'Terminate Date', width: 40, hidden: true, dataIndex: 'terminateDate'}
					],
					editor: {
						detailPageClass: 'Clifton.business.client.relationship.ClientRelationshipWindow',
						deleteURL: 'businessClientRelationshipDelete.json'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('workflowState.name', ['Active', 'Inactive'], false, true);
						}
						return {};
					}
				}]
			},


			{
				title: 'Company Links',
				items: [{
					xtype: 'gridpanel',
					name: 'businessClientCompanyRelationshipExtendedListFind',
					instructions: 'The following displays a customized view of the Business Company Relationships but with their explicit links to the Client Relationship and/or Client and the related company.  This can be used to find a list of client relationships/clients that utilize a specific company for a given relationship type, i.e. Outsourced CIO, Consultant, etc.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Relationship Type', name: 'relationshipTypeName', xtype: 'system-list-combo', listName: 'Company Relationship Types',
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							},
							{
								fieldLabel: 'Related Company', name: 'relatedCompanyIdOrParent', xtype: 'toolbar-combo', url: 'businessClientCompanyRelationshipExtendedRelatedCompanyListFind.json?orderBy=relatedCompanyName:ASC', displayField: 'nameWithType', qtip: 'Includes children, so you can search for a company and include all of the branches of that company',
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									combo.resetStore(); // Requery each time to get relationship type filters applied
									const gridPanel = TCG.getParentByClass(combo, TCG.grid.GridPanel);
									const relationshipType = TCG.getChildByName(gridPanel.getTopToolbar(), 'relationshipTypeName').getValue();
									queryEvent.combo.store.setBaseParam('nameEquals', relationshipType);
								}
							}
						];
					},
					groupField: 'name',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Relationships" : "Relationship"]})',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Relationship Type', width: 100, dataIndex: 'name', hidden: true, defaultSortColumn: true},
						{header: 'Client Relationship', width: 150, dataIndex: 'clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}},
						{header: 'Client Relationship Workflow State', width: 75, dataIndex: 'clientRelationship.workflowState.name', filter: {searchFieldName: 'clientRelationshipWorkflowStateNames', orNull: true, type: 'list', options: [['Prospect', 'Prospect'], ['Client-To-Be', 'Client-To-Be'], ['Active', 'Active'], ['Inactive', 'Inactive'], ['Terminated', 'Terminated']]}},
						{header: 'Client', width: 150, dataIndex: 'client.name', filter: {searchFieldName: 'clientName'}},
						{header: 'Client Inception Date', width: 30, hidden: true, dataIndex: 'client.inceptionDate', filter: {searchFieldName: 'clientInceptionDate'}},
						{header: 'Client Terminate Date', width: 30, hidden: true, dataIndex: 'client.terminateDate', filter: {searchFieldName: 'clientTerminateDate'}},
						{
							header: 'Client Active', width: 50, dataIndex: 'client.active', type: 'boolean', sortable: false, filter: {searchFieldName: 'clientActive', orNull: true},
							tooltip: 'Active flag driven by Client Inception/Terminate Dates'
						},
						{header: 'Related Company', width: 150, dataIndex: 'relatedCompany.label', filter: {searchFieldName: 'relatedCompanyName'}},
						{header: 'Related Company Type', width: 100, dataIndex: 'relatedCompany.type.name', hidden: true, filter: {searchFieldName: 'relatedCompanyTypeId', type: 'combo', url: 'businessCompanyTypeListFind.json'}},

						{header: 'Identification Code Type', width: 100, dataIndex: 'identificationCodeType', hidden: true},
						{header: 'Identification Code', width: 100, dataIndex: 'identificationCode', hidden: true},

						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},

						{
							header: 'Private', width: 50, dataIndex: 'privateRelationship', type: 'boolean', hidden: true,
							tooltip: 'Private relationships are excluded from Client facing reports.'
						},

						{header: 'Relationship Active', width: 50, dataIndex: 'active', type: 'boolean', tooltip: 'If the company relationship is active or not', sortable: false},
						{header: 'Relationship Start Date', width: 50, dataIndex: 'startDate', hidden: true},
						{header: 'Relationship End Date', width: 50, dataIndex: 'endDate', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
							this.setFilterValue('client.active', true);
							this.setFilterValue('clientRelationship.workflowState.name', ['Active', 'Inactive'], false, true);
						}
						const params = {};
						const tbar = this.getTopToolbar();
						const relationshipTypeName = TCG.getChildByName(tbar, 'relationshipTypeName').getValue();

						if (TCG.isNotBlank(relationshipTypeName)) {
							params.nameEquals = relationshipTypeName;
						}

						const relatedCompanyId = TCG.getChildByName(tbar, 'relatedCompanyIdOrParent').getValue();
						if (TCG.isNotBlank(relatedCompanyId)) {
							params.relatedCompanyIdOrParent = relatedCompanyId;
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.business.company.CompanyRelationshipWindow',
						deleteURL: 'businessCompanyRelationshipDelete.json'
					}
				}]
			},


			{
				title: 'Client Types',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'The following client types are defined in the system.',
					getSystemList: function() {
						return TCG.data.getData('systemListByName.json?name=Business Client Types', this, 'system.list.Business Client Types');
					}
				}]
			},


			{
				title: 'Client Categories',
				items: [{
					name: 'businessClientCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'The following categories classify clients in the system. Each category is assigned a category type which groups Clients into three types Client (Real Clients), Virtual Clients (Client Groups or other Clients that are used internally only), and Commingled Vehicles (Funds).',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Name', width: 75, dataIndex: 'name'},
						{header: 'Category Type', width: 50, dataIndex: 'categoryType'},
						{header: 'Detail Screen', width: 100, dataIndex: 'detailScreenClass', hidden: true},
						{header: 'Company Type', width: 75, dataIndex: 'companyType.name', filter: {searchFieldName: 'companyTypeName'}},
						{header: 'Client Relationship', width: 50, dataIndex: 'clientRelationshipUsed', type: 'boolean'},
						{header: 'Client Relationship Required', width: 50, dataIndex: 'clientRelationshipRequired', type: 'boolean', tooltip: 'Required validation is used only by a notification, not during saves'},
						{header: 'Children Allowed', width: 40, dataIndex: 'childrenAllowed', type: 'boolean'},
						{header: 'Child Category', width: 50, dataIndex: 'childClientCategory.name', filter: false},
						{header: 'Our Accounts Only', width: 50, dataIndex: 'ourAccountsOnly', type: 'boolean'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.business.client.ClientCategoryWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Client Relationship Categories',
				items: [{
					name: 'businessClientRelationshipCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'The following categories classify client relationships in the system.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Name', width: 75, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Firm Category List', width: 75, dataIndex: 'relationshipTypeSystemList.name', filter: {searchFieldName: 'relationshipTypeSystemListName'}},
						{header: 'Firm Sub-Category List', width: 75, dataIndex: 'relationshipSubTypeSystemList.name', filter: {searchFieldName: 'relationshipSubTypeSystemListName'}},
						{header: 'Retail', width: 20, dataIndex: 'retail', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.business.client.relationship.ClientRelationshipCategoryWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
