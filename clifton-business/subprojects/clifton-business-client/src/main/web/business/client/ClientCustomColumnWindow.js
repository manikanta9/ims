Clifton.business.client.ClientCustomColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client',
	iconCls: 'client',
	width: 750,
	height: 600,

	title: 'Client Setup',
	items: [{
		xtype: 'formpanel-custom-fields',
		labelFieldName: 'name',
		url: 'businessClient.json',
		getSaveURL: function() {
			return 'systemColumnValueListSave.json';
		},
		labelWidth: 250,
		loadValidation: false,

		items: [
			{name: 'columnGroupName', xtype: 'hidden'},
			{fieldLabel: 'Client', name: 'name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.ClientWindow', detailIdField: 'id', submitValue: false, submitDetailField: false},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
