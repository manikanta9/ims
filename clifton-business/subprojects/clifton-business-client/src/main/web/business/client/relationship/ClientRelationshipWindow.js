TCG.use('Clifton.system.common.AddressFormFragment');

Clifton.business.client.relationship.ClientRelationshipWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Relationship (Organization)',
	iconCls: 'people',
	height: 700,
	width: 1200,

	windowOnShow: function(w) {
		this.addAdditionalTabs(w);
	},

	// Additional tabs added: Billing tab
	addAdditionalTabs: function(w) {
		const arr = Clifton.business.client.ClientRelationshipWindowAdditionalTabs;
		const tabs = w.items.get(0);
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',

				tbar: [{
					text: 'Client Services',
					iconCls: 'config',
					tooltip: 'Client Relationship Services',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						const id = w.getMainFormId();
						const customDetailClass = 'Clifton.business.client.relationship.ClientRelationshipCustomColumnWindow';
						const cmpId = TCG.getComponentId(customDetailClass, 'Client Relationship Services' + id);
						TCG.createComponent(customDetailClass, {
							id: cmpId,
							params: {id: id, columnGroupName: 'Client Relationship Services'}
						});
					}
				}],
				items: [{
					xtype: 'formpanel-custom-fields',
					columnGroupName: 'Client Relationship Custom Fields',
					dynamicFieldFormFragment: 'relationshipCustomFields',
					url: 'businessClientRelationship.json',
					instructions: 'A client relationship (organization) is an umbrella relationship that encompasses all subsidiary related client accounts.' +
						'A general rule of thumb is that if there is a single decision making authority for multiple related clients they are grouped as a client relationship.' +
						'<ul class="c-list">' +
						'<li>Are the clients (investment entities)/accounts related?</li>' +
						'<li>Is there one decision maker?</li></ul>' +
						'If the answer is yes to both of the above – they are considered one client relationship (organization).',
					items: [
						{fieldLabel: 'Short Name', name: 'name'},
						{fieldLabel: 'Legal Name', name: 'legalName', qtip: 'Legal entity name of this relationship (if different than name)'},

						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{xtype: 'hidden', name: 'relationshipCategory.relationshipTypeSystemList.name', submitValue: false},
									{xtype: 'hidden', name: 'relationshipCategory.relationshipSubTypeSystemList.name', submitValue: false},
									{
										fieldLabel: 'Category', name: 'relationshipCategory.name', hiddenName: 'relationshipCategory.id', xtype: 'combo', url: 'businessClientRelationshipCategoryListFind.json', detailPageClass: 'Clifton.business.client.relationship.ClientRelationshipCategoryWindow',
										requestedProps: 'relationshipTypeSystemList.name|relationshipSubTypeSystemList.name',
										listeners: {
											select: function(combo, record, index) {
												const fp = combo.getParentForm();
												fp.setFormValue('relationshipCategory.relationshipTypeSystemList.name', TCG.getValue('relationshipTypeSystemList.name', record.json));
												fp.setFormValue('relationshipCategory.relationshipSubTypeSystemList.name', TCG.getValue('relationshipSubTypeSystemList.name', record.json));
											}
										}

									},
									{
										fieldLabel: 'Firm Category', name: 'relationshipType.text', hiddenName: 'relationshipType.id', xtype: 'system-list-combo', valueField: 'id', requiredFields: ['relationshipCategory.name', 'relationshipCategory.relationshipTypeSystemList.name'],
										beforequery: function(queryEvent) {
											const fp = queryEvent.combo.getParentForm();
											queryEvent.combo.listName = fp.getFormValue('relationshipCategory.relationshipTypeSystemList.name', true);
											queryEvent.combo.store.setBaseParam('listName', queryEvent.combo.listName);
										}
									},
									{
										fieldLabel: 'Workflow State',
										xtype: 'workflow-state-combo', tableName: 'BusinessClientRelationship',
										listeners: {
											// Reset Effective Start Date on State Change
											select: function(combo, record, index) {
												const form = combo.getParentForm().getForm();
												const efDateField = form.findField('workflowStateEffectiveStartDate');
												efDateField.setValue('');
											}
										}
									},
									{fieldLabel: 'Inception Date', name: 'inceptionDate', xtype: 'datefield', qtip: 'Date the organization\'s first account put positions on.'}

								]
							}, {
								rows: [
									{xtype: 'displayfield', html: '&nbsp;'},
									{
										fieldLabel: 'Firm Sub-Category', name: 'relationshipSubType.text', hiddenName: 'relationshipSubType.id', xtype: 'system-list-combo', valueField: 'id', requiredFields: ['relationshipCategory.name', 'relationshipCategory.relationshipSubTypeSystemList.name'],
										beforequery: function(queryEvent) {
											const fp = queryEvent.combo.getParentForm();
											queryEvent.combo.listName = fp.getFormValue('relationshipCategory.relationshipSubTypeSystemList.name', true);
											queryEvent.combo.store.setBaseParam('listName', queryEvent.combo.listName);
										}
									},
									{fieldLabel: 'Workflow Effective Start', xtype: 'datefield', name: 'workflowStateEffectiveStartDate', qtip: 'Date indicating when the Workflow State was changed.'},
									{fieldLabel: 'Terminate Date', name: 'terminateDate', xtype: 'datefield'}
								],
								config: {labelWidth: 155}
							}]
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},

						{
							xtype: 'fieldset',
							title: 'Contact Info',
							collapsed: true,
							items: [
								{fieldLabel: 'Phone', name: 'company.phoneNumber'},
								{fieldLabel: 'Fax', name: 'company.faxNumber'},
								{fieldLabel: 'E-Fax', name: 'company.efaxNumber'},
								{fieldLabel: 'Email', name: 'company.emailAddress', vtype: 'email'}
							]
						},

						{
							xtype: 'address-formfragment',
							frame: false,
							useFieldSet: true,
							fieldSetTitle: 'Main Address',
							fieldSetCollapsible: true,
							fieldSetCollapsed: true,
							addressFieldsPrefix: 'company.'
						},
						{
							xtype: 'fieldset',
							title: 'Marketing & Sales',
							collapsed: true,
							items: [{
								xtype: 'formfragment',
								frame: false,
								name: 'relationshipCustomFields',
								labelWidth: 145,
								bodyStyle: 'padding: 0',
								defaults: {anchor: '0'},
								items: []
							}]
						}
					]
				}]
			},


			{
				title: 'Clients',
				items: [{
					name: 'businessClientListFind',
					xtype: 'gridpanel',
					instructions: 'The following clients are managed by this client relationship.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Name', width: 50, dataIndex: 'label'},
						{header: 'Client Type', width: 40, dataIndex: 'clientType.text', filter: {searchFieldName: 'clientTypeName'}},
						{header: 'Inception Date', width: 20, dataIndex: 'inceptionDate'},
						{header: 'Terminate Date', width: 20, dataIndex: 'terminateDate'},
						{
							header: 'Workflow State', width: 25, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName', comparison: 'BEGINS_WITH'},
							tooltip: 'Account workflow state is populated automatically based on the Client\'s Clifton Account Workflow States.'
						},
						{header: 'Workflow Status', width: 25, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName', comparison: 'BEGINS_WITH'}, hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.business.client.ClientWindow',
						drillDownOnly: true
					},
					getLoadParams: function() {
						return {clientRelationshipId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Investment Accounts',
				items: [{
					xtype: 'investment-accountsGrid_byClientRelationship'
				}]
			},


			{
				title: 'Companies',
				items: [{
					xtype: 'business-companyRelationshipGrid',
					name: 'businessCompanyRelationshipListFind',
					instructions: 'The following companies are associated with this client relationship.',
					getCompany: function() {
						return TCG.getValue('company', this.getWindow().getMainForm().formValues);
					},

					appendAdditionalIncludeOptions: function(menu) {
						const gridPanel = this;
						menu.add({
							text: 'Include Client Company Relationships',
							xtype: 'menucheckitem',
							name: 'includeClient',
							checked: true,
							checkHandler: function() {
								gridPanel.reload();
							}
						});
					},

					appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
						const inclClient = TCG.getChildByName(includeOptionsMenu, 'includeClient').checked;
						if (inclClient) {
							params.includeClientOrClientRelationship = true;
						}
					}

				}]
			},


			{
				title: 'Contacts',
				items: [{
					xtype: 'business-companyContactEntryGrid-byCompany',
					instructions: 'The following contacts are associated with this client relationship\'s company.',
					getCompany: function() {
						return TCG.getValue('company', this.getWindow().getMainForm().formValues);
					},

					appendAdditionalIncludeOptions: function(menu) {
						const gridPanel = this;
						menu.add({
							text: 'Include Client Contacts',
							xtype: 'menucheckitem',
							name: 'includeClient',
							checked: true,
							checkHandler: function() {
								gridPanel.reload();
							}
						});
					},

					appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
						const inclClient = TCG.getChildByName(includeOptionsMenu, 'includeClient').checked;
						if (inclClient) {
							params.includeClientOrClientRelationship = true;
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('referenceOne.type.name'), false);
							cm.setHidden(cm.findColumnIndex('referenceOne.name'), false);
						}
					}
				}]
			},


			{
				title: 'Portal Files',
				items: [{
					xtype: 'business-posted-portal-file-grid',
					sourceTableName: 'BusinessClientRelationship'
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'BusinessClientRelationship',
					showOrderInfo: true,
					showCreateDate: true
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'BusinessClientRelationship',
					addTaskCategoryFilter: true
				}]
			},


			{
				title: 'Workflow History',
				items: [{
					xtype: 'workflow-history-effective-grid',
					tableName: 'BusinessClientRelationship'
				}]
			}
		]
	}]
});
