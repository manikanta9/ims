Clifton.business.client.ClientCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Category',
	width: 800,
	height: 500,

	items: [{
		xtype: 'formpanel',
		url: 'businessClientCategory.json',

		listeners: {
			afterload: function(panel) {
				this.setReadOnly(true);
				// Take read only off of description
				this.setReadOnlyField('description', false);
			}
		},

		items: [
			{fieldLabel: 'Category Name', name: 'name', xtype: 'displayfield'},
			{fieldLabel: 'Category Type', name: 'categoryType', xtype: 'displayfield'},
			{fieldLabel: 'Detail Screen', name: 'detailScreenClass', xtype: 'displayfield'},
			{fieldLabel: 'Company Type', name: 'companyType.name', xtype: 'linkfield', detailIdField: 'companyType.id', detailPageClass: 'Clifton.business.company.CompanyTypeWindow'},
			{boxLabel: 'Uses Client Relationship', name: 'clientRelationshipUsed', xtype: 'checkbox'},
			{boxLabel: 'Requires Client Relationship', name: 'clientRelationshipRequired', xtype: 'checkbox', requiredFields: ['clientRelationshipUsed'], qtip: 'Required validation is not performed on screen or during save, but by a notification'},
			{boxLabel: 'Children Clients Allowed', name: 'childrenAllowed', xtype: 'checkbox'},
			{fieldLabel: 'Child Category', name: 'childClientCategory.name', xtype: 'linkfield', detailIdField: 'childClientCategory.id', detailPageClass: 'Clifton.business.client.ClientCategoryWindow'},
			{boxLabel: 'Our Accounts Only', name: 'ourAccountsOnly', xtype: 'checkbox'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 150}
		]
	}]
});
