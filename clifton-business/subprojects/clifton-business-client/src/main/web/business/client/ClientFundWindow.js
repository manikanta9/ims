TCG.use('Clifton.business.client.ClientWindowBase');
Clifton.business.client.ClientFundWindow = Ext.extend(Clifton.business.client.ClientWindowBase, {

	// Adds Share Classes Tab from Accounting
	addAdditionalTabs: function(w) {
		Clifton.business.client.ClientFundWindow.superclass.addAdditionalTabs(w);
		const arr = Clifton.business.client.ClientFundWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			arr[i].addAdditionalTabs(w);
		}
	}
});
