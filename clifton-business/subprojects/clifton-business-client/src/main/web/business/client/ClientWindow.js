// not the actual window but a window selector based on client type name (if system defined)
Clifton.business.client.ClientWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'businessClient.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.business.client.BasicClientWindow';
		if (entity && entity.category) {
			className = entity.category.detailScreenClass;
		}
		else if (config.defaultData && config.defaultData.category) {

			if (config.defaultData.category.detailScreenClass) {
				className = config.defaultData.category.detailScreenClass;
			}
			else if (config.defaultData.category.name) {
				const cat = TCG.data.getData('businessClientCategoryByName.json?name=' + config.defaultData.category.name, this);
				if (cat) {
					config.defaultData.category = cat;
					className = cat.detailScreenClass;
				}
			}
		}
		if (className === 'Clifton.business.client.BasicClientWindow' && entity && entity.parent) {
			className = 'Clifton.business.client.ChildClientWindow';
		}
		return className;
	}
});
