Clifton.business.client.relationship.ClientRelationshipCustomColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Relationship',
	iconCls: 'people',
	width: 750,
	height: 600,

	title: 'Client Relationship Setup',
	items: [{
		xtype: 'formpanel-custom-fields',
		labelFieldName: 'name',
		url: 'businessClientRelationship.json',
		getSaveURL: function() {
			return 'systemColumnValueListSave.json';
		},
		labelWidth: 250,
		loadValidation: false,

		items: [
			{name: 'columnGroupName', xtype: 'hidden'},
			{fieldLabel: 'Client Relationship', name: 'name', xtype: 'linkfield', detailPageClass: 'Clifton.business.client.relationship.ClientRelationshipWindow', detailIdField: 'id', submitValue: false, submitDetailField: false},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
