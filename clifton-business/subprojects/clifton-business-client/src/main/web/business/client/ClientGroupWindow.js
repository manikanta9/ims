TCG.use('Clifton.business.client.ClientWindowBase');
Clifton.business.client.ClientGroupWindow = Ext.extend(Clifton.business.client.ClientWindowBase, {
	parentChildClient: 'PARENT',

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		tabs.add(this.clientsTab);
		Clifton.business.client.ClientGroupWindow.superclass.windowOnShow(w);
	},

	// Note: Only used for Client Group category only - if ever open up to other categories need
	// to test linking since currently it gives dropdowns based on client relationships
	clientsTab: {
		title: 'Clients',
		items: [{
			xtype: 'business-client-grid',
			deleteUrl: 'businessClientLinkToParentDelete.json',
			instructions: 'This client is a group of the following clients.  To link a client for the same client relationship as a child of this client, select the client from the list and click add.  To remove the parent/child link, select the child and click the Remove button.',
			editor: {
				detailPageClass: 'Clifton.business.client.ClientWindow',
				deleteURL: 'businessClientLinkToParentDelete.json',
				getDeleteParams: function(selectionModel) {
					return {childClientId: selectionModel.getSelected().id};
				},
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();

					toolBar.add(new TCG.form.ComboBox({
						name: 'child', url: 'businessClientListFind.json', width: 200, listWidth: 350,
						beforequery: function(queryEvent) {
							const fv = gridPanel.getWindow().getMainForm().formValues;
							const relId = TCG.getValue('clientRelationship.id', fv);
							const categoryId = TCG.getValue('category.childClientCategory.id', fv);

							queryEvent.combo.store.baseParams = {
								clientRelationshipId: relId,
								nullParent: true,
								categoryId: categoryId
							};
						}
					}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Add selected client as a child of this client group.',
						iconCls: 'add',
						handler: function() {
							const childId = TCG.getChildByName(toolBar, 'child').getValue();
							const parentId = gridPanel.getWindow().getMainFormId();

							if (TCG.isBlank(childId)) {
								TCG.showError('You must first select desired client to link to this client.');
							}
							else {
								const params = {'parentClientId': parentId, 'childClientId': childId};
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									waitMsg: 'Linking...',
									params: params,
									onLoad: function(record, conf) {
										gridPanel.reload();
										TCG.getChildByName(toolBar, 'child').reset();
									}
								});
								loader.load('businessClientToParentLink.json');
							}
						}
					});

					toolBar.add('-');
				}
			},
			getLoadParams: function(firstLoad) {
				return {parentId: this.getWindow().getMainFormId()};
			}
		}]
	}

});
