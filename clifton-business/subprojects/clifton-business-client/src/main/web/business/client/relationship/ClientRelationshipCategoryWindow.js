Clifton.business.client.relationship.ClientRelationshipCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Relationship Category',
	width: 800,
	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Details',
				items: [{
					xtype: 'formpanel',
					url: 'businessClientRelationshipCategory.json',
					labelWidth: 140,

					listeners: {
						afterload: function(panel) {
							// Only Allowing edits to description for now
							this.setReadOnly(true);
							this.setReadOnlyField('description', false);
						}
					},

					items: [
						{fieldLabel: 'Category Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 150},

						{fieldLabel: 'Firm Category List', name: 'relationshipTypeSystemList.name', hiddenName: 'relationshipTypeSystemList.id', xtype: 'combo', url: 'systemListListFind.json'},
						{fieldLabel: 'Firm Sub-Category List', name: 'relationshipSubTypeSystemList.name', hiddenName: 'relationshipSubTypeSystemList.id', xtype: 'combo', url: 'systemListListFind.json'},

						{boxLabel: 'Retail', name: 'retail', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Firm Categories',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'The following client relationship types/firm categories are defined in the system.',
					getSystemList: function() {
						return TCG.getValue('relationshipTypeSystemList', this.getWindow().getMainForm().formValues);
					},
					getLoadParams: function() {
						if (TCG.isBlank(this.getSystemList())) {
							TCG.showError('Firm Category List is not selected on the relationship category.  Nothing to display.', 'Not Applicable');
							return false;
						}
						return {'systemListId': TCG.getValue('id', this.getSystemList())};
					}
				}]
			},


			{
				title: 'Firm Sub-Categories',
				items: [{
					xtype: 'system-list-item-grid',
					instructions: 'The following client relationship sub types/firm sub-categories are defined in the system.',
					getSystemList: function() {
						return TCG.getValue('relationshipSubTypeSystemList', this.getWindow().getMainForm().formValues);
					},
					getLoadParams: function() {
						if (TCG.isBlank(this.getSystemList())) {
							TCG.showError('Firm Sub-Category List is not selected on the relationship category.  Nothing to display.', 'Not Applicable');
							return false;
						}
						return {'systemListId': TCG.getValue('id', this.getSystemList())};
					}
				}]
			}
		]
	}]
});
