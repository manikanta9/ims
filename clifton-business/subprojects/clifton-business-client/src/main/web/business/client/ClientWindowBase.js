TCG.use('Clifton.system.common.AddressFormFragment');

Clifton.business.client.ClientWindowBase = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client',
	width: 1350,
	height: 620,
	iconCls: 'client',

	// Overridden to true in Client Group Window  to [PARENT] show its children and drive tab options (ability to see child info)
	// Overridden to true in Child Client Window to [CHILD] drive tab options (ability to see parent info)
	parentChildClient: 'NONE',
	defaultDataIsReal: true,

	isIncludeRelatedCheckbox: function() {
		return this.parentChildClient !== 'NONE';
	},
	getRelatedCheckboxLabel: function(tabName) {
		if (this.parentChildClient === 'PARENT') {
			return 'Include Child Client\'s ' + tabName;
		}
		else if (this.parentChildClient === 'CHILD') {
			return 'Include Parent Client\'s ' + tabName;
		}
		return '';
	},

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		tabs.add(this.companiesTab);
		tabs.add(this.contactsTab);
		tabs.add(this.notesTab);
		tabs.add(this.tasksTab);
		tabs.add(this.notificationsTab);
		tabs.add(this.portalFileListTab);
		this.addAdditionalTabs(w);
	},

	// Additional tabs added in investment-shared to add ACCOUNTS/MANAGERS tabs
	// and Billing to add Billing tab
	addAdditionalTabs: function(w) {
		const arr = Clifton.business.client.ClientWindowBaseAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			arr[i].addAdditionalTabs(w);
		}
	},

	getTabPosition: function(w, tabTitle) {
		const tabs = w.items.get(0);
		for (let i = 0; i < tabs.items.length; i++) {
			if (tabs.items.get(i).title === tabTitle) {
				return i;
			}
		}
		return 0;
	},

	companiesTab: {
		title: 'Companies',
		items: [{
			xtype: 'business-companyRelationshipGrid',
			instructions: 'The following companies are associated with this client.',
			getCompany: function() {
				return TCG.getValue('company', this.getWindow().getMainForm().formValues);
			},
			isIncludeRelatedCompanyCheckbox: function() {
				return this.getWindow().isIncludeRelatedCheckbox();
			},
			getRelatedCompanyCheckboxLabel: function() {
				return this.getWindow().getRelatedCheckboxLabel('Company Relationships');
			},
			appendAdditionalIncludeOptions: function(menu) {
				const ct = TCG.getValue('category', this.getWindow().getMainForm().formValues);
				if (TCG.isTrue(ct.clientRelationshipUsed)) {
					const gridPanel = this;
					menu.add({
						text: 'Include Client Relationship Company Relationships',
						xtype: 'menucheckitem',
						name: 'includeClientRelationship',
						checked: true,
						checkHandler: function() {
							gridPanel.reload();
						}
					});
				}
			},

			appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
				const ct = TCG.getValue('category', this.getWindow().getMainForm().formValues);
				if (TCG.isTrue(ct.clientRelationshipUsed)) {
					const inclClientRel = TCG.getChildByName(includeOptionsMenu, 'includeClientRelationship').checked;
					if (inclClientRel) {
						params.includeClientOrClientRelationship = true;
					}
				}
			}
		}]
	},


	contactsTab: {
		title: 'Contacts',
		items: [{
			xtype: 'business-companyContactEntryGrid-byCompany',
			getCompany: function() {
				return TCG.getValue('company', this.getWindow().getMainForm().formValues);
			},
			isIncludeRelatedCompanyCheckbox: function() {
				return this.getWindow().isIncludeRelatedCheckbox();
			},
			getRelatedCompanyCheckboxLabel: function() {
				return this.getWindow().getRelatedCheckboxLabel('Contacts');
			},
			appendAdditionalIncludeOptions: function(menu) {
				const ct = TCG.getValue('category', this.getWindow().getMainForm().formValues);
				if (TCG.isTrue(ct.clientRelationshipUsed)) {
					const gridPanel = this;
					menu.add({
						text: 'Include Client Relationship Contacts',
						xtype: 'menucheckitem',
						name: 'includeClientRelationship',
						checked: true,
						checkHandler: function() {
							gridPanel.reload();
						}
					});
				}
			},

			appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
				const ct = TCG.getValue('category', this.getWindow().getMainForm().formValues);
				if (TCG.isTrue(ct.clientRelationshipUsed)) {
					const inclClientRel = TCG.getChildByName(includeOptionsMenu, 'includeClientRelationship').checked;
					if (inclClientRel) {
						params.includeClientOrClientRelationship = true;
					}
					const cm = this.getColumnModel();
					cm.setHidden(cm.findColumnIndex('referenceOne.type.name'), false);
				}
			}
		}]
	},


	notesTab: {
		title: 'Notes',
		items: [{
			xtype: 'document-system-note-grid',
			tableName: 'BusinessClient',
			showOrderInfo: true,
			showCreateDate: true,
			showAttachmentInfo: true
		}]
	},


	tasksTab: {
		title: 'Tasks',
		items: [{
			xtype: 'workflow-task-grid',
			tableName: 'BusinessClient',
			addTaskCategoryFilter: true
		}]
	},


	notificationsTab: {
		title: 'Notifications',
		items: [{
			xtype: 'notification-linked-grid',
			tableName: 'BusinessClient'
		}]
	},


	portalFileListTab: {
		title: 'Portal Files',
		items: [{
			xtype: 'business-posted-portal-file-grid',
			sourceTableName: 'BusinessClient'
		}]
	},


	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Details',

			tbar: [
				{
					text: 'CFS Report',
					iconCls: 'pdf',
					tooltip: 'Download PDF of Client Fact Sheet report for today',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						const id = w.getMainFormId();
						let reportId = Clifton.system.GetCustomColumnValue(id, 'BusinessClient', 'CFS Options', 'CFS Report', true);
						if (TCG.isBlank(reportId)) {
							const report = TCG.data.getData('reportByNameAndTemplate.json?reportName=Client Fact Sheet - No OTC&templateName=Client Fact Sheet', this);
							if (report) {
								reportId = report.id;
							}
							else {
								TCG.showError('Cannot find "Client Fact Sheet - No OTC" report for "Client Fact Sheet" template.', 'Report Not Found');
								return;
							}

						}
						let url = TCG.getResponseText('reportUrl.json?id=' + reportId, this);
						if (url !== undefined) {
							const date = new Date();
							url += '&exportFormat=PDF&ClientID=' + id + '&ReportDate=' + TCG.renderDate(date);
							TCG.downloadFile(url, null, this);
						}
					}
				}, '-',

				{
					text: 'CFS Options',
					iconCls: 'config',
					tooltip: 'Client Fact Sheet Options',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						const id = w.getMainFormId();
						const customDetailClass = 'Clifton.business.client.ClientCustomColumnWindow';
						const cmpId = TCG.getComponentId(customDetailClass, 'CFS Options' + id);
						TCG.createComponent(customDetailClass, {
							id: cmpId,
							params: {id: id, columnGroupName: 'CFS Options'}
						});
					}
				}, '-',

				{
					text: 'Client Contacts',
					iconCls: 'pdf',
					tooltip: 'Download PDF of Client Contacts report for today',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						const id = w.getMainFormId();
						const report = TCG.data.getData('reportByNameAndTemplate.json?reportName=Client Contacts&templateName=Client Fact Sheet', this);
						if (report) {
							let url = TCG.getResponseText('reportUrl.json?id=' + report.id, this);
							if (url !== undefined) {
								const date = new Date();
								url += '&exportFormat=PDF&ClientID=' + id + '&ReportDate=' + TCG.renderDate(date);
								TCG.downloadFile(url, null, this);
							}
						}
						else {
							TCG.showError('Cannot find "Client Contacts" report for "Client Fact Sheet" template.', 'Report Not Found');
						}
					}

				}, '-',
				{
					text: 'Client Clifton Contacts',
					iconCls: 'pdf',
					tooltip: 'Download PDF of Client Clifton Contacts report for today',
					handler: function() {
						const f = this.ownerCt.ownerCt.items.get(0);
						const w = f.getWindow();
						const id = w.getMainFormId();
						const report = TCG.data.getData('reportByNameAndTemplate.json?reportName=Client Clifton Contacts&templateName=Client Fact Sheet', this);
						if (report) {
							let url = TCG.getResponseText('reportUrl.json?id=' + report.id, this);
							if (url !== undefined) {
								const date = new Date();
								url += '&exportFormat=PDF&ClientID=' + id + '&ReportDate=' + TCG.renderDate(date);
								TCG.downloadFile(url, null, this);
							}
						}
						else {
							TCG.showError('Cannot find "Client Clifton Contacts" report for "Client Fact Sheet" template.', 'Report Not Found');
						}
					}
				}
			],

			items: [{
				xtype: 'formpanel',
				url: 'businessClient.json',
				labelWidth: 160,

				listeners: {
					afterload: function(panel) {
						// Use category to drive unused columns
						const f = panel.getForm();
						const ct = TCG.getValue('category', f.formValues);
						if (TCG.isFalse(ct.clientTypeUsed)) {
							const clientTypeField = f.findField('clientType.text');
							clientTypeField.setVisible(false);
							clientTypeField.allowBlank = true;
						}
						if (TCG.isFalse(ct.clientRelationshipUsed)) {
							const clientRelationshipField = f.findField('clientRelationship.name');
							const clientRelationshipWSFields = TCG.getChildByName(panel, 'mrLabel');
							clientRelationshipField.setVisible(false);
							clientRelationshipWSFields.setVisible(false);
						}
						const pc = TCG.getValue('parent.name', f.formValues);
						if (!pc) {
							const parentField = f.findField('parent.name');
							parentField.setVisible(false);
						}
					}
				},

				instructionsLoaded: false,
				updateTitle: function() {
					const f = this.getForm();
					const ct = TCG.getValue('category', f.formValues);
					const w = this.getWindow();
					if (this.instructionsLoaded === false) {
						this.insert(0, {xtype: 'label', html: ct.description + '<br /><br />'});
						this.instructionsLoaded = true;
					}
					w.titlePrefix = ct.name;

					const l = this.getFormLabel();
					if (TCG.isNotNull(l)) {
						if (w && w.setTitle) {
							w.setTitle(l);
						}
					}
				},

				items: [
					{fieldLabel: 'Category', name: 'category.id', xtype: 'hidden'},
					{fieldLabel: 'Client Relationship', name: 'clientRelationship.name', hiddenName: 'clientRelationship.id', xtype: 'combo', detailPageClass: 'Clifton.business.client.relationship.ClientRelationshipWindow', url: 'businessClientRelationshipListFind.json'},
					{fieldLabel: 'Client Group', name: 'parent.name', xtype: 'linkfield', detailIdField: 'parent.id', detailPageClass: 'Clifton.business.client.ClientWindow'},
					{fieldLabel: 'Short Name', name: 'name'},
					{fieldLabel: 'Legal Name', name: 'legalName', qtip: 'Legal entity name for this client (if different than name)'},
					{fieldLabel: 'Abbreviation', name: 'company.abbreviation', qtip: 'Abbreviations are used to standardize account names, security names (Swaps), etc.'},
					{fieldLabel: 'Client Type', name: 'clientType.text', hiddenName: 'clientType.id', xtype: 'system-list-combo', allowBlank: false, listName: 'Business Client Types', valueField: 'id'},
					{
						fieldLabel: 'Inception Date', xtype: 'panel', layout: 'hbox',
						defaults: {
							flex: 1
						},
						items: [
							{name: 'inceptionDate', xtype: 'datefield'},
							{xtype: 'label'},
							{html: 'Terminate Date:', xtype: 'label', flex: .35},
							{name: 'terminateDate', xtype: 'datefield'}
						]
					},
					{
						fieldLabel: 'Relationship Workflow State', xtype: 'panel', layout: 'hbox', name: 'mrLabel',
						defaults: {
							flex: 1
						},
						items: [
							{
								name: 'clientRelationship.workflowState.name', xtype: 'textfield', readOnly: true, submitValue: false,
								qtip: 'Client Relationship workflow state/status.'
							},
							{xtype: 'label'},
							{html: 'Status:', xtype: 'label', flex: .35},
							{name: 'clientRelationship.workflowStatus.name', xtype: 'textfield', readOnly: true, submitValue: false}
						]
					},
					{
						fieldLabel: 'Account Workflow State', xtype: 'panel', layout: 'hbox',
						defaults: {
							flex: 1
						},
						items: [
							{
								name: 'workflowState.name', xtype: 'textfield', readOnly: true, submitValue: false,
								qtip: 'Account workflow state is populated automatically based on its Client Accounts Workflow States.'
							},
							{xtype: 'label'},
							{html: 'Status:', xtype: 'label', flex: .35},
							{name: 'workflowStatus.name', xtype: 'textfield', readOnly: true, submitValue: false}
						]
					},
					{
						fieldLabel: 'Origin', xtype: 'panel', layout: 'hbox',
						defaults: {
							flex: 1
						},
						items: [
							{name: 'origin', xtype: 'system-list-combo', listName: 'Client Origin Options'},
							{xtype: 'label'},
							{html: 'Domicile:', xtype: 'label', flex: .35},
							{
								name: 'originDomicile', xtype: 'combo', displayField: 'text', tooltipField: 'tooltip', valueField: 'text', url: 'systemListItemListFind.json',
								requiredFields: ['origin'],
								beforequery: function(queryEvent) {
									const cmb = queryEvent.combo;
									const f = TCG.getParentFormPanel(cmb).getForm();
									const origin = f.findField('origin').getValue();
									if (origin === 'United States') {
										cmb.store.baseParams = {
											listName: 'States'
										};
									}
									else if (origin === 'International Entity') {
										cmb.store.baseParams = {
											listName: 'Countries'
										};
									}
									else if (origin === 'Canada') {
										cmb.store.baseParams = {
											listName: 'Canadian Provinces'
										};
									}
									else {
										TCG.showError('Domicile selection only applies where Origin = United States, Canada, or International Equity');
										return false;
									}
								}
							}
						]
					},
					{fieldLabel: 'Description', xtype: 'textarea', name: 'description', height: 140},
					{
						xtype: 'fieldset',
						title: 'Tax Info',
						collapsed: true,
						items: [
							{boxLabel: 'This Client is taxable', xtype: 'checkbox', name: 'taxable'},
							{fieldLabel: 'Federal Tax #', name: 'federalTaxNumber'},
							{fieldLabel: 'State Tax #', name: 'stateTaxNumber'}
						]
					},
					{
						xtype: 'fieldset',
						title: 'Contact Info',
						collapsed: true,
						items: [
							{fieldLabel: 'Phone', name: 'company.phoneNumber'},
							{fieldLabel: 'Fax', name: 'company.faxNumber'},
							{fieldLabel: 'E-Fax', name: 'company.efaxNumber'},
							{fieldLabel: 'Email', name: 'company.emailAddress', vtype: 'email'}
						]
					},
					{
						xtype: 'address-formfragment',
						defaults: {anchor: '0'},
						frame: false,
						useFieldSet: true,
						fieldSetTitle: 'Main Address',
						fieldSetCollapsible: true,
						fieldSetCollapsed: true,
						addressFieldsPrefix: 'company.'
					}
				]
			}]
		}]
	}]
});
