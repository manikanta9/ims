package com.clifton.business.client;


import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;


/**
 * The <code>Client</code>is the unique business entity for which we have entered a contract to do business with.
 * <p/>
 * Hierarchical Object - Max Levels Deep = 1
 * Parent clients are those of type "Client Group" and the same clientRelationship
 * to allow groups related clients that are really separate entities together for setup
 * and or processing (PIOS)
 *
 * @author akorver
 */
public class BusinessClient extends NamedHierarchicalEntity<BusinessClient, Integer> implements SystemColumnCustomValueAware {

	public static final String BUSINESS_CLIENT_TABLE_NAME = "BusinessClient";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String legalName;

	private BusinessClientCategory category;

	/**
	 * Uses SystemList "Business Client Types"
	 */
	private SystemListItem clientType;

	private BusinessClientRelationship clientRelationship;
	private BusinessCompany company;

	private Date inceptionDate;
	private Date terminateDate;

	private boolean taxable;
	private String federalTaxNumber;
	private String stateTaxNumber;

	/**
	 * Origin - Options: Federal Government, State Government, City Municipalities, United States, International Entity, Mutual Fund
	 * Domicile - If Origin - US then a State, if Original = International then a country
	 */
	private String origin;
	private String originDomicile;

	/**
	 * System Generated Values based on the Client Account's tied to the Client.
	 */
	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	/**
	 * Custom Field Support
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	//////////////////////////////////////////////////////
	//////////////////////////////////////////////////////


	@Override
	public Integer getMaxDepth() {
		return 1;
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(this.inceptionDate, this.terminateDate, false);
	}


	public boolean isLegalNameDefined() {
		return !StringUtils.isEmpty(getLegalName());
	}


	public String getLegalLabel() {
		if (isLegalNameDefined()) {
			return getLegalName();
		}
		return getName();
	}


	public BusinessClientRelationship getClientRelationship() {
		return this.clientRelationship;
	}


	public void setClientRelationship(BusinessClientRelationship clientRelationship) {
		this.clientRelationship = clientRelationship;
	}


	public Date getInceptionDate() {
		return this.inceptionDate;
	}


	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public boolean isTaxable() {
		return this.taxable;
	}


	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}


	public String getFederalTaxNumber() {
		return this.federalTaxNumber;
	}


	public void setFederalTaxNumber(String federalTaxNumber) {
		this.federalTaxNumber = federalTaxNumber;
	}


	public String getStateTaxNumber() {
		return this.stateTaxNumber;
	}


	public void setStateTaxNumber(String stateTaxNumber) {
		this.stateTaxNumber = stateTaxNumber;
	}


	public Date getTerminateDate() {
		return this.terminateDate;
	}


	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}


	public String getOrigin() {
		return this.origin;
	}


	public void setOrigin(String origin) {
		this.origin = origin;
	}


	public String getOriginDomicile() {
		return this.originDomicile;
	}


	public void setOriginDomicile(String originDomicile) {
		this.originDomicile = originDomicile;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public String getLegalName() {
		return this.legalName;
	}


	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}


	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public BusinessClientCategory getCategory() {
		return this.category;
	}


	public void setCategory(BusinessClientCategory category) {
		this.category = category;
	}


	public SystemListItem getClientType() {
		return this.clientType;
	}


	public void setClientType(SystemListItem clientType) {
		this.clientType = clientType;
	}
}
