package com.clifton.business.client.setup;


import com.clifton.business.company.BusinessCompanyType;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>BusinessEntityType</code> ...
 *
 * @author manderson
 */
@CacheByName
public class BusinessClientCategory extends NamedEntity<Short> {

	private BusinessCompanyType companyType;

	private BusinessClientCategoryTypes categoryType;

	/**
	 * Detail page for this category
	 */
	private String detailScreenClass;

	/**
	 * If Client Relationship is used - Currently used for Clients/Client Groups only
	 * Funds and other Virtual Clients don't use a Client Relationship
	 */
	private boolean clientRelationshipUsed;

	/**
	 * Depends on clientRelationshipUsed.  It should be required (not validated on screen or save of the client, but used by a notification)
	 * except for Client Groups which won't require it so we can support cross client relationship client groups (Investure)
	 */
	private boolean clientRelationshipRequired;

	/**
	 * True for Client Groups - allows adding Clients as children of Client Groups
	 */
	private boolean childrenAllowed;


	/**
	 * If children are allowed, this limits to what clients can be children.
	 * For example, Client Groups are groups of Clients, Sister Client groups are groups of Sister Clients
	 * and Commingled Vehicle Groups are groups of Commingled Vehicles.
	 */
	private BusinessClientCategory childClientCategory;

	/**
	 * True for Client Groups - allows only "Clifton" issued (account.accountType.isOurAccount = true)
	 * accounts associated with it.  For Client Groups, this allows us to group Clients together, but
	 * by not allowing holding accounts we are preventing the ability to trade directly under this client
	 */
	private boolean ourAccountsOnly;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isClientTypeUsed() {
		return BusinessClientCategoryTypes.CLIENT == getCategoryType();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyType getCompanyType() {
		return this.companyType;
	}


	public void setCompanyType(BusinessCompanyType companyType) {
		this.companyType = companyType;
	}


	public BusinessClientCategoryTypes getCategoryType() {
		return this.categoryType;
	}


	public void setCategoryType(BusinessClientCategoryTypes categoryType) {
		this.categoryType = categoryType;
	}


	public boolean isClientRelationshipUsed() {
		return this.clientRelationshipUsed;
	}


	public void setClientRelationshipUsed(boolean clientRelationshipUsed) {
		this.clientRelationshipUsed = clientRelationshipUsed;
	}


	public boolean isClientRelationshipRequired() {
		return this.clientRelationshipRequired;
	}


	public void setClientRelationshipRequired(boolean clientRelationshipRequired) {
		this.clientRelationshipRequired = clientRelationshipRequired;
	}


	public boolean isChildrenAllowed() {
		return this.childrenAllowed;
	}


	public void setChildrenAllowed(boolean childrenAllowed) {
		this.childrenAllowed = childrenAllowed;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public boolean isOurAccountsOnly() {
		return this.ourAccountsOnly;
	}


	public void setOurAccountsOnly(boolean ourAccountsOnly) {
		this.ourAccountsOnly = ourAccountsOnly;
	}


	public BusinessClientCategory getChildClientCategory() {
		return this.childClientCategory;
	}


	public void setChildClientCategory(BusinessClientCategory childClientCategory) {
		this.childClientCategory = childClientCategory;
	}
}
