package com.clifton.business.client.observers;


import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessClientRelationshipObserver</code> will create/update
 * related {@link BusinessCompany} object prior to saving the client relationship
 *
 * @author manderson
 */
@Component
public class BusinessClientRelationshipObserver extends SelfRegisteringDaoObserver<BusinessClientRelationship> {

	private BusinessCompanyService businessCompanyService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<BusinessClientRelationship> dao, DaoEventTypes event, BusinessClientRelationship bean) {
		if (event.isInsert() || event.isUpdate()) {
			BusinessClientRelationship original = null;
			if (event.isUpdate()) {
				original = getOriginalBean(dao, bean);
			}
			saveRelatedCompany(bean, original);
		}
	}


	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<BusinessClientRelationship> dao, DaoEventTypes event, BusinessClientRelationship bean, Throwable e) {
		if (e == null && event.isDelete()) {
			getBusinessCompanyService().deleteBusinessCompany(bean.getCompany().getId());
		}
	}


	/**
	 * Creates/Updates Company tied to this Client Relationship - Company Type is "Client Relationship"
	 *
	 * @param bean
	 * @param original
	 */
	private void saveRelatedCompany(BusinessClientRelationship bean, BusinessClientRelationship original) {
		BusinessCompany company = (original == null ? new BusinessCompany() : original.getCompany());
		if (bean.getCompany() != null) {
			BeanUtils.copyProperties(bean.getCompany(), company);
		}
		if (company.getType() == null) {
			company.setType(getBusinessCompanyService().getBusinessCompanyTypeByName(BusinessClientRelationship.COMPANY_TYPE_NAME));
		}
		// Use legal name if populated, else name field
		company.setName(bean.getLegalLabel());

		// If Legal Name is different than short name - set short name as alias
		if (!CompareUtils.isEqual(bean.getLegalLabel(), bean.getName())) {
			company.setAlias(bean.getName());
		}
		// Otherwise clear it - no reason to set alias if it's the same as the name
		else {
			company.setAlias(null);
		}

		getBusinessCompanyService().saveBusinessCompany(company);
		bean.setCompany(company);
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}
}
