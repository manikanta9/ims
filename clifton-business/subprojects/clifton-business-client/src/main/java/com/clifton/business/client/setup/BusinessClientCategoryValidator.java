package com.clifton.business.client.setup;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BusinessClientCategoryValidator</code> validates UPDATES only and description is the only field
 * that can be edited via UI.
 *
 * @author manderson
 */
@Component
public class BusinessClientCategoryValidator extends SelfRegisteringDaoValidator<BusinessClientCategory> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BusinessClientCategory bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert()) {
			throw new ValidationException("Creating new categories is not allowed.");
		}
		if (config.isDelete()) {
			throw new ValidationException("Deleting categories is not allowed.");
		}
		// The only field that can be modified is description - excluding that - ensure nothing else changed
		List<String> modified = CoreCompareUtils.getNoEqualProperties(bean, getOriginalBean(bean), false, "description");
		if (!CollectionUtils.isEmpty(modified)) {
			throw new ValidationException("The only field that can be updated for categories is the description field.  You have also updated the following fields ["
					+ StringUtils.collectionToCommaDelimitedString(modified) + "] which is not allowed.");
		}
	}
}
