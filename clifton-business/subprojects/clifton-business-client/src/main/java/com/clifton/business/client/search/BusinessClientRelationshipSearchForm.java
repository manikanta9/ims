package com.clifton.business.client.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.util.Date;


public class BusinessClientRelationshipSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField(searchField = "name,legalName", sortField = "name")
	private String searchPattern;

	@SearchField(searchField = "legalName", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean legalNameDefined;

	@SearchField(searchField = "relationshipCategory.id", sortField = "relationshipCategory.name")
	private Short relationshipCategoryId;

	@SearchField(searchField = "relationshipCategory.name")
	private String relationshipCategoryName;

	@SearchField(searchFieldPath = "relationshipCategory", searchField = "retail", searchFieldCustomType = SearchFieldCustomTypes.COALESCE_FALSE, leftJoin = true)
	private Boolean relationshipCategoryRetail;

	@SearchField(searchFieldPath = "relationshipType", searchField = "text")
	private String relationshipTypeName;

	@SearchField(searchFieldPath = "relationshipSubType", searchField = "text")
	private String relationshipSubTypeName;


	@SearchField
	private Date inceptionDate;

	@SearchField
	private Date terminateDate;

	@SearchField(searchField = "terminateDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean terminated;

	@SearchField
	private Date workflowStateEffectiveStartDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Date getTerminateDate() {
		return this.terminateDate;
	}


	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}


	public Date getWorkflowStateEffectiveStartDate() {
		return this.workflowStateEffectiveStartDate;
	}


	public void setWorkflowStateEffectiveStartDate(Date workflowStateEffectiveStartDate) {
		this.workflowStateEffectiveStartDate = workflowStateEffectiveStartDate;
	}


	public Boolean getLegalNameDefined() {
		return this.legalNameDefined;
	}


	public void setLegalNameDefined(Boolean legalNameDefined) {
		this.legalNameDefined = legalNameDefined;
	}


	public Date getInceptionDate() {
		return this.inceptionDate;
	}


	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}


	public Short getRelationshipCategoryId() {
		return this.relationshipCategoryId;
	}


	public void setRelationshipCategoryId(Short relationshipCategoryId) {
		this.relationshipCategoryId = relationshipCategoryId;
	}


	public String getRelationshipCategoryName() {
		return this.relationshipCategoryName;
	}


	public void setRelationshipCategoryName(String relationshipCategoryName) {
		this.relationshipCategoryName = relationshipCategoryName;
	}


	public Boolean getRelationshipCategoryRetail() {
		return this.relationshipCategoryRetail;
	}


	public void setRelationshipCategoryRetail(Boolean relationshipCategoryRetail) {
		this.relationshipCategoryRetail = relationshipCategoryRetail;
	}


	public String getRelationshipTypeName() {
		return this.relationshipTypeName;
	}


	public void setRelationshipTypeName(String relationshipTypeName) {
		this.relationshipTypeName = relationshipTypeName;
	}


	public String getRelationshipSubTypeName() {
		return this.relationshipSubTypeName;
	}


	public void setRelationshipSubTypeName(String relationshipSubTypeName) {
		this.relationshipSubTypeName = relationshipSubTypeName;
	}


	public Boolean getTerminated() {
		return this.terminated;
	}


	public void setTerminated(Boolean terminated) {
		this.terminated = terminated;
	}
}
