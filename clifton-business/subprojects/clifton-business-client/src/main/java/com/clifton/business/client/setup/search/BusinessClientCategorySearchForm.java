package com.clifton.business.client.setup.search;


import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BusinessClientCategorySearchForm</code> ...
 *
 * @author manderson
 */
public class BusinessClientCategorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "companyType.id")
	private Short companyTypeId;

	@SearchField(searchFieldPath = "companyType", searchField = "name")
	private String companyTypeName;

	@SearchField
	private BusinessClientCategoryTypes categoryType;

	@SearchField
	private String detailScreenClass;

	@SearchField
	private Boolean clientRelationshipUsed;

	@SearchField
	private Boolean clientRelationshipRequired;

	@SearchField
	private Boolean childrenAllowed;

	@SearchField
	private Boolean ourAccountsOnly;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getCompanyTypeId() {
		return this.companyTypeId;
	}


	public void setCompanyTypeId(Short companyTypeId) {
		this.companyTypeId = companyTypeId;
	}


	public String getCompanyTypeName() {
		return this.companyTypeName;
	}


	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}


	public BusinessClientCategoryTypes getCategoryType() {
		return this.categoryType;
	}


	public void setCategoryType(BusinessClientCategoryTypes categoryType) {
		this.categoryType = categoryType;
	}


	public Boolean getClientRelationshipUsed() {
		return this.clientRelationshipUsed;
	}


	public void setClientRelationshipUsed(Boolean clientRelationshipUsed) {
		this.clientRelationshipUsed = clientRelationshipUsed;
	}


	public Boolean getChildrenAllowed() {
		return this.childrenAllowed;
	}


	public void setChildrenAllowed(Boolean childrenAllowed) {
		this.childrenAllowed = childrenAllowed;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public Boolean getOurAccountsOnly() {
		return this.ourAccountsOnly;
	}


	public void setOurAccountsOnly(Boolean ourAccountsOnly) {
		this.ourAccountsOnly = ourAccountsOnly;
	}


	public Boolean getClientRelationshipRequired() {
		return this.clientRelationshipRequired;
	}


	public void setClientRelationshipRequired(Boolean clientRelationshipRequired) {
		this.clientRelationshipRequired = clientRelationshipRequired;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
