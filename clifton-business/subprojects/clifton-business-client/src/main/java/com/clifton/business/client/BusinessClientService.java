package com.clifton.business.client;


import com.clifton.business.client.search.BusinessClientCompanyRelationshipExtendedSearchForm;
import com.clifton.business.client.search.BusinessClientRelationshipExtendedSearchForm;
import com.clifton.business.client.search.BusinessClientRelationshipSearchForm;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyRelationship;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


public interface BusinessClientService {

	////////////////////////////////////////////////////////////////////////////////
	///////////             Business Client Relationship Methods          //////////
	////////////////////////////////////////////////////////////////////////////////


	public List<BusinessClientRelationship> getBusinessClientRelationshipList(BusinessClientRelationshipSearchForm searchForm);


	public BusinessClientRelationship getBusinessClientRelationship(int id);


	public BusinessClientRelationship getBusinessClientRelationshipByCompany(int companyId);


	public BusinessClientRelationship saveBusinessClientRelationship(BusinessClientRelationship clientRelationship);


	public void deleteBusinessClientRelationship(int id);


	////////////////////////////////////////////////////////////////////////////////
	////////           Business Client Relationship Extended Methods        ////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BusinessClientRelationship.class)
	public List<BusinessClientRelationshipExtended> getBusinessClientRelationshipExtendedList(final BusinessClientRelationshipExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////////
	///////////              Business Client Methods                   /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessClient getBusinessClient(int id);


	public BusinessClient getBusinessClientByCompany(int companyId);


	/**
	 * Returns the client for the given company.  Returns null if the company is not a valid client type of company
	 */
	@DoNotAddRequestMapping
	public BusinessClient getBusinessClientByCompanyFlexible(int companyId);


	public List<BusinessClient> getBusinessClientList(BusinessClientSearchForm searchForm);


	public BusinessClient saveBusinessClient(BusinessClient client);


	public void deleteBusinessClient(int id);


	@SecureMethod(dtoClass = BusinessClient.class)
	public void linkBusinessClientToParent(int parentClientId, int childClientId);


	@SecureMethod(dtoClass = BusinessClient.class)
	public void deleteBusinessClientLinkToParent(int childClientId);


	////////////////////////////////////////////////////////////////////////////////
	//////       Business Client Company Relationship Extended Methods        //////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BusinessCompanyRelationship.class)
	public List<BusinessClientCompanyRelationshipExtended> getBusinessClientCompanyRelationshipExtendedList(BusinessClientCompanyRelationshipExtendedSearchForm searchForm);


	/**
	 * Returns just the unique "Related Company" results from the getBusinessClientCompanyRelationshipExtendedList call.  Used to better limit related company filters on screen
	 */
	@SecureMethod(dtoClass = BusinessCompanyRelationship.class)
	public List<BusinessCompany> getBusinessClientCompanyRelationshipExtendedRelatedCompanyList(BusinessClientCompanyRelationshipExtendedSearchForm searchForm);
}
