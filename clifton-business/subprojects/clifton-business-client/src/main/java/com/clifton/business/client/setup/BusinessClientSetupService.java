package com.clifton.business.client.setup;


import com.clifton.business.client.setup.search.BusinessClientCategorySearchForm;
import com.clifton.business.client.setup.search.BusinessClientRelationshipCategorySearchForm;

import java.util.List;


/**
 * The <code>BusinessClientSetupService</code> ...
 *
 * @author manderson
 */
public interface BusinessClientSetupService {

	////////////////////////////////////////////////////////////////////////////////
	////////       Business Client Relationship Category Methods             ///////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessClientRelationshipCategory getBusinessClientRelationshipCategory(short id);


	public BusinessClientRelationshipCategory getBusinessClientRelationshipCategoryByName(String name);


	public List<BusinessClientRelationshipCategory> getBusinessClientRelationshipCategoryList(BusinessClientRelationshipCategorySearchForm searchForm);


	public BusinessClientRelationshipCategory saveBusinessClientRelationshipCategory(BusinessClientRelationshipCategory bean);


	////////////////////////////////////////////////////////////////////////////
	////////            Business Client Category Methods                 /////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessClientCategory getBusinessClientCategory(short id);


	public BusinessClientCategory getBusinessClientCategoryByName(String name);


	public List<BusinessClientCategory> getBusinessClientCategoryList(BusinessClientCategorySearchForm searchForm);


	public BusinessClientCategory saveBusinessClientCategory(BusinessClientCategory bean);
}
