package com.clifton.business.client;


import com.clifton.business.client.search.BusinessClientCompanyRelationshipExtendedSearchForm;
import com.clifton.business.client.search.BusinessClientRelationshipExtendedSearchForm;
import com.clifton.business.client.search.BusinessClientRelationshipSearchForm;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Service
public class BusinessClientServiceImpl implements BusinessClientService {

	private AdvancedUpdatableDAO<BusinessClient, Criteria> businessClientDAO;
	private AdvancedUpdatableDAO<BusinessClientRelationship, Criteria> businessClientRelationshipDAO;

	private AdvancedReadOnlyDAO<BusinessClientCompanyRelationshipExtended, Criteria> businessClientCompanyRelationshipExtendedDAO;
	private AdvancedReadOnlyDAO<BusinessClientRelationshipExtended, Criteria> businessClientRelationshipExtendedDAO;

	private DaoSingleKeyCache<BusinessClientRelationship, Integer> businessClientRelationshipCompanyCache;
	private DaoSingleKeyCache<BusinessClient, Integer> businessClientCompanyCache;

	private WorkflowDefinitionService workflowDefinitionService;


	////////////////////////////////////////////////////////////////////////////////
	///////////              Business Client Relationship Methods         //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessClientRelationship> getBusinessClientRelationshipList(BusinessClientRelationshipSearchForm searchForm) {
		return getBusinessClientRelationshipDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	public BusinessClientRelationship getBusinessClientRelationship(int id) {
		return getBusinessClientRelationshipDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessClientRelationship getBusinessClientRelationshipByCompany(int companyId) {
		return getBusinessClientRelationshipCompanyCache().getBeanForKeyValueStrict(getBusinessClientRelationshipDAO(), companyId);
	}


	@Override
	public BusinessClientRelationship saveBusinessClientRelationship(BusinessClientRelationship clientRelationship) {
		return getBusinessClientRelationshipDAO().save(clientRelationship);
	}


	@Override
	public void deleteBusinessClientRelationship(int id) {
		getBusinessClientRelationshipDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////           Business Client Relationship Extended Methods        ////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessClientRelationshipExtended> getBusinessClientRelationshipExtendedList(final BusinessClientRelationshipExtendedSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!StringUtils.isEmpty(searchForm.getNewBusinessContactName())) {
					String[] searchValues = searchForm.getNewBusinessContactName().split(",");
					// Example Mary,And  will look for (First Name like Mary or Last Name like Mary) and (First Name like And or last name like And)
					String contactAlias = getPathAlias("newBusinessContact", criteria);
					for (String v : searchValues) {
						criteria.add(Restrictions.or(Restrictions.like(contactAlias + ".firstName", v, MatchMode.ANYWHERE), Restrictions.like(contactAlias + ".lastName", v, MatchMode.ANYWHERE)));
					}
				}

				if (!StringUtils.isEmpty(searchForm.getPortfolioManagerContactName())) {
					String[] searchValues = searchForm.getPortfolioManagerContactName().split(",");
					// Example Mary,And  will look for (First Name like Mary or Last Name like Mary) and (First Name like And or last name like And)
					String contactAlias = getPathAlias("portfolioManagerContact", criteria);
					for (String v : searchValues) {
						criteria.add(Restrictions.or(Restrictions.like(contactAlias + ".firstName", v, MatchMode.ANYWHERE), Restrictions.like(contactAlias + ".lastName", v, MatchMode.ANYWHERE)));
					}
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// Special support for PM or NBD Contact Names
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					return false;
				}
				for (OrderByField field : orderByList) {
					String name = getOrderByFieldName(field, criteria);
					if ("portfolioManagerContactName".equals(name)) {
						name = getPathAlias("portfolioManagerContact", criteria) + ".firstName";
					}
					else if ("newBusinessContactName".equals(name)) {
						name = getPathAlias("newBusinessContact", criteria) + ".firstName";
					}
					else {
						name = getOrderByFieldName(field, criteria);
					}
					boolean asc = OrderByDirections.ASC == field.getDirection();
					criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
				}
				return true;
			}
		};

		return getBusinessClientRelationshipExtendedDAO().findBySearchCriteria(searchConfig);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////               Business Client Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessClient getBusinessClient(int id) {
		return getBusinessClientDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessClient getBusinessClientByCompany(int companyId) {
		return getBusinessClientCompanyCache().getBeanForKeyValueStrict(getBusinessClientDAO(), companyId);
	}


	@Override
	public BusinessClient getBusinessClientByCompanyFlexible(int companyId) {
		return getBusinessClientCompanyCache().getBeanForKeyValue(getBusinessClientDAO(), companyId);
	}


	@Override
	public List<BusinessClient> getBusinessClientList(final BusinessClientSearchForm searchForm) {
		// Active is a custom search restriction that sets other fields, if set as a search restriction
		// remove it because there is no definition attached to it.
		if (searchForm.containsSearchRestriction("active") || searchForm.getActive() != null) {
			Boolean active = searchForm.getActive();
			// If not set explicitly on the form, it's in the restriction list
			if (active == null) {
				SearchRestriction restrict = searchForm.getSearchRestriction("active");
				active = Boolean.valueOf((String) restrict.getValue());
			}
			Date today = DateUtils.clearTime(new Date());
			if (active) {
				// If active, set active on date to today
				searchForm.setActiveOnDate(today);
			}
			else {
				// If not active, set not active date
				searchForm.setNotActiveOnDate(today);
			}
			searchForm.removeSearchRestriction("active");
		}

		HibernateSearchFormConfigurer searchConfigurer = new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// also apply custom filters, if set
				if (searchForm.getActiveOnDate() != null) {
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(true, null, "inceptionDate", "terminateDate", searchForm.getActiveOnDate()));
				}
				else if (searchForm.getNotActiveOnDate() != null) {
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(false, null, "inceptionDate", "terminateDate", searchForm.getNotActiveOnDate()));
				}
			}
		};

		return getBusinessClientDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public BusinessClient saveBusinessClient(BusinessClient client) {
		return getBusinessClientDAO().save(client);
	}


	@Override
	public void deleteBusinessClient(int id) {
		getBusinessClientDAO().delete(id);
	}


	@Override
	public void linkBusinessClientToParent(int parentClientId, int childClientId) {
		BusinessClient parent = getBusinessClient(parentClientId);
		BusinessClientCategory parentCategory = parent.getCategory();

		BusinessClient child = getBusinessClient(childClientId);
		BusinessClientCategory childCategory = child.getCategory();

		// NOTE: SUPPORTS ONE LEVEL DEEP - IF A CATEGORY ALLOWS CHILDREN, IT CANNOT BE A CHILD
		ValidationUtils.assertTrue(parentCategory.isChildrenAllowed(), "Parent Client [" + parent.getName() + "] is in category [" + parentCategory.getName() + "] which does not allow children.");
		ValidationUtils.assertFalse(childCategory.isChildrenAllowed(), "Client Client [" + child.getName() + "] is in category [" + childCategory.getName()
				+ "] which does allows children, so it cannot be a child itself.");

		ValidationUtils.assertNull(child.getParent(), "Client [" + child.getName() + "] is already linked to parent Client [" + (child.getParent() != null ? child.getParent().getName() : "")
				+ "].  A client can only have one parent.");
		// Client Groups can Use a Relationship, but May Not have One give they are cross client relationship groups rare (Investure example)
		if (parentCategory.isClientRelationshipUsed() && parent.getClientRelationship() != null) {
			if (childCategory.isClientRelationshipUsed() && child.getClientRelationship() != null) {
				ValidationUtils.assertTrue(CompareUtils.isEqual(child.getClientRelationship(), parent.getClientRelationship()), "Unable to link client [" + child.getName() + "] to parent client ["
						+ parent.getName() + "] because they belong to different client relationships.");
			}
		}

		if (parentCategory.getChildClientCategory() != null) {
			ValidationUtils.assertTrue(parentCategory.getChildClientCategory().equals(child.getCategory()), "Parent Client [" + parent.getName() + "] only allows children in category [" + parentCategory.getChildClientCategory().getName() + "]. Child client is in category [" + childCategory.getName() + "]");
		}
		child.setParent(parent);
		saveBusinessClient(child);
	}


	@Override
	public void deleteBusinessClientLinkToParent(int childClientId) {
		BusinessClient child = getBusinessClient(childClientId);
		child.setParent(null);
		saveBusinessClient(child);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////       Business Client Company Relationship Extended Methods        //////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessClientCompanyRelationshipExtended> getBusinessClientCompanyRelationshipExtendedList(BusinessClientCompanyRelationshipExtendedSearchForm searchForm) {
		if (searchForm.containsSearchRestriction("clientActive")) {
			searchForm.setClientActive(BooleanUtils.isTrue(searchForm.getSearchRestriction("clientActive")));
		}
		HibernateSearchFormConfigurer searchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientActive() != null) {
					if (BooleanUtils.isTrue(searchForm.getClientActive())) {
						LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull(getPathAlias("client", criteria, JoinType.LEFT_OUTER_JOIN) + ".inceptionDate"),
								Restrictions.le(getPathAlias("client", criteria) + ".inceptionDate", new Date()));
						LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull(getPathAlias("client", criteria, JoinType.LEFT_OUTER_JOIN) + ".terminateDate"),
								Restrictions.ge(getPathAlias("client", criteria) + ".terminateDate", new Date()));
						criteria.add(Restrictions.and(startDateFilter, endDateFilter));
					}
					else {
						criteria.add(Restrictions.or(Restrictions.gt(getPathAlias("client", criteria, JoinType.LEFT_OUTER_JOIN) + ".inceptionDate", new Date()),
								Restrictions.lt(getPathAlias("client", criteria, JoinType.LEFT_OUTER_JOIN) + ".terminateDate", new Date())));
					}
				}
			}
		};
		return getBusinessClientCompanyRelationshipExtendedDAO().findBySearchCriteria(searchFormConfigurer);
	}


	@Override
	public List<BusinessCompany> getBusinessClientCompanyRelationshipExtendedRelatedCompanyList(BusinessClientCompanyRelationshipExtendedSearchForm searchForm) {
		// Track start and page size for the company filter
		int start = searchForm.getStart();
		int pageSize = searchForm.getLimit();

		// Clear values for the relationship query
		searchForm.setStart(PagingCommand.DEFAULT_START);
		searchForm.setLimit(PagingCommand.DEFAULT_LIMIT);

		List<BusinessClientCompanyRelationshipExtended> relationshipExtendedList = getBusinessClientCompanyRelationshipExtendedList(searchForm);
		List<BusinessCompany> companyList = Arrays.asList(BeanUtils.getPropertyValuesUniqueExcludeNull(relationshipExtendedList, BusinessClientCompanyRelationshipExtended::getRelatedCompany, BusinessCompany.class));

		// Reset Limits on the Search Form to what they were
		searchForm.setStart(start);
		searchForm.setLimit(pageSize);

		// Need to return PagingArrayList with proper start/size/page size so filtering in UI works properly
		// If more than one page of data
		int size = CollectionUtils.getSize(companyList);
		if (size > pageSize) {
			int maxIndex = start + pageSize;
			if (maxIndex > size) {
				maxIndex = size;
			}
			// Filter actual rows returned to the current page
			companyList = companyList.subList(start, maxIndex);
		}
		// Return Paging Array List with (if more than one page, then list is already a subset of data, else all the data), start index and the total size of the original list without paging
		return new PagingArrayList<>(companyList, start, size);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessClient, Criteria> getBusinessClientDAO() {
		return this.businessClientDAO;
	}


	public void setBusinessClientDAO(AdvancedUpdatableDAO<BusinessClient, Criteria> businessClientDAO) {
		this.businessClientDAO = businessClientDAO;
	}


	public AdvancedUpdatableDAO<BusinessClientRelationship, Criteria> getBusinessClientRelationshipDAO() {
		return this.businessClientRelationshipDAO;
	}


	public void setBusinessClientRelationshipDAO(AdvancedUpdatableDAO<BusinessClientRelationship, Criteria> businessClientRelationshipDAO) {
		this.businessClientRelationshipDAO = businessClientRelationshipDAO;
	}


	public AdvancedReadOnlyDAO<BusinessClientRelationshipExtended, Criteria> getBusinessClientRelationshipExtendedDAO() {
		return this.businessClientRelationshipExtendedDAO;
	}


	public void setBusinessClientRelationshipExtendedDAO(AdvancedReadOnlyDAO<BusinessClientRelationshipExtended, Criteria> businessClientRelationshipExtendedDAO) {
		this.businessClientRelationshipExtendedDAO = businessClientRelationshipExtendedDAO;
	}


	public DaoSingleKeyCache<BusinessClient, Integer> getBusinessClientCompanyCache() {
		return this.businessClientCompanyCache;
	}


	public void setBusinessClientCompanyCache(DaoSingleKeyCache<BusinessClient, Integer> businessClientCompanyCache) {
		this.businessClientCompanyCache = businessClientCompanyCache;
	}


	public DaoSingleKeyCache<BusinessClientRelationship, Integer> getBusinessClientRelationshipCompanyCache() {
		return this.businessClientRelationshipCompanyCache;
	}


	public void setBusinessClientRelationshipCompanyCache(DaoSingleKeyCache<BusinessClientRelationship, Integer> businessClientRelationshipCompanyCache) {
		this.businessClientRelationshipCompanyCache = businessClientRelationshipCompanyCache;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public AdvancedReadOnlyDAO<BusinessClientCompanyRelationshipExtended, Criteria> getBusinessClientCompanyRelationshipExtendedDAO() {
		return this.businessClientCompanyRelationshipExtendedDAO;
	}


	public void setBusinessClientCompanyRelationshipExtendedDAO(AdvancedReadOnlyDAO<BusinessClientCompanyRelationshipExtended, Criteria> businessClientCompanyRelationshipExtendedDAO) {
		this.businessClientCompanyRelationshipExtendedDAO = businessClientCompanyRelationshipExtendedDAO;
	}
}
