package com.clifton.business.client.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;


/**
 * The <code>BusinessClientRelationshipExtendedSearchForm</code> ...
 *
 * @author manderson
 */
public class BusinessClientRelationshipExtendedSearchForm extends BusinessClientRelationshipSearchForm {

	@SearchField(searchField = "newBusinessContact.id")
	private Integer newBusinessContactId;

	// Relationship Manager
	@SearchField(searchField = "portfolioManagerContact.id")
	private Integer portfolioManagerContactId;

	@SearchField(searchField = "portfolioManagerContact.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean portfolioManagerContactPopulated;

	// Custom Name search
	private String newBusinessContactName;

	// Custom Name searches
	private String portfolioManagerContactName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getNewBusinessContactId() {
		return this.newBusinessContactId;
	}


	public void setNewBusinessContactId(Integer newBusinessContactId) {
		this.newBusinessContactId = newBusinessContactId;
	}


	public Integer getPortfolioManagerContactId() {
		return this.portfolioManagerContactId;
	}


	public void setPortfolioManagerContactId(Integer portfolioManagerContactId) {
		this.portfolioManagerContactId = portfolioManagerContactId;
	}


	public String getNewBusinessContactName() {
		return this.newBusinessContactName;
	}


	public void setNewBusinessContactName(String newBusinessContactName) {
		this.newBusinessContactName = newBusinessContactName;
	}


	public String getPortfolioManagerContactName() {
		return this.portfolioManagerContactName;
	}


	public void setPortfolioManagerContactName(String portfolioManagerContactName) {
		this.portfolioManagerContactName = portfolioManagerContactName;
	}


	public Boolean getPortfolioManagerContactPopulated() {
		return this.portfolioManagerContactPopulated;
	}


	public void setPortfolioManagerContactPopulated(Boolean portfolioManagerContactPopulated) {
		this.portfolioManagerContactPopulated = portfolioManagerContactPopulated;
	}
}
