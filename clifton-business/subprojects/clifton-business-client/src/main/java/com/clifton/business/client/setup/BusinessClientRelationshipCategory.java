package com.clifton.business.client.setup;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.list.SystemList;


/**
 * The <code>BusinessClientRelationshipCategory</code> is used to classify our client relationships (and therefore client, and accounts)
 * <p>
 * This is currently only used with 2 values Institutional or Retail.  In case additional categories are added, retail flag is available to help with filtering as the two are often looked at separately.
 *
 * @author manderson
 */
@CacheByName
public class BusinessClientRelationshipCategory extends NamedEntity<Short> {


	private boolean retail;

	/**
	 * This is the system list used for relationship type (a.k.a. Firm Category) selections on relationships under this category
	 * If blank then the field is disabled
	 */
	private SystemList relationshipTypeSystemList;

	/**
	 * This is the system list used for relationship sub type (a.k.a. Firm Sub-Category) selections on relationships under this category
	 * If blank then the field is disabled
	 */
	private SystemList relationshipSubTypeSystemList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isRetail() {
		return this.retail;
	}


	public void setRetail(boolean retail) {
		this.retail = retail;
	}


	public SystemList getRelationshipTypeSystemList() {
		return this.relationshipTypeSystemList;
	}


	public void setRelationshipTypeSystemList(SystemList relationshipTypeSystemList) {
		this.relationshipTypeSystemList = relationshipTypeSystemList;
	}


	public SystemList getRelationshipSubTypeSystemList() {
		return this.relationshipSubTypeSystemList;
	}


	public void setRelationshipSubTypeSystemList(SystemList relationshipSubTypeSystemList) {
		this.relationshipSubTypeSystemList = relationshipSubTypeSystemList;
	}
}
