package com.clifton.business.client.setup.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class BusinessClientRelationshipCategorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Boolean retail;

	@SearchField(searchField = "relationshipTypeSystemList.name")
	private String relationshipTypeSystemListName;

	@SearchField(searchField = "relationshipSubTypeSystemList.name")
	private String relationshipSubTypeSystemListName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getRetail() {
		return this.retail;
	}


	public void setRetail(Boolean retail) {
		this.retail = retail;
	}


	public String getRelationshipTypeSystemListName() {
		return this.relationshipTypeSystemListName;
	}


	public void setRelationshipTypeSystemListName(String relationshipTypeSystemListName) {
		this.relationshipTypeSystemListName = relationshipTypeSystemListName;
	}


	public String getRelationshipSubTypeSystemListName() {
		return this.relationshipSubTypeSystemListName;
	}


	public void setRelationshipSubTypeSystemListName(String relationshipSubTypeSystemListName) {
		this.relationshipSubTypeSystemListName = relationshipSubTypeSystemListName;
	}
}
