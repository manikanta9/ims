package com.clifton.business.client.observers;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BusinessClientObserver</code> will create/update
 * related {@link BusinessCompany} object prior to saving the client
 *
 * @author manderson
 */
@Component
public class BusinessClientObserver extends SelfRegisteringDaoObserver<BusinessClient> {

	private BusinessClientService businessClientService;
	private BusinessCompanyService businessCompanyService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<BusinessClient> dao, DaoEventTypes event, BusinessClient bean) {
		if (event.isInsert() || event.isUpdate()) {
			BusinessClient original = null;
			if (event.isUpdate()) {
				original = getOriginalBean(dao, bean);
			}
			// On inserts of new clients, validate that not adding to a terminated relationship (if client terminate date is null - can't think of why we'd add a client that is already terminated though)
			if (bean.getTerminateDate() == null && bean.getClientRelationship() != null && (original == null || original.getClientRelationship() == null)) {
				ValidationUtils.assertNull(bean.getClientRelationship().getTerminateDate(), "Cannot add a client to a terminated client relationship.");
			}
			validateClientCategory(bean, original, dao, event);
			saveRelatedCompany(bean, original);
			saveClientRelationship(bean);
		}
	}


	@Override
	protected void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<BusinessClient> dao, DaoEventTypes event, BusinessClient bean, Throwable e) {
		if (e == null && event.isDelete()) {
			getBusinessCompanyService().deleteBusinessCompany(bean.getCompany().getId());
		}
	}


	/**
	 * Validates category specific information - i.e. Client Relationship Used, Client Type only required/used for actual Clients
	 */
	private void validateClientCategory(BusinessClient bean, BusinessClient original, ReadOnlyDAO<BusinessClient> dao, DaoEventTypes event) {
		BusinessClientCategory category = bean.getCategory();
		ValidationUtils.assertNotNull(category, "Client Category is required.");
		if (original != null && !category.equals(original.getCategory())) {
			throw new ValidationException("Cannot change category from [" + original.getCategory().getName() + "] to [" + category.getName() + "].");
		}

		if (category.getCategoryType() == BusinessClientCategoryTypes.CLIENT && bean.getClientType() == null) {
			throw new ValidationException("This client [" + bean.getName() + "] needs to have a Client Type, please select a Client Type");
		}

		// If used, Client Relationship isn't required because Client Admins create clients, and Marketing needs to maintain/create those relationships
		// Marketing gets Critical Notifications when Clients that use Client Relationships don't have it assigned
		if (!category.isClientRelationshipUsed()) {
			bean.setClientRelationship(null);
		}
		// Otherwise if it's being changed - make sure parent/child relationship is still OK
		else if (event.isUpdate() && original != null && bean.getClientRelationship() != null && !CompareUtils.isEqual(bean.getClientRelationship(), original.getClientRelationship())) {
			if (category.isChildrenAllowed()) {
				// Get all children and confirm relationship change
				List<BusinessClient> childrenList = dao.findByField("parent.id", bean.getId());
				for (BusinessClient child : CollectionUtils.getIterable(childrenList)) {
					if (child.getClientRelationship() != null && !CompareUtils.isEqual(bean.getClientRelationship(), child.getClientRelationship())) {
						throw new ValidationException("This client [" + bean.getName() + "] has a child " + child.getName() + " whose client relationship is set to [" + child.getClientRelationship().getName() + "]. Cannot have clients that belong to a different relationship than its children." + (!bean.getCategory().isClientRelationshipRequired() ? "You can clear the client relationship from the parent if you would like to have a cross client relationship children." : ""));
					}
				}
			}
			else if (bean.getParent() != null) {
				if (bean.getParent().getClientRelationship() != null && !CompareUtils.isEqual(bean.getParent().getClientRelationship(), bean.getClientRelationship())) {
					throw new ValidationException("This client [" + bean.getName() + "] is a member of " + bean.getParent().getCategory().getName() + " whose client relationship is set to [" + bean.getParent().getClientRelationship().getName() + "]. Cannot have clients that belong to a different relationship than its parent.");
				}
			}
		}
	}


	/**
	 * Creates/Updates Company tied to this Client - Company Type is defined by the category
	 */
	private void saveRelatedCompany(BusinessClient bean, BusinessClient original) {
		BusinessCompany company = (original == null ? new BusinessCompany() : original.getCompany());
		if (bean.getCompany() != null) {
			BeanUtils.copyProperties(bean.getCompany(), company);
		}
		if (company.getType() == null) {
			company.setType(bean.getCategory().getCompanyType());
		}
		// Use legal name if populated, else name field
		company.setName(bean.getLegalLabel());
		// If Legal Name is different than short name - set short name as alias
		if (!CompareUtils.isEqual(bean.getLegalLabel(), bean.getName())) {
			company.setAlias(bean.getName());
		}
		// Otherwise clear it - no reason to set alias if it's the same as the name
		else {
			company.setAlias(null);
		}

		// If client has a parent, make this client's company's parent the same company as the parent's company
		if (bean.getParent() != null && (company.getParent() == null || !company.getParent().equals(bean.getParent().getCompany()))) {
			company.setParent(bean.getParent().getCompany());
		}
		getBusinessCompanyService().saveBusinessCompany(company);
		bean.setCompany(company);
	}


	/**
	 * For those that use a client relationship, rolls up the inception date from the client to the  client relationship (when null)
	 */
	private void saveClientRelationship(BusinessClient bean) {
		// Set Client Relationship Inception Date = This Inception Date if Missing on the Relationship
		if (bean.getInceptionDate() != null && bean.getClientRelationship() != null && bean.getClientRelationship().getInceptionDate() == null) {
			BusinessClientRelationship cr = getBusinessClientService().getBusinessClientRelationship(bean.getClientRelationship().getId());
			cr.setInceptionDate(bean.getInceptionDate());
			getBusinessClientService().saveBusinessClientRelationship(cr);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}
}
