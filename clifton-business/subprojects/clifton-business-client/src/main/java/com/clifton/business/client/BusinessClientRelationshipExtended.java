package com.clifton.business.client;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.dataaccess.dao.NonPersistentObject;


/**
 * The <code>BusinessClientRelationshipExtended</code> is an extension of the {@link BusinessClientRelationship} object with additional calculated
 * fields, i.e. NBD Client Service Rep, PM Client Service Rep
 *
 * @author manderson
 */
@NonPersistentObject
public class BusinessClientRelationshipExtended extends BusinessClientRelationship {

	/**
	 * Clifton contacts responsible for this Client Relationship
	 * (Currently defined through Client Contact relationships and assumes only
	 * one per Client relationship (notification for this)) - Selects TOP 1
	 */
	private BusinessContact newBusinessContact; // Contact Type: NBD Client Service Representative
	private BusinessContact portfolioManagerContact; // Contact Type: Relationship Manager


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContact getNewBusinessContact() {
		return this.newBusinessContact;
	}


	public void setNewBusinessContact(BusinessContact newBusinessContact) {
		this.newBusinessContact = newBusinessContact;
	}


	public BusinessContact getPortfolioManagerContact() {
		return this.portfolioManagerContact;
	}


	public void setPortfolioManagerContact(BusinessContact portfolioManagerContact) {
		this.portfolioManagerContact = portfolioManagerContact;
	}
}
