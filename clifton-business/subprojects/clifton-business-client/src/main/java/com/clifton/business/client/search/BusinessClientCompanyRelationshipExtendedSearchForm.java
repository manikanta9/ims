package com.clifton.business.client.search;

import com.clifton.business.company.search.BusinessCompanyRelationshipSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;

import java.util.Date;


/**
 * @author manderson
 */
public class BusinessClientCompanyRelationshipExtendedSearchForm extends BusinessCompanyRelationshipSearchForm {

	@SearchField(searchFieldPath = "clientRelationship", searchField = "name,legalName")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "clientRelationship.workflowState", searchField = "name", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL, leftJoin = true)
	private String[] clientRelationshipWorkflowStateNames;

	@SearchField(searchFieldPath = "client", searchField = "name,legalName")
	private String clientName;

	@SearchField(searchFieldPath = "client", searchField = "inceptionDate")
	private Date clientInceptionDate;

	@SearchField(searchFieldPath = "client", searchField = "terminateDate")
	private Date clientTerminateDate;

	// Custom Search Field on clientInceptionDate and clientTerminateDate
	private Boolean clientActive;

	@SearchField(searchFieldPath = "relatedCompany", searchField = "type.id", sortField = "type.name")
	private Short relatedCompanyTypeId;

	@SearchField(searchFieldPath = "relatedCompany", searchField = "name")
	private String relatedCompanyName;

	@SearchField(searchField = "relatedCompany.id,relatedCompany.parent.id", sortField = "relatedCompany.name")
	private Integer relatedCompanyIdOrParent;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public String[] getClientRelationshipWorkflowStateNames() {
		return this.clientRelationshipWorkflowStateNames;
	}


	public void setClientRelationshipWorkflowStateNames(String[] clientRelationshipWorkflowStateNames) {
		this.clientRelationshipWorkflowStateNames = clientRelationshipWorkflowStateNames;
	}


	public String getClientName() {
		return this.clientName;
	}


	public void setClientName(String clientName) {
		this.clientName = clientName;
	}


	public Date getClientInceptionDate() {
		return this.clientInceptionDate;
	}


	public void setClientInceptionDate(Date clientInceptionDate) {
		this.clientInceptionDate = clientInceptionDate;
	}


	public Date getClientTerminateDate() {
		return this.clientTerminateDate;
	}


	public void setClientTerminateDate(Date clientTerminateDate) {
		this.clientTerminateDate = clientTerminateDate;
	}


	public Boolean getClientActive() {
		return this.clientActive;
	}


	public void setClientActive(Boolean clientActive) {
		this.clientActive = clientActive;
	}


	public Short getRelatedCompanyTypeId() {
		return this.relatedCompanyTypeId;
	}


	public void setRelatedCompanyTypeId(Short relatedCompanyTypeId) {
		this.relatedCompanyTypeId = relatedCompanyTypeId;
	}


	public String getRelatedCompanyName() {
		return this.relatedCompanyName;
	}


	public void setRelatedCompanyName(String relatedCompanyName) {
		this.relatedCompanyName = relatedCompanyName;
	}


	public Integer getRelatedCompanyIdOrParent() {
		return this.relatedCompanyIdOrParent;
	}


	public void setRelatedCompanyIdOrParent(Integer relatedCompanyIdOrParent) {
		this.relatedCompanyIdOrParent = relatedCompanyIdOrParent;
	}
}
