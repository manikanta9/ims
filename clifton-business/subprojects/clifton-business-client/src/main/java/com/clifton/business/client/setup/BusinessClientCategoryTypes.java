package com.clifton.business.client.setup;


/**
 * The <code>BusinessClientCategoryTypes</code> ...
 *
 * @author manderson
 */
public enum BusinessClientCategoryTypes {

	CLIENT, VIRTUAL_CLIENT, FUND, SISTER_CLIENT
}
