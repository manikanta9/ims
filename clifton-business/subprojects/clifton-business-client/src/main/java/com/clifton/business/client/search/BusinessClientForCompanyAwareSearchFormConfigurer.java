package com.clifton.business.client.search;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.search.BusinessCompanyAwareSearchForm;
import com.clifton.business.company.search.BusinessCompanyAwareSearchFormConfigurer;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


/**
 * @author danielh
 */
@Component
public class BusinessClientForCompanyAwareSearchFormConfigurer extends BusinessCompanyAwareSearchFormConfigurer {

	private BusinessClientService businessClientService;


	@Override
	public List<Integer> getCompanyIds(BusinessCompanyAwareSearchForm searchForm) {
		List<Integer> companyIds = super.getCompanyIds(searchForm);

		if (BooleanUtils.isTrue(searchForm.getIncludeClientOrClientRelationship()) && (searchForm.getCompanyId() != null || searchForm.getCompanyIdOrRelatedCompany() != null)) {
			// Look up the Company to See what type it is
			BusinessCompany company = getBusinessCompanyService().getBusinessCompany(ObjectUtils.coalesce(searchForm.getCompanyId(), searchForm.getCompanyIdOrRelatedCompany()));
			if (company != null) {
				if (StringUtils.isEqual(BusinessClientRelationship.COMPANY_TYPE_NAME, company.getType().getName())) {
					BusinessClientRelationship relationship = getBusinessClientService().getBusinessClientRelationshipByCompany(company.getId());
					if (relationship != null) {
						BusinessClientSearchForm clientSearchForm = new BusinessClientSearchForm();
						clientSearchForm.setClientRelationshipId(relationship.getId());
						List<BusinessClient> clientList = getBusinessClientService().getBusinessClientList(clientSearchForm);
						if (!CollectionUtils.isEmpty(clientList)) {
							companyIds.addAll(Arrays.asList(BeanUtils.getPropertyValuesUniqueExcludeNull(clientList, businessClient -> businessClient.getCompany().getId(), Integer.class)));
						}
					}
				}
				else {
					// Otherwise we assume it's a client - use flexible retrieval in case it isn't
					// If it's not a client, it's OK, we just don't add additional company ids to the filters
					BusinessClient client = getBusinessClientService().getBusinessClientByCompanyFlexible(company.getId());
					if (client != null && client.getClientRelationship() != null) {
						companyIds.add(client.getClientRelationship().getCompany().getId());
					}
				}
			}
		}
		return companyIds;
	}
	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}
}
