package com.clifton.business.client;


import com.clifton.business.client.setup.BusinessClientRelationshipCategory;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.workflow.BaseNamedWorkflowEffectiveDateAwareEntity;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessClientRelationship</code> ...
 *
 * @author manderson
 */
public class BusinessClientRelationship extends BaseNamedWorkflowEffectiveDateAwareEntity implements SystemColumnCustomValueAware {

	public static final String BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME = "BusinessClientRelationship";

	public static final String COMPANY_TYPE_NAME = "Client Relationship";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * a.k.a Retail or Institutional
	 */
	private BusinessClientRelationshipCategory relationshipCategory;

	private String legalName;

	private BusinessCompany company;

	/**
	 * Also called Firm Category
	 * Uses SystemList defined by the BusinessClientRelationshipCategory
	 */
	private SystemListItem relationshipType;

	/**
	 * Also called Firm Sub-Category
	 * Uses SystemList defined by the BusinessClientRelationshipCategory
	 */
	private SystemListItem relationshipSubType;

	/**
	 * This is the first date for any of it's client's accounts that positions were put on.
	 */
	private Date inceptionDate;

	/**
	 * This is the date that the client officially terminated
	 * their relationship with us.  Used only once the workflow
	 * hits the Terminate State.
	 */
	private Date terminateDate;

	/**
	 * Custom Columns/Values
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isLegalNameDefined() {
		return !StringUtils.isEmpty(getLegalName());
	}


	public String getLegalLabel() {
		if (isLegalNameDefined()) {
			return getLegalName();
		}
		return getName();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public String getLegalName() {
		return this.legalName;
	}


	public void setLegalName(String legalName) {
		this.legalName = legalName;
	}


	public Date getInceptionDate() {
		return this.inceptionDate;
	}


	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}


	public BusinessClientRelationshipCategory getRelationshipCategory() {
		return this.relationshipCategory;
	}


	public void setRelationshipCategory(BusinessClientRelationshipCategory relationshipCategory) {
		this.relationshipCategory = relationshipCategory;
	}


	public SystemListItem getRelationshipType() {
		return this.relationshipType;
	}


	public void setRelationshipType(SystemListItem relationshipType) {
		this.relationshipType = relationshipType;
	}


	public SystemListItem getRelationshipSubType() {
		return this.relationshipSubType;
	}


	public void setRelationshipSubType(SystemListItem relationshipSubType) {
		this.relationshipSubType = relationshipSubType;
	}


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public Date getTerminateDate() {
		return this.terminateDate;
	}


	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}
}
