package com.clifton.business.client.setup;


import com.clifton.business.client.setup.search.BusinessClientCategorySearchForm;
import com.clifton.business.client.setup.search.BusinessClientRelationshipCategorySearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>BusinessClientSetupServiceImpl</code> ...
 *
 * @author manderson
 */
@Service
public class BusinessClientSetupServiceImpl implements BusinessClientSetupService {


	private AdvancedUpdatableDAO<BusinessClientRelationshipCategory, Criteria> businessClientRelationshipCategoryDAO;
	private AdvancedUpdatableDAO<BusinessClientCategory, Criteria> businessClientCategoryDAO;

	private DaoNamedEntityCache<BusinessClientRelationshipCategory> businessClientRelationshipCategoryCache;
	private DaoNamedEntityCache<BusinessClientCategory> businessClientCategoryCache;


	////////////////////////////////////////////////////////////////////////////////
	////////       Business Client Relationship Category Methods             ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessClientRelationshipCategory getBusinessClientRelationshipCategory(short id) {
		return getBusinessClientRelationshipCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessClientRelationshipCategory getBusinessClientRelationshipCategoryByName(String name) {
		return getBusinessClientRelationshipCategoryCache().getBeanForKeyValueStrict(getBusinessClientRelationshipCategoryDAO(), name);
	}


	@Override
	public List<BusinessClientRelationshipCategory> getBusinessClientRelationshipCategoryList(BusinessClientRelationshipCategorySearchForm searchForm) {
		return getBusinessClientRelationshipCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessClientRelationshipCategory saveBusinessClientRelationshipCategory(BusinessClientRelationshipCategory bean) {
		return getBusinessClientRelationshipCategoryDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////              Business Client Category Methods                   ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessClientCategory getBusinessClientCategory(short id) {
		return getBusinessClientCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessClientCategory getBusinessClientCategoryByName(String name) {
		return getBusinessClientCategoryCache().getBeanForKeyValueStrict(getBusinessClientCategoryDAO(), name);
	}


	@Override
	public List<BusinessClientCategory> getBusinessClientCategoryList(BusinessClientCategorySearchForm searchForm) {
		return getBusinessClientCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessClientCategory saveBusinessClientCategory(BusinessClientCategory bean) {
		return getBusinessClientCategoryDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoNamedEntityCache<BusinessClientCategory> getBusinessClientCategoryCache() {
		return this.businessClientCategoryCache;
	}


	public void setBusinessClientCategoryCache(DaoNamedEntityCache<BusinessClientCategory> businessClientCategoryCache) {
		this.businessClientCategoryCache = businessClientCategoryCache;
	}


	public AdvancedUpdatableDAO<BusinessClientCategory, Criteria> getBusinessClientCategoryDAO() {
		return this.businessClientCategoryDAO;
	}


	public void setBusinessClientCategoryDAO(AdvancedUpdatableDAO<BusinessClientCategory, Criteria> businessClientCategoryDAO) {
		this.businessClientCategoryDAO = businessClientCategoryDAO;
	}


	public AdvancedUpdatableDAO<BusinessClientRelationshipCategory, Criteria> getBusinessClientRelationshipCategoryDAO() {
		return this.businessClientRelationshipCategoryDAO;
	}


	public void setBusinessClientRelationshipCategoryDAO(AdvancedUpdatableDAO<BusinessClientRelationshipCategory, Criteria> businessClientRelationshipCategoryDAO) {
		this.businessClientRelationshipCategoryDAO = businessClientRelationshipCategoryDAO;
	}


	public DaoNamedEntityCache<BusinessClientRelationshipCategory> getBusinessClientRelationshipCategoryCache() {
		return this.businessClientRelationshipCategoryCache;
	}


	public void setBusinessClientRelationshipCategoryCache(DaoNamedEntityCache<BusinessClientRelationshipCategory> businessClientRelationshipCategoryCache) {
		this.businessClientRelationshipCategoryCache = businessClientRelationshipCategoryCache;
	}
}
