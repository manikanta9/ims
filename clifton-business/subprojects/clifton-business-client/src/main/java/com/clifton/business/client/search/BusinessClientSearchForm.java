package com.clifton.business.client.search;


import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.util.Date;


public class BusinessClientSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,legalName", sortField = "name")
	private String searchPattern;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	@SearchField(searchField = "parent.name")
	private String parentName;

	@SearchField(searchField = "clientRelationship.id")
	private Integer clientRelationshipId;

	@SearchField(searchField = "name,legalName", searchFieldPath = "clientRelationship", sortField = "name")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "clientRelationship", searchField = "relationshipCategory.id", sortField = "relationshipCategory.name")
	private Short clientRelationshipCategoryId;

	@SearchField(searchFieldPath = "clientRelationship.relationshipCategory", searchField = "name")
	private String clientRelationshipCategoryName;

	@SearchField(searchFieldPath = "clientRelationship.relationshipCategory", searchField = "retail", searchFieldCustomType = SearchFieldCustomTypes.COALESCE_FALSE, leftJoin = true)
	private Boolean clientRelationshipCategoryRetail;

	@SearchField(searchFieldPath = "clientRelationship.relationshipType", searchField = "text")
	private String clientRelationshipTypeName; // a.k.a. "Firm Category"

	@SearchField(searchFieldPath = "clientRelationship.relationshipSubType", searchField = "text")
	private String clientRelationshipSubTypeName; // a.k.a. "Firm Sub-Category"

	@SearchField(searchFieldPath = "clientRelationship.workflowState", searchField = "name", comparisonConditions = {ComparisonConditions.BEGINS_WITH, ComparisonConditions.LIKE})
	private String clientRelationshipWorkflowStateName;

	@SearchField(searchFieldPath = "clientRelationship", searchField = "workflowStateEffectiveStartDate")
	private Date clientRelationshipWorkflowStateEffectiveStartDate;

	@SearchField(searchFieldPath = "clientType", searchField = "text")
	private String clientTypeName;

	@SearchField(searchFieldPath = "clientType", searchField = "text", comparisonConditions = ComparisonConditions.EQUALS)
	private String clientTypeNameEquals;

	@SearchField(searchFieldPath = "clientType", searchField = "text", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private String excludeClientTypeName;

	@SearchField(searchField = "category.id")
	private Short categoryId;

	@SearchField(searchFieldPath = "category", searchField = "name")
	private String categoryName;

	@SearchField(searchFieldPath = "category", searchField = "childrenAllowed")
	private Boolean categoryChildrenAllowed;

	@SearchField(searchFieldPath = "category.childClientCategory", searchField = "categoryType")
	private BusinessClientCategoryTypes categoryChildCategoryType;

	@SearchField(searchFieldPath = "category.childClientCategory", searchField = "categoryType", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private BusinessClientCategoryTypes excludeCategoryChildCategoryType;

	@SearchField(searchFieldPath = "category", searchField = "categoryType")
	private BusinessClientCategoryTypes categoryType;

	@SearchField(searchField = "parent.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullParent;

	@SearchField(searchField = "legalName", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean legalNameDefined;

	@SearchField
	private Date inceptionDate;

	@SearchField
	private Date terminateDate;

	@SearchField(searchField = "terminateDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean terminated;

	@SearchField
	private Boolean taxable;

	@SearchField
	private String federalTaxNumber;

	@SearchField
	private String stateTaxNumber;

	@SearchField
	private String origin;

	@SearchField
	private String originDomicile;

	@SearchField(searchFieldPath = "company", searchField = "addressLine1")
	private String addressLine1;

	@SearchField(searchFieldPath = "company", searchField = "addressLine2")
	private String addressLine2;

	@SearchField(searchFieldPath = "company", searchField = "addressLine3")
	private String addressLine3;

	@SearchField(searchFieldPath = "company", searchField = "city")
	private String city;

	@SearchField(searchFieldPath = "company", searchField = "state")
	private String state;

	@SearchField(searchFieldPath = "company", searchField = "postalCode")
	private String postalCode;

	@SearchField(searchFieldPath = "company", searchField = "country")
	private String country;

	// CUSTOM FILTER: If active == true, sets activeOnDate to today, if active == false, sets endDate to yesterday
	private Boolean active;

	// CUSTOM FILTER: (startDate IS NULL OR startDate >= activeOnDate) AND (endDate IS NULL OR endDate <= activeOnDate)
	private Date activeOnDate;

	// CUSTOM FILTER: (startDate IS NOT NULL AND startDate > activeOnDate) OR (endDate IS NOT NULL AND endDate < activeOnDate)
	private Date notActiveOnDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public String getParentName() {
		return this.parentName;
	}


	public void setParentName(String parentName) {
		this.parentName = parentName;
	}


	public Integer getClientRelationshipId() {
		return this.clientRelationshipId;
	}


	public void setClientRelationshipId(Integer clientRelationshipId) {
		this.clientRelationshipId = clientRelationshipId;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public Short getClientRelationshipCategoryId() {
		return this.clientRelationshipCategoryId;
	}


	public void setClientRelationshipCategoryId(Short clientRelationshipCategoryId) {
		this.clientRelationshipCategoryId = clientRelationshipCategoryId;
	}


	public String getClientRelationshipCategoryName() {
		return this.clientRelationshipCategoryName;
	}


	public void setClientRelationshipCategoryName(String clientRelationshipCategoryName) {
		this.clientRelationshipCategoryName = clientRelationshipCategoryName;
	}


	public Boolean getClientRelationshipCategoryRetail() {
		return this.clientRelationshipCategoryRetail;
	}


	public void setClientRelationshipCategoryRetail(Boolean clientRelationshipCategoryRetail) {
		this.clientRelationshipCategoryRetail = clientRelationshipCategoryRetail;
	}


	public String getClientRelationshipTypeName() {
		return this.clientRelationshipTypeName;
	}


	public void setClientRelationshipTypeName(String clientRelationshipTypeName) {
		this.clientRelationshipTypeName = clientRelationshipTypeName;
	}


	public String getClientRelationshipSubTypeName() {
		return this.clientRelationshipSubTypeName;
	}


	public void setClientRelationshipSubTypeName(String clientRelationshipSubTypeName) {
		this.clientRelationshipSubTypeName = clientRelationshipSubTypeName;
	}


	public String getClientRelationshipWorkflowStateName() {
		return this.clientRelationshipWorkflowStateName;
	}


	public void setClientRelationshipWorkflowStateName(String clientRelationshipWorkflowStateName) {
		this.clientRelationshipWorkflowStateName = clientRelationshipWorkflowStateName;
	}


	public Date getClientRelationshipWorkflowStateEffectiveStartDate() {
		return this.clientRelationshipWorkflowStateEffectiveStartDate;
	}


	public void setClientRelationshipWorkflowStateEffectiveStartDate(Date clientRelationshipWorkflowStateEffectiveStartDate) {
		this.clientRelationshipWorkflowStateEffectiveStartDate = clientRelationshipWorkflowStateEffectiveStartDate;
	}


	public String getClientTypeName() {
		return this.clientTypeName;
	}


	public void setClientTypeName(String clientTypeName) {
		this.clientTypeName = clientTypeName;
	}


	public String getClientTypeNameEquals() {
		return this.clientTypeNameEquals;
	}


	public void setClientTypeNameEquals(String clientTypeNameEquals) {
		this.clientTypeNameEquals = clientTypeNameEquals;
	}


	public String getExcludeClientTypeName() {
		return this.excludeClientTypeName;
	}


	public void setExcludeClientTypeName(String excludeClientTypeName) {
		this.excludeClientTypeName = excludeClientTypeName;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Boolean getCategoryChildrenAllowed() {
		return this.categoryChildrenAllowed;
	}


	public void setCategoryChildrenAllowed(Boolean categoryChildrenAllowed) {
		this.categoryChildrenAllowed = categoryChildrenAllowed;
	}


	public BusinessClientCategoryTypes getCategoryChildCategoryType() {
		return this.categoryChildCategoryType;
	}


	public void setCategoryChildCategoryType(BusinessClientCategoryTypes categoryChildCategoryType) {
		this.categoryChildCategoryType = categoryChildCategoryType;
	}


	public BusinessClientCategoryTypes getExcludeCategoryChildCategoryType() {
		return this.excludeCategoryChildCategoryType;
	}


	public void setExcludeCategoryChildCategoryType(BusinessClientCategoryTypes excludeCategoryChildCategoryType) {
		this.excludeCategoryChildCategoryType = excludeCategoryChildCategoryType;
	}


	public BusinessClientCategoryTypes getCategoryType() {
		return this.categoryType;
	}


	public void setCategoryType(BusinessClientCategoryTypes categoryType) {
		this.categoryType = categoryType;
	}


	public Boolean getNullParent() {
		return this.nullParent;
	}


	public void setNullParent(Boolean nullParent) {
		this.nullParent = nullParent;
	}


	public Boolean getLegalNameDefined() {
		return this.legalNameDefined;
	}


	public void setLegalNameDefined(Boolean legalNameDefined) {
		this.legalNameDefined = legalNameDefined;
	}


	public Date getInceptionDate() {
		return this.inceptionDate;
	}


	public void setInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
	}


	public Date getTerminateDate() {
		return this.terminateDate;
	}


	public void setTerminateDate(Date terminateDate) {
		this.terminateDate = terminateDate;
	}


	public Boolean getTerminated() {
		return this.terminated;
	}


	public void setTerminated(Boolean terminated) {
		this.terminated = terminated;
	}


	public Boolean getTaxable() {
		return this.taxable;
	}


	public void setTaxable(Boolean taxable) {
		this.taxable = taxable;
	}


	public String getFederalTaxNumber() {
		return this.federalTaxNumber;
	}


	public void setFederalTaxNumber(String federalTaxNumber) {
		this.federalTaxNumber = federalTaxNumber;
	}


	public String getStateTaxNumber() {
		return this.stateTaxNumber;
	}


	public void setStateTaxNumber(String stateTaxNumber) {
		this.stateTaxNumber = stateTaxNumber;
	}


	public String getOrigin() {
		return this.origin;
	}


	public void setOrigin(String origin) {
		this.origin = origin;
	}


	public String getOriginDomicile() {
		return this.originDomicile;
	}


	public void setOriginDomicile(String originDomicile) {
		this.originDomicile = originDomicile;
	}


	public String getAddressLine1() {
		return this.addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return this.addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getAddressLine3() {
		return this.addressLine3;
	}


	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}


	public String getCity() {
		return this.city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return this.state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getPostalCode() {
		return this.postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Date getNotActiveOnDate() {
		return this.notActiveOnDate;
	}


	public void setNotActiveOnDate(Date notActiveOnDate) {
		this.notActiveOnDate = notActiveOnDate;
	}
}
