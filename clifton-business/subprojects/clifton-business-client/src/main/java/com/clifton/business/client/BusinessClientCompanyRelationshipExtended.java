package com.clifton.business.client;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyRelationship;
import com.clifton.core.dataaccess.dao.NonPersistentObject;


/**
 * The <code>BusinessClientCompanyRelationshipExtended</code> is a customized view of the {@link com.clifton.business.company.BusinessCompanyRelationship} table
 * where it pulls the {@link BusinessClientRelationship} and/or {@link BusinessClient} into specific fields
 * <p>
 * This is used to more easily see a global view of the client relationships and/or clients that utilize a specific Outsourced CIO, Consultant, etc.
 *
 * @author manderson
 */
@NonPersistentObject
public class BusinessClientCompanyRelationshipExtended extends BusinessCompanyRelationship {


	/**
	 * If main company type = Client Relationship, then this is the client relationship for the main company
	 * Else if related company type = Client Relationship, then this is the client relationship for the related company
	 * Else - it maps from the client
	 */
	private BusinessClientRelationship clientRelationship;

	/**
	 * If main company type IN any of the company types from the BusinessClientCategory table (Client, Client Group, Sister Client) then this is the client for the main company
	 * Else if related company type IN any of the company types from the BusinessClientCategory table (Client, Client Group, Sister Client) then this is the client for the related company
	 */
	private BusinessClient client;


	/**
	 * If the clientRelationship or client is mapped from the main company, this this is the related company
	 * Else, it's the main company
	 */
	private BusinessCompany relatedCompany;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessClientRelationship getClientRelationship() {
		return this.clientRelationship;
	}


	public void setClientRelationship(BusinessClientRelationship clientRelationship) {
		this.clientRelationship = clientRelationship;
	}


	public BusinessClient getClient() {
		return this.client;
	}


	public void setClient(BusinessClient client) {
		this.client = client;
	}


	public BusinessCompany getRelatedCompany() {
		return this.relatedCompany;
	}


	public void setRelatedCompany(BusinessCompany relatedCompany) {
		this.relatedCompany = relatedCompany;
	}
}
