package com.clifton.business.client.cache;


import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessClientRelationshipCompanyCache</code> class provides caching and retrieval from cache of
 * BusinessClientRelationship objects by CompanyId
 *
 * @author manderson
 */
@Component
public class BusinessClientRelationshipCompanyCache extends SelfRegisteringSingleKeyDaoCache<BusinessClientRelationship, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "company.id";
	}


	@Override
	protected Integer getBeanKeyValue(BusinessClientRelationship bean) {
		if (bean.getCompany() != null) {
			return bean.getCompany().getId();
		}
		return null;
	}
}
