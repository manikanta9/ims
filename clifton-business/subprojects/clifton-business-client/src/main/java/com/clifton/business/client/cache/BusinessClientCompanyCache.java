package com.clifton.business.client.cache;


import com.clifton.business.client.BusinessClient;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessClientCompanyCache</code> class provides caching and retrieval from cache of
 * BusinessClient objects by CompanyId
 *
 * @author manderson
 */
@Component
public class BusinessClientCompanyCache extends SelfRegisteringSingleKeyDaoCache<BusinessClient, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "company.id";
	}


	@Override
	protected Integer getBeanKeyValue(BusinessClient bean) {
		if (bean.getCompany() != null) {
			return bean.getCompany().getId();
		}
		return null;
	}
}
