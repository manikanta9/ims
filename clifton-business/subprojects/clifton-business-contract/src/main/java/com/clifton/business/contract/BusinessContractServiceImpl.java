package com.clifton.business.contract;


import com.clifton.business.client.observers.BusinessClientObserver;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.observers.BusinessContractPartyValidator;
import com.clifton.business.contract.search.BusinessContractClauseExtendedSearchForm;
import com.clifton.business.contract.search.BusinessContractExtendedSearchForm;
import com.clifton.business.contract.search.BusinessContractPartyRoleSearchForm;
import com.clifton.business.contract.search.BusinessContractPartySearchForm;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.business.contract.search.BusinessContractSearchFormConfigurer;
import com.clifton.business.contract.search.BusinessContractTemplateSearchForm;
import com.clifton.business.contract.search.BusinessContractTypeSearchForm;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnGroup;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowStatus;
import org.hibernate.Criteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class BusinessContractServiceImpl implements BusinessContractService {

	private AdvancedUpdatableDAO<BusinessContractType, Criteria> businessContractTypeDAO;
	private AdvancedUpdatableDAO<BusinessContractTemplate, Criteria> businessContractTemplateDAO;

	private AdvancedUpdatableDAO<BusinessContractPartyRole, Criteria> businessContractPartyRoleDAO;

	private AdvancedUpdatableDAO<BusinessContract, Criteria> businessContractDAO;
	private AdvancedReadOnlyDAO<BusinessContractExtended, Criteria> businessContractExtendedDAO;
	private UpdatableDAO<BusinessContractLink> businessContractLinkDAO;

	private AdvancedUpdatableDAO<BusinessContractParty, Criteria> businessContractPartyDAO;
	private AdvancedReadOnlyDAO<BusinessContractClauseExtended, Criteria> businessContractClauseExtendedDAO;

	private DocumentManagementService documentManagementService;
	private SystemColumnService systemColumnService;
	private WorkflowDefinitionService workflowDefinitionService;

	private SystemColumnValueHandler systemColumnValueHandler;
	private DaoNamedEntityCache<BusinessContractPartyRole> businessContractPartyRoleCache;


	////////////////////////////////////////////////////////////////////////////
	////////            Business Contract Type Methods                 /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContractType getBusinessContractType(short id) {
		return getBusinessContractTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessContractType getBusinessContractTypeByName(String name) {
		return getBusinessContractTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<BusinessContractType> getBusinessContractTypeList(BusinessContractTypeSearchForm searchForm) {
		return getBusinessContractTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContractType saveBusinessContractType(BusinessContractType bean) {
		return getBusinessContractTypeDAO().save(bean);
	}


	@Override
	@Transactional
	public void copyBusinessContractType(short id, String name) {
		BusinessContractType type = getBusinessContractType(id);
		BusinessContractType newType = BeanUtils.cloneBean(type, false, false);
		newType.setName(name);
		// Save here so we have the id for the column's linked value
		saveBusinessContractType(newType);
		getSystemColumnService().copySystemColumnCustomListForLinkedValue(BusinessContract.CONTRACT_CLAUSE_COLUMN_GROUP_NAME, Short.toString(id), "" + newType.getId(), newType.getName());
	}


	@Override
	public void deleteBusinessContractType(short id) {
		getBusinessContractTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////            Business Contract Template Methods                 ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContractTemplate getBusinessContractTemplate(int id) {
		return getBusinessContractTemplateDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContractTemplate> getBusinessContractTemplateList(BusinessContractTemplateSearchForm searchForm) {
		return getBusinessContractTemplateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContractTemplate saveBusinessContractTemplate(BusinessContractTemplate bean) {
		return getBusinessContractTemplateDAO().save(bean);
	}


	@Override
	public void deleteBusinessContractTemplate(int id) {
		getBusinessContractTemplateDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Business Contract Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContract getBusinessContract(int id) {
		return getBusinessContractDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContract> getBusinessContractListByParent(final Integer parentId, final BusinessContractSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfig = new BusinessContractSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				if (parentId == null) {
					criteria.add(Restrictions.isNull("parent.id"));
				}
				else {
					criteria.add(Restrictions.eq("parent.id", parentId));
				}
				super.configureCriteria(criteria);
			}
		};
		return getBusinessContractDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public List<BusinessContract> getBusinessContractList(final BusinessContractSearchForm searchForm) {
		return getBusinessContractDAO().findBySearchCriteria(new BusinessContractSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	// see {@link BusinessContractValidator} for validation details
	@Override
	public BusinessContract saveBusinessContract(BusinessContract bean) {
		return getBusinessContractDAO().save(bean);
	}


	@Override
	public BusinessContract saveBusinessContractWithFile(BusinessContract bean, MultipartFile file) {
		bean = saveBusinessContract(bean);
		if (file != null) {
			DocumentRecord record = new DocumentRecord();
			record.setFile(file);
			record.setTableName(BusinessContract.CONTRACT_TABLE_NAME);
			record.setFkFieldId(bean.getId());
			getDocumentManagementService().uploadDocumentRecord(record);
		}
		return bean;
	}


	/**
	 * Called from {@link BusinessClientObserver} when renaming a Client - replaces the old Client name in the Contract name with the new client name
	 *
	 * @param beanList
	 */
	@Override
	public void saveBusinessContractList(List<BusinessContract> beanList) {
		getBusinessContractDAO().saveList(beanList);
	}


	@Override
	public BusinessContract copyBusinessContract(int id, Integer templateId, String name, boolean copyContractClauses, boolean copyContractDocument, boolean copyContractParties) {
		BusinessContract contract = getBusinessContract(id);
		if (contract.getParent() != null) {
			throw new ValidationException("Revisions can only be created for Contracts that have not yet been revised by another contract.  This contract has already been revised by ["
					+ contract.getParent().getLabel() + "]");
		}

		BusinessContractTemplate template = contract.getContractTemplate();
		// Only override the template, if it is set
		if (templateId != null) {
			template = getBusinessContractTemplate(templateId);
		}

		BusinessContract newContract = new BusinessContract();
		newContract.setName(name);
		newContract.setCompany(contract.getCompany());
		newContract.setContractType(contract.getContractType());

		if (contract.getContractType().isCopyContractDates()) {
			newContract.setEffectiveDate(contract.getEffectiveDate());
			newContract.setSignedDate(contract.getSignedDate());
		}

		// If copying it from the contract, need to first create the record, then copy the document, and then set the template
		// Otherwise the template will be automatically used as the initial document.
		if (!copyContractDocument) {
			newContract.setContractTemplate(template);
		}
		saveBusinessContract(newContract);

		contract.setParent(newContract);
		saveBusinessContract(contract);

		if (copyContractClauses) {
			contract.setColumnGroupName(BusinessContract.CONTRACT_CLAUSE_COLUMN_GROUP_NAME);
			getSystemColumnValueHandler().copySystemColumnValueList(contract, newContract, false);
		}

		if (copyContractParties) {
			List<BusinessContractParty> partyList = getBusinessContractPartyListByContract(id);
			if (!CollectionUtils.isEmpty(partyList)) {
				List<BusinessContractParty> newPartyList = new ArrayList<>();
				for (BusinessContractParty party : partyList) {
					BusinessContractParty newParty = BeanUtils.cloneBean(party, false, false);
					newParty.setContract(newContract);
					newPartyList.add(newParty);
				}
				saveBusinessContractPartyList(newPartyList);
			}
		}

		// Copy the document (if it exists)
		if (copyContractDocument) {
			getDocumentManagementService().copyDocumentRecord("BusinessContract", id, "BusinessContract", newContract.getId());

			if (template != null) {
				// If didn't set the template previously, set it now
				newContract.setContractTemplate(template);
				saveBusinessContract(newContract);
			}
		}
		return newContract;
	}


	@Override
	@Transactional
	public void deleteBusinessContract(int id) {
		// First check if this contract was a revision, remove the link from the child so can delete
		// (Workflow will automatically push the contract back to Published when Published Revision Pending)
		List<BusinessContract> childList = getBusinessContractDAO().findByField("parent.id", id);
		if (!CollectionUtils.isEmpty(childList)) {
			for (BusinessContract contract : childList) {
				contract.setParent(null);
				saveBusinessContract(contract);
			}
		}

		// Delete all Parties
		getBusinessContractPartyDAO().deleteList(getBusinessContractPartyListByContract(id));

		// Delete all Links
		getBusinessContractLinkDAO().deleteList(getBusinessContractLinkListByParent(id));
		getBusinessContractLinkDAO().deleteList(getBusinessContractLinkListByChild(id));

		// Then delete the contract
		getBusinessContractDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////        Business Contract Clause Copy Methods                    //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void copyBusinessContractSystemColumnValueList(int copyFromId, int id, String columnGroupName) {
		BusinessContract copyFrom = getBusinessContract(copyFromId);
		BusinessContract copyTo = getBusinessContract(id);

		// Verify both contracts are of the same type (i.e. linked value), and not the same bean
		ValidationUtils.assertNotNull(copyTo, "Missing Contract to copy values to.");
		ValidationUtils.assertNotNull(copyFrom, "Missing Contract to copy values from.");
		ValidationUtils.assertFalse(copyTo.equals(copyFrom), "Contract to copy from/to is the same.");
		ValidationUtils.assertNotNull(columnGroupName, "Missing column group name to determine which values to copy.");

		SystemColumnGroup group = getSystemColumnService().getSystemColumnGroupByName(columnGroupName);
		copyFrom.setColumnGroupName(columnGroupName);

		if (!StringUtils.isEmpty(group.getLinkedBeanProperty())) {
			Object copyToLinkedValue = BeanUtils.getPropertyValue(copyTo, group.getLinkedBeanProperty());
			Object copyFromLinkedValue = BeanUtils.getPropertyValue(copyFrom, group.getLinkedBeanProperty());
			if (!CompareUtils.isEqual(copyToLinkedValue, copyFromLinkedValue)) {
				Map<SystemColumnCustom, SystemColumnCustom> customColumnIdMap = getCrossContractTypeSystemColumnCustomMap(group.getId(), copyFrom, copyTo);
				//At this point it is safe to set the columnGroupName given we used it in conjunction with the entity to get all the columns and then verified them.
				copyTo.setColumnGroupName(columnGroupName);
				copyBusinessContractSystemColumnValueListCrossType(customColumnIdMap, copyFrom, copyTo);
			}
			else {
				getSystemColumnValueHandler().copySystemColumnValueList(copyFrom, copyTo, true);
			}
		}
	}


	@Transactional
	protected void copyBusinessContractSystemColumnValueListCrossType(Map<SystemColumnCustom, SystemColumnCustom> fromCustomColumnMap, BusinessContract fromContract, BusinessContract toContract) {
		getSystemColumnValueHandler().deleteSystemColumnValueListForEntity(toContract);
		List<SystemColumnValue> valueList = getSystemColumnValueHandler().getSystemColumnValueListForEntity(fromContract);
		List<SystemColumnValue> newValueList = new ArrayList<>();
		for (SystemColumnValue v : CollectionUtils.getIterable(valueList)) {
			SystemColumnValue nv = BeanUtils.cloneBean(v, false, false);
			nv.setFkFieldId(toContract.getId());
			nv.setColumn(fromCustomColumnMap.get(v.getColumn()));
			newValueList.add(nv);
		}
		toContract.setColumnValueList(newValueList);
		getSystemColumnValueHandler().saveSystemColumnValueListForEntity(toContract);
	}


	private Map<SystemColumnCustom, SystemColumnCustom> getCrossContractTypeSystemColumnCustomMap(short groupId, BusinessContract copyFrom, BusinessContract copyTo) {
		Map<SystemColumnCustom, SystemColumnCustom> customColumnIdMap = new HashMap<>();
		List<SystemColumnCustom> fromColumnList = getSystemColumnService().getSystemColumnCustomListForColumnGroupAndEntityId(groupId, copyFrom.getId(), CustomColumnTypes.COLUMN_VALUE);
		List<SystemColumnCustom> toColumnList = getSystemColumnService().getSystemColumnCustomListForColumnGroupAndEntityId(groupId, copyTo.getId(), CustomColumnTypes.COLUMN_VALUE);
		ValidationUtils.assertEquals(fromColumnList.size(), toColumnList.size(), "Cannot copy values from/to selected contracts because they do not use the same columns: Column number mismatch.");
		CollectionUtils.sort(fromColumnList, Comparator.comparing(NamedEntityWithoutLabel::getName));
		CollectionUtils.sort(toColumnList, Comparator.comparing(NamedEntityWithoutLabel::getName));
		for (SystemColumnCustom fromColumn : CollectionUtils.getIterable(fromColumnList)) {
			SystemColumnCustom toColumn = toColumnList.get(fromColumnList.indexOf(fromColumn));
			List<String> difList = CoreCompareUtils.getNoEqualPropertiesWithSetters(fromColumn, toColumn, false, "linkedValue", "linkedLabel", "description", "userInterfaceConfig", "customLabel", "order", "id", "labelOverride", "dependedColumn", "globalDefaultText", "defaultText", "gridFilterUserInterfaceConfig");
			ValidationUtils.assertEmpty(difList, "Cannot copy values from " + fromColumn.getName() + " to " + toColumn.getName() + ". The following fields do not match: " + StringUtils.collectionToCommaDelimitedString(difList));
			customColumnIdMap.put(fromColumn, toColumn);
		}
		return customColumnIdMap;
	}


	////////////////////////////////////////////////////////////////////////////
	//////         Business Contract Extended Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessContractExtended> getBusinessContractExtendedList(final BusinessContractExtendedSearchForm searchForm) {
		BusinessContractSearchFormConfigurer searchConfigurer = new BusinessContractSearchFormConfigurer(searchForm, getWorkflowDefinitionService()) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// also apply custom filters, if set for extended properties
				if (searchForm.getClientActive() != null) {
					String alias = getPathAlias("client", criteria, JoinType.LEFT_OUTER_JOIN);
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(searchForm.getClientActive(), alias, "inceptionDate", "terminateDate", null));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (!CollectionUtils.isEmpty(orderByList)) {
					return super.configureOrderBy(criteria);
				}
				// (used for groupings) - Only if Not Explicit: First Order by Category, Main Contract Name, Type, then other fields
				String typePath = getPathAlias("contractType", criteria);
				String mainPath = getPathAlias("mainContract", criteria);
				String mainTypePath = getPathAlias("mainContract.contractType", criteria);
				criteria.addOrder(Order.asc(typePath + ".categoryName"));
				criteria.addOrder(Order.asc(mainTypePath + ".name"));
				criteria.addOrder(Order.asc(mainPath + ".name"));
				return true;
			}
		};
		return getBusinessContractExtendedDAO().findBySearchCriteria(searchConfigurer);
	}


	////////////////////////////////////////////////////////////////////////////
	///////          Business Contract Link Methods                      ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessContractLink> getBusinessContractLinkListByParent(int parentId) {
		return getBusinessContractLinkDAO().findByField("referenceOne.id", parentId);
	}


	@Override
	public List<BusinessContractLink> getBusinessContractLinkListByChild(int childId) {
		return getBusinessContractLinkDAO().findByField("referenceTwo.id", childId);
	}


	@Override
	public BusinessContractLink saveBusinessContractLink(int parentId, int childId) {
		BusinessContract parent = getBusinessContract(parentId);
		BusinessContract child = getBusinessContract(childId);

		ValidationUtils.assertFalse(parent.equals(child), "You cannot link a contract to itself.");
		ValidationUtils.assertFalse(parent.getContractType().isAmendment(), "Parent contracts cannot be of a contract type that is flagged as an amendment");
		ValidationUtils.assertTrue(child.getContractType().isAmendment(), "Child contracts must be of a contract type that is flagged as an amendment");

		BusinessContractLink link = new BusinessContractLink();
		link.setReferenceOne(parent);
		link.setReferenceTwo(child);
		return getBusinessContractLinkDAO().save(link);
	}


	@Override
	@Transactional
	public List<BusinessContractLink> saveBusinessContractParentLinks(int[] parentIds, int childId) {
		List<BusinessContractLink> results = new ArrayList<>();
		if (parentIds != null && parentIds.length > 0) {
			for (Integer parent : parentIds) {
				BusinessContractLink businessContractLink = saveBusinessContractLink(parent, childId);
				results.add(businessContractLink);
			}
		}
		return results;
	}


	@Override
	@Transactional
	public List<BusinessContractLink> saveBusinessContractChildLinks(int parentId, int[] childIds) {
		List<BusinessContractLink> results = new ArrayList<>();
		if (childIds != null && childIds.length > 0) {
			for (Integer child : childIds) {
				BusinessContractLink businessContractLink = saveBusinessContractLink(parentId, child);
				results.add(businessContractLink);
			}
		}
		return results;
	}


	@Override
	public void deleteBusinessContractLink(int id) {
		getBusinessContractLinkDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////        Business Contract Clause Extended Methods               //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessContractClauseExtended> getBusinessContractClauseExtendedList(final BusinessContractClauseExtendedSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// also apply custom filters, if set
				if (searchForm.getClientActive() != null) {
					Date today = DateUtils.clearTime(new Date());
					String alias = getPathAlias("client", criteria);
					if (searchForm.getClientActive()) {
						LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull(alias + ".inceptionDate"), Restrictions.le(alias + ".inceptionDate", today));
						LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull(alias + ".terminateDate"), Restrictions.ge(alias + ".terminateDate", today));
						criteria.add(Restrictions.and(startDateFilter, endDateFilter));
					}
					else if (!searchForm.getClientActive()) {
						SimpleExpression startDateFilter = Restrictions.ge(alias + ".inceptionDate", today);
						SimpleExpression endDateFilter = Restrictions.le(alias + ".terminateDate", today);
						criteria.add(Restrictions.or(startDateFilter, endDateFilter));
					}
				}

				if (searchForm.getActive() != null) {
					if (searchForm.getActive()) {
						criteria.add(Restrictions.ne(getPathAlias("contract.workflowStatus", criteria) + ".name", WorkflowStatus.STATUS_NON_OPERATIONAL));
					}
					else {
						criteria.add(Restrictions.eq(getPathAlias("contract.workflowStatus", criteria) + ".name", WorkflowStatus.STATUS_NON_OPERATIONAL));
					}
				}

				if (searchForm.getValueOrText() != null) {
					LogicalExpression r1 = Restrictions.or(Restrictions.ilike(getPathAlias("value", criteria) + ".value", searchForm.getValueOrText(), MatchMode.ANYWHERE),
							Restrictions.ilike(getPathAlias("value", criteria) + ".text", searchForm.getValueOrText(), MatchMode.ANYWHERE));
					String otherValueOrText = null;
					if ("true".equalsIgnoreCase(searchForm.getValueOrText())) {
						otherValueOrText = "Yes";
					}
					else if ("Yes".equalsIgnoreCase(searchForm.getValueOrText())) {
						otherValueOrText = "true";
					}
					else if ("false".equalsIgnoreCase(searchForm.getValueOrText())) {
						otherValueOrText = "No";
					}
					else if ("No".equalsIgnoreCase(searchForm.getValueOrText())) {
						otherValueOrText = "false";
					}

					if (otherValueOrText != null) {
						LogicalExpression r2 = Restrictions.or(Restrictions.ilike(getPathAlias("value", criteria) + ".value", otherValueOrText, MatchMode.ANYWHERE),
								Restrictions.ilike(getPathAlias("value", criteria) + ".text", otherValueOrText, MatchMode.ANYWHERE));
						criteria.add(Restrictions.or(r1, r2));
					}
					else {
						criteria.add(r1);
					}
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// Special support for ValueOrText sorting
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					return false;
				}
				for (OrderByField field : orderByList) {
					String name = getOrderByFieldName(field, criteria);
					if ("valueOrText".equals(name)) {
						name = getPathAlias("value", criteria) + ".text";
					}
					else {
						name = getOrderByFieldName(field, criteria);
					}
					boolean asc = OrderByDirections.ASC == field.getDirection();
					criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
				}
				return true;
			}
		};

		return getBusinessContractClauseExtendedDAO().findBySearchCriteria(searchConfigurer);
	}


	////////////////////////////////////////////////////////////////////////////
	///////          Business Contract Party Type Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContractPartyRole getBusinessContractPartyRole(short id) {
		return getBusinessContractPartyRoleDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessContractPartyRole getBusinessContractPartyRoleByName(String name) {
		return getBusinessContractPartyRoleCache().getBeanForKeyValueStrict(getBusinessContractPartyRoleDAO(), name);
	}


	@Override
	public List<BusinessContractPartyRole> getBusinessContractPartyRoleList(BusinessContractPartyRoleSearchForm searchForm) {
		return getBusinessContractPartyRoleDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContractPartyRole saveBusinessContractPartyRole(BusinessContractPartyRole bean) {
		return getBusinessContractPartyRoleDAO().save(bean);
	}


	@Override
	public void deleteBusinessContractPartyRole(short id) {
		getBusinessContractPartyRoleDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////            Business Contract Party Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContractParty getBusinessContractParty(int id) {
		return getBusinessContractPartyDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContractParty> getBusinessContractPartyList(BusinessContractPartySearchForm searchForm) {
		return getBusinessContractPartyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<BusinessContractParty> getBusinessContractPartyListByContract(int contractId) {
		return getBusinessContractPartyDAO().findByField("contract.id", contractId);
	}


	@Override
	public List<BusinessContractParty> getBusinessContractPartyListByContractAndRole(int contractId, String... roleNames) {
		BusinessContractPartySearchForm searchForm = new BusinessContractPartySearchForm();
		searchForm.setContractId(contractId);
		Short[] roleIds = ArrayUtils.getStream(roleNames)
				.map(this::getBusinessContractPartyRoleByName)
				.map(BaseSimpleEntity::getId)
				.toArray(Short[]::new);
		searchForm.setRoleIds(roleIds);
		return getBusinessContractPartyList(searchForm);
	}


	/**
	 * see {@link BusinessContractPartyValidator} for validation details
	 */
	@Override
	public BusinessContractParty saveBusinessContractParty(BusinessContractParty bean) {
		return getBusinessContractPartyDAO().save(bean);
	}


	private void saveBusinessContractPartyList(List<BusinessContractParty> beanList) {
		getBusinessContractPartyDAO().saveList(beanList);
	}


	@Override
	public void deleteBusinessContractParty(int id) {
		getBusinessContractPartyDAO().delete(id);
	}


	@Override
	public List<BusinessCompany> getBusinessContractCompanyListByContractCompanyAndPartyRole(int contractCompanyId, short partyRoleId) {
		BusinessContractPartySearchForm searchForm = new BusinessContractPartySearchForm();
		searchForm.setRoleId(partyRoleId);
		searchForm.setContractCompanyId(contractCompanyId);
		List<BusinessCompany> companyList = CollectionUtils.toArrayList(CollectionUtils.getStream(getBusinessContractPartyList(searchForm)).map(BusinessContractParty::getCompany).collect(Collectors.toSet()));
		CollectionUtils.sort(companyList, Comparator.comparing(NamedEntityWithoutLabel::getName));
		return companyList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessContractType, Criteria> getBusinessContractTypeDAO() {
		return this.businessContractTypeDAO;
	}


	public void setBusinessContractTypeDAO(AdvancedUpdatableDAO<BusinessContractType, Criteria> businessContractTypeDAO) {
		this.businessContractTypeDAO = businessContractTypeDAO;
	}


	public AdvancedUpdatableDAO<BusinessContractPartyRole, Criteria> getBusinessContractPartyRoleDAO() {
		return this.businessContractPartyRoleDAO;
	}


	public void setBusinessContractPartyRoleDAO(AdvancedUpdatableDAO<BusinessContractPartyRole, Criteria> businessContractPartyRoleDAO) {
		this.businessContractPartyRoleDAO = businessContractPartyRoleDAO;
	}


	public AdvancedUpdatableDAO<BusinessContract, Criteria> getBusinessContractDAO() {
		return this.businessContractDAO;
	}


	public void setBusinessContractDAO(AdvancedUpdatableDAO<BusinessContract, Criteria> businessContractDAO) {
		this.businessContractDAO = businessContractDAO;
	}


	public AdvancedUpdatableDAO<BusinessContractParty, Criteria> getBusinessContractPartyDAO() {
		return this.businessContractPartyDAO;
	}


	public void setBusinessContractPartyDAO(AdvancedUpdatableDAO<BusinessContractParty, Criteria> businessContractPartyDAO) {
		this.businessContractPartyDAO = businessContractPartyDAO;
	}


	public AdvancedUpdatableDAO<BusinessContractTemplate, Criteria> getBusinessContractTemplateDAO() {
		return this.businessContractTemplateDAO;
	}


	public void setBusinessContractTemplateDAO(AdvancedUpdatableDAO<BusinessContractTemplate, Criteria> businessContractTemplateDAO) {
		this.businessContractTemplateDAO = businessContractTemplateDAO;
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public UpdatableDAO<BusinessContractLink> getBusinessContractLinkDAO() {
		return this.businessContractLinkDAO;
	}


	public void setBusinessContractLinkDAO(UpdatableDAO<BusinessContractLink> businessContractLinkDAO) {
		this.businessContractLinkDAO = businessContractLinkDAO;
	}


	public AdvancedReadOnlyDAO<BusinessContractExtended, Criteria> getBusinessContractExtendedDAO() {
		return this.businessContractExtendedDAO;
	}


	public void setBusinessContractExtendedDAO(AdvancedReadOnlyDAO<BusinessContractExtended, Criteria> businessContractExtendedDAO) {
		this.businessContractExtendedDAO = businessContractExtendedDAO;
	}


	public AdvancedReadOnlyDAO<BusinessContractClauseExtended, Criteria> getBusinessContractClauseExtendedDAO() {
		return this.businessContractClauseExtendedDAO;
	}


	public void setBusinessContractClauseExtendedDAO(AdvancedReadOnlyDAO<BusinessContractClauseExtended, Criteria> businessContractClauseExtendedDAO) {
		this.businessContractClauseExtendedDAO = businessContractClauseExtendedDAO;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public DaoNamedEntityCache<BusinessContractPartyRole> getBusinessContractPartyRoleCache() {
		return this.businessContractPartyRoleCache;
	}


	public void setBusinessContractPartyRoleCache(DaoNamedEntityCache<BusinessContractPartyRole> businessContractPartyRoleCache) {
		this.businessContractPartyRoleCache = businessContractPartyRoleCache;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
