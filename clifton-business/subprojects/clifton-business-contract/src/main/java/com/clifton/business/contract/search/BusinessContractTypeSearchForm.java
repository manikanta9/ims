package com.clifton.business.contract.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BusinessContractTypeSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessContractTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Boolean copyContractDates;

	@SearchField
	private Boolean amendment;

	@SearchField
	private Boolean restricted;

	@SearchField(searchField = "name", searchFieldPath = "additionalSecurityResource")
	private String additionalSecurityResourceName;

	@SearchField
	private String categoryName;


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getAmendment() {
		return this.amendment;
	}


	public void setAmendment(Boolean amendment) {
		this.amendment = amendment;
	}


	public Boolean getCopyContractDates() {
		return this.copyContractDates;
	}


	public void setCopyContractDates(Boolean copyContractDates) {
		this.copyContractDates = copyContractDates;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Boolean getRestricted() {
		return this.restricted;
	}


	public void setRestricted(Boolean restricted) {
		this.restricted = restricted;
	}


	public String getAdditionalSecurityResourceName() {
		return this.additionalSecurityResourceName;
	}


	public void setAdditionalSecurityResourceName(String additionalSecurityResourceName) {
		this.additionalSecurityResourceName = additionalSecurityResourceName;
	}
}
