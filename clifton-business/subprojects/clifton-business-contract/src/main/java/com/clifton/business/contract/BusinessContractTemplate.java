package com.clifton.business.contract;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.security.authorization.SecurityResourceAware;

import java.util.Date;


/**
 * The <code>BusinessContractTemplate</code> ...
 *
 * @author manderson
 */
public class BusinessContractTemplate extends NamedEntity<Integer> implements DocumentWithRecordStampAware, SecurityResourceAware {

	private BusinessContractType contractType;

	private boolean inactive;

	/**
	 * Optional warning message that can be displayed on Contract window for Contracts
	 * in Pending status for this template
	 */
	private String warningMessage;

	/**
	 * Document Properties
	 */
	private String documentFileType;

	/**
	 * Document Record Stamp Properties
	 */
	private Date documentUpdateDate;
	private String documentUpdateUser;


	/////////////////////////////////////////////////////////////////////////
	// Security Resource Aware 


	@Override
	public SecurityResource getSecurityResource() {
		if (getContractType() != null) {
			return getContractType().getAdditionalSecurityResource();
		}
		return null;
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public BusinessContractType getContractType() {
		return this.contractType;
	}


	public void setContractType(BusinessContractType contractType) {
		this.contractType = contractType;
	}


	@Override
	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	@Override
	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	@Override
	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	@Override
	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public boolean isInactive() {
		return this.inactive;
	}


	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}


	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	public String getWarningMessage() {
		return this.warningMessage;
	}


	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}
}
