package com.clifton.business.contract.search;

import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author danielh
 */
@Component
public class BusinessContractForCompanySearchFormConfigurer implements SearchFormCustomConfigurer<CompanySearchForm> {

	@Override
	public Class<CompanySearchForm> getSearchFormInterfaceClassName() {
		return CompanySearchForm.class;
	}


	@Override
	public void configureCriteria(Criteria criteria, CompanySearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {
		// Append Contract Party Restriction if any of the contract specific fields is defined
		if (searchForm.getContractTypeName() != null || searchForm.getContractCategoryName() != null || searchForm.getContractPartyRoleName() != null
				|| searchForm.getContractInvestmentAccountId() != null || searchForm.getContractCompanyId() != null || searchForm.getContractActive() != null) {

			List<Object> contractSqlParams = new ArrayList<>();
			List<Type> contractSqlParamTypes = new ArrayList<>();
			StringBuilder contractSql = new StringBuilder(" {alias}.BusinessCompanyID IN (" + "SELECT b.BusinessCompanyID FROM BusinessCompany b " //
					+ " INNER JOIN BusinessContractParty bcp ON bcp.BusinessCompanyID = b.BusinessCompanyID " //
					+ " INNER JOIN BusinessContract bc ON bc.BusinessContractID = bcp.BusinessContractID ");

			StringBuilder whereSql = new StringBuilder(" WHERE 1=1 ");

			// Active Contracts or Inactive Contracts Only (i.e. Active = WorkflowStatusName != Non-Operational)
			if (searchForm.getContractActive() != null) {
				contractSql.append(" INNER JOIN WorkflowStatus ws ON bc.WorkflowStatusID = ws.WorkflowStatusID ");
				whereSql.append(" AND ws.WorkflowStatusName ").append(searchForm.getContractActive() ? " <> " : " = ").append(" 'Non-Operational' ");
			}

			// Contract Type or Contract Type Category Filter
			if (searchForm.getContractTypeName() != null || searchForm.getContractCategoryName() != null) {
				contractSql.append(" INNER JOIN BusinessContractType bct ON bct.BusinessContractTypeID = bc.BusinessContractTypeID ");
				if (searchForm.getContractTypeName() != null) {
					contractSql.append(" AND bct.ContractTypeName = ? ");
					contractSqlParams.add(searchForm.getContractTypeName());
					contractSqlParamTypes.add(StringType.INSTANCE);
				}
				if (searchForm.getContractCategoryName() != null) {
					contractSql.append(" AND bct.ContractTypeCategoryName = ? ");
					contractSqlParams.add(searchForm.getContractCategoryName());
					contractSqlParamTypes.add(StringType.INSTANCE);
				}
			}

			// Contract Party Role
			if (searchForm.getContractPartyRoleName() != null) {
				contractSql.append(" INNER JOIN BusinessContractPartyRole cpr ON cpr.BusinessContractPartyRoleID = bcp.BusinessContractPartyRoleID ");
				whereSql.append(" AND cpr.ContractPartyRoleName = ? ");
				contractSqlParams.add(searchForm.getContractPartyRoleName());
				contractSqlParamTypes.add(StringType.INSTANCE);
			}

			// Tied to a specific Client Company or Account - only need to filter on one or the other
			if (searchForm.getContractCompanyId() != null) {
				whereSql.append(" AND bc.BusinessCompanyID = ? ");
				contractSqlParams.add(searchForm.getContractCompanyId());
				contractSqlParamTypes.add(IntegerType.INSTANCE);
			}
			else if (searchForm.getContractInvestmentAccountId() != null) {
				contractSql.append(" INNER JOIN BusinessClient bcl ON bcl.BusinessCompanyID = bc.BusinessCompanyID " //
						+ " INNER JOIN InvestmentAccount ia ON ia.BusinessClientID = bcl.BusinessClientID ");
				whereSql.append(" AND ia.InvestmentAccountID = ? ");
				contractSqlParams.add(searchForm.getContractInvestmentAccountId());
				contractSqlParamTypes.add(IntegerType.INSTANCE);
			}

			// Append Where & Group By
			contractSql.append(whereSql);
			contractSql.append(" GROUP BY b.BusinessCompanyID) ");
			criteria.add(Restrictions.sqlRestriction(contractSql.toString(), contractSqlParams.toArray(), contractSqlParamTypes.toArray(new Type[0])));
		}
	}
}
