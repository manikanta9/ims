package com.clifton.business.contract;


import com.clifton.business.client.BusinessClient;
import com.clifton.document.setup.DocumentSecurityRestriction;


/**
 * The <code>BusinessContractExtended</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessContractExtended extends BusinessContract {

	private boolean amendmentExists;

	// Contracts are tied to Companies, however for extended searching/display
	// want to filter/view client specific information for Client Contracts
	// So this field is exposed when company is a client type.
	private BusinessClient client;

	/**
	 * If is an amendment that has been linked - returns the first main contract for that amendment
	 * otherwise returns it's own name
	 */
	private BusinessContract mainContract;

	/**
	 * If there exists any entries in the {@link DocumentSecurityRestriction} table
	 * for this contract
	 */
	private boolean documentRestrictionsExist;


	public boolean isAmendment() {
		if (getMainContract() != null) {
			return !getId().equals(getMainContract().getId());
		}
		return false;
	}


	public boolean isAmendmentExists() {
		return this.amendmentExists;
	}


	public void setAmendmentExists(boolean amendmentExists) {
		this.amendmentExists = amendmentExists;
	}


	public BusinessClient getClient() {
		return this.client;
	}


	public void setClient(BusinessClient client) {
		this.client = client;
	}


	public BusinessContract getMainContract() {
		return this.mainContract;
	}


	public void setMainContract(BusinessContract mainContract) {
		this.mainContract = mainContract;
	}


	public boolean isDocumentRestrictionsExist() {
		return this.documentRestrictionsExist;
	}


	public void setDocumentRestrictionsExist(boolean documentRestrictionsExist) {
		this.documentRestrictionsExist = documentRestrictionsExist;
	}
}
