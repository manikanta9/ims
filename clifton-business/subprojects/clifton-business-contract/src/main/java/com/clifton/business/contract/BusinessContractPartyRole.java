package com.clifton.business.contract;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>BusinessContractPartyRole</code> defines types for Contract Parties.
 * Examples: Sub-Advisor, Investment Manager, Trust, Client
 *
 * @author akorver
 */
@CacheByName
public class BusinessContractPartyRole extends NamedEntity<Short> {

	// Party Role Name Constants
	public static final String PARTY_ROLE_COUNTERPARTY_NAME = "Counterparty";
	public static final String PARTY_ROLE_CREDIT_SUPPORT_PROVIDER_NAME = "Credit Support Provider";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Additional security condition that when populated must evaluate to true in order for a non admin user to be able to edit {@link BusinessContractParty} for this role.
	 */
	private SystemCondition entityModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
