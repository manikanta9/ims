package com.clifton.business.contract.search;


import com.clifton.business.contract.BusinessContractParty;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;


public class BusinessContractSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {


	public BusinessContractSearchFormConfigurer(BusinessContractSearchForm searchForm, WorkflowDefinitionService workflowDefinitionService) {
		super(searchForm, workflowDefinitionService);
	}


	@Override
	public void configureSearchForm() {
		BusinessContractSearchForm searchForm = (BusinessContractSearchForm) getSortableSearchForm();
		Boolean active = searchForm.getActive();
		if (active == null && searchForm.containsSearchRestriction("active")) {
			String activeString = (String) searchForm.getSearchRestriction("active").getValue();
			if (!StringUtils.isEmpty(activeString)) {
				active = Boolean.parseBoolean(activeString);
			}
		}
		if (active != null) {
			if (Boolean.TRUE.equals(active)) {
				searchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_NON_OPERATIONAL);
			}
			else {
				searchForm.setWorkflowStatusNameEquals(WorkflowStatus.STATUS_NON_OPERATIONAL);
			}
		}
		super.configureSearchForm();
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		BusinessContractSearchForm searchForm = (BusinessContractSearchForm) getSortableSearchForm();


		if (searchForm.getPartyCompanyId() != null) {
			ValidationUtils.assertNotNull(searchForm.getPartyRoleId(), "partyCompanyId filter must be used with partyRoleId", "partyRoleId");
			DetachedCriteria sub = DetachedCriteria.forClass(BusinessContractParty.class, "link");
			sub.setProjection(Projections.property("id"));
			sub.add(Restrictions.eqProperty("link.contract.id", criteria.getAlias() + ".id"));
			sub.add(Restrictions.eq("link.company.id", searchForm.getPartyCompanyId()));
			sub.add(Restrictions.eq("link.role.id", searchForm.getPartyRoleId()));
			criteria.add(Subqueries.exists(sub));
		}
		else {
			ValidationUtils.assertNull(searchForm.getPartyRoleId(), "partyRoleId filter must be used with partyCompanyId", "partyRoleId");
		}
	}
}
