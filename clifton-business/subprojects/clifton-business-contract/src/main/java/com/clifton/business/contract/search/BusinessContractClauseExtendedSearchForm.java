package com.clifton.business.contract.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


public class BusinessContractClauseExtendedSearchForm extends BaseEntitySearchForm {

	@SearchField(searchFieldPath = "column", searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "contract.id")
	private Integer contractId;

	@SearchField(searchFieldPath = "contract.company", searchField = "name")
	private String companyName;

	@SearchField(searchFieldPath = "client.clientRelationship", searchField = "name")
	private String clientRelationshipName;

	@SearchField(searchFieldPath = "client.clientType", searchField = "text", leftJoin = true)
	private String clientTypeName;

	// Custom Search Filter
	private Boolean clientActive;

	@SearchField(searchFieldPath = "contract", searchField = "name")
	private String contractName;

	@SearchField(searchFieldPath = "contract", searchField = "contractType.id")
	private Short contractTypeId;

	@SearchField(searchFieldPath = "contract.contractType", searchField = "name")
	private String contractTypeName;

	@SearchField(searchFieldPath = "contract.contractType", searchField = "categoryName")
	private String contractTypeCategory;

	// Custom Search Filter
	private Boolean active;

	@SearchField(searchFieldPath = "column", searchField = "name")
	private String columnName;

	@SearchField(searchFieldPath = "column", searchField = "description")
	private String columnDescription;

	@SearchField(searchField = "column.id")
	private Integer columnId;

	// Custom Search Field so that Yes/No also includes true/false
	private String valueOrText;

	@SearchField
	private Integer order;

	@SearchField(searchField = "value.id", comparisonConditions = {ComparisonConditions.IS_NOT_NULL})
	private Boolean populatedValuesOnly;

	@SearchField(searchFieldPath = "contract", searchField = "workflowState.id")
	private Short workflowStateId;

	@SearchField(searchFieldPath = "contract", searchField = "workflowState.name")
	private String workflowStateName;

	@SearchField(searchFieldPath = "contract", searchField = "workflowStatus.id")
	private Short workflowStatusId;

	@SearchField(searchFieldPath = "contract", searchField = "workflowStatus.name")
	private String workflowStatusName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getContractId() {
		return this.contractId;
	}


	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getClientRelationshipName() {
		return this.clientRelationshipName;
	}


	public void setClientRelationshipName(String clientRelationshipName) {
		this.clientRelationshipName = clientRelationshipName;
	}


	public String getClientTypeName() {
		return this.clientTypeName;
	}


	public void setClientTypeName(String clientTypeName) {
		this.clientTypeName = clientTypeName;
	}


	public Boolean getClientActive() {
		return this.clientActive;
	}


	public void setClientActive(Boolean clientActive) {
		this.clientActive = clientActive;
	}


	public String getContractName() {
		return this.contractName;
	}


	public void setContractName(String contractName) {
		this.contractName = contractName;
	}


	public Short getContractTypeId() {
		return this.contractTypeId;
	}


	public void setContractTypeId(Short contractTypeId) {
		this.contractTypeId = contractTypeId;
	}


	public String getContractTypeName() {
		return this.contractTypeName;
	}


	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}


	public String getContractTypeCategory() {
		return this.contractTypeCategory;
	}


	public void setContractTypeCategory(String contractTypeCategory) {
		this.contractTypeCategory = contractTypeCategory;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public String getColumnName() {
		return this.columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public String getColumnDescription() {
		return this.columnDescription;
	}


	public void setColumnDescription(String columnDescription) {
		this.columnDescription = columnDescription;
	}


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}


	public String getValueOrText() {
		return this.valueOrText;
	}


	public void setValueOrText(String valueOrText) {
		this.valueOrText = valueOrText;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public Boolean getPopulatedValuesOnly() {
		return this.populatedValuesOnly;
	}


	public void setPopulatedValuesOnly(Boolean populatedValuesOnly) {
		this.populatedValuesOnly = populatedValuesOnly;
	}


	public Short getWorkflowStateId() {
		return this.workflowStateId;
	}


	public void setWorkflowStateId(Short workflowStateId) {
		this.workflowStateId = workflowStateId;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}
}
