package com.clifton.business.contract;

/**
 * The <code>BusinessContractTypes</code> includes static content for {@link BusinessContractType}.
 * Examples: IMA, ISDA
 *
 * @author nickk
 */
public enum BusinessContractTypes {

	ISDA("ISDA"),
	ADDENDUM_CLEARED_DERIVATIVES("Addendum-Cleared Derivatives"),
	MASTER_REPURCHASE_AGREEMENT("Master Repurchase Agreement"),
	NFA_REGISTRATION("NFA Registration");

	private final String name;


	BusinessContractTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}
