package com.clifton.business.contract.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>BusinessContractPartySearchForm</code> ...
 *
 * @author manderson
 */
public class BusinessContractPartySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "company.name,contact.lastName,contact.firstName", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "contract.id", sortField = "contract.name")
	private Integer contractId;

	@SearchField(searchFieldPath = "contract", searchField = "name")
	private String contractName;

	@SearchField(searchFieldPath = "contract", searchField = "company.id", sortField = "company.name")
	private Integer contractCompanyId;

	@SearchField(searchFieldPath = "contract.contractType", searchField = "categoryName")
	private String categoryName;

	@SearchField(searchField = "contractType.id", searchFieldPath = "contract", sortField = "contractType.name")
	private Short contractTypeId;

	@SearchField(searchFieldPath = "contract.workflowState", searchField = "name")
	private String workflowStateName;

	@SearchField(searchFieldPath = "contract", searchField = "workflowStatus.id", sortField = "workflowStatus.name")
	private Short workflowStatusId;

	@SearchField(searchFieldPath = "contract", searchField = "effectiveDate")
	private Date effectiveDate;

	@SearchField(searchField = "role.id", sortField = "role.name")
	private Short roleId;

	@SearchField(searchField = "role.id", sortField = "role.name")
	private Short[] roleIds;

	@SearchField(searchFieldPath = "role", searchField = "name")
	private String roleName;

	@SearchField(searchFieldPath = "role", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String roleNameEquals;

	@SearchField(searchField = "company.id", sortField = "company.name")
	private Integer companyId;

	@SearchField(searchFieldPath = "company", searchField = "name")
	private String companyName;

	@SearchField(searchField = "contact.id", sortField = "contact.firstName")
	private Integer contactId;

	@SearchField(searchFieldPath = "contact", searchField = "firstName,lastName")
	private String contactName;

	@SearchField(searchField = "company.name,contact.lastName,contact.firstName", leftJoin = true)
	private String partyName;

	@SearchField
	private String comments;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getContractId() {
		return this.contractId;
	}


	public void setContractId(Integer contractId) {
		this.contractId = contractId;
	}


	public String getContractName() {
		return this.contractName;
	}


	public void setContractName(String contractName) {
		this.contractName = contractName;
	}


	public Integer getContractCompanyId() {
		return this.contractCompanyId;
	}


	public void setContractCompanyId(Integer contractCompanyId) {
		this.contractCompanyId = contractCompanyId;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public Short getContractTypeId() {
		return this.contractTypeId;
	}


	public void setContractTypeId(Short contractTypeId) {
		this.contractTypeId = contractTypeId;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}


	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Short getRoleId() {
		return this.roleId;
	}


	public void setRoleId(Short roleId) {
		this.roleId = roleId;
	}


	public Short[] getRoleIds() {
		return this.roleIds;
	}


	public void setRoleIds(Short[] roleIds) {
		this.roleIds = roleIds;
	}


	public String getRoleName() {
		return this.roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	public String getRoleNameEquals() {
		return this.roleNameEquals;
	}


	public void setRoleNameEquals(String roleNameEquals) {
		this.roleNameEquals = roleNameEquals;
	}


	public Integer getCompanyId() {
		return this.companyId;
	}


	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Integer getContactId() {
		return this.contactId;
	}


	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}


	public String getContactName() {
		return this.contactName;
	}


	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public String getPartyName() {
		return this.partyName;
	}


	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}


	public String getComments() {
		return this.comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}
}
