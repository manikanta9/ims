package com.clifton.business.contract.observers;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BusinessCompanyObserver</code> will update
 * Contract names (for company types that support contracts) when company name changes
 *
 * @author manderson
 */
@Component
public class BusinessContractUpdaterForCompanyObserver extends SelfRegisteringDaoObserver<BusinessCompany> {

	private BusinessContractService businessContractService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<BusinessCompany> dao, DaoEventTypes event, BusinessCompany bean, Throwable e) {
		// Changes to a Client Name - Replace Client names in Contract Names
		if (e == null) {
			BusinessCompany originalBean = getOriginalBean(dao, bean);
			if (originalBean != null) {
				if (bean.getType().isContractAllowed() && !bean.getLabel().equals(originalBean.getLabel())) {
					BusinessContractSearchForm searchForm = new BusinessContractSearchForm();
					searchForm.setCompanyId(bean.getId());
					List<BusinessContract> contractList = getBusinessContractService().getBusinessContractList(searchForm);
					if (!CollectionUtils.isEmpty(contractList)) {
						for (BusinessContract contract : contractList) {
							if (contract.getName() != null) {
								contract.setName(contract.getName().replace(originalBean.getLabel(), bean.getLabel()));
							}
						}
						DaoUtils.executeWithSpecificObserversDisabled(() -> getBusinessContractService().saveBusinessContractList(contractList), SystemEntityModifyConditionAwareObserver.class);
					}
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}
}
