package com.clifton.business.contract.comparisons;


import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.List;


/**
 * The <code>BusinessContractExists</code> evaluates for specified properties if a contract
 * exists in the system. Required is the companyIdBeanPropertyName so that
 * we always know which company (aka Client) we are looking for documents for.
 * <p>
 * Example. In order to publish an ISDA, if Clause Account Control Agreement = Yes, then
 * for that same client/company and ISDA ACA must also exist.
 *
 * @author manderson
 */
public class BusinessContractExistsComparison implements Comparison<IdentityObject> {

	private BusinessContractService businessContractService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Dynamically look for contracts associated with the same client that we are looking for.
	 * Required property - can't think of any cases where we wouldn't want to narrow down to the same client
	 */
	private String companyBeanPropertyName;

	/**
	 * Will include the company's parent's contracts (if a child), or the children company's contracts
	 * CompanyID = ? OR Company's Parent ID = ? OR (LOOKUP Company'S PARENT ID AND IF NOT NULL - THAT VALUE = CompanyID)
	 */
	private boolean includeRelatedCompanies;

	/**
	 * Can be used with partyRoleCompanyBeanPropertyName to find contracts with Counterparty role = Goldman Sachs
	 */
	private Short partyRoleId;

	private String partyRoleCompanyBeanPropertyName;

	private Short contractTypeId;

	private String workflowStatusName;

	private boolean activeOnly;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		BusinessContractSearchForm searchForm = new BusinessContractSearchForm();

		if (!StringUtils.isEmpty(getCompanyBeanPropertyName())) {
			Object companyId = BeanUtils.getPropertyValue(bean, getCompanyBeanPropertyName(), true);
			if (companyId != null) {
				if (isIncludeRelatedCompanies()) {
					searchForm.setCompanyIdOrRelatedCompany((Integer) companyId);
				}
				else {
					searchForm.setCompanyId((Integer) companyId);
				}
			}
		}

		if (!StringUtils.isEmpty(getPartyRoleCompanyBeanPropertyName())) {
			Object partyCompanyId = BeanUtils.getPropertyValue(bean, getCompanyBeanPropertyName(), true);
			if (partyCompanyId != null && getPartyRoleId() != null) {
				searchForm.setPartyRoleId(getPartyRoleId());
				searchForm.setPartyCompanyId((Integer) partyCompanyId);
			}
		}
		searchForm.setContractTypeId(getContractTypeId());
		searchForm.setWorkflowStatusName(getWorkflowStatusName());
		searchForm.setActive(isActiveOnly() ? true : null);

		List<BusinessContract> list = getBusinessContractService().getBusinessContractList(searchForm);
		boolean result = !CollectionUtils.isEmpty(list);
		result = isReverse() ? !result : result;
		StringBuilder message = new StringBuilder("(" + CollectionUtils.getSize(list) + " Contract(s) Found)");
		if (context != null) {
			if (result) {
				context.recordTrueMessage(message.toString());
			}
			else {
				context.recordFalseMessage(message.toString());
			}
		}

		return result;
	}


	protected boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public String getCompanyBeanPropertyName() {
		return this.companyBeanPropertyName;
	}


	public void setCompanyBeanPropertyName(String companyBeanPropertyName) {
		this.companyBeanPropertyName = companyBeanPropertyName;
	}


	public boolean isIncludeRelatedCompanies() {
		return this.includeRelatedCompanies;
	}


	public void setIncludeRelatedCompanies(boolean includeRelatedCompanies) {
		this.includeRelatedCompanies = includeRelatedCompanies;
	}


	public Short getContractTypeId() {
		return this.contractTypeId;
	}


	public void setContractTypeId(Short contractTypeId) {
		this.contractTypeId = contractTypeId;
	}


	public void setWorkflowStatusName(String workflowStatusName) {
		this.workflowStatusName = workflowStatusName;
	}


	public Short getPartyRoleId() {
		return this.partyRoleId;
	}


	public void setPartyRoleId(Short partyRoleId) {
		this.partyRoleId = partyRoleId;
	}


	public String getPartyRoleCompanyBeanPropertyName() {
		return this.partyRoleCompanyBeanPropertyName;
	}


	public void setPartyRoleCompanyBeanPropertyName(String partyRoleCompanyBeanPropertyName) {
		this.partyRoleCompanyBeanPropertyName = partyRoleCompanyBeanPropertyName;
	}


	public boolean isActiveOnly() {
		return this.activeOnly;
	}


	public void setActiveOnly(boolean activeOnly) {
		this.activeOnly = activeOnly;
	}


	public String getWorkflowStatusName() {
		return this.workflowStatusName;
	}
}
