package com.clifton.business.contract.observers;


import com.clifton.business.contract.BusinessContractParty;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessContractPartyValidator</code> ...
 *
 * @author manderson
 */
@Component
public class BusinessContractPartyValidator extends SelfRegisteringDaoValidator<BusinessContractParty> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BusinessContractParty party, @SuppressWarnings("unused") DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertMutuallyExclusive("Please select either a company or contact for the party, but not both.", party.getCompany(), party.getContact());
	}
}
