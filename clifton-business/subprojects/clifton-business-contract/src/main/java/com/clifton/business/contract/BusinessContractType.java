package com.clifton.business.contract;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.security.authorization.SecurityResource;


/**
 * The <code>BusinessContractType</code> defines a type of Contract.
 * Examples: IMA, ISDA
 *
 * @author akorver
 */
public class BusinessContractType extends NamedEntity<Short> {

	/**
	 * If true, then when copying, i.e. creating revised contracts,
	 * the Effective & Signed dates will also be copied over.
	 */
	private boolean copyContractDates;

	/**
	 * Defines whether this contract type is considered to be an "amendment". i.e. it is linked to a main contract.
	 */
	private boolean amendment;

	/**
	 * If set, the documents themselves are more restricted for viewing
	 * Used by DocumentDefinition's View condition for contracts -
	 * <p/>
	 * Requires user to have FULL CONTROL access in order to download the document
	 * instead of just read access
	 */
	private boolean restricted;

	/**
	 * In addition to security to contracts, additional access to the selected
	 * resource in order to edit/download, etc.
	 */
	private SecurityResource additionalSecurityResource;

	private String categoryName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCopyContractDates() {
		return this.copyContractDates;
	}


	public void setCopyContractDates(boolean copyContractDates) {
		this.copyContractDates = copyContractDates;
	}


	public boolean isAmendment() {
		return this.amendment;
	}


	public void setAmendment(boolean amendment) {
		this.amendment = amendment;
	}


	public boolean isRestricted() {
		return this.restricted;
	}


	public void setRestricted(boolean restricted) {
		this.restricted = restricted;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}


	public SecurityResource getAdditionalSecurityResource() {
		return this.additionalSecurityResource;
	}


	public void setAdditionalSecurityResource(SecurityResource additionalSecurityResource) {
		this.additionalSecurityResource = additionalSecurityResource;
	}
}
