package com.clifton.business.contract;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.search.BusinessContractClauseExtendedSearchForm;
import com.clifton.business.contract.search.BusinessContractExtendedSearchForm;
import com.clifton.business.contract.search.BusinessContractPartyRoleSearchForm;
import com.clifton.business.contract.search.BusinessContractPartySearchForm;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.business.contract.search.BusinessContractTemplateSearchForm;
import com.clifton.business.contract.search.BusinessContractTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface BusinessContractService {

	////////////////////////////////////////////////////////////////////////////
	////////            Business Contract Type Methods                 /////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractType getBusinessContractType(short id);


	public BusinessContractType getBusinessContractTypeByName(String name);


	public List<BusinessContractType> getBusinessContractTypeList(BusinessContractTypeSearchForm searchForm);


	public BusinessContractType saveBusinessContractType(BusinessContractType bean);


	public void copyBusinessContractType(short id, String name);


	public void deleteBusinessContractType(short id);


	////////////////////////////////////////////////////////////////////////////
	//////            Business Contract Template Methods                 ///////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractTemplate getBusinessContractTemplate(int id);


	public List<BusinessContractTemplate> getBusinessContractTemplateList(BusinessContractTemplateSearchForm searchForm);


	public BusinessContractTemplate saveBusinessContractTemplate(BusinessContractTemplate bean);


	public void deleteBusinessContractTemplate(int id);


	////////////////////////////////////////////////////////////////////////////
	////////              Business Contract Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContract getBusinessContract(int id);


	public List<BusinessContract> getBusinessContractListByParent(Integer parentId, BusinessContractSearchForm searchForm);


	public List<BusinessContract> getBusinessContractList(BusinessContractSearchForm searchForm);


	public BusinessContract saveBusinessContract(BusinessContract bean);


	/**
	 * Saves the contract object and uploads the file to document management system
	 */
	@DoNotAddRequestMapping
	public BusinessContract saveBusinessContractWithFile(BusinessContract bean, MultipartFile file);


	public void saveBusinessContractList(List<BusinessContract> beanList);


	/**
	 * Copies the given contract and optionally all of it's clauses, and optionally the document.
	 * The copied contract is given the new contract as its parent.
	 *
	 * @param id
	 * @param templateId           (If set, will use this as the template, else will use the original contract's template)
	 * @param copyContractClauses
	 * @param copyContractDocument (If set, will use the original contract as the initial document, else will use the template (if one is defined)
	 * @param copyContractParties
	 */
	public BusinessContract copyBusinessContract(int id, Integer templateId, String name, boolean copyContractClauses, boolean copyContractDocument, boolean copyContractParties);


	public void deleteBusinessContract(int id);


	////////////////////////////////////////////////////////////////////////////
	/////        Business Contract Clause Copy Methods                    //////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BusinessContract.class)
	public void copyBusinessContractSystemColumnValueList(int copyFromId, int id, String columnGroupName);


	////////////////////////////////////////////////////////////////////////////
	//////         Business Contract Extended Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BusinessContract.class)
	public List<BusinessContractExtended> getBusinessContractExtendedList(BusinessContractExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////          Business Contract Link Methods                      ///////
	////////////////////////////////////////////////////////////////////////////


	public List<BusinessContractLink> getBusinessContractLinkListByParent(int parentId);


	public List<BusinessContractLink> getBusinessContractLinkListByChild(int childId);


	public BusinessContractLink saveBusinessContractLink(int parentId, int childId);


	@SecureMethod(dtoClass = BusinessContract.class)
	public List<BusinessContractLink> saveBusinessContractParentLinks(int[] parentIds, int childId);


	@SecureMethod(dtoClass = BusinessContract.class)
	public List<BusinessContractLink> saveBusinessContractChildLinks(int parentId, int[] childIds);


	public void deleteBusinessContractLink(int id);


	////////////////////////////////////////////////////////////////////////////
	//////        Business Contract Clause Extended Methods               //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up "clauses" from custom column values tied to the Contract under the column group "Contract Clauses"
	 */
	@SecureMethod(dtoClass = BusinessContract.class)
	public List<BusinessContractClauseExtended> getBusinessContractClauseExtendedList(BusinessContractClauseExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////          Business Contract Party Type Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractPartyRole getBusinessContractPartyRole(short id);


	public BusinessContractPartyRole getBusinessContractPartyRoleByName(String name);


	public List<BusinessContractPartyRole> getBusinessContractPartyRoleList(BusinessContractPartyRoleSearchForm searchForm);


	public BusinessContractPartyRole saveBusinessContractPartyRole(BusinessContractPartyRole bean);


	public void deleteBusinessContractPartyRole(short id);


	////////////////////////////////////////////////////////////////////////////
	///////            Business Contract Party Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractParty getBusinessContractParty(int id);


	public List<BusinessContractParty> getBusinessContractPartyList(BusinessContractPartySearchForm searchForm);


	public List<BusinessContractParty> getBusinessContractPartyListByContract(int contractId);


	public List<BusinessContractParty> getBusinessContractPartyListByContractAndRole(int contractId, String... roleNames);


	public BusinessContractParty saveBusinessContractParty(BusinessContractParty bean);


	public void deleteBusinessContractParty(int id);


	public List<BusinessCompany> getBusinessContractCompanyListByContractCompanyAndPartyRole(int contractCompanyId, short partyRoleId);
}
