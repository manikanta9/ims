package com.clifton.business.contract.comparisons;


import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;


/**
 * The <code>BusinessContractRequiredLinkExistsComparison</code>
 * returns true for any contract that isn't an amendment type (no required links)
 * For amendments - returns true only if the contract is linked to a main contract
 *
 * @author manderson
 */
public class BusinessContractAmendmentAndLinkExistsComparison implements Comparison<BusinessContract> {

	private BusinessContractService businessContractService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(BusinessContract contract, ComparisonContext context) {
		AssertUtils.assertNotNull(contract, "Business Contract is required to find type and links.");

		boolean result = true;
		StringBuilder message = new StringBuilder("(Contract Type Is ");
		if (!contract.getContractType().isAmendment()) {
			message.append("Not An Amendment)");
		}
		else {
			message.append("An Amendment");
			if (CollectionUtils.isEmpty(getBusinessContractService().getBusinessContractLinkListByChild(contract.getId()))) {
				result = false;
				message.append(" and does not have a link to a main contract.)");
			}
			else {
				message.append(" and is linked to a main contract.)");
			}
		}
		if (context != null) {
			if (result) {
				context.recordTrueMessage(message.toString());
			}
			else {
				context.recordFalseMessage(message.toString());
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}
}
