package com.clifton.business.contract.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>BusinessContractTemplateSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessContractTemplateSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private Boolean inactive;

	@SearchField(searchField = "contractType.id", sortField = "contractType.name")
	private Short contractTypeId;

	@SearchField(searchFieldPath = "contractType", searchField = "name")
	private String contractTypeName;

	@SearchField
	private String description;

	@SearchField
	private String warningMessage;

	@SearchField
	private String documentFileType;

	@SearchField
	private Date documentUpdateDate;

	@SearchField
	private String documentUpdateUser;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public Short getContractTypeId() {
		return this.contractTypeId;
	}


	public void setContractTypeId(Short contractTypeId) {
		this.contractTypeId = contractTypeId;
	}


	public String getContractTypeName() {
		return this.contractTypeName;
	}


	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getWarningMessage() {
		return this.warningMessage;
	}


	public void setWarningMessage(String warningMessage) {
		this.warningMessage = warningMessage;
	}


	public String getDocumentFileType() {
		return this.documentFileType;
	}


	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}
}
