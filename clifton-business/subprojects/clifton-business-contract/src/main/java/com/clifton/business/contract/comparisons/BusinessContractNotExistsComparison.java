package com.clifton.business.contract.comparisons;


/**
 * The <code>BusinessContractNotExistsComparison</code> ...
 *
 * @author manderson
 */
public class BusinessContractNotExistsComparison extends BusinessContractExistsComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
