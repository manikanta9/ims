package com.clifton.business.contract;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.security.authorization.SecurityResourceAware;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>BusinessContractParty</code> associates a contract with a
 * {@link BusinessCompany} or {@link BusinessContact} and gives that relationship
 * a role.
 *
 * @author manderson
 */
public class BusinessContractParty extends BaseEntity<Integer> implements LabeledObject, SecurityResourceAware, SystemEntityModifyConditionAware {

	private BusinessContract contract;

	private BusinessCompany company;
	private BusinessContact contact;

	private BusinessContractPartyRole role;

	private String comments;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getContract() != null) {
			return getContract().getLabel() + " - " + getLabelShort();
		}
		return getLabelShort();
	}


	public String getLabelShort() {
		if (getCompany() != null) {
			return "Company: " + getCompany().getNameExpanded();
		}
		if (getContact() != null) {
			return "Contact: " + getContact().getLabel();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	///// Security Resource Aware


	/**
	 * Entity Modify Condition (if defined) takes precedence over security resource.
	 */
	@Override
	public SecurityResource getSecurityResource() {
		if (getContract() != null && getEntityModifyCondition() == null) {
			return getContract().getSecurityResource();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	///// Entity Modify Condition Aware


	/**
	 * Optional additional security for a specific role.  If defined, replaces security defined by {@link SecurityResourceAware}
	 */
	@Override
	public SystemCondition getEntityModifyCondition() {
		return getRole() == null ? null : getRole().getEntityModifyCondition();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContract getContract() {
		return this.contract;
	}


	public void setContract(BusinessContract contract) {
		this.contract = contract;
	}


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public BusinessContact getContact() {
		return this.contact;
	}


	public void setContact(BusinessContact contact) {
		this.contact = contact;
	}


	public BusinessContractPartyRole getRole() {
		return this.role;
	}


	public void setRole(BusinessContractPartyRole role) {
		this.role = role;
	}


	public String getComments() {
		return this.comments;
	}


	public void setComments(String comments) {
		this.comments = comments;
	}
}
