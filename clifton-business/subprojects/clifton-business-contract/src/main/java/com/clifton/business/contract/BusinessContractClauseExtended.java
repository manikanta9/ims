package com.clifton.business.contract;


import com.clifton.business.client.BusinessClient;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.value.SystemColumnValue;


/**
 * The <code>BusinessContractClauseExtended</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessContractClauseExtended extends BaseSimpleEntity<Integer> {

	private String uuid;

	private BusinessContract contract;
	private SystemColumnCustom column;
	private SystemColumnValue value;

	// Contracts are tied to Companies, however for extended searching/display
	// want to filter/view client specific information for Client Contracts
	// So this field is exposed when company is a client type.
	private BusinessClient client;

	// From the Column - Exposed for easier sorting
	private Integer order;


	public BusinessContract getContract() {
		return this.contract;
	}


	public void setContract(BusinessContract contract) {
		this.contract = contract;
	}


	public SystemColumnCustom getColumn() {
		return this.column;
	}


	public void setColumn(SystemColumnCustom column) {
		this.column = column;
	}


	public SystemColumnValue getValue() {
		return this.value;
	}


	public void setValue(SystemColumnValue value) {
		this.value = value;
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public BusinessClient getClient() {
		return this.client;
	}


	public void setClient(BusinessClient client) {
		this.client = client;
	}
}
