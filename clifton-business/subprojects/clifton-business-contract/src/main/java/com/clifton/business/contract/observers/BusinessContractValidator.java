package com.clifton.business.contract.observers;


import com.clifton.business.contract.BusinessContract;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessContractValidator</code> handles validating a {@link BusinessContract} prior to saving in the database.
 *
 * @author manderson
 */
@Component
public class BusinessContractValidator extends SelfRegisteringDaoValidator<BusinessContract> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(BusinessContract bean, DaoEventTypes config) throws ValidationException {
		ValidationUtils.assertTrue(bean.getCompany().getType().isContractAllowed(), "Company [" + bean.getCompany().getName() + "] is of type [" + bean.getCompany().getType().getName()
				+ "] which does not support contracts.");
		if (config.isUpdate()) {
			BusinessContract original = getOriginalBean(bean);
			if (!original.getCompany().equals(bean.getCompany())) {
				throw new FieldValidationException("You cannot change the Company for an existing contract.", "company.id");
			}
		}
	}
}
