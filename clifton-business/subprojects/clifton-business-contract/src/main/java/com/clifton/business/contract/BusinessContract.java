package com.clifton.business.contract;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.document.DocumentWithRecordStampAware;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.security.authorization.SecurityResourceAware;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessContract</code> class represents a Contract which is a specific document (IMA, ISDA, etc)
 * which is tied to a company (Client)
 *
 * @author manderson
 */
public class BusinessContract extends NamedHierarchicalEntity<BusinessContract, Integer> implements WorkflowAware, DocumentWithRecordStampAware, SystemColumnCustomValueAware, SecurityResourceAware {

	public static final String CONTRACT_TABLE_NAME = "BusinessContract";

	public static final String CONTRACT_CLAUSE_COLUMN_GROUP_NAME = "Contract Clauses";

	public static final String CONTRACT_ROUNDING_CUSTOM_COLUMN = "Rounding";

	//Independent Amount Allowed custom column is defined on the ISDA business contract associated with a balance.
	//A default Independent Amount % is required if the independent amount is allowed, but can then be overridden
	//with the value specified for certain securities (e.g. swaps)
	public static final String INDEPENDENT_AMOUNT_ALLOWED_CUSTOM_COLUMN = "Independent Amount Allowed";
	public static final String INDEPENDENT_AMOUNT_PERCENT_CUSTOM_COLUMN = "Independent Amount %";
	public static final String INDEPENDENT_AMOUNT_CALCULATOR_CUSTOM_COLUMN = "Independent Amount Calculator";
	public static final String INDEPENDENT_AMOUNT_FX_RATE_PROVIDER = "Independent Amount FX Rate Provider";

	// System Defined Custom Columns on the Business Contract (ISDA and REPOs both use same column names)
	public static final String CONTRACT_COLLATERAL_THRESHOLD_CUSTOM_COLUMN = "Collateral Threshold";
	public static final String CONTRACT_MIN_TRANSFER_AMOUNT_CUSTOM_COLUMN = "Minimum Transfer Amount";
	public static final String CONTRACT_COUNTERPARTY_COLLATERAL_THRESHOLD_CUSTOM_COLUMN = "Counterparty Collateral Threshold";
	public static final String CONTRACT_COUNTERPARTY_MIN_TRANSFER_AMOUNT_CUSTOM_COLUMN = "Counterparty Minimum Transfer Amount";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private BusinessCompany company;

	private BusinessContractType contractType;
	private BusinessContractTemplate contractTemplate;

	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;

	private Date effectiveDate;
	private Date signedDate;

	private boolean internalApproved;
	private boolean legalApproved;
	private boolean clientApproved;

	/**
	 * Document Properties
	 */
	private String documentFileType;

	/**
	 * Document Record Stamp Properties
	 */
	private Date documentUpdateDate;
	private String documentUpdateUser;

	/**
	 * Custom Column Value Properties
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		String lbl = getLabelShortWithDate();
		if (getWorkflowState() != null) {
			lbl += " " + getWorkflowState().getName();
		}
		if (!StringUtils.isEmpty(getDocumentFileType())) {
			lbl += "-" + getDocumentFileType();
		}
		return lbl;
	}


	public String getLabelShortWithDate() {
		String lbl = getLabelShort();
		if (getEffectiveDate() != null) {
			lbl += " (" + DateUtils.fromDateShort(getEffectiveDate()) + ")";
		}
		return lbl;
	}


	public String getLabelShort() {
		return getName();
	}


	public String getParentLabel() {
		if (getParent() != null) {
			return getParent().getLabel();
		}
		return null;
	}


	public boolean isActive() {
		return (getWorkflowStatus() != null && !WorkflowStatus.STATUS_NON_OPERATIONAL.equals(getWorkflowStatus().getName()));
	}


	/////////////////////////////////////////////////////////////////////////
	// Security Resource Aware


	@Override
	public SecurityResource getSecurityResource() {
		if (getContractType() != null) {
			return getContractType().getAdditionalSecurityResource();
		}
		return null;
	}


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	public BusinessContractType getContractType() {
		return this.contractType;
	}


	public void setContractType(BusinessContractType contractType) {
		this.contractType = contractType;
	}


	public Date getSignedDate() {
		return this.signedDate;
	}


	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}


	@Override
	public WorkflowState getWorkflowState() {
		return this.workflowState;
	}


	@Override
	public void setWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
	}


	@Override
	public WorkflowStatus getWorkflowStatus() {
		return this.workflowStatus;
	}


	@Override
	public void setWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public BusinessContractTemplate getContractTemplate() {
		return this.contractTemplate;
	}


	public void setContractTemplate(BusinessContractTemplate contractTemplate) {
		this.contractTemplate = contractTemplate;
	}


	public boolean isInternalApproved() {
		return this.internalApproved;
	}


	public void setInternalApproved(boolean internalApproved) {
		this.internalApproved = internalApproved;
	}


	public boolean isLegalApproved() {
		return this.legalApproved;
	}


	public void setLegalApproved(boolean legalApproved) {
		this.legalApproved = legalApproved;
	}


	public boolean isClientApproved() {
		return this.clientApproved;
	}


	public void setClientApproved(boolean clientApproved) {
		this.clientApproved = clientApproved;
	}


	@Override
	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	@Override
	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	@Override
	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	@Override
	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}
}
