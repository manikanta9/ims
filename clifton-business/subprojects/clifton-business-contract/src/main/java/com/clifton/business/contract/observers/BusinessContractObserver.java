package com.clifton.business.contract.observers;


import com.clifton.business.contract.BusinessContract;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.document.DocumentManagementService;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessContractObserver</code> is used to create new Contract documents from existing
 * templates when a template is selected.  This occurs for Inserts and Updates.  If the template document
 * doesn't exist, or the contract already has a document attached, then nothing is copied and no exception is thrown.
 *
 * @author manderson
 */
@Component
public class BusinessContractObserver extends BaseDaoEventObserver<BusinessContract> {

	private DocumentManagementService documentManagementService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unused")
	// NOTE: execute update outside of main transaction to avoid deadlock: successful copy will also update contract date/user properties
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<BusinessContract> dao, DaoEventTypes event, BusinessContract bean, Throwable e) {
		// Copy is only valid if a template was selected
		if (bean.getContractTemplate() != null) {
			// Removing this check - copyDocumentRecord call may or may not actually copy the document
			// but because of automatic transitions there could actually be an active transaction from workflow
			// The only case that I can think of (where it happened) was publishing a revised document (which also moves the original to Non-Operational)
			// in this case there wouldn't be a document to copy anyway, so it's ok if we are in an active transaction.
			//if (TransactionSynchronizationManager.isActualTransactionActive()) {
			// may result in a deadlock: original bean uses a different connection
			//	throw new IllegalStateException("Document copy cannot be executed inside of active transaction.");
			//}
			// NOTE: This method will ensure that the template document exists, and the contract itself doesn't have a document attached.
			// If either of those conditions fail - copy is not performed, but no exceptions are thrown!
			getDocumentManagementService().copyDocumentRecord("BusinessContractTemplate", bean.getContractTemplate().getId(), "BusinessContract", bean.getId());
		}
	}


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}
}
