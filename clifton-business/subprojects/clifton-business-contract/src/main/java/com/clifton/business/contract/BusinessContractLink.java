package com.clifton.business.contract;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.security.authorization.SecurityResourceAware;


/**
 * The <code>BusinessContractLink</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessContractLink extends ManyToManyEntity<BusinessContract, BusinessContract, Integer> implements SecurityResourceAware {

	/////////////////////////////////////////////////////////////////////////
	// Security Resource Aware - NOTE: Applying Security From the Main Contract  


	@Override
	public SecurityResource getSecurityResource() {
		if (getReferenceOne() != null) {
			return getReferenceOne().getSecurityResource();
		}
		return null;
	}
}
