package com.clifton.business.contract.search;


import com.clifton.business.company.search.BusinessCompanyAwareSearchForm;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessContractSearchForm</code> ...
 * <p>
 * NOTE: This cannot implement BusinessCompanyAwareSearchForm because it has custom logic when applying as "includeAsRelatedParty"
 *
 * @author manderson
 */
public class BusinessContractSearchForm extends BaseWorkflowAwareSearchForm implements BusinessCompanyAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyId;

	@SearchField(searchFieldPath = "company", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField(searchFieldPath = "company.type", searchField = "name")
	private String companyTypeName;

	@SearchField(searchField = "contractType.id", sortField = "contractType.name")
	private Short contractTypeId;

	@SearchField(searchField = "name", searchFieldPath = "contractType", comparisonConditions = {ComparisonConditions.EQUALS})
	private String contractTypeName;

	@SearchField(searchField = "name", searchFieldPath = "contractType", comparisonConditions = {ComparisonConditions.IN})
	private String[] contractTypeNames;

	@SearchField(searchField = "contractTemplate.id")
	private Integer contractTemplateId;

	@SearchField(searchField = "name", searchFieldPath = "contractTemplate", comparisonConditions = {ComparisonConditions.EQUALS})
	private String contractTemplateName;


	// Custom search filter - Inactive contracts are those that have WorkflowStatus.Name as Non-Operational
	private Boolean active;

	// Custom search filter - partyRoleId and partyCompanyId filters must be used together (EXISTS clause)
	private Short partyRoleId;
	private Integer partyCompanyId;

	// Used for Billing to filter on IMA or IMA Amendments - uses beginsWithContractTypeName = IMA
	@SearchField(searchField = "name", searchFieldPath = "contractType", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String beginsWithContractTypeName;

	@SearchField
	private Date effectiveDate;

	@SearchField
	private Date signedDate;

	@SearchField
	private String description;

	@SearchField(dateFieldIncludesTime = true)
	private Date documentUpdateDate;

	@SearchField
	private String documentUpdateUser;

	@SearchField(searchFieldPath = "contractType", searchField = "amendment")
	private Boolean amendmentType;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyIdOrRelatedCompany;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Boolean includeClientOrClientRelationship;

	// Custom filter applied with companyIds (built from companyId, companyIdOrRelatedCompany, and additionalCompanyIds)
	// where the contract is not specifically for the company, but the company is related to the contract through parties for the contract
	// Applied through custom logic to the BusinessCompanyAwareSearchForm below
	private Boolean includeAsRelatedParty;

	@SearchField(searchFieldPath = "contractType", searchField = "categoryName")
	private String categoryName;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getCompanyPropertyNames() {
		return new String[]{"company"};
	}


	@Override
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds) {
		if (!CollectionUtils.isEmpty(companyIds) && BooleanUtils.isTrue(getIncludeAsRelatedParty())) {
			DetachedCriteria partyExists = DetachedCriteria.forClass(BusinessContractParty.class, "bcp");
			partyExists.setProjection(Projections.property("id"));
			partyExists.add(Restrictions.eqProperty("contract.id", criteria.getAlias() + ".id"));
			if (companyIds.size() == 1) {
				partyExists.add(Restrictions.eq("company.id", companyIds.get(0)));
			}
			else {
				partyExists.add(Restrictions.in("company.id", companyIds));
			}
			orCompanyFilters.add(Subqueries.exists(partyExists));
		}
	}


	@Override
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId) {
		if (parentCompanyId != null && BooleanUtils.isTrue(getIncludeAsRelatedParty())) {
			DetachedCriteria partyExists = DetachedCriteria.forClass(BusinessContractParty.class, "bcpp");
			partyExists.setProjection(Projections.property("id"));
			partyExists.add(Restrictions.eqProperty("contract.id", criteria.getAlias() + ".id"));
			partyExists.createAlias("company", "bcppc");
			partyExists.add(Restrictions.eq("bcppc.parent.id", parentCompanyId));
		}
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	@Override
	public Integer getCompanyId() {
		return this.companyId;
	}


	@Override
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getCompanyTypeName() {
		return this.companyTypeName;
	}


	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}


	public Short getContractTypeId() {
		return this.contractTypeId;
	}


	public void setContractTypeId(Short contractTypeId) {
		this.contractTypeId = contractTypeId;
	}


	public String getContractTypeName() {
		return this.contractTypeName;
	}


	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}


	public String[] getContractTypeNames() {
		return this.contractTypeNames;
	}


	public void setContractTypeNames(String[] contractTypeNames) {
		this.contractTypeNames = contractTypeNames;
	}


	public Integer getContractTemplateId() {
		return this.contractTemplateId;
	}


	public void setContractTemplateId(Integer contractTemplateId) {
		this.contractTemplateId = contractTemplateId;
	}


	public String getContractTemplateName() {
		return this.contractTemplateName;
	}


	public void setContractTemplateName(String contractTemplateName) {
		this.contractTemplateName = contractTemplateName;
	}


	public Boolean getActive() {
		return this.active;
	}


	public void setActive(Boolean active) {
		this.active = active;
	}


	public Short getPartyRoleId() {
		return this.partyRoleId;
	}


	public void setPartyRoleId(Short partyRoleId) {
		this.partyRoleId = partyRoleId;
	}


	public Integer getPartyCompanyId() {
		return this.partyCompanyId;
	}


	public void setPartyCompanyId(Integer partyCompanyId) {
		this.partyCompanyId = partyCompanyId;
	}


	public String getBeginsWithContractTypeName() {
		return this.beginsWithContractTypeName;
	}


	public void setBeginsWithContractTypeName(String beginsWithContractTypeName) {
		this.beginsWithContractTypeName = beginsWithContractTypeName;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getSignedDate() {
		return this.signedDate;
	}


	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public Boolean getAmendmentType() {
		return this.amendmentType;
	}


	public void setAmendmentType(Boolean amendmentType) {
		this.amendmentType = amendmentType;
	}


	@Override
	public Integer getCompanyIdOrRelatedCompany() {
		return this.companyIdOrRelatedCompany;
	}


	@Override
	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany) {
		this.companyIdOrRelatedCompany = companyIdOrRelatedCompany;
	}


	@Override
	public Boolean getIncludeClientOrClientRelationship() {
		return this.includeClientOrClientRelationship;
	}


	@Override
	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship) {
		this.includeClientOrClientRelationship = includeClientOrClientRelationship;
	}


	public Boolean getIncludeAsRelatedParty() {
		return this.includeAsRelatedParty;
	}


	public void setIncludeAsRelatedParty(Boolean includeAsRelatedParty) {
		this.includeAsRelatedParty = includeAsRelatedParty;
	}


	public String getCategoryName() {
		return this.categoryName;
	}


	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
