package com.clifton.business.contract.search;


import com.clifton.core.dataaccess.search.SearchField;


/**
 * The <code>BusinessContractExtendedSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessContractExtendedSearchForm extends BusinessContractSearchForm {

	@SearchField
	public Boolean amendmentExists;

	@SearchField(searchField = "text", searchFieldPath = "client.clientType", leftJoin = true)
	private String clientTypeName;

	@SearchField(searchField = "client.id", leftJoin = true)
	private Integer clientId;

	// Custom Search Field - still does a left join
	private Boolean clientActive;

	@SearchField
	private Boolean documentRestrictionsExist;


	public Boolean getAmendmentExists() {
		return this.amendmentExists;
	}


	public void setAmendmentExists(Boolean amendmentExists) {
		this.amendmentExists = amendmentExists;
	}


	public Boolean getClientActive() {
		return this.clientActive;
	}


	public void setClientActive(Boolean clientActive) {
		this.clientActive = clientActive;
	}


	public String getClientTypeName() {
		return this.clientTypeName;
	}


	public void setClientTypeName(String clientTypeName) {
		this.clientTypeName = clientTypeName;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Boolean getDocumentRestrictionsExist() {
		return this.documentRestrictionsExist;
	}


	public void setDocumentRestrictionsExist(Boolean documentRestrictionsExist) {
		this.documentRestrictionsExist = documentRestrictionsExist;
	}
}
