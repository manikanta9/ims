Ext.ns('Clifton.business.contract');

Clifton.business.ContractsGrid_ByCompany = Ext.extend(TCG.grid.GridPanel, {
	name: 'businessContractExtendedListFind',
	xtype: 'gridpanel',
	isIncludeRelatedCompanyCheckbox: function() {
		return false;
	},
	getRelatedCompanyCheckboxLabel: function() {
		return 'Include Parent/Child Company\'s Contracts';
	},
	instructions: 'The following contracts are associated to the company.',

	groupField: 'groupValue',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Contracts" : "Contract"]})',
	groupMode: 'display',
	columns: [
		{
			header: 'Group', width: 100, hidden: true, dataIndex: 'groupValue', groupingOn: 'Category', filter: false, sortable: false,
			renderer: function(v, metaData, r) {
				if (this.groupingOn === 'Category') {
					return r.data['contractType.categoryName'];
				}
				if (TCG.isTrue(r.data['amendmentExists']) || TCG.isTrue(r.data['amendment'])) {
					return r.data['contractType.categoryName'] + ': ' + r.data['mainContract.name'];
				}
				return r.data['contractType.categoryName'];
			}
		},
		{header: 'Main Contract', width: 100, dataIndex: 'mainContract.name', filter: false, hidden: true},
		{header: 'Amendment', width: 100, dataIndex: 'amendment', type: 'boolean', filter: false, hidden: true},
		{header: 'Category', width: 140, hidden: true, dataIndex: 'contractType.categoryName', filter: {type: 'combo', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Contract Type Categories', searchFieldName: 'categoryName'}},
		{header: 'Type', width: 140, dataIndex: 'contractType.name', filter: {type: 'combo', searchFieldName: 'contractTypeId', url: 'businessContractTypeListFind.json'}},
		{header: 'Company', width: 140, dataIndex: 'company.name', searchFieldName: 'companyName', hidden: true},
		{header: 'Amendment Type', width: 70, dataIndex: 'contractType.amendment', type: 'boolean', hidden: true},
		{
			header: 'Format', width: 30, dataIndex: 'documentFileType', align: 'center',
			renderer: function(ft, args, r) {
				return TCG.renderDocumentFileTypeIcon(ft);
			}
		},
		{header: 'Template', width: 75, dataIndex: 'contractTemplate.name', filter: {type: 'combo', searchFieldName: 'contractTemplateId', url: 'businessContractTemplateListFind.json'}},
		{header: 'Revised By', width: 100, dataIndex: 'parent.label', filter: false, hidden: true},
		{
			header: 'Name', width: 265, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
			renderer: function(v, metaData, r) {
				const note = r.data['description'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.data['description'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Effective On', width: 50, dataIndex: 'effectiveDate'},
		{header: 'Doc Updated On', width: 80, dataIndex: 'documentUpdateDate'},
		{header: 'Comments', hidden: true, width: 200, dataIndex: 'description', filter: false, sortable: false},
		{header: 'Workflow State', width: 60, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 65, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
		{header: 'Signed On', width: 50, dataIndex: 'signedDate', hidden: true},
		{header: 'Doc Updated By', width: 50, dataIndex: 'documentUpdateUser', hidden: true},
		{header: 'Amended', width: 45, dataIndex: 'amendmentExists', type: 'boolean'},
		{
			header: 'Restricted Doc', width: 50, dataIndex: 'documentRestrictionsExist', type: 'boolean', hidden: true,
			tooltip: 'If checked, there are explicit document restrictions entered for users and/or groups'
		},
		{header: 'Active', width: 42, dataIndex: 'active', hidden: true, type: 'boolean', filter: {searchFieldName: 'active'}, sortable: false},
		{header: 'Comments', dataIndex: 'description', hidden: true}

	],


	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({
			fieldLabel: 'Counterparty', name: 'partyCompany', xtype: 'toolbar-combo', width: 150, displayField: 'label', tooltipField: 'label', valueField: 'id', url: 'businessContractCompanyListByContractCompanyAndPartyRole.json', root: 'data',
			listeners: {
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', combo, 'business.contract.party.role.Counterparty');
					combo.store.baseParams = {
						contractCompanyId: combo.getWindow().defaultData.company.id,
						partyRoleId: role.id
					};

				}
			}
		});

		filters.push({
			fieldLabel: 'Category', xtype: 'toolbar-combo', width: 150, name: 'categoryName', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Contract Type Categories',
			linkedFilter: 'contractType.categoryName'
		});

		filters.push({
			fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'displayType', width: 120, mode: 'local', emptyText: '< All Contracts >', displayField: 'name', valueField: 'value',
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name'],
				data: [['ALL', 'All Contracts']
					, ['ALL-GROUPED', 'All Contracts (Group Main with Amendments)']
					, ['MAIN', 'Main Contracts Only']
					, ['AMENDMENT', 'Amendments Only']]
			})
		});


		const menu = new Ext.menu.Menu();
		this.appendIncludeOptions(menu);
		if (menu.items && menu.items.length > 0) {
			toolbar.add({
				text: 'Include',
				iconCls: 'find',
				name: 'includeOptions',
				menu: menu
			});
			toolbar.add('-');
		}
		return filters;
	},

	appendIncludeOptions: function(menu) {
		const gridPanel = this;
		if (TCG.isTrue(this.isIncludeRelatedCompanyCheckbox())) {
			menu.add(
				{
					text: this.getRelatedCompanyCheckboxLabel()
					, xtype: 'menucheckitem'
					, name: 'includeRelatedCompanies'
					, checkHandler: function() {
						gridPanel.reload();
					}
				});
		}

		menu.add(
			{
				text: 'Include As Related Party'
				, xtype: 'menucheckitem'
				, name: 'includeAsRelatedParty'
				, checkHandler: function() {
					gridPanel.reload();
				}
			});

		this.appendAdditionalIncludeOptions(menu);
	},
	appendAdditionalIncludeOptions: function(menu) {
		// OVERRIDE ME TO ADD MORE OPTIONS
	},


	getLoadParams: function(firstLoad) {
		const grid = this;
		if (firstLoad) {
			// default to active contracts
			this.setFilterValue('active', true);
		}

		const params = {requestedMaxDepth: 4, companyId: this.getCompanyId()};
		const dType = TCG.getChildByName(grid.getTopToolbar(), 'displayType').getValue();
		if (dType === 'MAIN') {
			params.amendmentType = false;
		}
		else if (dType === 'AMENDMENT') {
			params.amendmentType = true;
		}
		const i = this.grid.getColumnModel().findColumnIndex('groupValue');
		if (dType === 'ALL-GROUPED') {
			this.grid.getColumnModel().config[i].groupingOn = 'Related Contracts';
		}
		else {
			this.grid.getColumnModel().config[i].groupingOn = 'Category';
		}

		const includeOptions = TCG.getChildByName(this.getTopToolbar(), 'includeOptions');
		if (includeOptions && includeOptions.menu) {
			this.appendIncludeOptionsLoadParams(includeOptions.menu, params);
		}
		const partyCompanyId = TCG.getChildByName(grid.getTopToolbar(), 'partyCompany').getValue();
		const role = TCG.data.getData('businessContractPartyRoleByName.json?name=Counterparty', grid, 'business.contract.party.role.Counterparty');
		if (partyCompanyId) {
			params.partyRoleId = role.id;
			params.partyCompanyId = partyCompanyId;
		}
		return params;
	},

	appendIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
		let showCompany = false;
		if (TCG.isTrue(this.isIncludeRelatedCompanyCheckbox())) {
			const inclRelated = TCG.getChildByName(includeOptionsMenu, 'includeRelatedCompanies').checked;

			if (inclRelated) {
				showCompany = true;
				delete params.companyId;
				params.companyIdOrRelatedCompany = this.getCompanyId();
			}
		}

		const inclAsRelatedParty = TCG.getChildByName(includeOptionsMenu, 'includeAsRelatedParty').checked;
		if (inclAsRelatedParty) {
			showCompany = true;
			params.includeAsRelatedParty = true;
		}

		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('company.name'), showCompany === false);

		this.appendAdditionalIncludeOptionsLoadParams(includeOptionsMenu, params);
	},

	appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
		// OVERRIDE ME IF ADDITIONAL OPTIONS ARE ADDED TO MENU
	},

	getCompanyId: function() {
		return this.getCompany().id;
	},

	getCompany: function() {
		return this.getWindow().getMainForm().formValues;
	},

	editor: {
		ptype: 'documentAware-grideditor',
		tableName: 'BusinessContract',
		entityText: 'contract',

		detailPageClass: {
			getItemText: function(rowItem) {
				return (TCG.isTrue(rowItem.get('contractType.amendment'))) ? 'Amendment' : 'Main';
			},
			items: [
				{text: 'Main', iconCls: 'contract', className: 'Clifton.business.contract.MainContractWindow'},
				{text: 'Amendment', iconCls: 'contract', className: 'Clifton.business.contract.AmendmentContractWindow'}
			]
		},
		copyURL: 'businessContractCopy.json',
		copyButtonName: 'Revise',
		copyButtonTooltip: 'Creates a copy of the selected contract as a revision.',
		getDefaultData: function(gridPanel) { // defaults client id for the detail page
			const dd = {
				company: gridPanel.getCompany()
			};
			if (gridPanel.defaultCategory) {
				dd.contractType = {categoryName: gridPanel.defaultCategory};
			}
			return dd;
		},
		deleteURL: 'businessContractDelete.json',
		copyHandler: function(gridPanel) {
			const editor = this;

			const grid = this.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select a row to be copied.', 'No Row(s) Selected');
				return;
			}
			else if (sm.getCount() !== 1) {
				TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
				return;
			}
			Ext.Msg.confirm('Revise Selected Contract', 'Are you sure you would like to revise the selected contract?', function(a) {
				if (a === 'yes') {
					const id = sm.getSelected().id;

					editor.openDetailPage('Clifton.business.contract.ContractCopyWindow', editor.getGridPanel(), id);
				}
			});
		}
	}
});
Ext.reg('business-contractsGrid-byCompany', Clifton.business.ContractsGrid_ByCompany);


if (Clifton.business.client.ClientRelationshipWindowAdditionalTabs) {
	Clifton.business.client.ClientRelationshipWindowAdditionalTabs.push({
		title: 'Documents',

		items: [{
			name: 'businessContractExtendedListFind',
			xtype: 'business-contractsGrid-byCompany',
			getCompany: function() {
				return TCG.getValue('company', this.getWindow().getMainForm().formValues);
			},
			defaultCategory: 'Performance Summaries',
			appendAdditionalIncludeOptions: function(menu) {
				const gridPanel = this;
				menu.add({
					text: 'Include Client Documents',
					xtype: 'menucheckitem',
					name: 'includeClient',
					checkHandler: function() {
						gridPanel.reload();
					}
				});
			},

			appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
				const inclClient = TCG.getChildByName(includeOptionsMenu, 'includeClient').checked;
				if (inclClient) {
					params.includeClientOrClientRelationship = true;
					const cm = this.getColumnModel();
					cm.setHidden(cm.findColumnIndex('company.name'), false);
					// Don't Hide it in case previous filters set to include the company
				}
			}
		}]
	});
}

if (Clifton.business.client.ClientWindowBaseAdditionalTabs) {
	Clifton.business.client.ClientWindowBaseAdditionalTabs.push({
		addAdditionalTabs: function(w) {
			const tabs = w.items.get(0);
			const pos = w.getTabPosition(w, 'Manager Accounts');
			tabs.insert(pos + 1, this.contractsTab);
		},
		contractsTab: {
			title: 'Documents',
			items: [{
				xtype: 'business-contractsGrid-byCompany',
				name: 'businessContractExtendedListFind',
				getCompany: function() {
					return TCG.getValue('company', this.getWindow().getMainForm().formValues);
				},
				isIncludeRelatedCompanyCheckbox: function() {
					return this.getWindow().isIncludeRelatedCheckbox();
				},
				getRelatedCompanyCheckboxLabel: function() {
					return this.getWindow().getRelatedCheckboxLabel('Documents');
				},

				appendAdditionalIncludeOptions: function(menu) {
					const ct = TCG.getValue('category', this.getWindow().getMainForm().formValues);
					if (TCG.isTrue(ct.clientRelationshipUsed)) {
						const gridPanel = this;
						menu.add({
							text: 'Include Client Relationship Documents',
							xtype: 'menucheckitem',
							name: 'includeClientRelationship',
							checkHandler: function() {
								gridPanel.reload();
							}
						});
					}
				},

				appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
					const ct = TCG.getValue('category', this.getWindow().getMainForm().formValues);
					if (TCG.isTrue(ct.clientRelationshipUsed)) {
						const inclClientRel = TCG.getChildByName(includeOptionsMenu, 'includeClientRelationship').checked;
						if (inclClientRel) {
							params.includeClientOrClientRelationship = true;
							const cm = this.getColumnModel();
							cm.setHidden(cm.findColumnIndex('company.name'), false);
						}
					}
				}
			}]
		}
	});
}

if (Clifton.business.company.BasicCompanyWindowAdditionalTabs) {
	Clifton.business.company.BasicCompanyWindowAdditionalTabs.push({
		addAdditionalTabs: function(w) {
			if (w.defaultData.type.contractAllowed) {
				const tabs = w.items.get(0).items.get(0);
				const pos = this.getTabPosition(w.items.get(0), 'Notes');
				tabs.insert(pos + 1, this.contractsTab);
			}
		},
		getTabPosition: function(w, tabTitle) {
			const tabs = w.items.get(0);
			for (let i = 0; i < tabs.items.length; i++) {
				if (tabs.items.get(i).title === tabTitle) {
					return i;
				}
			}
			return 0;
		},
		contractsTab: {
			title: 'Documents',
			items: [{
				xtype: 'business-contractsGrid-byCompany'
			}]
		}
	});
}
