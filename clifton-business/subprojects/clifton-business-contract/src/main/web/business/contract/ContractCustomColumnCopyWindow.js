Clifton.business.contract.ContractCustomColumnCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Business Document - Copy Clauses',
	iconCls: 'contract',
	width: 700,
	height: 400,

	items: [{
		xtype: 'formpanel',
		url: 'businessContract.json',
		getSaveURL: function() {
			return 'businessContractSystemColumnValueListCopy.json';
		},
		loadValidation: false,

		getConfirmBeforeSaveMsg: function() {
			const f = this.getForm();
			if (TCG.isTrue(f.findField('clausesExist').getValue())) {
				return '<b>WARNING: This contract already has clauses populated.</b><br/><br/>Are you sure you want to lose those clauses and copy clauses from the selected contract?';
			}
			return undefined;
		},

		getWarningMessage: function(form) {
			const id = form.idFieldValue;
			if (id) {
				const existingClauses = TCG.data.getData('systemColumnValueListFind.json?requestedPropertiesRoot=data&requestedProperties=id&tableName=BusinessContract&columnGroupName=Contract Clauses&fkFieldId=' + id, this);
				if (existingClauses && existingClauses.length > 0) {
					form.findField('clausesExist').setValue(true);
					return '<b>WARNING:</b> This contract already has clauses populated.  If you continue, they will be overwritten with the clauses from the selected contract below.';
				}
			}
			return undefined;
		},

		items: [
			{name: 'clausesExist', xtype: 'hidden', value: false, submitValue: false},
			{name: 'columnGroupName', xtype: 'hidden', value: 'Contract Clauses'},
			{fieldLabel: 'Company', name: 'company.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'company.id', submitDetailField: false},
			{fieldLabel: 'Type', name: 'contractType.name', detailIdField: 'contractType.id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.TypeWindow', submitDetailField: false},
			{fieldLabel: 'Template', name: 'contractTemplate.name', detailIdField: 'contractTemplate.id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.TemplateWindow', submitDetailField: false},
			{fieldLabel: 'Document', name: 'name', detailIdField: 'id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', submitDetailField: false},
			{xtype: 'label', html: '<hr/>'},
			{
				fieldLabel: 'Copy Type', name: 'contractType.name', displayField: 'name', hiddenName: 'copyFromType.id', xtype: 'combo', url: 'businessContractTypeListFind.json', submitValue: false, listeners: {
					beforeRender: function(combo) {
						const defaultComboValue = TCG.getParentFormPanel(combo).getWindow().defaultData.copyFromType;
						combo.setValue({value: defaultComboValue.id, text: defaultComboValue.name});
					}
				}
			},
			{
				fieldLabel: 'Copy From', name: 'copyFrom.label', hiddenName: 'copyFromId', xtype: 'combo', url: 'businessContractListFind.json?active=true', requiredFields: ['copyFromType.id'],
				displayField: 'label',
				detailPageClass: 'Clifton.business.contract.ContractWindow',
				allowBlank: false,
				beforequery: function(queryEvent) {
					const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
					const templateId = TCG.getValue('contractTemplate.id', form.formValues);
					if (TCG.isNotBlank(templateId) && TCG.isFalse(form.findField('searchAll').checked)) {
						queryEvent.combo.store.baseParams = {contractTemplateId: form.findField('contractTemplate.id').value};
					}
					else {
						queryEvent.combo.store.baseParams = {contractTypeId: form.findField('copyFromType.id').value};
					}
				}
			},
			{
				xtype: 'checkbox', fieldLabel: '', boxLabel: 'Leave unchecked to search for template (if supplied) specific active contracts', name: 'searchAll', submitValue: false,
				listeners: {
					check: function(f) {
						// On changes reset contract dropdown
						const p = TCG.getParentFormPanel(f);
						const bc = p.getForm().findField('copyFrom.label');
						bc.clearValue();
						bc.store.removeAll();
						bc.lastQuery = null;
					}
				}
			}
		]
	}]
});
