Clifton.business.contract.PartyRoleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Party Role',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Role',
				items: [{
					xtype: 'formpanel',
					instructions: 'Contract Party Roles define the roles for companies or contacts involved in a contract.',
					url: 'businessContractPartyRole.json',
					items: [
						{fieldLabel: 'Role Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{xtype: 'system-entity-modify-condition-combo'}
					]
				}]
			},


			{
				title: 'Used By',
				xtype: 'gridpanel',
				name: 'businessContractPartyListFind.json',
				instructions: 'The following contract parties are using the selected party role.',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Contract', width: 125, dataIndex: 'contract.name'},
					{header: 'Party', width: 125, dataIndex: 'labelShort'},
					{header: 'Comments', width: 100, dataIndex: 'comments'}

				],
				editor: {
					addEnabled: false,
					detailPageClass: 'Clifton.business.contract.PartyWindow'
				},
				getLoadParams: function() {
					return {roleId: this.getWindow().getMainFormId()};
				}
			}
		]
	}]
});

