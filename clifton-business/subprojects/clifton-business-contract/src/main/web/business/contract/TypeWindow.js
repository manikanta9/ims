Clifton.business.contract.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Document Type',
	width: 900,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Type',
				items: [{
					xtype: 'formpanel',
					instructions: 'Business Document Types are used to classify business documents, organize templates, and define their applicable clauses.',
					url: 'businessContractType.json',
					items: [
						{fieldLabel: 'Name', name: 'name'},
						{fieldLabel: 'Category', name: 'categoryName', xtype: 'system-list-combo', listName: 'Contract Type Categories'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Additional Security', name: 'additionalSecurityResource.labelExpanded', hiddenName: 'additionalSecurityResource.id', displayField: 'labelExpanded', queryParam: 'labelExpanded', xtype: 'combo', loadAll: false, url: 'securityResourceListFind.json',
							qtip: 'Additional Security Resource to use for editing documents associated with this contract type. When not selected, will use the resource attached to the Contract table. When selected, also applies to Party Roles unless Entity Modify Condition is selected on Role Type.'
						},
						{boxLabel: 'Amendment', name: 'amendment', xtype: 'checkbox'},
						{
							boxLabel: 'Restricted Document', name: 'restricted', xtype: 'checkbox',
							qtip: 'Restricted Documents automatically allow only those with Full Control access to contracts (and Full Control access to additional security resource if selected) to be able to download the document for viewing.  Specific contracts themselves can be further restricted to specific users/groups when needed.'
						},
						{boxLabel: 'Copy Document Dates (When copying/creating revisions of documents, also copy the dates)', name: 'copyContractDates', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Templates',
				xtype: 'gridpanel',
				name: 'businessContractTemplateListFind',
				instructions: 'The following document templates are available for this document type.',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Template Name', width: 150, dataIndex: 'name'},
					{
						header: 'Format', width: 30, dataIndex: 'documentFileType',
						renderer: function(ft, args, r) {
							return TCG.renderDocumentFileTypeIcon(ft);
						}
					},
					{header: 'Description', width: 150, dataIndex: 'description'},
					{header: 'Warning Message', width: 150, dataIndex: 'warningMessage'},
					{header: 'Document Updated On', dataIndex: 'documentUpdateDate', hidden: true},
					{header: 'Document Updated By', dataIndex: 'documentUpdateUser', hidden: true}
				],
				getLoadParams: function() {
					return {contractTypeId: this.getWindow().getMainFormId()};
				},
				editor: {
					ptype: 'documentAware-grideditor',
					tableName: 'BusinessContractTemplate',
					entityText: 'contract template',
					detailPageClass: 'Clifton.business.contract.TemplateWindow',
					getDefaultData: function(gridPanel) { // defaults contract type id for the detail page
						return {
							contractType: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			},


			{
				title: 'Custom Columns',
				items: [{
					name: 'systemColumnCustomListFind',
					xtype: 'gridpanel',
					instructions: 'The following custom columns are associated with documents of this type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Column Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Order', width: 50, dataIndex: 'order', type: 'int'},
						{header: 'Required', width: 50, dataIndex: 'required', type: 'boolean'},
						{header: 'System', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					getLoadParams: function() {
						return {
							linkedValue: this.getWindow().getMainFormId(),
							columnGroupName: 'Contract Clauses'
						};
					},
					editor: {
						detailPageClass: 'Clifton.system.schema.ColumnWindow',
						deleteURL: 'systemColumnDelete.json',
						getDefaultData: function(gridPanel) { // defaults group
							const grp = TCG.data.getData('systemColumnGroupByName.json?name=Contract Clauses', gridPanel, 'system.schema.group.Contract Clauses');
							const fVal = gridPanel.getWindow().getMainForm().formValues;
							const lbl = TCG.getValue('name', fVal);
							return {
								columnGroup: grp
								, linkedValue: TCG.getValue('id', fVal)
								, linkedLabel: lbl
								, table: grp.table
							};
						},
						getDefaultDataForExisting: function(gridPanel, row, detailPageClass) {
							return undefined; // Not needed for Existing
						}
					}
				}]
			},


			{
				title: 'Clauses',
				name: 'businessContractClauseExtendedListFind',
				xtype: 'gridpanel',
				instructions: 'Use the toolbar filters to select a specific clause you are looking for.  The value column can be further filtered if looking for documents with a specific value for that clause.  Only documents with clauses populated will be displayed.  By default documents will be filtered on Active Clients and Active Documents (i.e. Document is not in Non-Operational status)',
				reloadOnRender: false,
				columns: [
					{header: 'ContractID', width: 20, dataIndex: 'contract.id', hidden: true, type: 'int', filter: {searchFieldName: 'contractId'}},
					{header: 'Order', width: 20, dataIndex: 'order', hidden: true, type: 'int', filter: {searchFieldName: 'order'}, defaultSortColumn: true, defaultSortDirection: 'asc'},
					{header: 'Company', width: 100, dataIndex: 'contract.company.name', filter: {searchFieldName: 'companyName'}, hidden: true},
					{header: 'Client Type', width: 100, dataIndex: 'contract.client.clientType.text', hidden: true, filter: {searchFieldName: 'clientTypeName'}},
					{header: 'Client Relationship', width: 100, dataIndex: 'contract.client.clientRelationship.name', hidden: true, filter: false},
					{header: 'Document', width: 250, dataIndex: 'contract.label', filter: {searchFieldName: 'contractName'}},
					{header: 'Workflow State', width: 100, dataIndex: 'contract.workflowState.name', hidden: true, filter: false},
					{header: 'Workflow Status', width: 100, dataIndex: 'contract.workflowStatus.name', hidden: true, filter: false},
					{header: 'Active Client', width: 70, sortable: false, dataIndex: 'contract.client.active', type: 'boolean', filter: {searchFieldName: 'clientActive'}},
					{header: 'Active', width: 50, sortable: false, dataIndex: 'contract.active', type: 'boolean', filter: {searchFieldName: 'active'}},
					{header: 'Clause', width: 150, dataIndex: 'column.name', filter: {searchFieldName: 'columnName'}},
					{header: 'Data Type', width: 50, dataIndex: 'column.dataType.name', hidden: true, filter: false, sortable: false},
					{
						header: 'Value', width: 150, dataIndex: 'value.text', filter: {searchFieldName: 'valueOrText'},
						renderer: function(v, c, r) {
							if (TCG.isNull(v)) {
								return '';
							}
							if (r.data['column.dataType.name'] === 'BOOLEAN') {
								if (TCG.isTrue(v) || v === 'Yes') {
									return 'Yes';
								}
								return 'No';
							}
							return v;
						}
					}
				],
				getTopToolbarFilters: function(toolbar) {
					const grid = this;
					return [
						{
							fieldLabel: 'Clause', xtype: 'combo', name: 'columnId', width: 150, url: 'systemColumnCustomListFind.json',
							beforequery: function(queryEvent) {
								const cmb = queryEvent.combo;
								cmb.store.baseParams = {
									tableName: 'BusinessContract',
									customOnly: true
								};
								cmb.store.baseParams.linkedValue = grid.getWindow().getMainFormId();
								cmb.store.baseParams.includeNullLinkedValue = true;
							},
							listeners: {
								select: function(field) {
									TCG.getParentByClass(field, Ext.Panel).reload();
								}
							}
						}
					];
				},
				editor: {
					drillDownOnly: true,
					detailPageClass: 'Clifton.business.contract.ContractCustomColumnWindow',
					getDetailPageId: function(gridPanel, row) {
						return row.json.contract.id;
					}
				},
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						// default to active
						this.setFilterValue('contract.client.active', true);
						this.setFilterValue('contract.active', true);
					}
					const params = {
						populatedValuesOnly: true,
						contractTypeId: this.getWindow().getMainFormId()
					};
					const t = this.getTopToolbar();
					const column = TCG.getChildByName(t, 'columnId');
					if (TCG.isNotBlank(column.getValue())) {
						params.columnId = column.getValue();
					}
					return params;
				}
			}
		]
	}]
});
