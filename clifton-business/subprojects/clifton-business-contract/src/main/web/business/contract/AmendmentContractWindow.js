TCG.use('Clifton.business.contract.BusinessContractSearchWindow');
TCG.use('Clifton.business.contract.BasicContractWindow');

// adds "Linked Contracts" tab at the end of BasicCompanyWindow
Clifton.business.contract.AmendmentContractWindow = Ext.extend(Clifton.business.contract.BasicContractWindow, {

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		tabs.add(this.linkedContractsTab);
	},


	linkedContractsTab: {
		title: 'Main Documents',
		items: [{
			xtype: 'gridpanel',
			name: 'businessContractLinkListByChild',
			instructions: 'This document is considered an amendment of the following documents',
			columns: [
				{header: 'ID', dataIndex: 'id', hidden: true},
				{header: 'ContractID', dataIndex: 'referenceOne.id', hidden: true},
				{header: 'Type', width: 50, dataIndex: 'referenceOne.contractType.name'},
				{
					header: 'Format', width: 30, dataIndex: 'referenceOne.documentFileType', filter: false, align: 'center',
					renderer: function(ft, args, r) {
						return TCG.renderDocumentFileTypeIcon(ft);
					}
				},
				{header: 'Company', dataIndex: 'referenceOne.company.nameWithType'},
				{header: 'Template', width: 100, hidden: true, dataIndex: 'referenceOne.contractTemplate.name'},
				{header: 'Document', width: 175, dataIndex: 'referenceOne.name'},
				{header: 'Workflow State', width: 60, dataIndex: 'referenceOne.workflowState.name'},
				{header: 'Workflow Status', width: 60, dataIndex: 'referenceOne.workflowStatus.name'},
				{header: 'Effective On', width: 57, dataIndex: 'referenceOne.effectiveDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
				{header: 'Doc Updated On', width: 65, dataIndex: 'referenceOne.documentUpdateDate'},
				{header: 'Doc Updated By', width: 65, dataIndex: 'referenceOne.documentUpdateUser', hidden: true}
			],
			editor: {
				deleteURL: 'businessContractLinkDelete.json',
				detailPageClass: 'Clifton.business.contract.ContractWindow',
				getDetailPageId: function(grid, row) {
					return TCG.getValue('referenceOne.id', row.json);
				},

				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					const chId = gridPanel.getWindow().getMainFormId();
					const companyId = TCG.getValue('company.id', gridPanel.getWindow().getMainForm().formValues);
					toolBar.add(new TCG.form.ComboBox({name: 'con', url: 'businessContractListFind.json?amendmentType=false&companyId=' + companyId, displayField: 'label', emptyText: '< Select Contract >', width: 200, listWidth: 600}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Add selected document as amendment(s) to this document',
						iconCls: 'add',
						menu: new Ext.menu.Menu({
							items: [
								{
									text: 'Add',
									tooltip: 'Add selected document as amendment to this document',
									iconCls: 'add',
									handler: function() {
										const conId = TCG.getChildByName(toolBar, 'con').getValue();
										if (conId === '') {
											TCG.showError('You must first select desired document from the list.');
										}
										else {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Linking...',
												params: {parentId: conId, childId: chId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'con').reset();
												}
											});
											loader.load('businessContractLinkSave.json');
										}
									}
								},
								{
									text: 'Advanced Add',
									tooltip: 'Add selected document(s) as an amendment to this document',
									iconCls: 'add',
									scope: gridPanel,
									handler: function() {
										TCG.createComponent('Clifton.business.contract.BusinessContractSearchWindow', {
											amendmentType: false,
											openerCt: this
										});
									}
								}
							]
						})
					});
					toolBar.add('-');
				}
			},
			getLoadParams: function() {
				return {childId: this.getWindow().getMainFormId()};
			},
			processSelection: function(records) {
				if (records && records.length > 0) {
					const gridPanel = this;
					const grid = gridPanel.grid;
					grid.loading = true;

					const pIds = records.map(r => r.id);
					const chId = gridPanel.getWindow().getMainFormId();
					const loader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						waitMsg: 'Linking...',
						params: {
							parentIds: pIds,
							childId: chId
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('businessContractParentLinksSave.json');

				}
			}
		}]
	}
});
