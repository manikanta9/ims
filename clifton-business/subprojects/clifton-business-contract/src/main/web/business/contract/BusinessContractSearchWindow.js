TCG.use('Clifton.business.contract.BusinessContractGrid');

Clifton.business.contract.BusinessContractSearchWindow = Ext.extend(TCG.app.SelectWindow, {
	id: 'businessContractSearchWindow',
	width: 1100,
	height: 400,
	title: 'Select Business Documents',
	iconCls: 'contract',
	modal: true,
	allowOpenFromModal: true,

	items: [{
		name: 'businessContractExtendedListFind',
		xtype: 'businessContractGrid',
		editor: undefined,
		hideToolsMenu: true,
		instructions: 'Add selected document amendments to this contract',
		rowSelectionModel: 'checkbox',
		columnOverrides: [
			{dataIndex: 'client.label', hidden: false}
		]
	}]
});

