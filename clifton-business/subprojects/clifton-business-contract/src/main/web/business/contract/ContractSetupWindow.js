TCG.use('Clifton.business.contract.BusinessContractGrid');

Clifton.business.contract.ContractSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'businessContractSetupWindow',
	title: 'Business Documents',
	iconCls: 'contract',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Documents',
				items: [{
					name: 'businessContractExtendedListFind',
					xtype: 'businessContractGrid'
				}]
			},


			{
				title: 'Document Templates',
				items: [{
					name: 'businessContractTemplateListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					importTableName: 'BusinessContractTemplate',
					instructions: 'Documents templates are approved files by document type that are used as a starting point for contract creation depending on the company\'s needs.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Template Name', width: 150, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Type', width: 50, dataIndex: 'contractType.name', filter: {searchFieldName: 'contractTypeName'}},
						{
							header: 'Format', width: 25, dataIndex: 'documentFileType', filter: false, align: 'center',
							renderer: function(ft, args, r) {
								return TCG.renderDocumentFileTypeIcon(ft);
							}
						},
						{header: 'Inactive', width: 40, type: 'boolean', dataIndex: 'inactive'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Warning Message', width: 150, dataIndex: 'warningMessage'},
						{header: 'Doc Updated On', width: 50, dataIndex: 'documentUpdateDate'},
						{header: 'Doc Updated By', width: 50, dataIndex: 'documentUpdateUser'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active templates
							this.setFilterValue('inactive', false);
						}
						return {};
					},
					editor: {
						ptype: 'documentAware-grideditor',
						tableName: 'BusinessContractTemplate',
						entityText: 'contract template',
						detailPageClass: 'Clifton.business.contract.TemplateWindow'
					}
				}]
			},


			{
				title: 'Document Types',
				items: [{
					name: 'businessContractTypeListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					importTableName: 'BusinessContractType',
					instructions: 'Documents Types are used to classify business documents, organize templates, and define their applicable clauses.',

					groupField: 'categoryName',
					groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Types" : "Type"]})',

					columns: [
						{header: 'Category', width: 75, hidden: true, dataIndex: 'categoryName'},
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Document Type Name', width: 100, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Amendment', width: 30, dataIndex: 'amendment', type: 'boolean'},
						{
							header: 'Restricted', width: 30, dataIndex: 'restricted', type: 'boolean',
							tooltip: 'User will be required to have Full Control access to Contracts (and additional security resource if selected) in order to download/view the attached document'
						},
						{
							header: 'Additional Security', width: 80, dataIndex: 'additionalSecurityResource.labelExpanded', filter: {searchFieldName: 'additionalSecurityResourceName'},
							tooltip: 'In addition to access to contracts, user will also be required to have access to the additional security resource in order to modify the contract or the attached document.'
						},
						{
							header: 'Copy Contract Dates', width: 50, dataIndex: 'copyContractDates', type: 'boolean', hidden: true,
							tooltip: 'Used to determine if dates on the contract should be copied to the new contract when creating revisions.'
						},
						{header: 'Description', width: 250, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.contract.TypeWindow',
						copyURL: 'businessContractTypeCopy.json'
					}
				}]
			},


			{
				title: 'Parties',
				items: [{
					name: 'businessContractPartyListFind',
					xtype: 'gridpanel',
					instructions: 'Use the following to search for parties across all contracts.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Category', xtype: 'combo', width: 130, name: 'categoryName', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Contract Type Categories',
								linkedFilter: 'contract.contractType.categoryName',
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category', width: 60, hidden: true, dataIndex: 'contract.contractType.categoryName', filter: {type: 'combo', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Contract Type Categories', searchFieldName: 'categoryName'}},
						{header: 'Type', width: 85, dataIndex: 'contract.contractType.name', filter: {type: 'combo', searchFieldName: 'contractTypeId', url: 'businessContractTypeListFind.json'}, defaultSortColumn: true},
						{header: 'Contract Company', width: 125, dataIndex: 'contract.company.name', hidden: true, filter: {searchFieldName: 'contractCompanyId', type: 'combo', url: 'businessCompanyListFind.json?contractAllowed=true', displayField: 'nameExpandedWithType'}},
						{header: 'Contract', width: 150, dataIndex: 'contract.name', filter: {searchFieldName: 'contractName'}},
						{header: 'Role', width: 60, dataIndex: 'role.name', filter: {searchFieldName: 'roleName'}},
						{header: 'Party', width: 150, dataIndex: 'labelShort', filter: {searchFieldName: 'partyName'}, sortable: false},
						{header: 'Company', width: 125, hidden: true, dataIndex: 'company.nameExpandedWithType', filter: {searchFieldName: 'companyName'}},
						{header: 'Contact', width: 125, hidden: true, dataIndex: 'contact.label', filter: {searchFieldName: 'contactId', displayField: 'label', type: 'combo', url: 'businessContactListFind.json'}},
						{header: 'Comments', width: 150, hidden: true, dataIndex: 'comments'},
						{header: 'Workflow State', width: 60, hidden: true, dataIndex: 'contract.workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Workflow Status', width: 60, dataIndex: 'contract.workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=BusinessContract', showNotEquals: true}},
						{header: 'Effective On', width: 48, hidden: true, dataIndex: 'contract.effectiveDate', filter: {searchFieldName: 'effectiveDate'}}
					],
					editor: {
						detailPageClass: 'Clifton.business.contract.PartyWindow'
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const st = TCG.data.getData('workflowStatusByName.json?name=Non-Operational', this, 'workflow.status.Non-Operational');
							if (st) {
								this.setFilterValue('contract.workflowStatus.name', {'not_equals': {value: st.id, text: st.name}});
							}
						}
					}
				}]
			},


			{
				title: 'Party Roles',
				items: [{
					name: 'businessContractPartyRoleListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					instructions: 'Business Contract Party Roles provide classifications for parties involved in a contract.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Role', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', filter: false, sortable: false}
					],
					editor: {
						detailPageClass: 'Clifton.business.contract.PartyRoleWindow'
					}
				}]
			},


			{
				title: 'Clauses',
				name: 'businessContractClauseExtendedListFind',
				xtype: 'gridpanel',
				reloadOnRender: false,
				instructions: 'Use the toolbar filters to select a business document type and specific clause you are looking for.  The value column can be further filtered if looking for contracts with a specific value for that clause.  Only contracts with Clauses populated will be displayed.  By default contracts will be filtered on Active Clients and Active Contracts (i.e. Contract is not in Non-Operational status)',
				topToolbarSearchParameter: 'searchPattern',
				columns: [
					{header: 'ContractID', width: 20, dataIndex: 'contract.id', hidden: true, type: 'int', filter: {searchFieldName: 'contractId'}},
					{header: 'Order', width: 20, dataIndex: 'order', hidden: true, type: 'int', filter: {searchFieldName: 'order'}, defaultSortColumn: true, defaultSortDirection: 'asc'},
					{header: 'Company', width: 100, dataIndex: 'contract.company.name', filter: {searchFieldName: 'companyName'}, hidden: true},
					// NOTE: THIS IS AN EXTENDED DAO AND THE CLIENT IS POPULATED WHERE IT EXISTS SO CAN STILL SEARCH AGAINST THESE
					{header: 'Client Type', width: 100, dataIndex: 'client.clientType.text', hidden: true, filter: {searchFieldName: 'clientTypeName'}},
					{header: 'Client Relationship', width: 100, dataIndex: 'client.clientRelationship.name', hidden: true, filter: {searchFieldName: 'clientRelationshipName'}},

					{header: 'Document', width: 250, dataIndex: 'contract.label', filter: {searchFieldName: 'contractName'}},
					{header: 'Document Type', width: 75, dataIndex: 'contract.contractType.name', filter: {searchFieldName: 'contractTypeName'}, hidden: true},
					{header: 'Workflow State', width: 80, dataIndex: 'contract.workflowState.name', hidden: true, filter: {searchFieldName: 'workflowStateName'}},
					{header: 'Workflow Status', width: 80, dataIndex: 'contract.workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=BusinessContract'}},
					// NOTE: THIS IS AN EXTENDED DAO AND THE CLIENT IS POPULATED WHERE IT EXISTS SO CAN STILL SEARCH AGAINST THESE
					// THIS IS FILTERED BY DEFAULT, BUT IT IS HIDDEN, SO DOES A LEFT JOIN SO WE CAN STILL SEE OTHER COMPANY CONTRACTS THAT AREN'T CLIENTS
					{header: 'Active Client', width: 60, sortable: false, dataIndex: 'client.active', type: 'boolean', filter: {searchFieldName: 'clientActive'}},
					{header: 'Active', width: 40, sortable: false, dataIndex: 'contract.active', type: 'boolean', filter: {searchFieldName: 'active'}},
					{header: 'Clause', width: 150, dataIndex: 'column.name', filter: {searchFieldName: 'columnName'}},
					{header: 'Data Type', width: 50, dataIndex: 'column.dataType.name', hidden: true, filter: false, sortable: false},
					{
						header: 'Value', width: 170, dataIndex: 'value.text', filter: {searchFieldName: 'valueOrText'},
						renderer: function(v, c, r) {
							if (r.data['column.dataType.name'] === 'BOOLEAN') {
								if (TCG.isNull(v)) {
									return '';
								}
								if (TCG.isTrue(v) || v === 'Yes') {
									return 'Yes';
								}
								return 'No';
							}
							return v;
						}
					}
				],
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Document Type', xtype: 'combo', name: 'contractTypeId', width: 220, url: 'businessContractTypeListFind.json'},
						{
							fieldLabel: 'Clause', xtype: 'toolbar-combo', name: 'columnId', width: 220, url: 'systemColumnCustomListFind.json',
							displayField: 'customLabel',
							beforequery: function(queryEvent) {
								const cmb = queryEvent.combo;
								cmb.store.baseParams = {
									tableName: 'BusinessContract',
									customOnly: true
								};
								const ct = TCG.getChildByName(toolbar, 'contractTypeId');
								if (TCG.isNotBlank(ct.getValue())) {
									cmb.store.baseParams.linkedValue = ct.getValue();
									cmb.store.baseParams.includeNullLinkedValue = true;
								}
							}
						},
						{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
					];
				},

				editor: {
					drillDownOnly: true,
					detailPageClass: 'Clifton.business.contract.ContractCustomColumnWindow',
					getDetailPageId: function(gridPanel, row) {
						return row.json.contract.id;
					}
				},
				getTopToolbarInitialLoadParams: function(firstLoad) {
					if (firstLoad) {
						// default to active
						this.setFilterValue('client.active', true);
						this.setFilterValue('contract.active', true);
					}
					const params = {};
					const t = this.getTopToolbar();
					const ct = TCG.getChildByName(t, 'contractTypeId');
					const column = TCG.getChildByName(t, 'columnId');
					if (TCG.isNotBlank(ct.getValue())) {
						params.contractTypeId = ct.getValue();
					}
					if (TCG.isNotBlank(column.getValue())) {
						params.columnId = column.getValue();
					}
					params.populatedValuesOnly = true;
					return params;
				}
			}
		]
	}]
});

