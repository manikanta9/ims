Clifton.business.contract.ContractCopyWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Revise Business Document',
	iconCls: 'contract',
	height: 500,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		url: 'businessContract.json',
		instructions: 'Please select the items that should be copied from the original document to the new document.',
		items: [
			{fieldLabel: 'Company', name: 'company.name', xtype: 'displayfield', submitValue: false},
			{fieldLabel: 'Document', name: 'label', xtype: 'displayfield', submitValue: false},
			{fieldLabel: 'Template', name: 'contractTemplate.name', xtype: 'displayfield', submitValue: false},
			{fieldLabel: 'Name', name: 'name', maxLength: 100},
			{boxLabel: 'Copy clause values from original document', name: 'copyContractClauses', xtype: 'checkbox', checked: true},
			{boxLabel: 'Copy parties from original document', name: 'copyContractParties', xtype: 'checkbox', checked: true},
			{
				xtype: 'fieldset',
				collapsible: false,
				title: 'Document File Copy Options',
				labelWidth: 1,
				instructions: 'The initial file attached to this contract can either be created as a copy of the original document\'s file, or a template can be used as the initial document.  If you choose to use a template, but wish to use a different template than the one defined for the original, you may select that here.',
				items: [
					{boxLabel: 'Copy Document\'s File as Initial File', xtype: 'radio', name: 'copyContractDocument', inputValue: 'true', checked: true},
					{xtype: 'label', html: '<hr/>'},
					{boxLabel: 'Use Template as Initial Document\'s File', xtype: 'radio', name: 'copyContractDocument', inputValue: 'false'},
					{boxLabel: 'Select New Document Template', xtype: 'checkbox', name: 'selectNewTemplate'},
					{
						fieldLabel: '', hiddenName: 'templateId', xtype: 'combo', requiredFields: ['selectNewTemplate'], url: 'businessContractTemplateListFind.json?inactive=false', detailPageClass: 'Clifton.business.contract.TemplateWindow',
						beforequery: function(queryEvent) {
							const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
							queryEvent.combo.store.baseParams = {contractTypeId: TCG.getValue('contractType.id', form.formValues)};
						},
						getDefaultData: function(f) { // defaults contract type for "add new" drill down
							const tId = TCG.getValue('contractType.id', f.formValues);
							const tName = TCG.getValue('contractType.name', f.formValues);
							return {
								contractType: {
									id: tId,
									name: tName
								}
							};
						}
					}
				]
			}
		],

		getSaveURL: function() {
			return 'businessContractCopy.json';
		}
	}]
});
