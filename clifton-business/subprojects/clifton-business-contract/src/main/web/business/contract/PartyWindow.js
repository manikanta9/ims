Clifton.business.contract.PartyWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Contract Party',
	width: 700,

	items: [{
		xtype: 'formpanel',
		instructions: 'Contract Party associates a contract with a company or contract as a party to the contract.',
		url: 'businessContractParty.json',
		items: [
			{fieldLabel: 'Contract Company', name: 'contract.company.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'contract.company.id'},
			{fieldLabel: 'Contract', name: 'contract.label', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'contract.id'},
			{fieldLabel: 'Company', name: 'company.nameExpandedWithType', hiddenName: 'company.id', xtype: 'combo', displayField: 'nameExpandedWithType', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', mutuallyExclusiveFields: ['contact.id']},
			{fieldLabel: 'Contact', name: 'contact.label', hiddenName: 'contact.id', displayField: 'label', xtype: 'combo', url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow', mutuallyExclusiveFields: ['company.id']},
			{fieldLabel: 'Role', name: 'role.name', hiddenName: 'role.id', xtype: 'combo', url: 'businessContractPartyRoleListFind.json', detailPageClass: 'Clifton.business.contract.PartyRoleWindow'},
			{fieldLabel: 'Comments', name: 'comments', xtype: 'textarea'}
		]
	}]
});
