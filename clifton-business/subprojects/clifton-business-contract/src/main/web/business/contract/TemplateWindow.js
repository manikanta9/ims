Clifton.business.contract.TemplateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Document Template',
	width: 800,
	height: 500,
	iconCls: 'contract',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Document Template',
				tbar: {
					xtype: 'document-toolbar',
					tableName: 'BusinessContractTemplate'
				},
				items: [{
					xtype: 'formpanel',
					instructions: 'Document templates are approved files that are used to create specific contracts of a given type.',
					url: 'businessContractTemplate.json',
					labelWidth: 150,
					items: [
						{fieldLabel: 'Document Type', name: 'contractType.name', hiddenName: 'contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json', detailPageClass: 'Clifton.business.contract.TypeWindow'},
						{fieldLabel: 'Template Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Warning Message', name: 'warningMessage', xtype: 'textarea',
							qtip: 'Optional warning message this is displayed on Contract windows for this template when in <b>Pending Status</b> only.'
						},
						{fieldLabel: 'Inactive', name: 'inactive', xtype: 'checkbox'},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Document Updated By', xtype: 'compositefield',
							items: [
								{name: 'documentUpdateUser', xtype: 'displayfield', flex: 1},
								{value: 'On:', xtype: 'displayfield', width: 50},
								{name: 'documentUpdateDate', xtype: 'displayfield', flex: 2}
							]
						}
					]
				}]
			},


			{
				title: 'Revision History',
				items: [{
					xtype: 'document-version-grid',
					tableName: 'BusinessContractTemplate'
				}]
			}
		]
	}]
});
