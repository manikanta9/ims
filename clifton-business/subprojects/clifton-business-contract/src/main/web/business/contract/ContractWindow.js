// not the actual window but a window selector based on contract type is amendment
Clifton.business.contract.ContractWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'businessContract.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.business.contract.MainContractWindow';
		if ((entity && entity.contractType.amendment === true) || (config.defaultData && config.defaultData.contractType.amendment === true)) {
			className = 'Clifton.business.contract.AmendmentContractWindow';
		}
		return className;
	}
});
