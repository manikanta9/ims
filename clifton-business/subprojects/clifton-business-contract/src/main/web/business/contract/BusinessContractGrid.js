Clifton.business.contract.BusinessContractGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'businessContractExtendedListFind',
	xtype: 'gridpanel',
	importTableName: 'BusinessContract',
	instructions: 'The following business documents exist in the system across all companies.  If a file reference exists for a contract, click to highlight the contract and click the Download button to download the actual file.',
	topToolbarSearchParameter: 'searchPattern',
	reloadOnRender: false,

	groupField: 'groupValue',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Contracts" : "Contract"]})',
	groupMode: 'display',
	remoteSort: true,

	columns: [
		// Hidden Columns
		{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
		{
			header: 'Group', width: 100, hidden: true, dataIndex: 'groupValue', groupingOn: 'Category', filter: false, sortable: false,
			renderer: function(v, metaData, r) {
				if (this.groupingOn === 'Category') {
					return r.data['contractType.categoryName'];
				}
				if (TCG.isTrue(r.data['amendmentExists']) || TCG.isTrue(r.data['amendment'])) {
					return r.data['contractType.categoryName'] + ': ' + r.data['mainContract.name'];
				}
				return r.data['contractType.categoryName'];
			}
		},
		{header: 'Main Document Name', width: 100, dataIndex: 'mainContract.name', filter: false, hidden: true},
		{header: 'Amendment', width: 100, dataIndex: 'amendment', type: 'boolean', filter: false, hidden: true},

		{header: 'Company', width: 100, dataIndex: 'company.name', hidden: true, filter: {type: 'combo', searchFieldName: 'companyId', url: 'businessCompanyListFind.json?contractAllowed=true', sortFieldName: 'companyName'}},
		{header: 'Company Type', width: 60, hidden: true, dataIndex: 'company.type.name', filter: {searchFieldName: 'companyTypeName'}},
		{header: 'Client', width: 100, dataIndex: 'client.label', hidden: true, filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'label', url: 'businessClientListFind.json'}},
		{header: 'Client Type', width: 100, dataIndex: 'client.clientType.text', hidden: true, filter: {searchFieldName: 'clientTypeName'}},
		{header: 'Revised By', width: 100, dataIndex: 'parent.label', filter: false, hidden: true},
		{header: 'Amendment Type', width: 70, dataIndex: 'contractType.amendment', type: 'boolean', hidden: true, filter: {searchFieldName: 'amendmentType'}},

		// Visible Columns
		{header: 'Category', width: 140, hidden: true, dataIndex: 'contractType.categoryName', filter: {type: 'combo', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Contract Type Categories', searchFieldName: 'categoryName'}},
		{header: 'Document Type', width: 60, dataIndex: 'contractType.name', filter: {type: 'combo', searchFieldName: 'contractTypeId', url: 'businessContractTypeListFind.json'}},
		{
			header: 'Format', width: 30, dataIndex: 'documentFileType', filter: false, align: 'center',
			renderer: function(ft, args, r) {
				return TCG.renderDocumentFileTypeIcon(ft);
			}
		},
		{header: 'Template', width: 90, dataIndex: 'contractTemplate.name', filter: {type: 'combo', searchFieldName: 'contractTemplateId', url: 'businessContractTemplateListFind.json'}},
		{
			header: 'Name', width: 110, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
			renderer: function(v, metaData, r) {
				const note = r.data['description'];
				if (TCG.isNotBlank(note)) {
					const qtip = r.data['description'];
					metaData.css = 'amountAdjusted';
					metaData.attr = TCG.renderQtip(qtip);
				}
				return v;
			}
		},
		{header: 'Comments', hidden: true, width: 200, dataIndex: 'description', filter: false, sortable: false},
		{header: 'Workflow State', width: 80, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
		{header: 'Workflow Status', width: 58, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
		{header: 'Effective On', width: 48, dataIndex: 'effectiveDate'},
		{header: 'Doc Updated On', width: 65, dataIndex: 'documentUpdateDate'},
		{header: 'Doc Updated By', width: 55, dataIndex: 'documentUpdateUser'},
		{header: 'Amended', width: 38, dataIndex: 'amendmentExists', type: 'boolean'},


		// Hidden
		{
			header: 'Restricted Doc', width: 50, dataIndex: 'documentRestrictionsExist', type: 'boolean', hidden: true,
			tooltip: 'If checked, there are explicit document restrictions entered for users and/or groups'
		},
		{header: 'Active', width: 50, dataIndex: 'active', hidden: true, type: 'boolean', filter: {searchFieldName: 'active'}, sortable: false},
		{header: 'Active Client', width: 50, dataIndex: 'client.active', hidden: true, type: 'boolean', filter: {searchFieldName: 'clientActive'}, sortable: false},
		{header: 'Signed On', width: 50, dataIndex: 'signedDate', hidden: true},
		{header: 'Comments', dataIndex: 'description', hidden: true}
	],

	onRender: function() {
		Clifton.business.contract.BusinessContractGrid.superclass.onRender.apply(this, arguments);

		const grid = this;
		const displayCombo = TCG.getChildByName(grid.getTopToolbar(), 'displayType');

		if (TCG.isNotNull(this.getWindow().amendmentType)) {
			const displayValue = this.getWindow().amendmentType === true ? 'AMENDMENT' : 'MAIN';
			displayCombo.setValue(displayValue);
			displayCombo.setDisabled(true);
		}
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{
				fieldLabel: 'Category', xtype: 'combo', width: 130, name: 'categoryName', displayField: 'text', tooltipField: 'tooltip', valueField: 'value', url: 'systemListItemListFind.json?listName=Contract Type Categories',
				linkedFilter: 'contractType.categoryName',
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			},
			{
				fieldLabel: 'Company', xtype: 'combo', name: 'companyId', width: 150, url: 'businessCompanyListFind.json?contractAllowed=true', linkedFilter: 'company.name',
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			},
			{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'},
			{
				fieldLabel: 'Display', xtype: 'combo', name: 'displayType', width: 130, minListWidth: 230, mode: 'local', emptyText: '< All Contracts >', displayField: 'name', valueField: 'value',
				store: new Ext.data.ArrayStore({
					fields: ['value', 'name', 'description'],
					data: [['ALL', 'Display All Contracts'], ['ALL-GROUPED', 'All Contracts (Group Main with Amendments)'], ['MAIN', 'Main Contracts Only', 'Display main contracts only'], ['AMENDMENT', 'Amendments Only', 'Display amendments only']]
				}),
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const grid = this;
		const displayCombo = TCG.getChildByName(grid.getTopToolbar(), 'displayType');

		if (firstLoad) {
			// default to active clients & contracts
			this.setFilterValue('client.active', true);
			this.setFilterValue('active', true);
		}

		const dType = displayCombo.getValue();

		const i = this.grid.getColumnModel().findColumnIndex('groupValue');
		if (dType === 'ALL-GROUPED') {
			this.grid.getColumnModel().config[i].groupingOn = 'Related Contracts';
		}
		else {
			this.grid.getColumnModel().config[i].groupingOn = 'Category';
		}


		if (dType === 'MAIN') {
			return {requestedMaxDepth: 4, amendmentType: false};
		}
		else if (dType === 'AMENDMENT') {
			return {requestedMaxDepth: 4, amendmentType: true};
		}
		return {requestedMaxDepth: 4};
	},
	editor: {
		ptype: 'documentAware-grideditor',
		tableName: 'BusinessContract',
		entityText: 'contract',

		detailPageClass: {
			getItemText: function(rowItem) {
				return TCG.isTrue(rowItem.get('contractType.amendment')) ? 'Amendment' : 'Main';
			},
			items: [
				{text: 'Main', iconCls: 'contract', className: 'Clifton.business.contract.MainContractWindow'},
				{text: 'Amendment', iconCls: 'contract', className: 'Clifton.business.contract.AmendmentContractWindow'}
			]
		},
		deleteURL: 'businessContractDelete.json',
		copyURL: 'businessContractCopy.json',
		copyButtonName: 'Revise Document',
		copyButtonTooltip: 'Creates a copy of the selected document as a revision.',
		copyHandler: function(gridPanel) {
			const editor = this;

			const grid = this.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select a row to be copied.', 'No Row(s) Selected');
				return;
			}
			else if (sm.getCount() !== 1) {
				TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
				return;
			}
			Ext.Msg.confirm('Revise Selected Document', 'Are you sure you would like to revise the selected document?', function(a) {
				if (a === 'yes') {
					const id = sm.getSelected().id;

					editor.openDetailPage('Clifton.business.contract.ContractCopyWindow', editor.getGridPanel(), id);
				}
			});
		}
	}
});
Ext.reg('businessContractGrid', Clifton.business.contract.BusinessContractGrid);
