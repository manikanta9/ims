Clifton.business.contract.BasicContractWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Document',
	width: 1000,
	height: 600,
	iconCls: 'contract',
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Document',
				tbar: {
					xtype: 'document-toolbar',
					tableName: 'BusinessContract',
					showPDFButton: true,
					addToolbarButtons: function() {
						const toolbar = this;
						this.items.push({
							text: 'Post',
							tooltip: 'Post this contract to the Client Website (Document will first be converted to PDF)',
							name: 'postButton',
							iconCls: 'www',
							hidden: true,
							enabledFor: ['checkout', 'view', 'checkin'],
							handler: function() {
								toolbar.postContract();
							},
							validateEnabled: function(button, type) {
								// First - is the post button available given current type
								let enabled = (button.enabledFor.indexOf(type) >= 0);
								// if yes, then validate if the company.type.name = 'Client'
								if (enabled === true) {
									const f = toolbar.ownerCt.items.get(0);
									const companyType = TCG.getValue('company.type.name', f.getForm().formValues);
									if (companyType !== 'Client' && companyType !== 'Client Group' && companyType !== 'Virtual Client' && companyType !== 'Commingled Vehicle' && companyType !== 'Client Relationship') {
										enabled = false;
									}
								}
								return enabled;
							}

						});
						this.items.push({xtype: 'tbseparator', hidden: true});
					},
					postContract: function() {
						const f = this.ownerCt.items.get(0);
						const w = f.getWindow();

						const companyType = TCG.getValue('company.type.name', f.getForm().formValues);
						const company = TCG.getValue('company.id', f.getForm().formValues);

						const defaultData = {};
						defaultData.reportPostingFileConfiguration = {};

						if (companyType === 'Client Relationship') {
							const clientRelationship = TCG.data.getData('businessClientRelationshipByCompany.json?companyId=' + company, this);
							if (!clientRelationship) {
								TCG.showError('Unable to find Client Relationship for [' + companyType + '] Company with ID [' + company + ']');
								return;
							}
							defaultData.businessClientRelationshipId = clientRelationship.id;
							defaultData.reportPostingFileConfiguration.postEntitySourceTableName = 'BusinessClientRelationship';
							defaultData.reportPostingFileConfiguration.postEntitySourceFkFieldId = clientRelationship.id;
						}
						else {
							const client = TCG.data.getData('businessClientByCompany.json?companyId=' + company, this);
							if (!client) {
								TCG.showError('Unable to find Client for [' + companyType + '] Company with ID [' + company + ']');
								return;
							}
							defaultData.businessClientId = client.id;
							defaultData.reportPostingFileConfiguration.postEntitySourceTableName = 'BusinessClient';
							defaultData.reportPostingFileConfiguration.postEntitySourceFkFieldId = client.id;
						}
						defaultData.documentName = TCG.getValue('name', f.getForm().formValues);
						defaultData.documentId = this.getDocumentId();

						const clz = 'Clifton.report.posting.ReportPostingWindow';
						const id = w.getMainFormId();
						const cmpId = TCG.getComponentId(clz, id);

						TCG.createComponent(clz, {
							id: cmpId,
							postingType: 'Contract',
							defaultData: defaultData,
							openerCt: this
						});
					}
				},
				items: [{
					xtype: 'formpanel-custom-fields',
					columnGroupName: 'Business Contract Custom Fields',
					url: 'businessContract.json',

					getDefaultData: function(win) {
						if (!win.defaultData) {
							win.defaultData = {};
						}

						if (win.defaultData.company && win.defaultData.company.type) {
							if (win.defaultData.company.type.systemDefined === false) {
								win.defaultData.companyNameForDocument = win.defaultData.company.name;
							}
							else {
								win.defaultData.companyNameForDocument = win.defaultData.company.label;
							}
						}
						return win.defaultData;
					},

					getWarningMessage: function(f) {
						// Note: Because we only show the template warning during pending status, would only have
						// one or the other.  if we ever open template warning message to published documents,
						// then need to change this code to support either/or or both messages
						const templateWarning = TCG.getValue('contractTemplate.warningMessage', f.formValues);
						const wfStatus = TCG.getValue('workflowStatus.name', f.formValues);
						if (TCG.isNotBlank(templateWarning) && wfStatus === 'Pending') {
							return templateWarning;
						}
						const pName = f.findField('parentLabel').getValue();
						if (TCG.isNotBlank(pName)) {
							return [
								{xtype: 'label', html: 'This contract has been revised by&nbsp;'},
								{xtype: 'linkfield', style: 'padding: 0', value: pName, detailPageClass: 'Clifton.business.contract.ContractWindow', detailIdField: 'parent.id'}
							];
						}
						return undefined;
					},
					instructions: 'You cannot change the type of an existing contract if it has existing clauses.  If a document is not associated with this contract, and a template is selected, the initial document will be created as a copy of the template.',
					labelWidth: 150,
					items: [
						{name: 'parent.id', xtype: 'hidden'},
						{name: 'parentLabel', xtype: 'hidden'},
						{name: 'label', xtype: 'hidden'},
						{name: 'company.type.name', xtype: 'hidden', submitValue: false},
						{
							fieldLabel: 'Company', name: 'company.nameExpandedWithType', hiddenName: 'company.id', xtype: 'combo', displayField: 'nameExpandedWithType', tooltipField: 'label', url: 'businessCompanyListFind.json?contractAllowed=true', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
							requestedProps: 'type.systemDefined|name',
							listeners: {
								select: function(combo, record, index) {
									// update company alias and counterparty fields from selected ISDA contract
									const fp = combo.getParentForm();
									const form = fp.getForm();
									const companyLabelField = form.findField('companyNameForDocument');
									const company = record.json;
									let companyLabel = company.label;
									// Company Label would be coalesce alias, name - for system defined companies (client, client relationships, etc) the alias is the "legal name" for
									// others it is a trading alias.  if it's not system defined, just use the company name
									if (company.type.systemDefined === false) {
										companyLabel = company.name;
									}
									companyLabelField.setValue(companyLabel);

									const nameField = form.findField('name');

									if (TCG.isBlank(nameField.getValue())) {
										const contractTypeField = form.findField('contractType.name');
										if (TCG.isNotBlank(contractTypeField.getValue())) {
											nameField.setValue(companyLabel + '-' + contractTypeField.getRawValue());
										}
									}
								}
							}
						},
						{name: 'companyNameForDocument', xtype: 'hidden'},
						{fieldLabel: 'Category', name: 'contractType.categoryName', xtype: 'system-list-combo', listName: 'Contract Type Categories'},
						{
							fieldLabel: 'Document Type', name: 'contractType.name', hiddenName: 'contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json', detailPageClass: 'Clifton.business.contract.TypeWindow',
							beforequery: function(queryEvent) {
								const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
								const contractTypeCategoryName = form.findField('contractType.categoryName');
								if (contractTypeCategoryName && TCG.isNotBlank(contractTypeCategoryName.value)) {
									queryEvent.combo.store.baseParams = {categoryName: contractTypeCategoryName.value};
								}
							},
							listeners: {
								// default Contract Name if blank
								select: function(combo, record, index) {
									const form = combo.getParentForm().getForm();
									const nameField = form.findField('name');

									if (TCG.isBlank(nameField.getValue())) {
										const companyField = form.findField('companyNameForDocument');
										if (TCG.isNotBlank(companyField.getValue())) {
											nameField.setValue(companyField.getValue() + '-' + combo.getRawValue());
										}
									}
								}
							}
						},
						{
							fieldLabel: 'Document Template', name: 'contractTemplate.name', hiddenName: 'contractTemplate.id', xtype: 'combo', url: 'businessContractTemplateListFind.json?inactive=false', detailPageClass: 'Clifton.business.contract.TemplateWindow',
							requiredFields: ['contractType.id'],
							beforequery: function(queryEvent) {
								const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
								queryEvent.combo.store.baseParams = {contractTypeId: form.findField('contractType.id').value};
							},
							getDefaultData: function(f) { // defaults contract type for "add new" drill down
								const t = f.findField('contractType.id');
								return {
									contractType: {
										id: t.getValue(),
										name: t.getRawValue()
									}
								};
							}
						},
						{fieldLabel: 'Name', name: 'name', requiredFields: ['company.id', 'contractType.id']},
						{fieldLabel: 'Workflow State', xtype: 'workflow-state-combo', tableName: 'BusinessContract'},
						{fieldLabel: 'Effective Date', name: 'effectiveDate', xtype: 'datefield'},
						{fieldLabel: 'Signed Date', name: 'signedDate', xtype: 'datefield'},
						{
							fieldLabel: 'Approved By', xtype: 'checkboxgroup', columns: 3, items: [
								{boxLabel: 'Internal', name: 'internalApproved'},
								{boxLabel: 'Legal', name: 'legalApproved'},
								{boxLabel: 'Client', name: 'clientApproved'}
							]
						},
						{fieldLabel: 'Comments', xtype: 'textarea', name: 'description'},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Document Updated By', xtype: 'compositefield',
							items: [
								{name: 'documentUpdateUser', xtype: 'displayfield', flex: 1},
								{value: 'On:', xtype: 'displayfield', width: 50},
								{name: 'documentUpdateDate', xtype: 'displayfield', flex: 2}
							]
						},
						{xtype: 'label', html: '<hr/>'}
					]
				}]
			},


			{
				title: 'Clauses',
				items: [{
					name: 'businessContractClauseExtendedListFind',
					xtype: 'gridpanel',
					instructions: 'The following clauses have been defined for this contract. Click on the Edit Clauses button to update this information.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'Order', width: 20, dataIndex: 'order', hidden: true, type: 'int', filter: {searchFieldName: 'order'}, defaultSortColumn: true, defaultSortDirection: 'asc'},
						{header: 'Clause Name', width: 300, dataIndex: 'column.name', filter: {searchFieldName: 'columnName'}},
						{header: 'Description', width: 500, dataIndex: 'column.description', filter: {searchFieldName: 'columnDescription'}, hidden: true},
						{header: 'Data Type', width: 100, dataIndex: 'column.dataType.name', hidden: true},
						{
							header: 'Value', width: 300, dataIndex: 'value.text', filter: {searchFieldName: 'valueOrText'},
							renderer: function(v, c, r) {
								if (r.data['column.dataType.name'] === 'BOOLEAN') {
									if (TCG.isNull(v)) {
										return '';
									}
									if (TCG.isTrue(v) || v === 'Yes') {
										return 'Yes';
									}
									return 'No';
								}
								return v;
							}
						}
					],
					editor: {
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Edit Clauses',
								tooltip: 'Edit Contract Clauses',
								iconCls: 'pencil',
								handler: function() {
									const w = gridPanel.getWindow();
									const cid = w.getMainFormId();
									TCG.createComponent('Clifton.business.contract.ContractCustomColumnWindow', {
										id: TCG.getComponentId('Clifton.business.contract.ContractCustomColumnWindow', cid),
										params: {id: cid},
										openerCt: gridPanel
									});
								}
							}, '-');
							t.add({
								text: 'Copy Clauses',
								tooltip: 'Populate clauses by copying from another contract.',
								iconCls: 'copy',
								handler: function() {
									const w = gridPanel.getWindow();
									const form = w.getMainForm();
									const cid = w.getMainFormId();
									const defaultData = {
										copyFromType: form.formValues.contractType
									};
									TCG.createComponent('Clifton.business.contract.ContractCustomColumnCopyWindow', {
										id: TCG.getComponentId('Clifton.business.contract.ContractCustomColumnCopyWindow', cid),
										params: {id: cid},
										defaultData: defaultData,
										openerCt: gridPanel
									});
								}
							}, '-');
						}
					},
					getTopToolbarInitialLoadParams: function() {
						return {contractId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Parties',
				items: [{
					xtype: 'gridpanel',
					name: 'businessContractPartyListByContract',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Party', width: 125, dataIndex: 'labelShort'},
						{header: 'Role', width: 100, dataIndex: 'role.name'},
						{header: 'Comments', width: 100, dataIndex: 'comments'}

					],
					editor: {
						detailPageClass: 'Clifton.business.contract.PartyWindow',
						getDefaultData: function(gridPanel) { // defaults contract id for the detail page
							return {
								contract: gridPanel.getWindow().getMainForm().formValues
							};
						},
						deleteURL: 'businessContractPartyDelete.json'
					},
					getLoadParams: function() {
						return {contractId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'BusinessContract',
					showOrderInfo: true,
					showCreateDate: true
				}]
			},


			{
				title: 'Workflow History',
				items: [{
					xtype: 'workflow-history-grid',
					tableName: 'BusinessContract'
				}]
			},


			{
				title: 'Revision History',
				items: [{
					xtype: 'document-version-grid',
					tableName: 'BusinessContract'
				}]
			}
		]
	}]
});
