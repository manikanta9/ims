TCG.use('Clifton.business.contract.BusinessContractSearchWindow');
TCG.use('Clifton.business.contract.BasicContractWindow');

// adds "Amendments" tab at the end of BasicCompanyWindow
Clifton.business.contract.MainContractWindow = Ext.extend(Clifton.business.contract.BasicContractWindow, {

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		tabs.add(this.linkedContractsTab);
	},


	linkedContractsTab: {
		title: 'Amendments',
		items: [{
			xtype: 'gridpanel',
			name: 'businessContractLinkListByParent',
			instructions: 'The following amendments have been linked to this document.',
			columns: [
				{header: 'ID', dataIndex: 'id', hidden: true},
				{header: 'ContractID', dataIndex: 'referenceTwo.id', hidden: true},
				{header: 'Type', width: 50, dataIndex: 'referenceTwo.contractType.name'},
				{
					header: 'Format', width: 30, dataIndex: 'referenceTwo.documentFileType', filter: false, align: 'center',
					renderer: function(ft, args, r) {
						return TCG.renderDocumentFileTypeIcon(ft);
					}
				},
				{header: 'Company', dataIndex: 'referenceTwo.company.nameWithType'},
				{header: 'Template', width: 100, hidden: true, dataIndex: 'referenceTwo.contractTemplate.name'},
				{header: 'Document', width: 170, dataIndex: 'referenceTwo.name'},
				{header: 'Workflow State', width: 60, dataIndex: 'referenceTwo.workflowState.name'},
				{header: 'Workflow Status', width: 60, dataIndex: 'referenceTwo.workflowStatus.name'},
				{header: 'Effective On', width: 57, dataIndex: 'referenceTwo.effectiveDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
				{header: 'Doc Updated On', width: 65, dataIndex: 'referenceTwo.documentUpdateDate'},
				{header: 'Doc Updated By', width: 65, dataIndex: 'referenceTwo.documentUpdateUser', hidden: true}
			],
			editor: {
				deleteURL: 'businessContractLinkDelete.json',
				detailPageClass: 'Clifton.business.contract.ContractWindow',
				getDetailPageId: function(grid, row) {
					return TCG.getValue('referenceTwo.id', row.json);
				},
				addToolbarAddButton: function(toolBar) {
					const gridPanel = this.getGridPanel();
					const parId = gridPanel.getWindow().getMainFormId();
					const companyId = TCG.getValue('company.id', gridPanel.getWindow().getMainForm().formValues);
					toolBar.add(new TCG.form.ComboBox({name: 'con', url: 'businessContractListFind.json?amendmentType=true&companyId=' + companyId, displayField: 'label', emptyText: '< Select Contract >', width: 200, listWidth: 600}));
					toolBar.add({
						text: 'Add',
						tooltip: 'Add selected document amendments to this contract',
						iconCls: 'add',
						menu: new Ext.menu.Menu({
							items: [
								{
									text: 'Add',
									tooltip: 'Add selected document amendments to this contract',
									iconCls: 'add',
									handler: function() {
										const conId = TCG.getChildByName(toolBar, 'con').getValue();
										if (conId === '') {
											TCG.showError('You must first select desired document from the list.');
										}
										else {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Linking...',
												params: {parentId: parId, childId: conId},
												onLoad: function(record, conf) {
													gridPanel.reload();
													TCG.getChildByName(toolBar, 'con').reset();
												}
											});
											loader.load('businessContractLinkSave.json');
										}
									}
								},
								{
									text: 'Advanced Add',
									tooltip: 'Add selected document(s) as an amendment to this document',
									iconCls: 'add',
									scope: gridPanel,
									handler: function() {
										TCG.createComponent('Clifton.business.contract.BusinessContractSearchWindow', {
											amendmentType: true,
											openerCt: this
										});
									}
								}
							]
						})
					});
					toolBar.add('-');
				}
			},
			getLoadParams: function() {
				return {parentId: this.getWindow().getMainFormId()};
			},
			processSelection: function(records) {
				if (records && records.length > 0) {
					const gridPanel = this;
					const grid = gridPanel.grid;
					grid.loading = true;

					const cIds = records.map(r => r.id);
					const pId = gridPanel.getWindow().getMainFormId();
					const loader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						waitMsg: 'Linking...',
						params: {
							parentId: pId,
							childIds: cIds
						},
						onLoad: function(record, conf) {
							gridPanel.reload();
						}
					});
					loader.load('businessContractChildLinksSave.json');
				}
			}
		}]
	}
});
