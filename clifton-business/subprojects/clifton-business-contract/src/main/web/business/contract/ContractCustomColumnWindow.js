Clifton.business.contract.ContractCustomColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Document',
	iconCls: 'contract',
	width: 750,
	height: 600,

	title: 'Document Clauses',
	items: [{
		xtype: 'formpanel-custom-fields',
		labelFieldName: 'label',
		url: 'businessContract.json',
		getSaveURL: function() {
			return 'systemColumnValueListSave.json';
		},
		labelWidth: 250,
		loadValidation: false,

		items: [
			{name: 'columnGroupName', xtype: 'hidden', value: 'Contract Clauses'},
			{fieldLabel: 'Company', name: 'company.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'company.id', submitDetailField: false},
			{fieldLabel: 'Document Type', name: 'contractType.name', detailIdField: 'contractType.id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.TypeWindow', submitDetailField: false},
			{fieldLabel: 'Template', name: 'contractTemplate.name', detailIdField: 'contractTemplate.id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.TemplateWindow', submitDetailField: false},
			{fieldLabel: 'Document', name: 'name', detailIdField: 'id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contract.ContractWindow', submitDetailField: false},
			{xtype: 'label', html: '<hr/>'}
		]
	}]
});
