package com.clifton.business.contract.comparisons;


import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration("../BusinessContractServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class BusinessContractExistsComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private BusinessContractService businessContractService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private ContextHandler contextHandler;

	private static final short IMA_TYPE = 1;
	private static final short AUTHORIZATION_TYPE = 2;

	private static final int COMPANY_ARIZONA = 202;
	private static final int COMPANY_LIUNA_NATIONAL = 204;


	@BeforeEach
	public void resetTests() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@Test
	public void testContractExists() {
		BusinessContractExistsComparison existsComparison = new BusinessContractExistsComparison();
		BusinessContractNotExistsComparison notExistsComparison = new BusinessContractNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(existsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(notExistsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// Set up Contracts
		BusinessContract ima = setupContract(IMA_TYPE, COMPANY_ARIZONA);
		setupContract(AUTHORIZATION_TYPE, COMPANY_ARIZONA);
		// For an IMA - does authorization contract exist? 
		existsComparison.setCompanyBeanPropertyName("company.id");
		existsComparison.setContractTypeId(AUTHORIZATION_TYPE);
		notExistsComparison.setCompanyBeanPropertyName("company.id");
		notExistsComparison.setContractTypeId(AUTHORIZATION_TYPE);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(existsComparison.evaluate(ima, context));
		Assertions.assertEquals("(1 Contract(s) Found)", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(notExistsComparison.evaluate(ima, context));
		Assertions.assertEquals("(1 Contract(s) Found)", context.getFalseMessage());

		// For company - verify authorization contract exists
		existsComparison.setCompanyBeanPropertyName("id");
		existsComparison.setContractTypeId(AUTHORIZATION_TYPE);
		notExistsComparison.setCompanyBeanPropertyName("id");
		notExistsComparison.setContractTypeId(AUTHORIZATION_TYPE);

		context = new SimpleComparisonContext();
		Assertions.assertTrue(existsComparison.evaluate(this.businessCompanyService.getBusinessCompany(COMPANY_ARIZONA), context));
		Assertions.assertEquals("(1 Contract(s) Found)", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(notExistsComparison.evaluate(this.businessCompanyService.getBusinessCompany(COMPANY_ARIZONA), context));
		Assertions.assertEquals("(1 Contract(s) Found)", context.getFalseMessage());
	}


	@Test
	public void testContractNotExists() {
		BusinessContractExistsComparison existsComparison = new BusinessContractExistsComparison();
		BusinessContractNotExistsComparison notExistsComparison = new BusinessContractNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(existsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(notExistsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		// Set up Contracts
		BusinessContract ima = setupContract(IMA_TYPE, COMPANY_ARIZONA);
		setupContract(AUTHORIZATION_TYPE, COMPANY_LIUNA_NATIONAL);
		// For an IMA - does authorization contract exist? 
		existsComparison.setCompanyBeanPropertyName("company.id");
		existsComparison.setContractTypeId(AUTHORIZATION_TYPE);
		notExistsComparison.setCompanyBeanPropertyName("company.id");
		notExistsComparison.setContractTypeId(AUTHORIZATION_TYPE);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(existsComparison.evaluate(ima, context));
		Assertions.assertEquals("(0 Contract(s) Found)", context.getFalseMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(notExistsComparison.evaluate(ima, context));
		Assertions.assertEquals("(0 Contract(s) Found)", context.getTrueMessage());

		// For company - verify authorization contract exists
		existsComparison.setCompanyBeanPropertyName("id");
		existsComparison.setContractTypeId(AUTHORIZATION_TYPE);
		notExistsComparison.setCompanyBeanPropertyName("id");
		notExistsComparison.setContractTypeId(AUTHORIZATION_TYPE);

		context = new SimpleComparisonContext();
		Assertions.assertFalse(existsComparison.evaluate(this.businessCompanyService.getBusinessCompany(COMPANY_ARIZONA), context));
		Assertions.assertEquals("(0 Contract(s) Found)", context.getFalseMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(notExistsComparison.evaluate(this.businessCompanyService.getBusinessCompany(COMPANY_ARIZONA), context));
		Assertions.assertEquals("(0 Contract(s) Found)", context.getTrueMessage());
	}


	private BusinessContract setupContract(Short typeId, Integer companyId) {
		BusinessContract contract = new BusinessContract();
		contract.setContractType(this.businessContractService.getBusinessContractType(typeId));
		contract.setCompany(this.businessCompanyService.getBusinessCompany(companyId));
		contract.setName(contract.getContractType() + " " + contract.getCompany().getName());
		this.businessContractService.saveBusinessContract(contract);
		return contract;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
