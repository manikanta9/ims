package com.clifton.business.contract;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;


/**
 * @author danielh
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessContractProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "business-contract";
	}


	/**
	 * Can be overridden to skip automated testing for certain methods.
	 */
	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("getBusinessContractExtendedList");
		skipMethods.add("getBusinessContractClauseExtendedList");
		return skipMethods.contains(method.getName());
	}
}
