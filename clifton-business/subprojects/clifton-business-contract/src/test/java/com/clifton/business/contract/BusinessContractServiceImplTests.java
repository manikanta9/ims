package com.clifton.business.contract;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistoryService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessContractServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessContractServiceImplTests {

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private BusinessContractService businessContractService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowHistoryService workflowHistoryService;

	@Resource
	private ContextHandler contextHandler;

	/////////////////////////////////////////////////

	private static final short STATE_WAITING_APPROVAL = 202;
	private static final short STATE_APPROVED = 203;
	private static final short STATE_PUBLISHED = 204;


	/////////////////////////////////////////////////


	@BeforeEach
	public void resetTests() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@Test
	public void testSaveBusinessContract() {
		// Company 202 && Contract 1 (IMA)
		BusinessContract contract = new BusinessContract();
		contract.setCompany(this.businessCompanyService.getBusinessCompany(202));
		contract.setContractType(this.businessContractService.getBusinessContractType(MathUtils.SHORT_ONE));
		contract.setContractTemplate(this.businessContractService.getBusinessContractTemplate(MathUtils.SHORT_ONE));
		contract.setEffectiveDate(new Date());
		this.businessContractService.saveBusinessContract(contract);
	}


	@Test
	public void testBusinessContractWorkflow() {
		BusinessContract contract = new BusinessContract();
		contract.setCompany(this.businessCompanyService.getBusinessCompany(202));
		contract.setContractType(this.businessContractService.getBusinessContractType(MathUtils.SHORT_ONE));
		contract.setEffectiveDate(new Date());
		this.businessContractService.saveBusinessContract(contract);

		// Verify Initial Workflow Assignment
		verifyWorkflow(contract, "Draft", "Draft");

		// Move the Contract through the Workflow
		contract.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_WAITING_APPROVAL));
		this.businessContractService.saveBusinessContract(contract);
		verifyWorkflow(contract, "Waiting for Approval", "Pending");

		contract.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_APPROVED));
		this.businessContractService.saveBusinessContract(contract);
		verifyWorkflow(contract, "Approved", "Pending");

		// Should NOT be able to Publish Contract without a Signed Date or Effective Date
		contract.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_PUBLISHED));
		try {
			this.businessContractService.saveBusinessContract(contract);
		}
		catch (Exception e) {
			// Set the Signed Date & Effective Date, so now it can be published.
			contract.setSignedDate(new Date());
			contract.setEffectiveDate(new Date());
			this.businessContractService.saveBusinessContract(contract);
			verifyWorkflow(contract, "Published", "Final");

			// Create a Revision
			testCopyContract(contract);

			return;
		}
		Assertions.fail("Should have encountered an exception when trying to publish a contract without a signed date");
	}


	/**
	 * Tests creating a copy/revision of an existing contract.
	 *
	 * @param contract
	 */
	private void testCopyContract(BusinessContract contract) {
		verifyWorkflow(contract, "Published", "Final");

		// Create a new revision of this contract.  (Cannot copy clauses as custom fields as XML DAO needs to be changed to support subclasses)
		this.businessContractService.copyBusinessContract(contract.getId(), null, "Test Name", false, false, false);
		contract = this.businessContractService.getBusinessContract(contract.getId());

		// Verify workflow for the original
		verifyWorkflow(contract, "Revised", "Final");

		// Verify Parent is set to the new revision on the original
		Assertions.assertNotNull(contract.getParent());
		BusinessContract newContract = contract.getParent();

		// Verify Workflow of the new revision
		verifyWorkflow(newContract, "Draft", "Draft");

		// Verify copied properties
		Assertions.assertEquals(contract.getContractType(), newContract.getContractType());
		Assertions.assertEquals(contract.getContractTemplate(), newContract.getContractTemplate());

		// These properties shouldn't be copied since the type is not set to copy dates.
		Assertions.assertNull(newContract.getEffectiveDate());
		Assertions.assertNull(newContract.getSignedDate());

		// Move the New Contract Through the Workflow
		newContract.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_WAITING_APPROVAL));
		this.businessContractService.saveBusinessContract(newContract);
		verifyWorkflow(newContract, "Waiting for Approval", "Pending");

		newContract.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_APPROVED));
		this.businessContractService.saveBusinessContract(newContract);
		verifyWorkflow(newContract, "Approved", "Pending");

		// Set the Signed Date & Effective Date, so now it can be published.
		newContract.setSignedDate(new Date());
		newContract.setEffectiveDate(new Date());
		newContract.setWorkflowState(this.workflowDefinitionService.getWorkflowState(STATE_PUBLISHED));
		this.businessContractService.saveBusinessContract(newContract);
		verifyWorkflow(newContract, "Published", "Final");

		// Now that it is published, the original contract should have been auto moved to be in Non-Op State
		contract = this.businessContractService.getBusinessContract(contract.getId());
		verifyWorkflow(contract, "Non-Operational", "Final");
		List<WorkflowHistory> historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(contract, true);
		Assertions.assertEquals(6, CollectionUtils.getSize(historyList));

		historyList = this.workflowHistoryService.getWorkflowHistoryListByWorkflowAwareEntity(newContract, true);
		Assertions.assertEquals(4, CollectionUtils.getSize(historyList));

		// Try to create another revision, this time with option to copy the dates
		BusinessContractType type = newContract.getContractType();
		type.setCopyContractDates(true);
		this.businessContractService.saveBusinessContractType(type);

		BusinessContract newNewContract = this.businessContractService.copyBusinessContract(newContract.getId(), null, "Test Latest", false, false, false);
		Assertions.assertEquals(newContract.getEffectiveDate(), newNewContract.getEffectiveDate(), "Effective dates should be the same, because Type properties says to copy them.");
		Assertions.assertEquals(newContract.getSignedDate(), newNewContract.getSignedDate(), "Signed dates should be the same, because Type properties says to copy them.");

		// Try to update a non-op contract
		try {
			contract.setEffectiveDate(new Date());
			this.businessContractService.saveBusinessContract(contract);
		}
		catch (Exception e) {
			// Do nothing - expected exception
			return;
		}
		Assertions.fail("Expected exception when trying to edit a Non Operational contract - Should have failed because of Entity Modify Condition on Workflow State.");
	}


	private void verifyWorkflow(BusinessContract contract, String state, String status) {
		Assertions.assertEquals(state, contract.getWorkflowState().getName());
		Assertions.assertEquals(status, contract.getWorkflowStatus().getName());
	}


	@Test
	public void testUpdateContractNameOnCompanyNameChange() {
		// Arizona Company - ID = 202
		BusinessCompany company = this.businessCompanyService.getBusinessCompany(202);
		String originalCompanyName = company.getName();
		// Should be re-named
		BusinessContract contract1 = new BusinessContract();
		contract1.setCompany(company);
		// IMA Type = ID 1
		contract1.setContractType(this.businessContractService.getBusinessContractType(MathUtils.SHORT_ONE));
		contract1.setName(company.getName() + "-" + contract1.getContractType().getName());
		contract1.setDescription("Contract 1");
		this.businessContractService.saveBusinessContract(contract1);

		// Should NOT be renamed
		BusinessContract contract2 = new BusinessContract();
		contract2.setCompany(company);
		contract2.setContractType(this.businessContractService.getBusinessContractType(MathUtils.SHORT_ONE));
		contract2.setName("ACF-" + contract2.getContractType().getName() + "-V2");
		contract2.setDescription("Contract 2");
		this.businessContractService.saveBusinessContract(contract2);

		// Should be renamed
		BusinessContract contract3 = new BusinessContract();
		contract3.setCompany(company);
		// Authorization ID = 2
		contract3.setContractType(this.businessContractService.getBusinessContractType(MathUtils.SHORT_TWO));
		contract3.setName(contract3.getContractType().getName() + "-" + company.getName());
		contract3.setDescription("Contract 3");
		this.businessContractService.saveBusinessContract(contract3);

		// Update the Company Name - add , Inc. to the name
		String appendToCompanyName = " (ACF)";// ", Inc.";
		company.setName(company.getName() + appendToCompanyName);
		this.businessCompanyService.saveBusinessCompany(company);

		BusinessContractSearchForm searchForm = new BusinessContractSearchForm();
		searchForm.setCompanyId(company.getId());
		List<BusinessContract> list = this.businessContractService.getBusinessContractList(searchForm);

		for (BusinessContract c : list) {
			if ("Contract 1".equals(c.getDescription())) {
				Assertions.assertEquals(originalCompanyName + appendToCompanyName + "-IMA", c.getName());
			}
			if ("Contract 2".equals(c.getDescription())) {
				Assertions.assertEquals("ACF-IMA-V2", c.getName());
			}
			if ("Contract 3".equals(c.getDescription())) {
				Assertions.assertEquals("Authorization-" + originalCompanyName + appendToCompanyName, c.getName());
			}
		}

		// Change company name back
		company.setName(originalCompanyName);
		this.businessCompanyService.saveBusinessCompany(company);

		// Validate Contract Names are changed back
		searchForm = new BusinessContractSearchForm();
		searchForm.setCompanyId(company.getId());
		list = this.businessContractService.getBusinessContractList(searchForm);

		for (BusinessContract c : list) {
			if ("Contract 1".equals(c.getDescription())) {
				Assertions.assertEquals(originalCompanyName + "-IMA", c.getName());
			}
			if ("Contract 2".equals(c.getDescription())) {
				Assertions.assertEquals("ACF-IMA-V2", c.getName());
			}
			if ("Contract 3".equals(c.getDescription())) {
				Assertions.assertEquals("Authorization-" + originalCompanyName, c.getName());
			}
		}
	}
}
