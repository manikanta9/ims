package com.clifton.business.contract.comparisons;


import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration("../BusinessContractServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class BusinessContractAmendmentAndLinkExistsComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private BusinessContractService businessContractService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private ContextHandler contextHandler;

	private static final short IMA_TYPE = 1;
	private static final short IMA_AMENDMENT_TYPE = 10;


	@BeforeEach
	public void resetTests() {
		SecurityUser user = new SecurityUser();
		user.setId(new Integer(7).shortValue());
		user.setUserName("TestUser");
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}


	@Test
	public void testContractNoLinks() {
		BusinessContractAmendmentAndLinkExistsComparison comparison = new BusinessContractAmendmentAndLinkExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		BusinessContract contract = setupContract(false);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(contract, context));
		Assertions.assertEquals("(Contract Type Is Not An Amendment)", context.getTrueMessage());

		BusinessContract amend = setupContract(true);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(amend, context));
		Assertions.assertEquals("(Contract Type Is An Amendment and does not have a link to a main contract.)", context.getFalseMessage());
	}


	@Test
	public void testContractLinks() {
		BusinessContractAmendmentAndLinkExistsComparison comparison = new BusinessContractAmendmentAndLinkExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		BusinessContract contract = setupContract(false);
		BusinessContract amend = setupContract(true);

		this.businessContractService.saveBusinessContractLink(contract.getId(), amend.getId());

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(contract, context));
		Assertions.assertEquals("(Contract Type Is Not An Amendment)", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(amend, context));
		Assertions.assertEquals("(Contract Type Is An Amendment and is linked to a main contract.)", context.getTrueMessage());
	}


	private BusinessContract setupContract(boolean amendment) {
		BusinessContract contract = new BusinessContract();
		contract.setName(amendment ? "Test Amendment Contract" : "Test Contract");
		contract.setContractType(this.businessContractService.getBusinessContractType(amendment ? IMA_AMENDMENT_TYPE : IMA_TYPE));
		contract.setCompany(this.businessCompanyService.getBusinessCompany(202));
		this.businessContractService.saveBusinessContract(contract);
		return contract;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
