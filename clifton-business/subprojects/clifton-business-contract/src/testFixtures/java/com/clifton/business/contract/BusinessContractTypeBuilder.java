package com.clifton.business.contract;

import com.clifton.core.security.authorization.SecurityResourceBuilder;


public class BusinessContractTypeBuilder {

	private BusinessContractType businessContractType;


	private BusinessContractTypeBuilder(BusinessContractType businessContractType) {
		this.businessContractType = businessContractType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessContractTypeBuilder createContractor() {
		BusinessContractType businessContractType = new BusinessContractType();

		businessContractType.setId((short) 1);
		businessContractType.setName("Contractor");
		businessContractType.setDescription("A contractor hired by the company to perform temporary duties.");
		businessContractType.setCategoryName("Client");


		return new BusinessContractTypeBuilder(businessContractType);
	}


	public static BusinessContractTypeBuilder createISDA() {
		BusinessContractType businessContractType = new BusinessContractType();

		businessContractType.setId((short) 4);
		businessContractType.setName("ISDA");
		businessContractType.setDescription("International Swaps and Derivatives Agreement");
		businessContractType.setCategoryName("ISDA");
		businessContractType.setAdditionalSecurityResource(SecurityResourceBuilder.createClientDocuments().toSecurityResource());

		return new BusinessContractTypeBuilder(businessContractType);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractTypeBuilder withId(short id) {
		getBusinessContractType().setId(id);
		return this;
	}


	public BusinessContractTypeBuilder withName(String name) {
		getBusinessContractType().setName(name);
		return this;
	}


	public BusinessContractTypeBuilder withDescription(String description) {
		getBusinessContractType().setDescription(description);
		return this;
	}


	public BusinessContractType toBusinessContractType() {
		return this.businessContractType;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessContractType getBusinessContractType() {
		return this.businessContractType;
	}
}
