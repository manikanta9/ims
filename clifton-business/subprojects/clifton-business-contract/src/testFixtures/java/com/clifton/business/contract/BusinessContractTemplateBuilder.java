package com.clifton.business.contract;

import com.clifton.core.util.date.DateUtils;


public class BusinessContractTemplateBuilder {

	private BusinessContractTemplate businessContractTemplate;


	private BusinessContractTemplateBuilder(BusinessContractTemplate businessContractTemplate) {
		this.businessContractTemplate = businessContractTemplate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessContractTemplateBuilder createTradeWorkflow() {
		BusinessContractTemplate businessContractTemplate = new BusinessContractTemplate();

		businessContractTemplate.setId(8);
		businessContractTemplate.setContractType(BusinessContractTypeBuilder.createContractor().toBusinessContractType());
		businessContractTemplate.setName("PIOS - Non-ERISA - No OTC");
		businessContractTemplate.setDocumentFileType("doc");
		businessContractTemplate.setDocumentUpdateDate(DateUtils.toDate("11/23/2010"));
		businessContractTemplate.setDocumentUpdateUser("test");

		return new BusinessContractTemplateBuilder(businessContractTemplate);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractTemplateBuilder withId(int id) {
		getBusinessContractTemplate().setId(id);
		return this;
	}


	public BusinessContractTemplateBuilder withName(String name) {
		getBusinessContractTemplate().setName(name);
		return this;
	}


	public BusinessContractTemplateBuilder withDescription(String description) {
		getBusinessContractTemplate().setDescription(description);
		return this;
	}


	public BusinessContractTemplate toBusinessContractTemplate() {
		return this.businessContractTemplate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessContractTemplate getBusinessContractTemplate() {
		return this.businessContractTemplate;
	}
}
