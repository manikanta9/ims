package com.clifton.business.contract;

public class BusinessContractPartyRoleBuilder {

	private BusinessContractPartyRole businessContractPartyRole;


	private BusinessContractPartyRoleBuilder(BusinessContractPartyRole businessContractPartyRole) {
		this.businessContractPartyRole = businessContractPartyRole;
	}


	public static BusinessContractPartyRoleBuilder createCounterparty() {
		BusinessContractPartyRoleBuilder builder = new BusinessContractPartyRoleBuilder(new BusinessContractPartyRole());
		builder.withId((short) 4).withName("Counterparty").withDescription("The other party that participates in a financial transaction. Every transaction must have a counterparty in order for the transaction to go through. More specifically, every buyer of an asset must be paired up with a seller that is willing to sell and vice versa.");
		return builder;
	}


	public BusinessContractPartyRoleBuilder withId(Short id) {
		getBusinessContractPartyRole().setId(id);
		return this;
	}


	public BusinessContractPartyRoleBuilder withName(String name) {
		getBusinessContractPartyRole().setName(name);
		return this;
	}


	public BusinessContractPartyRoleBuilder withDescription(String description) {
		getBusinessContractPartyRole().setDescription(description);
		return this;
	}


	public BusinessContractPartyRole getBusinessContractPartyRole() {
		return this.businessContractPartyRole;
	}
}
