package com.clifton.business.contract;

import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.definition.WorkflowStatusBuilder;


public class BusinessContractBuilder {

	private BusinessContract businessContract;


	private BusinessContractBuilder(BusinessContract businessContract) {
		this.businessContract = businessContract;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BusinessContractBuilder createTestContract1() {
		BusinessContract businessContract = new BusinessContract();

		businessContract.setId(351);
		businessContract.setContractType(BusinessContractTypeBuilder.createContractor().toBusinessContractType());
		businessContract.setContractTemplate(BusinessContractTemplateBuilder.createTradeWorkflow().toBusinessContractTemplate());
		businessContract.setCompany(BusinessCompanyBuilder.createClient1().toBusinessCompany());
		businessContract.setWorkflowState(WorkflowStateBuilder.createPublished().toWorkflowState());
		businessContract.setWorkflowStatus(WorkflowStatusBuilder.createFinal().toWorkflowStatus());

		return new BusinessContractBuilder(businessContract);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContractBuilder withId(int id) {
		getBusinessContract().setId(id);
		return this;
	}


	public BusinessContractBuilder withName(String name) {
		getBusinessContract().setName(name);
		return this;
	}


	public BusinessContractBuilder withDescription(String description) {
		getBusinessContract().setDescription(description);
		return this;
	}


	public BusinessContract toBusinessContract() {
		return this.businessContract;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessContract getBusinessContract() {
		return this.businessContract;
	}
}
