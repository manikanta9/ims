package com.clifton.business.contact;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>BusinessContactServiceImplTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessContactServiceImplTests {

	@Resource
	private BusinessContactService businessContactService;


	@Test
	public void testBusinessContactLabel() {
		BusinessContact contact = this.businessContactService.getBusinessContact(101);
		Assertions.assertEquals("Contact Test Business", contact.getLabelShort());
		Assertions.assertEquals("Mr. Contact Test Business Jr.", contact.getLabel());

		contact = this.businessContactService.getBusinessContact(102);
		Assertions.assertEquals("Smith", contact.getLabelShort());
		Assertions.assertEquals("Smith", contact.getLabel());
	}


	@Test
	public void testBusinessContactStartEndDates() {
		BusinessContact contact = new BusinessContact();
		contact.setLastName("Test Contact");
		this.businessContactService.saveBusinessContact(contact);

		contact.setStartDate(DateUtils.toDate("01/01/2012"));
		this.businessContactService.saveBusinessContact(contact);

		contact.setEndDate(DateUtils.toDate("01/01/2013"));
		this.businessContactService.saveBusinessContact(contact);

		contact.setStartDate(null);
		this.businessContactService.saveBusinessContact(contact);
	}


	@Test
	public void testBusinessContactStartEndDatesInvalid() {
		TestUtils.expectException(FieldValidationException.class, () -> {
			BusinessContact contact = new BusinessContact();
			contact.setLastName("Test Contact 2");
			contact.setStartDate(DateUtils.toDate("01/01/2013"));
			contact.setEndDate(DateUtils.toDate("01/01/2012"));
			this.businessContactService.saveBusinessContact(contact);
		}, "Invalid Start/End Dates entered. Start Date must be before End Date.");
	}
}
