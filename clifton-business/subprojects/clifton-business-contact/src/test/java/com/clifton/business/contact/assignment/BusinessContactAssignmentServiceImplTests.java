package com.clifton.business.contact.assignment;

import com.clifton.business.contact.BusinessContactService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BusinessContactAssignmentServiceImplTests {

	private static final int SYSTEM_CONDITION_TEST_CONDITION_ID = 1;

	private static final int CONTACT_TRADER_ID = 404;

	private static final int CLIENT_ARIZONA_ID = 101;
	private static final int CLIENT_LIUNA_ID = 102;
	private static final int CLIENT_DEFENSIVE_EQUITY_ID = 201;

	private static final short CONTACT_ASSIGNMENT_CATEGORY_PARAMETRIC_ACCOUNTS_ID = 1101;
	private static final short CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID = 1102;

	private static final short CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID = 1111;
	private static final short CONTACT_ASSIGNMENT_ROLE_PM_BACKUP_ID = 1112;

	private static final short CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID = 1121;
	private static final short CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID = 1122;
	private static final short CONTACT_ASSIGNMENT_NOT_ACTIVE_PM_BACKUP_BAIDUM_ID = 1123;
	private static final short CONTACT_ASSIGNMENT_NOT_SYSTEM_DEFINED_PM_BACKUP_TRADER_ID = 1124;


	@Resource
	private BusinessContactAssignmentService businessContactAssignmentService;

	@Resource
	private BusinessContactService businessContactService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;

	@Resource
	private XmlUpdatableDAO<BusinessContactAssignmentCategory> businessContactAssignmentCategoryDAO;

	@Resource
	private XmlUpdatableDAO<BusinessContactAssignmentRole> businessContactAssignmentRoleDAO;

	@Resource
	private XmlReadOnlyDAO<BusinessContactAssignment> businessContactAssignmentDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTests() {
		this.businessContactAssignmentCategoryDAO.setLogicalEvaluatesToTrue(true);
		this.businessContactAssignmentRoleDAO.setLogicalEvaluatesToTrue(true);
		this.businessContactAssignmentDAO.setLogicalEvaluatesToTrue(true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Business Contact Assignment Category Tests         ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testContactAssignmentCategory_Delete() {
		List<BusinessContactAssignment> assignmentsWithParametricAccountsCategory = this.businessContactAssignmentService.getBusinessContactAssignmentListByCategoryId(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
		deleteAssignmentList(assignmentsWithParametricAccountsCategory);
		this.businessContactAssignmentService.deleteBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
	}


	@Test
	public void testContactAssignmentCategory_AddNew_Fail_SystemDefinedCategory() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessContactAssignmentCategory systemDefinedCategory = new BusinessContactAssignmentCategory();
			systemDefinedCategory.setContactCategory(this.businessContactAssignmentService.getBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_PARAMETRIC_ACCOUNTS_ID).getContactCategory());
			systemDefinedCategory.setName("Test System Defined Category Name");
			systemDefinedCategory.setSystemDefined(true);
			this.businessContactAssignmentService.saveBusinessContactAssignmentCategory(systemDefinedCategory);
		}, "System Defined Contact Assignment Categories cannot be added.");
	}


	@Test
	public void testContactAssignmentCategory_Delete_Fail_SystemDefinedCategory() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.businessContactAssignmentService.deleteBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_PARAMETRIC_ACCOUNTS_ID);
		}, "System Defined Contact Assignment Categories cannot be deleted.");
	}


	@Test
	public void testContactAssignmentCategory_Delete_Fail_ExistingAssignments() {
		List<BusinessContactAssignment> assignmentsWithParametricAccountsCategory = this.businessContactAssignmentService.getBusinessContactAssignmentListByCategoryId(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
		String expectedExceptionMsg = BusinessContactAssignmentUtils.getLinkedContactAssignmentListViolationString("Cannot delete Contact Assignment Category",
				CollectionUtils.getFirstElementStrict(assignmentsWithParametricAccountsCategory).getContactAssignmentCategory().getName(), assignmentsWithParametricAccountsCategory);
		TestUtils.expectException(ValidationException.class, () -> {
			this.businessContactAssignmentService.deleteBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
		}, expectedExceptionMsg);
	}


	@Test
	public void testContactAssignmentCategory_UpdateName() {
		BusinessContactAssignmentCategory notSystemDefinedCategory = this.businessContactAssignmentService.getBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
		notSystemDefinedCategory.setName("Test Category Name");
		this.businessContactAssignmentService.saveBusinessContactAssignmentCategory(notSystemDefinedCategory);
	}


	@Test
	public void testContactAssignmentCategory_UpdateName_Pass_CategoryHasEntityListUrl() {
		BusinessContactAssignmentCategory notSystemDefinedCategory = this.businessContactAssignmentService.getBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
		SystemTable systemTable = notSystemDefinedCategory.getEntitySystemTable();
		systemTable.setEntityListUrl(null);
		this.systemSchemaService.saveSystemTable(systemTable);
		notSystemDefinedCategory.setName("Test Category Name");
		notSystemDefinedCategory.setEntityListUrl("testUrl.json");
		this.businessContactAssignmentService.saveBusinessContactAssignmentCategory(notSystemDefinedCategory);
	}


	@Test
	public void testContactAssignmentCategory_UpdateName_Fail_SystemTableAndCategoryMissingEntityListUrl() {
		BusinessContactAssignmentCategory notSystemDefinedCategory = this.businessContactAssignmentService.getBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID);
		SystemTable systemTable = notSystemDefinedCategory.getEntitySystemTable();
		systemTable.setEntityListUrl(null);
		this.systemSchemaService.saveSystemTable(systemTable);
		TestUtils.expectException(ValidationException.class, () -> {
			notSystemDefinedCategory.setName("Test Category Name");
			this.businessContactAssignmentService.saveBusinessContactAssignmentCategory(notSystemDefinedCategory);
		}, String.format("Cannot save Contact Assignment Category because System Table [BusinessCompany] is missing an entityListUrl and no entityListUrl has been defined for this Category.", systemTable.getLabel()));
	}


	@Test
	public void testContactAssignmentCategory_UpdateName_Fail_SystemDefinedCategory() {
		TestUtils.expectException(ValidationException.class, () -> {
			BusinessContactAssignmentCategory systemDefinedCategory = this.businessContactAssignmentService.getBusinessContactAssignmentCategory(CONTACT_ASSIGNMENT_CATEGORY_PARAMETRIC_ACCOUNTS_ID);
			systemDefinedCategory.setName("Test Category Name");
			this.businessContactAssignmentService.saveBusinessContactAssignmentCategory(systemDefinedCategory);
		}, "Only Administrators are allowed to update system defined Contact Assignment Categories.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////           Business Contact Assignment Role Tests           ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testContactAssignmentRole_DecreaseMaxAssigneesPerEntity() {
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_ARIZONA_ID);
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, null);
	}


	@Test
	public void testContactAssignmentRole_DecreaseMaxAssigneesPerEntity_DecreaseExistingValue() {
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 2);
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_ARIZONA_ID);
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 1);
	}


	@Test
	public void testContactAssignmentRole_DecreaseMaxAssigneesPerEntity_ActiveOnly_DecreaseExistingValue() {
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 2);
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_ARIZONA_ID);
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_NOT_ACTIVE_PM_BACKUP_BAIDUM_ID, CLIENT_ARIZONA_ID);
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 1);
	}


	@Test
	public void testContactAssignmentRole_DecreaseMaxAssigneesPerEntity_DecreaseExistingValue_Fail_ExistingAssignmentsLinkedToEntities() {
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 2);
		BusinessContactAssignment speakesAssignment = linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_DEFENSIVE_EQUITY_ID);
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID, CLIENT_DEFENSIVE_EQUITY_ID);

		int newMaxAssigneesPerEntity = 1;
		speakesAssignment.getContactAssignmentRole().setMaxAssigneesPerEntity(newMaxAssigneesPerEntity);
		Set<String> fkFieldIdsExceedingMaxAssigneesPerEntity = BusinessContactAssignmentUtils.getAssignmentEntityLabelsExceedingMaxAssigneesPerEntity(this.businessContactAssignmentService, speakesAssignment.getContactAssignmentRole());
		String expectedErrorMsg = BusinessContactAssignmentUtils.getMaxAssigneesPerEntityErrorMessage(speakesAssignment.getContactAssignmentRole(), fkFieldIdsExceedingMaxAssigneesPerEntity);
		TestUtils.expectException(ValidationException.class, () -> {
			updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, newMaxAssigneesPerEntity);
		}, expectedErrorMsg);
	}


	@Test
	public void testContactAssignmentRole_DecreaseMaxAssigneesPerEntity_AddNewValue_Fail_ExistingAssignmentsLinkedToEntities() {
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, null);
		BusinessContactAssignment speakesAssignment = linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_DEFENSIVE_EQUITY_ID);
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID, CLIENT_DEFENSIVE_EQUITY_ID);

		int newMaxAssigneesPerEntity = 1;
		speakesAssignment.getContactAssignmentRole().setMaxAssigneesPerEntity(newMaxAssigneesPerEntity);
		Set<String> fkFieldIdsExceedingMaxAssigneesPerEntity = BusinessContactAssignmentUtils.getAssignmentEntityLabelsExceedingMaxAssigneesPerEntity(this.businessContactAssignmentService, speakesAssignment.getContactAssignmentRole());
		String expectedErrorMsg = BusinessContactAssignmentUtils.getMaxAssigneesPerEntityErrorMessage(speakesAssignment.getContactAssignmentRole(), fkFieldIdsExceedingMaxAssigneesPerEntity);
		TestUtils.expectException(ValidationException.class, () -> {
			updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, newMaxAssigneesPerEntity);
		}, expectedErrorMsg);
	}


	@Test
	public void testContactAssignmentRole_Delete() {
		List<BusinessContactAssignment> assignmentsWithPmPrimaryRole = this.businessContactAssignmentService.getBusinessContactAssignmentListByRoleId(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID);
		deleteAssignmentList(assignmentsWithPmPrimaryRole);
		this.businessContactAssignmentService.deleteBusinessContactAssignmentRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID);
	}


	@Test
	public void testContactAssignmentRole_Delete_Fail_ExistingAssignments() {
		List<BusinessContactAssignment> assignmentsWithPmPrimaryRole = this.businessContactAssignmentService.getBusinessContactAssignmentListByRoleId(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID);
		TestUtils.expectException(ValidationException.class, () -> {
			this.businessContactAssignmentService.deleteBusinessContactAssignmentRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID);
		}, BusinessContactAssignmentUtils.getLinkedContactAssignmentListViolationString("Cannot delete Contact Assignment Role",
				CollectionUtils.getFirstElementStrict(assignmentsWithPmPrimaryRole).getContactAssignmentRole().getName(), assignmentsWithPmPrimaryRole));
	}


	@Test
	public void testContactAssignmentRole_UpdateAssignmentRole_Fail_EntityModifyCondition() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		addEntityModifyConditionToRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, SYSTEM_CONDITION_TEST_CONDITION_ID);
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessContactAssignmentRole.class))).thenReturn("Current user does not have permission to update contact assignment role.");
			updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 2);
		}, "Current user does not have permission to update contact assignment role.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////             Business Contact Assignment Tests              ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testContactAssignment_CreateAssignment_Pass_EndDateEqualsStartDate() {
		BusinessContactAssignment velaskiAssignment = this.businessContactAssignmentService.getBusinessContactAssignment(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID);
		Date startDate = new Date();
		velaskiAssignment.setStartDate(startDate);
		velaskiAssignment.setEndDate(startDate);
		this.businessContactAssignmentService.saveBusinessContactAssignment(velaskiAssignment);
	}


	@Test
	public void testContactAssignment_CreateAssignment_Fail_EndDateBeforeStartDate() {
		BusinessContactAssignment velaskiAssignment = this.businessContactAssignmentService.getBusinessContactAssignment(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID);
		Date startDate = new Date();
		velaskiAssignment.setStartDate(startDate);
		velaskiAssignment.setEndDate(DateUtils.addDays(startDate, -1));
		TestUtils.expectException(FieldValidationException.class, () -> this.businessContactAssignmentService.saveBusinessContactAssignment(velaskiAssignment),
				"End date must be after start date");
	}


	@Test
	public void testContactAssignment_CreateAssignment_Fail_EntityModifyCondition() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		addEntityModifyConditionToRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, SYSTEM_CONDITION_TEST_CONDITION_ID);
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessContactAssignment.class))).thenReturn("Current user does not have permission to create contact assignment.");
			createContactAssignment(CONTACT_ASSIGNMENT_CATEGORY_NOT_SYSTEM_DEFINED_ID, CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, CONTACT_TRADER_ID);
		}, "Current user does not have permission to create contact assignment.");
	}


	@Test
	public void testContactAssignment_UpdateAssignment_Fail_EntityModifyCondition() {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(true);
		addEntityModifyConditionToRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, SYSTEM_CONDITION_TEST_CONDITION_ID);
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 2);
		TestUtils.expectException(ValidationException.class, () -> {
			Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(false);
			Mockito.when(this.systemConditionEvaluationHandler.getConditionFalseMessage(ArgumentMatchers.any(SystemCondition.class), ArgumentMatchers.any(BusinessContactAssignment.class))).thenReturn("Current user does not have permission to update contact assignment.");
			linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_LIUNA_ID);
		}, "Current user does not have permission to update contact assignment.");
	}


	@Test
	public void testContactAssignment_SaveMultipleAssignmentsForEntity() {
		updateMaxAssigneesPerEntityForRole(CONTACT_ASSIGNMENT_ROLE_PM_PRIMARY_ID, 2);
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_LIUNA_ID);
	}


	@Test
	public void testContactAssignment_SaveMultipleAssignmentsForEntity_Fail_MaxAssigneesPerEntity() {
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_ARIZONA_ID);
		BusinessContactAssignment velaskiAssignment = this.businessContactAssignmentService.getBusinessContactAssignment(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID);
		velaskiAssignment.setFkFieldId(CLIENT_ARIZONA_ID);
		TestUtils.expectException(MaximumAssigneesEntityExceededException.class, () -> {
			this.businessContactAssignmentService.saveBusinessContactAssignment(velaskiAssignment);
		}, BusinessContactAssignmentUtils.getMaxAssigneesPerEntityErrorMessage(velaskiAssignment));
	}


	@Test
	public void testContactAssignment_SaveMultipleAssignmentsForEntity_Fail_StartDateEndDateOverlap() {
		linkAssignmentToEntity(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_SPEAKES_ID, CLIENT_ARIZONA_ID);
		BusinessContactAssignment velaskiAssignment = this.businessContactAssignmentService.getBusinessContactAssignment(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID);
		velaskiAssignment.setFkFieldId(CLIENT_ARIZONA_ID);
		velaskiAssignment.setEndDate(DateUtils.addDays(new Date(), -1));
		TestUtils.expectException(MaximumAssigneesEntityExceededException.class, () -> {
			this.businessContactAssignmentService.saveBusinessContactAssignment(velaskiAssignment);
		}, BusinessContactAssignmentUtils.getMaxAssigneesPerEntityErrorMessage(velaskiAssignment));
	}


	@Test
	public void testContactAssignment_SaveDuplicateAssignmentsForEntity_Pass_InactiveDuplicatesAllowed() {
		BusinessContactAssignment inactiveBaidumAssignment = this.businessContactAssignmentService.getBusinessContactAssignment(CONTACT_ASSIGNMENT_NOT_ACTIVE_PM_BACKUP_BAIDUM_ID);
		BusinessContactAssignment duplicateAssignment = new BusinessContactAssignment();
		BeanUtils.copyProperties(inactiveBaidumAssignment, duplicateAssignment);
		this.businessContactAssignmentService.saveBusinessContactAssignment(duplicateAssignment);
	}


	@Test
	public void testContactAssignment_SaveDuplicateAssignmentsForEntity_Fail_ActiveAssignmentWithSamePropsExists() {
		BusinessContactAssignment velaskiAssignment = this.businessContactAssignmentService.getBusinessContactAssignment(CONTACT_ASSIGNMENT_PARAMETRIC_ACCOUNT_CONTACTS_PM_PRIMARY_VELASKI_ID);
		BusinessContactAssignment duplicateAssignment = new BusinessContactAssignment();
		BeanUtils.copyProperties(velaskiAssignment, duplicateAssignment);
		duplicateAssignment.setId(null);
		TestUtils.expectException(ValidationException.class, () -> {
			this.businessContactAssignmentService.saveBusinessContactAssignment(duplicateAssignment);
		}, String.format("Cannot save Contact Assignment [%s] because an assignment that is active between %s with the same properties already exists.", duplicateAssignment.getLabel(), duplicateAssignment.getActiveOnDateRangeLabel()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessContactAssignment createContactAssignment(short categoryId, short roleId, int contactId) {
		BusinessContactAssignment assignment = new BusinessContactAssignment();
		assignment.setContactAssignmentCategory(this.businessContactAssignmentService.getBusinessContactAssignmentCategory(categoryId));
		assignment.setContactAssignmentRole(this.businessContactAssignmentService.getBusinessContactAssignmentRole(roleId));
		assignment.setContact(this.businessContactService.getBusinessContact(contactId));
		return this.businessContactAssignmentService.saveBusinessContactAssignment(assignment);
	}


	private BusinessContactAssignment linkAssignmentToEntity(short contactAssignmentId, int fkFieldId) {
		return linkAssignmentToEntity(this.businessContactAssignmentService.getBusinessContactAssignment(contactAssignmentId), fkFieldId);
	}


	private BusinessContactAssignment linkAssignmentToEntity(BusinessContactAssignment contactAssignment, int fkFieldId) {
		contactAssignment.setFkFieldId(fkFieldId);
		contactAssignment.setAssignmentEntityLabel(null);
		return this.businessContactAssignmentService.saveBusinessContactAssignment(contactAssignment);
	}


	private BusinessContactAssignmentRole updateMaxAssigneesPerEntityForRole(short roleId, Integer maxAssigneesPerEntity) {
		return updateMaxAssigneesPerEntityForRole(this.businessContactAssignmentService.getBusinessContactAssignmentRole(roleId), maxAssigneesPerEntity);
	}


	private BusinessContactAssignmentRole updateMaxAssigneesPerEntityForRole(BusinessContactAssignmentRole role, Integer maxAssigneesPerEntity) {
		role.setMaxAssigneesPerEntity(maxAssigneesPerEntity);
		return this.businessContactAssignmentService.saveBusinessContactAssignmentRole(role);
	}


	private BusinessContactAssignmentRole addEntityModifyConditionToRole(short roleId, int systemConditionId) {
		BusinessContactAssignmentRole role = this.businessContactAssignmentService.getBusinessContactAssignmentRole(roleId);
		role.setEntityModifyCondition(this.systemConditionService.getSystemCondition(systemConditionId));
		return this.businessContactAssignmentService.saveBusinessContactAssignmentRole(role);
	}


	private void deleteAssignmentList(List<BusinessContactAssignment> contactAssignmentList) {
		CollectionUtils.getIterable(contactAssignmentList).forEach(contactAssignment -> this.businessContactAssignmentService.deleteBusinessContactAssignment(contactAssignment.getId()));
	}
}
