package com.clifton.business.company.contact;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyRelationship;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactRelationship;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.AddressUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class BusinessCompanyContactServiceImplTests {

	@Resource
	private BusinessCompanyContactService businessCompanyContactService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private BusinessContactService businessContactService;

	@Resource
	private XmlReadOnlyDAO<BusinessCompanyContact> businessCompanyContactDAO;

	@Resource
	private XmlUpdatableDAO<BusinessContactRelationship> businessContactRelationshipDAO;

	@Resource
	private XmlUpdatableDAO<BusinessCompanyRelationship> businessCompanyRelationshipDAO;


	///////////////////////////////////////////


	@BeforeEach
	public void setupTests() {
		this.businessCompanyContactDAO.setLogicalEvaluatesToTrue(true);
		this.businessContactRelationshipDAO.setLogicalEvaluatesToTrue(true);
		this.businessCompanyRelationshipDAO.setLogicalEvaluatesToTrue(true);
	}


	///////////////////////////////////////////


	@Test
	public void testGetBusinessCompanyContacts() {
		// Get List of CompanyContacts for a Company
		BusinessCompanyContactSearchForm sf = new BusinessCompanyContactSearchForm();
		sf.setCompanyId(201);
		List<BusinessCompanyContact> list = this.businessCompanyContactService.getBusinessCompanyContactList(sf);
		Assertions.assertEquals(2, CollectionUtils.getSize(list));

		sf = new BusinessCompanyContactSearchForm();
		sf.setCompanyId(202);
		list = this.businessCompanyContactService.getBusinessCompanyContactList(sf);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		// Verify Company Labels
		BusinessCompanyContact companyContact = list.get(0);
		Assertions.assertTrue(companyContact.isActive());
		BusinessCompany company = companyContact.getReferenceOne();
		Assertions.assertEquals("Parametric Clifton", company.getLabel());
		company.setAlias("Parametric Clifton (Our Company)");
		Assertions.assertEquals("Parametric Clifton (Our Company)", company.getLabel());
		Assertions.assertEquals("Trading (Our Company)", company.getType().getLabel());

		// Get List of CompanyContacts for a Contact
		sf = new BusinessCompanyContactSearchForm();
		sf.setContactId(101);
		list = this.businessCompanyContactService.getBusinessCompanyContactList(sf);
		Assertions.assertEquals(2, CollectionUtils.getSize(list));

		sf = new BusinessCompanyContactSearchForm();
		sf.setContactId(102);
		list = this.businessCompanyContactService.getBusinessCompanyContactList(sf);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		// Verify Company Label
		company = list.get(0).getReferenceOne();
		Assertions.assertEquals("ABC Client", company.getLabel());
		Assertions.assertEquals("Client", company.getType().getLabel());
	}


	////////////////////////////////////////////////////////////////////
	/////   "Company" Copy/Merge Specific Tests
	////////////////////////////////////////////////////////////////////


	@Test
	public void testCopyCompanyRelationships() {
		// For the purpose of testing copying - and xml restrictions - remove all existing company-company relationships so can use Logical Evaluates to true on the DAO
		this.businessCompanyRelationshipDAO.deleteList(this.businessCompanyRelationshipDAO.findAll());

		BusinessContact c1 = setupTestContact("Joe", "Smith", false);
		BusinessContact c2 = setupTestContact("Jane", "Doe", false);

		BusinessCompany cm1 = setupTestCompany("Test Company 1", true);
		BusinessCompany cm2 = setupTestCompany("Test Company 2", false);
		BusinessCompany cm3 = setupTestCompany("Test Company 3", false);

		setupCompanyContactRelationship(cm1, c1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		setupCompanyContactRelationship(cm1, c2, "Consultant", true);

		BusinessCompanyRelationship cmr = new BusinessCompanyRelationship();
		cmr.setReferenceOne(cm1);
		cmr.setReferenceTwo(cm3);
		cmr.setName("Power of Attorney");
		this.businessCompanyService.saveBusinessCompanyRelationship(cmr);

		String result = this.businessCompanyContactService.copyBusinessCompanyRelationships(cm1.getId(), cm2.getId(), false, false, false);
		Assertions.assertEquals(StringUtils.NEW_LINE + "[2] company-contact relationships copied." + StringUtils.NEW_LINE + "[1] company-company relationships copied.", result);

		List<BusinessCompanyContact> c1_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByCompany(cm1.getId());
		List<BusinessCompanyContact> c2_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByCompany(cm2.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(c1_ccList));
		Assertions.assertEquals(2, CollectionUtils.getSize(c2_ccList));

		List<BusinessCompanyRelationship> c1_crList = this.businessCompanyRelationshipDAO.findByField("referenceOne.id", cm1.getId());
		List<BusinessCompanyRelationship> c2_crList = this.businessCompanyRelationshipDAO.findByField("referenceOne.id", cm2.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(c1_crList));
		Assertions.assertEquals(1, CollectionUtils.getSize(c2_crList));

		// Copying of Relationships - now Contact 1 has 2 Employee Relationships - should remove main company from the contact
		c1 = this.businessContactService.getBusinessContact(c1.getId());
		Assertions.assertNull(c1.getCompany());
		Assertions.assertTrue(AddressUtils.isEmpty(c1));
	}


	@Test
	public void testMoveCompanyRelationships() {
		// For the purpose of testing copying - and xml restrictions - remove all existing company-company relationships so can use Logical Evaluates to true on the DAO
		this.businessCompanyRelationshipDAO.deleteList(this.businessCompanyRelationshipDAO.findAll());

		BusinessContact c1 = setupTestContact("Joe", "Smith", false);
		BusinessContact c2 = setupTestContact("Jane", "Doe", false);

		BusinessCompany cm1 = setupTestCompany("Test Company 1", true);
		BusinessCompany cm2 = setupTestCompany("Test Company 2", false);
		BusinessCompany cm3 = setupTestCompany("Test Company 3", false);

		setupCompanyContactRelationship(cm1, c1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		setupCompanyContactRelationship(cm1, c2, "Consultant", true);

		BusinessCompanyRelationship cmr = new BusinessCompanyRelationship();
		cmr.setReferenceOne(cm1);
		cmr.setReferenceTwo(cm3);
		cmr.setName("Power of Attorney");
		this.businessCompanyService.saveBusinessCompanyRelationship(cmr);

		String result = this.businessCompanyContactService.copyBusinessCompanyRelationships(cm1.getId(), cm2.getId(), true, false, false);
		Assertions.assertEquals(StringUtils.NEW_LINE + "[2] company-contact relationships moved." + StringUtils.NEW_LINE + "[1] company-company relationships moved.", result);

		List<BusinessCompanyContact> c1_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByCompany(cm1.getId());
		List<BusinessCompanyContact> c2_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByCompany(cm2.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(c1_ccList));
		Assertions.assertEquals(2, CollectionUtils.getSize(c2_ccList));

		List<BusinessCompanyRelationship> c1_crList = this.businessCompanyRelationshipDAO.findByField("referenceOne.id", cm1.getId());
		List<BusinessCompanyRelationship> c2_crList = this.businessCompanyRelationshipDAO.findByField("referenceOne.id", cm2.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(c1_crList));
		Assertions.assertEquals(1, CollectionUtils.getSize(c2_crList));

		// Copying of Relationships - now Contact 1 has Employee Relationships moved to Company 2 - should update main company from the contact
		c1 = this.businessContactService.getBusinessContact(c1.getId());
		Assertions.assertNotNull(c1.getCompany());
		Assertions.assertEquals(cm2, c1.getCompany());
		Assertions.assertTrue(AddressUtils.isEqual(c1, cm2, false));
	}


	@Test
	public void testMoveCompanyRelationships_DeleteFromCompany() {
		// For the purpose of testing copying - and xml restrictions - remove all existing Company-Company relationships so can use Logical Evaluates to true on the DAO
		this.businessCompanyRelationshipDAO.deleteList(this.businessCompanyRelationshipDAO.findAll());

		BusinessContact c1 = setupTestContact("Joe", "Smith", false);
		BusinessContact c2 = setupTestContact("Jane", "Doe", false);

		BusinessCompany cm1 = setupTestCompany("Test Company 1", true);
		BusinessCompany cm2 = setupTestCompany("Test Company 2", false);
		BusinessCompany cm3 = setupTestCompany("Test Company 3", false);

		setupCompanyContactRelationship(cm1, c1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		setupCompanyContactRelationship(cm1, c2, "Consultant", true);

		BusinessCompanyRelationship cmr = new BusinessCompanyRelationship();
		cmr.setReferenceOne(cm1);
		cmr.setReferenceTwo(cm3);
		cmr.setName("Power of Attorney");
		this.businessCompanyService.saveBusinessCompanyRelationship(cmr);

		String result = this.businessCompanyContactService.copyBusinessCompanyRelationships(cm1.getId(), cm2.getId(), true, false, false);
		Assertions.assertEquals(StringUtils.NEW_LINE + "[2] company-contact relationships moved." + StringUtils.NEW_LINE + "[1] company-company relationships moved.", result);

		List<BusinessCompanyContact> c1_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByCompany(cm1.getId());
		List<BusinessCompanyContact> c2_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByCompany(cm2.getId());
		Assertions.assertEquals(0, CollectionUtils.getSize(c1_ccList));
		Assertions.assertEquals(2, CollectionUtils.getSize(c2_ccList));

		//NOTE: Was moved (see text result above) but because of Logical Evaluates to True
		//When deleting the from company - it also deletes the company relationships - and with logical = true
		//will delete all.
		//List<BusinessCompanyRelationship> c1_crList = this.businessCompanyRelationshipDAO.findByField("referenceOne.id", cm1.getId());
		//List<BusinessCompanyRelationship> c2_crList = this.businessCompanyRelationshipDAO.findByField("referenceOne.id", cm2.getId());
		//Assertions.assertEquals(0, CollectionUtils.getSize(c1_crList));
		//Assertions.assertEquals(1, CollectionUtils.getSize(c2_crList));

		// Copying of Relationships - now Contact 1 has Employee Relationships moved to Company 2 - should update main company from the contact
		c1 = this.businessContactService.getBusinessContact(c1.getId());
		Assertions.assertNotNull(c1.getCompany());
		Assertions.assertEquals(cm2, c1.getCompany());
		Assertions.assertTrue(AddressUtils.isEqual(c1, cm2, false));
	}


	////////////////////////////////////////////////////////////////////
	/////   "Contact" Copy/Merge Specific Tests
	////////////////////////////////////////////////////////////////////


	@Test
	public void testCopyContactRelationships() {
		// For the purpose of testing copying - and xml restrictions - remove all existing contact-contact relationships so can use Logical Evaluates to true on the DAO
		this.businessContactRelationshipDAO.deleteList(this.businessContactRelationshipDAO.findAll());

		BusinessContact c1 = setupTestContact("Joe", "Smith", false);
		BusinessContact c2 = setupTestContact("Jane", "Doe", false);
		BusinessContact c3 = setupTestContact("Jessica", "Smith", false);

		BusinessCompany cm1 = setupTestCompany("Test Company 1", true);
		BusinessCompany cm2 = setupTestCompany("Test Company 2", false);

		setupCompanyContactRelationship(cm1, c1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		setupCompanyContactRelationship(cm2, c1, "Consultant", true);

		BusinessContactRelationship cnr = new BusinessContactRelationship();
		cnr.setReferenceOne(c1);
		cnr.setReferenceTwo(c3);
		cnr.setName("Power of Attorney");
		this.businessContactService.saveBusinessContactRelationship(cnr);

		String result = this.businessCompanyContactService.copyBusinessContactRelationships(c1.getId(), c2.getId(), false, false, false);
		Assertions.assertEquals(StringUtils.NEW_LINE + "[2] company-contact relationships copied." + StringUtils.NEW_LINE + "[1] contact-contact relationships copied.", result);

		List<BusinessCompanyContact> c1_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByContact(c1.getId());
		List<BusinessCompanyContact> c2_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByContact(c2.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(c1_ccList));
		Assertions.assertEquals(2, CollectionUtils.getSize(c2_ccList));

		List<BusinessContactRelationship> c1_crList = this.businessContactRelationshipDAO.findByField("referenceOne.id", c1.getId());
		List<BusinessContactRelationship> c2_crList = this.businessContactRelationshipDAO.findByField("referenceOne.id", c2.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(c1_crList));
		Assertions.assertEquals(1, CollectionUtils.getSize(c2_crList));

		c1 = this.businessContactService.getBusinessContact(c1.getId());
		Assertions.assertNotNull(c1.getCompany());
		c2 = this.businessContactService.getBusinessContact(c2.getId());
		Assertions.assertNotNull(c2.getCompany());
		Assertions.assertEquals(c1.getCompany(), c2.getCompany());
		Assertions.assertEquals(c1.getCompany(), cm1);

		Assertions.assertTrue(AddressUtils.isEqual(c2, c1, false));
	}


	@Test
	public void testMoveContactRelationships() {
		// For the purpose of testing copying - and xml restrictions - remove all existing contact-contact relationships so can use Logical Evaluates to true on the DAO
		this.businessContactRelationshipDAO.deleteList(this.businessContactRelationshipDAO.findAll());

		BusinessContact c1 = setupTestContact("Joe", "Smith", false);
		BusinessContact c2 = setupTestContact("Jane", "Doe", false);
		BusinessContact c3 = setupTestContact("Jessica", "Smith", false);

		BusinessCompany cm1 = setupTestCompany("Test Company 1", true);
		BusinessCompany cm2 = setupTestCompany("Test Company 2", false);

		setupCompanyContactRelationship(cm1, c1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		setupCompanyContactRelationship(cm2, c1, "Consultant", true);

		BusinessContactRelationship cnr = new BusinessContactRelationship();
		cnr.setReferenceOne(c1);
		cnr.setReferenceTwo(c3);
		cnr.setName("Power of Attorney");
		this.businessContactService.saveBusinessContactRelationship(cnr);

		String result = this.businessCompanyContactService.copyBusinessContactRelationships(c1.getId(), c2.getId(), true, false, false);
		Assertions.assertEquals(StringUtils.NEW_LINE + "[2] company-contact relationships moved." + StringUtils.NEW_LINE + "[1] contact-contact relationships moved.", result);

		List<BusinessCompanyContact> c1_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByContact(c1.getId());
		List<BusinessCompanyContact> c2_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByContact(c2.getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(c1_ccList));
		Assertions.assertEquals(2, CollectionUtils.getSize(c2_ccList));

		List<BusinessContactRelationship> c1_crList = this.businessContactRelationshipDAO.findByField("referenceOne.id", c1.getId());
		List<BusinessContactRelationship> c2_crList = this.businessContactRelationshipDAO.findByField("referenceOne.id", c2.getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(c1_crList));
		Assertions.assertEquals(1, CollectionUtils.getSize(c2_crList));

		c1 = this.businessContactService.getBusinessContact(c1.getId());
		Assertions.assertNull(c1.getCompany());
		c2 = this.businessContactService.getBusinessContact(c2.getId());
		Assertions.assertNotNull(c2.getCompany());
		Assertions.assertEquals(c2.getCompany(), cm1);

		Assertions.assertTrue(AddressUtils.isEqual(c2, cm1, false));
	}


	@Test
	public void testMoveContactRelationships_DeleteFromContact() {
		// For the purpose of testing copying - and xml restrictions - remove all existing contact-contact relationships so can use Logical Evaluates to true on the DAO
		this.businessContactRelationshipDAO.deleteList(this.businessContactRelationshipDAO.findAll());

		BusinessContact c1 = setupTestContact("Joe", "Smith", false);
		//Integer c1Id = c1.getId();
		BusinessContact c2 = setupTestContact("Jane", "Doe", false);
		BusinessContact c3 = setupTestContact("Jessica", "Smith", false);

		BusinessCompany cm1 = setupTestCompany("Test Company 1", true);
		BusinessCompany cm2 = setupTestCompany("Test Company 2", false);

		setupCompanyContactRelationship(cm1, c1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		setupCompanyContactRelationship(cm2, c1, "Consultant", true);

		BusinessContactRelationship cnr = new BusinessContactRelationship();
		cnr.setReferenceOne(c1);
		cnr.setReferenceTwo(c3);
		cnr.setName("Power of Attorney");
		this.businessContactService.saveBusinessContactRelationship(cnr);

		String result = this.businessCompanyContactService.copyBusinessContactRelationships(c1.getId(), c2.getId(), true, true, false);
		Assertions.assertEquals(StringUtils.NEW_LINE + "[2] company-contact relationships moved." + StringUtils.NEW_LINE + "[1] contact-contact relationships moved." + StringUtils.NEW_LINE
				+ " Contact [Joe Smith] has been removed.", result);

		List<BusinessCompanyContact> c1_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByContact(c1.getId());
		List<BusinessCompanyContact> c2_ccList = this.businessCompanyContactService.getBusinessCompanyContactListByContact(c2.getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(c1_ccList));
		Assertions.assertEquals(2, CollectionUtils.getSize(c2_ccList));

		//NOTE: Was moved (see text result above) but because of Logical Evaluates to True
		//When deleting the from contact - it also deletes the contact relationships - and with logical = true
		//will delete all.
		//List<BusinessContactRelationship> c1_crList = this.businessContactRelationshipDAO.findByField("referenceOne.id", c1.getId());
		//List<BusinessContactRelationship> c2_crList = this.businessContactRelationshipDAO.findByField("referenceOne.id", c2.getId());
		//Assertions.assertEquals(0, CollectionUtils.getSize(c1_crList));
		//Assertions.assertEquals(1, CollectionUtils.getSize(c2_crList));

		c1 = this.businessContactService.getBusinessContact(c1.getId());
		Assertions.assertNull(c1);
		c2 = this.businessContactService.getBusinessContact(c2.getId());
		Assertions.assertNotNull(c2.getCompany());
		Assertions.assertEquals(c2.getCompany(), cm1);

		Assertions.assertTrue(AddressUtils.isEqual(c2, cm1, false));
	}


	////////////////////////////////////////////////////////////////////
	/////   "Employee" Specific Tests
	////////////////////////////////////////////////////////////////////


	/**
	 * Verifies adding new employee relationship
	 * main company/address changes are reflected on the contact under specific conditions
	 */
	@Test
	public void testAddEmployeeContact() {
		BusinessCompany testCompany = setupTestCompany("Test Company 123", true);
		BusinessContact testContact = setupTestContact("Joe", "Smith", false);
		BusinessCompanyContact emp = setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);

		// Pull Contact Again and Check for Populated Fields
		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertNotNull(testContact.getCompany(), "Expect Company set on Contact by Employee Relationship save.");
		Assertions.assertEquals(testCompany, testContact.getCompany());
		Assertions.assertTrue(AddressUtils.isEqual(testCompany, testContact, true), "Expected Company address to be copied to the contact.");

		// Change Address on the company
		testCompany.setAddressLine2("Suite 44455");
		this.businessCompanyService.saveBusinessCompany(testCompany);

		// Pull Contact Again and Check if address was copied over
		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertTrue(AddressUtils.isEqual(testCompany, testContact, true), "Expected Company address to be copied to the contact.");

		// Change address on contact
		AddressUtils.clearAddress(testContact);
		testContact.setAddressLine1("789 51st Avenue");
		testContact.setCity("New York");
		testContact.setState("NY");
		testContact.setPostalCode("10010");
		this.businessContactService.saveBusinessContact(testContact);

		// Change address on the company again
		testCompany.setAddressLine2("Suite 55566");
		this.businessCompanyService.saveBusinessCompany(testCompany);

		// Pull Contact Again and Check that address was NOT copied over
		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertFalse(AddressUtils.isEqual(testCompany, testContact, true), "Expected Company address to NOT copy to the contact.");

		// Clear Address on the Contact - Should re-populate to the company address
		AddressUtils.clearAddress(testContact);
		this.businessContactService.saveBusinessContact(testContact);

		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertTrue(AddressUtils.isEqual(testCompany, testContact, true), "Expected Company address to be copied to the contact.");

		// Clear Address On the Company
		AddressUtils.clearAddress(testCompany);
		this.businessCompanyService.saveBusinessCompany(testCompany);

		// Should have also cleared on the contact
		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertTrue(AddressUtils.isEmpty(testContact), "Expected Contact address to be cleared on the contact.");

		// Re-add address to the company
		testCompany.setCity("Any City");
		testCompany.setState("Any State");
		this.businessCompanyService.saveBusinessCompany(testCompany);

		// Should have added back to the contact
		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertTrue(AddressUtils.isEqual(testCompany, testContact, true), "Expected Company address to be copied to the contact.");

		// Change Employee Relationship to something else
		emp.setContactType(this.businessContactService.getBusinessContactTypeByName("Consultant"));
		this.businessCompanyContactService.saveBusinessCompanyContact(emp, false);

		// Should have cleared main company AND Contact address
		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertNull(testContact.getCompany(), "Expected Main company to be cleared from the contact");
		Assertions.assertTrue(AddressUtils.isEmpty(testContact), "Expected Contact address to be cleared with main company clearing");

		// Setup a new Company
		BusinessCompany testCompany2 = setupTestCompany("Test Company 2", true);
		// Add employee relationship to company 2
		setupCompanyContactRelationship(testCompany2, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);

		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertNotNull(testContact.getCompany(), "Expect Company set on Contact by Employee Relationship save.");
		Assertions.assertEquals(testCompany2, testContact.getCompany());
		Assertions.assertTrue(AddressUtils.isEqual(testCompany2, testContact, true), "Expected Company address to be copied to the contact.");

		// Make original relationship also an employee relationship - so contact has employee relationship to both - should clear it
		emp.setContactType(this.businessContactService.getBusinessContactTypeByName(BusinessContactType.EMPLOYEE_CONTACT_TYPE));
		this.businessCompanyContactService.saveBusinessCompanyContact(emp, false);

		testContact = this.businessContactService.getBusinessContact(testContact.getId());
		Assertions.assertNull(testContact.getCompany(), "Expected Main company to be cleared from the contact");
		Assertions.assertTrue(AddressUtils.isEmpty(testContact), "Expected Contact address to be cleared with main company clearing");
	}


	////////////////////////////////////////////////////////////////////
	/////   Company Contact Mapping Tests
	////////////////////////////////////////////////////////////////////


	/**
	 * Verifies {@link BusinessCompanyContactMapping} oneAllowed functionality.
	 */
	@Test
	public void testMappingOneAllowed() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BusinessCompany testCompany = setupTestCompany("The Living", false);
			BusinessContact testContact = setupTestContact("Jon", "Snow", false);
			setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
			setupCompanyContactMapping(BusinessContactType.EMPLOYEE_CONTACT_TYPE, null, true, false, false);
			BusinessContact testContact1 = setupTestContact("Arya", "Stark", false);
			setupCompanyContactRelationship(testCompany, testContact1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		});
	}


	/**
	 * Verifies {@link BusinessCompanyContactMapping} onePrimaryRequired functionality.
	 * This is ignored because the intended logic prevents the user from switching which contact is the primary.
	 * The validation is also commented out in the {@link BusinessCompanyContactService}
	 */
	@Test
	@Disabled
	public void testMappingOnePrimaryRequired() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BusinessCompany testCompany = setupTestCompany("The Living", false);
			BusinessContact testContact = setupTestContact("Jon", "Snow", false);
			BusinessCompanyContact companyContact = setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
			companyContact.setPrimary(true);
			this.businessCompanyContactService.saveBusinessCompanyContact(companyContact, false);
			setupCompanyContactMapping(BusinessContactType.EMPLOYEE_CONTACT_TYPE, null, false, true, false);
			BusinessContact testContact1 = setupTestContact("Arya", "Stark", false);
			BusinessCompanyContact companyContact1 = setupCompanyContactRelationship(testCompany, testContact1, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
			companyContact1.setPrimary(true);
			this.businessCompanyContactService.saveBusinessCompanyContact(companyContact1, false);
		});
	}


	/**
	 * Verifies {@link BusinessCompanyContactMapping} notPreferred functionality.
	 */
	@Test
	public void testMappingNotPreferred() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BusinessCompany testCompany = setupTestCompany("The Living", false);
			BusinessContact testContact = setupTestContact("Jon", "Snow", false);
			setupCompanyContactMapping(BusinessContactType.EMPLOYEE_CONTACT_TYPE, "Consultant", false, false, true);
			setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		});
	}


	/**
	 * Verifies {@link BusinessCompanyContactMapping} deletion with a Mapping[ContactType, null].
	 */
	@Test
	public void testMappingDelete_nullCompanyType_fail() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BusinessCompany testCompany = setupTestCompany("The Living", false);
			BusinessContact testContact = setupTestContact("Jon", "Snow", false);
			setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
			this.businessCompanyContactService.deleteBusinessCompanyContactMapping(3); // delete Mapping[Employee, null], should fail because there is an existing Company Contact
		});
	}


	/**
	 * Verifies {@link BusinessCompanyContactMapping} deletion with a Mapping[ContactType, null].
	 */
	@Test
	public void testMappingDelete_nullCompanyType_pass() {
		BusinessCompany testCompany = setupTestCompany("The Living", false);
		BusinessContact testContact = setupTestContact("Jon", "Snow", false);
		setupCompanyContactMapping(BusinessContactType.EMPLOYEE_CONTACT_TYPE, "Consultant", false, false, false);
		setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		this.businessCompanyContactService.deleteBusinessCompanyContactMapping(3); // delete Mapping[Employee, null], should pass because existing Company Contacts belong to "Consultant" company type
	}


	/**
	 * Verifies {@link BusinessCompanyContactMapping} deletion with a Mapping[ContactType, CompanyType].
	 */
	@Test
	public void testMappingDelete_nonNullCompanyType_fail() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BusinessCompany testCompany = setupTestCompany("The Living", false);
			BusinessContact testContact = setupTestContact("Jon", "Snow", false);
			BusinessCompanyContactMapping mapping = setupCompanyContactMapping(BusinessContactType.EMPLOYEE_CONTACT_TYPE, "Consultant", false, false, false);
			this.businessCompanyContactService.deleteBusinessCompanyContactMapping(3);
			setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
			this.businessCompanyContactService.deleteBusinessCompanyContactMapping(mapping.getId()); // delete Mapping[Employee, Consultant], should fail because there is no mapping to adopt existing Company Contacts
		});
	}


	/**
	 * Verifies {@link BusinessCompanyContactMapping} deletion with a Mapping[ContactType, CompanyType].
	 */
	@Test
	public void testMappingDelete_nonNullCompanyType_Pass() {
		BusinessCompany testCompany = setupTestCompany("The Living", false);
		BusinessContact testContact = setupTestContact("Jon", "Snow", false);
		BusinessCompanyContactMapping mapping = setupCompanyContactMapping(BusinessContactType.EMPLOYEE_CONTACT_TYPE, "Consultant", false, false, false);
		setupCompanyContactRelationship(testCompany, testContact, BusinessContactType.EMPLOYEE_CONTACT_TYPE, true);
		this.businessCompanyContactService.deleteBusinessCompanyContactMapping(mapping.getId()); // delete Mapping[Employee, Consultant], should pass because there Mapping[Employee, null] will adopt existing Company Contacts
	}


	////////////////////////////////////////////////////////////////////
	/////   Company Contact Entry Tests
	////////////////////////////////////////////////////////////////////


	/**
	 * Verifies deleting all {@link BusinessCompanyContact}s on a {@link BusinessCompanyContactEntry}.
	 */
	@Test
	public void testEntryAddAndRemoveCompanyContacts() {
		BusinessCompany testCompany = setupTestCompany("The Living", false);
		BusinessContact testContact = setupTestContact("Jon", "Snow", false);
		BusinessCompanyContactEntry companyContactEntry = new BusinessCompanyContactEntry(testCompany, testContact);
		BusinessContactType type = this.businessContactService.getBusinessContactTypeByName(BusinessContactType.EMPLOYEE_CONTACT_TYPE);
		BusinessCompanyContact companyContact = new BusinessCompanyContact();
		companyContact.setContactType(type);
		companyContact.setReferenceOne(testCompany);
		companyContact.setReferenceTwo(testContact);
		companyContactEntry.addBusinessCompanyContact(companyContact);
		companyContactEntry = this.businessCompanyContactService.saveBusinessCompanyContactEntry(companyContactEntry, false);
		companyContactEntry.setCompanyContactList(null);
		this.businessCompanyContactService.saveBusinessCompanyContactEntry(companyContactEntry, false);
	}


	//////////////////////////////////////////////////////////////
	///////  Helper Methods
	//////////////////////////////////////////////////////////////


	private BusinessCompany setupTestCompany(String name, boolean populateAddress) {
		BusinessCompany testCompany = new BusinessCompany();
		testCompany.setName(name);
		testCompany.setType(this.businessCompanyService.getBusinessCompanyTypeByName("Consultant"));
		if (populateAddress) {
			testCompany.setAddressLine1("123 Main Street");
			testCompany.setAddressLine2("Suite 123");
			testCompany.setCity("Any City");
			testCompany.setState("Any State");
			testCompany.setPostalCode("12345");
		}
		this.businessCompanyService.saveBusinessCompany(testCompany);
		return testCompany;
	}


	private BusinessContact setupTestContact(String firstName, String lastName, boolean populateAddress) {
		BusinessContact testContact = new BusinessContact();
		testContact.setFirstName(firstName);
		testContact.setLastName(lastName);
		if (populateAddress) {
			testContact.setAddressLine1("999 Ninth Street");
			testContact.setAddressLine2("Suite 99");
			testContact.setCity("Any City");
			testContact.setState("Any State");
			testContact.setPostalCode("99999");
		}
		this.businessContactService.saveBusinessContact(testContact);
		return testContact;
	}


	private BusinessCompanyContact setupCompanyContactRelationship(BusinessCompany company, BusinessContact contact, String typeName, boolean active) {
		BusinessContactType type = this.businessContactService.getBusinessContactTypeByName(typeName);
		BusinessCompanyContact bcc = new BusinessCompanyContact();
		bcc.setContactType(type);
		bcc.setReferenceOne(company);
		bcc.setReferenceTwo(contact);
		if (!active) {
			bcc.setEndDate(DateUtils.addDays(new Date(), -7));
		}
		this.businessCompanyContactService.saveBusinessCompanyContact(bcc, false);
		return bcc;
	}


	private BusinessCompanyContactMapping setupCompanyContactMapping(String contactTypeName, String companyTypeName, boolean oneAllowed, boolean onePrimaryRequired, boolean notPreferred) {
		BusinessContactType contactType = this.businessContactService.getBusinessContactTypeByName(contactTypeName);
		BusinessCompanyType companyType = this.businessCompanyService.getBusinessCompanyTypeByName(companyTypeName);
		BusinessCompanyContactMapping mapping = this.businessCompanyContactService.getBusinessCompanyContactMappingFor(contactType.getId(), companyType == null ? null : companyType.getId());
		if (mapping == null || (mapping.getBusinessCompanyType() == null && companyType != null)) {
			mapping = new BusinessCompanyContactMapping();
		}
		mapping.setBusinessContactType(contactType);
		mapping.setBusinessCompanyType(companyType);
		mapping.setOneAllowed(oneAllowed);
		mapping.setOnePrimaryRequired(onePrimaryRequired);
		mapping.setNotPreferred(notPreferred);
		return this.businessCompanyContactService.saveBusinessCompanyContactMapping(mapping);
	}
}
