Clifton.business.contact.ContactTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Contact Type',
	iconCls: 'group',

	items: [{
		xtype: 'formpanel',
		instructions: 'Business Contact Types are used for classification purposes to group contacts into categories.  System defined contact types cannot be deleted and their names cannot be changed.',
		url: 'businessContactType.json',
		items: [
			{fieldLabel: 'Contact Category', name: 'contactCategory.name', hiddenName: 'contactCategory.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactCategoryListFind.json', detailPageClass: 'Clifton.business.contact.ContactCategoryWindow'},
			{fieldLabel: 'Type Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{boxLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox'}
		],
		listeners: {
			afterload: function(panel) {
				// If contact type is system defined disable name field
				const f = panel.getForm();
				const sd = TCG.getValue('systemDefined', f.formValues);
				let field = f.findField('name');
				field.setDisabled(sd === true);
				field = f.findField('systemDefined');
				field.setDisabled(sd === true);
			}
		}
	}]
});
