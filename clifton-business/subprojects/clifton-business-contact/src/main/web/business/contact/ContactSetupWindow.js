Clifton.business.contact.ContactSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'contactSetupWindow',
	title: 'Contact Setup',
	iconCls: 'group',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Contacts',
				items: [{
					name: 'businessContactListFind',
					xtype: 'gridpanel',
					instructions: 'The following business contacts exist in the system.  Each contact can be tied to a company or another contact.  For the contact filter, use "," between text to search against first and/or last name (Example john,doe to search for John Doe)',
					reloadOnRender: false,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category', width: 65, dataIndex: 'contactCategory.name', filter: {searchFieldName: 'contactCategoryId', type: 'combo', url: 'businessContactCategoryListFind.json'}, hidden: true},
						{header: 'Contact Name', width: 150, dataIndex: 'label', filter: {searchFieldName: 'searchPattern'}},

						{header: 'Main Company', width: 100, dataIndex: 'company.name', filter: {searchFieldName: 'companyName'}, hidden: true},
						{header: 'First Name', width: 80, dataIndex: 'firstName', hidden: true},
						{header: 'Middle Name', width: 50, dataIndex: 'middleName', hidden: true},
						{header: 'Last Name', width: 80, dataIndex: 'lastName', hidden: true},
						{header: 'Name Prefix', width: 50, dataIndex: 'namePrefix', hidden: true},
						{header: 'Name Suffix', width: 50, dataIndex: 'nameSuffix', hidden: true},
						{header: 'Salutation', width: 50, dataIndex: 'saluation', hidden: true},

						{header: 'Title', width: 100, dataIndex: 'title'},
						{header: 'Phone', width: 50, dataIndex: 'phoneNumber'},
						{header: 'Mobile', width: 50, dataIndex: 'mobileNumber', hidden: true},
						{header: 'Fax', width: 50, dataIndex: 'faxNumber', hidden: true},
						{header: 'E-Fax', width: 50, dataIndex: 'efaxNumber', hidden: true},

						{header: 'Email', width: 100, dataIndex: 'emailAddress', renderer: TCG.renderEmailAddress},
						{header: 'Secondary Email', width: 100, dataIndex: 'emailAddressSecondary', renderer: TCG.renderEmailAddress, hidden: true},
						{header: 'City/State', width: 75, dataIndex: 'cityState'},
						{header: 'Address 1', width: 75, dataIndex: 'addressLine1', hidden: true},
						{header: 'Address 2', width: 75, dataIndex: 'addressLine2', hidden: true},
						{header: 'Address 3', width: 75, dataIndex: 'addressLine3', hidden: true},
						{header: 'City', width: 75, dataIndex: 'city', hidden: true},
						{header: 'State', width: 50, dataIndex: 'state', hidden: true},
						{header: 'Postal Code', width: 50, dataIndex: 'postalCode', hidden: true},
						{header: 'Country', width: 75, dataIndex: 'country', hidden: true},

						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
						{header: 'Start Date', width: 75, dataIndex: 'startDate', hidden: true},
						{header: 'End Date', width: 75, dataIndex: 'endDate', hidden: true}

					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Contact Name', width: 150, xtype: 'toolbar-textfield', name: 'companyName'},
							{fieldLabel: 'Category', xtype: 'toolbar-combo', name: 'contactCategoryId', width: 120, url: 'businessContactCategoryListFind.json', linkedFilter: 'contactCategory.name'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						const companyName = TCG.getChildByName(this.getTopToolbar(), 'companyName');
						if (TCG.isNotEquals(companyName.getValue(), '')) {
							return {'searchPattern': companyName.getValue()};
						}
					},
					editor: {
						detailPageClass: 'Clifton.business.contact.ContactWindow',
						deleteURL: 'businessContactDelete.json'
					},

					addToolbarButtons: function(t, gridPanel) {
						t.add({
							text: 'Reload Company Field',
							tooltip: 'Reload main company selected on the contact.',
							iconCls: 'run',
							handler: function() {
								const loader = new TCG.data.JsonLoader({
									waitTarget: gridPanel,
									onLoad: function(record, conf) {
										gridPanel.reload();
									}
								});
								loader.load('businessContactCompanyFieldUpdate.json');
							}
						});
						t.add('-');

						t.add({
							text: 'Move/Copy Relationships',
							tooltip: 'Move/Copy Contact Relationships from one Contact to another.',
							iconCls: 'run',
							handler: function() {
								TCG.createComponent('Clifton.business.contact.ContactCopyRelationshipsWindow', {
									openerCt: gridPanel
								});
							}
						});
						t.add('-');
					}
				}]
			},
			{
				title: 'Contact Types',
				items: [{
					name: 'businessContactTypeListFind',
					xtype: 'gridpanel',
					groupField: 'contactCategory.name',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Types" : "Type"]}',
					instructions: 'Business Contact Types provide classification of contacts for companies.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category', width: 100, dataIndex: 'contactCategory.name', hidden: true},
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.business.contact.ContactTypeWindow'
					}
				}]
			},
			{
				title: 'Contact Categories',
				items: [{
					name: 'businessContactCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'Business Contact Categories are used to categorize and define modify access to contacts and company-contact relationships. For company-contact relationships, category will first use what is defined on the contact type, and if missing, will then use the category on the contact type.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Category Name', width: 75, dataIndex: 'name'},
						{header: 'Modify Condition', width: 175, dataIndex: 'entityModifyCondition.name'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.contact.ContactCategoryWindow'
					}
				}]
			}
		]
	}]
});

