Clifton.business.contact.assignment.ContactAssignmentRoleWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Contact Assignment Role',
	height: 400,
	width: 600,

	items: [{
		xtype: 'formpanel',
		instructions: 'A Contact Assignment Role is used to characterize a responsibility. A Role can also reference a Parent Role. For example, PM (Primary) is the parent of PM (Backup).',
		url: 'businessContactAssignmentRole.json',
		labelWidth: 170,
		items: [
			{fieldLabel: 'Parent Role', name: 'parentAssignmentRole.name', hiddenName: 'parentAssignmentRole.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactAssignmentRoleListFind.json', detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentRoleWindow'},
			{fieldLabel: 'Role Name', name: 'name'},
			{fieldLabel: 'Description', width: 175, name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Assignment Modify Condition', name: 'entityModifyCondition.label', hiddenName: 'entityModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo', qtip: 'If this condition is specified, it must evaluate to true in order to be able to create or update Contact Role Assignment. It will usually limit current user to a specific user group.'},
			{fieldLabel: 'Max Assignees Per Entity', name: 'maxAssigneesPerEntity', xtype: 'spinnerfield', minValue: 1, maxValue: 1000, qtip: 'Optional maximum number of assignees of this role that are allowed for the same entity.'},
			{fieldLabel: 'Role Order', name: 'roleOrder', xtype: 'spinnerfield', minValue: 1, maxValue: 1000}
		]
	}]
});
