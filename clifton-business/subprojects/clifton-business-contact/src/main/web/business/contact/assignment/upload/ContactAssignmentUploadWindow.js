Clifton.business.contact.assignment.upload.ContactAssignmentUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'contactAssignmentUploadWindow',
	title: 'Contact Assignment Import',
	iconCls: 'import',
	height: 650,
	width: 800,
	hideOKButton: true,

	tbar: [Clifton.system.LinkedEntitySampleFileDownlaodToolbarItems],

	items: [{
		xtype: 'system-linked-entity-file-upload-formpanel',
		instructions: 'Select a Contact Assignment Category to determine which type of entity that the uploaded Contact Assignments can be linked to. Download a sample file after selection to see which columns are required. ' +
			'Existing Contact Assignments will be automatically replaced by new Contact Assignments from the upload. For example, the system will automatically deactivate the existing PM (Primary) for a given entity if there is a new PM (Primary) specified for that entity in the upload file.',
		tableName: 'BusinessContactAssignment',
		dtoClassForBinding: 'com.clifton.business.contact.upload.BusinessContactAssignmentUploadCommand',
		defaultBeanPropertiesToExclude: ['investmentAccount.issuingCompany'],

		initComponent: function() {
			TCG.callParent(this, 'initComponent', arguments);
			const defaultAssignmentCategory = TCG.data.getData('businessContactAssignmentCategoryListFind.json?name=Parametric Account Contacts', this);
			if (defaultAssignmentCategory) {
				this.getForm().findField('contactAssignmentCategory.name').setValue({value: defaultAssignmentCategory[0].id, text: defaultAssignmentCategory[0].name});
			}
		},

		uploadCommandItems: [
			{
				fieldLabel: 'Assignment Category', name: 'contactAssignmentCategory.name', hiddenName: 'contactAssignmentCategory.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactAssignmentCategoryListFind.json', detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentCategoryWindow',
				limitRequestedProperties: false, allowBlank: false
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: true, qtip: 'Optional Start Date to use for all assignments in the upload file. NOTE: This can be overridden for a row in the upload file by setting a different Start Date for that row.'},
			Clifton.system.upload.InvalidDataEncounteredItems
		],

		getSaveURL: function() {
			return 'businessContactAssignmentFileUpload.json';
		}
	}]
});
