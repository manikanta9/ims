Clifton.business.contact.assignment.ContactAssignmentCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Contact Assignment Category',
	height: 350,
	width: 600,

	items: [{
		xtype: 'formpanel',
		instructions: 'A Contact Assignment Category is used to categorize Contact Assignments.',
		url: 'businessContactAssignmentCategory.json',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Contact Category', name: 'contactCategory.name', hiddenName: 'contactCategory.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactCategoryListFind.json', detailPageClass: 'Clifton.business.contact.ContactCategoryWindow', qtip: 'Only contacts from this Contact Category will be allowed.'},
			{fieldLabel: 'Category Name', name: 'name'},
			{fieldLabel: 'Description', width: 175, name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Entity System Table', name: 'entitySystemTable.name', hiddenName: 'entitySystemTable.id', xtype: 'combo', url: 'systemTableListFind.json', detailPageClass: 'Clifton.system.schema.TableWindow', qtip: 'Contacts in this Assignment Category will be linked to entities in this table.'},
			{fieldLabel: 'Entity List URL', name: 'entityListUrl', requiredFields: ['entitySystemTable.name']},
			{fieldLabel: 'System Defined', name: 'systemDefined', xtype: 'checkbox', disabled: true}
		],
		listeners: {
			afterload: function(form) {
				if (form.getFormValue('systemDefined')) {
					TCG.getChildByName(form, 'name').setDisabled(true);
				}
			}
		}
	}]
});
