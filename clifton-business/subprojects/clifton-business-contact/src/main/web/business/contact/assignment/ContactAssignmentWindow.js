Clifton.business.contact.assignment.ContactAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Contact Assignment',
	height: 350,
	width: 700,

	items: [{
		xtype: 'formpanel',
		instructions: 'Contact Assignments are used to define the role of a contact for a given entity in the system.',
		url: 'businessContactAssignment.json',
		labelWidth: 150,

		items: [
			{
				fieldLabel: 'Assignment Category', name: 'contactAssignmentCategory.name', hiddenName: 'contactAssignmentCategory.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactAssignmentCategoryListFind.json', detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentCategoryWindow',
				limitRequestedProperties: false,
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						let contactCategoryId = void 0;
						const contactCategory = record.json.contactCategory;
						if (contactCategory) {
							contactCategoryId = contactCategory.id;
						}
						fp.getForm().findField('contactAssignmentCategory.contactCategory.id').setValue(contactCategoryId);

						let entityListUrl = void 0;
						if (record.json.entityListUrl) {
							entityListUrl = record.json.entityListUrl;
						}
						fp.getForm().findField('contactAssignmentCategory.entityListUrl').setValue(entityListUrl);

						let entitySystemTableEntityListUrl = void 0;
						let entitySystemTableName = void 0;
						let entitySystemTableDetailScreenClass = void 0;
						const entitySystemTable = record.json.entitySystemTable;
						if (entitySystemTable) {
							entitySystemTableEntityListUrl = entitySystemTable.entityListUrl;
							entitySystemTableName = entitySystemTable.name;
							entitySystemTableDetailScreenClass = entitySystemTable.detailScreenClass;
						}
						fp.getForm().findField('contactAssignmentCategory.entitySystemTable.entityListUrl').setValue(entitySystemTableEntityListUrl);
						fp.getForm().findField('contactAssignmentCategory.entitySystemTable.name').setValue(entitySystemTableName);
						fp.getForm().findField('contactAssignmentCategory.entitySystemTable.detailScreenClass').setValue(entitySystemTableDetailScreenClass);
					}
				}
			},
			{fieldLabel: 'contactCategoryId', name: 'contactAssignmentCategory.contactCategory.id', type: 'int', hidden: true},
			{fieldLabel: 'entityListUrl', name: 'contactAssignmentCategory.entityListUrl', hidden: true},
			{fieldLabel: 'entitySystemTableEntityListUrl', name: 'contactAssignmentCategory.entitySystemTable.entityListUrl', hidden: true},
			{fieldLabel: 'entitySystemTableName', name: 'contactAssignmentCategory.entitySystemTable.name', hidden: true},
			{fieldLabel: 'entitySystemTableDetailScreenClass', name: 'contactAssignmentCategory.entitySystemTable.detailScreenClass', hidden: true},
			{fieldLabel: 'Assigned Role', name: 'contactAssignmentRole.name', hiddenName: 'contactAssignmentRole.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactAssignmentRoleListFind.json', detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentRoleWindow'},
			{
				fieldLabel: 'Contact', name: 'contact.label', hiddenName: 'contact.id', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.resetStore();
						const contactCategoryId = combo.getParentForm().getFormValue('contactAssignmentCategory.contactCategory.id');
						if (contactCategoryId) {
							combo.store.setBaseParam('contactCategoryId', contactCategoryId);
						}
					}
				}
			},
			{
				fieldLabel: 'Linked Entity', name: 'assignmentEntityLabel', hiddenName: 'fkFieldId', xtype: 'combo', detailIdField: 'fkFieldId', submitValue: true, displayField: 'label', url: 'REPLACE_ME_DYNAMICALLY', detailPageClass: 'REPLACE_ME_DYNAMICALLY',
				getDetailPageClass: function(form) {
					const entitySystemTableDetailScreenClass = TCG.getChildByName(this.getParentForm(), 'contactAssignmentCategory.entitySystemTable.detailScreenClass');
					if (entitySystemTableDetailScreenClass && entitySystemTableDetailScreenClass.getValue()) {
						return entitySystemTableDetailScreenClass.getValue();
					}
				},
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const assignmentCategoryField = TCG.getChildByName(combo.getParentForm(), 'contactAssignmentCategory.name');
						if (assignmentCategoryField) {
							if (TCG.isBlank(assignmentCategoryField.getValue())) {
								TCG.showError('Assignment Category must be selected in order to choose a Linked Entity.');
								return false;
							}
							const entityListUrl = TCG.getChildByName(combo.getParentForm(), 'contactAssignmentCategory.entityListUrl');
							if (entityListUrl && entityListUrl.getValue()) {
								combo.resetStore();
								combo.setUrl(entityListUrl.getValue());
							}
							else {
								const entitySystemTableEntityListUrl = TCG.getChildByName(combo.getParentForm(), 'contactAssignmentCategory.entitySystemTable.entityListUrl');
								if (entitySystemTableEntityListUrl && entitySystemTableEntityListUrl.getValue()) {
									combo.resetStore();
									combo.setUrl(entitySystemTableEntityListUrl.getValue());
								}
								else {
									const entitySystemTableName = TCG.getChildByName(combo.getParentForm(), 'contactAssignmentCategory.entitySystemTable.name');
									if (entitySystemTableName && entitySystemTableName.getValue()) {
										TCG.showError('Unable to determine entityListUrl for Entity Table [' + entitySystemTableName.getValue + '] associated with the selected Assignment Category.', 'Entity Table Missing entityListUrl');
									}
									else {
										TCG.showError('Unable to determine entityListUrl for the Entity Table associated with the selected Assignment Category.', 'Entity Table Missing entityListUrl');
									}
								}
							}
						}
					}
				}
			},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
		]
	}]
});
