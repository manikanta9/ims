Clifton.business.contact.assignment.ContactAssignmentSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'businessContactAssignmentAssignmentList',
	title: 'Contact Assignment Setup',
	iconCls: 'group',
	width: 1400,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Role Assignments',
				items: [{
					xtype: 'business-contact-assignments-grid'
				}]
			},


			{
				title: 'Assignment Roles',
				items: [{
					name: 'businessContactAssignmentRoleListFind',
					xtype: 'gridpanel',
					instructions: 'Contact Assignment Roles are used to define the responsibility of a contact for a given entity in the system.',
					topToolbarSearchParameter: 'searchPattern',
					additionalPropertiesToRequest: 'parentAssignmentRole.id|order',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', type: 'int', hidden: true},
						{header: 'Parent Role', width: 60, dataIndex: 'parentAssignmentRole.name', filter: {searchFieldName: 'parentAssignmentRoleId', type: 'combo', url: 'businessContactAssignmentRoleListFind.json'}},
						{header: 'Role Name', width: 60, dataIndex: 'name', filter: {searchFieldName: 'id', type: 'combo', url: 'businessContactAssignmentRoleListFind.json'}},
						{header: 'Description', width: 120, dataIndex: 'description'},
						{header: 'Assignment Modify Condition', width: 100, dataIndex: 'entityModifyCondition.name', filter: {searchFieldName: 'entityModifyConditionId'}},
						{header: 'Max Assignees', type: 'int', width: 30, dataIndex: 'maxAssigneesPerEntity', tooltip: 'Optional maximum number of assignees of this role that are allowed for the same entity.'},
						{header: 'Role Order', width: 30, type: 'int', dataIndex: 'roleOrder', useNull: true, defaultSortColumn: true}
					],
					editor: {
						detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentRoleWindow'
					}
				}]
			},


			{
				title: 'Assignment Categories',
				items: [{
					name: 'businessContactAssignmentCategoryListFind',
					xtype: 'gridpanel',
					instructions: 'Contact Assignment Categories are used to categorize Contact Assignments.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Contact Category', width: 50, dataIndex: 'contactCategory.name', filter: {type: 'combo', url: 'businessContactCategoryListFind.json', searchFieldName: 'contactCategoryId', detailPageClass: 'Clifton.business.contact.ContactCategoryWindow'}},
						{header: 'Category Name', width: 65, dataIndex: 'name', filter: {searchFieldName: 'id', type: 'combo', url: 'businessContactAssignmentCategoryListFind.json'}, defaultSortColumn: true},
						{header: 'Description', width: 175, dataIndex: 'description'},
						{header: 'Entity Table', width: 75, dataIndex: 'entitySystemTable.name', filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'entitySystemTableId', detailPageClass: 'Clifton.system.schema.TableWindow'}},
						{header: 'System Defined', width: 30, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentCategoryWindow'
					}
				}]
			}
		]
	}]
});

