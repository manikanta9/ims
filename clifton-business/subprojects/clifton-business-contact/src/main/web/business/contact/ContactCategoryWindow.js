Clifton.business.contact.ContactCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Contact Category',
	iconCls: 'group',

	items: [{
		xtype: 'formpanel',
		instructions: 'Business Contact Categories are used to categorize and define modify access to contacts and company-contact relationships. For company-contact relationships, category will first use what is defined on the contact type, and if missing, will then use the category on the contact type.',
		url: 'businessContactCategory.json',
		items: [
			{fieldLabel: 'Category Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{xtype: 'system-entity-modify-condition-combo'}
		]
	}]
});
