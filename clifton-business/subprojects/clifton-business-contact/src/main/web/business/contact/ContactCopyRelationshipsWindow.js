Clifton.business.contact.ContactCopyRelationshipsWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Contact - Move/Copy Relationships',
	iconCls: 'group',
	items: [{
		xtype: 'formpanel',
		instructions: 'Moving/Copying Contact relationships allows moving or copying company-contact and contact-contact relationships from one company to another and optionally deleting the original contact.  This can be useful for cleaning up duplicates in the system.',
		getSaveURL: function() {
			return 'businessContactRelationshipsCopy.json';
		},
		items: [
			{fieldLabel: 'From Contact', name: 'fromContactName', hiddenName: 'fromContactId', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow'},
			{fieldLabel: 'To Contact', name: 'toContactName', hiddenName: 'toContactId', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow'},
			{boxLabel: 'Move Contact Relationships (If unchecked will just copy relationships, otherwise will move them to the new contact and delete all relationships from the from contact)', name: 'moveRelationships', xtype: 'checkbox'},
			{boxLabel: 'DELETE FROM Contact', name: 'deleteFrom', xtype: 'checkbox'}
		],
		saveForm: function(closeOnSuccess, forms, form, panel, params) {
			const win = this;
			win.closeOnSuccess = closeOnSuccess;
			win.modifiedFormCount = forms.length;
			// TODO: data binding for partial field updates
			// TODO: concurrent updates validation
			form.trackResetOnLoad = true;
			form.submit(Ext.applyIf({
				url: encodeURI(panel.getSaveURL()),
				params: params,
				waitMsg: 'Saving...',
				success: function(form, action) {
					TCG.showInfo(action.result.result, 'Contact Move/Copy Status');
					win.savedSinceOpen = true;
					if (win.closeOnSuccess) {
						win.closeWindow();
					}
				}
			}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
		}
	}]
});
