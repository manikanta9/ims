TCG.use('Clifton.system.common.AddressFormFragment');

Clifton.business.contact.ContactWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Contact',
	height: 600,
	width: 650,
	iconCls: 'group',

	items: [{
		xtype: 'formwithtabs-custom-fields',
		columnGroupName: 'Employee Details',
		dynamicFieldFormFragment: 'contactCustomFields',
		url: 'businessContact.json',

		// Give Save Confirm if can find another contact with the same first/last name
		confirmBeforeSaveMsgTitle: 'Create New Contact?',
		getConfirmBeforeSaveMsg: function() {
			const newContact = TCG.isNull(this.getIdFieldValue());
			const firstName = this.getForm().findField('firstName').getValue();
			const lastName = this.getForm().findField('lastName').getValue();
			if (newContact) {
				this.confirmBeforeSaveMsgTitle = 'Create New Contact?';
				const matchedByName = TCG.data.getData('businessContactListFind.json?firstName=' + firstName + '&lastName=' + lastName + '&requestedPropertiesRoot=data&requestedProperties=label', this);

				if (matchedByName && matchedByName.length > 0) {
					let msg = 'Are you sure you want to add this contact?  Found [' + matchedByName.length + '] contact(s) already in the system with similar first/last name: ';
					for (let c = 0; c < matchedByName.length; c++) {
						msg += matchedByName[c].label;
						if (c !== (matchedByName.length - 1)) {
							msg += ', ';
						}
					}
					return msg;
				}
			}
			else {
				// shouldn't conflict with the above logic because a new contact won't have any relationships defined yet
				if (TCG.getChildByName(this, 'endDate').isDirty()) {
					const companyContacts = TCG.data.getData('businessCompanyContactListFind.json?contactId=' + this.getIdFieldValue() + '&noEndDate=true&requestedPropertiesRoot=data&requestedProperties=referenceOne.label|contactType.name', this);
					if (companyContacts.length > 0) {
						this.confirmBeforeSaveMsgTitle = 'Update Contact End Date?';
						let msg = 'Updating the End Date for <b>' + firstName + ' ' + lastName + '</b> will update the End Date for their relationship with <b>' + companyContacts.length + '</b> companies:<br><br>';
						msg += '<li>' + companyContacts.slice(0, 5).map(companyContact => companyContact.contactType.name + ' for ' + companyContact.referenceOne.label).join('</li><li>') + (companyContacts.length > 5 ? '</li><li>...</li>' : '</li>');
						return msg + '<br>Are you sure you want to update <b>' + firstName + ' ' + lastName + '\'s</b> End Date?';
					}
				}
			}
			return undefined;
		},


		items: [{
			xtype: 'tabpanel',
			requiredFormIndex: [0, 1],

			items: [
				{
					title: 'Contact',
					items: {
						xtype: 'formfragment',
						instructions: 'A contact can be associated with a company or another contact. The main company selected for a contact is determined as the only active Employee relationship for that contact.',
						items: [
							{
								xtype: 'fieldset',
								collapsible: false,
								title: 'Contact Info',
								labelWidth: 145,
								items: [
									{fieldLabel: 'Category', name: 'contactCategory.name', hiddenName: 'contactCategory.id', displayField: 'name', xtype: 'combo', loadAll: false, url: 'businessContactCategoryListFind.json', detailPageClass: 'Clifton.business.contact.ContactCategoryWindow'},
									{
										fieldLabel: 'Main Company', name: 'company.name', xtype: 'linkfield', detailPageClass: 'Clifton.business.company.CompanyWindow',
										qtip: 'Main Company of a contact is managed by the system and populated when there is ONLY ONE Employee relationship to a company for the contact.',
										getDetailIdFieldValue: function(formPanel) {
											return TCG.getValue('company.id', formPanel.getForm().formValues);
										}
									},
									{fieldLabel: 'First Name', name: 'firstName'},
									{fieldLabel: 'Middle Name', name: 'middleName'},
									{fieldLabel: 'Last Name', name: 'lastName'},
									{fieldLabel: 'Prefix', name: 'namePrefix'},
									{fieldLabel: 'Suffix', name: 'nameSuffix'},
									{fieldLabel: 'Saluation', name: 'saluation'},
									{fieldLabel: 'Job Title', name: 'title'},
									{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
									{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Employee Details',
								collapsed: true,
								items: [{
									xtype: 'formfragment',
									frame: false,
									name: 'contactCustomFields',
									labelWidth: 145,
									bodyStyle: 'padding: 0',
									defaults: {anchor: '0'},
									items: []
								}]
							},
							{
								xtype: 'system-tags-fieldset',
								title: 'Tags',
								tableName: 'BusinessContact',
								labelWidth: 145,
								collapsed: true
							}
						]
					}
				},


				{
					title: 'Address',
					items: {
						xtype: 'address-formfragment',
						useFieldSet: true,

						getAddressWarningMessage: function(form) {
							const cName = TCG.getValue('company.name', form.formValues);
							if (TCG.isNotBlank(cName)) {
								let valuePrefix = 'This contact is using a <b>different</b> address than its main company:&nbsp;';
								if (TCG.isTrue(form.formValues.usingCompanyAddress)) {
									valuePrefix = 'This contact is using the domicile address populated from:&nbsp;';
								}
								return [{
									xtype: 'linkfield', style: 'padding: 0', valuePrefix: valuePrefix, value: cName, detailPageClass: 'Clifton.business.company.CompanyWindow', detailIdField: 'company.id',
									qtip: 'Contact\'s with a main company will be default use the company domicile address.  Changes made here will apply to this contact only.  Clear the address fields below to re-synchronize the address with the company domicile address.'
								}];

							}
							return undefined;
						},

						resetAddressWarning: function() {
							const fs = this.addressFieldSet;
							const fp = (this instanceof Ext.form.FormPanel) ? this : TCG.getParentFormPanel(this);
							const msg = this.getAddressWarningMessage(fp.getForm());
							if (this.warningMsg !== msg) {
								this.warningMsg = msg;
								const warnId = this.id + '-warn';
								const warn = Ext.get(warnId);
								if (TCG.isNotNull(warn)) {
									fs.remove(0);
									Ext.destroy(warn);
									fs.doLayout();
								}
								if (TCG.isNotNull(msg)) {
									fs.insert(0, {xtype: 'container', layout: 'hbox', id: warnId, autoEl: 'div', cls: this.warningMessageCls, items: msg});
									fs.doLayout();
								}
							}
						},

						listeners: {
							'afterrender': function() {
								this.resetAddressWarning();

								const addFF = this;
								const fp = (this instanceof Ext.form.FormPanel) ? this : TCG.getParentFormPanel(this);

								if (fp) {
									fp.doLayout();
									fp.addListener('afterload', function(fp) {
										addFF.resetAddressWarning();
									});
								}
							}
						},

						itemsBefore: [{
							xtype: 'fieldset',
							collapsible: false,
							title: 'Contact Info',
							items: [
								{fieldLabel: 'Phone', name: 'phoneNumber'},
								{fieldLabel: 'Mobile', name: 'mobileNumber'},
								{fieldLabel: 'Fax', name: 'faxNumber'},
								{fieldLabel: 'E-Fax', name: 'efaxNumber'},
								{fieldLabel: 'Email', name: 'emailAddress', vtype: 'email'},
								{fieldLabel: 'Secondary Email', name: 'emailAddressSecondary', vtype: 'email'}
							]
						}]
					}
				},


				{
					title: 'Notes',
					items: [{
						xtype: 'document-system-note-grid',
						tableName: 'BusinessContact'
					}]
				},


				{
					title: 'Assignments',
					items: [{
						name: 'businessContactAssignmentListFind',
						xtype: 'gridpanel',
						instructions: 'This contact is assigned to the following entities in the following roles.',
						topToolbarSearchParameter: 'assignmentEntityLabel',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', type: 'int', hidden: true},
							{header: 'FkFieldID', width: 50, dataIndex: 'fkFieldId', type: 'int', filter: {searchFieldName: 'fkFieldId'}, doNotFormat: true, useNull: true, hidden: true},
							{header: 'Linked Entity', width: 125, dataIndex: 'assignmentEntityLabel'},
							{header: 'Assigned Role', width: 50, dataIndex: 'contactAssignmentRole.name', filter: {searchFieldName: 'contactAssignmentRoleId', type: 'combo', url: 'businessContactAssignmentRoleListFind.json'}},
							{header: 'Assignment Category', width: 75, dataIndex: 'contactAssignmentCategory.name', hidden: true},
							{header: 'Active', width: 25, dataIndex: 'active', type: 'boolean'},
							{header: 'Start Date', width: 75, dataIndex: 'startDate', hidden: true},
							{header: 'End Date', width: 75, dataIndex: 'endDate', hidden: true}
						],
						getTopToolbarInitialLoadParams: function() {
							return {businessContactId: this.getWindow().getMainFormId()};
						},
						editor: {
							detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentWindow',
							drillDownOnly: true
						}
					}]
				},


				{
					title: 'Companies',
					items: [{
						name: 'businessCompanyContactListFind',
						xtype: 'gridpanel',
						instructions: 'The following companies are associated with this contact.',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'Company', width: 150, dataIndex: 'referenceOne.nameExpandedWithType', filter: {searchFieldName: 'companyName'}},
							{header: 'Contact Type', width: 120, dataIndex: 'contactType.label', filter: {type: 'combo', url: 'businessContactTypeListFind.json', searchFieldName: 'contactTypeId'}},
							{header: 'Start Date', width: 65, dataIndex: 'startDate', hidden: true},
							{header: 'End Date', width: 65, dataIndex: 'endDate', hidden: true},
							{header: 'Primary', width: 50, dataIndex: 'primary', type: 'boolean'},
							{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false}
						],
						editor: {
							detailPageClass: 'Clifton.business.company.CompanyContactWindow',
							getDefaultData: function(gridPanel) { // defaults contact id for the detail page
								return {
									referenceTwo: gridPanel.getWindow().getMainForm().formValues
								};
							}
						},
						getLoadParams: function() {
							return {'contactId': this.getWindow().getMainFormId()};
						}
					}]
				},


				{
					title: 'Relationships',
					items: [{
						name: 'businessContactRelationshipListByContact',
						xtype: 'gridpanel',
						instructions: 'The following contacts are associated with this contact.',
						columns: [
							{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
							{header: 'First Contact', width: 100, dataIndex: 'referenceOne.label'},
							{header: 'Second Contact', width: 100, dataIndex: 'referenceTwo.label'},
							{header: 'Relationship', width: 100, dataIndex: 'name'},
							{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false}
						],
						editor: {
							detailPageClass: 'Clifton.business.contact.ContactRelationshipWindow',
							getDefaultData: function(gridPanel) { // defaults contact id for the detail page
								return {
									referenceOne: gridPanel.getWindow().getMainForm().formValues
								};
							}
						},
						getLoadParams: function() {
							return {'contactId': this.getWindow().getMainFormId()};
						}
					}]
				},


				{
					title: 'Photo',
					items: [{
						xtype: 'document-version-grid',
						tableName: 'BusinessContact',
						showEditToolbar: true,
						hideFileFormat: true,
						hideSecurityButton: true,
						hideEditOnlineButton: true,
						useDynamicTooltipColumns: true,
						columns: [
							{header: 'Document ID', width: 30, dataIndex: 'documentId', hidden: true},
							{
								header: '&nbsp;', width: 10, filter: false, sortable: false,
								renderer: function(v, metaData, r) {
									return TCG.renderDynamicTooltipColumn('bubble', 'ShowPhoto');
								}
							},
							{header: 'Version', width: 40, dataIndex: 'versionLabel'},
							{header: 'Red Line', width: 40, dataIndex: 'redLine', type: 'boolean', hidden: true},
							{header: 'Comments', width: 200, dataIndex: 'checkinComment'},
							{header: 'Changed By', width: 60, dataIndex: 'updateUser'},
							{header: 'Changed On', width: 85, dataIndex: 'updateDate'}
						],
						getTooltipTextForRow: function(row, id, tooltipEventName) {
							const params = {
								tableName: 'BusinessContact',
								fkFieldId: this.getWindow().getMainFormId(),
								documentId: row.data.documentId
							};
							return '<img src="documentRecordDownload.json?' + Ext.urlEncode(params) + '" width="400" height="400" />';
						}
					}]
				}
			]
		}]
	}]
});
