Clifton.business.contact.ContactRelationshipWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Contact Relationship',
	iconCls: 'group',
	items: [{
		xtype: 'formpanel',
		instructions: 'A Contact Relationship associates a contact with another contact.',
		url: 'businessContactRelationship.json',
		items: [
			{fieldLabel: 'First Contact', name: 'referenceOne.label', hiddenName: 'referenceOne.id', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow'},
			{fieldLabel: 'Relationship Name', name: 'name'},
			{fieldLabel: 'Second Contact', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow'},

			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
		]
	}]
});
