Clifton.business.company.CompanyContactMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company Contact Mapping',
	iconCls: 'link',
	width: 1000,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Mapping',
				items: [{
					xtype: 'formpanel',
					instructions: 'Company Contacts can only be created if there is a mapping here that allows a relationship for the type of the Contact and the type of the Company (optional).',
					url: 'businessCompanyContactMapping.json',
					labelWidth: 130,
					items: [
						{fieldLabel: 'Contact Type', name: 'businessContactType.name', hiddenName: 'businessContactType.id', xtype: 'combo', url: 'businessContactTypeListFind.json'},
						{fieldLabel: 'Company Type', name: 'businessCompanyType.name', hiddenName: 'businessCompanyType.id', xtype: 'combo', url: 'businessCompanyTypeListFind.json'},
						{
							fieldLabel: 'Company Contact Modify Condition', name: 'companyContactModifyCondition.name', hiddenName: 'companyContactModifyCondition.id', xtype: 'system-condition-combo',
							qtip: 'When adding/editing/deleting Company Contacts for this mapping, the following modify condition must evaluate to true. This can be used to allow certain users access to modifying specific Company Type/Contact Type associations.'
						},
						{fieldLabel: '', boxLabel: 'Generate notifications for companies that are missing a contact with the selected Contact Type', name: 'required', xtype: 'checkbox'},
						{boxLabel: 'Allow only one contact with the selected Contact Type', name: 'oneAllowed', xtype: 'checkbox'},
						{boxLabel: 'Generate notifications for companies that have multiple contacts with the selected Contact Type and don\'t have exactly one contact specified as the primary', name: 'onePrimaryRequired', xtype: 'checkbox'},
						{
							fieldLabel: '', boxLabel: 'Warn the user during company contact updates that the selected Company Type is NOT the preferred level for the selected Contact Type', name: 'notPreferred', xtype: 'checkbox',
							qtip: 'Used to generate Notifications to prevent users from accidentally adding relationships at the wrong "level" (e.g. Client vs. Client Relationships)'
						}
					]
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'BusinessCompanyContactMapping'
				}]
			},


			{
				title: 'Company Contacts',
				items: [{
					xtype: 'business-companyContactGrid',
					instructions: 'This mapping applies to the following company/contact relationships.',
					reloadOnRender: true,
					columnOverrides: [
						{dataIndex: 'contactType.label', hidden: true},
						{dataIndex: 'referenceOne.type.label', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						const params = TCG.callSuper(this, 'getLoadParams', arguments);
						params.contactTypeId = this.getWindow().getMainForm().findField('businessContactType.id').value;
						params.companyTypeId = this.getWindow().getMainForm().findField('businessCompanyType.id').value;
						return params;
					}
				}]
			}
		]
	}]
});
