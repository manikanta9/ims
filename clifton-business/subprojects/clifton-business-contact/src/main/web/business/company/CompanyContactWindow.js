Clifton.business.company.CompanyContactWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company Contact',
	iconCls: 'group',
	width: 600,
	height: 475,
	items: [{
		xtype: 'formpanel-custom-fields',
		url: 'businessCompanyContact.json',
		columnGroupName: 'Company Contact Custom Fields',
		instructions: 'Company-Contact associates a contact with a company for a specified time period',
		labelWidth: 175,
		items: [
			{fieldLabel: 'Company', name: 'referenceOne.nameExpandedWithType', hiddenName: 'referenceOne.id', displayField: 'nameExpandedWithType', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{
				fieldLabel: 'Contact', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.resetStore();
						combo.store.setBaseParam('contactTypeId', combo.getParentForm().getFormValue('contactType.label', true, true));
					}
				}
			},
			{fieldLabel: 'Contact Type', name: 'contactType.label', hiddenName: 'contactType.id', xtype: 'combo', loadAll: false, url: 'businessContactTypeListFind.json', detailPageClass: 'Clifton.business.contact.ContactTypeWindow', displayField: 'label'},

			{fieldLabel: 'Functional Role', name: 'functionalRole'},
			{fieldLabel: 'Primary', name: 'primary', xtype: 'checkbox'},
			{boxLabel: 'Private Relationship (excluded from Client facing reports)', name: 'privateRelationship', xtype: 'checkbox'},
			{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: true},
			{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', allowBlank: true},
			{fieldLabel: 'Display Order', name: 'displayOrder', xtype: 'spinnerfield', minValue: 1, maxValue: 1000}
		]
	}]
});
