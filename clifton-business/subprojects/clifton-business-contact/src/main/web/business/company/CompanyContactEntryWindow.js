Clifton.business.company.CompanyContactEntryWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Company Contact Relationships',
	iconCls: 'group',
	width: 1050,
	height: 650,

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			controlWindowModified: false,
			getSaveURL: function() {
				return 'businessCompanyContactEntrySave.json';
			},
			instructions: 'Use the following screen to view and edit company contact relationships for the currently company and selected contact.',
			confirmChangeCompany: async function() {
				return 'yes' === await TCG.showConfirm('You are attempting to view Company Contact Relationships for a different company, but there are unsaved changes for the current company. Would you like to discard these changes?', 'Discard Changes');
			},
			confirmChangeContact: async function() {
				return 'yes' === await TCG.showConfirm('You are attempting to view Company Contact Relationships for a different contact, but there are unsaved changes for the current contact. Would you like to discard these changes?', 'Discard Changes');
			},
			items: [
				{
					fieldLabel: 'Company', name: 'referenceOne.name', hiddenName: 'referenceOne.id', displayField: 'nameExpandedWithType', xtype: 'combo', loadAll: false, url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow', allowBlank: false,
					onSelect: async function(record, index) {
						const formPanel = TCG.getParentByClass(this, TCG.form.FormPanel);
						if (TCG.getChildByName(formPanel, 'companyContactListGrid').gridModified && !await formPanel.confirmChangeCompany()) {
							return;
						}
						TCG.callSuper(this, 'onSelect', arguments);
						TCG.getChildByName(formPanel, 'companyContactListGrid').reloadGrid();
					},
					onSpecialKey: async function(field, e) {
						const formPanel = TCG.getParentByClass(this, TCG.form.FormPanel);
						if (TCG.getChildByName(formPanel, 'companyContactListGrid').gridModified && !await formPanel.confirmChangeCompany()) {
							return;
						}
						TCG.callSuper(this, 'onSpecialKey', arguments);
						TCG.getChildByName(formPanel, 'companyContactListGrid').reloadGrid();
					}
				},
				{
					fieldLabel: 'Contact', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', displayField: 'label', xtype: 'combo', loadAll: false, url: 'businessContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow', allowBlank: false,
					onSelect: async function(record, index) {
						const formPanel = TCG.getParentByClass(this, TCG.form.FormPanel);
						if (TCG.getChildByName(formPanel, 'companyContactListGrid').gridModified && !await formPanel.confirmChangeContact()) {
							return;
						}
						TCG.callSuper(this, 'onSelect', arguments);
						TCG.getChildByName(formPanel, 'companyContactListGrid').reloadGrid();
					},
					onSpecialKey: async function(field, e) {
						const formPanel = TCG.getParentByClass(this, TCG.form.FormPanel);
						if (TCG.getChildByName(formPanel, 'companyContactListGrid').gridModified && !await formPanel.confirmChangeContact()) {
							return;
						}
						TCG.callSuper(this, 'onSpecialKey', arguments);
						TCG.getChildByName(formPanel, 'companyContactListGrid').reloadGrid();
					}
				},

				{
					xtype: 'formgrid',
					title: 'Company Contact Relationships',
					name: 'companyContactListGrid',
					storeRoot: 'companyContactList',
					controlWindowModified: true,
					dtoClass: 'com.clifton.business.company.contact.BusinessCompanyContact',
					gridModified: false,
					columnsConfig: [

						{header: 'Contact Type', width: 250, dataIndex: 'contactType.label', idDataIndex: 'contactType.id', editor: {xtype: 'combo', url: 'businessContactTypeListFind.json', detailPageClass: 'Clifton.business.contact.ContactTypeWindow', allowBlank: false}},
						{header: 'Functional Role', dataIndex: 'functionalRole', width: 200, editor: {xtype: 'textfield'}},
						{header: 'Primary', dataIndex: 'primary', width: 60, type: 'boolean', editor: {xtype: 'checkbox'}},
						{header: 'Private', dataIndex: 'privateRelationship', width: 60, type: 'boolean', editor: {xtype: 'checkbox'}, tooltip: 'Private Relationship (excluded from Client Facing Reports)'},
						{header: 'Active', width: 80, dataIndex: 'active', type: 'boolean', sortable: false, hidden: false},
						{header: 'Start Date', dataIndex: 'startDate', width: 80, editor: {xtype: 'datefield'}},
						{header: 'End Date', dataIndex: 'endDate', width: 80, editor: {xtype: 'datefield'}},
						{header: 'Display Order', dataIndex: 'displayOrder', width: 80, type: 'int', editor: {xtype: 'spinnerfield', minValue: 1, maxValue: 1000}, useNull: true},
						{
							width: 25, renderer: function(v, args, r) {
								if (r.json && TCG.isNotNull(r.json.id)) {
									return TCG.renderActionColumn('group', '', 'View/Edit Details of this specific relationship', 'view');
								}
								return '';
							}
						}
					],

					listeners: {
						afterRender: function() {
							const fp = TCG.getParentFormPanel(this);
							this.reloadGrid.defer(10, this);

							fp.on('afterload', function() {
								this.reloadGrid();
							}, this);

							const w = this.getWindow();
							w.on('modified', function(win, mod) {
								if (mod) {
									this.gridModified = true;
									TCG.getParentFormPanel(this).controlWindowModified = true;
								}
								else {
									this.gridModified = false;
									TCG.getParentFormPanel(this).controlWindowModified = false;
								}
							}, this);
						},

						// Drill Into Event
						'cellclick': function(grid, rowIndex, cellIndex, evt) {
							if (TCG.isActionColumn(evt.target)) {
								if (TCG.isTrue(grid.gridModified)) {
									TCG.showError('Please save your changes before viewing company contact details.  If you wish to discard your changes, click Reload to refresh.');
									return;
								}

								const row = grid.store.data.items[rowIndex];
								const id = row.json['id'];
								const eventName = TCG.getActionColumnEventName(evt);
								if (eventName === 'view') {
									TCG.createComponent('Clifton.business.company.CompanyContactWindow', {
										id: TCG.getComponentId('Clifton.business.company.CompanyContactWindow', id),
										params: {id: id},
										openerCt: grid
									});
								}
							}
						}
					},

					addToolbarButtons: function(toolBar) {
						const gp = this;
						toolBar.add({
							text: 'Reload',
							tooltip: 'Load Existing Company Contact Relationships for the selected company and contact',
							iconCls: 'table-refresh',
							scope: this,
							handler: function() {
								gp.reloadGrid(true);
							}
						});
					},

					reload: function() {
						this.reloadGrid(false);
					},

					reloadGrid: function(showError) {
						const grid = this;
						// get the submit parameters
						const panel = TCG.getParentFormPanel(this);
						const form = panel.getForm();

						const businessCompanyId = form.findField('referenceOne.id').getValue();
						const businessContactId = form.findField('referenceTwo.id').getValue();

						if (TCG.isBlank(businessCompanyId) || TCG.isBlank(businessContactId)) {
							if (TCG.isTrue(showError)) {
								TCG.showError('A company and contact must be selected in order to view existing relationships.');
							}
							return;
						}

						const loader = new TCG.data.JsonLoader({
							waitTarget: TCG.getParentFormPanel(grid).getEl(),
							params: {businessCompanyId: businessCompanyId, businessContactId: businessContactId},
							scope: grid,
							onLoad: function(record, conf) {
								if (grid.store.loadData) {  // not sure why, but console error is logged without this check - functionality still works though
									grid.store.loadData(record);
								}
								// Reset Window Modified
								grid.getWindow().setModified(false);
							}
						});
						loader.load('businessCompanyContactEntry.json');
					}

				}
			]
		}
	]
});
