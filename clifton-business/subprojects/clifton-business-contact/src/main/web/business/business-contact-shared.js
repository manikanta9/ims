Ext.ns('Clifton.business.contact', 'Clifton.business.contact.assignment', 'Clifton.business.contact.assignment.upload', 'Clifton.business.company.contact');

Clifton.system.schema.SimpleUploadWindows['BusinessContactAssignment'] = 'Clifton.business.contact.assignment.upload.ContactAssignmentUploadWindow';

Clifton.business.CompanyContactEntryGrid_ByCompany = Ext.extend(TCG.grid.GridPanel, {
	instructions: 'The following contacts are associated with this client\'s company. To remove a company contact, drill into that contact\'s row.',
	includeAllColumnsView: false,
	remoteSort: false,

	getLoadURL: function() {
		let url = 'businessCompanyContactEntryListFind.json';
		if (this.currentViewName === 'Not Pivoted (Old View)') {
			url = 'businessCompanyContactListFind.json';
		}
		return url;
	},

	listeners: {
		afterRender: function() {
			this.contactTypeSet = new Set();
			this.grid.store.on('load', () => this.loadHandler());
			// Note: Because the URL Changes Based on the View, need to make sure URL is updated appropriately.
			// Otherwise when clicking on a column to sort it reverts back to the original default run view url.
			const gp = this;
			this.grid.store.on('beforeload', function(store, options) {
				store.url = encodeURI(gp.getLoadURL());
				store.proxy.setUrl(store.url);
				store.reader.meta.root = (TCG.isBlank(gp.currentViewName) || gp.currentViewName === 'Pivoted (New View)') ? 'data.rows' : 'data';
				delete store.reader.ef;
				store.reader.buildExtractors();
			});
		}
	},

	loadHandler: function() {
		if (TCG.isBlank(this.currentViewName) || this.currentViewName === 'Pivoted (New View)') {
			const grid = this.grid;
			grid.store.each(contactEntry => {
				try {
					contactEntry.beginEdit();
					Ext.each(contactEntry.json.companyContactList, companyContact => {
						const contactTypeName = companyContact.contactType.name;
						if (!this.contactTypeSet.has(contactTypeName)) {
							const column = {
								header: contactTypeName, width: 80, dataIndex: contactTypeName, type: 'boolean', filter: false, sortable: true, tooltip: contactTypeName, viewNames: ['Pivoted (New View)'],
								renderer: function(value, meta, row) {
									return TCG.renderBoolean(value, contactTypeName);
								}
							};
							this.contactTypeSet.add(contactTypeName);
							grid.addColumn(column);
						}
						contactEntry.set(contactTypeName, true);

						if (TCG.isTrue(companyContact.active)) {
							contactEntry.set('active', true);
						}
						if (TCG.isTrue(companyContact.primary)) {
							contactEntry.set('primary', true);
						}
						if (TCG.isTrue(companyContact.privateRelationship)) {
							contactEntry.set('privateRelationship', true);
						}
					});
				}
				catch (e) {
					console.warn(e);
				}
				finally {
					contactEntry.commit();
				}
			});
			const contactColumn = {
				width: 18, dataIndex: 'referenceTwo.id', renderer: function(url, args) {
					return TCG.renderActionColumn('group', '', 'Open Related Contact', 'Contact');
				}
			};
			this.grid.addColumn(contactColumn);
		}
	},

	isIncludeRelatedCompanyCheckbox: function() {
		return false;
	},
	getRelatedCompanyCheckboxLabel: function() {
		return 'Include Parent/Child Company Relationships';
	},


	// Override if Company is not the Main Form's Entity
	getCompany: function() {
		return this.getWindow().getMainForm().formValues;
	},

	getCompanyId: function() {
		return this.getCompany().id;
	},

	exportGrid: function(outputFormat, includeHidden) {
		TCG.showError('Export is not available for this screen at this time.  Please use the Print option.', 'Not Available');
	},


	groupField: 'coalesceContactCompany.name',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Relationships" : "Relationship"]})',
	additionalPropertiesToRequest: 'companyContactList|referenceOne.id|referenceOne.type.name|referenceTwo.id',
	topToolbarSearchParameter: 'expandedSearchPattern',
	viewNames: ['Pivoted (New View)', 'Not Pivoted (Old View)'],

	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Main Company', width: 100, dataIndex: 'coalesceContactCompany.name', filter: {searchFieldName: 'coalesceContactCompanyName'}, hidden: true},
		{
			header: 'Company Type', width: 65, dataIndex: 'referenceOne.type.name', filter: {searchFieldName: 'companyTypeName'}, hidden: true,
			renderer: function(v, metaData, r) {
				if (v === 'Client Relationship') {
					metaData.css = 'amountPositive';
				}
				else if (v === 'Client' || v === 'Client Group' || v === 'Commingled Vehicle' || v === 'Virtual Client') {
					metaData.css = 'amountAdjusted';
				}
				metaData.attr = 'qtip=\'' + r.data['referenceOne.name'] + '\'';
				return v;
			}
		},
		{header: 'Company', width: 100, dataIndex: 'referenceOne.name', filter: false, sortable: false, hidden: true, renderer: TCG.renderValueWithTooltip},
		{header: 'Contact Name', width: 100, dataIndex: 'referenceTwo.nameLabel', filter: {searchFieldName: 'contactName'}},
		{header: 'Contact', width: 100, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'searchPattern'}, hidden: true},
		{header: 'Display Order', width: 50, dataIndex: 'displayOrder', type: 'int', useNull: true, hidden: true},
		{header: 'Type Category', width: 65, dataIndex: 'contactType.contactCategory.name', filter: {searchFieldName: 'contactTypeCategoryId', type: 'combo', url: 'businessContactCategoryListFind.json'}, hidden: true},
		{header: 'Contact Type', width: 110, dataIndex: 'contactType.label', filter: {searchFieldName: 'contactTypeName'}, hidden: true, viewNames: ['Not Pivoted (Old View)']},
		{header: 'Functional Role', width: 75, dataIndex: 'functionalRole', hidden: true, viewNames: ['Not Pivoted (Old View)']},
		{
			header: 'Functional Role(s)', hidden: false, width: 100, dataIndex: '', type: 'textarea', readOnly: true, filter: false, sortable: false, viewNames: ['Pivoted (New View)'],
			renderer: function(value, metaData, row) {
				const result = (row.json.companyContactList) ? row.json.companyContactList.map(companyContact => companyContact.functionalRole)
					.filter(functionalRole => TCG.isNotBlank(functionalRole))
					.join(', ') : '';
				return TCG.renderTextNowrapWithTooltip(result, metaData);
			}
		},
		{header: 'Title', width: 120, dataIndex: 'referenceTwo.title', filter: {searchFieldName: 'contactTitle'}},
		{header: 'Phone', width: 60, dataIndex: 'referenceTwo.phoneNumber', filter: {searchFieldName: 'contactPhoneNumber'}},
		{header: 'Mobile', width: 100, dataIndex: 'referenceTwo.mobileNumber', hidden: true, filter: {searchFieldName: 'contactMobileNumber'}},
		{header: 'Fax', width: 100, dataIndex: 'referenceTwo.faxNumber', hidden: true, filter: {searchFieldName: 'contactFaxNumber'}},
		{header: 'Email', width: 115, dataIndex: 'referenceTwo.emailAddress', renderer: TCG.renderEmailAddress, filter: {searchFieldName: 'contactEmailAddress'}, viewNames: ['Pivoted (New View)']},
		{header: 'Secondary Email', width: 100, dataIndex: 'referenceTwo.emailAddressSecondary', renderer: TCG.renderEmailAddress, hidden: true, filter: {searchFieldName: 'contactEmailAddressSecondary'}},
		{header: 'Address', width: 100, dataIndex: 'referenceTwo.addressLabel', hidden: true, filter: {searchFieldName: 'contactAddressLabel'}},
		{header: 'City/State', width: 75, dataIndex: 'referenceTwo.cityState', hidden: true, filter: {searchFieldName: 'contactCityState'}},
		{
			header: 'Primary', width: 35, dataIndex: 'primary', type: 'boolean', hidden: true, viewNames: ['Not Pivoted (Old View)'],
			tooltip: 'True if the Contact is a Primary for one or more of the their Contact Types.'
		},
		{
			header: 'Private', width: 35, dataIndex: 'privateRelationship', type: 'boolean', hidden: true, viewNames: ['Not Pivoted (Old View)'],
			tooltip: 'Private relationships are excluded from Client facing reports. True if the Contact is Private for one or more of their Contact Types.'
		},
		{header: 'Start Date', width: 50, dataIndex: 'startDate', hidden: true, viewNames: ['Not Pivoted (Old View)']},
		{header: 'End Date', width: 50, dataIndex: 'endDate', hidden: true, viewNames: ['Not Pivoted (Old View)']},
		{
			header: 'Active', width: 35, dataIndex: 'active', type: 'boolean', sortable: false,
			tooltip: 'True if the Contact is Active for one or more of their Contact Types.'
		}
	],
	editor: {
		deleteEnabled: false,
		detailPageClass: 'Clifton.business.company.CompanyContactEntryWindow',
		getDefaultData: function(gridPanel, row) {
			return {
				referenceOne: TCG.isBlank(row) ? gridPanel.getCompany() : TCG.getValue('referenceOne', row.json),
				referenceTwo: TCG.isBlank(row) ? null : TCG.getValue('referenceTwo', row.json)
			};
		},
		addToolbarAddButton: function(toolBar, gridPanel) {
			const editor = this;
			toolBar.add({
				text: 'Add',
				xtype: 'button',
				tooltip: 'Edit/Enter Multiple relationships for a Company and Contact',
				iconCls: 'add',
				handler: function() {
					editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
				}
			});
			toolBar.add('-');
		}
	},
	getTopToolbarFilters: function(toolbar) {
		const menu = new Ext.menu.Menu();
		this.appendIncludeOptions(menu);
		if (menu.items && menu.items.length > 0) {
			toolbar.add({
				text: 'Include',
				iconCls: 'find',
				name: 'includeOptions',
				menu: menu
			});
			toolbar.add('-');
		}

		return [
			{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'},
			{fieldLabel: 'Contact Type Category', xtype: 'toolbar-combo', name: 'contactTypeCategoryId', width: 180, url: 'businessContactCategoryListFind.json', displayField: 'name', linkedFilter: 'contactType.contactCategory.name'}
		];
	},

	appendIncludeOptions: function(menu) {
		const gridPanel = this;
		if (TCG.isTrue(this.isIncludeRelatedCompanyCheckbox())) {
			menu.add(
				{
					text: this.getRelatedCompanyCheckboxLabel()
					, xtype: 'menucheckitem'
					, name: 'includeRelatedCompanies'
					, checkHandler: function() {
						gridPanel.reload();
					}
				});
		}
		this.appendAdditionalIncludeOptions(menu);
	},
	appendAdditionalIncludeOptions: function(menu) {
		// OVERRIDE ME TO ADD MORE OPTIONS
	},

	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('active', true);
			const addCompConfig = this.getWindow().additionalComponentConfig;
			if (addCompConfig && addCompConfig.contactType && addCompConfig.contactType.label) {
				this.setFilterValue('contactType.label', addCompConfig.contactType.label, false, true);
			}
		}

		const params = {companyId: this.getCompanyId()};

		const includeOptions = TCG.getChildByName(this.getTopToolbar(), 'includeOptions');
		if (includeOptions && includeOptions.menu) {
			this.appendIncludeOptionsLoadParams(includeOptions.menu, params);
		}
		return params;
	},


	appendIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
		let showCompany = false;
		if (TCG.isTrue(this.isIncludeRelatedCompanyCheckbox())) {
			const inclRelated = TCG.getChildByName(includeOptionsMenu, 'includeRelatedCompanies').checked;

			if (inclRelated) {
				showCompany = true;
				delete params.companyId;
				params.companyIdOrRelatedCompany = this.getCompanyId();
			}
		}

		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('referenceOne.name'), showCompany === false);

		this.appendAdditionalIncludeOptionsLoadParams(includeOptionsMenu, params);
	},

	appendAdditionalIncludeOptionsLoadParams: function(includeOptionsMenu, params) {
		// OVERRIDE ME IF ADDITIONAL OPTIONS ARE ADDED TO MENU
	},

	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					const gridPanel = grid.ownerGridPanel;
					const eventName = TCG.getActionColumnEventName(evt);

					if (eventName === 'Contact') {
						const id = row.json.referenceTwo.id;
						gridPanel.editor.openDetailPage('Clifton.business.contact.ContactWindow', gridPanel, id, row);
					}
				}
			}
		}
	}
});
Ext.reg('business-companyContactEntryGrid-byCompany', Clifton.business.CompanyContactEntryGrid_ByCompany);


Clifton.business.CompanyContactGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'businessCompanyContactListFind',
	instructions: 'The following company/contact relationships are in the system.',
	topToolbarSearchParameter: 'expandedSearchPattern',
	reloadOnRender: false,
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Company', width: 300, dataIndex: 'referenceOne.nameExpandedWithType', filter: {searchFieldName: 'companyName'}},
		{header: 'Contact Name', width: 100, dataIndex: 'referenceTwo.nameLabel', filter: {searchFieldName: 'contactName'}},
		{header: 'First Name', width: 75, hidden: true, dataIndex: 'referenceTwo.firstName', filter: {searchFieldName: 'contactName'}},
		{header: 'Last Name', width: 75, hidden: true, dataIndex: 'referenceTwo.lastName', filter: {searchFieldName: 'contactName'}},
		{header: 'Email Address', width: 75, hidden: true, dataIndex: 'referenceTwo.emailAddress', filter: {searchFieldName: 'contactEmailAddress'}},
		{header: 'Phone Number', width: 75, hidden: true, dataIndex: 'referenceTwo.phoneNumber', filter: {searchFieldName: 'contactPhoneNumber'}},
		{header: 'Display Order', width: 50, dataIndex: 'displayOrder', type: 'int', useNull: true, hidden: true},
		{header: 'Type Category', width: 65, dataIndex: 'contactType.contactCategory.name', filter: {searchFieldName: 'contactTypeCategoryId', type: 'combo', url: 'businessContactCategoryListFind.json'}, hidden: true},
		{header: 'Contact Type', width: 120, dataIndex: 'contactType.label', filter: {type: 'combo', url: 'businessContactTypeListFind.json', searchFieldName: 'contactTypeId'}},
		{header: 'Company Type', width: 120, dataIndex: 'referenceOne.type.label', filter: {type: 'combo', url: 'businessCompanyTypeListFind.json', searchFieldName: 'companyTypeId'}},
		{header: 'Functional Role', width: 100, dataIndex: 'functionalRole'},
		{header: 'Start Date', width: 65, dataIndex: 'startDate', hidden: true},
		{header: 'End Date', width: 65, dataIndex: 'endDate', hidden: true},
		{header: 'Primary', width: 50, dataIndex: 'primary', type: 'boolean'},
		{
			header: 'Private', width: 50, dataIndex: 'privateRelationship', type: 'boolean',
			tooltip: 'Private relationships are excluded from Client facing reports.'
		},
		{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false}
	],
	editor: {
		detailPageClass: 'Clifton.business.company.CompanyContactWindow',
		deleteURL: 'businessCompanyContactDelete.json'
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'},
			{fieldLabel: 'Contact Type Category', xtype: 'toolbar-combo', name: 'contactTypeCategoryId', width: 180, url: 'businessContactCategoryListFind.json', displayField: 'name', linkedFilter: 'contactType.contactCategory.name'}
		];
	},
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			this.setFilterValue('active', true);
		}
	}
});
Ext.reg('business-companyContactGrid', Clifton.business.CompanyContactGrid);

Clifton.business.contact.assignment.ContactAssignmentGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'businessContactAssignmentListFind',
	xtype: 'gridpanel',
	instructions: 'The following Contact Assignments exist in the system.',
	entityTableNameFilter: undefined,
	importTableName: 'BusinessContactAssignment',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', type: 'int', hidden: true},
		{header: 'Contact Name', width: 80, dataIndex: 'contact.labelShort', filter: {searchFieldName: 'searchPattern'}},
		{header: 'FkFieldID', width: 50, dataIndex: 'fkFieldId', type: 'int', filter: {searchFieldName: 'fkFieldId'}, doNotFormat: true, useNull: true, hidden: true},
		{header: 'Linked Entity', width: 125, dataIndex: 'assignmentEntityLabel'},
		{header: 'Assigned Role', width: 50, dataIndex: 'contactAssignmentRole.name', filter: {searchFieldName: 'contactAssignmentRoleId', type: 'combo', url: 'businessContactAssignmentRoleListFind.json'}},
		{
			header: 'Assignment Category', width: 75, dataIndex: 'contactAssignmentCategory.name', hidden: true,
			filter: {
				searchFieldName: 'contactAssignmentCategoryId', type: 'combo', url: 'businessContactAssignmentCategoryListFind.json',
				beforequery: function(queryEvent) {
					queryEvent.combo.store.setBaseParam('entitySystemTableName', this.gridPanel.entityTableNameFilter);
				}
			}
		},

		// contact properties
		{header: 'Contact ID', width: 15, dataIndex: 'contact.id', filter: {searchFieldName: 'contactEmailAddress'}, hidden: true},
		{header: 'Main Company', width: 75, dataIndex: 'contact.company.name', filter: {searchFieldName: 'contactCompanyName'}, hidden: true},
		{header: 'First Name', width: 75, dataIndex: 'contact.firstName', filter: {searchFieldName: 'contactFirstName'}, hidden: true},
		{header: 'Middle Name', width: 50, dataIndex: 'contact.middleName', filter: {searchFieldName: 'contactMiddleName'}, hidden: true},
		{header: 'Last Name', width: 75, dataIndex: 'contact.lastName', filter: {searchFieldName: 'contactLastName'}, hidden: true},
		{header: 'Name Prefix', width: 50, dataIndex: 'contact.namePrefix', filter: {searchFieldName: 'contactNamePrefix'}, hidden: true},
		{header: 'Name Suffix', width: 50, dataIndex: 'contact.nameSuffix', filter: {searchFieldName: 'contactNameSuffix'}, hidden: true},
		{header: 'Saluation', width: 50, dataIndex: 'contact.saluation', filter: {searchFieldName: 'contactSalutation'}, hidden: true},
		{header: 'Job Title', width: 60, dataIndex: 'contact.title', filter: {searchFieldName: 'contactTitle'}},
		{header: 'Phone', width: 50, dataIndex: 'contact.phoneNumber', filter: {searchFieldName: 'contactPhoneNumber'}},
		{header: 'Mobile', width: 50, dataIndex: 'contact.mobileNumber', filter: {searchFieldName: 'contactMobileNumber'}, hidden: true},
		{header: 'Fax', width: 50, dataIndex: 'contact.faxNumber', filter: {searchFieldName: 'contactFaxNumber'}, hidden: true},
		{header: 'E-Fax', width: 50, dataIndex: 'contact.efaxNumber', filter: {searchFieldName: 'contactEFaxNumber'}, hidden: true},
		{header: 'Email', width: 75, dataIndex: 'contact.emailAddress', filter: {searchFieldName: 'contactEmailAddress'}, renderer: TCG.renderEmailAddress},
		{header: 'Secondary Email', width: 75, dataIndex: 'contact.emailAddressSecondary', filter: {searchFieldName: 'contactEmailAddressSecondary'}, renderer: TCG.renderEmailAddress, hidden: true},
		{header: 'City/State', width: 75, dataIndex: 'contact.cityState', filter: {searchFieldName: 'contactCityState'}, hidden: true},
		{header: 'Address 1', width: 75, dataIndex: 'contact.addressLine1', filter: {searchFieldName: 'contactAddressLine1'}, hidden: true},
		{header: 'Address 2', width: 75, dataIndex: 'contact.addressLine2', filter: {searchFieldName: 'contactAddressLine2'}, hidden: true},
		{header: 'Address 3', width: 75, dataIndex: 'contact.addressLine3', filter: {searchFieldName: 'contactAddressLine3'}, hidden: true},
		{header: 'City', width: 75, dataIndex: 'contact.city', filter: {searchFieldName: 'contactCity'}, hidden: true},
		{header: 'State', width: 50, dataIndex: 'contact.state', filter: {searchFieldName: 'contactState'}, hidden: true},
		{header: 'Postal Code', width: 50, dataIndex: 'contact.postalCode', filter: {searchFieldName: 'contactPostalCode'}, hidden: true},
		{header: 'Country', width: 75, dataIndex: 'contact.country', filter: {searchFieldName: 'contactCountry'}, hidden: true},

		// assignment start/end properties
		{header: 'Active', width: 25, dataIndex: 'active', type: 'boolean'},
		{header: 'Start Date', width: 75, dataIndex: 'startDate', hidden: true},
		{header: 'End Date', width: 75, dataIndex: 'endDate', hidden: true}
	],
	listeners: {
		afterRender: function() {
			this.grid.store.on('load', () => {
				const contactColumn = {
					width: 25, dataIndex: 'contact.id', renderer: function(url, args) {
						return TCG.renderActionColumn('group', '', 'Open Related Contact', 'Contact');
					}
				};
				this.grid.addColumn(contactColumn);
			});
		}
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					const gridPanel = grid.ownerGridPanel;

					if (TCG.getActionColumnEventName(evt) === 'Contact') {
						const id = row.json.contact.id;
						gridPanel.editor.openDetailPage('Clifton.business.contact.ContactWindow', gridPanel, id, row);
					}
				}
			}
		}
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Contact Name', width: 150, xtype: 'toolbar-textfield', name: 'contactName'},
			{
				fieldLabel: 'Assignment Category', xtype: 'toolbar-combo', name: 'contactAssignmentCategoryId', width: 180, url: 'businessContactAssignmentCategoryListFind.json', linkedFilter: 'contactAssignmentCategory.name', limitRequestedProperties: false,
				beforequery: function(queryEvent) {
					queryEvent.combo.store.setBaseParam('entitySystemTableName', TCG.getParentByClass(this, TCG.grid.GridPanel).entityTableNameFilter);
				}
			}
		];
	},
	getLoadParams: function(firstLoad) {
		const params = {};
		if (firstLoad) {
			this.setFilterValue('active', true);
			if (this.getWindow().getMainFormId) {
				this.setFilterValue('fkFieldId', {eq: this.getWindow().getMainFormId()});
			}
		}
		params.entityTableName = this.entityTableNameFilter;
		params.searchPattern = TCG.getChildByName(this.getTopToolbar(), 'contactName').getValue();
		return params;
	},
	editor: {
		detailPageClass: 'Clifton.business.contact.assignment.ContactAssignmentWindow',
		deleteURL: 'businessContactAssignmentDelete.json',
		getDefaultData: function(gridPanel) {
			const defaultData = {};
			const toolbar = gridPanel.getTopToolbar();
			const assignmentCategoryFilter = TCG.getChildByName(toolbar, 'contactAssignmentCategoryId');
			if (TCG.isNotBlank(assignmentCategoryFilter.getValue())) {
				let assignmentCategoryObject = assignmentCategoryFilter.store.getById(assignmentCategoryFilter.getValue());
				if (!TCG.isBlank(assignmentCategoryObject)) {
					assignmentCategoryObject = assignmentCategoryObject.json;
				}
				defaultData.contactAssignmentCategory = assignmentCategoryObject;
			}
			if (this.getWindow().getMainForm) {
				const mainFormValues = this.getWindow().getMainForm().formValues;
				if (!TCG.isBlank(mainFormValues)) {
					defaultData.fkFieldId = mainFormValues.id;
					defaultData.assignmentEntityLabel = mainFormValues.label;
				}
			}
			defaultData.startDate = new Date().format('Y-m-d 00:00:00');
			return defaultData;
		}
	}
});
Ext.reg('business-contact-assignments-grid', Clifton.business.contact.assignment.ContactAssignmentGrid);


if (Clifton.business.BusinessSetupWindowAdditionalTabs) {
	Clifton.business.BusinessSetupWindowAdditionalTabs.push({
		addAdditionalTabs: function(w) {
			const tabs = w.items.get(0);
			let pos = this.getTabPosition(w, 'Companies');
			tabs.insert(pos + 1, this.contactsTab);
			tabs.insert(pos + 2, this.companyContactsTab);
			tabs.insert(pos + 3, this.companyContactMappingsTab);
			tabs.insert(pos + 4, this.contactRelationshipsTab);
			pos = this.getTabPosition(w, 'Company Types');
			tabs.insert(pos + 1, this.contactTypesTab);
			tabs.insert(pos + 2, this.contactCategoriesTab);

		},
		getTabPosition: function(w, tabTitle) {
			const tabs = w.items.get(0);
			for (let i = 0; i < tabs.items.length; i++) {
				if (tabs.items.get(i).title === tabTitle) {
					return i;
				}
			}
			return 0;
		},

		contactsTab: {
			title: 'Contacts',
			items: [{
				name: 'businessContactListFind',
				xtype: 'gridpanel',
				instructions: 'The following business contacts exist in the system.  Each contact can be tied to a company or another contact.  For the contact filter, use "," between text to search against first and/or last name (Example john,doe to search for John Doe)',
				reloadOnRender: false,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Category', width: 65, dataIndex: 'contactCategory.name', filter: {searchFieldName: 'contactCategoryId', type: 'combo', url: 'businessContactCategoryListFind.json'}, hidden: true},
					{header: 'Contact Name', width: 150, dataIndex: 'label', filter: {searchFieldName: 'searchPattern'}},

					{header: 'Main Company', width: 100, dataIndex: 'company.name', filter: {searchFieldName: 'companyName'}, hidden: true},
					{header: 'First Name', width: 80, dataIndex: 'firstName', hidden: true},
					{header: 'Middle Name', width: 50, dataIndex: 'middleName', hidden: true},
					{header: 'Last Name', width: 80, dataIndex: 'lastName', hidden: true},
					{header: 'Name Prefix', width: 50, dataIndex: 'namePrefix', hidden: true},
					{header: 'Name Suffix', width: 50, dataIndex: 'nameSuffix', hidden: true},
					{header: 'Saluation', width: 50, dataIndex: 'saluation', hidden: true},

					{header: 'Title', width: 100, dataIndex: 'title'},
					{header: 'Phone', width: 50, dataIndex: 'phoneNumber'},
					{header: 'Mobile', width: 50, dataIndex: 'mobileNumber', hidden: true},
					{header: 'Fax', width: 50, dataIndex: 'faxNumber', hidden: true},
					{header: 'E-Fax', width: 50, dataIndex: 'efaxNumber', hidden: true},

					{header: 'Email', width: 100, dataIndex: 'emailAddress', renderer: TCG.renderEmailAddress},
					{header: 'Secondary Email', width: 100, dataIndex: 'emailAddressSecondary', renderer: TCG.renderEmailAddress, hidden: true},
					{header: 'City/State', width: 75, dataIndex: 'cityState'},
					{header: 'Address 1', width: 75, dataIndex: 'addressLine1', hidden: true},
					{header: 'Address 2', width: 75, dataIndex: 'addressLine2', hidden: true},
					{header: 'Address 3', width: 75, dataIndex: 'addressLine3', hidden: true},
					{header: 'City', width: 75, dataIndex: 'city', hidden: true},
					{header: 'State', width: 50, dataIndex: 'state', hidden: true},
					{header: 'Postal Code', width: 50, dataIndex: 'postalCode', hidden: true},
					{header: 'Country', width: 75, dataIndex: 'country', hidden: true},

					{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
					{header: 'Start Date', width: 75, dataIndex: 'startDate', hidden: true},
					{header: 'End Date', width: 75, dataIndex: 'endDate', hidden: true}

				],
				getTopToolbarFilters: function(toolbar) {
					return [
						{fieldLabel: 'Contact Name', width: 150, xtype: 'toolbar-textfield', name: 'companyName'},
						{fieldLabel: 'Category', xtype: 'toolbar-combo', name: 'contactCategoryId', width: 120, url: 'businessContactCategoryListFind.json', linkedFilter: 'contactCategory.name'}
					];
				},
				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						this.setFilterValue('active', true);
					}
					const companyName = TCG.getChildByName(this.getTopToolbar(), 'companyName');
					if (TCG.isNotEquals(companyName.getValue(), '')) {
						return {'searchPattern': companyName.getValue()};
					}
				},
				editor: {
					detailPageClass: 'Clifton.business.contact.ContactWindow',
					deleteURL: 'businessContactDelete.json'
				},

				addToolbarButtons: function(t, gridPanel) {
					t.add({
						text: 'Reload Company Field',
						tooltip: 'Reload main company selected on the contact.',
						iconCls: 'run',
						handler: function() {
							const loader = new TCG.data.JsonLoader({
								waitTarget: gridPanel,
								onLoad: function(record, conf) {
									gridPanel.reload();
								}
							});
							loader.load('businessContactCompanyFieldUpdate.json');
						}
					});
					t.add('-');

					t.add({
						text: 'Move/Copy Relationships',
						tooltip: 'Move/Copy Contact Relationships from one Contact to another.',
						iconCls: 'run',
						handler: function() {
							TCG.createComponent('Clifton.business.contact.ContactCopyRelationshipsWindow', {
								openerCt: gridPanel
							});
						}
					});
					t.add('-');
				}
			}]
		},

		companyContactsTab: {
			title: 'Company Contacts',
			items: [{
				xtype: 'business-companyContactGrid'
			}]
		},

		companyContactMappingsTab: {
			title: 'Company Contact Mappings',
			items: [{
				xtype: 'gridpanel',
				name: 'businessCompanyContactMappingListFind',
				requestIdDataIndex: true,
				instructions: 'A list of all allowed mappings between Contact Types and Company Types. Company Contacts can be created only if there is a mapping here that allows an association between the given Contact Type and Company Type (optional).',
				columns: [
					{header: 'ID', dataIndex: 'id', width: 30, hidden: true},
					{header: 'Contact Type', width: 175, dataIndex: 'businessContactType.name', idDataIndex: 'businessContactType.id', filter: {type: 'combo', searchFieldName: 'businessContactTypeId', url: 'businessContactTypeListFind.json'}},
					{header: 'Company Type', width: 175, dataIndex: 'businessCompanyType.name', idDataIndex: 'businessCompanyType.id', filter: {type: 'combo', searchFieldName: 'businessCompanyTypeId', displayField: 'name', url: 'businessCompanyTypeListFind.json'}},
					{header: 'Company Contact Modify Condition', width: 200, dataIndex: 'companyContactModifyCondition.name', idDataIndex: 'companyContactModifyCondition.id', renderer: TCG.renderValueWithTooltip, filter: {type: 'combo', searchFieldName: 'companyContactModifyConditionId', url: 'systemConditionListFind.json'}},
					{header: 'Required', dataIndex: 'required', width: 100, type: 'boolean'},
					{header: 'One Allowed', dataIndex: 'oneAllowed', width: 100, type: 'boolean'},
					{header: 'One Primary Required', dataIndex: 'onePrimaryRequired', width: 100, type: 'boolean'},
					{header: 'Not Preferred', dataIndex: 'notPreferred', width: 100, type: 'boolean'}
				],
				editor: {
					detailPageClass: 'Clifton.business.company.CompanyContactMappingWindow'
				}
			}]
		},

		contactRelationshipsTab: {
			title: 'Contact Relationships',
			items: [{
				name: 'businessContactRelationshipListFind',
				xtype: 'gridpanel',
				instructions: 'The following contact - contact relationships have been defined.  Select a contact in the toolbar to search for their relationships in either direction.',
				reloadOnRender: false,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'First Contact', width: 100, dataIndex: 'referenceOne.label', filter: {type: 'combo', url: 'businessContactListFind.json', searchFieldName: 'referenceOneId'}},
					{header: 'Second Contact', width: 100, dataIndex: 'referenceTwo.label', filter: {type: 'combo', url: 'businessContactListFind.json', searchFieldName: 'referenceTwoId'}},
					{header: 'Relationship', width: 100, dataIndex: 'name'},
					{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean', sortable: false}
				],
				getTopToolbarFilters: function(toolbar) {
					return [
						{
							fieldLabel: 'Contact', xtype: 'combo', name: 'contactId', width: 180, url: 'businessContactListFind.json', displayField: 'label',
							listeners: {
								select: function(field) {
									TCG.getParentByClass(field, Ext.Panel).reload();
								}
							}
						}
					];
				},
				getLoadParams: function(firstLoad) {
					const params = {};
					const t = this.getTopToolbar();
					const cn = TCG.getChildByName(t, 'contactId');
					if (TCG.isNotEquals(cn.getValue(), '')) {
						params.contactId = cn.getValue();
					}
					return params;
				},
				editor: {
					detailPageClass: 'Clifton.business.contact.ContactRelationshipWindow'
				}
			}]
		},

		contactTypesTab: {
			title: 'Contact Types',
			items: [{
				name: 'businessContactTypeListFind',
				xtype: 'gridpanel',
				groupField: 'contactCategory.name',
				groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Types" : "Type"]}',
				instructions: 'Business Contact Types provide classification of contacts for companies.',
				topToolbarSearchParameter: 'searchPattern',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Category', width: 100, dataIndex: 'contactCategory.name', hidden: true},
					{header: 'Type Name', width: 100, dataIndex: 'name'},
					{header: 'Description', width: 300, dataIndex: 'description'},
					{header: 'System Defined', width: 50, dataIndex: 'systemDefined', type: 'boolean'}
				],
				editor: {
					detailPageClass: 'Clifton.business.contact.ContactTypeWindow'
				}
			}]
		},

		contactCategoriesTab: {
			title: 'Contact Categories',
			items: [{
				name: 'businessContactCategoryListFind',
				xtype: 'gridpanel',
				instructions: 'Business Contact Categories are used to categorize and define modify access to contacts and company-contact relationships. For company-contact relationships, category will first use what is defined on the contact type, and if missing, will then use the category on the contact type.',
				topToolbarSearchParameter: 'searchPattern',
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Category Name', width: 75, dataIndex: 'name'},
					{header: 'Modify Condition', width: 175, dataIndex: 'entityModifyCondition.name'},
					{header: 'Description', width: 300, dataIndex: 'description'}
				],
				editor: {
					detailPageClass: 'Clifton.business.contact.ContactCategoryWindow'
				}
			}]
		}
	});
}

if (Clifton.business.company.BasicCompanyWindowAdditionalTabs) {
	Clifton.business.company.BasicCompanyWindowAdditionalTabs.push({
		addAdditionalTabs: function(w) {
			if (w.defaultData.type.contractAllowed) {
				const tabs = w.items.get(0).items.get(0);
				const pos = this.getTabPosition(w.items.get(0), 'Subsidiaries');
				tabs.insert(pos + 1, this.contactsTab);
			}
		},
		getTabPosition: function(w, tabTitle) {
			const tabs = w.items.get(0);
			for (let i = 0; i < tabs.items.length; i++) {
				if (tabs.items.get(i).title === tabTitle) {
					return i;
				}
			}
			return 0;
		},
		contactsTab: {
			title: 'Contacts',
			items: [{
				xtype: 'business-companyContactEntryGrid-byCompany',
				isIncludeRelatedCompanyCheckbox: function() {
					return true;
				},
				getRelatedCompanyCheckboxLabel: function() {
					return 'Include Parent/Subsidiary Contacts';
				}
			}]
		}
	});
}
