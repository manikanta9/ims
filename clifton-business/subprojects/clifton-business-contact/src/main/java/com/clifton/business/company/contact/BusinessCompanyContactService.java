package com.clifton.business.company.contact;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.UserIgnorableValidation;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>BusinessCompanyContactService</code>
 *
 * @author manderson
 */
public interface BusinessCompanyContactService {

	//////////////////////////////////////////////////////////////////////////// 
	/////////           Business Company Contact Methods              ////////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyContact getBusinessCompanyContact(int id);


	public List<BusinessCompanyContact> getBusinessCompanyContactList(BusinessCompanyContactSearchForm searchForm);


	public List<BusinessCompanyContact> getBusinessCompanyContactListByContact(int contactId);


	public List<BusinessCompanyContact> getBusinessCompanyContactListByCompany(int companyId);


	@UserIgnorableValidation
	public BusinessCompanyContact saveBusinessCompanyContact(BusinessCompanyContact bean, boolean ignoreValidation);


	@UserIgnorableValidation
	public void saveBusinessCompanyContactList(List<BusinessCompanyContact> beanList, boolean ignoreValidation);


	public void deleteBusinessCompanyContact(int id);


	public void deleteBusinessCompanyContactList(List<BusinessCompanyContact> deleteList);


	public void deleteBusinessCompanyContactListByCompany(int companyId);


	public void deleteBusinessCompanyContactListByContact(int contactId);


	////////////////////////////////////////////////////////////////////////////
	///          Business Company Contact Mapping Business Methods           ///
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyContactMapping getBusinessCompanyContactMapping(int id);


	/**
	 * Takes a Short object for businessCompanyTypeId because it is optional. For example, Employee contact type is
	 * valid for any company type by default (businessCompanyTypeId = null), but this can be overridden (businessCompanyTypeId != null).
	 */
	public BusinessCompanyContactMapping getBusinessCompanyContactMappingFor(short businessContactTypeId, Short businessCompanyTypeId);


	public List<BusinessCompanyContactMapping> getBusinessCompanyContactMappingList(BusinessCompanyContactMappingSearchForm searchForm);


	public BusinessCompanyContactMapping saveBusinessCompanyContactMapping(BusinessCompanyContactMapping mapping);


	/**
	 * Validates that no Company Contacts will be orphaned before the mapping is deleted.
	 */
	public void deleteBusinessCompanyContactMapping(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////         Business Company Contact Entry Methods          //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the {@link BusinessCompanyContactEntry} object for the given company and contact with the list of existing {@link BusinessCompanyContact} relationships populated
	 */
	@SecureMethod(dtoClass = BusinessCompanyContact.class)
	public BusinessCompanyContactEntry getBusinessCompanyContactEntry(int businessCompanyId, int businessContactId);


	/**
	 * Returns a list of {@link BusinessCompanyContactEntry} objects for the given company.
	 */
	@SecureMethod(dtoClass = BusinessCompanyContact.class)
	List<BusinessCompanyContactEntry> getBusinessCompanyContactEntryList(BusinessCompanyContactSearchForm searchForm);


	/**
	 * For the given {@link BusinessCompanyContactEntry} object - inserts/updates/deletes the list of {@link BusinessCompanyContact} objects for the company and contact
	 */
	@SecureMethod(dtoClass = BusinessCompanyContact.class)
	@UserIgnorableValidation
	public BusinessCompanyContactEntry saveBusinessCompanyContactEntry(BusinessCompanyContactEntry bean, boolean ignoreValidation);


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Contact Methods                     ////////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Provides the ability to move contacts relationships from one contact to another (essentially merging the contacts with the option to delete the original contact)
	 * Or just copying contact relationships from one contact to another.
	 * <p/>
	 * Method here first moves CompanyContact entities, then calls contact service to move/delete everything else
	 *
	 * @param fromContactId
	 * @param toContactId
	 * @param moveRelationships (if false just copies them)
	 * @param deleteFrom
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = BusinessContact.class)
	@UserIgnorableValidation
	public String copyBusinessContactRelationships(int fromContactId, int toContactId, boolean moveRelationships, boolean deleteFrom, boolean ignoreValidation);


	/**
	 * Updates all contacts so that the company selected on the contact = the active Employee relationship
	 * for that contact to another company (if there is only one)
	 */
	@SecureMethod(dtoClass = BusinessContact.class)
	public void updateBusinessContactCompanyField();


	/**
	 * Updates the company on the Contact to reflect the active Employee relationship
	 * for a specific contact
	 * <p/>
	 * If more the one active - there is no main company selected
	 *
	 * @param contact
	 */
	public void updateBusinessContactCompanyFieldForContact(BusinessContact contact);


	//////////////////////////////////////////////////////////////////////////// 
	/////////            Business Company Methods                     ////////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Provides the ability to move contacts relationships from one contact to another (essentially merging the contacts with the option to delete the original contact)
	 * Or just copying contact relationships from one contact to another.
	 * <p/>
	 * Method here first moves CompanyContact entities, then calls company service to move/delete everything else
	 *
	 * @param fromCompanyId
	 * @param toCompanyId
	 * @param moveRelationships (if false just copies them)
	 * @param deleteFrom
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = BusinessCompany.class)
	@UserIgnorableValidation
	public String copyBusinessCompanyRelationships(int fromCompanyId, int toCompanyId, boolean moveRelationships, boolean deleteFrom, boolean ignoreValidation);
}
