package com.clifton.business.company.contact;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyCache;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>BusinessCompanyContactServiceImpl</code> provides a basic implementation of the
 * {@link BusinessCompanyContactService} interface
 * <p/>
 * Used to manage company-contact relationships and also provides company specific methods that need access to their contacts and vice versa.
 *
 * @author manderson
 */
@Service
public class BusinessCompanyContactServiceImpl implements BusinessCompanyContactService {

	private AdvancedUpdatableDAO<BusinessCompanyContact, Criteria> businessCompanyContactDAO;
	private AdvancedUpdatableDAO<BusinessCompanyContactMapping, Criteria> businessCompanyContactMappingDAO;

	private BusinessCompanyService businessCompanyService;
	private BusinessContactService businessContactService;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SecurityAuthorizationService securityAuthorizationService;

	private DataTableRetrievalHandler dataTableRetrievalHandler;

	private DaoCompositeKeyCache<BusinessCompanyContactMapping, Short, Short> businessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache;


	////////////////////////////////////////////////////////////////////////////
	/////////           Business Company Contact Methods              //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompanyContact getBusinessCompanyContact(int id) {
		return getBusinessCompanyContactDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessCompanyContact> getBusinessCompanyContactListByCompany(int companyId) {
		return getBusinessCompanyContactDAO().findByField("referenceOne.id", companyId);
	}


	@Override
	public List<BusinessCompanyContact> getBusinessCompanyContactListByContact(int contactId) {
		return getBusinessCompanyContactDAO().findByField("referenceTwo.id", contactId);
	}


	private List<BusinessCompanyContact> getBusinessCompanyContactListByCompanyAndContact(int companyId, int contactId) {
		return getBusinessCompanyContactDAO().findByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{companyId, contactId});
	}


	@Override
	public List<BusinessCompanyContact> getBusinessCompanyContactList(final BusinessCompanyContactSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!StringUtils.isEmpty(searchForm.getContactName())) {
					String[] searchValues = searchForm.getContactName().split("[ ,]");
					// Example Mary,And  will look for (First Name like Mary or Last Name like Mary) and (First Name like And or last name like And)
					String contactAlias = getPathAlias("referenceTwo", criteria);
					for (String v : searchValues) {
						criteria.add(Restrictions.or(Restrictions.like(contactAlias + ".firstName", v, MatchMode.ANYWHERE), Restrictions.like(contactAlias + ".lastName", v, MatchMode.ANYWHERE)));
					}
				}

				if (!StringUtils.isEmpty(searchForm.getSearchPattern())) {
					String[] searchValues = searchForm.getSearchPattern().split(",");
					// Example Mary,And  will look for (First Name like Mary or Last Name like Mary) and (First Name like And or last name like And)
					String contactAlias = getPathAlias("referenceTwo", criteria);
					String companyAlias = getPathAlias("referenceOne", criteria);
					for (String v : searchValues) {
						criteria.add(Restrictions.disjunction()
								.add(Restrictions.like(contactAlias + ".firstName", v, MatchMode.ANYWHERE))
								.add(Restrictions.like(contactAlias + ".lastName", v, MatchMode.ANYWHERE))
								.add(Restrictions.like(companyAlias + ".name", v, MatchMode.ANYWHERE)));
					}
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// Special support for custom searches
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					return false;
				}
				for (OrderByField field : orderByList) {
					String name = getOrderByFieldName(field, criteria);
					if ("contactName".equals(name)) {
						name = getPathAlias("referenceTwo", criteria) + ".lastName";
					}
					else if ("searchPattern".equals(name)) {
						name = getPathAlias("referenceOne", criteria) + ".name";
					}
					else {
						name = getOrderByFieldName(field, criteria);
					}
					boolean asc = OrderByDirections.ASC == field.getDirection();
					criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
				}
				return true;
			}
		};
		return getBusinessCompanyContactDAO().findBySearchCriteria(config);
	}


	@Override
	public BusinessCompanyContact saveBusinessCompanyContact(BusinessCompanyContact bean, boolean ignoreValidation) {
		// do validation here instead of in validator because UserIgnorableValidation must be defined at the service method level
		validateCompanyContact(bean, DaoEventTypes.SAVE, ignoreValidation);
		return getBusinessCompanyContactDAO().save(bean);
	}


	private void validateCompanyContacts(List<BusinessCompanyContact> beans, DaoEventTypes config, boolean ignoreValidation) throws ValidationException {
		Set<String> notPreferredContactTypeSet = new HashSet<>();
		Set<String> preferredCompanyTypeSet = new HashSet<>();
		CollectionUtils.getIterable(beans).forEach(companyContact -> {
			try {
				validateCompanyContact(companyContact, config, ignoreValidation);
			}
			catch (UserIgnorableValidationException e) {
				notPreferredContactTypeSet.add(companyContact.getContactType().getName());
				String companyTypeNameString = e.getMessage().substring(e.getMessage().lastIndexOf("[") + 1, e.getMessage().lastIndexOf("]"));
				preferredCompanyTypeSet.addAll(companyTypeNameString == null ? Collections.emptyList() : Arrays.asList(companyTypeNameString.trim().split("\\s*,\\s*")));
			}
		});
		if (!CollectionUtils.isEmpty(notPreferredContactTypeSet)) {
			String isOrAreNotPreferred = notPreferredContactTypeSet.size() == 1 ? "is" : "are";
			if (CollectionUtils.isEmpty(preferredCompanyTypeSet)) {
				throw new UserIgnorableValidationException("Contact Type(s): [" + StringUtils.join(notPreferredContactTypeSet, ", ")
						+ "] " + isOrAreNotPreferred + " NOT preferred for Company Type: [" + CollectionUtils.getFirstElementStrict(beans).getReferenceOne().getType().getName() + "]");
			}
			else {
				throw new UserIgnorableValidationException("Contact Type(s): [" + StringUtils.join(notPreferredContactTypeSet, ", ") + "] is NOT preferred for Company Type: [" + CollectionUtils.getFirstElementStrict(beans).getReferenceOne().getType().getName()
						+ "]. The following Company Type(s) are preferred: [" + StringUtils.join(preferredCompanyTypeSet, ", ") + "]");
			}
		}
	}


	private void validateCompanyContact(BusinessCompanyContact bean, DaoEventTypes config, boolean ignoreValidation) throws ValidationException {
		// Retrieve the Mapping that applies to this relationship and validate Entity Modify Condition
		BusinessCompanyContactMapping mapping = validateBusinessCompanyContactMapping(bean, config);

		BusinessCompanyContactSearchForm searchForm = new BusinessCompanyContactSearchForm();
		searchForm.setContactTypeId(bean.getContactType().getId());
		searchForm.setCompanyTypeId(bean.getReferenceOne().getType().getId());
		List<BusinessCompanyContact> companyContactList = getBusinessCompanyContactList(searchForm);
		if (mapping.isOneAllowed()) {
			List<BusinessCompanyContact> companyContactListForCompany = companyContactList.stream()
					.filter(companyContact -> companyContact.getReferenceOne().getId().equals(bean.getReferenceOne().getId()))
					.collect(Collectors.toList());
			ValidationUtils.assertEmpty(companyContactListForCompany, "Only one Company Contact with Contact Type: ["
					+ bean.getContactType().getName() + "] can be assigned to a company with Company Type: [" + bean.getReferenceOne().getType().getName() + "]");
		}
		// commenting this out for now because this prevents the user from changing which contact is the primary
		// if (mapping.isOnePrimaryRequired()) {
		// 	List<BusinessCompanyContact> companyContactListForCompany = companyContactList.stream()
		// 			.filter(companyContact -> companyContact.getReferenceOne().getId().equals(bean.getReferenceOne().getId()))
		// 			.collect(Collectors.toList());
		// 	List<BusinessCompanyContact> primaryContactListForCompany = companyContactListForCompany.stream()
		// 			.filter(companyContact -> companyContact.isPrimary())
		// 			.collect(Collectors.toList());
		//
		// 	ValidationUtils.assertTrue(companyContactListForCompany.isEmpty() || ((bean.isPrimary() && primaryContactListForCompany.isEmpty()) || (!bean.isPrimary() && primaryContactListForCompany.size() == 1)),
		// 			"When multiple company contacts with Contact Type: [" + bean.getContactType().getName() + "] exist for a company with Company Type: [" + bean.getReferenceOne().getType().getName() + "] exactly one of them must be the primary.");
		// }
		if (mapping.isNotPreferred() && !ignoreValidation) {
			BusinessCompanyContactMappingSearchForm mappingSearchForm = new BusinessCompanyContactMappingSearchForm();
			mappingSearchForm.setBusinessContactTypeId(bean.getContactType().getId());
			mappingSearchForm.setNotPreferred(false);
			List<String> preferredCompanyTypes = getBusinessCompanyContactMappingList(mappingSearchForm).stream()
					.map(companyContactMapping -> companyContactMapping.getBusinessCompanyType() != null ? companyContactMapping.getBusinessCompanyType().getName() : "ALL")
					.collect(Collectors.toList());
			if (CollectionUtils.isEmpty(preferredCompanyTypes)) {
				throw new UserIgnorableValidationException("Contact Type: [" + bean.getContactType().getName() + "] is NOT preferred for Company Type: [" + bean.getReferenceOne().getType().getName() + "]");
			}
			else {
				throw new UserIgnorableValidationException("Contact Type: [" + bean.getContactType().getName() + "] is NOT preferred for Company Type: [" + bean.getReferenceOne().getType().getName() + "]. The following Company Type(s) are preferred: ["
						+ StringUtils.join(preferredCompanyTypes, ", ") + "]");
			}
		}
	}


	@Override
	public void saveBusinessCompanyContactList(List<BusinessCompanyContact> beanList, boolean ignoreValidation) {
		validateCompanyContacts(beanList, DaoEventTypes.SAVE, ignoreValidation);
		getBusinessCompanyContactDAO().saveList(beanList);
	}


	@Override
	public void deleteBusinessCompanyContactList(List<BusinessCompanyContact> deleteList) {
		getBusinessCompanyContactDAO().deleteList(deleteList);
	}


	@Override
	public void deleteBusinessCompanyContact(int id) {
		getBusinessCompanyContactDAO().delete(id);
	}


	@Override
	public void deleteBusinessCompanyContactListByCompany(int companyId) {
		getBusinessCompanyContactDAO().deleteList(getBusinessCompanyContactDAO().findByField("referenceOne.id", companyId));
	}


	@Override
	public void deleteBusinessCompanyContactListByContact(int contactId) {
		getBusinessCompanyContactDAO().deleteList(getBusinessCompanyContactListByContact(contactId));
	}


	////////////////////////////////////////////////////////////////////////////
	///          Business Company Contact Mapping Business Methods           ///
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessCompanyContactMapping getBusinessCompanyContactMapping(int id) {
		return getBusinessCompanyContactMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessCompanyContactMapping getBusinessCompanyContactMappingFor(short businessContactTypeId, Short businessCompanyTypeId) {
		BusinessCompanyContactMapping mapping = getBusinessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache().getBeanForKeyValues(getBusinessCompanyContactMappingDAO(), businessContactTypeId, businessCompanyTypeId);
		if (mapping == null && businessCompanyTypeId != null) {
			mapping = getBusinessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache().getBeanForKeyValues(getBusinessCompanyContactMappingDAO(), businessContactTypeId, null);
		}
		return mapping;
	}


	@Override
	public List<BusinessCompanyContactMapping> getBusinessCompanyContactMappingList(BusinessCompanyContactMappingSearchForm searchForm) {
		return getBusinessCompanyContactMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessCompanyContactMapping saveBusinessCompanyContactMapping(BusinessCompanyContactMapping mapping) {
		return getBusinessCompanyContactMappingDAO().save(mapping);
	}


	@Override
	public void deleteBusinessCompanyContactMapping(int id) {
		// the following validation ensure that existing Company Contacts won't be orphaned if this mapping is deleted
		BusinessCompanyContactMapping mapping = getBusinessCompanyContactMapping(id);
		BusinessCompanyContactSearchForm companyContactSearchForm = new BusinessCompanyContactSearchForm();
		companyContactSearchForm.setContactTypeId(mapping.getBusinessContactType().getId());
		if (mapping.getBusinessCompanyType() != null) {
			if (getBusinessCompanyContactMappingFor(mapping.getBusinessContactType().getId(), null) == null) {
				// there is no BusinessCompanyContactMapping[ContactType, null] so make sure there are no Company Contacts that will be orphaned on deletion
				companyContactSearchForm.setCompanyTypeId(mapping.getBusinessCompanyType().getId());
				ValidationUtils.assertEmpty(getBusinessCompanyContactList(companyContactSearchForm),
						"Mapping with Contact Type: [" + mapping.getBusinessContactType().getName() + "] and Company Type: [" + mapping.getBusinessCompanyType().getName()
								+ "] cannot be deleted because existing Company Contacts that are dependent on this mapping.");
			}
			// no else clause - ok to delete this mapping if there is a BusinessCompanyContactMapping[ContactType, null] that will adopt existing Company Contacts
		}
		else {
			// get all company contacts with the to-be-deleted mapping's ContactType
			List<BusinessCompanyContact> companyContactList = getBusinessCompanyContactList(companyContactSearchForm);
			// get the other mappings with the same ContactType as the to-be-deleted mapping
			BusinessCompanyContactMappingSearchForm mappingSearchForm = new BusinessCompanyContactMappingSearchForm();
			mappingSearchForm.setBusinessContactTypeId(mapping.getBusinessContactType().getId());
			List<Short> companyTypeIdList = getBusinessCompanyContactMappingList(mappingSearchForm).stream()
					.filter(companyContactMapping -> !companyContactMapping.getId().equals(mapping.getId()))
					.map(companyContactMapping -> companyContactMapping.getBusinessCompanyType().getId())
					.collect(Collectors.toList());
			// find the company contacts that will be orphaned if the to-be-deleted mapping is deleted
			ValidationUtils.assertEmpty(CollectionUtils.getStream(companyContactList)
							.filter(companyContact -> !companyTypeIdList.contains(companyContact.getReferenceOne().getType().getId()))
							.collect(Collectors.toList()),
					"Mapping with Contact Type: [" + mapping.getBusinessContactType().getName() + "] and Company Type: [ALL] cannot " +
							"be deleted because existing Company Contacts are dependent on this mapping.");
		}
		getBusinessCompanyContactMappingDAO().delete(id);
	}


	private BusinessCompanyContactMapping validateBusinessCompanyContactMapping(BusinessCompanyContact bean, DaoEventTypes eventType) {
		BusinessCompanyContactMapping mapping = getBusinessCompanyContactMappingFor(bean.getContactType().getId(),
				bean.getReferenceOne().getType().getId());
		ValidationUtils.assertNotNull(mapping, "No Company Contact Mapping exists for Contact Type: [" + bean.getContactType().getName() + "]");

		if (!getSecurityAuthorizationService().isSecurityUserAdmin()) {
			if (mapping.getCompanyContactModifyCondition() != null) {
				String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(mapping.getCompanyContactModifyCondition(), bean);
				if (!StringUtils.isEmpty(error)) {
					throw new ValidationException(eventType.toString() + " Not Allowed. You do not have permission to modify this Company Contact because: " + error);
				}
			}
		}

		return mapping;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////         Business Company Contact Entry Methods          //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BusinessCompanyContactEntry> getBusinessCompanyContactEntryList(BusinessCompanyContactSearchForm searchForm) {
		int start = searchForm.getStart();
		int pageSize = searchForm.getLimit();

		List<BusinessCompanyContact> companyContacts = getBusinessCompanyContactList(searchForm);
		Map<Integer, Map<Integer, BusinessCompanyContactEntry>> contactEntryMap = new HashMap<>();
		for (BusinessCompanyContact companyContact : companyContacts) {
			Map<Integer, BusinessCompanyContactEntry> contactEntryMapForCompany = contactEntryMap.computeIfAbsent(companyContact.getReferenceOne().getId(), HashMap::new);
			BusinessCompanyContactEntry companyContactEntry = contactEntryMapForCompany.computeIfAbsent(companyContact.getReferenceTwo().getId(), key -> {
				BusinessCompany businessCompany = companyContact.getReferenceOne();
				BusinessContact businessContact = companyContact.getReferenceTwo();
				return new BusinessCompanyContactEntry(businessCompany, businessContact);
			});
			companyContactEntry.addBusinessCompanyContact(companyContact);
		}

		// Create a list of the results
		List<BusinessCompanyContactEntry> companyContactEntries = new ArrayList<>();
		for (Map<Integer, BusinessCompanyContactEntry> entryMap : contactEntryMap.values()) {
			companyContactEntries.addAll(entryMap.values());
		}

		// Need to return PagingArrayList with proper start/size/page size so filtering in UI works properly
		// If more than one page of data
		int size = companyContactEntries.size();
		if (size > pageSize) {
			int maxIndex = start + pageSize;
			if (maxIndex > size) {
				maxIndex = size;
			}
			// Filter actual rows returned to the current page
			companyContactEntries = companyContactEntries.subList(start, maxIndex);
		}
		// Return Paging Array List with (if more than one page, then list is already a subset of data, else all the data), start index and the total size of the original list without paging
		return new PagingArrayList<>(companyContactEntries, start, size);
	}


	@Override
	public BusinessCompanyContactEntry getBusinessCompanyContactEntry(int businessCompanyId, int businessContactId) {
		BusinessCompany businessCompany = getBusinessCompanyService().getBusinessCompany(businessCompanyId);
		ValidationUtils.assertNotNull(businessCompany, "Missing business company with id " + businessCompanyId);
		BusinessContact businessContact = getBusinessContactService().getBusinessContact(businessContactId);
		ValidationUtils.assertNotNull(businessContact, "Missing business contact with id " + businessContactId);
		BusinessCompanyContactEntry companyContactEntry = new BusinessCompanyContactEntry(businessCompany, businessContact);
		companyContactEntry.setCompanyContactList(getBusinessCompanyContactListByCompanyAndContact(businessCompanyId, businessContactId));
		return companyContactEntry;
	}


	@Override
	public BusinessCompanyContactEntry saveBusinessCompanyContactEntry(BusinessCompanyContactEntry bean, boolean ignoreValidation) {
		ValidationUtils.assertNotNull(bean.getReferenceOne(), "Business Company selection is required.");
		ValidationUtils.assertNotNull(bean.getReferenceTwo(), "Business Contact selection is required.");

		List<BusinessCompanyContact> originalList = getBusinessCompanyContactListByCompanyAndContact(bean.getReferenceOne().getId(), bean.getReferenceTwo().getId());
		for (BusinessCompanyContact companyContact : CollectionUtils.getIterable(bean.getCompanyContactList())) {
			companyContact.setReferenceOne(bean.getReferenceOne());
			companyContact.setReferenceTwo(bean.getReferenceTwo());
		}
		validateCompanyContacts(bean.getCompanyContactList(), DaoEventTypes.SAVE, ignoreValidation);
		getBusinessCompanyContactDAO().saveList(bean.getCompanyContactList(), originalList);
		return getBusinessCompanyContactEntry(bean.getReferenceOne().getId(), bean.getReferenceTwo().getId());
	}


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Contact Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public String copyBusinessContactRelationships(int fromContactId, int toContactId, boolean moveRelationships, boolean deleteFrom, boolean ignoreValidation) {
		BusinessContact from = getBusinessContactService().getBusinessContact(fromContactId);
		BusinessContact to = getBusinessContactService().getBusinessContact(toContactId);
		ValidationUtils.assertNotNull(from, "From Contact with id [" + fromContactId + "] is missing from the database.");
		ValidationUtils.assertNotNull(to, "To Contact with id [" + toContactId + "] is missing from the database.");

		if (from.equals(to)) {
			throw new ValidationException("Cannot select the same from and to contacts to merge.");
		}

		StringBuilder result = new StringBuilder(20);

		// Copy BusinessCompanyContact(s)
		copyBusinessCompanyContactsByContact(from, to, moveRelationships, result, ignoreValidation);

		// Copy BusinessContactRelationship(s)
		getBusinessContactService().copyBusinessContactRelationships(from, to, moveRelationships, result);

		if (deleteFrom) {
			getBusinessContactService().deleteBusinessContact(fromContactId);
			result.append(StringUtils.NEW_LINE + " Contact [").append(from.getNameLabel()).append("] has been removed.");
		}
		return result.toString();
	}


	@Transactional
	protected void copyBusinessCompanyContactsByContact(BusinessContact from, BusinessContact to, boolean moveRelationships, StringBuilder result, boolean ignoreValidation) {
		int count = 0;
		List<BusinessCompanyContact> fromCompanyContactList = getBusinessCompanyContactListByContact(from.getId());
		if (!CollectionUtils.isEmpty(fromCompanyContactList)) {
			List<BusinessCompanyContact> updateList = new ArrayList<>();
			List<BusinessCompanyContact> deleteList = new ArrayList<>();

			List<BusinessCompanyContact> toCompanyContactList = getBusinessCompanyContactListByContact(to.getId());
			for (BusinessCompanyContact fromCC : CollectionUtils.getIterable(fromCompanyContactList)) {
				boolean found = false;
				for (BusinessCompanyContact toCC : CollectionUtils.getIterable(toCompanyContactList)) {
					if (fromCC.getReferenceOne().equals(toCC.getReferenceOne()) && fromCC.getContactType().equals(toCC.getContactType())) {
						found = true;
						break;
					}
				}
				if (!found) {
					if (moveRelationships) {
						fromCC.setReferenceTwo(to);
						updateList.add(fromCC);
					}
					else {
						BusinessCompanyContact copy = BeanUtils.cloneBean(fromCC, false, false);
						copy.setReferenceTwo(to);
						updateList.add(copy);
					}
					count++;
				}
				else if (moveRelationships) {
					// Already there - just delete the old reference
					deleteList.add(fromCC);
				}
			}
			saveBusinessCompanyContactList(updateList, ignoreValidation);
			deleteBusinessCompanyContactList(deleteList);
		}
		result.append(StringUtils.NEW_LINE + "[").append(count).append("] company-contact relationships ").append(moveRelationships ? "moved" : "copied").append(".");
	}


	@Override
	public void updateBusinessContactCompanyField() {
		// Returns a list of ContactIDs and the correct CompanyID where it isn't currently set correctly
		DataTable resetContactCompanyTable = getDataTableRetrievalHandler().findDataTable( //
				"SELECT c.BusinessContactID \"ContactID\", cm_cn.BusinessCompanyID \"CompanyID\" " + //
						"FROM BusinessContact c " + //
						"OUTER APPLY ( " + //
						"SELECT cc.BusinessCompanyID " + //
						"FROM BusinessCompanyContact cc " + //
						"INNER JOIN BusinessContactType ct ON cc.BusinessContactTypeID = ct.BusinessContactTypeID " + //
						"WHERE cc.BusinessContactID = c.BusinessContactID " + //
						"AND ct.ContactTypeName = '" + BusinessContactType.EMPLOYEE_CONTACT_TYPE + "' " + //
						"AND (cc.StartDate IS NULL OR cc.StartDate <= GETDATE()) " + //
						"AND (cc.EndDate IS NULL OR cc.EndDate >= GETDATE()) " + //
						"AND NOT EXISTS ( " + //
						"SELECT cc2.BusinessCompanyID " + //
						"FROM BusinessCompanyContact cc2 " + //
						"WHERE cc.BusinessContactID = cc2.BusinessContactID " + //
						"AND cc.BusinessContactTypeID = cc2.BusinessContactTypeID " + //
						"AND cc.BusinessCompanyID != cc2.BusinessCompanyID " + //
						"AND (cc2.StartDate IS NULL OR cc2.StartDate <= GETDATE()) " + //
						"AND (cc2.EndDate IS NULL OR cc2.EndDate >= GETDATE()) " + //
						") " + //
						") cm_cn " + //
						"WHERE COALESCE(c.BusinessCompanyID,0) != COALESCE(cm_cn.BusinessCompanyID,0) ");

		if (resetContactCompanyTable != null && resetContactCompanyTable.getTotalRowCount() > 0) {
			List<BusinessContact> saveList = new ArrayList<>();
			for (int i = 0; i < resetContactCompanyTable.getTotalRowCount(); i++) {

				Integer contactId = (Integer) resetContactCompanyTable.getRow(i).getValue("ContactID");
				if (contactId != null) {

					BusinessContact contact = getBusinessContactService().getBusinessContact(contactId);
					if (contact != null) {
						Integer companyId = (Integer) resetContactCompanyTable.getRow(i).getValue("CompanyID");
						BusinessCompany company = null;
						if (companyId != null) {
							company = getBusinessCompanyService().getBusinessCompany(companyId);
						}
						contact.setCompany(company);
						saveList.add(contact);
					}
				}
			}
			if (!CollectionUtils.isEmpty(saveList)) {
				getBusinessContactService().saveBusinessContactList(saveList);
			}
		}
	}


	@Override
	public void updateBusinessContactCompanyFieldForContact(BusinessContact contact) {
		BusinessContactType employee = getBusinessContactService().getBusinessContactTypeByName(BusinessContactType.EMPLOYEE_CONTACT_TYPE);

		BusinessCompanyContactSearchForm searchForm = new BusinessCompanyContactSearchForm();
		searchForm.setActive(true);
		searchForm.setContactTypeId(employee.getId());
		searchForm.setContactId(contact.getId());

		List<BusinessCompanyContact> list = getBusinessCompanyContactList(searchForm);
		BusinessCompany mainCompany = null;

		// If Only One - set it
		if (CollectionUtils.getSize(list) == 1) {
			mainCompany = list.get(0).getReferenceOne();
		}

		if (!CompareUtils.isEqual(mainCompany, contact.getCompany())) {
			contact.setCompany(mainCompany);
			getBusinessContactService().saveBusinessContact(contact);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Company Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public String copyBusinessCompanyRelationships(int fromCompanyId, int toCompanyId, boolean moveRelationships, boolean deleteFrom, boolean ignoreValidation) {
		BusinessCompany from = getBusinessCompanyService().getBusinessCompany(fromCompanyId);
		BusinessCompany to = getBusinessCompanyService().getBusinessCompany(toCompanyId);
		ValidationUtils.assertNotNull(from, "From Company with id [" + fromCompanyId + "] is missing from the database.");
		ValidationUtils.assertNotNull(to, "To Company with id [" + toCompanyId + "] is missing from the database.");

		if (from.equals(to)) {
			throw new ValidationException("Cannot select the same from and to companies to merge.");
		}

		if (deleteFrom && from.getType().isSystemDefined()) {
			throw new ValidationException("Cannot delete from company [" + from.getName()
					+ "] because it is a system defined type.  You can still move or copy contact relationships, but please uncheck the delete from company checkbox.");
		}

		StringBuilder result = new StringBuilder(20);

		// Copy BusinessCompanyContact(s)
		copyBusinessCompanyContactsByCompany(from, to, moveRelationships, result, ignoreValidation);

		// Copy BusinessCompanyRelationship(s)
		getBusinessCompanyService().copyBusinessCompanyRelationships(from, to, moveRelationships, result);

		if (deleteFrom) {
			getBusinessCompanyService().deleteBusinessCompany(fromCompanyId);
			result.append(StringUtils.NEW_LINE + " Company [").append(from.getName()).append("] has been removed.");
		}
		return result.toString();
	}


	@Transactional
	protected void copyBusinessCompanyContactsByCompany(BusinessCompany from, BusinessCompany to, boolean moveRelationships, StringBuilder result, boolean ignoreValidation) {
		int count = 0;
		List<BusinessCompanyContact> fromList = getBusinessCompanyContactListByCompany(from.getId());
		if (!CollectionUtils.isEmpty(fromList)) {
			List<BusinessCompanyContact> updateList = new ArrayList<>();
			List<BusinessCompanyContact> deleteList = new ArrayList<>();

			List<BusinessCompanyContact> toList = getBusinessCompanyContactListByCompany(to.getId());
			for (BusinessCompanyContact fromCC : CollectionUtils.getIterable(fromList)) {
				boolean found = false;
				for (BusinessCompanyContact toCC : CollectionUtils.getIterable(toList)) {
					if (fromCC.getReferenceTwo().equals(toCC.getReferenceTwo()) && fromCC.getContactType().equals(toCC.getContactType())) {
						found = true;
						break;
					}
				}
				if (!found) {
					if (moveRelationships) {
						fromCC.setReferenceOne(to);
						updateList.add(fromCC);
					}
					else {
						BusinessCompanyContact copy = BeanUtils.cloneBean(fromCC, false, false);
						copy.setReferenceOne(to);
						updateList.add(copy);
					}
					count++;
				}
				else if (moveRelationships) {
					// Already there - just delete the old reference
					deleteList.add(fromCC);
				}
			}
			saveBusinessCompanyContactList(updateList, ignoreValidation);
			deleteBusinessCompanyContactList(deleteList);
		}
		result.append(StringUtils.NEW_LINE + "[").append(count).append("] company-contact relationships ").append(moveRelationships ? "moved" : "copied").append(".");
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessCompanyContact, Criteria> getBusinessCompanyContactDAO() {
		return this.businessCompanyContactDAO;
	}


	public void setBusinessCompanyContactDAO(AdvancedUpdatableDAO<BusinessCompanyContact, Criteria> businessCompanyContactDAO) {
		this.businessCompanyContactDAO = businessCompanyContactDAO;
	}


	public AdvancedUpdatableDAO<BusinessCompanyContactMapping, Criteria> getBusinessCompanyContactMappingDAO() {
		return this.businessCompanyContactMappingDAO;
	}


	public void setBusinessCompanyContactMappingDAO(AdvancedUpdatableDAO<BusinessCompanyContactMapping, Criteria> businessCompanyContactMappingDAO) {
		this.businessCompanyContactMappingDAO = businessCompanyContactMappingDAO;
	}


	public DaoCompositeKeyCache<BusinessCompanyContactMapping, Short, Short> getBusinessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache() {
		return this.businessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache;
	}


	public void setBusinessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache(DaoCompositeKeyCache<BusinessCompanyContactMapping, Short, Short> businessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache) {
		this.businessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache = businessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}
}
