package com.clifton.business.contact.assignment;


import com.clifton.business.contact.assignment.search.BusinessContactAssignmentCategorySearchForm;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentRoleSearchForm;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentSearchForm;

import java.util.List;


/**
 * The <code>BusinessContactAssignmentService</code> defines methods for working with
 * {@link BusinessContactAssignment} and related objects
 *
 * @author michaelm
 */
public interface BusinessContactAssignmentService {

	////////////////////////////////////////////////////////////////////////////
	///////////       Business Contact Assignment Methods           ////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignment getBusinessContactAssignment(short id);


	public List<BusinessContactAssignment> getBusinessContactAssignmentList(BusinessContactAssignmentSearchForm searchForm);


	public List<BusinessContactAssignment> getBusinessContactAssignmentListByCategoryId(short id);


	public List<BusinessContactAssignment> getBusinessContactAssignmentListByRoleId(short id);


	public BusinessContactAssignment saveBusinessContactAssignment(BusinessContactAssignment assignment);


	public void saveBusinessContactAssignmentList(List<BusinessContactAssignment> assignmentList);


	public void deleteBusinessContactAssignment(short id);


	////////////////////////////////////////////////////////////////////////////
	///////       Business Contact Assignment Category Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentCategory getBusinessContactAssignmentCategory(short id);


	public List<BusinessContactAssignmentCategory> getBusinessContactAssignmentCategoryList(BusinessContactAssignmentCategorySearchForm searchForm);


	public BusinessContactAssignmentCategory saveBusinessContactAssignmentCategory(BusinessContactAssignmentCategory assignmentCategory);


	public void deleteBusinessContactAssignmentCategory(short id);


	////////////////////////////////////////////////////////////////////////////
	////////         Business Contact Assignment Role Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentRole getBusinessContactAssignmentRole(short id);


	public List<BusinessContactAssignmentRole> getBusinessContactAssignmentRoleListByContact(int contactId);


	public List<BusinessContactAssignmentRole> getBusinessContactAssignmentRoleList(BusinessContactAssignmentRoleSearchForm searchForm);


	public BusinessContactAssignmentRole saveBusinessContactAssignmentRole(BusinessContactAssignmentRole assignmentRole);


	public void deleteBusinessContactAssignmentRole(short id);
}
