package com.clifton.business.contact.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>BusinessContactRelationshipSearchForm</code> ...
 *
 * @author manderson
 */
public class BusinessContactRelationshipSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer referenceOneId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer referenceTwoId;

	// Custom Search Field - referenceOne.id = contactId OR referenceTwo.id = contactId
	private Integer contactId;

	@SearchField
	private String name;


	public Integer getReferenceOneId() {
		return this.referenceOneId;
	}


	public void setReferenceOneId(Integer referenceOneId) {
		this.referenceOneId = referenceOneId;
	}


	public Integer getReferenceTwoId() {
		return this.referenceTwoId;
	}


	public void setReferenceTwoId(Integer referenceTwoId) {
		this.referenceTwoId = referenceTwoId;
	}


	public Integer getContactId() {
		return this.contactId;
	}


	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
