package com.clifton.business.company.contact;


import com.clifton.business.company.search.BusinessCompanyAwareSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;

import java.util.List;


/**
 * The <code>BusinessCompanyContactSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class BusinessCompanyContactSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm implements BusinessCompanyAwareSearchForm {

	// Custom Search Field @SearchField(searchField = "referenceTwo.firstName,referenceTwo.lastName,referenceOne.name", sortField = "referenceTwo.lastName")
	private String searchPattern;

	@SearchField(searchField = "referenceTwo.firstName,referenceTwo.lastName,referenceOne.name,contactType.name,functionalRole,referenceTwo.emailAddress")
	private String expandedSearchPattern;

	@SearchField(searchFieldPath = "referenceOne", searchField = "name,companyLegalName", sortField = "name")
	private String companyName;

	@SearchField(searchField = "referenceOne.type.id")
	private Short companyTypeId;

	@SearchField(searchFieldPath = "referenceOne.type", searchField = "name")
	private String companyTypeName;

	@SearchField(searchField = "referenceOne.id", sortField = "referenceOne.name")
	private Integer[] companyIds;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */

	private Integer companyId;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Integer companyIdOrRelatedCompany;

	/**
	 * @see BusinessCompanyAwareSearchForm
	 */
	private Boolean includeClientOrClientRelationship;

	@SearchField(searchField = "referenceTwo.id")
	private Integer contactId;

	@SearchField
	private Integer displayOrder;

	// Custom Search Field
	private String contactName;

	@SearchField(searchFieldPath = "referenceTwo.company", searchField = "name")
	private String contactMainCompanyName;

	@SearchField(searchField = "referenceTwo.company.name,referenceOne.name", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String coalesceContactCompanyName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "title")
	private String contactTitle;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "phoneNumber")
	private String contactPhoneNumber;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "mobileNumber")
	private String contactMobileNumber;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "faxNumber")
	private String contactFaxNumber;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "emailAddress")
	private String contactEmailAddress;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "emailAddressSecondary")
	private String contactEmailAddressSecondary;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "addressLine1,addressLine2,addressLine3", sortField = "addressLine1")
	private String contactAddressLabel;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "city,state", sortField = "city")
	private String contactCityState;

	@SearchField
	private Boolean primary;

	@SearchField
	private Boolean privateRelationship;

	@SearchField
	private String functionalRole;

	@SearchField(searchField = "contactType.id")
	private Short contactTypeId;

	@SearchField(searchField = "contactType.id")
	private Short[] contactTypeIds;

	@SearchField(searchFieldPath = "contactType", searchField = "name")
	private String contactTypeName;

	@SearchField(searchFieldPath = "contactType", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String contactTypeNameEquals;

	@SearchField(searchFieldPath = "contactType", searchField = "contactCategory.id")
	private Short contactTypeCategoryId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String[] getCompanyPropertyNames() {
		return new String[]{"referenceOne"};
	}


	@Override
	public void applyAdditionalCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, List<Integer> companyIds) {
		// DO NOTHING
	}


	@Override
	public void applyAdditionalParentCompanyFilter(Criteria criteria, List<Criterion> orCompanyFilters, Integer parentCompanyId) {
		// DO NOTHING
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getCompanyId() {
		return this.companyId;
	}


	@Override
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public Integer getContactId() {
		return this.contactId;
	}


	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}


	public Short[] getContactTypeIds() {
		return this.contactTypeIds;
	}


	public void setContactTypeIds(Short[] contactTypeIds) {
		this.contactTypeIds = contactTypeIds;
	}


	public Boolean getPrimary() {
		return this.primary;
	}


	public void setPrimary(Boolean primary) {
		this.primary = primary;
	}


	public Short getContactTypeId() {
		return this.contactTypeId;
	}


	public void setContactTypeId(Short contactTypeId) {
		this.contactTypeId = contactTypeId;
	}


	public String getContactTypeName() {
		return this.contactTypeName;
	}


	public void setContactTypeName(String contactTypeName) {
		this.contactTypeName = contactTypeName;
	}


	public Integer[] getCompanyIds() {
		return this.companyIds;
	}


	public void setCompanyIds(Integer[] companyIds) {
		this.companyIds = companyIds;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getFunctionalRole() {
		return this.functionalRole;
	}


	public void setFunctionalRole(String functionalRole) {
		this.functionalRole = functionalRole;
	}


	public String getContactMainCompanyName() {
		return this.contactMainCompanyName;
	}


	public void setContactMainCompanyName(String contactMainCompanyName) {
		this.contactMainCompanyName = contactMainCompanyName;
	}


	public Boolean getPrivateRelationship() {
		return this.privateRelationship;
	}


	public void setPrivateRelationship(Boolean privateRelationship) {
		this.privateRelationship = privateRelationship;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getContactTitle() {
		return this.contactTitle;
	}


	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}


	public String getContactPhoneNumber() {
		return this.contactPhoneNumber;
	}


	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}


	public String getContactMobileNumber() {
		return this.contactMobileNumber;
	}


	public void setContactMobileNumber(String contactMobileNumber) {
		this.contactMobileNumber = contactMobileNumber;
	}


	public String getContactFaxNumber() {
		return this.contactFaxNumber;
	}


	public void setContactFaxNumber(String contactFaxNumber) {
		this.contactFaxNumber = contactFaxNumber;
	}


	public String getContactEmailAddress() {
		return this.contactEmailAddress;
	}


	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}


	public String getContactEmailAddressSecondary() {
		return this.contactEmailAddressSecondary;
	}


	public void setContactEmailAddressSecondary(String contactEmailAddressSecondary) {
		this.contactEmailAddressSecondary = contactEmailAddressSecondary;
	}


	public String getContactAddressLabel() {
		return this.contactAddressLabel;
	}


	public void setContactAddressLabel(String contactAddressLabel) {
		this.contactAddressLabel = contactAddressLabel;
	}


	public String getContactCityState() {
		return this.contactCityState;
	}


	public void setContactCityState(String contactCityState) {
		this.contactCityState = contactCityState;
	}


	@Override
	public Integer getCompanyIdOrRelatedCompany() {
		return this.companyIdOrRelatedCompany;
	}


	@Override
	public void setCompanyIdOrRelatedCompany(Integer companyIdOrRelatedCompany) {
		this.companyIdOrRelatedCompany = companyIdOrRelatedCompany;
	}


	public String getContactName() {
		return this.contactName;
	}


	public void setContactName(String contactName) {
		this.contactName = contactName;
	}


	public Integer getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}


	public Short getContactTypeCategoryId() {
		return this.contactTypeCategoryId;
	}


	public void setContactTypeCategoryId(Short contactTypeCategoryId) {
		this.contactTypeCategoryId = contactTypeCategoryId;
	}


	public Short getCompanyTypeId() {
		return this.companyTypeId;
	}


	public void setCompanyTypeId(Short companyTypeId) {
		this.companyTypeId = companyTypeId;
	}


	public String getCompanyTypeName() {
		return this.companyTypeName;
	}


	public void setCompanyTypeName(String companyTypeName) {
		this.companyTypeName = companyTypeName;
	}


	public String getCoalesceContactCompanyName() {
		return this.coalesceContactCompanyName;
	}


	public void setCoalesceContactCompanyName(String coalesceContactCompanyName) {
		this.coalesceContactCompanyName = coalesceContactCompanyName;
	}


	public String getContactTypeNameEquals() {
		return this.contactTypeNameEquals;
	}


	public void setContactTypeNameEquals(String contactTypeNameEquals) {
		this.contactTypeNameEquals = contactTypeNameEquals;
	}


	@Override
	public Boolean getIncludeClientOrClientRelationship() {
		return this.includeClientOrClientRelationship;
	}


	@Override
	public void setIncludeClientOrClientRelationship(Boolean includeClientOrClientRelationship) {
		this.includeClientOrClientRelationship = includeClientOrClientRelationship;
	}


	public String getExpandedSearchPattern() {
		return this.expandedSearchPattern;
	}


	public void setExpandedSearchPattern(String expandedSearchPattern) {
		this.expandedSearchPattern = expandedSearchPattern;
	}
}
