package com.clifton.business.contact.assignment.observers;

import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.business.contact.assignment.BusinessContactAssignmentCategory;
import com.clifton.business.contact.assignment.BusinessContactAssignmentService;
import com.clifton.business.contact.assignment.BusinessContactAssignmentUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BusinessContactAssignmentCategoryValidator</code> validates changes to {@link BusinessContactAssignmentCategory}s.
 *
 * @author michaelm
 */
@Component
public class BusinessContactAssignmentCategoryValidator extends SelfRegisteringDaoValidator<BusinessContactAssignmentCategory> {

	private SecurityAuthorizationService securityAuthorizationService;
	private BusinessContactAssignmentService businessContactAssignmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BusinessContactAssignmentCategory assignmentCategory, DaoEventTypes config) {
		if (assignmentCategory.isSystemDefined()) {
			if (config.isInsert() || config.isDelete()) {
				throw new ValidationException("System Defined Contact Assignment Categories cannot be " + (config.isInsert() ? "added." : "deleted."));
			}
			else if (config.isUpdate()) {
				boolean admin = getSecurityAuthorizationService().isSecurityUserAdmin();
				ValidationUtils.assertTrue(admin, "Only Administrators are allowed to update system defined Contact Assignment Categories.");
			}
		}

		if (config.isInsert() || config.isUpdate()) {
			ValidationUtils.assertNotNull(getEntityListUrlForCategory(assignmentCategory), String.format("Cannot save Contact Assignment Category because System Table [%s] is missing an entityListUrl and no entityListUrl has been defined for this Category.", assignmentCategory.getEntitySystemTable().getLabel()));
		}

		if (config.isDelete()) {
			List<BusinessContactAssignment> linkedAssignmentList = getBusinessContactAssignmentService().getBusinessContactAssignmentListByCategoryId(assignmentCategory.getId());
			ValidationUtils.assertEmpty(linkedAssignmentList, BusinessContactAssignmentUtils.getLinkedContactAssignmentListViolationString("Cannot delete Contact Assignment Category", assignmentCategory.getName(), linkedAssignmentList));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getEntityListUrlForCategory(BusinessContactAssignmentCategory assignmentCategory) {
		String entityListUrl = assignmentCategory.getEntityListUrl();
		if (entityListUrl == null && assignmentCategory.getEntitySystemTable() != null) {
			return assignmentCategory.getEntitySystemTable().getEntityListUrl();
		}
		return entityListUrl;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public BusinessContactAssignmentService getBusinessContactAssignmentService() {
		return this.businessContactAssignmentService;
	}


	public void setBusinessContactAssignmentService(BusinessContactAssignmentService businessContactAssignmentService) {
		this.businessContactAssignmentService = businessContactAssignmentService;
	}
}
