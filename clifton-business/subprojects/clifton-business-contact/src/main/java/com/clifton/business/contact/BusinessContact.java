package com.clifton.business.contact;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.beans.common.Contact;
import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.AddressUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessContact</code> represents a business contact.
 * <p/>
 * DocumentAware interface is used to associate contacts with corresponding headshot photo.
 *
 * @author manderson
 */
public class BusinessContact extends Contact implements AddressObject, SystemEntityModifyConditionAware, SystemColumnCustomValueAware, DocumentAware {

	public static final String BUSINESS_CONTACT_EMPLOYEE_DETAILS_CUSTOM_COLUMN_GROUP_NAME = "Employee Details";
	public static final String CUSTOM_COLUMN_BIOGRAPHY = "Biography";
	public static final String CUSTOM_COLUMN_PROFESSIONAL_DESIGNATION = "Professional Designation";
	public static final String CUSTOM_COLUMN_PROFESSIONAL_DESIGNATION_2 = "Professional Designation 2";
	public static final String CUSTOM_COLUMN_PROFESSIONAL_DESIGNATION_3 = "Professional Designation 3";

	/**
	 * Main company for the contact set/cleared automatically as the
	 * company for the "Employee" relationship for the contact
	 * that is current active - if there is only one.
	 */
	private BusinessCompany company;

	// Used to categorize and define modify access to the contact and company relationships that use this contact
	// For company contact relationships, will use the category on the type, and if not defined, then access is granted to anyone with write access to category on the contact
	private BusinessContactCategory contactCategory;

	private String middleName;
	private String namePrefix;
	private String nameSuffix;
	private String saluation;

	private String title;


	private String mobileNumber;
	private String efaxNumber;
	private String emailAddressSecondary;

	private String addressLine1;
	private String addressLine2;
	private String addressLine3;

	private String city;
	private String state;
	private String postalCode;
	private String country;

	private Date startDate;
	private Date endDate;

	/**
	 * A List of custom column values
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;

	@NonPersistentField
	private String documentFileType;


	/////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder();
		sb.append(getNameLabel());
		if (getCompany() != null) {
			sb.append(" (").append(getCompany().getName()).append(")");
		}
		return sb.toString();
	}

	@Override
	public String getNameLabel() {
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isEmpty(getNamePrefix())) {
			sb.append(getNamePrefix()).append(" ");
		}
		sb.append(getLabelShort());
		if (!StringUtils.isEmpty(getNameSuffix())) {
			sb.append(" ").append(getNameSuffix());
		}
		return sb.toString();
	}

	@Override
	public String getLabelShort() {
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isEmpty(getFirstName())) {
			sb.append(getFirstName()).append(" ");
		}
		if (!StringUtils.isEmpty(getMiddleName())) {
			sb.append(getMiddleName()).append(" ");
		}
		sb.append(getLastName());
		return sb.toString();
	}


	@Override
	public String getAddressLabel() {
		return AddressUtils.getAddressLabel(this);
	}


	@Override
	public String getCityState() {
		return AddressUtils.getCityState(this);
	}


	@Override
	public String getCityStatePostalCode() {
		return AddressUtils.getCityStatePostalCode(this);
	}


	/**
	 * Returns true if the contact is currently using the main company's address
	 */
	public boolean isUsingCompanyAddress() {
		return getCompany() != null && AddressUtils.isEqual(getCompany(), this, false);
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getContactCategory() != null) {
			return getContactCategory().getEntityModifyCondition();
		}
		return null;
	}




	@Override
	public String getDocumentFileType() {
		return this.documentFileType;
	}


	@Override
	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}


	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////


	@Override
	public String getAddressLine1() {
		return this.addressLine1;
	}


	@Override
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	@Override
	public String getAddressLine2() {
		return this.addressLine2;
	}


	@Override
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	@Override
	public String getAddressLine3() {
		return this.addressLine3;
	}


	@Override
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}


	@Override
	public String getCity() {
		return this.city;
	}


	@Override
	public void setCity(String city) {
		this.city = city;
	}


	@Override
	public String getState() {
		return this.state;
	}


	@Override
	public void setState(String state) {
		this.state = state;
	}


	@Override
	public String getPostalCode() {
		return this.postalCode;
	}


	@Override
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	@Override
	public String getCountry() {
		return this.country;
	}


	@Override
	public void setCountry(String country) {
		this.country = country;
	}


	public String getMiddleName() {
		return this.middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getNamePrefix() {
		return this.namePrefix;
	}


	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}


	public String getNameSuffix() {
		return this.nameSuffix;
	}


	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}


	public String getSaluation() {
		return this.saluation;
	}


	public void setSaluation(String saluation) {
		this.saluation = saluation;
	}


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getMobileNumber() {
		return this.mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getEmailAddressSecondary() {
		return this.emailAddressSecondary;
	}


	public void setEmailAddressSecondary(String emailAddressSecondary) {
		this.emailAddressSecondary = emailAddressSecondary;
	}


	public String getEfaxNumber() {
		return this.efaxNumber;
	}


	public void setEfaxNumber(String efaxNumber) {
		this.efaxNumber = efaxNumber;
	}


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BusinessContactCategory getContactCategory() {
		return this.contactCategory;
	}


	public void setContactCategory(BusinessContactCategory contactCategory) {
		this.contactCategory = contactCategory;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
