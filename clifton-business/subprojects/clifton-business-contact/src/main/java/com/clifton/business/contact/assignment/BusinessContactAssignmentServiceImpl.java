package com.clifton.business.contact.assignment;


import com.clifton.business.contact.assignment.search.BusinessContactAssignmentCategorySearchForm;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentRoleSearchForm;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>BusinessContactAssignmentServiceImpl</code> provides a basic implementation of the
 * {@link BusinessContactAssignmentService} interface
 *
 * @author michaelm
 */
@Service
public class BusinessContactAssignmentServiceImpl implements BusinessContactAssignmentService {

	private AdvancedUpdatableDAO<BusinessContactAssignment, Criteria> businessContactAssignmentDAO;
	private AdvancedUpdatableDAO<BusinessContactAssignmentCategory, Criteria> businessContactAssignmentCategoryDAO;
	private AdvancedUpdatableDAO<BusinessContactAssignmentRole, Criteria> businessContactAssignmentRoleDAO;


	////////////////////////////////////////////////////////////////////////////
	///////////        Business Contact Assignment Methods          ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContactAssignment getBusinessContactAssignment(short id) {
		return getBusinessContactAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContactAssignment> getBusinessContactAssignmentList(final BusinessContactAssignmentSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!StringUtils.isEmpty(searchForm.getSearchPattern())) {
					String[] searchValues = searchForm.getSearchPattern().split("[ ,]");
					// Example Mary,And  will look for (First Name like Mary or Last Name like Mary) and (First Name like And or last name like And)
					String contactAlias = getPathAlias("contact", criteria, JoinType.LEFT_OUTER_JOIN);
					for (String v : searchValues) {
						if (!StringUtils.isEmpty(v)) {
							// in case users do [, ] instead of just [,] between search patterns
							v = v.trim();
							criteria.add(Restrictions.disjunction()
									.add(Restrictions.like(contactAlias + ".firstName", v, MatchMode.ANYWHERE))
									.add(Restrictions.like(contactAlias + ".lastName", v, MatchMode.ANYWHERE)));
						}
					}
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// Special support for Custom Search Fields (search pattern)
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					return false;
				}
				for (OrderByField field : orderByList) {
					String name = getOrderByFieldName(field, criteria);
					if ("searchPattern".equals(name)) {
						String contactAlias = getPathAlias("contact", criteria, JoinType.LEFT_OUTER_JOIN);
						name = contactAlias + ".lastName";
					}
					else if ("active".equals(name)) {
						name = "endDate";
					}
					else {
						name = getOrderByFieldName(field, criteria);
					}
					boolean asc = OrderByDirections.ASC == field.getDirection();
					criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
				}
				return true;
			}
		};
		return getBusinessContactAssignmentDAO().findBySearchCriteria(config);
	}


	@Override
	public List<BusinessContactAssignment> getBusinessContactAssignmentListByCategoryId(short id) {
		return getBusinessContactAssignmentDAO().findByField("contactAssignmentCategory.id", id);
	}


	@Override
	public List<BusinessContactAssignment> getBusinessContactAssignmentListByRoleId(short id) {
		return getBusinessContactAssignmentDAO().findByField("contactAssignmentRole.id", id);
	}


	@Override
	public BusinessContactAssignment saveBusinessContactAssignment(BusinessContactAssignment assignment) {
		return getBusinessContactAssignmentDAO().save(assignment);
	}


	@Override
	public void saveBusinessContactAssignmentList(List<BusinessContactAssignment> assignmentList) {
		getBusinessContactAssignmentDAO().saveList(assignmentList);
	}


	@Override
	public void deleteBusinessContactAssignment(short id) {
		getBusinessContactAssignmentDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////       Business Contact Assignment Category Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContactAssignmentCategory getBusinessContactAssignmentCategory(short id) {
		return getBusinessContactAssignmentCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContactAssignmentCategory> getBusinessContactAssignmentCategoryList(BusinessContactAssignmentCategorySearchForm searchForm) {
		return getBusinessContactAssignmentCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContactAssignmentCategory saveBusinessContactAssignmentCategory(BusinessContactAssignmentCategory assignmentCategory) {
		return getBusinessContactAssignmentCategoryDAO().save(assignmentCategory);
	}


	@Override
	public void deleteBusinessContactAssignmentCategory(short id) {
		getBusinessContactAssignmentCategoryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////         Business Contact Assignment Role Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContactAssignmentRole getBusinessContactAssignmentRole(short id) {
		return getBusinessContactAssignmentRoleDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContactAssignmentRole> getBusinessContactAssignmentRoleListByContact(final int contactId) {
		BusinessContactAssignmentSearchForm assignmentSearchForm = new BusinessContactAssignmentSearchForm();
		assignmentSearchForm.setBusinessContactId(contactId);
		return CollectionUtils.getStream(getBusinessContactAssignmentList(assignmentSearchForm))
				.map(BusinessContactAssignment::getContactAssignmentRole)
				.collect(Collectors.toList());
	}


	@Override
	public List<BusinessContactAssignmentRole> getBusinessContactAssignmentRoleList(final BusinessContactAssignmentRoleSearchForm searchForm) {
		return getBusinessContactAssignmentRoleDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContactAssignmentRole saveBusinessContactAssignmentRole(BusinessContactAssignmentRole assignmentRole) {
		return getBusinessContactAssignmentRoleDAO().save(assignmentRole);
	}


	@Override
	public void deleteBusinessContactAssignmentRole(short id) {
		getBusinessContactAssignmentRoleDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessContactAssignment, Criteria> getBusinessContactAssignmentDAO() {
		return this.businessContactAssignmentDAO;
	}


	public void setBusinessContactAssignmentDAO(AdvancedUpdatableDAO<BusinessContactAssignment, Criteria> businessContactAssignmentDAO) {
		this.businessContactAssignmentDAO = businessContactAssignmentDAO;
	}


	public AdvancedUpdatableDAO<BusinessContactAssignmentCategory, Criteria> getBusinessContactAssignmentCategoryDAO() {
		return this.businessContactAssignmentCategoryDAO;
	}


	public void setBusinessContactAssignmentCategoryDAO(AdvancedUpdatableDAO<BusinessContactAssignmentCategory, Criteria> businessContactAssignmentCategoryDAO) {
		this.businessContactAssignmentCategoryDAO = businessContactAssignmentCategoryDAO;
	}


	public AdvancedUpdatableDAO<BusinessContactAssignmentRole, Criteria> getBusinessContactAssignmentRoleDAO() {
		return this.businessContactAssignmentRoleDAO;
	}


	public void setBusinessContactAssignmentRoleDAO(AdvancedUpdatableDAO<BusinessContactAssignmentRole, Criteria> businessContactAssignmentRoleDAO) {
		this.businessContactAssignmentRoleDAO = businessContactAssignmentRoleDAO;
	}
}
