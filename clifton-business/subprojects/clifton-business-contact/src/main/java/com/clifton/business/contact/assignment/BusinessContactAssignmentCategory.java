package com.clifton.business.contact.assignment;


import com.clifton.business.contact.BusinessContactCategory;
import com.clifton.core.beans.NamedEntity;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>BusinessContactAssignmentCategory</code> is used to categorize {@link BusinessContactAssignment}s.
 *
 * @author michaelm
 */
public class BusinessContactAssignmentCategory extends NamedEntity<Short> {

	/**
	 * Only contacts from this Contact Category will be allowed.
	 */
	private BusinessContactCategory contactCategory;

	/**
	 * Contacts in this Assignment Category will be linked to entities in this table.
	 */
	private SystemTable entitySystemTable;

	/**
	 * For example, investmentAccountListFind.json?ourAccount=true
	 */
	private String entityListUrl;

	/**
	 * If the assignment is system defined, users cannot insert or delete or change the name. Also cannot change system defined value.
	 */
	private boolean systemDefined;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactCategory getContactCategory() {
		return this.contactCategory;
	}


	public void setContactCategory(BusinessContactCategory contactCategory) {
		this.contactCategory = contactCategory;
	}


	public SystemTable getEntitySystemTable() {
		return this.entitySystemTable;
	}


	public void setEntitySystemTable(SystemTable entitySystemTable) {
		this.entitySystemTable = entitySystemTable;
	}


	public String getEntityListUrl() {
		return this.entityListUrl;
	}


	public void setEntityListUrl(String entityListUrl) {
		this.entityListUrl = entityListUrl;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}
}
