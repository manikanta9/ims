package com.clifton.business.contact.observers;


import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessContactTypeValidator</code> ...
 *
 * @author manderson
 */
@Component
public class BusinessContactTypeValidator extends SelfRegisteringDaoValidator<BusinessContactType> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE_OR_DELETE;
	}


	@Override
	public void validate(BusinessContactType bean, DaoEventTypes config) throws ValidationException {
		if (config.isDelete()) {
			ValidationUtils.assertFalse(bean.isSystemDefined(), "You cannot delete a system defined contact type.");
		}
		if (config.isUpdate()) {
			BusinessContactType originalBean = getOriginalBean(bean);
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(bean.isSystemDefined(), "You cannot change a system defined contact type to be not system defined.", "systemDefined");
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "You cannot change the name of a system defined contact type.", "name");
			}
		}
	}
}
