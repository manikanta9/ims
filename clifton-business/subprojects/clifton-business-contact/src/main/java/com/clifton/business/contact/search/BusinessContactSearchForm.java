package com.clifton.business.contact.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>BusinessContactSearchForm</code> class defines search configuration for BusinessContact objects.
 *
 * @author vgomelsky
 */
public class BusinessContactSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField(searchField = "id")
	private Integer[] ids;

	// Custom Search Field
	private String searchPattern;

	@SearchField(searchField = "company.id")
	private Integer companyId;

	@SearchField(searchField = "contactCategory.id")
	private Short contactCategoryId;

	// NOTE: ANY OF THE 3 FOLLOWING DOES A WHERE EXISTS AND APPLIES ALL FILTERS TO THE SAME EXISTS CHECK

	// Custom Search Field - Where exists in company/contact relationship for this contact type
	private Short contactTypeId;

	// Custom Search Field - Where exists in company/contact relationship for any of the contact type names
	private String[] contactTypeNames;

	// Custom Search Field - Where exists in company/contact relationship for company type our company = true
	private Boolean companyContactOurCompany;

	// Custom Search Field - Where exists in company/contact relationship for company with given tag a.k.a. hierarchy
	// Works with the below hierarchy category as well to be selected
	private String companyContactCompanyHierarchyName;

	// Hierarchy category for the above companyContactCompanyHierarchyName search
	private String companyContactCompanyHierarchyCategoryName;

	@SearchField(searchFieldPath = "company", searchField = "name", leftJoin = true)
	private String companyName;

	@SearchField(searchFieldPath = "company.type", searchField = "ourCompany", leftJoin = true)
	private Boolean ourCompany;

	@SearchField
	private String firstName;
	@SearchField
	private String middleName;
	@SearchField
	private String lastName;
	@SearchField
	private String namePrefix;
	@SearchField
	private String nameSuffix;
	@SearchField
	private String saluation;
	@SearchField
	private String title;
	@SearchField
	private String phoneNumber;
	@SearchField
	private String mobileNumber;
	@SearchField
	private String faxNumber;
	@SearchField
	private String emailAddress;
	@SearchField
	private String emailAddressSecondary;

	@SearchField(searchField = "city,state", sortField = "city")
	private String cityState;

	@SearchField
	private String efaxNumber;

	@SearchField
	private String addressLine1;
	@SearchField
	private String addressLine2;
	@SearchField
	private String addressLine3;
	@SearchField
	private String city;
	@SearchField
	private String state;
	@SearchField
	private String postalCode;
	@SearchField
	private String country;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getFirstName() {
		return this.firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getMiddleName() {
		return this.middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getLastName() {
		return this.lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getNamePrefix() {
		return this.namePrefix;
	}


	public void setNamePrefix(String namePrefix) {
		this.namePrefix = namePrefix;
	}


	public String getNameSuffix() {
		return this.nameSuffix;
	}


	public void setNameSuffix(String nameSuffix) {
		this.nameSuffix = nameSuffix;
	}


	public String getSaluation() {
		return this.saluation;
	}


	public void setSaluation(String saluation) {
		this.saluation = saluation;
	}


	public String getTitle() {
		return this.title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getMobileNumber() {
		return this.mobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getFaxNumber() {
		return this.faxNumber;
	}


	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getEmailAddressSecondary() {
		return this.emailAddressSecondary;
	}


	public void setEmailAddressSecondary(String emailAddressSecondary) {
		this.emailAddressSecondary = emailAddressSecondary;
	}


	public String getCityState() {
		return this.cityState;
	}


	public void setCityState(String cityState) {
		this.cityState = cityState;
	}


	public String getEfaxNumber() {
		return this.efaxNumber;
	}


	public void setEfaxNumber(String efaxNumber) {
		this.efaxNumber = efaxNumber;
	}


	public String getAddressLine1() {
		return this.addressLine1;
	}


	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	public String getAddressLine2() {
		return this.addressLine2;
	}


	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	public String getAddressLine3() {
		return this.addressLine3;
	}


	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}


	public String getCity() {
		return this.city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return this.state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getPostalCode() {
		return this.postalCode;
	}


	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public Integer getCompanyId() {
		return this.companyId;
	}


	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}


	public Short getContactCategoryId() {
		return this.contactCategoryId;
	}


	public void setContactCategoryId(Short contactCategoryId) {
		this.contactCategoryId = contactCategoryId;
	}


	public Boolean getOurCompany() {
		return this.ourCompany;
	}


	public void setOurCompany(Boolean ourCompany) {
		this.ourCompany = ourCompany;
	}


	public Short getContactTypeId() {
		return this.contactTypeId;
	}


	public void setContactTypeId(Short contactTypeId) {
		this.contactTypeId = contactTypeId;
	}


	public String[] getContactTypeNames() {
		return this.contactTypeNames;
	}


	public void setContactTypeNames(String[] contactTypeNames) {
		this.contactTypeNames = contactTypeNames;
	}


	public Boolean getCompanyContactOurCompany() {
		return this.companyContactOurCompany;
	}


	public void setCompanyContactOurCompany(Boolean companyContactOurCompany) {
		this.companyContactOurCompany = companyContactOurCompany;
	}


	public String getCompanyContactCompanyHierarchyName() {
		return this.companyContactCompanyHierarchyName;
	}


	public void setCompanyContactCompanyHierarchyName(String companyContactCompanyHierarchyName) {
		this.companyContactCompanyHierarchyName = companyContactCompanyHierarchyName;
	}


	public String getCompanyContactCompanyHierarchyCategoryName() {
		return this.companyContactCompanyHierarchyCategoryName;
	}


	public void setCompanyContactCompanyHierarchyCategoryName(String companyContactCompanyHierarchyCategoryName) {
		this.companyContactCompanyHierarchyCategoryName = companyContactCompanyHierarchyCategoryName;
	}
}
