package com.clifton.business.contact.upload;

import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.core.security.authorization.SecureMethod;


/**
 * <code>BusinessContactAssignmentUploadService</code> facilitates the upload of {@link BusinessContactAssignment}s.
 *
 * @author michaelm
 */
public interface BusinessContactAssignmentUploadService {

	/**
	 * Custom upload to populate {@link BusinessContactAssignment} beans so that the user can configure the upload to automatically end an existing assignment when a new assignment
	 * is being added for that entity and the {@link com.clifton.business.contact.assignment.BusinessContactAssignmentRole#maxAssigneesPerEntity} has already been reached.
	 */
	@SecureMethod(dtoClass = BusinessContactAssignment.class)
	public void uploadBusinessContactAssignmentFile(BusinessContactAssignmentUploadCommand uploadCommand);
}
