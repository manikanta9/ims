package com.clifton.business.contact;


import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.contact.search.BusinessContactCategorySearchForm;
import com.clifton.business.contact.search.BusinessContactRelationshipSearchForm;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.business.contact.search.BusinessContactTypeSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BusinessContactServiceImpl</code> provides a basic implementation of the
 * {@link BusinessContactService} interface
 *
 * @author manderson
 */
@Service
public class BusinessContactServiceImpl implements BusinessContactService {

	private AdvancedUpdatableDAO<BusinessContact, Criteria> businessContactDAO;
	private AdvancedUpdatableDAO<BusinessContactCategory, Criteria> businessContactCategoryDAO;
	private AdvancedUpdatableDAO<BusinessContactType, Criteria> businessContactTypeDAO;
	private AdvancedUpdatableDAO<BusinessContactRelationship, Criteria> businessContactRelationshipDAO;


	////////////////////////////////////////////////////////////////////////////
	///////////            Business Contact Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContact getBusinessContact(int id) {
		return getBusinessContactDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContact> getBusinessContactList(final BusinessContactSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!StringUtils.isEmpty(searchForm.getSearchPattern())) {
					String[] searchValues = searchForm.getSearchPattern().split("[ ,]");
					// Example Mary,And  will look for (First Name like Mary or Last Name like Mary) and (First Name like And or last name like And)
					String companyAlias = getPathAlias("company", criteria, JoinType.LEFT_OUTER_JOIN);
					for (String v : searchValues) {
						if (!StringUtils.isEmpty(v)) {
							// in case users do [, ] instead of just [,] between search patterns
							v = v.trim();
							criteria.add(Restrictions.disjunction()
									.add(Restrictions.like("firstName", v, MatchMode.ANYWHERE))
									.add(Restrictions.like("lastName", v, MatchMode.ANYWHERE))
									.add(Restrictions.like(companyAlias + ".name", v, MatchMode.ANYWHERE)));
						}
					}
				}

				if (searchForm.getContactTypeId() != null || !ArrayUtils.isEmpty(searchForm.getContactTypeNames()) || searchForm.getCompanyContactOurCompany() != null || !StringUtils.isEmpty(searchForm.getCompanyContactCompanyHierarchyName())) {
					DetachedCriteria sub = DetachedCriteria.forClass(BusinessCompanyContact.class, "bcc");
					sub.setProjection(Projections.property("id"));
					sub.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".id"));
					if (searchForm.getContactTypeId() != null) {
						sub.add(Restrictions.eq("contactType.id", searchForm.getContactTypeId()));
					}
					// Don't apply contact type names in if already filtering by contact type id
					else if (searchForm.getContactTypeNames() != null) {
						sub.createAlias("contactType", "ct");
						sub.add(Restrictions.in("ct.name", searchForm.getContactTypeNames()));
					}
					if (searchForm.getCompanyContactOurCompany() != null) {
						sub.createAlias("referenceOne", "cm");
						sub.createAlias("cm.type", "cmt");
						sub.add(Restrictions.eq("cmt.ourCompany", searchForm.getCompanyContactOurCompany()));
					}
					if (!StringUtils.isEmpty(searchForm.getCompanyContactCompanyHierarchyName())) {
						if (StringUtils.isEmpty(searchForm.getCompanyContactCompanyHierarchyCategoryName())) {
							throw new ValidationException("In order to search using the company contact company hierarchy name, the category must also be defined.");
						}
						DetachedCriteria tagSub = DetachedCriteria.forClass(SystemHierarchyLink.class, "shl");
						tagSub.setProjection(Projections.property("id"));
						tagSub.createAlias("hierarchy", "h");
						tagSub.createAlias("h.category", "hc");
						tagSub.add(Restrictions.eq("h.name", searchForm.getCompanyContactCompanyHierarchyName()));
						tagSub.add(Restrictions.eq("hc.name", searchForm.getCompanyContactCompanyHierarchyCategoryName()));
						tagSub.add(Restrictions.eqProperty("shl.fkFieldId", sub.getAlias() + ".referenceOne.id"));
						sub.add(Subqueries.exists(tagSub));
					}
					criteria.add(Subqueries.exists(sub));
				}
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				// Special support for Custom Search Fields (search pattern)
				List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
				if (CollectionUtils.isEmpty(orderByList)) {
					return false;
				}
				for (OrderByField field : orderByList) {
					String name = getOrderByFieldName(field, criteria);
					if ("searchPattern".equals(name)) {
						name = "lastName";
					}
					else if ("active".equals(name)) {
						name = "endDate";
					}
					else {
						name = getOrderByFieldName(field, criteria);
					}
					boolean asc = OrderByDirections.ASC == field.getDirection();
					criteria.addOrder(asc ? Order.asc(name) : Order.desc(name));
				}
				return true;
			}
		};

		return getBusinessContactDAO().findBySearchCriteria(config);
	}


	@Override
	public BusinessContact saveBusinessContact(BusinessContact bean) {
		return getBusinessContactDAO().save(bean);
	}


	@Override
	public void saveBusinessContactList(List<BusinessContact> beanList) {
		getBusinessContactDAO().saveList(beanList);
	}


	@Override
	@Transactional
	public void deleteBusinessContact(int id) {
		deleteBusinessContactRelationshipListByContact(id);
		// Deleting Company Contacts for this contact is in the observer
		getBusinessContactDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Contact Type Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContactType getBusinessContactType(short id) {
		return getBusinessContactTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public BusinessContactType getBusinessContactTypeByName(String name) {
		return getBusinessContactTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<BusinessContactType> getBusinessContactTypeList(BusinessContactTypeSearchForm searchForm) {
		return getBusinessContactTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContactType saveBusinessContactType(BusinessContactType bean) {
		return getBusinessContactTypeDAO().save(bean);
	}


	@Override
	public void deleteBusinessContactType(short id) {
		getBusinessContactTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////            Business Contact Category Methods                ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContactCategory getBusinessContactCategory(short id) {
		return getBusinessContactCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContactCategory> getBusinessContactCategoryList(BusinessContactCategorySearchForm searchForm) {
		return getBusinessContactCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessContactCategory saveBusinessContactCategory(BusinessContactCategory bean) {
		return getBusinessContactCategoryDAO().save(bean);
	}


	@Override
	public void deleteBusinessContactCategory(short id) {
		getBusinessContactCategoryDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          Business Contact Relationship Methods             ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessContactRelationship getBusinessContactRelationship(int id) {
		return getBusinessContactRelationshipDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessContactRelationship> getBusinessContactRelationshipListByContact(final int contactId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.add(Restrictions.or(Restrictions.eq("referenceOne.id", contactId), Restrictions.eq("referenceTwo.id", contactId)));
		return getBusinessContactRelationshipDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<BusinessContactRelationship> getBusinessContactRelationshipList(final BusinessContactRelationshipSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getContactId() != null) {
					criteria.add(Restrictions.or(Restrictions.eq("referenceOne.id", searchForm.getContactId()), Restrictions.eq("referenceTwo.id", searchForm.getContactId())));
				}
			}
		};

		return getBusinessContactRelationshipDAO().findBySearchCriteria(config);
	}


	@Override
	public BusinessContactRelationship saveBusinessContactRelationship(BusinessContactRelationship bean) {
		return getBusinessContactRelationshipDAO().save(bean);
	}


	@Override
	@Transactional
	public void copyBusinessContactRelationships(BusinessContact from, BusinessContact to, boolean moveRelationships, StringBuilder result) {
		// Copy BusinessContactRelationship(s)
		int count = 0;
		List<BusinessContactRelationship> fromContactContactList = getBusinessContactRelationshipListByContact(from.getId());
		if (!CollectionUtils.isEmpty(fromContactContactList)) {
			List<BusinessContactRelationship> updateList = new ArrayList<>();
			List<BusinessContactRelationship> deleteList = new ArrayList<>();

			List<BusinessContactRelationship> toContactContactList = getBusinessContactRelationshipListByContact(to.getId());
			for (BusinessContactRelationship fromCC : CollectionUtils.getIterable(fromContactContactList)) {
				boolean found = false;
				BusinessContact fromRelatedContact = (fromCC.getReferenceOne().equals(from) ? fromCC.getReferenceTwo() : fromCC.getReferenceOne());
				for (BusinessContactRelationship toCC : CollectionUtils.getIterable(toContactContactList)) {
					BusinessContact toRelatedContact = (toCC.getReferenceOne().equals(to) ? toCC.getReferenceTwo() : toCC.getReferenceOne());
					if (fromRelatedContact.equals(toRelatedContact) && StringUtils.isEqual(fromCC.getName(), toCC.getName())) {
						found = true;
						break;
					}
				}
				if (!found) {
					if (fromCC.getReferenceOne().equals(from)) {
						if (moveRelationships) {
							fromCC.setReferenceOne(to);
							updateList.add(fromCC);
						}
						else {
							BusinessContactRelationship copy = BeanUtils.cloneBean(fromCC, false, false);
							copy.setReferenceOne(to);
							updateList.add(copy);
						}
					}
					else {
						if (moveRelationships) {
							fromCC.setReferenceTwo(to);
							updateList.add(fromCC);
						}
						else {
							BusinessContactRelationship copy = BeanUtils.cloneBean(fromCC, false, false);
							copy.setReferenceTwo(to);
							updateList.add(copy);
						}
					}
					count++;
				}
				else if (moveRelationships) {
					// Already there - just delete the old reference
					deleteList.add(fromCC);
				}
			}
			getBusinessContactRelationshipDAO().saveList(updateList);
			getBusinessContactRelationshipDAO().deleteList(deleteList);
		}
		result.append(StringUtils.NEW_LINE + "[").append(count).append("] contact-contact relationships ").append(moveRelationships ? "moved" : "copied").append(".");
	}


	@Override
	public void deleteBusinessContactRelationship(int id) {
		getBusinessContactRelationshipDAO().delete(id);
	}


	private void deleteBusinessContactRelationshipListByContact(int contactId) {
		getBusinessContactRelationshipDAO().deleteList(getBusinessContactRelationshipListByContact(contactId));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessContact, Criteria> getBusinessContactDAO() {
		return this.businessContactDAO;
	}


	public void setBusinessContactDAO(AdvancedUpdatableDAO<BusinessContact, Criteria> businessContactDAO) {
		this.businessContactDAO = businessContactDAO;
	}


	public AdvancedUpdatableDAO<BusinessContactType, Criteria> getBusinessContactTypeDAO() {
		return this.businessContactTypeDAO;
	}


	public void setBusinessContactTypeDAO(AdvancedUpdatableDAO<BusinessContactType, Criteria> businessContactTypeDAO) {
		this.businessContactTypeDAO = businessContactTypeDAO;
	}


	public AdvancedUpdatableDAO<BusinessContactRelationship, Criteria> getBusinessContactRelationshipDAO() {
		return this.businessContactRelationshipDAO;
	}


	public void setBusinessContactRelationshipDAO(AdvancedUpdatableDAO<BusinessContactRelationship, Criteria> businessContactRelationshipDAO) {
		this.businessContactRelationshipDAO = businessContactRelationshipDAO;
	}


	public AdvancedUpdatableDAO<BusinessContactCategory, Criteria> getBusinessContactCategoryDAO() {
		return this.businessContactCategoryDAO;
	}


	public void setBusinessContactCategoryDAO(AdvancedUpdatableDAO<BusinessContactCategory, Criteria> businessContactCategoryDAO) {
		this.businessContactCategoryDAO = businessContactCategoryDAO;
	}
}
