package com.clifton.business.contact.upload;


import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.business.contact.assignment.BusinessContactAssignmentCategory;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.upload.SystemUploadCommand;

import java.util.Date;


/**
 * The <code>BusinessContactAssignmentUploadCommand</code> extends the {@link SystemUploadCommand}
 * however has fields that make it convenient to assign {@link com.clifton.business.contact.BusinessContact}s
 * to <code>InvestmentAccount</code>s.
 *
 * @author michaelm
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class BusinessContactAssignmentUploadCommand extends SystemUploadCommand {

	/**
	 * Used to determine the {@link com.clifton.system.schema.SystemTable} used for {@link BusinessContactAssignment#fkFieldId}s.
	 */
	private BusinessContactAssignmentCategory contactAssignmentCategory;


	/**
	 * Optional startDate for all assignments in the upload file.
	 */
	private Date startDate;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "BusinessContactAssignment";
	}


	@Override
	public void applyUploadSpecificProperties(IdentityObject bean, DataRow row) {
		BusinessContactAssignment contactAssignment = (BusinessContactAssignment) bean;
		if (contactAssignment.getContactAssignmentCategory() == null) {
			ValidationUtils.assertNotNull(getContactAssignmentCategory(), "Please select a Contact Assignment Category from the selection on screen.");
			if (getContactAssignmentCategory() != null) {
				contactAssignment.setContactAssignmentCategory(getContactAssignmentCategory());
			}
		}
		contactAssignment.setStartDate(ObjectUtils.coalesce(contactAssignment.getStartDate(), getStartDate(), new Date()));
	}


	@Override
	@ValueIgnoringGetter
	public String getLinkedEntityTableNameBeanProperty() {
		return "contactAssignmentCategory.entitySystemTable.name";
	}


	@Override
	@ValueIgnoringGetter
	public String getLinkedEntityIdBeanProperty() {
		return "fkFieldId";
	}


	@Override
	@ValueIgnoringGetter
	public String getLinkedEntityLabelBeanProperty() {
		return "assignmentEntityLabel";
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentCategory getContactAssignmentCategory() {
		return this.contactAssignmentCategory;
	}


	public void setContactAssignmentCategory(BusinessContactAssignmentCategory contactAssignmentCategory) {
		this.contactAssignmentCategory = contactAssignmentCategory;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
}
