package com.clifton.business.contact.observers;


import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.company.contact.BusinessCompanyContactSearchForm;
import com.clifton.business.company.contact.BusinessCompanyContactService;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactRelationship;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.AddressUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>BusinessContactObserver</code> ...
 *
 * @author manderson
 */
@Component
public class BusinessContactObserver extends SelfRegisteringDaoObserver<BusinessContact> {

	private BusinessContactService businessContactService;
	private BusinessCompanyContactService businessCompanyContactService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<BusinessContact> dao, DaoEventTypes event, BusinessContact bean) {
		if (event.isDelete()) {
			getBusinessCompanyContactService().deleteBusinessCompanyContactListByContact(bean.getId());
		}
		else {

			// Validate Start/End Dates
			if (bean.getStartDate() != null && bean.getEndDate() != null) {
				ValidationUtils.assertTrue(bean.getEndDate().after(bean.getStartDate()), "Invalid Start/End Dates entered. Start Date must be before End Date.", "startDate");
			}

			if (event.isUpdate()) {
				BusinessContact originalBean = getOriginalBean(dao, bean);

				if (DateUtils.compare(originalBean.getEndDate(), bean.getEndDate(), false) != 0) {
					// Get all Contact - Contact Relationships and End Them where End Date is missing
					List<BusinessContactRelationship> contactRelationshipList = getBusinessContactService().getBusinessContactRelationshipListByContact(bean.getId());
					for (BusinessContactRelationship r : CollectionUtils.getIterable(contactRelationshipList)) {
						if (r.getEndDate() == null) {
							r.setEndDate(bean.getEndDate());
							getBusinessContactService().saveBusinessContactRelationship(r);
						}
					}

					// Get all Contact - Company Relationships and End Them where End Date is missing
					BusinessCompanyContactSearchForm searchForm = new BusinessCompanyContactSearchForm();
					searchForm.setContactId(bean.getId());
					List<BusinessCompanyContact> list = getBusinessCompanyContactService().getBusinessCompanyContactList(searchForm);
					for (BusinessCompanyContact bcc : CollectionUtils.getIterable(list)) {
						if (bcc.getEndDate() == null) {
							bcc.setEndDate(bean.getEndDate());
							getBusinessCompanyContactService().saveBusinessCompanyContact(bcc, true); // okay to ignore validation here because they are existing Company Contacts
						}
					}
				}

				// Main Company Change
				if (!CompareUtils.isEqual(originalBean.getCompany(), bean.getCompany())) {
					// Check if contact was using it's main company's address - or address is blank
					if (AddressUtils.isEmpty(bean) || AddressUtils.isEqual(originalBean.getCompany(), bean, false)) {
						// If yes, copy to the new address (Note, if the new company is null will automatically clear the address in copy method)
						AddressUtils.copyAddress(bean.getCompany(), bean);
					}
				}

				// Otherwise if address is blank and there is a main company - copy the address
				if (AddressUtils.isEmpty(bean) && bean.getCompany() != null) {
					AddressUtils.copyAddress(bean.getCompany(), bean);
				}
			}
		}
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public BusinessCompanyContactService getBusinessCompanyContactService() {
		return this.businessCompanyContactService;
	}


	public void setBusinessCompanyContactService(BusinessCompanyContactService businessCompanyContactService) {
		this.businessCompanyContactService = businessCompanyContactService;
	}
}
