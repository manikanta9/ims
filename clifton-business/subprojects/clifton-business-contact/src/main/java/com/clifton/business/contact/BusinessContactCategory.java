package com.clifton.business.contact;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>BusinessContactCategory</code> class is used to categorize and define modify access to the contact.
 * For example: Client, Parametric Clifton, Operation, Trading.
 *
 * @author manderson
 */
public class BusinessContactCategory extends NamedEntity<Short> implements SystemEntityModifyConditionAware {

	private SystemCondition entityModifyCondition;

	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}
}
