package com.clifton.business.company.contact;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;

import java.util.List;


/**
 * The <code>BusinessCompanyContactEntry</code> class allows bulk edits to multiple company-contact relationships for a specific company and contact.
 * This is specifically useful for cases like Clients where each contact for a Client may have various "contact type" associations.  i.e. Employee, Authorized Signer, Disaster Recovery Contact, etc.
 *
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class BusinessCompanyContactEntry {

	// use referenceOne and referenceTwo so that this object works well with CompanyContacts grids
	// see CompanyContactEntryGrid_ByCompany
	private BusinessCompany referenceOne;
	private BusinessContact referenceTwo;

	private List<BusinessCompanyContact> companyContactList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyContactEntry() {
		super();
	}


	public BusinessCompanyContactEntry(BusinessCompany referenceOne, BusinessContact referenceTwo) {
		this();
		this.referenceOne = referenceOne;
		this.referenceTwo = referenceTwo;
	}


	public void addBusinessCompanyContact(BusinessCompanyContact companyContact) {
		if (getCompanyContactList() == null) {
			setCompanyContactList(CollectionUtils.createList(companyContact));
		}
		else {
			getCompanyContactList().add(companyContact);
		}
	}


	/**
	 * If contact itself doesn't have a main company (either none, or multiple employee relationships)
	 * then this method will return the company (referenceOne)
	 */
	public BusinessCompany getCoalesceContactCompany() {
		if (getReferenceTwo() != null) {
			return ObjectUtils.coalesce(getReferenceTwo().getCompany(), getReferenceOne());
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getReferenceOne() {
		return this.referenceOne;
	}


	public void setReferenceOne(BusinessCompany referenceOne) {
		this.referenceOne = referenceOne;
	}


	public BusinessContact getReferenceTwo() {
		return this.referenceTwo;
	}


	public void setReferenceTwo(BusinessContact referenceTwo) {
		this.referenceTwo = referenceTwo;
	}


	public List<BusinessCompanyContact> getCompanyContactList() {
		return this.companyContactList;
	}


	public void setCompanyContactList(List<BusinessCompanyContact> companyContactList) {
		this.companyContactList = companyContactList;
	}
}
