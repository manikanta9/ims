package com.clifton.business.company.contact;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessCompanyContact</code> classifies a
 * {@link BusinessContact} for a {@link BusinessCompany}.
 *
 * @author manderson
 */
public class BusinessCompanyContact extends ManyToManyEntity<BusinessCompany, BusinessContact, Integer> implements SystemColumnCustomValueAware, SystemEntityModifyConditionAware {

	public static final String COMPANY_CONTACT_CUSTOM_FIELDS_GROUP_NAME = "Company Contact Custom Fields";

	public static final String CUSTOM_FIELD_PORTAL_SUPPORT_TEAM_OVERRIDE = "Portal Support Team Override";
	public static final String CUSTOM_FIELD_PORTAL_CONTACT_TEAM_OVERRIDE = "Portal Contact Team Override";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private BusinessContactType contactType;
	private boolean primary;

	private Date startDate;
	private Date endDate;

	private Integer displayOrder;

	private String functionalRole;

	/**
	 * Used to exclude displaying contacts on reports
	 * i.e. CFS - may have contacts that we know about but we don't want them displayed on the CFS report
	 */
	private boolean privateRelationship;

	/**
	 * A List of custom column values for this security (field are assigned and vary by investment hierarchy)
	 */
	@NonPersistentField
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;


	//////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		if (getReferenceTwo() != null) {
			result.append(getReferenceTwo().getLabel());
		}
		else {
			result.append("UNKNOWN CONTACT");
		}
		result.append(" (");
		if (this.contactType != null) {
			result.append(this.contactType.getName());
			result.append(" for ");
		}
		if (getReferenceOne() != null) {
			result.append(getReferenceOne().getLabel());
		}
		else {
			result.append("UNKNOWN COMPANY");
		}
		result.append(')');
		if (!isActive()) {
			result.append(" - INACTIVE");
		}
		return result.toString();
	}


	/**
	 * If contact itself doesn't have a main company (either none, or multiple employee relationships)
	 * then this method will return the company (referenceOne)
	 */
	public BusinessCompany getCoalesceContactCompany() {
		if (getReferenceTwo() != null) {
			return ObjectUtils.coalesce(getReferenceTwo().getCompany(), getReferenceOne());
		}
		return null;
	}


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getContactType() != null && getContactType().getContactCategory() != null) {
			return getContactType().getContactCategory().getEntityModifyCondition();
		}
		else if (getReferenceTwo() != null && getReferenceTwo().getContactCategory() != null) {
			return getReferenceTwo().getContactCategory().getEntityModifyCondition();
		}
		return null;
	}

	//////////////////////////////////////////////////////////////


	public BusinessContactType getContactType() {
		return this.contactType;
	}


	public void setContactType(BusinessContactType contactType) {
		this.contactType = contactType;
	}


	public boolean isPrimary() {
		return this.primary;
	}


	public void setPrimary(boolean primary) {
		this.primary = primary;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	@Override
	public String getColumnGroupName() {
		return this.columnGroupName;
	}


	@Override
	public void setColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
	}


	@Override
	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	@Override
	public void setColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
	}


	public String getFunctionalRole() {
		return this.functionalRole;
	}


	public void setFunctionalRole(String functionalRole) {
		this.functionalRole = functionalRole;
	}


	public boolean isPrivateRelationship() {
		return this.privateRelationship;
	}


	public void setPrivateRelationship(boolean privateRelationship) {
		this.privateRelationship = privateRelationship;
	}


	public Integer getDisplayOrder() {
		return this.displayOrder;
	}


	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}
}
