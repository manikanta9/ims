package com.clifton.business.contact;


import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>BusinessContactRelationship</code> is used to maintain representative,
 * referral, consultants, etc. on a contact - contact basis.  Similar to the
 * {@link BusinessCompanyContact} table, however this table connects {@link BusinessContact}s.
 *
 * @author manderson
 */
public class BusinessContactRelationship extends ManyToManyEntity<BusinessContact, BusinessContact, Integer> {

	private String name;
	private Date startDate;
	private Date endDate;


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
