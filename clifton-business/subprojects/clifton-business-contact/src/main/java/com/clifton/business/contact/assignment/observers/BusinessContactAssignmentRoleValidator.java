package com.clifton.business.contact.assignment.observers;

import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.business.contact.assignment.BusinessContactAssignmentRole;
import com.clifton.business.contact.assignment.BusinessContactAssignmentService;
import com.clifton.business.contact.assignment.BusinessContactAssignmentUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;


/**
 * The <code>BusinessContactAssignmentRoleValidator</code> validates changes to {@link BusinessContactAssignmentRole}s.
 *
 * @author michaelm
 */
@Component
public class BusinessContactAssignmentRoleValidator extends SelfRegisteringDaoValidator<BusinessContactAssignmentRole> {

	private BusinessContactAssignmentService businessContactAssignmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BusinessContactAssignmentRole assignmentRole, DaoEventTypes config) {
		if (config.isUpdate() && isAuditOfExistingFkFieldsNecessary(assignmentRole)) {
			Set<String> assignmentEntityLabelsExceedingMaxAssigneesPerEntity = BusinessContactAssignmentUtils.getAssignmentEntityLabelsExceedingMaxAssigneesPerEntity(getBusinessContactAssignmentService(), assignmentRole);
			ValidationUtils.assertEmpty(assignmentEntityLabelsExceedingMaxAssigneesPerEntity,
					BusinessContactAssignmentUtils.getMaxAssigneesPerEntityErrorMessage(assignmentRole, assignmentEntityLabelsExceedingMaxAssigneesPerEntity));
		}
		if (config.isDelete()) {
			List<BusinessContactAssignment> linkedAssignmentList = getBusinessContactAssignmentService().getBusinessContactAssignmentListByRoleId(assignmentRole.getId());
			ValidationUtils.assertEmpty(linkedAssignmentList,
					BusinessContactAssignmentUtils.getLinkedContactAssignmentListViolationString("Cannot delete Contact Assignment Role", assignmentRole.getName(), linkedAssignmentList));
		}
	}


	private boolean isAuditOfExistingFkFieldsNecessary(BusinessContactAssignmentRole assignmentRole) {
		Integer newMaxAssigneesPerEntity = assignmentRole.getMaxAssigneesPerEntity();
		if (newMaxAssigneesPerEntity == null) {
			return false;
		}
		BusinessContactAssignmentRole originalBean = getOriginalBean(assignmentRole);
		Integer originalMaxAssigneesPerEntity = originalBean.getMaxAssigneesPerEntity();
		if (originalMaxAssigneesPerEntity == null) {
			return true;
		}
		return originalMaxAssigneesPerEntity > newMaxAssigneesPerEntity;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentService getBusinessContactAssignmentService() {
		return this.businessContactAssignmentService;
	}


	public void setBusinessContactAssignmentService(BusinessContactAssignmentService businessContactAssignmentService) {
		this.businessContactAssignmentService = businessContactAssignmentService;
	}
}
