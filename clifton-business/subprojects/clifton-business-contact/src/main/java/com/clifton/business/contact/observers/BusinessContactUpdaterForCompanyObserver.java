package com.clifton.business.contact.observers;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.contact.BusinessCompanyContactService;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.AddressUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author danielh
 */
@Component
public class BusinessContactUpdaterForCompanyObserver extends SelfRegisteringDaoObserver<BusinessCompany> {

	private BusinessContactService businessContactService;
	private BusinessCompanyContactService businessCompanyContactService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<BusinessCompany> dao, DaoEventTypes event, BusinessCompany bean) {

		// If deleting - also delete CompanyContacts that reference this company
		if (event.isDelete()) {
			getBusinessCompanyContactService().deleteBusinessCompanyContactListByCompany(bean.getId());
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<BusinessCompany> dao, DaoEventTypes event, BusinessCompany bean, Throwable e) {
		// Changes to a Client Name - Replace Client names in Contract Names
		if (e == null && event.isUpdate()) {
			BusinessCompany originalBean = getOriginalBean(dao, bean);
			if (originalBean != null) {
				// If a change to the address - get all contacts where this company is the main company
				if (!AddressUtils.isEqual(originalBean, bean, true)) {
					BusinessContactSearchForm searchForm = new BusinessContactSearchForm();
					searchForm.setCompanyId(bean.getId());
					List<BusinessContact> contactList = getBusinessContactService().getBusinessContactList(searchForm);
					if (!CollectionUtils.isEmpty(contactList)) {
						List<BusinessContact> updateList = new ArrayList<>();
						for (BusinessContact contact : contactList) {
							// If Address was the same as the original, or is blank - update it to the company address
							if (AddressUtils.isEmpty(contact) || AddressUtils.isEqual(originalBean, contact, false)) {
								AddressUtils.copyAddress(bean, contact);
								updateList.add(contact);
							}
						}
						if (!CollectionUtils.isEmpty(updateList)) {
							DaoUtils.executeWithSpecificObserversDisabled(() -> getBusinessContactService().saveBusinessContactList(updateList), SystemEntityModifyConditionAwareObserver.class);
						}
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public BusinessCompanyContactService getBusinessCompanyContactService() {
		return this.businessCompanyContactService;
	}


	public void setBusinessCompanyContactService(BusinessCompanyContactService businessCompanyContactService) {
		this.businessCompanyContactService = businessCompanyContactService;
	}
}
