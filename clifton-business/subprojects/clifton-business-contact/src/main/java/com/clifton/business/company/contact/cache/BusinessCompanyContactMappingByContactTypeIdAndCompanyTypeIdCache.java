package com.clifton.business.company.contact.cache;

import com.clifton.business.company.contact.BusinessCompanyContactMapping;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import org.springframework.stereotype.Component;


/**
 * <code>BusinessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache</code> cache for {@link BusinessCompanyContactMapping} entities, allows fetching by
 * {@link BusinessCompanyContactMapping#businessContactType} and {@link BusinessCompanyContactMapping#businessCompanyType}.
 *
 * @author michaelm
 */
@Component
public class BusinessCompanyContactMappingByContactTypeIdAndCompanyTypeIdCache extends SelfRegisteringCompositeKeyDaoCache<BusinessCompanyContactMapping, Short, Short> {

	@Override
	protected String getBeanKey1Property() {
		return "businessContactType.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "businessCompanyType.id";
	}


	@Override
	protected Short getBeanKey1Value(BusinessCompanyContactMapping bean) {
		return bean.getBusinessContactType().getId();
	}


	@Override
	protected Short getBeanKey2Value(BusinessCompanyContactMapping bean) {
		if (bean.getBusinessCompanyType() != null) {
			return bean.getBusinessCompanyType().getId();
		}
		return null;
	}
}
