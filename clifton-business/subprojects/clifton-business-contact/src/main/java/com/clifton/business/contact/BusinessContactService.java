package com.clifton.business.contact;


import com.clifton.business.contact.search.BusinessContactCategorySearchForm;
import com.clifton.business.contact.search.BusinessContactRelationshipSearchForm;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.business.contact.search.BusinessContactTypeSearchForm;
import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


/**
 * The <code>BusinessContactService</code> defines methods for working with
 * {@link BusinessContact} and related objects
 *
 * @author manderson
 */
public interface BusinessContactService {

	//////////////////////////////////////////////////////////////////////////// 
	///////////            Business Contact Methods                 //////////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessContact getBusinessContact(int id);


	public List<BusinessContact> getBusinessContactList(BusinessContactSearchForm searchForm);


	public BusinessContact saveBusinessContact(BusinessContact bean);


	public void saveBusinessContactList(List<BusinessContact> beanList);


	public void deleteBusinessContact(int id);


	////////////////////////////////////////////////////////////////////////////
	/////////            Business Contact Type Methods                ////////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactType getBusinessContactType(short id);


	public BusinessContactType getBusinessContactTypeByName(String name);


	public List<BusinessContactType> getBusinessContactTypeList(BusinessContactTypeSearchForm searchForm);


	public BusinessContactType saveBusinessContactType(BusinessContactType bean);


	public void deleteBusinessContactType(short id);


	////////////////////////////////////////////////////////////////////////////
	///////            Business Contact Category Methods                //////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactCategory getBusinessContactCategory(short id);


	public List<BusinessContactCategory> getBusinessContactCategoryList(BusinessContactCategorySearchForm searchForm);


	public BusinessContactCategory saveBusinessContactCategory(BusinessContactCategory bean);


	public void deleteBusinessContactCategory(short id);


	////////////////////////////////////////////////////////////////////////////
	////////          Business Contact Relationship Methods             //////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactRelationship getBusinessContactRelationship(int id);


	/**
	 * Returns all relationships where either the first or second contact matches the given
	 * contact id
	 *
	 * @param contactId
	 */
	public List<BusinessContactRelationship> getBusinessContactRelationshipListByContact(int contactId);


	public List<BusinessContactRelationship> getBusinessContactRelationshipList(BusinessContactRelationshipSearchForm searchForm);


	public BusinessContactRelationship saveBusinessContactRelationship(BusinessContactRelationship bean);


	@DoNotAddRequestMapping
	public void copyBusinessContactRelationships(BusinessContact from, BusinessContact to, boolean moveRelationships, StringBuilder result);


	public void deleteBusinessContactRelationship(int id);
}
