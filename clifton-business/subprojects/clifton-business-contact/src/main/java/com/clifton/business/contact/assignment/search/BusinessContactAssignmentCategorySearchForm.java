package com.clifton.business.contact.assignment.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BusinessContactAssignmentCategorySearchForm</code> class defines search configuration for {@link com.clifton.business.contact.assignment.BusinessContactAssignmentCategory} objects.
 *
 * @author michaelm
 */
public class BusinessContactAssignmentCategorySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "contactCategory.id")
	private Short contactCategoryId;

	@SearchField(searchField = "entitySystemTable.id")
	private Short entitySystemTableId;

	@SearchField(searchField = "entitySystemTable.name")
	private String entitySystemTableName;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getContactCategoryId() {
		return this.contactCategoryId;
	}


	public void setContactCategoryId(Short contactCategoryId) {
		this.contactCategoryId = contactCategoryId;
	}


	public Short getEntitySystemTableId() {
		return this.entitySystemTableId;
	}


	public void setEntitySystemTableId(Short entitySystemTableId) {
		this.entitySystemTableId = entitySystemTableId;
	}


	public String getEntitySystemTableName() {
		return this.entitySystemTableName;
	}


	public void setEntitySystemTableName(String entitySystemTableName) {
		this.entitySystemTableName = entitySystemTableName;
	}
}
