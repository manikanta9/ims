package com.clifton.business.contact.assignment;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>BusinessContactAssignmentRole</code> is used to characterize a responsibility. A Role can also reference a Parent Role. For example, PM (Primary) is the parent of PM (Backup).
 *
 * @author michaelm
 */
public class BusinessContactAssignmentRole extends NamedEntity<Short> implements SystemEntityModifyConditionAware {

	private BusinessContactAssignmentRole parentAssignmentRole;

	/**
	 * If this condition is specified, it must evaluate to true in order to be able to create or update {@link BusinessContactAssignment}.
	 * It will usually limit current user to a specific user group.
	 */
	private SystemCondition entityModifyCondition;

	/**
	 * Used to validate on assignment save to enforce based on start/end dates.
	 */
	private Integer maxAssigneesPerEntity;

	private Integer roleOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////



	public BusinessContactAssignmentRole getParentAssignmentRole() {
		return this.parentAssignmentRole;
	}


	public void setParentAssignmentRole(BusinessContactAssignmentRole parentAssignmentRole) {
		this.parentAssignmentRole = parentAssignmentRole;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public Integer getMaxAssigneesPerEntity() {
		return this.maxAssigneesPerEntity;
	}


	public void setMaxAssigneesPerEntity(Integer maxAssigneesPerEntity) {
		this.maxAssigneesPerEntity = maxAssigneesPerEntity;
	}


	public Integer getRoleOrder() {
		return this.roleOrder;
	}


	public void setRoleOrder(Integer roleOrder) {
		this.roleOrder = roleOrder;
	}
}
