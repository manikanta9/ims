package com.clifton.business.contact.assignment.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>BusinessContactAssignmentRoleSearchForm</code> class defines search configuration for {@link com.clifton.business.contact.assignment.BusinessContactAssignmentRole}
 * objects.
 *
 * @author michaelm
 */
public class BusinessContactAssignmentRoleSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "parentAssignmentRole.id")
	private Short parentAssignmentRoleId;

	@SearchField(searchField = "entityModifyCondition.id")
	private Integer entityModifyConditionId;

	@SearchField
	private Integer maxAssigneesPerEntity;

	@SearchField
	private Integer roleOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getParentAssignmentRoleId() {
		return this.parentAssignmentRoleId;
	}


	public void setParentAssignmentRoleId(Short parentAssignmentRoleId) {
		this.parentAssignmentRoleId = parentAssignmentRoleId;
	}


	public Integer getEntityModifyConditionId() {
		return this.entityModifyConditionId;
	}


	public void setEntityModifyConditionId(Integer entityModifyConditionId) {
		this.entityModifyConditionId = entityModifyConditionId;
	}


	public Integer getMaxAssigneesPerEntity() {
		return this.maxAssigneesPerEntity;
	}


	public void setMaxAssigneesPerEntity(Integer maxAssigneesPerEntity) {
		this.maxAssigneesPerEntity = maxAssigneesPerEntity;
	}


	public Integer getRoleOrder() {
		return this.roleOrder;
	}


	public void setRoleOrder(Integer roleOrder) {
		this.roleOrder = roleOrder;
	}
}
