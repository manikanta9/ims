package com.clifton.business.contact.assignment.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


/**
 * The <code>BusinessContactAssignmentForm</code> class defines search configuration for {@link com.clifton.business.contact.assignment.BusinessContactAssignment} objects.
 *
 * @author michaelm
 */
public class BusinessContactAssignmentSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	// Custom Search Field
	private String searchPattern;

	@SearchField(searchField = "contactAssignmentCategory.id")
	private Short contactAssignmentCategoryId;

	@SearchField(searchField = "contactAssignmentCategory.name")
	private String contactAssignmentCategoryName;

	@SearchField(searchField = "contactAssignmentCategory.entitySystemTable.name")
	private String entityTableName;

	@SearchField(searchField = "contactAssignmentRole.id")
	private Short contactAssignmentRoleId;

	@SearchField(searchField = "contactAssignmentRole.name")
	private String contactAssignmentRoleName;

	@SearchField(searchField = "contact.id")
	private Integer businessContactId;

	@SearchField
	private Integer fkFieldId;

	@SearchField
	private String assignmentEntityLabel;


	// BusinessContact properties
	@SearchField(searchField = "contact.id")
	private Integer contactId;

	@SearchField(searchFieldPath = "contact.company", searchField = "name", leftJoin = true)
	private String contactCompanyName;

	@SearchField(searchField = "contact.firstName")
	private String contactFirstName;

	@SearchField(searchField = "contact.middleName")
	private String contactMiddleName;

	@SearchField(searchField = "contact.lastName")
	private String contactLastName;

	@SearchField(searchField = "contact.namePrefix")
	private String contactNamePrefix;

	@SearchField(searchField = "contact.nameSuffix")
	private String contactNameSuffix;

	@SearchField(searchField = "contact.saluation")
	private String contactSaluation;

	@SearchField(searchField = "contact.title")
	private String contactTitle;

	@SearchField(searchField = "contact.phoneNumber")
	private String contactPhoneNumber;

	@SearchField(searchField = "contact.mobileNumber")
	private String contactMobileNumber;

	@SearchField(searchField = "contact.faxNumber")
	private String contactFaxNumber;

	@SearchField(searchField = "contact.efaxNumber")
	private String contactEFaxNumber;

	@SearchField(searchField = "contact.emailAddress")
	private String contactEmailAddress;

	@SearchField(searchField = "contact.emailAddressSecondary")
	private String contactEmailAddressSecondary;

	@SearchField(searchField = "contact.city,contact.state", sortField = "contact.city")
	private String contactCityState;

	@SearchField(searchField = "contact.addressLine1")
	private String contactAddressLine1;

	@SearchField(searchField = "contact.addressLine2")
	private String contactAddressLine2;

	@SearchField(searchField = "contact.addressLine3")
	private String contactAddressLine3;

	@SearchField(searchField = "contact.city")
	private String contactCity;

	@SearchField(searchField = "contact.state")
	private String contactState;

	@SearchField(searchField = "contact.postalCode")
	private String contactPostalCode;

	@SearchField(searchField = "contact.country")
	private String contactCountry;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getContactAssignmentCategoryId() {
		return this.contactAssignmentCategoryId;
	}


	public void setContactAssignmentCategoryId(Short contactAssignmentCategoryId) {
		this.contactAssignmentCategoryId = contactAssignmentCategoryId;
	}


	public String getContactAssignmentCategoryName() {
		return this.contactAssignmentCategoryName;
	}


	public void setContactAssignmentCategoryName(String contactAssignmentCategoryName) {
		this.contactAssignmentCategoryName = contactAssignmentCategoryName;
	}


	public String getEntityTableName() {
		return this.entityTableName;
	}


	public void setEntityTableName(String entityTableName) {
		this.entityTableName = entityTableName;
	}


	public Short getContactAssignmentRoleId() {
		return this.contactAssignmentRoleId;
	}


	public void setContactAssignmentRoleId(Short contactAssignmentRoleId) {
		this.contactAssignmentRoleId = contactAssignmentRoleId;
	}


	public String getContactAssignmentRoleName() {
		return this.contactAssignmentRoleName;
	}


	public void setContactAssignmentRoleName(String contactAssignmentRoleName) {
		this.contactAssignmentRoleName = contactAssignmentRoleName;
	}


	public Integer getBusinessContactId() {
		return this.businessContactId;
	}


	public void setBusinessContactId(Integer businessContactId) {
		this.businessContactId = businessContactId;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public String getAssignmentEntityLabel() {
		return this.assignmentEntityLabel;
	}


	public void setAssignmentEntityLabel(String assignmentEntityLabel) {
		this.assignmentEntityLabel = assignmentEntityLabel;
	}


	public Integer getContactId() {
		return this.contactId;
	}


	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}


	public String getContactCompanyName() {
		return this.contactCompanyName;
	}


	public void setContactCompanyName(String contactCompanyName) {
		this.contactCompanyName = contactCompanyName;
	}


	public String getContactFirstName() {
		return this.contactFirstName;
	}


	public void setContactFirstName(String contactFirstName) {
		this.contactFirstName = contactFirstName;
	}


	public String getContactMiddleName() {
		return this.contactMiddleName;
	}


	public void setContactMiddleName(String contactMiddleName) {
		this.contactMiddleName = contactMiddleName;
	}


	public String getContactLastName() {
		return this.contactLastName;
	}


	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}


	public String getContactNamePrefix() {
		return this.contactNamePrefix;
	}


	public void setContactNamePrefix(String contactNamePrefix) {
		this.contactNamePrefix = contactNamePrefix;
	}


	public String getContactNameSuffix() {
		return this.contactNameSuffix;
	}


	public void setContactNameSuffix(String contactNameSuffix) {
		this.contactNameSuffix = contactNameSuffix;
	}


	public String getContactSaluation() {
		return this.contactSaluation;
	}


	public void setContactSaluation(String contactSaluation) {
		this.contactSaluation = contactSaluation;
	}


	public String getContactTitle() {
		return this.contactTitle;
	}


	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}


	public String getContactPhoneNumber() {
		return this.contactPhoneNumber;
	}


	public void setContactPhoneNumber(String contactPhoneNumber) {
		this.contactPhoneNumber = contactPhoneNumber;
	}


	public String getContactMobileNumber() {
		return this.contactMobileNumber;
	}


	public void setContactMobileNumber(String contactMobileNumber) {
		this.contactMobileNumber = contactMobileNumber;
	}


	public String getContactFaxNumber() {
		return this.contactFaxNumber;
	}


	public void setContactFaxNumber(String contactFaxNumber) {
		this.contactFaxNumber = contactFaxNumber;
	}


	public String getContactEFaxNumber() {
		return this.contactEFaxNumber;
	}


	public void setContactEFaxNumber(String contactEFaxNumber) {
		this.contactEFaxNumber = contactEFaxNumber;
	}


	public String getContactEmailAddress() {
		return this.contactEmailAddress;
	}


	public void setContactEmailAddress(String contactEmailAddress) {
		this.contactEmailAddress = contactEmailAddress;
	}


	public String getContactEmailAddressSecondary() {
		return this.contactEmailAddressSecondary;
	}


	public void setContactEmailAddressSecondary(String contactEmailAddressSecondary) {
		this.contactEmailAddressSecondary = contactEmailAddressSecondary;
	}


	public String getContactCityState() {
		return this.contactCityState;
	}


	public void setContactCityState(String contactCityState) {
		this.contactCityState = contactCityState;
	}


	public String getContactAddressLine1() {
		return this.contactAddressLine1;
	}


	public void setContactAddressLine1(String contactAddressLine1) {
		this.contactAddressLine1 = contactAddressLine1;
	}


	public String getContactAddressLine2() {
		return this.contactAddressLine2;
	}


	public void setContactAddressLine2(String contactAddressLine2) {
		this.contactAddressLine2 = contactAddressLine2;
	}


	public String getContactAddressLine3() {
		return this.contactAddressLine3;
	}


	public void setContactAddressLine3(String contactAddressLine3) {
		this.contactAddressLine3 = contactAddressLine3;
	}


	public String getContactCity() {
		return this.contactCity;
	}


	public void setContactCity(String contactCity) {
		this.contactCity = contactCity;
	}


	public String getContactState() {
		return this.contactState;
	}


	public void setContactState(String contactState) {
		this.contactState = contactState;
	}


	public String getContactPostalCode() {
		return this.contactPostalCode;
	}


	public void setContactPostalCode(String contactPostalCode) {
		this.contactPostalCode = contactPostalCode;
	}


	public String getContactCountry() {
		return this.contactCountry;
	}


	public void setContactCountry(String contactCountry) {
		this.contactCountry = contactCountry;
	}
}
