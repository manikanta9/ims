package com.clifton.business.company.contact;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author michaelm
 */
public class BusinessCompanyContactMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "businessContactType.id")
	private Short businessContactTypeId;

	@SearchField(searchField = "businessCompanyType.id")
	private Short businessCompanyTypeId;

	@SearchField
	private Boolean required;

	@SearchField
	private Boolean oneAllowed;

	@SearchField
	private Boolean onePrimaryRequired;

	@SearchField
	private Boolean notPreferred;

	@SearchField(searchField = "companyContactModifyCondition.id", sortField = "companyContactModifyCondition.name")
	private Integer companyContactModifyCondition;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short getBusinessCompanyTypeId() {
		return this.businessCompanyTypeId;
	}


	public void setBusinessCompanyTypeId(Short businessCompanyTypeId) {
		this.businessCompanyTypeId = businessCompanyTypeId;
	}


	public Short getBusinessContactTypeId() {
		return this.businessContactTypeId;
	}


	public void setBusinessContactTypeId(Short businessContactTypeId) {
		this.businessContactTypeId = businessContactTypeId;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}


	public Boolean getOneAllowed() {
		return this.oneAllowed;
	}


	public void setOneAllowed(Boolean oneAllowed) {
		this.oneAllowed = oneAllowed;
	}


	public Boolean getOnePrimaryRequired() {
		return this.onePrimaryRequired;
	}


	public void setOnePrimaryRequired(Boolean onePrimaryRequired) {
		this.onePrimaryRequired = onePrimaryRequired;
	}


	public Boolean getNotPreferred() {
		return this.notPreferred;
	}


	public void setNotPreferred(Boolean notPreferred) {
		this.notPreferred = notPreferred;
	}


	public Integer getCompanyContactModifyCondition() {
		return this.companyContactModifyCondition;
	}


	public void setCompanyContactModifyCondition(Integer companyContactModifyCondition) {
		this.companyContactModifyCondition = companyContactModifyCondition;
	}
}
