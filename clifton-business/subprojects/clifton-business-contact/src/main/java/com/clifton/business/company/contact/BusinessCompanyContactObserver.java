package com.clifton.business.company.contact;


import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.compare.CompareUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>BusinessCompanyContactObserver</code> set company field on the contact if there isn't one already selected
 * and it's an Employee relationship that's active "today".
 * <p/>
 * There is a batch job that will run nightly to reload for all contacts, and button on Contact setup list window to run immediately if needed.
 *
 * @author manderson
 */
@Component
public class BusinessCompanyContactObserver extends SelfRegisteringDaoObserver<BusinessCompanyContact> {

	private BusinessCompanyContactService businessCompanyContactService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<BusinessCompanyContact> dao, DaoEventTypes event, BusinessCompanyContact bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<BusinessCompanyContact> dao, DaoEventTypes event, BusinessCompanyContact bean, Throwable e) {
		if (e == null) {
			BusinessCompanyContact originalBean = null;
			if (event.isUpdate()) {
				originalBean = getOriginalBean(dao, bean);
			}

			// IF ORIGINAL OR CURRENT IS AN EMPLOYEE RELATIONSHIP 
			// THEN NEED TO CHECK IF MAIN COMPANY FOR THE CONTACT NEEDS TO BE UPDATED
			if (isEmployeeRelationship(originalBean) || isEmployeeRelationship(bean)) {
				// Inserts/Deletes
				if (originalBean == null) {
					getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(bean.getReferenceTwo());
				}
				// Updates - Only if a change in relationship/contact/start/end
				else {
					// Was Employee
					if (isEmployeeRelationship(originalBean)) {
						// Now different type - update contact from original bean
						if (!isEmployeeRelationship(bean)) {
							getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(originalBean.getReferenceTwo());
						}
						// Still an employee
						else {
							// but Contact was changed - update on both
							if (!CompareUtils.isEqual(originalBean.getReferenceTwo(), bean.getReferenceTwo())) {
								getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(originalBean.getReferenceTwo());
								getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(bean.getReferenceTwo());
							}
							// but Company was changed - update one (same contact)
							else if (!CompareUtils.isEqual(originalBean.getReferenceOne(), bean.getReferenceOne())) {
								getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(bean.getReferenceTwo());
							}

							// otherwise if start/end dates changed - active/inactive 
							else if (originalBean.isActive() != bean.isActive()) {
								getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(bean.getReferenceTwo());
							}
						}
					}
					// Otherwise - type was changed to an Employee from something else
					else {
						getBusinessCompanyContactService().updateBusinessContactCompanyFieldForContact(bean.getReferenceTwo());
					}
				}
			}
		}
	}


	private boolean isEmployeeRelationship(BusinessCompanyContact bcc) {
		if (bcc != null && bcc.getContactType() != null && BusinessContactType.EMPLOYEE_CONTACT_TYPE.equals(bcc.getContactType().getName())) {
			return true;
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyContactService getBusinessCompanyContactService() {
		return this.businessCompanyContactService;
	}


	public void setBusinessCompanyContactService(BusinessCompanyContactService businessCompanyContactService) {
		this.businessCompanyContactService = businessCompanyContactService;
	}
}
