package com.clifton.business.contact.assignment;

import com.clifton.business.contact.assignment.search.BusinessContactAssignmentSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Convenience methods for working with {@link BusinessContactAssignment}s and related entities.
 *
 * @author michaelm
 */
public class BusinessContactAssignmentUtils {

	private BusinessContactAssignmentUtils() {
		// Private constructor
	}


	public static String getLinkedContactAssignmentListViolationString(String errorMessagePrefix, String entityName, List<BusinessContactAssignment> linkedAssignmentList) {
		String errorString = String.format("%s [%s] due to [%d] existing Contact Assignments that are using it:", errorMessagePrefix, entityName, CollectionUtils.getSize(linkedAssignmentList));
		return StringUtils.joinErrorListOfViolations(errorString, getAssignmentViolationStringList(linkedAssignmentList));
	}


	public static String getMaxAssigneesPerEntityErrorMessage(BusinessContactAssignment assignment) {
		return String.format("Cannot save Contact Assignment because the Max Assignees Per Entity of [%d] has already been reached for Assignment Role [%s] and Entity [%s] between %s.",
				assignment.getContactAssignmentRole().getMaxAssigneesPerEntity(), assignment.getContactAssignmentRole().getName(), assignment.getAssignmentEntityLabel(), assignment.getActiveOnDateRangeLabel());
	}


	public static String getMaxAssigneesPerEntityErrorMessage(BusinessContactAssignmentRole assignmentRole, Set<String> fkFieldIdsExceedingMaxAssigneesPerEntity) {
		String errorString = String.format("Cannot save Contact Assignment Role with Max Assignees Per Entity of [%d] because the following entities have more than %d Contact Assignments with this Role:",
				assignmentRole.getMaxAssigneesPerEntity(), assignmentRole.getMaxAssigneesPerEntity());
		return StringUtils.joinErrorListOfViolations(errorString, fkFieldIdsExceedingMaxAssigneesPerEntity);
	}


	public static List<String> getAssignmentViolationStringList(List<BusinessContactAssignment> linkedAssignmentList) {
		return CollectionUtils.getStream(linkedAssignmentList)
				.map(BusinessContactAssignment::getLabel)
				.collect(Collectors.toList());
	}


	public static Set<String> getAssignmentEntityLabelsExceedingMaxAssigneesPerEntity(BusinessContactAssignmentService businessContactAssignmentService, BusinessContactAssignmentRole assignmentRole) {
		Map<Integer, List<BusinessContactAssignment>> linkedAssignmentListByEntityMap = getLinkedAssignmentListByFkFieldIdMap(businessContactAssignmentService, assignmentRole);
		Set<String> assignmentEntityLabelsExceedingMaxAssigneesPerEntity = new HashSet<>();
		if (CollectionUtils.isEmpty(linkedAssignmentListByEntityMap)) {
			return assignmentEntityLabelsExceedingMaxAssigneesPerEntity;
		}
		for (List<BusinessContactAssignment> assignmentListByEntityEntry : linkedAssignmentListByEntityMap.values()) {
			if (CollectionUtils.getSize(assignmentListByEntityEntry) > assignmentRole.getMaxAssigneesPerEntity()) {
				assignmentEntityLabelsExceedingMaxAssigneesPerEntity.add(CollectionUtils.getFirstElementStrict(assignmentListByEntityEntry).getAssignmentEntityLabel());
			}
		}
		return assignmentEntityLabelsExceedingMaxAssigneesPerEntity;
	}


	public static Map<Integer, List<BusinessContactAssignment>> getLinkedAssignmentListByFkFieldIdMap(BusinessContactAssignmentService businessContactAssignmentService, BusinessContactAssignmentRole assignmentRole) {
		Map<Integer, List<BusinessContactAssignment>> linkedAssignmentListByFkFieldIdMap = new HashMap<>();
		if (assignmentRole.isNewBean()) {
			return linkedAssignmentListByFkFieldIdMap;
		}
		BusinessContactAssignmentSearchForm assignmentSearchForm = new BusinessContactAssignmentSearchForm();
		assignmentSearchForm.setActive(true);
		assignmentSearchForm.setContactAssignmentRoleId(assignmentRole.getId());
		for (BusinessContactAssignment assignment : CollectionUtils.getIterable(businessContactAssignmentService.getBusinessContactAssignmentList(assignmentSearchForm))) {
			if (assignment.getFkFieldId() != null) {
				linkedAssignmentListByFkFieldIdMap.computeIfAbsent(assignment.getFkFieldId(), contactAssignment -> new ArrayList<>()).add(assignment);
			}
		}
		return linkedAssignmentListByFkFieldIdMap;
	}
}
