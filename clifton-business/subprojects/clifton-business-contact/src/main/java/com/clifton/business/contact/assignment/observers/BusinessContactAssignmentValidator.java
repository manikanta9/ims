package com.clifton.business.contact.assignment.observers;

import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.business.contact.assignment.BusinessContactAssignmentRole;
import com.clifton.business.contact.assignment.BusinessContactAssignmentService;
import com.clifton.business.contact.assignment.BusinessContactAssignmentUtils;
import com.clifton.business.contact.assignment.MaximumAssigneesEntityExceededException;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentSearchForm;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>BusinessContactAssignmentValidator</code> validates changes to {@link BusinessContactAssignment}s.
 *
 * @author michaelm
 */
@Component
public class BusinessContactAssignmentValidator extends SelfRegisteringDaoValidator<BusinessContactAssignment> {

	private BusinessContactAssignmentService businessContactAssignmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(BusinessContactAssignment assignment, DaoEventTypes config) {
		if (config.isInsert() || config.isUpdate()) {
			validateStartDateEndDate(assignment);
			BusinessContactAssignmentRole assignmentRole = assignment.getContactAssignmentRole();
			ValidationUtils.assertNotNull(assignmentRole, "Contact Assignment Role is required for Contact Assignments.");
			ValidationUtils.assertFalse(!StringUtils.isEmpty(assignment.getAssignmentEntityLabel()) && assignment.getFkFieldId() == 0, String.format("Linked Entity Label [%s] is populated for Contact Assignment [%s] but the Assignment is not linked to an entity.", assignment.getAssignmentEntityLabel(), assignment.getLabel()));
			if (assignment.getFkFieldId() != null && assignmentRole.getMaxAssigneesPerEntity() != null && assignmentRole.getMaxAssigneesPerEntity() > 0) {
				List<BusinessContactAssignment> validatedExistingAssignmentList = getValidatedExistingAssignmentList(assignment, config.isUpdate());
				if (CollectionUtils.getSize(validatedExistingAssignmentList) >= assignmentRole.getMaxAssigneesPerEntity()) {
					throw new MaximumAssigneesEntityExceededException(BusinessContactAssignmentUtils.getMaxAssigneesPerEntityErrorMessage(assignment), validatedExistingAssignmentList);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateStartDateEndDate(BusinessContactAssignment assignment) {
		if (assignment.getStartDate() != null) {
			ValidationUtils.assertBefore(assignment.getStartDate(), assignment.getEndDate(), "endDate");
		}
	}


	private List<BusinessContactAssignment> getValidatedExistingAssignmentList(BusinessContactAssignment assignment, boolean update) {
		BusinessContactAssignmentRole assignmentRole = assignment.getContactAssignmentRole();
		BusinessContactAssignmentSearchForm assignmentSearchForm = new BusinessContactAssignmentSearchForm();
		assignmentSearchForm.setContactAssignmentRoleId(assignmentRole.getId());
		assignmentSearchForm.setFkFieldId(assignment.getFkFieldId());
		List<BusinessContactAssignment> existingAssignmentList = getBusinessContactAssignmentService().getBusinessContactAssignmentList(assignmentSearchForm);
		return CollectionUtils.getStream(existingAssignmentList)
				.filter(existingAssignment -> {
					boolean assignmentIdsEqual = false;
					boolean existingAssignmentHasOverlappingActiveDates = DateUtils.isOverlapInDates(assignment.getStartDate(), assignment.getEndDate(), existingAssignment.getStartDate(), existingAssignment.getEndDate());
					if (update) {
						assignmentIdsEqual = existingAssignment.getId().equals(assignment.getId());
					}
					ValidationUtils.assertTrue(assignmentIdsEqual || !existingAssignmentHasOverlappingActiveDates || !existingAssignment.equals(assignment),
							String.format("Cannot save Contact Assignment [%s] because an assignment that is active between %s with the same properties already exists.", assignment.getLabel(), assignment.getActiveOnDateRangeLabel()));
					// exclude the assignment from the list of existing assignments if:
					// 1. the assignment being validated IS this assignment
					// 2. the active date range for the assignment being validated does not overlap with this assignment
					return !assignmentIdsEqual && existingAssignmentHasOverlappingActiveDates;
				})
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentService getBusinessContactAssignmentService() {
		return this.businessContactAssignmentService;
	}


	public void setBusinessContactAssignmentService(BusinessContactAssignmentService businessContactAssignmentService) {
		this.businessContactAssignmentService = businessContactAssignmentService;
	}
}
