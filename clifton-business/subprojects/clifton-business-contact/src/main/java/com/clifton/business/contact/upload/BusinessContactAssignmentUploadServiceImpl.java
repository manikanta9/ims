package com.clifton.business.contact.upload;

import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.business.contact.assignment.BusinessContactAssignmentService;
import com.clifton.business.contact.assignment.MaximumAssigneesEntityExceededException;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BusinessContactAssignmentUploadServiceImpl</code> provides a basic implementation of the {@link BusinessContactAssignmentUploadService}.
 *
 * @author michaelm
 */
@Service
public class BusinessContactAssignmentUploadServiceImpl implements BusinessContactAssignmentUploadService {

	private static final String REPLACED_CONTACT_ASSIGNMENT_MESSAGE_KEY = "Replaced Contact Assignments";

	private BusinessContactAssignmentService businessContactAssignmentService;

	private SystemUploadHandler systemUploadHandler;

	////////////////////////////////////////////////////////////////////////////
	////////      AccountingPositionTransferUploadService Methods       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadBusinessContactAssignmentFile(BusinessContactAssignmentUploadCommand uploadCommand) {
		executeUploadReplaceExistingAssignmentForEntity(uploadCommand);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Automatically end an existing assignment when a new assignment is being added for that entity and the
	 * {@link com.clifton.business.contact.assignment.BusinessContactAssignmentRole#maxAssigneesPerEntity} has already been reached.
	 */
	private void executeUploadReplaceExistingAssignmentForEntity(BusinessContactAssignmentUploadCommand uploadCommand) {
		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, true);
		if (CollectionUtils.isEmpty(beanList)) {
			return;
		}
		if (uploadCommand.isPartialUploadAllowed() || !uploadCommand.isError()) {
			List<String> replacedContactAssignmentMessageList = new ArrayList<>();
			int numInsertedOrUpdatedBeans = 0;
			for (int i = 0; i < CollectionUtils.getSize(beanList); i++) {
				BusinessContactAssignment contactAssignment = (BusinessContactAssignment) beanList.get(i);
				try {
					getBusinessContactAssignmentService().saveBusinessContactAssignment(contactAssignment);
					numInsertedOrUpdatedBeans++;
				}
				catch (MaximumAssigneesEntityExceededException e) {
					try {
						ValidationUtils.assertTrue(CollectionUtils.getSize(e.getExistingAssignmentList()) == 1, String.format("Unable to assign %s as the %s for %s because there are multiple existing assignments between %s. Contact Assignment replacement is only supported when Max Assignees Per Entity is 1.",
								contactAssignment.getContact().getNameLabel(), contactAssignment.getContactAssignmentRole().getLabel(), contactAssignment.getAssignmentEntityLabel(), contactAssignment.getActiveOnDateRangeLabel()));
						Date newAssignmentStartDate = ObjectUtils.coalesce(contactAssignment.getStartDate(), new Date());
						BusinessContactAssignment existingAssignment = CollectionUtils.getFirstElementStrict(e.getExistingAssignmentList());
						existingAssignment.setEndDate(DateUtils.addDays(newAssignmentStartDate, -1));
						existingAssignment = getBusinessContactAssignmentService().saveBusinessContactAssignment(existingAssignment);
						contactAssignment.setStartDate(newAssignmentStartDate);
						contactAssignment = getBusinessContactAssignmentService().saveBusinessContactAssignment(contactAssignment);
						replacedContactAssignmentMessageList.add(String.format("Replaced %s with %s as the %s for %s",
								existingAssignment.getContact().getNameLabel(), contactAssignment.getContact().getNameLabel(), contactAssignment.getContactAssignmentRole().getLabel(), contactAssignment.getAssignmentEntityLabel()));
						numInsertedOrUpdatedBeans++;
					}
					catch (Exception ex) {
						uploadCommand.getUploadResult().addBeanError(i, (StringUtils.isEmpty(ex.getMessage()) ? ExceptionUtils.getOriginalMessage(ex) : ex.getMessage()));
						if (!uploadCommand.isPartialUploadAllowed()) {
							throw ex;
						}
					}
				}
			}
			uploadCommand.getUploadResult().addUploadResults(uploadCommand.getTableName(), numInsertedOrUpdatedBeans,
					FileUploadExistingBeanActions.UPDATE == uploadCommand.getExistingBeans());
			if (!CollectionUtils.isEmpty(replacedContactAssignmentMessageList)) {
				uploadCommand.getUploadResult().addUploadResults(REPLACED_CONTACT_ASSIGNMENT_MESSAGE_KEY, StringUtils.join(replacedContactAssignmentMessageList, StringUtils.NEW_LINE));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentService getBusinessContactAssignmentService() {
		return this.businessContactAssignmentService;
	}


	public void setBusinessContactAssignmentService(BusinessContactAssignmentService businessContactAssignmentService) {
		this.businessContactAssignmentService = businessContactAssignmentService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
