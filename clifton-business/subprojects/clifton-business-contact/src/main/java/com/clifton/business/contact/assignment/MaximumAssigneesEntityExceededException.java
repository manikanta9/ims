package com.clifton.business.contact.assignment;

import com.clifton.core.util.validation.ValidationException;

import java.util.Collections;
import java.util.List;


/**
 * The MaximumEntitiesPerRoleExceededException class represent a {@link ValidationException} that should be thrown by
 * when the a {@link BusinessContactAssignment} is being saved and the {@link BusinessContactAssignmentRole#maxAssigneesPerEntity} has already been reached.
 * The initial use case for this is for automatically "ending" existing assignments and "starting" the assignment that is being saved during uploads.
 *
 * @author michaelm
 */
public class MaximumAssigneesEntityExceededException extends ValidationException {

	private final List<BusinessContactAssignment> existingAssignmentList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MaximumAssigneesEntityExceededException(String message) {
		super(message);
		this.existingAssignmentList = Collections.emptyList();
	}


	public MaximumAssigneesEntityExceededException(String message, List<BusinessContactAssignment> existingAssignmentList) {
		super(message);
		this.existingAssignmentList = existingAssignmentList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<BusinessContactAssignment> getExistingAssignmentList() {
		return this.existingAssignmentList;
	}
}
