package com.clifton.business.contact.assignment;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.util.Date;
import java.util.Objects;


/**
 * <code>BusinessContactAssignment</code> are used to define the role of a contact for a given entity in the system.
 *
 * @author michaelm
 */
public class BusinessContactAssignment extends BaseEntity<Short> implements LabeledObject, SystemEntityModifyConditionAware {

	private BusinessContactAssignmentCategory contactAssignmentCategory;

	private BusinessContactAssignmentRole contactAssignmentRole;

	private BusinessContact contact;

	@SoftLinkField(tableBeanPropertyName = "contactAssignmentCategory.entitySystemTable")
	private int fkFieldId;

	private String assignmentEntityLabel;

	private Date startDate;

	private Date endDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getContactAssignmentRole() != null) {
			return getContactAssignmentRole().getEntityModifyCondition();
		}
		return null;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof BusinessContactAssignment) {
			BusinessContactAssignment objAssignment = (BusinessContactAssignment) obj;
			return getContactAssignmentCategory().equals(objAssignment.getContactAssignmentCategory()) &&
					getContactAssignmentRole().equals(objAssignment.getContactAssignmentRole()) &&
					getContact().equals(objAssignment.getContact()) &&
					Objects.equals(getFkFieldId(), objAssignment.getFkFieldId());
		}
		return false;
	}


	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getContactAssignmentCategory(), getContactAssignmentRole(), getContact(), getFkFieldId());
	}


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		if (getContact() != null) {
			label.append(getContact().getNameLabel());
		}
		else {
			label.append("UNKNOWN CONTACT");
		}
		if (getContactAssignmentRole() != null) {
			label.append(" is ");
			label.append(getContactAssignmentRole().getName());
		}
		if (getContactAssignmentCategory() != null) {
			label.append(" for ");
			label.append(getContactAssignmentCategory().getLabel());
		}
		else {
			label.append(" for UNKNOWN CATEGORY");
		}
		if (!isActive()) {
			label.append(" - INACTIVE");
		}
		return label.toString();
	}


	public boolean isActive() {
		return isActiveOnDate(new Date());
	}


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}


	public String getActiveOnDateRangeLabel() {
		return DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false);
	}


	public String getAssignmentEntityLabel() {
		if (!StringUtils.isEmpty(this.assignmentEntityLabel)) {
			return this.assignmentEntityLabel;
		}
		else if (getContactAssignmentCategory() != null) {
			return getContactAssignmentCategory().getEntitySystemTable().getName() + ": " + getFkFieldId();
		}
		return null;
	}


	public void setAssignmentEntityLabel(String assignmentEntityLabel) {
		this.assignmentEntityLabel = assignmentEntityLabel;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessContactAssignmentCategory getContactAssignmentCategory() {
		return this.contactAssignmentCategory;
	}


	public void setContactAssignmentCategory(BusinessContactAssignmentCategory contactAssignmentCategory) {
		this.contactAssignmentCategory = contactAssignmentCategory;
	}


	public BusinessContactAssignmentRole getContactAssignmentRole() {
		return this.contactAssignmentRole;
	}


	public void setContactAssignmentRole(BusinessContactAssignmentRole contactAssignmentRole) {
		this.contactAssignmentRole = contactAssignmentRole;
	}


	public BusinessContact getContact() {
		return this.contact;
	}


	public void setContact(BusinessContact contact) {
		this.contact = contact;
	}


	public Integer getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(Integer fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
