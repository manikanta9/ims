package com.clifton.business.company.contact;

import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.system.condition.SystemCondition;


/**
 * @author michaelm
 */
public class BusinessCompanyContactMapping extends BaseEntity<Integer> implements LabeledObject {

	private BusinessContactType businessContactType;
	/**
	 * NULL Means Allow Any (use for type "Employee")
	 */
	private BusinessCompanyType businessCompanyType;

	/**
	 * If TRUE, used to generate Notifications if a company exists without a contact associated with the type
	 * Notification is user acknowledged (i.e. not a hard requirement we don't have start/end date on companies, so we'd need to be able to filter out terminated client relationships, clients, etc.)
	 */
	private boolean required;
	/**
	 * If TRUE, used to generate notifications when multiple are found for a company. Validation on save won't allow the duplicate to get in, but notification will be a backup.
	 * If this is set to true after the mapping is set up - check all companies at that point and don't allow that? - or just let notification find the violations?
	 */
	private boolean oneAllowed;
	/**
	 * When there are multiple contacts of the given Contact Type for the given Company Type, exactly one of them must be the primary.
	 */
	private boolean onePrimaryRequired;
	/**
	 * Allows multiple contact type mappings, but sets one of them as preferred. Used for UI Alert and/or Notification
	 * Example: NBD Client Service Rep is "preferred" on the Client Relationship, however it can be entered at the Client level if different than the relationship.
	 * Would prevent users from accidentally adding relationships at the wrong "level" for Client vs. Client Relationships
	 */
	private boolean notPreferred;

	/**
	 * A condition that must pass when users are attempt to create/update/delete {@link BusinessCompanyContact}s
	 * of the given {@link BusinessCompanyType} and {@link BusinessContactType}
	 * <p>
	 * Validated in the {@link BusinessCompanyContactService}
	 */
	private SystemCondition companyContactModifyCondition;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		label.append(getBusinessContactType().getName()).append(" - ");
		label.append(getBusinessCompanyType() != null ? getBusinessCompanyType().getName() : "ALL");
		return label.toString();
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BusinessContactType getBusinessContactType() {
		return this.businessContactType;
	}


	public void setBusinessContactType(BusinessContactType businessContactType) {
		this.businessContactType = businessContactType;
	}


	public BusinessCompanyType getBusinessCompanyType() {
		return this.businessCompanyType;
	}


	public void setBusinessCompanyType(BusinessCompanyType businessCompanyType) {
		this.businessCompanyType = businessCompanyType;
	}


	public boolean isRequired() {
		return this.required;
	}


	public void setRequired(boolean required) {
		this.required = required;
	}


	public boolean isOneAllowed() {
		return this.oneAllowed;
	}


	public void setOneAllowed(boolean oneAllowed) {
		this.oneAllowed = oneAllowed;
	}


	public boolean isOnePrimaryRequired() {
		return this.onePrimaryRequired;
	}


	public void setOnePrimaryRequired(boolean onePrimaryRequired) {
		this.onePrimaryRequired = onePrimaryRequired;
	}


	public boolean isNotPreferred() {
		return this.notPreferred;
	}


	public void setNotPreferred(boolean notPreferred) {
		this.notPreferred = notPreferred;
	}


	public SystemCondition getCompanyContactModifyCondition() {
		return this.companyContactModifyCondition;
	}


	public void setCompanyContactModifyCondition(SystemCondition companyContactModifyCondition) {
		this.companyContactModifyCondition = companyContactModifyCondition;
	}
}
