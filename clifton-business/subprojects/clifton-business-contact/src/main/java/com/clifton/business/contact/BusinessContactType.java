package com.clifton.business.contact;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>BusinessContactType</code> defines the classification or title of a contact.
 * i.e. Manager, Attorney, Analyst
 *
 * @author manderson
 */
public class BusinessContactType extends NamedEntity<Short> implements SystemEntityModifyConditionAware {

	// System Defined Contact Type that drives the Company selection on the Contact
	// When there is only one of these relationships defined
	public static final String EMPLOYEE_CONTACT_TYPE = "Employee";

	public static final String RELATIONSHIP_MANAGER_CONTACT_TYPE = "Relationship Manager";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	// Used to categorize and define modify access to these contact types and company relationships that use this type
	// For company contact relationships, if not defined, then access is granted to anyone with write access to category on the contact
	private BusinessContactCategory contactCategory;

	// Determines whether or not the contact type name can be changed
	private boolean systemDefined;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getContactCategory() != null) {
			return getName() + " (" + getContactCategory().getName() + ")";
		}
		return getName();
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		if (getContactCategory() != null) {
			return getContactCategory().getEntityModifyCondition();
		}
		return null;
	}



	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public BusinessContactCategory getContactCategory() {
		return this.contactCategory;
	}


	public void setContactCategory(BusinessContactCategory contactCategory) {
		this.contactCategory = contactCategory;
	}
}
