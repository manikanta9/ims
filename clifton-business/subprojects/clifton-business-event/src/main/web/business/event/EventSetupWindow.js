Clifton.business.event.EventSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'businessEventSetupWindow',
	title: 'Business Events',
	iconCls: 'schedule',
	width: 1200,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Events',
				items: [{
					name: 'businessEventListFind',
					xtype: 'gridpanel',
					instructions: 'The following business events have been recorded in the system.  A business event is an event that occurs on a specific date where some action may be required, i.e. notify all clients of a change to Senior Management.',
					columns: [
						{header: 'Event Type', width: 100, dataIndex: 'type.name'},
						{header: 'Event Date', width: 40, dataIndex: 'eventDate', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.event.EventWindow',
						deleteURL: 'businessEventDelete.json'
					}
				}]
			},


			{
				title: 'Event Types',
				items: [{
					name: 'businessEventTypeListFind',
					xtype: 'gridpanel',
					instructions: 'The following event types are in the system.  An event type defines the business event types that we should track in the system.',
					columns: [
						{header: 'Type Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.event.TypeWindow',
						deleteURL: 'businessEventTypeDelete.json'
					}
				}]
			}
		]
	}]
});

