Clifton.business.event.TypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Event Type',
	iconCls: 'schedule',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Event Type',
				items: [{
					xtype: 'formpanel',
					instructions: 'An event type defines an event type that we should track in the system.',
					url: 'businessEventType.json',
					items: [
						{fieldLabel: 'Type Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'Events',
				items: [{
					name: 'businessEventListFind',
					xtype: 'gridpanel',
					instructions: 'The following events for the selected event type have occurred or are scheduled to occur.',
					columns: [
						{header: 'Event Date', width: 50, dataIndex: 'eventDate'},
						{header: 'Description', width: 200, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.business.event.EventWindow',
						getDefaultData: function(gridPanel) {
							return {
								type: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function() {
						return {'typeId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
