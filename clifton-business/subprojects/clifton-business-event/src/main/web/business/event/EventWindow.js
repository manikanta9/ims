Clifton.business.event.EventWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Business Event',
	iconCls: 'schedule',

	items: [{
		xtype: 'formpanel',
		url: 'businessEvent.json',
		instructions: 'A business event defines a specific occurrence of a business event type.',
		items: [
			{fieldLabel: 'Event Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', url: 'businessEventTypeListFind.json', detailPageClass: 'Clifton.business.event.TypeWindow'},
			{fieldLabel: 'Event Date', name: 'eventDate', xtype: 'datefield'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
