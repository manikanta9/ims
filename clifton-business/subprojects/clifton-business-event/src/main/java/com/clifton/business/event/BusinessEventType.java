package com.clifton.business.event;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>BusinessEventType</code> defines the Business Event Types that may occur at Clifton.
 * <p/>
 * Examples:
 * Change to Senior Management
 * Change to Corporate Structure
 * Change to Ownership Structure
 * Change to Key Personnel
 * Change to ADV I and/or II
 *
 * @author Mary Anderson
 */
public class BusinessEventType extends NamedEntity<Short> {

	// NOTHING HERE FOR NOW
}
