package com.clifton.business.event;


import com.clifton.business.event.search.BusinessEventSearchForm;
import com.clifton.business.event.search.BusinessEventTypeSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>BusinessEventServiceImpl</code> implements the {@link BusinessEventService}
 *
 * @author Mary Anderson
 */
@Service
public class BusinessEventServiceImpl implements BusinessEventService {

	private AdvancedUpdatableDAO<BusinessEventType, Criteria> businessEventTypeDAO;
	private AdvancedUpdatableDAO<BusinessEvent, Criteria> businessEventDAO;


	//////////////////////////////////////////////////////////////////////////// 
	//////////              Business Event Methods                  //////////// 
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessEvent getBusinessEvent(int id) {
		return getBusinessEventDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessEvent> getBusinessEventList(BusinessEventSearchForm searchForm) {
		return getBusinessEventDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessEvent saveBusinessEvent(BusinessEvent bean) {
		return getBusinessEventDAO().save(bean);
	}


	@Override
	public void deleteBusinessEvent(int id) {
		getBusinessEventDAO().delete(id);
	}


	//////////////////////////////////////////////////////////////////////////// 
	//////////            Business Event Type Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BusinessEventType getBusinessEventType(short id) {
		return getBusinessEventTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<BusinessEventType> getBusinessEventTypeList(BusinessEventTypeSearchForm searchForm) {
		return getBusinessEventTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public BusinessEventType saveBusinessEventType(BusinessEventType bean) {
		return getBusinessEventTypeDAO().save(bean);
	}


	@Override
	public void deleteBusinessEventType(short id) {
		getBusinessEventTypeDAO().delete(id);
	}


	//////////////////////////////////////////////////////////////////////////// 
	////////////            Getter & Setter Methods                 //////////// 
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<BusinessEventType, Criteria> getBusinessEventTypeDAO() {
		return this.businessEventTypeDAO;
	}


	public void setBusinessEventTypeDAO(AdvancedUpdatableDAO<BusinessEventType, Criteria> businessEventTypeDAO) {
		this.businessEventTypeDAO = businessEventTypeDAO;
	}


	public AdvancedUpdatableDAO<BusinessEvent, Criteria> getBusinessEventDAO() {
		return this.businessEventDAO;
	}


	public void setBusinessEventDAO(AdvancedUpdatableDAO<BusinessEvent, Criteria> businessEventDAO) {
		this.businessEventDAO = businessEventDAO;
	}
}
