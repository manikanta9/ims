package com.clifton.business.event.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>BusinessEventSearchForm</code> defines the fields when searching for BusinessEvents
 *
 * @author Mary Anderson
 */
public class BusinessEventSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "type.id", comparisonConditions = {ComparisonConditions.EQUALS})
	private Short typeId;

	@SearchField(searchField = "name", searchFieldPath = "type", comparisonConditions = {ComparisonConditions.EQUALS})
	private String typeName;

	@SearchField
	private Date eventDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public String getTypeName() {
		return this.typeName;
	}


	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
}
