package com.clifton.business.event;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>BusinessEvent</code> defines a specific business event that occurs at a given date.
 *
 * @author Mary Anderson
 */
public class BusinessEvent extends BaseEntity<Integer> implements LabeledObject {

	private BusinessEventType type;
	private Date eventDate;
	private String description;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		if (getType() != null && getEventDate() != null) {
			return getType().getLabel() + " (" + DateUtils.fromDate(getEventDate(), DateUtils.DATE_FORMAT_INPUT) + ")";
		}
		return null;
	}


	public BusinessEventType getType() {
		return this.type;
	}


	public void setType(BusinessEventType type) {
		this.type = type;
	}


	public Date getEventDate() {
		return this.eventDate;
	}


	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
