package com.clifton.business.event;


import com.clifton.business.event.search.BusinessEventSearchForm;
import com.clifton.business.event.search.BusinessEventTypeSearchForm;

import java.util.List;


/**
 * The <code>BusinessEventService</code> defines methods for working with {@link BusinessEventType}s
 * and {@link BusinessEvent}s.
 *
 * @author Mary Anderson
 */
public interface BusinessEventService {

	//////////////////////////////////////////////////////////////////////////// 
	//////////              Business Event Methods                  //////////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessEvent getBusinessEvent(int id);


	public List<BusinessEvent> getBusinessEventList(BusinessEventSearchForm searchForm);


	public BusinessEvent saveBusinessEvent(BusinessEvent bean);


	public void deleteBusinessEvent(int id);


	//////////////////////////////////////////////////////////////////////////// 
	//////////            Business Event Type Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public BusinessEventType getBusinessEventType(short id);


	public List<BusinessEventType> getBusinessEventTypeList(BusinessEventTypeSearchForm searchForm);


	public BusinessEventType saveBusinessEventType(BusinessEventType bean);


	public void deleteBusinessEventType(short id);
}
