package com.clifton.business.event;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author danielh
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BusinessEventProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "business-event";
	}
}
