package com.clifton.build;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;


/**
 * @author NickK
 */

@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BuildProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "build";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.reflections.");
		// Bamboo Build Plan items;
		imports.add("com.atlassian.bamboo.specs.");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		// Bamboo Build Plan items;
		imports.add("com.atlassian.bamboo.specs.");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("build");
		approvedList.add("bamboo");
	}
}
