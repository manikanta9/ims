package com.clifton.build.bamboo.plan;

import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.clifton.build.bamboo.util.BambooUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


/**
 * <code>BambooProjectPlanTests</code> base test file for Bamboo build plan validation.
 *
 * @author NickK
 */
public class BambooProjectPlanTests {

	@Test
	public void testValidationOfBambooProjectBuildPlans() {
		List<BambooProjectPlan> projectBuildPlanList = BambooUtils.getBambooProjectBuildPlanListFromClasspath();
		StringBuilder errors = new StringBuilder();
		projectBuildPlanList.forEach(buildPlan -> {
			try {
				Plan plan = buildPlan.getPlan();
				EntityPropertiesBuilders.build(plan);
				System.out.println("Validated build plan: " + buildPlan);
			}
			catch (Exception e) {
				errors.append("Failed to validate plan: ").append(buildPlan).append(", error: ").append(e.getMessage());
			}
		});
		if (errors.length() > 0) {
			Assertions.fail(errors.toString());
		}
	}
}
