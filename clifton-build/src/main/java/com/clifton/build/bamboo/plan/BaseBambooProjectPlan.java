package com.clifton.build.bamboo.plan;


import com.atlassian.bamboo.specs.api.builders.BambooKey;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.clifton.build.bamboo.util.BambooUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * <code>BaseBambooProjectPlan</code> base implementation of {@link BambooProjectPlan} defining
 * common functionality that can differ based on the plan type and defined properties.
 *
 * @author NickK
 */
public class BaseBambooProjectPlan implements BambooProjectPlan {

	public static final String DEFAULT_REPOSITORY = "Build";
	public static final String RELEASE_BRANCH_PATTERN = "release/app-clifton-ims*.*";

	// project properties
	private final String projectName;
	private final Project project;
	// plan properties
	private final String name;
	private final BambooProjectPlanTypes planType;
	private String planKey;
	private String description;
	private BambooProjectArtifactTypes artifactType = BambooProjectArtifactTypes.NONE;
	private String repository = DEFAULT_REPOSITORY;
	private boolean enableBranching;
	private String branchPattern = RELEASE_BRANCH_PATTERN;
	private final List<String> commandLineArgumentList = new ArrayList<>();
	private final List<String> gradleTaskList = new ArrayList<>();

	///////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a new instance for the provided project name and plan type. The following defaults are made:
	 * <br/>- name = {@link BambooProjectPlanTypes#getPlanName()}
	 *
	 * @param projectName
	 * @param type
	 */
	public BaseBambooProjectPlan(String projectName, BambooProjectPlanTypes type) {
		this.projectName = projectName;
		this.planType = type;
		this.name = type.getPlanName().toUpperCase();
		this.enableBranching = getPlanType() == BambooProjectPlanTypes.BUILD;

		String shortProjectName = getProjectName().toLowerCase()
				.replace("clifton-", "")
				.replace("client-", "")
				.toUpperCase();
		this.project = new Project().name(shortProjectName)
				.key(new BambooKey(shortProjectName.replace("APP", "").replace("SERVICE", "").replaceAll("-", "")));
		List<String> planKeyStringList = ArrayUtils.getStream(shortProjectName.split("-")).collect(Collectors.toList());
		planKeyStringList.add(getName());
		this.planKey = CollectionUtils.getStream(planKeyStringList)
				.map(string -> string.substring(0, 1))
				.collect(Collectors.joining());
	}

	///////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "{project: " + getProjectName() + ", plan: " + getName() + ", type: " + getPlanType() + "}";
	}


	@Override
	public Plan getPlan() {
		Plan plan = new Plan(getProject(), getName(), getPlanKey())
				.description(StringUtils.isEmpty(getDescription()) ? getProjectName() + " plan." : getDescription())
				.stages(getStages())
				.linkedRepositories(getRepository())
				.triggers(getTriggers())
				.enabled(true);
		if (isEnableBranching()) {
			plan.planBranchManagement(StringUtils.isEmpty(getBranchPattern()) ? BambooUtils.getPlanBranchManagementForEveryBranch() : BambooUtils.getPlanBranchManagementForBranchesMatchingPattern(getBranchPattern()));
		}
		return plan;
	}


	@Override
	public Permissions getPermissions() {
		return null;
	}

	///////////////////////////////////////////////////////////////////////////


	protected Stage[] getStages() {
		Job job = new Job(getPlanType().getStageName() + " Job", new BambooKey(getName()));
		Artifact artifact = BambooUtils.getArtifact(getArtifactType(), getProjectName());
		if (artifact != null) {
			job.artifacts(artifact);
		}
		switch (getPlanType()) {
			case BUILD:
				if (CollectionUtils.isEmpty(getCommandLineArgumentList())) {
					addCommandLineArguments("${bamboo.gradleArgs}", "-Dorg.gradle.jvmargs=${bamboo.vmArgs}",
							"-PisResourceFilteringEnabled=false", "-PbuildCompressedDistribution=true",
							"-PincludeAdditionalHibernateMappingInput=true");
				}
				if (CollectionUtils.isEmpty(getGradleTaskList())) {
					if (getArtifactType() == BambooProjectArtifactTypes.WAR) {
						addGradleTasks(getProjectName() + ":cleanWarDir");
					}
					addGradleTasks(getProjectName() + ":buildNeeded");
				}
				job.tasks(BambooUtils.defaultSourceCodeCheckoutTask(),
						BambooUtils.newGradleTask(getCommandLineArgumentList(), getGradleTaskList()),
						BambooUtils.windowsTestResultFileTouchTask(),
						BambooUtils.gradleJunitTestResultParserTask());
				break;
			case PUBLISH:
				if (CollectionUtils.isEmpty(getCommandLineArgumentList())) {
					addCommandLineArguments("${bamboo.gradleArgs}", "-Dorg.gradle.jvmargs=${bamboo.vmArgs}",
							"-Dbamboo.server=${bamboo.bambooPublishServer}", "-Dbamboo.user=${bamboo.bambooPublishUser}",
							"-Dbamboo.password=${bamboo.bambooPublishPassword}");
				}
				if (CollectionUtils.isEmpty(getGradleTaskList())) {
					addGradleTasks(getProjectName() + ":publishBambooPlan");
				}
				job.tasks(BambooUtils.defaultSourceCodeCheckoutTask(),
						BambooUtils.newGradleTask(getCommandLineArgumentList(), getGradleTaskList()));
				break;
			default:
				// Do nothing
		}
		return new Stage[]{new Stage(getPlanType().getStageName() + " Stage").jobs(job)};
	}


	protected Trigger<?, ?>[] getTriggers() {
		if (getPlanType() == BambooProjectPlanTypes.BUILD) {
			return new Trigger<?, ?>[]{BambooUtils.getDefaultBranchCommitTrigger()};
		}
		return new Trigger[0];
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getProjectName() {
		return this.projectName;
	}


	@Override
	public Project getProject() {
		return this.project;
	}


	/**
	 * In order to correctly match the plan to an existing Bamboo {@link Project} the project key must be specified
	 * instead of the generated default so the Bamboo server can find the correct project.
	 */
	public BaseBambooProjectPlan setProjectKey(String projectKey) {
		getProject().key(projectKey);
		return this;
	}


	@Override
	public String getName() {
		return this.name;
	}


	@Override
	public String getPlanKey() {
		return this.planKey;
	}


	/**
	 * In order to correctly match the plan to an existing Bamboo {@link Plan} the plan key must be specified
	 * instead of the generated default so the Bamboo server can find the correct plan.
	 */
	public BaseBambooProjectPlan setPlanKey(String planKey) {
		this.planKey = planKey;
		return this;
	}


	@Override
	public String getDescription() {
		return this.description;
	}


	public BaseBambooProjectPlan setDescription(String description) {
		this.description = description;
		return this;
	}


	@Override
	public BambooProjectPlanTypes getPlanType() {
		return this.planType;
	}


	@Override
	public BambooProjectArtifactTypes getArtifactType() {
		return this.artifactType;
	}


	public BaseBambooProjectPlan setArtifactType(BambooProjectArtifactTypes artifactType) {
		this.artifactType = artifactType;
		return this;
	}


	@Override
	public String getRepository() {
		return this.repository;
	}


	public BaseBambooProjectPlan setRepository(String repository) {
		this.repository = repository;
		return this;
	}


	@Override
	public boolean isEnableBranching() {
		return this.enableBranching;
	}


	public BaseBambooProjectPlan setEnableBranching(boolean enableBranching) {
		this.enableBranching = enableBranching;
		return this;
	}


	@Override
	public String getBranchPattern() {
		return this.branchPattern;
	}


	public BaseBambooProjectPlan setBranchPattern(String branchPattern) {
		this.branchPattern = branchPattern;
		return this;
	}


	public void addCommandLineArguments(String... arguments) {
		ArrayUtils.getStream(arguments)
				.filter(arg -> !StringUtils.isEmpty(arg))
				.forEach(this.commandLineArgumentList::add);
	}


	@Override
	public List<String> getCommandLineArgumentList() {
		return this.commandLineArgumentList;
	}


	public void addGradleTasks(String... tasks) {
		ArrayUtils.getStream(tasks)
				.filter(task -> !StringUtils.isEmpty(task))
				.forEach(this.gradleTaskList::add);
	}


	@Override
	public List<String> getGradleTaskList() {
		return this.gradleTaskList;
	}
}
