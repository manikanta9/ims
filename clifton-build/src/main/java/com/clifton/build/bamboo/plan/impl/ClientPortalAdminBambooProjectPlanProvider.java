package com.clifton.build.bamboo.plan.impl;

import com.clifton.build.bamboo.plan.AbstractBambooProjectPlanProvider;
import com.clifton.build.bamboo.plan.BambooProjectPlan;


/**
 * @author NickK
 */
public class ClientPortalAdminBambooProjectPlanProvider extends AbstractBambooProjectPlanProvider {

	@Override
	protected String getProjectName() {
		return "app-clifton-client-portal-admin";
	}


	@Override
	protected BambooProjectPlan.BambooProjectArtifactTypes getArtifactType() {
		return BambooProjectPlan.BambooProjectArtifactTypes.WAR;
	}
}
