package com.clifton.build.bamboo.plan.impl;

import com.clifton.build.bamboo.plan.AbstractBambooProjectPlanProvider;
import com.clifton.build.bamboo.plan.BambooProjectPlan;

import java.util.List;


/**
 * @author NickK
 */
public class ImsBambooProjectPlanProvider extends AbstractBambooProjectPlanProvider {

	@Override
	public List<BambooProjectPlan> getBambooProjectPlanList() {
		List<BambooProjectPlan> projectPlanList = super.getBambooProjectPlanList();

		// TODO add Integration, PIOS, and Inspection plans

		return projectPlanList;
	}


	@Override
	protected String getProjectName() {
		return "app-clifton-ims";
	}


	@Override
	protected BambooProjectPlan.BambooProjectArtifactTypes getArtifactType() {
		return BambooProjectPlan.BambooProjectArtifactTypes.WAR;
	}


	@Override
	protected String getBranchPattern() {
		return null;
	}


	@Override
	protected String getProjectKey() {
		return "AP";
	}


	@Override
	protected String getBuildPlanKey() {
		return "TRUN";
	}
}
