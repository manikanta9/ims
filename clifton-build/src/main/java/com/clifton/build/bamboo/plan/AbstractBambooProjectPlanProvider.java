package com.clifton.build.bamboo.plan;

import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * <code>AbstractBambooProjectPlanProvider</code> is an abstract implementation of {@link BambooProjectPlanProvider} that creates
 * {@link BambooProjectPlan}s for the following {@link BambooProjectPlan.BambooProjectPlanTypes}: BUILD and PUBLISH.
 *
 * @author NickK
 */
public abstract class AbstractBambooProjectPlanProvider implements BambooProjectPlanProvider {

	@Override
	public List<BambooProjectPlan> getBambooProjectPlanList() {
		List<BambooProjectPlan> projectPlanList = new ArrayList<>();
		String projectName = getProjectName();

		BaseBambooProjectPlan buildPlan = new BaseBambooProjectPlan(projectName, BambooProjectPlan.BambooProjectPlanTypes.BUILD)
				.setArtifactType(getArtifactType())
				.setBranchPattern(getBranchPattern());
		if (!StringUtils.isEmpty(getProjectKey())) {
			buildPlan.setProjectKey(getProjectKey());
		}
		if (!StringUtils.isEmpty(getBuildPlanKey())) {
			buildPlan.setPlanKey(getBuildPlanKey());
		}
		projectPlanList.add(buildPlan);

		return projectPlanList;
	}


	protected abstract String getProjectName();


	/**
	 * Returns {@link BambooProjectPlan.BambooProjectArtifactTypes#NONE}.
	 */
	protected BambooProjectPlan.BambooProjectArtifactTypes getArtifactType() {
		return BambooProjectPlan.BambooProjectArtifactTypes.NONE;
	}


	/**
	 * Returns {@link BaseBambooProjectPlan#RELEASE_BRANCH_PATTERN}
	 */
	protected String getBranchPattern() {
		return BaseBambooProjectPlan.RELEASE_BRANCH_PATTERN;
	}


	/**
	 * Returns null to default the project key.
	 */
	protected String getProjectKey() {
		return null;
	}


	/**
	 * Returns null to default the build plan's key;
	 */
	protected String getBuildPlanKey() {
		return null;
	}
}
