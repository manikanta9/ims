package com.clifton.build.bamboo;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.permission.PlanPermissions;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.util.BambooServer;
import com.atlassian.bamboo.specs.util.SimpleUserPasswordCredentials;
import com.clifton.build.bamboo.plan.BambooProjectPlan;
import com.clifton.build.bamboo.util.BambooUtils;

import java.util.List;


/**
 * @author NickK
 */
@BambooSpec
public class BambooProjectBuildPlanPublisher {

	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("\nUsage: Invoke BambooProjectBuildPlanPublisher with the arguments bambooServerUrl, user, and password.\n" +
					"\tExample: java -cp ... BambooProjectBuildPlanPublisher https://bamboo.server.com userName userPassword");
			System.exit(0);
		}
		List<BambooProjectPlan> bambooProjectPlans = BambooUtils.getBambooProjectBuildPlanListFromClasspath();
		BambooServer bambooServer = new BambooServer(args[0], new SimpleUserPasswordCredentials(args[1], args[2]));
		bambooProjectPlans.forEach(buildPlan -> {
			try {
				Plan plan = buildPlan.getPlan();
				bambooServer.publish(plan);

				Permissions permissions = buildPlan.getPermissions();
				if (permissions == null) {
					permissions = BambooUtils.getDefaultPlanPermissions();
				}
				bambooServer.publish(new PlanPermissions(plan.getIdentifier()).permissions(permissions));
			}
			catch (Exception e) {
				System.out.println("Unable to publish plan: " + buildPlan.getName() + ", error: " + e.getMessage());
			}
		});
	}
}
