package com.clifton.build.bamboo.plan;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.clifton.build.bamboo.util.BambooUtils;

import java.util.List;


/**
 * <code>BambooProjectPlan</code> interface for creating a Bamboo build plan
 *
 * @author NickK
 */
@BambooSpec
public interface BambooProjectPlan {

	/**
	 * Artifact types that can be generated from a build of a project.
	 */
	public enum BambooProjectArtifactTypes {
		WAR, SERVICE, NONE
	}

	/**
	 * Build plan types.
	 */
	public enum BambooProjectPlanTypes {
		/**
		 * Type for building a project to compile source, run tests, and create an artifact
		 */
		BUILD("TRUNK", "Build"),
		/**
		 * Type for publishing Bamboo plans
		 */
		PUBLISH("PUBLISH", "Publish"),
		/**
		 * Type for running SonarQube inspections
		 */
		INSPECTION("INSPECTION", "Inspection"),
		/**
		 * Type for running Integration tests
		 */
		INTEGRATION("INTEGRATION", "Integration"),
		/**
		 * Type for running PIOS tests
		 */
		PIOS("PIOS", "PIOS");

		private final String planName;
		private final String stageName;


		BambooProjectPlanTypes(String planName, String stageName) {
			this.planName = planName;
			this.stageName = stageName;
		}


		public String getPlanName() {
			return this.planName;
		}


		public String getStageName() {
			return this.stageName;
		}
	}


	/**
	 * Return the name of the Bamboo plan.
	 */
	public String getName();


	/**
	 * Return the plan key for this plan on the Bamboo server.
	 * <p>
	 * A plan's name and key must be unique for a project.
	 */
	public String getPlanKey();


	public String getDescription();


	/**
	 * Return the name of the Bamboo project this plan belongs to.
	 */
	public String getProjectName();


	/**
	 * Return the {@link Project} for this plan.
	 */
	public Project getProject();


	public BambooProjectPlanTypes getPlanType();


	public BambooProjectArtifactTypes getArtifactType();


	/**
	 * Returns the build repository to use for the build plan. It is usually a linked repository configured in the Bamboo server.
	 */
	public String getRepository();


	/**
	 * Return true if the plan supports branching.
	 */
	public boolean isEnableBranching();


	/**
	 * Return a pattern to use when creating branch builds. If empty a build will be created for all branches.
	 */
	public String getBranchPattern();


	/**
	 * Return a list of arguments to pass to the build via command line arguments.
	 */
	public List<String> getCommandLineArgumentList();


	/**
	 * Return a list of Gradle tasks to run for this plan
	 */
	public List<String> getGradleTaskList();


	/**
	 * Return a {@link Plan} generated from the configured properties of this object to publish to a Bamboo server.
	 */
	public Plan getPlan();


	/**
	 * Return {@link Permissions} to use for this plan. If the result is null, default permissions will be used with {@link BambooUtils#getDefaultPlanPermissions()}
	 */
	public Permissions getPermissions();
}
