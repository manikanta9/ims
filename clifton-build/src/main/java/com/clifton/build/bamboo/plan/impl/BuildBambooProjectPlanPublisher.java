package com.clifton.build.bamboo.plan.impl;

import com.clifton.build.bamboo.plan.AbstractBambooProjectPlanProvider;
import com.clifton.build.bamboo.plan.BambooProjectPlan;
import com.clifton.build.bamboo.plan.BaseBambooProjectPlan;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author NickK
 */
public class BuildBambooProjectPlanPublisher extends AbstractBambooProjectPlanProvider {

	@Override
	protected String getProjectName() {
		return "clifton-build";
	}


	@Override
	public List<BambooProjectPlan> getBambooProjectPlanList() {
		List<BambooProjectPlan> projectPlanList = new ArrayList<>();
		String projectName = getProjectName();

		BaseBambooProjectPlan publishPlan = new BaseBambooProjectPlan(projectName, BambooProjectPlan.BambooProjectPlanTypes.PUBLISH);
		if (!StringUtils.isEmpty(getProjectKey())) {
			publishPlan.setProjectKey(getProjectKey());
		}
		projectPlanList.add(publishPlan);

		return projectPlanList;
	}
}
