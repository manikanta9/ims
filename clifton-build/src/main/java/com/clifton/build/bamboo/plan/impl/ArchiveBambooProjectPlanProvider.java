package com.clifton.build.bamboo.plan.impl;

import com.clifton.build.bamboo.plan.AbstractBambooProjectPlanProvider;
import com.clifton.build.bamboo.plan.BambooProjectPlan;


/**
 * @author NickK
 */
public class ArchiveBambooProjectPlanProvider extends AbstractBambooProjectPlanProvider {

	@Override
	protected String getProjectName() {
		return "app-clifton-archive";
	}


	@Override
	protected BambooProjectPlan.BambooProjectArtifactTypes getArtifactType() {
		return BambooProjectPlan.BambooProjectArtifactTypes.WAR;
	}
}
