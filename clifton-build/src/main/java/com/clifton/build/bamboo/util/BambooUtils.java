package com.clifton.build.bamboo.util;

import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.builders.trigger.Trigger;
import com.atlassian.bamboo.specs.api.model.task.AnyTaskProperties;
import com.atlassian.bamboo.specs.builders.task.CheckoutItem;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.builders.task.VcsCheckoutTask;
import com.atlassian.bamboo.specs.builders.trigger.BitbucketServerTrigger;
import com.atlassian.bamboo.specs.model.task.ScriptTaskProperties;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.specs.model.task.VcsCheckoutTaskProperties;
import com.atlassian.bamboo.specs.model.trigger.BitbucketServerTriggerProperties;
import com.atlassian.bamboo.specs.util.MapBuilder;
import com.clifton.build.bamboo.plan.BambooProjectPlan;
import com.clifton.build.bamboo.plan.BambooProjectPlanProvider;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <code>BambooUtils</code> contains utilities to assist with creating Bamboo build specs.
 *
 * @author NickK
 */
public class BambooUtils {

	private BambooUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns an {@link Artifact} for the type and name provided.
	 *
	 * @param type
	 * @param artifactName
	 */
	public static Artifact getArtifact(BambooProjectPlan.BambooProjectArtifactTypes type, String artifactName) {
		if (type == null) {
			return null;
		}
		switch (type) {
			case SERVICE:
				return getServiceArtifact(artifactName);
			case WAR:
				return getWarArtifact(artifactName);
			default:
				return null;
		}
	}


	/**
	 * Returns an {@link Artifact} for a web application with the name provided.
	 * <p>
	 * The artifact is defined as **&#47*.war from the build location: "<i>artifactName</i>/build/distributions"
	 */
	public static Artifact getWarArtifact(String artifactName) {
		return new Artifact()
				.name(artifactName)
				.copyPattern("**/*.war")
				.location(artifactName + "/build/distributions");
	}


	/**
	 * Returns an {@link Artifact} for a service application with the name provided.
	 * <p>
	 * The artifact is defined as **&#47*.zip from the build location: "<i>artifactName</i>/build/install"
	 */
	public static Artifact getServiceArtifact(String artifactName) {
		return new Artifact()
				.name(artifactName)
				.copyPattern("**/*.zip")
				.location(artifactName + "/build/install");
	}


	/**
	 * Returns a VCS checkout task for the default repository.
	 */
	public static Task<VcsCheckoutTask, VcsCheckoutTaskProperties> defaultSourceCodeCheckoutTask() {
		return new VcsCheckoutTask()
				.description("Source Code Checkout")
				.checkoutItems(new CheckoutItem().defaultRepository());
	}


	/**
	 * Returns a Windows PowerShell script task that updates the last modified time for all files in
	 * a project's build output for test files (TEST*.xml).
	 */
	public static Task<ScriptTask, ScriptTaskProperties> windowsTestResultFileTouchTask() {
		return new ScriptTask()
				.description("Update test result files modification time")
				.interpreterWindowsPowerShell()
				.inlineBody("ls -filter TEST*.xml -recurse | ForEach-Object {$_.LastWriteTime = Get-Date}");
	}


	/**
	 * Returns a Linux script task that updates the last modified time for all files in
	 * a project's build output for test files (TEST*.xml).
	 */
	public static Task<ScriptTask, ScriptTaskProperties> linuxTestResultFileTouchTask() {
		return new ScriptTask()
				.description("Update test result file modification time")
				.interpreterShell()
				.inlineBody("find . -type f -name 'TEST*.xml' -print | while read filename; do\n\ttouch -d \"$(date)\" \"$filename\"\ndone");
	}


	/**
	 * Returns a JUnit test result parser task with the pattern: **&#47test-results&#47*&#47*.xml
	 */
	public static Task<TestParserTask, TestParserTaskProperties> gradleJunitTestResultParserTask() {
		return TestParserTask.createJUnitParserTask()
				.description("Parse test results")
				.resultDirectories("**/test-results/*/*.xml") // default is **/test-results/*.xml
				.pickUpTestResultsCreatedOutsideOfThisBuild(true);
	}


	/**
	 * Returns an task to execute a Gradle task using the provided command line arguments and gradle tasks.
	 */
	public static Task<AnyTask, AnyTaskProperties> newGradleTask(List<String> commandLineArgumentList, List<String> gradleTaskList) {
		String commandLine = CollectionUtils.getStream(commandLineArgumentList)
				.filter(arg -> !StringUtils.isEmpty(arg))
				.distinct()
				.collect(Collectors.joining(" "));
		String gradleTask = CollectionUtils.getStream(gradleTaskList)
				.filter(arg -> !StringUtils.isEmpty(arg))
				.distinct()
				.collect(Collectors.joining(" "));

		return new AnyTask(new AtlassianModule("org.jfrog.bamboo.bamboo-artifactory-plugin:artifactoryGradleTask"))
				.description("Gradle Build")
				.configuration(new MapBuilder<String, String>()
						// required properties
						.put("builder.artifactoryGradleBuilder.switches", commandLine)
						.put("builder.artifactoryGradleBuilder.tasks", gradleTask)
						.put("builder.artifactoryGradleBuilder.buildJdk", "JDK")
						.put("builder.artifactoryGradleBuilder.executable", "Gradle")

						// Not sure if any of the following are necessary, they do not appear to be

//						.put("builder.artifactoryGradleBuilder.disableAutoLicenseDiscovery", "false")
//						.put("builder.artifactoryGradleBuilder.filterExcludedArtifactsFromBuild", "false")
//						.put("builder.artifactoryGradleBuilder.includePublishedArtifacts", "false")
//						.put("builder.artifactoryGradleBuilder.artifactoryServerId", "-1")
//						.put("builder.artifactoryGradleBuilder.gitReleaseBranch", "")
//						.put("builder.artifactoryGradleBuilder.publishBuildInfo", "")
//						.put("builder.artifactoryGradleBuilder.artifactPattern", "")
//						.put("builder.artifactoryGradleBuilder.deployerUsername", "")
//						.put("builder.artifactoryGradleBuilder.artifactSpecs", "")
//						.put("builder.artifactoryGradleBuilder.environmentVariables", "")
//						.put("builder.artifactoryGradleBuilder.resolutionRepo", "")
//						.put("builder.artifactoryGradleBuilder.releaseProps", "")
//						.put("builder.artifactoryGradleBuilder.includeEnvVars", "")
//						.put("builder.artifactoryGradleBuilder.nextIntegProps", "")
//						.put("builder.artifactoryGradleBuilder.testChecked", "")
//						.put("builder.artifactoryGradleBuilder.alternativeTasks", "")
//						.put("builder.artifactoryGradleBuilder.testDirectoryOption", "")
//						.put("builder.artifactoryGradleBuilder.useGradleWrapper", "")
//						.put("builder.artifactoryGradleBuilder.runLicenseChecks", "")
//						.put("builder.artifactoryGradleBuilder.publishExcludePatterns", "")
//						.put("builder.artifactoryGradleBuilder.publishIvyDescriptors", "")
//						.put("builder.artifactoryGradleBuilder.enableReleaseManagement", "")
//						.put("builder.artifactoryGradleBuilder.vcsTagBase", "")
//						.put("builder.artifactoryGradleBuilder.publishIncludePatterns", "")
//						.put("builder.artifactoryGradleBuilder.deployerPassword", "")
//						.put("builder.artifactoryGradleBuilder.gradleWrapperLocation", "")
//						.put("builder.artifactoryGradleBuilder.publishMavenDescriptors", "")
//						.put("builder.artifactoryGradleBuilder.limitChecksToScopes", "")
//						.put("builder.artifactoryGradleBuilder.useM2CompatiblePatterns", "")
//						.put("builder.artifactoryGradleBuilder.buildScript", "")
//						.put("builder.artifactoryGradleBuilder.useArtifactoryGradlePlugin", "")
//						.put("builder.artifactoryGradleBuilder.ivyPattern", "")
//						.put("builder.artifactoryGradleBuilder.buildFile", "")
//						.put("builder.artifactoryGradleBuilder.publishArtifacts", "")
//						.put("builder.artifactoryGradleBuilder.testResultsDirectory", "")
//						.put("builder.artifactoryGradleBuilder.licenseViolationRecipients", "")
//						.put("builder.artifactoryGradleBuilder.publishingRepo", "")

//						.put("runLicenseChecks", "false")
//						.put("envVarsIncludePatterns", "")
//						.put("plan.storageTag", "AP-TRUN")
//						.put("envVarsExcludePatterns", "*password*,*secret*")
//						.put("publishArtifacts", "false")
//						.put("publishBuildInfo", "true")
//						.put("testChecked", "")
//						.put("useM2CompatiblePatterns", "true")
//						.put("testDirectoryOption", "")
//						.put("includeEnvVars", "")
//						.put("baseUrl", "http://bamboo.paraport.com")
//						.put("enableReleaseManagement", "false")

//						.put("artifactory.common.blackduck.appVersion", "")
//						.put("artifactory.common.blackduck.appName", "")
//						.put("artifactory.common.blackduck.autoDiscardStaleComponentRequests", "")
//						.put("artifactory.common.blackduck.autoCreateMissingComponentRequests", "")
//						.put("artifactory.common.blackduck.includePublishedArtifacts", "")
//						.put("artifactory.common.blackduck.reportRecipients", "")
//						.put("artifactory.common.blackduck.runChecks", "")
//						.put("artifactory.common.blackduck.scopes", "")
//						.put("artifactory.vcs.type", "GIT")
//						.put("artifactory.vcs.git.password", "")
//						.put("artifactory.vcs.git.ssh.key", "")
//						.put("artifactory.vcs.git.username", "")
//						.put("artifactory.vcs.git.url", "")
//						.put("artifactory.vcs.git.authenticationType", "NONE")
//						.put("artifactory.vcs.git.ssh.passphrase", "")
//						.put("artifactory.vcs.p4.client", "")
//						.put("artifactory.vcs.p4.port", "")
//						.put("artifactory.vcs.p4.depot", "")
//						.put("artifactory.vcs.p4.password", "")
//						.put("artifactory.vcs.p4.username", "")

//						.put("bintrayConfiguration", "")
//						.put("bintray.licenses", "")
//						.put("bintray.signMethod", "false")
//						.put("bintray.subject", "")
//						.put("bintray.repository", "")
//						.put("bintray.mavenSync", "")
//						.put("bintray.vcsUrl", "")
//						.put("bintray.gpgPassphrase", "")
//						.put("bintray.packageName", "")

//						.put("planRepository.branch", "master")
//						.put("planRepository.branchDisplayName", "master")
//						.put("planRepository.branchName", "master")
//						.put("planRepository.name", "Build")
//						.put("planRepository.previousRevision", "29d73902814664dce2f5d1bf3ea070aa8b3d9e5e")
//						.put("planRepository.repositoryUrl", "ssh://git@stash:7999/ims/ims.git")
//						.put("planRepository.revision", "5ec334c121686d3df3c274e3b47de0dabf6eb36d")
// 						.put("planRepository.type", "bbserver")
//						.put("planRepository.username", "")
//						.put("planRepository.1.branch", "master")
//						.put("planRepository.1.branchDisplayName", "master")
//						.put("planRepository.1.branchName", "master")
//						.put("planRepository.1.name", "Build")
//						.put("planRepository.1.branchName", "master")
//						.put("planRepository.1.previousRevision", "29d73902814664dce2f5d1bf3ea070aa8b3d9e5e")
//						.put("planRepository.1.repositoryUrl", "ssh://git@stash:7999/ims/ims.git")
//						.put("planRepository.1.revision", "5ec334c121686d3df3c274e3b47de0dabf6eb36d")
//						.put("planRepository.1.type", "bbserver")
//						.put("planRepository.1.username", "")

//						.put("repository.branch.name", "master")
//						.put("repository.git.branch", "master")
//						.put("repository.git.repositoryUrl", "ssh://git@stash:7999/ims/ims.git")
//						.put("repository.git.username", "")
//						.put("repository.name", "Build")
//						.put("repository.revision.number", "5ec334c121686d3df3c274e3b47de0dabf6eb36d")
//						.put("repository.previous.revision.number", "29d73902814664dce2f5d1bf3ea070aa8b3d9e5e")
//						.put("repository.88375301.git.branch", "master")
//						.put("repository.88375301.git.username", "")
//						.put("repository.88375301.git.repositoryUrl", "ssh://git@stash:7999/ims/ims.git")
//						.put("repository.88375301.branch.name", "master")
//						.put("repository.88375301.name", "Build")
//						.put("repository.88375301.revision.number", "5ec334c121686d3df3c274e3b47de0dabf6eb36d")
//						.put("repository.88375301.previous.revision.number", "29d73902814664dce2f5d1bf3ea070aa8b3d9e5e")

						.build());
	}


	/**
	 * Returns a trigger for execution upon VCS commit.
	 */
	public static Trigger<BitbucketServerTrigger, BitbucketServerTriggerProperties> getDefaultBranchCommitTrigger() {
		// empty repositories list includes all repositories of the plan
		return new BitbucketServerTrigger().name("BitBucket Server repository triggered").description("Commit Change");
	}


	/**
	 * Returns a branch management plan that:
	 * <br/>- creates based on evert VCS branch
	 * <br/>- cleans up after 1 day after removal or 45 days of inactivity
	 * <br/>- notifies committers of build activity
	 */
	public static PlanBranchManagement getPlanBranchManagementForEveryBranch() {
		return new PlanBranchManagement()
				.createForVcsBranch()
				.delete(new BranchCleanup()
						.whenRemovedFromRepositoryAfterDays(1)
						.whenInactiveInRepositoryAfterDays(45))
				.notificationForCommitters();
	}


	/**
	 * Returns a branch management plan that:
	 * <br/>- creates based on VCS branches matching the provided pattern
	 * <br/>- cleans up after 1 day after removal or 45 days of inactivity
	 * <br/>- notifies committers of build activity
	 */
	public static PlanBranchManagement getPlanBranchManagementForBranchesMatchingPattern(String branchPattern) {
		return new PlanBranchManagement()
				.createForVcsBranchMatching(branchPattern)
				.delete(new BranchCleanup()
						.whenRemovedFromRepositoryAfterDays(1)
						.whenInactiveInRepositoryAfterDays(45))
				.notificationForCommitters();
	}


	/**
	 * Returns Permissions with:
	 * <br/>- admin = permissionTypes(ADMIN, CLONE, EDIT)
	 * <br/>- bamboo-admin = permissionTypes(ADMIN)
	 * <br/>- logged in user = permissionTypes(VIEW)
	 * <br/>- anonymous user = permissionTypes()
	 */
	public static Permissions getDefaultPlanPermissions() {
		return new Permissions()
				.groupPermissions("admin", PermissionType.ADMIN, PermissionType.CLONE, PermissionType.EDIT)
				.groupPermissions("bamboo-admin", PermissionType.ADMIN)
				.loggedInUserPermissions(PermissionType.VIEW)
				.anonymousUserPermissionView();
	}


	/**
	 * Returns a list of {@link BambooProjectPlan} in the invoking thread's classpath.
	 * Throws a runtime exception with a list of errors if any failures occur.
	 */
	public static List<BambooProjectPlan> getBambooProjectBuildPlanListFromClasspath() {
		List<BambooProjectPlan> buildPlanList = new ArrayList<>();
		StringBuilder errors = new StringBuilder();
		Set<Class<? extends BambooProjectPlanProvider>> projectBuildPlans = new Reflections("com.clifton", new SubTypesScanner(true))
				.getSubTypesOf(BambooProjectPlanProvider.class);
		CollectionUtils.getStream(projectBuildPlans)
				.filter(providerClass -> !providerClass.isInterface() && !Modifier.isAbstract(providerClass.getModifiers()))
				.forEach(providerClass -> {
					try {
						BambooProjectPlanProvider planProvider = providerClass.getConstructor().newInstance();
						buildPlanList.addAll(planProvider.getBambooProjectPlanList());
					}
					catch (Exception e) {
						errors.append("Unable to instantiate plan provider: ").append(providerClass).append(", error: ").append(e.getMessage()).append('\n');
					}
				});
		if (errors.length() > 0) {
			throw new RuntimeException(errors.toString());
		}
		return buildPlanList;
	}
}
