package com.clifton.build.bamboo.plan.impl;

import com.clifton.build.bamboo.plan.AbstractBambooProjectPlanProvider;
import com.clifton.build.bamboo.plan.BambooProjectPlan;


/**
 * @author NickK
 */
public class IntegrationBambooProjectPlanProvider extends AbstractBambooProjectPlanProvider {

	@Override
	protected String getProjectName() {
		return "service-clifton-integration";
	}


	@Override
	protected BambooProjectPlan.BambooProjectArtifactTypes getArtifactType() {
		return BambooProjectPlan.BambooProjectArtifactTypes.SERVICE;
	}
}
