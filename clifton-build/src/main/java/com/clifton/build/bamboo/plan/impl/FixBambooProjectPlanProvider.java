package com.clifton.build.bamboo.plan.impl;

import com.clifton.build.bamboo.plan.AbstractBambooProjectPlanProvider;
import com.clifton.build.bamboo.plan.BambooProjectPlan;


/**
 * @author NickK
 */
public class FixBambooProjectPlanProvider extends AbstractBambooProjectPlanProvider {

	@Override
	protected String getProjectName() {
		return "service-clifton-fix";
	}


	@Override
	protected BambooProjectPlan.BambooProjectArtifactTypes getArtifactType() {
		return BambooProjectPlan.BambooProjectArtifactTypes.SERVICE;
	}
}
