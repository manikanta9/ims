package com.clifton.build.bamboo.plan;

import java.util.List;


/**
 * <code>BambooProjectPlanProvider</code> interface for IMS projects to implement to provide a list of plans
 * tha can be published to a Bamboo server.
 *
 * @author NickK
 */
public interface BambooProjectPlanProvider {

	public List<BambooProjectPlan> getBambooProjectPlanList();
}
