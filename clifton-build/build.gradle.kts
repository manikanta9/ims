tasks.register("publishBambooPlan") {
	dependsOn(sourceSets.main.map { it.runtimeClasspath })
	val bambooServer = System.getProperty("bamboo.server")
	val bambooUser = System.getProperty("bamboo.user")
	val bambooPassword = System.getProperty("bamboo.password")

	onlyIf { bambooServer != null && bambooUser != null && bambooPassword != null }

	doLast {
		javaexec {
			main = "com.clifton.build.bamboo.BambooProjectBuildPlanPublisher"
			environment("CLASSPATH", sourceSets.main.get().runtimeClasspath.files.joinToString(File.pathSeparator) { it.path })
			args(bambooServer, bambooUser, bambooPassword)
		}
	}
}

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Bamboo build plans
	api("com.atlassian.bamboo:bamboo-specs:6.2.3")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	/////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
}
