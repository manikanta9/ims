package com.clifton.rule.tests;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.JsonStrategy;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.BeanFieldValueRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.MockRuleEvaluatorContext;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Testing class for unit tests of ruleEvaluators
 *
 * @author jonathanr
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class RuleEvaluatorTests {

	private static final BeanFieldValueRuleEvaluator.BeanFieldJsonDto[] BOTH_FIELDS_EQUAL = {new BeanFieldValueRuleEvaluator.BeanFieldJsonDto("ignorable", "Ignorable", "true"), new BeanFieldValueRuleEvaluator.BeanFieldJsonDto("ruleCategory.name", "Rule Category Name", "Test Category")};
	private static final BeanFieldValueRuleEvaluator.BeanFieldJsonDto[] ONE_FIELD_EQUAL = {new BeanFieldValueRuleEvaluator.BeanFieldJsonDto("ignorable", "Ignorable", "false"), new BeanFieldValueRuleEvaluator.BeanFieldJsonDto("ruleCategory.name", "Rule Category Name", "Test Category")};
	private static final BeanFieldValueRuleEvaluator.BeanFieldJsonDto[] BOTH_FIELDS_NOT_EQUAL = {new BeanFieldValueRuleEvaluator.BeanFieldJsonDto("ignorable", "Ignorable", "false"), new BeanFieldValueRuleEvaluator.BeanFieldJsonDto("ruleCategory.name", "Rule Category Name", null)};
	@Resource
	public JsonHandler<JsonStrategy> jsonHandler;
	@Resource
	private RuleViolationService ruleViolationService;
	@Resource
	private RuleDefinitionService ruleDefinitionService;
	@Resource
	private RuleEvaluatorService ruleEvaluatorService;


	@Test
	public void BeanFieldValueRuleEvaluatorTestViolationIfAllFieldsAreNotEqualTExpectedValuesTest() {
		//If all fields do not equal expected values generate rule violation
		BeanFieldValueRuleEvaluator evaluator = buildBeanFieldValueRuleEvaluator();
		RuleDefinition testBean = this.ruleDefinitionService.getRuleDefinition((short) 1);
		RuleConfig config = buildRuleConfig(this.ruleDefinitionService.getRuleAssignment(1));
		List<RuleViolation> violationList;
		evaluator.setAll(true);
		evaluator.setNotEqual(true);
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_EQUAL));
		// all fields equal expected  values no violations expected.
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertTrue(violationList.isEmpty(), "all fields equal to expected values no violations should be generated");
		// one of the fields does not equal expected value no violation expected
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(ONE_FIELD_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertTrue(violationList.isEmpty(), "one field not equal, no violation expected");
		//both fields are not equal to expected values violations
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_NOT_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertFalse(violationList.isEmpty(), "both fields not equal, expect violation");
	}


	@Test
	public void BeanFieldValueRuleEvaluatorTestViolationIfAnyFieldsAreNotEqualToExpectedValuesTest() {
		BeanFieldValueRuleEvaluator evaluator = buildBeanFieldValueRuleEvaluator();
		RuleDefinition testBean = this.ruleDefinitionService.getRuleDefinition((short) 1);
		RuleConfig config = buildRuleConfig(this.ruleDefinitionService.getRuleAssignment(1));
		List<RuleViolation> violationList;
		//Generate violation if any fields are not equal to expected values
		evaluator.setAll(false);
		evaluator.setNotEqual(true);
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_EQUAL));
		// all fields are equal to expected values
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertTrue(violationList.isEmpty(), "all fields equal expected values no violations should be generated");
		// one of the fields does not equal expected value
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(ONE_FIELD_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertFalse(violationList.isEmpty(), "one field not equal expect violation");
		violationList.clear();
		//both fields are not equal to expected values violations
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_NOT_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertFalse(violationList.isEmpty(), "both fields not equal expect violation");
	}


	@Test
	public void BeanFieldValueRuleEvaluatorTestViolationIfAnyFieldsAreEqualToExpectedValuesTest() {
		BeanFieldValueRuleEvaluator evaluator = buildBeanFieldValueRuleEvaluator();
		RuleDefinition testBean = this.ruleDefinitionService.getRuleDefinition((short) 1);
		RuleConfig config = buildRuleConfig(this.ruleDefinitionService.getRuleAssignment(1));
		List<RuleViolation> violationList;
		//Generate violation if any fields are equal to expected values
		evaluator.setAll(false);
		evaluator.setNotEqual(false);
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_EQUAL));
		// all fields are equal to expected values
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertFalse(violationList.isEmpty(), "all fields equal expected values Violation Should be generated");
		violationList.clear();
		// one of the fields does equal expected value
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(ONE_FIELD_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertFalse(violationList.isEmpty(), "one field equal expect violation");
		violationList.clear();
		//both fields are not equal to expected values violations
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_NOT_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertTrue(violationList.isEmpty(), "both field not equal no violation expected.");
	}


	@Test
	public void BeanFieldValueRuleEvaluatorTestViolationIfAllFieldsAreEqualToExpectedValuesTest() {
		BeanFieldValueRuleEvaluator evaluator = buildBeanFieldValueRuleEvaluator();
		RuleDefinition testBean = this.ruleDefinitionService.getRuleDefinition((short) 1);
		RuleConfig config = buildRuleConfig(this.ruleDefinitionService.getRuleAssignment(1));
		List<RuleViolation> violationList;
		//Generate violation if all fields are equal to expected values
		evaluator.setAll(true);
		evaluator.setNotEqual(false);
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_EQUAL));
		// all fields are equal to expected values
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertFalse(violationList.isEmpty(), "all fields equal expected values Violation Should be generated");
		violationList.clear();
		// one of the fields does equal expected value
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(ONE_FIELD_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertTrue(violationList.isEmpty(), "one field equal expect no violation");
		//both fields are not equal to expected values violations
		evaluator.setBeanFieldsJson(this.jsonHandler.toJson(BOTH_FIELDS_NOT_EQUAL));
		violationList = evaluator.evaluateRule(testBean, config, new MockRuleEvaluatorContext());
		Assertions.assertTrue(violationList.isEmpty(), "both field not equal no violation expected.");
	}


	@Test
	public void testContextFiltering() {
		List<RuleViolation> violations = this.ruleEvaluatorService.executeRules("Test Category 2", (long) 5, false);
		Assertions.assertEquals(3, violations.size(), "Did not receive expected number of violations.");
		// Group violations by context
		Map<String, List<RuleViolation>> violationContextToNoteMap = BeanUtils.getBeansMap(violations,
				violation -> {
					RuleDefinition def = violation.getRuleAssignment().getRuleDefinition();
					RuleCategory category = def.getRuleCategory();
					if (def.getRuleScope() == null) {
						return category.getRuleEvaluatorContextBean().getType().getClassName();
					}
					return def.getRuleScope().getRuleEvaluatorContextBean() == null
							? category.getRuleEvaluatorContextBean().getType().getClassName()
							: def.getRuleScope().getRuleEvaluatorContextBean().getType().getClassName();
				});
		// global and scope overridden context
		Assertions.assertEquals(2, violationContextToNoteMap.size());
		List<RuleViolation> scopedViolations = violationContextToNoteMap.get("com.clifton.rule.evaluator.context.ExtendedTestRuleEvaluatorContext");
		Assertions.assertEquals(1, scopedViolations.size());
		Assertions.assertEquals(scopedViolations.get(0).getViolationNote(), "Rule was executed for {id=5,label=Rule Entity Scope Applies} with context class com.clifton.rule.evaluator.context.ExtendedTestRuleEvaluatorContext");
		// global context has two items: global and one scoped (inheriting global context)
		scopedViolations = violationContextToNoteMap.get("com.clifton.rule.evaluator.context.BaseTestRuleEvaluatorContext");
		Assertions.assertEquals(2, scopedViolations.size());
		Map<String, String> globalViolationsMap = scopedViolations.stream()
				.collect(Collectors.toMap(
						violation -> violation.getRuleAssignment().getRuleDefinition().getRuleScope() == null ? "GLOBAL"
								: violation.getRuleAssignment().getRuleDefinition().getRuleScope().getName(),
						RuleViolation::getViolationNote
				));
		Assertions.assertEquals(2, globalViolationsMap.size());
		Assertions.assertEquals(globalViolationsMap.get("GLOBAL"), "Rule was executed for {id=5,label=Rule Entity Scope Applies} with context class com.clifton.rule.evaluator.context.BaseTestRuleEvaluatorContext");
		Assertions.assertEquals(globalViolationsMap.get("Test Scope No Context Override"), "Rule was executed for {id=5,label=Rule Entity Scope Applies} with context class com.clifton.rule.evaluator.context.BaseTestRuleEvaluatorContext");
		violations = this.ruleEvaluatorService.executeRules("Test Category 2", (long) 6, false);

		ValidationUtils.assertEquals(1, violations.size(), "Did not receive expected number of violations.");
		ValidationUtils.assertEquals(CollectionUtils.getOnlyElement(violations).getViolationNote(), "Rule was executed for {id=6,label=Rule Entity Scope Does Not Apply} with context class com.clifton.rule.evaluator.context.BaseTestRuleEvaluatorContext", "Did not receive expected rule violation.");
	}


	////////////////////////////////////////////////////////////////////////////
	//////////               builder methods                          //////////
	////////////////////////////////////////////////////////////////////////////
	private BeanFieldValueRuleEvaluator buildBeanFieldValueRuleEvaluator() {
		BeanFieldValueRuleEvaluator evaluator = new BeanFieldValueRuleEvaluator();
		evaluator.setJsonHandler(this.jsonHandler);
		evaluator.setRuleDefinitionService(this.ruleDefinitionService);
		evaluator.setRuleViolationService(this.ruleViolationService);
		return evaluator;
	}


	private RuleConfig buildRuleConfig(RuleAssignment assignment) {

		RuleConfig config = new RuleConfig(assignment);
		EntityConfig entityConfig = new EntityConfig(assignment);
		config.addEntityConfig(entityConfig);
		return config;
	}
}
