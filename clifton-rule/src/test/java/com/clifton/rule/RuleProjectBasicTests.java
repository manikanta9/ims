package com.clifton.rule;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class RuleProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "rule";
	}


	@Override
	protected void configureApprovedClassImports(@SuppressWarnings("unused") List<String> imports) {
		imports.add("java.sql.Types");
	}
}
