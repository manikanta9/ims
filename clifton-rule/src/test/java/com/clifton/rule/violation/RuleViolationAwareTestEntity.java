package com.clifton.rule.violation;

import com.clifton.core.beans.NamedEntity;
import com.clifton.rule.violation.status.RuleViolationStatus;


/**
 * @author manderson
 */
public class RuleViolationAwareTestEntity extends NamedEntity<Integer> implements RuleViolationAware, RuleViolationCodeAware {

	private RuleViolationStatus violationStatus;

	private RuleViolationAwareTestEntityCategory category;

	private String ruleViolationCodes;


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	public RuleViolationAwareTestEntityCategory getCategory() {
		return this.category;
	}


	public void setCategory(RuleViolationAwareTestEntityCategory category) {
		this.category = category;
	}


	@Override
	public String getRuleViolationCodes() {
		return this.ruleViolationCodes;
	}


	@Override
	public void setRuleViolationCodes(String ruleViolationCodes) {
		this.ruleViolationCodes = ruleViolationCodes;
	}
}
