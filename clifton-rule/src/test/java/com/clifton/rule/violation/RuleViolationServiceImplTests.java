package com.clifton.rule.violation;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class RuleViolationServiceImplTests {

	private static final Integer TEST_BEAN_NO_VIOLATIONS = 1;
	private static final Integer TEST_BEAN_MANUAL_VIOLATIONS = 2;
	private static final Integer TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS = 3;
	private static final Integer TEST_BEAN_WITH_CODE = 4;
	private static final Integer TEST_BEAN_WITH_NO_CODE = 5;

	private static final Short RULE_DEFINITION_INACTIVE = 2;
	private static final Short RULE_DEFINITION_PRE_PROCESS = 4;
	private static final Short RULE_DEFINITION_REGULAR_IGNORABLE = 5;
	private static final Short RULE_DEFINITION_REGULAR_NOT_IGNORABLE = 6;
	private static final Short RULE_DEFINITION_WITH_CODE = 7;
	private static final Short RULE_DEFINITION_WITH_NO_CODE = 8;


	@Resource
	private RuleDefinitionService ruleDefinitionService;

	@Resource
	private RuleEvaluatorService ruleEvaluatorService;

	@Resource
	private RuleViolationService ruleViolationService;

	@Resource
	private UpdatableDAO<RuleViolationAwareTestEntity> ruleViolationAwareTestEntityDAO;

	@Resource
	private XmlReadOnlyDAO<RuleAssignment> ruleAssignmentDAO;


	@Resource
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNoViolationsGenerated() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				return getMockRuleEvaluator(null);
			}
			return getMockRuleEvaluatorContext();
		});

		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, null, 0, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS);

		List<RuleViolation> list = runAndValidateRuleViolations(TEST_BEAN_MANUAL_VIOLATIONS, null, 1, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_IGNORED);
		ValidationUtils.assertTrue(list.get(0).getRuleAssignment().getRuleDefinition().isManual(), "Expected one existing violation to be manual.");
		ValidationUtils.assertTrue(list.get(0).isIgnored(), "Expected one existing violation to be ignored.");

		list = runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, null, 1, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		ValidationUtils.assertTrue(list.get(0).getRuleAssignment().getRuleDefinition().isSystemDefined(), "Expected one existing violation to be system defined.");
		ValidationUtils.assertFalse(list.get(0).isIgnored(), "Expected one existing violation to NOT be ignored.");
	}


	@Test
	public void testPreProcessViolationsGenerated() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				Map<Short, Integer> ruleDefinitionCountMap = new HashMap<>();
				ruleDefinitionCountMap.put(RULE_DEFINITION_PRE_PROCESS, 2);
				return getMockRuleEvaluator(ruleDefinitionCountMap);
			}
			return getMockRuleEvaluatorContext();
		});

		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, true, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, false, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);

		runAndValidateRuleViolations(TEST_BEAN_MANUAL_VIOLATIONS, true, 3, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		runAndValidateRuleViolations(TEST_BEAN_MANUAL_VIOLATIONS, false, 3, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);

		runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, true, 3, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		Assertions.assertDoesNotThrow(() -> runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, false, 3, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS));
	}


	@Test
	public void testPostProcessViolationsGenerated() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				Map<Short, Integer> ruleDefinitionCountMap = new HashMap<>();
				ruleDefinitionCountMap.put(RULE_DEFINITION_REGULAR_NOT_IGNORABLE, 1);
				return getMockRuleEvaluator(ruleDefinitionCountMap);
			}
			return getMockRuleEvaluatorContext();
		});

		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, true, 0, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS);
		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, false, 1, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED);

		runAndValidateRuleViolations(TEST_BEAN_MANUAL_VIOLATIONS, true, 1, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_IGNORED);
		runAndValidateRuleViolations(TEST_BEAN_MANUAL_VIOLATIONS, false, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED);

		runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, true, 1, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		Assertions.assertDoesNotThrow(() -> runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, false, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED));
	}


	@Test
	public void testPreAndPostWarningsGenerated() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				Map<Short, Integer> ruleDefinitionCountMap = new HashMap<>();
				ruleDefinitionCountMap.put(RULE_DEFINITION_INACTIVE, 5); // SHOULDN'T COUNT BECAUSE INACTIVE
				ruleDefinitionCountMap.put(RULE_DEFINITION_PRE_PROCESS, 2);
				ruleDefinitionCountMap.put(RULE_DEFINITION_REGULAR_IGNORABLE, 1);
				return getMockRuleEvaluator(ruleDefinitionCountMap);
			}
			return getMockRuleEvaluatorContext();
		});

		// Run Separately, Run Together
		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, true, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, false, 3, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);

		runAndValidateRuleViolations(TEST_BEAN_MANUAL_VIOLATIONS, null, 4, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);

		runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, true, 3, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		Assertions.assertDoesNotThrow(() -> runAndValidateRuleViolations(TEST_BEAN_SYSTEM_DEFINED_VIOLATIONS, false, 4, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS));
	}


	@Test
	public void testIgnoreWarnings_StatusChanges() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				Map<Short, Integer> ruleDefinitionCountMap = new HashMap<>();
				ruleDefinitionCountMap.put(RULE_DEFINITION_INACTIVE, 5); // SHOULDN'T COUNT BECAUSE INACTIVE
				ruleDefinitionCountMap.put(RULE_DEFINITION_PRE_PROCESS, 2);
				return getMockRuleEvaluator(ruleDefinitionCountMap);
			}
			return getMockRuleEvaluatorContext();
		});

		List<RuleViolation> list = runAndValidateRuleViolations(TEST_BEAN_NO_VIOLATIONS, null, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS);
		// Ignore the violations
		for (RuleViolation violation : list) {
			violation.setIgnored(true);
			violation.setIgnoreNote("test ignore");
			this.ruleViolationService.saveRuleViolation(violation);
		}

		// Should Change Status
		validateViolationCountAndStatus(TEST_BEAN_NO_VIOLATIONS, 2, RuleViolationStatus.RuleViolationStatusNames.PROCESSED_IGNORED, false);
	}


	@Test
	public void testViolationWithNoCodeGenerated() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				Map<Short, Integer> ruleDefinitionCountMap = new HashMap<>();
				ruleDefinitionCountMap.put(RULE_DEFINITION_WITH_NO_CODE, 1);
				return getMockRuleEvaluator(ruleDefinitionCountMap);
			}
			return getMockRuleEvaluatorContext();
		});
		runAndValidateRuleViolationsWithCode(TEST_BEAN_WITH_NO_CODE, false, null);
	}


	@Test
	public void testViolationWithCodeGenerated() {
		this.ruleAssignmentDAO.setLogicalEvaluatesToTrue(true);
		Mockito.when(this.systemBeanService.getBeanInstance(ArgumentMatchers.nullable(SystemBean.class))).thenAnswer(invocation -> {
			SystemBean bean = invocation.getArgument(0);
			if (bean == null || MathUtils.isEqual(1, bean.getId())) {
				Map<Short, Integer> ruleDefinitionCountMap = new HashMap<>();
				ruleDefinitionCountMap.put(RULE_DEFINITION_WITH_CODE, 1);
				return getMockRuleEvaluator(ruleDefinitionCountMap);
			}
			return getMockRuleEvaluatorContext();
		});

		runAndValidateRuleViolationsWithCode(TEST_BEAN_WITH_CODE, false, "T");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<RuleViolation> runAndValidateRuleViolations(int id, Boolean preProcess, int expectedCount, RuleViolationStatus.RuleViolationStatusNames expectedStatus) {
		this.ruleEvaluatorService.executeRules("Test Category", MathUtils.getNumberAsLong(id), preProcess);
		return validateViolationCountAndStatus(id, expectedCount, expectedStatus, true);
	}


	private List<RuleViolation> validateViolationCountAndStatus(int id, int count, RuleViolationStatus.RuleViolationStatusNames expectedStatus, boolean triggerSave) {
		RuleViolationAwareTestEntity bean = this.ruleViolationAwareTestEntityDAO.findByPrimaryKey(id);
		// Trigger a save on the bean so that warning status is updated
		if (triggerSave) {
			this.ruleViolationAwareTestEntityDAO.save(bean);
		}
		List<RuleViolation> list = this.ruleViolationService.getRuleViolationListByLinkedEntity("RuleViolationAwareTestEntity", id);
		ValidationUtils.assertEquals(count, CollectionUtils.getSize(list), "Expected bean to have " + count + " violations, but has " + CollectionUtils.getSize(list) + ".");
		ValidationUtils.assertEquals(expectedStatus, bean.getViolationStatus().getRuleViolationStatusName(), "Expected bean to have status " + expectedStatus + ", but was " + bean.getViolationStatus().getRuleViolationStatusName());
		return list;
	}


	private String runAndValidateRuleViolationsWithCode(int id, Boolean preProcess, String expectedCode) {
		this.ruleEvaluatorService.executeRules("Test Category", MathUtils.getNumberAsLong(id), preProcess);
		return validateViolationWithCode(id, expectedCode, true);
	}


	private String validateViolationWithCode(int id, String expectedCode, boolean triggerSave) {
		RuleViolationAwareTestEntity bean = this.ruleViolationAwareTestEntityDAO.findByPrimaryKey(id);
		// Trigger a save on the bean so that warning status is updated
		if (triggerSave) {
			this.ruleViolationAwareTestEntityDAO.save(bean);
		}
		String violationCode = this.ruleViolationService.getRuleViolationCodeByLinkedEntity("RuleViolationAwareTestEntity", id);
		ValidationUtils.assertEquals(expectedCode, bean.getRuleViolationCodes(), "Expected bean to have code " + expectedCode + ", but was " + bean.getRuleViolationCodes() + "code");
		return violationCode;
	}


	@SuppressWarnings("rawtypes")
	private MockRuleEvaluator getMockRuleEvaluator(Map<Short, Integer> warningTypeCountMap) {
		MockRuleEvaluator evaluator = new MockRuleEvaluator();
		// Note - not using application context so not wiring this bean
		evaluator.setRuleDefinitionCountMap(warningTypeCountMap);
		evaluator.setRuleDefinitionService(this.ruleDefinitionService);
		return evaluator;
	}


	private MockRuleEvaluatorContext getMockRuleEvaluatorContext() {
		return new MockRuleEvaluatorContext();
	}
}
