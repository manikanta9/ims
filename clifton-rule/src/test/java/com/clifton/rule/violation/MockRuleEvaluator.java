package com.clifton.rule.violation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.evaluator.RuleEvaluatorContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public class MockRuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> implements RuleEvaluator<T, K> {

	private Map<Short, Integer> ruleDefinitionCountMap;

	private RuleDefinitionService ruleDefinitionService;


	@Override
	public List<RuleViolation> evaluateRule(T ruleBean, RuleConfig ruleConfig, K context) {
		List<RuleViolation> list = new ArrayList<>();
		if (!CollectionUtils.isEmpty(this.ruleDefinitionCountMap)) {
			Integer count = this.ruleDefinitionCountMap.get(ruleConfig.getRuleDefinitionId());
			if (count != null) {
				for (int i = 0; i < count; i++) {
					list.add(generateViolation(ruleConfig, ruleBean, i + 1));
				}
			}
		}
		return list;
	}


	private RuleViolation generateViolation(RuleConfig ruleConfig, T ruleBean, int count) {
		RuleViolation violation = new RuleViolation();
		violation.setRuleAssignment(getRuleDefinitionService().getRuleAssignment(ruleConfig.getEntityConfig(null).getRuleAssignmentId()));
		violation.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(ruleBean));
		violation.setViolationNote(violation.getRuleAssignment().getRuleDefinition().getName() + "TEST VIOLATION " + count);
		violation.setRuleViolationCode(violation.getRuleAssignment().getRuleDefinition().getRuleViolationCode());
		violation.setCauseFkFieldId(MathUtils.getNumberAsLong(count));
		return violation;
	}

	/////////////////////////////////////////////////////////////////////


	public Map<Short, Integer> getRuleDefinitionCountMap() {
		return this.ruleDefinitionCountMap;
	}


	public void setRuleDefinitionCountMap(Map<Short, Integer> ruleDefinitionCountMap) {
		this.ruleDefinitionCountMap = ruleDefinitionCountMap;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}
}
