package com.clifton.rule.evaluator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.violation.RuleViolation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * Unit tests for {@link AlwaysTrueFalseRuleEvaluator}
 *
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class AlwaysTrueFalseRuleEvaluatorTests {

	@Resource
	private AlwaysTrueFalseRuleEvaluator evaluator;

	@Resource
	private RuleDefinitionService ruleDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testEvaluatorReturnsViolation() {
		RuleAssignment assignment = getRuleDefinitionService().getRuleAssignment(1);
		RuleConfig config = new RuleConfig(assignment);
		config.addEntityConfig(new EntityConfig(assignment));

		getEvaluator().setGenerateViolation(true);
		List<RuleViolation> ruleViolationList = getEvaluator().evaluateRule(assignment, config, null);
		Assertions.assertTrue(CollectionUtils.isSingleElement(ruleViolationList));
	}


	@Test
	public void testEvaluatorReturnsNoViolations() {
		RuleAssignment assignment = getRuleDefinitionService().getRuleAssignment(1);
		RuleConfig config = new RuleConfig(assignment);
		config.addEntityConfig(new EntityConfig(assignment));

		getEvaluator().setGenerateViolation(false);
		List<RuleViolation> ruleViolationList = getEvaluator().evaluateRule(assignment, config, null);
		Assertions.assertTrue(CollectionUtils.isEmpty(ruleViolationList));
	}


	@Test
	public void testEvaluatorReturnsNoViolationsEntityExcluded() {
		RuleAssignment assignment = getRuleDefinitionService().getRuleAssignment(1);
		assignment.setExcluded(true);
		RuleConfig config = new RuleConfig(assignment);
		config.addEntityConfig(new EntityConfig(assignment));

		getEvaluator().setGenerateViolation(false);
		List<RuleViolation> ruleViolationList = getEvaluator().evaluateRule(assignment, config, null);
		Assertions.assertTrue(CollectionUtils.isEmpty(ruleViolationList));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AlwaysTrueFalseRuleEvaluator getEvaluator() {
		return this.evaluator;
	}


	public void setEvaluator(AlwaysTrueFalseRuleEvaluator evaluator) {
		this.evaluator = evaluator;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}
}
