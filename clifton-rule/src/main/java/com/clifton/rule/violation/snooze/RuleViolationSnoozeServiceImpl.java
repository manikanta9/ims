package com.clifton.rule.violation.snooze;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.retriever.RuleRetrieverService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/**
 * @author StevenF
 */
@Service
public class RuleViolationSnoozeServiceImpl implements RuleViolationSnoozeService {

	private static final String[] SNOOZED_RULE_ASSIGNMENT_CLONE_IGNORED_PROPERTIES = new String[]{"id", "startDate", "endDate"};

	private DaoLocator daoLocator;

	private RuleDefinitionService ruleDefinitionService;
	private RuleRetrieverService ruleRetrieverService;
	private RuleViolationService ruleViolationService;


	@Override
	@Transactional
	public void snoozeRuleViolationByCommand(BeanListCommand<RuleViolationSnoozeCommand> command) {
		for (RuleViolationSnoozeCommand snoozeCommand : CollectionUtils.getIterable(command)) {

			long id = snoozeCommand.getId();
			int numberOfDays = snoozeCommand.getNumberOfDays();
			Date startDate = snoozeCommand.getStartDate();
			String snoozeNote = snoozeCommand.getSnoozeNote();

			ValidationUtils.assertTrue(numberOfDays > 0, "Number of days to snooze must be greater than 0.");
			ValidationUtils.assertNotNull(startDate, "You must specify a start date for the snooze assignment.");
			ValidationUtils.assertNotNull(snoozeNote, "A snooze note is required.");

			RuleViolation ruleViolation = getRuleViolationService().getRuleViolation(id);
			ValidationUtils.assertNotNull(ruleViolation, "Unable to locate rule violation with id [" + id + "]");

			RuleAssignment existingRuleAssignment = ruleViolation.getRuleAssignment();
			RuleDefinition existingRuleDefinition = existingRuleAssignment.getRuleDefinition();

			String maxSnoozeDaysAssertion = "Number of days to snooze must be less than or equal to " + existingRuleDefinition.getMaxSnoozeDays() + ".";
			if (existingRuleDefinition.getMaxSnoozeDays() == 0) {
				maxSnoozeDaysAssertion = "Snoozing is not allowed for this rule [" + existingRuleDefinition.getName() + "]";
			}
			ValidationUtils.assertTrue(numberOfDays <= existingRuleDefinition.getMaxSnoozeDays(), maxSnoozeDaysAssertion);

			boolean hasEntityScope = existingRuleDefinition.getEntityTable() != null;

			//Copy all but ignored properties from existing assignment, then override as needed.
			RuleAssignment ruleAssignment = new RuleAssignment(existingRuleAssignment, SNOOZED_RULE_ASSIGNMENT_CLONE_IGNORED_PROPERTIES);
			//Check if the existingAssignment is for the most granular level of scope and re-enable/snooze if it is.
			if (existingRuleAssignment.getAdditionalScopeFkFieldId() != null &&
					(hasEntityScope && existingRuleAssignment.getEntityFkFieldId() != null)) {
				ruleAssignment = existingRuleAssignment;
			}
			ruleAssignment.setSnoozed(true);
			ruleAssignment.setStartDate(startDate);
			ruleAssignment.setEndDate(DateUtils.addDays(startDate, numberOfDays));
			ruleAssignment.setNote("Snooze Assignment");

			//Set scope of snooze assignment
			if (existingRuleAssignment.getAdditionalScopeFkFieldId() == null || existingRuleAssignment.getAdditionalScopeLabel() == null) {
				setAdditionalScope(ruleAssignment, ruleViolation);
			}
			if (hasEntityScope && (existingRuleAssignment.getEntityFkFieldId() == null || existingRuleAssignment.getEntityLabel() == null)) {
				setEntityScope(ruleAssignment, ruleViolation);
			}

			ValidationUtils.assertTrue(ruleAssignment.getAdditionalScopeFkFieldId() != null, "Unable to snooze as no additional scope has been assigned.");
			if (hasEntityScope) {
				ValidationUtils.assertTrue(ruleAssignment.getEntityFkFieldId() != null, "Unable to snooze as no entity scope has been assigned.");
			}
			getRuleDefinitionService().saveRuleAssignment(ruleAssignment);
			ruleViolation.setRuleAssignment(ruleAssignment);

			//Should I auto-ignore? Makes modifications to filter snoozed from ViolationAwareObserver and Comparison pointless?
			//Other than the status may show PROCESSED_SUCCESS vs PROCESSED_IGNORED if all violations were snoozed rather than ignored...
			ruleViolation.setIgnoreNote(snoozeNote);
			ruleViolation.setIgnored(true);

			getRuleViolationService().saveRuleViolation(ruleViolation);
		}
	}


	@Override
	@Transactional
	public void unsnoozeRuleViolationByCommand(BeanListCommand<RuleViolationSnoozeCommand> command) {
		for (RuleViolationSnoozeCommand snoozeCommand : CollectionUtils.getIterable(command)) {
			long id = snoozeCommand.getId();
			RuleViolation ruleViolation = getRuleViolationService().getRuleViolation(id);
			RuleAssignment ruleAssignment = ruleViolation.getRuleAssignment();
			ValidationUtils.assertTrue(ruleAssignment.isSnoozed(), "This violation is not currently snoozed and so cannot be un-snoozed.");
			//End the assignment today
			Date date = DateUtils.addDays(new Date(), -1);
			if (DateUtils.isDateBefore(date, ruleAssignment.getStartDate(), false)) {
				ruleAssignment.setEndDate(ruleAssignment.getStartDate());
			}
			else {
				ruleAssignment.setEndDate(date);
			}
			getRuleDefinitionService().saveRuleAssignment(ruleAssignment);

			//Now that we've updated the rule assignment to 'disable' we need to re-evaluate the config for this definition/violation to assign
			//the new applicable assignment and save the violation again so the update is reflected on the UI.
			IdentityObject additionalScopeEntity = getAdditionalScopeEntity(ruleViolation);
			IdentityObject entityScope = null;
			if (ruleAssignment.getRuleDefinition().getEntityTable() != null) {
				entityScope = getEntityScope(ruleViolation);
			}
			RuleConfig ruleConfig = getRuleRetrieverService().getRuleConfigListForAdditionalScopeEntityAndDefinition(ruleViolation.getRuleAssignment().getRuleDefinition(), additionalScopeEntity, ruleAssignment.getRuleDefinition().isPreProcess());
			EntityConfig entityConfig = ruleConfig.getEntityConfig(entityScope != null ? BeanUtils.getIdentityAsLong(entityScope) : null);

			ruleViolation.setRuleAssignment(getRuleDefinitionService().getRuleAssignment(entityConfig.getRuleAssignmentId()));
			ruleViolation.setIgnoreNote("");
			ruleViolation.setIgnored(false);
			getRuleViolationService().saveRuleViolation(ruleViolation);
		}
	}


	private void setAdditionalScope(RuleAssignment ruleAssignment, RuleViolation ruleViolation) {
		IdentityObject additionalScopeEntity = getAdditionalScopeEntity(ruleViolation);
		if (additionalScopeEntity != null) {
			ruleAssignment.setAdditionalScopeFkFieldId(BeanUtils.getIdentityAsLong(additionalScopeEntity));
			ruleAssignment.setAdditionalScopeLabel(BeanUtils.getLabel(additionalScopeEntity));
		}
	}


	private IdentityObject getAdditionalScopeEntity(RuleViolation ruleViolation) {
		RuleAssignment existingRuleAssignment = ruleViolation.getRuleAssignment();
		IdentityObject linkedEntity = getDaoLocator().locate(existingRuleAssignment.getRuleDefinition().getRuleCategory().getCategoryTable().getName()).findByPrimaryKey(ruleViolation.getLinkedFkFieldId());
		String additionalScopeBeanFieldPath = existingRuleAssignment.getRuleDefinition().getRuleCategory().getAdditionalScopeBeanFieldPath();
		if (additionalScopeBeanFieldPath != null) {
			return (IdentityObject) BeanUtils.getPropertyValue(linkedEntity, additionalScopeBeanFieldPath);
		}
		return null;
	}


	private void setEntityScope(RuleAssignment ruleAssignment, RuleViolation ruleViolation) {
		IdentityObject entityScope = getEntityScope(ruleViolation);
		if (entityScope != null) {
			ruleAssignment.setEntityFkFieldId(BeanUtils.getIdentityAsLong(entityScope));
			ruleAssignment.setEntityLabel(BeanUtils.getLabel(entityScope));
		}
	}


	private IdentityObject getEntityScope(RuleViolation ruleViolation) {
		RuleAssignment existingRuleAssignment = ruleViolation.getRuleAssignment();
		ValidationUtils.assertNotNull(ruleViolation.getEntityFkFieldId(), "This violation does not specify an entityFkFieldId, but is required to.  This indicates this is a historical violation prior to snooze functionality being implemented.  Please reprocess the run before attempting to snooze this violation.");
		return getDaoLocator().locate(existingRuleAssignment.getRuleDefinition().getEntityTable().getName()).findByPrimaryKey(ruleViolation.getEntityFkFieldId());
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleRetrieverService getRuleRetrieverService() {
		return this.ruleRetrieverService;
	}


	public void setRuleRetrieverService(RuleRetrieverService ruleRetrieverService) {
		this.ruleRetrieverService = ruleRetrieverService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
