package com.clifton.rule.violation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberIntegerMethod;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberMoneyMethod;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberPercentMethod;
import com.clifton.core.converter.template.method.FreeMarkerFormatNumberPreciseMethod;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.rule.violation.search.RuleViolationSearchFormConfigurer;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class RuleViolationServiceImpl implements RuleViolationService {

	private AdvancedUpdatableDAO<RuleViolation, Criteria> ruleViolationDAO;
	private DaoCompositeKeyListCache<RuleViolation, Short, Long> ruleViolationByEntityCache;

	private RuleDefinitionService ruleDefinitionService;
	private SystemSchemaService systemSchemaService;
	private TemplateConverter templateConverter;

	private DaoLocator daoLocator;


	@Override
	public RuleViolation createRuleViolation(EntityConfig entityConfig, Serializable linkedFkFieldId) {
		return createRuleViolationWithCause(entityConfig, linkedFkFieldId, null);
	}


	@Override
	public RuleViolation createRuleViolation(EntityConfig entityConfig, Serializable linkedFkFieldId, Map<String, Object> contextValuesMap) {
		return createRuleViolationWithCause(entityConfig, linkedFkFieldId, null, null, contextValuesMap);
	}


	@Override
	public RuleViolation createRuleViolationWithCause(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable causeFkFieldId) {
		return createRuleViolationWithCause(entityConfig, linkedFkFieldId, causeFkFieldId, null);
	}


	@Override
	public RuleViolation createRuleViolationWithCause(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable) {
		return createRuleViolationWithCause(entityConfig, linkedFkFieldId, causeFkFieldId, causeTable, null);
	}


	@Override
	public RuleViolation createRuleViolationWithCause(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap) {
		return createRuleViolationWithNoteTemplate(entityConfig, linkedFkFieldId, null, causeFkFieldId, causeTable, contextValuesMap, null, null);
	}


	@Override
	public RuleViolation createRuleViolationWithEntity(EntityConfig entityConfig, Long linkedFkFieldId, Long entityFkFieldId, Map<String, Object> contextValuesMap) {
		return createRuleViolationWithNoteTemplate(entityConfig, linkedFkFieldId, entityFkFieldId, null, null, contextValuesMap, null, null);
	}


	@Override
	public RuleViolation createRuleViolationWithEntityAndCause(EntityConfig entityConfig, Long linkedFkFieldId, Long entityFkFieldId, Long causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap) {
		return createRuleViolationWithNoteTemplate(entityConfig, linkedFkFieldId, entityFkFieldId, causeFkFieldId, causeTable, contextValuesMap, null, null);
	}


	@Override
	public RuleViolation createRuleViolationWithNoteTemplate(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable entityFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap, String violationNoteTemplateOverride, String ruleViolationCodeTemplate) {
		RuleAssignment ruleAssignment = getRuleDefinitionService().getRuleAssignment(entityConfig.getRuleAssignmentId());
		return createRuleViolationWithNoteTemplateImpl(ruleAssignment, entityConfig, linkedFkFieldId, entityFkFieldId, causeFkFieldId, causeTable, contextValuesMap, violationNoteTemplateOverride, ruleViolationCodeTemplate);
	}


	private RuleViolation createRuleViolationWithNoteTemplateImpl(RuleAssignment ruleAssignment, EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable entityFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap, String violationNoteTemplateOverride, String ruleViolationCodeTemplate) {
		RuleDefinition ruleDefinition = ruleAssignment.getRuleDefinition();
		ValidationUtils.assertNotNull(ruleDefinition.getViolationNoteTemplate(), "Violation Note Template may not be null.");

		RuleViolation ruleViolation = new RuleViolation();
		ruleViolation.setRuleAssignment(ruleAssignment);

		if (ruleDefinition.getRuleViolationCode() != null) {
			ruleViolation.setRuleViolationCode(ruleDefinition.getRuleViolationCode());
		}
		ruleViolation.setLinkedFkFieldId(MathUtils.getNumberAsLong((Number) linkedFkFieldId));

		IdentityObject additionalScopeEntity = null;
		IdentityObject linkedEntity = getDaoLocator().locate(ruleDefinition.getRuleCategory().getCategoryTable().getName()).findByPrimaryKey(linkedFkFieldId);
		String additionalScopeBeanFieldPath = ruleDefinition.getRuleCategory().getAdditionalScopeBeanFieldPath();
		if (additionalScopeBeanFieldPath != null) {
			additionalScopeEntity = (IdentityObject) BeanUtils.getPropertyValue(linkedEntity, additionalScopeBeanFieldPath);
		}

		IdentityObject causeEntity = null;
		if (causeFkFieldId != null) {
			if (causeTable == null) {
				causeTable = ruleDefinition.getCauseTable();
			}
			ValidationUtils.assertNotNull(causeTable, "Cause Table may not be null when causeFkFieldId is not.\n" +
					"Check the configuration of rule: " + ruleDefinition.getName());
			causeEntity = getDaoLocator().locate(causeTable.getName()).findByPrimaryKey(causeFkFieldId);
			ruleViolation.setCauseFkFieldId(MathUtils.getNumberAsLong((Number) causeFkFieldId));
			ruleViolation.setCauseTable(causeTable);
		}

		IdentityObject entityScope = null;
		if (entityFkFieldId != null) {
			entityScope = getDaoLocator().locate(ruleDefinition.getEntityTable().getName()).findByPrimaryKey(entityFkFieldId);
			ruleViolation.setEntityFkFieldId(MathUtils.getNumberAsLong((Number) entityFkFieldId));
		}

		Map<String, Object> templateContextValuesMap = new HashMap<>();
		//Add objects to context for template evaluation
		templateContextValuesMap.put(RuleEvaluator.ADDITIONAL_SCOPE_ENTITY_FREEMARKER_TEMPLATE_KEY, additionalScopeEntity);
		templateContextValuesMap.put(RuleEvaluator.CAUSE_ENTITY_FREEMARKER_TEMPLATE_KEY, causeEntity);
		templateContextValuesMap.put(RuleEvaluator.ENTITY_CONFIG_FREEMARKER_TEMPLATE_KEY, entityConfig);
		templateContextValuesMap.put(RuleEvaluator.ENTITY_SCOPE_FREEMARKER_TEMPLATE_KEY, entityScope);
		templateContextValuesMap.put(RuleEvaluator.LINKED_ENTITY_FREEMARKER_TEMPLATE_KEY, linkedEntity);

		//Add custom context values to the template context.
		if (contextValuesMap != null) {
			templateContextValuesMap.putAll(contextValuesMap);
		}
		//generate violation note using freemarker template.
		ruleViolation.setViolationNote(generateViolationString(templateContextValuesMap, (StringUtils.isEmpty(violationNoteTemplateOverride) ? ruleDefinition.getViolationNoteTemplate() : violationNoteTemplateOverride)));

		//generate violation code using freemarker template.
		if (!StringUtils.isEmpty(ruleViolationCodeTemplate)) {
			ruleViolation.setRuleViolationCode(generateViolationString(templateContextValuesMap, (StringUtils.isEmpty(ruleViolationCodeTemplate) ? "" : ruleViolationCodeTemplate)));
		}
		return ruleViolation;
	}


	@Override
	public RuleViolation createRuleViolationSystemDefined(String violationNote, String ruleDefinitionName, Serializable linkedFkFieldId) {
		return createRuleViolationSystemDefinedWithCause(violationNote, ruleDefinitionName, linkedFkFieldId, null, null);
	}


	@Override
	public RuleViolation createRuleViolationSystemDefinedWithCause(String violationNote, String ruleDefinitionName, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable) {
		ValidationUtils.assertNotNull(linkedFkFieldId, "Linked Entity ID cannot be null");
		ValidationUtils.assertNotNull(violationNote, "Violation Note cannot be null");
		ValidationUtils.assertNotNull(ruleDefinitionName, "Rule Definition Name is required.");

		//System Defined Rules should only have a single global assignment.
		RuleAssignment ruleAssignment = getRuleDefinitionService().getRuleAssignmentGlobalByDefinitionName(ruleDefinitionName);

		RuleViolation ruleViolation = new RuleViolation();
		ruleViolation.setRuleAssignment(ruleAssignment);
		ruleViolation.setLinkedFkFieldId(MathUtils.getNumberAsLong((Number) linkedFkFieldId));
		ruleViolation.setViolationNote(violationNote);
		ruleViolation.setCauseFkFieldId(causeFkFieldId != null ? ((Number) causeFkFieldId).longValue() : null);
		if (causeTable != null) {
			ruleViolation.setCauseTable(causeTable);
		}
		return ruleViolation;
	}


	@Override
	public RuleViolation createRuleViolationSystemDefinedWithCauseUsingTemplate(String ruleDefinitionName, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> templateContextValuesMap) {
		ValidationUtils.assertNotNull(linkedFkFieldId, "Linked Entity ID cannot be null");
		ValidationUtils.assertNotNull(ruleDefinitionName, "Rule Definition Name is required.");

		//System Defined Rules should only have a single global assignment.
		RuleAssignment ruleAssignment = getRuleDefinitionService().getRuleAssignmentGlobalByDefinitionName(ruleDefinitionName);
		return createRuleViolationWithNoteTemplateImpl(ruleAssignment, null, linkedFkFieldId, null, causeFkFieldId, causeTable, templateContextValuesMap, null, null);
	}


	/**
	 * This method can be used to generate the violation note or code from freemarker template
	 */
	private String generateViolationString(Map<String, Object> templateContextValuesMap, String violationNoteTemplate) {
		TemplateConfig config = new TemplateConfig(violationNoteTemplate);
		for (Map.Entry<String, Object> templateContextEntry : CollectionUtils.getIterable(templateContextValuesMap.entrySet())) {
			Object templateContextValue = templateContextEntry.getValue();
			if (templateContextValue != null) {
				config.addBeanToContext(templateContextEntry.getKey(), templateContextValue);
			}
		}
		config.addBeanToContext("formatNumberInteger", new FreeMarkerFormatNumberIntegerMethod());
		config.addBeanToContext("formatNumberMoney", new FreeMarkerFormatNumberMoneyMethod());
		config.addBeanToContext("formatNumberPercent", new FreeMarkerFormatNumberPercentMethod());
		config.addBeanToContext("formatNumberPrecise", new FreeMarkerFormatNumberPreciseMethod());
		return getTemplateConverter().convert(config);
	}


	@Override
	public RuleViolation getRuleViolation(long id) {
		return getRuleViolationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<RuleViolation> getRuleViolationList(RuleViolationSearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getLinkedTableName())) {
			searchForm.setLinkedTableId(getSystemSchemaService().getSystemTableByName(searchForm.getLinkedTableName()).getId());
			searchForm.setLinkedTableName(null);
		}
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("priorityOrder:asc#ruleDefinitionName:asc#violationNote:asc");
		}
		return getRuleViolationDAO().findBySearchCriteria(new RuleViolationSearchFormConfigurer(searchForm));
	}


	@Override
	public List<RuleViolation> getRuleViolationListByLinkedEntity(String tableName, Serializable fkFieldId) {
		return getRuleViolationByEntityCache().getBeanListForKeyValues(getRuleViolationDAO(), getSystemSchemaService().getSystemTableByName(tableName).getId(), MathUtils.getNumberAsLong((Number) fkFieldId));
	}


	/**
	 * Get the list of violation codes and concatenate to a single value
	 */

	@Override
	public String getRuleViolationCodeByLinkedEntity(String tableName, Serializable fkFieldId) {
		List<RuleViolation> ruleViolationList = getRuleViolationListByLinkedEntity(tableName, fkFieldId);
		String result = Optional.ofNullable(ruleViolationList)
				.orElseGet(Collections::emptyList).stream()
				.map(RuleViolation::getRuleViolationCode).filter(Objects::nonNull)
				.filter(code -> !code.isEmpty()).distinct().collect(Collectors.joining(" "));
		return (StringUtils.isEmpty(result) ? null : result);
	}


	@Override
	public List<RuleViolation> getRuleViolationListByLinkedEntityAndDefinition(String linkedTableName, Serializable linkedFkFieldId, String definitionName) {
		return getRuleViolationListByLinkedEntityAndDefinition(getSystemSchemaService().getSystemTableByName(linkedTableName).getId(), linkedFkFieldId, definitionName);
	}


	@Override
	public List<RuleViolation> getRuleViolationListByLinkedEntityAndDefinition(Short linkedTableId, Serializable linkedFkFieldId, String definitionName) {
		List<RuleViolation> ruleViolationList = getRuleViolationByEntityCache().getBeanListForKeyValues(getRuleViolationDAO(), linkedTableId, MathUtils.getNumberAsLong((Number) linkedFkFieldId));

		if (!StringUtils.isEmpty(definitionName)) {
			ruleViolationList = BeanUtils.filter(ruleViolationList, violation -> violation.getRuleAssignment().getRuleDefinition().getName(), definitionName);
		}
		return ruleViolationList;
	}


	@Override
	public RuleViolation saveRuleViolation(RuleViolation bean) {
		return getRuleViolationDAO().save(bean);
	}


	@Override
	public RuleViolation saveRuleViolationManual(RuleViolation ruleViolation) {
		ValidationUtils.assertTrue(ruleViolation.getRuleAssignment().getRuleDefinition().isManual(), "Only Manual Violations can be saved directly in the system.");
		return getRuleViolationDAO().save(ruleViolation);
	}


	@Override
	public RuleViolation saveRuleViolationFromException(String ruleDefinitionName, Serializable linkedFkFieldId, ValidationExceptionWithCause exception) {
		ValidationUtils.assertNotNull(exception, "Validation Exception cannot be null");
		ValidationUtils.assertNotNull(ruleDefinitionName, "Rule Definition Name is required.");

		RuleViolation ruleViolation = new RuleViolation();
		ruleViolation.setRuleAssignment(getRuleDefinitionService().getRuleAssignmentGlobalByDefinitionName(ruleDefinitionName));
		ruleViolation.setCauseFkFieldId(exception.getCauseFkFieldId());
		if (!StringUtils.isEmpty(exception.getCauseTableName())) {
			ruleViolation.setCauseTable(getSystemSchemaService().getSystemTableByName(exception.getCauseTableName()));
		}
		ruleViolation.setLinkedFkFieldId(MathUtils.getNumberAsLong((Number) linkedFkFieldId));
		ruleViolation.setViolationNote(exception.getMessage());
		// Call DAO method directly - save method in the service is for manual warnings
		return getRuleViolationDAO().save(ruleViolation);
	}


	/**
	 * Retrieves existingList for the entity and definition
	 * If newList is empty = all existing violations are deleted.
	 * If existingList is empty = all new violations are added.
	 * If both are populated, newList is updated with proper id values and ignore properties.
	 * Warnings are considered to be equal based on the overridden equals method in RuleViolation class
	 */
	@Override
	public List<RuleViolation> updateRuleViolationListForEntityAndDefinition(List<RuleViolation> newList, String linkedTableName, Serializable linkedFkFieldId, String definitionName) {
		List<RuleViolation> existingList = getRuleViolationListByLinkedEntityAndDefinition(linkedTableName, linkedFkFieldId, definitionName);
		return updateRuleViolationList(newList, existingList);
	}


	@Override
	@Transactional
	public List<RuleViolation> updateRuleViolationList(List<RuleViolation> newList, List<RuleViolation> existingList) {
		if (!CollectionUtils.isEmpty(newList) || !CollectionUtils.isEmpty(existingList)) {
			// Make sure no duplicates within the new list and
			// ensure only adding warnings for types that are currently active
			newList = filterViolationListForDupesAndInactive(newList);

			// IF BOTH lists aren't empty, then need to go through them and properly set ids, etc for updates.
			if (!CollectionUtils.isEmpty(newList) && !CollectionUtils.isEmpty(existingList)) {
				for (RuleViolation violation : CollectionUtils.getIterable(newList)) {
					for (RuleViolation existing : CollectionUtils.getIterable(existingList)) {
						// Call special overridden equals method in System Warning to see if both entities are really equal
						if (violation.equals(existing)) {
							// Set ID on the the new list, so updates and copy over ignored properties
							violation.setId(existing.getId());
							violation.setIgnored(existing.isIgnored());
							violation.setIgnoreNote(existing.getIgnoreNote());
						}
					}
				}
			}
			getRuleViolationDAO().saveList(newList, existingList);
		}
		return newList;
	}


	/**
	 * Returns a filtered {@link RuleViolation} List containing only those active with no duplicates.
	 */
	@Override
	public List<RuleViolation> filterViolationListForDupesAndInactive(List<RuleViolation> list) {
		if (CollectionUtils.isEmpty(list)) {
			return list;
		}
		return list.stream().filter(violation -> {
			boolean active = !violation.getRuleAssignment().getRuleDefinition().isInactive();
			if (active && StringUtils.isEmpty(violation.getViolationNote())) {
				// Invalid warning processing - likely because of SQL warning adding strings where one of the values is NULL
				// and needs to use COALESCE
				throw new RuntimeException("Errors running rule [" + violation.getRuleAssignment().getRuleDefinition().getName() + "] for [" +
						violation.getRuleAssignment().getRuleDefinition().getRuleCategory().getCategoryTable().getName() + ": " + violation.getLinkedFkFieldId() +
						"] " + (violation.getCauseFkFieldId() != null ? " and cause [" + violation.getViolationCauseTable().getName() + ": " +
						violation.getCauseFkFieldId() + "]" : "") + ": violation note generated is NULL.");
			}
			return active;
		}).distinct().collect(Collectors.toList());
	}


	@Override
	public void deleteRuleViolation(long id) {
		getRuleViolationDAO().delete(id);
	}


	@Override
	public void deleteRuleViolationManual(long id) {
		RuleViolation ruleViolation = getRuleViolation(id);
		if (ruleViolation != null) {
			ValidationUtils.assertTrue(ruleViolation.getRuleAssignment().getRuleDefinition().isManual(), "Only Manual Violations can be deleted directly in the system.");
			getRuleViolationDAO().delete(ruleViolation);
		}
	}


	@Override
	public void deleteRuleViolationListByLinkedEntity(String tableName, long fkFieldId) {
		List<RuleViolation> list = getRuleViolationListByLinkedEntity(tableName, fkFieldId);
		getRuleViolationDAO().deleteList(list);
	}


	@Override
	public void deleteRuleViolationListByLinkedEntityNotIgnorable(String categoryName, long fkFieldId) {
		RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
		RuleCategory category = getRuleDefinitionService().getRuleCategoryByName(categoryName);
		searchForm.setCategoryId(category.getId());
		searchForm.setLinkedFkFieldId(fkFieldId);
		searchForm.setIgnorable(false);
		getRuleViolationDAO().deleteList(getRuleViolationList(searchForm));
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<RuleViolation, Criteria> getRuleViolationDAO() {
		return this.ruleViolationDAO;
	}


	public void setRuleViolationDAO(AdvancedUpdatableDAO<RuleViolation, Criteria> ruleViolationDAO) {
		this.ruleViolationDAO = ruleViolationDAO;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public DaoCompositeKeyListCache<RuleViolation, Short, Long> getRuleViolationByEntityCache() {
		return this.ruleViolationByEntityCache;
	}


	public void setRuleViolationByEntityCache(DaoCompositeKeyListCache<RuleViolation, Short, Long> ruleViolationByEntityCache) {
		this.ruleViolationByEntityCache = ruleViolationByEntityCache;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
