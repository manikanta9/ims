package com.clifton.rule.violation;

import com.clifton.core.beans.LabeledObject;


/**
 * The <code>RuleViolationIgnoreNote</code> is simply a holder object used to provide users
 * with a list of previously entered ignore notes that can be automatically applied and/or
 * selected to be used for a new violation while ignoring.
 *
 * @author StevenF
 */
public class RuleViolationIgnoreNote implements LabeledObject {

	private String ignoreNote;

	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return null;
	}

	/////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getIgnoreNote() {
		return this.ignoreNote;
	}


	public void setIgnoreNote(String ignoreNote) {
		this.ignoreNote = ignoreNote;
	}
}
