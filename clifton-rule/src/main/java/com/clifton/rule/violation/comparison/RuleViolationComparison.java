package com.clifton.rule.violation.comparison;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.rule.violation.RuleViolation;


/**
 * The <code>RuleViolationComparison</code> is an abstract class that can be used by {@link Comparison}s
 * that are used for RuleViolations.  Has lookup methods for cause/linked entity
 *
 * @author manderson
 */
public abstract class RuleViolationComparison<T extends IdentityObject> implements Comparison<T> {

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	protected T getLinkedEntity(RuleViolation violation) {
		return (T) getDaoLocator().locate(violation.getRuleAssignment().getRuleDefinition().getRuleCategory().getCategoryTable().getName()).findByPrimaryKey(violation.getLinkedFkFieldId());
	}


	@SuppressWarnings("unchecked")
	protected T getCauseEntity(RuleViolation violation) {
		// The condition should be passed the bean that the warning was caused by and if no cause, then linked to.
		if (violation.getViolationCauseTable() != null) {
			if (violation.getCauseFkFieldId() != null) {
				return (T) getDaoLocator().locate(violation.getViolationCauseTable().getName()).findByPrimaryKey(violation.getCauseFkFieldId());
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
