package com.clifton.rule.violation;

import com.clifton.core.beans.IdentityObject;


/**
 * @author AbhinayaM
 */
public interface RuleViolationCodeAware extends IdentityObject {


	public String getRuleViolationCodes();


	public void setRuleViolationCodes(String ruleViolationCodes);
}
