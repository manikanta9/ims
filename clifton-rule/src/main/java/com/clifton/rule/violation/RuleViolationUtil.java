package com.clifton.rule.violation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class RuleViolationUtil {

	public static boolean isRuleViolationAwareSupported(Object bean) {
		return bean instanceof RuleViolationAware;
	}


	public static RuleViolationAware getRuleViolationAwareBean(Object bean) {
		return isRuleViolationAwareSupported(bean) ? ((RuleViolationAware) bean) : null;
	}


	public static boolean isObjectOutdated(Date controlDate, Date objectDate, BigDecimal dateThreshold, Map<String, Object> templateValues) {
		int dateDiff = DateUtils.getDaysDifference(controlDate, objectDate);
		int outdatedDaysAllowed = dateThreshold != null ? dateThreshold.intValue() : 0;
		if (MathUtils.isGreaterThan(dateDiff, outdatedDaysAllowed)) {
			templateValues.put("dateDiff", dateDiff);
			return true;
		}
		return false;
	}


	public static boolean isRuleViolationAwareProcessed(RuleViolationAware bean) {
		if (bean == null) {
			return false;
		}
		if (bean.getViolationStatus() == null) {
			return false;
		}
		return bean.getViolationStatus().isProcessed();
	}


	public static boolean isRuleViolationAwareReadyForUse(RuleViolationAware bean) {
		if (bean == null) {
			return false;
		}
		if (bean.getViolationStatus() == null) {
			return false;
		}
		return bean.getViolationStatus().isReadyForUse();
	}


	public static boolean isRuleViolationAwareUnresolvedWarningsExist(RuleViolationAware bean) {
		if (bean == null) {
			return false;
		}
		if (bean.getViolationStatus() == null) {
			return false;
		}
		return bean.getViolationStatus().isUnresolvedViolationsExist();
	}


	/**
	 * Returns the status of violations for the entity with the specified violations.  Ignores snoozed violations.
	 */
	public static RuleViolationStatus.RuleViolationStatusNames getRuleViolationStatus(List<RuleViolation> violationList) {
		violationList = BeanUtils.filter(violationList, RuleViolation::isSnoozed, false);
		if (CollectionUtils.isEmpty(violationList)) {
			return RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS;
		}
		// If Warnings that are not ignorable, then consider processing failed
		else if (!CollectionUtils.isEmpty(violationList.stream().filter(violation -> BooleanUtils.isFalse(violation.getRuleAssignment().getRuleDefinition().isIgnorable())).collect(Collectors.toList()))) {
			return RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED;
		}
		// If not all ignorable warnings are ignored, consider PROCESSED_VIOLATIONS
		else if (!CollectionUtils.isEmpty(BeanUtils.filter(violationList, RuleViolation::isIgnored, false))) {
			return RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS;
		}
		// Otherwise mark it as PROCESSED_IGNORED, i.e. there are ignorable warnings and they've all been ignored
		else {
			return RuleViolationStatus.RuleViolationStatusNames.PROCESSED_IGNORED;
		}
	}
}
