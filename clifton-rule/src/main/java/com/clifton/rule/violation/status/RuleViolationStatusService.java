package com.clifton.rule.violation.status;

import java.util.List;


/**
 * @author manderson
 */
public interface RuleViolationStatusService {

	////////////////////////////////////////////////////////////////////////////
	///////          Rule Violation Status Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	public RuleViolationStatus getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames name);


	public List<RuleViolationStatus> getRuleViolationStatusList();
}
