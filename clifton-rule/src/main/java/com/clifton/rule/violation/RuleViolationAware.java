package com.clifton.rule.violation;


import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.violation.status.RuleViolationStatus;


/**
 * The <code>RuleViolationAware</code> interface must be implemented by entities that use the RuleViolation features.
 * <p>
 * Rule Violation Status defines if any violations exist and their status (Ignored or Not)
 * {@link RuleViolationAwareObserver} is registered as an observer for those entities to manage the violation status field automatically
 *
 * @author Mary Anderson
 */
public interface RuleViolationAware extends IdentityObject {


	public RuleViolationStatus getViolationStatus();


	public void setViolationStatus(RuleViolationStatus violationStatus);
}
