package com.clifton.rule.violation.ignore;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author StevenF
 */
@Service
public class RuleViolationIgnoreServiceImpl implements RuleViolationIgnoreService {

	private DaoLocator daoLocator;
	private AdvancedUpdatableDAO<RuleViolation, Criteria> ruleViolationDAO;

	private RuleDefinitionService ruleDefinitionService;
	private RuleViolationService ruleViolationService;


	@Override
	@Transactional
	public void ignoreRuleViolationByCommand(BeanListCommand<RuleViolationIgnoreCommand> command) {
		for (RuleViolationIgnoreCommand ignoreCommand : CollectionUtils.getIterable(command)) {
			ValidationUtils.assertNotNull(ignoreCommand.getId(), "You must specify the id of the violation to ignore.");

			RuleViolation ruleViolation = getRuleViolationService().getRuleViolation(ignoreCommand.getId());
			ruleViolation.setIgnored(true);
			validateAndSetIgnoreNote(ruleViolation, ignoreCommand.getIgnoreNote());

			if (ignoreCommand.getIncludeIgnoreRelatedByField() != null) {
				// Need to store table name here because get list changes table name to an id value for quicker look ups
				String tableName = ignoreCommand.getLinkedTableName();
				// Ignore Related Violations of those we just ignored
				Long[] relatedIds = getRelatedRuleViolationFkFieldIds(tableName, ignoreCommand.getIncludeIgnoreRelatedByField(), ignoreCommand.getLinkedFkFieldId(), null);
				ignoreRuleViolationRelatedListImpl(Collections.singletonList(ruleViolation), tableName, relatedIds);
			}
			getRuleViolationService().saveRuleViolation(ruleViolation);
		}
	}


	@Override
	@Transactional
	public void ignoreRuleViolationList(BeanListCommand<RuleViolationIgnoreCommand> command) {
		for (RuleViolationIgnoreCommand ignoreCommand : CollectionUtils.getIterable(command)) {
			//Set defaults for the search
			ignoreCommand.setIgnorable(true);
			//Ensure we don't filter on ignored, this allows updating ignore notes for ignored items.
			ignoreCommand.setIgnored(null);
			//Ignore note required in the command is used to drive the UI, clear before executing the search
			//the ruleDefinition is used to enforce an ignore note when needed.
			ignoreCommand.setIgnoreNoteRequired(null);
			String ignoreNote = ignoreCommand.getIgnoreNote();
			if (ignoreNote != null) {
				ignoreCommand.setIgnoreNote(null);
			}

			List<RuleViolation> violations = getRuleViolationService().getRuleViolationList(ignoreCommand);
			if (CollectionUtils.isEmpty(violations)) {
				throw new ValidationException("No Violations Updated.  There were no ignorable violations found to be updated.");
			}

			for (RuleViolation violation : violations) {
				violation.setIgnored(true);
				validateAndSetIgnoreNote(violation, ignoreNote);
			}
			getRuleViolationDAO().saveList(violations);
		}
	}


	private void validateAndSetIgnoreNote(RuleViolation violation, String ignoreNote) {
		//Apply the ignoreNote to all violations
		if (ignoreNote == null && violation.getRuleAssignment().getRuleDefinition().isIgnoreNoteRequired()) {
			throw new ValidationException("Unable to update violation [" + violation.getViolationNote() + "] as an ignore note is required.");
		}
		else {
			violation.setIgnoreNote(ignoreNote);
		}
	}


	private Long[] getRelatedRuleViolationFkFieldIds(String tableName, String relatedFieldName, Long mainLinkedId, Object relatedFieldValue) {
		// If used - get list of ids that match related field value
		if (!StringUtils.isEmpty(relatedFieldName)) {
			ValidationUtils.assertNotNull(tableName, "Linked Table Name is required when using the option to also ignore violations for related entities.");
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(tableName);
			if (relatedFieldValue == null) {
				ValidationUtils.assertNotNull(mainLinkedId, "Linked FKFieldID is required when using the option to also ignore warnings for related entities without related entity value passed.");
				IdentityObject obj = dao.findByPrimaryKey(mainLinkedId);
				relatedFieldValue = BeanUtils.getPropertyValue(obj, relatedFieldName);
			}
			if (relatedFieldValue != null) {
				List<IdentityObject> relatedObjects = dao.findByField(relatedFieldName, relatedFieldValue);
				if (relatedObjects != null) {
					return relatedObjects.stream().map(BeanUtils::getIdentityAsLong).toArray(Long[]::new);
				}
			}
		}
		return null;
	}


	/**
	 * If matchToViolationList is empty - will consider everything a match
	 */
	private void ignoreRuleViolationRelatedListImpl(List<RuleViolation> matchToViolationList, String tableName, Long[] relatedIds) {
		if (relatedIds == null || relatedIds.length == 0) {
			return;
		}
		RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
		searchForm.setIgnorable(true);
		searchForm.setIgnored(false);
		searchForm.setIgnoreNoteRequired(false);
		searchForm.setLinkedFkFieldIds(relatedIds);
		searchForm.setLinkedTableName(tableName);
		List<RuleViolation> relatedViolations = getRuleViolationService().getRuleViolationList(searchForm);

		// No violations for related entity
		if (CollectionUtils.isEmpty(relatedViolations)) {
			return;
		}

		// Match related violations for related entities by matching types
		if (!CollectionUtils.isEmpty(matchToViolationList)) {
			List<RuleViolation> ignoreViolations = new ArrayList<>();
			Set<Short> processDefinitions = new HashSet<>();
			for (RuleViolation mainViolation : matchToViolationList) {
				if (processDefinitions.add(mainViolation.getRuleAssignment().getRuleDefinition().getId())) {
					for (RuleViolation relatedViolation : relatedViolations) {
						if (mainViolation.equals(relatedViolation)) {
							continue;
						}
						if (mainViolation.getRuleAssignment().getRuleDefinition().equals(relatedViolation.getRuleAssignment().getRuleDefinition())) {
							relatedViolation.setIgnored(true);
							ignoreViolations.add(relatedViolation);
						}
					}
				}
			}
			getRuleViolationDAO().saveList(ignoreViolations);
		}
		else {
			// Otherwise - Ignore all related Warnings
			for (RuleViolation violation : relatedViolations) {
				violation.setIgnored(true);
			}
			getRuleViolationDAO().saveList(relatedViolations);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////          Rule Violation Ignore Note Business Methods           //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> getRuleViolationIgnoreNoteList(RuleViolationIgnoreCommand command) {
		List<RuleViolation> ruleViolationList = getRuleViolationService().getRuleViolationList(initializeIgnoreNoteListCommand(command));
		if (CollectionUtils.isEmpty(ruleViolationList)) {
			ruleViolationList = new PagingArrayList<>();
		}
		return new PagingArrayList<>(filterRuleViolationList(ruleViolationList),
				((PagingArrayList<RuleViolation>) ruleViolationList).getFirstElementIndex(), ruleViolationList.size());
	}


	private RuleViolationIgnoreCommand initializeIgnoreNoteListCommand(RuleViolationIgnoreCommand command) {
		//Validate that ruleDefinitionIds is set, ignore notes are retrieved for a given ruleDefinitionId:
		ValidationUtils.assertNotNull(command.getRuleDefinitionIds(), "A ruleDefinitionId is required to properly limit ignore notes.");

		AdvancedReadOnlyDAO<IdentityObject, Criteria> entityDao = null;
		BaseEntity<?> entity = null;
		String additionalScopeBeanFieldPath = null;
		BaseEntity<?> additionalScopeEntity = null;

		for (Short id : command.getRuleDefinitionIds()) {
			RuleDefinition ruleDefinition = getRuleDefinitionService().getRuleDefinition(id);
			if (ruleDefinition != null) {
				RuleCategory ruleCategory = ruleDefinition.getRuleCategory();
				entityDao = (AdvancedReadOnlyDAO<IdentityObject, Criteria>) getDaoLocator().locate(ruleCategory.getCategoryTable().getName());
				entity = (BaseEntity<?>) entityDao.findByPrimaryKey(command.getLinkedFkFieldId());
				ValidationUtils.assertNotNull(entity, "Unable to locate entity for table [" + ruleCategory.getCategoryTable().getName() + " and id [" + command.getLinkedFkFieldId() + "]");
				additionalScopeBeanFieldPath = ruleCategory.getAdditionalScopeBeanFieldPath();
				additionalScopeEntity = (BaseEntity<?>) BeanUtils.getPropertyValue(entity, additionalScopeBeanFieldPath);
				//We found our additional scope entity (e.g. client account, manager account, etc),
				//All rule definitions should be of the same category so we break at this point;
				break;
			}
		}

		ValidationUtils.assertNotNull(entityDao, "Failed to locate the DAO for the entity.");
		ValidationUtils.assertNotNull(additionalScopeBeanFieldPath, "Failed to find the additional scope bean field path.");
		ValidationUtils.assertNotNull(additionalScopeEntity, "Failed to locate the additional scope entity.");

		AuditableEntitySearchForm searchForm = new AuditableEntitySearchForm();
		searchForm.addSearchRestriction("createDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.addDays(entity.getCreateDate(), -command.getNumberOfDaysBack()));

		final String beanFieldName = additionalScopeBeanFieldPath + ".id";
		final Serializable beanFieldValue = additionalScopeEntity.getIdentity();
		//Now that we have the additionalScopeEntity we want to use the entityDao to search for the additionalScopeEntity
		List<IdentityObject> entitiesByAdditionalScopeEntitiesList = entityDao.findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				HibernateUtils.addEqualsRestriction(beanFieldName, beanFieldValue, criteria);
				super.configureCriteria(criteria);
			}
		});
		if (!CollectionUtils.isEmpty(entitiesByAdditionalScopeEntitiesList)) {
			//We've validated that the id was available when the command was issued; however, we want to remove and instead limit by the
			//entities located be searching for the additional scope entities for the past 'x' number of days rather than to the one specific entity.
			//e.g. We located the client account for a portfolio run, then searched for all portfolio runs for that client account for the past
			//7 days and now we want to search for any violations for those runs and get the distinct ignore notes for this rule definition.
			command.setLinkedFkFieldId(null);
			command.setLinkedFkFieldIds(entitiesByAdditionalScopeEntitiesList.stream().map(BeanUtils::getIdentityAsLong).collect(Collectors.toList()).toArray(new Long[entitiesByAdditionalScopeEntitiesList.size()]));
		}
		command.setIgnoreNoteNotNull(true);
		command.setOrderBy("createDate:desc#ruleDefinitionId:desc");
		return command;
	}


	private List<RuleViolation> filterRuleViolationList(List<RuleViolation> ruleViolationList) {
		Map<String, RuleViolation> filteredRuleViolationMap = new HashMap<>();
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			String key = ruleViolation.getRuleAssignment().getRuleDefinition().getId() + "_" + ruleViolation.getIgnoreNote();
			RuleViolation filteredRuleViolation = filteredRuleViolationMap.get(key);
			if (filteredRuleViolation == null) {
				filteredRuleViolationMap.put(key, ruleViolation);
			}
		}
		return new ArrayList<>(filteredRuleViolationMap.values());
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public AdvancedUpdatableDAO<RuleViolation, Criteria> getRuleViolationDAO() {
		return this.ruleViolationDAO;
	}


	public void setRuleViolationDAO(AdvancedUpdatableDAO<RuleViolation, Criteria> ruleViolationDAO) {
		this.ruleViolationDAO = ruleViolationDAO;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
