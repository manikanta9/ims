package com.clifton.rule.violation.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;

import java.util.List;


/**
 * The <code>RuleViolationExistsComparison</code> class represents a comparison that checks if the specified bean has any rule violations associated with it.
 * The code will ignore any violations that are snoozed or ignored.
 * <p>
 * NOTE: ALSO CHECKS SYSTEM WARNINGS FOR BACKWARDS COMPATIBILITY
 *
 * @author vgomelsky
 */
public class RuleViolationExistsComparison implements Comparison<IdentityObject> {

	/**
	 * If true, also checks ignored violations/warnings and returns true if any violation/warning is present.
	 * Defaults to false and checks only for violations/warnings that haven't been ignored or snoozed
	 */
	private boolean checkIgnored;

	/**
	 * If true, checks not ignorable violations only - i.e. those that cause the entity to be "Failed"
	 */
	private boolean checkNotIgnorableOnly;

	private DaoLocator daoLocator;

	private RuleViolationService ruleViolationService;


	/**
	 * Return false to reverse the comparison: check not exists
	 */
	protected boolean isExistsComparison() {
		return true;
	}


	@Override
	@SuppressWarnings("unchecked")
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		// get all violations/warnings that exist for the bean
		String tableName = getDaoLocator().locate((Class<IdentityObject>) bean.getClass()).getConfiguration().getTableName();
		Long beanId = BeanUtils.getIdentityAsLong(bean);
		ValidationUtils.assertNotNull(beanId, "Cannot check rule violations or system warnings for a bean that does not exist: " + bean, "id");

		List<RuleViolation> violationList = getRuleViolationService().getRuleViolationListByLinkedEntity(tableName, beanId);
		return evaluateResults(context, violationList);
	}


	protected boolean evaluateResults(ComparisonContext context, List<RuleViolation> violationList) {
		boolean result = !isExistsComparison();
		if (CollectionUtils.getSize(violationList) > 0) {
			if (this.checkIgnored) {
				result = isExistsComparison();
			}
			else {
				for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
					if (!violation.isIgnored() && !violation.isSnoozed() && (!isCheckNotIgnorableOnly() || !violation.getRuleAssignment().getRuleDefinition().isIgnorable())) {
						result = isExistsComparison();
						break;
					}
				}
			}
		}

		// record comparison result message
		if (context != null) {
			if (result) {
				context.recordTrueMessage(isExistsComparison() ? "(Rule Violations Present)" : "(No Rule Violations Found)");
			}
			else {
				context.recordFalseMessage(isExistsComparison() ? "(No Rule Violations Found)" : "(Rule Violations Present)");
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCheckNotIgnorableOnly() {
		return this.checkNotIgnorableOnly;
	}


	public void setCheckNotIgnorableOnly(boolean checkNotIgnorableOnly) {
		this.checkNotIgnorableOnly = checkNotIgnorableOnly;
	}


	public boolean isCheckIgnored() {
		return this.checkIgnored;
	}


	public void setCheckIgnored(boolean checkIgnored) {
		this.checkIgnored = checkIgnored;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
