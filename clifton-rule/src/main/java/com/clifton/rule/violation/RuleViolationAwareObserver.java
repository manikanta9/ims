package com.clifton.rule.violation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>RuleViolationAwareObserver</code> handles before/after events for beans that implement
 * the {@link RuleViolationAware} interface
 * <p>
 * DELETE - Delete All Associated RuleViolations
 * INSERT & UPDATE - If RuleViolationStatus is NULL will set it to {@link com.clifton.rule.violation.status.RuleViolationStatus} UNPROCESSED
 * UPDATE - If current RuleViolationStatus.IsProcessed flag = true, will retrieve list of violations for the entity and validate violation status matches violations, i.e. Can't be Processed Success if there are any violations
 *
 * @author Mary Anderson
 */
@Component
public class RuleViolationAwareObserver<T extends RuleViolationAware> extends BaseDaoEventObserver<T> {

	private RuleViolationService ruleViolationService;
	private RuleViolationStatusService ruleViolationStatusService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		if (!event.isDelete()) {
			// If Violation Status is missing, set it to UNPROCESSED
			if (bean.getViolationStatus() == null) {
				bean.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.UNPROCESSED));
			}

			// no need to do anything on insert because the entity does not exist and violations that reference this entity cannot exist either
			if (event.isUpdate() && bean.getViolationStatus().isProcessed()) {
				// SET TO APPROPRIATE STATUS BASED ON CURRENT VIOLATIONS
				List<RuleViolation> violationList = getRuleViolationService().getRuleViolationListByLinkedEntity(dao.getConfiguration().getTableName(), BeanUtils.getIdentityAsLong(bean));
				bean.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationUtil.getRuleViolationStatus(violationList)));
			}
		}
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null && event.isDelete()) {
			getRuleViolationService().deleteRuleViolationListByLinkedEntity(dao.getConfiguration().getTableName(), BeanUtils.getIdentityAsLong(bean));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////                   Getter & Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}
}
