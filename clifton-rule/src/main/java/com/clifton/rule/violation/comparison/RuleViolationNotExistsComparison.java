package com.clifton.rule.violation.comparison;


/**
 * The <code>RuleViolationNotExistsComparison</code> class represents a comparison that checks if the specified bean has NO rule violations associated with it.
 * <p>
 * NOTE: ALSO CHECKS FOR WARNINGS FOR BACKWARDS COMPATIBILITY
 *
 * @author manderson
 */
public class RuleViolationNotExistsComparison extends RuleViolationExistsComparison {

	@Override
	protected boolean isExistsComparison() {
		return false;
	}
}
