package com.clifton.rule.violation;

import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The {@link RuleViolationCommandAndAssignmentSecureMethodResolver} returns security resource for rule category if set - otherwise security resource associated with the table associated with the linked table
 * <p>
 * As long as there is a violation id, then will pull this information off of its rule definition.
 * If there is no id value, then check for at String[] of ruleDefinitionIds, and retrieve the first rule definition.
 * The Category will be the same for all rule definitions in a given sections, so we only need to retrieve the firt.
 * If neither of these values exist - then will require write access to the linked table name property.  Would be rare and only when ignoring ALL violations for an entity.
 *
 * @author Mary Anderson
 */
@Component
public class RuleViolationCommandAndAssignmentSecureMethodResolver implements SecureMethodSecurityResourceResolver {

	private RuleDefinitionService ruleDefinitionService;
	private RuleViolationService ruleViolationService;
	private SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSecurityResourceName(@SuppressWarnings("unused") Method method, Map<String, ?> config) {
		// As long as we can retrieve a ruleDefinition either from a violation, or a definition id - use that to determine security
		RuleDefinition definition = getRuleDefinition(method, config);
		if (definition != null) {
			if (definition.getRuleCategory().getSecurityResource() != null) {
				return definition.getRuleCategory().getSecurityResource().getName();
			}
			if (definition.getRuleCategory().getCategoryTable().getSecurityResource() != null) {
				return definition.getRuleCategory().getCategoryTable().getSecurityResource().getName();
			}
		}
		// Otherwise, use the linkedTableName parameter
		String tableName = MapUtils.getParameterAsString("linkedTableName", config);
		if (!StringUtils.isEmpty(tableName)) {
			SystemTable table = getSystemSchemaService().getSystemTableByName(tableName);
			if (table.getSecurityResource() != null) {
				return table.getSecurityResource().getName();
			}
		}
		return null;
	}


	private RuleDefinition getRuleDefinition(Method method, Map<String, ?> config) {
		RuleDefinition ruleDefinition = null;
		if ("saveRuleAssignment".equals(method.getName())) {
			Short ruleDefinitionId = MapUtils.getParameterAsShort("ruleDefinition.id", config, 0);
			AssertUtils.assertNotNull(ruleDefinitionId, "Could not get Rule Definition ID from method " + method.getName());
			return getRuleDefinitionService().getRuleDefinition(ruleDefinitionId);
		}
		if ("deleteRuleAssignment".equals(method.getName())) {
			Integer ruleAssignmentId = MapUtils.getParameterAsInteger("id", config);
			AssertUtils.assertNotNull(ruleAssignmentId, "Could not get Rule Assignment ID from method " + method.getName());
			return getRuleDefinitionService().getRuleAssignment(ruleAssignmentId).getRuleDefinition();
		}
		Map<String, Object> valueMap = getValueMapFromJson(MapUtils.getParameterAsString("beanList", config));
		if (valueMap != null) {
			Long violationId = MapUtils.getParameterAsLong("id", valueMap);
			if (violationId != null) {
				RuleViolation ruleViolation = getRuleViolationService().getRuleViolation(violationId);
				ruleDefinition = ruleViolation != null ? ruleViolation.getRuleAssignment().getRuleDefinition() : null;
			}
			else {
				Short ruleDefinitionId = MapUtils.getParameterAsShort("ruleDefinitionIds", valueMap, 0);
				if (ruleDefinitionId != null) {
					ruleDefinition = getRuleDefinitionService().getRuleDefinition(ruleDefinitionId);
				}
			}
			return ruleDefinition;
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	private Map<String, Object> getValueMapFromJson(String json) {
		try {
			Map<String, Object> valueMap = new HashMap<>();
			JsonStringToMapConverter converter = new JsonStringToMapConverter();
			Map<String, Object> objectMap = converter.convert(json);
			List<Object> dataList = (List<Object>) objectMap.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
			for (Object obj : CollectionUtils.getIterable(dataList)) {
				if (obj instanceof Map) {
					valueMap.putAll((Map<String, Object>) obj);
				}
			}
			return valueMap;
		}
		catch (Exception e) {
			//Invalid json
			return null;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
