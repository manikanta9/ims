package com.clifton.rule.violation.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.rule.violation.RuleViolation;
import org.springframework.stereotype.Component;


/**
 * The <code>RuleViolationByEntityCache</code> class provides caching and retrieval from cache of RuleViolation objects
 * by table and linked entity fkFieldId.
 *
 * @author apopp
 */
@Component
public class RuleViolationByEntityCache extends SelfRegisteringCompositeKeyDaoListCache<RuleViolation, Short, Long> {

	@Override
	protected String getBeanKey1Property() {
		return "ruleAssignment.ruleDefinition.ruleCategory.categoryTable.id";
	}


	@Override
	protected String getBeanKey2Property() {
		return "linkedFkFieldId";
	}


	@Override
	protected Short getBeanKey1Value(RuleViolation bean) {
		if (bean != null && bean.getRuleAssignment() != null && bean.getRuleAssignment().getRuleDefinition() != null && bean.getRuleAssignment().getRuleDefinition().getRuleCategory() != null) {
			return BeanUtils.getBeanIdentity(bean.getRuleAssignment().getRuleDefinition().getRuleCategory().getCategoryTable());
		}
		return null;
	}


	@Override
	protected Long getBeanKey2Value(RuleViolation bean) {
		if (bean != null) {
			return bean.getLinkedFkFieldId();
		}
		return null;
	}
}
