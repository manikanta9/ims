package com.clifton.rule.violation.status;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The RuleViolationStatus class defines all possible statuses for an entity that is {@link com.clifton.rule.violation.RuleViolationAware}.
 *
 * @author manderson
 */
@CacheByName
public class RuleViolationStatus extends NamedEntity<Short> {

	/**
	 * Indicates whether the rules have been processed for the entity and the status is set based on results.
	 */
	private boolean processed;
	/**
	 * Indicates that processing was completed successfully and either no violations exist or all violations are ignorable (may or may not have been ignored).
	 */
	private boolean readyForUse;
	private boolean unresolvedViolationsExist;

	private int order;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	public enum RuleViolationStatusNames {
		UNPROCESSED, PROCESSED_FAILED, PROCESSED_IGNORED, PROCESSED_VIOLATIONS, PROCESSED_SUCCESS;
	}


	public RuleViolationStatusNames getRuleViolationStatusName() {
		return RuleViolationStatusNames.valueOf(getName());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isSuccess() {
		return (isProcessed() && isReadyForUse() && !isUnresolvedViolationsExist());
	}


	public boolean isProcessed() {
		return this.processed;
	}


	public void setProcessed(boolean processed) {
		this.processed = processed;
	}


	public boolean isReadyForUse() {
		return this.readyForUse;
	}


	public void setReadyForUse(boolean readyForUse) {
		this.readyForUse = readyForUse;
	}


	public boolean isUnresolvedViolationsExist() {
		return this.unresolvedViolationsExist;
	}


	public void setUnresolvedViolationsExist(boolean unresolvedViolationsExist) {
		this.unresolvedViolationsExist = unresolvedViolationsExist;
	}


	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
