package com.clifton.rule.violation.snooze;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author StevenF
 */
public interface RuleViolationSnoozeService {

	@RequestMapping("ruleViolationSnoozeByCommand")
	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver")
	public void snoozeRuleViolationByCommand(BeanListCommand<RuleViolationSnoozeCommand> command);


	@RequestMapping("ruleViolationUnsnoozeByCommand")
	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver")
	public void unsnoozeRuleViolationByCommand(BeanListCommand<RuleViolationSnoozeCommand> command);
}
