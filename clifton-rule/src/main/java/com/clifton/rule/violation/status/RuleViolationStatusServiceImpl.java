package com.clifton.rule.violation.status;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class RuleViolationStatusServiceImpl implements RuleViolationStatusService {

	private AdvancedReadOnlyDAO<RuleViolationStatus, Criteria> ruleViolationStatusDAO;
	private DaoNamedEntityCache<RuleViolationStatus> ruleViolationStatusCache;


	////////////////////////////////////////////////////////////////////////////////
	/////////          System Warning Status Business Methods            ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public RuleViolationStatus getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames name) {
		if (name == null) {
			throw new ValidationException("getRuleViolationStatusByName requires RuleViolationStatusNames name parameter");
		}
		return getRuleViolationStatusCache().getBeanForKeyValueStrict(getRuleViolationStatusDAO(), name.name());
	}


	@Override
	public List<RuleViolationStatus> getRuleViolationStatusList() {
		return getRuleViolationStatusDAO().findAll();
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////                   Getter & Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<RuleViolationStatus, Criteria> getRuleViolationStatusDAO() {
		return this.ruleViolationStatusDAO;
	}


	public void setRuleViolationStatusDAO(AdvancedReadOnlyDAO<RuleViolationStatus, Criteria> ruleViolationStatusDAO) {
		this.ruleViolationStatusDAO = ruleViolationStatusDAO;
	}


	public DaoNamedEntityCache<RuleViolationStatus> getRuleViolationStatusCache() {
		return this.ruleViolationStatusCache;
	}


	public void setRuleViolationStatusCache(DaoNamedEntityCache<RuleViolationStatus> ruleViolationStatusCache) {
		this.ruleViolationStatusCache = ruleViolationStatusCache;
	}
}
