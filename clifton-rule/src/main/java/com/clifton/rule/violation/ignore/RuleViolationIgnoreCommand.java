package com.clifton.rule.violation.ignore;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.search.RuleViolationSearchForm;


/**
 * The <code>RuleViolationIgnoreCommand</code> object is used to control how ignoring rule violations is handled.
 * If allows for controlled validation of the parameters and creation of the search form so that violations can not
 * be accidentally ignored en masse due to possible bugs in the UI or direct manipulation of the service call.
 *
 * @author StevenF
 */
public class RuleViolationIgnoreCommand extends RuleViolationSearchForm {

	/**
	 * Determines the number of days back that should be searched for additional scope entities for the given entity
	 * when populating the search form for ruleViolations/ignore notes.
	 */
	private int numberOfDaysBack = 7;
	/**
	 * includeIgnoreRelatedByField can be used as a "shortcut" ignoring similar violations across a group of entities
	 * Example: Trades that are part of a Trade Group.  If there is a warning on one that Trade Date is in the Future, would like to ignore the same warning on all Trades in the group
	 * includeIgnoreRelatedByField: tradeGroup.id the TradeGroupID value for the current Trade we are looking at.
	 */
	private String includeIgnoreRelatedByField;


	/////////////////////////////////////////////////////////////////////////////


	/**
	 * The validate method is called during instantiation of the <code>HibernateSearchFormConfigurer</code> when the method 'configureSearchForm()'
	 * is called, which in turn calls 'SearchUtils.validateRestrictionList(searchForm.getRestrictionList(), this.restrictionDefinitionMap, searchForm)'
	 * which then calls the validate method, by extending the searchForm and using the already defined fields and implementing the validate command
	 * we can reduce the amount of redundant code by still providing the same level of security and validation and configuration the command object offers.
	 */
	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getLinkedTableId(), "A linkedTableId is required to properly determine security.");
		Long[] fkFieldIds = getLinkedFkFieldIds() != null ? getLinkedFkFieldIds() : ArrayUtils.createArray(getLinkedFkFieldId());
		ValidationUtils.assertNotNull(fkFieldIds, "At least one linkedFkFieldId is required to properly limit violations to specific entities.");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public int getNumberOfDaysBack() {
		return this.numberOfDaysBack;
	}


	public void setNumberOfDaysBack(int numberOfDaysBack) {
		this.numberOfDaysBack = numberOfDaysBack;
	}


	public String getIncludeIgnoreRelatedByField() {
		return this.includeIgnoreRelatedByField;
	}


	public void setIncludeIgnoreRelatedByField(String includeIgnoreRelatedByField) {
		this.includeIgnoreRelatedByField = includeIgnoreRelatedByField;
	}
}
