package com.clifton.rule.violation.ignore;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.rule.violation.RuleViolation;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author StevenF
 */
public interface RuleViolationIgnoreService {

	/**
	 * Marks Violation as Ignored for the specified command object
	 */
	@RequestMapping("ruleViolationIgnoreByCommand")
	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver")
	public void ignoreRuleViolationByCommand(BeanListCommand<RuleViolationIgnoreCommand> command);


	/**
	 * Marks all Ignorable Violations as Ignored for the specified command object
	 */
	@RequestMapping("ruleViolationListIgnore")
	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver")
	public void ignoreRuleViolationList(BeanListCommand<RuleViolationIgnoreCommand> command);


	////////////////////////////////////////////////////////////////////////////
	//////          Rule Violation Ignore Note Business Methods           //////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver")
	public List<RuleViolation> getRuleViolationIgnoreNoteList(RuleViolationIgnoreCommand command);
}
