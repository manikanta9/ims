package com.clifton.rule.violation.snooze;

import java.util.Date;


/**
 * The <code>RuleViolationSnoozeCommand</code> object is used to control how ignoring rule violations is handled.
 * If allows for controlled validation of the parameters and creation of the search form so that violations can not
 * be accidentally ignored en masse due to possible bugs in the UI or direct manipulation of the service call.
 *
 * @author StevenF
 */
public class RuleViolationSnoozeCommand {

	private long id;

	private int numberOfDays;

	private Date startDate;

	private String snoozeNote;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public long getId() {
		return this.id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getNumberOfDays() {
		return this.numberOfDays;
	}


	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public String getSnoozeNote() {
		return this.snoozeNote;
	}


	public void setSnoozeNote(String snoozeNote) {
		this.snoozeNote = snoozeNote;
	}
}
