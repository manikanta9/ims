package com.clifton.rule.violation;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.system.schema.SystemTable;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * The <code>RuleViolationService</code> ...
 *
 * @author Mary Anderson
 */
public interface RuleViolationService {

	@DoNotAddRequestMapping
	public RuleViolation createRuleViolation(EntityConfig entityConfig, Serializable linkedFkFieldId);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolation(EntityConfig entityConfig, Serializable linkedFkFieldId, Map<String, Object> contextValuesMap);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationWithCause(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable causeFkFieldId);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationWithCause(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationWithCause(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap);


	public RuleViolation createRuleViolationWithEntity(EntityConfig entityConfig, Long linkedFkFieldId, Long entityFkFieldId, Map<String, Object> contextValuesMap);


	public RuleViolation createRuleViolationWithEntityAndCause(EntityConfig entityConfig, Long linkedFkFieldId, Long entityFkFieldId, Long causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationWithNoteTemplate(EntityConfig entityConfig, Serializable linkedFkFieldId, Serializable entityFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> contextValuesMap, String violationNoteTemplateOverride, String ruleViolationCodeTemplate);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationSystemDefined(String violationNote, String ruleDefinitionName, Serializable linkedFkFieldId);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationSystemDefinedWithCause(String violationNote, String ruleDefinitionName, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable);


	@DoNotAddRequestMapping
	public RuleViolation createRuleViolationSystemDefinedWithCauseUsingTemplate(String ruleDefinitionName, Serializable linkedFkFieldId, Serializable causeFkFieldId, SystemTable causeTable, Map<String, Object> templateContextValuesMap);


	public RuleViolation getRuleViolation(long id);


	public List<RuleViolation> getRuleViolationList(RuleViolationSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<RuleViolation> getRuleViolationListByLinkedEntity(String tableName, Serializable fkFieldId);


	@DoNotAddRequestMapping
	public String getRuleViolationCodeByLinkedEntity(String tableName, Serializable fkFieldId);


	@DoNotAddRequestMapping
	public List<RuleViolation> getRuleViolationListByLinkedEntityAndDefinition(String linkedTableName, Serializable linkedFkFieldId, String definitionName);


	@DoNotAddRequestMapping
	public List<RuleViolation> getRuleViolationListByLinkedEntityAndDefinition(Short linkedTableId, Serializable linkedFkFieldId, String definitionName);


	/**
	 * Retrieves existingList for the entity and definition and saves changes (see updateRuleViolationList)
	 */
	@DoNotAddRequestMapping
	public List<RuleViolation> updateRuleViolationListForEntityAndDefinition(List<RuleViolation> newList, String linkedTableName, Serializable linkedFkFieldId, String definitionName);


	@DoNotAddRequestMapping
	public List<RuleViolation> filterViolationListForDupesAndInactive(List<RuleViolation> list);


	/**
	 * If newList is empty = all existing violations are deleted.
	 * If existingList is empty = all new violations are added.
	 * If both are populated, newList is updated with proper id values and ignore properties.
	 * Violations are considered to be equal based on the overridden equals method in RuleViolation class
	 */
	@DoNotAddRequestMapping
	public List<RuleViolation> updateRuleViolationList(List<RuleViolation> newList, List<RuleViolation> existingList);


	@SecureMethod(dynamicSecurityResourceBeanPath = "ruleAssignment.ruleDefinition.ruleCategory.securityResource.name", dynamicTableNameBeanPath = "ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name")
	public RuleViolation saveRuleViolation(RuleViolation bean);


	@SecureMethod(dynamicSecurityResourceBeanPath = "ruleAssignment.ruleDefinition.ruleCategory.securityResource.name", dynamicTableNameBeanPath = "ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name")
	public RuleViolation saveRuleViolationManual(RuleViolation ruleViolation);


	@DoNotAddRequestMapping
	public RuleViolation saveRuleViolationFromException(String ruleDefinitionName, Serializable linkedFkFieldId, ValidationExceptionWithCause exception);


	@DoNotAddRequestMapping
	public void deleteRuleViolation(long id);


	@SecureMethod(dtoClass = RuleViolation.class, dynamicSecurityResourceBeanPath = "ruleAssignment.ruleDefinition.ruleCategory.securityResource.name", dynamicTableNameBeanPath = "ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name", permissions = SecurityPermission.PERMISSION_WRITE)
	public void deleteRuleViolationManual(long id);


	@DoNotAddRequestMapping
	public void deleteRuleViolationListByLinkedEntity(String tableName, long fkFieldId);


	@DoNotAddRequestMapping
	public void deleteRuleViolationListByLinkedEntityNotIgnorable(String categoryName, long fkFieldId);
}


