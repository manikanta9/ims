package com.clifton.rule.violation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>RuleViolationCodeAwareObserver</code> handles before/after events for beans that implement
 * the {@link RuleViolationCodeAware} interface
 *
 * @author AbhinayaM
 */
@Component
public class RuleViolationCodeAwareObserver<T extends RuleViolationCodeAware> extends BaseDaoEventObserver<T> {

	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Since this is only used by OMS right now which uses transaction isolation level, need to reset the violation code AFTER the transaction, so we capture the current list of violations
	 */
	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null && event.isUpdate()) {
			String newRuleViolationCode = StringUtils.formatStringUpToNCharsWithDots(getRuleViolationService().getRuleViolationCodeByLinkedEntity(dao.getConfiguration().getTableName(), BeanUtils.getIdentityAsLong(bean)), 50);
			if (!StringUtils.isEqual(newRuleViolationCode, bean.getRuleViolationCodes())) {
				bean.setRuleViolationCodes(newRuleViolationCode);
				((UpdatableDAO<T>) dao).save(bean);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getters and Setters                             ////////
	////////////////////////////////////////////////////////////////////////////


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
