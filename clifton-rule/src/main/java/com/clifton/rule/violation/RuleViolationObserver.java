package com.clifton.rule.violation;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Component;


/**
 * The <code>RuleViolationObserver</code> validates ignoring violations, also after ignored, updates the linked entity
 * so if all violations were ignored, the warning status is changed to PROCESSED_IGNORED
 * <p>
 * Inserts & Updates
 * <p>
 * On Inserts - if the rule definition is automatically ignore on create, will auto flag the violation as ignored
 *
 * @author Mary Anderson
 */
@Component
public class RuleViolationObserver extends SelfRegisteringDaoObserver<RuleViolation> {

	private DaoLocator daoLocator;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<RuleViolation> dao, DaoEventTypes event, RuleViolation bean) {
		RuleDefinition ruleDefinition = bean.getRuleAssignment().getRuleDefinition();
		if (event.isUpdate()) {
			// Get original bean so we have it for after
			getOriginalBean(dao, bean);
		}
		else if (ruleDefinition.isAutomaticIgnoreOnCreate()) {
			bean.setIgnored(true);
		}

		// Validate Ignored
		if (bean.isIgnored()) {
			if (!ruleDefinition.isIgnorable()) {
				throw new ValidationException("Rule Violations for Definition [" + ruleDefinition.getLabel() + "] cannot be marked as ignored.");
			}
			if (ruleDefinition.isIgnoreNoteRequired()) {
				ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getIgnoreNote()), "Rule Violations for Definition [" + ruleDefinition.getLabel() + "] require a comment when marked as ignored.");
			}
			if (event.isUpdate() && ruleDefinition.getIgnoreCondition() != null) {
				RuleViolation originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !originalBean.isIgnored()) {
					validateIgnoreCondition(bean);
				}
			}
		}
		// If violation note is over 4000 characters, truncate it
		bean.setViolationNote(StringUtils.formatStringUpToNCharsWithDots(bean.getViolationNote(), DataTypes.DESCRIPTION_LONG.getLength(), true));
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<RuleViolation> dao, DaoEventTypes event, RuleViolation bean, Throwable e) {
		// Only update if no error
		if (e == null) {
			boolean updateBean = false;
			if (event.isUpdate()) {
				RuleViolation originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && originalBean.isIgnored() != bean.isIgnored()) {
					updateBean = true;
				}
			}
			else if (event.isInsert() && bean.isIgnored()) {
				updateBean = true;
			}
			if (updateBean) {
				UpdatableDAO<IdentityObject> linkedDao = (UpdatableDAO<IdentityObject>) getDaoLocator().locate(bean.getRuleAssignment().getRuleDefinition().getRuleCategory().getCategoryTable().getName());
				IdentityObject linkedBean = linkedDao.findByPrimaryKey(bean.getLinkedFkFieldId());
				linkedDao.save(linkedBean);
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Helper Methods                 ////////////////
	/////////////////////////////////////////////////////////////////////////////


	private void validateIgnoreCondition(RuleViolation violation) {
		// Unless Specified to Always Use Linked Entity, Ignore Conditions Will Pass Cause Entity First if Available
		IdentityObject bean = (violation.getRuleAssignment().getRuleDefinition().isUseLinkedEntityForIgnoreCondition() ? null : getCauseEntity(violation));
		if (bean == null) {
			// Otherwise Linked Entity
			bean = getLinkedEntity(violation);
		}
		String error = getSystemConditionEvaluationHandler().getConditionFalseMessage(violation.getRuleAssignment().getRuleDefinition().getIgnoreCondition(), bean);
		if (!StringUtils.isEmpty(error)) {
			throw new ValidationException("You cannot ignore this violation because: [" + error + "].");
		}
	}


	private IdentityObject getLinkedEntity(RuleViolation violation) {
		return getDaoLocator().locate(violation.getRuleAssignment().getRuleDefinition().getRuleCategory().getCategoryTable().getName()).findByPrimaryKey(violation.getLinkedFkFieldId());
	}


	private IdentityObject getCauseEntity(RuleViolation violation) {
		// The condition should be passed the bean that the warning was caused by and if no cause, then linked to.
		if (violation.getViolationCauseTable() != null) {
			if (violation.getCauseFkFieldId() != null) {
				return getDaoLocator().locate(violation.getViolationCauseTable().getName()).findByPrimaryKey(violation.getCauseFkFieldId());
			}
		}
		return null;
	}
	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
