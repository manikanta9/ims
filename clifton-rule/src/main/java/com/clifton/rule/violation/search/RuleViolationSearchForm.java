package com.clifton.rule.violation.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class RuleViolationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Long id;

	@SearchField(searchField = "id")
	private Long[] ids;

	@SearchField(searchFieldPath = "ruleAssignment", searchField = "additionalScopeLabel")
	private String additionalScopeLabel;

	@SearchField(searchFieldPath = "ruleAssignment", searchField = "entityLabel")
	private String entityLabel;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "ruleCategory.id", sortField = "ruleCategory.name")
	private Short categoryId;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "ruleCategory.name", comparisonConditions = ComparisonConditions.EQUALS)
	private String categoryNameEquals;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition.ruleCategory.categoryTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String linkedTableName; // Note SearchForm is intercepted and changes to use linkedTableId

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition.ruleCategory", searchField = "categoryTable.id")
	private Short linkedTableId;

	@SearchField
	private Long linkedFkFieldId;

	@SearchField(searchField = "linkedFkFieldId", comparisonConditions = ComparisonConditions.IN)
	private Long[] linkedFkFieldIds;

	@SearchField(searchField = "causeTable.id")
	private Short causeTableId;

	@SearchField
	private Long causeFkFieldId;

	@SearchField(searchField = "ruleAssignment.id")
	private Integer ruleAssignmentId;

	@SearchField(searchFieldPath = "ruleAssignment", searchField = "ruleDefinition.id")
	private Short ruleDefinitionId;

	@SearchField(searchFieldPath = "ruleAssignment", searchField = "ruleDefinition.id")
	private Short[] ruleDefinitionIds;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "name")
	private String ruleDefinitionName;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String ruleDefinitionNameEquals;

	@SearchField(searchFieldPath = "ruleAssignment", searchField = "snoozed")
	private Boolean snoozed;

	//CUSTOM FILTER
	private Boolean snoozedTodayOrNotSnoozed;

	//CUSTOM FILTER
	//Only used if snoozedTodayOrNotSnoozed is set
	private Date snoozedStartDate;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "priority.id")
	private Short priorityId;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "priority.order")
	private Integer priorityOrder;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "order")
	private Integer order;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "preProcess")
	private Boolean preProcess;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "systemDefined")
	private Boolean systemDefined;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "evaluatorBean.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean evaluatorBeanPopulated;


	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "manual")
	private Boolean manual;

	@SearchField(searchField = "violationNote,ignoreNote")
	private String searchPattern;

	@SearchField
	private String violationNote;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "ignorable")
	private Boolean ignorable;

	@SearchField
	private Boolean ignored;

	@SearchField
	private String ignoreNote;

	@SearchField(searchField = "ignoreNote", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean ignoreNoteNotNull;

	@SearchField(searchFieldPath = "ruleAssignment.ruleDefinition", searchField = "ignoreNoteRequired")
	private Boolean ignoreNoteRequired;

	@SearchField
	private Boolean cancelledLinkedEntity;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getLinkedTableName() {
		return this.linkedTableName;
	}


	public void setLinkedTableName(String linkedTableName) {
		this.linkedTableName = linkedTableName;
	}


	public Short getLinkedTableId() {
		return this.linkedTableId;
	}


	public void setLinkedTableId(Short linkedTableId) {
		this.linkedTableId = linkedTableId;
	}


	public Long getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(Long linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}


	public Short getCauseTableId() {
		return this.causeTableId;
	}


	public void setCauseTableId(Short causeTableId) {
		this.causeTableId = causeTableId;
	}


	public Long getCauseFkFieldId() {
		return this.causeFkFieldId;
	}


	public void setCauseFkFieldId(Long causeFkFieldId) {
		this.causeFkFieldId = causeFkFieldId;
	}


	public Short getRuleDefinitionId() {
		return this.ruleDefinitionId;
	}


	public void setRuleDefinitionId(Short ruleDefinitionId) {
		this.ruleDefinitionId = ruleDefinitionId;
	}


	public String getRuleDefinitionName() {
		return this.ruleDefinitionName;
	}


	public void setRuleDefinitionName(String ruleDefinitionName) {
		this.ruleDefinitionName = ruleDefinitionName;
	}


	public String getRuleDefinitionNameEquals() {
		return this.ruleDefinitionNameEquals;
	}


	public void setRuleDefinitionNameEquals(String ruleDefinitionNameEquals) {
		this.ruleDefinitionNameEquals = ruleDefinitionNameEquals;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Integer getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public String getViolationNote() {
		return this.violationNote;
	}


	public void setViolationNote(String violationNote) {
		this.violationNote = violationNote;
	}


	public Boolean getIgnorable() {
		return this.ignorable;
	}


	public void setIgnorable(Boolean ignorable) {
		this.ignorable = ignorable;
	}


	public Boolean getIgnored() {
		return this.ignored;
	}


	public void setIgnored(Boolean ignored) {
		this.ignored = ignored;
	}


	public String getIgnoreNote() {
		return this.ignoreNote;
	}


	public void setIgnoreNote(String ignoreNote) {
		this.ignoreNote = ignoreNote;
	}


	public Boolean getIgnoreNoteNotNull() {
		return this.ignoreNoteNotNull;
	}


	public void setIgnoreNoteNotNull(Boolean ignoreNoteNotNull) {
		this.ignoreNoteNotNull = ignoreNoteNotNull;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Boolean getIgnoreNoteRequired() {
		return this.ignoreNoteRequired;
	}


	public void setIgnoreNoteRequired(Boolean ignoreNoteRequired) {
		this.ignoreNoteRequired = ignoreNoteRequired;
	}


	public Long[] getLinkedFkFieldIds() {
		return this.linkedFkFieldIds;
	}


	public void setLinkedFkFieldIds(Long[] linkedFkFieldIds) {
		this.linkedFkFieldIds = linkedFkFieldIds;
	}


	public Boolean getPreProcess() {
		return this.preProcess;
	}


	public void setPreProcess(Boolean preProcess) {
		this.preProcess = preProcess;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getManual() {
		return this.manual;
	}


	public void setManual(Boolean manual) {
		this.manual = manual;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Boolean getEvaluatorBeanPopulated() {
		return this.evaluatorBeanPopulated;
	}


	public void setEvaluatorBeanPopulated(Boolean evaluatorBeanPopulated) {
		this.evaluatorBeanPopulated = evaluatorBeanPopulated;
	}


	public String getCategoryNameEquals() {
		return this.categoryNameEquals;
	}


	public void setCategoryNameEquals(String categoryNameEquals) {
		this.categoryNameEquals = categoryNameEquals;
	}


	public Long[] getIds() {
		return this.ids;
	}


	public void setIds(Long[] ids) {
		this.ids = ids;
	}


	public Integer getRuleAssignmentId() {
		return this.ruleAssignmentId;
	}


	public void setRuleAssignmentId(Integer ruleAssignmentId) {
		this.ruleAssignmentId = ruleAssignmentId;
	}


	public Short[] getRuleDefinitionIds() {
		return this.ruleDefinitionIds;
	}


	public void setRuleDefinitionIds(Short[] ruleDefinitionIds) {
		this.ruleDefinitionIds = ruleDefinitionIds;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getAdditionalScopeLabel() {
		return this.additionalScopeLabel;
	}


	public void setAdditionalScopeLabel(String additionalScopeLabel) {
		this.additionalScopeLabel = additionalScopeLabel;
	}


	public String getEntityLabel() {
		return this.entityLabel;
	}


	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}


	public Boolean getSnoozed() {
		return this.snoozed;
	}


	public void setSnoozed(Boolean snoozed) {
		this.snoozed = snoozed;
	}


	public Boolean getSnoozedTodayOrNotSnoozed() {
		return this.snoozedTodayOrNotSnoozed;
	}


	public void setSnoozedTodayOrNotSnoozed(Boolean snoozedTodayOrNotSnoozed) {
		this.snoozedTodayOrNotSnoozed = snoozedTodayOrNotSnoozed;
	}


	public Date getSnoozedStartDate() {
		return this.snoozedStartDate;
	}


	public void setSnoozedStartDate(Date snoozedStartDate) {
		this.snoozedStartDate = snoozedStartDate;
	}


	public Boolean getCancelledLinkedEntity() {
		return this.cancelledLinkedEntity;
	}


	public void setCancelledLinkedEntity(Boolean cancelledLinkedEntity) {
		this.cancelledLinkedEntity = cancelledLinkedEntity;
	}
}
