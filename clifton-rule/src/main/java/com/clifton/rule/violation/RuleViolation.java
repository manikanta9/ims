package com.clifton.rule.violation;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The RuleViolation class represents a single instance of a rule violation.  The violation is linked
 * to corresponding entity and cause entity.
 *
 * @author vgomelsky
 */
public class RuleViolation extends BaseEntity<Long> {

	private RuleAssignment ruleAssignment;

	/**
	 * The linkedFkFieldId stores the ID of the applicable linked entity for a rule violation.
	 * This is the primaryKey of an entity in the categoryTable for the given assignment, definition and category
	 * e.g. PortfolioRun, CollateralBalance, Trade, etc.
	 */
	@SoftLinkField(tableBeanPropertyName = "ruleAssignment.ruleDefinition.ruleCategory.categoryTable")
	private long linkedFkFieldId;

	/**
	 * The entityFkFieldId is used to store the identity of an entity used as second level scope.
	 * This is the primaryKey of an entity in the entityTable for the given assignment and definition
	 * e.g. InvestmentAccount, InvestmentAccountAssetClass, etc.
	 * <p>
	 * In many cases, the entityFkFieldId may be the same as the causeFkFieldId, however, there are numerous
	 * definition in which the causeTable and entityTable do not match, and so we must store the entityFkFieldId
	 * separately.
	 * <p>
	 * -First level scope is derived from the linkedEntity by using the 'additionalScopeBeanFieldPath'
	 * defined on the ruleCategory.
	 */
	@SoftLinkField(tableBeanPropertyName = "ruleAssignment.ruleDefinition.entityTable")
	private Long entityFkFieldId;

	/**
	 * RuleViolation defines causeTable to allow for a given rule to override the causeTable that is defined on
	 * the RuleDefinition - this is most commonly used in the  <code>RuleViolationServer</code> by the 'saveRuleViolationFromException' method.
	 */
	private SystemTable causeTable;

	/**
	 * The causeFkFieldId stores the ID of the applicable entity that was the cause of a rule violation.
	 * This is the primaryKey of an entity in the causeTable for the given assignment and definition and in
	 * many cases, but not all, is the same as the entityFkFieldId
	 * e.g. InvestmentAccount, InvestmentAccountAssetClass, etc.
	 */
	@SoftLinkField(tableBeanPropertyName = "causeTable")
	private Long causeFkFieldId;

	private String violationNote;

	private boolean ignored;
	private String ignoreNote;

	/**
	 * Each Rule Violation is linked via {@link #linkedFkFieldId} to the corresponding entity defined by the Task Category.
	 * If the entity is cancelled in its life cycle (Cancelled Trade), it is still useful to preserve corresponding Rule Violations because the
	 * cancellation may have been due to a specific Rule Violation. This field should be set to true in those cases (usually via Workflow Transition Action)
	 * to help with filtering of Rule Violations: want to focus on active violations and not those that were cancelled.
	 */
	private boolean cancelledLinkedEntity;

	/**
	 * For each Rule Definition, we may have a Rule Code (Optional)
	 * with a 1 or 2 digit character that represents that rule
	 */
	private String ruleViolationCode;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	//Convenience method
	public boolean isSnoozed() {
		return getRuleAssignment() != null && getRuleAssignment().isSnoozed();
	}


	@Override
	public boolean equals(Object o) {
		// Two Rule Violations are Equal if
		// 1. Types Are Equal
		// 2. Both Have NO Cause Table, or the Same Cause Table
		// 3. If the Same Cause Table, then both have the same Cause FK FieldID OR same message
		// 4. Both have the same linkedFkFieldId value
		if (!(o instanceof RuleViolation)) {
			return false;
		}
		RuleViolation oBean = (RuleViolation) o;
		if (this.getLinkedFkFieldId() == oBean.getLinkedFkFieldId() && MathUtils.isEqual(this.getEntityFkFieldId(), oBean.getEntityFkFieldId())) {
			RuleDefinition ruleDefinition = getRuleAssignment() != null ? getRuleAssignment().getRuleDefinition() : null;
			RuleDefinition originalRuleDefinition = oBean.getRuleAssignment() != null ? oBean.getRuleAssignment().getRuleDefinition() : null;
			if (ruleDefinition == originalRuleDefinition || (ruleDefinition != null && ruleDefinition.equals(originalRuleDefinition))) {
				if (ruleDefinition != null && !ruleDefinition.isUseViolationNoteFilteringOnly()) {
					if (this.getViolationCauseTable() == null && oBean.getViolationCauseTable() == null) {
						return true;
					}
					if (this.getViolationCauseTable() != null && this.getViolationCauseTable().equals(oBean.getViolationCauseTable())) {
						if (MathUtils.isEqual(this.getCauseFkFieldId(), oBean.getCauseFkFieldId())) {
							return true;
						}
						// If cause id is different but message is the same - then considered equal
						if (StringUtils.isEqual(StringUtils.trim(this.getViolationNote()), StringUtils.trim(oBean.getViolationNote()))) {
							return true;
						}
					}
				}
				else {
					if (StringUtils.isEqual(StringUtils.trim(this.getViolationNote()), StringUtils.trim(oBean.getViolationNote()))) {
						return true;
					}
				}
			}
		}
		return false;
	}


	public SystemTable getViolationCauseTable() {
		if (getCauseTable() != null) {
			return getCauseTable();
		}
		else if (getRuleAssignment().getRuleDefinition() != null) {
			return getRuleAssignment().getRuleDefinition().getCauseTable();
		}
		return null;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append('{');
		result.append(getViolationNote());
		if (isIgnored()) {
			result.append(", Ignored");
			if (!StringUtils.isEmpty(getIgnoreNote())) {
				result.append(": ").append(getIgnoreNote());
			}
		}
		result.append('}');
		return result.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleAssignment getRuleAssignment() {
		return this.ruleAssignment;
	}


	public void setRuleAssignment(RuleAssignment ruleAssignment) {
		this.ruleAssignment = ruleAssignment;
	}


	public long getLinkedFkFieldId() {
		return this.linkedFkFieldId;
	}


	public void setLinkedFkFieldId(long linkedFkFieldId) {
		this.linkedFkFieldId = linkedFkFieldId;
	}


	public Long getEntityFkFieldId() {
		return this.entityFkFieldId;
	}


	public void setEntityFkFieldId(Long entityFkFieldId) {
		this.entityFkFieldId = entityFkFieldId;
	}


	public SystemTable getCauseTable() {
		return this.causeTable;
	}


	public void setCauseTable(SystemTable causeTable) {
		this.causeTable = causeTable;
	}


	public Long getCauseFkFieldId() {
		return this.causeFkFieldId;
	}


	public void setCauseFkFieldId(Long causeFkFieldId) {
		this.causeFkFieldId = causeFkFieldId;
	}


	public String getViolationNote() {
		return this.violationNote;
	}


	public void setViolationNote(String violationNote) {
		this.violationNote = violationNote;
	}


	public boolean isIgnored() {
		return this.ignored;
	}


	public void setIgnored(boolean ignored) {
		this.ignored = ignored;
	}


	public String getIgnoreNote() {
		return this.ignoreNote;
	}


	public void setIgnoreNote(String ignoreNote) {
		this.ignoreNote = ignoreNote;
	}


	public boolean isCancelledLinkedEntity() {
		return this.cancelledLinkedEntity;
	}


	public void setCancelledLinkedEntity(boolean cancelledLinkedEntity) {
		this.cancelledLinkedEntity = cancelledLinkedEntity;
	}


	public String getRuleViolationCode() {
		return this.ruleViolationCode;
	}


	public void setRuleViolationCode(String ruleViolationCode) {
		this.ruleViolationCode = ruleViolationCode;
	}
}
