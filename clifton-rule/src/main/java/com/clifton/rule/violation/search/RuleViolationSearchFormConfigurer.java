package com.clifton.rule.violation.search;

import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.Date;


/**
 * @author StevenF
 */
public class RuleViolationSearchFormConfigurer extends HibernateSearchFormConfigurer {

	private final RuleViolationSearchForm ruleViolationSearchForm;


	public RuleViolationSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);
		this.ruleViolationSearchForm = (RuleViolationSearchForm) searchForm;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		RuleViolationSearchForm searchForm = this.ruleViolationSearchForm;
		if (searchForm.getSnoozedTodayOrNotSnoozed() != null) {
			Date startDate = searchForm.getSnoozedStartDate() != null ? searchForm.getSnoozedStartDate() : new Date();
			if (BooleanUtils.isTrue(searchForm.getSnoozedTodayOrNotSnoozed())) {
				//Include items that are not snoozed, or items that have been snoozed today.
				String pathAlias = getPathAlias("ruleAssignment", criteria);
				criteria.add(
						Restrictions.or(
								Restrictions.eq(pathAlias + ".snoozed", false),
								Restrictions.and(
										Restrictions.eq(pathAlias + ".snoozed", true),
										Restrictions.eq(pathAlias + ".startDate", startDate)
								)
						)
				);
			}
			else {
				//Include items that are snoozed, but not snoozed today.
				String pathAlias = getPathAlias("ruleAssignment", criteria);
				criteria.add(
						Restrictions.and(
								Restrictions.eq(pathAlias + ".snoozed", true),
								Restrictions.ne(pathAlias + ".startDate", startDate)
						)
				);
			}
			//Remove snoozed from the global scope of the query
			searchForm.setSnoozed(null);
			searchForm.removeSearchRestriction("snoozed");
		}
		//Configure any additional criteria now that we've handled our custom criteria
		super.configureCriteria(criteria);
	}
}
