package com.clifton.rule.retriever;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.RuleScope;
import com.clifton.rule.definition.cache.RuleAssignmentCache;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author vgomelsky
 */
@Service
public class RuleRetrieverServiceImpl implements RuleRetrieverService {

	private DaoLocator daoLocator;

	private RuleAssignmentCache ruleAssignmentCache;
	private RuleDefinitionService ruleDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleAssignment> getRuleAssignmentListForAdditionalScopeEntity(RuleAssignmentSearchForm searchForm) {
		// used to display PortfolioRun rules for a Client Account
		if (searchForm.getCategoryId() == null) {
			throw new ValidationException("Category Selection is Required");
		}
		RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategory(searchForm.getCategoryId());
		String additionalScopeLabel = ruleCategory.getAdditionalScopeLabel();
		if (additionalScopeLabel == null) {
			additionalScopeLabel = "Additional Entity ID";
		}
		if (searchForm.getAdditionalScopeFkFieldId() == null) {
			throw new ValidationException(additionalScopeLabel + " Selection is Required");
		}
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("ruleDefinitionName:asc");
		}
		// NOTE: IF WE USE CACHING OF ASSIGNMENTS FOR CATEGORY AND ADDITIONAL ENTITY ID = CAN USE THAT CACHED LIST OF IDS TO PASS TO THE SEARCH FORM AND
		// ADD WHERE ASSIGNMENT ID IN (PRE-FILTERED LIST OF ASSIGNMENTS).  THAT MAY NOT BE WORTHWHILE AS ACTIVE ON DATE FILTER CAN BE DIFFERENT AND WE'D PROBABLY ONLY CACHE THOSE ACTIVE NOW
		List<RuleAssignment> assignmentList = getRuleDefinitionService().getRuleAssignmentList(searchForm);
		if (!CollectionUtils.isEmpty(assignmentList)) {
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(ruleCategory.getAdditionalScopeTable().getName());
			IdentityObject entity = dao.findByPrimaryKey(searchForm.getAdditionalScopeFkFieldId());
			assignmentList = filterRuleAssignmentListForAdditionalScopeEntity(assignmentList, entity);
			assignmentList = BeanUtils.sortWithFunctions(assignmentList, CollectionUtils.createList(
					ruleAssignment -> ruleAssignment.getRuleDefinition().getOrder(),
					ruleAssignment -> ruleAssignment.getRuleDefinition().getName(),
					RuleAssignment::getAdditionalScopeFkFieldId,
					RuleAssignment::getEntityFkFieldId),
					CollectionUtils.createList(true, true, true, true));
		}
		return assignmentList;
	}


	private List<RuleAssignment> filterRuleAssignmentListForAdditionalScopeEntity(List<RuleAssignment> assignmentList, IdentityObject additionalScopeEntity) {

		// used to display PortfolioRun rules for a Client Account
		List<RuleAssignment> filteredList = new ArrayList<>();
		// Scope selections are small, so evaluate each one only once and re-use
		Map<Short, String> ruleScopeValueMap = new HashMap<>();
		Long entityId = BeanUtils.getIdentityAsLong(additionalScopeEntity);
		for (RuleAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
			if (assignment.getAdditionalScopeFkFieldId() == null || MathUtils.isEqual(assignment.getAdditionalScopeFkFieldId(), entityId)) {
				// If explicit - keep it
				if (assignment.getAdditionalScopeFkFieldId() != null || assignment.getRuleDefinition().getRuleScope() == null) {
					filteredList.add(assignment);
				}
				// Else Filter on Rule Scope
				else {
					RuleScope ruleScope = assignment.getRuleDefinition().getRuleScope();
					if (!ruleScopeValueMap.containsKey(ruleScope.getId())) {
						String ruleScopePath = ruleScope.getScopeBeanFieldPath();
						Object entityValue = BeanUtils.getPropertyValue(additionalScopeEntity, ruleScopePath);
						ruleScopeValueMap.put(ruleScope.getId(), (entityValue == null ? null : entityValue.toString()));
					}
					if (StringUtils.isEqual(ruleScope.getScopeBeanFieldValue(), ruleScopeValueMap.get(ruleScope.getId()))) {
						filteredList.add(assignment);
					}
				}
			}
		}
		return filteredList;
	}


	@Override
	public List<RuleConfig> getRuleConfigListForEntity(String categoryName, long entityId, Boolean preProcess) {
		RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategoryByName(categoryName);
		IdentityObject entity = getDaoLocator().locate(ruleCategory.getCategoryTable().getName()).findByPrimaryKey(entityId);
		IdentityObject additionalScopeEntity = (IdentityObject) BeanUtils.getPropertyValue(entity, ruleCategory.getAdditionalScopeBeanFieldPath());
		return getRuleConfigListForAdditionalScopeEntity(ruleCategory, additionalScopeEntity, preProcess, null);
	}


	@Override
	public RuleConfig getRuleConfigListForAdditionalScopeEntityAndDefinition(RuleDefinition ruleDefinition, IdentityObject additionalScopeEntity, Boolean preProcess) {
		return CollectionUtils.getOnlyElement(getRuleConfigListForAdditionalScopeEntity(ruleDefinition.getRuleCategory(), additionalScopeEntity, preProcess, null).stream().filter(ruleConfig -> ruleConfig.getRuleDefinitionId().equals(ruleDefinition.getId())).collect(Collectors.toList()));
	}


	//entry point for 'doExecuteRules'
	@Override
	public List<RuleConfig> getRuleConfigListForAdditionalScopeEntity(RuleCategory ruleCategory, IdentityObject additionalScopeEntity, Boolean preProcess, LocalDate activeOnDate) {
		ValidationUtils.assertNotNull(additionalScopeEntity, "Additional Scope Entity cannot be null.");
		if (activeOnDate == null) {
			activeOnDate = LocalDate.now();
		}
		List<RuleAssignment> ruleAssignments = getRuleAssignmentCache().getRuleAssignmentList(ruleCategory.getId(), activeOnDate);
		if (ruleAssignments == null) {
			RuleAssignmentSearchForm ruleAssignmentSearchForm = new RuleAssignmentSearchForm();
			ruleAssignmentSearchForm.setCategoryId(ruleCategory.getId());
			ruleAssignmentSearchForm.setActiveOnDate(DateUtils.asUtilDate(activeOnDate));
			ruleAssignments = getRuleDefinitionService().getRuleAssignmentList(ruleAssignmentSearchForm);
			getRuleAssignmentCache().setRuleAssignmentMap(ruleCategory.getId(), activeOnDate, ruleAssignments);
		}

		ruleAssignments = filterRuleAssignmentListForAdditionalScopeEntity(ruleAssignments, additionalScopeEntity);

		Stream<RuleAssignment> ruleAssignmentStream = ruleAssignments.stream();
		if (preProcess != null) {
			ruleAssignmentStream = ruleAssignmentStream.filter(ruleAssignment -> ruleAssignment.getRuleDefinition().isPreProcess() == preProcess);
		}

		return getRuleConfigListForRuleAssignments(ruleAssignmentStream.collect(Collectors.toList()), true, activeOnDate);
	}


	private List<RuleConfig> getRuleConfigListForRuleAssignments(List<RuleAssignment> assignments, Boolean evaluatorBeanPopulated, LocalDate activeOnDate) {
		// Temporarily store configs in map by rule definition
		Map<Short, RuleConfig> ruleConfigMap = new LinkedHashMap<>();
		if (activeOnDate == null) {
			activeOnDate = LocalDate.now();
		}
		for (RuleAssignment assignment : CollectionUtils.getIterable(assignments)) {
			RuleDefinition ruleDefinition = assignment.getRuleDefinition();
			if (checkEvaluateRuleDefinition(ruleDefinition, evaluatorBeanPopulated)) {
				RuleConfig ruleConfig = ruleConfigMap.get(ruleDefinition.getId());
				if (ruleConfig == null) {
					ruleConfig = new RuleConfig(assignment);
				}
				LocalDate endDate = DateUtils.asLocalDate(assignment.getEndDate());
				LocalDate startDate = DateUtils.asLocalDate(assignment.getStartDate());
				//If the start and end dates are the same, then we exclude since an 'unSnoozed' assignment will set both dates the same.
				if (DateUtils.isDateBetween(activeOnDate, startDate, endDate) && isNullOrNotEqual(startDate, endDate)) {
					ruleConfig.addEntityConfig(new EntityConfig(assignment));
					ruleConfigMap.put(ruleDefinition.getId(), ruleConfig);
				}
			}
		}

		return new ArrayList<>(ruleConfigMap.values());
	}


	private boolean isNullOrNotEqual(LocalDate startDate, LocalDate endDate) {
		if (startDate == null || endDate == null) {
			return true;
		}
		return !startDate.isEqual(endDate);
	}


	private boolean checkEvaluateRuleDefinition(RuleDefinition ruleDefinition, Boolean evaluatorBeanPopulated) {
		if (ruleDefinition.isManual() || ruleDefinition.isInactive()) {
			return false;
		}
		if (evaluatorBeanPopulated != null && (ruleDefinition.getEvaluatorBean() == null) == evaluatorBeanPopulated) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////////////
	//   Rule Assignment Retriever for Specific Definition Helper Methods
	//  Useful for finding Min amount, Max Amount, Rule Amount, On/Off options for a particular definition and additional scope entity
	//  Currently only defined for Max Amount Retrieval since that is only use case right now
	////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the max amount from the rule config for a specific category/definition and additional scope entity.
	 * i.e. Synthetic Exposure Limit is defined by rules, but also used by code process - and UI - so for an account we want to easily retrieve the max value
	 * used by an account, or if the account doesn't use that option.
	 */
	@Override
	public BigDecimal getRuleConfigMaxAmountForAdditionalScopeEntityAndDefinition(String categoryName, String definitionName, long additionalScopeEntityId) {
		RuleConfig ruleConfig = getRuleConfigForAdditionalScopeEntityAndDefinition(categoryName, definitionName, additionalScopeEntityId);
		if (ruleConfig != null) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
			if (entityConfig != null && !entityConfig.isExcluded()) {
				return entityConfig.getMaxAmount();
			}
		}
		return null;
	}


	private RuleConfig getRuleConfigForAdditionalScopeEntityAndDefinition(String categoryName, String definitionName, long additionalScopeEntityId) {
		RuleCategory category = getRuleDefinitionService().getRuleCategoryByName(categoryName);
		RuleDefinition definition = getRuleDefinitionService().getRuleDefinitionByName(definitionName);
		ValidationUtils.assertTrue(category.equals(definition.getRuleCategory()), "Rule Definition [" + definitionName + "] belongs to category [" + definition.getRuleCategory().getName() + "], not expected category [" + categoryName + "]");

		RuleAssignmentSearchForm searchForm = new RuleAssignmentSearchForm();
		searchForm.setCategoryId(category.getId());
		searchForm.setRuleDefinitionId(definition.getId());
		searchForm.setAdditionalScopeFkFieldId(additionalScopeEntityId);

		return CollectionUtils.getOnlyElement(getRuleConfigListForRuleAssignments(getRuleAssignmentListForAdditionalScopeEntity(searchForm), null, null)); // Should be the only one in the list because we already filtered
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleAssignmentCache getRuleAssignmentCache() {
		return this.ruleAssignmentCache;
	}


	public void setRuleAssignmentCache(RuleAssignmentCache ruleAssignmentCache) {
		this.ruleAssignmentCache = ruleAssignmentCache;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
