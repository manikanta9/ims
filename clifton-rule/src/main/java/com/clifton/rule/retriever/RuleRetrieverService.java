package com.clifton.rule.retriever;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.evaluator.RuleConfig;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;


/**
 * RuleRetrieverService has customized methods to retrieve RuleAssignments, as well as RuleConfig objects used for rule evaluation
 * for linked entities, additional scope entity, etc.
 *
 * @author vgomelsky
 */
public interface RuleRetrieverService {

	/////////////////////////////////////////////////////////////////////
	////    Rule Assignment Retrieval Methods
	/////////////////////////////////////////////////////////////////////


	/**
	 * Used by UI to find all applicable {@link RuleAssignment} for required search form values: Category ID and Additional Scope Entity ID
	 * For example, view Portfolio Run Rules associated with a given client account
	 */
	@ModelAttribute("data")
	public List<RuleAssignment> getRuleAssignmentListForAdditionalScopeEntity(RuleAssignmentSearchForm searchForm);

	/////////////////////////////////////////////////////////////////////
	////    Rule Config Retrieval Methods - Used for Evaluation of Rules
	/////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public List<RuleConfig> getRuleConfigListForEntity(String categoryName, long entityId, Boolean preProcess);


	@DoNotAddRequestMapping
	public RuleConfig getRuleConfigListForAdditionalScopeEntityAndDefinition(RuleDefinition ruleDefinition, IdentityObject additionalScopeEntity, Boolean preProcess);


	@DoNotAddRequestMapping
	public List<RuleConfig> getRuleConfigListForAdditionalScopeEntity(RuleCategory ruleCategory, IdentityObject additionalScopeEntity, Boolean preProcess, LocalDate activeOnDate);

	////////////////////////////////////////////////////////////////////////////////////////
	//   Rule Assignment Amount Retrieval for Specific Definition Methods
	//  Useful for finding Min amount, Max Amount, Rule Amount, On/Off options for a particular definition and additional scope entity
	//  Currently only defined for Max Amount Retrieval since that is only use case right now
	////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the max amount from the rule config for a specific category/definition and additional scope entity.
	 * i.e. Synthetic Exposure Limit is defined by rules, but also used by code process - and UI - so for an account we want to easily retrieve the max value
	 * used by an account, or if the account doesn't use that option.
	 */
	@ResponseBody
	@SecureMethod(dtoClass = RuleAssignment.class)
	public BigDecimal getRuleConfigMaxAmountForAdditionalScopeEntityAndDefinition(String categoryName, String definitionName, long additionalScopeEntityId);
}
