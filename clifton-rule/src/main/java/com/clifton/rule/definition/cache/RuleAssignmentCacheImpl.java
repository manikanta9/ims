package com.clifton.rule.definition.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * This class caches the ruleConfig object in several tiers to allow for smart cache clearing.
 * e.g. the cache can be cleared entirely, by category, or by additionalScopeEntity.
 * The cache is initially built by the IMSPreLoadCacheJob that iterates all accounts and generates
 * their ruleConfig objects - see 'loadRuleConfigs' method in 'IMSPreLoadCache' class.
 * The pre-load cache is the only time the cache is cleared in its entirety.
 * <p>
 * This cache should only contain rule configs for assignments that are entity-specific
 * Non-entity specific rule assignments should be cached somewhere else.
 *
 * @author stevenf
 */
@Component
public class RuleAssignmentCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T>
		implements CustomCache<Short, Map<LocalDate, List<RuleAssignment>>>, RuleAssignmentCache {

	/*
	 *  The cacheHandler for ruleConfig cache consists of three levels to all for 'smart clearing' of cache.
	 *  If a rule definition or assignment is modified the cache can be cleared at the category level for a rule
	 *  definition change.  The second level in the map
	 *  is to prevent caching of conflicting rules (e.g. multiple assignments that are active on different dates,
	 *  without the LocalDate parameter those rules would overwrite each other.
	 *
	 *  The cache keys are as follows:
	 *  Short ruleCategoryId, LocalDate activeOnDate
	 *
	 */
	private CacheHandler<Short, Map<LocalDate, List<RuleAssignment>>> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	public List<RuleAssignment> getRuleAssignmentList(Short ruleCategoryId, LocalDate activeOnDate) {
		List<RuleAssignment> ruleAssignmentList = null;
		Map<LocalDate, List<RuleAssignment>> dateMap = getCacheHandler().get(getCacheName(), ruleCategoryId);
		if (dateMap != null) {
			ruleAssignmentList = dateMap.get(activeOnDate);
		}
		return ruleAssignmentList;
	}


	@Override
	public void setRuleAssignmentMap(Short ruleCategoryId, LocalDate activeOnDate, List<RuleAssignment> ruleAssignmentList) {
		Map<LocalDate, List<RuleAssignment>> localDateMap = getCacheHandler().get(getCacheName(), ruleCategoryId);
		if (localDateMap == null) {
			localDateMap = new ConcurrentHashMap<>();
		}
		localDateMap.put(activeOnDate, ruleAssignmentList);
		getCacheHandler().put(getCacheName(), ruleCategoryId, localDateMap);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                    Observer Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////////


	//Two observers are registered for ruleDefinitionDAO, and ruleAssignmentDAO
	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			Map<LocalDate, List<RuleAssignment>> dateMap = null;
			if (bean instanceof RuleDefinition) {
				//If a rule definition changes, just clear the whole category cache.
				dateMap = getCacheHandler().get(getCacheName(), ((RuleDefinition) bean).getRuleCategory().getId());
			}
			else if (bean instanceof RuleAssignment) {
				dateMap = getCacheHandler().get(getCacheName(), ((RuleAssignment) bean).getRuleDefinition().getRuleCategory().getId());
			}
			if (dateMap != null) {
				dateMap.clear();
			}
		}
	}


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}

	////////////////////////////////////////////////////////////////////////////
	//////                   Getter and Setter Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<Short, Map<LocalDate, List<RuleAssignment>>> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Short, Map<LocalDate, List<RuleAssignment>>> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
