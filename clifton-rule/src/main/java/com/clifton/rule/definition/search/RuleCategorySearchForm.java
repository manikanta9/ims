package com.clifton.rule.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class RuleCategorySearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField
	private String additionalScopeBeanFieldPath;

	@SearchField
	private String additionalScopeLabel;

	@SearchField(searchField = "additionalScopeTable.id")
	private Short additionalScopeTableId;

	@SearchField(searchFieldPath = "additionalScopeTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String additionalScopeTableNameEquals;

	@SearchField(searchField = "categoryTable.id")
	private Short categoryTableId;

	@SearchField(searchFieldPath = "categoryTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String categoryTableName; // Note SearchForm is intercepted and changes to use categoryTableId

	@SearchField(searchField = "securityResource.id")
	private Integer securityResourceId;

	@SearchField(searchFieldPath = "securityResource", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String securityResourceName; // Note SearchForm is intercepted and changes to use categoryTableId

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////



	public Short getCategoryTableId() {
		return this.categoryTableId;
	}


	public void setCategoryTableId(Short categoryTableId) {
		this.categoryTableId = categoryTableId;
	}


	public String getCategoryTableName() {
		return this.categoryTableName;
	}


	public void setCategoryTableName(String categoryTableName) {
		this.categoryTableName = categoryTableName;
	}


	public String getAdditionalScopeBeanFieldPath() {
		return this.additionalScopeBeanFieldPath;
	}


	public void setAdditionalScopeBeanFieldPath(String additionalScopeBeanFieldPath) {
		this.additionalScopeBeanFieldPath = additionalScopeBeanFieldPath;
	}


	public Short getAdditionalScopeTableId() {
		return this.additionalScopeTableId;
	}


	public void setAdditionalScopeTableId(Short additionalScopeTableId) {
		this.additionalScopeTableId = additionalScopeTableId;
	}


	public String getAdditionalScopeTableNameEquals() {
		return this.additionalScopeTableNameEquals;
	}


	public void setAdditionalScopeTableNameEquals(String additionalScopeTableNameEquals) {
		this.additionalScopeTableNameEquals = additionalScopeTableNameEquals;
	}


	public String getAdditionalScopeLabel() {
		return this.additionalScopeLabel;
	}


	public void setAdditionalScopeLabel(String additionalScopeLabel) {
		this.additionalScopeLabel = additionalScopeLabel;
	}


	public Integer getSecurityResourceId() {
		return this.securityResourceId;
	}


	public void setSecurityResourceId(Integer securityResourceId) {
		this.securityResourceId = securityResourceId;
	}


	public String getSecurityResourceName() {
		return this.securityResourceName;
	}


	public void setSecurityResourceName(String securityResourceName) {
		this.securityResourceName = securityResourceName;
	}
}
