package com.clifton.rule.definition.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleDefinition;
import org.springframework.stereotype.Component;


/**
 * The <code>RuleDefinitionValidator</code> validates
 * 1. changes to system defined Rule Definitions
 * 2. Ignorable options/Manual Options
 *
 * @author Mary Anderson
 */
@Component
public class RuleDefinitionValidator extends SelfRegisteringDaoValidator<RuleDefinition> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(RuleDefinition bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("System Defined Rule Definitions cannot be deleted.");
			}
			if (config.isInsert()) {
				throw new FieldValidationException("System Defined Rule Definitions cannot be added.", "systemDefined");
			}
		}
		if (config.isUpdate()) {
			RuleDefinition originalBean = getOriginalBean(bean);
			ValidationUtils.assertTrue(originalBean.isSystemDefined() == bean.isSystemDefined(), "System Defined field cannot be edited.", "systemDefined");
			if (originalBean.isNameSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "You cannot change the name for a Name System Defined Rule Definition.", "name");
			}
		}

		if (config.isInsert() || config.isUpdate()) {
			// Reset Ignore fields if not ignorable
			if (bean.isSystemDefined()) {
				bean.setNameSystemDefined(true);
			}

			// Reset Ignore fields if not ignorable
			if (!bean.isIgnorable()) {
				bean.setIgnoreCondition(null);
				bean.setIgnoreNoteRequired(false);
				bean.setAutomaticIgnoreOnCreate(false);
				bean.setUseLinkedEntityForIgnoreCondition(false);
			}
			// Reset Rule Evaluator if manual
			if (bean.isManual()) {
				bean.setEvaluatorBean(null);
				bean.setCauseTable(null);
			}
		}
	}
}
