package com.clifton.rule.definition;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.ArrayUtils;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.usedby.softlink.SoftLinkField;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The RuleAssignment class assigns a specific RuleDefinition to a specific entity.
 * The assignment may also specify rule specific configuration parameters: min/max amounts, etc.
 * <p>
 * Each RuleDefinition must have one default assignment (additionalScopeFkFieldId == null).
 * Additional assignments for the RuleDefinition can be used to exclude certain entities from that rule
 * or to specify different parameter values for the rule. For example, specific client account
 * may use different parameters than global defaults.
 *
 * @author vgomelsky
 */
public class RuleAssignment extends BaseEntity<Integer> implements LabeledObject, SystemEntityModifyConditionAware {

	private RuleDefinition ruleDefinition;

	/**
	 * Additional scope is defined by RuleCategory. For example, Client Account for PortfolioRun or Trade; Billing Definition for Billing Invoice.
	 */
	@SoftLinkField(tableBeanPropertyName = "ruleDefinition.ruleCategory.additionalScopeTable")
	private Long additionalScopeFkFieldId;
	private String additionalScopeLabel;

	/**
	 * Optional additional entity can be used by rule itself to apply business functionality to specific entities.
	 * For example, InvestmentReplication or InvestmentManagerAccount for a Portfolio Run
	 */
	@SoftLinkField(tableBeanPropertyName = "ruleDefinition.entityTable")
	private Long entityFkFieldId;
	private String entityLabel;

	/**
	 * Specifies whether this rule assignment should be included in or excluded from evaluation for the corresponding entity.
	 */
	private boolean excluded;

	// the data range when this rule assignment is valid: null value means start/end of time
	private Date endDate;
	private Date startDate;

	/**
	 * Specifies if this rule assignment is 'snoozing'; used to ignore a violation for a given period of time
	 * The time period is based on the startDate and endDate fields for the assignment.
	 */
	private boolean snoozed;

	/**
	 * The following fields apply only if the rule definition has labels defined for them
	 */
	private BigDecimal maxAmount;
	private BigDecimal minAmount;
	private BigDecimal ruleAmount;


	/**
	 * Used to add a note as to why an assignment is being overridden.
	 */
	private String note;

	////////////////////////////////////////////////////////////////////////////


	public RuleAssignment() {
		//force use of explicit constructor
	}


	//Copy Constructor used to 'clone' a ruleAssignment for use with snooze functionality
	public RuleAssignment(RuleAssignment config, String[] ignoredProperties) {
		BeanUtils.copyProperties(config, this, ArrayUtils.addAll(ignoredProperties, BeanUtils.AUDIT_PROPERTIES));
	}

	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder(getRuleDefinition().getName());
		if (getAdditionalScopeFkFieldId() != null) {
			label.append("; Scope [").append(getAdditionalScopeLabel()).append("]");
		}
		if (getEntityFkFieldId() != null) {
			label.append("; Entity [").append(getEntityLabel()).append("]");
		}
		if (getMinAmount() != null) {
			label.append("; ").append(getRuleDefinition().getMinAmountLabel()).append(" [").append(getMinAmount()).append("]");
		}
		if (getMaxAmount() != null) {
			label.append("; ").append(getRuleDefinition().getMaxAmountLabel()).append(" [").append(getMaxAmount()).append("]");
		}
		return label.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////  SystemEntityModifyConditionAware Interface Methods  ///////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getRuleDefinition().getEntityModifyCondition();
	}



	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Long getAdditionalScopeFkFieldId() {
		return this.additionalScopeFkFieldId;
	}


	public void setAdditionalScopeFkFieldId(Long additionalScopeFkFieldId) {
		this.additionalScopeFkFieldId = additionalScopeFkFieldId;
	}


	public String getAdditionalScopeLabel() {
		return this.additionalScopeLabel;
	}


	public void setAdditionalScopeLabel(String additionalScopeLabel) {
		this.additionalScopeLabel = additionalScopeLabel;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Long getEntityFkFieldId() {
		return this.entityFkFieldId;
	}


	public void setEntityFkFieldId(Long entityFkFieldId) {
		this.entityFkFieldId = entityFkFieldId;
	}


	public String getEntityLabel() {
		return this.entityLabel;
	}


	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}


	public boolean isExcluded() {
		return this.excluded;
	}


	public void setExcluded(boolean excluded) {
		this.excluded = excluded;
	}


	public BigDecimal getMaxAmount() {
		return this.maxAmount;
	}


	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}


	public BigDecimal getMinAmount() {
		return this.minAmount;
	}


	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}


	public BigDecimal getRuleAmount() {
		return this.ruleAmount;
	}


	public void setRuleAmount(BigDecimal ruleAmount) {
		this.ruleAmount = ruleAmount;
	}


	public RuleDefinition getRuleDefinition() {
		return this.ruleDefinition;
	}


	public void setRuleDefinition(RuleDefinition ruleDefinition) {
		this.ruleDefinition = ruleDefinition;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public boolean isSnoozed() {
		return this.snoozed;
	}


	public void setSnoozed(boolean snoozed) {
		this.snoozed = snoozed;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
