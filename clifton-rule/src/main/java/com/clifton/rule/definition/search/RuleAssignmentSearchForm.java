package com.clifton.rule.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.math.BigDecimal;


public class RuleAssignmentSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private Integer id;

	@SearchField
	private String note;

	@SearchField(searchFieldPath = "ruleDefinition", searchField = "ruleCategory.id", sortField = "ruleCategory.name")
	private Short categoryId;


	@SearchField(searchField = "ruleDefinition.id", sortField = "ruleDefinition.name")
	private Short ruleDefinitionId;

	@SearchField(searchField = "name", searchFieldPath = "ruleDefinition")
	private String ruleDefinitionName;

	@SearchField(searchField = "name", searchFieldPath = "ruleDefinition", comparisonConditions = ComparisonConditions.EQUALS)
	private String ruleDefinitionNameEquals;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Long additionalScopeFkFieldId;

	@SearchField(searchField = "additionalScopeFkFieldId", comparisonConditions = ComparisonConditions.EQUALS)
	private Long additionalScopeFkFieldIdEquals;

	@SearchField(searchField = "additionalScopeFkFieldId", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullAdditionalScopeFkFieldId;

	@SearchField
	private String additionalScopeLabel;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Long entityFkFieldId;

	@SearchField(searchField = "entityFkFieldId", comparisonConditions = ComparisonConditions.EQUALS)
	private Long entityFkFieldIdEquals;

	@SearchField(searchField = "entityFkFieldId", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullEntityFkFieldId;

	@SearchField
	private String entityLabel;

	@SearchField(searchField = "name", searchFieldPath = "ruleDefinition.entityTable")
	private String entityTableName;

	@SearchField(searchField = "name", searchFieldPath = "ruleDefinition.entityTable", comparisonConditions = ComparisonConditions.EQUALS)
	private String entityTableNameEquals;


	@SearchField
	private Boolean excluded;

	@SearchField(searchField = "additionalScopeFkFieldId", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean globalOnly;

	@SearchField(searchField = "ruleDefinition.inactive")
	private Boolean inactiveDefinition;

	@SearchField(searchField = "ruleDefinition.manual")
	private Boolean manualDefinition;

	@SearchField(searchField = "additionalScopeFkFieldId", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean overridesOnly;

	@SearchField(searchFieldPath = "ruleDefinition", searchField = "preProcess")
	private Boolean preProcessDefinition;

	@SearchField(searchFieldPath = "ruleDefinition", searchField = "systemDefined")
	private Boolean systemDefinedDefinition;

	@SearchField(searchFieldPath = "ruleDefinition", searchField = "evaluatorBean.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean evaluatorBeanPopulated;


	@SearchField(searchFieldPath = "ruleDefinition", searchField = "manual")
	private Boolean manual;

	@SearchField
	private Boolean snoozed;


	@SearchField
	private BigDecimal maxAmount;

	@SearchField
	private BigDecimal minAmount;

	@SearchField
	private BigDecimal ruleAmount;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Short getRuleDefinitionId() {
		return this.ruleDefinitionId;
	}


	public void setRuleDefinitionId(Short ruleDefinitionId) {
		this.ruleDefinitionId = ruleDefinitionId;
	}


	public String getRuleDefinitionName() {
		return this.ruleDefinitionName;
	}


	public void setRuleDefinitionName(String ruleDefinitionName) {
		this.ruleDefinitionName = ruleDefinitionName;
	}


	public Long getAdditionalScopeFkFieldId() {
		return this.additionalScopeFkFieldId;
	}


	public void setAdditionalScopeFkFieldId(Long additionalScopeFkFieldId) {
		this.additionalScopeFkFieldId = additionalScopeFkFieldId;
	}


	public Long getEntityFkFieldId() {
		return this.entityFkFieldId;
	}


	public void setEntityFkFieldId(Long entityFkFieldId) {
		this.entityFkFieldId = entityFkFieldId;
	}


	public String getAdditionalScopeLabel() {
		return this.additionalScopeLabel;
	}


	public void setAdditionalScopeLabel(String additionalScopeLabel) {
		this.additionalScopeLabel = additionalScopeLabel;
	}


	public String getEntityLabel() {
		return this.entityLabel;
	}


	public void setEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
	}


	public String getEntityTableName() {
		return this.entityTableName;
	}


	public void setEntityTableName(String entityTableName) {
		this.entityTableName = entityTableName;
	}


	public Boolean getExcluded() {
		return this.excluded;
	}


	public void setExcluded(Boolean excluded) {
		this.excluded = excluded;
	}


	public BigDecimal getMaxAmount() {
		return this.maxAmount;
	}


	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}


	public BigDecimal getMinAmount() {
		return this.minAmount;
	}


	public void setMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
	}


	public Short getCategoryId() {
		return this.categoryId;
	}


	public void setCategoryId(Short categoryId) {
		this.categoryId = categoryId;
	}


	public Boolean getGlobalOnly() {
		return this.globalOnly;
	}


	public void setGlobalOnly(Boolean globalOnly) {
		this.globalOnly = globalOnly;
	}


	public String getEntityTableNameEquals() {
		return this.entityTableNameEquals;
	}


	public void setEntityTableNameEquals(String entityTableNameEquals) {
		this.entityTableNameEquals = entityTableNameEquals;
	}


	public Boolean getOverridesOnly() {
		return this.overridesOnly;
	}


	public void setOverridesOnly(Boolean overridesOnly) {
		this.overridesOnly = overridesOnly;
	}


	public BigDecimal getRuleAmount() {
		return this.ruleAmount;
	}


	public void setRuleAmount(BigDecimal ruleAmount) {
		this.ruleAmount = ruleAmount;
	}


	public Boolean getInactiveDefinition() {
		return this.inactiveDefinition;
	}


	public void setInactiveDefinition(Boolean inactiveDefinition) {
		this.inactiveDefinition = inactiveDefinition;
	}


	public Boolean getManualDefinition() {
		return this.manualDefinition;
	}


	public void setManualDefinition(Boolean manualDefinition) {
		this.manualDefinition = manualDefinition;
	}


	public Boolean getSystemDefinedDefinition() {
		return this.systemDefinedDefinition;
	}


	public void setSystemDefinedDefinition(Boolean systemDefinedDefinition) {
		this.systemDefinedDefinition = systemDefinedDefinition;
	}


	public Boolean getPreProcessDefinition() {
		return this.preProcessDefinition;
	}


	public void setPreProcessDefinition(Boolean preProcessDefinition) {
		this.preProcessDefinition = preProcessDefinition;
	}


	public Boolean getEvaluatorBeanPopulated() {
		return this.evaluatorBeanPopulated;
	}


	public void setEvaluatorBeanPopulated(Boolean evaluatorBeanPopulated) {
		this.evaluatorBeanPopulated = evaluatorBeanPopulated;
	}


	public Boolean getNullAdditionalScopeFkFieldId() {
		return this.nullAdditionalScopeFkFieldId;
	}


	public void setNullAdditionalScopeFkFieldId(Boolean nullAdditionalScopeFkFieldId) {
		this.nullAdditionalScopeFkFieldId = nullAdditionalScopeFkFieldId;
	}


	public Boolean getNullEntityFkFieldId() {
		return this.nullEntityFkFieldId;
	}


	public void setNullEntityFkFieldId(Boolean nullEntityFkFieldId) {
		this.nullEntityFkFieldId = nullEntityFkFieldId;
	}


	public String getRuleDefinitionNameEquals() {
		return this.ruleDefinitionNameEquals;
	}


	public void setRuleDefinitionNameEquals(String ruleDefinitionNameEquals) {
		this.ruleDefinitionNameEquals = ruleDefinitionNameEquals;
	}


	public Boolean getManual() {
		return this.manual;
	}


	public void setManual(Boolean manual) {
		this.manual = manual;
	}


	public Boolean getSnoozed() {
		return this.snoozed;
	}


	public void setSnoozed(Boolean snoozed) {
		this.snoozed = snoozed;
	}


	public Long getAdditionalScopeFkFieldIdEquals() {
		return this.additionalScopeFkFieldIdEquals;
	}


	public void setAdditionalScopeFkFieldIdEquals(Long additionalScopeFkFieldIdEquals) {
		this.additionalScopeFkFieldIdEquals = additionalScopeFkFieldIdEquals;
	}


	public Long getEntityFkFieldIdEquals() {
		return this.entityFkFieldIdEquals;
	}


	public void setEntityFkFieldIdEquals(Long entityFkFieldIdEquals) {
		this.entityFkFieldIdEquals = entityFkFieldIdEquals;
	}
}
