package com.clifton.rule.definition;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.definition.search.RuleCategorySearchForm;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.rule.definition.search.RuleScopeSearchForm;
import com.clifton.rule.definition.validation.RuleDefinitionValidator;
import com.clifton.system.schema.SystemSchemaService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RuleDefinitionServiceImpl implements RuleDefinitionService {

	private AdvancedUpdatableDAO<RuleAssignment, Criteria> ruleAssignmentDAO;
	private AdvancedUpdatableDAO<RuleCategory, Criteria> ruleCategoryDAO;
	private AdvancedUpdatableDAO<RuleDefinition, Criteria> ruleDefinitionDAO;
	private AdvancedUpdatableDAO<RuleScope, Criteria> ruleScopeDAO;

	private DaoNamedEntityCache<RuleCategory> ruleCategoryCache;
	private DaoNamedEntityCache<RuleDefinition> ruleDefinitionCache;
	private DaoSingleKeyListCache<RuleScope, Short> ruleScopeListByCategoryCache;

	private SystemSchemaService systemSchemaService;

	/////////////////////////////////////////////////////////////////////////////
	////////////         RuleAssignment Business Methods         ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public RuleAssignment getRuleAssignment(int id) {
		return getRuleAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<RuleAssignment> getRuleAssignmentList(RuleAssignmentSearchForm searchForm) {
		return getRuleAssignmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * This is used for systemDefined and manual violations.
	 * There should only be a single global rule assignment for both of these cases
	 * So the function throws an error if more are found, which likely means the
	 * function is being used incorrectly.
	 */
	@Override
	public RuleAssignment getRuleAssignmentGlobalByDefinitionName(String ruleDefinitionName) {
		RuleAssignmentSearchForm searchForm = new RuleAssignmentSearchForm();
		searchForm.setRuleDefinitionNameEquals(ruleDefinitionName);
		searchForm.setNullAdditionalScopeFkFieldId(true);
		searchForm.setNullEntityFkFieldId(true);
		List<RuleAssignment> ruleAssignmentList = getRuleAssignmentList(searchForm);
		return CollectionUtils.getOnlyElement(ruleAssignmentList);
	}


	@Override
	public RuleAssignment saveRuleAssignment(RuleAssignment bean) {
		return getRuleAssignmentDAO().save(bean);
	}


	@Override
	public void deleteRuleAssignment(int id) {
		getRuleAssignmentDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////         RuleCategory Business Methods           ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public RuleCategory getRuleCategory(short id) {
		return getRuleCategoryDAO().findByPrimaryKey(id);
	}


	@Override
	public RuleCategory getRuleCategoryByName(String name) {
		return getRuleCategoryCache().getBeanForKeyValueStrict(getRuleCategoryDAO(), name);
	}


	@Override
	public List<RuleCategory> getRuleCategoryList(RuleCategorySearchForm searchForm) {
		if (!StringUtils.isEmpty(searchForm.getCategoryTableName())) {
			// use cached lookup to avoid extra join
			searchForm.setCategoryTableId(getSystemSchemaService().getSystemTableByName(searchForm.getCategoryTableName()).getId());
			searchForm.setCategoryTableName(null);
		}
		return getRuleCategoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public RuleCategory saveRuleCategory(RuleCategory bean) {
		return getRuleCategoryDAO().save(bean);
	}


	@Override
	public void deleteRuleCategory(short id) {
		getRuleCategoryDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////         RuleDefinition Business Methods          ///////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public RuleDefinition getRuleDefinition(short id) {
		return getRuleDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public RuleDefinition getRuleDefinitionByName(String definitionName) {
		return getRuleDefinitionCache().getBeanForKeyValueStrict(getRuleDefinitionDAO(), definitionName);
	}


	@Override
	public List<RuleDefinition> getRuleDefinitionList(RuleDefinitionSearchForm searchForm) {
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("ruleCategoryName:asc#name:asc");
		}
		return getRuleDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see RuleDefinitionValidator
	 */
	@Override
	public RuleDefinition saveRuleDefinition(RuleDefinition bean) {
		return getRuleDefinitionDAO().save(bean);
	}


	/**
	 * @see RuleDefinitionValidator
	 */
	@Override
	public void deleteRuleDefinition(short id) {
		getRuleDefinitionDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////           RuleScope Business Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public RuleScope getRuleScope(short id) {
		return getRuleScopeDAO().findByPrimaryKey(id);
	}


	@Override
	public List<RuleScope> getRuleScopeList(RuleScopeSearchForm searchForm) {
		return getRuleScopeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<RuleScope> getRuleScopeListByCategory(short ruleCategoryId) {
		return getRuleScopeListByCategoryCache().getBeanListForKeyValue(getRuleScopeDAO(), ruleCategoryId);
	}


	@Override
	public RuleScope saveRuleScope(RuleScope bean) {
		return getRuleScopeDAO().save(bean);
	}


	@Override
	public void deleteRuleScope(short id) {
		getRuleScopeDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<RuleAssignment, Criteria> getRuleAssignmentDAO() {
		return this.ruleAssignmentDAO;
	}


	public void setRuleAssignmentDAO(AdvancedUpdatableDAO<RuleAssignment, Criteria> ruleAssignmentDAO) {
		this.ruleAssignmentDAO = ruleAssignmentDAO;
	}


	public AdvancedUpdatableDAO<RuleCategory, Criteria> getRuleCategoryDAO() {
		return this.ruleCategoryDAO;
	}


	public void setRuleCategoryDAO(AdvancedUpdatableDAO<RuleCategory, Criteria> ruleCategoryDAO) {
		this.ruleCategoryDAO = ruleCategoryDAO;
	}


	public AdvancedUpdatableDAO<RuleDefinition, Criteria> getRuleDefinitionDAO() {
		return this.ruleDefinitionDAO;
	}


	public void setRuleDefinitionDAO(AdvancedUpdatableDAO<RuleDefinition, Criteria> ruleDefinitionDAO) {
		this.ruleDefinitionDAO = ruleDefinitionDAO;
	}


	public AdvancedUpdatableDAO<RuleScope, Criteria> getRuleScopeDAO() {
		return this.ruleScopeDAO;
	}


	public void setRuleScopeDAO(AdvancedUpdatableDAO<RuleScope, Criteria> ruleScopeDAO) {
		this.ruleScopeDAO = ruleScopeDAO;
	}


	public DaoNamedEntityCache<RuleCategory> getRuleCategoryCache() {
		return this.ruleCategoryCache;
	}


	public void setRuleCategoryCache(DaoNamedEntityCache<RuleCategory> ruleCategoryCache) {
		this.ruleCategoryCache = ruleCategoryCache;
	}


	public DaoNamedEntityCache<RuleDefinition> getRuleDefinitionCache() {
		return this.ruleDefinitionCache;
	}


	public void setRuleDefinitionCache(DaoNamedEntityCache<RuleDefinition> ruleDefinitionCache) {
		this.ruleDefinitionCache = ruleDefinitionCache;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public DaoSingleKeyListCache<RuleScope, Short> getRuleScopeListByCategoryCache() {
		return this.ruleScopeListByCategoryCache;
	}


	public void setRuleScopeListByCategoryCache(DaoSingleKeyListCache<RuleScope, Short> ruleScopeListByCategoryCache) {
		this.ruleScopeListByCategoryCache = ruleScopeListByCategoryCache;
	}
}
