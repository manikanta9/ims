package com.clifton.rule.definition.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class RuleScopeSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "ruleCategory.id")
	private Short ruleCategoryId;

	@SearchField
	private String scopeBeanFieldPath;

	@SearchField
	private String scopeBeanFieldValue;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getRuleCategoryId() {
		return this.ruleCategoryId;
	}


	public void setRuleCategoryId(Short ruleCategoryId) {
		this.ruleCategoryId = ruleCategoryId;
	}


	public String getScopeBeanFieldPath() {
		return this.scopeBeanFieldPath;
	}


	public void setScopeBeanFieldPath(String scopeBeanFieldPath) {
		this.scopeBeanFieldPath = scopeBeanFieldPath;
	}


	public String getScopeBeanFieldValue() {
		return this.scopeBeanFieldValue;
	}


	public void setScopeBeanFieldValue(String scopeBeanFieldValue) {
		this.scopeBeanFieldValue = scopeBeanFieldValue;
	}
}
