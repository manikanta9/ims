package com.clifton.rule.definition;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.definition.search.RuleCategorySearchForm;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.rule.definition.search.RuleScopeSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author vgomelsky
 */
public interface RuleDefinitionService {

	/////////////////////////////////////////////////////////////////////////////
	////////////         RuleAssignment Business Methods         ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleAssignment getRuleAssignment(int id);


	public List<RuleAssignment> getRuleAssignmentList(RuleAssignmentSearchForm searchForm);


	@RequestMapping("ruleAssignmentByDefinitionName")
	public RuleAssignment getRuleAssignmentGlobalByDefinitionName(String ruleDefinitionName);


	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver")
	public RuleAssignment saveRuleAssignment(RuleAssignment bean);


	@SecureMethod(securityResourceResolverBeanName = "ruleViolationCommandAndAssignmentSecureMethodResolver", permissions = SecurityPermission.PERMISSION_WRITE)
	public void deleteRuleAssignment(int id);

	/////////////////////////////////////////////////////////////////////////////
	////////////         RuleCategory Business Methods           ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleCategory getRuleCategory(short id);


	public RuleCategory getRuleCategoryByName(String name);


	public List<RuleCategory> getRuleCategoryList(RuleCategorySearchForm searchForm);


	public RuleCategory saveRuleCategory(RuleCategory bean);


	public void deleteRuleCategory(short id);

	/////////////////////////////////////////////////////////////////////////////
	////////////         RuleDefinition Business Methods          ///////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleDefinition getRuleDefinition(short id);


	public RuleDefinition getRuleDefinitionByName(String definitionName);


	public List<RuleDefinition> getRuleDefinitionList(RuleDefinitionSearchForm searchForm);


	public RuleDefinition saveRuleDefinition(RuleDefinition bean);


	public void deleteRuleDefinition(short id);

	/////////////////////////////////////////////////////////////////////////////
	////////////           RuleScope Business Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleScope getRuleScope(short id);


	public List<RuleScope> getRuleScopeList(RuleScopeSearchForm searchForm);


	public List<RuleScope> getRuleScopeListByCategory(short ruleCategoryId);


	public RuleScope saveRuleScope(RuleScope bean);


	public void deleteRuleScope(short id);
}
