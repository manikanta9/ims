package com.clifton.rule.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityNamedSearchForm;


public class RuleDefinitionSearchForm extends BaseAuditableEntityNamedSearchForm {

	@SearchField(searchField = "ruleCategory.id", sortField = "ruleCategory.name")
	private Short ruleCategoryId;

	@SearchField(searchField = "name", searchFieldPath = "ruleCategory")
	private String ruleCategoryName;

	@SearchField(searchField = "name", searchFieldPath = "ruleCategory", comparisonConditions = ComparisonConditions.EQUALS)
	private String ruleCategoryNameEquals;

	@SearchField(searchField = "ruleScope.id")
	private Short ruleScopeId;

	@SearchField(searchField = "ruleScope.name")
	private String ruleScopeName;

	@SearchField(searchField = "name", searchFieldPath = "ruleScope", comparisonConditions = ComparisonConditions.EQUALS)
	private String ruleScopeNameEquals;

	@SearchField(searchField = "name", searchFieldPath = "ruleScope", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL, leftJoin = true)
	private String[] ruleScopeNames;

	@SearchField(searchField = "name", searchFieldPath = "ruleScope", comparisonConditions = ComparisonConditions.IN)
	private String[] ruleScopeNamesStrict;

	@SearchField(searchField = "additionalScopeTable.id", searchFieldPath = "ruleCategory")
	private Short additionalScopeTableId;

	@SearchField(searchField = "name", searchFieldPath = "entityTable")
	private String entityTableName;

	@SearchField(searchField = "entityTable.id")
	private Short entityTableId;


	@SearchField(searchField = "causeTable.id")
	private Short causeTableId;

	@SearchField(searchFieldPath = "evaluatorBean", searchField = "name")
	private String evaluatorBeanName;

	@SearchField(searchField = "evaluatorBean.id")
	private Integer evaluatorBeanId;

	@SearchField
	private Boolean nameSystemDefined;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "evaluatorBean.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean evaluatorBeanPopulated;


	@SearchField
	private Boolean automaticIgnoreOnCreate;

	@SearchField
	private Boolean ignorable;

	@SearchField
	private Boolean ignoreNoteRequired;

	@SearchField(searchField = "ignoreCondition.id", sortField = "ignoreCondition.name")
	private Integer ignoreConditionId;

	@SearchField
	private Boolean useLinkedEntityForIgnoreCondition;

	@SearchField
	private Boolean inactive;

	@SearchField
	private Boolean manual;

	@SearchField
	private Boolean preProcess;

	@SearchField
	private Short maxSnoozeDays;

	@SearchField
	private String ruleAmountLabel;

	@SearchField
	private String minAmountLabel;

	@SearchField
	private String maxAmountLabel;

	@SearchField(searchField = "priority.id")
	private Short priorityId;

	@SearchField(searchField = "priority.order")
	private Integer priorityOrder;

	@SearchField
	private Integer order;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getRuleCategoryName() {
		return this.ruleCategoryName;
	}


	public void setRuleCategoryName(String ruleCategoryName) {
		this.ruleCategoryName = ruleCategoryName;
	}


	public String getEntityTableName() {
		return this.entityTableName;
	}


	public void setEntityTableName(String entityTableName) {
		this.entityTableName = entityTableName;
	}


	public Short getEntityTableId() {
		return this.entityTableId;
	}


	public void setEntityTableId(Short entityTableId) {
		this.entityTableId = entityTableId;
	}


	public String getEvaluatorBeanName() {
		return this.evaluatorBeanName;
	}


	public void setEvaluatorBeanName(String evaluatorBeanName) {
		this.evaluatorBeanName = evaluatorBeanName;
	}


	public Integer getEvaluatorBeanId() {
		return this.evaluatorBeanId;
	}


	public void setEvaluatorBeanId(Integer evaluatorBeanId) {
		this.evaluatorBeanId = evaluatorBeanId;
	}


	public Boolean getNameSystemDefined() {
		return this.nameSystemDefined;
	}


	public void setNameSystemDefined(Boolean nameSystemDefined) {
		this.nameSystemDefined = nameSystemDefined;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getAutomaticIgnoreOnCreate() {
		return this.automaticIgnoreOnCreate;
	}


	public void setAutomaticIgnoreOnCreate(Boolean automaticIgnoreOnCreate) {
		this.automaticIgnoreOnCreate = automaticIgnoreOnCreate;
	}


	public Boolean getIgnorable() {
		return this.ignorable;
	}


	public void setIgnorable(Boolean ignorable) {
		this.ignorable = ignorable;
	}


	public Boolean getIgnoreNoteRequired() {
		return this.ignoreNoteRequired;
	}


	public void setIgnoreNoteRequired(Boolean ignoreNoteRequired) {
		this.ignoreNoteRequired = ignoreNoteRequired;
	}


	public Boolean getInactive() {
		return this.inactive;
	}


	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	public Boolean getManual() {
		return this.manual;
	}


	public void setManual(Boolean manual) {
		this.manual = manual;
	}


	public Boolean getPreProcess() {
		return this.preProcess;
	}


	public void setPreProcess(Boolean preProcess) {
		this.preProcess = preProcess;
	}


	public Short getAdditionalScopeTableId() {
		return this.additionalScopeTableId;
	}


	public void setAdditionalScopeTableId(Short additionalScopeTableId) {
		this.additionalScopeTableId = additionalScopeTableId;
	}


	public Short getRuleCategoryId() {
		return this.ruleCategoryId;
	}


	public void setRuleCategoryId(Short ruleCategoryId) {
		this.ruleCategoryId = ruleCategoryId;
	}


	public Short getRuleScopeId() {
		return this.ruleScopeId;
	}


	public void setRuleScopeId(Short ruleScopeId) {
		this.ruleScopeId = ruleScopeId;
	}


	public String getRuleScopeName() {
		return this.ruleScopeName;
	}


	public void setRuleScopeName(String ruleScopeName) {
		this.ruleScopeName = ruleScopeName;
	}


	public String getRuleScopeNameEquals() {
		return this.ruleScopeNameEquals;
	}


	public void setRuleScopeNameEquals(String ruleScopeNameEquals) {
		this.ruleScopeNameEquals = ruleScopeNameEquals;
	}


	public String[] getRuleScopeNames() {
		return this.ruleScopeNames;
	}


	public void setRuleScopeNames(String[] ruleScopeNames) {
		this.ruleScopeNames = ruleScopeNames;
	}


	public String[] getRuleScopeNamesStrict() {
		return this.ruleScopeNamesStrict;
	}


	public void setRuleScopeNamesStrict(String[] ruleScopeNamesStrict) {
		this.ruleScopeNamesStrict = ruleScopeNamesStrict;
	}


	public Short getCauseTableId() {
		return this.causeTableId;
	}


	public void setCauseTableId(Short causeTableId) {
		this.causeTableId = causeTableId;
	}


	public Short getMaxSnoozeDays() {
		return this.maxSnoozeDays;
	}


	public void setMaxSnoozeDays(Short maxSnoozeDays) {
		this.maxSnoozeDays = maxSnoozeDays;
	}


	public String getRuleAmountLabel() {
		return this.ruleAmountLabel;
	}


	public void setRuleAmountLabel(String ruleAmountLabel) {
		this.ruleAmountLabel = ruleAmountLabel;
	}


	public String getMinAmountLabel() {
		return this.minAmountLabel;
	}


	public void setMinAmountLabel(String minAmountLabel) {
		this.minAmountLabel = minAmountLabel;
	}


	public String getMaxAmountLabel() {
		return this.maxAmountLabel;
	}


	public void setMaxAmountLabel(String maxAmountLabel) {
		this.maxAmountLabel = maxAmountLabel;
	}


	public Boolean getEvaluatorBeanPopulated() {
		return this.evaluatorBeanPopulated;
	}


	public void setEvaluatorBeanPopulated(Boolean evaluatorBeanPopulated) {
		this.evaluatorBeanPopulated = evaluatorBeanPopulated;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Integer getPriorityOrder() {
		return this.priorityOrder;
	}


	public void setPriorityOrder(Integer priorityOrder) {
		this.priorityOrder = priorityOrder;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}


	public String getRuleCategoryNameEquals() {
		return this.ruleCategoryNameEquals;
	}


	public void setRuleCategoryNameEquals(String ruleCategoryNameEquals) {
		this.ruleCategoryNameEquals = ruleCategoryNameEquals;
	}


	public Integer getIgnoreConditionId() {
		return this.ignoreConditionId;
	}


	public void setIgnoreConditionId(Integer ignoreConditionId) {
		this.ignoreConditionId = ignoreConditionId;
	}


	public Boolean getUseLinkedEntityForIgnoreCondition() {
		return this.useLinkedEntityForIgnoreCondition;
	}


	public void setUseLinkedEntityForIgnoreCondition(Boolean useLinkedEntityForIgnoreCondition) {
		this.useLinkedEntityForIgnoreCondition = useLinkedEntityForIgnoreCondition;
	}
}
