package com.clifton.rule.definition;

import com.clifton.core.beans.NamedEntity;
import com.clifton.system.bean.SystemBean;


/**
 * The RuleScope class identifies what entities from RuleCategory a specific RuleDefinition should apply.
 * For example, if the scope is not set on RuleDefinition, then the rule will apply to all entities.
 * Or the scope can limit to client accounts that have "LDI" service processing type.
 * "clientInvestmentAccount.businessService.type.processingType" = "LDI" (scopeBeanFieldPath = scopeBeanFieldValue)
 * <p>
 * There are 4 levels of rule scope specificity:
 * - Global scope is when no scope is specified for RuleDefinition and no RuleAssignment exists with AdditionalScopeFKFieldID or EntityFKFieldID populated (all Client Accounts)
 * - RuleDefinition scope is specified but no RuleAssignment exists with AdditionalScopeFKFieldID or EntityFKFieldID populated (only LDI Client Accounts)
 * - Additional Scope is specified via AdditionalScopeFKFieldID on RuleAssignment but EntityFKFieldID is NULL (specific Client Account only)
 * - Both AdditionalScopeFKFieldID and EntityFKFieldID are specified (specific Investment Manager Account for a specific Client Account only)
 *
 * @author vgomelsky
 */
public class RuleScope extends NamedEntity<Short> {

	private RuleCategory ruleCategory;

	// For example, serviceProcessingType.processingType = LDI
	private String scopeBeanFieldPath;
	private String scopeBeanFieldValue;

	private SystemBean ruleEvaluatorContextBean;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleCategory getRuleCategory() {
		return this.ruleCategory;
	}


	public void setRuleCategory(RuleCategory ruleCategory) {
		this.ruleCategory = ruleCategory;
	}


	public String getScopeBeanFieldPath() {
		return this.scopeBeanFieldPath;
	}


	public void setScopeBeanFieldPath(String scopeBeanFieldPath) {
		this.scopeBeanFieldPath = scopeBeanFieldPath;
	}


	public String getScopeBeanFieldValue() {
		return this.scopeBeanFieldValue;
	}


	public void setScopeBeanFieldValue(String scopeBeanFieldValue) {
		this.scopeBeanFieldValue = scopeBeanFieldValue;
	}


	public SystemBean getRuleEvaluatorContextBean() {
		return this.ruleEvaluatorContextBean;
	}


	public void setRuleEvaluatorContextBean(SystemBean ruleEvaluatorContextBean) {
		this.ruleEvaluatorContextBean = ruleEvaluatorContextBean;
	}
}
