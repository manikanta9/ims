package com.clifton.rule.definition.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * This class provides validation for RuleAssignments prior to saving, updating, or deleting an assignment.
 *
 * @author stevenf on 8/11/2015.
 */
@Component
public class RuleAssignmentValidator extends SelfRegisteringDaoValidator<RuleAssignment> {

	private RuleViolationService ruleViolationService;
	private SecurityAuthorizationService securityAuthorizationService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(RuleAssignment bean, DaoEventTypes config) throws ValidationException {
		// NOTHING HERE - USES METHOD WITH DAO PARAM
	}


	@Override
	public void validate(RuleAssignment ruleAssignment, DaoEventTypes config, ReadOnlyDAO<RuleAssignment> ruleAssignmentDAO) throws ValidationException {
		if (ruleAssignment == null) {
			throw new ValidationException("Rule assignment may not be null.");
		}

		if (config.isDelete()) {
			RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
			ruleViolationSearchForm.setRuleAssignmentId(ruleAssignment.getId());
			if (!CollectionUtils.isEmpty(getRuleViolationService().getRuleViolationList(ruleViolationSearchForm))) {
				throw new ValidationException("Unable to delete the rule assignment as there are existing violations associated with it.<br />Please exclude or end the assignment instead.");
			}
		}

		//Global rules require admin privileges.
		if (isGlobalRuleAssignment(ruleAssignment, config)) {
			ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityUserAdmin(), "Only Administrators are allowed to create/edit/delete global rules.");
			if (config.isDelete()) {
				throw new ValidationException("Global assignments can not be deleted.");
			}
		}

		RuleDefinition ruleDefinition = ruleAssignment.getRuleDefinition();
		ValidationUtils.assertNotNull(ruleDefinition, "Rule definition may not be null.");

		//Do not allow rule assignments to be created when NOT ignorable.
		//*Normally the initial global rule assignment is created through migrations with the definition;
		// however, if creating a new definition and global assignment through the UI the definition must initially
		// be marked as ignorable and not system defined in order to add the assignment,
		// after which it can be made not ignorable or system defined.
		if (config.isInsert()) {
			if (!ruleDefinition.isIgnorable()) {
				throw new ValidationException("Global, not ignorable, rules can not be overridden.");
			}
			if (ruleDefinition.isSystemDefined()) {
				throw new ValidationException("System defined rules can not be overridden.");
			}
		}

		if (config.isInsert() || config.isUpdate()) {
			//Locate existing assignments
			List<RuleAssignment> existingRuleAssignmentList =
					ruleAssignmentDAO.findByFields(
							new String[]{"ruleDefinition.id", "additionalScopeFkFieldId", "entityFkFieldId"},
							new Object[]{
									ruleDefinition.getId(),
									ruleAssignment.getAdditionalScopeFkFieldId(),
									ruleAssignment.getEntityFkFieldId()
							});

			//Validate there are no rule assignment conflicts
			for (RuleAssignment existingRuleAssignment : CollectionUtils.getIterable(existingRuleAssignmentList)) {
				if (!ruleAssignment.equals(existingRuleAssignment) && (ruleAssignment.isSnoozed() == existingRuleAssignment.isSnoozed()) &&
						DateUtils.isOverlapInDates(ruleAssignment.getStartDate(), ruleAssignment.getEndDate(), existingRuleAssignment.getStartDate(), existingRuleAssignment.getEndDate())) {
					StringBuilder exceptionMessage = new StringBuilder("There already exists a");
					if (!ruleAssignment.isSnoozed()) {
						exceptionMessage.append("n assignment for definition [").append(ruleDefinition.getName()).append("]");
					}
					else {
						exceptionMessage.append(" snoozed assignment for the violation with definition [").append(ruleDefinition.getName()).append("]");
						if (existingRuleAssignment.getAdditionalScopeLabel() != null) {
							exceptionMessage.append(" for [").append(existingRuleAssignment.getAdditionalScopeLabel()).append("]");
						}
						if (existingRuleAssignment.getEntityLabel() != null) {
							exceptionMessage.append(" and [").append(existingRuleAssignment.getEntityLabel()).append("]");
						}
					}
					exceptionMessage.append(" with an overlapping date range.  Please either edit the existing assignment, or end it and start a new one.");
					throw new ValidationException(exceptionMessage.toString());
				}
			}
		}
	}


	private boolean isGlobalRuleAssignment(RuleAssignment ruleAssignment, DaoEventTypes config) {
		if (config.isUpdate() || config.isDelete()) {
			RuleAssignment originalRuleAssignment = getOriginalBean(ruleAssignment);
			if (originalRuleAssignment.getAdditionalScopeFkFieldId() == null && originalRuleAssignment.getEntityFkFieldId() == null) {
				return true;
			}
		}
		else {
			if (ruleAssignment.getAdditionalScopeFkFieldId() == null && ruleAssignment.getEntityFkFieldId() == null) {
				return true;
			}
		}
		return false;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
