package com.clifton.rule.definition.cache;

import com.clifton.rule.definition.RuleAssignment;

import java.time.LocalDate;
import java.util.List;


/**
 * This class caches the ruleConfig object in several tiers to allow for smart cache clearing.
 * e.g. the cache can be cleared entirely, by category, or by additionalScopeEntity.
 * The cache is initially built by the IMSPreLoadCacheJob that iterates all accounts and generates
 * their ruleConfig objects - see 'loadRuleConfigs' method in 'IMSPreLoadCache' class.
 * The pre-load cache is the only time the cache is cleared in its entirety.
 *
 * @author stevenf
 */
public interface RuleAssignmentCache {

	public List<RuleAssignment> getRuleAssignmentList(Short ruleCategoryId, LocalDate activeOnDate);


	public void setRuleAssignmentMap(Short ruleCategoryId, LocalDate activeOnDate, List<RuleAssignment> ruleAssignmentList);
}
