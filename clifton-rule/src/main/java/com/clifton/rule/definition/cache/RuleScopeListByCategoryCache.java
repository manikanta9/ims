package com.clifton.rule.definition.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.rule.definition.RuleScope;
import org.springframework.stereotype.Component;


/**
 * @author AbhinayaM
 */
@Component
public class RuleScopeListByCategoryCache extends SelfRegisteringSingleKeyDaoListCache<RuleScope, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "ruleCategory.id";
	}


	@Override
	protected Short getBeanKeyValue(RuleScope bean) {
		if (bean.getRuleCategory() != null) {
			return bean.getRuleCategory().getId();
		}
		return null;
	}
}
