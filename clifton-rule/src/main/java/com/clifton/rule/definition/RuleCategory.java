package com.clifton.rule.definition;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.rule.converter.RuleEntityConverter;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.schema.SystemTable;


/**
 * The RuleCategory class represents domain objects (PortfolioRun or Trade) that have
 * specific rules assigned to and executed for.
 *
 * @author vgomelsky
 */
@CacheByName
public class RuleCategory extends NamedEntity<Short> {

	/**
	 * Rules are executed for entities in this table. For example: PortfolioRun, Trade, etc.
	 */
	private SystemTable categoryTable;

	/**
	 * The bean that implements {@link RuleEntityConverter} interface and implements methods to
	 * convert a given entity into one or more entities for rule processing.
	 */
	private SystemBean entityConverterBean;

	private SystemBean ruleEvaluatorContextBean;

	/**
	 * securityResource is used to apply conditional security conditions to methods annotated with 'SecureMethod'
	 * For example, some trading rules grant compliance access to methods based on the 'Trade' table, by granting
	 * RW Access to that table for those specific methods.
	 */
	private SecurityResource securityResource;
	/**
	 * When rules are run, the activeOnDateBeanFieldPath, if defined, is used to pull a date from the target
	 * entity which should be the date used when loading the ruleConfig objects for the entity.  This allows
	 * for rules to be run in a historically accurate context (e.g. when executing rules for a portfolioRun,
	 * the bean fieldPath would be 'runDate'.  BeanUtils is then used to pull the date from the run and
	 * retrieve the rule config object based on that date value; otherwise the current date is used which
	 * can result in inaccurate rule evaluation on historical runs.
	 */
	private String activeOnDateBeanFieldPath;
	/**
	 * RuleAssignment can be used to define or exclude more specific assignments.
	 * For example, for PortfolioRun the value is "clientInvestmentAccount.id" and RuleAssignment entry
	 * with corresponding "additionalScopeFkFieldId" set to a client account id can be used to exclude that
	 * client account or to override global parameters (min/max) only for runs for that client account.
	 */
	private String additionalScopeBeanFieldPath;
	private SystemTable additionalScopeTable;
	/**
	 * Note: Use Singular representation
	 * i.e. Investment Account Scope is usually just Client Account
	 */
	private String additionalScopeLabel;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public SystemTable getCategoryTable() {
		return this.categoryTable;
	}


	public void setCategoryTable(SystemTable categoryTable) {
		this.categoryTable = categoryTable;
	}


	public SystemBean getEntityConverterBean() {
		return this.entityConverterBean;
	}


	public void setEntityConverterBean(SystemBean entityConverterBean) {
		this.entityConverterBean = entityConverterBean;
	}


	public SystemBean getRuleEvaluatorContextBean() {
		return this.ruleEvaluatorContextBean;
	}


	public void setRuleEvaluatorContextBean(SystemBean ruleEvaluatorContextBean) {
		this.ruleEvaluatorContextBean = ruleEvaluatorContextBean;
	}


	public SecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(SecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public String getActiveOnDateBeanFieldPath() {
		return this.activeOnDateBeanFieldPath;
	}


	public void setActiveOnDateBeanFieldPath(String activeOnDateBeanFieldPath) {
		this.activeOnDateBeanFieldPath = activeOnDateBeanFieldPath;
	}


	public String getAdditionalScopeBeanFieldPath() {
		return this.additionalScopeBeanFieldPath;
	}


	public void setAdditionalScopeBeanFieldPath(String additionalScopeBeanFieldPath) {
		this.additionalScopeBeanFieldPath = additionalScopeBeanFieldPath;
	}


	public SystemTable getAdditionalScopeTable() {
		return this.additionalScopeTable;
	}


	public void setAdditionalScopeTable(SystemTable additionalScopeTable) {
		this.additionalScopeTable = additionalScopeTable;
	}


	public String getAdditionalScopeLabel() {
		return this.additionalScopeLabel;
	}


	public void setAdditionalScopeLabel(String additionalScopeLabel) {
		this.additionalScopeLabel = additionalScopeLabel;
	}
}
