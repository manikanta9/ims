package com.clifton.rule.evaluator;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Map;


/**
 * Contains common methods for evaluating rule values
 *
 * @author manderson
 */
public class RuleEvaluatorUtils {

	public static String evaluateValueForRange(BigDecimal value, BigDecimal minAmount, BigDecimal maxAmount, String numberFormat) {
		return evaluateValueForRange(value, minAmount, maxAmount, numberFormat, false);
	}


	public static String evaluateValueForRange(BigDecimal value, BigDecimal minAmount, BigDecimal maxAmount, String numberFormat, boolean appendPercentSign) {
		// MAKE SURE RESULT STRING MAKES SENSE FOR SPECIFIC USES
		if (value != null) {
			if (minAmount != null && MathUtils.isLessThan(value, minAmount)) {
				return getValueFormatted(value, numberFormat, appendPercentSign) + " is less than required minimum " + getValueFormatted(minAmount, numberFormat, appendPercentSign);
			}
			if (maxAmount != null && MathUtils.isGreaterThan(value, maxAmount)) {
				return getValueFormatted(value, numberFormat, appendPercentSign) + " is greater than required maximum " + getValueFormatted(maxAmount, numberFormat, appendPercentSign);
			}
		}
		return null;
	}


	public static boolean isEntityConfigRangeViolated(BigDecimal rangeValue, EntityConfig entityConfig, Map<String, Object> templateValues) {
		return isEntityConfigRangeViolated(rangeValue, null, entityConfig, templateValues);
	}


	public static boolean isEntityConfigRangeViolated(BigDecimal rangeValue, BigDecimal minAmount, BigDecimal maxAmount, Map<String, Object> templateValues) {
		return isEntityConfigRangeViolated(rangeValue, null, minAmount, maxAmount, null, templateValues);
	}


	public static boolean isEntityConfigRangeViolated(BigDecimal rangeValue, BigDecimal deviationValue, EntityConfig entityConfig, Map<String, Object> templateValues) {
		return isEntityConfigRangeViolated(rangeValue, deviationValue, entityConfig.getMinAmount(), entityConfig.getMaxAmount(), entityConfig.getRuleAmount(), templateValues);
	}


	public static boolean isEntityConfigRangeViolated(BigDecimal rangeValue, BigDecimal deviationValue, BigDecimal minAmount, BigDecimal maxAmount, BigDecimal deviationAmount, Map<String, Object> templateValues) {
		AssertUtils.assertNotNull(rangeValue, "Range value may not be null when evaluating range violations.");

		boolean violated = false;

		templateValues.put("maxAmount", maxAmount);
		templateValues.put("minAmount", minAmount);
		templateValues.put("rangeValue", rangeValue);
		if (minAmount != null && MathUtils.isLessThan(rangeValue, minAmount)) {
			templateValues.put("lessThanRange", true);
			templateValues.put("greaterThanRange", false);
			violated = true;
		}
		if (maxAmount != null && MathUtils.isGreaterThan(rangeValue, maxAmount)) {
			templateValues.put("lessThanRange", false);
			templateValues.put("greaterThanRange", true);
			violated = true;
		}

		//If we have a violation, but the deviationValue is within the specified threshold, then we cancel the violation.
		if (deviationValue != null && violated && MathUtils.isLessThan(deviationValue, deviationAmount)) {
			violated = false;
		}
		return violated;
	}


	private static String getValueFormatted(BigDecimal value, String numberFormat, boolean appendPercentSign) {
		return "[" + CoreMathUtils.formatNumber(value, numberFormat) + (appendPercentSign ? "]%" : "]");
	}
}
