package com.clifton.rule.evaluator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The {@link AlwaysTrueFalseRuleEvaluator} will always generate a {@link RuleViolation} or never generate a {@link RuleViolation}, depending
 * on the value of the generateViolation flag.
 *
 * @author lnaylor
 */
public class AlwaysTrueFalseRuleEvaluator extends BaseRuleEvaluator<IdentityObject, RuleEvaluatorContext> {

	/**
	 * If true, the evaluator will always generate a violation, unless the rule config entity is excluded.
	 * Else, the evaluator will never generate a violation.
	 */
	private boolean generateViolation;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject ruleBean, RuleConfig ruleConfig, RuleEvaluatorContext context) {
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig.isExcluded() || !isGenerateViolation()) {
			return new ArrayList<>();
		}
		RuleViolation violation = getRuleViolationService().createRuleViolation(entityConfig, ruleBean.getIdentity());
		return Collections.singletonList(violation);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isGenerateViolation() {
		return this.generateViolation;
	}


	public void setGenerateViolation(boolean generateViolation) {
		this.generateViolation = generateViolation;
	}
}
