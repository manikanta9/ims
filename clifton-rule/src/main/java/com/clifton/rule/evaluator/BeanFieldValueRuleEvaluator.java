package com.clifton.rule.evaluator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.JsonStrategy;
import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author jonathanr
 * <p>
 * This rule evaluator is designed as a generic solution for checking bean fields for configured values. A Json string is used to pass in bean fields paired with a userfriendly name
 * and an expected value. Violations are generated based on failed compairsons and configured values. Uses the {@Link JsonHandler} to parse incoming json.
 */
public class BeanFieldValueRuleEvaluator extends BaseRuleEvaluator<IdentityObject, RuleEvaluatorContext> implements ValidationAware {

	private String beanFieldsJson;
	private JsonHandler<JsonStrategy> jsonHandler;
	private boolean notEqual;
	private boolean all;

	////////////////////////////////////////////////////////////////////////////
	//////////             Implemented Override Methods               //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject ruleBean, RuleConfig ruleConfig, RuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			BeanFieldJsonDto[] beanFields = (BeanFieldJsonDto[]) getJsonHandler().fromJson(getBeanFieldsJson(), BeanFieldJsonDto[].class);
			List<String> violationNotes = new ArrayList<>();
			for (BeanFieldJsonDto beanField : beanFields) {
				Object propertyValue = BeanUtils.getPropertyValue(ruleBean, beanField.getBeanFieldPath());
				if (beanField.getExpectedValue() == null || beanField.getExpectedValue().isEmpty()) {
					if (propertyValue == null ^ isNotEqual()) {
						violationNotes.add(beanField.getUserFriendlyName() + (!isNotEqual() ? " cannot be null" : " must be null"));
					}
				}
				else {
					if (propertyValue == null) {
						if (isNotEqual()) {
							violationNotes.add(beanField.getUserFriendlyName() + " must be '" + beanField.getExpectedValue() + "' but was null");
						}
					}
					else {
						PropertyDataTypeConverter converter = new PropertyDataTypeConverter();
						DataTypes dataTypeName = converter.convert(BeanUtils.getNestedPropertyDescriptor(ruleBean, beanField.getBeanFieldPath()));
						if (CompareUtils.isEqual(propertyValue, DataTypeNameUtils.convertObjectToDataTypeName(beanField.getExpectedValue(), dataTypeName.getTypeName(), dataTypeName.getDeclaringClass())) ^ isNotEqual()) {
							violationNotes.add(beanField.getUserFriendlyName() + (isNotEqual() ? " cannot be '" + beanField.getExpectedValue() : "' must be '" + beanField.getExpectedValue() + "' but was '" + propertyValue + "'"));
						}
					}
				}
			}
			if ((!isAll() && !violationNotes.isEmpty()) || (isAll() && violationNotes.size() == beanFields.length)) {
				Map<String, Object> contextViolationMap = new HashMap<>();
				contextViolationMap.put("violationNotes", violationNotes);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, ruleBean.getIdentity(), contextViolationMap));
			}
		}
		return ruleViolationList;
	}


	@Override
	public void validate() throws ValidationException {
		if (getBeanFieldsJson() == null || getBeanFieldsJson().isEmpty()) {
			throw new ValidationException("Json string cannot be null. Evaluator needs a bean field to check.");
		}
		BeanFieldJsonDto[] beanFields;
		try {
			beanFields = (BeanFieldJsonDto[]) getJsonHandler().fromJson(getBeanFieldsJson(), BeanFieldJsonDto[].class);
		}
		catch (Exception e) {
			throw new ValidationException("Json Handler Error. Json input requires a standard json array of object format. i.e: >[{\"beanFieldName\": \"name\",\"userFriendlyName\": \"My Name\",\"expectedValue\"(optional): \"myExpectedValue\" }, ...]: \nOriginal Error was: " + e.getMessage());
		}
		if (beanFields == null || beanFields.length < 1) {
			throw new ValidationException("Json input requires a standard json array of object format. i.e: >[{\"beanFieldName\": \"name\",\"userFriendlyName\": \"My Name\",\"expectedValue\"(optional): \"myExpectedValue\" }, ...]");
		}

		for (int i = 0; i < beanFields.length; i++) {
			BeanFieldJsonDto beanField = beanFields[i];
			if (StringUtils.isEmpty(beanField.getBeanFieldPath()) || StringUtils.isEmpty(beanField.getUserFriendlyName())) {
				throw new ValidationException("Json input for beanField at index " + i + " of the configured fields has too many or two few parameters. Parameters names are beanFieldName, userFriendlyName, expectedValue.");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanFieldsJson() {
		return this.beanFieldsJson;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              Getters and Setters                       //////////
	////////////////////////////////////////////////////////////////////////////


	public void setBeanFieldsJson(String beanFieldsJson) {
		this.beanFieldsJson = beanFieldsJson;
	}


	public JsonHandler<JsonStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JsonStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public boolean isNotEqual() {
		return this.notEqual;
	}


	public void setNotEqual(boolean notEqual) {
		this.notEqual = notEqual;
	}


	public boolean isAll() {
		return this.all;
	}


	public void setAll(boolean all) {
		this.all = all;
	}


	public static class BeanFieldJsonDto {

		private String beanFieldPath;
		private String userFriendlyName;
		private String expectedValue;


		public BeanFieldJsonDto() {
		}


		public BeanFieldJsonDto(String beanFieldPath, String userFriendlyName, String expectedValue) {
			this.beanFieldPath = beanFieldPath;
			this.userFriendlyName = userFriendlyName;
			this.expectedValue = expectedValue;
		}


		public String getBeanFieldPath() {
			return this.beanFieldPath;
		}


		public void setBeanFieldPath(String beanFieldPath) {
			this.beanFieldPath = beanFieldPath;
		}


		public String getUserFriendlyName() {
			return this.userFriendlyName;
		}


		public void setUserFriendlyName(String userFriendlyName) {
			this.userFriendlyName = userFriendlyName;
		}


		public String getExpectedValue() {
			return this.expectedValue;
		}


		public void setExpectedValue(String expectedValue) {
			this.expectedValue = expectedValue;
		}
	}
}
