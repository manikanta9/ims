package com.clifton.rule.evaluator;


import com.clifton.core.beans.IdentityObject;


/**
 * The BaseRangeRuleEvaluator provides all subclasses with a pre-existing 'rangeType' beanProperty that is typically
 * used to determine if violations should be calculated using 'amount' or 'percent' ranges.
 *
 * @author StevenF
 */
public abstract class BaseRangeRuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> extends BaseRuleEvaluator<T, K> {

	private String rangeType;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getRangeType() {
		return this.rangeType;
	}


	public void setRangeType(String rangeType) {
		this.rangeType = rangeType;
	}
}
