package com.clifton.rule.evaluator;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextImpl;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.schema.SystemSchemaService;

import java.util.HashMap;
import java.util.Map;


/**
 * @author manderson
 * This was added to ensure the creation of a new context object during processing of rules.
 */
public abstract class BaseRuleEvaluatorContext implements RuleEvaluatorContext {

	private final Context context;

	private static final String SECURITY_USER_MAP = "SECURITY_USER_MAP";

	private SecurityUserService securityUserService;
	private SystemSchemaService systemSchemaService;
	private RuleViolationStatusService ruleViolationStatusService;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Used for cases where we want to know the last update user of an object for rule violation reporting.
	 */
	@SuppressWarnings("unchecked")
	public SecurityUser getSecurityUser(short securityUserId) {
		Map<Short, SecurityUser> securityUserMap = (Map<Short, SecurityUser>) getContext().getBean(SECURITY_USER_MAP);
		if (securityUserMap == null) {
			securityUserMap = new HashMap<>();
		}
		SecurityUser user = securityUserMap.get(securityUserId);
		if (user == null) {
			user = getSecurityUserService().getSecurityUser(securityUserId);
			if (user == null) {
				user = new SecurityUser();
				user.setDisplayName("Unknown User");
			}
			securityUserMap.put(securityUserId, user);
			getContext().setBean(SECURITY_USER_MAP, securityUserMap);
		}
		return user;
	}


	public BaseRuleEvaluatorContext() {
		this.context = new ContextImpl();
		getContext().setBean(EVALUATOR_CONTEXT_SELF_REFERENCE_KEY, this);
	}


	@Override
	public Context getContext() {
		return this.context;
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}
}
