package com.clifton.rule.evaluator;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The RuleConfig object holds references to all applicable entityConfigs (i.e. RuleAssignments)
 * for a given RuleDefinition.  The object prevents duplicate global rules in additional to conflicting
 * specific assignments at both the additional scope and entity level.  The RuleConfig object is passed
 * into all RuleEvaluator classes.
 *
 * @author vgomelsky
 * @author StevenF
 */
public class RuleConfig implements Serializable {

	private final Short entityTableId;
	private final Short evaluatorBeanId;
	private final Short ruleDefinitionId;

	private final boolean preProcess;

	private EntityConfig defaultConfig;
	/**
	 * Maps entity identifiers (bean's primary key) to corresponding configuration objects.
	 */
	private final Map<Long, EntityConfig> entityConfigMap = new ConcurrentHashMap<>();

	private final List<EntityConfig> snoozedEntityConfigList = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////


	public RuleConfig(RuleAssignment ruleAssignment) {
		RuleDefinition ruleDefinition = ruleAssignment.getRuleDefinition();
		this.entityTableId = ruleDefinition.getEntityTable() != null ? ruleDefinition.getEntityTable().getId() : null;
		this.evaluatorBeanId = ruleDefinition.getEvaluatorBean() != null ? ruleDefinition.getEvaluatorBean().getIdentity().shortValue() : null;
		this.preProcess = ruleDefinition.isPreProcess();
		this.ruleDefinitionId = ruleAssignment.getRuleDefinition().getId();
	}

	////////////////////////////////////////////////////////////////////////////


	public void addEntityConfig(EntityConfig entityConfig) {
		boolean snoozed = entityConfig.isSnoozed();
		if (entityConfig.getEntityFkFieldId() != null) {
			//If we already have an entity scope assignment, then we only override if the new assignment is snoozed
			if (this.entityConfigMap.get(entityConfig.getEntityFkFieldId()) != null) {
				if (snoozed) {
					this.entityConfigMap.put(entityConfig.getEntityFkFieldId(), entityConfig);
				}
			}
			else {
				this.entityConfigMap.put(entityConfig.getEntityFkFieldId(), entityConfig);
			}
		}
		else {
			if (this.defaultConfig != null) {
				//A global rule assignment cannot be snoozed so we do this validation check as usual.
				ValidationUtils.assertFalse(this.defaultConfig.getAdditionalScopeFkFieldId() == null
						&& entityConfig.getAdditionalScopeFkFieldId() == null, "Conflicting global rule assignments found for rule definition with id: [" + getRuleDefinitionId() + "]");
				//If a user has snoozed at the additional scope level then go ahead and override.
				if (this.defaultConfig.getAdditionalScopeFkFieldId() != null && snoozed) {
					this.defaultConfig = entityConfig;
				}
				else {
					//You can't have two assignments (un-snoozed) at the additional scope level
					ValidationUtils.assertFalse(this.defaultConfig.getAdditionalScopeFkFieldId() != null
							&& entityConfig.getAdditionalScopeFkFieldId() != null, "Conflicting specific rule assignments found.");
					if (entityConfig.getAdditionalScopeFkFieldId() != null) {
						this.defaultConfig = entityConfig;
					}
				}
			}
			else {
				this.defaultConfig = entityConfig;
			}
		}
	}


	public EntityConfig getEntityConfig(Long entityId) {
		EntityConfig result = entityId != null ? this.entityConfigMap.get(entityId) : null;
		if (result == null) {
			result = this.defaultConfig;
		}
		return result;
	}


	public void addSnoozedEntityConfig(EntityConfig entityConfig) {
		this.snoozedEntityConfigList.add(entityConfig);
	}


	public List<EntityConfig> getSnoozedEntityConfigList() {
		return this.snoozedEntityConfigList;
	}


	/**
	 * Returns the list of entity ids explicitly specified
	 */
	public Set<Long> getEntityIdList() {
		return this.entityConfigMap.keySet();
	}


	public Short getEntityTableId() {
		return this.entityTableId;
	}


	public Short getEvaluatorBeanId() {
		return this.evaluatorBeanId;
	}


	public boolean isPreProcess() {
		return this.preProcess;
	}


	public Short getRuleDefinitionId() {
		return this.ruleDefinitionId;
	}
}
