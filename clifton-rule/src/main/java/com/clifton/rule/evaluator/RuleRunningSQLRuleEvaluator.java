package com.clifton.rule.evaluator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemSchemaService;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The RuleRunningSQLRuleEvaluator class executes the specified SELECT SQL statement and generates
 * a {@link RuleViolation} for every row returned the the SQL statement.
 * <p>
 * The SQL statement must return 2 columns: Cause FK Field ID in the first column and Violation Note in the second.
 * <p>
 * These cannot be excluded per entity, but instead included/excluded at the additional scope entity level (i.e. on or off by account).
 * MinAmount, MaxAmount, and RuleAmount are set up in the template config through the entityConfig object and can be referenced using ${entityConfig.minAmount}, ${entityConfig.maxAmount}, ${entityConfig.ruleAmount}
 *
 * @author StevenF
 */
public class RuleRunningSQLRuleEvaluator extends BaseRuleEvaluator<IdentityObject, RuleEvaluatorContext> {

	private String dataSourceName;
	private String sql;

	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;
	private SystemSchemaService systemSchemaService;
	private TemplateConverter templateConverter;


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject ruleBean, RuleConfig ruleConfig, RuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			ValidationUtils.assertNotNull(getSql(), "SQL string is required");
			TemplateConfig config = new TemplateConfig(getSql());
			addBeansToConfig(config, ruleBean, entityConfig);
			String runSql = getTemplateConverter().convert(config);

			DataTable dt = getQueryResult(runSql);


			if (validateResults(dt)) {
				for (int i = 0; i < dt.getTotalRowCount(); i++) {
					DataRow row = dt.getRow(i);
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put(RuleEvaluator.VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY, row.getValue(1));
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig,
							ruleBean.getIdentity(), (Integer) row.getValue(0), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private void addBeansToConfig(TemplateConfig config, IdentityObject entity, EntityConfig entityConfig) {
		config.addBeanToContext("bean", entity);
		config.addBeanToContext("entityConfig", entityConfig);
	}


	/**
	 * Returns true is results are validated and system warnings should be generated.
	 * Throws ValidationException if DataTable has invalid columns.
	 */
	private boolean validateResults(DataTable dt) {
		if (dt == null) {
			return false;
		}
		if (dt.getTotalRowCount() == 0) {
			return false;
		}
		DataColumn[] columns = dt.getColumnList();
		if (columns == null || columns.length < 2) {
			throw new ValidationException("Invalid SQL.  SQL MUST return at least 2 columns.  The first should be the Cause FK Field ID and the second should be the warning message.");
		}
		DataColumnImpl column = (DataColumnImpl) columns[0];
		ValidationUtils.assertTrue(Types.INTEGER == column.getDataType(), "First column in results must be an Integer");
		return true;
	}


	private DataTable getQueryResult(String runSql) {
		String dataSource = getDataSourceName();
		if (StringUtils.isEmpty(dataSource)) {
			dataSource = getSystemSchemaService().getSystemDataSourceDefault().getName();
		}
		return getDataTableRetrievalHandlerLocator().locate(dataSource).findDataTable(runSql);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public String getSql() {
		return this.sql;
	}


	public void setSql(String sql) {
		this.sql = sql;
	}


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
