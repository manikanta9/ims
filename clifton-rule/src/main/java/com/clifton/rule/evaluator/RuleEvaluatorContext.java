package com.clifton.rule.evaluator;


import com.clifton.core.context.Context;


/**
 * Interface for all rule evaluator contexts -
 * Each RuleCategory defines its own RuleEvaluatorContext implementation, e.g.:
 * <p>
 * PortfolioRunRuleEvaluatorContext maintains cached data applicable to all rules within the Portfolio Run Rules category
 * significantly reducing the number of database look ups across the entire run.
 *
 * @author StevenF
 */
public interface RuleEvaluatorContext {

	/**
	 * Allows this Rule Evaluator Context to propagate through anywhere a Context is used.
	 * <p>
	 */
	public static final String EVALUATOR_CONTEXT_SELF_REFERENCE_KEY = "EVALUATOR_CONTEXT_SELF_REFERENCE_KEY";

	/**
	 * Allows storage and retrieval of the Rule Config through this Evaluators inner Context.
	 * <p>
	 * See {@link RuleConfig}
	 */
	public static final String EVALUATOR_RULE_CONFIG_REFERENCE_KEY = "EVALUATOR_RULE_CONFIG_REFERENCE_KEY";

	/**
	 * Allows storage and retrieval of the Rule Violations through this Evaluators inner Context.
	 * <p>
	 * See {@link com.clifton.rule.violation.RuleViolation}
	 */
	public static final String EVALUATOR_RULE_VIOLATION_LIST_REFERENCE_KEY = "EVALUATOR_RULE_VIOLATION_LIST_REFERENCE_KEY";


	public Context getContext();
}
