package com.clifton.rule.evaluator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.violation.RuleViolation;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface RuleEvaluatorService {

	/**
	 * Runs all rules that apply to the specified entity from the specified category.
	 * Updates violations for the specified entity in the database based on results of rule executions.
	 * Returns a List of {@link RuleViolation} objects if any or an empty List if none were detected.
	 *
	 * @param categoryName
	 * @param entityId
	 * @param preProcess   - PreProcess can be optional (pass null to run all) and can be used to run rules in two batches.
	 *                     PreProcess allows running rules that are based on setup information, not on the results so if there is a non-ignorable rule violated,
	 *                     we know that before processing and wasting extra time processing that would need to be re-processed after the violation is addressed.
	 */
	@ModelAttribute("data")
	@RequestMapping("ruleExecuteRules")
	public List<RuleViolation> executeRules(String categoryName, Long entityId, Boolean preProcess);


	/**
	 * Runs all rules that apply to the specified entity from the specified category.
	 * Updates violations for the specified entity in the database based on results of rule executions.
	 * Returns a List of {@link RuleViolation} objects if any or an empty List if none were detected.
	 *
	 * @param ruleCategory
	 * @param entity
	 * @param preProcess   - PreProcess can be optional (pass null to run all) and can be used to run rules in two batches.
	 *                     PreProcess allows running rules that are based on setup information, not on the results so if there is a non-ignorable rule violated,
	 *                     we know that before processing and wasting extra time processing that would need to be re-processed after the violation is addressed.
	 */
	@DoNotAddRequestMapping
	public List<RuleViolation> executeRules(RuleCategory ruleCategory, IdentityObject entity, Boolean preProcess);


	/**
	 * Runs all rules that apply to the specified entity and returns violations if any.
	 * This method does NOT update rule violations in the database.
	 */
	@RequestMapping("ruleViolationEvaluatorsPreviewRunList")
	public List<RuleViolation> previewRuleViolations(String categoryTableName, long categoryTableEntityId);
}
