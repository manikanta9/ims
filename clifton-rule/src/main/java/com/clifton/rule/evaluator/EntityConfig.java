package com.clifton.rule.evaluator;

import com.clifton.rule.definition.RuleAssignment;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * The EntityConfig is a lightweight wrapper for RuleAssignments applicable to specific entities.
 * EntityConfig objects are added to the RuleConfig object for their respective RuleDefinitions.
 * <p>
 * The values of the EntityConfig object may or may not be used
 * and the values of the fields will vary based on the category configuration e.g.:
 * For the Portfolio Run Rules category -
 * <p>
 * additionalScopeFkFieldId = optionally the id of the client account
 * entityFkFieldId = optionally the id of a manager account (varies per RuleDefinition)
 * Rules with global scope specify neither value.
 * <p>
 * additionalScopeLabel and entityLabel are used for UI readability, rather than using generic labels.
 *
 * @author vgomelsky
 * @author StevenF
 */
public class EntityConfig implements Serializable {

	private final Long additionalScopeFkFieldId;
	private final String additionalScopeLabel;

	private final Long entityFkFieldId;
	private final String entityLabel;

	private final boolean excluded;
	private final boolean snoozed;

	private final BigDecimal maxAmount;
	private final BigDecimal minAmount;
	private final BigDecimal ruleAmount;

	private final Integer ruleAssignmentId;


	public EntityConfig(RuleAssignment ruleAssignment) {
		this.additionalScopeFkFieldId = ruleAssignment.getAdditionalScopeFkFieldId();
		this.additionalScopeLabel = ruleAssignment.getAdditionalScopeLabel();
		this.entityFkFieldId = ruleAssignment.getEntityFkFieldId();
		this.entityLabel = ruleAssignment.getEntityLabel();
		this.excluded = ruleAssignment.isExcluded();
		this.snoozed = ruleAssignment.isSnoozed();
		this.maxAmount = ruleAssignment.getMaxAmount();
		this.minAmount = ruleAssignment.getMinAmount();
		this.ruleAmount = ruleAssignment.getRuleAmount();
		this.ruleAssignmentId = ruleAssignment.getId();
	}


	public Long getAdditionalScopeFkFieldId() {
		return this.additionalScopeFkFieldId;
	}


	public String getAdditionalScopeLabel() {
		return this.additionalScopeLabel;
	}


	public String getEntityLabel() {
		return this.entityLabel;
	}


	public Long getEntityFkFieldId() {
		return this.entityFkFieldId;
	}


	public boolean isExcluded() {
		return this.excluded;
	}


	public boolean isSnoozed() {
		return this.snoozed;
	}


	public BigDecimal getRuleAmount() {
		return this.ruleAmount;
	}


	public BigDecimal getMaxAmount() {
		return this.maxAmount;
	}


	public BigDecimal getMinAmount() {
		return this.minAmount;
	}


	public Integer getRuleAssignmentId() {
		return this.ruleAssignmentId;
	}
}
