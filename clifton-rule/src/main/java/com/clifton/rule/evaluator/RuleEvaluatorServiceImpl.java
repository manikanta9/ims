package com.clifton.rule.evaluator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.converter.RuleEntityConverter;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.RuleScope;
import com.clifton.rule.definition.search.RuleCategorySearchForm;
import com.clifton.rule.retriever.RuleRetrieverService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author vgomelsky
 */
@Service
public class RuleEvaluatorServiceImpl implements RuleEvaluatorService {

	private DaoLocator daoLocator;
	private RuleDefinitionService ruleDefinitionService;
	private RuleRetrieverService ruleRetrieverService;
	private RuleViolationService ruleViolationService;
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> executeRules(String categoryName, Long entityId, Boolean preProcess) {
		RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategoryByName(categoryName);
		IdentityObject entity = getDaoLocator().locate(ruleCategory.getCategoryTable().getName()).findByPrimaryKey(entityId);
		return executeRules(ruleCategory, entity, preProcess);
	}


	@Override
	@Transactional
	@SuppressWarnings("unchecked")
	public List<RuleViolation> executeRules(RuleCategory ruleCategory, IdentityObject entity, Boolean preProcess) {
		List<IdentityObject> convertedEntityList = Collections.singletonList(entity);
		if (ruleCategory.getEntityConverterBean() != null) {
			RuleEntityConverter<IdentityObject, IdentityObject> ruleEntityConverter =
					(RuleEntityConverter<IdentityObject, IdentityObject>) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(ruleCategory.getEntityConverterBean().getId()));
			convertedEntityList = ruleEntityConverter.convertEntity(entity);
		}
		List<RuleViolation> existingViolationList = new ArrayList<>();
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		for (IdentityObject convertedEntity : CollectionUtils.getIterable(convertedEntityList)) {
			existingViolationList.addAll(CollectionUtils.asNonNullList(getExistingRuleViolations(ruleCategory, convertedEntity, preProcess)));
			ruleViolationList.addAll(CollectionUtils.asNonNullList(doExecuteRules(ruleCategory, convertedEntity, preProcess)));
		}
		return getRuleViolationService().updateRuleViolationList(ruleViolationList, existingViolationList);
	}


	private List<RuleViolation> doExecuteRules(RuleCategory ruleCategory, IdentityObject entity, Boolean preProcess) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();

		// get scope for entity/category if applicable
		RuleScope entityScope = getRuleScopeForEntity(ruleCategory, entity);
		RuleEvaluatorContext globalContext = (RuleEvaluatorContext) getSystemBeanService().getBeanInstance(ruleCategory.getRuleEvaluatorContextBean());
		boolean multipleContextTypes = false;
		RuleEvaluatorContext context = null;
		if (entityScope != null && entityScope.getRuleEvaluatorContextBean() != null) {
			context = (RuleEvaluatorContext) getSystemBeanService().getBeanInstance(entityScope.getRuleEvaluatorContextBean());
			multipleContextTypes = !globalContext.getClass().equals(context.getClass());
		}

		IdentityObject additionalScopeEntity = (IdentityObject) BeanUtils.getPropertyValue(entity, ruleCategory.getAdditionalScopeBeanFieldPath());
		List<RuleConfig> globalRuleConfigList = getRuleRetrieverService().getRuleConfigListForAdditionalScopeEntity(ruleCategory, additionalScopeEntity, preProcess, getActiveOnDate(entity, ruleCategory));
		if (multipleContextTypes) {
			// We want the scope restricted rules to be processed with the scoped context. Remaining rules should use the global context.
			// Three "types" of rule assignments we need to account for
			// - True global (category-wide) without scope override
			// - Global but scope restricted (scope-wide)
			// - entity-specific
			Map<Boolean, List<RuleConfig>> configMap = BeanUtils.getBeansMap(globalRuleConfigList, config -> {
				RuleDefinition def = getRuleDefinitionService().getRuleDefinition(config.getRuleDefinitionId());
				RuleScope scope = def.getRuleScope();
				// use global context if no scope or scope's evaluation context does not matches the entity's evaluator context
				return scope == null || !CompareUtils.isEqual(scope.getRuleEvaluatorContextBean(), entityScope.getRuleEvaluatorContextBean());
			});
			globalRuleConfigList = configMap.get(Boolean.TRUE);
			List<RuleConfig> scopedRuleConfig = configMap.get(Boolean.FALSE);
			boolean copyProps = processRuleConfigList(scopedRuleConfig, entity, context, ruleViolationList);
			if (copyProps) {
				BeanUtils.copyProperties(context, globalContext);
			}
		}
		processRuleConfigList(globalRuleConfigList, entity, globalContext, ruleViolationList);

		return ruleViolationList;
	}


	private boolean processRuleConfigList(List<RuleConfig> ruleConfigList, IdentityObject entity, RuleEvaluatorContext context, List<RuleViolation> ruleViolationList) {
		boolean ruleProcessed = false;
		for (RuleConfig ruleConfig : CollectionUtils.getIterable(ruleConfigList)) {
			@SuppressWarnings("unchecked")
			RuleEvaluator<IdentityObject, RuleEvaluatorContext> ruleEvaluator =
					(RuleEvaluator<IdentityObject, RuleEvaluatorContext>) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(ruleConfig.getEvaluatorBeanId()));
			try {
				ruleViolationList.addAll(ruleEvaluator.evaluateRule(entity, ruleConfig, context));
				ruleProcessed = true;
			}
			catch (RuntimeException re) {
				String ruleDefinitionName = getRuleDefinitionService().getRuleDefinition(ruleConfig.getRuleDefinitionId()).getName();
				throw new ValidationException("Failed to execute rule '" + ruleDefinitionName + "': " + re.getMessage(), re);
			}
		}
		return ruleProcessed;
	}


	private RuleScope getRuleScopeForEntity(RuleCategory ruleCategory, IdentityObject entity) {
		String additionalScopePath = ruleCategory.getAdditionalScopeBeanFieldPath();
		if (additionalScopePath != null) {
			Object additionalScopeEntity = BeanUtils.getPropertyValue(entity, additionalScopePath);
			List<RuleScope> categoryScopes = getRuleDefinitionService().getRuleScopeListByCategory(ruleCategory.getId());
			for (RuleScope scope : CollectionUtils.getIterable(categoryScopes)) {
				Object additionalScopeValue = BeanUtils.getPropertyValue(additionalScopeEntity, scope.getScopeBeanFieldPath());
				if (CompareUtils.isEqual(String.valueOf(additionalScopeValue), scope.getScopeBeanFieldValue())) {
					return scope;
				}
			}
		}
		return null;
	}


	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true) // improves performance by maintaining one Hibernate session
	public List<RuleViolation> previewRuleViolations(String categoryTableName, long categoryTableEntityId) {
		RuleCategorySearchForm ruleCategorySearchForm = new RuleCategorySearchForm();
		ruleCategorySearchForm.setCategoryTableName(categoryTableName);
		RuleCategory ruleCategory = CollectionUtils.getOnlyElement(getRuleDefinitionService().getRuleCategoryList(ruleCategorySearchForm));
		ValidationUtils.assertNotNull(ruleCategory, "No Rule Category is defined for table: " + categoryTableName);

		//The following code executes rules and retrieved the violations, from there it retrieves all rules and adds rules that applied, but passed
		//in addition to rules that were explicitly excluded based on their additional and entity scope.
		IdentityObject entity = getDaoLocator().locate(ruleCategory.getCategoryTable().getName()).findByPrimaryKey(categoryTableEntityId);
		List<IdentityObject> convertedEntityList = Collections.singletonList(entity);
		if (ruleCategory.getEntityConverterBean() != null) {
			RuleEntityConverter<IdentityObject, IdentityObject> ruleEntityConverter =
					(RuleEntityConverter<IdentityObject, IdentityObject>) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBean(ruleCategory.getEntityConverterBean().getId()));
			convertedEntityList = ruleEntityConverter.convertEntity(entity);
		}
		List<RuleViolation> ruleViolationPreviewList = new ArrayList<>();
		for (IdentityObject convertedEntity : CollectionUtils.getIterable(convertedEntityList)) {
			IdentityObject additionalScopeEntity = (IdentityObject) BeanUtils.getPropertyValue(convertedEntity, ruleCategory.getAdditionalScopeBeanFieldPath());
			//Long additionalScopeId = BeanUtils.getIdentityAsLong(additionalScopeEntity);

			List<RuleViolation> ruleViolationList = doExecuteRules(ruleCategory, convertedEntity, null);
			List<RuleConfig> ruleConfigList = getRuleRetrieverService().getRuleConfigListForAdditionalScopeEntity(ruleCategory, additionalScopeEntity, null, getActiveOnDate(convertedEntity, ruleCategory));
			Map<Short, List<RuleViolation>> violationsMapByDefinition = BeanUtils.getBeansMap(ruleViolationList, ruleViolation -> ruleViolation.getRuleAssignment().getRuleDefinition().getId());
			for (RuleConfig ruleConfig : ruleConfigList) {
				if (ruleConfig != null && violationsMapByDefinition.get(ruleConfig.getRuleDefinitionId()) == null) {
					// Process additional and entity specific assignments.
					// Excluding this piece - can check each entity since we don't know which entities actually apply so just use the default one as passed
					// i.e. Collateral Rules use Collateral Type as scope, but the entities are the accounts - previewing a rule for a specific account shows excluded for all of the other entitiy
					// assignments. Instead we should have the rule evaluator beans if the entity APPLIES to the current bean we are processing. Or the rule evaluator should return a list of entity ids (if used) that apply to compare to
					/*
					 for (Long entityId : ruleConfig.getEntityIdList()) {
					 EntityConfig entityConfig = ruleConfig.getEntityConfig(entityId);
					 if (entityConfig.getAdditionalScopeFkFieldId().equals(additionalScopeId)) {
					 ruleViolationList.add(createPreviewRuleViolation(entityConfig));
					 }
					 }
					 **/
					EntityConfig defaultConfig = ruleConfig.getEntityConfig(null);
					if (defaultConfig != null) {
						//Process Global Rule - global rule is retrieved with 'null'
						ruleViolationList.add(createPreviewRuleViolation(defaultConfig));
					}
				}
			}
			ruleViolationPreviewList.addAll(ruleViolationList);
		}
		ruleViolationPreviewList = getRuleViolationService().filterViolationListForDupesAndInactive(ruleViolationPreviewList);
		ruleViolationPreviewList = BeanUtils.sortWithFunctions(ruleViolationPreviewList, CollectionUtils.createList(
						RuleViolation::getIgnoreNote,
						ruleViolation -> ruleViolation.getRuleAssignment().isSnoozed(),
						ruleViolation -> ruleViolation.getRuleAssignment().getRuleDefinition().getName()),
				CollectionUtils.createList(true, true, true));
		return ruleViolationPreviewList;
	}


	private LocalDate getActiveOnDate(IdentityObject entity, RuleCategory ruleCategory) {
		LocalDate activeOnDate = null;
		String activeOnDateBeanFieldPath = ruleCategory.getActiveOnDateBeanFieldPath();
		if (activeOnDateBeanFieldPath != null) {
			activeOnDate = DateUtils.asLocalDate((Date) BeanUtils.getPropertyValue(entity, ruleCategory.getActiveOnDateBeanFieldPath()));
		}
		return activeOnDate;
	}


	/*
	This method creates violations for rules that did not fail, but may have passed or been excluded.
	The violationNote is used in the UI to provide useful information in identifying the application of rules.
	 */
	private RuleViolation createPreviewRuleViolation(EntityConfig entityConfig) {
		RuleAssignment ruleAssignment = getRuleDefinitionService().getRuleAssignment(entityConfig.getRuleAssignmentId());
		RuleViolation ruleViolation = processEntity(entityConfig);
		ruleViolation.setRuleAssignment(ruleAssignment);
		ruleViolation.setEntityFkFieldId(entityConfig.getEntityFkFieldId());

		String violationNote = ruleAssignment.getRuleDefinition().getName();
		if (entityConfig.getAdditionalScopeLabel() != null) {
			violationNote += " : " + entityConfig.getAdditionalScopeLabel();
		}
		if (entityConfig.getEntityLabel() != null) {
			violationNote += " : " + entityConfig.getEntityLabel();
		}
		ruleViolation.setViolationNote(violationNote);
		return ruleViolation;
	}


	/*
	This method appends additional labels to the ignoreNote for preview functionality.
	The UI groups the violations based on the ignoreNote so that violations for specific
	entities are easily identified.
	 */
	private RuleViolation processEntity(EntityConfig entityConfig) {
		RuleViolation ruleViolation = new RuleViolation();
		String ignoreNote = "Passed";
		if (entityConfig != null && entityConfig.isExcluded()) {
			ignoreNote = "Excluded";
		}
		if (entityConfig != null && entityConfig.isSnoozed()) {
			ignoreNote = "Snoozed";
		}
		if (!"Passed".equals(ignoreNote)) {
			if (entityConfig.getAdditionalScopeFkFieldId() != null) {
				if (entityConfig.getEntityFkFieldId() != null) {
					ignoreNote += ": Entity";
				}
				else {
					ignoreNote += ": Additional Scope";
				}
			}
			else if (entityConfig.getEntityFkFieldId() == null) {
				ignoreNote += ": Global";
			}
		}
		ruleViolation.setIgnoreNote(ignoreNote);
		return ruleViolation;
	}


	private List<RuleViolation> getExistingRuleViolations(RuleCategory category, IdentityObject entity, Boolean preProcess) {
		List<RuleViolation> violationList = getRuleViolationService().getRuleViolationListByLinkedEntity(category.getCategoryTable().getName(), BeanUtils.getIdentityAsLong(entity));
		return CollectionUtils.getStream(violationList)
				.filter(ruleViolation -> ruleViolation.getRuleAssignment().getRuleDefinition().getEvaluatorBean() != null)
				.filter(ruleViolation -> preProcess == null || preProcess == ruleViolation.getRuleAssignment().getRuleDefinition().isPreProcess())
				.filter(ruleViolation -> !ruleViolation.getRuleAssignment().getRuleDefinition().isManual())
				.filter(ruleViolation -> CompareUtils.isEqual(category, ruleViolation.getRuleAssignment().getRuleDefinition().getRuleCategory()))
				.collect(Collectors.toList());
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleRetrieverService getRuleRetrieverService() {
		return this.ruleRetrieverService;
	}


	public void setRuleRetrieverService(RuleRetrieverService ruleRetrieverService) {
		this.ruleRetrieverService = ruleRetrieverService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
