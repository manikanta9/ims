package com.clifton.rule.evaluator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.bean.SystemBeanService;


/**
 * The BaseRuleEvaluator provides all subclasses access to the injected services.  Primarily this is used to allow
 * RuleEvaluators to call getRuleViolationService.createRuleViolation(...) for clean and consistent use.
 *
 * @author StevenF
 */
public abstract class BaseRuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> implements RuleEvaluator<T, K> {

	public RuleDefinitionService ruleDefinitionService;
	public RuleViolationService ruleViolationService;
	public SystemBeanService systemBeanService;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
