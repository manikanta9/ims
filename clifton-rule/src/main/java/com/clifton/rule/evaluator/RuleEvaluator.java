package com.clifton.rule.evaluator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.violation.RuleViolation;

import java.util.List;


/**
 * The RuleEvaluator interface must be implemented by each rule implementation that provides rule specific business logic.
 *
 * @author StevenF
 */
public interface RuleEvaluator<T extends IdentityObject, K extends RuleEvaluatorContext> {

	public static final String ADDITIONAL_SCOPE_ENTITY_FREEMARKER_TEMPLATE_KEY = "additionalScopeEntity";
	public static final String CAUSE_ENTITY_FREEMARKER_TEMPLATE_KEY = "causeEntity";
	public static final String ENTITY_CONFIG_FREEMARKER_TEMPLATE_KEY = "entityConfig";
	public static final String ENTITY_SCOPE_FREEMARKER_TEMPLATE_KEY = "entityScope";
	public static final String LINKED_ENTITY_FREEMARKER_TEMPLATE_KEY = "linkedEntity";
	public static final String VIOLATION_NOTE_FREEMARKER_TEMPLATE_KEY = "violationNote";


	/**
	 * Executes {@link com.clifton.rule.definition.RuleDefinition} specific business logic for the specified ruleBean.
	 * Applies entity specific rule configuration parameters from the specified ruleConfig where applicable.
	 * Returns a List of {@link RuleViolation} objects for every violation found.  Returns an empty List if no violations were found.
	 * <p>
	 * The specified context object is used to retrieve and cache common data used by multiple RuleEvaluator objects that are often
	 * executed sequentially with the same context.
	 */
	public List<RuleViolation> evaluateRule(T ruleBean, RuleConfig ruleConfig, K context);
}
