package com.clifton.rule.converter;


import com.clifton.core.beans.IdentityObject;

import java.util.List;


/**
 * The RuleConverter interface must be implemented by each implementation
 * that provides conversion from one entity to another to facilitate rule processing.
 *
 * @author StevenF
 */
@SuppressWarnings("JavadocReference")
public interface RuleEntityConverter<I extends IdentityObject, O extends IdentityObject> {

	/**
	 * Converts the given entity to one or more other entities for rule processing.
	 * e.g. {@link AccountingPositionTransfer} is converted into one or more {@link AccountingPositionTransferRuleDetail}
	 * which implements 'AccountingBean' - this allows for consistent rule processing both real-time and during preview.
	 */
	public List<O> convertEntity(I entity);
}
