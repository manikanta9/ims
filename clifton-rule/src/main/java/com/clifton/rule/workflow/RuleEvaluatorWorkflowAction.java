package com.clifton.rule.workflow;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>RuleEvaluatorWorkflowAction</code> class represents a workflow transition action that generates rule violations for the specified bean.
 *
 * @author vgomelsky
 */
public class RuleEvaluatorWorkflowAction<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T> {

	private RuleDefinitionService ruleDefinitionService;
	private RuleEvaluatorService ruleEvaluatorService;
	private RuleViolationStatusService ruleViolationStatusService;
	private DaoLocator daoLocator;

	private short ruleCategoryId;
	private boolean processViolationStatus;

	// Values are Yes = Pre-Process Rules Only, No = Post-Process Rules Only, or Blank = all Rules
	private String preProcessOnly;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		RuleCategory ruleCategory = getRuleDefinitionService().getRuleCategory(getRuleCategoryId());
		ValidationUtils.assertNotNull(ruleCategory, "Cannot find required RuleCategory for id = " + getRuleCategoryId());

		// Empty Value means run all
		Boolean preProcess = null;
		if (!StringUtils.isEmpty(getPreProcessOnly())) {
			preProcess = BooleanUtils.isTrue(getPreProcessOnly());
		}
		getRuleEvaluatorService().executeRules(ruleCategory.getName(), BeanUtils.getIdentityAsLong(bean), preProcess);

		if (isProcessViolationStatus()) {
			processViolationStatus(bean);
		}
		return bean;
	}


	private void processViolationStatus(T bean) {
		ValidationUtils.assertTrue(RuleViolationUtil.isRuleViolationAwareSupported(bean), "Processing rules: Process Violation Status = true but Bean is not an instance of RuleViolationAware");
		// After processing is complete - Move the bean to Processed (ONLY IF NOT ALREADY THERE)
		RuleViolationAware ruleViolationAwareBean = RuleViolationUtil.getRuleViolationAwareBean(bean);
		if (ruleViolationAwareBean != null && !RuleViolationUtil.isRuleViolationAwareProcessed(ruleViolationAwareBean)) {
			ruleViolationAwareBean.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));
			ReadOnlyDAO<T> beanDAO = getDaoLocator().locate(bean);

			if (beanDAO instanceof UpdatableDAO) {
				((UpdatableDAO<T>) beanDAO).save(bean);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public short getRuleCategoryId() {
		return this.ruleCategoryId;
	}


	public void setRuleCategoryId(short ruleCategoryId) {
		this.ruleCategoryId = ruleCategoryId;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleEvaluatorService getRuleEvaluatorService() {
		return this.ruleEvaluatorService;
	}


	public void setRuleEvaluatorService(RuleEvaluatorService ruleEvaluatorService) {
		this.ruleEvaluatorService = ruleEvaluatorService;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public boolean isProcessViolationStatus() {
		return this.processViolationStatus;
	}


	public void setProcessViolationStatus(boolean processViolationStatus) {
		this.processViolationStatus = processViolationStatus;
	}


	public String getPreProcessOnly() {
		return this.preProcessOnly;
	}


	public void setPreProcessOnly(String preProcessOnly) {
		this.preProcessOnly = preProcessOnly;
	}
}
