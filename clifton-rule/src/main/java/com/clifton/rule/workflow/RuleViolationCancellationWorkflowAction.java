package com.clifton.rule.workflow;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The RuleViolationCancellationWorkflowAction class retrieves all rule violations associated with the bean going through
 * the workflow transition (Trade, etc.) and updates all of these violations to be marked as cancelled.
 *
 * @author vgomelsky
 */
public class RuleViolationCancellationWorkflowAction<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T> {

	private DaoLocator daoLocator;

	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		// get all violations/warnings that exist for the bean
		String tableName = getDaoLocator().locate(bean.getClass()).getConfiguration().getTableName();
		Long beanId = BeanUtils.getIdentityAsLong(bean);
		ValidationUtils.assertNotNull(beanId, "Cannot check rule violations or system warnings for a bean that does not exist: " + bean, "id");

		List<RuleViolation> violationList = getRuleViolationService().getRuleViolationListByLinkedEntity(tableName, beanId);
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (!violation.isCancelledLinkedEntity()) {
				violation.setCancelledLinkedEntity(true);
				getRuleViolationService().saveRuleViolation(violation);
			}
		}

		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
