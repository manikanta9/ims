// Used for all warnings that aren't manual
Clifton.rule.violation.GenericViolationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rule Violation',
	iconCls: 'rule',
	height: 550,
	width: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Violation',
				tbar: [
					{
						text: 'View Cause',
						tooltip: 'View entity that caused this violation',
						iconCls: 'view',
						handler: function() {
							const fp = TCG.getParentTabPanel(this).getWindow().getMainFormPanel();
							const f = fp.getForm();
							const causeId = TCG.getValue('causeFkFieldId', f.formValues);
							const causeDetailScreen = TCG.getValue('violationCauseTable.detailScreenClass', f.formValues);
							if (TCG.isNotBlank(causeDetailScreen) && TCG.isNotBlank(causeId)) {
								const cmpId = TCG.getComponentId(causeDetailScreen, causeId);
								TCG.createComponent(causeDetailScreen, {
									id: cmpId,
									params: {id: causeId}
								});
							}
							else {
								TCG.showError('Link to the cause entity is not available for this violation.', 'Not Available');
							}
						}
					},
					'-',
					{
						text: 'View Link',
						tooltip: 'View entity that is linked to this violation',
						iconCls: 'view',
						handler: function() {
							const fp = TCG.getParentTabPanel(this).getWindow().getMainFormPanel();
							const f = fp.getForm();
							const linkedId = TCG.getValue('linkedFkFieldId', f.formValues);
							const linkedDetailScreen = TCG.getValue('ruleAssignment.ruleDefinition.ruleCategory.categoryTable.detailScreenClass', f.formValues);
							if (TCG.isNotBlank(linkedDetailScreen) && TCG.isNotBlank(linkedId)) {
								const cmpId = TCG.getComponentId(linkedDetailScreen, linkedId);
								TCG.createComponent(linkedDetailScreen, {
									id: cmpId,
									params: {id: linkedId}
								});
							}
							else {
								TCG.showError('Link to the linked entity is not available for this violation.', 'Not Available');
							}
						}
					},
					'-',
					{
						text: 'View Entity',
						tooltip: 'View entity scope for this violation',
						iconCls: 'view',
						handler: function() {
							const fp = TCG.getParentTabPanel(this).getWindow().getMainFormPanel();
							const f = fp.getForm();
							const entityId = TCG.getValue('entityFkFieldId', f.formValues);
							const entityDetailScreen = TCG.getValue('ruleAssignment.ruleDefinition.entityTable.detailScreenClass', f.formValues);
							if (!TCG.isBlank(entityDetailScreen) && !TCG.isBlank(entityId)) {
								const cmpId = TCG.getComponentId(entityDetailScreen, entityId);
								TCG.createComponent(entityDetailScreen, {
									id: cmpId,
									params: {id: entityId}
								});
							}
							else {
								TCG.showError('Link to the entity scope is not available for this violation.', 'Not Available');
							}
						}
					},
					'->',
					{
						text: 'Guidance',
						tooltip: 'Click to see guidance for this rule definition',
						iconCls: 'help',
						handler: function() {
							const fp = TCG.getParentTabPanel(this).getWindow().getMainFormPanel();
							new TCG.app.CloseWindow({
								title: 'Guidance Help - ' + fp.getFormValue('ruleAssignment.ruleDefinition.name'),
								iconCls: 'help',
								width: 750,
								height: 450,
								modal: true,
								openerCt: this,
								items: [{
									xtype: 'formpanel',
									labelWidth: 1,
									items: [
										{
											xtype: 'htmleditor',
											value: fp.getFormValue('ruleAssignment.ruleDefinition.description'),
											anchor: '0 -20',
											readOnly: true
										}
									]
								}]
							});
						}
					}
				],

				items: [{
					xtype: 'formpanel',
					labelWidth: 150,
					instructions: 'A rule violation flags a possible issue in the system.  Processing entities can only occur if there are no unresolved violations.  Violations that can be ignored will allow processing to continue without specified data.',
					url: 'ruleViolation.json',
					labelFieldName: 'ruleAssignment.ruleDefinition.name',
					getWarningMessage: function(form) {
						if (TCG.getValue('cancelledLinkedEntity', form.formValues)) {
							return 'This violation was cancelled because of its Linked entity. See Linked entity for more details.';
						}
						return undefined;
					},
					listeners: {
						afterload: function(panel) {
							// If warning type is not ignorable disable ignore fields
							const f = panel.getForm();
							const ig = TCG.getValue('ruleAssignment.ruleDefinition.ignorable', f.formValues);
							if (!TCG.isTrue(ig)) {
								// Get ignore fields and disable them
								let field = f.findField('ignored');
								field.setDisabled(true);
								field = f.findField('ignoreNote');
								field.setDisabled(true);
							}
						}
					},
					items: [
						{fieldLabel: 'Rule Definition', name: 'ruleAssignment.ruleDefinition.name', xtype: 'linkfield', detailIdField: 'ruleAssignment.ruleDefinition.id', detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow'},
						{
							fieldLabel: 'Violation Message', name: 'violationNote', xtype: 'htmleditor',
							anchor: '0 -250',
							createToolbar: function(editor) {
								//hide toolbar
								this.tb = new Ext.Toolbar({
									renderTo: this.wrap.dom.firstChild, items: [], hidden: true
								});
							},
							getValue: function() {
								return this.value;
							},
							submitValue: false,
							readOnly: true,
							plugins: [new TCG.form.HtmlEditorFreemarker()]
						},
						{xtype: 'sectionheaderfield', header: 'Ignore Info'},
						{name: 'ignored', xtype: 'checkbox', boxLabel: 'Ignore this violation and allow processing to continue.'},
						{fieldLabel: 'Ignore Note', name: 'ignoreNote', xtype: 'textarea'},
						{
							fieldLabel: 'Recent Ignore Notes', name: 'recentIgnoreNote', displayField: 'ignoreNote', tooltipField: 'ignoreNote',
							xtype: 'combo', url: 'ruleViolationIgnoreNoteListFind.json',
							qtip: 'Used to copy an existing Ignore Note for this entity and rule definition from last 7 calendar days. The note can be updated before saving.',
							allowBlank: true, forceSelection: false, doNotAddContextMenu: true,
							listeners: {
								beforequery: function(queryEvent) {
									const fp = TCG.getParentTabPanel(this).getWindow().getMainFormPanel();
									const f = fp.getForm();
									queryEvent.combo.store.baseParams = {
										linkedTableName: TCG.getValue('ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name', f.formValues),
										linkedFkFieldId: TCG.getValue('linkedFkFieldId', f.formValues),
										requestedProperties: 'ignoreNote',
										requestedPropertiesRoot: 'data',
										ruleDefinitionIds: TCG.getValue('ruleAssignment.ruleDefinition.id', f.formValues)
									};
								},
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									fp.setFormValue('ignoreNote', record.json.ignoreNote);
								}
							}
						}
					]
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'RuleViolation'
				}]
			}
		]
	}]
});
