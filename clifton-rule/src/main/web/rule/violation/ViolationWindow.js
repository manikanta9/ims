// not the actual window but a window selector:
//   - generic violations (generated by the system) or manual (adding/edited/deleted by users)
Clifton.rule.violation.ViolationWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'ruleViolation.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.rule.violation.ManualViolationWindow';
		if (entity && entity.ruleAssignment.ruleDefinition && entity.ruleAssignment.ruleDefinition.manual === false) {
			className = 'Clifton.rule.violation.GenericViolationWindow';
		}
		return className;
	}
});
