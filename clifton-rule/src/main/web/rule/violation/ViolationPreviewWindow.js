Clifton.rule.violation.ViolationPreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Rule Violation Preview',
	iconCls: 'rule',
	width: 950,
	height: 500,

	items: [{
		xtype: 'gridpanel',
		name: 'ruleViolationEvaluatorsPreviewRunList',
		instructions: 'Violation Preview can be used to see what violations would apply now for the entity, as well as what other violations were processed and passed, or what was excluded from processing.',
		groupField: 'ignoreNote',
		emptyGroupText: 'Failed',
		groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Violations" : "Violation"]})',
		pageSize: 200,
		appendStandardColumns: false,
		remoteSort: true,
		useBufferView: false, // allow cell wrapping

		columns: [
			{header: 'ID', width: 15, dataIndex: 'ruleAssignment.ruleDefinition.id', hidden: true},
			{header: 'CauseFKFieldID', width: 15, dataIndex: 'causeFkFieldId', hidden: true},
			{header: 'Violation Definition', width: 100, dataIndex: 'ruleAssignment.ruleDefinition.name', hidden: true},
			{header: 'Ignorable', width: 50, dataIndex: 'ruleAssignment.ruleDefinition.ignorable', type: 'boolean', hidden: true},
			{header: 'Snoozed', width: 50, dataIndex: 'ruleAssignment.snoozed', type: 'boolean', hidden: true},
			{
				header: 'Violation Message', width: 200, dataIndex: 'violationNote',
				renderer: function(v, c, r) {
					if (TCG.isBlank(r.data['ignoreNote'])) {
						const str = '<div style="WHITE-SPACE: normal; PADDING: 3px"><b>{0}: </b><br />{1}</div>';
						return String.format(str, r.data['ruleAssignment.ruleDefinition.name'], v);
					}
					return v;
				}
			},
			{
				header: 'Status', width: 85, dataIndex: 'ignoreNote', hidden: true,
				renderer: function(v, c, r) {
					if (TCG.isBlank(v)) {
						if (TCG.isTrue(r.data['ruleAssignment.snoozed'])) {
							return 'Failed: Snoozed';
						}
						else {
							return 'Failed';
						}
					}
					return v;
				}
			},
			{
				header: 'Cause', width: 20, dataIndex: 'causeTable', align: 'center',
				renderer: function(clz, args, r) {
					if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.causeFkFieldId)) {
						return TCG.renderActionColumn('view', 'View', 'View entity that caused this violation');
					}
					return 'N/A';
				}
			}
		],
		getLoadParams: function() {
			const win = this.getWindow();
			return {
				categoryTableName: win.params.categoryTableName,
				categoryTableEntityId: win.params.categoryTableEntityId
			};
		},
		editor: {
			detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow',
			getDetailPageId: function(gridPanel, row) {
				return row.data['ruleAssignment.ruleDefinition.id'];
			}
		},
		gridConfig: {
			listeners: {
				'rowclick': function(grid, rowIndex, evt) {
					if (TCG.isActionColumn(evt.target)) {
						const row = grid.store.data.items[rowIndex];
						const clz = row.json.causeTable.detailScreenClass;
						const id = row.json.causeFkFieldId;
						const gridPanel = grid.ownerGridPanel;
						gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
					}
				}
			}
		}
	}]
});
