// Used for manual warnings
Clifton.rule.violation.ManualViolationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Manual Rule Violation',
	iconCls: 'rule',
	width: 700,

	tbar: [{
		text: 'View Link',
		tooltip: 'View entity that is linked to this violation',
		iconCls: 'view',
		handler: function() {
			const fp = this.ownerCt.ownerCt.items.get(0);
			const f = fp.getForm();
			const linkedId = TCG.getValue('linkedFkFieldId', f.formValues);
			const linkedDetailScreen = TCG.getValue('ruleAssignment.ruleDefinition.ruleCategory.categoryTable.detailScreenClass', f.formValues);
			if (TCG.isNotBlank(linkedDetailScreen) && TCG.isNotBlank(linkedId)) {
				const cmpId = TCG.getComponentId(linkedDetailScreen, linkedId);
				TCG.createComponent(linkedDetailScreen, {
					id: cmpId,
					params: {id: linkedId}
				});
			}
			else {
				TCG.showError('Link to the linked entity is not available for this violation.', 'Not Available');
			}
		}
	}],

	items: [{
		xtype: 'formpanel',
		instructions: 'A rule violation flags a possible issue in the system.  Manual violations are entered by users to flag an issue or bring attention to something that could not be flagged by the system.',
		url: 'ruleViolation.json',
		getSaveURL: function() {
			return 'ruleViolationManualSave.json';
		},
		labelFieldName: 'ruleAssignment.ruleDefinition.name',
		getWarningMessage: function(form) {
			if (TCG.getValue('cancelledLinkedEntity', form.formValues)) {
				return 'This violation was cancelled because of its Linked entity.';
			}
			return undefined;
		},
		listeners: {
			afterload: function(panel) {
				// If warning type is not ignorable disable ignore fields
				// Most cases these warnings should always be ignorable, but
				// may be cases where users want to enter a stop on something where an issue
				// needs to be resolved.  For these cases, the only way around it is to manually
				// delete the warning.
				const f = panel.getForm();
				const ig = TCG.getValue('ruleAssignment.ruleDefinition.ignorable', f.formValues);
				if (!TCG.isTrue(ig)) {
					// Get ignore fields and disable them
					let field = f.findField('ignored');
					field.setDisabled(true);
					field = f.findField('ignoreNote');
					field.setDisabled(true);
				}
			}
		},
		items: [
			{name: 'linkedFkFieldId', xtype: 'hidden'},
			{
				fieldLabel: 'Rule Definition', name: 'ruleAssignment.name', xtype: 'combo', hiddenName: 'ruleAssignment.id', displayField: 'ruleDefinition.name', detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow',
				url: 'ruleAssignmentListFind.json?manual=true'
			},
			{fieldLabel: 'Violation Message', name: 'violationNote', xtype: 'textarea'},
			{xtype: 'sectionheaderfield', header: 'Ignore Info'},
			{name: 'ignored', xtype: 'checkbox', boxLabel: 'Ignore this violation and allow processing to continue.'},
			{fieldLabel: 'Ignore Note', name: 'ignoreNote', xtype: 'textarea'}
		]
	}]
});
