Clifton.rule.definition.RuleDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rule Definition',
	iconCls: 'rule',
	width: 1200,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Definition',
				items: [{
					xtype: 'formpanel',
					instructions: 'A rule definition defines a specific business rule and what table it can be linked to.  System Defined rule definition names cannot be changed. Definition Amounts labels also drive if the field applies to the definition.  i.e. On/Off type rules would not use any labels.',
					url: 'ruleDefinition.json',
					labelWidth: 140,
					items: [
						{fieldLabel: 'Category', name: 'ruleCategory.name', hiddenName: 'ruleCategory.id', xtype: 'combo', url: 'ruleCategoryListFind.json', detailPageClass: 'Clifton.rule.definition.RuleCategoryWindow'},
						{fieldLabel: 'Definition Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'htmleditor', height: 100},
						{
							fieldLabel: 'Rule Scope', name: 'ruleScope.name', hiddenName: 'ruleScope.id', xtype: 'combo', url: 'ruleScopeListFind.json', detailPageClass: 'Clifton.rule.definition.RuleScopeWindow', requiredFields: ['ruleCategory.name'],
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								combo.store.baseParams = {ruleCategoryId: f.findField('ruleCategory.name').getValue()};
							}
						},
						{
							fieldLabel: 'Evaluator Bean', name: 'evaluatorBean.name', hiddenName: 'evaluatorBean.id', displayField: 'name', xtype: 'combo', url: 'systemBeanListFind.json?groupName=System Rule Evaluator',
							mutuallyExclusiveFields: ['manual'],
							detailPageClass: 'Clifton.system.bean.BeanWindow',
							getDefaultData: function() {
								return {type: {group: {name: 'System Rule Evaluator'}}};
							}
						},
						{fieldLabel: 'Modify Condition', name: 'entityModifyCondition.name', hiddenName: 'entityModifyCondition.id', xtype: 'system-condition-combo', qtip: 'Specifies the modify condition that must evaluate to \'true\' for an assignment for this definition to be modified.'},
						{fieldLabel: 'Priority', name: 'priority.label', hiddenName: 'priority.id', displayField: 'label', xtype: 'combo', url: 'systemPriorityListFind.json', detailPageClass: 'Clifton.system.priority.PriorityWindow'},
						{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield'},

						{xtype: 'sectionheaderfield', header: 'Related Entities Configuration'},
						{fieldLabel: 'Cause Table', name: 'causeTable.name', hiddenName: 'causeTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', disableAddNewItem: true, qtip: 'The default table where a violation would originate from. If a rule does not explicitly set the cause table when creating a violation, then this causeTable is used.'},
						{fieldLabel: 'Entity Table', name: 'entityTable.name', hiddenName: 'entityTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', disableAddNewItem: true, qtip: 'Optionally some rule definitions may apply to more specific "entities". For example, InvestmentReplication or InvestmentManagerAccount for a Portfolio Run.'},
						{fieldLabel: 'Entity Value List URL', name: 'entityValueListUrl', qtip: 'Optionally some rule definitions may allow for selection of an applicable entity from a list.'},
						{fieldLabel: 'Entity Value UI Config', name: 'entityValueUserInterfaceConfig', xtype: 'textarea', height: 35, grow: true, qtip: 'Optional override for Value List User Interface.'},

						{xtype: 'sectionheaderfield', header: 'Processing Configuration'},
						{
							fieldLabel: 'Manual', boxLabel: 'Check for violations that are added manually by users via UI', xtype: 'checkbox', name: 'manual', mutuallyExclusiveFields: ['evaluatorBean.name', 'systemDefined'],
							qtip: 'Manual violations are entered by users to add additional information that the system cannot flag automatically.'
						},
						{
							fieldLabel: 'Name System Defined', boxLabel: 'Check if definition name is referenced in code and can not be changed', xtype: 'checkbox', name: 'nameSystemDefined',
							qtip: 'System Defined Names are referenced in code and/or reporting functionality and so may not be changed in order to maintain the integrity of the referencing code. Example: A rule referenced in an SQL report for displaying rule violations of that type.'
						},
						{
							fieldLabel: 'System Defined', boxLabel: 'Check if this definition does not use an evaluator bean, but is used when creating violations directly from code.', xtype: 'checkbox', name: 'systemDefined', mutuallyExclusiveFields: ['manual'],
							qtip: 'System Defined Violations are most commonly generated in the code and not handled by a rule evaluator.  Because of this, they are referenced by name. Example: Exceptions found during processing for a missing price that prevents processing to continue.'
						},
						{fieldLabel: 'Pre-Process', name: 'preProcess', xtype: 'checkbox', boxLabel: 'Check to generate violations before processing occurs, rather than after'},
						{fieldLabel: 'Filter By Note Only', name: 'useViolationNoteFilteringOnly', xtype: 'checkbox', boxLabel: 'Check to filter duplicates for this definition using the violation note only'},
						{fieldLabel: 'Inactive', name: 'inactive', xtype: 'checkbox'},

						{xtype: 'sectionheaderfield', header: 'Violations Configuration'},
						{fieldLabel: 'Max Snooze Days', name: 'maxSnoozeDays', xtype: 'integerfield', qtip: 'Maximum number of days that violations for this definition may be snoozed for.  A value of 0 indicates the violations generated by this definition are not snoozable.'},
						{fieldLabel: 'Rule Violation Code', name:'ruleViolationCode', qtip:'For each Rule Definition, we may have a Rule Code (Optional) with a 1 or 2 digit character that represents that rule'},
						{
							fieldLabel: 'Violation Note Template', name: 'violationNoteTemplate', xtype: 'textarea', height: 70, grow: true, qtip: TCG.trimWhitespace(`
								<p>The Freemarker template that will be used to generate rule violation notes.</p>
								<br/>
								<p>You can use the following variables and their attributes:</p>
								<ul>
									<li>- <code>causeEntity</code> (Object)</li>
									<li>- <code>linkedEntity</code> (Object)</li>
									<li>- <code>additionalScopeEntity</code> (Object)</li>
									<li>- <code>rangeValue</code> (BigDecimal)</li>
									<li>- <code>minAmount</code> (BigDecimal)</li>
									<li>- <code>maxAmount</code> (BigDecimal)</li>
									<li>- <code>greaterThanRange</code> (Boolean)</li>
									<li>- <code>lessThanRange</code> (Boolean)</li>
								</ul>
								<br/>
								<p>Additionally, the following custom functions may be used:</p>
								<ul>
									<li>- <code>formatNumberMoney(BigDecimal value)</code></li>
									<li>- <code>formatNumberInteger(BigDecimal value)</code></li>
									<li>- <code>formatNumberPercent(BigDecimal value)</code></li>
									<li>- <code>formatNumberPrecise(BigDecimal value)</code></li>
								</ul>
							`)
						},

						{
							xtype: 'fieldset-checkbox',
							title: 'Ignorable',
							labelWidth: 125,
							checkboxName: 'ignorable',
							instructions: 'Violations for this rule can be ignored.  If violations are ignored, processing will be allowed to continue. If an ignore condition is set, then the entity must fulfill that condition in order to be ignored.  This is useful to restricting who can ignore violations for this rule.  Unless specified to always pass linked entity, conditions will be passed the Cause Entity (if exists - see Evaluator Bean) otherwise will be passed the linked entity.',
							items: [
								{name: 'automaticIgnoreOnCreate', xtype: 'checkbox', boxLabel: 'Automatically mark violations as ignored when created', mutuallyExclusiveFields: ['ignoreNoteRequired', 'ignoreCondition.name']},
								{name: 'ignoreNoteRequired', xtype: 'checkbox', boxLabel: 'Require a note to be entered when warnings are ignored', mutuallyExclusiveFields: ['automaticIgnoreOnCreate']},
								{fieldLabel: 'Ignore Condition', name: 'ignoreCondition.name', hiddenName: 'ignoreCondition.id', xtype: 'system-condition-combo', mutuallyExclusiveFields: ['automaticIgnoreOnCreate']},
								{
									name: 'useLinkedEntityForIgnoreCondition', xtype: 'checkbox', boxLabel: 'Always Pass Linked Entity to Ignore Condition', requiredFields: ['ignoreCondition.name'],
									qtip: 'If left unchecked, the ignore condition will be passed the cause entity (if exists) else the linked entity.  Otherwise, if checked, the ignore condition will always be passed the linked entity.'
								}
							]
						},

						{xtype: 'sectionheaderfield', header: 'Rule Assignment Amounts Labels'},
						{fieldLabel: 'Rule Amount Label', name: 'ruleAmountLabel'},
						{fieldLabel: 'Rule Amount Description', name: 'ruleAmountDescription'},
						{fieldLabel: 'Min Amount Label', name: 'minAmountLabel'},
						{fieldLabel: 'Min Amount Description', name: 'minAmountDescription'},
						{fieldLabel: 'Max Amount Label', name: 'maxAmountLabel'},
						{fieldLabel: 'Max Amount Description', name: 'maxAmountDescription'}
					]
				}]
			},


			{
				title: 'Rule Assignments',
				items: [{
					xtype: 'rule-assignment-grid',
					groupField: undefined,
					groupTextTpl: undefined,
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Active On Date', xtype: 'toolbar-datefield', name: 'activeOnDate'}
						];
					},
					editor: {
						detailPageClass: 'Clifton.rule.definition.RuleAssignmentWindow',
						getDefaultData: function(gridPanel) { // defaults category for the detail page
							return {
								ruleDefinition: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function(firstLoad) {
						const dateField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
						const params = {};
						if (firstLoad) {
							// default to today
							if (dateField.getValue() === '') {
								dateField.setValue((new Date()).format('m/d/Y'));
							}
						}
						if (dateField.getValue() !== '') {
							params.activeOnDate = (dateField.getValue()).format('m/d/Y');

						}
						params.ruleDefinitionId = this.getWindow().getMainFormId();
						return params;
					}
				}]
			},


			{
				title: 'Rule Violations',
				items: [{
					xtype: 'base-rule-violation-grid',
					instructions: 'The following rule violations have been recorded for selected rule definition. Defaults to last 7 days.',
					includeRuleCategoryFilter: false,
					includeRuleDefinitionFilter: false,
					includeDisplayFilter: true,
					groupField: undefined,
					hiddenColumns: [],
					columnOverrides: [
						{dataIndex: 'cancelledLinkedEntity', hidden: false},
						{dataIndex: 'createDate', defaultSortColumn: true, defaultSortDirection: 'DESC'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('createDate', {'after': new Date().add(Date.DAY, -7)});
						}
						return {ruleDefinitionId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'RuleDefinition'
				}]
			}
		]
	}]
});
