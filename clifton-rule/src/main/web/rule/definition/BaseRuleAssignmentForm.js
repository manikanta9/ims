Clifton.rule.definition.BaseRuleAssignmentForm = Ext.extend(TCG.form.FormPanel, {
	instructions: 'A rule assignment defines specific limitations that a rule should follow and when that rule is violated a violation is generated. Global rules apply to all, or all limited by the rule definition scope.',
	url: 'ruleAssignment.json',
	labelWidth: 170,
	labelFieldName: 'label',

	initComponent: function() {
		const currentItems = [];
		Ext.each(this.assignmentSpecificColumns, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.definitionItems, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.commonItems, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.rule.definition.BaseRuleAssignmentForm.superclass.initComponent.call(this);
	},
	assignmentSpecificColumns: [],

	getWarningMessage: function(form) {
		if (form.formValues.excluded === true) {
			return 'Execution of this rule is EXCLUDED for the specified scope.';
		}
		return undefined;
	},

	listeners: {
		afterload: function(form, isClosing) {
			this.setResetAmountFields();
			if (this.resetAdditionalFields) {
				this.resetAdditionalFields();
			}
		}
	},

	definitionItems: [
		{fieldLabel: 'Rule Category', name: 'ruleDefinition.ruleCategory.name', detailIdField: 'ruleDefinition.ruleCategory.id', xtype: 'linkfield', detailPageClass: 'Clifton.rule.definition.RuleCategoryWindow', submitValue: false, submitDetailField: false},
		{
			fieldLabel: 'Rule Definition', name: 'ruleDefinition.name', hiddenName: 'ruleDefinition.id', xtype: 'combo', url: 'ruleDefinitionListFind.json',
			detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow',
			beforequery: function(queryEvent) {
				const combo = queryEvent.combo;
				const f = combo.getParentForm().getForm();
				const rc = f.findField('ruleDefinition.ruleCategory.id').getValue();
				combo.store.baseParams = {ruleCategoryId: rc};
				if (f.findField('ruleDefinition.entityTable.name')) {
					combo.store.baseParams.entityTableName = f.findField('ruleDefinition.entityTable.name').getValue();
				}
			},
			listeners: {
				select: function(f) {
					const p = TCG.getParentFormPanel(f);
					p.setResetAmountFields();
				}
			}
		},
		{name: 'ruleDefinition.description', xtype: 'textarea', readOnly: true}
	],

	commonItems: [
		{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 50},
		{
			fieldLabel: 'Start Date', xtype: 'container', layout: 'hbox',
			items: [
				{name: 'startDate', xtype: 'datefield', flex: 1},
				{value: '&nbsp;&nbsp;&nbsp;&nbsp;End Date:', xtype: 'displayfield', width: 165},
				{name: 'endDate', xtype: 'datefield', flex: 1}
			]
		},
		{xtype: 'sectionheaderfield', header: 'Configuration'},
		{
			boxLabel: 'Snooze From Rule Evaluation', xtype: 'checkbox', name: 'snoozed', mutuallyExclusiveFields: ['excluded'],
			listeners: {
				check: function(f) {
					const p = TCG.getParentFormPanel(f);
					p.setResetAmountFields();
				}
			}
		},
		{
			boxLabel: 'Exclude From Rule Evaluation', xtype: 'checkbox', name: 'excluded', mutuallyExclusiveFields: ['snoozed'],
			listeners: {
				check: function(f) {
					const p = TCG.getParentFormPanel(f);
					p.setResetAmountFields();
				}
			}
		},
		{fieldLabel: 'Rule Amount', name: 'ruleAmount', xtype: 'currencyfield', mutuallyExclusiveFields: ['excluded', 'snoozed'], visible: false},
		{fieldLabel: 'Minimum Amount', name: 'minAmount', xtype: 'currencyfield', mutuallyExclusiveFields: ['excluded', 'snoozed'], visible: false},
		{fieldLabel: 'Maximum Amount', name: 'maxAmount', xtype: 'currencyfield', mutuallyExclusiveFields: ['excluded', 'snoozed'], visible: false}
	],

	setResetAmountFields: function() {
		const f = this.getForm();
		const exclude = f.findField('excluded');
		const snoozed = f.findField('snoozed');
		const excludeValue = exclude.getValue();
		const snoozedValue = snoozed.getValue();
		const ruleDefinition = TCG.getValue('ruleDefinition', f.formValues);
		if (excludeValue === true || snoozedValue === true || !ruleDefinition) {
			this.setResetAmountField('ruleAmount');
			this.setResetAmountField('minAmount');
			this.setResetAmountField('maxAmount');
		}
		else {

			this.setResetAmountField('ruleAmount', ruleDefinition.ruleAmountLabel, ruleDefinition.ruleAmountDescription);
			this.setResetAmountField('minAmount', ruleDefinition.minAmountLabel, ruleDefinition.minAmountDescription);
			this.setResetAmountField('maxAmount', ruleDefinition.maxAmountLabel, ruleDefinition.maxAmountDescription);
		}
	},

	setResetAmountField: function(fieldName, fieldLabel, fieldDescription) {
		const formPanel = this;
		const form = formPanel.getForm();
		const field = form.findField(fieldName);
		if (fieldLabel && fieldLabel !== '') {
			field.setVisible(true);
			field.setFieldLabel(fieldLabel + ':');
			if (fieldDescription && fieldDescription !== '') {
				formPanel.setFieldQtip(fieldName, fieldDescription);
			}
		}
		else {
			field.setVisible(false);
		}
	}
});
Ext.reg('rule-assignment-form', Clifton.rule.definition.BaseRuleAssignmentForm);
