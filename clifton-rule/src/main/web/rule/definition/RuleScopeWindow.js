Clifton.rule.definition.RuleScopeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rule Scope',
	iconCls: 'rule',
	width: 670,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Scope',
				items: [{
					xtype: 'formpanel',
					instructions: 'Rule scopes limit the number of entities from the corresponding Rule Category for which a specific Rule Definition should apply. For example, if the scope is not set on Rule Definition, then the rule will apply to all Rule Category entities.',
					url: 'ruleScope.json',
					labelWidth: 140,
					items: [
						{fieldLabel: 'Rule Category', name: 'ruleCategory.name', hiddenName: 'ruleCategory.id', xtype: 'combo', url: 'ruleCategoryListFind.json', detailPageClass: 'Clifton.rule.definition.RuleCategoryWindow'},
						{fieldLabel: 'Scope Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Scope Bean Field Path', name: 'scopeBeanFieldPath', qtip: 'The path applies to Rule Category entities but starts after Additional Scope Path.'},
						{fieldLabel: 'Scope Bean Field Value', name: 'scopeBeanFieldValue'},
						{
							fieldLabel: 'Evaluator Context Bean', name: 'ruleEvaluatorContextBean.name', hiddenName: 'ruleEvaluatorContextBean.id', xtype: 'combo',
							url: 'systemBeanListFind.json?groupName=Rule Evaluator Context', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'System Bean that is used for Context during rule execution for rules in this category. Can optionally select a context bean here to override the context bean specified on the parent Rule Category.'
						}
					]
				}]
			},


			{
				title: 'Rule Definitions',
				items: [{
					name: 'ruleDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A list of business rules applicable to this scope.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 140, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Ignorable', width: 35, dataIndex: 'ignorable', type: 'boolean'},
						{header: 'Auto-ignore', width: 35, dataIndex: 'automaticIgnoreOnCreate', type: 'boolean'},
						{
							header: 'Pre-Process', width: 35, dataIndex: 'preProcess', type: 'boolean',
							qtip: 'Pre-Process warnings indicate the warning should be generated before any processing occurs.'
						},
						{header: 'Inactive', width: 30, dataIndex: 'inactive', type: 'boolean'},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int'}
					],
					getLoadParams: function(firstLoad) {
						return {ruleScopeId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow'
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'RuleScope'
				}]
			}
		]
	}]
});
