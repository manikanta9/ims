// not the actual window but a window selector:
//   - Global, Additional Scope or Additional Scope w/ Entity
Clifton.rule.definition.RuleAssignmentWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'ruleAssignment.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.rule.definition.GlobalAssignmentWindow';
		if (entity) {
			if (TCG.isNotBlank(entity.entityFkFieldId) && TCG.isBlank(entity.ruleDefinition.entityValueListUrl)) {
				className = 'Clifton.rule.definition.AdditionalScopeEntityAssignmentWindow';
			}
			else if (TCG.isNotBlank(entity.additionalScopeFkFieldId)) {
				className = 'Clifton.rule.definition.AdditionalScopeAssignmentWindow';
			}
		}
		else if (config.defaultData) {
			if (config.defaultData.entityFkFieldId && TCG.isBlank(config.defaultData.ruleDefinition.entityValueListUrl)) {
				className = 'Clifton.rule.definition.AdditionalScopeEntityAssignmentWindow';
			}
			else if (config.defaultData.additionalScopeFkFieldId) {
				className = 'Clifton.rule.definition.AdditionalScopeAssignmentWindow';
			}
		}
		return className;
	}
});
