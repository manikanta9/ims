Clifton.rule.definition.RuleCategoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rule Category',
	iconCls: 'rule',
	width: 670,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Category',
				items: [{
					xtype: 'formpanel',
					instructions: 'Rule Category defines entities in the Category Table that have specific rules assigned to and executed for. Additional Scope allows to define entities on Rule Assignments that should use different configuration or be excluded from rule execution.',
					url: 'ruleCategory.json',
					labelWidth: 140,
					items: [
						{fieldLabel: 'Category Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Category Table', name: 'categoryTable.name', hiddenName: 'categoryTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', disableAddNewItem: true},
						{fieldLabel: 'Active On Date Path', name: 'activeOnDateBeanFieldPath', qtip: 'The path to a date from an entity belonging to the category table (e.g. balanceDate for an entity from the PortfolioRun table).'},
						{
							fieldLabel: 'Entity Converter Bean', name: 'entityConverterBean.name', hiddenName: 'entityConverterBean.id', displayField: 'name', xtype: 'combo',
							url: 'systemBeanListFind.json?groupName=Rule Entity Converter', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'System Bean that is used for converting an entity into one or more compatible entities for rule evaluation.'
						}, {
							fieldLabel: 'Evaluator Context Bean', name: 'ruleEvaluatorContextBean.name', hiddenName: 'ruleEvaluatorContextBean.id', xtype: 'combo',
							url: 'systemBeanListFind.json?groupName=Rule Evaluator Context', detailPageClass: 'Clifton.system.bean.BeanWindow',
							qtip: 'System Bean that is used for Context during rule execution for rules in this category. The Context is usually used to cache common data used during rule executions and provides simple API for related data retrieval.'
						},
						{
							fieldLabel: 'Security Resource', displayField: 'labelExpanded', name: 'securityResource.labelExpanded', hiddenName: 'securityResource.id', xtype: 'combo',
							url: 'securityResourceListFind.json', queryParam: 'labelExpanded', detailPageClass: 'Clifton.security.authorization.ResourceWindow',
							qtip: 'When not set, the user must have write access to the Category Table (linkedTable); when set, the user must have write access to the selected Security Resource.  This security is used to control who can ignore violations within this category and who can modify/create Rule Assignments for this category.  An additional ignoreCondition can be set on individual Rule Definitions within the category to control the ability to ignore specific violations.'
						},
						{xtype: 'sectionheaderfield', header: 'Additional Scope'},
						{fieldLabel: 'Additional Scope Table', name: 'additionalScopeTable.name', hiddenName: 'additionalScopeTable.id', xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', detailPageClass: 'Clifton.system.schema.TableWindow', disableAddNewItem: true},
						{fieldLabel: 'Additional Scope Path', name: 'additionalScopeBeanFieldPath', qtip: 'The path to an entity belonging to the the additional scope table from an entity belonging to the category table. (e.g. clientInvestmentAccount for an entity belonging to the InvestmentAccount table, derived from an entity belonging to the PortfolioRun table)'},
						{
							fieldLabel: 'Additional Scope Label', name: 'additionalScopeLabel',
							qtip: 'Scope label to use in UI for representation.  Please use singular form.  i.e. for InvestmentAccount table the Scope label would be Client Account.'
						}
					]
				}]
			},


			{
				title: 'Rule Definitions',
				items: [{
					name: 'ruleDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'A list of business rules defined for this rule category.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Definition Name', width: 140, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Ignorable', width: 35, dataIndex: 'ignorable', type: 'boolean'},
						{header: 'Auto-ignore', width: 35, dataIndex: 'automaticIgnoreOnCreate', type: 'boolean'},
						{
							header: 'Pre-Process', width: 35, dataIndex: 'preProcess', type: 'boolean',
							qtip: 'Pre-Process warnings indicate the warning should be generated before any processing occurs.'
						},
						{header: 'Inactive', width: 30, dataIndex: 'inactive', type: 'boolean'},
						{header: 'Order', width: 30, dataIndex: 'order', type: 'int'}
					],
					getLoadParams: function(firstLoad) {
						return {ruleCategoryId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow'
					}
				}]
			},


			{
				title: 'Rule Scopes',
				items: [{
					name: 'ruleScopeListFind',
					xtype: 'gridpanel',
					instructions: 'A list of rule scopes that are allowed for entities of this rule category.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Scope Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'}
					],
					getLoadParams: function(firstLoad) {
						return {ruleCategoryId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.rule.definition.RuleScopeWindow'
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'RuleCategory'
				}]
			}
		]
	}]
});



