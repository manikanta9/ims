TCG.use('Clifton.rule.definition.BaseRuleAssignmentForm');

Clifton.rule.definition.GlobalAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Global Rule Assignment',
	iconCls: 'rule',
	width: 650,
	height: 450,
	defaultDataIsReal: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					// Nothing to Add for Global Right Now
					xtype: 'rule-assignment-form'
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'RuleAssignment'
				}]
			}
		]
	}]
});
