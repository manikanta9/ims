TCG.use('Clifton.rule.definition.BaseRuleAssignmentForm');

Clifton.rule.definition.AdditionalScopeEntityAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rule Assignment',
	iconCls: 'rule',
	width: 900,
	height: 570,
	defaultDataIsReal: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'rule-assignment-form',

					definitionItems: [
						{fieldLabel: 'Rule Category', name: 'ruleDefinition.ruleCategory.name', detailIdField: 'ruleDefinition.ruleCategory.id', xtype: 'linkfield', detailPageClass: 'Clifton.rule.definition.RuleCategoryWindow', submitValue: false, submitDetailField: false},
						{fieldLabel: 'Rule Definition', name: 'ruleDefinition.name', detailIdField: 'ruleDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow'},
						{name: 'ruleDefinition.description', xtype: 'textarea', readOnly: true}
					],

					assignmentSpecificColumns: [
						{xtype: 'sectionheaderfield', header: 'Scope'},
						{fieldLabel: '', linkName: 'additionalScopeLabelLink', name: 'additionalScopeLabel', detailIdField: 'additionalScopeFkFieldId', xtype: 'linkfield', submitValue: true, submitDetailField: true},
						{fieldLabel: '', linkName: 'entityLabelLink', name: 'entityLabel', detailIdField: 'entityFkFieldId', xtype: 'linkfield', submitValue: true, submitDetailField: true},
						{xtype: 'hidden', name: 'ruleDefinition.entityTable.name'},
						{xtype: 'sectionheaderfield', header: 'Details'}
					],

					resetAdditionalFields: function() {
						const table = TCG.getValue('ruleDefinition.ruleCategory.additionalScopeTable', this.getForm().formValues);
						//This is the label to be used for what the field represents (e.g. Client Account)
						const categoryScopeLabel = TCG.getValue('ruleDefinition.ruleCategory.additionalScopeLabel', this.getForm().formValues);
						//This is the label of the actual entity (e.g. the client account label)
						const additionalScopeLabel = TCG.getValue('additionalScopeLabel', this.getForm().formValues);
						const detailScreen = table.detailScreenClass;

						const entityTable = TCG.getValue('ruleDefinition.entityTable', this.getForm().formValues);
						const entityLabel = TCG.getValue('entityLabel', this.getForm().formValues);
						const entityDetailScreen = entityTable.detailScreenClass;

						const f = this.getForm();
						let fld = f.findField('additionalScopeLabelLink');
						fld.detailPageClass = detailScreen;
						fld.originalValue = additionalScopeLabel;
						fld.setFieldLabel(categoryScopeLabel + ':');
						fld.setValue(additionalScopeLabel);

						fld = f.findField('entityLabelLink');
						fld.detailPageClass = entityDetailScreen;
						fld.originalValue = entityLabel;
						fld.setFieldLabel(entityTable.label + ':');
						fld.setValue(entityLabel);
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'RuleAssignment'
				}]
			}
		]
	}]
});
