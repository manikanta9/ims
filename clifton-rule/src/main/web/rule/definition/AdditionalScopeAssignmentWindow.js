TCG.use('Clifton.rule.definition.BaseRuleAssignmentForm');

Clifton.rule.definition.AdditionalScopeAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rule Assignment',
	iconCls: 'rule',
	width: 900,
	height: 570,
	defaultDataIsReal: true,
	loadDefaultDataAfterRender: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'rule-assignment-form',
					assignmentSpecificColumns: [
						{xtype: 'sectionheaderfield', header: 'Scope'},
						{fieldLabel: '', linkName: 'additionalScopeLabelLink', name: 'additionalScopeLabel', detailIdField: 'additionalScopeFkFieldId', xtype: 'linkfield', submitValue: true, submitDetailField: true},
						{xtype: 'formfragment', frame: false, labelWidth: 170, items: [], name: 'entitySelectionFormFragment'},
						{xtype: 'sectionheaderfield', header: 'Details'}
					],

					definitionItems: [
						{fieldLabel: 'Rule Category', name: 'ruleDefinition.ruleCategory.name', detailIdField: 'ruleDefinition.ruleCategory.id', xtype: 'linkfield', detailPageClass: 'Clifton.rule.definition.RuleCategoryWindow', submitValue: false, submitDetailField: false},
						{fieldLabel: 'Rule Definition', name: 'ruleDefinition.name', detailIdField: 'ruleDefinition.id', xtype: 'linkfield', detailPageClass: 'Clifton.rule.definition.RuleDefinitionWindow'},
						{name: 'ruleDefinition.description', xtype: 'textarea', readOnly: true}
					],

					resetAdditionalFields: function() {
						const table = TCG.getValue('ruleDefinition.ruleCategory.additionalScopeTable', this.getForm().formValues);
						//This is the label to be used for what the field represents (e.g. Client Account)
						const categoryScopeLabel = TCG.getValue('ruleDefinition.ruleCategory.additionalScopeLabel', this.getForm().formValues);
						//This is the label of the actual entity (e.g. the client account label)
						const additionalScopeLabel = TCG.getValue('additionalScopeLabel', this.getForm().formValues);
						const detailScreen = table.detailScreenClass;

						const f = this.getForm();
						const fld = f.findField('additionalScopeLabelLink');
						fld.detailPageClass = detailScreen;
						fld.originalValue = additionalScopeLabel;
						fld.setFieldLabel(categoryScopeLabel + ':');
						fld.setValue(additionalScopeLabel);

						const ff = TCG.getChildByName(this, 'entitySelectionFormFragment');
						ff.removeAll(true);

						const entityValueListUrl = TCG.getValue('ruleDefinition.entityValueListUrl', this.getForm().formValues);
						if (entityValueListUrl && TCG.isNotBlank(entityValueListUrl)) {
							const entityTable = TCG.getValue('ruleDefinition.entityTable', this.getForm().formValues);
							const entityDetailScreen = entityTable.detailScreenClass;

							const fieldConfig = {
								name: 'entityLabel',
								hiddenName: 'entityFkFieldId',
								fieldLabel: entityTable.label,
								detailPageClass: entityDetailScreen,
								url: entityValueListUrl,
								submitValue: true,
								xtype: 'combo'
							};
							// apply custom configuration if any
							const entityValueUserInterfaceConfig = TCG.getValue('ruleDefinition.entityValueUserInterfaceConfig', this.getForm().formValues);
							if (TCG.isNotNull(entityValueUserInterfaceConfig)) {
								Ext.apply(fieldConfig, Ext.decode(entityValueUserInterfaceConfig));
							}
							const field = new TCG.form.ComboBox(fieldConfig);
							ff.add(field);
							// Value doesn't seem to update automatically based on field names - maybe because it's added after?
							field.setValue({value: TCG.getValue('entityFkFieldId', this.getForm().formValues), text: TCG.getValue('entityLabel', this.getForm().formValues)});
						}
						ff.doLayout();
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'RuleAssignment'
				}]
			}
		]
	}]
});



