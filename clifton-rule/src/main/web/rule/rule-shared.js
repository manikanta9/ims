Ext.ns('Clifton.rule', 'Clifton.rule.setup', 'Clifton.rule.definition', 'Clifton.rule.violation');


Clifton.rule.violation.StatusOptions = [['UNPROCESSED', 'UNPROCESSED'], ['PROCESSED_FAILED', 'PROCESSED_FAILED'], ['PROCESSED_SUCCESS', 'PROCESSED_SUCCESS'], ['PROCESSED_IGNORED', 'PROCESSED_IGNORED'], ['PROCESSED_VIOLATIONS', 'PROCESSED_VIOLATIONS']];


Clifton.rule.violation.renderViolationStatus = function(v) {
	if (v === 'PROCESSED_FAILED' || v === 'UNPROCESSED') {
		return '<div class="amountNegative">' + v + '</div>';
	}
	if (v === 'PROCESSED_SUCCESS') {
		return '<div class="amountPositive">' + v + '</div>';
	}
	if (v === 'PROCESSED_VIOLATIONS') {
		return '<div class="amountAdjusted">' + v + '</div>';
	}
	return v;
};

Clifton.rule.violation.getRuleViolationWarningMessage = function(type, formValues, showIgnored) {
	// Used to generate the warning message on the detail window
	let msg = undefined;
	if (formValues && formValues.violationStatus) {
		const status = formValues.violationStatus.name;
		if (status === 'UNPROCESSED') {
			msg = 'This ' + type + ' record has not been processed yet.';
		}
		else if (status === 'PROCESSED_VIOLATIONS') {
			msg = 'This ' + type + ' record has violation(s) that have not been addressed yet. Please see Violations tab for more information.';
		}
		else if (status === 'PROCESSED_IGNORED' && showIgnored) {
			msg = 'This ' + type + ' record was successfully processed but has violation(s) that have been ignored. Please see Violations tab for more information.';
		}
		else if (status === 'PROCESSED_FAILED') {
			msg = 'This ' + type + ' record has failed processing. Please see Violations tab for more information.';
		}
	}
	return msg;
};


Clifton.rule.setup.getRuleCategory = function(categoryName, componentScope) {
	return TCG.data.getData('ruleCategoryByName.json?name=' + categoryName, componentScope, 'RULE_CATEGORY_' + categoryName);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

Clifton.rule.violation.BaseViolationGridPanel = Ext.extend(TCG.grid.EditorGridPanel, {
	xtype: 'editorgrid',
	name: 'ruleViolationListFind',
	instructions: 'The following rule violations have been recorded for selected filters. Defaults to today.',
	wikiPage: 'IT/Rule+Engine',
	groupField: 'ruleAssignment.ruleDefinition.name',
	additionalPropertiesToRequest: 'ruleAssignment.ruleDefinition.priority.cssStyle|ruleAssignment.ruleDefinition.id',
	topToolbarSearchParameter: 'searchPattern',
	staticLoadParams: {}, // optional additional restrictions
	remoteSort: true,
	useBufferView: false, // allow cell wrapping when grouping is disabled
	pageSize: 500,
	showCreateDate: true,
	showViolationCode: false, // Can be enabled for entities that use violation codes

	getStartDate: function() {
		return new Date().format('m/d/Y');
	},
	updateRecord: function(store, record, index) {
		//PREVENT FROM ATTEMPTING TO SAVE RECORD WHEN IGNORE NOTE IS SELECTED FROM COMBOBOX
	},
	hiddenColumns: ['ruleAssignment.ruleDefinition.ruleCategory.categoryTable.detailScreenClass'],
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Rule Definition', width: 50, dataIndex: 'ruleAssignment.ruleDefinition.name', filter: {type: 'combo', url: 'ruleDefinitionListFind.json', searchFieldName: 'ruleDefinitionId'}, hidden: true},
		{header: 'CauseFKFieldID', width: 15, dataIndex: 'causeFkFieldId', hidden: true},
		{header: 'LinkedFKFieldID', width: 15, dataIndex: 'linkedFkFieldId', hidden: true},
		{header: 'EntityFKFieldID', width: 15, dataIndex: 'entityFkFieldId', hidden: true},
		{header: 'Ignorable', width: 50, dataIndex: 'ruleAssignment.ruleDefinition.ignorable', type: 'boolean', hidden: true},
		{header: 'Linked Table', width: 75, dataIndex: 'ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name', hidden: true, filter: {type: 'combo', url: 'systemTableListFind.json?defaultDataSource=true', searchFieldName: 'linkedTableId'}},
		{header: 'Additional Scope Label', width: 75, dataIndex: 'ruleAssignment.additionalScopeLabel', hidden: true},
		{header: 'Entity Label', width: 75, dataIndex: 'ruleAssignment.entityLabel', hidden: true},
		{
			header: 'Violation Message', width: 300, dataIndex: 'violationNote',
			renderer: function(v, c, r) {
				if (TCG.isTrue(r.data['ruleAssignment.snoozed'])) {
					const endDate = r.data['ruleAssignment.endDate'];
					if (!TCG.isBlank(endDate)) {
						c.attr = TCG.renderQtip('Snoozed Until: ' + endDate.format('m/d/Y'));
					}
				}

				if (TCG.isTrue(r.data['ruleAssignment.ruleDefinition.ignorable']) && TCG.isTrue(r.data['ruleAssignment.ruleDefinition.manual'])) {
					return TCG.renderText(v, '', 'amountAdjusted');
				}
				return TCG.renderText(v);
			}
		},
		{header: 'Start Date', width: 25, dataIndex: 'ruleAssignment.startDate', hidden: true},
		{header: 'End Date', width: 25, dataIndex: 'ruleAssignment.endDate', hidden: true},
		{header: 'Snoozed', width: 75, dataIndex: 'ruleAssignment.snoozed', type: 'boolean', hidden: true},
		{header: 'Max Snooze Days', width: 25, dataIndex: 'ruleAssignment.ruleDefinition.maxSnoozeDays', type: 'int', hidden: true},
		{header: 'Manual', width: 25, dataIndex: 'ruleAssignment.ruleDefinition.manual', type: 'boolean', hidden: true},
		{header: 'Cancelled', width: 30, dataIndex: 'cancelledLinkedEntity', type: 'boolean', hidden: true, tooltip: 'If the Linked Entity is cancelled in its life cycle (Cancelled Trade), this Rule Violation will also be marked as Cancelled. It may still be useful for research but does not need to be acted upon.'},
		{header: 'Violation Code', width: 50, dataIndex: 'ruleViolationCode', tooltip: 'For each Rule Definition, we may have a Rule Code (Optional) with a 1 or 2 digit character that represents that rule'},
		{
			header: 'Ignored', width: 35, dataIndex: 'ignored', type: 'boolean',
			renderer: function(v, c, r) {
				if (TCG.isTrue(r.data['ruleAssignment.snoozed'])) {
					return '<img src="core/images/icons/snooze.png" title="Snoozed Until: ' + (r.data['ruleAssignment.endDate'] ? r.data['ruleAssignment.endDate'].format('m/d/Y') : 'No End Date') + '" />';
				}
				return TCG.renderBoolean(v);
			}
		},
		{
			header: 'Ignore Note',
			editor: {
				xtype: 'combo', url: 'ruleViolationIgnoreNoteListFind.json', fieldLabel: 'Ignore Note', displayField: 'ignoreNote', tooltipField: 'ignoreNote',
				allowBlank: true, forceSelection: false, doNotAddContextMenu: true,
				beforequery: function(queryEvent) {
					const rowIndex = queryEvent.combo.gridEditor.row;
					const row = queryEvent.combo.gridEditor.containerGrid.grid.store.data.items[rowIndex];
					queryEvent.combo.store.baseParams = {
						limit: 50,
						linkedTableName: row.json.ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name,
						linkedFkFieldId: row.json.linkedFkFieldId,
						requestedMaxDepth: 4,
						requestedProperties: 'id|description|ignoreNote',
						requestedPropertiesRoot: 'data',
						ruleDefinitionIds: row.json.ruleAssignment.ruleDefinition.id
					};
					//Clear the query so the data is always retrieved fresh
					//otherwise every combo in the grid will show the same data
					//from the first combo loaded.
					queryEvent.combo.lastQuery = null;
				}
			},
			width: 85,
			dataIndex: 'ignoreNote',
			renderer: function(v, c, r) {
				let returnVal = v;
				let style = r.json.ruleAssignment.ruleDefinition.priority.cssStyle;
				if (TCG.isFalse(r.data['ruleAssignment.ruleDefinition.ignorable'])) {
					returnVal = 'Not Ignorable';
				}
				else if (TCG.isTrue(r.data['ruleAssignment.ruleDefinition.ignoreNoteRequired']) && TCG.isBlank(r.data['ignoreNote'])) {
					returnVal = '*IGNORE NOTE REQUIRED*';
				}
				if (TCG.isTrue(r.data['ruleAssignment.snoozed'])) {
					if (r.data['ruleAssignment.startDate'] && r.data['ruleAssignment.startDate'].format('m/d/Y') === this.gridPanel.getStartDate()) {
						if (TCG.isNotBlank(style)) {
							style += '; COLOR: #CC3333 !important;';
						}
						else {
							style = 'COLOR: #CC3333 !important;';
						}
						returnVal = 'SNOOZED TODAY:<br />' + v;
					}
				}
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return returnVal;
			}
		},
		{header: 'Ignore Note Required', width: 25, dataIndex: 'ruleAssignment.ruleDefinition.ignoreNoteRequired', type: 'boolean', hidden: true},
		{
			header: 'Priority', width: 25, dataIndex: 'ruleAssignment.ruleDefinition.priority.name', sortField: 'ruleAssignment.ruleDefinition.priority.order',
			filter: {searchFieldName: 'priorityId', type: 'combo', displayField: 'name', url: 'systemPriorityListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.ruleAssignment.ruleDefinition.priority.cssStyle;
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{
			header: 'Cause', width: 35, dataIndex: 'violationCauseTable.detailScreenClass', align: 'center', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.causeFkFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View entity that caused this violation', 'cause');
				}
				return 'N/A';
			}
		},
		{
			header: 'Link', width: 35, dataIndex: 'ruleAssignment.ruleDefinition.ruleCategory.categoryTable.detailScreenClass', align: 'center', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.data.linkedFkFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View entity that is linked to this violation', 'link');
				}
				return 'N/A';
			}
		},
		{header: 'Created On', width: 55, dataIndex: 'createDate'}
	],

	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		const cm = this.getColumnModel();
		cm.setHidden(cm.findColumnIndex('createDate'), TCG.isFalse(this.showCreateDate));
		cm.setHidden(cm.findColumnIndex('ruleViolationCode'), TCG.isFalse(this.showViolationCode));

		if (this.hiddenColumns) {
			for (let i = 0; i < this.hiddenColumns.length; i++) {
				const name = this.hiddenColumns[i];
				const colIndex = cm.findColumnIndex(name);
				if (colIndex > 0) {
					cm.setHidden(colIndex, true);
				}
			}
		}
	},

	includeRuleCategoryFilter: true,
	includeRuleDefinitionFilter: true,
	includeDisplayFilter: false,
	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		if (this.includeRuleCategoryFilter) {
			filters.push({fieldLabel: 'Rule Category', xtype: 'toolbar-combo', name: 'categoryId', width: 150, url: 'ruleCategoryListFind.json'});
		}
		if (this.includeRuleDefinitionFilter) {
			filters.push({
				fieldLabel: 'Rule Definition', xtype: 'combo', name: 'ruleDefinitionId', width: 150, url: 'ruleDefinitionListFind.json', linkedFilter: 'ruleAssignment.ruleDefinition.name',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.baseParams = {
							categoryId: TCG.getChildByName(toolbar, 'categoryId').getValue()
						};
						const s = TCG.getChildByName(toolbar, 'ruleDefinitionId');
						s.store.removeAll();
						s.lastQuery = null;
						s.store.baseParams = {ruleCategoryId: TCG.getChildByName(toolbar, 'categoryId').getValue()};
					}
				}
			});
		}
		if (this.includeDisplayFilter) {
			filters.push({
				fieldLabel: 'Display', xtype: 'combo', name: 'displayFilters', width: 100, minListWidth: 100, emptyText: 'All', forceSelection: true, mode: 'local', displayField: 'name', valueField: 'value',
				store: new Ext.data.ArrayStore({
					fields: ['value', 'name', 'description'],
					data: [
						['ALL', 'All', 'Display all rule violations regardless of whether they were Ignored or Cancelled'],
						['OUTSTANDING', 'Outstanding', 'Display rule violations that have not been Ignored or Cancelled'],
						['NOT_CANCELLED', 'Not Cancelled', 'Display rule violations that have not been Cancelled'],
						['NOT_IGNORED', 'Not Ignored', 'Display rule violations that have not been Ignored']
					]
				}),
				listeners: {
					select: function(field) {
						const gp = TCG.getParentByClass(field, Ext.Panel);
						const v = field.getValue();
						if (v === 'OUTSTANDING') {
							gp.setFilterValue('cancelledLinkedEntity', false);
							gp.setFilterValue('ignored', false);
						}
						else if (v === 'NOT_CANCELLED') {
							gp.setFilterValue('cancelledLinkedEntity', false);
							gp.clearFilter('ignored', true);
						}
						else if (v === 'NOT_IGNORED') {
							gp.clearFilter('cancelledLinkedEntity', true);
							gp.setFilterValue('ignored', false);
						}
						else { // same as ALL
							gp.clearFilter('cancelledLinkedEntity', true);
							gp.clearFilter('ignored', true);
						}
						gp.reload();
					}
				}
			});
		}
		filters.push({fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'});
		return filters;
	},
	defaultCreateDateDaysBack: 7,
	getTopToolbarInitialLoadParams: function(firstLoad) {
		if (firstLoad) {
			const dd = this.getWindow().defaultData;
			if (dd && dd.displayFilter) {
				const t = this.getTopToolbar();
				const displayFilters = TCG.getChildByName(t, 'displayFilters');
				displayFilters.setValue(dd.displayFilter);
				displayFilters.fireEvent('select', displayFilters);
			}
			if (dd && dd.createDate) { // createDate can be defaulted by the caller of window open
				this.setFilterValue('createDate', {'on': dd.createDate});
			}
			else {
				this.setFilterValue('createDate', {'after': new Date().add(Date.DAY, -this.defaultCreateDateDaysBack)});
			}
		}
		const params = Ext.apply({}, this.staticLoadParams);
		const t = this.getTopToolbar();
		const cat = TCG.getChildByName(t, 'categoryId');
		if (cat && TCG.isNotBlank(cat.getValue())) {
			params.categoryId = cat.getValue();
		}
		return params;
	},

	editor: {
		detailPageClass: 'Clifton.rule.violation.ViolationWindow',
		drillDownOnly: true
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					let clz = row.json.ruleAssignment.ruleDefinition.ruleCategory.categoryTable.detailScreenClass;
					let id = row.json.linkedFkFieldId;
					const eventName = TCG.getActionColumnEventName(evt);
					if (eventName === 'cause') {
						clz = row.json.violationCauseTable.detailScreenClass;
						id = row.json.causeFkFieldId;
					}
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
				}
			}
		}
	},
	getRowSelectionModel: function() {
		this.rowSelectionModel = new Ext.grid.RowSelectionModel({singleSelect: false});
		return this.rowSelectionModel;
	}
});
Ext.reg('base-rule-violation-grid', Clifton.rule.violation.BaseViolationGridPanel);


Clifton.rule.violation.ViolationGridPanel = Ext.extend(Clifton.rule.violation.BaseViolationGridPanel, {
	tableName: 'REQUIRED-TABLE-NAME',
	showSnoozed: false,
	requireIgnoreNote: false,
	instructions: 'The following rule violations have been recorded for selected entity.',
	rowSelectionModel: 'multiple',
	previewAllowed: true,
	manualAllowed: false, // if true adds add/remove buttons available for manual warning types only
	hiddenColumns: ['ruleAssignment.ruleDefinition.ruleCategory.categoryTable.detailScreenClass'],
	showCreateDate: false,

	onRender: function(gridPanel, position) {
		const tabPanel = TCG.getParentTabPanel(this);
		tabPanel.getActiveTab().reloadOnTabChange = true;
		this.on('afterrender', function(gridPanel) {
			const el = gridPanel.getEl();
			el.on('contextmenu', function(e, target) {
				e.preventDefault();
				const grid = gridPanel.grid;
				//get the index of the row that was right-clicked and pass to the ignore action.
				const contextMenuRowIndex = grid.view.findRowIndex(target);
				if (TCG.isNotNull(contextMenuRowIndex) && contextMenuRowIndex !== false) {
					grid.drillDownMenu = new Ext.menu.Menu({
						items: gridPanel.getContextMenu(true, contextMenuRowIndex)
					});
					TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(gridPanel, grid.drillDownMenu, contextMenuRowIndex, grid.view.findCellIndex(target), e);
				}
			}, gridPanel);

			gridPanel.grid.store.on('load', function(store, records) {
				gridPanel.updateSnoozed();
			});

		});

		Clifton.rule.violation.ViolationGridPanel.superclass.onRender.apply(this, arguments);
	},
	updateSnoozed: function() {
		const gridPanel = this;
		const loader = new TCG.data.JsonLoader({
			waitTarget: gridPanel,
			params: {snoozed: true, linkedTableName: this.tableName, linkedFkFieldId: this.getEntityId()},
			onLoad: function(record, conf) {
				const toolbar = gridPanel.getTopToolbar();
				const snoozedLabel = TCG.getChildByName(toolbar, 'snoozedLabel');
				let snoozeStyle = 'font-weight: bold';
				if (record.length > 0) {
					snoozeStyle += '; color:red;';
				}
				snoozedLabel.update('Show Snoozed <span style=\'' + snoozeStyle + '\'>(' + record.length + ')</span>&nbsp;');
			}
		});
		loader.load('ruleViolationListFind.json');
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{html: 'Show Snoozed&nbsp;', xtype: 'label', name: 'snoozedLabel'},
			{width: 150, xtype: 'toolbar-checkbox', name: 'snoozed', value: this.showSnoozed},
			{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
		];
	},
	getTopToolbarInitialLoadParams: function() {
		const snoozed = TCG.getChildByName(this.getTopToolbar(), 'snoozed');
		const params = {
			snoozed: snoozed.checked,
			snoozedStartDate: this.getStartDate(),
			linkedTableName: this.tableName,
			linkedFkFieldId: this.getEntityId()
		};
		if (!snoozed.checked) {
			params.snoozedTodayOrNotSnoozed = true;
		}
		return params;
	},
	getEntityId: function() {
		return this.getWindow().getMainFormId();
	},
	prepareIgnoreViolations: function(ignoreType, contextMenuRowIndex) {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const selectionModel = grid.getSelectionModel();
		const gridRows = grid.getStore().getRange();
		//If an index from a right-click was passed, then we use that as our selection
		if (TCG.isNotNull(contextMenuRowIndex)) {
			Ext.each(gridRows, function(row, rowIndex) {
				selectionModel.deselectRow(rowIndex);
			});
			selectionModel.selectRow(contextMenuRowIndex, true);
		}
		if (ignoreType === 'ignoreAndSaveNotes') {
			//Here we want to iterate the grid and select all rows that have been modified then we call the normal
			// ignoreViolations so it will ignore them one by one with their modified ignore notes.
			Ext.each(gridRows, function(row, rowIndex) {
				if (TCG.isTrue(row.dirty)) {
					grid.getSelectionModel().selectRow(rowIndex, true);
				}
			});
			gridPanel.ignoreViolations();
		}
		else {
			if (selectionModel.getCount() === 0) {
				if (ignoreType === 'ignoreByDefinition') {
					TCG.showError('Please select a definition to ignore by selecting a violation under that definition.', 'No Row(s) Selected');
				}
				else {
					TCG.showError('Please select a violation to mark as ignored.', 'No Row(s) Selected');
				}
			}
			else {
				const ignoreNoteMenu = TCG.getChildByName(gridPanel.getTopToolbar(), 'ignoreMenu', true, true);
				const ignoreNoteRequiredMenuItem = TCG.getChildByName(ignoreNoteMenu.menu, 'ignoreNoteRequired', false, true);
				if (ignoreNoteRequiredMenuItem && ignoreNoteRequiredMenuItem.checked) {
					if (this.isDirty()) {
						Ext.Msg.confirm('Ignore Unsaved Changes', 'You have unsaved changes on the grid, are you sure you wish to continue?  Your changes will be lost.', function(a) {
							if (a === 'yes') {
								if (ignoreType === 'ignoreByDefinition') {
									gridPanel.ignoreByRuleDefinition();
								}
								else {
									gridPanel.ignoreViolationsWithNote();
								}
							}
						});
					}
					else {
						if (ignoreType === 'ignoreByDefinition') {
							gridPanel.ignoreByRuleDefinition();
						}
						else {
							gridPanel.ignoreViolationsWithNote();
						}
					}
				}
				else {
					if (ignoreType === 'ignoreByDefinition') {
						//Here we want to iterate the grid and select all items with the same rule definition id(s)
						// of the originally selected row then we call the normal ignoreViolations so it will save
						// them one by one with their existing ignore notes.
						const selections = grid.getSelectionModel().getSelections();
						const ruleDefinitionIds = selections.map(function(obj) {
							return obj.json.ruleAssignment.ruleDefinition.id;
						});
						Ext.each(gridRows, function(row, rowIndex) {
							Ext.each(ruleDefinitionIds, function(ruleDefinitionId, definitionIndex) {
								if (row.json.ruleAssignment.ruleDefinition.id === ruleDefinitionId) {
									grid.getSelectionModel().selectRow(rowIndex, true);
								}
							});
						});
						gridPanel.ignoreViolations();
					}
					else {
						gridPanel.ignoreViolations();
					}
				}
			}
		}
	},
	ignoreByRuleDefinition: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const params = gridPanel.getLoadParams();

		const selectionModel = grid.getSelectionModel();
		const selections = selectionModel.getSelections();
		if (selections) {
			params['ruleDefinitionIds'] = selections.map(function(obj) {
				return obj.json.ruleAssignment.ruleDefinition.id;
			});
			TCG.createComponent('TCG.app.OKCancelWindow', {
				title: 'Rule Definition Violations - Ignore Note',
				iconCls: 'rule',
				height: 250,
				width: 700,
				modal: true,
				okButtonText: 'Ignore',
				okButtonTooltip: 'Apply this ignore note to all violations for selected definition(s).',
				openerCt: gridPanel,
				items: [{
					xtype: 'formpanel',
					instructions: 'Enter new or select recent Ignore Note for all violations for selected definition(s). Existing notes will be overwritten.',
					labelWidth: 140,
					items: [
						{xtype: 'label', html: '<br />'},
						{fieldLabel: 'Ignore Note', name: 'ignoreNote', xtype: 'textarea'},
						{
							fieldLabel: 'Recent Ignore Notes', name: 'recentIgnoreNote', displayField: 'ignoreNote',
							xtype: 'combo', url: 'ruleViolationIgnoreNoteListFind.json', tooltipField: 'ignoreNote',
							qtip: 'Used to copy an existing Ignore Note for this entity and rule definition from last 7 calendar days. The note can be updated before saving.',
							allowBlank: true, forceSelection: false, doNotAddContextMenu: true,
							listeners: {
								beforequery: function(queryEvent) {
									queryEvent.combo.store.baseParams = {
										limit: 50,
										linkedTableName: gridPanel.tableName,
										linkedFkFieldId: gridPanel.getEntityId(),
										requestedMaxDepth: 4,
										requestedProperties: 'id|description|ignoreNote',
										requestedPropertiesRoot: 'data',
										ruleDefinitionIds: params['ruleDefinitionIds']
									};
								},
								select: function(combo, record, index) {
									const fp = combo.getParentForm();
									fp.setFormValue('ignoreNote', record.json.ignoreNote);
								}
							}
						}
					]
				}],
				saveWindow: function(closeOnSuccess, forceSubmit) {
					const panel = this.getMainFormPanel();
					const invalidField = panel.getFirstInValidField();
					if (TCG.isNotNull(invalidField)) {
						invalidField.focus(!!invalidField.el.dom.select);
						TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
					}
					else {
						const ignoreCommandList = [{
							class: 'com.clifton.rule.violation.ignore.RuleViolationIgnoreCommand',
							ignoreNote: panel.getFormValue('ignoreNote'),
							linkedTableName: gridPanel.tableName,
							linkedFkFieldId: gridPanel.getEntityId(),
							ruleDefinitionIds: params['ruleDefinitionIds']
						}];

						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Ignoring...',
							params: {beanList: Ext.util.JSON.encode(ignoreCommandList)},
							onLoad: function(record, conf) {
								gridPanel.afterIgnoreSuccessAction(null);
								panel.getWindow().closeWindow();
							}
						});
						loader.load('ruleViolationListIgnore.json');
					}
				}
			});
		}
	},
	ignoreViolationsWithNote: function() {
		const gridPanel = this;
		let ruleDefinitionIds = [];
		const selections = gridPanel.grid.getSelectionModel().getSelections();
		if (selections) {
			ruleDefinitionIds = selections.map(function(obj) {
				return obj.json.ruleAssignment.ruleDefinition.id;
			});
		}

		TCG.createComponent('TCG.app.OKCancelWindow', {
			title: 'Rule Violations - Ignore Note',
			iconCls: 'rule',
			height: 250,
			width: 700,
			modal: true,
			okButtonText: 'Ignore',
			okButtonTooltip: 'Apply this ignore note to all selected violations.',
			openerCt: gridPanel,
			items: [{
				xtype: 'formpanel',
				instructions: 'Enter new or select recent Ignore Note for selected violations. Existing notes will be overwritten.',
				labelWidth: 140,
				items: [
					{xtype: 'label', html: '<br />'},
					{fieldLabel: 'Ignore Note', name: 'ignoreNote', xtype: 'textarea'},
					{
						fieldLabel: 'Recent Ignore Notes', name: 'recentIgnoreNote', displayField: 'ignoreNote',
						xtype: 'combo', url: 'ruleViolationIgnoreNoteListFind.json', tooltipField: 'ignoreNote',
						qtip: 'Used to copy an existing Ignore Note for this entity and rule definition from last 7 calendar days. The note can be updated before saving.',
						allowBlank: true, forceSelection: false, doNotAddContextMenu: true,
						listeners: {
							beforequery: function(queryEvent) {
								queryEvent.combo.store.baseParams = {
									limit: 50,
									linkedTableName: gridPanel.tableName,
									linkedFkFieldId: gridPanel.getEntityId(),
									requestedMaxDepth: 4,
									requestedProperties: 'id|description|ignoreNote',
									requestedPropertiesRoot: 'data',
									ruleDefinitionIds: ruleDefinitionIds
								};
							},
							select: function(combo, record, index) {
								const fp = combo.getParentForm();
								fp.setFormValue('ignoreNote', record.json.ignoreNote);
							}
						}
					}
				]
			}],
			saveWindow: function(closeOnSuccess, forceSubmit) {
				const panel = this.getMainFormPanel();
				const invalidField = panel.getFirstInValidField();
				if (TCG.isNotNull(invalidField)) {
					invalidField.focus(!!invalidField.el.dom.select);
					TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
				}
				else {
					gridPanel.ignoreViolations(panel.getFormValue('ignoreNote'));
					panel.getWindow().closeWindow();
				}
			}
		});
	},
	ignoreViolations: function(ignoreNote, includeIgnoreRelatedByField) {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const selectionModel = grid.getSelectionModel();
		const selections = selectionModel.getSelections();

		const ignoreCommandList = [];
		Ext.each(selections, function(row, rowIndex) {
			ignoreCommandList.push(gridPanel.getIgnoreParams(row, ignoreNote, includeIgnoreRelatedByField));
		});

		const onLoadFunction = function(record, conf) {
			gridPanel.afterIgnoreSuccessAction(includeIgnoreRelatedByField);
		};

		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Ignoring...',
			params: {beanList: Ext.util.JSON.encode(ignoreCommandList)},
			onLoad: onLoadFunction
		});
		loader.load('ruleViolationIgnoreByCommand.json');
	},
	getIgnoreParams: function(selection, ignoreNote, includeIgnoreRelatedByField) {
		const params = {
			class: 'com.clifton.rule.violation.ignore.RuleViolationIgnoreCommand',
			id: selection.data['id'],
			linkedTableName: selection.data['ruleAssignment.ruleDefinition.ruleCategory.categoryTable.name'],
			linkedFkFieldId: selection.data['linkedFkFieldId']
		};
		if (ignoreNote) {
			params.ignoreNote = ignoreNote;
		}
		else {
			params.ignoreNote = selection.data['ignoreNote'];
		}
		if (includeIgnoreRelatedByField) {
			params.includeIgnoreRelatedByField = includeIgnoreRelatedByField;
		}
		return params;
	},
	getContextMenu: function(isContextMenu, contextMenuRowIndex) {
		const contextMenuItems = this.getIgnoreMenu(isContextMenu, contextMenuRowIndex);
		const row = this.grid.store.data.items[contextMenuRowIndex];
		if (row.json.ruleAssignment.ruleDefinition.maxSnoozeDays > 0) {
			contextMenuItems.push('-');
			contextMenuItems.push(this.getSnoozeMenu(isContextMenu, contextMenuRowIndex));
		}
		return contextMenuItems;
	},
	getIgnoreMenu: function(isContextMenu, contextMenuRowIndex) {
		const gridPanel = this;
		//Group additional Ignore buttons under the main Ignore button
		let ignoreLabel = 'Ignore Selected';
		if (isContextMenu) {
			ignoreLabel = 'Ignore Violation';
		}
		let ignoreMenuItems = [
			{
				text: ignoreLabel,
				tooltip: 'Mark selected violation as ignored.',
				iconCls: 'cancel',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareIgnoreViolations('ignoreSelected', contextMenuRowIndex);
				}
			},
			{
				text: 'Ignore All For Definition',
				tooltip: 'Ignore all violations under the selected Rule Definition.',
				iconCls: 'cancel',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareIgnoreViolations('ignoreByDefinition', contextMenuRowIndex);
				}
			},
			{
				text: 'Ignore All',
				tooltip: 'Mark all violations as ignored.',
				iconCls: 'cancel',
				scope: gridPanel,
				handler: function() {
					gridPanel.grid.getSelectionModel().selectAll();
					gridPanel.prepareIgnoreViolations('ignoreAll');
				}
			}, '-',
			{
				text: 'Ignore and Save Notes',
				tooltip: 'Ignore and save all violations with notes that have been modified.',
				iconCls: 'disk',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareIgnoreViolations('ignoreAndSaveNotes');
				}
			}
		];

		if (gridPanel.addCustomIgnoreButtons) {
			ignoreMenuItems = ignoreMenuItems.concat(gridPanel.addCustomIgnoreButtons());
		}
		return ignoreMenuItems;
	},
	getToolbarIgnoreMenu: function() {
		const gridPanel = this;
		const toolbarMenu = gridPanel.getIgnoreMenu();
		//Keep this as a menu item, but hide and disable it; we will remove entirely once they force notes on all violations.
		//Trading doesn't want to allow for them to uncheck, but wanted to hide for now, rather than changing the functionality
		//this close to release.
		toolbarMenu.push({
			name: 'ignoreNoteRequired',
			text: 'Require Ignore Note',
			tooltip: 'Prompt for an ignore note that will be applied to all violations being ignored.',
			xtype: 'menucheckitem',
			checked: this.requireIgnoreNote,
			hideOnClick: false,
			disabled: true,
			style: 'display: none'
		});
		return toolbarMenu;
	},
	getToolbarSnoozeMenu: function() {
		const gridPanel = this;
		//Allows for manipulation of toolbar snooze menu (e.g. new options?)
		return gridPanel.getSnoozeMenu();
	},
	prepareSnoozeViolations: function(snoozeType, numberOfDays, contextMenuRowIndex, isUnsnooze) {
		let hasError = false;
		const gridPanel = this;
		const grid = gridPanel.grid;
		const selectionModel = grid.getSelectionModel();
		const gridRows = grid.getStore().getRange();

		//If an index from a right-click was passed, then we use that as our selection
		if (TCG.isNotNull(contextMenuRowIndex)) {
			Ext.each(gridRows, function(row, rowIndex) {
				selectionModel.deselectRow(rowIndex);
			});
			selectionModel.selectRow(contextMenuRowIndex, true);
		}

		if (selectionModel.getCount() === 0) {
			hasError = true;
			TCG.showError('Please select a violation to snooze.', 'No Row(s) Selected');
		}
		else if (snoozeType === 'single') {
			if (selectionModel.getCount() > 1) {
				hasError = true;
				TCG.showError('Please select only one row, or snooze using a different option.', 'Multiple Row(s) Selected');
			}
		}
		else if (snoozeType === 'definition') {
			const selections = grid.getSelectionModel().getSelections();
			const ruleDefinitionIds = selections.map(function(obj) {
				return obj.json.ruleAssignment.ruleDefinition.id;
			});
			Ext.each(gridRows, function(row, rowIndex) {
				Ext.each(ruleDefinitionIds, function(ruleDefinitionId, definitionIndex) {
					if (row.json.ruleAssignment.ruleDefinition.id === ruleDefinitionId) {
						grid.getSelectionModel().selectRow(rowIndex, true);
					}
				});
			});
		}

		if (!hasError) {
			if (isUnsnooze) {
				gridPanel.unsnoozeViolations();
			}
			else {
				TCG.createComponent('TCG.app.OKCancelWindow', {
					title: 'Number of Days to Snooze',
					iconCls: 'snooze',
					height: 250,
					width: 600,
					modal: true,
					okButtonText: 'Snooze',
					okButtonTooltip: 'Snooze this violation for the specified number of days.',
					openerCt: gridPanel,
					items: [{
						xtype: 'formpanel',
						instructions: 'Enter the number of days to snooze this violation for:',
						labelWidth: 125,
						items: [
							{fieldLabel: 'Number of Days', name: 'numberOfDays', xtype: 'integerfield', value: numberOfDays, allowBlank: false},
							{fieldLabel: 'Snooze Note', name: 'snoozeNote', xtype: 'textarea', allowBlank: false}
						]
					}],
					saveWindow: function(closeOnSuccess, forceSubmit) {
						const panel = this.getMainFormPanel();
						const invalidField = panel.getFirstInValidField();
						if (TCG.isNotNull(invalidField)) {
							invalidField.focus(!!invalidField.el.dom.select);
							TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
						}
						else {
							const numberOfDays = parseInt(panel.getFormValue('numberOfDays'));
							const snoozeNote = panel.getFormValue('snoozeNote');
							if (numberOfDays > 0) {
								gridPanel.snoozeViolations(numberOfDays, snoozeNote);
								panel.getWindow().closeWindow();
							}
							else {
								TCG.showError('The number of days to snooze must be greater than 0.', 'Invalid Number of Days');
							}
						}
					}
				});
			}
		}
	},
	snoozeViolations: function(numberOfDays, snoozeNote) {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const selectionModel = grid.getSelectionModel();
		const selections = selectionModel.getSelections();

		const onLoadFunction = function(record, conf) {
			gridPanel.afterSnoozeSuccessAction(record);
		};

		const snoozeCommandList = [];
		Ext.each(selections, function(row, rowIndex) {
			snoozeCommandList.push(gridPanel.getSnoozeParams(row, numberOfDays, snoozeNote));
		});

		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Snoozing...',
			params: {beanList: Ext.util.JSON.encode(snoozeCommandList)},
			onLoad: onLoadFunction
		});
		loader.load('ruleViolationSnoozeByCommand.json');
	},
	unsnoozeViolations: function() {
		const gridPanel = this;
		const grid = gridPanel.grid;
		const selectionModel = grid.getSelectionModel();
		const selections = selectionModel.getSelections();

		const onLoadFunction = function(record, conf) {
			gridPanel.afterSnoozeSuccessAction(record);
		};

		const snoozeCommandList = [];
		Ext.each(selections, function(row, rowIndex) {
			snoozeCommandList.push(gridPanel.getSnoozeParams(row));
		});

		const loader = new TCG.data.JsonLoader({
			waitTarget: grid.ownerCt,
			waitMsg: 'Unsnoozing...',
			params: {beanList: Ext.util.JSON.encode(snoozeCommandList)},
			onLoad: onLoadFunction
		});
		loader.load('ruleViolationUnsnoozeByCommand.json');
	},
	getSnoozeParams: function(selection, numberOfDays, snoozeNote) {
		const params = {
			class: 'com.clifton.rule.violation.snooze.RuleViolationSnoozeCommand',
			id: selection.data['id']
		};
		params.startDate = this.getStartDate();
		params.numberOfDays = numberOfDays;
		params.snoozeNote = snoozeNote;
		return params;
	},
	getSnoozeMenu: function(isContextMenu, contextMenuRowIndex) {
		const gridPanel = this;
		//Group additional Ignore buttons under the main Ignore button
		const snoozeMenu = [];
		snoozeMenu.push({
			text: 'Snooze Violation',
			tooltip: 'Snooze this violation.',
			iconCls: 'snooze',
			scope: gridPanel,
			menu: {
				xtype: 'menu',
				items: gridPanel.getSnoozeMenuItems(gridPanel, 'single', contextMenuRowIndex)
			}
		});
		if (!isContextMenu) {
			snoozeMenu.push({
				text: 'Snooze Selected',
				tooltip: 'Snooze selected violations.',
				iconCls: 'snooze',
				scope: gridPanel,
				menu: {
					xtype: 'menu',
					items: gridPanel.getSnoozeMenuItems(gridPanel, 'selected', contextMenuRowIndex)
				}
			});
		}
		snoozeMenu.push({
			text: 'Snooze All For Definition',
			tooltip: 'Snooze all violations for this definition.',
			iconCls: 'snooze',
			scope: gridPanel,
			menu: {
				xtype: 'menu',
				items: gridPanel.getSnoozeMenuItems(gridPanel, 'definition', contextMenuRowIndex)
			}
		});
		return snoozeMenu;
	},
	getSnoozeMenuItems: function(gridPanel, snoozeType, contextMenuRowIndex) {
		let snoozeMenuItems = [
			{
				text: '3 Days',
				tooltip: 'Snooze for 3 days.',
				iconCls: 'snooze',
				scope: this,
				handler: function() {
					gridPanel.prepareSnoozeViolations(snoozeType, 3, contextMenuRowIndex);
				}
			},
			{
				text: '5 Days',
				tooltip: 'Snooze for 5 days.',
				iconCls: 'snooze',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareSnoozeViolations(snoozeType, 5, contextMenuRowIndex);
				}
			},
			{
				text: '10 Days',
				tooltip: 'Snooze for for 10 days.',
				iconCls: 'snooze',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareSnoozeViolations(snoozeType, 10, contextMenuRowIndex);
				}
			},
			{
				text: '30 Days',
				tooltip: 'Snooze for 30 days.',
				iconCls: 'snooze',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareSnoozeViolations(snoozeType, 30, contextMenuRowIndex);
				}
			},
			{
				text: 'Custom',
				tooltip: 'Snooze for specified number of days.',
				iconCls: 'snooze',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareSnoozeViolations(snoozeType, null, contextMenuRowIndex);
				}
			}, '-',
			{
				text: 'Unsnooze',
				tooltip: 'End the current snooze assignment.',
				iconCls: 'unsnooze',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareSnoozeViolations(snoozeType, null, contextMenuRowIndex, true);
				}
			}
		];
		if (gridPanel.addCustomSnoozeButtons) {
			snoozeMenuItems = snoozeMenubItems.concat(gridPanel.addCustomSnoozeButtons());
		}
		return snoozeMenuItems;
	},
	previewHandler: function() {
		const p = TCG.getParentTabPanel(this);

		TCG.createComponent('Clifton.rule.violation.ViolationPreviewWindow', {
			params: {categoryTableName: this.tableName, categoryTableEntityId: this.getEntityId()},
			openerCt: p,
			defaultIconCls: p.getWindow().iconCls
		});
	},
	// Can be overridden to do something else, i.e. Trades after ignored will be automatically ignored (see trade-shared.js for override)
	afterIgnoreSuccessAction: function(includeIgnoreRelatedByField) {
		const gridPanel = this;
		gridPanel.reload();
	},
	// Can be overridden to do something else
	afterSnoozeSuccessAction: function(status) {
		const gridPanel = this;
		gridPanel.reload();
	},
	editor: {
		detailPageClass: 'Clifton.rule.violation.ViolationWindow',
		deleteURL: 'ruleViolationManualDelete.json',
		addEditButtons: function(toolBar, gridPanel) {
			toolBar.add({
				iconCls: 'expand-all',
				tooltip: 'Expand or Collapse all Violations',
				scope: gridPanel.grid,
				handler: function() {
					this.expanded = (this.expanded === undefined) ? false : !this.expanded;
					this.view.toggleAllGroups(this.expanded);
				}
			});
			toolBar.add('-');

			if (gridPanel.manualAllowed === true) {
				this.addToolbarAddButton(toolBar, gridPanel);
				// Special Delete Method To verify Manual
				toolBar.add({
					text: 'Remove',
					tooltip: 'Remove selected manual violation',
					iconCls: 'remove',
					scope: this,
					handler: function() {
						const editor = this;
						const grid = this.grid;
						const selectionModel = grid.getSelectionModel();
						if (selectionModel.getCount() === 0) {
							TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
						}
						else if (selectionModel.getCount() !== 1) {
							TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							const manual = selectionModel.getSelected().get('ruleDefinition.manual');
							if (TCG.isFalse(manual)) {
								TCG.showError('Deletes are supported for manual violations only.', 'MANUAL ONLY');
							}
							else {
								Ext.Msg.confirm('Delete Selected Row', 'Would you like to delete selected manual violation?', function(a) {
									if (a === 'yes') {
										const loader = new TCG.data.JsonLoader({
											waitTarget: grid.ownerCt,
											waitMsg: 'Deleting...',
											params: editor.getDeleteParams(selectionModel),
											conf: {rowId: selectionModel.getSelected().id},
											onLoad: function(record, conf) {
												grid.getStore().remove(selectionModel.getSelected());
												grid.ownerCt.updateCount();
											}
										});
										loader.load(editor.getDeleteURL());
									}
								});
							}
						}
					}
				});
				toolBar.add('-');
			}

			toolBar.add({
				name: 'ignoreMenu',
				text: 'Ignore',
				xtype: 'splitbutton',
				tooltip: 'Mark selected violation as ignored.',
				iconCls: 'cancel',
				scope: gridPanel,
				handler: function() {
					gridPanel.prepareIgnoreViolations('ignoreSelected');
				},
				//We push the checkbox only to the top toolbar - it must be unchecked there
				//otherwise code needs to be updated to sync the context menu and toolbar checkbox when changed.
				menu: new Ext.menu.Menu({
					items: gridPanel.getToolbarIgnoreMenu()
				})
			});
			toolBar.add('-');
			toolBar.add({
				name: 'snoozeMenu',
				text: 'Snooze',
				xtype: 'splitbutton',
				tooltip: 'Snooze selected violation.',
				iconCls: 'snooze',
				scope: gridPanel,
				menu: new Ext.menu.Menu({
					items: gridPanel.getToolbarSnoozeMenu()
				}),
				handler: function() {
					gridPanel.prepareSnoozeViolations('selected');
				}
			});
			toolBar.add('-');

			// Add custom additional buttons - i.e. Trade Window adds a button to Ignore Selected For All Trades In Trade Group
			if (gridPanel.addCustomEditButtons) {
				gridPanel.addCustomEditButtons(toolBar);
			}

			if (TCG.isTrue(gridPanel.previewAllowed)) {
				toolBar.add({
					text: 'Preview',
					tooltip: 'Preview all rule violations including those that passed.',
					iconCls: 'preview',
					scope: gridPanel,
					handler: function() {
						gridPanel.previewHandler();
					}
				});
				toolBar.add('-');
			}
		},

		getDefaultData: function(gridPanel) {
			const tbl = TCG.data.getData('systemTableByName.json?tableName=' + gridPanel.tableName, this, 'system.table.' + gridPanel.tableName);
			return {
				ruleDefinition: {ruleCategory: {categoryTable: tbl}},
				linkedFkFieldId: gridPanel.getEntityId()
			};
		},

		gridConfig: {
			listeners: {
				'rowclick': function(grid, rowIndex, evt) {
					if (TCG.isActionColumn(evt.target)) {
						const row = grid.store.data.items[rowIndex];
						const clz = row.json.violationCauseTable.detailScreenClass;
						const id = row.json.causeFkFieldId;
						const gridPanel = grid.ownerGridPanel;
						gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
					}
				}
			}
		}
	}
});
Ext.reg('rule-violation-grid', Clifton.rule.violation.ViolationGridPanel);


Clifton.rule.definition.AssignmentGridPanel = Ext.extend(TCG.grid.GridPanel, {
	name: 'ruleAssignmentListFind',
	xtype: 'gridpanel',
	instructions: 'Rule assignments are used to define default rule parameters, override rule parameters for a specific entity or to exclude an entity from rule execution.',
	wikiPage: 'IT/Rule+Engine',

	groupField: 'ruleDefinition.name',
	groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Assignments" : "Assignment"]}',

	additionalPropertiesToRequest: 'additionalScopeLabel|additionalScopeFkFieldId|ruleDefinition.ruleCategory.additionalScopeLabel|entityLabel|entityFkFieldId|ruleDefinition.entityTable.label|ruleDefinition.ruleScope.name',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Rule Definition', dataIndex: 'ruleDefinition.name', hidden: true, width: 100, filter: {type: 'combo', searchFieldName: 'ruleDefinitionId', url: 'ruleDefinitionListFind.json'}},
		{header: 'Entity Table', dataIndex: 'ruleDefinition.entityTable.label', hidden: true, width: 100, filter: {searchFieldName: 'entityTableName'}},
		{
			header: 'Apply to', dataIndex: 'scope', width: 70, filter: false, sortable: false,
			renderer: function(v, p, r) {
				if (r.json.entityFkFieldId) {
					p.attr = 'style="PADDING-LEFT: 20px"';
					return r.json.ruleDefinition.entityTable.label;
				}
				if (r.json.additionalScopeFkFieldId) {
					p.attr = 'style="PADDING-LEFT: 15px"';
					p.css = 'amountAdjusted';
					return r.json.ruleDefinition.ruleCategory.additionalScopeLabel;
				}
				p.css = 'amountPositive';
				if (r.json.ruleDefinition.ruleScope) {
					p.attr = 'style="PADDING-LEFT: 10px"';
					return r.json.ruleDefinition.ruleScope.name;
				}
				return 'All ' + r.json.ruleDefinition.ruleCategory.additionalScopeLabel + 's';
			}
		},
		{
			header: 'Scope', width: 120, dataIndex: 'ruleDefinition.ruleCategory.additionalScopeTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && r.json.additionalScopeFkFieldId) {
					return TCG.renderActionColumn('view', r.json.additionalScopeLabel, 'View ' + r.json.ruleDefinition.ruleCategory.additionalScopeLabel + ' linked to this assignment', 'additionalScope');
				}
				else if (r.json.additionalScopeLabel) {
					return r.json.additionalScopeLabel;
				}
				return '';
			}
		},
		{
			header: 'Entity', width: 120, dataIndex: 'ruleDefinition.entityTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && r.json.entityFkFieldId) {
					return TCG.renderActionColumn('view', r.json.entityLabel, 'View ' + r.json.ruleDefinition.entityTable.label + ' linked to this assignment', 'entity');
				}
				else if (r.json.entityLabel) {
					return r.json.entityLabel;
				}
				return '';
			}
		},
		{header: 'Rule Scope', width: 100, dataIndex: 'ruleDefinition.ruleScope.name', hidden: true},
		{header: 'Note', dataIndex: 'note', width: 200, hidden: true},
		{header: 'Excluded', width: 30, dataIndex: 'excluded', type: 'boolean'},
		{header: 'Snoozed', width: 30, dataIndex: 'snoozed', type: 'boolean'},
		{header: 'Amount', dataIndex: 'ruleAmount', width: 30, type: 'float', useNull: true},
		{header: 'Minimum', dataIndex: 'minAmount', width: 30, type: 'float', useNull: true},
		{header: 'Maximum', dataIndex: 'maxAmount', width: 30, type: 'float', useNull: true},
		{header: 'Priority', dataIndex: 'ruleDefinition.priority.name', width: 30, hidden: true},
		{header: 'Order', dataIndex: 'ruleDefinition.order', width: 30, hidden: true},
		{header: 'Start Date', dataIndex: 'startDate', width: 40, hidden: true},
		{header: 'End Date', dataIndex: 'endDate', width: 40, hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.rule.definition.RuleAssignmentWindow',
		deleteURL: 'ruleAssignmentDelete.json',
		getDefaultDataForExisting: function(gridPanel) {
			return undefined;
		},
		getDefaultData: function(gridPanel) { // defaults category for the detail page
			const category = gridPanel.getCategorySelected();
			if (!category) {
				return false;
			}
			return {
				ruleDefinition: {ruleCategory: category}
			};
		}
	},
	getCategorySelected: function() {
		const t = this.getTopToolbar();
		const cat = TCG.getChildByName(t, 'categoryId');
		if (TCG.isNotBlank(cat.getRawValue())) {
			return Clifton.rule.setup.getRuleCategory(cat.getRawValue(), this);
		}
		TCG.showError('Please select a category first before clicking Add', 'Category Required');
	},
	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					let clz = row.json.ruleDefinition.ruleCategory.additionalScopeTable.detailScreenClass;
					let id = row.json.additionalScopeFkFieldId;
					const eventName = TCG.getActionColumnEventName(evt);
					if (eventName === 'entity') {
						clz = row.json.ruleDefinition.entityTable.detailScreenClass;
						id = row.json.entityFkFieldId;
					}
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
				}
			}
		}
	},
	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: '', boxLabel: 'Global Only&nbsp;', name: 'globalOnly', xtype: 'toolbar-checkbox'},
			{fieldLabel: 'Rule Category', xtype: 'toolbar-combo', name: 'categoryId', width: 150, url: 'ruleCategoryListFind.json'},
			{fieldLabel: 'Rule Definition', xtype: 'toolbar-combo', name: 'ruleDefinitionId', width: 150, url: 'ruleDefinitionListFind.json', linkedFilter: 'ruleDefinition.name'},
			{fieldLabel: 'Active On', xtype: 'toolbar-datefield', name: 'activeOnDate'}
		];
	},
	getLoadParams: function(firstLoad) {
		const dateField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		const params = {};
		if (firstLoad) {
			// default to today
			if (TCG.isBlank(dateField.getValue())) {
				dateField.setValue((new Date()).format('m/d/Y'));
			}
		}
		if (TCG.isNotBlank(dateField.getValue())) {
			params.activeOnDate = (dateField.getValue()).format('m/d/Y');

		}

		const t = this.getTopToolbar();
		const cat = TCG.getChildByName(t, 'categoryId');
		if (TCG.isNotBlank(cat.getValue())) {
			params.categoryId = cat.getValue();
		}
		const globalOnly = TCG.getChildByName(t, 'globalOnly');
		if (globalOnly.checked) {
			params.globalOnly = true;
		}
		return params;
	}
});
Ext.reg('rule-assignment-grid', Clifton.rule.definition.AssignmentGridPanel);


Clifton.rule.definition.AssignmentGridPanel_ForAdditionalScope = Ext.extend(TCG.grid.GridPanel, {

	////////////////////////////////////////////////////////////////////////
	// Common Data/Methods to Override

	scopeTableName: 'REQUIRED_TABLE_NAME',
	defaultCategoryName: undefined, // override to default a tab to specific category - i.e. Portfolio Run Rules is the default for Client Accounts

	getAdditionalScopeFkFieldId: function() {
		return this.getWindow().getMainFormId();
	},
	getAdditionalScopeFkFieldLabel: function() {
		return TCG.getValue('label', this.getWindow().getMainForm().formValues);
	},

	appendAdditionalLoadParams: function(firstLoad, params) {
		return params;
	},

	appendAdditionalDefaultData: function(row, params) {
		return params;
	},

	getCategorySelected: function() {
		const t = this.getTopToolbar();
		const cat = TCG.getChildByName(t, 'categoryId');
		if (TCG.isNotBlank(cat.getRawValue())) {
			return Clifton.rule.setup.getRuleCategory(cat.getRawValue(), this);
		}
		TCG.showError('Please select a category from the toolbar selection.', 'Category Required');
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Category', xtype: 'toolbar-combo', name: 'categoryId', width: 150, url: 'ruleCategoryListFind.json?additionalScopeTableNameEquals=' + this.scopeTableName},
			{fieldLabel: 'Active On', xtype: 'datefield', name: 'activeOnDate', width: 80}
		];
	},

	////////////////////////////////////////////////////////////////////////

	viewNames: ['Overrides Only', 'Global and Overrides'],
	defaultViewName: undefined, // optionally switch to this view on load
	includeAllColumnsView: false,
	name: 'ruleAssignmentListForAdditionalScopeEntityFind',
	instructions: 'The following rules have been defined that apply to this entity.  By selecting a View you can see Overrides only or Global and Overrides.<br />In order to add a new account rule, please first select an existing row with the definition you want to add an override for.<br/>Additionally:<br/><br/>1.  Global assignments may only be edited by admins.<br />2.  <span style="color: #ff0000">Critical</span> (global and not ignorable) rules cannot be overridden by more specific assignments.<br />3.  If the rule definition for an assignment specifies a modify condition, it must evaluate to \'true\' for a user to be allowed to edit it.',
	groupField: undefined,
	remoteSort: true,
	isPagingEnabled: function() {
		return false;
	},
	pageSize: 1000,


	additionalPropertiesToRequest: 'additionalScopeLabel|additionalScopeFkFieldId|ruleDefinition.id|ruleDefinition.manual|ruleDefinition.ruleCategory.id|ruleDefinition.ruleCategory.additionalScopeLabel|entityLabel|entityFkFieldId|ruleDefinition.entityTable.label|ruleDefinition.ruleScope.name|ruleDefinition.ignorable|ruleDefinition.priority.cssStyle',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Rule Definition', dataIndex: 'ruleDefinition.name', width: 115, filter: {type: 'combo', searchFieldName: 'ruleDefinitionId', url: 'ruleDefinitionListFind.json'}},
		{header: 'Entity Table', dataIndex: 'ruleDefinition.entityTable.label', hidden: true, width: 100, filter: {searchFieldName: 'entityTableName'}},
		{
			header: 'Apply to', dataIndex: 'scope', width: 90, filter: false, sortable: false,
			renderer: function(v, p, r) {
				if (r.json.entityFkFieldId) {
					p.attr = 'style="PADDING-LEFT: 20px"';
					return r.json.ruleDefinition.entityTable.label;
				}
				if (r.json.additionalScopeFkFieldId) {
					p.attr = 'style="PADDING-LEFT: 15px"';
					p.css = 'amountAdjusted';
					return r.json.ruleDefinition.ruleCategory.additionalScopeLabel;
				}
				p.css = 'amountPositive';
				if (r.json.ruleDefinition.ruleScope) {
					p.attr = 'style="PADDING-LEFT: 10px"';
					return r.json.ruleDefinition.ruleScope.name;
				}
				return 'All ' + r.json.ruleDefinition.ruleCategory.additionalScopeLabel + 's';
			}
		},
		{
			header: 'Scope', width: 120, dataIndex: 'ruleDefinition.ruleCategory.additionalScopeTable.detailScreenClass', hidden: true, filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && r.json.additionalScopeFkFieldId) {
					return TCG.renderActionColumn('view', r.json.additionalScopeLabel, 'View ' + r.json.ruleDefinition.ruleCategory.additionalScopeLabel + ' linked to this assignment', 'additionalScope');
				}
				else if (r.json.additionalScopeLabel) {
					return r.json.additionalScopeLabel;
				}
				return '';
			}
		},
		{
			header: 'Entity', width: 120, dataIndex: 'ruleDefinition.entityTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && r.json.entityFkFieldId) {
					return TCG.renderActionColumn('view', r.json.entityLabel, 'View ' + r.json.ruleDefinition.entityTable.label + ' linked to this assignment', 'entity');
				}
				else if (r.json.entityLabel) {
					return r.json.entityLabel;
				}
				return '';
			}
		},
		{header: 'Rule Scope', width: 100, dataIndex: 'ruleDefinition.ruleScope.name', hidden: true},
		{header: 'Note', width: 200, dataIndex: 'note', hidden: true},
		{
			header: 'Priority', width: 30, dataIndex: 'ruleDefinition.priority.name',
			filter: {searchFieldName: 'priorityId', type: 'combo', displayField: 'name', url: 'systemPriorityListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.ruleDefinition.priority.cssStyle;
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Excluded', width: 30, dataIndex: 'excluded', type: 'boolean'},
		{header: 'Snoozed', width: 30, dataIndex: 'snoozed', type: 'boolean'},
		{header: 'Amount', dataIndex: 'ruleAmount', width: 35, type: 'float', useNull: true},
		{header: 'Min', dataIndex: 'minAmount', width: 35, type: 'float', useNull: true},
		{header: 'Max', dataIndex: 'maxAmount', width: 35, type: 'float', useNull: true},
		{header: 'Order', dataIndex: 'ruleDefinition.order', width: 30, hidden: true},
		{header: 'Start Date', dataIndex: 'startDate', width: 40, hidden: true},
		{header: 'End Date', dataIndex: 'endDate', width: 40, hidden: true}
	],
	editor: {
		detailPageClass: 'Clifton.rule.definition.RuleAssignmentWindow',
		deleteURL: 'ruleAssignmentDelete.json',
		addToolbarAddButton: function(toolBar) {
			const editor = this;
			const gridPanel = this.getGridPanel();
			toolBar.add({
				text: 'Add',
				tooltip: 'Add override for selected rule definition.',
				iconCls: 'add',
				scope: gridPanel,
				handler: function() {
					editor.addRuleAssignmentOverride(gridPanel);
				}
			});
			toolBar.add('-');
		},
		addToolbarDeleteButton: function(toolBar) {
			const editor = this;
			toolBar.add({
				text: 'Remove',
				tooltip: 'Remove selected assignment',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					editor.deleteRuleAssignmentOverride();
				}
			});
			toolBar.add('-');
		},
		addRuleAssignmentOverride: function(gridPanel) {
			const row = this.getSelectedRow(true);
			if (row !== false) {
				if (!row.json.additionalScopeFkFieldId) {
					if (TCG.isFalse(row.json.excluded) && TCG.isFalse(row.json.ruleDefinition.ignorable)) {
						TCG.showError('You cannot add an override for global not ignorable rule definitions.', 'Cannot Override');
						return;
					}
				}
				if (TCG.isTrue(row.json.ruleDefinition.manual)) {
					TCG.showError('You cannot add an override for manual rule definitions.', 'Cannot Override');
					return;
				}
				this.openDetailPage(this.detailPageClass, gridPanel, null, row);
			}
		},
		deleteRuleAssignmentOverride: function() {
			const editor = this;
			const grid = this.grid;
			const row = this.getSelectedRow(false);
			if (row !== false) {
				if (!row.json.additionalScopeFkFieldId) {
					TCG.showError('You cannot delete global rule assignments.  In order to disable a rule assignment please add an override with Excluded option checked.', 'Cannot Delete');
					return;
				}
				Ext.Msg.confirm('Delete Selected Assignment', 'Would you like to delete selected assignment?', function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Deleting...',
							params: {id: row.json.id},
							conf: {rowId: row.id},
							onLoad: function(record, conf) {
								editor.getGridPanel().reload();
							}
						});
						loader.load(editor.getDeleteURL());
					}
				});
			}
		},
		getSelectedRow: function(add) {
			const grid = this.grid;
			const selectionModel = grid.getSelectionModel();
			if (selectionModel.getCount() === 0) {
				let msg = 'Please select an existing assignment to remove.';
				if (TCG.isTrue(add)) {
					msg = 'In order to add an override assignment, please select an existing assignment from the list.  You may need to change the Views option to <b>Global and Overrides</b> to see all rules available.';
				}
				TCG.showError(msg, 'No Row(s) Selected');
				return false;
			}
			else if (selectionModel.getCount() !== 1) {
				TCG.showError('Multiple selections are not supported.  Please select one row.', 'NOT SUPPORTED');
				return false;
			}
			return selectionModel.getSelected();
		},
		getDefaultDataForExisting: function(gridPanel) {
			return undefined;
		},
		getDefaultData: function(gridPanel, row) { // defaults category and additional scope (optionally entity) for the detail page
			return gridPanel.getDefaultData(row);
		}
	},

	getDefaultData: function(row) { // defaults rule definition and additional scope for the detail page
		const params = {};
		params.ruleDefinition = TCG.data.getData('ruleDefinition.json?id=' + row.json.ruleDefinition.id, this);
		if (TCG.isNotBlank(this.getAdditionalScopeFkFieldId())) {
			params.additionalScopeFkFieldId = this.getAdditionalScopeFkFieldId();
			params.additionalScopeLabel = this.getAdditionalScopeFkFieldLabel();
		}
		return this.appendAdditionalDefaultData(row, params);
	},

	gridConfig: {
		listeners: {
			'rowclick': function(grid, rowIndex, evt) {
				if (TCG.isActionColumn(evt.target)) {
					const row = grid.store.data.items[rowIndex];
					let clz = row.json.ruleDefinition.ruleCategory.additionalScopeTable.detailScreenClass;
					let id = row.json.additionalScopeFkFieldId;
					const eventName = TCG.getActionColumnEventName(evt);
					if (eventName === 'entity') {
						clz = row.json.ruleDefinition.entityTable.detailScreenClass;
						id = row.json.entityFkFieldId;
					}
					const gridPanel = grid.ownerGridPanel;
					gridPanel.editor.openDetailPage(clz, gridPanel, id, row);
				}
			}
		}
	},

	updateCount: function() {
		// OVERRIDDEN TO DO NOTHING - DON'T SHOW RECORD COUNT
	},

	getLoadParams: function(firstLoad) {
		const dateField = TCG.getChildByName(this.getTopToolbar(), 'activeOnDate');
		let params = {};

		// Call this first because it might need to set up something
		params = this.appendAdditionalLoadParams(firstLoad, params);
		if (TCG.isFalse(params)) {
			return false;
		}

		if (firstLoad) {
			// default to today
			if (TCG.isBlank(dateField.getValue())) {
				dateField.setValue((new Date()).format('m/d/Y'));
			}
			const viewName = this.defaultViewName || 'Overrides Only';
			this.setDefaultView(viewName); // sets it as "checked"
			this.switchToView(viewName, true);

			if (this.defaultCategoryName) {
				const cat = Clifton.rule.setup.getRuleCategory(this.defaultCategoryName, this);
				if (TCG.getChildByName(this.getTopToolbar(), 'categoryId')) {
					TCG.getChildByName(this.getTopToolbar(), 'categoryId').setValue({value: cat.id, text: cat.name});
				}
			}
			else {
				return false;
			}
		}
		if (TCG.isNotBlank(dateField.getValue())) {
			params.activeOnDate = (dateField.getValue()).format('m/d/Y');
		}
		params.categoryId = this.getCategorySelected().id;
		params.additionalScopeFkFieldId = this.getAdditionalScopeFkFieldId();

		if (this.currentViewName === 'Overrides Only') {
			params.overridesOnly = true;
		}
		return params;
	}
});
Ext.reg('rule-assignment-grid-forAdditionalScope', Clifton.rule.definition.AssignmentGridPanel_ForAdditionalScope);


Clifton.rule.definition.AssignmentGridPanel_ForAdditionalScopeEntity = Ext.extend(Clifton.rule.definition.AssignmentGridPanel_ForAdditionalScope, {
	scopeTableName: 'REQUIRED_TABLE_NAME',
	entityTableName: 'REQUIRED_TABLE_NAME',
	defaultCategoryName: undefined, // ENTITY LEVELS NEED TO REQUIRE A CATEGORY?
	columnOverrides: [{dataIndex: 'ruleDefinition.entityTable.detailScreenClass', hidden: true}],

	// SHOULD BE OVERRIDDEN
	getAdditionalScopeFkFieldId: function() {
		return undefined;
	},
	// SHOULD BE OVERRIDDEN
	getAdditionalScopeFkFieldLabel: function() {
		return undefined;
	},

	getEntityFkFieldId: function() {
		return this.getWindow().getMainFormId();
	},
	getEntityFkFieldLabel: function() {
		return TCG.getValue('label', this.getWindow().getMainForm().formValues);
	},

	appendAdditionalLoadParams: function(firstLoad, params) {
		params.entityTableNameEquals = this.entityTableName;
		params.entityFkFieldId = this.getEntityFkFieldId();
		return params;
	},

	appendAdditionalDefaultData: function(row, params) { // appends entityFkFieldId & entityLabel
		if (TCG.isNotBlank(this.getEntityFkFieldId())) {
			params.entityFkFieldId = this.getEntityFkFieldId();
			params.entityLabel = this.getEntityFkFieldLabel();
		}
		return params;
	},

	getCategorySelected: function() {
		if (!this.defaultCategoryName) {
			TCG.showError('Default Category Name is required');
			return false;
		}
		return Clifton.rule.setup.getRuleCategory(this.defaultCategoryName, this);
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'Active On', xtype: 'datefield', name: 'activeOnDate', width: 80}
		];
	}
});
Ext.reg('rule-assignment-grid-forAdditionalScopeEntity', Clifton.rule.definition.AssignmentGridPanel_ForAdditionalScopeEntity);
