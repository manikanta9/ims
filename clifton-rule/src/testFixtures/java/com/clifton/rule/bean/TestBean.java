package com.clifton.rule.bean;

import com.clifton.core.beans.IdentityObject;

import java.io.Serializable;


/**
 * @author mitchellf
 */
public class TestBean implements IdentityObject {

	private String name;
	private int id;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Serializable getIdentity() {
		return getId();
	}


	@Override
	public boolean isNewBean() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}
}
