package com.clifton.rule.violation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.rule.violation.cache.RuleViolationByEntityCache;

import java.util.List;


/**
 * For Tests, because of multiple sub-criteria not handled in XML, should reference this class instead
 *
 * @author manderson
 */
public class MockedRuleViolationByEntityCache extends RuleViolationByEntityCache {


	@Override
	protected List<RuleViolation> lookupBeanList(ReadOnlyDAO<RuleViolation> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		List<RuleViolation> violationList = dao.findByField("linkedFkFieldId", keyProperties[1]);
		if (violationList != null) {
			violationList = BeanUtils.filter(violationList, violation -> violation.getRuleAssignment().getRuleDefinition().getRuleCategory().getCategoryTable().getId(), keyProperties[0]);
		}
		return violationList;
	}
}
