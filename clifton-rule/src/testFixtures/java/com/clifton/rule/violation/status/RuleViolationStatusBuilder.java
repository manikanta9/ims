package com.clifton.rule.violation.status;


public class RuleViolationStatusBuilder {

	private RuleViolationStatus ruleViolationStatus;


	private RuleViolationStatusBuilder(RuleViolationStatus ruleViolationStatus) {
		this.ruleViolationStatus = ruleViolationStatus;
	}


	public static RuleViolationStatusBuilder createUnprocessed() {
		RuleViolationStatusBuilder builder = new RuleViolationStatusBuilder(new RuleViolationStatus());
		builder.withId((short) 1).withName("UNPROCESSED").withDescription("Processing has not been performed yet.").withOrder(10);
		return builder;
	}


	public static RuleViolationStatusBuilder createProcessedFailed() {
		RuleViolationStatusBuilder builder = new RuleViolationStatusBuilder(new RuleViolationStatus());
		builder.withId((short) 2).withName("PROCESSED_FAILED").withDescription("Processing was attempted but failed because of non-ignorable warnings.").withOrder(20);
		return builder;
	}


	public static RuleViolationStatusBuilder createProcessedViolations() {
		RuleViolationStatusBuilder builder = new RuleViolationStatusBuilder(new RuleViolationStatus());
		builder.withId((short) 3).withName("PROCESSED_VIOLATIONS").withDescription("Processing succeeded with warnings that are still waiting to be reviewed and ignored.").withOrder(30);
		return builder;
	}


	public static RuleViolationStatusBuilder createProcessedIgnored() {
		RuleViolationStatusBuilder builder = new RuleViolationStatusBuilder(new RuleViolationStatus());
		builder.withId((short) 4).withName("PROCESSED_IGNORED").withDescription("Processing succeeded with warnings.  All warnings have been reviewed and ignored.").withOrder(40);
		return builder;
	}


	public static RuleViolationStatusBuilder createProcessedSuccess() {
		RuleViolationStatusBuilder builder = new RuleViolationStatusBuilder(new RuleViolationStatus());
		builder.withId((short) 5).withName("PROCESSED_SUCCESS").withDescription("Processing succeeded without any warnings.").withOrder(50);
		return builder;
	}


	public RuleViolationStatusBuilder withId(short id) {
		getRuleViolationStatus().setId(id);
		return this;
	}


	public RuleViolationStatusBuilder withName(String name) {
		getRuleViolationStatus().setName(name);
		return this;
	}


	public RuleViolationStatusBuilder withDescription(String description) {
		getRuleViolationStatus().setDescription(description);
		return this;
	}


	public RuleViolationStatusBuilder asProcessed() {
		getRuleViolationStatus().setProcessed(true);
		return this;
	}


	public RuleViolationStatusBuilder asReadyForUse() {
		getRuleViolationStatus().setReadyForUse(true);
		return this;
	}


	public RuleViolationStatusBuilder asUnresolvedViolationsExist() {
		getRuleViolationStatus().setUnresolvedViolationsExist(true);
		return this;
	}


	public RuleViolationStatusBuilder withOrder(int order) {
		getRuleViolationStatus().setOrder(order);
		return this;
	}


	private RuleViolationStatus getRuleViolationStatus() {
		return this.ruleViolationStatus;
	}


	public RuleViolationStatus toRuleViolationStatus() {
		return this.ruleViolationStatus;
	}
}
