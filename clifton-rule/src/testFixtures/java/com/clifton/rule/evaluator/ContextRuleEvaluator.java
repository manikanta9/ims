package com.clifton.rule.evaluator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Test fixture to relay the information contained on the Rule Evaluator context for testing/verification
 *
 * @author mitchellf
 */
public class ContextRuleEvaluator extends BaseRuleEvaluator<IdentityObject, RuleEvaluatorContext> {

	private RuleViolationService ruleViolationService;


	@Override
	public List<RuleViolation> evaluateRule(IdentityObject ruleBean, RuleConfig ruleConfig, RuleEvaluatorContext context) {

		List<RuleViolation> violations = new ArrayList<>();

		Map<String, Object> templateValues = new HashMap<>();
		StringBuilder violationMessage = new StringBuilder();
		violationMessage.append("Rule was executed for ")
				.append(ruleBean)
				.append(" with context ")
				.append(context.getClass());
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		templateValues.put("violationNote", violationMessage.toString());

		violations.add(getRuleViolationService().createRuleViolation(entityConfig, ruleBean.getIdentity(), templateValues));

		return violations;
	}


	@Override
	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	@Override
	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
