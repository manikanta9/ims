import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// Used by Crowd
	runtimeOnly("org.codehaus.xfire:xfire-aegis:1.2.6") {
		exclude(module = "XmlSchema")
		exclude(module = "jaxen")
		exclude(module = "junit")
	}

	// JSP API library
	implementation("javax.servlet.jsp:jsp-api:2.2")

	// Charting JS library
	implementation("org.webjars.npm:chart.js:2.7.3")
	implementation("org.webjars.npm:chartjs-plugin-datalabels:0.7.0")
	implementation("org.webjars.npm:moment:2.24.0")


	// WebJars
	// implementation("org.webjars:extjs:3.3.0") // TODO: Switch to official webjars distribution
	implementation("sencha:extjs:3.3.0:clifton-webjar")
	implementation("sencha:extjs-printer:1.0.0:webjar")
	implementation("org.webjars.npm:jsoneditor:5.5.7")
	implementation("org.webjars:google-diff-match-patch:20121119-1")


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	javascript(project(":clifton-core"))
	javascript(project(":clifton-core:clifton-core-messaging"))
	javascript(project(":clifton-security"))
	javascript(project(":clifton-system"))
	javascript(project(":clifton-system:clifton-system-search"))
	javascript(project(":clifton-system:clifton-system-statistic"))
	javascript(project(":clifton-system:clifton-system-note"))
	javascript(project(":clifton-system:clifton-system-navigation"))
	javascript(project(":clifton-system:clifton-system-priority"))
	javascript(project(":clifton-system:clifton-system-query"))
	javascript(project(":clifton-system:clifton-system-field-mapping"))
	javascript(project(":clifton-replication"))
	javascript(project(":clifton-bloomberg"))
	javascript(project(":clifton-workflow"))
	javascript(project(":clifton-calendar"))
	javascript(project(":clifton-calendar:clifton-calendar-schedule"))
	javascript(project(":clifton-notification"))
	javascript(project(":clifton-batch"))
	javascript(project(":clifton-document"))
	javascript(project(":clifton-report"))
	javascript(project(":clifton-export"))
	javascript(project(":clifton-otc"))
	javascript(project(":clifton-business"))
	javascript(project(":clifton-business:clifton-business-event"))
	javascript(project(":clifton-business:clifton-business-contact"))
	javascript(project(":clifton-business:clifton-business-client"))
	javascript(project(":clifton-business:clifton-business-contract"))
	javascript(project(":clifton-investment"))
	javascript(project(":clifton-marketdata"))
	javascript(project(":clifton-accounting"))
	javascript(project(":clifton-portfolio"))
	javascript(project(":clifton-portfolio:clifton-portfolio-target"))
	javascript(project(":clifton-performance"))
	javascript(project(":clifton-performance:clifton-performance-portfolio"))
	javascript(project(":clifton-performance:clifton-performance-target"))
	javascript(project(":clifton-lending"))
	javascript(project(":clifton-collateral"))
	javascript(project(":clifton-fix"))
	javascript(project(":clifton-trade"))
	javascript(project(":clifton-trade:clifton-trade-order"))
	javascript(project(":clifton-trade:clifton-trade-order-fix"))
	javascript(project(":clifton-product"))
	javascript(project(":clifton-dw"))
	javascript(project(":clifton-billing"))
	javascript(project(":clifton-portal"))
	javascript(project(":clifton-integration"))
	javascript(project(":clifton-reconcile"))
	javascript(project(":clifton-reconciliation"))
	javascript(project(":clifton-compliance"))
	javascript(project(":clifton-fax"))
	javascript(project(":clifton-rule"))
	javascript(project(":clifton-swift"))
	javascript(project(":clifton-instruction"))
	javascript(project(":clifton-archive"))
	javascript(project(":clifton-websocket"))

	implementation(project(":clifton-accounting"))
	implementation(project(":clifton-archive")) { usingVariant("server") } // TODO: This is temporarily included in order to allow IMS to cover both functions until the Archive application is activated
	implementation(project(":clifton-business:clifton-business-event"))
	implementation(project(":clifton-business:clifton-business-contact"))
	implementation(project(":clifton-business:clifton-business-client"))
	implementation(project(":clifton-business:clifton-business-contract"))
	implementation(project(":clifton-billing"))
	implementation(project(":clifton-bloomberg")) { usingVariant("client") }
	implementation(project(":clifton-calendar"))
	implementation(project(":clifton-calendar:clifton-calendar-schedule"))
	implementation(project(":clifton-compliance"))
	implementation(project(":clifton-core"))
	implementation(project(":clifton-core:clifton-core-messaging"))
	implementation(project(":clifton-export"))
	implementation(project(":clifton-export:clifton-export-budi"))
	implementation(project(":clifton-fax"))
	implementation(project(":clifton-dw"))
	implementation(project(":clifton-fix")) { usingVariant("api") }
	implementation(project(":clifton-integration")) { usingVariant("api") }
	implementation(project(":clifton-lending"))
	implementation(project(":clifton-notification"))
	implementation(project(":clifton-otc"))
	implementation(project(":clifton-portal")) { usingVariant("api") }
	implementation(project(":clifton-product"))
	implementation(project(":clifton-reconcile"))
	implementation(project(":clifton-reconciliation")) { usingVariant("api") }
	implementation(project(":clifton-replication"))
	implementation(project(":clifton-swift")) { usingVariant("client") }
	implementation(project(":clifton-archive"))
	implementation(project(":clifton-websocket"))

	implementation(project(":clifton-system"))
	implementation(project(":clifton-system:clifton-system-navigation"))
	implementation(project(":clifton-system:clifton-system-note"))
	implementation(project(":clifton-system:clifton-system-priority"))
	implementation(project(":clifton-system:clifton-system-query"))
	implementation(project(":clifton-system:clifton-system-search"))
	implementation(project(":clifton-system:clifton-system-statistic"))
	implementation(project(":clifton-system:clifton-system-field-mapping"))

	implementation(project(":clifton-trade"))
	implementation(project(":clifton-trade:clifton-trade-order"))
	implementation(project(":clifton-trade:clifton-trade-order-fix"))

	implementation(project(":clifton-portfolio"))
	implementation(project(":clifton-portfolio:clifton-portfolio-target"))

	implementation(project(":clifton-performance"))
	implementation(project(":clifton-performance:clifton-performance-portfolio"))
	implementation(project(":clifton-performance:clifton-performance-target"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-accounting")))
	testFixturesApi(testFixtures(project(":clifton-billing")))
	testFixturesApi(testFixtures(project(":clifton-trade:clifton-trade-order")))
	testFixturesApi(testFixtures(project(":clifton-calendar:clifton-calendar-schedule")))

	////////////////////////////////////////////////////////////////////////////
	////////            Integration Test External Dependencies          ////////
	////////////////////////////////////////////////////////////////////////////

	// For email and general export tests
	integTestImplementation("org.mockftpserver:MockFtpServer:2.5")
	// test smtp server
	integTestImplementation("org.subethamail:subethasmtp:3.1.7")

	// For AWS S3 export tests
	integTestImplementation("com.adobe.testing:s3mock-junit5:2.1.26") {
		exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
	}
	integTestImplementation("javax.jms:javax.jms-api:2.0.1")

	// Assertions for smtp
	integTestImplementation("org.hamcrest:hamcrest-core")
	//JSOUP is an excellent library for parsing HTML in java
	integTestImplementation("org.jsoup:jsoup:1.7.2")
	//SWIFT
	integTestImplementation("org.springframework.integration:spring-integration-core")
	integTestImplementation("org.springframework.integration:spring-integration-file")

	////////////////////////////////////////////////////////////////////////////
	////////            Integration Test Internal Dependencies          ////////
	////////////////////////////////////////////////////////////////////////////

	integTestImplementation(testFixtures(project(":clifton-test")))

	// Project dependencies
	integTestImplementation(testFixtures(project(":clifton-fix:clifton-fix-quickfix")))
	integTestImplementation(testFixtures(project(":clifton-fix:clifton-fix-server")))
	integTestImplementation(testFixtures(project(":clifton-fix:clifton-fix-quickfix-server")))
	integTestImplementation(testFixtures(project(":clifton-swift")))
	integTestImplementation(testFixtures(project(":clifton-portal"))) // Search Form Tests need this to get the hibernate mapping
	integTestImplementation(testFixtures(project(":clifton-reconciliation"))) // Search Form Tests need this to get the hibernate mapping
	integTestImplementation(testFixtures(project(":clifton-trade")))
	integTestImplementation(testFixtures(project(":clifton-trade:clifton-trade-order")))
	integTestImplementation(testFixtures(project(":clifton-trade:clifton-trade-order-fix")))
	integTestImplementation(testFixtures(project(":clifton-fix")))
}
