package com.clifton.ims.tests.lending.repo;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.search.LendingRepoSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * A set of tests to verify the functionality of the LendingRepoService method calls.
 *
 * @author davidi
 */
public class LendingRepoServiceTests extends BaseImsIntegrationTest {

	@Resource
	private LendingRepoService lendingRepoService;

	@Resource
	private LendingRepoUtils lendingRepoUtils;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetLendingRepo_existing_id() {
		final int id = 4;
		LendingRepo lendingRepo = this.lendingRepoService.getLendingRepo(id);
		Assertions.assertNotNull(lendingRepo);
		Assertions.assertEquals(id, lendingRepo.getId());
	}


	@Test
	public void testGetLendingRepo_existing_id_is_roll() {
		final int id = 12;
		final int expectedParentId = 10;
		LendingRepo lendingRepo = this.lendingRepoService.getLendingRepo(id);
		Assertions.assertNotNull(lendingRepo);
		Assertions.assertEquals(id, lendingRepo.getId());
		Assertions.assertTrue(lendingRepo.isRoll());
		Assertions.assertEquals(expectedParentId, lendingRepo.getParentIdentifier());
	}


	@Test
	public void testGetLendingRepo_existing_id_is_not_roll() {
		final int id = 3;
		LendingRepo lendingRepo = this.lendingRepoService.getLendingRepo(id);
		Assertions.assertNotNull(lendingRepo);
		Assertions.assertEquals(id, lendingRepo.getId());
		Assertions.assertFalse(lendingRepo.isRoll());
		Assertions.assertNull(lendingRepo.getParentIdentifier());
	}


	@Test
	public void testGetLendingRepoList_search_for_roll() {
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		SearchRestriction idRestriction = new SearchRestriction("id", ComparisonConditions.LESS_THAN, 7);
		searchForm.addSearchRestriction(idRestriction);
		searchForm.setRoll(true);

		Set<Integer> expectedIds = new HashSet<>();
		expectedIds.add(6);
		expectedIds.add(4);
		Set<Integer> lendingRepoSet = CollectionUtils.getStream(this.lendingRepoService.getLendingRepoList(searchForm)).map(LendingRepo::getId).collect(Collectors.toSet());

		Assertions.assertEquals(expectedIds, lendingRepoSet);
	}


	@Test
	public void testGetLendingRepoList_search_for_non_roll() {
		LendingRepoSearchForm searchForm = new LendingRepoSearchForm();
		SearchRestriction idRestriction = new SearchRestriction("id", ComparisonConditions.LESS_THAN, 7);
		searchForm.addSearchRestriction(idRestriction);
		searchForm.setRoll(false);

		Set<Integer> expectedIds = new HashSet<>();
		expectedIds.add(2);
		expectedIds.add(3);
		Set<Integer> lendingRepoSet = CollectionUtils.getStream(this.lendingRepoService.getLendingRepoList(searchForm)).map(LendingRepo::getId).collect(Collectors.toSet());

		Assertions.assertEquals(expectedIds, lendingRepoSet);
	}


	@Test
	public void testSaveLendingRepo() {
		LendingRepo lendingRepo = createNewLendingRepo(false);

		LendingRepo savedLendingRepo = this.lendingRepoService.saveLendingRepo(lendingRepo);
		Assertions.assertNotNull(savedLendingRepo);

		LendingRepo retrievedEntity = this.lendingRepoService.getLendingRepo(savedLendingRepo.getId());
		Assertions.assertNotNull(retrievedEntity);
		Assertions.assertEquals(savedLendingRepo.getId(), retrievedEntity.getId());
	}


	@Test
	public void testSaveLendingRepo_with_parent_identifier() {
		LendingRepo lendingRepo = createNewLendingRepo(true);

		LendingRepo savedLendingRepo = this.lendingRepoService.saveLendingRepo(lendingRepo);
		Assertions.assertNotNull(savedLendingRepo);
		final int expectedParentIdentifier = savedLendingRepo.getParentIdentifier();

		LendingRepo retrievedEntity = this.lendingRepoService.getLendingRepo(savedLendingRepo.getId());
		Assertions.assertNotNull(retrievedEntity);
		Assertions.assertEquals(savedLendingRepo.getId(), retrievedEntity.getId());
		Assertions.assertEquals(expectedParentIdentifier, retrievedEntity.getParentIdentifier());
		// Ensure it's not sitting in Draft Rolled
		Assertions.assertEquals("Invalid", retrievedEntity.getWorkflowState().getName());
	}


	@Test
	public void testCopyLendingRepo() {
		LendingRepo lendingRepo = createNewLendingRepo(false);

		LendingRepo savedLendingRepo = this.lendingRepoService.saveLendingRepo(lendingRepo);
		Assertions.assertNotNull(savedLendingRepo);

		LendingRepo copiedEntity = this.lendingRepoService.copyLendingRepo(savedLendingRepo.getId(), true);
		Assertions.assertNotNull(copiedEntity);
		Assertions.assertNotNull(copiedEntity.getId());
		Assertions.assertNotNull(copiedEntity.getRepoDefinition());
		Assertions.assertEquals(savedLendingRepo.getDescription(), copiedEntity.getDescription());
		Assertions.assertNotEquals(savedLendingRepo.getId(), copiedEntity.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private LendingRepo createNewLendingRepo(boolean roll) {
		InvestmentSecurity bond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("912828JE1");
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.DEUTSCHE_BANK_SECURITIES);

		LendingRepoBuilder lendingRepoBuilder = this.lendingRepoUtils.newOpenLendingRepoBuilder(bond, BigDecimal.valueOf(10000.00), new Date(), accountInfo);
		lendingRepoBuilder.dayCountConvention(DayCountConventions.ACTUAL_THREESIXTY.getAccrualString());
		lendingRepoBuilder.roll(roll);
		return lendingRepoBuilder.build();
	}
}
