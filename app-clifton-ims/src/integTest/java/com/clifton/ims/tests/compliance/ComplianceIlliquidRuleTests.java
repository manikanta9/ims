package com.clifton.ims.tests.compliance;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.AccountsCreationCommand;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.trade.TradeType;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;


public class ComplianceIlliquidRuleTests extends ComplianceRealTimeTradeTests {

	private static final Map<String, InvestmentSecurity> symbolToSecurityMap = new ConcurrentHashMap<>();

	@Resource
	private BusinessContractService businessContractService;
	@Resource
	private BusinessClientService businessClientService;
	@Resource
	private WorkflowDefinitionService workflowDefinitionService;


	@BeforeEach
	public void publishContracts() {
		Stream.of("RUY OTC 12/08/16 C1330.93", "RUY OTC 06/09/16 C1436.48", "RUY OTC 03/10/16 C1401.81")
				.map(symbol -> symbolToSecurityMap.computeIfAbsent(symbol, k -> this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(k)))
				.map(InvestmentSecurity::getBusinessContract)
				.forEach(contract -> {
					if (!contract.isActive()) {
						WorkflowState publishedState = this.workflowDefinitionService.getWorkflowStateByName(contract.getWorkflowState().getWorkflow().getId(), "Published");
						contract.setWorkflowState(publishedState);
						contract.setWorkflowStatus(publishedState.getStatus());
						RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.businessContractService.saveBusinessContract(contract), 2);
					}
					activateClientForContract(contract);
				});
	}


	private void activateClientForContract(BusinessContract contract) {
		BusinessClient client = this.businessClientService.getBusinessClientByCompany(contract.getCompany().getId());
		if (client.getTerminateDate() != null) {
			client.setTerminateDate(null);
			RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.businessClientService.saveBusinessClient(client), 1);
		}
	}


	@Test
	public void testIlliquidOtcOptions() {
		InvestmentSecurity option = symbolToSecurityMap.computeIfAbsent("RUY OTC 12/08/16 C1330.93", k -> this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(k));
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(AccountsCreationCommand.ofOtcSecurityAndCustodian(option, BusinessCompanies.NORTHERN_TRUST, this.options, InvestmentAccountUtils.RULE_DEFINITION_CONTRACT_APPROVED, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG));
		this.complianceUtils.createTradeAllRule(accountInfo.getClientAccount());
		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Max 15% in Illiquid Securities");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, option.getSymbol(), BigDecimal.ONE, true, new Date(), option.getBusinessCompany());
		validateViolationNotes(tradeResults, true, "Max 15\\% in Illiquid Securities : Net Assets: \\$[0-9,.]+ : Failed \\[Illiquid securities, Limit Value: 15.00000, Result: 100.000000000000000\\]");
	}


	@Test
	public void testIlliquidOptionsWithVesting() {
		// all options must be for the same client in the same client account (but can have different holding accounts)
		InvestmentSecurity option = symbolToSecurityMap.computeIfAbsent("RUY OTC 03/10/16 C1401.81", k -> this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(k));
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(AccountsCreationCommand.ofOtcSecurityAndCustodian(option, BusinessCompanies.NORTHERN_TRUST, this.options, InvestmentAccountUtils.RULE_DEFINITION_CONTRACT_APPROVED, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG));
		this.complianceUtils.createTradeAllRule(accountInfo.getClientAccount());
		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Max 15% in Illiquid Securities");

		Date today = new Date();
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, "EEM US 09/16/16 P24", new BigDecimal(90000), true, today, this.goldmanSachs);
		validateViolationNotes(tradeResults);

		accountInfo = this.investmentAccountUtils.createAccountsForTrading(AccountsCreationCommand.ofOtcSecurityAndClientAccount(option, accountInfo.getClientAccount(), this.options));
		this.complianceUtils.createTradeAllRule(accountInfo.getClientAccount());
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, option.getSymbol(), BigDecimal.ONE, true, today, option.getBusinessCompany());
		validateViolationNotes(tradeResults);

		option = symbolToSecurityMap.computeIfAbsent("RUY OTC 12/08/16 C1330.93", k -> this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(k));
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(AccountsCreationCommand.ofOtcSecurityAndClientAccount(option, accountInfo.getClientAccount(), this.options));
		this.complianceUtils.createTradeAllRule(accountInfo.getClientAccount());
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, option.getSymbol(), BigDecimal.ONE, true, today, option.getBusinessCompany());
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, option.getSymbol(), new BigDecimal(1500), true, today, option.getBusinessCompany());
		validateViolationNotes(tradeResults, true, "Max 15\\% in Illiquid Securities : Net Assets: \\$[0-9,.]+ : Failed \\[Illiquid securities, Limit Value: 15.00000, Result: [0-9.]+\\]");
	}
}
