package com.clifton.ims.tests.example;


import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.security.user.SecurityGroup;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SwitchUserExampleTest extends BaseImsIntegrationTest {

	private static final Logger log = LoggerFactory.getLogger(SwitchUserExampleTest.class);


	@Test
	public void test() {
		// Start with admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);

		// Which user do we want to add permissions to:
		String username = ImsTestProperties.TEST_USER_2;
		log.info("Adding " + username + " to the 'Accounting' SecurityGroup...");
		this.userUtils.addUserToSecurityGroup(username, SecurityGroup.ACCOUNTING);

		log.info("Removing " + username + " from the 'Accounting' SecurityGroup...");
		this.userUtils.removeUserFromSecurityGroup(username, SecurityGroup.ACCOUNTING);

		log.info("Giving " + username + " full permissions to the AccountingTransaction table");
		this.userUtils.addPermissionToUser(username, "AccountingTransaction");

		// Switch to log in as the test user
		this.userUtils.switchUser(username, ImsTestProperties.TEST_USER_2_PASSWORD);

		// Do your testing

		// Clean up - Switch Back to admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);

		// Clean Up All Security Permissions for the test user
		this.userUtils.removeAllPermissionsForUser(username);
	}
}
