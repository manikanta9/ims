package com.clifton.ims.tests.accounting.commission;

import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.AccountsCreationCommand;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class AccountingAmortizedCommissionTests extends BaseImsIntegrationTest {


	/**
	 * Opens a position in a Total Return Swap, generates and posts several reset events, then closes the position and verifies the commission (and the other entries in the GL).
	 * <p>
	 * This integration test assumes that Commission Definition 352 will be triggered for this total return swap; this commission definition is amortized over ranges based on the number of days since the
	 * opening of the position.
	 */
	@Test
	public void testAmortizedCommissionForTRS() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("EUK495317");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(AccountsCreationCommand.ofOtcSecurityAndCustodian(swap, BusinessCompanies.NORTHERN_TRUST, this.totalReturnSwaps));

		Date openDate = DateUtils.toDate("4/30/2014");
		Date closeDate = DateUtils.toDate("10/08/2014");

		//Open a position
		BusinessCompany executingBroker = swap.getBusinessCompany();
		Trade buy = this.tradeUtils
				.newTotalReturnSwapTradeBuilder(swap, new BigDecimal("12267.7238"), new BigDecimal("520.637"), true, openDate, executingBroker, accountInfo)
				.withSettlementDate(DateUtils.toDate("04/30/2014"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		List<? extends AccountingJournalDetailDefinition> actualList = this.accountingUtils.getFirstTradeFillDetails(buy, true);
		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"16125481	null	CA84222321	HA266635403	Position	EUK495317	520.637	12,267.7238	0	04/30/2014	1	0	6,387,030.92	04/30/2014	04/30/2014	Position opening of EUK495317"
		}, false);

		//Generate and post all the swap reset events
		this.accountingUtils.postOtcTrsSwapEventsForDateRange(swap, accountInfo.getClientAccount(), openDate, closeDate);

		//Close the position
		Trade sell = this.tradeUtils
				.newTotalReturnSwapTradeBuilder(swap, new BigDecimal("12267.7238"), new BigDecimal("520.637"), false, DateUtils.toDate("10/08/2014"), executingBroker, accountInfo)
				.withSettlementDate(DateUtils.toDate("10/08/2014"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		actualList = this.accountingUtils.getFirstTradeFillDetails(sell, true);
		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"16125532	16125529	CA84222321	HA266635403	Position	EUK495317	520.637	-12,267.7238	0	10/08/2014	1	0	-6,390,306.4	04/30/2014	10/08/2014	Full position close for EUK495317",
				"16125533	16125532	CA84222321	HA266635403	Interest Leg	EUK495317			1,273.09	10/08/2014	1	1,273.09	0	04/30/2014	10/08/2014	Accrued Interest Leg for EUK495317",
				"16125534	16125532	CA84222321	HA266635403	Realized Gain / Loss	EUK495317	520.637	12,267.7238	3,275.48	10/08/2014	1	3,275.48	0	04/30/2014	10/08/2014	Realized Gain / Loss loss for EUK495317",
				"16125535	16125532	CA84222321	HA266635403	Termination Fee	EUK495317	0.780956	12,267.7238	9,580.55	10/08/2014	1	9,580.55	0	04/30/2014	10/08/2014	Termination Fee expense for EUK495317",
				"16125536	16125532	CA84222321	HA1714793551	Cash	USD			-14,129.12	10/08/2014	1	-14,129.12	0	04/30/2014	10/08/2014	Cash expense from close of EUK495317"
		}, false);
	}
}
