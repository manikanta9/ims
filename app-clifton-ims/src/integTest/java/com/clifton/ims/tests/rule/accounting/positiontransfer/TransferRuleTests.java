package com.clifton.ims.tests.rule.accounting.positiontransfer;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitService;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleSearchForm;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Disabled
public class TransferRuleTests extends RuleTests {

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private ComplianceOldService complianceOldService;

	@Resource
	private CompliancePositionExchangeLimitService compliancePositionExchangeLimitService;

	@Resource
	private TradeService tradeService;


	private static AccountInfo fromAccount;
	private static AccountInfo toAccount;
	private static boolean testInitialized = false;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	@Override
	public void initializeTest() {
		if (!isTestInitialized()) {
			super.initializeTest();
			// Create new accounts
			fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
			toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);
			//Create trade all compliance rules for accounts
			this.complianceUtils.createTradeAllComplianceRule(fromAccount);
			this.complianceUtils.createTradeAllComplianceRule(toAccount);
			this.setTestInitialized(true);
		}
	}


	public boolean isTestInitialized() {
		return testInitialized;
	}


	public void setTestInitialized(boolean initialized) {
		testInitialized = initialized;
	}


	@Override
	public String getCategoryName() {
		return "Transfer Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		Set<String> excludedTestMethodSet = new HashSet<>();
		//Handled by ComplianceSystemRuleEvaluatorTests
		excludedTestMethodSet.add("testComplianceRealTimeRulesRule");
		return excludedTestMethodSet;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////

	@Resource
	private ComplianceRuleService complianceRuleService;
	@Resource
	private SystemBeanService systemBeanService;


	@Test
	public void testTransferRulesComplianceRealTimeRulesRule() {
		AccountInfo fromBondAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.bonds);
		AccountInfo toBondAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.bonds);
		this.complianceUtils.createTradeAllComplianceRule(fromBondAccount);
		this.complianceUtils.createTradeAllComplianceRule(toBondAccount);

		//Create underlying bean for allowing all trades for the given account
		SystemBean creditRatingBean;
		try {
			creditRatingBean = this.systemBeanService.getSystemBeanByName("AAA Credit Ratings");
		}
		catch (Exception e) {
			creditRatingBean = SystemBeanBuilder.newBuilder("AAA Credit Ratings", "Security Credit Rating Rule Evaluator", this.systemBeanService)
					.addProperty("Require All Ratings", "true")
					.addProperty("Fitch", "1")
					.addProperty("Moody's", "27")
					.addProperty("S&P", "52")
					.addProperty("Min Pass Count", "1")
					.addProperty("Cash Equivalent", "false")
					.addProperty("Cash Equivalent Threshold", "365")
					.build();
		}

		ComplianceRuleSearchForm searchForm = new ComplianceRuleSearchForm();
		searchForm.setName("AAA Credit Rating Rule");
		ComplianceRule creditRatingRule = CollectionUtils.getFirstElement(this.complianceRuleService.getComplianceRuleList(searchForm));
		if (creditRatingRule == null) {
			creditRatingRule = new ComplianceRule();
			creditRatingRule.setName("AAA Credit Rating Rule");
			creditRatingRule.setDescription("AAA Credit Rating Rule");
			creditRatingRule.setRuleEvaluatorBean(creditRatingBean);
			creditRatingRule.setRuleType(this.complianceRuleService.getComplianceRuleTypeByName("Company Credit Rating Rule"));
			creditRatingRule.setBatch(true);
			creditRatingRule.setRealTime(true);
			creditRatingRule.setCurrencyType(InvestmentCurrencyTypes.BOTH);
			this.complianceRuleService.saveComplianceRule(creditRatingRule);
		}

		//Assign trade all rule to the account
		ComplianceRuleAssignment complianceRuleAssignment = new ComplianceRuleAssignment();
		complianceRuleAssignment.setRule(creditRatingRule);
		complianceRuleAssignment.setClientInvestmentAccount(toBondAccount.getClientAccount());
		this.complianceRuleService.saveComplianceRuleAssignment(complianceRuleAssignment);

		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("36962G5Y6");

		this.complianceUtils.createApprovedContractsOldComplianceRule(fromBondAccount, security.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(toBondAccount, security.getInstrument());

		Trade buyTrade = this.tradeUtils.newBondTradeBuilder(security, new BigDecimal("100"), true, DateUtils.toDate("05/30/2017"), fromBondAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Get the position to transfer
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(fromBondAccount, security, new Date());
		Assertions.assertEquals(1, positionList.size());

		AccountingPositionTransfer transfer = this.accountingUtils.transferBetweenAccounts(fromBondAccount, toBondAccount, positionList.get(0), new BigDecimal("100"), new Date(), new Date(), false);

		boolean foundViolation = false;
		String expectedViolationNote = "AAA Credit Rating Rule : Security satisfies at least 1 of credit ratings [AAA (Fitch: long-term), Aaa (Moody's Corp: long-term), AAA (Standard & Poor's: long-term)] : Failed";
		List<RuleViolation> ruleViolationList = this.ruleViolationUtils.getRuleViolationList(TRANSFER_TABLE_NAME, transfer.getId());
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			if (ruleViolation.getViolationNote().contains(expectedViolationNote)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	@Test
	public void testTransferRulesClientApprovedContractsRule() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ5");

		// Create a position for 'fromAccount'
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("11/20/2015");
		BigDecimal positionSize = new BigDecimal("2000");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(security, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		//Get the position to transfer
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(fromAccount, security, openDate);
		Assertions.assertEquals(1, positionList.size());

		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, security.getInstrument());

		//Transfer the position to the other account
		Date transactionDate = DateUtils.toDate("11/20/2015");
		AccountingPositionTransfer transfer = this.accountingUtils.transferBetweenAccounts(fromAccount, toAccount, positionList.get(0), positionSize, transactionDate, openDate, false);

		boolean foundViolation = false;
		String expectedViolationNote = "Investment instrument S&P 500 Mini Futures (ES) (Equities / Futures / Index) is not approved for account " + toAccount.getHoldingAccount().getLabel();
		List<RuleViolation> ruleViolationList = this.ruleViolationUtils.getRuleViolationList(TRANSFER_TABLE_NAME, transfer.getId());
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			if (ruleViolation.getViolationNote().contains(expectedViolationNote)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	//TODO - lower the trade quantity on this; otherwise, this test in conjunction with others can cause a position limit failure for the security.
	@Test
	public void testTransferRulesNotionalLimitRule() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create a position for 'fromAccount'
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("12/16/2013");
		BigDecimal positionSize = new BigDecimal("1000");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(security, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		//Get the position to transfer
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(fromAccount, security, openDate);
		Assertions.assertEquals(1, positionList.size());

		ComplianceRuleOld complianceRuleOld = new ComplianceRuleOld();
		complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Notional Limit"));
		complianceRuleOld.setClientInvestmentAccount(toAccount.getClientAccount());
		complianceRuleOld.setInvestmentInstrument(security.getInstrument());
		complianceRuleOld.setMaxBuyQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxSellQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxBuyAccountingNotional(BigDecimal.valueOf(9900));
		complianceRuleOld.setMaxSellAccountingNotional(BigDecimal.valueOf(9900));
		complianceRuleOld.setIgnorable(true);
		this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, security.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, security.getInstrument());

		//Transfer the position to the other account
		Date transactionDate = DateUtils.toDate("12/17/2013");
		AccountingPositionTransfer transfer = this.accountingUtils.transferBetweenAccounts(fromAccount, toAccount, positionList.get(0), positionSize, transactionDate, openDate, false);

		boolean foundViolation = false;
		String expectedViolationNote = "The notional value of the trade <span class='amountPositive'>10,000.00</span> exceeds the buy limit of <span class='amountPositive'>9,900.00</span>";
		List<RuleViolation> ruleViolationList = this.ruleViolationUtils.getRuleViolationList(TRANSFER_TABLE_NAME, transfer.getId());
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			if (ruleViolation.getViolationNote().contains(expectedViolationNote)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	private InvestmentInstrument getInvestmentInstrument() {
		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName("S&P Midcap 400 Mini Futures (FA)");
		return CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));
	}


	private InvestmentSecurity getInvestmentSecurity(InvestmentInstrument instrument) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInstrumentId(instrument.getId());
		searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN, new Date()));
		searchForm.setOrderBy("endDate:desc");
		searchForm.setLimit(1);
		return CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));
	}
}
