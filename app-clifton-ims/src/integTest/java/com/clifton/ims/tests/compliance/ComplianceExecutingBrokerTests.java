package com.clifton.ims.tests.compliance;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class ComplianceExecutingBrokerTests extends ComplianceRealTimeTradeTests {


	@Test
	public void testExecutingBrokerExcludeRule() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, true, "Approved Executing Broker (Multi): Star V Partners (All Investments)");

		//Goldman Sachs not allowed
		BusinessCompany disallowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "088023EF9", new BigDecimal(123), true, new Date(), disallowedBroker);
		validateViolationNotes(tradeResults, "Approved Executing Broker (Multi): Star V Partners (All Investments) : Executing Broker [Goldman Sachs & Co.] is not approved as it is in the broker exclusion list. : Failed");

		//Bank of America is allowed
		BusinessCompany allowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "088023EF9", new BigDecimal(123), true, new Date(), allowedBroker);
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testExecutingBrokerExcludeRulePendingTrade() {
		//AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, true, "Approved Executing Broker (Multi): Star V Partners (All Investments)");
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, true);

		// First, create trade with allowed broker (so no violation) and fully execute, so that the client account holds a position to validate
		BusinessCompany allowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC);
		Trade validTrade = this.complianceUtils.buildComplianceTrade(accountInfo, TradeType.BONDS, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("64970KH83"), new BigDecimal(321), true, DateUtils.toDate("03/24/2020"), allowedBroker);
		this.tradeUtils.fullyExecuteTrade(validTrade);

		// Now, create pending trade with invalid broker
		BusinessCompany disallowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		this.complianceUtils.buildComplianceTrade(accountInfo, TradeType.BONDS, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("64970KH83"), new BigDecimal(123), true, DateUtils.toDate("03/24/2020"), disallowedBroker);
		// Trade is created but not executed, as such should be in pending state

		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Approved Executing Broker (Multi): Star V Partners (All Investments)");
		// Rule should still fail, even though the trade is pending
		Status complianceRunStatus = this.complianceUtils.rebuildComplianceRun(DateUtils.toDate("03/24/2020"), accountInfo.getClientAccount(), this.complianceUtils.getComplianceRuleByName("Approved Executing Broker (Multi): Star V Partners (All Investments)"));
		ValidationUtils.assertTrue(StringUtils.contains(CollectionUtils.getOnlyElement(complianceRunStatus.getDetailList()).getNote(), "Executing Broker Rule"), "Failed to validate expected warning");
	}


	@Test
	public void testExecutingBrokerInclusionRule() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, true, "Approved Executing Broker (Multi): Vanderbilt (All Investments)");

		//Goldman Sachs is allowed
		BusinessCompany allowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "088023EF9", new BigDecimal(123), true, new Date(), allowedBroker);
		validateViolationNotes(tradeResults);

		//Deustche Bank is not allowed
		BusinessCompany disallowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.DEUTSCHE_BANK_SECURITIES);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "088023EF9", new BigDecimal(123), true, new Date(), disallowedBroker);
		validateViolationNotes(tradeResults, "Approved Executing Broker (Multi): Vanderbilt (All Investments) : Executing Broker [Deutsche Bank Securities Inc.] is not approved in the broker inclusion list. : Failed");
	}


	@Test
	public void testExecutingBrokerUsesSpecificityScopeBatch() {
		AccountInfo accountInfo = this.complianceUtils.createComplianceClientAccountForTrading(TradeType.STOCKS, true);
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Trade trade1 = this.complianceUtils.buildComplianceTrade(accountInfo, TradeType.STOCKS, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL"), new BigDecimal(123), true, DateUtils.toDate("03/24/2020"), broker);
		this.tradeUtils.fullyExecuteTrade(trade1);
		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Approved Executing Broker (Multi): Star V Partners (All Investments)");
		Status complianceRunStatus = this.complianceUtils.rebuildComplianceRun(DateUtils.toDate("03/24/2020"), accountInfo.getClientAccount(), this.complianceUtils.getComplianceRuleByName("Approved Executing Broker (Multi): Star V Partners (All Investments)"));
		ValidationUtils.assertTrue(StringUtils.contains(CollectionUtils.getOnlyElement(complianceRunStatus.getDetailList()).getNote(), "Executing Broker Rule"), "Failed to validate expected warning");

		// Now, trying one with a different specificity scope - this rule only applies to central govt bonds, so it shouldn't generate an error due to the AAPL position
		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Approved Executing Broker (Multi): American Beacon (US Treasuries)");
		this.complianceUtils.removeComplianceRuleAssignment(accountInfo.getClientAccount(), "Approved Executing Broker (Multi): Star V Partners (All Investments)");
		Status complianceRunStatusUpdated = this.complianceUtils.rebuildComplianceRun(DateUtils.toDate("03/24/2020"), accountInfo.getClientAccount(), this.complianceUtils.getComplianceRuleByName("Approved Executing Broker (Multi): American Beacon (US Treasuries)"));
		// No new error
		ValidationUtils.assertEmpty(complianceRunStatusUpdated.getDetailList(), "Unexpected validation error for trade OUTSIDE specificity scope");
	}


	@Test
	public void testExecutingBrokerIgnoresCancelledTradesBatch() {
		AccountInfo accountInfo = this.complianceUtils.createComplianceClientAccountForTrading(TradeType.STOCKS, true);

		//First, create trade with invalid broker, and then cancel (so this trade won't be validated against)
		BusinessCompany disallowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Trade trade1 = this.complianceUtils.buildComplianceTrade(accountInfo, TradeType.STOCKS, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL"), new BigDecimal(123), true, DateUtils.toDate("03/24/2020"), disallowedBroker);
		this.tradeUtils.cancelTrade(trade1);

		// Then, create trade with allowed broker (so no violation) and fully execute, so that the client account holds a position to validate
		BusinessCompany allowedBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC);
		Trade trade2 = this.complianceUtils.buildComplianceTrade(accountInfo, TradeType.STOCKS, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL"), new BigDecimal(321), true, DateUtils.toDate("03/24/2020"), allowedBroker);
		this.tradeUtils.fullyExecuteTrade(trade2);

		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Approved Executing Broker (Multi): Star V Partners (All Investments)");
		Status complianceRunStatus = this.complianceUtils.rebuildComplianceRun(DateUtils.toDate("03/24/2020"), accountInfo.getClientAccount(), this.complianceUtils.getComplianceRuleByName("Approved Executing Broker (Multi): Star V Partners (All Investments)"));
		ValidationUtils.assertEmpty(complianceRunStatus.getDetailList(), "Unexpected validation error occurred for CANCELLED trade");
	}
}
