package com.clifton.ims.tests.core.dataaccess.dao;

import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * @author stevenf
 */
public class MaxQuerySizeLimitTests extends BaseImsIntegrationTest {

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	SystemSchemaService systemSchemaService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMaxQuerySizeLimitSearchForm() {
		SystemTable investmentSecurityTable = this.systemSchemaService.getSystemTableByName("InvestmentSecurity");
		investmentSecurityTable.setMaxQuerySizeLimit(100);
		investmentSecurityTable = this.systemSchemaService.saveSystemTable(investmentSecurityTable);

		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setMaxLimit(200);
		String exceptionMessage = null;
		try {
			this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm);
		}
		catch (Throwable e) {
			if (e instanceof ImsErrorResponseException) {
				exceptionMessage = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			if (exceptionMessage == null) {
				exceptionMessage = e.getMessage();
			}
		}
		Assertions.assertEquals("The size of the resultSet returned exceeds 100% of the maximum size of 200 defined by the SearchForm [InvestmentSecurity]", exceptionMessage);
		investmentSecurityTable.setMaxQuerySizeLimit(null);
		this.systemSchemaService.saveSystemTable(investmentSecurityTable);
	}


	@Test
	public void testMaxQuerySizeLimitSystemTable() {
		SystemTable investmentSecurityTable = this.systemSchemaService.getSystemTableByName("InvestmentSecurity");
		investmentSecurityTable.setMaxQuerySizeLimit(100);
		investmentSecurityTable = this.systemSchemaService.saveSystemTable(investmentSecurityTable);

		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		String exceptionMessage = null;
		try {
			this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm);
		}
		catch (Throwable e) {
			if (e instanceof ImsErrorResponseException) {
				exceptionMessage = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			if (exceptionMessage == null) {
				exceptionMessage = e.getMessage();
			}
		}
		Assertions.assertEquals("The size of the resultSet returned exceeds 100% of the maximum size of 100 defined by the SystemTable [InvestmentSecurity]", exceptionMessage);
		investmentSecurityTable.setMaxQuerySizeLimit(null);
		this.systemSchemaService.saveSystemTable(investmentSecurityTable);
	}


	// @Test
	// TODO - once paging command max limit is lowered, this test can be enabled again to test.
	// public void testMaxQuerySizeLimitPagingCommand() {
	// 	SecuritySearchForm securitySearchForm = new SecuritySearchForm();
	// 	String exceptionMessage = null;
	// 	try {
	// 		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm), 1);
	// 	}
	// 	catch (Throwable e) {
	// 		if (e.getCause() instanceof ImsErrorResponseException) {
	// 			exceptionMessage = ((ImsErrorResponseException) e.getCause()).getErrorMessageFromIMS();
	// 		}
	// 		if (exceptionMessage == null) {
	// 			exceptionMessage = e.getMessage();
	// 		}
	// 	}
	// 	Assertions.assertEquals("The size of the resultSet returned exceeds 100% of the maximum size of " + NumberFormat.getIntegerInstance().format(PagingCommand.MAX_LIMIT) + " defined by the PagingCommand Max Limit [InvestmentSecurity]", exceptionMessage);
	// }
}
