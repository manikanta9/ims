package com.clifton.ims.tests.investment.instrument.copy;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instruction.delivery.InvestmentDeliveryMonthCodes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.copy.InvestmentSecurityCopyCommand;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author NickK
 */
public class InvestmentInstrumentCopyServiceImplTests extends BaseImsIntegrationTest {

	// Following copied from InvestmentInstrumentCopyServiceImpl
	private static final Pattern OPTION_SYMBOL_STRIKE_PATTERN = Pattern.compile("[\\w\\s\\d](((?:[CP]|Put[s]?|Call[s]?)\\s{0,2})(\\d{1,5}(?:\\.\\d{0,3})?)|(\\b\\d{1,5}(?:\\.\\d{0,3})?)(\\s{0,2}(?:[CP]|Call[s]?|Put[s]?)))(?:\\b[-\\w()\\s]+)?$");
	private static final Pattern OPTION_SYMBOL_DATE_PATTERN = Pattern.compile(".+\\b((\\d{1,2}/\\d{1,2}/(?:\\d{4}|\\d{2}))|(\\d{2}\\d{2}\\d{4})|((?:Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun[e]?|Jul[y]?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|(?:Nov|Dec)(?:ember)?)\\s\\d{4}))\\b.+");

	@Resource
	private InvestmentInstrumentCopyService investmentInstrumentCopyService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	/////////////////////////////////////////////////////////////////////
	// Copy Option Tests (a.k.a. Create From Template)
	/////////////////////////////////////////////////////////////////////


	@Test
	public void testCreateOptionFromTemplate() {
		testCreateOptionFromTemplateImpl("SPXW US 09/12/14 P1890", BigDecimal.valueOf(1895), "SPXW US 09/12/14 P1895");
		testCreateOptionFromTemplateImpl("SPX OTC 12/08/14 C1953.37", BigDecimal.valueOf(1953), "SPX OTC 12/08/14 C1953.00");
		testCreateOptionFromTemplateImpl("VXX OTC 11/22/14 P25", BigDecimal.valueOf(27.705), "VXX OTC 11/22/14 P27.705");
		// Big Security Is also an Option and should also be copied from original security with new strike price if it doesn't exist
		testCreateOptionFromTemplateImpl("SCG5P 1850", BigDecimal.valueOf(1855), "SCG5P 1855");
		// Has OCC Symbol that needs to have strike price replaced
		testCreateOptionFromTemplateImpl("SPXW US 01/18/17 P2250", BigDecimal.valueOf(2250.5), "SPXW US 01/18/17 P2250.5");
		testCreateOptionFromTemplateImpl("SPXW US 01/18/17 P2250", BigDecimal.valueOf(2251), "SPXW US 01/18/17 P2251");
		testCreateOptionFromTemplateImpl("EEM US 01/06/17 C37.5", BigDecimal.valueOf(38), "EEM US 01/06/17 C38");
		testCreateOptionFromTemplateImpl("EEM US 01/06/17 C37.5", BigDecimal.valueOf(37.6), "EEM US 01/06/17 C37.6");
		// FLEX
		testCreateOptionFromTemplateImpl("IR FLEX 08/01/19 C131.00", BigDecimal.valueOf(132), "IR FLEX 08/01/19 C132.00");
		testCreateOptionFromTemplateImpl("RUT FLEX 06/06/19 P1584.39", BigDecimal.valueOf(1584.4), "RUT FLEX 06/06/19 P1584.40");
		testCreateOptionFromTemplateImpl("EFA FLEX 05/24/19 P59.75", BigDecimal.valueOf(60), "EFA FLEX 05/24/19 P60.00");
		testCreateOptionFromTemplateImpl("EFA FLEX 05/24/19 P59.75", BigDecimal.valueOf(60.25), "EFA FLEX 05/24/19 P60.25");
		// OTC
		testCreateOptionFromTemplateImpl("3690 12302021 300.00P", BigDecimal.valueOf(325.00), "3690 12302021 325.00P");
		testCreateOptionFromTemplateImpl("3690 12302021 300.00P", BigDecimal.valueOf(375.00), DateUtils.toDate("04/23/2021"), "3690 04232021 375.00P");
		// New Expiration Date
		testCreateOptionFromTemplateImpl("AAPL US 05/17/19 C190", BigDecimal.valueOf(195), DateUtils.toDate("06/14/2019"), "AAPL US 06/14/19 C195");
		testCreateOptionFromTemplateImpl("3DK9P 2850", BigDecimal.valueOf(2855), DateUtils.toDate("06/14/2019"), "3DK9P 2855");
	}


	@Test
	public void testCreateOptionFromTemplateWithTypeOverride() {
		testCreateOptionFromTemplateImpl("SPXW US 09/12/14 P1890", BigDecimal.valueOf(1895), InvestmentSecurityOptionTypes.CALL, "SPXW US 09/12/14 C1895");
		testCreateOptionFromTemplateImpl("SPX OTC 12/08/14 C1953.37", BigDecimal.valueOf(1953), InvestmentSecurityOptionTypes.PUT, "SPX OTC 12/08/14 P1953.00");
		testCreateOptionFromTemplateImpl("VXX OTC 11/22/14 P25", BigDecimal.valueOf(27.705), InvestmentSecurityOptionTypes.CALL, "VXX OTC 11/22/14 C27.705");
		// Big Security Is also an Option and should also be copied from original security with new strike price if it doesn't exist
		testCreateOptionFromTemplateImpl("SCG5P 1850", BigDecimal.valueOf(1855), InvestmentSecurityOptionTypes.CALL, "SCG5C 1855");
		// Has OCC Symbol that needs to have strike price replaced
		testCreateOptionFromTemplateImpl("SPXW US 01/18/17 P2250", BigDecimal.valueOf(2250.5), InvestmentSecurityOptionTypes.CALL, "SPXW US 01/18/17 C2250.5");
		testCreateOptionFromTemplateImpl("SPXW US 01/18/17 P2250", BigDecimal.valueOf(2251), InvestmentSecurityOptionTypes.CALL, "SPXW US 01/18/17 C2251");
		testCreateOptionFromTemplateImpl("EEM US 01/06/17 C37.5", BigDecimal.valueOf(38), InvestmentSecurityOptionTypes.PUT, "EEM US 01/06/17 P38");
		testCreateOptionFromTemplateImpl("EEM US 01/06/17 C37.5", BigDecimal.valueOf(37.6), InvestmentSecurityOptionTypes.PUT, "EEM US 01/06/17 P37.6");
		// FLEX
		testCreateOptionFromTemplateImpl("IR FLEX 08/01/19 C131.00", BigDecimal.valueOf(132), InvestmentSecurityOptionTypes.PUT, "IR FLEX 08/01/19 P132.00");
		testCreateOptionFromTemplateImpl("RUT FLEX 06/06/19 P1584.39", BigDecimal.valueOf(1584.4), InvestmentSecurityOptionTypes.CALL, "RUT FLEX 06/06/19 C1584.40");
		testCreateOptionFromTemplateImpl("EFA FLEX 05/24/19 P59.75", BigDecimal.valueOf(60), InvestmentSecurityOptionTypes.CALL, "EFA FLEX 05/24/19 C60.00");
		testCreateOptionFromTemplateImpl("EFA FLEX 05/24/19 P59.75", BigDecimal.valueOf(60.25), InvestmentSecurityOptionTypes.CALL, "EFA FLEX 05/24/19 C60.25");
		// OTC
		testCreateOptionFromTemplateImpl("3690 12302021 300.00P", BigDecimal.valueOf(325.00), InvestmentSecurityOptionTypes.CALL, "3690 12302021 325.00C");
		testCreateOptionFromTemplateImpl("3690 12302021 300.00P", BigDecimal.valueOf(375.00), DateUtils.toDate("04/23/2021"), InvestmentSecurityOptionTypes.CALL, "3690 04232021 375.00C");
		// New Expiration Date
		testCreateOptionFromTemplateImpl("AAPL US 05/17/19 C190", BigDecimal.valueOf(195), DateUtils.toDate("06/14/2019"), InvestmentSecurityOptionTypes.PUT, "AAPL US 06/14/19 P195");
		testCreateOptionFromTemplateImpl("3DK9P 2850", BigDecimal.valueOf(2855), DateUtils.toDate("06/14/2019"), InvestmentSecurityOptionTypes.CALL, "3DK9C 2855");
	}


	@Test
	public void testCreateOptionOtcFromListedTemplate() {
		// Listed Option to OTC Listed Look-alike
		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setInvestmentType(InvestmentType.OPTIONS);
		instrumentSearchForm.setInvestmentTypeSubType(InvestmentTypeSubType.OPTIONS_FUNDS);
		instrumentSearchForm.setInvestmentTypeSubType2(InvestmentTypeSubType2.OPTIONS_LOOK_ALIKE);
		instrumentSearchForm.setIdentifierPrefix("LO-EEM100");
		List<InvestmentInstrument> instrumentList = this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(instrumentList), "Only expected one Instrument for [type: " + InvestmentType.OPTIONS + ", subtype: " + InvestmentTypeSubType.OPTIONS_FUNDS + ", subtype2: " + InvestmentTypeSubType2.OPTIONS_LOOK_ALIKE + "]");

		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("800025");
		testCreateOptionFromTemplateImpl("EEM US 03/18/16 C48", BigDecimal.valueOf(49), null, null, CollectionUtils.getFirstElementStrict(instrumentList), clientAccount, "EEM 03/18/16 C49 (PGDE-BANA)");
	}


	@Test
	public void testCreateOptionWithDifferentNonOtcInstrumentFails() {
		// Listed Option to OTC Listed Look-alike
		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setInvestmentType(InvestmentType.OPTIONS);
		instrumentSearchForm.setInvestmentTypeSubType(InvestmentTypeSubType.OPTIONS_FUNDS);
		instrumentSearchForm.setInvestmentTypeSubType2(InvestmentTypeSubType2.OPTIONS_FLEX);
		instrumentSearchForm.setIdentifierPrefix("FO-EEM");
		List<InvestmentInstrument> instrumentList = this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm);
		Assertions.assertEquals(1, CollectionUtils.getSize(instrumentList), "Only expected one Instrument for [type: " + InvestmentType.OPTIONS + ", subtype: " + InvestmentTypeSubType.OPTIONS_FUNDS + ", subtype2: " + InvestmentTypeSubType2.OPTIONS_LOOK_ALIKE + "]");

		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("800025");
		testCreateOptionFromTemplateImpl("EEM US 03/18/16 C48", BigDecimal.valueOf(49), null, null, CollectionUtils.getFirstElementStrict(instrumentList), clientAccount, "EEM US 03/18/16 C49 (PGDE-BANA)");
	}


	private void testCreateOptionFromTemplateImpl(String optionSymbol, BigDecimal strikePrice, InvestmentSecurityOptionTypes optionType, String expectedNewSymbol) {
		testCreateOptionFromTemplateImpl(optionSymbol, strikePrice, null, optionType, null, null, expectedNewSymbol);
	}


	private void testCreateOptionFromTemplateImpl(String optionSymbol, BigDecimal strikePrice, String expectedNewSymbol) {
		testCreateOptionFromTemplateImpl(optionSymbol, strikePrice, null, null, null, null, expectedNewSymbol);
	}


	private void testCreateOptionFromTemplateImpl(String optionSymbol, BigDecimal strikePrice, Date expirationDate, InvestmentSecurityOptionTypes optionType, String expectedNewSymbol) {
		testCreateOptionFromTemplateImpl(optionSymbol, strikePrice, expirationDate, optionType, null, null, expectedNewSymbol);
	}


	private void testCreateOptionFromTemplateImpl(String optionSymbol, BigDecimal strikePrice, Date expirationDate, String expectedNewSymbol) {
		testCreateOptionFromTemplateImpl(optionSymbol, strikePrice, expirationDate, null, null, null, expectedNewSymbol);
	}


	private void testCreateOptionFromTemplateImpl(String optionSymbol, BigDecimal strikePrice, Date expirationDate, InvestmentSecurityOptionTypes optionType, InvestmentInstrument newInstrument, InvestmentAccount clientAccount, String expectedNewSymbol) {
		InvestmentSecurity optionSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(optionSymbol);
		ValidationUtils.assertNotNull(optionSecurity, "Missing Option with symbol [" + optionSymbol + "].");

		InvestmentSecurityCopyCommand command = new InvestmentSecurityCopyCommand();
		command.setSecurityToCopy(optionSecurity);
		command.setStrikePrice(strikePrice);
		command.setExpirationDate(expirationDate);
		command.setOptionType(optionType);
		command.setReturnExisting(false);
		command.setInstrument(newInstrument);
		command.setClientAccount(clientAccount);
		if (clientAccount != null) {
			command.setCounterparty(getClientAccountCounterParty(clientAccount));
		}
		String expectedException = null;
		if (newInstrument != null && !newInstrument.getHierarchy().isOtc()) {
			InvestmentInstrumentHierarchy newHierarchy = newInstrument.getHierarchy();
			InvestmentInstrumentHierarchy templateHierarchy = optionSecurity.getInstrument().getHierarchy();
			expectedException = new StringBuilder("Error was returned from IMS: [Can only create a new security from a template when the new security and template use the same investment instrument ")
					.append("or the new security's instrument is based on an OTC hierarchy with the same investment type and investment subtype2 as ")
					.append("the templates instrument's hierarchy.\nNew Instrument's Hierarchy [otc: ").append(newHierarchy.isOtc())
					.append(", investmentType: ").append(newHierarchy.getInvestmentType())
					.append(", investmentTypeSubType: ").append(newHierarchy.getInvestmentTypeSubType())
					.append(", investmentTypeSubType2: ").append(newHierarchy.getInvestmentTypeSubType2()).append("]\n")
					.append("Templates Instrument's Hierarchy [otc: ").append(templateHierarchy.isOtc())
					.append(", investmentType: ").append(templateHierarchy.getInvestmentType())
					.append(", investmentTypeSubType: ").append(templateHierarchy.getInvestmentTypeSubType())
					.append(", investmentTypeSubType2: ").append(templateHierarchy.getInvestmentTypeSubType2()).append("]]").toString();
		}
		Runnable action = () -> {
			InvestmentSecurity newOptionSecurity = this.investmentInstrumentCopyService.saveInvestmentSecurityOptionWithCommand(command);

			InvestmentSecurity bigSecurity = optionSecurity.getBigSecurity();
			boolean bigSecurityIsOption = false;
			if (bigSecurity != null && InvestmentUtils.isSecurityOfType(bigSecurity, InvestmentType.OPTIONS)) {
				bigSecurityIsOption = true;
			}

			try {
				if (bigSecurityIsOption && optionSecurity.getBigSecurity() != null && newOptionSecurity.getBigSecurity() != null) {
					validateCopiedOption(optionSecurity.getBigSecurity(), newOptionSecurity.getBigSecurity(), strikePrice, null, optionType, clientAccount);
				}
				validateCopiedOption(optionSecurity, newOptionSecurity, strikePrice, expirationDate, optionType, clientAccount);
				Assertions.assertEquals(expectedNewSymbol, newOptionSecurity.getSymbol());
			}
			finally {
				// Delete the new option so we can re-run test
				this.investmentInstrumentService.deleteInvestmentSecurity(newOptionSecurity.getId());
				if (bigSecurityIsOption && newOptionSecurity.getBigSecurity() != null) {
					this.investmentInstrumentService.deleteInvestmentSecurity(newOptionSecurity.getBigSecurity().getId());
				}
			}
		};
		if (expectedException != null) {
			TestUtils.expectException(ImsErrorResponseException.class, action::run, expectedException);
		}
		else {
			action.run();
		}
	}


	private BusinessCompany getClientAccountCounterParty(InvestmentAccount clientAccount) {
		CompanySearchForm companySearchForm = new CompanySearchForm();
		companySearchForm.setContractCompanyId(clientAccount.getBusinessClient().getCompany().getId());
		companySearchForm.setContractTypeName(BusinessContractTypes.ISDA.getName());
		companySearchForm.setContractPartyRoleName(BusinessContractPartyRole.PARTY_ROLE_COUNTERPARTY_NAME);
		List<BusinessCompany> companyList = this.businessCompanyService.getBusinessCompanyList(companySearchForm);
		Assertions.assertTrue(CollectionUtils.getSize(companyList) > 0, "Unable to find Counterparty for client account [" + clientAccount + "]");
		return CollectionUtils.getFirstElement(companyList);
	}


	private void validateCopiedOption(InvestmentSecurity optionSecurity, InvestmentSecurity newOptionSecurity, BigDecimal strikePrice, Date expirationDate, InvestmentSecurityOptionTypes optionType, InvestmentAccount clientAccount) {
		Assertions.assertTrue(MathUtils.isEqual(strikePrice, newOptionSecurity.getOptionStrikePrice()), "Strike prices not equal: [" + optionSecurity.getOptionStrikePrice() + "] and [" + newOptionSecurity.getOptionStrikePrice() + "]");
		Assertions.assertEquals(ObjectUtils.coalesce(optionType, optionSecurity.getOptionType()), newOptionSecurity.getOptionType(), "Option Types do not match: [" + ObjectUtils.coalesce(optionType, optionSecurity.getOptionType()) + "] and [" + newOptionSecurity.getOptionType() + "]");
		Assertions.assertEquals(optionSecurity.getSettlementExpiryTime(), newOptionSecurity.getSettlementExpiryTime(), "Option Expiry times do not match: [" + optionSecurity.getSettlementExpiryTime() + "] and [" + newOptionSecurity.getSettlementExpiryTime() + "]");
		Assertions.assertEquals(optionSecurity.getOptionStyle(), newOptionSecurity.getOptionStyle(), "Option Styles do not match: [" + optionSecurity.getOptionStyle() + "] and [" + newOptionSecurity.getOptionStyle() + "]");

		// Check Custom Fields, only change should be to Strike Price
		List<SystemColumnValue> optionValues = getInvestmentSecurityCustomColumnValueList(optionSecurity);
		List<SystemColumnValue> newOptionValues = getInvestmentSecurityCustomColumnValueList(newOptionSecurity);
		ValidationUtils.assertEquals(optionValues.size(), newOptionValues.size(), "Custom Field Values should be the same size.  Original option has [" + optionValues.size()
				+ "], and new Option has [" + newOptionValues.size() + "] values.");
		for (SystemColumnValue cv : optionValues) {
			boolean found = false;
			for (SystemColumnValue ncv : newOptionValues) {
				if (cv.getColumn().getName().equals(ncv.getColumn().getName())) {
					found = true;
					Assertions.assertEquals(cv.getValue(), ncv.getValue());
					Assertions.assertEquals(cv.getText(), ncv.getText());
					break;
				}
			}
			if (!found) {
				throw new ValidationException("Couldn't find column [" + cv.getColumn().getName() + "] defined on new Option.");
			}
		}

		// Copy command for validation
		InvestmentSecurityCopyCommand command = new InvestmentSecurityCopyCommand();
		command.setSecurityToCopy(optionSecurity);
		command.setStrikePrice(strikePrice);
		command.setExpirationDate(expirationDate);
		command.setOptionType(optionType);
		command.setInstrument(newOptionSecurity.getInstrument());
		command.setCounterparty(newOptionSecurity.getBusinessCompany());
		command.setClientAccount(clientAccount);
		command.setReturnExisting(true);
		// Validate for Symbol, Name, Description, and OCC Symbol Strike price changed
		String[] changeStrikePriceFields = new String[]{"symbol", "name", "occSymbol", "optionStrikePrice"};
		validateChangeInFields(expirationDate, optionSecurity, newOptionSecurity, changeStrikePriceFields, command);

		// All other fields should be the same
		boolean otcConversion = command.getInstrument() != null && !command.getInstrument().equals(optionSecurity.getInstrument()) && command.getInstrument().getHierarchy().isOtc();
		Set<String> excludeFields = CollectionUtils.createHashSet("symbol", "name", "description", "occSymbol", "label", "labelWithBigSecurity", "id", "optionType", "putOption", "callOption",
				"identity", "columnGroupName", "columnValueList", "uniqueLabel", "cusip", "bigSecurity", "referenceSecurity", "optionStrikePrice", "figi");
		if (otcConversion) {
			excludeFields.addAll(CollectionUtils.createHashSet("businessCompany", "instrument", "illiquid", "businessContract"));
		}
		if (expirationDate != null) {
			excludeFields.addAll(CollectionUtils.createHashSet("endDate", "firstNoticeDateOrEndDate", "lastPositionDate", "lastDeliveryDate", "firstDeliveryDate", "lastDeliveryOrEndDate", "startDate", "active"));
		}
		List<String> notEqualFields = CoreCompareUtils.getNoEqualProperties(optionSecurity, newOptionSecurity, false, excludeFields.toArray(new String[0]));
		ValidationUtils.assertEmpty(notEqualFields, "The following fields changed on the new option, but should not have: " + StringUtils.collectionToCommaDelimitedString(notEqualFields));

		// Try to create again - set option to return existing
		InvestmentSecurity dupeOptionSecurity = this.investmentInstrumentCopyService.saveInvestmentSecurityOptionWithCommand(command);
		Assertions.assertEquals(newOptionSecurity, dupeOptionSecurity);

		// Try again, this time don't set option to use existing - should fail
		String errorMessage = null;
		try {
			command.setReturnExisting(false);
			this.investmentInstrumentCopyService.saveInvestmentSecurityOptionWithCommand(command);
		}
		catch (ImsErrorResponseException e) {
			errorMessage = e.getErrorMessageFromIMS();
		}

		Assertions.assertTrue(errorMessage != null && errorMessage.endsWith("Security with Symbol [" + newOptionSecurity.getSymbol() + "] already exists."));
	}


	private void validateChangeInFields(Date newExpiration, InvestmentSecurity option, InvestmentSecurity newOption, String[] fields, InvestmentSecurityCopyCommand command) {
		boolean otcConversion = command.getInstrument() != null && !command.getInstrument().equals(option.getInstrument()) && command.getInstrument().getHierarchy().isOtc();
		String otcSuffix = !otcConversion ? "" :
				new StringBuilder(" (")
						.append(command.getClientAccount().getBusinessClient().getCompany().getAbbreviation())
						.append('-')
						.append(command.getCounterparty().getAbbreviation())
						.append(')').toString();
		String decimalFormat = InvestmentUtils.isListedOption(newOption) ? "#0.###" : "#0.00#";
		String originalStrikePriceString = getStrikePriceFromSecurityProperty(option, "symbol");
		String newStrikePriceString = convertStrikePriceToString(newOption.getOptionStrikePrice(), originalStrikePriceString.contains("."), decimalFormat);

		// validation of put/call from template need special treatment
		boolean optionTypeChange = command.getOptionType() != null && option.getOptionType() != command.getOptionType();

		for (String field : fields) {
			String value;
			if ("optionStrikePrice".equals(field)) {
				BigDecimal strikePrice = (BigDecimal) BeanUtils.getPropertyValue(option, field);
				value = convertStrikePriceToString(strikePrice, originalStrikePriceString.contains("."), decimalFormat);
			}
			else {
				value = (String) BeanUtils.getPropertyValue(option, field);
			}
			if (!StringUtils.isEmpty(value)) {
				String newValue;
				if ("optionStrikePrice".equals(field)) {
					BigDecimal strikePrice = (BigDecimal) BeanUtils.getPropertyValue(newOption, field);
					newValue = convertStrikePriceToString(strikePrice, newStrikePriceString.contains("."), decimalFormat);
				}
				else {
					newValue = (String) BeanUtils.getPropertyValue(newOption, field);
				}

				String expectedNewValue;
				if (otcConversion && ("name".equals(field) || "symbol".equals(field))) {
					String occString = "symbol".equals(field)
							? new StringBuilder(option.getSymbol().replace(" US", "")).append(otcSuffix).toString()
							: new StringBuilder(option.getName().replace(" US", "")).insert(option.getName().indexOf(" "), " OTC").append(otcSuffix).toString();
					expectedNewValue = StringUtils.replace(occString, originalStrikePriceString, newStrikePriceString);
				}
				else if (otcConversion && "label".equals(field)) {
					String symbol = StringUtils.replace(
							new StringBuilder(option.getSymbol().replace(" US", "")).append(otcSuffix).toString(),
							originalStrikePriceString, newStrikePriceString);
					String name = StringUtils.replace(
							new StringBuilder(option.getName().replace(" US", "")).insert(option.getName().indexOf(" "), " OTC").append(otcSuffix).toString(),
							originalStrikePriceString, newStrikePriceString);
					expectedNewValue = symbol + " (" + name + ')';
				}
				else if ("occSymbol".equals(field)) {
					String newOccStrikeString = getOptionOccStrikePriceString(newStrikePriceString);
					expectedNewValue = StringUtils.replace(value, getOptionOccStrikePriceString(originalStrikePriceString), newOccStrikeString);
					if (optionTypeChange) {
						char existingOptionTypeFirstChar = option.getOccSymbol().charAt(12);
						char newOptionTypeFirstChar = newOption.getOccSymbol().charAt(12);
						expectedNewValue = StringUtils.replace(expectedNewValue, existingOptionTypeFirstChar + newOccStrikeString, newOptionTypeFirstChar + newOccStrikeString);
					}
				}
				else {
					if ("name".equals(field)) {
						// fetch strike price from original field since it can differ from that of the symbol
						originalStrikePriceString = getStrikePriceFromSecurityProperty(option, field);
					}
					expectedNewValue = StringUtils.replace(value, originalStrikePriceString, newStrikePriceString);
					if (optionTypeChange && ("name".equals(field) || "symbol".equals(field))) {
						expectedNewValue = getExpectedPutCallStringFromSecurityProperty(expectedNewValue, command.getOptionType());
					}
				}

				expectedNewValue = applyOptionExpirationDateUpdate(field, expectedNewValue, option, newExpiration);
				ValidationUtils.assertEquals(expectedNewValue, newValue, "Expected " + field + " to be [" + expectedNewValue + "] but was [" + newValue + "]");
			}
		}
	}


	private String getStrikePriceFromSecurityProperty(InvestmentSecurity option, String property) {
		String propertyValue = (String) BeanUtils.getPropertyValue(option, property);
		Matcher strikeMatcher = OPTION_SYMBOL_STRIKE_PATTERN.matcher(propertyValue);
		Assertions.assertTrue(strikeMatcher.find(), "Unable to find strike price in " + property + ": " + propertyValue);
		return StringUtils.isEmpty(strikeMatcher.group(3)) ? strikeMatcher.group(4) : strikeMatcher.group(3);
	}


	private String getExpectedPutCallStringFromSecurityProperty(String expectedPropertyValue, InvestmentSecurityOptionTypes optionType) {
		Matcher matcher = OPTION_SYMBOL_STRIKE_PATTERN.matcher(expectedPropertyValue);
		ValidationUtils.assertTrue(matcher.find(), "Unable to resolve Strike Price from expected value " + expectedPropertyValue);

		String currentSymbolStrikeString = matcher.group(1); // e.g. P122.50, C2850, P 165.50, 300 Puts
		String currentPutCallPrefix = StringUtils.isEmpty(matcher.group(2)) ? "" : matcher.group(2); // e.g. P, C, Put[s], Call[s] (prefix)
		String originalStrikePriceString = StringUtils.isEmpty(matcher.group(3)) ? matcher.group(4) : matcher.group(3); // e.g. 122.50, 2850, 165.50
		String currentPutCallSuffix = StringUtils.isEmpty(matcher.group(5)) ? "" : matcher.group(5); // e.g. P, C, Put[s], Call[s] (suffix)

		String newSymbolStrikeString = getOptionTypeValueForExistingProperty(optionType, currentPutCallPrefix) + originalStrikePriceString + getOptionTypeValueForExistingProperty(optionType, currentPutCallSuffix);
		return StringUtils.replace(expectedPropertyValue, currentSymbolStrikeString, newSymbolStrikeString);
	}


	private String getOptionTypeValueForExistingProperty(InvestmentSecurityOptionTypes optionType, String existingValue) {
		if (StringUtils.isEmpty(existingValue)) {
			return existingValue;
		}

		String newTypeString = optionType.name();
		char newTypeValueFirstChar = newTypeString.charAt(0);
		// trim leading and trailing spaces that may be on the existing value.
		String trimmedExistingValue = StringUtils.trim(existingValue);
		char existingTypeValueFirstChar = trimmedExistingValue.charAt(0);

		if (newTypeValueFirstChar == existingTypeValueFirstChar) {
			return existingValue;
		}

		StringBuilder newValue = new StringBuilder(Character.toString(newTypeValueFirstChar));
		for (int i = 1; i < trimmedExistingValue.length(); i++) {
			if (i < newTypeString.length()) {
				newValue.append(Character.toLowerCase(newTypeString.charAt(i)));
			}
			else {
				// append s for Puts or Calls
				newValue.append('s');
				break;
			}
		}
		int trimmedExistingLength = trimmedExistingValue.length();
		int existingLength = existingValue.length();
		if (trimmedExistingLength != existingLength) {
			// add spaces that may be in the regex group
			if (existingValue.charAt(0) == existingTypeValueFirstChar) {
				// add spaces to end
				newValue.append(existingValue.substring(trimmedExistingLength));
			}
			else {
				// add spaces to beginning
				newValue.insert(0, existingValue.substring(0, existingLength - trimmedExistingLength));
			}
		}
		return newValue.toString();
	}


	private String convertStrikePriceToString(BigDecimal strikePrice, boolean format, String decimalFormat) {
		if (strikePrice == null) {
			return "";
		}

		String strikePriceString = strikePrice.stripTrailingZeros().toPlainString();
		int decimalIndex = strikePriceString.indexOf('.');
		if (decimalIndex > -1 || format) {
			return CoreMathUtils.formatNumber(strikePrice, decimalFormat);
		}
		return strikePriceString;
	}


	private List<SystemColumnValue> getInvestmentSecurityCustomColumnValueList(InvestmentSecurity security) {
		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setFkFieldId(security.getId());
		return this.systemColumnValueService.getSystemColumnValueList(searchForm);
	}


	// This is copied from InvestmentUtils to validate independently of IMS
	private String getOptionOccStrikePriceString(String strikeString) {
		int decimalIndex = strikeString.indexOf(".");
		String explicitStrike, decimal;
		if (decimalIndex > -1) {
			explicitStrike = strikeString.substring(0, decimalIndex);
			decimal = strikeString.substring(decimalIndex + 1);
		}
		else {
			explicitStrike = strikeString;
			decimal = StringUtils.EMPTY_STRING;
		}
		StringBuilder occStrikeStringBuilder = new StringBuilder(explicitStrike).append(decimal);
		int prefixingZeros = 5 - explicitStrike.length();
		for (int i = 0; i < prefixingZeros; i++) {
			occStrikeStringBuilder.insert(0, '0');
		}
		int suffixingZeros = 3 - decimal.length();
		for (int i = 0; i < suffixingZeros; i++) {
			occStrikeStringBuilder.append('0');
		}
		return occStrikeStringBuilder.toString();
	}


	private String applyOptionExpirationDateUpdate(String field, String value, InvestmentSecurity copiedOption, Date expirationDate) {
		if (expirationDate == null || StringUtils.isEmpty(value)) {
			return value;
		}

		int year = DateUtils.getYear(expirationDate);
		int month = DateUtils.getMonthOfYear(expirationDate);
		int day = DateUtils.getDayOfMonth(expirationDate);

		if ("occSymbol".equals(field)) {
			return value.replaceFirst("\\d{6}([CP])", Integer.toString(year).substring(2) + ((month < 10 ? "0" : "") + month) + ((day < 10 ? "0" : "") + day) + "$1");
		}

		Matcher matcher = "name".equals(field) ? OPTION_SYMBOL_DATE_PATTERN.matcher(copiedOption.getName())
				: OPTION_SYMBOL_DATE_PATTERN.matcher(copiedOption.getSymbol());

		if (matcher.matches()) {
			String existingDateString = matcher.group(1);
			String dateString = ((month < 10 ? "0" : "") + month) + "/" + ((day < 10 ? "0" : "") + day) + "/" + Integer.toString(year).substring(2);
			if (!StringUtils.isEmpty(matcher.group(3))) {
				dateString = ((month < 10 ? "0" : "") + month) + ((day < 10 ? "0" : "") + day) + (year);
			}
			else if (!StringUtils.isEmpty(matcher.group(4))) {
				dateString = DateUtils.fromDate(expirationDate, "MMMM yyyy");
			}

			switch (field) {
				case "symbol":
				case "name":
				case "description": {
					return value.replaceFirst(existingDateString, dateString);
				}
			}
		}
		else {
			InvestmentDeliveryMonthCodes monthCode = InvestmentDeliveryMonthCodes.valueOfMonthNumber(month);
			String securityMonthExpirationCode = monthCode.name() + (Integer.toString(year).substring(3));
			if (copiedOption.getSymbol().matches(securityMonthExpirationCode)) {
				String securityMonthExpirationLongCode = monthCode.getMonthName().substring(0, 2) + (Integer.toString(year).substring(2));
				switch (field) {
					case "symbol":
					case "name":
					case "description": {
						return value.replaceFirst("", securityMonthExpirationLongCode);
					}
				}
			}
		}
		return value;
	}
}
