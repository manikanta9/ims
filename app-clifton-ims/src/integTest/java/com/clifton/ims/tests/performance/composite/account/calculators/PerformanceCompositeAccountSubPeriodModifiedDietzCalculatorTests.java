package com.clifton.ims.tests.performance.composite.account.calculators;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountSubPeriodModifiedDietzCalculatorTests</code> tests the PerformanceCompositeAccountSubPeriodModifiedDietzCalculator
 * <p>
 * Note: The new IMS calculation is slightly different than the old spreadsheet calculation, so there could be some small discrepancies.
 * Also, in some cases the market value is slightly different than what was originally recorded because there are accrual/factor updates that were entered after they originally
 * pulled the market value for the spreadsheet calculation.
 * <p>
 * Once this calculator is used exclusively in IMS for a few months, should add more months to these tests
 *
 * @author manderson
 */
public class PerformanceCompositeAccountSubPeriodModifiedDietzCalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Test
	public void test_account_428600() {
		String accountNumber = "428600";

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("170860608.88")); // Actual Value: 170,856,879.12
		overrideValueMap.put("periodNetReturn", new BigDecimal("-1.63321941")); // -1.64958607
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-65857")); // Accounting Value: Missing
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("-65857")); // Accounting Value: Missing
		overrideValueMap.put("periodGainLoss", new BigDecimal("-2812485.59"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		// SUB PERIOD CASH FLOW ON 1/20/2016
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "01/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "02/29/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "03/31/2016");
	}


	@Test
	public void test_account_456000() {
		String accountNumber = "456000";

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("43247151.67")); // Actual Value: 43,241,018.40
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-4.84590505")); // Actual Value: -4.859399033935700
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("-4.873606")); // Based Accounting Gross Return Value: -4.887096
		overrideValueMap.put("periodNetReturn", new BigDecimal("-4.84590505")); // Actual Value: -4.887095884202530
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("59.57")); // Actual Value: 65.95 (Accounting didn't weight this cash flow)
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("43295744")); // Actual Value: 43,295,725
		overrideValueMap.put("periodGrossReturn", new BigDecimal("0.11225662")); // Actual Value: 0.12641188
		overrideValueMap.put("periodNetReturn", new BigDecimal("0.11225662")); // Actual Value: 0.09726358
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("0.083112")); // Based Accounting Gross Return Value: 0.097264
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("46243260")); // Actual Value: 46,243,240
		overrideValueMap.put("periodNetReturn", new BigDecimal("6.80786599")); // Actual Value: 6.77677214
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_456280() {
		String accountNumber = "456280";

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("111706262")); // Actual Value: 111,704,815
		overrideValueMap.put("periodNetReturn", new BigDecimal("-4.84937282")); // Actual Value: -4.87916145
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-4165000")); // Actual Value: 4165000 - Looks like incorrectly entered with wrong sign.  Weighted cash flow matches.
		overrideValueMap.put("periodGainLoss", new BigDecimal("-5796949.67"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("106009312")); // Actual Value: 105,999,685
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-8.50278382")); // Actual Value: -8.51122598
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("-8.530581")); // Based Accounting Gross Return Value: -8.537860
		overrideValueMap.put("periodNetReturn", new BigDecimal("-8.50278382")); // Actual Value: -8.537859728590099
		overrideValueMap.put("periodGainLoss", new BigDecimal("-9695502.93"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		// SUB PERIOD CASH FLOW ON 02/16/2016
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("118742466")); // Actual Value: 118,741,706
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-0.02896035")); // Actual Value: -0.02107594
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.02896035")); // Actual Value: -0.05018130
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("-0.058063")); // Based Accounting Gross Return Value: -0.050181
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("128511463")); // Actual Value: 128,510,070
		overrideValueMap.put("periodNetReturn", new BigDecimal("8.23445726")); // Actual Value: 8.20364204
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_122511() {
		String accountNumber = "122511";

		// Note: During this month the account had 2 consecutive days of cash flows
		// In reality - Accounting moved the second cash flow so it would appear to have happened on the same date, but this is a one off
		// and IMS calculated value would be slightly different without those overrides
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("16277430")); // Accounting Value: 16,277,339
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-0.72938902")); // Actual Value: -0.71488495
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.79593766")); // Actual Value: -0.75614208
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("-0.7706401")); // Based Accounting Gross Return Value: -0.756142
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("-6980000")); // Actual Value: -7420000
		overrideValueMap.put("periodGainLoss", new BigDecimal("-103362.08"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "06/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-449796.01"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("16114706")); // Actual Value: 16,114,600
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-20569")); // Actual Value: Missing
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("-18441")); // Actual Value: Missing
		overrideValueMap.put("periodGainLoss", new BigDecimal("129944.39"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("16546543")); // Actual Value: 16,546,445
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}
}
