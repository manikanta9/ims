package com.clifton.ims.tests.investment.instruction;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionContactGroup;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;

import java.util.Date;


public class InvestmentInstructionContactBuilder {

	private InvestmentInstructionContactGroup group;
	private BusinessCompany company;
	private Date startDate;
	private Date endDate;

	private BusinessContact recipientContact;
	private SystemBeanBuilder beanBuilder;

	private InvestmentInstructionSetupService investmentInstructionSetupService;
	private SystemBeanService systemBeanService;

	///////////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionContactBuilder(String contactType, SystemBeanService systemBeanService, InvestmentInstructionSetupService investmentInstructionSetupService) {
		this.beanBuilder = SystemBeanBuilder.newBuilder(null, "Instruction " + contactType, systemBeanService);
		this.systemBeanService = systemBeanService;
		this.investmentInstructionSetupService = investmentInstructionSetupService;
	}


	public InvestmentInstructionContact build() {
		InvestmentInstructionContact instructionContact = new InvestmentInstructionContact();
		instructionContact.setGroup(this.group);
		instructionContact.setCompany(this.company);
		instructionContact.setStartDate(this.startDate);
		instructionContact.setEndDate(this.endDate);
		SystemBean bean = this.beanBuilder.build();
		bean.setName("Test Contact" + (this.recipientContact != null ? " - " + this.recipientContact.getLabelShort() : "") + " - " + RandomUtils.randomNumber());
		instructionContact.setDestinationSystemBean(bean);
		this.investmentInstructionSetupService.saveInvestmentInstructionContact(instructionContact);
		return instructionContact;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionContactBuilder contactGroup(InvestmentInstructionContactGroup group) {
		this.group = group;
		return this;
	}


	public InvestmentInstructionContactBuilder company(BusinessCompany company) {
		this.company = company;
		return this;
	}


	public InvestmentInstructionContactBuilder startDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public InvestmentInstructionContactBuilder endDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	/**
	 * For use on Email or Fax destinations
	 */
	public InvestmentInstructionContactBuilder contact(BusinessContact contact) {
		this.recipientContact = contact;
		return contact(contact.getId());
	}


	/**
	 * For use on Email or Fax destinations
	 */
	public InvestmentInstructionContactBuilder contact(Integer contactId) {
		this.beanBuilder.addProperty("Recipient Contact", contactId.toString());
		return this;
	}


	/**
	 * For use on Email destinations
	 */
	public InvestmentInstructionContactBuilder hostname(String hostname) {
		this.beanBuilder.addProperty("SMTP Host Url", hostname);
		return this;
	}


	/**
	 * For use on Email destinations
	 */
	public InvestmentInstructionContactBuilder port(Integer port) {
		this.beanBuilder.addProperty("SMTP Host Port", port.toString());
		return this;
	}


	/**
	 * For use with Ftp Destinations
	 */
	public InvestmentInstructionContactBuilder sharedFtpDestination(ExportDefinitionDestination sharedFtpDestination) {
		this.systemBeanService.saveSystemBean(sharedFtpDestination.getDestinationSystemBean());
		this.beanBuilder.addProperty("FTP Destination", sharedFtpDestination.getDestinationSystemBean().getId().toString());
		return this;
	}
}
