package com.clifton.ims.tests.rule.portfolio.run.target;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.portfolio.run.PortfolioRunUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildCommand;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.balance.search.PortfolioTargetBalanceSearchForm;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
public class PortfolioRunTargetRuleTests extends RuleTests {

	private static boolean testInitialized = false;
	protected static final short PIOS_BUSINESS_SERVICE_ID = 3;
	protected static final short CURRENCY_HEDGE_PROCESSING_TYPE_ID = 16;
	protected static final String ADEPT_19_ACCOUNT_NUMBER = "051451";
	protected static final String PROCESS_DATE = "06/28/2021";

	public static PortfolioRun portfolioRun;

	@Resource
	private PortfolioTargetBalanceService portfolioTargetBalanceService;
	@Resource
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private PortfolioRunService portfolioRunService;
	@Resource
	private PortfolioRunUtils portfolioRunUtils;
	@Resource
	private PortfolioTargetService portfolioTargetService;
	@Resource
	private InvestmentReplicationService investmentReplicationService;
	@Resource
	private WorkflowTransitionService workflowTransitionService;


	public boolean isTestInitialized() {
		return testInitialized;
	}


	public void setTestInitialized(boolean initialized) {
		testInitialized = initialized;
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		Set<String> excludedTestMethodSet = new HashSet<>();
		excludedTestMethodSet.add("testCashExposureOfTotalPortfolioValuePortfolioTargetsRule");
		excludedTestMethodSet.add("testCashTargetBalanceRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testCashTargetChangeSincePreviousDayPortfolioTargetsRule");
		excludedTestMethodSet.add("testContractsUnmatchedExcludeBondsAndCurrencyPortfolioTargetsRule");
		excludedTestMethodSet.add("testContractsUnmatchedExcludeCollateralandCurrencyPortfolioTargetsRule");
		excludedTestMethodSet.add("testContractsUnmatchedPortfolioTargetsRule");
		excludedTestMethodSet.add("testExplicitPositionAllocationChangeSincePreviousDayPortfolioTargetsRule");
		excludedTestMethodSet.add("testExplicitPositionAllocationUnableToFulFillPortfolioTargetsRule");
		excludedTestMethodSet.add("testExplicitPositionAllocationUnusedPortfolioTargetsRule");
		excludedTestMethodSet.add("testSyntheticallyAdjustedPositionsExposureRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testAssetClassLargeChangeInBenchmarkDurationSincePreviousDayPortfolioTargetsRule");
		excludedTestMethodSet.add("testAssetClassMiniRebalanceRecommendedPortfolioTargetsRule");
		excludedTestMethodSet.add("testAssetClassOutdatedBenchmarkDurationPortfolioTargetsRule");
		excludedTestMethodSet.add("testAssetClassOutdatedReplicationPortfolioTargetsRule");
		excludedTestMethodSet.add("testAssetClassRebalanceRecommendedPortfolioTargetsRule");
		excludedTestMethodSet.add("testAssetClassReplicationConsecutive0AllocationWithContractsPortfolioTargetsRule");
		excludedTestMethodSet.add("testCurrencyBalanceSingleBaseExposureOfReplicationRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testCurrencyBalanceSingleBaseExposureRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testCurrencyBalanceTotalBaseExposureOfCashRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testCurrencyBalanceTotalBaseExposureRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testCurrencyBalanceTotalBaseExposureofTargetRangeViolationPortfolioTargetsRule");
		excludedTestMethodSet.add("testManagerAccountDailyChangevsBenchmarkPortfolioTargetsRule");
		excludedTestMethodSet.add("testManagerAccountRebalanceRecommendedPortfolioTargetsRule");
		excludedTestMethodSet.add("testManagerAccountTotalMarketValuevsPreviousRunTotalMarketValuePortfolioTargetsRule");
		excludedTestMethodSet.add("testManagerAccountTotalPortfolioCashPercentageRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testManagerAccountTotalPortfolioPercentageRangePortfolioTargetsRule");
		excludedTestMethodSet.add("testManagerAccountTotalPortfolioSecuritiesPercentageRangePortfolioTargetsRule");

		return excludedTestMethodSet;
	}


	@Override
	public String getCategoryName() {
		return "Portfolio Run Rules";
	}


	@BeforeEach
	@Override
	public void initializeTest() {
		if (!isTestInitialized()) {
			super.initializeTest();

			portfolioRun = createPortfolioRun();

			setTestInitialized(true);
		}
	}


	@Override
	protected void applyRuleDefinitionSearchFormOverrides(RuleDefinitionSearchForm ruleDefinitionSearchForm) {
		ruleDefinitionSearchForm.setRuleScopeNameEquals("Portfolio Target Client Accounts");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPortfolioTargetOutdatedReplicationRule() {
		createPortfolioRun(DateUtils.addWeekDays(new Date(), -2));

		createRuleAssignment("Portfolio Target: Outdated Replication");

		// Convert bean to single-day outdated threshold
		SystemBean ruleEvalBean = this.systemBeanService.getSystemBeanByName("Violation: Portfolio Target Outdated Replication");
		SystemBeanProperty days = CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyListByBeanId(ruleEvalBean.getId()));
		days.setValue("0");
		ruleEvalBean.setPropertyList(Collections.singletonList(days));
		this.systemBeanService.saveSystemBean(ruleEvalBean);

		reprocessRun(portfolioRun);

		validateViolationNoteExists(portfolioRun, "which is greater than 0 days</span>");
	}


	@Test
	public void testPortfolioTargetOutsideOfRebalanceBandRule() {
		createRuleAssignment("Portfolio Target: Outside Of Rebalance Band");

		reprocessRun(portfolioRun);

		validateViolationNoteExists(portfolioRun, "which is greater than the maximum deviation of 5.0%.");
		validateViolationNoteExists(portfolioRun, "which is greater than the maximum deviation of 10.0%.");
	}


	@Test
	public void testPortfolioTargetPercentChangeSincePreviousDayRule() {
		createRuleAssignment("Portfolio Target: Percent Change Since Previous Day");

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_19_ACCOUNT_NUMBER);

		// Need to create run for next day
		PortfolioRun oldRun = this.portfolioRunService.getPortfolioRunByMainRun(clientAccount.getId(), DateUtils.addWeekDays(DateUtils.toDate(PROCESS_DATE), 1));
		if (oldRun != null) {
			oldRun.setMainRun(false);
			this.portfolioRunService.savePortfolioRun(oldRun);
		}

		// Drop percent change threshold down so that violation is triggered
		SystemBean ruleEvalBean = this.systemBeanService.getSystemBeanByName("Violation: Portfolio Target Percent Change Since Previous Day");
		SystemBeanProperty days = CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyListByBeanId(ruleEvalBean.getId()));
		days.setValue("0.1");
		ruleEvalBean.setPropertyList(Collections.singletonList(days));
		this.systemBeanService.saveSystemBean(ruleEvalBean);

		PortfolioRun nextDayRun = this.portfolioRunUtils.createPortfolioRun(clientAccount, DateUtils.addWeekDays(DateUtils.toDate(PROCESS_DATE), 1), true);

		reprocessRun(nextDayRun);

		validateViolationNoteExists(nextDayRun, "Total exposure $-88,452,343.12 differed from the previous exposure $-88,147,974.42 by 0.35%, which is greater than the maximum deviation of 0.1%.");
	}


	@Test
	public void testPortfolioTargetClientDirectedTargetBalanceChangedSincePreviousDayRule() {
		createRuleAssignment("Portfolio Target: Client Directed Target Balance Changed Since Previous Day");

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_19_ACCOUNT_NUMBER);

		PortfolioTargetBalance balance = new PortfolioTargetBalance();

		List<PortfolioTarget> targets = this.portfolioTargetService.getPortfolioTargetListByClientAccount(clientAccount.getId(), DateUtils.toDate(PROCESS_DATE), true);

		PortfolioTarget targetToUse = CollectionUtils.getOnlyElement(targets.stream()
				.filter(target -> target.getName().equals("LGIM ACS"))
				.collect(Collectors.toList()));

		targetToUse.setTargetCalculatorBean(this.systemBeanService.getSystemBeanByName("Client Directed Balance Target Calculator Bean"));
		targetToUse.setTargetPercent(new BigDecimal("100"));

		targetToUse = this.portfolioTargetService.savePortfolioTarget(targetToUse);

		balance.setTarget(targetToUse);
		balance.setStartDate(DateUtils.toDate(PROCESS_DATE));
		balance.setValue(new BigDecimal("1000000"));

		// Check for existing balance in case test was already ran
		PortfolioTargetBalanceSearchForm sf = new PortfolioTargetBalanceSearchForm();
		sf.setTargetId(targetToUse.getId());
		sf.setActiveOnDate(DateUtils.toDate(PROCESS_DATE));
		PortfolioTargetBalance existingBalance = CollectionUtils.getOnlyElement(this.portfolioTargetBalanceService.getPortfolioTargetBalanceList(sf));

		if (existingBalance == null) {
			balance = this.portfolioTargetBalanceService.savePortfolioTargetBalance(balance);
		}
		else if (!MathUtils.isEqual(existingBalance.getValue(), balance.getValue())) {
			existingBalance.setValue(new BigDecimal("1000000"));
			balance = this.portfolioTargetBalanceService.savePortfolioTargetBalance(existingBalance);
		}

		try {
			reprocessRun(portfolioRun);
			validateViolationNoteExists(portfolioRun, "Balance for target 051451: LGIM ACS with effective range [06/23/2021-] is $1,000,000.00, which differed from the previous day's balance $83,550,577.70 by 98.80%, which is greater than the maximum deviation of 3.0%.");
		}
		finally {
			// cleanup
			this.portfolioTargetBalanceService.deletePortfolioTargetBalance(balance.getId());
			targetToUse.setTargetCalculatorBean(this.systemBeanService.getSystemBeanByName("Default Manager Allocation Target Calculator"));
			this.portfolioTargetService.savePortfolioTarget(targetToUse);
		}
	}


	@Test
	public void testPortfolioTargetZeroAllocationWithNonzeroClientHoldingsRule() {
		createRuleAssignment("Portfolio Target: Zero Allocation With Nonzero Client Holdings");

		// Update replication allocation history
		InvestmentReplicationSearchForm sf = new InvestmentReplicationSearchForm();
		sf.setNameEquals("Adept 19 - LGIM ACS BP Only (Short)");
		InvestmentReplication replication = CollectionUtils.getOnlyElement(this.investmentReplicationService.getInvestmentReplicationList(sf));

		List<InvestmentReplicationAllocation> allocations = this.investmentReplicationService.getInvestmentReplicationAllocationListByReplicationActive(replication.getId(), DateUtils.toDate(PROCESS_DATE), false);

		InvestmentReplicationAllocation replicationAllocation = CollectionUtils.getOnlyElement(allocations
				.stream()
				.filter(alloc -> alloc.getAllocationLabel().equals("Security [AUD/GBP20210730 (AUD/GBP Curr Fwd 07/30/2021) [INACTIVE]]"))
				.collect(Collectors.toList()));

		replicationAllocation.setAlwaysIncludeCurrentSecurity(true);
		replicationAllocation.setAllocationWeight(BigDecimal.ZERO);
		replicationAllocation.setStartDate(DateUtils.toDate("06/01/2021"));
		replicationAllocation = this.investmentReplicationService.saveInvestmentReplicationAllocation(replicationAllocation);

		InvestmentReplicationHistoryRebuildCommand command = new InvestmentReplicationHistoryRebuildCommand();
		command.setReplicationId(replication.getId());
		command.setRebuildExisting(true);
		command.setStartBalanceDate(DateUtils.toDate("06/01/2021"));
		command.setEndBalanceDate(DateUtils.toDate("07/01/2021"));

		this.investmentReplicationHistoryRebuildService.rebuildInvestmentReplicationHistory(command);

		reprocessRun(portfolioRun);

		validateViolationNoteExists(portfolioRun, "AUD/GBP Currency Forward is configured to have 0% allocation");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void createRuleAssignment(String definitionName) {
		RuleDefinition definition = this.ruleDefinitionService.getRuleDefinitionByName(definitionName);
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(ADEPT_19_ACCOUNT_NUMBER);

		RuleAssignmentSearchForm assignmentSearchForm = new RuleAssignmentSearchForm();
		assignmentSearchForm.setActiveOnDate(new Date());
		assignmentSearchForm.setRuleDefinitionId(definition.getId());
		assignmentSearchForm.setAdditionalScopeFkFieldIdEquals(BeanUtils.getIdentityAsLong(clientAccount));

		List<RuleAssignment> assignments = this.ruleDefinitionService.getRuleAssignmentList(assignmentSearchForm);

		if (CollectionUtils.isEmpty(assignments)) {
			RuleAssignment assignment = new RuleAssignment();
			assignment.setRuleDefinition(definition);
			assignment.setAdditionalScopeFkFieldId(BeanUtils.getIdentityAsLong(clientAccount));
			assignment.setNote("Test");
			this.ruleDefinitionService.saveRuleAssignment(assignment);
		}
	}


	protected void validateViolationNoteExists(PortfolioRun run, String expectedViolationNote) {
		RuleViolationSearchForm sf = new RuleViolationSearchForm();
		sf.setLinkedTableName("PortfolioRun");
		sf.setLinkedFkFieldId(new Long(run.getId()));
		List<RuleViolation> violations = this.ruleViolationService.getRuleViolationList(sf);
		boolean foundViolationNote = false;
		StringBuilder failureMessage = new StringBuilder("Failed to find expected Violation: '" + expectedViolationNote + "' in ruleViolationList: \n");
		if (!CollectionUtils.isEmpty(violations)) {
			Pattern pattern = Pattern.compile(expectedViolationNote);
			for (RuleViolation ruleViolation : CollectionUtils.getIterable(violations)) {
				Matcher matcher = pattern.matcher(ruleViolation.getViolationNote());
				if (ruleViolation.getViolationNote().equals(expectedViolationNote) || matcher.find()) {
					foundViolationNote = true;
				}
				failureMessage.append("\t").append(ruleViolation.getId()).append(" - ").append(ruleViolation.getViolationNote()).append("\n");
			}
		}
		Assertions.assertTrue(foundViolationNote, failureMessage.toString());
	}


	private PortfolioRun createPortfolioRun() {
		return createPortfolioRun(DateUtils.toDate(PROCESS_DATE));
	}


	private PortfolioRun createPortfolioRun(Date balanceDate) {
		// update processing type
		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber(ADEPT_19_ACCOUNT_NUMBER);

		// Check existing run first and make it not main run
		PortfolioRun oldRun = this.portfolioRunService.getPortfolioRunByMainRun(clientAccount.getId(), balanceDate);
		if (oldRun != null) {
			oldRun.setMainRun(false);
			this.portfolioRunService.savePortfolioRun(oldRun);
		}

		portfolioRun = this.portfolioRunUtils.createPortfolioRun(clientAccount, balanceDate, true);
		return portfolioRun;
	}


	public PortfolioRun reprocessRun(PortfolioRun run) {
		this.workflowTransitionService.executeWorkflowTransitionByTransition("PortfolioRun", run.getId(), 345);
		run = this.portfolioRunService.getPortfolioRun(run.getId());
		return run;
	}
}
