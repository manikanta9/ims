package com.clifton.ims.tests.system.note;

import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.search.SystemNoteSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * The class <code>SystemNoteServiceTests</code> contains tests for SystemNoteService
 *
 * @author manderson
 */
public class SystemNoteServiceTests extends BaseImsIntegrationTest {

	@Resource
	private SystemNoteService systemNoteService;


	@Test
	public void testGetSystemNoteListForEntity_includeNotesForLinkedEntity() {
		// Trades are Unique Because they also display notes for the Security
		// Using Trade ID 501549 - for TRS Swap Trade - The Security has already matured, so there should be any more notes added to it
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setEntityTableName("Trade");
		searchForm.setFkFieldId((long) 501549);
		searchForm.setIncludeNotesAsLinkedEntity(false); // UI Sets this to false for Trades, because we know Trades aren't used as a linked entity
		searchForm.setIncludeNotesForLinkedEntity(true); // This triggers Security Notes to be included
		List<SystemNote> noteList = this.systemNoteService.getSystemNoteListForEntity(searchForm);

		Assertions.assertEquals(7, CollectionUtils.getSize(noteList), "Expected 7 Notes for Trade 501549");

		// If we were to chose NOT to include Security notes, there should only be 6 notes
		searchForm.setIncludeNotesForLinkedEntity(false);
		noteList = this.systemNoteService.getSystemNoteListForEntity(searchForm);

		Assertions.assertEquals(6, CollectionUtils.getSize(noteList), "Expected 6 Notes for Trade 501549, excluding Security Notes");
	}


	@Test
	public void testGetSystemNoteListForLinkedEntity_includeNotesAsLinkedEntity() {
		// InvestmentSecurities are Unique Because they also display notes for all Trades for the Security
		// Using TRS "MacArthur - DB MSCI ExUS 100 Hedged" (same security that is used for above Trade 501549) because it's matured and shouldn't have any additional notes associated with it
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setEntityTableName("InvestmentSecurity");
		searchForm.setFkFieldId((long) 27491);
		searchForm.setIncludeNotesAsLinkedEntity(true); // UI Sets this to true for Securities, because we know Trades reference Security as it's Linked Entity, and we want to include those Trade Notes
		searchForm.setIncludeNotesForLinkedEntity(false); // UI Sets this to false, because there are no linked associations we need to include
		List<SystemNote> noteList = this.systemNoteService.getSystemNoteListForEntity(searchForm);

		Assertions.assertEquals(15, CollectionUtils.getSize(noteList), "Expected 15 Notes for Security ID 27491 MacArthur - DB MSCI ExUS 100 Hedged");

		// If we were to chose NOT to include Trade notes, there should only be 1 note
		searchForm.setIncludeNotesAsLinkedEntity(false);
		noteList = this.systemNoteService.getSystemNoteListForEntity(searchForm);

		Assertions.assertEquals(1, CollectionUtils.getSize(noteList), "Expected 1 Note for Security ID 27491 MacArthur - DB MSCI ExUS 100 Hedged, excluding Trade Notes");
	}


	@Test
	public void testGetSystemNoteListForLinkedEntity_noLinksToInclude() {
		// Investment Accounts do not currently have any links set up, so all notes would need to be associated with that account only
		// Using Account # 128000 Crown Cork which has been terminated, so additional notes will likely not be added
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setEntityTableName("InvestmentAccount");
		searchForm.setFkFieldId((long) 100);
		// Accounts don't use these, so account notes by default don't set these parameters
		searchForm.setIncludeNotesForLinkedEntity(false);
		searchForm.setIncludeNotesAsLinkedEntity(false);
		List<SystemNote> noteList = this.systemNoteService.getSystemNoteListForEntity(searchForm);

		Assertions.assertEquals(9, CollectionUtils.getSize(noteList), "Expected 9 Notes for Account ID 100 12800 Crown Cork");
	}
}
