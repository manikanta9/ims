package com.clifton.ims.tests.accounting.m2m;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


public class AccountingM2MServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingM2MService accountingM2MService;


	////////////////////////////////////////////////////////////////////////////
	//////////////////////////////// Currency //////////////////////////////////


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_FullLastCurrencyClose() {
		// full CAD balance closing trade on 6/17 with 6/18 settlement
		Assertions.assertEquals(new BigDecimal("198782.45"), getMarkAmountBase("157-25631", "6/16/2014"));
		Assertions.assertEquals(new BigDecimal("-1483876.58"), getMarkAmountBase("157-25631", "6/17/2014"));
		Assertions.assertEquals(new BigDecimal("1457585.27"), getMarkAmountBase("157-25631", "6/18/2014"));
		Assertions.assertEquals(new BigDecimal("177055.80"), getMarkAmountBase("157-25631", "6/19/2014"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_FullCurrencyClose_4_of_5() {
		// full AUD, EUR, GBP, JPY balance closing trades on 5/7 with 5/9 settlement; CAD balance remaining
		Assertions.assertEquals(new BigDecimal("3881804.19"), getMarkAmountBase("157-25631", "5/6/2014"));
		Assertions.assertEquals(new BigDecimal("-3396622.69"), getMarkAmountBase("157-25631", "5/7/2014"));
		Assertions.assertEquals(new BigDecimal("-753370.45"), getMarkAmountBase("157-25631", "5/8/2014"));
		Assertions.assertEquals(new BigDecimal("-3564064.24"), getMarkAmountBase("157-25631", "5/9/2014"));
		Assertions.assertEquals(new BigDecimal("-5017365.39"), getMarkAmountBase("157-25631", "5/12/2014"));
	}


	@Test
	@Disabled // NOTE: Ignore for JIRA Issue http://jira/browse/ACCOUNTING-410
	public void testGetAccountingPositionDailyLiveForM2MList_KRW_Interest_Income() {
		// The currency translation GL issue and the Interest Income issue need to be fixed for these to work.
		// See simple journal 298383 for the Interest Income entry that caused this issue.
		Assertions.assertEquals(new BigDecimal("797317.51"), getMarkAmountBase("157-89267", "10/10/2014"));
		Assertions.assertEquals(new BigDecimal("2462622.20"), getMarkAmountBase("157-89267", "10/13/2014"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_JPY() {
		Assertions.assertEquals(new BigDecimal("-12752.45"), getMarkAmountBase("052-BADET", "10/23/2014"));
	}


	@Test
	@Disabled // NOTE: Ignore for JIRA Issue http://jira/browse/ACCOUNTING-410
	public void testGetAccountingPositionDailyLiveForM2MList_TradeOpeningCurrencyBalance_Trade_371710() {
		// Trade that creates CAD balance
		Assertions.assertEquals(new BigDecimal("123792.53"), getMarkAmountBase("157-45547", "06/16/2014")); // TradeID = 371710
	}


	@Test
	@Disabled // NOTE: Ignore for JIRA Issue http://jira/browse/ACCOUNTING-410
	public void testGetAccountingPositionDailyLiveForM2MList_TradeOpeningCurrencyBalance_Trade_390250() {
		// Trade that creates CAD balance
		Assertions.assertEquals(new BigDecimal("-1369.68"), getMarkAmountBase("157-62606", "09/15/2014")); // TradeID = 390250
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_TradeOpeningCurrencyBalance() {
		// Marks with EUR balances that break when backing out currency translation GL - Client 056901
		Assertions.assertEquals(new BigDecimal("35361.45"), getMarkAmountBase("157-87713", "09/04/2014"));
		Assertions.assertEquals(new BigDecimal("-42.03"), getMarkAmountBase("157-87713", "09/05/2014"));
		Assertions.assertEquals(new BigDecimal("-6008.81"), getMarkAmountBase("157-87713", "09/08/2014"));
		Assertions.assertEquals(new BigDecimal("-12998.69"), getMarkAmountBase("157-87713", "09/09/2014"));
		Assertions.assertEquals(new BigDecimal("-2.95"), getMarkAmountBase("157-87713", "09/10/2014"));
		Assertions.assertEquals(new BigDecimal("-6703.27"), getMarkAmountBase("157-87713", "09/11/2014"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_DayAfterCurrencyBecomesNonZeroAgain() {
		// Currency balances keep accumulating Currency Translation Gain/Loss in  Base Debit/Credit field.
		// The first day the balance becomes non-zero again, it's important to account for previous currency translation
		// gain/loss in mark to market.
		// For more details see: ACCOUNTING-534
		Assertions.assertEquals(new BigDecimal("2298862.50"), getMarkAmountBase("052-BADQZ", "03/16/2017"));
		Assertions.assertEquals(new BigDecimal("865019.98"), getMarkAmountBase("052-BADQZ", "03/17/2017"));
		Assertions.assertEquals(new BigDecimal("-1011687.28"), getMarkAmountBase("052-BADQZ", "03/20/2017"));
		Assertions.assertEquals(new BigDecimal("-2385842.95"), getMarkAmountBase("052-BADQZ", "03/21/2017"));
		Assertions.assertEquals(new BigDecimal("-1684342.09"), getMarkAmountBase("052-BADQZ", "03/22/2017"));
	}


	////////////////////////////////////////////////////////////////////////////
	/////////////////////////// Options on Futures /////////////////////////////


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_OptionsOnFutures() {
		// open and close for 1DQ5P 1965 (T+0 Settlement!!!)
		Assertions.assertEquals(new BigDecimal("0.00"), getMarkAmountBase("157-78121", "07/03/2015"));
		Assertions.assertEquals(new BigDecimal("26460.00"), getMarkAmountBase("157-78121", "07/06/2015"));
		Assertions.assertEquals(new BigDecimal("0.00"), getMarkAmountBase("157-78121", "07/07/2015"));

		Assertions.assertEquals(new BigDecimal("0.00"), getMarkAmountBase("157-78121", "08/06/2015"));
		Assertions.assertEquals(new BigDecimal("27402.00"), getMarkAmountBase("157-78121", "08/07/2015"));
		Assertions.assertEquals(new BigDecimal("19335.00"), getMarkAmountBase("157-78121", "08/10/2015"));
	}


	////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Cleared CDS ////////////////////////////////


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedCDSCouponPaymentOnMonday() {
		// Friday accrual needs to include Sat and Sun (CCP Convention)
		Assertions.assertEquals(new BigDecimal("737.05"), getMarkAmountBase("047-789136", "09/18/2014"));
		Assertions.assertEquals(new BigDecimal("419.12"), getMarkAmountBase("047-789136", "09/19/2014"));
		Assertions.assertEquals(new BigDecimal("-1045.30"), getMarkAmountBase("047-789136", "09/22/2014"));
		Assertions.assertEquals(new BigDecimal("-813.72"), getMarkAmountBase("047-789136", "09/23/2014"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedCDSCouponPaymentOnFriday() {
		Assertions.assertEquals(new BigDecimal("2655.71"), getMarkAmountBase("047-789136", "06/18/2014"));
		Assertions.assertEquals(new BigDecimal("377.62"), getMarkAmountBase("047-789136", "06/19/2014"));
		Assertions.assertEquals(new BigDecimal("-274.21"), getMarkAmountBase("047-789136", "06/20/2014"));
		Assertions.assertEquals(new BigDecimal("785.12"), getMarkAmountBase("047-789136", "06/23/2014"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedCDSDayAfterClose() {
		Assertions.assertEquals(new BigDecimal("-95156.35"), getMarkAmountBase("047-529821", "11/13/2014"));
		Assertions.assertEquals(new BigDecimal("-26475.70"), getMarkAmountBase("047-484845", "11/13/2014"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedCDSCouponAndCreditEvent() {
		Assertions.assertEquals(new BigDecimal("877212.18"), getMarkAmountBase("047-529821", "06/18/2015"));
		Assertions.assertEquals(new BigDecimal("780907.15"), getMarkAmountBase("047-529821", "06/19/2015"));
		Assertions.assertEquals(new BigDecimal("3769389.93"), getMarkAmountBase("047-529821", "06/22/2015")); // coupon payment
		Assertions.assertEquals(new BigDecimal("169575.61"), getMarkAmountBase("047-529821", "06/23/2015"));
		Assertions.assertEquals(new BigDecimal("-1065066.82"), getMarkAmountBase("047-529821", "06/24/2015"));
		Assertions.assertEquals(new BigDecimal("147733.80"), getMarkAmountBase("047-529821", "06/25/2015"));
		Assertions.assertEquals(new BigDecimal("-97162.61"), getMarkAmountBase("047-529821", "06/26/2015")); // credit event payment
		Assertions.assertEquals(new BigDecimal("-5795028.79"), getMarkAmountBase("047-529821", "06/29/2015"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedCDSForeignCreditEvent() {
		// TX2402D20E0500XXI default with Ex Date of 07/22/2016 and Payment Date of 07/26/2016
		// Need to account for Currency Translation Gain/Loss and have Event Journal use FX from Posting Date
		Assertions.assertEquals(new BigDecimal("911164.93"), getMarkAmountBase("047-529821", "07/20/2016"));
		Assertions.assertEquals(new BigDecimal("-743366.14"), getMarkAmountBase("047-529821", "07/21/2016"));
		Assertions.assertEquals(new BigDecimal("621677.09"), getMarkAmountBase("047-529821", "07/22/2016"));
		Assertions.assertEquals(new BigDecimal("-1641437.40"), getMarkAmountBase("047-529821", "07/25/2016"));
		Assertions.assertEquals(new BigDecimal("-1085407.49"), getMarkAmountBase("047-529821", "07/26/2016"));
		Assertions.assertEquals(new BigDecimal("1043929.12"), getMarkAmountBase("047-529821", "07/27/2016"));
		Assertions.assertEquals(new BigDecimal("-1142407.82"), getMarkAmountBase("047-529821", "07/28/2016"));
	}


	////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Cleared IRS ////////////////////////////////


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedIRSCouponPayment() {
		Assertions.assertEquals(new BigDecimal("-736336.64"), getMarkAmountBase("047-911730", "4/30/2015"));
		Assertions.assertEquals(new BigDecimal("-3644284.95"), getMarkAmountBase("047-911730", "5/01/2015"));
		Assertions.assertEquals(new BigDecimal("-1464009.18"), getMarkAmountBase("047-911730", "5/04/2015"));
		Assertions.assertEquals(new BigDecimal("-1453172.45"), getMarkAmountBase("047-911730", "5/05/2015"));
		Assertions.assertEquals(new BigDecimal("-3105478.12"), getMarkAmountBase("047-911730", "5/06/2015"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedIRSPartialClose() {
		Assertions.assertEquals(new BigDecimal("6546335.59"), getMarkAmountBase("061-7HNF80", "07/08/2015"));
		Assertions.assertEquals(new BigDecimal("-15685879.37"), getMarkAmountBase("061-7HNF80", "07/09/2015"));
		Assertions.assertEquals(new BigDecimal("-15715387.15"), getMarkAmountBase("061-7HNF80", "07/10/2015"));
	}


	@Test
	public void testGetAccountingPositionDailyLiveForM2MList_ClearedIRSFullClose() {
		Assertions.assertEquals(new BigDecimal("80291.91"), getMarkAmountBase("047-911730", "05/20/2015"));
		Assertions.assertEquals(new BigDecimal("1343904.04"), getMarkAmountBase("047-911730", "05/21/2015"));
		Assertions.assertEquals(new BigDecimal("-480072.07"), getMarkAmountBase("047-911730", "05/22/2015"));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BigDecimal getMarkAmountBase(String holdingAccountNumber, String date) {
		InvestmentAccount holdingAccount = this.investmentAccountUtils.getHoldingAccountByNumber(holdingAccountNumber);
		List<AccountingPositionDaily> positionList = this.accountingM2MService.getAccountingPositionDailyLiveForM2MList(AccountingPositionDailyLiveSearchForm.forSnapshotDate(DateUtils.toDate(date)).forHoldingAccount(holdingAccount.getId()));
		return CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getMarkAmountBase);
	}
}
