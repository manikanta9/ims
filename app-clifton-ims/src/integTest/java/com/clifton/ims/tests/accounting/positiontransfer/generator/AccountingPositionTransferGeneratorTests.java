package com.clifton.ims.tests.accounting.positiontransfer.generator;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.accounting.positiontransfer.generator.AccountingPositionTransferGeneratorService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingJournalDetailBuilder;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.accounting.GeneralLedgerDescriptions;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;


/**
 * Position transfer generator service and the transfer upload service both use the
 * {@link com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommandHandler}
 * to process the requests. This test class extends the upload tests by testing maturity on close security.
 *
 * @author NickK
 */
public class AccountingPositionTransferGeneratorTests extends BaseImsIntegrationTest {

	private static InvestmentSecurity LAN15;

	@Resource
	private AccountingPositionTransferGeneratorService accountingPositionTransferGeneratorService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;


	@BeforeEach
	public void beforeTest() {
		if (LAN15 == null) {
			LAN15 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("LAN15");
		}
	}


	@Test
	public void testLmeNetTransfer() {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);

		// Create a position for 'fromAccount'
		Trade[] trades = createTrades(fromAccount);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createTradeAllComplianceRule(toAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, LAN15.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, LAN15.getInstrument());

		// Generate and book transfer
		AccountingPositionTransferCommand command = getTransferCommandTemplate(fromAccount, toAccount, DateUtils.addDays(trades[1].getTradeDate(), 1));
		command.setQuantityPercent(new BigDecimal("20"));
		command.setPercentRoundingMode(RoundingMode.UP);
		// 20% of -5 (net position)
		BigDecimal transferQuantity = new BigDecimal("-1");

		generateAndValidateTransfer(command, transferQuantity, new BigDecimal("41868.75"), true);

		BigDecimal[] quantities = Stream.concat(
				this.tradeUtils.getTradeFills(trades[0]).stream()
						.map(TradeFill::getQuantity),
				this.tradeUtils.getTradeFills(trades[1]).stream()
						.map(TradeFill::getQuantity)
						.map(q -> MathUtils.isEqual(q, new BigDecimal("5")) ? q.add(transferQuantity) : q)
						.map(BigDecimal::negate)
		).sorted().toArray(BigDecimal[]::new);

		// Get and validate positions
		assertPositionList(fromAccount, quantities);
	}


	@Test
	public void testLmeTransfer() {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);

		// Create a position for 'fromAccount'
		Trade[] trades = createTrades(fromAccount);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createTradeAllComplianceRule(toAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, LAN15.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, LAN15.getInstrument());

		// Generate and book transfer
		AccountingPositionTransferCommand command = getTransferCommandTemplate(fromAccount, toAccount, DateUtils.addDays(trades[0].getTradeDate(), 1));
		BigDecimal transferQuantity = new BigDecimal("4");
		command.setQuantity(transferQuantity);

		generateAndValidateTransfer(command, transferQuantity, new BigDecimal("167975"), false);

		List<TradeFill> buyFillList = this.tradeUtils.getTradeFills(trades[0]);
		Assertions.assertEquals(1, buyFillList.size());

		BigDecimal[] quantities = Stream.concat(
				buyFillList.stream()
						.map(TradeFill::getQuantity)
						.map(q -> q.subtract(transferQuantity)),
				this.tradeUtils.getTradeFills(trades[1]).stream()
						.map(TradeFill::getQuantity)
						.map(BigDecimal::negate)
		).sorted().toArray(BigDecimal[]::new);

		// Get and validate positions
		assertPositionList(fromAccount, quantities);
	}


	private Trade[] createTrades(AccountInfo accountInfo) {
		// Create a position for 'fromAccount'
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(LAN15, new BigDecimal("20"), true, DateUtils.toDate("06/16/2015"), broker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);
		Trade sellTrade = this.tradeUtils.newFuturesTradeBuilder(LAN15, new BigDecimal("25"), false, DateUtils.toDate("06/18/2015"), broker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sellTrade);

		BigDecimal[] quantities = Stream.concat(
				this.tradeUtils.getTradeFills(buyTrade).stream().map(TradeFill::getQuantity),
				this.tradeUtils.getTradeFills(sellTrade).stream().map(TradeFill::getQuantity).map(BigDecimal::negate)
		).sorted().toArray(BigDecimal[]::new);

		// Get and validate positions
		assertPositionList(accountInfo, quantities);

		return new Trade[]{buyTrade, sellTrade};
	}


	private void assertPositionList(AccountInfo accountInfo, BigDecimal... positionQuantities) {
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(accountInfo, LAN15, new Date());
		positionList.sort(Comparator.comparing(AccountingPosition::getRemainingQuantity));
		Assertions.assertEquals(positionQuantities.length, positionList.size());
		for (int i = 0; i < positionQuantities.length; i++) {
			if (!MathUtils.isEqual(positionQuantities[i], positionList.get(i).getRemainingQuantity())) {
				Assertions.fail("Position quantity [" + positionList.get(i).getRemainingQuantity() + "] does not match expected [" + positionQuantities[i] + "]");
			}
		}
	}


	private AccountingPositionTransferCommand getTransferCommandTemplate(AccountInfo fromAccount, AccountInfo toAccount, Date transactionDate) {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setFromClientInvestmentAccount((InvestmentAccount) getBaseEntityWithIdOnly(fromAccount.getClientAccount()));
		command.setFromHoldingInvestmentAccount((InvestmentAccount) getBaseEntityWithIdOnly(fromAccount.getHoldingAccount()));
		String transferTypeName = AccountingUtils.FROM_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME;
		StringBuilder noteBuilder = new StringBuilder("Transfer from client account ").append(fromAccount.getClientAccount().getName()).append(" and holding account ").append(fromAccount.getHoldingAccount().getName());
		if (toAccount != null) {
			transferTypeName = AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME;
			noteBuilder.append("\nto client account ").append(toAccount.getClientAccount().getName()).append(" and holding account ").append(toAccount.getHoldingAccount().getName());
			command.setToClientInvestmentAccount((InvestmentAccount) getBaseEntityWithIdOnly(toAccount.getClientAccount()));
			command.setToHoldingInvestmentAccount((InvestmentAccount) getBaseEntityWithIdOnly(toAccount.getHoldingAccount()));
		}
		command.setType(this.accountingUtils.getAccountingPositionTransferType(transferTypeName));
		command.setNote(noteBuilder.toString());
		command.setSecurity((InvestmentSecurity) getBaseEntityWithIdOnly(LAN15));
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(transactionDate);
		command.setPriceDate(DateUtils.addDays(transactionDate, -1));
		return command;
	}


	private void generateAndValidateTransfer(AccountingPositionTransferCommand command, BigDecimal transferQuantity, BigDecimal gainLoss, boolean shortTransfer) {
		this.accountingPositionTransferGeneratorService.generateAccountingPositionTransferList(command);
		AccountingPositionTransfer transfer = bookTransfer(command);

		Assertions.assertNotNull(transfer);
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.security(LAN15);
		List<AccountingJournalDetailDefinition> expectedTransferDetails = new ArrayList<>();
		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.quantity(transferQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.clientInvestmentAccount(transfer.getFromClientInvestmentAccount())
				.holdingInvestmentAccount(transfer.getFromHoldingInvestmentAccount())
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, transfer.getToClientInvestmentAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());
		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.quantity(transferQuantity)
				.opening(true)
				.clientInvestmentAccount(transfer.getToClientInvestmentAccount())
				.holdingInvestmentAccount(transfer.getToHoldingInvestmentAccount())
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, transfer.getFromClientInvestmentAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());
		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.quantity(shortTransfer ? transferQuantity.negate() : transferQuantity)
				.opening(false)
				.localDebitCredit(shortTransfer ? gainLoss : gainLoss.negate())
				.clientInvestmentAccount(transfer.getFromClientInvestmentAccount())
				.holdingInvestmentAccount(transfer.getFromHoldingInvestmentAccount())
				.description(shortTransfer ? GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_LOSS : GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_GAIN, LAN15)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());
		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.opening(true)
				.price(null)
				.quantity(null)
				.localDebitCredit(shortTransfer ? gainLoss.negate() : gainLoss)
				.clientInvestmentAccount(transfer.getFromClientInvestmentAccount())
				.holdingInvestmentAccount(transfer.getFromHoldingInvestmentAccount())
				.description(shortTransfer ? GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE : GeneralLedgerDescriptions.CASH_PROCEEDS_FROM_CLOSE, LAN15)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		List<? extends AccountingJournalDetailDefinition> actualTransferDetails = this.accountingUtils.getPositionTransferDetails(transfer);
		AccountingUtils.assertJournalDetailList(expectedTransferDetails, actualTransferDetails);
	}


	@SuppressWarnings("unchecked")
	private <T extends Serializable> BaseEntity<T> getBaseEntityWithIdOnly(BaseEntity<T> object) {
		BaseEntity<T> newObject = BeanUtils.newInstance(object.getClass());
		newObject.setId(object.getId());
		return newObject;
	}


	private AccountingPositionTransfer bookTransfer(AccountingPositionTransferCommand command) {
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setTransferTypeName(command.getTypeName());
		searchForm.setTransactionDate(command.getTransactionDate());
		searchForm.setSettlementDate(command.getSettlementDate());
		searchForm.setFromClientInvestmentAccountId(command.getFromClientInvestmentAccount().getId());
		searchForm.setFromHoldingInvestmentAccountId(command.getFromHoldingInvestmentAccount().getId());
		if (command.getToClientInvestmentAccount() != null) {
			searchForm.setToClientInvestmentAccountId(command.getToClientInvestmentAccount().getId());
			searchForm.setToHoldingInvestmentAccountId(command.getToHoldingInvestmentAccount().getId());
		}
		searchForm.setJournalNotBooked(Boolean.TRUE);
		AccountingPositionTransfer transfer = CollectionUtils.getOnlyElement(this.accountingPositionTransferService.getAccountingPositionTransferList(searchForm));
		Assertions.assertNotNull(transfer);
		return this.accountingUtils.bookAccountingPositionTransfer(transfer);
	}
}
