package com.clifton.ims.tests.calendar.schedule;

import com.clifton.business.contract.BusinessContractTemplate;
import com.clifton.business.service.BusinessService;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleOccurrenceCommand;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.search.CalendarScheduleSearchForm;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author mitchellf
 */
public class CalendarScheduleTests extends BaseImsIntegrationTest {

	private final String SCHEDULE_TYPE_NAME = "Investment Calendar Event Schedules";
	private final String US_BANKS_CALENDAR = "US Banks Calendar";
	private final String NYSE_CALENDAR = "New York Stock Exchange";
	private final String CONTACT_CLIENT_REMINDER = "Contact Client Reminder";
	private final String INVESTMENT_CALENDAR_WORKFLOW = "Investment Calendar Workflow (Trading)";

	@Resource
	private CalendarScheduleService calendarScheduleService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentCalendarService investmentCalendarService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;


	/**
	 * Tests a unique case where getting monthly calendar schedule occurrences generates a stack overflow error from getting stuck in a recursive loop.
	 * Additionally, this will bring out the case where (before code changes) the 1-minute adjustment to the request date causes the next occurrence to be before the request date.
	 * After code changes, the service call should execute with no problems.
	 */
	@Test
	public void testGetMonthlyOccurrencesBetween_NegativeAdjustment() {
		// The 'getOccurrencesBetween' logic calls getNextOccurrence, so this will test that as well
		CalendarSchedule schedule = getOrCreateMonthlyCalendarSchedule("Schedule 1", US_BANKS_CALENDAR, -5, "01/24/2020", 1, 0, BusinessDayConventions.PREVIOUS_MODIFIED);
		CalendarScheduleOccurrenceCommand command = CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/10/2020 5:00 AM", "12/23/2020 6:00 AM").build();

		this.calendarScheduleService.getCalendarScheduleOccurrences(command);
	}


	/**
	 * Tests a unique case where getting monthly calendar schedule occurrences generates a stack overflow error from getting stuck in a recursive loop.
	 * Additionally, this will bring out the case where (before code changes) the 1-minute adjustment to the request date causes the next occurrence to be before the request date.
	 * After code changes, the service call should execute with no problems.
	 */
	@Test
	public void testGetMonthlyOccurrencesBetween_PositiveAdjustment() {
		// The 'getOccurrencesBetween' logic calls getNextOccurrence, so this will test that as well
		CalendarSchedule schedule = getOrCreateMonthlyCalendarSchedule("Schedule 2", US_BANKS_CALENDAR, 5, "01/24/2020", 1, 0, BusinessDayConventions.PREVIOUS_MODIFIED);
		CalendarScheduleOccurrenceCommand command = CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesBetween("08/10/2020 5:00 AM", "12/23/2020 6:00 AM").build();

		this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		List<Date> result = this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		validateResults(result, "09/09/2020", "10/08/2020", "11/09/2020", "12/08/2020");
	}


	/**
	 * Tests a unique case where getting monthly calendar schedule occurrences generates a stack overflow error from getting stuck in a recursive loop.
	 * Additionally, this will bring out the case where (before code changes) the 1-minute adjustment to the request date causes the next occurrence to be before the request date.
	 * After code changes, the service call should execute with no problems.
	 */
	@Test
	public void testGetMonthlyOccurrencesPrevious_NegativeAdjustment() {
		CalendarSchedule schedule = getOrCreateMonthlyCalendarSchedule("Schedule 3", US_BANKS_CALENDAR, -5, "01/24/2020", 1, 0, BusinessDayConventions.PREVIOUS_MODIFIED);
		CalendarScheduleOccurrenceCommand command = CalendarScheduleOccurrenceCommand.of(schedule)
				.withPreviousOccurrenceFromStartDate("08/25/2020 12:00 AM").build();

		this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		List<Date> result = this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		validateResults(result, "08/25/2020");
	}


	/**
	 * Tests a unique case where getting monthly calendar schedule occurrences generates a stack overflow error from getting stuck in a recursive loop.
	 * Additionally, this will bring out the case where (before code changes) the 1-minute adjustment to the request date causes the previous occurrence to be before the request date.
	 * After code changes, the service call should execute with no problems.
	 */
	@Test
	public void testGetMonthlyOccurrencesPrevious_PositiveAdjustment() {
		CalendarSchedule schedule = getOrCreateMonthlyCalendarSchedule("Schedule 4", US_BANKS_CALENDAR, 5, "01/24/2020", 1, 0, BusinessDayConventions.PREVIOUS_MODIFIED);
		CalendarScheduleOccurrenceCommand command = CalendarScheduleOccurrenceCommand.of(schedule)
				.withOccurrencesFromStartDate("08/25/2020 12:00 AM", -5).build();

		this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		List<Date> result = this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		// since the business day convention used is PREVIOUS_MODIFIED, and the 08/01/2020 is a Saturday, we end up with 08/10/2020 for the first result
		validateResults(result, "08/10/2020", "07/09/2020", "06/08/2020", "05/08/2020", "04/08/2020");
	}


	/**
	 * This test exists to check a case where monthly schedule with recurrence 3 (i.e. quarterly) was not properly processed.
	 * The issue was caused by the non-persistent firstOccurrence field on the schedule not being populated, because the schedule was not a new bean.
	 */
	@Test
	public void testEventWithQuarterlyScheduleNotNewBean() {
		// Create schedule
		Date today = new Date();
		String startDate = DateUtils.fromDate(today);
		CalendarSchedule schedule = getOrCreateMonthlyCalendarSchedule("Schedule 5", NYSE_CALENDAR, -1, startDate, 3, 21, BusinessDayConventions.PREVIOUS);

		// The root cause of this issue is exposed specifically in Investment Event Recurrence processing, it does not occur using the schedule preview functionality.  As such, we can use the schedule preview functionality to validate the results.

		CalendarScheduleOccurrenceCommand command = CalendarScheduleOccurrenceCommand.of(schedule).withOccurrencesBetween(today, DateUtils.addYears(today, 1)).build();
		List<Date> expectedResults = this.calendarScheduleService.getCalendarScheduleOccurrences(command);

		// Create event and assign to schedule
		InvestmentEvent event = new InvestmentEvent();
		event.setEventType(this.investmentCalendarService.getInvestmentEventTypeByName(CONTACT_CLIENT_REMINDER));
		InvestmentAccount account = this.investmentAccountService.getInvestmentAccountByNumber("000001");
		event.setInvestmentAccount(account);
		event.setRecurrenceSchedule(schedule);
		InvestmentEvent finalEvent = event;
		event = RequestOverrideHandler.serviceCallWithOverrides(() -> this.investmentCalendarService.saveInvestmentEvent(finalEvent, true),
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentEvent.class, false, "coalesceTeam", "eventDate"),
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, false, "aumCalculatorBean"),
				new RequestOverrideHandler.EntityPropertyOverrides(BusinessService.class, false, "aumCalculatorBean"),
				new RequestOverrideHandler.EntityPropertyOverrides(BusinessContractTemplate.class, false, "securityResource"));

		// Run event through workflow transitions
		Workflow wf = this.workflowDefinitionService.getWorkflowByName(INVESTMENT_CALENDAR_WORKFLOW);
		WorkflowState pending = this.workflowDefinitionService.getWorkflowStateByName(wf.getId(), "Pending Approval");
		WorkflowState approved = this.workflowDefinitionService.getWorkflowStateByName(wf.getId(), "Approved");

		this.workflowTransitionService.executeWorkflowTransition("InvestmentEvent", event.getId(), pending.getId());
		this.workflowTransitionService.executeWorkflowTransition("InvestmentEvent", event.getId(), approved.getId());

		// Get event occurrences, verify that it actually is quarterly
		InvestmentEventSearchForm sf = new InvestmentEventSearchForm();
		sf.setSeriesDefinitionId(event.getId());
		List<InvestmentEvent> events = this.investmentCalendarService.getInvestmentEventList(sf);
		List<Date> results = events.stream().map(InvestmentEvent::getEventDate).collect(Collectors.toList());
		validateResults(results, expectedResults);
	}


	private CalendarSchedule getOrCreateMonthlyCalendarSchedule(String name, String calendarName, int businessDayAdjustment, String startDate, int recurrence, int dayOfMonth, BusinessDayConventions businessDayConvention) {

		CalendarScheduleSearchForm scheduleSearchForm = new CalendarScheduleSearchForm();
		scheduleSearchForm.setName(name);
		CalendarSchedule testSchedule = CollectionUtils.getOnlyElement(this.calendarScheduleService.getCalendarScheduleList(scheduleSearchForm));
		if (testSchedule == null) {
			testSchedule = new CalendarSchedule();
			testSchedule.setName(name);
		}
		testSchedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName(SCHEDULE_TYPE_NAME));
		testSchedule.setDescription("Default required");
		testSchedule.setCalendar(this.calendarSetupService.getCalendarByName(calendarName));
		testSchedule.setBusinessDayConvention(businessDayConvention);
		testSchedule.setFrequency(ScheduleFrequencies.MONTHLY);
		testSchedule.setRecurrence(recurrence);
		testSchedule.setDayOfMonth(dayOfMonth);
		testSchedule.setStartDate(DateUtils.toDate(startDate));
		testSchedule.setAdditionalBusinessDays(businessDayAdjustment);
		return this.calendarScheduleService.saveCalendarSchedule(testSchedule);
	}


	private void validateResults(List<Date> results, String... expectedDates) {
		if (results.size() != expectedDates.length) {
			throw new ValidationException("Expected dates were not found.");
		}
		List<Date> dates = ArrayUtils.getStream(expectedDates).map(DateUtils::toDate).collect(Collectors.toList());

		if (CollectionUtils.getIntersection(dates, results).size() != expectedDates.length) {
			throw new ValidationException("Expected dates were not found.");
		}
	}


	private void validateResults(List<Date> results, List<Date> expectedDates) {
		if (results.size() != expectedDates.size()) {
			throw new ValidationException("Expected dates were not found.");
		}

		if (CollectionUtils.getIntersection(results, expectedDates).size() != expectedDates.size()) {
			throw new ValidationException("Expected dates were not found.");
		}
	}
}
