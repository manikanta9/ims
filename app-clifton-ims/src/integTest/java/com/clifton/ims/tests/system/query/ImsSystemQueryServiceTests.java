package com.clifton.ims.tests.system.query;

import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.spring.ImsIntegrationConfiguration;
import com.clifton.test.system.query.BaseSystemQueryServiceTests;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;


/**
 * The SystemQueryServiceTests tests all select queries that they can execute without error
 * <p>
 *
 * @author manderson
 */
@ContextConfiguration(classes = {ImsIntegrationConfiguration.class})
public class ImsSystemQueryServiceTests extends BaseSystemQueryServiceTests {

	static {
		// THESE ARE TEMPORARY QUERY EXPORTS AND BECAUSE OF CROSS DBS - CAN'T RUN OUTSIDE OF PROD SO IGNORING THEM
		addQueryToExcludeSet("Trade: Need Change from Assumed to Actual Factor");

		// EXEC sp_executesql - Needs Real Results or else will throw an exception
		addQueryParameterValueOverride("Account AUM, Gain Loss, Billing Projected Revenue For An Accounting Period", "Accounting Period End Date", "01/31/2011");
		addQueryParameterValueOverride("Client Account Asset Class AUM Based on Business Service", "Date", "01/31/2011");
		addQueryParameterValueOverride("Client Account Currency Balances", "Date", "01/31/2050"); // Seems to work faster locally with a future date, rather than a past date - still slower than preferred

		addQueryParameterValueOverride("Historical KRD Values (Pivoted with KRD Fields across top)", "Client Account", "1");
		addQueryParameterValueOverride("Performance Summary Asset Class Gain Loss", "Summary", "10922");
		addQueryParameterValueOverride("Performance Summary Asset Class Overlay Targets", "Summary", "10922");
		addQueryParameterValueOverride("Billing Invoice Group: Shared Allocations (Includes Monthly Averages)", "Invoice Group #", "2"); // Needs an existing invoice group
		addQueryParameterValueOverride("PIOS Overlay Target by Contract", "Client Account", "1");
		addQueryParameterValueOverride("PIOS Overlay Target by Contract", "Start Date", "01/30/2015");
		addQueryParameterValueOverride("PIOS Overlay Target by Contract", "End Date", "01/30/2015");
		addQueryParameterValueOverride("PIOS - Actual Exposure Across one or All Accounts for a Date Range", "Client Account", "1");
		addQueryParameterValueOverride("PIOS - Actual Exposure Across one or All Accounts for a Date Range", "Start Date", "01/30/2015");
		addQueryParameterValueOverride("PIOS - Actual Exposure Across one or All Accounts for a Date Range", "End Date", "01/30/2015");

		addQueryParameterValueOverride("Performance Summary History", "clientAccountId", "-1");
		addQueryParameterValueOverride("Reconciliation Position Activity Query", "clientInvestmentAccountId", "1");

		addQueryParameterValueOverride("Counterparty OTC Exposure Summary", "Report Date", DateUtils.fromDate(DateUtils.getPreviousWeekday(new Date()), DateUtils.DATE_FORMAT_INPUT));
		addQueryParameterValueOverride("Counterparty OTC Exposure Summary", "Base Currency", "1927");
		addQueryParameterValueOverride("Counterparty OTC Exposure Summary", "FX Source", "3");

		addQueryParameterValueOverride("Client Account and Security Net Exposure Summary", "Report Date", DateUtils.fromDate(DateUtils.toDate("08/30/2019"), DateUtils.DATE_FORMAT_INPUT));
		addQueryParameterValueOverride("Client Account and Security Net Exposure Summary", "Base Currency", "1927");
		addQueryParameterValueOverride("Client Account and Security Net Exposure Summary", "FX Source", "2");
		addQueryParameterValueOverride("Client Account and Security Net Exposure Summary", "Instrument Group", "22");

		// NOTE: SPECIAL CASE - IF CLIENT ACCOUNT IS AN OPTIONAL PARAMETER IT IS ALWAYS SET TO DEFAULT VALUE (-1)
		// TO REDUCE THE NUMBER OF OVERRIDES EXPLICITLY NEEDED
		addQueryParameterRequiredOverride("Client Account");
		addQueryParameterRequiredOverride("ClientInvestmentAccountID");
		addQueryParameterRequiredOverride("ClientAccountID");
		addQueryParameterRequiredOverride("clientAccountId");

		// These queries do not require parameters via UI, but in sql require x or y to be selected (i.e. account or account group)
		addQueryParameterRequiredOverride("Investment Manager Balance Adjustment History (Manual)", "Client");
		addQueryParameterRequiredOverride("M2M Holding Account Transfer Report", "Holding Company");
		addQueryParameterRequiredOverride("Benchmark Returns", "Security");
		addQueryParameterRequiredOverride("Electra Holdings File", "Investment Account Group");
		addQueryParameterRequiredOverride("Electra Holdings File", "ReportDate");
		addQueryParameterRequiredOverride("Collateral: Calculated Required Collateral for Options", "Holding Account");

		// These queries do not require parameters, but we set them to make tests run faster/return less data
		// We'll still just use default values
		addQueryParameterRequiredOverride("Client Turnover", "Start Date");
		addQueryParameterRequiredOverride("Client Turnover", "End Date");
		addQueryParameterRequiredOverride("PIOS Cash Bucket Totals Account History", "End Date");
		addQueryParameterRequiredOverride("Holdings Snapshot with Shares Outstanding", "Client Account Group");
		addQueryParameterRequiredOverride("EV: Client Account Calculated AUM with Daily Change", "Date");
		addQueryParameterRequiredOverride("Billing Invoices: Projected vs. Actual Revenue and AUM vs. Billing Basis", "Quarter End Date");
		addQueryParameterRequiredOverride("Billing Invoice Details (Includes Client NBD Rep)", "End Date");
		addQueryParameterRequiredOverride("Commodity Roll Futures Positions (Across All Client Accounts)", "Date");

		// Explicit Overrides
		addQueryParameterValueOverride("Trade Commission Overrides History", "From", "01/01/2050");
		addQueryParameterValueOverride("Compliance: Positions vs. Exchange Limits", "restrictionList", "[{\"comparison\":\"GREATER_THAN\",\"value\":80,\"field\":\"percentOfLimit\"}]");
		addQueryParameterValueOverride("Market Data: Future Fair Value Adjustment & Mispricing Per Contract", "Date", "01/01/2050");
		addQueryParameterValueOverride("OTI Security Event Review", "Instrument Group", "-1");
		addQueryParameterValueOverride("Electra Transactions File", "Investment Account Group", "-1");
		addQueryParameterValueOverride("Client Account Info (Includes Client and Client Relationship Information) And Custodian", "AccountStatusOption", "Terminated");
		addQueryParameterValueOverride("OCC Options Margin Calculator Export", "Holding Account", "-1");
		addQueryParameterValueOverride("DUMAC Data File: Header/Trailer Record", "Run Time", "15:00:00.000");
		addQueryParameterValueOverride("Transaction History (Position and Currency Activity Only)", "Client Account Group", "-1");

		addQueryParameterValueOverride("Counterparty OTC Exposure Summary", "Report Date", "11/30/2020");
		addQueryParameterValueOverride("Counterparty OTC Exposure Summary", "Base Currency", "1927");
		addQueryParameterValueOverride("Counterparty OTC Exposure Summary", "FX Source", "2");

		addQueryParameterValueOverride("Counterparty OTC Exposure Summary (Gross)", "Report Date", "11/30/2020");
		addQueryParameterValueOverride("Counterparty OTC Exposure Summary (Gross)", "Base Currency", "1927");
		addQueryParameterValueOverride("Counterparty OTC Exposure Summary (Gross)", "FX Source", "2");
	}
}
