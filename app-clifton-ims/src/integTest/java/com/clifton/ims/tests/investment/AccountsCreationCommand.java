package com.clifton.ims.tests.investment;

import com.clifton.business.client.BusinessClient;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.TradeType;


/**
 * The AccountsCreationCommand class holds parameters used to create InvestmentAccount objects
 * and corresponding relationships.
 *
 * @author vgomelsky
 */
public class AccountsCreationCommand {

	private BusinessClient client; // optionally specified the client account to bypass client creation and only create investment accounts and relationships
	private InvestmentAccount clientAccount; // optionally specifies the client account to bypass client/client account creation and only create holding account and relationships

	private InvestmentSecurity otcSecurity; // when defined, takes precedence over holdingCompany: contract and company from otcSecurity will be used
	private BusinessCompanies holdingCompany;
	private BusinessCompanies executingCompany;       // If execution and custodian holding company are set also create a Broker account and relate it to the custodian account
	private BusinessCompanies custodianHoldingCompany; // If custodian holding company is set will also create a Custodian account and relate it to the main and holding accounts
	private InvestmentAccount custodianAccount; // optionally specifies the custodian account
	private BusinessCompanies repoHoldingCompany;
	private BusinessCompanies marginHoldingCompany;
	private BusinessCompanies defaultExchangeRateSourceCompany;

	private String[] excludeDefinitionNames;

	private TradeType tradeType;
	private boolean clsHoldingAccount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountsCreationCommand of(BusinessCompanies holdingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = new AccountsCreationCommand();
		result.holdingCompany = holdingCompany;
		result.tradeType = tradeType;
		result.excludeDefinitionNames = excludeDefinitionNames;
		return result;
	}


	public static AccountsCreationCommand ofCustodian(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.of(holdingCompany, tradeType, excludeDefinitionNames);
		result.custodianHoldingCompany = custodianHoldingCompany;
		return result;
	}


	public static AccountsCreationCommand ofClientAndCustodian(BusinessClient client, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType, excludeDefinitionNames);
		result.client = client;
		return result;
	}


	public static AccountsCreationCommand ofClientAccountAndCustodian(InvestmentAccount clientAccount, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType, excludeDefinitionNames);
		result.clientAccount = clientAccount;
		return result;
	}


	public static AccountsCreationCommand ofClientAccountAndCustodianAccount(InvestmentAccount clientAccount, BusinessCompanies holdingCompany, InvestmentAccount custodianAccount, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofCustodian(holdingCompany, null, tradeType, excludeDefinitionNames);
		result.clientAccount = clientAccount;
		result.custodianAccount = custodianAccount;
		return result;
	}


	public static AccountsCreationCommand ofCustodianAndExecution(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, BusinessCompanies executingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType, excludeDefinitionNames);
		result.executingCompany = executingCompany;
		return result;
	}


	public static AccountsCreationCommand ofCustodianAndREPO(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, BusinessCompanies repoHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType, excludeDefinitionNames);
		result.repoHoldingCompany = repoHoldingCompany;
		return result;
	}


	public static AccountsCreationCommand ofOtcSecurity(InvestmentSecurity otcSecurity, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = new AccountsCreationCommand();
		result.otcSecurity = otcSecurity;
		result.tradeType = tradeType;
		result.excludeDefinitionNames = excludeDefinitionNames;
		return result;
	}


	public static AccountsCreationCommand ofOtcSecurityAndCustodian(InvestmentSecurity otcSecurity, BusinessCompanies custodianHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofOtcSecurity(otcSecurity, tradeType, excludeDefinitionNames);
		result.custodianHoldingCompany = custodianHoldingCompany;
		return result;
	}


	public static AccountsCreationCommand ofOtcSecurityAndClientAccount(InvestmentSecurity otcSecurity, InvestmentAccount clientAccount, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofOtcSecurity(otcSecurity, tradeType, excludeDefinitionNames);
		result.clientAccount = clientAccount;
		return result;
	}


	public static AccountsCreationCommand ofCustodianAndMargin(BusinessCompanies marginHoldingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		AccountsCreationCommand result = AccountsCreationCommand.ofCustodian(marginHoldingCompany, custodianHoldingCompany, tradeType, excludeDefinitionNames);
		result.holdingCompany = null;
		result.marginHoldingCompany = marginHoldingCompany;
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessClient getClient() {
		return this.client;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public InvestmentSecurity getOtcSecurity() {
		return this.otcSecurity;
	}


	public BusinessCompanies getHoldingCompany() {
		return this.holdingCompany;
	}


	public BusinessCompanies getExecutingCompany() {
		return this.executingCompany;
	}


	public void setExecutingCompany(BusinessCompanies executingCompany) {
		this.executingCompany = executingCompany;
	}


	public BusinessCompanies getCustodianHoldingCompany() {
		return this.custodianHoldingCompany;
	}


	public BusinessCompanies getRepoHoldingCompany() {
		return this.repoHoldingCompany;
	}


	public BusinessCompanies getMarginHoldingCompany() {
		return this.marginHoldingCompany;
	}


	public void setMarginHoldingCompany(BusinessCompanies marginHoldingCompany) {
		this.marginHoldingCompany = marginHoldingCompany;
	}


	public BusinessCompanies getDefaultExchangeRateSourceCompany() {
		return this.defaultExchangeRateSourceCompany;
	}


	public void setDefaultExchangeRateSourceCompany(BusinessCompanies defaultExchangeRateSourceCompany) {
		this.defaultExchangeRateSourceCompany = defaultExchangeRateSourceCompany;
	}


	public TradeType getTradeType() {
		return this.tradeType;
	}


	public String[] getExcludeDefinitionNames() {
		return this.excludeDefinitionNames;
	}


	public InvestmentAccount getCustodianAccount() {
		return this.custodianAccount;
	}


	public boolean isClsHoldingAccount() {
		return this.clsHoldingAccount;
	}


	public void setClsHoldingAccount(boolean clsHoldingAccount) {
		this.clsHoldingAccount = clsHoldingAccount;
	}
}
