package com.clifton.ims.tests.rule.collateral.balance;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetailBuilder;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferTypeBuilder;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class CollateralBalanceRuleTests extends RuleTests {

	private static final String CLEARED_OTC_COLLATERAL = "Cleared OTC Collateral";
	private static final String FUTURES_COLLATERAL = "Futures Collateral";
	private static final String OTC_COLLATERAL = "OTC Collateral";
	private static final String COLLATERAL_BALANCE_TABLE_NAME = "collateralBalance";

	@Resource
	CollateralBalanceService collateralBalanceService;

	@Resource
	private MarketDataFieldService marketDataFieldService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private AccountingBookingService<?> accountingBookingService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private AccountingPostingService accountingPostingService;

	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	@Resource
	private TradeService tradeService;

	@Resource
	private AccountingJournalService accountingJournalService;

	@Resource
	private AccountingEventJournalService accountingEventJournalService;


	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Collateral Balance Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return new HashSet<>();
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCollateralBalanceFuturesCashThresholdRangeRule() {
		this.ruleViolationUtils.createRuleAssignment("Collateral Balance: Futures Cash Threshold Range", null,
				BigDecimal.valueOf(0), BigDecimal.valueOf(1), null, null);
		validateViolationExistsRegex(getCollateralBalanceUsingClientInvestmentAccountLabel("122511", "07/07/2016", FUTURES_COLLATERAL)
				, Pattern.quote("07/07/2016: 260-70065: Children's Health Care RSVP Plan (Futures Broker) (Futures Collateral):<br /><span class='violationMessage'>Excess Collateral Percentage ") +
						"<span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span>" + Pattern.quote(" is greater than the maximum allowed <span class='amountPositive'>1%</span></span>"));
	}


	@Test
	public void testCollateralBalanceFuturesSecuritiesThresholdRangeRule() {
		this.ruleViolationUtils.createRuleAssignment("Collateral Balance: Futures Securities Threshold Range", null,
				BigDecimal.valueOf(0), BigDecimal.valueOf(1), null, null);
		validateViolationExistsRegex(getCollateralBalanceUsingClientInvestmentAccountLabel("456280", "07/07/2016", FUTURES_COLLATERAL)
				, Pattern.quote("07/07/2016: 052-88049: ND State Investment Board Pension ERuss2000 (Futures Broker) (Futures Collateral):<br /><span class='violationMessage'>Excess Collateral Percentage ") +
						"<span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span>" + Pattern.quote(" is greater than the maximum allowed <span class='amountPositive'>1%</span></span>"));
	}


	@Test
	public void testCollateralBalanceClearedOTCCashThresholdRangeRule() {
		this.ruleViolationUtils.createRuleAssignment("Collateral Balance: Cleared OTC Cash Threshold Range", null,
				BigDecimal.valueOf(0), BigDecimal.valueOf(1), null, null);
		validateViolationExistsRegex(getCollateralBalanceUsingClientInvestmentAccountLabel("222491", "07/06/2016", CLEARED_OTC_COLLATERAL)
				, Pattern.quote("07/06/2016: 047-484696: ERP of Duke University-Cleared CDX (OTC Cleared) (Cleared OTC Collateral):<br /><span class='violationMessage'>Excess Collateral Percentage ") +
						"<span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span>" + Pattern.quote(" is greater than the maximum allowed <span class='amountPositive'>1%</span></span>"));
	}


	//<span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span>
	@Test
	public void testCollateralBalanceClearedOTCSecuritiesThresholdRangeRule() {
		this.ruleViolationUtils.createRuleAssignment("Collateral Balance: Cleared OTC Securities Threshold Range", null,
				BigDecimal.valueOf(0), BigDecimal.valueOf(1), null, null);
		validateViolationExistsRegex(getCollateralBalanceUsingClientInvestmentAccountLabel("522450", "07/06/2016", CLEARED_OTC_COLLATERAL)
				, Pattern.quote("07/06/2016: 047-911730: Philips Electronics North America Corporation MRT Pension Plan (OTC Cleared) (Cleared OTC Collateral):<br /><span class='violationMessage'>Excess Collateral Percentage ") +
						"<span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span>" + Pattern.quote(" is greater than the maximum allowed <span class='amountPositive'>1%</span></span>"));
	}


	@Test
	public void testMarginCallCalculationsMissingOrStalePricesRule() {
		CollateralBalance balance = getCollateralBalanceUsingHoldingAccount("NUSC-BBNK", DateUtils.toDate("07/10/2019"), OTC_COLLATERAL);
		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm();
		marketDataValueSearchForm.setInvestmentSecurityId(112410);
		marketDataValueSearchForm.setMeasureDate(DateUtils.toDate("07/10/2019"));
		MarketDataValue marketDataValue = CollectionUtils.getOnlyElementStrict(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
		try {
			this.marketDataFieldService.deleteMarketDataValue(marketDataValue.getId());
			validateViolationExists(balance, "Prior Business Day Pricing is missing for Security [21834741 (Barc - Eversource - SPTR) [INACTIVE]]");
		}
		finally {
			MarketDataValue marketDataValueCheck = CollectionUtils.getOnlyElement(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
			if (marketDataValueCheck == null) {
				marketDataValue.setId(null);
				this.marketDataFieldService.saveMarketDataValue(marketDataValue);
			}
			else {
				Assertions.fail("Failed to delete Market Data Value required to validate rule!");
			}
		}
		validateViolationPassed(balance, "Margin Call Calculations: Missing Or Stale Prices", true);
	}


	@Test
	public void testMarginCallCalculationsClientCounterpartyCollateralPostedRule() {
		CollateralBalance balance = getCollateralBalanceUsingHoldingAccount("NUSC-BBNK", DateUtils.toDate("07/10/2019"), OTC_COLLATERAL);

		InvestmentAccountSearchForm investmentAccountSearchForm = new InvestmentAccountSearchForm();
		investmentAccountSearchForm.setNumber("455000");
		InvestmentAccount toFromClientInvestmentAccount = CollectionUtils.getOnlyElementStrict(this.investmentAccountService.getInvestmentAccountList(investmentAccountSearchForm));
		investmentAccountSearchForm.setNumber("689192");
		InvestmentAccount fromHoldingInvestmentAccount = CollectionUtils.getOnlyElementStrict(this.investmentAccountService.getInvestmentAccountList(investmentAccountSearchForm));
		investmentAccountSearchForm.setNumber("NUSC-BBNK");
		InvestmentAccount toHoldingInvestmentAccount = CollectionUtils.getOnlyElementStrict(this.investmentAccountService.getInvestmentAccountList(investmentAccountSearchForm));
		List<AccountingPositionTransferDetail> detailList = Collections.singletonList(AccountingPositionTransferDetailBuilder.createEmpty().withOriginalPositionOpenDate(DateUtils.toDate("07/11/2019")).withPositionCostBasis(BigDecimal.valueOf(950000)).withExchangeRateToBase(BigDecimal.ONE).withSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true)).toAccountingPositionTransferDetail());
		AccountingPositionTransferType transferType = AccountingPositionTransferTypeBuilder.createClientCashCollateralTransferPost().toAccountingPositionTransferType();
		AccountingPositionTransfer transfer = null;
		AccountingJournal journal = null;
		List<AccountingPeriodClosing> periodsToReClose = null;
		try {
			transfer = AccountingPositionTransferBuilder.createEmpty()
					.withTransactionDate(DateUtils.toDate("07/11/2019")).withSettlementDate(DateUtils.toDate("07/11/2019"))
					.withFromClientInvestmentAccount(toFromClientInvestmentAccount).withToClientInvestmentAccount(toFromClientInvestmentAccount)
					.withFromHoldingInvestmentAccount(fromHoldingInvestmentAccount).withToHoldingInvestmentAccount(toHoldingInvestmentAccount)
					.withNote("Posting of Client Owned Collateral from Collateral Balance Date [07/10/2019]")
					.withDetailList(detailList).withType(transferType).withSourceSystemTable(this.systemSchemaService.getSystemTableByName("CollateralBalance")).withSourceFkFieldId(balance.getId())
					.toAccountPositionTransfer();
			this.accountingPositionTransferService.saveAccountingPositionTransfer(transfer);
			periodsToReClose = this.accountingUtils.openAccountingPeriods(transfer.getBookingDate(), Collections.singletonList(transfer.getFromClientInvestmentAccount().getId()));
			journal = this.accountingBookingService.bookAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, transfer.getId(), true);
			validateViolationExists(balance, "Client & Counterparty Collateral are both posted at the same time. Review prior day collateral movements.");
		}
		finally {
			if (journal != null) {
				this.accountingPostingService.unpostAccountingJournal(journal.getId());
			}
			if (transfer != null && transfer.getId() != null) {
				this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
			}
			this.accountingUtils.closeAccountingPeriods(periodsToReClose);
		}
		validateViolationPassed(balance, "Margin Call Calculations: Client & Counterparty Collateral Posted", true);
	}


	@Test
	public void testMarginCallCalculationsUnbookedCollateralTransfersRule() {
		CollateralBalance balance = getCollateralBalanceUsingHoldingAccount("NUSC-BBNK", DateUtils.toDate("07/10/2019"), OTC_COLLATERAL);
		InvestmentAccountSearchForm investmentAccountSearchForm = new InvestmentAccountSearchForm();
		investmentAccountSearchForm.setNumber("455000");
		InvestmentAccount toFromClientInvestmentAccount = CollectionUtils.getOnlyElementStrict(this.investmentAccountService.getInvestmentAccountList(investmentAccountSearchForm));
		investmentAccountSearchForm.setNumber("689192");
		InvestmentAccount fromHoldingInvestmentAccount = CollectionUtils.getOnlyElementStrict(this.investmentAccountService.getInvestmentAccountList(investmentAccountSearchForm));
		investmentAccountSearchForm.setNumber("NUSC-BBNK");
		InvestmentAccount toHoldingInvestmentAccount = CollectionUtils.getOnlyElementStrict(this.investmentAccountService.getInvestmentAccountList(investmentAccountSearchForm));
		List<AccountingPositionTransferDetail> detailList = Collections.singletonList(AccountingPositionTransferDetailBuilder.createEmpty().withOriginalPositionOpenDate(DateUtils.toDate("07/11/2019")).withPositionCostBasis(BigDecimal.valueOf(950000)).withExchangeRateToBase(BigDecimal.ONE).withSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true)).toAccountingPositionTransferDetail());
		AccountingPositionTransferType transferType = AccountingPositionTransferTypeBuilder.createClientCashCollateralTransferPost().toAccountingPositionTransferType();
		AccountingPositionTransfer transfer = null;
		try {
			transfer = AccountingPositionTransferBuilder.createEmpty()
					.withTransactionDate(DateUtils.toDate("07/11/2019")).withSettlementDate(DateUtils.toDate("07/11/2019"))
					.withFromClientInvestmentAccount(toFromClientInvestmentAccount).withToClientInvestmentAccount(toFromClientInvestmentAccount)
					.withFromHoldingInvestmentAccount(fromHoldingInvestmentAccount).withToHoldingInvestmentAccount(toHoldingInvestmentAccount)
					.withNote("Posting of Client Owned Collateral from Collateral Balance Date [07/10/2019]")
					.withDetailList(detailList).withType(transferType).withSourceSystemTable(this.systemSchemaService.getSystemTableByName("CollateralBalance")).withSourceFkFieldId(balance.getId())
					.toAccountPositionTransfer();
			this.accountingPositionTransferService.saveAccountingPositionTransfer(transfer);
			validateViolationExists(balance, "Unbooked Collateral Transfer: [From 689192: EverSource Retirement Plan Master Trust-Portable Alpha (Custodian) To NUSC-BBNK: NUSC-OTC-BBNK (OTC ISDA) on 07/11/2019 settling on 07/11/2019]");
		}
		finally {
			if (transfer != null && transfer.getId() != null) {
				this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
			}
		}
		validateViolationPassed(balance, "Margin Call Calculations: Unbooked Collateral Transfers", true);
	}


	@Test
	public void testMarginCallCalculationsUnbookedEventsRule() {
		AccountingEventJournal accountingEventJournal = this.accountingEventJournalService.getAccountingEventJournal(302869);
		AccountingJournal accountingJournal = this.accountingJournalService.getAccountingJournalBySource("AccountingEventJournal", accountingEventJournal.getId());
		CollateralBalance collateralBalance = getCollateralBalanceUsingHoldingAccount("AS9-BNYM", accountingEventJournal.getSecurityEvent().getEventDate(), OTC_COLLATERAL);
		List<AccountingPeriodClosing> periodsToReClose = this.accountingUtils.openAccountingPeriods(collateralBalance.getBalanceDate(), accountingJournal.getJournalDetailList().stream().map(definition -> definition.getClientInvestmentAccount().getId()).collect(Collectors.toList()));
		try {
			accountingJournal = this.accountingPostingService.unpostAccountingJournal(accountingJournal.getId());
			validateViolationExists(collateralBalance, "Unbooked Event: [Security Maturity for EUR/GBP20190731 on 07/31/2019]");
		}
		finally {
			if (accountingJournal == null || accountingJournal.getId() == null) {
				EventJournalGeneratorCommand command = EventJournalGeneratorCommand.ofEvent(accountingEventJournal.getSecurityEvent().getId(), AccountingEventJournalGeneratorTypes.POST);
				command.setSeparateJournalPerHoldingAccount(true);
				this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(command);
			}
			this.accountingUtils.closeAccountingPeriods(periodsToReClose);
		}
		validateViolationPassed(collateralBalance, "Margin Call Calculations: Unbooked Events", true);
	}


	@Test
	public void testMarginCallCalculationsUnbookedTradesRule() {
		CollateralBalance collateralBalance = getCollateralBalanceUsingHoldingAccount("NUSC-BBNK", DateUtils.toDate("07/10/2019"), OTC_COLLATERAL);
		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setHoldingInvestmentAccountId(collateralBalance.getHoldingInvestmentAccount().getId());
		tradeSearchForm.setTradeDateLessThanOrEquals(collateralBalance.getBalanceDate());
		tradeSearchForm.setWorkflowStateName("Booked");
		tradeSearchForm.setOrderBy("tradeDate:DESC");
		Trade tradeToCopy = CollectionUtils.getFirstElement(this.tradeService.getTradeList(tradeSearchForm));
		Assertions.assertNotNull(tradeToCopy);
		Trade tradeToSave = this.tradeUtils.copyTrade(tradeToCopy.getId(), tradeToCopy.getQuantity(), collateralBalance.getBalanceDate());
		try {
			this.tradeUtils.saveTrade(tradeToSave);
			validateViolationExists(collateralBalance, "Unbooked Trade: [455000-NUSC-BBNK: BUY 30,844.132734 of 36667485B on 07/10/2019]");
		}
		finally {
			this.tradeUtils.rejectAndCancelTrade(tradeToSave);
		}
		validateViolationPassed(collateralBalance, "Margin Call Calculations: Unbooked Trades", true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CollateralBalance getCollateralBalanceUsingClientInvestmentAccountLabel(String accountLabel, String balanceDate, String collateralTypeName) {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setClientInvestmentAccountLabel(accountLabel);
		searchForm.setBalanceDate(DateUtils.toDate(balanceDate));
		searchForm.setCollateralTypeName(collateralTypeName);
		return CollectionUtils.getOnlyElementStrict(this.collateralBalanceService.getCollateralBalanceList(searchForm));
	}


	private CollateralBalance getCollateralBalanceUsingHoldingAccount(String holdingAccountNumber, Date balanceDate, String collateralTypeName) {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setHoldingInvestmentAccountNumber(holdingAccountNumber);
		searchForm.setBalanceDate(balanceDate);
		searchForm.setCollateralTypeName(collateralTypeName);
		return CollectionUtils.getOnlyElementStrict(this.collateralBalanceService.getCollateralBalanceList(searchForm));
	}
	/////////////////////////////////////////////////////////////////////////////
	////////////             Test Assertion Methods              ////////////////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates Violation with Violation Note exists in the Preview list
	 */
	private void validateViolationExists(CollateralBalance collateralBalance, String violationNoteExpected) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (StringUtils.isEqual(violation.getViolationNote(), violationNoteExpected)) {
				return;
			}
		}
		Assertions.fail("Did not find Expected Violation with Message: " + violationNoteExpected + " for Collateral Balance " + collateralBalance.getLabel());
	}


	/**
	 * Validates Violation with Violation Note exists in the Preview list
	 */
	private void validateViolationExistsRegex(CollateralBalance collateralBalance, String violationNoteExpected) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (violation.getViolationNote().matches(violationNoteExpected)) {
				return;
			}
		}
		Assertions.fail("Did not find Expected Violation with Message Matching: " + violationNoteExpected + " for Collateral Balance " + collateralBalance.getLabel());
	}


	/**
	 * Validates there are no violations in the preview list for the given rule definition name
	 * Note: exists means would still be in the preview list, with the rule definition name as the violation note, and "Passed" as the ignore note
	 * Otherwise it should be missing from the list because the rule scope is set up to not even run it
	 */
	private void validateViolationPassed(CollateralBalance collateralBalance, String definitionName, boolean exists) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(COLLATERAL_BALANCE_TABLE_NAME, collateralBalance.getId());
		RuleViolation firstRuleViolation = null;
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (definitionName.equals(violation.getRuleAssignment().getRuleDefinition().getName())) {
				firstRuleViolation = violation;
				break;
			}
		}
		if (exists) {
			Assertions.assertNotNull(firstRuleViolation, "Rule Definition: " + definitionName + " for Collateral Balance " + collateralBalance.getLabel() + " should have been executed, but it wasn't");
			Assertions.assertEquals("Passed", firstRuleViolation.getIgnoreNote(), "Rule Definition: " + definitionName + " for Collateral Balance " + collateralBalance.getLabel() + " should not have been executed, but found violation with message " + firstRuleViolation.getViolationNote());
		}
		else {
			Assertions.assertNull(firstRuleViolation, "Rule Definition: " + definitionName + " for Collateral Balance " + collateralBalance.getLabel() + " should not have been executed, but it did");
		}
	}
}
