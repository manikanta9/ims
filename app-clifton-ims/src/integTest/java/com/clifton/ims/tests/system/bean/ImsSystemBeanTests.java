package com.clifton.ims.tests.system.bean;


import com.clifton.ims.tests.spring.ImsIntegrationConfiguration;
import com.clifton.test.system.bean.BaseSystemBeanTests;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(classes = {ImsIntegrationConfiguration.class})
public class ImsSystemBeanTests extends BaseSystemBeanTests {

	// NOTHING HERE
}
