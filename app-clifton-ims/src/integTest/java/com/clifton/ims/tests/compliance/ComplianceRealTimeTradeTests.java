package com.clifton.ims.tests.compliance;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.rule.violation.RuleViolation;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Stream;


public abstract class ComplianceRealTimeTradeTests extends BaseImsIntegrationTest {


	protected void validateViolationNotes(List<RuleViolation> ruleViolationList, String... notes) {
		validateViolationNotes(ruleViolationList, false, notes);
	}


	protected void validateViolationNotes(List<RuleViolation> ruleViolationList, boolean useRegexMatcher, String... notes) {
		validateViolationNotes(ruleViolationList, useRegexMatcher, false, notes);
	}


	protected void validateViolationNotes(List<RuleViolation> ruleViolationList, boolean useRegexMatcher, boolean useContains, String... notes) {
		// Verify number of violations
		if (CollectionUtils.getSize(ruleViolationList) != ArrayUtils.getLength(notes)) {
			// Print each violation note before reporting failure
			CollectionUtils.getStream(ruleViolationList)
					.forEach(violation -> System.out.println("Found Violation Note: " + violation.getViolationNote()));
			AssertUtils.fail("Error incorrect amount of rule failures detected. Expected [%d]. Found [%d]. Notes: %s.", ArrayUtils.getLength(notes), CollectionUtils.getSize(ruleViolationList), CollectionUtils.toString(CollectionUtils.getConverted(ruleViolationList, RuleViolation::getViolationNote), 5));
		}

		// Compare violation notes against expected notes
		for (String note : CollectionUtils.createList(notes)) {
			Stream<RuleViolation> violationStream = CollectionUtils.getStream(ruleViolationList)
					.peek(violation -> System.out.println("Found Violation Note: " + violation.getViolationNote()));
			if (useRegexMatcher) {
				// Use regex comparison
				final Pattern violationNotePattern = Pattern.compile(note);
				violationStream = violationStream.filter(violation -> violationNotePattern.matcher(violation.getViolationNote()).find());
			}
			else {
				// Use string equality comparison
				violationStream = violationStream.filter(violation -> useContains ? violation.getViolationNote().contains(note) : violation.getViolationNote().equals(note));
			}
			Optional<RuleViolation> matchingViolationOpt = violationStream.findAny();
			ValidationUtils.assertTrue(matchingViolationOpt.isPresent(), "Expected violation failure not found. Expected [" + note + "]");
		}
	}


	protected void validateViolationNoteExists(List<RuleViolation> ruleViolationList, String note, boolean useContains) {
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(filterViolationList(ruleViolationList, note, useContains)), "Expected violation failure not found. Expected [" + note + "]");
	}


	protected void validateViolationNoteDoesNotExist(List<RuleViolation> ruleViolationList, String note, boolean useContains) {
		ValidationUtils.assertTrue(CollectionUtils.isEmpty(filterViolationList(ruleViolationList, note, useContains)), "Unexpected violation failure found. Did not expect [" + note + "]");
	}


	private List<RuleViolation> filterViolationList(List<RuleViolation> ruleViolations, String note, boolean useContains) {
		Predicate<RuleViolation> filter = useContains ? violation -> violation.getViolationNote().contains(note) : violation -> violation.getViolationNote().equals(note);
		return BeanUtils.filter(ruleViolations, filter);
	}
}
