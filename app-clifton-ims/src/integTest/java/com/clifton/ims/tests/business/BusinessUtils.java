package com.clifton.ims.tests.business;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.client.setup.BusinessClientSetupService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.service.BusinessServiceBusinessServiceProcessingType;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.business.service.search.BusinessServiceBusinessServiceProcessingTypeSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListService;
import com.clifton.test.util.RandomUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


@Component
public class BusinessUtils {

	public static final String CLIENT_RELATIONSHIP_CATEGORY_INSTITUTIONAL = "Institutional";

	public static final String CLIENT_CATEGORY_CLIENT = "Client (Investing Entity)";
	public static final String CLIENT_CATEGORY_CLIENT_GROUP = "Client Group";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Resource
	private BusinessClientSetupService businessClientSetupService;

	@Resource
	private BusinessClientService businessClientService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private BusinessServiceService businessServiceService;

	@Resource
	private SystemListService systemListService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getBusinessCompany(BusinessCompanies company) {
		BusinessCompany result = this.businessCompanyService.getBusinessCompanyByTypeAndName(company.getCompanyType(), company.getCompanyName());
		if (result == null) {
			throw new IllegalArgumentException("Could not find BusinessCompany with type [" + company.getCompanyType()
					+ "] and name [" + company.getCompanyName() + "]. (Was it renamed?)");
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessClientRelationship createBusinessClientRelationship() {
		// Create a BusinessClientRelationship
		BusinessClientRelationship businessClientRelationship = new BusinessClientRelationship();
		businessClientRelationship.setName(RandomUtils.randomNameAndNumber());


		// Default to Institutional
		businessClientRelationship.setRelationshipCategory(this.businessClientSetupService.getBusinessClientRelationshipCategoryByName(CLIENT_RELATIONSHIP_CATEGORY_INSTITUTIONAL));
		SystemList systemList = businessClientRelationship.getRelationshipCategory().getRelationshipTypeSystemList();
		businessClientRelationship.setRelationshipType(this.systemListService.getSystemListItemByListAndValue(systemList.getName(), "Corporation"));
		return this.businessClientService.saveBusinessClientRelationship(businessClientRelationship);
	}


	public BusinessClient getBusinessClientByCompany(int companyId) {
		return this.businessClientService.getBusinessClientByCompany(companyId);
	}


	public BusinessClient createBusinessClient() {
		BusinessClientCategory clientBusinessClientCategory = this.businessClientSetupService.getBusinessClientCategoryByName(CLIENT_CATEGORY_CLIENT);

		// Create a BusinessClientRelationship
		BusinessClientRelationship businessClientRelationship = createBusinessClientRelationship();

		// Create a BusinessClient
		BusinessClient businessClient = new BusinessClient();
		businessClient.setName(RandomUtils.randomNameAndNumber());
		businessClient.setClientRelationship(businessClientRelationship);
		businessClient.setCategory(clientBusinessClientCategory);
		businessClient.setClientType(this.systemListService.getSystemListItemByListAndValue("Business Client Types", "Corporation"));
		return this.businessClientService.saveBusinessClient(businessClient);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Just returns the first one found to make it easy to assign the service and service processing type to a test client account
	 */
	public BusinessServiceBusinessServiceProcessingType getBusinessServiceBusinessServiceProcessingTypeForProcessingType(ServiceProcessingTypes processingType) {
		BusinessServiceBusinessServiceProcessingTypeSearchForm searchForm = new BusinessServiceBusinessServiceProcessingTypeSearchForm();
		searchForm.setProcessingType(processingType);
		BusinessServiceBusinessServiceProcessingType result = CollectionUtils.getFirstElement(this.businessServiceService.getBusinessServiceBusinessServiceProcessingTypeList(searchForm));
		ValidationUtils.assertNotNull(result, "Cannot find a service / service processing type mapping for processing type " + processingType.name());
		return result;
	}
}
