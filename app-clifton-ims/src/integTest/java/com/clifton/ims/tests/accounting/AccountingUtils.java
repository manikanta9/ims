package com.clifton.ims.tests.accounting;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.booking.BookableEntity;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetail;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyRebuildCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.M2MRebuildCommand;
import com.clifton.accounting.m2m.rebuild.AccountingM2MRebuildService;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.accounting.period.search.AccountingPeriodSearchForm;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.otc.swap.trs.OtcTotalReturnSwapEvent;
import com.clifton.otc.swap.trs.OtcTotalReturnSwapService;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingUtils</code> defines several convenience methods related to accounting.
 *
 * @author jgommels
 */
@Component
public class AccountingUtils {

	private static final Logger log = LoggerFactory.getLogger(AccountingUtils.class);

	private static final String JOURNAL_DETAIL_FORMAT = "%-10s %-10s %-12s %-12s %-32s %-20s %-12s %-12s %-20s %-20s %-8s %-11s %-15s %s";
	private static final Object[] JOURNAL_DETAIL_HEADERS = {"ID", "PID", "Client #", "Holding #", "GL Account", "Security", "Price", "Qty", "Local Debit\\Credit", "Base Debit\\Credit", "Opening",
			"Tran Date", "Original Date", "Description"};

	/**
	 * Properties on {@link AccountingJournalDetail} to be excluded in comparison. Primitive booleans cannot be safely compared using object comparison.
	 * A better long-term solution would be to change AccountingJournalDetail to use boxed Boolean types.
	 */
	private static final String[] EXCLUDED_JOURNAL_DETAIL_COMPARE_PROPERTIES = {"newBean", "opening", "closing", "initialMarginPerUnit"};

	public static final String FROM_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME = "From Our Account";
	public static final String TO_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME = "To Our Account";
	public static final String BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME = "Between Our Accounts";

	@Resource
	private AccountingAccountService accountingAccountService;

	@Resource
	private AccountingBalanceService accountingBalanceService;

	@SuppressWarnings("rawtypes")
	@Resource
	private AccountingBookingService accountingBookingService;

	@Resource
	private AccountingEventJournalService accountingEventJournalService;

	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	@Resource
	private AccountingJournalService accountingJournalService;

	@Resource
	private AccountingM2MRebuildService accountingM2MRebuildService;

	@Resource
	private AccountingM2MService accountingM2MService;


	@Resource
	private AccountingPeriodService accountingPeriodService;

	@Resource
	private AccountingPositionDailyService accountingPositionDailyService;

	@Resource
	private AccountingPositionService accountingPositionService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private AccountingTransactionService accountingTransactionService;

	@Resource
	private TradeService tradeService;

	@Resource
	private OtcTotalReturnSwapService otcTotalReturnSwapService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AccountingAccount getAccountingAccount(String name) {
		return this.accountingAccountService.getAccountingAccountByName(name);
	}


	public AccountingPeriod getAccountingPeriodForDate(Date date) {
		return this.accountingPeriodService.getAccountingPeriodForDate(date);
	}


	public List<AccountingPeriodClosing> openAccountingPeriods(Date endDate, List<Integer> clientInvestmentAccountIdList) {
		AccountingPeriodSearchForm accountingPeriodSearchForm = new AccountingPeriodSearchForm();
		accountingPeriodSearchForm.setInactive(true);
		accountingPeriodSearchForm.addSearchRestriction("endDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, endDate);
		accountingPeriodSearchForm.addSearchRestriction("endDate", ComparisonConditions.LESS_THAN_OR_EQUALS, new Date());
		accountingPeriodSearchForm.setOrderBy("endDate:desc");
		List<AccountingPeriod> accountingPeriods = this.accountingPeriodService.getAccountingPeriodList(accountingPeriodSearchForm);
		List<AccountingPeriodClosing> periodsToReClose = new ArrayList<>();
		for (AccountingPeriod period : CollectionUtils.getIterable(accountingPeriods)) {
			for (Integer id : CollectionUtils.getIterable(clientInvestmentAccountIdList)) {
				AccountingPeriodClosing accountingPeriodClosing = this.accountingPeriodService.getAccountingPeriodClosingByPeriodAndAccount(period.getId(), id);
				if (accountingPeriodClosing != null && accountingPeriodClosing.isClosed()) {
					periodsToReClose.add(accountingPeriodClosing);
					this.accountingPeriodService.openAccountingPeriod(period.getId(), id, "TEST OPEN");
				}
			}
		}
		return periodsToReClose;
	}


	public void closeAccountingPeriods(List<AccountingPeriodClosing> periodsToReClose) {
		CollectionUtils.reverse(periodsToReClose);
		for (AccountingPeriodClosing accountingPeriodClosing : periodsToReClose) {
			this.accountingPeriodService.closeAccountingPeriod(accountingPeriodClosing.getAccountingPeriod().getId(), accountingPeriodClosing.getInvestmentAccount().getId(), "Test Closed");
		}
	}


	public AccountingPositionTransferType getAccountingPositionTransferType(String name) {
		return this.accountingPositionTransferService.getAccountingPositionTransferTypeByName(name);
	}


	public AccountingPositionTransfer transferToAccount(AccountInfo accountInfo, InvestmentSecurity security, BigDecimal quantity, Date transactionDate, Date originalPositionOpenDate) {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();

		transfer.setToClientInvestmentAccount(accountInfo.getClientAccount());
		transfer.setToHoldingInvestmentAccount(accountInfo.getHoldingAccount());
		transfer.setType(getAccountingPositionTransferType(TO_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(transactionDate);
		transfer.setNote("Transfer to client account " + accountInfo.getClientAccount().getName() + " and holding account " + accountInfo.getHoldingAccount().getName());

		List<AccountingPositionTransferDetail> transferDetails = new ArrayList<>();
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-10);
		transferDetail.setSecurity(security);
		transferDetail.setPositionCostBasis(new BigDecimal("10000"));
		transferDetail.setCostPrice(BigDecimal.ONE);
		transferDetail.setTransferPrice(transferDetail.getCostPrice());
		transferDetail.setQuantity(quantity);
		transferDetail.setExchangeRateToBase(BigDecimal.ONE);
		transferDetail.setOriginalPositionOpenDate(originalPositionOpenDate);
		transferDetails.add(transferDetail);
		transfer.setDetailList(transferDetails);

		transfer = saveAccountingPositionTransfer(transfer);
		return bookAndPost(transfer);
	}


	public AccountingPositionTransfer transferBetweenAccounts(AccountInfo fromAccount, AccountInfo toAccount, AccountingPosition position, BigDecimal quantity, Date transactionDate,
	                                                          Date originalPositionOpenDate) {
		return transferBetweenAccounts(fromAccount, toAccount, position, quantity, transactionDate, originalPositionOpenDate, true);
	}


	public AccountingPositionTransfer transferBetweenAccounts(AccountInfo fromAccount, AccountInfo toAccount, AccountingPosition position, BigDecimal quantity, Date transactionDate,
	                                                          Date originalPositionOpenDate, boolean bookAndPost) {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();

		transfer.setFromClientInvestmentAccount(fromAccount.getClientAccount());
		transfer.setFromHoldingInvestmentAccount(fromAccount.getHoldingAccount());
		transfer.setToClientInvestmentAccount(toAccount.getClientAccount());
		transfer.setToHoldingInvestmentAccount(toAccount.getHoldingAccount());
		transfer.setType(getAccountingPositionTransferType(BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(transactionDate);

		transfer.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "\nto client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());

		List<AccountingPositionTransferDetail> transferDetails = new ArrayList<>();
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-10);
		transferDetail.setExistingPosition(this.accountingTransactionService.getAccountingTransaction(position.getId()));
		transferDetail.setSecurity(position.getInvestmentSecurity());
		transferDetail.setPositionCostBasis(position.getPositionCostBasis());
		transferDetail.setCostPrice(position.getPrice());
		transferDetail.setTransferPrice(transferDetail.getCostPrice());
		transferDetail.setQuantity(quantity);
		transferDetail.setExchangeRateToBase(BigDecimal.ONE);
		transferDetail.setOriginalPositionOpenDate(originalPositionOpenDate);
		transferDetails.add(transferDetail);
		transfer.setDetailList(transferDetails);

		transfer = saveAccountingPositionTransfer(transfer);
		if (bookAndPost) {
			transfer = bookAndPost(transfer);
		}
		return transfer;
	}


	public AccountingPositionTransfer saveAccountingPositionTransfer(AccountingPositionTransfer transfer) {
		this.accountingPositionTransferService.saveAccountingPositionTransfer(transfer);
		return transfer;
	}


	public AccountingPositionTransfer bookAndPost(AccountingPositionTransfer transfer) {
		return bookAccountingPositionTransfer(transfer);
	}


	public AccountingPositionTransfer bookAccountingPositionTransfer(AccountingPositionTransfer transfer) {
		this.accountingBookingService.bookAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL, transfer.getId(), true);

		//Refresh the AccountingPositionTransfer
		transfer = this.accountingPositionTransferService.getAccountingPositionTransfer(transfer.getId());

		if (log.isInfoEnabled()) {
			log.info(asString(transfer));
		}

		return transfer;
	}


	public List<AccountingPosition> getAccountingPositionList(AccountInfo accountInfo, InvestmentSecurity security, Date transactinoDate) {
		return this.accountingPositionService.getAccountingPositionList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), security.getId(), transactinoDate);
	}


	public List<AccountingBalance> getAccountingBalanceList(InvestmentSecurity security, Date transactionDate, Boolean positionsOnly) {
		AccountingBalanceCommand accountingBalanceCommand = AccountingBalanceCommand.forTransactionDate(transactionDate)
				.withInvestmentSecurityId(security.getId());
		accountingBalanceCommand.setPositionAccountingAccount(positionsOnly);
		return getAccountingBalanceList(accountingBalanceCommand);
	}


	public List<AccountingBalance> getAccountingBalanceList(AccountingBalanceCommand accountingBalanceCommand) {
		return this.accountingBalanceService.getAccountingBalanceList(accountingBalanceCommand);
	}


	public List<AccountingPositionBalance> getAccountingPositionBalanceList(InvestmentSecurity security, Date transactionDate) {
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(transactionDate)
				.forInvestmentSecurity(security.getId());
		return getAccountingPositionBalanceList(positionCommand);
	}


	public List<AccountingPositionBalance> getAccountingPositionBalanceList(InvestmentAccount clientAccount, InvestmentSecurity security, Date transactionDate) {
		AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(transactionDate)
				.forClientAccount(clientAccount.getId())
				.forInvestmentSecurity(security.getId());
		return getAccountingPositionBalanceList(positionCommand);
	}


	public List<AccountingPositionBalance> getAccountingPositionBalanceList(AccountingPositionCommand accountingPositionCommand) {
		return this.accountingPositionService.getAccountingPositionBalanceList(accountingPositionCommand);
	}


	/**
	 * Returns a list of journal details for the first trade fill associated with the trade.
	 *
	 * @param trade                      the trade to get journal details of its first trade fill
	 * @param exceptionIfMoreThanOneFill if true, an exception is thrown if more than one trade fill is found for the given trade.
	 */
	public List<? extends AccountingJournalDetailDefinition> getFirstTradeFillDetails(Trade trade, boolean exceptionIfMoreThanOneFill) {
		return getFirstTradeFillDetails(trade.getId(), exceptionIfMoreThanOneFill);
	}


	/**
	 * Returns a list of journal details for the first trade fill associated with the trade.
	 *
	 * @param tradeId                    the trade to get journal details of its first trade fill
	 * @param exceptionIfMoreThanOneFill if true, an exception is thrown if more than one trade fill is found for the given trade.
	 */
	public List<? extends AccountingJournalDetailDefinition> getFirstTradeFillDetails(int tradeId, boolean exceptionIfMoreThanOneFill) {
		List<TradeFill> fills = this.tradeService.getTradeFillListByTrade(tradeId);

		if (exceptionIfMoreThanOneFill && CollectionUtils.getSize(fills) > 1) {
			throw new ValidationException("The TradeFill list was greater than 1.");
		}
		Assertions.assertEquals(1, fills.size());

		return getTradeFillDetails(fills.get(0));
	}


	public List<? extends AccountingJournalDetailDefinition> getTradeFillDetails(TradeFill fill) {
		return getTradeFillDetails(fill.getId());
	}


	public List<? extends AccountingJournalDetailDefinition> getTradeFillDetails(int id) {
		return this.accountingJournalService.getAccountingJournalBySource(TradeFill.TABLE_NAME, id).getJournalDetailList();
	}


	public AccountingEventJournal generateEventJournal(InvestmentSecurityEvent event, InvestmentAccount clientAccount) {
		// Process Event for this new position
		EventJournalGeneratorCommand command = EventJournalGeneratorCommand.ofEvent(event.getId(), AccountingEventJournalGeneratorTypes.GENERATE_PREVIEWED);
		command.setClientAccountId(clientAccount.getId());

		this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(command);

		AccountingEventJournalSearchForm journalSearchForm = new AccountingEventJournalSearchForm();
		journalSearchForm.setClientInvestmentAccountId(clientAccount.getId());
		journalSearchForm.setSecurityEventId(event.getId());
		List<AccountingEventJournal> journalList = this.accountingEventJournalService.getAccountingEventJournalList(journalSearchForm);
		ValidationUtils.assertTrue(CollectionUtils.getSize(journalList) == 1, "Expected one AccountingEventJournal generated for event " + event.getLabel());

		// Pull journal again with detail list populated - Should only have one detail
		return this.accountingEventJournalService.getAccountingEventJournal(journalList.get(0).getId());
	}


	public AccountingEventPostResult generateAndPostEventJournal(InvestmentSecurityEvent event, InvestmentAccount clientAccount) {
		return generateAndPostEventJournal(event, clientAccount, null);
	}


	public AccountingEventPostResult generateAndPostEventJournal(InvestmentSecurityEvent event, InvestmentAccount clientAccount, Date generationDate) {

		// Generate the event journals
		EventJournalGeneratorCommand command = EventJournalGeneratorCommand.ofEvent(event.getId(), AccountingEventJournalGeneratorTypes.POST);
		command.setGenerationDate(generationDate);
		command.setClientAccountId(clientAccount.getId());
		this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(command);

		// Get the AccountingEventJournal that was generated
		AccountingEventJournalSearchForm searchForm = new AccountingEventJournalSearchForm();
		searchForm.setSecurityEventId(event.getId());
		searchForm.setClientInvestmentAccountId(clientAccount.getId());
		AccountingEventJournal eventJournal = CollectionUtils.getFirstElement(this.accountingEventJournalService.getAccountingEventJournalList(searchForm));
		if (eventJournal == null) {
			return new AccountingEventPostResult(eventJournal, null, null);
		}
		// Look up event journal to hydrate details
		eventJournal = this.accountingEventJournalService.getAccountingEventJournal(eventJournal.getId());

		AccountingJournal mainJournal = getJournal(eventJournal, 1);
		if (mainJournal != null && log.isInfoEnabled()) {
			log.info(asString(eventJournal, mainJournal.getJournalDetailList()), false, "Main Journal");
		}

		AccountingJournal accrualReversalJournal = getJournal(eventJournal, 2);
		if (accrualReversalJournal != null && log.isInfoEnabled()) {
			log.info(asString(eventJournal, accrualReversalJournal.getJournalDetailList(), false, "Reverse Accrual"));
		}

		return new AccountingEventPostResult(eventJournal, mainJournal, accrualReversalJournal);
	}


	public void postOtcTrsSwapEventsForDateRange(InvestmentSecurity security, InvestmentAccount clientAccount, Date startDate, Date endDate) {
		List<OtcTotalReturnSwapEvent> events = this.otcTotalReturnSwapService.getOtcTotalReturnSwapEventList(security.getId());
		BeanUtils.sortWithFunction(events, event -> event.getEquityLeg().getEventDate(), true);

		for (OtcTotalReturnSwapEvent event : CollectionUtils.getIterable(events)) {
			InvestmentSecurityEvent interestEvent = event.getInterestLeg();
			InvestmentSecurityEvent equityEvent = event.getEquityLeg();

			Date eventDate = equityEvent.getEventDate();

			if (DateUtils.isDateBetween(eventDate, startDate, endDate, false)) {
				this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(EventJournalGeneratorCommand.ofEventForClientAccount(interestEvent.getId(), clientAccount.getId(), AccountingEventJournalGeneratorTypes.POST_PREVIEWED));

				this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(EventJournalGeneratorCommand.ofEventForClientAccount(equityEvent.getId(), clientAccount.getId(), AccountingEventJournalGeneratorTypes.POST_PREVIEWED));

				log.info("Posted {} for security {}", event.getEventType(), security.getSymbol());
			}
		}
	}


	public List<? extends AccountingJournalDetailDefinition> getPositionTransferDetails(AccountingPositionTransfer transfer) {
		AccountingJournal journal = this.accountingJournalService.getAccountingJournalBySource("AccountingPositionTransfer", transfer.getId());
		return journal.getJournalDetailList();
	}


	public AccountingJournal getJournal(BookableEntity bookableEntity) {
		return getJournal(bookableEntity, 1);
	}


	public AccountingJournal getJournal(BookableEntity bookableEntity, int sequence) {
		String tableName = bookableEntity.getClass().getSimpleName();
		Integer id = (Integer) bookableEntity.getIdentity();
		return this.accountingJournalService.getAccountingJournalBySourceAndSequence(tableName, id, sequence);
	}


	/**
	 * @param trade             the trade to retrieve fields for such as the client and holding accounts
	 * @param accountingAccount the {@link AccountingAccount} to expect
	 * @param price             the expected price
	 * @param description       the expected description
	 * @param security          the expected security.
	 * @return a new journal detail representing an expected detail to compare against
	 * @deprecated Use {@link AccountingJournalDetailBuilder} instead. This method will likely be removed unless we decide to use factory methods with long signatures like this as opposed to a builder.
	 * <p>
	 * Creates a new {@link AccountingJournalDetailDefinition} to compare against. If a field shouldn't be compared against, then null
	 * can be passed for that parameter. <code>trade</code> should not be null.
	 */
	@Deprecated
	public AccountingJournalDetailDefinition generateExpectedJournalDetail(Trade trade, AccountingAccount accountingAccount, BigDecimal price, BigDecimal quantity, String description,
	                                                                       InvestmentSecurity security) {
		AccountingJournalDetailDefinition expected = new AccountingJournalDetail();
		expected.setAccountingAccount(accountingAccount);
		expected.setClientInvestmentAccount(trade.getClientInvestmentAccount());
		expected.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
		expected.setExecutingCompany(trade.getExecutingBrokerCompany());
		expected.setInvestmentSecurity(security);
		expected.setSettlementDate(trade.getSettlementDate());
		expected.setTransactionDate(trade.getTradeDate());
		expected.setPrice(price);
		expected.setQuantity(quantity);
		expected.setDescription(description);

		return expected;
	}


	/**
	 * Generate AccountingJournalDetail from string parameters
	 */
	public static AccountingJournalDetail createAccountingJournalDetail(String accountingAccountName, String description, String clientInvestmentAccountNumber, String holdingAccountNumber, String securitySymbol, String securityQuantity, String securityPrice, String localDebitCredit, String baseDebitCredit, String positionCostBasis, String exchangeRateToBase, String transactionDate, String settlementDate, String originalTransactionDate) {
		return createAccountingJournalDetail(accountingAccountName, description, clientInvestmentAccountNumber, holdingAccountNumber, securitySymbol, securityQuantity, securityPrice, localDebitCredit, baseDebitCredit, positionCostBasis, exchangeRateToBase, transactionDate, settlementDate, originalTransactionDate, null);
	}


	/**
	 * Generate AccountingJournalDetail from string parameters
	 */
	public static AccountingJournalDetail createAccountingJournalDetail(String accountingAccountName, String description, String clientInvestmentAccountNumber, String holdingAccountNumber, String securitySymbol, String securityQuantity, String securityPrice, String localDebitCredit, String baseDebitCredit, String positionCostBasis, String exchangeRateToBase, String transactionDate, String settlementDate, String originalTransactionDate, String executingCompany) {
		AccountingJournalDetail detail = new AccountingJournalDetail();
		AccountingAccount accountingAccount = new AccountingAccount();
		InvestmentAccount investmentAccount = new InvestmentAccount();
		InvestmentAccount holdingAccount = new InvestmentAccount();
		InvestmentSecurity security = new InvestmentSecurity();
		BusinessCompany businessCompany = new BusinessCompany();

		if (executingCompany == null) {
			executingCompany = "Goldman Sachs International";
		}

		businessCompany.setName(executingCompany);
		accountingAccount.setName(accountingAccountName);
		detail.setDescription(description);
		investmentAccount.setNumber(clientInvestmentAccountNumber);
		holdingAccount.setNumber(holdingAccountNumber);
		security.setSymbol(securitySymbol);

		if (securityPrice != null) {
			detail.setPrice(new BigDecimal(securityPrice));
		}
		if (securityQuantity != null) {
			detail.setQuantity(new BigDecimal(securityQuantity));
		}

		detail.setLocalDebitCredit(new BigDecimal(localDebitCredit));
		detail.setBaseDebitCredit(new BigDecimal(baseDebitCredit));
		detail.setPositionCostBasis(new BigDecimal(positionCostBasis));
		detail.setExchangeRateToBase(new BigDecimal(exchangeRateToBase));
		detail.setTransactionDate(DateUtils.toDate(transactionDate));
		detail.setSettlementDate(DateUtils.toDate(settlementDate));
		detail.setOriginalTransactionDate(DateUtils.toDate(originalTransactionDate));

		detail.setAccountingAccount(accountingAccount);
		detail.setClientInvestmentAccount(investmentAccount);
		detail.setHoldingInvestmentAccount(holdingAccount);
		detail.setInvestmentSecurity(security);
		detail.setExecutingCompany(businessCompany);

		return detail;
	}


	public String createMarkToMarket(Date date, InvestmentAccount holdingInvestmentAccount, BusinessCompany holdingAccountIssuingCompany, InvestmentAccountType holdingAccountType) {
		M2MRebuildCommand command = new M2MRebuildCommand(date, null, null, null, null, false);
		command.setHoldingInvestmentAccountId(holdingInvestmentAccount.getId());
		command.setAsynchronous(false);
		command.setHoldingAccountIssuingCompanyId(holdingAccountIssuingCompany.getId());
		command.setHoldingAccountTypeId(holdingAccountType.getId());

		return this.accountingM2MRebuildService.rebuildAccountingM2MDaily(command);
	}


	public List<AccountingM2MDaily> getAccountingM2MDailyList(InvestmentAccount clientInvestmentAccount, InvestmentAccount holdingInvestmentAccount, Date date) {
		AccountingM2MDailySearchForm accountingM2MDailySearchForm = new AccountingM2MDailySearchForm();
		accountingM2MDailySearchForm.setMarkDate(date);
		accountingM2MDailySearchForm.setClientInvestmentAccountId(clientInvestmentAccount.getId());
		accountingM2MDailySearchForm.setHoldingInvestmentAccountId(holdingInvestmentAccount.getId());
		accountingM2MDailySearchForm.setHoldingInvestmentAccountTypeId(holdingInvestmentAccount.getType().getId());

		return this.accountingM2MService.getAccountingM2MDailyList(accountingM2MDailySearchForm);
	}


	public void updateAccountingM2MDailyAmounts(AccountingM2MDaily accountingM2MDaily, BigDecimal amount) {
		accountingM2MDaily.setOurTransferAmount(amount);
		accountingM2MDaily.setExpectedTransferAmount(amount);
		accountingM2MDaily.setTransferAmount(amount);
		this.accountingM2MService.saveAccountingM2MDaily(accountingM2MDaily);
	}


	public int reconcileAccountingM2MDaily(Date m2mDate, BigDecimal thresholdAmount, InvestmentAccountType holdingAccountType) {
		return this.accountingM2MRebuildService.reconcileAccountingM2MDaily(
				m2mDate,
				null,
				null,
				thresholdAmount,
				false,
				true,
				holdingAccountType.getId(),
				null
		);
	}


	public int bookAndPostAccountingM2MDaily(Date m2mDate, InvestmentAccountType holdingAccountType, InvestmentAccount clientInvestmentAccount) {
		return this.accountingM2MRebuildService.bookAndPostAccountingM2MDaily(
				m2mDate,
				holdingAccountType.getId(),
				null,
				null,
				clientInvestmentAccount.getId(),
				false,
				false
		);
	}


	/**
	 * Iterates through the list of expected and actual journal details and compares them in order by checking the equality of non-null values from the
	 * <code>expected</code> list. If the lists are not equal in order and contents, then an exception is thrown.
	 *
	 * @param expected the list of expected journal details. Only non-null values are compared with the <code>actual</code> parameter.
	 * @param actual   the actual list of journal details
	 */
	public static void assertJournalDetailList(List<? extends AccountingJournalDetailDefinition> expected, List<? extends AccountingJournalDetailDefinition> actual, String... includedProperties) {
		StringBuilder errorBuilder = new StringBuilder();
		errorBuilder.append("The expected journal details were: \n");
		errorBuilder.append(asString(null, expected, true, null));
		errorBuilder.append("\n\nThe actual journal details were: \n");
		errorBuilder.append(asString(null, actual, true, null));

		Assertions.assertEquals(expected.size(), actual.size(), errorBuilder.toString());

		for (int i = 0; i < actual.size(); i++) {
			AccountingJournalDetailDefinition expectedDetail = expected.get(i);
			AccountingJournalDetailDefinition actualDetail = actual.get(i);
			try {
				if (includedProperties != null && includedProperties.length > 0) {
					CoreCompareUtils.validateNonNullPropertiesEqual(expectedDetail, actualDetail, includedProperties);
				}
				else {
					assertJournalDetail(expectedDetail, actualDetail);
				}
			}
			catch (Throwable e) {
				errorBuilder.append("\n\n");
				errorBuilder.append("There was a validation error for the expected journal detail #").append(i + 1).append(": \n");
				errorBuilder.append(formatJournalDetailExcelFriendly(expectedDetail, true));
				errorBuilder.append("\n\n");
				errorBuilder.append("The actual journal detail at this position was: \n");
				errorBuilder.append(formatJournalDetailExcelFriendly(actualDetail, true));
				errorBuilder.append("\n\n");

				throw new RuntimeException(errorBuilder.toString(), e);
			}
		}
	}


	/**
	 * Compares the non-null properties of the expected journal detail with the actual journal detail and throws an exception if any of the compared properties do not match.
	 */
	public static void assertJournalDetail(AccountingJournalDetailDefinition expected, AccountingJournalDetailDefinition actual) {
		CoreCompareUtils.isNonNullPropertiesEqual(expected, actual, true, null, EXCLUDED_JOURNAL_DETAIL_COMPARE_PROPERTIES);
		Assertions.assertEquals(expected.isOpening(), actual.isOpening(), "Expected Opening flag to be " + expected.isOpening());
		Assertions.assertEquals(expected.isClosing(), actual.isClosing(), "Expected Closing flag to be " + expected.isClosing());
	}


	public void assertPositionOpen(Trade trade) {
		//Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> buyDetails = getFirstTradeFillDetails(trade, true);

		BigDecimal expectedQuantity;
		if (trade.isBuy()) {
			expectedQuantity = trade.getQuantityIntended();
		}
		else {
			expectedQuantity = trade.getQuantityIntended().negate();
		}

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(trade);

		//Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
		expectedListForBuy.add(detailBuilder.accountingAccount(getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(expectedQuantity)
				.opening(true)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.POSITION_OPENING, trade.getInvestmentSecurity())
				.build());

		//Validate the buy trade
		AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);
	}


	public String asString(BookableEntity bookableEntity) {
		return asString(bookableEntity, getJournal(bookableEntity).getJournalDetailList());
	}


	public static String asString(BookableEntity bookableEntity, List<? extends AccountingJournalDetailDefinition> journalDetails) {
		return asString(bookableEntity, journalDetails, false, null);
	}


	public static String asString(List<? extends AccountingJournalDetailDefinition> journalDetails, boolean excelFriendlyFormat, String descriptionPrefix) {
		return asString(null, journalDetails, excelFriendlyFormat, descriptionPrefix);
	}


	public static String asString(BookableEntity bookableEntity, List<? extends AccountingJournalDetailDefinition> journalDetails, boolean excelFriendlyFormat, String descriptionPrefix) {
		@SuppressWarnings("unchecked")
		List<AccountingJournalDetail> details = (List<AccountingJournalDetail>) journalDetails;

		StringBuilder sb = new StringBuilder();
		if (descriptionPrefix != null) {
			sb.append("'");
			sb.append(descriptionPrefix);
			sb.append("' ");
		}

		sb.append("Journal Details ");

		if (bookableEntity != null) {
			String table = bookableEntity.getClass().getSimpleName();
			Integer id = (Integer) bookableEntity.getIdentity();

			sb.append(" for ");
			sb.append(table);
			sb.append(" ");
			sb.append(id);
			if (bookableEntity instanceof TradeFill) {
				sb.append(" of Trade ");
				sb.append(((TradeFill) bookableEntity).getTrade().getIdentity());
			}
		}

		if (excelFriendlyFormat) {
			sb.append(" (Excel-Friendly Format)");
		}
		sb.append(":\n");

		if (excelFriendlyFormat) {
			sb.append(formatJournalDetailExcelFriendly(CollectionUtils.getFirstElementStrict(details), true));

			for (int i = 1; i < CollectionUtils.getSize(details); i++) {
				sb.append("\n");
				sb.append(formatJournalDetailExcelFriendly(details.get(i), false));
			}
		}
		else {
			sb.append(String.format(JOURNAL_DETAIL_FORMAT, JOURNAL_DETAIL_HEADERS));

			for (AccountingJournalDetail detail : CollectionUtils.getIterable(details)) {
				sb.append("\n");
				sb.append(formatJournalDetailPretty(detail));
			}
		}

		return sb.toString();
	}


	public static String formatJournalDetailPretty(AccountingJournalDetailDefinition journalDetail) {
		String glAccount = journalDetail.getAccountingAccount().getName();

		return String.format(JOURNAL_DETAIL_FORMAT, journalDetail.getIdentity(),
				journalDetail.getParentTransaction() != null ? journalDetail.getParentTransaction().getId() : (journalDetail.getParentDefinition() != null ? journalDetail.getParentDefinition()
						.getIdentity() : null), journalDetail.getClientInvestmentAccount().getNumber(), journalDetail.getHoldingInvestmentAccount().getNumber(), glAccount.substring(0,
						Math.min(32, glAccount.length())), journalDetail.getInvestmentSecurity().getSymbol(), CoreMathUtils.formatNumberDecimal(journalDetail.getPrice()),
				CoreMathUtils.formatNumberDecimal(journalDetail.getQuantity()), CoreMathUtils.formatNumberDecimal(journalDetail.getLocalDebitCredit()),
				CoreMathUtils.formatNumberDecimal(journalDetail.getBaseDebitCredit()), journalDetail.isOpening(), DateUtils.fromDateShort(journalDetail.getTransactionDate()),
				DateUtils.fromDateShort(journalDetail.getOriginalTransactionDate()), journalDetail.getDescription());
	}


	public static String formatJournalDetailExcelFriendly(AccountingJournalDetailDefinition journalDetail, boolean includeHeader) {
		return journalDetail.toStringFormatted(includeHeader);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////           Accounting Position Daily Rebuilds          /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks if snapshots exist on start - if they do - does nothing, otherwise rebuilds snapshots for the client account and period
	 */
	public void rebuildAccountingPositionDailySnapshot(int clientAccountId, Date startDate, Date endDate) {
		Assertions.assertNotNull(startDate, "Start Date is required");
		Assertions.assertNotNull(endDate, "End Date is required");
		// Also check if records exist on the start date, then APD had previously been rebuilt so not necessary
		// Will speed up subsequent tests after first run until db is restored again
		AccountingPositionDailySearchForm snapshotSearchForm = new AccountingPositionDailySearchForm();
		snapshotSearchForm.setClientInvestmentAccountId(clientAccountId);
		snapshotSearchForm.setPositionDate(startDate);
		snapshotSearchForm.setLimit(1); // Don't need all of the results returned, just need to know if one exists
		if (CollectionUtils.isEmpty(RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.accountingPositionDailyService.getAccountingPositionDailyList(snapshotSearchForm), "id"))) {
			AccountingPositionDailyRebuildCommand command = new AccountingPositionDailyRebuildCommand();
			command.setClientAccountId(clientAccountId);
			command.setStartSnapshotDate(startDate);
			command.setEndSnapshotDate(endDate);
			command.setSynchronous(true);
			this.accountingPositionDailyService.rebuildAccountingPositionDaily(command);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	//////////      Journal Detail Validation Utils             ///////////////
	///////////////////////////////////////////////////////////////////////////


	public static void assertJournalDetailList(AccountingJournal journal, String... expectedJournalDetails) {
		assertJournalDetailList(journal, expectedJournalDetails, false);
	}


	public static void assertJournalDetailList(AccountingJournal journal, String[] expectedJournalDetails, boolean printTransactions) {
		assertJournalDetailList(journal, expectedJournalDetails, printTransactions, false);
	}


	public static void assertJournalDetailList(AccountingJournal journal, String[] expectedJournalDetails, boolean printTransactions, boolean withAdditionalFields) {
		String[] actualDetailStrings = CollectionUtils.getConverted(journal.getJournalDetailList(), detail -> {
			detail.setJournal(journal);
			return detail.toStringFormatted(false, withAdditionalFields);
		}).toArray(new String[0]);
		assertJournalDetailList(actualDetailStrings, expectedJournalDetails, printTransactions);
	}


	public static void assertJournalDetailList(List<? extends AccountingJournalDetailDefinition> journalDetailList, String... expectedJournalDetails) {
		assertJournalDetailList(journalDetailList, expectedJournalDetails, false);
	}


	public static void assertJournalDetailList(List<? extends AccountingJournalDetailDefinition> journalDetailList, String[] expectedJournalDetails, boolean printTransactions) {
		assertJournalDetailList(journalDetailList, expectedJournalDetails, printTransactions, false);
	}


	public static void assertJournalDetailList(List<? extends AccountingJournalDetailDefinition> journalDetailList, String[] expectedJournalDetails, boolean printTransactions, boolean withAdditionalFields) {
		String[] actualDetailStrings = CollectionUtils.getStream(journalDetailList)
				.map(detail -> detail.toStringFormatted(false, withAdditionalFields))
				.toArray(String[]::new);
		assertJournalDetailList(actualDetailStrings, expectedJournalDetails, printTransactions);
	}


	public static void assertJournalDetailList(String[] actualDetails, String[] expectedDetails) {
		assertJournalDetailList(actualDetails, expectedDetails, false);
	}


	public static void assertJournalDetailList(String[] actualDetails, String[] expectedDetails, boolean printTransactions) {
		if (printTransactions) {
			System.out.println("Details:\n" + String.join("\n", actualDetails) + '\n');
		}

		if (expectedDetails.length != actualDetails.length) {
			failJournalDetailValidation(actualDetails, "Journal Detail count mismatch. Expected: " + expectedDetails.length + ", Actual: " + actualDetails.length, printTransactions);
		}

		List<String> failedDetails = new ArrayList<>();
		for (int i = 0; i < expectedDetails.length; i++) {
			String[] expectedValues = expectedDetails[i].split("	");
			String[] actualValues = actualDetails[i].split("	");
			boolean mismatch = false;
			// skip journal ID, Parent ID, and client and holding accounts from validation
			for (int j = 4; j < expectedValues.length; j++) {
				if (!expectedValues[j].equals(actualValues[j])) {
					mismatch = true;
					break;
				}
			}
			if (mismatch) {
				failedDetails.add(String.format("Detail %d:%n\tExpect: %s%n\tActual: %s", i + 1, String.join("	", expectedValues), String.join("	", actualValues)));
			}
		}
		if (!CollectionUtils.isEmpty(failedDetails)) {
			failJournalDetailValidation(actualDetails, "Journal Detail validation failed! Details with mismatch include:\n" + String.join("\n", failedDetails), printTransactions);
		}
	}


	private static void failJournalDetailValidation(String[] actualDetails, String message, boolean printTransactions) {
		if (!printTransactions) {
			System.out.println("Full details upon error:\n" + String.join("\n", actualDetails));
		}
		Assertions.fail(message);
	}
}
