package com.clifton.ims.tests.lending.repo;


public enum LendingRepoTypes {

	OVERNIGHT("Overnight"), TERM("Term"), OPEN("Open"), OPEN_TRI_PARTY("Open Tri-Party"), TERM_TRI_PARTY("Term Tri-Party"), OVERNIGHT_TRI_PARTY("Overnight Tri-Party");

	private final String name;



	LendingRepoTypes(String name) {
		this.name = name;

	}


	public String getName() {
		return this.name;
	}



}
