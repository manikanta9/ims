package com.clifton.ims.tests.rule.setup;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Test the setup of rule related entities to ensure they follow our conventions
 */
public abstract class RuleCategoryTests extends RuleSetupBaseTests {

	@Override
	protected void verifyRuleCategoryName(RuleCategory ruleCategory) {
		//Each word starts with capital letter and name ends in RULE_CATEGORY_NAME_SUFFIX
		//Ensure description is not null
		ValidationUtils.assertNotNull(ruleCategory.getDescription(), "Description empty for Rule Category {" + ruleCategory.getLabel() + "}");
		String categoryName = ruleCategory.getName();
		ValidationUtils.assertTrue(categoryName.endsWith(this.RULE_CATEGORY_NAME_SUFFIX), "Category {" + ruleCategory.getLabel() + "} does not end in required suffix {" + this.RULE_CATEGORY_NAME_SUFFIX + "}");
		List<String> words = StringUtils.delimitedStringToList(categoryName, " ");
		ValidationUtils.assertNotNull(words, "Multiple words delimited by a space not found in Category Name {" + categoryName + "}, does this name only contain one word?");

		String lowerCaseWords = words.stream().filter(s -> Character.isLowerCase(s.charAt(0))).collect(Collectors.joining(","));
		ValidationUtils.assertTrue(StringUtils.isEmpty(lowerCaseWords), "Category Name Words must all be Capital, found lower case words {" + lowerCaseWords + "}");
	}


	@Override
	protected void verifyRuleCategoryTable(RuleCategory ruleCategory) {
		//First word of category name equals the first word in the category table
		String categoryNamePrefix = (ruleCategory.getName().contains(" ")) ? ruleCategory.getName().split(" ")[0] : ruleCategory.getName();
		ValidationUtils.assertTrue(ruleCategory.getCategoryTable().getName().startsWith(categoryNamePrefix), "Category Name Starts with {" + categoryNamePrefix + "} the Category Table Name must be the same but found {" + ruleCategory.getCategoryTable().getName() + "}");

		if (!EXCLUDED_TABLE_VERIFICATION.contains(ruleCategory.getCategoryTable().getName())) {
			ValidationUtils.assertNotNull(ruleCategory.getCategoryTable().getDetailScreenClass(), "Category Table [" + ruleCategory.getCategoryTable().getName() + "] Detail Screen Class Not Set. Cannot dynamically link to entities in UI without it.");
		}
	}


	@Override
	protected void verifyRuleCategoryEvaluatorContextBean(RuleCategory ruleCategory) {
		//Category Rule Category Evaluator Context Bean must end with RULE_CATEGORY_EVALUATOR_CONTEXT_BEAN_SUFFIX
		ValidationUtils.assertTrue(ruleCategory.getRuleEvaluatorContextBean().getName().endsWith(this.RULE_CATEGORY_EVALUATOR_CONTEXT_BEAN_SUFFIX), "Rule Category Evaluator Context Name must end with {" + this.RULE_CATEGORY_EVALUATOR_CONTEXT_BEAN_SUFFIX + "} but is {" + ruleCategory.getRuleEvaluatorContextBean().getName() + "}");
	}


	@Override
	protected void verifyRuleCategorySecurityResource(RuleCategory ruleCategory) {
		//Needed?
	}


	@Override
	protected void verifyRuleCategoryAdditionalScopes(RuleCategory ruleCategory) {
		//Scope path must be a bean property from the category table columns
		//Replace scope section with a SystemColumn drop-down scoped by the selected Category table. Move attributes there.
		//SystemColumns do not have a reference to what System Table they are for therefore we can only verify the first level of the path

		String scopeBeanPath = ruleCategory.getAdditionalScopeBeanFieldPath();
		scopeBeanPath = (scopeBeanPath.contains(".")) ? AssertUtils.assertNotNull(StringUtils.delimitedStringToList(scopeBeanPath, "\\."), "Scope path is null.").get(0) : scopeBeanPath;
		ValidationUtils.assertTrue(beanPropertyNameExistsForSystemTable(ruleCategory.getCategoryTable(), scopeBeanPath), "First level Additional Scope Path {" + scopeBeanPath + "} does not exist on a System Column Bean Property defined in specified Category Table {" + ruleCategory.getCategoryTable().getName() + "}");
	}


	private boolean beanPropertyNameExistsForSystemTable(SystemTable systemTable, String beanPropertyName) {
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setTableId(systemTable.getId());
		searchForm.setBeanPropertyNameEquals(beanPropertyName);
		int listSize = CollectionUtils.getSize(this.systemColumnService.getSystemColumnList(searchForm));
		ValidationUtils.assertTrue(listSize <= 1, "System Table {" + systemTable.getName() + "} has {" + listSize + "} columns which the same bean property name {" + beanPropertyName + "}");
		return listSize == 1;
	}


	@Override
	protected StringBuilder verifyRuleDefinitionListForCategory(RuleCategory ruleCategory) {
		StringBuilder ruleCategoryViolationBuilder = new StringBuilder();
		for (RuleDefinition ruleDefinition : CollectionUtils.getIterable(getRuleDefinitionListForVerification(ruleCategory))) {
			StringBuilder ruleDefinitionViolationBuilder = new StringBuilder();
			try {
				verifyName(ruleDefinition);
			}
			catch (Exception e) {
				appendToBuilder(ruleDefinitionViolationBuilder, "Verify Name", e.getMessage());
			}
			try {
				verifyCauseTable(ruleDefinition);
			}
			catch (Exception e) {
				appendToBuilder(ruleDefinitionViolationBuilder, "Verify Cause Table", e.getMessage());
			}
			try {
				verifyEvaluatorBean(ruleDefinition);
			}
			catch (Exception e) {
				e.printStackTrace();
				appendToBuilder(ruleDefinitionViolationBuilder, "Verify Evaluator Bean", e.getMessage());
			}
			try {
				verifyFreemarkerNoteTemplate(ruleDefinition);
			}
			catch (Exception e) {
				appendToBuilder(ruleDefinitionViolationBuilder, "Verify Freemarker Note Template", e.getMessage());
			}

			if (!StringUtils.isEmpty(ruleDefinitionViolationBuilder.toString())) {
				//There was a failure for this Definition, Record it at the Category Level
				appendToBuilder(ruleCategoryViolationBuilder, ruleDefinition.getName().replace("\"", "'"), "{" + ruleDefinitionViolationBuilder.toString() + "}");
			}
		}
		return StringUtils.isEmpty(ruleCategoryViolationBuilder.toString()) ? null : ruleCategoryViolationBuilder;
	}


	protected abstract List<RuleDefinition> getRuleDefinitionListForVerification(RuleCategory ruleCategory);


	protected abstract void verifyCauseTable(RuleDefinition ruleDefinition);


	protected abstract void verifyFreemarkerNoteTemplate(RuleDefinition ruleDefinition);


	protected abstract void verifyName(RuleDefinition ruleDefinition);


	protected abstract void verifyEvaluatorBean(RuleDefinition ruleDefinition);
}
