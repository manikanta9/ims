package com.clifton.ims.tests.compliance;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.search.BusinessContractSearchForm;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentSearchForm;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import javax.annotation.Nullable;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author MitchellF
 */
public class ComplianceExecutingCompanyMarketDataRangeRuleTests extends ComplianceRealTimeTradeTests {

	@Resource
	public SystemBeanService systemBeanService;

	@Resource
	public MarketDataFieldService marketDataFieldService;

	@Resource
	public ComplianceRuleService complianceRuleService;

	@Resource
	public InvestmentSetupService investmentSetupService;

	@Resource
	public InvestmentAccountService investmentAccountService;

	@Resource
	public BusinessContractService businessContractService;


	private SystemBean createRuleEvaluatorBean(BusinessCompanies companyName, @Nullable BigDecimal minValue, @Nullable BigDecimal maxValue) {
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(companyName);
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CAIG1U5 CBIN");
		MarketDataField dataField = this.marketDataFieldService.getMarketDataFieldByName("Entity Spread - Mid Flat Spread");
		SystemBean ruleEvaluatorBean = SystemBeanBuilder.newBuilder("Executing Company Market Data Range Evaluator Bean", "Compliance Executing Company Market Data Range Rule Evaluator", this.systemBeanService)
				.addProperty("Executing Companies", executingBroker.getId().toString())
				.addProperty("Benchmark Security", security.getId().toString())
				.addProperty("Market Data Field", dataField.getId().toString())
				.addProperty("Minimum Value", minValue != null ? minValue.toString() : null)
				.addProperty("Maximum Value", maxValue != null ? maxValue.toString() : null)
				.addProperty("Flexible Market Data Lookup", "true")
				.build();

		return ruleEvaluatorBean;
	}


	private ComplianceRule createRule(BusinessCompanies companyName, @Nullable BigDecimal minValue, @Nullable BigDecimal maxValue) {

		SystemBean ruleEvaluatorBean = createRuleEvaluatorBean(companyName, minValue, maxValue);

		ComplianceRule marketDataRangeRule = new ComplianceRule();
		marketDataRangeRule.setName("Executing Company Market Data Range Rule");
		marketDataRangeRule.setDescription("Executing Company Market Data Range Rule");
		marketDataRangeRule.setRuleEvaluatorBean(ruleEvaluatorBean);
		marketDataRangeRule.setRuleType(this.complianceRuleService.getComplianceRuleTypeByName("Executing Company Market Data Range"));
		marketDataRangeRule.setBatch(true);
		marketDataRangeRule.setRealTime(true);
		marketDataRangeRule.setCurrencyType(InvestmentCurrencyTypes.BOTH);
		this.complianceRuleService.saveComplianceRule(marketDataRangeRule);
		return marketDataRangeRule;
	}


	private ComplianceRule createRule(BusinessCompanies companyName, @Nullable BigDecimal minValue, @Nullable BigDecimal maxValue, String instrumentHierarchyLabel) {

		SystemBean ruleEvaluatorBean = createRuleEvaluatorBean(companyName, minValue, maxValue);

		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		List<InvestmentInstrumentHierarchy> hierarchyList = this.investmentSetupService.getInvestmentInstrumentHierarchyList(searchForm);
		InvestmentInstrumentHierarchy hierarchy = CollectionUtils.getOnlyElement(hierarchyList.stream().filter(e -> e.getLabelExpanded().equals(instrumentHierarchyLabel)).collect(Collectors.toList()));

		ComplianceRule marketDataRangeRule = new ComplianceRule();
		marketDataRangeRule.setName("Executing Company Market Data Range Rule");
		marketDataRangeRule.setDescription("Executing Company Market Data Range Rule");
		marketDataRangeRule.setRuleEvaluatorBean(ruleEvaluatorBean);
		marketDataRangeRule.setRuleType(this.complianceRuleService.getComplianceRuleTypeByName("Executing Company Market Data Range"));
		marketDataRangeRule.setBatch(true);
		marketDataRangeRule.setRealTime(true);
		marketDataRangeRule.setCurrencyType(InvestmentCurrencyTypes.BOTH);
		marketDataRangeRule.setInvestmentHierarchy(hierarchy);
		this.complianceRuleService.saveComplianceRule(marketDataRangeRule);
		return marketDataRangeRule;
	}


	@Test
	public void testWithinRange_Success() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, new BigDecimal(0), new BigDecimal(1000));
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteDoesNotExist(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testAboveThreshold_Success() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, new BigDecimal(1), null);
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteDoesNotExist(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testBelowThreshold_Success() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, null, new BigDecimal(1000));
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteDoesNotExist(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testWithinRange_Fail() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, new BigDecimal(0), new BigDecimal(1));
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteExists(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testAboveThreshold_Fail() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, new BigDecimal(1000), null);
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteExists(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testBelowThreshold_Fail() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, null, new BigDecimal(1));
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteExists(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testUnlistedCompany() {
		ComplianceRule rule = createRule(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, new BigDecimal(0), new BigDecimal(1));
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteDoesNotExist(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testRuleNotAppliedToNonApplicableSecurity() {
		ComplianceRule rule = createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, new BigDecimal(0), new BigDecimal(1), "Equities / Swaps / Total Return Swaps / Index");
		try {
			AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, "Executing Company Market Data Range Rule");
			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", BigDecimal.TEN, true, new Date());
			validateViolationNoteDoesNotExist(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testRuleAppliedToApplicableSecurity() {
		ComplianceRule rule = createRule(BusinessCompanies.BARCLAYS_BANK_PLC, new BigDecimal(0), new BigDecimal(1), "Equities / Swaps / Total Return Swaps / Index");
		try {
			InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber("385416");
			this.complianceUtils.assignRulesToAccountByNames(clientAccount, "Executing Company Market Data Range Rule");

			InvestmentAccount holdingAccount = this.investmentAccountService.getInvestmentAccountByNumber("MAC-BBNK");
			InvestmentAccount swapHoldingAccount = this.investmentAccountService.getInvestmentAccountByNumber("MAC-BBNK");

			AccountInfo accountInfo = new AccountInfo();
			accountInfo.setClientAccount(clientAccount);
			accountInfo.setHoldingAccount(holdingAccount);
			accountInfo.setSwapsHoldingAccount(swapHoldingAccount);
			BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.BARCLAYS_BANK_PLC);

			// update security to have proper ISDA
			InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("N100817B");
			BusinessContractSearchForm businessContractSearchForm = new BusinessContractSearchForm();
			businessContractSearchForm.setSearchPattern("The John D. and Catherine T. MacArthur Foundation-ISDA-Barclays Bank PLC");
			security.setBusinessContract(CollectionUtils.getOnlyElement(this.businessContractService.getBusinessContractList(businessContractSearchForm)));
			this.investmentInstrumentService.saveInvestmentSecurity(security);

			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.TOTAL_RETURN_SWAPS, "N100817B", BigDecimal.TEN, true, DateUtils.toDate("05/01/2019"), broker);
			validateViolationNoteExists(tradeResults, "Executing Company Market Data Range Rule", true);
		}
		finally {
			cleanupRule(rule);
		}
	}


	@Test
	public void testMinAndMaxNotBothNull() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> createRule(BusinessCompanies.GOLDMAN_SACHS_AND_CO, null, null), "At least one of [minValue, maxValue] must be defined.");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void cleanupRule(ComplianceRule rule) {
		ComplianceRuleAssignmentSearchForm sf = new ComplianceRuleAssignmentSearchForm();
		sf.setRuleId(rule.getId());
		List<ComplianceRuleAssignment> assignments = this.complianceRuleService.getComplianceRuleAssignmentList(sf);
		if (!CollectionUtils.isEmpty(assignments)) {
			this.complianceRuleService.deleteComplianceRuleAssignment(CollectionUtils.getOnlyElement(assignments).getId());
		}
		this.complianceRuleService.deleteComplianceRule(rule.getId());
	}
}
