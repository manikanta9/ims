package com.clifton.ims.tests.compliance;


import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


public class ComplianceRollupRuleTests extends ComplianceRealTimeTradeTests {


	@Test
	public void testRollupRule() {
		Date tradeDate = new Date();
		String dateStr = DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, "Government and Sovereign Bonds allowed - Long Only");

		Map<String, String> rollupRuleDataMap = new LinkedHashMap<>();
		rollupRuleDataMap.put("Permitted Security: Bonds (Central Govt - Notes & Bonds) - All (Long Only)", "912810FF0");
		rollupRuleDataMap.put("Permitted Security: Bonds (Central Govt - Bills - United States) - All (Long Only)", "912796FP9");
		rollupRuleDataMap.put("Permitted Security: Bonds (Central Govt - STRIPS) - All (Long Only)", "912803BG7");
		rollupRuleDataMap.put("Permitted Security: Bonds (Central Govt - Inflation Linked) - All (Long Only)", "FR0010447367");
		rollupRuleDataMap.put("Permitted Security: Bonds (Central Govt - Sovereign) - All (Long Only)", "46513EFF4");

		List<RuleViolation> tradeResults;
		for (Map.Entry<String, String> stringStringEntry : rollupRuleDataMap.entrySet()) {
			String rollupRuleSecurity = stringStringEntry.getValue();
			InvestmentSecurity bond = this.investmentInstrumentUtils.getCopyOfSecurity(rollupRuleSecurity, rollupRuleSecurity + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));
			tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, bond.getSymbol(), new BigDecimal(123), true, tradeDate);
			validateViolationNotes(tradeResults);

			tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, bond.getSymbol(), new BigDecimal(123), false, tradeDate);
			validateViolationNotes(tradeResults);

			tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, bond.getSymbol(), new BigDecimal(123), false, tradeDate);
			validateViolationNotes(tradeResults, stringStringEntry.getKey() + " : -123 Face for " + bond.getSymbol() + " open on " + dateStr + " : Long Allowed : Failed");
		}

		//Exclude all corporate bonds
		InvestmentSecurity corporateBond = this.investmentInstrumentUtils.getCopyOfSecurity("38141EKF5", "38141EKF5" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, corporateBond.getSymbol(), new BigDecimal(180), false, tradeDate);
		validateViolationNotes(tradeResults, "Exclude All Positions : -180 Face for " + corporateBond.getSymbol() + " open on " + dateStr + " : Failed");
	}
}
