package com.clifton.ims.tests.system.query;

import com.clifton.test.util.templates.ImsTestProperties;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.Connection;


/**
 * @author stevenf
 */
public class JdbcUtils {


	public static void executeQuery(String sql) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(getDataSource());
		jdbcTemplate.execute(sql);
	}


	public static <T> T executeQuery(String sql, ResultSetExtractor<T> resultSetExtractor) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getDataSource());
		return jdbcTemplate.query(sql, resultSetExtractor);
	}


	private static DataSource getDataSource() {
		DataSource dataSource = new DataSource();
		dataSource.setName(ImsTestProperties.DATASOURCE_NAME);//"CliftonIMS");
		dataSource.setDriverClassName(ImsTestProperties.DATASOURCE_DRIVER_CLASS_NAME);//"com.microsoft.sqlserver.jdbc.SQLServerDriver");
		dataSource.setUrl(ImsTestProperties.DATASOURCE_URL);//"jdbc:sqlserver://localhost:1433;databaseName=CliftonIMS");
		dataSource.setUsername(ImsTestProperties.DATASOURCE_USER);//"cliftonUser");
		dataSource.setPassword(ImsTestProperties.DATASOURCE_PASSWORD);//"password");
		dataSource.setInitialSize(0);
		dataSource.setMaxActive(50);
		dataSource.setMaxIdle(20);
		dataSource.setMinIdle(0);
		dataSource.setMaxWait(5000);
		dataSource.setTestWhileIdle(true);
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setValidationInterval(60000);
		dataSource.setTimeBetweenEvictionRunsMillis(300_000);
		dataSource.setMinEvictableIdleTimeMillis(600_000);
		dataSource.setRemoveAbandoned(true);
		dataSource.setRemoveAbandonedTimeout(600);
		dataSource.setDefaultTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
		dataSource.setInitSQL("SET ARITHABORT ON; SET CONCAT_NULL_YIELDS_NULL ON; SET QUOTED_IDENTIFIER ON; SET ANSI_NULLS ON; SET ANSI_PADDING ON; SET ANSI_WARNINGS ON; SET NUMERIC_ROUNDABORT OFF");
		return dataSource;
	}
}
