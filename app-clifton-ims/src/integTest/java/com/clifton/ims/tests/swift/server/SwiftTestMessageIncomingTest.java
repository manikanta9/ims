package com.clifton.ims.tests.swift.server;

import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.autoclient.SwiftExpectedMessage;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;

import java.time.Duration;
import java.time.temporal.ChronoUnit;


public class SwiftTestMessageIncomingTest extends BaseSwiftTestMessageTest {

	private static final String TEST_INSTRUCTION_MESSAGE = "{1:F21PPSCUS66AXXX0058058388}{4:{177:1708231442}{451:0}}{1:F01FOOSEDR0AXXX0000000000}{2:I202FOORECV0XXXXN}{4:\n" +
			":20:IMS:M2M:56872\n" +
			":21:MARG\n" +
			":32A:161026EUR10120,\n" +
			":53B:/12345678901234567890\n" +
			":57A:BANKANC0XXX\n" +
			":58A:/12345678901234567XXX\n" +
			"BANKANC0XXX\n}";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void bypassImsTest() {
		// no matching identifier in database, will not send any response but will save to SwiftMessage.
		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create().
				with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_INSTRUCTION_MESSAGE).build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder.create(TEST_INSTRUCTION_MESSAGE).build()
				).build();
		setTestCase(swiftTestCase);
		SwiftMessage swiftMessage = new SwiftMessage();
		swiftMessage.setText(TEST_INSTRUCTION_MESSAGE);
		ApplicationContext applicationContext = getRunnerContext();
		SwiftServerOutgoingFileMessageHandler swiftServerOutgoingFileMessageHandler = (SwiftServerOutgoingFileMessageHandler) applicationContext.getBean("swiftServerOutgoingFileMessageHandler");
		swiftServerOutgoingFileMessageHandler.send(swiftMessage);
		SwiftTestStatusMessage msg = awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(5, ChronoUnit.SECONDS));
		Assertions.assertNotNull(msg);
		MatcherAssert.assertThat(msg.getStatus(), IsEqual.equalTo(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE));
	}
}
