package com.clifton.ims.tests.portfolio.run;

import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;


/**
 * @author stevenf
 */
@Component
public class PortfolioRunUtils {

	@Resource
	private InvestmentAccountUtils investmentAccountUtils;

	@Resource
	private PortfolioRunService portfolioRunService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioRun createPortfolioRun(String accountNumber) {
		return createPortfolioRun(accountNumber, new Date());
	}


	public PortfolioRun createPortfolioRun(String accountNumber, Date balanceDate) {
		InvestmentAccount investmentAccount = getInvestmentAccountUtils().getClientAccountByNumber(accountNumber);
		return createPortfolioRun(investmentAccount, balanceDate);
	}


	public PortfolioRun createPortfolioRunFromExisting(String accountNumber) {
		PortfolioRun existingRun = getLastRunForAccount(accountNumber);
		return createPortfolioRun(existingRun.getClientInvestmentAccount(), existingRun.getBalanceDate());
	}


	public PortfolioRun createPortfolioRun(InvestmentAccount clientAccount, Date balanceDate) {
		return createPortfolioRun(clientAccount, balanceDate, false);
	}


	public PortfolioRun createPortfolioRun(InvestmentAccount clientAccount, Date balanceDate, boolean mainRun) {
		PortfolioRun run = new PortfolioRun();
		run.setClientInvestmentAccount(clientAccount);
		run.setBalanceDate(balanceDate);
		run.setMainRun(mainRun);
		run.setNotes("Test");
		return getPortfolioRunService().savePortfolioRun(run);
	}


	public PortfolioRun getLastRunForAccount(String accountNumber) {
		PortfolioRunSearchForm runSearchForm = new PortfolioRunSearchForm();
		runSearchForm.setClientInvestmentAccountId(getInvestmentAccountUtils().getClientAccountByNumber(accountNumber).getId());
		runSearchForm.setLimit(1);
		runSearchForm.setOrderBy("balanceDate:desc");

		PortfolioRun existingRun = CollectionUtils.getFirstElement(getPortfolioRunService().getPortfolioRunList(runSearchForm));
		if (existingRun == null) {
			throw new RuntimeException("Expected to find a run for account #" + accountNumber + ", but didn't");
		}
		return existingRun;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public InvestmentAccountUtils getInvestmentAccountUtils() {
		return this.investmentAccountUtils;
	}


	public void setInvestmentAccountUtils(InvestmentAccountUtils investmentAccountUtils) {
		this.investmentAccountUtils = investmentAccountUtils;
	}
}
