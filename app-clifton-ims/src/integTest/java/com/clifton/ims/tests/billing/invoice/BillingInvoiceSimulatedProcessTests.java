package com.clifton.ims.tests.billing.invoice;

import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleFrequencies;
import com.clifton.billing.definition.BillingScheduleTier;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceRevenueTypes;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.process.BillingInvoiceProcessService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.dw.rebuild.DwRebuildCommand;
import com.clifton.dw.rebuild.DwRebuildService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.builders.billing.BillingDefinitionBuilder;
import com.clifton.ims.tests.builders.investment.business.contract.BusinessContractBuilder;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;


/**
 * The <code>BillingInvoiceSimulatedProcessTests</code> are similar to the BillingInvoiceProcessTests HOWEVER they will create a new billing definition and generate a specific invoice using real account data.
 * This does not rely on existing invoices for comparison and can be used to replicate specific set up options and the end result.
 * <p>
 * NOTE: Should be used for rather simplified setup where possible to limit the complexity of the test.  Use the real definition tests to test more complex features
 *
 * @author manderson
 */
public class BillingInvoiceSimulatedProcessTests extends BaseImsIntegrationTest {

	@Resource
	private BillingDefinitionService billingDefinitionService;

	@Resource
	private BillingInvoiceProcessService billingInvoiceProcessService;

	@Resource
	private BillingInvoiceService billingInvoiceService;

	@Resource
	private BillingBasisService billingBasisService;

	@Resource
	private BusinessContractService businessContractService;

	@Resource
	private DwRebuildService dwRebuildService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static final Short CONTRACT_TYPE_FEE_SCHEDULE = 19;


	private static final Short INVOICE_TYPE_MANAGEMENT_FEE = 1;
	private static final Short INVOICE_TYPE_MANAGEMENT_FEE_ADVISOR = 5;

	private static final Short VALUATION_TYPE_NOTIONAL_UNDERLYING = 11;

	private static final Short CALCULATION_TYPE_PERIOD_END = 3;

	private static final Short SCHEDULE_TYPE_TIERED_FEE = 1;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a Billing Definition for Account # W-013246
	 * And Processes Invoice for 3Q 2019 - as though: Full Period and Various Versions of Partial Period Prorating (Start and End)
	 */
	@Test
	public void testQuarterlyInvoiceMonthlyAccrualPeriodEnd() {
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("W-013246");
		BillingDefinition billingDefinition = buildBillingDefinition(true, clientAccount.getBusinessClient().getCompany(), INVOICE_TYPE_MANAGEMENT_FEE, BillingFrequencies.QUARTERLY, DateUtils.toDate("07/01/2019"), null, null);
		BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount = generateBillingDefinitionInvestmentAccount(billingDefinition, clientAccount, VALUATION_TYPE_NOTIONAL_UNDERLYING, CALCULATION_TYPE_PERIOD_END, null, null);
		generateBillingScheduleTiered(billingDefinition, BillingScheduleFrequencies.ANNUALLY, BillingFrequencies.MONTHLY, new BigDecimal[]{BigDecimal.valueOf(500000000)}, new BigDecimal[]{BigDecimal.valueOf(0.1)}, BigDecimal.valueOf(0.05));

		// Ensure DW Snapshots Are Built on Dates we'll be pulling values
		rebuildDataWareHouseForAccount(clientAccount.getId(), DateUtils.toDate("07/31/2019"), DateUtils.toDate("07/31/2019"));
		rebuildDataWareHouseForAccount(clientAccount.getId(), DateUtils.toDate("08/31/2019"), DateUtils.toDate("08/31/2019"));
		rebuildDataWareHouseForAccount(clientAccount.getId(), DateUtils.toDate("09/30/2019"), DateUtils.toDate("09/30/2019"));
		rebuildDataWareHouseForAccount(clientAccount.getId(), DateUtils.toDate("09/05/2019"), DateUtils.toDate("09/05/2019"));

		// Generate 9/30 Invoice - NO Prorating
		BillingInvoice invoice = this.billingInvoiceProcessService.generateBillingInvoice(billingDefinition.getId(), DateUtils.toDate("09/30/2019"));
		validateInvoiceTotal(invoice, 157_295);
		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());

		// Change Start Date to 7/17 - Prorate July Only
		billingDefinition.setStartDate(DateUtils.toDate("07/17/2019"));
		this.billingDefinitionService.saveBillingDefinition(billingDefinition);
		invoice = this.billingInvoiceProcessService.generateBillingInvoice(billingDefinition.getId(), DateUtils.toDate("09/30/2019"));
		validateInvoiceTotal(invoice, 130_080);
		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());

		// Change Start Date to 8/17 - Skip July, Prorate August Only
		billingDefinition.setStartDate(DateUtils.toDate("08/17/2019"));
		this.billingDefinitionService.saveBillingDefinition(billingDefinition);
		invoice = this.billingInvoiceProcessService.generateBillingInvoice(billingDefinition.getId(), DateUtils.toDate("09/30/2019"));
		validateInvoiceTotal(invoice, 77_741);

		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());

		// Change End Date to 9/5 - Skip July, Prorate August, Prorate September
		billingDefinition.setEndDate(DateUtils.toDate("09/05/2019"));
		this.billingDefinitionService.saveBillingDefinition(billingDefinition);
		invoice = this.billingInvoiceProcessService.generateBillingInvoice(billingDefinition.getId(), DateUtils.toDate("09/30/2019"));
		validateInvoiceTotal(invoice, 33_894);
		this.billingInvoiceService.deleteBillingInvoice(invoice.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BillingDefinition buildBillingDefinition(boolean save, BusinessCompany company, Short invoiceTypeId, BillingFrequencies billingFrequency, Date startDate, Date endDate, Date secondYearStartDate) {
		BusinessContract contract = BusinessContractBuilder.businessContract(this.businessContractService)
				.withContractType(this.businessContractService.getBusinessContractType(CONTRACT_TYPE_FEE_SCHEDULE))
				.withCompany(company)
				.withName("Test Billing " + RandomUtils.randomName())
				.buildAndSave();

		BillingDefinitionBuilder definitionBuilder = BillingDefinitionBuilder.billingDefinition(this.billingDefinitionService)
				.withContract(contract)
				.withBillingCurrency(this.usd)
				.withBillingFrequency(billingFrequency)
				.withInvoiceType(this.billingInvoiceService.getBillingInvoiceType(invoiceTypeId))
				.withStartDate(startDate)
				.withEndDate(endDate)
				.withSecondYearStartDate(secondYearStartDate);

		if (save) {
			return definitionBuilder.buildAndSave();
		}
		return definitionBuilder.build();
	}


	protected BillingDefinitionInvestmentAccount generateBillingDefinitionInvestmentAccount(BillingDefinition billingDefinition, InvestmentAccount clientAccount, short valuationTypeId, Short calculationTypeId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount definitionInvestmentAccount = new BillingDefinitionInvestmentAccount();
		definitionInvestmentAccount.setReferenceOne(billingDefinition);
		definitionInvestmentAccount.setReferenceTwo(clientAccount);
		definitionInvestmentAccount.setBillingBasisValuationType(this.billingBasisService.getBillingBasisValuationType(valuationTypeId));
		if (calculationTypeId != null) {
			definitionInvestmentAccount.setBillingBasisCalculationType(this.billingBasisService.getBillingBasisCalculationType(calculationTypeId));
		}
		definitionInvestmentAccount.setStartDate(startDate);
		definitionInvestmentAccount.setEndDate(endDate);
		return this.billingDefinitionService.saveBillingDefinitionInvestmentAccount(definitionInvestmentAccount);
	}


	protected BillingSchedule generateBillingScheduleTiered(BillingDefinition billingDefinition, BillingScheduleFrequencies scheduleFrequency, BillingFrequencies accrualFrequency, BigDecimal[] tiers, BigDecimal[] tierFees, BigDecimal maxTierFee) {
		BillingSchedule schedule = new BillingSchedule();
		schedule.setBillingDefinition(billingDefinition);
		schedule.setScheduleType(this.billingDefinitionService.getBillingScheduleType(SCHEDULE_TYPE_TIERED_FEE));
		schedule.setRevenueType(BillingInvoiceRevenueTypes.REVENUE);
		schedule.setName(schedule.getScheduleType().getName());
		schedule.setScheduleFrequency(scheduleFrequency);
		schedule.setAccrualFrequency(accrualFrequency);
		schedule.setScheduleTierList(new ArrayList<>());

		for (int i = 0; i < tiers.length; i++) {
			BillingScheduleTier tier = new BillingScheduleTier();
			tier.setThresholdAmount(tiers[i]);
			tier.setFeePercent(tierFees[i]);
			schedule.getScheduleTierList().add(tier);
		}
		if (maxTierFee != null) {
			BillingScheduleTier tier = new BillingScheduleTier();
			tier.setFeePercent(maxTierFee);
			schedule.getScheduleTierList().add(tier);
		}
		return this.billingDefinitionService.saveBillingSchedule(schedule);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void rebuildDataWareHouseForAccount(int clientAccountId, Date startDate, Date endDate) {
		DwRebuildCommand command = new DwRebuildCommand();
		command.setClientAccountId(clientAccountId);
		command.setStartSnapshotDate(startDate);
		command.setEndSnapshotDate(endDate);
		command.setSynchronous(true);
		this.dwRebuildService.rebuildAccountingSnapshots(command);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateInvoiceTotal(BillingInvoice invoice, double expectedTotal) {
		String actualTotalString = CoreMathUtils.formatNumberMoney(invoice.getTotalAmount());
		String expectedTotalString = CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(expectedTotal));
		AssertUtils.assertEquals(actualTotalString, expectedTotalString, "Expected invoice total be be " + expectedTotalString + " but was " + actualTotalString);
	}
}
