package com.clifton.ims.tests.portfolio.cashflow.ldi.history.executor;


import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistoryService;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildService;


/**
 * The <code>PortfolioCashFlowHistoryCalculatorTestServiceHolder</code> holds the service objects that are required by the {@link PortfolioCashflowHistoryCalculatorTestExecutor}.
 *
 * @author michaelm
 */
public class PortfolioCashFlowHistoryCalculatorTestServiceHolder {

	private PortfolioCashFlowHistoryService portfolioCashFlowHistoryService;
	private PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashFlowHistoryService getPortfolioCashFlowHistoryService() {
		return this.portfolioCashFlowHistoryService;
	}


	public void setPortfolioCashFlowHistoryService(PortfolioCashFlowHistoryService portfolioCashFlowHistoryService) {
		this.portfolioCashFlowHistoryService = portfolioCashFlowHistoryService;
	}


	public PortfolioCashFlowHistoryRebuildService getPortfolioCashFlowHistoryRebuildService() {
		return this.portfolioCashFlowHistoryRebuildService;
	}


	public void setPortfolioCashFlowHistoryRebuildService(PortfolioCashFlowHistoryRebuildService portfolioCashFlowHistoryRebuildService) {
		this.portfolioCashFlowHistoryRebuildService = portfolioCashFlowHistoryRebuildService;
	}
}
