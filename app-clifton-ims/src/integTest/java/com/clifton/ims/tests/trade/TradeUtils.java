package com.clifton.ims.tests.trade;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.builders.investment.trade.TradeBuilder;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.business.BusinessUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.marketdata.MarketDataService;
import com.clifton.security.system.SecuritySystem;
import com.clifton.security.system.SecuritySystemService;
import com.clifton.security.system.SecuritySystemUser;
import com.clifton.security.system.search.SecuritySystemUserSearchForm;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>TradeUtils</code> provides convenience methods for creating and executing trades.
 *
 * @author jgommels
 */
@Component
public class TradeUtils {

	private static final Logger log = LoggerFactory.getLogger(TradeUtils.class);
	private static final String TRADE_TABLE_NAME = "Trade";

	@Resource
	private AccountingUtils accountingUtils;
	@Resource
	private InvestmentCalculatorService investmentCalculatorService;
	@Resource
	private MarketDataService marketDataService;
	@Resource
	private RuleViolationUtils ruleViolationUtils;
	@Resource
	private SecuritySystemService securitySystemService;
	@Resource
	private SecurityUserService securityUserService;
	@Resource
	private TradeDestinationService tradeDestinationService;
	@Resource
	private TradeExecutionTypeService tradeExecutionTypeService;
	@Resource
	private TradeGroupService tradeGroupService;
	@Resource
	private TradeService tradeService;
	@Resource
	private WorkflowTransitionService workflowTransitionService;
	@Resource
	private WorkflowTransitioner workflowTransitioner;
	@Resource
	private BusinessUtils businessUtils;

	private BusinessCompany defaultExecutingBroker;
	private TradeDestination manualTradeDestination;
	private SecurityUser admin;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@PostConstruct
	public void initialize() {
		this.manualTradeDestination = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.MANUAL);
		this.admin = this.securityUserService.getSecurityUserByName(SecurityUser.SYSTEM_USER);
		this.defaultExecutingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
	}


	public TradeType getTradeType(String tradeTypeName) {
		return this.tradeService.getTradeTypeByName(tradeTypeName);
	}


	public TradeGroup newTradeGroup(List<Trade> tradeList) {
		Trade firstTrade = tradeList.get(0);
		TradeGroup tradeGroup = new TradeGroup();
		tradeGroup.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName("Trade Import"));
		tradeGroup.setTradeList(tradeList);
		tradeGroup.setTraderUser(firstTrade.getTraderUser());
		tradeGroup.setTradeDate(firstTrade.getTradeDate());
		tradeGroup.setTradeGroupAction(TradeGroup.TradeGroupActions.VALIDATE);
		return this.tradeGroupService.saveTradeGroup(tradeGroup);
	}


	public TradeBuilder newBondTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.BONDS), security, quantity, BigDecimal.ONE, buy, date, null, accountInfo);
	}


	public TradeBuilder newBondTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, AccountInfo accountInfo, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.BONDS), security, quantity, BigDecimal.ONE, buy, date, null, accountInfo, description);
	}

	public TradeBuilder newBondTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, AccountInfo accountInfo, BigDecimal expectedUnitPrice) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.BONDS), security, quantity, BigDecimal.ONE, buy, date, null, accountInfo, expectedUnitPrice);
	}

	public TradeBuilder newBondTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, AccountInfo accountInfo, BigDecimal expectedUnitPrice, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.BONDS), security, quantity, BigDecimal.ONE, buy, date, null, accountInfo, expectedUnitPrice, description);
	}


	public TradeBuilder newBondTradeBuilder(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, boolean buy, Date date, AccountInfo accountInfo, BusinessCompany executingBroker) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.BONDS), security, quantity, price, buy, date, executingBroker, accountInfo);
	}

	public TradeBuilder newBondTradeBuilder(InvestmentSecurity security, BigDecimal quantity, BigDecimal price, boolean buy, Date date, AccountInfo accountInfo, BusinessCompany executingBroker, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.BONDS), security, quantity, price, buy, date, executingBroker, accountInfo, description);
	}


	public TradeBuilder newFuturesTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.FUTURES), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo);
	}


	public TradeBuilder newFuturesTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.FUTURES), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo, description);
	}


	public TradeBuilder newOptionsTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.OPTIONS), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo);
	}


	public TradeBuilder newOptionsTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.OPTIONS), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo, description);
	}



	/**
	 * NOTE:  When creating a forward trade for FIX execution the price MUST be null.
	 */
	public TradeBuilder newForwardsTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.FORWARDS), security, quantity, null, buy, date, executingBroker, accountInfo);
	}


	/**
	 * NOTE:  When creating a forward trade for FIX execution the price MUST be null.
	 */
	public TradeBuilder newForwardsTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.FORWARDS), security, quantity, null, buy, date, executingBroker, accountInfo, description);
	}


	public TradeBuilder newFundsTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.FUNDS), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo);
	}


	public TradeBuilder newFundsTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.FUNDS), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo, description);
	}


	public TradeBuilder newStocksTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.STOCKS), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo);
	}


	public TradeBuilder newStocksTradeBuilder(InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.STOCKS), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo, description);
	}


	public TradeBuilder newCreditDefaultSwapTradeBuilder(InvestmentSecurity security, BigDecimal originalNotional, BigDecimal factor, boolean buy, BigDecimal price, Date date,
	                                                     BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newCreditDefaultSwapTradeBuilder(security, originalNotional, factor, buy, price, date,
				executingBroker, accountInfo, TradeType.CREDIT_DEFAULT_SWAPS, null);
	}


	public TradeBuilder newCreditDefaultSwapTradeBuilder(InvestmentSecurity security, BigDecimal originalNotional, BigDecimal factor, boolean buy, BigDecimal price, Date date,
	                                                     BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newCreditDefaultSwapTradeBuilder(security, originalNotional, factor, buy, price, date,
				executingBroker, accountInfo, TradeType.CREDIT_DEFAULT_SWAPS, description);
	}



	public TradeBuilder newClearedCreditDefaultSwapTradeBuilder(InvestmentSecurity security, BigDecimal originalNotional, BigDecimal factor, boolean buy, BigDecimal price, Date date,
	                                                            BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newCreditDefaultSwapTradeBuilder(security, originalNotional, factor, buy, price, date,
				executingBroker, accountInfo, TradeType.CLEARED_CREDIT_DEFAULT_SWAPS, null);
	}

	public TradeBuilder newClearedCreditDefaultSwapTradeBuilder(InvestmentSecurity security, BigDecimal originalNotional, BigDecimal factor, boolean buy, BigDecimal price, Date date,
	                                                            BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newCreditDefaultSwapTradeBuilder(security, originalNotional, factor, buy, price, date,
				executingBroker, accountInfo, TradeType.CLEARED_CREDIT_DEFAULT_SWAPS, description);
	}



	private TradeBuilder newCreditDefaultSwapTradeBuilder(InvestmentSecurity security, BigDecimal originalNotional, BigDecimal factor, boolean buy, BigDecimal price, Date date,
	                                                      BusinessCompany executingBroker, AccountInfo accountInfo, String tradeTypeName, String description) {
		BigDecimal adjustedNotional = MathUtils.multiply(originalNotional, factor);
		BigDecimal accrualAmount = this.investmentCalculatorService.getInvestmentSecurityAccruedInterest(security.getId(), originalNotional, null, DateUtils.addDays(date, 1), null).abs();
		if (!buy) {
			accrualAmount = accrualAmount.negate();
		}

		return newTradeBuilder(this.tradeService.getTradeTypeByName(tradeTypeName), security, adjustedNotional, price, buy, date, executingBroker, accountInfo, description)
				.withAccountingNotional(originalNotional.negate())
				.withAccrualAmount1(accrualAmount)
				.withCurrentFactor(factor)
				.withOriginalFace(originalNotional)
				.withExpectedUnitPrice(price);
	}


	public TradeBuilder newTotalReturnSwapTradeBuilder(InvestmentSecurity security, BigDecimal units, BigDecimal price, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.TOTAL_RETURN_SWAPS), security, units, price, buy, date, executingBroker, accountInfo);
	}


	/**
	 * Creates a new TradeBuilder for trading Currency. The key difference for currency trades is the trade amount is set on the accountingNotional property of Trade instead of the
	 * quantityIntended property.
	 */
	public TradeBuilder newCurrencyTradeBuilder(InvestmentSecurity ccy, InvestmentSecurity payingCcy, BigDecimal tradeAmount, BigDecimal fxRate, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(this.tradeService.getTradeTypeByName(TradeType.CURRENCY), ccy, null, null, buy, date, executingBroker, accountInfo).withAccountingNotional(tradeAmount).withExchangeRate(fxRate).withPayingSecurity(payingCcy);
	}


	/**
	 * Creates a new TradeBuilder for trading Currency. The key difference for currency trades is the trade amount is set on the accountingNotional property of Trade instead of the
	 * quantityIntended property.
	 */
	public TradeBuilder newCurrencyTradeBuilder(InvestmentSecurity ccy, InvestmentSecurity payingCcy, BigDecimal tradeAmount, BigDecimal fxRate, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String destinationName) {
		TradeDestination tradeDestination = this.tradeDestinationService.getTradeDestinationByName(destinationName);

		return newCurrencyTradeBuilder(ccy, payingCcy, tradeAmount, fxRate, buy, date, executingBroker, accountInfo)
				.withTradeDestination(tradeDestination);
	}


	public TradeBuilder newTradeBuilder(TradeType tradeType, InvestmentSecurity security, BigDecimal quantity, BigDecimal price, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo) {
		return newTradeBuilder(tradeType, security, quantity, price, buy, date, executingBroker, accountInfo, null, null);
	}


	public TradeBuilder newTradeBuilder(TradeType tradeType, InvestmentSecurity security, BigDecimal quantity, BigDecimal price, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, String description) {
		return newTradeBuilder(tradeType, security, quantity, price, buy, date, executingBroker, accountInfo, null, description);
	}


	public TradeBuilder newTradeBuilder(TradeType tradeType, InvestmentSecurity security, BigDecimal quantity, BigDecimal price, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, BigDecimal expectedUnitPrice) {
		return newTradeBuilder(tradeType, security, quantity, price, buy, date, executingBroker, accountInfo, expectedUnitPrice, null);
	}


	public TradeBuilder newTradeBuilder(TradeType tradeType, InvestmentSecurity security, BigDecimal quantity, BigDecimal price, boolean buy, Date date, BusinessCompany executingBroker, AccountInfo accountInfo, BigDecimal expectedUnitPrice, String description) {
		// Get notional multiplier (index ratio) for bonds; this is typically populated by the client and may result in booking violations if not set
		BigDecimal notionalMultiplier = null;
		if (TradeType.BONDS.equalsIgnoreCase(tradeType.getName())) {
			notionalMultiplier = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), date);
		}
		TradeExecutionType tradeExecutionType = TradeType.FORWARDS.equalsIgnoreCase(tradeType.getName()) ? this.tradeExecutionTypeService.getTradeExecutionTypeByName(TradeExecutionType.NON_CLS_TRADE) : null;

		return TradeBuilder.trade(this.tradeService)
				.withInvestmentSecurity(security)
				.withBuy(buy)
				.withTradeDestination(this.manualTradeDestination)
				.withQuantityIntended(quantity)
				.withAverageUnitPrice(price)
				.withExpectedUnitPrice(expectedUnitPrice)
				.withNotionalMultiplier(notionalMultiplier)
				.withClientAccount(accountInfo.getClientAccount())
				.withHoldingAccount(accountInfo.getHoldingAccount())
				.withExecutingBroker(executingBroker == null ? this.defaultExecutingBroker : executingBroker)
				.withTradeType(this.tradeService.getTradeTypeByName(tradeType.getName()))
				.withTradeExecutionType(tradeExecutionType)
				.withPayingSecurity(security.getInstrument().getTradingCurrency())
				.withTraderUser(this.admin)
				.withTradeDate(date)
				.withSettlementDate(this.investmentCalculatorService.getInvestmentSecuritySettlementDate(security.getId(), security.getInstrument().getTradingCurrency() == null ? null : security.getInstrument().getTradingCurrency().getId(), date))
				.withDescription(description != null ? description : "Integration Test Trade");
	}


	public Trade getTrade(int tradeId) {
		return this.tradeService.getTrade(tradeId);
	}


	/**
	 * Validates the trade if it has not been validated yet, optionally ignoring any specified violations.
	 */
	public void validateTrade(Trade trade, String... violationDescriptionsToIgnore) {
		//Validate the trade
		if (!executeValidateTradeTransition(trade)) {
			// trade is not in a state to be validated
			return;
		}

		this.ruleViolationUtils.ignoreViolations(TRADE_TABLE_NAME, trade.getId(), false,
				//Ignore 'non-current' security violations for now
				"not the current security for instrument",
				// price threshold is a post process rule but can be found on validate when rejected
				"price differed from expected");

		//Ignore expected system violations
		if (!ArrayUtils.isEmpty(violationDescriptionsToIgnore)) {
			this.ruleViolationUtils.ignoreViolations(TRADE_TABLE_NAME, trade.getId(), true, violationDescriptionsToIgnore);
		}

		//Validate the trade again now that system violations have been ignored
		executeValidateTradeTransition(trade);
	}


	private boolean executeValidateTradeTransition(Trade trade) {
		List<WorkflowTransition> transitionList = this.workflowTransitionService.getWorkflowTransitionNextListByEntity("Trade", BeanUtils.getIdentityAsLong(trade));
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
			if ("Validate Trade".equals(transition.getName()) || "Validate Trade Execution".equals(transition.getName())) {
				this.workflowTransitionService.executeWorkflowTransitionByTransition(TRADE_TABLE_NAME, trade.getId(), transition.getId());
				return true;
			}
		}
		return false;
	}


	/**
	 * Fully executes the trade by transitioning through the following states:
	 * <ol>
	 * <li>Validate the trade</li>
	 * <li>Approve the trade</li>
	 * <li>Book and Post the trade</li>
	 * <li>Finish execution of the trade</li>
	 *
	 * @param trade
	 * @param violationDescriptionsToIgnore
	 */
	public void fullyExecuteTrade(Trade trade, String... violationDescriptionsToIgnore) {
		validateTrade(trade, violationDescriptionsToIgnore);

		//Assert no unignored system violations
		this.ruleViolationUtils.assertNoRuleViolations(TRADE_TABLE_NAME, trade.getId());

		//Transition through rest of states
		this.workflowTransitioner.transitionIfPresent("Approve Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
		this.workflowTransitioner.transitionIfPresent("Book and Post Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));

		//Assert that the trade is booked and closed
		WorkflowTransition transition = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade)).get(0);
		if (TradeService.TRADE_EXECUTED_INVALID_STATE_NAME.equals(transition.getName())) {
			validateTrade(trade);
			this.workflowTransitioner.transitionIfPresent("Book and Post Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
			transition = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade)).get(0);
		}
		Assertions.assertEquals(TradeService.TRADE_BOOKED_STATE_NAME, transition.getName());
		Assertions.assertEquals(TradeService.TRADES_CLOSED_STATUS_NAME, transition.getEndWorkflowState().getStatus().getName());

		if (log.isInfoEnabled()) {
			List<TradeFill> fills = this.tradeService.getTradeFillListByTrade(trade.getId());
			for (TradeFill fill : fills) {
				log.info(this.accountingUtils.asString(fill));
			}
		}
	}


	public List<TradeFill> getTradeFills(Trade trade) {
		return this.tradeService.getTradeFillListByTrade(trade.getId());
	}


	public Trade rejectTrade(Trade trade) {
		this.workflowTransitioner.transitionIfPresent("Reject Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
		return this.tradeService.getTrade(trade.getId());
	}


	public void cancelTrade(Trade trade) {
		this.workflowTransitioner.transitionIfPresent("Cancel Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
	}


	public void unbookAndUnpostTrade(Trade trade) {
		this.workflowTransitioner.transitionIfPresent("Un-Post and Un-Book Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
	}


	public void rejectAndCancelTrade(Trade trade) {
		this.workflowTransitioner.transitionIfPresent("Reject Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
		this.workflowTransitioner.transitionIfPresent("Cancel Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
	}


	public Trade saveTrade(Trade trade) {
		return this.tradeService.saveTrade(trade);
	}


	public void deleteTradeFill(TradeFill tradeFill) {
		this.tradeService.deleteTradeFill(tradeFill.getId());
	}


	public void approveTrade(Trade trade, String... violationDescriptionsToIgnore) {
		validateTrade(trade, violationDescriptionsToIgnore);

		//Assert no unignored system violations
		this.ruleViolationUtils.assertNoRuleViolations(TRADE_TABLE_NAME, trade.getId());

		//Transition through rest of states
		this.workflowTransitioner.transitionIfPresent("Approve Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
	}


	public void finishExecutionTrade(Trade trade, String... violationDescriptionsToIgnore) {
		validateTrade(trade, violationDescriptionsToIgnore);

		//Assert no unignored system violations
		this.ruleViolationUtils.assertNoRuleViolations(TRADE_TABLE_NAME, trade.getId());

		//Transition through rest of states
		this.workflowTransitioner.transitionIfPresent("Finish Execution", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
	}


	public void bookAndPostTrade(Trade trade, String... violationDescriptionsToIgnore) {
		validateTrade(trade, violationDescriptionsToIgnore);

		//Assert no unignored system violations
		this.ruleViolationUtils.assertNoRuleViolations(TRADE_TABLE_NAME, trade.getId());

		//Transition through rest of states
		this.workflowTransitioner.transitionIfPresent("Book and Post Trade", TRADE_TABLE_NAME, BeanUtils.getIdentityAsLong(trade));
	}


	public void createSecuritySystemUser(String userName, String securitySystemName, String systemUserName) {

		SecurityUser user = this.securityUserService.getSecurityUserByPseudonym(userName);
		SecuritySystem system = this.securitySystemService.getSecuritySystemByName(securitySystemName);

		SecuritySystemUserSearchForm securitySystemUserSearchForm = new SecuritySystemUserSearchForm();
		securitySystemUserSearchForm.setUserName(systemUserName);
		if (system != null) {
			securitySystemUserSearchForm.setSecuritySystemId(system.getId());
		}
		securitySystemUserSearchForm.setSecurityUserId(user.getId());

		if (CollectionUtils.isEmpty(this.securitySystemService.getSecuritySystemUserList(securitySystemUserSearchForm))) {

			SecuritySystemUser systemUser = new SecuritySystemUser();
			systemUser.setSecurityUser(user);
			systemUser.setSecuritySystem(system);
			systemUser.setUserName(systemUserName);

			this.securitySystemService.saveSecuritySystemUser(systemUser);
		}
	}


	/**
	 * Looks up the {@link Trade} with the provided ID and returns an unsaved copy with the provided quantity and trade date set.
	 * The returned trade will have id, workflowSate, and workflowStatus set to null and it will be ready to save.
	 */
	public Trade copyTrade(int tradeId, BigDecimal quantity, Date tradeDate) {
		return copyTrade(tradeId, quantity, tradeDate, false);
	}


	/**
	 * Looks up the {@link Trade} with the provided ID and creates a copy with the provided quantity and trade date set.
	 * If saveCopy is true, returns the saved copy. If saveCopy is false, the returned trade will have id, workflowSate, and workflowStatus set to null.
	 */
	public Trade copyTrade(int tradeId, BigDecimal quantity, Date tradeDate, boolean saveCopy) {
		Trade trade = new Trade();
		Trade toCopy = getTrade(tradeId);
		ValidationUtils.assertNotNull(toCopy, "Unable to find trade to copy with ID: " + tradeId);
		BeanUtils.copyProperties(getTrade(tradeId), trade);
		trade.setId(null);
		trade.setWorkflowStatus(null);
		trade.setWorkflowState(null);
		trade.setQuantityIntended(quantity);
		trade.setTradeDate(tradeDate);
		Date settlementDate = this.investmentCalculatorService.getInvestmentSecuritySettlementDate(toCopy.getInvestmentSecurity().getId(), toCopy.getSettlementCurrency().getId(), tradeDate);
		trade.setSettlementDate(settlementDate);
		return saveCopy ? this.tradeService.saveTrade(trade) : trade;
	}


	public Date getSettlementDateForTrade(Trade trade) {
		return getSettlementDate(trade.getInvestmentSecurity(), trade.getSettlementCurrency(), trade.getTradeDate());
	}


	public Date getSettlementDate(InvestmentSecurity security, Date tradeDate) {
		ValidationUtils.assertNotNull(security, "Investment Security must be provided");
		InvestmentSecurity settlementSecurity = ObjectUtils.coalesce(security.getSettlementCurrency(), security.getInstrument().getTradingCurrency());
		ValidationUtils.assertNotNull(settlementSecurity, "Settlement Security Could not be obtained from provides security: " + security);
		return getSettlementDate(security, settlementSecurity, tradeDate);
	}


	public Date getSettlementDate(InvestmentSecurity security, InvestmentSecurity settlementSecurity, Date tradeDate) {
		ValidationUtils.assertNotNull(security, "Investment Security must be provided");
		ValidationUtils.assertNotNull(settlementSecurity, "Settlement Security must be provided");
		ValidationUtils.assertNotNull(tradeDate, "Trade Date must be provided");
		return this.investmentCalculatorService.getInvestmentSecuritySettlementDate(security.getId(), settlementSecurity.getId(), tradeDate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getDefaultExecutingBroker() {
		return this.defaultExecutingBroker;
	}


	public TradeDestination getManualTradeDestination() {
		return this.manualTradeDestination;
	}


	public SecurityUser getDefaultTrader() {
		return this.admin;
	}
}
