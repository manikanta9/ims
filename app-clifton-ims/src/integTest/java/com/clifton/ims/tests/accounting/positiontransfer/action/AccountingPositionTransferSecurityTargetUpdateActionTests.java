package com.clifton.ims.tests.accounting.positiontransfer.action;

import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.accounting.positiontransfer.action.AccountingPositionTransferSecurityTargetUpdateAction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.account.securitytarget.util.InvestmentAccountSecurityTargetAdjustmentUtilHandlerImpl;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;


/**
 * A set of tests to test the {@link AccountingPositionTransferSecurityTargetUpdateAction} for proper operation.
 *
 * @author davidi
 */

public class AccountingPositionTransferSecurityTargetUpdateActionTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private InvestmentAccountUtils investmentAccountUtils;

	private AccountingPositionTransferSecurityTargetUpdateAction accountingPositionTransferSecurityTargetUpdateAction;

	private AccountingJournal transferJournal = new AccountingJournal();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		if (this.accountingPositionTransferSecurityTargetUpdateAction == null) {
			//SystemBean systemBean =  getSystemBeanService().getSystemBeanByName("Position Transfer Security Target Update Action");
			//this.accountingPositionTransferSecurityTargetUpdateAction = (AccountingPositionTransferSecurityTargetUpdateAction) getSystemBeanService().getBeanInstance(systemBean);
			this.accountingPositionTransferSecurityTargetUpdateAction = new AccountingPositionTransferSecurityTargetUpdateAction();
			this.accountingPositionTransferSecurityTargetUpdateAction.setInvestmentAccountSecurityTargetService(this.investmentAccountSecurityTargetService);
			InvestmentAccountSecurityTargetAdjustmentUtilHandlerImpl handler = new InvestmentAccountSecurityTargetAdjustmentUtilHandlerImpl();
			handler.setInvestmentAccountSecurityTargetService(this.investmentAccountSecurityTargetService);
			this.accountingPositionTransferSecurityTargetUpdateAction.setInvestmentAccountSecurityTargetAdjustmentUtilHandler(handler);
			this.accountingPositionTransferSecurityTargetUpdateAction.setAccountingPositionTransferService(this.accountingPositionTransferService);
		}
	}


	@Test
	public void testProcessAction_contribution() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, BigDecimal.valueOf(quantity)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.processAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testRollbackAction_contribution() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.subtract(initialQuantity, BigDecimal.valueOf(quantity)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.rollbackAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_inactive_target() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		secTarget.setEndDate(secTarget.getStartDate());
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.processAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		// inactive target -- a new active target should not have been created
		Assertions.assertNull(updatedSecurityTarget);

		// inactive target -- initial quantity should not have been altered
		secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(transfer.getToClientInvestmentAccount().getId(),
				investmentSecurity.getId(), date);
		Assertions.assertEquals(initialQuantity, secTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testRollbackAction_inactive_target() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		secTarget.setEndDate(secTarget.getStartDate());
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.rollbackAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		// inactive target -- a new active target should not have been created
		Assertions.assertNull(updatedSecurityTarget);

		// inactive target -- initial quantity should not have been altered
		secTarget = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetForAccountAndUnderlyingSecurityId(transfer.getToClientInvestmentAccount().getId(),
				investmentSecurity.getId(), date);
		Assertions.assertEquals(initialQuantity, secTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_distribution() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, false);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.subtract(initialQuantity, BigDecimal.valueOf(quantity)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.processAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getFromClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testRollbackAction_distribution() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, false);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity();
		BigDecimal expectedQuantity = MathUtils.add(initialQuantity, BigDecimal.valueOf(quantity)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.rollbackAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getFromClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_contribution_notional_target() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(null);
		BigDecimal notionalValue = BigDecimal.valueOf(100000.00);
		secTarget.setTargetUnderlyingNotional(notionalValue);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal expectedNotional = MathUtils.add(notionalValue, BigDecimal.valueOf(1000.00)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.processAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testRollbackAction_contribution_notional_target() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(null);
		BigDecimal notionalValue = BigDecimal.valueOf(100000.00);
		secTarget.setTargetUnderlyingNotional(notionalValue);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal expectedNotional = MathUtils.subtract(notionalValue, BigDecimal.valueOf(1000.00)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.rollbackAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_distribution_notional_target() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, false);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(null);
		BigDecimal notionalValue = BigDecimal.valueOf(100000.00);
		secTarget.setTargetUnderlyingNotional(notionalValue);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal expectedNotional = MathUtils.subtract(notionalValue, BigDecimal.valueOf(1000.00)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.processAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getFromClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testRollbackAction_distribution_notional_target() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, false);
		InvestmentSecurity investmentSecurity = transfer.getDetailList().get(0).getSecurity();
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(null);
		BigDecimal notionalValue = BigDecimal.valueOf(100000.00);
		secTarget.setTargetUnderlyingNotional(notionalValue);
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal expectedNotional = MathUtils.add(notionalValue, BigDecimal.valueOf(1000.00)).setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.rollbackAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getFromClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(expectedNotional, updatedSecurityTarget.getTargetUnderlyingNotional().setScale(2, RoundingMode.DOWN));
	}


	@Test
	public void testProcessAction_non_matching_security() {
		Date date = DateUtils.toDate("02/01/14");
		double quantity = 100.0;

		AccountInfo clientInvestmentAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.options);
		InvestmentAccount clientInvestmentAccount = clientInvestmentAccountInfo.getClientAccount();
		AccountingPositionTransfer transfer = createTransfer(clientInvestmentAccountInfo, "PG", quantity, 10.00, date, true);
		InvestmentSecurity investmentSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM");
		InvestmentAccountSecurityTarget secTarget = createSecurityTarget(clientInvestmentAccount, investmentSecurity, date);
		secTarget.setTargetUnderlyingQuantity(BigDecimal.valueOf(1000.00));
		this.investmentAccountSecurityTargetService.saveInvestmentAccountSecurityTarget(secTarget);
		transfer.setSourceFkFieldId(secTarget.getId());

		BigDecimal initialQuantity = secTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN);

		this.accountingPositionTransferSecurityTargetUpdateAction.processAction(transfer, this.transferJournal);
		InvestmentAccountSecurityTarget updatedSecurityTarget = getActiveInvestmentAccountSecurityTarget(transfer.getToClientInvestmentAccount(), investmentSecurity);

		Assertions.assertNotNull(updatedSecurityTarget);
		Assertions.assertEquals(initialQuantity, updatedSecurityTarget.getTargetUnderlyingQuantity().setScale(2, RoundingMode.DOWN));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AccountingPositionTransfer createTransfer(AccountInfo accountInfo, String ticker, double quantity, double averagePrice, Date transactionDate, boolean isTransferTo) {
		InvestmentSecurity testSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(ticker);

		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setBookingDate(null);
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(DateUtils.addDays(transactionDate, 1));
		transfer.setNote("Test transfer.");

		AccountingPositionTransferType transferType = this.accountingPositionTransferService.getAccountingPositionTransferTypeByName(isTransferTo ? AccountingPositionTransferType.SECURITY_TARGET_CONTRIBUTION : AccountingPositionTransferType.SECURITY_TARGET_DISTRIBUTION);
		if (isTransferTo) {
			transfer.setToClientInvestmentAccount(accountInfo.getClientAccount());
			transfer.setToHoldingInvestmentAccount(accountInfo.getHoldingAccount());
		}
		else {
			transfer.setFromClientInvestmentAccount(accountInfo.getClientAccount());
			transfer.setFromHoldingInvestmentAccount(accountInfo.getHoldingAccount());
		}

		transfer.setType(transferType);


		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		detail.setSecurity(testSecurity);
		detail.setQuantity(BigDecimal.valueOf(quantity));
		detail.setTransferPrice(BigDecimal.valueOf(averagePrice));
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setOriginalPositionOpenDate(transactionDate);

		List<AccountingPositionTransferDetail> detailList = CollectionUtils.createList(detail);
		transfer.setDetailList(detailList);

		SystemTable systemTableForSecurityTargets = this.systemSchemaService.getSystemTableByName("InvestmentAccountSecurityTarget");
		transfer.setSourceSystemTable(systemTableForSecurityTargets);

		return transfer;
	}


	private InvestmentAccountSecurityTarget getActiveInvestmentAccountSecurityTarget(InvestmentAccount account, InvestmentSecurity security) {
		InvestmentAccountSecurityTargetSearchForm investmentAccountSecurityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();

		investmentAccountSecurityTargetSearchForm.setClientInvestmentAccountId(account.getId());
		investmentAccountSecurityTargetSearchForm.setTargetUnderlyingSecurityId(security.getId());
		investmentAccountSecurityTargetSearchForm.setActive(true);
		investmentAccountSecurityTargetSearchForm.setOrderBy("startDate:desc");

		List<InvestmentAccountSecurityTarget> targetList = this.investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(investmentAccountSecurityTargetSearchForm);
		if (targetList.size() > 0) {
			return targetList.get(0);
		}
		return null;
	}


	private InvestmentAccountSecurityTarget createSecurityTarget(InvestmentAccount account, InvestmentSecurity security, Date startDate) {
		InvestmentAccountSecurityTarget securityTarget = new InvestmentAccountSecurityTarget();
		securityTarget.setStartDate(startDate);
		securityTarget.setClientInvestmentAccount(account);
		securityTarget.setTargetUnderlyingSecurity(security);
		return securityTarget;
	}
}
