package com.clifton.ims.tests.accounting.positiontransfer;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingJournalDetailBuilder;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.accounting.GeneralLedgerDescriptions;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AccountingPositionTransferMultipleForeignLotsTests extends BaseImsIntegrationTest {

	private static InvestmentSecurity TPZ5;


	@BeforeEach
	public void beforeTest() {
		if (TPZ5 == null) {
			TPZ5 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("TPZ5");
		}
	}


	@Test
	public void testTransferOfForeignFuturePositionBetweenAccounts() {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, this.futures);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createTradeAllComplianceRule(toAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, TPZ5.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, TPZ5.getInstrument());

		List<AccountingTransaction> positions = openTrades(fromAccount);

		Date transactionDate = DateUtils.toDate("10/05/2015");
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		transfer.setFromClientInvestmentAccount(fromAccount.getClientAccount());
		transfer.setFromHoldingInvestmentAccount(fromAccount.getHoldingAccount());
		transfer.setToClientInvestmentAccount(toAccount.getClientAccount());
		transfer.setToHoldingInvestmentAccount(toAccount.getHoldingAccount());
		transfer.setType(this.accountingUtils.getAccountingPositionTransferType(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
		transfer.setUseOriginalTransactionDate(false);
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(transactionDate);
		transfer.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "to client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());

		List<AccountingPositionTransferDetail> transferDetails = new ArrayList<>();
		transferDetails.add(createTransferDetail(positions.get(0), new BigDecimal("0.00831427"), new BigDecimal("1435.2"), new BigDecimal("1441.5"), new BigDecimal("-1744215000.00"), DateUtils.toDate("09/08/2015")));
		transferDetails.add(createTransferDetail(positions.get(1), new BigDecimal("0.00834097"), new BigDecimal("1409"), new BigDecimal("1441.5"), new BigDecimal("-518940000.00"), DateUtils.toDate("09/30/2015")));

		transfer.setDetailList(transferDetails);
		transfer = this.accountingUtils.saveAccountingPositionTransfer(transfer);
		transfer = this.accountingUtils.bookAndPost(transfer);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.transactionDate(transactionDate);

		List<AccountingJournalDetailDefinition> expectedJournalDetails = new ArrayList<>();

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.security(TPZ5)
				.price(new BigDecimal("1441.5"))
				.quantity(new BigDecimal("121"))
				.opening(false)
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.quantity(new BigDecimal("-121"))
				.opening(true)
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.security(TPZ5)
				.price(new BigDecimal("1441.5"))
				.quantity(new BigDecimal("36"))
				.opening(false)
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.quantity(new BigDecimal("-36"))
				.opening(true)
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.quantity(new BigDecimal("121"))
				.localDebitCredit(new BigDecimal("7623000.00"))
				.baseDebitCredit(new BigDecimal("63379.68"))
				.opening(false)
				.description(GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_LOSS, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.quantity(new BigDecimal("36"))
				.localDebitCredit(new BigDecimal("11700000.00"))
				.baseDebitCredit(new BigDecimal("97589.35"))
				.opening(false)
				.description(GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_LOSS, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());


		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(new BigDecimal("121"))
				.localDebitCredit(new BigDecimal("21175.00"))
				.baseDebitCredit(new BigDecimal("176.05"))
				.opening(true)
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(new BigDecimal("121"))
				.localDebitCredit(new BigDecimal("21175.00"))
				.baseDebitCredit(new BigDecimal("176.05"))
				.opening(true)
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, TPZ5)
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(new BigDecimal("36"))
				.localDebitCredit(new BigDecimal("6300.00"))
				.baseDebitCredit(new BigDecimal("52.55"))
				.opening(true)
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(new BigDecimal("36"))
				.localDebitCredit(new BigDecimal("6300.00"))
				.baseDebitCredit(new BigDecimal("52.55"))
				.opening(true)
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());


		InvestmentSecurity ccy = this.investmentInstrumentUtils.getCurrencyBySymbol("JPY");
		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(ccy)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("-7665350.00"))
				.baseDebitCredit(new BigDecimal("-63731.78"))
				.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_CLOSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(ccy)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("-11712600.00"))
				.baseDebitCredit(new BigDecimal("-97694.45"))
				.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_CLOSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		List<? extends AccountingJournalDetailDefinition> actualJournalDetails = this.accountingUtils.getJournal(transfer).getJournalDetailList();

		AccountingUtils.assertJournalDetailList(expectedJournalDetails, actualJournalDetails);
	}


	private AccountingPositionTransferDetail createTransferDetail(AccountingTransaction existingPosition, BigDecimal exchangeRate, BigDecimal costPrice, BigDecimal transferPrice, BigDecimal costBasis, Date openDate) {
		//Create another position transfer detail of 10 CCH with no commission override
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-10);
		transferDetail.setExistingPosition(existingPosition);
		transferDetail.setSecurity(existingPosition.getInvestmentSecurity());
		transferDetail.setPositionCostBasis(costBasis);
		transferDetail.setCostPrice(costPrice);
		transferDetail.setTransferPrice(transferPrice);
		transferDetail.setQuantity(existingPosition.getQuantity());
		transferDetail.setExchangeRateToBase(exchangeRate);
		transferDetail.setOriginalPositionOpenDate(openDate);
		return transferDetail;
	}


	private List<AccountingTransaction> openTrades(AccountInfo fromAccount) {

		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP);
		Date openDate = DateUtils.toDate("9/08/2015");
		BigDecimal positionSize = new BigDecimal("121");

		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(TPZ5, positionSize, false, openDate, broker, fromAccount)
				.withExchangeRate(new BigDecimal("0.00831427"))
				.withAverageUnitPrice(new BigDecimal("1435.2")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		List<AccountingTransaction> result = new ArrayList<>();
		result.add((AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0));


		broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		openDate = DateUtils.toDate("9/30/2015");
		positionSize = new BigDecimal("36");

		buyTrade = this.tradeUtils.newFuturesTradeBuilder(TPZ5, positionSize, false, openDate, broker, fromAccount)
				.withExchangeRate(new BigDecimal("0.00834097"))
				.withAverageUnitPrice(new BigDecimal("1409")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);
		result.add((AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0));

		return result;
	}
}
