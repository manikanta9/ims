package com.clifton.ims.tests.trade;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.business.BusinessUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentInstrumentUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.execution.TradeExecutionTypeService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;


@Component
public class TradeCreator {

	//////////////////////////////////////////////////////////////////
	//						Test Util Classes
	//////////////////////////////////////////////////////////////////

	@Resource
	private InvestmentInstrumentUtils investmentInstrumentUtils;

	@Resource
	private BusinessUtils businessUtils;

	@Resource
	private TradeUtils tradeUtils;

	//////////////////////////////////////////////////////////////////
	//						IMS Service Classes
	//////////////////////////////////////////////////////////////////

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private TradeExecutionTypeService tradeExecutionTypeService;

	@Resource
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	@Resource
	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Trade createClearedCreditDefaultSwap(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, String destinationName) {
		return createClearedCreditDefaultSwap(accountInfo, symbol, tradeDate, quantity, destinationName, null, null);
	}


	public Trade createClearedCreditDefaultSwap(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, String destinationName, String description) {
		return createClearedCreditDefaultSwap(accountInfo, symbol, tradeDate, quantity, destinationName, null, description);
	}


	public Trade createClearedCreditDefaultSwap(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, String destinationName,
	                                            BusinessCompanies executingSponsorCompany) {
		return createClearedCreditDefaultSwap(accountInfo, symbol, tradeDate, quantity, destinationName, executingSponsorCompany, null);
	}


	public Trade createClearedCreditDefaultSwap(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, String destinationName,
	                                            BusinessCompanies executingSponsorCompany, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);

		BusinessCompany executingSponsorBusinessCompany = Objects.nonNull(executingSponsorCompany) ? this.businessUtils.getBusinessCompany(executingSponsorCompany) : null;

		return this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(security, quantity, BigDecimal.ONE, true, new BigDecimal(100), DateUtils.toDate(tradeDate), null, accountInfo, description)
				.withCommissionAmount(BigDecimal.ZERO)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.withFeeAmount(BigDecimal.ZERO)
				.withEnableCommissionOverride(true)
				.withExecutingSponsor(executingSponsorBusinessCompany)
				.withExecutingBroker(null)
				.buildAndSave();
	}


	public Trade createFuturesTrade(AccountInfo accountInfo, String symbol, String tradeDate, double quantity, boolean buy, String destinationName,
	                                BusinessCompanies executingBrokerCompany) {
		return createFuturesTrade(accountInfo, symbol, tradeDate, quantity, buy, destinationName, executingBrokerCompany, null);
	}


	public Trade createFuturesTrade(AccountInfo accountInfo, String symbol, String tradeDate, double quantity, boolean buy, String destinationName,
	                                BusinessCompanies executingBrokerCompany, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(executingBrokerCompany);

		return this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(quantity), buy, DateUtils.toDate(tradeDate), executingBroker, accountInfo, description)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.buildAndSave();
	}


	public Trade createFundsTrade(AccountInfo accountInfo, String symbol, String tradeDate, double quantity, boolean buy, double commissionPerUnit, String destinationName,
	                              BusinessCompanies executingBrokerCompany) {
		return createFundsTrade(accountInfo, symbol, tradeDate, quantity, buy, commissionPerUnit, destinationName, executingBrokerCompany, null);
	}


	public Trade createFundsTrade(AccountInfo accountInfo, String symbol, String tradeDate, double quantity, boolean buy, double commissionPerUnit, String destinationName,
	                              BusinessCompanies executingBrokerCompany, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(executingBrokerCompany);

		return this.tradeUtils.newFundsTradeBuilder(security, BigDecimal.valueOf(quantity), buy, DateUtils.toDate(tradeDate), executingBroker, accountInfo, description)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.withCommissionPerUnit(BigDecimal.valueOf(commissionPerUnit))
				.buildAndSave();
	}


	public Trade createStocksTrade(AccountInfo accountInfo, String symbol, String tradeDate, double quantity, boolean buy, double commissionPerUnit, String destinationName,
	                               BusinessCompanies executingBrokerCompany) {
		return createStocksTrade(accountInfo, symbol, tradeDate, quantity, buy, commissionPerUnit, destinationName, executingBrokerCompany, null);
	}


	public Trade createStocksTrade(AccountInfo accountInfo, String symbol, String tradeDate, double quantity, boolean buy, double commissionPerUnit, String destinationName,
	                               BusinessCompanies executingBrokerCompany, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(executingBrokerCompany);

		return this.tradeUtils.newStocksTradeBuilder(security, BigDecimal.valueOf(quantity), buy, DateUtils.toDate(tradeDate), executingBroker, accountInfo, description)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.withCommissionPerUnit(BigDecimal.valueOf(commissionPerUnit))
				.buildAndSave();
	}


	public Trade createForwardTrade(AccountInfo accountInfo, String symbol, String payingCurrencySymbol, String tradeDate, BigDecimal quantity
			, BigDecimal notional
			, BigDecimal expectUnitPrice
			, boolean buy, String destinationName,
			                        BusinessCompanies executingBrokerCompany) {
		return createForwardTrade(accountInfo, symbol, payingCurrencySymbol, tradeDate, tradeDate, quantity, notional, expectUnitPrice, buy, destinationName,
				executingBrokerCompany, false, false, null);
	}


	public Trade createForwardTrade(AccountInfo accountInfo, String symbol, String payingCurrencySymbol, String tradeDate, BigDecimal quantity
			, BigDecimal notional
			, BigDecimal expectUnitPrice
			, boolean buy, String destinationName,
			                        BusinessCompanies executingBrokerCompany, String description) {
		return createForwardTrade(accountInfo, symbol, payingCurrencySymbol, tradeDate, tradeDate, quantity, notional, expectUnitPrice, buy, destinationName,
				executingBrokerCompany, false, false, description);
	}


	public Trade createForwardTrade(AccountInfo accountInfo, String symbol, String payingCurrencySymbol, String tradeDate, BigDecimal quantity, BigDecimal notional, BigDecimal expectUnitPrice, boolean buy, String destinationName,
	                                BusinessCompanies executingBrokerCompany, boolean useNullHoldingAccountForCompetitive, boolean clsTrade, String description) {
		return createForwardTrade(accountInfo, symbol, payingCurrencySymbol, tradeDate, tradeDate, quantity, notional, expectUnitPrice, buy, destinationName,
				executingBrokerCompany, useNullHoldingAccountForCompetitive, clsTrade, description);
	}


	public Trade createForwardTrade(AccountInfo accountInfo, String symbol, String payingCurrencySymbol, String tradeDate, BigDecimal quantity, BigDecimal notional, BigDecimal expectUnitPrice, boolean buy, String destinationName,
	                                BusinessCompanies executingBrokerCompany, boolean useNullHoldingAccountForCompetitive, boolean clsTrade) {
		return createForwardTrade(accountInfo, symbol, payingCurrencySymbol, tradeDate, tradeDate, quantity, notional, expectUnitPrice, buy, destinationName,
				executingBrokerCompany, useNullHoldingAccountForCompetitive, clsTrade, null);
	}


	private Trade createForwardTrade(AccountInfo accountInfo, String symbol, String payingCurrencySymbol, String tradeDate, String settlementDate, BigDecimal quantity, BigDecimal notional, BigDecimal expectUnitPrice, boolean buy, String destinationName,
	                                 BusinessCompanies executingBrokerCompany, boolean useNullHoldingAccountForCompetitive, boolean clsTrade, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		InvestmentSecurity payingSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(payingCurrencySymbol, true);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = executingBrokerCompany != null ? this.businessUtils.getBusinessCompany(executingBrokerCompany) : null;

		BigDecimal accountingNotional = payingSecurity.equals(security.getInstrument().getTradingCurrency()) ? notional : quantity;
		BigDecimal quantityIntended = payingSecurity.equals(security.getInstrument().getTradingCurrency()) ? quantity : notional;

		TradeExecutionType tradeExecutionType = this.tradeExecutionTypeService.getTradeExecutionTypeByName(clsTrade ? TradeExecutionType.CLS_TRADE : TradeExecutionType.NON_CLS_TRADE);
		if (clsTrade && executingBroker != null) {
			setClsMemberTagOnBroker(executingBroker);
		}

		return this.tradeUtils.newForwardsTradeBuilder(security, null, buy, DateUtils.toDate(tradeDate), executingBroker, accountInfo, description)
				.withHoldingAccount(useNullHoldingAccountForCompetitive ? null : accountInfo.getHoldingAccount())
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.withSettlementDate(DateUtils.toDate(settlementDate))
				.withAccountingNotional(accountingNotional)
				.withQuantityIntended(quantityIntended)
				.withExpectedUnitPrice(expectUnitPrice)
				.withPayingSecurity(payingSecurity)
				.withExecutingBroker(executingBroker)
				.withTradeExecutionType(tradeExecutionType)
				.withDescription("")
				.buildAndSave();
	}


	public void setClsMemberTagOnBroker(BusinessCompany broker) {
		List<SystemHierarchyLink> systemHierarchyLinkList = this.systemHierarchyAssignmentService.getSystemHierarchyLinkList(
				"BusinessCompany", broker.getId(), "Business Company Tags");
		boolean exists = CollectionUtils.getStream(systemHierarchyLinkList).map(entity -> entity.getHierarchy().getName()).anyMatch("CLS Member"::equals);

		if (!exists) {
			SystemHierarchySearchForm systemHierarchySearchForm = new SystemHierarchySearchForm();
			systemHierarchySearchForm.setSearchPattern("CLS Member");
			List<SystemHierarchy> systemHierarchyList = this.systemHierarchyDefinitionService.getSystemHierarchyList(systemHierarchySearchForm);
			AssertUtils.assertNotEmpty(systemHierarchyList, "Cannot find SystemHierarchy with name: CLS Member");

			this.systemHierarchyAssignmentService.saveSystemHierarchyLink("BusinessCompany", broker.getId(), systemHierarchyList.get(0).getId());
		}
	}


	public Trade createBondTrade(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, boolean buy, String destinationName) {
		return createBondTrade(accountInfo, symbol, tradeDate, quantity, buy, destinationName, null);
	}


	public Trade createBondTrade(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, boolean buy, String destinationName, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		return this.tradeUtils.newBondTradeBuilder(security, quantity, buy, DateUtils.toDate(tradeDate), accountInfo, description)
				.withExecutingBroker(null)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.buildAndSave();
	}


	public Trade createBondTrade(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal originalFace, BigDecimal averageUnitPrice, BigDecimal accruedInterest, BigDecimal indexRatio, boolean buy, String destinationName, BusinessCompanies executingBrokerCompany) {
		return createBondTrade(accountInfo, symbol, tradeDate, originalFace, averageUnitPrice, accruedInterest, indexRatio, buy, destinationName, executingBrokerCompany, null);
	}


	public Trade createBondTrade(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal originalFace, BigDecimal averageUnitPrice, BigDecimal accruedInterest, BigDecimal indexRatio, boolean buy, String destinationName, BusinessCompanies executingBrokerCompany, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(executingBrokerCompany);
		return this.tradeUtils.newBondTradeBuilder(security, originalFace, averageUnitPrice, buy, DateUtils.toDate(tradeDate), accountInfo, executingBroker, description)
				.withAccrualAmount1(accruedInterest)
				.withNotionalMultiplier(indexRatio)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.buildAndSave();
	}


	public Trade createOptionsTrade(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, boolean buy, String destinationName,
	                                BusinessCompanies executingBrokerCompany, AtomicReference<TradeOpenCloseType> tradeOpenCloseTypeAtomicReference) {
		return createOptionsTrade(accountInfo, symbol, tradeDate, quantity, buy, destinationName, executingBrokerCompany, tradeOpenCloseTypeAtomicReference, null);
	}


	public Trade createOptionsTrade(AccountInfo accountInfo, String symbol, String tradeDate, BigDecimal quantity, boolean buy, String destinationName,
	                                BusinessCompanies executingBrokerCompany, AtomicReference<TradeOpenCloseType> tradeOpenCloseTypeAtomicReference, String description) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = executingBrokerCompany != null ? this.businessUtils.getBusinessCompany(executingBrokerCompany) : null;
		return this.tradeUtils.newOptionsTradeBuilder(security, quantity, buy, DateUtils.toDate(tradeDate), executingBroker, accountInfo, description)
				.withTradeDestination(destination)
				.withOpenCloseType(tradeOpenCloseTypeAtomicReference)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.buildAndSave();
	}
}
