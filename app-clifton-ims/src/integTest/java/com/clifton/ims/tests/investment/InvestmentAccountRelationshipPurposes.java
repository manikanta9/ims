package com.clifton.ims.tests.investment;


import java.util.Arrays;
import java.util.NoSuchElementException;


public enum InvestmentAccountRelationshipPurposes {
	COLLATERAL("Collateral"),
	CUSTODIAN("Custodian"),
	MONEY_MARKET("Money Market"),
	MARK_TO_MARKET("Mark to Market"),
	TRADING_BONDS("Trading: Bonds"),
	TRADING_CURRENCIES("Trading: Currencies"),
	TRADING_FUTURES("Trading: Futures"),
	TRADING_OPTIONS("Trading: Options"),
	TRADING_STOCKS("Trading: Stocks"),
	TRADING_SWAPS("Trading: Swaps"),
	TRADING_FORWARDS("Trading: Forwards"),
	TRADING_MUTUAL_FUNDS("Trading: Mutual Funds"),
	CLIFTON_SUB_ACCOUNT("Clifton Sub-Account"),
	CLIFTON_CLIENT_ACCOUNT("Clifton Child-Account"),
	EXECUTING_STOCKS("Executing: Stocks"),
	EXECUTING_FUNDS("Executing: Funds"),
	TRANSITION("Transition"),
	REPO("REPO"),
	TRI_PARTY_REPO("Tri-Party REPO"),
	WHOLLY_OWNED_SUBSIDIARY("Wholly Owned Subsidiary");

	private final String name;


	InvestmentAccountRelationshipPurposes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}


	public static InvestmentAccountRelationshipPurposes findByName(String name) {
		return Arrays.stream(InvestmentAccountRelationshipPurposes.values())
				.filter(p -> p.getName().equals(name)).findFirst().orElseThrow(NoSuchElementException::new);
	}
}
