package com.clifton.ims.tests.rule;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class RuleTests extends BaseImsIntegrationTest {

	public static final String TRANSFER_TABLE_NAME = "AccountingPositionTransfer";
	public static final String PORTFOLIO_RUN_TABLE_NAME = "PortfolioRun";
	public static final String TRADE_TABLE_NAME = "Trade";

	@Resource
	public CalendarBusinessDayService calendarBusinessDayService;
	@Resource
	public RuleDefinitionService ruleDefinitionService;
	@Resource
	public RuleEvaluatorService ruleEvaluatorService;
	@Resource
	public RuleViolationService ruleViolationService;
	@Resource
	public RuleViolationUtils ruleViolationUtils;
	@Resource
	public SystemRequestStatsService systemRequestStatsService;

	@Resource
	public WorkflowTransitioner workflowTransitioner;


	public abstract String getCategoryName();


	// Defaults to Null to Apply All Rules
	public Boolean getPreProcess() {
		return null;
	}


	public abstract Set<String> getExcludedTestMethodSet();


	public static final Map<String, String> expectedViolationNoteMap = new HashMap<>();
	public static final Map<String, Throwable> testExceptionsMap = new HashMap<>();

	public static final Set<String> excludedTestMethodSet = new HashSet<>();
	public static final Set<Method> testMethodSet = new HashSet<>();

	public static List<RuleViolation> ruleViolationList = new ArrayList<>();


	private List<RuleDefinition> getRuleDefinitionList() {
		RuleDefinitionSearchForm ruleDefinitionSearchForm = new RuleDefinitionSearchForm();
		ruleDefinitionSearchForm.setManual(false);
		ruleDefinitionSearchForm.setInactive(false);
		ruleDefinitionSearchForm.setPreProcess(getPreProcess());
		ruleDefinitionSearchForm.setRuleCategoryName(getCategoryName());

		applyRuleDefinitionSearchFormOverrides(ruleDefinitionSearchForm);

		return this.ruleDefinitionService.getRuleDefinitionList(ruleDefinitionSearchForm);
	}


	protected void applyRuleDefinitionSearchFormOverrides(RuleDefinitionSearchForm ruleDefinitionSearchForm) {
		// do nothing - available for extended class use
	}


	@BeforeEach
	public void initializeTest() {
		if (testMethodSet.isEmpty()) {
			// initialize only once per test class
			Set<String> excludeTestMethods = getExcludedTestMethodSet();
			if (excludeTestMethods == null) {
				excludeTestMethods = new HashSet<>();
			}
			excludeTestMethods.add("testGetRuleCategoryByName");
			excludeTestMethods.add("testGetRuleDefinitionList");
			excludeTestMethods.add("testAllActiveRulesHaveTests");
			excludeTestMethods.add("testRuleTestsExistForRulesThatDoNotApply");
			excludeTestMethods.forEach(methodName -> excludedTestMethodSet.add(methodName.toLowerCase()));
			for (Method method : this.getClass().getMethods()) {
				if (AnnotationUtils.isAnnotationPresent(method, Test.class) && !excludedTestMethodSet.contains(method.getName().toLowerCase())) {
					testMethodSet.add(method);
				}
			}
		}
	}


	@AfterAll
	public static void clearTestDataStructures() {
		// clean up after each test class executes so the data structures are not polluted with data for the next test class
		excludedTestMethodSet.clear();
		testMethodSet.clear();
		expectedViolationNoteMap.clear();
		testExceptionsMap.clear();
	}


	@Test
	public void testGetRuleCategoryByName() {

		Assertions.assertEquals(getCategoryName(), this.ruleDefinitionService.getRuleCategoryByName(getCategoryName()).getName());
	}


	@Test
	public void testGetRuleDefinitionList() {
		RuleDefinitionSearchForm ruleDefinitionSearchForm = new RuleDefinitionSearchForm();
		ruleDefinitionSearchForm.setRuleCategoryName(getCategoryName());
		Assertions.assertNotNull(CollectionUtils.getFirstElement(getRuleDefinitionList()));
	}


	@Test
	public void testAllActiveRulesHaveTests() {
		Set<String> methodSet = new HashSet<>();
		for (Method method : testMethodSet) {
			methodSet.add(method.getName().toLowerCase());
		}

		List<String> missingTests = new ArrayList<>();
		for (RuleDefinition ruleDefinition : CollectionUtils.getIterable(getRuleDefinitionList())) {
			String testMethodName = "test" + ruleDefinition.getName().replaceAll("\\s", "").replaceAll("\\W", "") + "Rule";
			if (!methodSet.contains(testMethodName.toLowerCase()) && !excludedTestMethodSet.contains(testMethodName.toLowerCase())) {
				missingTests.add(testMethodName);
			}
		}
		Collections.sort(missingTests);
		Assertions.assertEquals(0, missingTests.size(), "\n\nTests do not exist for:\n\n" + String.join("\n", missingTests) + "\n\n");
	}


	@Test
	public void testRuleTestsExistForRulesThatDoNotApply() {
		Set<String> definitionSet = new HashSet<>();
		for (RuleDefinition ruleDefinition : CollectionUtils.getIterable(getRuleDefinitionList())) {
			definitionSet.add(("test" + ruleDefinition.getName().replaceAll("\\s", "").replaceAll("\\W", "") + "Rule").toLowerCase());
		}

		List<String> superfluousTests = new ArrayList<>();
		for (Method method : testMethodSet) {
			String testMethodName = method.getName();
			if (!definitionSet.contains(testMethodName.toLowerCase()) && !excludedTestMethodSet.contains(testMethodName.toLowerCase())) {
				superfluousTests.add(testMethodName);
			}
		}
		Collections.sort(superfluousTests);
		Assertions.assertEquals(0, superfluousTests.size(), "\n\nTests should not exist for:\n\n" + String.join("\n", superfluousTests) + "\n\n");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////             Test Assertion Methods              ////////////////
	/////////////////////////////////////////////////////////////////////////////


	protected void validateTestMethodResult(String methodName) {
		boolean foundViolationNote = false;
		Throwable exception = testExceptionsMap.get(methodName);
		if (exception == null) {
			String expectedViolationNote = expectedViolationNoteMap.get(methodName);
			StringBuilder failureMessage = new StringBuilder("Failed to find expected Violation: '" + expectedViolationNote + "' in ruleViolationList: \n");

			Pattern pattern = Pattern.compile(expectedViolationNote);
			for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
				Matcher matcher = pattern.matcher(ruleViolation.getViolationNote());
				if (ruleViolation.getViolationNote().equals(expectedViolationNote) || matcher.find()) {
					foundViolationNote = true;
				}
				failureMessage.append("\t").append(ruleViolation.getId()).append(" - ").append(ruleViolation.getViolationNote()).append("\n");
			}
			Assertions.assertTrue(foundViolationNote, failureMessage.toString());
		}
		else {
			exception.printStackTrace();
			Assertions.fail("Exception thrown by test method; see stack trace.");
		}
	}
}
