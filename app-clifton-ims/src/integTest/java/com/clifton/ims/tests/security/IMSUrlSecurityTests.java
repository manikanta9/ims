package com.clifton.ims.tests.security;


import com.clifton.ims.tests.spring.ImsIntegrationConfiguration;
import com.clifton.test.security.BaseUrlSecurityTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * The <code>IMSUrlSecurityTests</code> checks each URL is able to determine a way to lookup security
 * If @SecureMethod annotation is associated with the Method, will consider it to be configured.  Will only actually attempt URLs that do not have explicit annotations
 *
 * @author manderson
 */
@ContextConfiguration(classes = {ImsIntegrationConfiguration.class})
public class IMSUrlSecurityTests extends BaseUrlSecurityTests {

	// NOTHING HERE
}
