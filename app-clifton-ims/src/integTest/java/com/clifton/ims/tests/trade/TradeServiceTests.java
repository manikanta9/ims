package com.clifton.ims.tests.trade;

import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;


/**
 * @author NickK
 */
public class TradeServiceTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentCurrencyService investmentCurrencyService;

	@Resource
	MarketDataRatesService marketDataRatesService;

	@Resource
	private TradeService tradeService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrencyUsdEur() {
		testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrency("USD", "EUR");
	}


	@Test
	public void testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrencyEurUsd() {
		testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrency("EUR", "USD");
	}


	@Test
	public void testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrencyUsdCad() {
		testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrency("USD", "CAD");
	}


	@Test
	public void testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrencyCadUsd() {
		testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrency("CAD", "USD");
	}


	@Test
	public void testSpotCurrencyExecutedWithManualDefaultsExchangeRateToDominantCurrencyCadUsd() {
		Trade trade = testSpotCurrencyDefaultsExchangeRateToDominantCurrency("CAD", "USD", "Manual");
		this.tradeUtils.fullyExecuteTrade(trade);
		Assertions.assertEquals("Booked", this.tradeService.getTrade(trade.getId()).getWorkflowState().getName());
	}


	@Test
	public void testForwardTradeSecurityUpdateModifiesExchangeRateToBase() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.forwards);
		InvestmentSecurity originalSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP/EUR20200430");
		BigDecimal price = new BigDecimal("0.855168");
		BigDecimal quantity = new BigDecimal("499225.00");
		Trade trade = this.tradeUtils.newForwardsTradeBuilder(originalSecurity, quantity, false, DateUtils.toDate("02/27/2020"), accountInfo.getHoldingCompany(), accountInfo)
				.withAverageUnitPrice(price)
				.buildAndSave();
		Assertions.assertEquals(new BigDecimal("1.1029500000000000"), trade.getExchangeRateToBase()); // Goldman Sachs exchange rate from EUR to USD
		Assertions.assertEquals(price, trade.getAverageUnitPrice());
		Assertions.assertEquals(quantity, trade.getQuantityIntended());
		Assertions.assertEquals(new BigDecimal("583774.18"), trade.getAccountingNotional());

		trade = this.tradeUtils.rejectTrade(trade);

		InvestmentSecurity correctSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("EUR/GBP20200430");
		trade.setInvestmentSecurity(correctSecurity);
		trade = this.tradeUtils.saveTrade(trade);
		Assertions.assertNotEquals(new BigDecimal("1.241250000000"), trade.getExchangeRateToBase()); // Goldman Sachs exchange rate from GPP to USD
		Assertions.assertEquals(new BigDecimal("682642.69"), trade.getQuantityIntended());
		Assertions.assertEquals(new BigDecimal("583774.18"), trade.getAccountingNotional());
	}


	@Test
	public void testBondWithFactorEventTradeRoundsQuantityIntended() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.bonds);
		InvestmentSecurity bond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("31394EB98");

		BigDecimal face = new BigDecimal("2178983.00");
		BigDecimal factor = new BigDecimal("0.05211712");
		BigDecimal quantity = new BigDecimal("113562.32");

		Consumer<Boolean> tradeExecutor = (buy) -> {
			Trade trade = this.tradeUtils.newBondTradeBuilder(bond, MathUtils.multiply(face, factor), buy, DateUtils.toDate("02/13/2020"), accountInfo)
					.withOriginalFace(face)
					.withCurrentFactor(factor)
					.buildAndSave();
			Assertions.assertTrue(MathUtils.isEqual(quantity, trade.getQuantityIntended()));
			this.tradeUtils.fullyExecuteTrade(trade);

			List<AccountingPositionBalance> positionBalanceList = this.accountingUtils.getAccountingPositionBalanceList(accountInfo.getClientAccount(), bond, trade.getTradeDate());
			if (!buy) {
				Assertions.assertTrue(CollectionUtils.isEmpty(positionBalanceList));
			}
			else {
				AccountingPositionBalance positionBalance = CollectionUtils.getOnlyElement(positionBalanceList);
				Assertions.assertTrue(MathUtils.isEqual(quantity, positionBalance.getRemainingQuantity()));
				Assertions.assertTrue(MathUtils.isEqual(face, positionBalance.getUnadjustedQuantity()));
			}
		};

		tradeExecutor.accept(true);
		tradeExecutor.accept(false);
	}


	@Test
	public void testForwardTradeCorrectlyRoundsValuesPerCurrencyConvention() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.forwards);

		// "JPY/GBP20210730"
		String forwardSymbol = "JPY/GBP20210730";
		BigDecimal price = new BigDecimal("155.0146656294335");
		BigDecimal quantity = new BigDecimal("123456.78");
		BigDecimal expectedQuantity = new BigDecimal("123457.00");
		BigDecimal expectedAccountingNotional = new BigDecimal("796.42");
		// foreign CCY
		validateForwardTradeWithForeignCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		quantity = new BigDecimal("123456.48");
		expectedQuantity = new BigDecimal("123456.00");
		expectedAccountingNotional = new BigDecimal("796.41");
		validateForwardTradeWithForeignCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		// Trading CCY
		quantity = new BigDecimal("796.4145");
		expectedQuantity = new BigDecimal("123455.00");
		validateForwardTradeWithTradingCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		quantity = new BigDecimal("796.4155");
		expectedQuantity = new BigDecimal("123457.00");
		expectedAccountingNotional = new BigDecimal("796.42");
		validateForwardTradeWithTradingCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);

		// USD/EUR
		forwardSymbol = "USD/EUR20210730";
		price = new BigDecimal("1.2129866383661");
		quantity = new BigDecimal("123456.78");
		expectedQuantity = new BigDecimal("123456.78");
		expectedAccountingNotional = new BigDecimal("101779.18");
		// Foreign CCY
		validateForwardTradeWithForeignCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		quantity = new BigDecimal("123456.48");
		expectedQuantity = new BigDecimal("123456.48");
		expectedAccountingNotional = new BigDecimal("101778.93");
		validateForwardTradeWithForeignCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		quantity = new BigDecimal("123456.7855");
		expectedQuantity = new BigDecimal("123456.79");
		expectedAccountingNotional = new BigDecimal("101779.18");
		validateForwardTradeWithForeignCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		// Trading CCY
		quantity = new BigDecimal("101779.18");
		;
		expectedQuantity = new BigDecimal("123456.79");
		expectedAccountingNotional = quantity;
		validateForwardTradeWithTradingCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
		quantity = new BigDecimal("101779.185");
		expectedQuantity = new BigDecimal("123456.80");
		expectedAccountingNotional = new BigDecimal("101779.19");
		validateForwardTradeWithTradingCcyAmount(accountInfo, forwardSymbol, quantity, price, expectedQuantity, expectedAccountingNotional);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void testSpotCurrencyExecutedWithFxConnectDefaultsExchangeRateToDominantCurrency(String accountBaseCurrencySymbol, String tradingCurrencySymbol) {
		testSpotCurrencyDefaultsExchangeRateToDominantCurrency(accountBaseCurrencySymbol, tradingCurrencySymbol, "FX Connect Ticket");
	}


	private Trade testSpotCurrencyDefaultsExchangeRateToDominantCurrency(String accountBaseCurrencySymbol, String tradingCurrencySymbol, String tradeDestination) {
		AccountInfo currencyAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.currency);
		InvestmentSecurity baseCurrency = currencyAccountInfo.getClientAccount().getBaseCurrency();
		if (!baseCurrency.getSymbol().equals(accountBaseCurrencySymbol)) {
			baseCurrency = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(accountBaseCurrencySymbol, true);
			currencyAccountInfo.getClientAccount().setBaseCurrency(baseCurrency);
			this.investmentAccountService.saveInvestmentAccount(currencyAccountInfo.getClientAccount());
		}

		Date tradeDate = DateUtils.toDate("03/04/2016");
		BigDecimal quantity = new BigDecimal("100");

		InvestmentSecurity tradingCurrency = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(tradingCurrencySymbol, true);
		BigDecimal expectedExchangeRate = getDominantCurrencyExchangeRate(currencyAccountInfo.getHoldingAccount().getIssuingCompany().getId(), tradeDate, tradingCurrency, baseCurrency);
		Trade buy = this.tradeUtils.newCurrencyTradeBuilder(tradingCurrency, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, "Manual".equals(tradeDestination) ? expectedExchangeRate : null, true, tradeDate, this.goldmanSachs, currencyAccountInfo, tradeDestination)
				.buildAndSave();

		Assertions.assertEquals(buy.getExchangeRateToBase(), expectedExchangeRate);
		return buy;
	}


	private BigDecimal getDominantCurrencyExchangeRate(Integer issuingCompanyId, Date tradeDate, InvestmentSecurity currencyOne, InvestmentSecurity currencyTwo) {
		String dominantCurrencySymbol = this.investmentCurrencyService.getInvestmentCurrencyConventionDominantCurrency(currencyOne.getSymbol(), currencyTwo.getSymbol());
		InvestmentSecurity dominantCurrency;
		InvestmentSecurity nonDominantCurrency;
		if (currencyOne.getSymbol().equals(dominantCurrencySymbol)) {
			dominantCurrency = currencyOne;
			nonDominantCurrency = currencyTwo;
		}
		else {
			dominantCurrency = currencyTwo;
			nonDominantCurrency = currencyOne;
		}
		return this.marketDataRatesService.getMarketDataExchangeRateForDateFlexible(issuingCompanyId, dominantCurrency.getId(), nonDominantCurrency.getId(), tradeDate).getExchangeRate();
	}


	private Trade validateForwardTradeWithForeignCcyAmount(AccountInfo accountInfo, String forwardSymbol, BigDecimal quantity, BigDecimal price, BigDecimal expectedForeignAmount, BigDecimal expectedTradingAmount) {
		return validateForwardTradeAmounts(accountInfo, forwardSymbol, quantity, price, false, expectedForeignAmount, expectedTradingAmount);
	}


	private Trade validateForwardTradeWithTradingCcyAmount(AccountInfo accountInfo, String forwardSymbol, BigDecimal quantity, BigDecimal price, BigDecimal expectedForeignAmount, BigDecimal expectedTradingAmount) {
		return validateForwardTradeAmounts(accountInfo, forwardSymbol, quantity, price, true, expectedForeignAmount, expectedTradingAmount);
	}


	private Trade validateForwardTradeAmounts(AccountInfo accountInfo, String forwardSymbol, BigDecimal quantity, BigDecimal price, boolean quantityForDenominatingCurrency, BigDecimal expectedForeignAmount, BigDecimal expectedTradingAmount) {
		InvestmentSecurity forward = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(forwardSymbol);
		BigDecimal tradeQuantity = quantityForDenominatingCurrency ? null : quantity;
		BigDecimal accountingNotional = quantityForDenominatingCurrency ? quantity : null;
		InvestmentSecurity settlementCurrency = quantityForDenominatingCurrency ? forward.getUnderlyingSecurity() : forward.getInstrument().getTradingCurrency();
		Trade trade = this.tradeUtils.newForwardsTradeBuilder(forward, tradeQuantity, quantityForDenominatingCurrency, DateUtils.toDate("06/16/2021"), accountInfo.getHoldingCompany(), accountInfo)
				.withAccountingNotional(accountingNotional)
				.withAverageUnitPrice(price)
				.withPayingSecurity(settlementCurrency)
				.buildAndSave();
		validateForwardTradeAmounts(trade, price, expectedForeignAmount, expectedTradingAmount);
		return trade;
	}


	private void validateForwardTradeAmounts(Trade trade, BigDecimal price, BigDecimal expectedForeignAmount, BigDecimal expectedTradingAmount) {
		Assertions.assertEquals(price, trade.getAverageUnitPrice(), String.format("Forward trade price did not match. Expect [%s], Received [%s]", price, trade.getAccountingNotional()));
		Assertions.assertTrue(MathUtils.isEqual(expectedForeignAmount, trade.getQuantityIntended()), String.format("Forward trade quantityIntended did not match. Expect [%s], Received [%s]", expectedForeignAmount, trade.getQuantityIntended()));
		Assertions.assertTrue(MathUtils.isEqual(expectedTradingAmount, trade.getAccountingNotional()), String.format("Forward trade accountingNotional did not match. Expect [%s], Received [%s]", expectedTradingAmount, trade.getAccountingNotional()));
	}
}
