package com.clifton.ims.tests.instruction.mt54x;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.SimpleEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.builders.investment.trade.TradeBuilder;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.instruction.InvestmentInstructionUtils;
import com.clifton.ims.tests.trade.TradeCreator;
import com.clifton.ims.tests.utility.PeriodicRepeatingExecutor;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBusinessCompany;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBusinessCompanyService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.swift.server.SwiftTestMessageServer;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.autoclient.SwiftExpectedMessage;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import com.clifton.test.util.PropertiesLoader;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


public class MT541Test extends BaseImsIntegrationTest {

	private static final String CONTEXT_PROPERTIES_PATH = PropertiesLoader.getPropertiesFileUri();
	private static final String TEST_DATA_PATH = "com/clifton/ims/tests/swift/server/data";
	private static final String DELIVERY_NAME = MT541Test.class.getSimpleName();

	private static final String FUTURES_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":94B::TRAD//EXCH/XCME\n" +
			":98A::SETT//20170823\n" +
			":98A::TRAD//20170823\n" +
			":90B::DEAL//ACTU/USD1702,771428\n" +
			":35B:/TS/FAU17\n" +
			"SP MID 400 EMINI Sep17\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/FUT\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20170915\n" +
			":36B::SIZE//UNIT/100,\n" +
			":16S:FIA\n" +
			":22F::PROC//OPEP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/7,\n" +
			":97A::SAFE//1495990614\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95Q::PSET//XX\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::DEAG//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::SELL//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD15,33\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD0,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD15,33\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String FUTURES_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I541SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//1\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX;


	private static final String FUTURES_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0058058413}{2:I541SBOSUS3UXIMS}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//1\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";


	private static final String FUTURES_CANCEL_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I541SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//2\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//1\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX;

	private static final String FUTURES_CANCEL_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0058058413}{2:I541SBOSUS3UXIMS}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//2\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//1\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	private static final String EQUITIES_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":98A::SETT//20170825\n" +
			":98A::TRAD//20170823\n" +
			":90B::DEAL//ACTU/USD84,38\n" +
			":35B:ISIN US4642874576\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/ETF\n" +
			":11A::DENO//USD\n" +
			":36B::SIZE//UNIT/1,\n" +
			":16S:FIA\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/1130,\n" +
			":97A::SAFE//362876374\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95P::PSET//DTCYUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::DEAG/DTCYID/0005\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::SELL/DTCYID/0005\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD95360,7\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD95349,4\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD11,3\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::REGF//USD0,\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String EQUITIES_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I543IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//1234\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			EQUITIES_COMMON_SUFFIX;

	private static final String EQUITIES_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I543IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//1234\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			EQUITIES_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	private static final String T_BILLS_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":98A::SETT//20171201\n" +
			":98A::TRAD//20171130\n" +
			":90A::DEAL//PRCT/99,8125\n" +
			":35B:ISIN US912810RW09\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/TIPS\n" +
			":11A::DENO//USD\n" +
			":98A::MATU//20470215\n" +
			":98A::ISSU//20170228\n" +
			":92A::INTR//0,875\n" +
			":16S:FIA\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//AMOR/9345467,2\n" +
			":36B::SETT//FAMT/9140000,\n" +
			":97A::SAFE//SWBF0909002\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95P::PSET//FRNYUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::DEAG/USFW/021000018\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::SELL/DTCYID/0229\n" +
			":97A::SAFE//BARCLAYS\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD9351943,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD9327944,45\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::ACRU//USD23998,55\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String T_BILLS_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I541MELNUS3PXGLBN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//456\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			T_BILLS_COMMON_SUFFIX;

	private static final String T_BILLS_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I541MELNUS3PXGLBN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//456\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			T_BILLS_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";


	private static final String OPTIONS_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":94B::TRAD//EXCH/XCBO\n" +
			":98A::SETT//20180117\n" +
			":98A::TRAD//20180116\n" +
			":90B::DEAL//ACTU/USD10,7\n" +
			":35B:/TS/SPXW 180216P02650000\n" +
			"SPXW US 02/16/18 P2650\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/OPT\n" +
			":12B::OPST//EURO\n" +
			":12B::OPTI//PUTO\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20180216\n" +
			":90B::EXER//ACTU/USD2650,\n" +
			":36B::SIZE//UNIT/100,\n" +
			":16S:FIA\n" +
			":22F::PROC//OPEP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/6,\n" +
			":97A::SAFE//927801\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95Q::PSET//XX\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::DEAG//SBNYUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::SELL//SBNYUS33\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD6420,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD6420,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD0,\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String OPTIONS_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I541IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//678\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			OPTIONS_COMMON_SUFFIX;

	private static final String OPTIONS_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I541IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//456\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			OPTIONS_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	private static final String REPO_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":98A::SETT//20160929\n" +
			":98A::TRAD//20160929\n" +
			":90A::DEAL//PRCT/102,0234375\n" +
			":35B:ISIN US912828H458\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/TIPS\n" +
			":11A::DENO//USD\n" +
			":98A::MATU//20250115\n" +
			":98A::ISSU//20150130\n" +
			":92A::INTR//0,25\n" +
			":16S:FIA\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//FAMT/48940000,\n" +
			":97A::SAFE//CUST\n" +
			":16S:FIAC\n" +
			":16R:REPO\n" +
			":98A::TERM//OPEN\n" +
			":22F::RERT//VARI\n" +
			":92A::REPO//0,54\n" +
			":99B::TOCO//001\n" +
			":19A::TRTE//USD49766257,77\n" +
			":19A::ACRU//USD746,48\n" +
			":16S:REPO\n" +
			":16R:SETDET\n" +
			":22F::SETR//RVPO\n" +
			":16R:SETPRTY\n" +
			":95P::PSET//FRNYUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::REAG/USFW/021000018\n" +
			":97A::SAFE//4175220\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::BUYR/DTCYID/901\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD49765511,29\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD49765511,29\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String REPO_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I543IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//IMS1II??5EIMS1415LR\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			REPO_COMMON_SUFFIX;

	private static final String REPO_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I543IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//IMS1II??5EIMS1415LR\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			REPO_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	private static final String REPO_CANCEL_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I543IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//IMS1II??5EIMS1415LR\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//1\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			REPO_COMMON_SUFFIX;

	private static final String REPO_CANCEL_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I543IRVTUS3NXIBKN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//IMS1II??5EIMS1415LR\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//1\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			REPO_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	private static SwiftTestMessageServer swiftTestMessageServer;

	@Resource
	private TradeCreator tradeCreator;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentInstructionUtils investmentInstructionUtils;

	@Resource
	private InvestmentInstructionService investmentInstructionService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	@Resource
	private InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService;

	private SimpleEntity<Integer> fkObject;

	private Date tradeDate;

	private InvestmentInstructionCategory category;

	private InvestmentInstructionDeliveryType originalDeliveryType;

	private final Set<InvestmentInstructionDeliveryBusinessCompany> investmentInstructionDeliveryBusinessCompanies = new HashSet<>();

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void beforeClass() throws Exception {
		swiftTestMessageServer = new SwiftTestMessageServer(CONTEXT_PROPERTIES_PATH, TEST_DATA_PATH);
		swiftTestMessageServer.start();
	}


	@AfterAll
	public static void afterClass() throws Exception {
		swiftTestMessageServer.shutdown();
	}


	@BeforeEach
	public void before() {
		Assertions.assertNotNull(swiftTestMessageServer.sendStartTestSession());
		this.category = this.investmentInstructionUtils.getInvestmentInstructionCategoryByName("Trade Counterparty");
		Assertions.assertNotNull(this.category);

		InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setName("FED Book Delivery Instructions - Barclays Capital Inc");

		InvestmentInstructionDefinition fixedIncomeInstructionDefinition = this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm).stream()
				.findFirst()
				.orElseThrow(() -> new RuntimeException("Cannot find Investment Instruction Definition with name [FED Book Delivery Instructions - Barclays Capital Inc]"));
		this.originalDeliveryType = fixedIncomeInstructionDefinition.getDeliveryType();
		InvestmentInstructionDeliveryType deliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, true);
		Assertions.assertNotNull(deliveryType);

		searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setDeliveryTypeId(deliveryType.getId());
		CollectionUtils.getStream(this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm))
				.forEach(d -> {
					d.setDeliveryType(this.originalDeliveryType);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		this.investmentInstructionDeliveryBusinessCompanies
				.forEach(d -> this.investmentInstructionDeliveryBusinessCompanyService.deleteInvestmentInstructionDeliveryBusinessCompany(d.getId()));
	}


	@AfterEach
	public void after() {
		swiftTestMessageServer.sendStopTestSession();

		InvestmentInstructionStatus errorStatus = this.investmentInstructionUtils.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);

		List<InvestmentInstructionItem> items = this.investmentInstructionUtils.getInstructionItemList(this.category, this.tradeDate, this.fkObject).stream()
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.COMPLETED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.CANCELED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.ERROR.name()))
				.collect(Collectors.toList());
		for (InvestmentInstructionItem item : items) {
			item.setStatus(errorStatus);
			this.investmentInstructionUtils.saveInvestmentInstructionItem(item);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void futuresTest() throws Exception {
		this.tradeDate = DateUtils.toDate("8/23/2017", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.STATE_STREET_BANK,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(
				accountInfo,
				"FAU17",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				7.00000000,
				true,
				TradeDestination.MANUAL,
				BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		this.fkObject = trade;
		trade = this.approveTrade(trade);

		// update the trade fill average price.
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		TradeFill tradeFill = tradeFills.get(0);
		tradeFill.setQuantity(new BigDecimal("7"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("1702.771428"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("1191940.00"));
		this.tradeService.saveTradeFill(tradeFill);

		trade = this.fullyExecuteTrade(trade);
		Assertions.assertNotNull(trade);

		// setup test case
		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create().
				with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_REQUEST)
								.withReplacement("97A", ":SAFE//1495990614", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_RESPONSE)
								.withReplacement("97A", ":SAFE//1495990614", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build()
				).build();
		swiftTestMessageServer.sendSwiftTestCase(swiftTestCase);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, trade);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// send instruction items.
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift to exhaust test case
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for instruction items to be marked completed.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	@Test
	public void futuresCancelTest() throws Exception {
		this.tradeDate = DateUtils.toDate("8/23/2017", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.STATE_STREET_BANK,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(
				accountInfo,
				"FAU7",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				7.00000000,
				true,
				TradeDestination.MANUAL,
				BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		this.fkObject = trade;
		trade = this.approveTrade(trade);

		// update the trade fill average price.
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		TradeFill tradeFill = tradeFills.get(0);
		tradeFill.setQuantity(new BigDecimal("7"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("1702.771428"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("1191940.00"));
		this.tradeService.saveTradeFill(tradeFill);

		trade = this.fullyExecuteTrade(trade);
		Assertions.assertNotNull(trade);

		// setup test case
		swiftTestMessageServer.sendSwiftTestCase(SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_REQUEST)
								.withReplacement("97A", ":SAFE//1495990614", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_RESPONSE)
								.withReplacement("97A", ":SAFE//1495990614", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build()
				)
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_CANCEL_REQUEST)
								.withReplacement("97A", ":SAFE//1495990614", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C", "SEME")
								.withCopyRequestValue("20C", "PREV")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_CANCEL_RESPONSE)
								.withReplacement("97A", ":SAFE//1495990614", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C", "SEME")
								.withCopyRequestValue("20C", "PREV")
								.build()
				)
				.build());

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, trade);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");

		// get items to cancel.
		Integer[] items = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("Trade", trade.getId()).stream()
				.map(InvestmentInstructionItem::getId)
				.toArray(Integer[]::new);

		// cancel items.
		this.investmentInstructionService.cancelInvestmentInstructionItemList(items);
		int[] pendingCancelItems = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("Trade", trade.getId()).stream()
				.filter(i -> InvestmentInstructionStatusNames.PENDING_CANCEL == i.getStatus().getInvestmentInstructionStatusName())
				.mapToInt(InvestmentInstructionItem::getId)
				.toArray();
		Assertions.assertTrue(pendingCancelItems.length > 0);

		// send PENDING_CANCEL items.
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift server to send all responses.
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for cancelled status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.CANCELED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, trade, InvestmentInstructionStatusNames.CANCELED, instructionItems), "All instructions items should be [CANCELLED].");
	}


	@Test
	public void fundsTest() throws Exception {
		this.tradeDate = DateUtils.toDate("8/23/2017", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.STATE_STREET_BANK,
				this.tradeUtils.getTradeType(TradeType.FUNDS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFundsTrade(
				accountInfo,
				"SHY",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				1130.00000000,
				true,
				0.01,
				TradeDestination.MANUAL,
				BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		this.fkObject = trade;
		trade = this.approveTrade(trade);

		// update the trade fill average price.
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		TradeFill tradeFill = tradeFills.get(0);
		tradeFill.setQuantity(new BigDecimal("1130"));
		tradeFill.setPaymentUnitPrice(new BigDecimal("84.38"));
		tradeFill.setPaymentTotalPrice(new BigDecimal("95349.40"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("84.38"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("95349.40"));
		this.tradeService.saveTradeFill(tradeFill);

		trade = this.fullyExecuteTrade(trade);

		// setup test case
		swiftTestMessageServer.sendSwiftTestCase(SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(EQUITIES_REQUEST)
								.withReplacement("97A", ":SAFE//362876374", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(EQUITIES_RESPONSE)
								.withReplacement("97A", ":SAFE//362876374", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build()
				)
				.build());

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, trade);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// send instructions
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift to exhaust test case
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for complete status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	@Test
	public void tBillsTest() throws Exception {
		this.tradeDate = DateUtils.toDate("11/30/2017", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.BONDS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		// 770784
		Trade trade = this.tradeCreator.createBondTrade(
				accountInfo,
				"912810RW0",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				new BigDecimal("9140000.00"),
				new BigDecimal("99.8125"),
				new BigDecimal("23998.55"),
				new BigDecimal("1.02248"),
				true,
				TradeDestination.BLOOMBERG_TICKET,
				BusinessCompanies.BARCLAYS_CAPITOL_INC);
		this.fkObject = trade;
		trade = this.approveTrade(trade);

		// update the trade fill average price.
		TradeFill tradeFill = new TradeFill();
		tradeFill.setTrade(trade);
		tradeFill.setQuantity(new BigDecimal("9140000.00"));
		tradeFill.setPaymentUnitPrice(new BigDecimal("99.8125"));
		tradeFill.setPaymentTotalPrice(new BigDecimal("9327944.45"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("99.8125"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("9327944.45"));
		this.tradeService.saveTradeFill(tradeFill);

		trade = this.finishExecuteTrade(trade);
		trade = this.bookAndPostTrade(trade);

		// setup test case
		swiftTestMessageServer.sendSwiftTestCase(SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(T_BILLS_REQUEST)
								.withReplacement("97A", ":SAFE//SWBF0909002", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(T_BILLS_RESPONSE)
								.withReplacement("97A", ":SAFE//SWBF0909002", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build()
				)
				.build());

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		List<InvestmentInstructionItem> investmentInstructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade))
				.collect(Collectors.toList());
		Assertions.assertFalse(investmentInstructionItems.isEmpty(), "Should have Open Instruction Items.");

		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, true);
		Assertions.assertNotNull(investmentInstructionDeliveryType);

		InvestmentInstructionDelivery instructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
				BusinessCompanies.HSBC_BANK_USA_NATIONAL_ASSOCIATION), DELIVERY_NAME);
		if (Objects.isNull(instructionDelivery)) {
			instructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
					BusinessCompanies.HSBC_BANK_USA_NATIONAL_ASSOCIATION), DELIVERY_NAME, "89-0000-6978", "021001088");
		}
		Assertions.assertNotNull(instructionDelivery);

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, trade);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// make sure definitions have a delivery type
		investmentInstructionItems.stream()
				.map(InvestmentInstructionItem::getInstruction)
				.map(InvestmentInstruction::getDefinition)
				.filter(i -> Objects.isNull(i.getDeliveryType()))
				.forEach(d -> {
					d.setDeliveryType(investmentInstructionDeliveryType);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		// link between type and business company (morgan stanley).
		InvestmentInstructionDeliveryBusinessCompanySearchForm linkSearchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		linkSearchForm.setBusinessCompanyId(accountInfo.getHoldingAccount().getIssuingCompany().getId());
		linkSearchForm.setTypeId(investmentInstructionDeliveryType.getId());
		linkSearchForm.setDeliveryCurrencyId(this.usd.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> links = this.investmentInstructionDeliveryBusinessCompanyService.getInvestmentInstructionDeliveryBusinessCompanyList(linkSearchForm);
		if (links.isEmpty()) {
			InvestmentInstructionDeliveryBusinessCompany link = new InvestmentInstructionDeliveryBusinessCompany();
			link.setDeliveryCurrency(this.usd);
			link.setInvestmentInstructionDeliveryType(investmentInstructionDeliveryType);
			link.setReferenceOne(instructionDelivery);
			link.setReferenceTwo(accountInfo.getHoldingAccount().getIssuingCompany());
			this.investmentInstructionDeliveryBusinessCompanyService.saveInvestmentInstructionDeliveryBusinessCompany(link);
			this.investmentInstructionDeliveryBusinessCompanies.add(link);
		}

		// send instructions
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift to exhaust test case
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for complete status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	@Test
	public void optionsTest() throws Exception {
		this.tradeDate = DateUtils.toDate("1/16/2018", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForOptions(BusinessCompanies.CITIGROUP, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.OPTIONS));

		InvestmentAccountGroup investmentAccountGroup = this.investmentAccountGroupService.getInvestmentAccountGroupByName("Options Margin Collateral Accounts");
		Assertions.assertNotNull(investmentAccountGroup);
		this.investmentAccountGroupService.rebuildInvestmentAccountGroup(investmentAccountGroup.getId());

		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createOptionsTrade(
				accountInfo,
				"SPXW US 02/16/18 P2650",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				new BigDecimal("6"),
				true,
				TradeDestination.MANUAL,
				BusinessCompanies.CITIGROUP,
				TradeBuilder.BUY_TO_OPEN);
		this.fkObject = trade;
		trade = this.approveTrade(trade);

		// update the trade fill average price.
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		TradeFill tradeFill = tradeFills.get(0);
		tradeFill.setQuantity(new BigDecimal("6"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("10.7"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("6420.00"));
		tradeFill.setPaymentUnitPrice(new BigDecimal("10.7"));
		tradeFill.setPaymentTotalPrice(new BigDecimal("6420.00"));
		this.tradeService.saveTradeFill(tradeFill);

		trade = this.fullyExecuteTrade(trade);
		Assertions.assertNotNull(trade);

		// setup test case
		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create().
				with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(OPTIONS_REQUEST)
								.withReplacement("97A", ":SAFE//927801", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(OPTIONS_RESPONSE)
								.withReplacement("97A", ":SAFE//927801", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.build()
				).build();
		swiftTestMessageServer.sendSwiftTestCase(swiftTestCase);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// wait for the system managed account to rebuild linkages.
		int holdingAccountId = accountInfo.getHoldingAccount().getId();
		PeriodicRepeatingExecutor.await(() -> this.investmentAccountGroupService.getInvestmentAccountListByGroup("Options Margin Collateral Accounts").stream().anyMatch(g -> g.getId().equals(holdingAccountId)), 20, 1);
		Assertions.assertTrue(this.investmentAccountGroupService.getInvestmentAccountListByGroup("Options Margin Collateral Accounts").stream().anyMatch(g -> g.getId().equals(holdingAccountId)));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, trade);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// send instruction items.
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift to exhaust test case
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for instruction items to be marked completed.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	@Test
	public void reposTest() throws InterruptedException {
		this.tradeDate = DateUtils.toDate("09/29/2016", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, "Prohibit REPOs");
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828H45", false);

		LendingRepo repo = this.lendingRepoUtils.newOpenLendingRepoBuilder(security, new BigDecimal("48940000.0000000000"), this.tradeDate, accountInfo)
				.price(new BigDecimal("102.0234375000"))
				.haircutPercent(new BigDecimal("102.0000000000"))
				.indexRatio(new BigDecimal("1.0161200000"))
				.interestRate(new BigDecimal("0.5400000000"))
				.settlementDate(DateUtils.toDate("09/29/2016"))
				.dayCountConvention("Actual/360")
				.active(true)
				.instrumentHierarchy(security.getInstrument().getHierarchy())
				.reverse(true)
				.buildAndSave();
		this.fkObject = repo;

		swiftTestMessageServer.sendSwiftTestCase(SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(REPO_REQUEST)
								.withReplacement("97A", ":SAFE//CUST", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("97A", ":SAFE//4175220", ":SAFE//BK OF NYC/SEC LEND")
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(REPO_RESPONSE)
								.withReplacement("97A", ":SAFE//CUST", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("97A", ":SAFE//4175220", ":SAFE//BK OF NYC/SEC LEND")
								.withCopyRequestValue("20C")
								.build()
				)
				.build());

		// advance workflow and confirm
		this.lendingRepoUtils.executeRepo(repo, false);
		this.lendingRepoUtils.confirmLendingRepo(repo.getId(), "JUnit Test", true);

		// category
		this.category = this.investmentInstructionUtils.getInvestmentInstructionCategoryByName("REPO Counterparty");
		Assertions.assertNotNull(this.category);
		this.investmentInstructionUtils.activateInstructionDefinition("REPO Counterparty", "Lending REPO Instructions - All brokers");

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		// should have created them.
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, repo))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, repo).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, repo);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType("REPO", true);
		Assertions.assertNotNull(investmentInstructionDeliveryType);

		InvestmentInstructionDelivery instructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
				BusinessCompanies.MELLON_BANK_NA), DELIVERY_NAME + "-reposTest");
		if (Objects.isNull(instructionDelivery)) {
			instructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
					BusinessCompanies.MELLON_BANK_NA), DELIVERY_NAME + "-reposTest", "89-0000-6979", "021001089");
		}
		Assertions.assertNotNull(instructionDelivery);

		// link between type and business company
		InvestmentInstructionDeliveryBusinessCompanySearchForm linkSearchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		linkSearchForm.setBusinessCompanyId(instructionDelivery.getDeliveryCompany().getId());
		linkSearchForm.setTypeId(investmentInstructionDeliveryType.getId());
		linkSearchForm.setDeliveryCurrencyId(this.usd.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> links = this.investmentInstructionDeliveryBusinessCompanyService.getInvestmentInstructionDeliveryBusinessCompanyList(linkSearchForm);
		if (links.isEmpty()) {
			InvestmentInstructionDeliveryBusinessCompany link = new InvestmentInstructionDeliveryBusinessCompany();
			link.setDeliveryCurrency(this.usd);
			link.setInvestmentInstructionDeliveryType(investmentInstructionDeliveryType);
			link.setReferenceOne(instructionDelivery);
			link.setReferenceTwo(instructionDelivery.getDeliveryCompany());
			this.investmentInstructionDeliveryBusinessCompanyService.saveInvestmentInstructionDeliveryBusinessCompany(link);
			this.investmentInstructionDeliveryBusinessCompanies.add(link);
		}

		// send SWIFT message
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift to exhaust test case
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for instruction items to be marked completed.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, repo, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	@Test
	public void reposCancelTest() throws InterruptedException {
		this.tradeDate = DateUtils.toDate("09/29/2016", DateUtils.DATE_FORMAT_INPUT);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, "Prohibit REPOs");
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828H45", false);

		LendingRepo repo = this.lendingRepoUtils.newOpenLendingRepoBuilder(security, new BigDecimal("48940000.0000000000"), this.tradeDate, accountInfo)
				.price(new BigDecimal("102.0234375000"))
				.haircutPercent(new BigDecimal("102.0000000000"))
				.indexRatio(new BigDecimal("1.0161200000"))
				.interestRate(new BigDecimal("0.5400000000"))
				.settlementDate(DateUtils.toDate("09/29/2016"))
				.dayCountConvention("Actual/360")
				.active(true)
				.instrumentHierarchy(security.getInstrument().getHierarchy())
				.reverse(true)
				.buildAndSave();
		this.fkObject = repo;

		swiftTestMessageServer.sendSwiftTestCase(SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(REPO_REQUEST)
								.withReplacement("97A", ":SAFE//CUST", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("97A", ":SAFE//4175220", ":SAFE//BK OF NYC/SEC LEND")
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(REPO_RESPONSE)
								.withReplacement("97A", ":SAFE//CUST", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("97A", ":SAFE//4175220", ":SAFE//BK OF NYC/SEC LEND")
								.withCopyRequestValue("20C")
								.build()
				)
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(REPO_CANCEL_REQUEST)
								.withReplacement("97A", ":SAFE//CUST", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("97A", ":SAFE//4175220", ":SAFE//BK OF NYC/SEC LEND")
								.withCopyRequestValue("20C", "SEME")
								.withCopyRequestValue("20C", "PREV")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(REPO_CANCEL_RESPONSE)
								.withReplacement("97A", ":SAFE//CUST", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("97A", ":SAFE//4175220", ":SAFE//BK OF NYC/SEC LEND")
								.withCopyRequestValue("20C", "SEME")
								.withCopyRequestValue("20C", "PREV")
								.build()
				)
				.build());

		// advance workflow and confirm
		this.lendingRepoUtils.executeRepo(repo, false);
		this.lendingRepoUtils.confirmLendingRepo(repo.getId(), "JUnit Test", true);

		// category
		this.category = this.investmentInstructionUtils.getInvestmentInstructionCategoryByName("REPO Counterparty");
		Assertions.assertNotNull(this.category);
		this.investmentInstructionUtils.activateInstructionDefinition("REPO Counterparty", "Lending REPO Instructions - All brokers");

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		// should have created them.
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, repo))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, repo).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		assertInstructionDefinitionExists(investmentInstruction, repo);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType("REPO", true);
		Assertions.assertNotNull(investmentInstructionDeliveryType);

		InvestmentInstructionDelivery instructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
				BusinessCompanies.MELLON_BANK_NA), DELIVERY_NAME + "-reposTest");
		if (Objects.isNull(instructionDelivery)) {
			instructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
					BusinessCompanies.MELLON_BANK_NA), DELIVERY_NAME + "-reposTest", "89-0000-6979", "021001089");
		}
		Assertions.assertNotNull(instructionDelivery);

		// link between type and business company
		InvestmentInstructionDeliveryBusinessCompanySearchForm linkSearchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		linkSearchForm.setBusinessCompanyId(instructionDelivery.getDeliveryCompany().getId());
		linkSearchForm.setTypeId(investmentInstructionDeliveryType.getId());
		linkSearchForm.setDeliveryCurrencyId(this.usd.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> links = this.investmentInstructionDeliveryBusinessCompanyService.getInvestmentInstructionDeliveryBusinessCompanyList(linkSearchForm);
		if (links.isEmpty()) {
			InvestmentInstructionDeliveryBusinessCompany link = new InvestmentInstructionDeliveryBusinessCompany();
			link.setDeliveryCurrency(this.usd);
			link.setInvestmentInstructionDeliveryType(investmentInstructionDeliveryType);
			link.setReferenceOne(instructionDelivery);
			link.setReferenceTwo(instructionDelivery.getDeliveryCompany());
			this.investmentInstructionDeliveryBusinessCompanyService.saveInvestmentInstructionDeliveryBusinessCompany(link);
			this.investmentInstructionDeliveryBusinessCompanies.add(link);
		}

		// send SWIFT message
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for instruction items to be marked completed.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, repo, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");

		// get items to cancel.
		Integer[] items = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("LendingRepo", repo.getId()).stream()
				.map(InvestmentInstructionItem::getId)
				.toArray(Integer[]::new);

		// cancel items.
		this.investmentInstructionService.cancelInvestmentInstructionItemList(items);
		int[] pendingCancelItems = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("LendingRepo", repo.getId()).stream()
				.filter(i -> InvestmentInstructionStatusNames.PENDING_CANCEL == i.getStatus().getInvestmentInstructionStatusName())
				.mapToInt(InvestmentInstructionItem::getId)
				.toArray();
		Assertions.assertTrue(pendingCancelItems.length > 0);

		// send PENDING_CANCEL items.
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift server to send all responses.
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for cancelled status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.fkObject, InvestmentInstructionStatusNames.CANCELED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, repo, InvestmentInstructionStatusNames.CANCELED, instructionItems), "All instructions items should be [CANCELLED].");
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private Trade approveTrade(Trade trade) {
		this.tradeUtils.approveTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private Trade fullyExecuteTrade(Trade trade) {
		this.tradeUtils.fullyExecuteTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private Trade finishExecuteTrade(Trade trade) {
		this.tradeUtils.finishExecutionTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private Trade bookAndPostTrade(Trade trade) {
		this.tradeUtils.bookAndPostTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private void assertInstructionDefinitionExists(InvestmentInstruction investmentInstruction, BaseEntity<?> baseEntity) {
		// If no Investment Instruction is found, the Instruction Definition may be in a non-Active workflow state.
		// If that is the case, check with the OPS team because they may want it to be Active.
		Assertions.assertNotNull(investmentInstruction, String.format("Unable to find Instruction Definition for Trade [%s]", baseEntity));
	}
}
