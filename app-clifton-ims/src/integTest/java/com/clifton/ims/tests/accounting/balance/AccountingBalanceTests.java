package com.clifton.ims.tests.accounting.balance;

import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingBalanceRetrieverTestExecutor;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.junit.jupiter.api.Test;


/**
 * @author nickk
 */
public class AccountingBalanceTests extends BaseImsIntegrationTest {


	@Test
	public void testAccountingBalanceAndAccountingPositionBalanceMatch() {
		AccountingBalanceRetrieverTestExecutor.newExecutorForTransactionDate(this.accountingUtils, DateUtils.toDate("03/31/2020"))
				.withSecurity(getSecurityBySymbol("78445JAA5"), 3) // ABS Bond
				.withSecurity(getSecurityBySymbol("31342A7A1"), 4) // MBS Bond
				.withSecurity(getSecurityBySymbol("38377XMP4"), 4) // CMO Bond
				.withSecurity(getSecurityBySymbol("CTH516593840AA"), 2) // CDS Swap
				.withSecurity(getSecurityBySymbol("BOU20"), 4) // Future
				.withSecurity(getSecurityBySymbol("AAPL US 04/17/20 C325"), 2) // Option
				.withSecurity(getSecurityBySymbol("BABA"), 3) // Stock
				.execute();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private InvestmentSecurity getSecurityBySymbol(String symbol) {
		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol), 2);
	}
}
