package com.clifton.ims.tests.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.commission.AccountingCommissionApplyMethods;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.accounting.commission.AccountingCommissionTypes;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationMethods;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;


public class AccountingCommissionDefinitionSaveTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingCommissionService accountingCommissionService;

	@Resource
	private AccountingTransactionTypeService accountingTransactionTypeService;


	@Test
	public void testNewDefinitionUpdatesOld() {
		InvestmentSecurity ubm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("UBM4");
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		AccountingCommissionDefinition firstCommission = null;
		AccountingCommissionDefinition secondCommission = null;

		try {
			firstCommission = createOrUpdateDefinition(ubm4, accountInfo.getHoldingAccount(), DateUtils.toDate("01/01/2014"), null);

			firstCommission = this.accountingCommissionService.getAccountingCommissionDefinition(firstCommission.getId());
			Assertions.assertEquals(DateUtils.toDate("01/01/2014"), firstCommission.getStartDate());
			Assertions.assertNull(firstCommission.getEndDate());

			//Since the "new" definition has the same level of "specificity" as the existing, it should cause the existing definition's end date to be updated to one day before the new Start Date
			secondCommission = createOrUpdateDefinition(ubm4, accountInfo.getHoldingAccount(), DateUtils.toDate("02/02/2014"), null);
			firstCommission = this.accountingCommissionService.getAccountingCommissionDefinition(firstCommission.getId());
			Assertions.assertEquals(DateUtils.toDate("01/01/2014"), firstCommission.getStartDate());
			Assertions.assertEquals(DateUtils.toDate("02/01/2014"), firstCommission.getEndDate());

			secondCommission = this.accountingCommissionService.getAccountingCommissionDefinition(secondCommission.getId());
			Assertions.assertEquals(DateUtils.toDate("02/02/2014"), secondCommission.getStartDate());
			Assertions.assertNull(secondCommission.getEndDate());
		}
		finally {
			if (firstCommission != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(firstCommission.getId());
			}
			if (secondCommission != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(secondCommission.getId());
			}
		}
	}


	@Test
	public void testCreateOldDefinitionDoesNotUpdateNew() {
		InvestmentSecurity ubm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("UBM4");
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		AccountingCommissionDefinition firstCommission = null;
		AccountingCommissionDefinition secondCommission = null;

		try {
			firstCommission = createOrUpdateDefinition(ubm4, accountInfo.getHoldingAccount(), DateUtils.toDate("01/01/2014"), null);

			firstCommission = this.accountingCommissionService.getAccountingCommissionDefinition(firstCommission.getId());
			Assertions.assertEquals(DateUtils.toDate("01/01/2014"), firstCommission.getStartDate());
			Assertions.assertNull(firstCommission.getEndDate());

			//Since we are creating an "old" definition that has the same level of "specificity" of a newer existing definition, the existing definition should not be updated
			secondCommission = createOrUpdateDefinition(ubm4, accountInfo.getHoldingAccount(), DateUtils.toDate("01/01/2013"), DateUtils.toDate("02/01/2013"));

			firstCommission = this.accountingCommissionService.getAccountingCommissionDefinition(firstCommission.getId());
			Assertions.assertEquals(DateUtils.toDate("01/01/2014"), firstCommission.getStartDate());
			Assertions.assertNull(firstCommission.getEndDate());

			secondCommission = this.accountingCommissionService.getAccountingCommissionDefinition(secondCommission.getId());
			Assertions.assertEquals(DateUtils.toDate("01/01/2013"), secondCommission.getStartDate());
			Assertions.assertEquals(DateUtils.toDate("02/01/2013"), secondCommission.getEndDate());
		}
		finally {
			if (firstCommission != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(firstCommission.getId());
			}
			if (secondCommission != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(secondCommission.getId());
			}
		}
	}


	@Test
	public void testOverlappingDatesNotAllowed() {
		Assertions.assertThrows(ImsErrorResponseException.class, () -> {
			InvestmentSecurity ubm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("UBM4");
			AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

			AccountingCommissionDefinition firstCommission = null;
			AccountingCommissionDefinition secondCommission = null;

			try {
				firstCommission = createOrUpdateDefinition(ubm4, accountInfo.getHoldingAccount(), DateUtils.toDate("01/01/2014"), DateUtils.toDate("02/01/2014"));

				firstCommission = this.accountingCommissionService.getAccountingCommissionDefinition(firstCommission.getId());
				Assertions.assertEquals(DateUtils.toDate("01/01/2014"), firstCommission.getStartDate());
				Assertions.assertEquals(DateUtils.toDate("02/01/2014"), firstCommission.getEndDate());

				//The dates overlap, so this shouldn't be allowed (exception will be thrown)
				secondCommission = createOrUpdateDefinition(ubm4, accountInfo.getHoldingAccount(), DateUtils.toDate("01/15/2014"), DateUtils.toDate("02/15/2014"));
			}
			finally {
				if (firstCommission != null) {
					this.accountingCommissionService.deleteAccountingCommissionDefinition(firstCommission.getId());
				}
				if (secondCommission != null) {
					this.accountingCommissionService.deleteAccountingCommissionDefinition(secondCommission.getId());
				}
			}
		});
	}


	private AccountingCommissionDefinition createOrUpdateDefinition(InvestmentSecurity security, InvestmentAccount holdingAccount, Date startDate, Date endDate) {
		AccountingCommissionDefinition commissionDefinition = getPopulatedCommissionDefinition(security, holdingAccount, startDate, endDate);
		return this.accountingCommissionService.saveAccountingCommissionDefinition(commissionDefinition);
	}


	private AccountingCommissionDefinition getPopulatedCommissionDefinition(InvestmentSecurity security, InvestmentAccount holdingAccount, Date startDate, Date endDate) {
		AccountingCommissionDefinition commissionDefinition = new AccountingCommissionDefinition();
		commissionDefinition.setInvestmentSecurity(security);
		commissionDefinition.setHoldingAccount(holdingAccount);
		commissionDefinition.setStartDate(startDate);
		commissionDefinition.setEndDate(endDate);
		commissionDefinition.setExpenseAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_COMMISSION));
		commissionDefinition.setTransactionType(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));
		commissionDefinition.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
		commissionDefinition.setCommissionType(AccountingCommissionTypes.SIMPLE);
		commissionDefinition.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
		return commissionDefinition;
	}
}
