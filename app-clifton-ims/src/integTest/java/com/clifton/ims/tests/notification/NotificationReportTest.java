package com.clifton.ims.tests.notification;

import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.notification.generator.Notification;
import com.clifton.notification.generator.NotificationGeneratorService;
import com.clifton.notification.generator.NotificationSearchForm;
import com.clifton.notification.runner.NotificationBatchRunnerCommand;
import com.clifton.notification.runner.NotificationBatchRunnerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * This is a specific test that generates notifications based on specified definitions. The intent is to use these
 * notifications to detect issues with a pre-release build.  To configure a specific notification
 * to run, add its definition names to the 'definitionNames' array.  The test will run each definition and fail if any
 * notification entry is generated. (which would indicate an issue was detected).
 *
 * @author davidi
 */
public class NotificationReportTest extends BaseImsIntegrationTest {

	// Add definition names here to have the test run and check for generated notifications.
	final String[] definitionNames = {
			"CustomJsonString Data Integrity Notification", // if this notification triggers, look at the notification text to figure out which tables have issue(s)
			"System: Bean Property Type without Value Table",
			"System: Missing Foreign Key Constraints",
			"System: Orphan System Column Definitions",
			"System: Orphan System Relationships Definitions",
			"System: Table Security Resource Not Assigned",
			"System: Undefined System Relationships",
			"System: Undefined Table Columns",
			"Workflow: Invalid Transition Action Indicator",
			"Workflow History: Multiple Records with NULL End Date",
	};

	@Resource
	private NotificationDefinitionService<NotificationDefinitionBatch> notificationDefinitionService;

	@Resource
	private NotificationGeneratorService<NotificationDefinitionBatch> notificationGeneratorService;

	@Resource
	private NotificationBatchRunnerService notificationBatchRunnerService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void ReportSelectedNotificationsTest() {

		List<String> messages = new ArrayList<>();
		int definitionCount = 0;

		for (String definitionName : this.definitionNames) {
			NotificationDefinitionSearchForm searchForm = new NotificationDefinitionSearchForm();
			searchForm.setName(definitionName);

			List<NotificationDefinition> notificationDefinitionList = this.notificationDefinitionService.getNotificationDefinitionGenericList(searchForm);

			if (notificationDefinitionList.isEmpty()) {
				messages.add("Cannot find NotificationDefinition with name: " + definitionName + ".");
				continue;
			}

			NotificationDefinitionBatch notificationDefinition = (NotificationDefinitionBatch) notificationDefinitionList.get(0);
			NotificationBatchRunnerCommand command = new NotificationBatchRunnerCommand();
			command.setNotificationDefinitionId(notificationDefinition.getId());
			command.setRunIfInactive(true);
			command.setSynchronous(true);
			this.notificationBatchRunnerService.runNotificationBatchByCommand(command);

			NotificationSearchForm notificationSearchForm = new NotificationSearchForm();
			notificationSearchForm.setDefinitionId(notificationDefinition.getId());
			notificationSearchForm.setDoNotSetUserId(true);
			List<Notification> notificationList = this.notificationGeneratorService.getNotificationList(notificationSearchForm);

			if (!CollectionUtils.isEmpty(notificationList)) {
				messages.add(notificationDefinition.getLabel());
				definitionCount++;
				for (Notification notification : notificationList) {
					messages.add("\t" + notification.getLabel());
				}
			}
		}

		// convert messages to single text block
		String combinedMessages = String.join(System.lineSeparator(), messages);
		Assertions.assertEquals(0, messages.size() - definitionCount, "Notifications detected:" + System.lineSeparator() + combinedMessages + System.lineSeparator());
	}
}

