package com.clifton.ims.tests.investment.instrument.customcolumns;

import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * @author MitchellF
 */
public class InvestmentInstrumentJsonColumnsTests extends BaseImsIntegrationTest {

	private static final String SP_MINI_FUTURES_INSTRUMENT_NAME = "S&P 500 Mini Futures (ES)";
	private static final String SYSTEM_TABLE_NAME = "InvestmentInstrument";

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private SystemColumnService systemColumnService;

	@Resource
	private SystemSchemaService systemSchemaService;


	@Test
	public void testAssignValueToExistingJsonColumn() {
		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(SP_MINI_FUTURES_INSTRUMENT_NAME);
		InvestmentInstrument existing = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		CustomJsonString customColumns = existing.getCustomColumns();
		ValidationUtils.assertNotNull(customColumns, "Instrument did not have expected custom columns");

		customColumns = CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(customColumns);

		String bticPrefix = (String) customColumns.getColumnValue("BTIC Prefix");
		ValidationUtils.assertEquals(bticPrefix, "STE", "Custom JSON value for BTIC prefix was not as expected");

		customColumns.replaceColumnValue("BTIC Prefix", "TEST");
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(customColumns);
		existing.setCustomColumns(customColumns);
		this.investmentInstrumentService.saveInvestmentInstrument(existing);
		//reload existing
		existing = this.investmentInstrumentService.getInvestmentInstrument(existing.getId());

		customColumns = existing.getCustomColumns();
		ValidationUtils.assertNotNull(customColumns, "Instrument did not have expected custom columns");

		customColumns = CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(customColumns);

		bticPrefix = (String) customColumns.getColumnValue("BTIC Prefix");
		ValidationUtils.assertEquals(bticPrefix, "TEST", "Custom JSON value for BTIC prefix was not as expected");
	}


	@Test
	public void testCreateNewCustomColumn() {
		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(SP_MINI_FUTURES_INSTRUMENT_NAME);
		InvestmentInstrument existing = CollectionUtils.getOnlyElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));

		CustomJsonString customColumns = existing.getCustomColumns();
		ValidationUtils.assertNotNull(customColumns, "Instrument did not have expected custom columns");

		customColumns = CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(customColumns);

		customColumns.setColumnValue("TestColumn", "val", "text");
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(customColumns);
		existing.setCustomColumns(customColumns);

		InvestmentInstrument finalExisting = existing;
		TestUtils.expectException(ImsErrorResponseException.class, () -> this.investmentInstrumentService.saveInvestmentInstrument(finalExisting), "A custom value cannot be assigned for custom JSON column that does not exist.");

		// Create new column and see if it allows save (it should)
		SystemColumnCustom newCustomColumn = new SystemColumnCustom();
		newCustomColumn.setDataType(this.systemSchemaService.getSystemDataTypeByName("STRING"));
		newCustomColumn.setColumnGroup(this.systemColumnService.getSystemColumnGroupByName(InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME));
		newCustomColumn.setTable(this.systemSchemaService.getSystemTableByName(SYSTEM_TABLE_NAME));
		newCustomColumn.setName("TestColumn");
		newCustomColumn.setLabel("Test Column");
		newCustomColumn.setDescription("Test Column");
		this.systemColumnService.saveSystemColumnCustom(newCustomColumn);
		this.investmentInstrumentService.saveInvestmentInstrument(existing);
		//reload existing
		existing = this.investmentInstrumentService.getInvestmentInstrument(existing.getId());
		customColumns = existing.getCustomColumns();
		ValidationUtils.assertNotNull(customColumns, "Instrument did not have expected custom columns");

		customColumns = CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(customColumns);

		String value = (String) customColumns.getColumnValue("TestColumn");
		ValidationUtils.assertEquals(value, "val", "Custom JSON value for test column was not as expected");
	}
}
