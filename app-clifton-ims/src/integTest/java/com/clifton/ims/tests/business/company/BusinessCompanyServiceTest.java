package com.clifton.ims.tests.business.company;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;


public class BusinessCompanyServiceTest extends BaseImsIntegrationTest {

	private static final short TYPE_BROKER = 3;

	@Resource
	private BusinessCompanyService businessCompanyService;

	public static final String BUSINESS_COMPANY_HIERARCHY = "business-company-hierarchy";

	private static final String ExpectedTree1 = "#" +
			"+- 1_Root#" +
			"...+- 1_Child_1#" +
			"...|..+- 1_GrandChild_1#" +
			"...|.....+- 1_GreatGrandChild_1#" +
			"...+- 2_Child_1#" +
			"......+- 2_GrandChild_1#" +
			".........+- 2_GreatGrandChild_1#" +
			"............+- 2_GreatGreatGrandChild_1#" +
			"#";
	private static final String ExpectedTree2 = "#" +
			"+- 2_Root#" +
			"...+- 3_Child_1#" +
			"...|..+- 3_GrandChild_1#" +
			"...|.....+- 3_GreatGrandChild_1#" +
			"...+- 4_Child_1#" +
			"......+- 4_GrandChild_1#" +
			".........+- 4_GreatGrandChild_1#" +
			"............+- 4_GreatGreatGrandChild_1#" +
			"#";

	private List<Node<BusinessCompany>> roots;


	@BeforeEach
	public void before() throws IOException {
		this.roots = buildCompanyTrees();
		Assertions.assertFalse(this.roots.isEmpty());
		Assertions.assertTrue(this.roots.size() == 2);
		MatcherAssert.assertThat(getTreeString(this.roots.get(0)), IsEqual.equalTo(ExpectedTree1));
		MatcherAssert.assertThat(getTreeString(this.roots.get(1)), IsEqual.equalTo(ExpectedTree2));
	}


	@AfterEach
	public void teardown() {
		for (Node<BusinessCompany> root : this.roots) {
			List<Node<BusinessCompany>> visited = root.depthFirstTraverse();
			Collections.reverse(visited);
			for (Node<BusinessCompany> node : visited) {
				deleteBusinessCompany(node.getValue());
			}
		}
	}


	@Test
	public void testNamedQuery() {
		BusinessCompany testBusinessCompany = findBusinessCompanyInTree("2_GreatGreatGrandChild_1");
		Assertions.assertNotNull(testBusinessCompany);
		Assertions.assertNotNull(testBusinessCompany.getId());
		Assertions.assertTrue(testBusinessCompany.getId() > 0);

		List<?> companies = this.businessCompanyService.getBusinessCompanyRelatedCompanyList(testBusinessCompany.getId());

		List<BusinessCompany> hierarchy = companies.stream().map(o -> (BusinessCompany) o).collect(Collectors.toList());
		Assertions.assertNotNull(hierarchy);
		Assertions.assertTrue(hierarchy.contains(this.roots.get(0).getValue()));
		List<BusinessCompany> nodes = this.roots.get(0).depthFirstTraverse().stream().map(Node::getValue).collect(Collectors.toList());
		Assertions.assertTrue(nodes.size() == hierarchy.size());
		hierarchy.removeAll(nodes);
		Assertions.assertTrue(hierarchy.isEmpty());
	}


	public String getTreeString(Node<BusinessCompany> node) throws IOException {
		try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(); PrintStream printStream = new PrintStream(byteArrayOutputStream)) {
			node.outputTree(n -> n.getValue().getName(), Node::getChildren, printStream);
			return new String(byteArrayOutputStream.toByteArray(), StandardCharsets.US_ASCII).replaceAll(System.lineSeparator(), "#");
		}
	}


	/**
	 * +- 1_Root
	 * .  +- 1_Child_1
	 * .  |  +- 1_GrandChild_1
	 * .  |     +- 1_GreatGrandChild_1
	 * .  +- 2_Child_1
	 * .     +- 2_GrandChild_1
	 * .        +- 2_GreatGrandChild_1
	 * .           +- 2_GreatGreatGrandChild_1
	 * +- 2_Root
	 * .  +- 3_Child_1
	 * .  |  +- 3_GrandChild_1
	 * .  |     +- 3_GreatGrandChild_1
	 * .  +- 4_Child_1
	 * .     +- 4_GrandChild_1
	 * .        +- 4_GreatGrandChild_1
	 * .           +- 4_GreatGreatGrandChild_1
	 */
	public List<Node<BusinessCompany>> buildCompanyTrees() throws IOException {
		List<Node<BusinessCompany>> roots = new ArrayList<>();

		final Node<BusinessCompany> root1 = new Node<>(createBusinessCompany("1_Root", null));
		root1.addChild(() -> {
			final Node<BusinessCompany> child = new Node<>(createBusinessCompany("1_Child_1", root1.getValue()));
			child.addChild(() -> {
				final Node<BusinessCompany> grandChild = new Node<>(createBusinessCompany("1_GrandChild_1", child.getValue()));
				grandChild.addChild(() -> new Node<>(createBusinessCompany("1_GreatGrandChild_1", grandChild.getValue()))
				);
				return grandChild;
			});
			return child;
		});
		root1.addChild(() -> {
			final Node<BusinessCompany> child = new Node<>(createBusinessCompany("2_Child_1", root1.getValue()));
			child.addChild(() -> {
				final Node<BusinessCompany> grandChild = new Node<>(createBusinessCompany("2_GrandChild_1", child.getValue()));
				grandChild.addChild(() -> {
							final Node<BusinessCompany> greatGrandChild = new Node<>(createBusinessCompany("2_GreatGrandChild_1", grandChild.getValue()));
							greatGrandChild.addChild(() -> new Node<>(createBusinessCompany("2_GreatGreatGrandChild_1", greatGrandChild.getValue())));
							return greatGrandChild;
						}
				);
				return grandChild;
			});
			return child;
		});
		roots.add(root1);

		final Node<BusinessCompany> root2 = new Node<>(createBusinessCompany("2_Root", null));
		root2.addChild(() -> {
			final Node<BusinessCompany> child = new Node<>(createBusinessCompany("3_Child_1", root2.getValue()));
			child.addChild(() -> {
				final Node<BusinessCompany> grandChild = new Node<>(createBusinessCompany("3_GrandChild_1", child.getValue()));
				grandChild.addChild(() -> new Node<>(createBusinessCompany("3_GreatGrandChild_1", grandChild.getValue())));
				return grandChild;
			});
			return child;
		});
		root2.addChild(() -> {
			final Node<BusinessCompany> child = new Node<>(createBusinessCompany("4_Child_1", root2.getValue()));
			child.addChild(() -> {
				final Node<BusinessCompany> grandChild = new Node<>(createBusinessCompany("4_GrandChild_1", child.getValue()));
				grandChild.addChild(() -> {
							final Node<BusinessCompany> greatGrandChild = new Node<>(createBusinessCompany("4_GreatGrandChild_1", grandChild.getValue()));
							greatGrandChild.addChild(() -> new Node<>(createBusinessCompany("4_GreatGreatGrandChild_1", greatGrandChild.getValue())));
							return greatGrandChild;
						}
				);
				return grandChild;
			});
			return child;
		});
		roots.add(root2);

		return roots;
	}


	private static class Node<V> {

		private V value;
		private List<Node<V>> children;


		Node(V value) {
			this.value = value;
			this.children = new ArrayList<>();
		}


		public V getValue() {
			return this.value;
		}


		public void setValue(V value) {
			this.value = value;
		}


		public List<Node<V>> getChildren() {
			return Collections.unmodifiableList(this.children);
		}


		public void addChild(Supplier<Node<V>> childProducer) {
			this.children.add(childProducer.get());
		}


		public List<Node<V>> depthFirstTraverse() {
			List<Node<V>> visited = new ArrayList<>();
			Stack<Node<V>> nodeStack = new Stack<>();

			nodeStack.add(this);

			while (!nodeStack.isEmpty()) {
				Node<V> currentNode = nodeStack.pop();
				visited.add(currentNode);
				nodeStack.addAll(currentNode.getChildren());
			}

			return visited;
		}


		public void outputTree(Function<Node<V>, String> nodeLabelProducer, Function<Node<V>, List<Node<V>>> nodeChildrenProducer, PrintStream outputStream) {
			outputStream.print(System.lineSeparator());
			Node<V> root = this;
			List<Node<V>> firstStack = new ArrayList<>();
			firstStack.add(root);

			List<List<Node<V>>> childListStack = new ArrayList<>();
			childListStack.add(firstStack);

			while (!childListStack.isEmpty()) {
				List<Node<V>> childStack = childListStack.get(childListStack.size() - 1);

				if (childStack.isEmpty()) {
					childListStack.remove(childListStack.size() - 1);
				}
				else {
					root = childStack.get(0);
					childStack.remove(0);

					String indent = "";
					for (int i = 0; i < childListStack.size() - 1; i++) {
						indent += (!childListStack.get(i).isEmpty()) ? "|.." : "...";
					}

					outputStream.println(indent + "+- " + nodeLabelProducer.apply(root));

					if (!nodeChildrenProducer.apply(root).isEmpty()) {
						childListStack.add(new ArrayList<>(nodeChildrenProducer.apply(root)));
					}
				}
			}
			outputStream.print(System.lineSeparator());
		}
	}


	public BusinessCompany createBusinessCompany(String name, BusinessCompany parent) {
		BusinessCompany businessCompany = new BusinessCompany();

		businessCompany.setType(this.businessCompanyService.getBusinessCompanyType(TYPE_BROKER));
		businessCompany.setName(name);
		businessCompany.setAlias("TEST - " + name);
		businessCompany.setCity("Minneapolis");
		businessCompany.setState("MN");
		businessCompany.setParent(parent);

		this.businessCompanyService.saveBusinessCompany(businessCompany);
		return businessCompany;
	}


	public void deleteBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompanyService.deleteBusinessCompany(businessCompany.getId());
	}


	public BusinessCompany findBusinessCompanyInTree(String name) {
		return this.roots.stream().flatMap(r -> r.depthFirstTraverse().stream()).map(Node::getValue).filter(c -> c.getName().equals(name)).findFirst().orElseThrow(IndexOutOfBoundsException::new);
	}
}
