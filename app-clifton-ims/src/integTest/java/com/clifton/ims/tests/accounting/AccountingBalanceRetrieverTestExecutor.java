package com.clifton.ims.tests.accounting;

import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.position.AccountingPositionBalance;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * <code>AccountingBalanceRetrieverTestExecutor</code> is a helping utility for obtaining and validating position balance lists of {@link AccountingBalance}s and/or {@link AccountingPositionBalance}s.
 *
 * @author nickk
 */
public class AccountingBalanceRetrieverTestExecutor {

	private final AccountingUtils accountingUtils;
	private final Date transactionDate;
	private final Map<InvestmentSecurity, Integer> expectedSecurityToBalanceSizeMap = new HashMap<>();
	private boolean excludeAccountingBalance = false;
	private boolean excludeAccountingPositionBalance = false;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingBalanceRetrieverTestExecutor(AccountingUtils accountingUtils, Date transactionDate) {
		this.accountingUtils = accountingUtils;
		this.transactionDate = transactionDate;
	}


	/**
	 * Create a new instance for obtaining {@link AccountingBalance}s and {@link AccountingPositionBalance}s on transaction date.
	 * Either balance can be excluded using {@link #excludeAccountingBalance(boolean)} or {@link #excludeAccountingPositionBalance(boolean)};
	 */
	public static AccountingBalanceRetrieverTestExecutor newExecutorForTransactionDate(AccountingUtils accountingUtils, Date transactionDate) {
		return new AccountingBalanceRetrieverTestExecutor(accountingUtils, transactionDate);
	}


	/**
	 * Create a new instance for obtaining only {@link AccountingBalance}s on transaction date.
	 */
	public static AccountingBalanceRetrieverTestExecutor newExecutorForAccountingBalancesOn(AccountingUtils accountingUtils, Date transactionDate) {
		return new AccountingBalanceRetrieverTestExecutor(accountingUtils, transactionDate).excludeAccountingPositionBalance(true);
	}


	/**
	 * Create a new instance for obtaining only {@link AccountingPositionBalance}s on transaction date.
	 */
	public static AccountingBalanceRetrieverTestExecutor newExecutorForAccountingPositionBalancesOn(AccountingUtils accountingUtils, Date transactionDate) {
		return new AccountingBalanceRetrieverTestExecutor(accountingUtils, transactionDate).excludeAccountingBalance(true);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AccountingBalanceRetrieverTestExecutor excludeAccountingBalance(boolean excludeAccountingBalance) {
		this.excludeAccountingBalance = excludeAccountingBalance;
		return this;
	}


	public AccountingBalanceRetrieverTestExecutor excludeAccountingPositionBalance(boolean excludeAccountingPositionBalance) {
		this.excludeAccountingPositionBalance = excludeAccountingPositionBalance;
		return this;
	}


	/**
	 * Adds the security to the balance retrieval command. The expected count will be used for validation on results.
	 */
	public AccountingBalanceRetrieverTestExecutor withSecurity(InvestmentSecurity security, int expectedBalanceSize) {
		if (security != null) {
			this.expectedSecurityToBalanceSizeMap.put(security, expectedBalanceSize);
		}
		return this;
	}


	/**
	 * Executes the commands to obtain the balance list(s). The resulting balances will be validated by expected security counts and a {@link AccountingBalancesList} is returned.
	 */
	public AccountingBalancesList execute() {
		AccountingBalancesList accountingBalancesList = getAccountingBalanceList();

		Map<InvestmentSecurity, List<AccountingBalance>> securityToBalanceListMap = this.excludeAccountingBalance ? Collections.emptyMap()
				: BeanUtils.getBeansMap(accountingBalancesList.getAccountingBalanceList(), AccountingBalance::getInvestmentSecurity);
		Map<InvestmentSecurity, List<AccountingPositionBalance>> securityToPositionBalanceListMap = this.excludeAccountingPositionBalance ? Collections.emptyMap()
				: BeanUtils.getBeansMap(accountingBalancesList.getAccountingPositionBalanceList(), AccountingPositionBalance::getInvestmentSecurity);

		this.expectedSecurityToBalanceSizeMap.forEach((security, expectedBalanceSize) -> {
			List<AccountingBalance> accountingBalanceList = securityToBalanceListMap.get(security);
			List<AccountingPositionBalance> accountingPositionBalanceList = securityToPositionBalanceListMap.get(security);

			if (!this.excludeAccountingBalance) {
				Assertions.assertEquals(expectedBalanceSize, accountingBalanceList.size(), String.format("Did not find %d balances for security '%s'", expectedBalanceSize, security.getSymbol()));
			}
			if (!this.excludeAccountingPositionBalance) {
				Assertions.assertEquals(expectedBalanceSize, accountingPositionBalanceList.size(), String.format("Did not find %d position balances for security '%s'", expectedBalanceSize, security.getSymbol()));
			}

			if (!this.excludeAccountingPositionBalance && !this.excludeAccountingBalance) {
				Map<InvestmentAccount, List<AccountingBalance>> accountAccountingBalanceMap = BeanUtils.getBeansMap(accountingBalanceList, AccountingBalance::getClientInvestmentAccount);
				Map<InvestmentAccount, List<AccountingPositionBalance>> accountAccountingPositionBalanceMap = BeanUtils.getBeansMap(accountingPositionBalanceList, AccountingPositionBalance::getClientInvestmentAccount);
				accountAccountingBalanceMap.forEach((clientAccount, clientAccountingBalanceList) -> {
					List<AccountingPositionBalance> clientPositionBalanceList = accountAccountingPositionBalanceMap.get(clientAccount);
					Assertions.assertNotNull(clientPositionBalanceList, String.format("Missing Position Balances for security '%s' and client account '%s'", security.getSymbol(), clientAccount));
					Assertions.assertEquals(clientAccountingBalanceList.size(), clientPositionBalanceList.size(), String.format("Client Account balance list mismatch for security '%s' and client account '%s'", security.getSymbol(), clientAccount));

					clientAccountingBalanceList.sort((balance1, balance2) -> CompareUtils.compare(balance1.getHoldingInvestmentAccount(), balance2.getHoldingInvestmentAccount()));
					clientPositionBalanceList.sort((balance1, balance2) -> CompareUtils.compare(balance1.getHoldingInvestmentAccount(), balance2.getHoldingInvestmentAccount()));

					Iterator<AccountingPositionBalance> positionBalanceIterator = clientPositionBalanceList.iterator();
					for (AccountingBalance accountingBalance : clientAccountingBalanceList) {
						AccountingPositionBalance positionBalance = positionBalanceIterator.next();
						Assertions.assertNotNull(positionBalance, String.format("Missing Position Balance for security '%s' and client account '%s'", security.getSymbol(), clientAccount));
						Assertions.assertEquals(accountingBalance.getHoldingInvestmentAccount(), positionBalance.getHoldingInvestmentAccount(), String.format("Holding Account mismatch between balances for security '%s' and client account '%s'", security.getSymbol(), clientAccount));
						Assertions.assertEquals(accountingBalance.getQuantity(), positionBalance.getRemainingQuantity(), String.format("Quantity mismatch between balances for security '%s' and client account '%s'", security.getSymbol(), clientAccount));
						Assertions.assertEquals(accountingBalance.getLocalCostBasis(), positionBalance.getRemainingCostBasis(), String.format("Cost Basis mismatch between balances for security '%s' and client account '%s'", security.getSymbol(), clientAccount));
						Assertions.assertEquals(accountingBalance.getBaseAmount(), positionBalance.getRemainingBaseDebitCredit(), String.format("Base Amount mismatch between balances for security '%s' and client account '%s'", security.getSymbol(), clientAccount));
					}
				});
			}
		});

		return accountingBalancesList;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountingBalancesList getAccountingBalanceList() {
		Integer[] securityIds = this.expectedSecurityToBalanceSizeMap.keySet().stream()
				.map(InvestmentSecurity::getId)
				.toArray(Integer[]::new);

		List<AccountingBalance> accountingBalanceList;
		if (this.excludeAccountingBalance) {
			accountingBalanceList = Collections.emptyList();
		}
		else {
			AccountingBalanceCommand balanceCommand = AccountingBalanceCommand.forTransactionDate(this.transactionDate);
			balanceCommand.setInvestmentSecurityIds(securityIds);
			balanceCommand.setPositionAccountingAccount(Boolean.TRUE);
			accountingBalanceList = this.accountingUtils.getAccountingBalanceList(balanceCommand);
		}
		List<AccountingPositionBalance> accountingPositionBalanceList;
		if (this.excludeAccountingPositionBalance) {
			accountingPositionBalanceList = Collections.emptyList();
		}
		else {
			AccountingPositionCommand positionCommand = AccountingPositionCommand.onPositionTransactionDate(this.transactionDate);
			positionCommand.setInvestmentSecurityIds(securityIds);
			accountingPositionBalanceList = this.accountingUtils.getAccountingPositionBalanceList(positionCommand);
		}
		return new AccountingBalancesList(accountingBalanceList, accountingPositionBalanceList);
	}


	public static final class AccountingBalancesList {

		private final List<AccountingBalance> accountingBalanceList;
		private final List<AccountingPositionBalance> accountingPositionBalanceList;


		private AccountingBalancesList(List<AccountingBalance> accountingBalanceList, List<AccountingPositionBalance> accountingPositionBalanceList) {
			this.accountingBalanceList = accountingBalanceList;
			this.accountingPositionBalanceList = accountingPositionBalanceList;
		}


		public List<AccountingBalance> getAccountingBalanceList() {
			return this.accountingBalanceList;
		}


		public List<AccountingPositionBalance> getAccountingPositionBalanceList() {
			return this.accountingPositionBalanceList;
		}
	}
}
