package com.clifton.ims.tests.investment;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.BusinessContractTypes;
import com.clifton.business.service.BusinessServiceBusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.builders.investment.account.InvestmentAccountBuilder;
import com.clifton.ims.tests.builders.investment.account.relationship.InvestmentAccountRelationshipBuilder;
import com.clifton.ims.tests.builders.investment.assetclass.rule.definition.RuleAssignmentBuilder;
import com.clifton.ims.tests.builders.investment.business.contract.BusinessContractBuilder;
import com.clifton.ims.tests.builders.investment.business.contract.BusinessContractPartyBuilder;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.business.BusinessUtils;
import com.clifton.ims.tests.trade.TradeUtils;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.test.util.RandomUtils;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/**
 * The <code>AccountUtils</code> provides convenience methods for creating client/holding accounts, activating accounts, etc.
 *
 * @author jgommels
 * @author stevenf
 */
@Component
public class InvestmentAccountUtils {

	private static final Map<String, Lock> RULE_DEFINITION_LOCK_MAP = new ConcurrentHashMap<>();

	//Injected in constructor
	private final InvestmentAccountService investmentAccountService;
	private final InvestmentSecurity usd;

	@Resource
	private BusinessContractService businessContractService;

	@Resource
	private BusinessUtils businessUtils;

	@Resource
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	public RuleDefinitionService ruleDefinitionService;

	@Resource
	private TradeUtils tradeUtils;

	@Resource
	private WorkflowTransitioner workflowTransitioner;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	public static final String RULE_DEFINITION_CONTRACT_APPROVED = "Trading Rules: Client Approved Contracts";
	public static final String RULE_DEFINITION_SHORT_LONG = "Short/Long Change";
	public static final String RULE_DEFINITION_COMPLIANCE = "Compliance Real Time Rules";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Inject
	public InvestmentAccountUtils(InvestmentAccountService investmentAccountService, InvestmentInstrumentService investmentInstrumentService) {
		this.investmentAccountService = investmentAccountService;
		this.usd = investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccountByNumber(String number) {
		return getAccountByNumber(number, true);
	}


	public InvestmentAccount getHoldingAccountByNumber(String number) {
		return getAccountByNumber(number, false);
	}


	private InvestmentAccount getAccountByNumber(String number, boolean ourAccount) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(ourAccount);
		searchForm.setNumberEquals(number);
		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm);
		int count = CollectionUtils.getSize(accountList);
		if (count == 0) {
			throw new IllegalStateException("Cannot find " + (ourAccount ? "client" : "holding") + " account with number = '" + number + "'");
		}
		if (count > 1) {
			throw new IllegalStateException("Cannot have more than one " + (ourAccount ? "client" : "holding") + " accounts with number = '" + number + "': " + accountList);
		}
		return accountList.get(0);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addInvestmentAccountToAccountGroupByName(InvestmentAccount investmentAccount, String accountGroupName) {
		InvestmentAccountGroup accountGroup = this.investmentAccountGroupService.getInvestmentAccountGroupByName(accountGroupName);
		ValidationUtils.assertNotNull(accountGroup, "Cannot find account group by name [" + accountGroupName + "]");
		this.investmentAccountGroupService.linkInvestmentAccountGroupToAccount(accountGroup.getId(), investmentAccount.getId());
	}


	/**
	 * Calls createAccountsForTrading to create the custodian and trading accounts, then it creates a REPO account and it relationships.
	 */
	public AccountInfo createAccountsForRepo(BusinessCompanies custodianHoldingCompany, BusinessCompanies repoCompany, String... excludeDefinitionNames) {
		TradeType bondTradeType = this.tradeUtils.getTradeType(TradeType.BONDS);

		AccountInfo accountInfo = createAccountsForTrading(AccountsCreationCommand.ofCustodianAndREPO(custodianHoldingCompany, custodianHoldingCompany, repoCompany, bondTradeType, excludeDefinitionNames));

		InvestmentAccount repoHoldingAccount = createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.REPO_ACCOUNT, accountInfo.getBusinessClient(), repoCompany, BusinessContractTypes.MASTER_REPURCHASE_AGREEMENT));
		accountInfo.setRepoHoldingAccount(repoHoldingAccount);
		createAccountRelationship(accountInfo.getClientAccount(), repoHoldingAccount, InvestmentAccountRelationshipPurposes.REPO);
		createAccountRelationship(accountInfo.getCustodianAccount(), repoHoldingAccount, InvestmentAccountRelationshipPurposes.REPO);

		return accountInfo;
	}


	public AccountInfo createAccountsForPortfolioRun(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, BusinessCompanies repoHoldingCompany, ServiceProcessingTypes processingType, TradeType tradeType, String... excludeDefinitionNames) {
		AccountInfo accountInfo = createAccountsForTrading(AccountsCreationCommand.ofCustodianAndREPO(holdingCompany, custodianHoldingCompany, repoHoldingCompany, tradeType, excludeDefinitionNames));
		if (processingType != null) {
			InvestmentAccount account = accountInfo.getClientAccount();

			BusinessServiceBusinessServiceProcessingType businessServiceBusinessServiceProcessingType = this.businessUtils.getBusinessServiceBusinessServiceProcessingTypeForProcessingType(ServiceProcessingTypes.PORTFOLIO_RUNS);
			account.setBusinessService(businessServiceBusinessServiceProcessingType.getBusinessService());
			account.setServiceProcessingType(businessServiceBusinessServiceProcessingType.getBusinessServiceProcessingType());
			accountInfo.setClientAccount(this.investmentAccountService.saveInvestmentAccount(account));
		}
		return accountInfo;
	}


	public AccountInfo createAccountsForOptions(BusinessCompanies marginHoldingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType) {
		return createAccountsForTrading(AccountsCreationCommand.ofCustodianAndMargin(marginHoldingCompany, custodianHoldingCompany, tradeType));
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, TradeType tradeType) {
		return createAccountsForTrading(holdingCompany, null, tradeType);
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		return createAccountsForTrading(AccountsCreationCommand.of(holdingCompany, tradeType, excludeDefinitionNames));
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType) {
		return createAccountsForTrading(AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType));
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, BusinessCompanies defaultExchangeRateSourceCompany, TradeType tradeType) {
		AccountsCreationCommand command = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType);
		command.setDefaultExchangeRateSourceCompany(defaultExchangeRateSourceCompany);
		return createAccountsForTrading(command);
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, boolean clsHoldingAccount) {
		AccountsCreationCommand command = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType);
		command.setClsHoldingAccount(clsHoldingAccount);
		return createAccountsForTrading(command);
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, BusinessCompanies defaultExchangeRateSourceCompany, TradeType tradeType, boolean clsHoldingAccount) {
		AccountsCreationCommand command = AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType);
		command.setClsHoldingAccount(clsHoldingAccount);
		command.setDefaultExchangeRateSourceCompany(defaultExchangeRateSourceCompany);
		return createAccountsForTrading(command);
	}


	public AccountInfo createAccountsForTrading(InvestmentAccount clientAccount, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType) {
		return createAccountsForTrading(AccountsCreationCommand.ofClientAccountAndCustodian(clientAccount, holdingCompany, custodianHoldingCompany, tradeType));
	}


	public AccountInfo createAccountsForTrading(InvestmentAccount clientAccount, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, boolean clsHoldingAccount) {
		AccountsCreationCommand command = AccountsCreationCommand.ofClientAccountAndCustodian(clientAccount, holdingCompany, custodianHoldingCompany, tradeType);
		command.setClsHoldingAccount(clsHoldingAccount);
		return createAccountsForTrading(command);
	}


	public AccountInfo createAccountsForTrading(InvestmentAccount clientAccount, BusinessCompanies holdingCompany, InvestmentAccount custodianAccount, TradeType tradeType) {
		return createAccountsForTrading(AccountsCreationCommand.ofClientAccountAndCustodianAccount(clientAccount, holdingCompany, custodianAccount, tradeType));
	}


	public AccountInfo createAccountsForTrading(InvestmentAccount clientAccount, BusinessCompanies holdingCompany, InvestmentAccount custodianAccount, TradeType tradeType, boolean clsHoldingAccount) {
		AccountsCreationCommand command = AccountsCreationCommand.ofClientAccountAndCustodianAccount(clientAccount, holdingCompany, custodianAccount, tradeType);
		command.setClsHoldingAccount(clsHoldingAccount);
		return createAccountsForTrading(command);
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, TradeType tradeType, String... excludeDefinitionNames) {
		return createAccountsForTrading(AccountsCreationCommand.ofCustodian(holdingCompany, custodianHoldingCompany, tradeType, excludeDefinitionNames));
	}


	public AccountInfo createAccountsForTrading(BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, BusinessCompanies executionCompany, TradeType tradeType, String... excludeDefinitionNames) {
		return createAccountsForTrading(AccountsCreationCommand.ofCustodianAndExecution(holdingCompany, custodianHoldingCompany, executionCompany, tradeType, excludeDefinitionNames));
	}


	/**
	 * Convenience method that creates:
	 * <ul>
	 * <li>BusinessClientRelationship</li>
	 * <li>BusinessClient</li>
	 * <li>InvestmentAccount(s) (Client Account and Holding Account) - Accounts Are Also All Activated (Clifton is used as the issuing company of the client account.)</li>
	 * <li>InvestmentAccountRelationship(s) necessary to trade selected InvestmentType</li>
	 * <li>Compliance Rule is also created to allow trading for the investment type</li>
	 * </ul>
	 * <p>
	 * NOTE: As new InvestmentTypes are implemented, custom logic may need to be added,
	 * <p>
	 * HoldingAccountType is selected based on TradeType to one that applies, if specific type is needed, may want to re-factor to allow overriding
	 */
	public AccountInfo createAccountsForTrading(AccountsCreationCommand command) {
		BusinessClient client;
		if (command.getOtcSecurity() != null) {
			client = this.businessUtils.getBusinessClientByCompany(command.getOtcSecurity().getBusinessContract().getCompany().getId());
		}
		else if (command.getClient() != null) {
			client = command.getClient();
		}
		else if (command.getClientAccount() != null) {
			client = command.getClientAccount().getBusinessClient();
		}
		else {
			client = this.businessUtils.createBusinessClient();
		}

		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setClientAccount(command.getClientAccount() != null ? command.getClientAccount() : createClientAccount(client));
		if (command.getClientAccount() == null) {
			excludeComplianceRulesToAllowTrading(accountInfo.getClientAccount(), command.getExcludeDefinitionNames());
		}

		if (command.getMarginHoldingCompany() != null) {
			accountInfo = createMarginHoldingAccount(accountInfo, client, command.getMarginHoldingCompany(), command.getTradeType());
		}
		else {
			accountInfo = createHoldingAccount(accountInfo, false, command);
		}

		if (command.getCustodianHoldingCompany() != null) {
			accountInfo = createCustodianAccount(accountInfo, client, command.getCustodianHoldingCompany());
		}
		if (command.getCustodianAccount() != null) {
			// assume the relationship has already be created
			accountInfo.setCustodianAccount(command.getCustodianAccount());
		}
		if (command.getRepoHoldingCompany() != null) {
			accountInfo = createRepoHoldingAccount(accountInfo, client, command.getRepoHoldingCompany());
		}
		if (command.getExecutingCompany() != null) {
			createExecutingBrokerAccount(accountInfo.getHoldingAccount(), client, command.getExecutingCompany(), command.getTradeType());
		}

		// TODO: why is this here? should only be called when needed for Cleared OTC tests
		if (command.getHoldingCompany() != null && accountInfo.getSwapsHoldingAccount() != null) {
			accountInfo.setSwapsHoldingAccount(createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.OTC_CLEARED, client, command.getHoldingCompany())));
			createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getSwapsHoldingAccount(), InvestmentAccountRelationshipPurposes.MARK_TO_MARKET);
		}

		return accountInfo;
	}


	public AccountInfo createHoldingAccount(AccountInfo accountInfo, BusinessCompanies holdingCompany, TradeType tradeType) {
		return createHoldingAccount(accountInfo, AccountsCreationCommand.of(holdingCompany, tradeType));
	}


	public AccountInfo createHoldingAccount(AccountInfo accountInfo, AccountsCreationCommand command) {
		return createHoldingAccount(accountInfo, true, command);
	}

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////


	private AccountInfo createHoldingAccount(AccountInfo accountInfo, boolean cloneAccountInfo, AccountsCreationCommand command) {
		AccountInfo clonedAccountInfo = cloneAccountInfo ? AccountInfo.copy(accountInfo) : accountInfo;

		String accountType;
		BusinessContractTypes contractType = null;
		boolean excludeContractFromAccount = false;
		List<InvestmentAccountRelationshipPurpose> relationshipPurposes = new ArrayList<>();
		relationshipPurposes.add(command.getTradeType().getHoldingAccountPurpose());

		switch (command.getTradeType().getName()) {
			case TradeType.FUTURES:
				accountType = InvestmentAccountType.HOLDING_FUTURES_BROKER;
				contractType = BusinessContractTypes.NFA_REGISTRATION;
				excludeContractFromAccount = true;
				break;
			case TradeType.BONDS:
			case TradeType.FUNDS:
				accountType = InvestmentAccountType.CUSTODIAN;
				break;
			//If investment type is options, also allow trading Futures and Index
			case TradeType.OPTIONS:
				// OTC Options must use OTC ISDA account
				accountType = (command.getOtcSecurity() != null) ? InvestmentAccountType.OTC_ISDA : InvestmentAccountType.FUTURES_BROKER;
				contractType = (command.getOtcSecurity() != null) ? BusinessContractTypes.ISDA : BusinessContractTypes.NFA_REGISTRATION;
				excludeContractFromAccount = command.getOtcSecurity() == null;
				// Note: Can have multiple account relationships for Options, but not Futures - Should account type be changed to Options Broker
				// If someone needs Futures, they should be explicit not just automatically added when Options relationships are added (can just add second relationship to existing accounts)
				break;
			case TradeType.CURRENCY:
			case TradeType.CLEARED_CREDIT_DEFAULT_SWAPS:
			case TradeType.CLEARED_INTEREST_RATE_SWAPS:
				accountType = InvestmentAccountType.OTC_CLEARED;
				contractType = BusinessContractTypes.ADDENDUM_CLEARED_DERIVATIVES;
				break;
			case TradeType.TOTAL_RETURN_SWAPS:
			case TradeType.CREDIT_DEFAULT_SWAPS:
			case TradeType.INTEREST_RATE_SWAPS:
			case TradeType.FORWARDS:
				accountType = command.isClsHoldingAccount() ? InvestmentAccountType.OTC_CLS : InvestmentAccountType.OTC_ISDA;
				contractType = BusinessContractTypes.ISDA;
				break;
			default:
				accountType = InvestmentAccountType.BROKER;
				break;
		}

		BusinessClient client = accountInfo.getClientAccount().getBusinessClient();
		AccountCreationCommand holdingCommand = (command.getOtcSecurity() == null) ? AccountCreationCommand.ofActive(accountType, client, command.getHoldingCompany(), contractType, excludeContractFromAccount) : AccountCreationCommand.ofActive(accountType, client, command.getOtcSecurity().getBusinessCompany(), command.getOtcSecurity().getBusinessContract(), excludeContractFromAccount);
		if (command.getDefaultExchangeRateSourceCompany() != null) {
			holdingCommand.setDefaultExchangeRateSourceCompany(this.businessUtils.getBusinessCompany(command.getDefaultExchangeRateSourceCompany()));
		}
		return createHoldingAccount(clonedAccountInfo, relationshipPurposes, holdingCommand);
	}


	private AccountInfo createHoldingAccount(AccountInfo accountInfo, List<InvestmentAccountRelationshipPurpose> holdingAccountRelationshipPurposes, AccountCreationCommand command) {
		// Create a holding account
		InvestmentAccount holdingAccount = createInvestmentAccount(command);
		accountInfo.setHoldingAccount(holdingAccount);

		// Create relationships between client account and holding account
		for (InvestmentAccountRelationshipPurpose relationshipPurpose : holdingAccountRelationshipPurposes) {
			createAccountRelationship(accountInfo.getClientAccount(), holdingAccount, relationshipPurpose);
		}
		return accountInfo;
	}


	public void excludeComplianceRulesToAllowTrading(InvestmentAccount clientAccount, String... excludeDefinitionNames) {
		disableRuleDefinitionForAccount(clientAccount, (excludeDefinitionNames == null || excludeDefinitionNames.length == 0 ? new String[]{RULE_DEFINITION_CONTRACT_APPROVED, RULE_DEFINITION_SHORT_LONG, RULE_DEFINITION_COMPLIANCE} : excludeDefinitionNames));
	}


	private void disableRuleDefinitionForAccount(InvestmentAccount clientAccount, String... ruleDefinitionNames) {
		if (ruleDefinitionNames == null) {
			return;
		}
		for (String ruleDefinitionName : ruleDefinitionNames) {
			Lock definitionLock = RULE_DEFINITION_LOCK_MAP.computeIfAbsent(ruleDefinitionName, definition -> new ReentrantLock());
			/*
			 * Use lock to ensure that a RuleDefinition is only be modified by one thread at a time. Using a
			 * lock per definition improves throughput when many threads are working in this method. Without
			 * locking this code will produce an intermittent failure trying to modify a global rule.
			 */
			definitionLock.lock();
			try {
				RuleDefinition ruleDefinition = this.ruleDefinitionService.getRuleDefinitionByName(ruleDefinitionName);
				boolean isSystemDefined = ruleDefinition.isSystemDefined();
				boolean isIgnorable = ruleDefinition.isIgnorable();
				if (isSystemDefined || !isIgnorable) {
					ruleDefinition.setSystemDefined(false);
					ruleDefinition.setIgnorable(true);
					ruleDefinition = this.ruleDefinitionService.saveRuleDefinition(ruleDefinition);
				}
				RuleAssignmentBuilder.ruleAssignment(this.ruleDefinitionService).withRuleDefinition(ruleDefinition).withAdditionalScopeFkFieldId(BeanUtils.getIdentityAsLong(clientAccount)).withExcluded(true).withNote("Integration Test Exclude Rule").buildAndSave();
				if (isSystemDefined || !isIgnorable) {
					ruleDefinition.setSystemDefined(isSystemDefined);
					ruleDefinition.setIgnorable(isIgnorable);
					this.ruleDefinitionService.saveRuleDefinition(ruleDefinition);
				}
			}
			finally {
				definitionLock.unlock();
			}
		}
	}


	public InvestmentAccount createClientAccount(BusinessClient client) {
		return createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.CLIENT, client, BusinessCompanies.CLIFTON));
	}


	public InvestmentAccount createInactiveClientAccount(BusinessClient client) {
		return createInvestmentAccount(AccountCreationCommand.ofInactive(InvestmentAccountType.CLIENT, client, BusinessCompanies.CLIFTON));
	}


	public AccountInfo createCustodianAccount(AccountInfo accountInfo, BusinessClient businessClient, BusinessCompanies holdingCompany) {
		accountInfo.setCustodianAccount(createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.CUSTODIAN, businessClient, holdingCompany)));
		createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getCustodianAccount(), InvestmentAccountRelationshipPurposes.CUSTODIAN);
		return accountInfo;
	}


	public AccountInfo createRepoHoldingAccount(AccountInfo accountInfo, BusinessClient businessClient, BusinessCompanies holdingCompany) {
		accountInfo.setRepoHoldingAccount(createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.REPO_ACCOUNT, businessClient, holdingCompany, BusinessContractTypes.MASTER_REPURCHASE_AGREEMENT)));
		createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getRepoHoldingAccount(), InvestmentAccountRelationshipPurposes.REPO);
		createAccountRelationship(accountInfo.getCustodianAccount(), accountInfo.getRepoHoldingAccount(), InvestmentAccountRelationshipPurposes.REPO);
		return accountInfo;
	}


	public AccountInfo createMarginHoldingAccount(AccountInfo accountInfo, BusinessClient businessClient, BusinessCompanies holdingCompany, TradeType tradeType) {
		accountInfo.setHoldingAccount(createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.MARGIN, businessClient, holdingCompany)));
		String purpose = TradeService.TRADING_PURPOSE_NAME_PREFIX + tradeType.getName();
		createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.findByName(purpose));
		return accountInfo;
	}


	public void createExecutingBrokerAccount(InvestmentAccount holdingAccount, BusinessClient client, BusinessCompanies executingCompanies, TradeType tradeType) {
		AccountCreationCommand executionCommand = AccountCreationCommand.ofActive(InvestmentAccountType.BROKER, client, executingCompanies);
		InvestmentAccount executionAccount = createInvestmentAccount(executionCommand);
		String purpose = TradeService.EXECUTING_PURPOSE_NAME_PREFIX + tradeType.getName();
		createAccountRelationship(holdingAccount, executionAccount, InvestmentAccountRelationshipPurposes.findByName(purpose));
	}


	public InvestmentAccount createInvestmentAccount(AccountCreationCommand command) {
		// Create a BusinessContract if needed
		BusinessContract contract = command.getBusinessContract();
		if (contract == null && command.getBusinessContractType() != null) {
			contract = BusinessContractBuilder.businessContract(this.businessContractService)
					.withContractType(this.businessContractService.getBusinessContractTypeByName(command.getBusinessContractType().getName()))
					.withCompany(command.getClient().getCompany())
					.buildAndSave();
		}

		InvestmentAccountType accountType = this.investmentAccountService.getInvestmentAccountTypeByName(command.getAccountType());
		String accountTypeAbbreviation = accountType.isOurAccount() ? "CA" : "HA";
		InvestmentAccount account = InvestmentAccountBuilder.investmentAccount(this.investmentAccountService)
				.withBaseCurrency(this.usd)
				.withIssuingCompany(command.getIssuingCompany() != null ? command.getIssuingCompany() : this.businessUtils.getBusinessCompany(command.getIssuer()))
				.withName(RandomUtils.randomNameAndNumber())
				.withUsedForSpeculation(true)
				.withNumber(accountTypeAbbreviation + RandomUtils.randomNumber())
				.withType(accountType)
				.withBusinessClient(command.getClient())
				.withBusinessContract(command.isExcludeBusinessContract() ? null : contract)
				.withDefaultExchangeRateSourceCompany(command.getDefaultExchangeRateSourceCompany())
				.build();

		if (account.getDefaultExchangeRateSourceCompany() == null && account.getType().isDefaultExchangeRateSourceCompanySameAsIssuer()) {
			account.setDefaultExchangeRateSourceCompany(account.getIssuingCompany());
		}

		//Create BusinessContractParty if BusinessContract is needed
		if (contract != null && command.getBusinessContract() == null && account.getType().getIssuerContractPartyRole() != null) {
			BusinessContractPartyBuilder.businessContractParty(this.businessContractService)
					.withRole(account.getType().getIssuerContractPartyRole())
					.withCompany(account.getIssuingCompany())
					.withContract(contract)
					.buildAndSave();
		}

		account = this.investmentAccountService.saveInvestmentAccount(account);
		if (command.isActivate()) {
			//Activate Account
			activateAccount(account);
			// refresh account for workflow transition(s)
			account = this.investmentAccountService.getInvestmentAccount(account.getId());
		}
		return account;
	}


	public InvestmentAccountRelationship createAccountRelationship(InvestmentAccount account1, InvestmentAccount account2, InvestmentAccountRelationshipPurposes purposeName) {
		return createAccountRelationship(account1, account2, this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(purposeName.getName()));
	}


	public InvestmentAccountRelationship createAccountRelationship(InvestmentAccount account1, InvestmentAccount account2, InvestmentAccountRelationshipPurpose purpose) {
		return InvestmentAccountRelationshipBuilder.investmentAccountRelationship(this.investmentAccountRelationshipService)
				.withReferenceOne(account1)
				.withReferenceTwo(account2)
				.withPurpose(this.investmentAccountRelationshipService.getInvestmentAccountRelationshipPurposeByName(purpose.getName()))
				.withStartDate(DateUtils.toDate("1/1/2010"))
				.withEndDate(DateUtils.toDate("1/1/2100"))
				.buildAndSave();
	}


	/**
	 * Transitions the account to an active state.
	 */
	public void activateAccount(InvestmentAccount account) {
		String tableName = "InvestmentAccount";
		// Use special transition for tests to prevent having to evaluate specific conditions that we use, like same person can't submit for approval and activate, and short/long compliance rule is required
		if (account.getType().isOurAccount()) {
			this.workflowTransitioner.transitionForTests("Account Activation for Integration Tests", "Active", tableName, BeanUtils.getIdentityAsLong(account));
		}
		else {
			this.workflowTransitioner.transitionIfPresent("Submit for Approval", tableName, BeanUtils.getIdentityAsLong(account));
			this.workflowTransitioner.transitionIfPresent("Activate Account", tableName, BeanUtils.getIdentityAsLong(account));
		}
	}


	public InvestmentAccount reactivateClientAccount(String accountNumber, Date effectiveStart) {
		return reactivateAccount(getClientAccountByNumber(accountNumber), effectiveStart);
	}


	public InvestmentAccount reactivateHoldingAccount(String accountNumber, Date effectiveStart) {
		return reactivateAccount(getHoldingAccountByNumber(accountNumber), effectiveStart);
	}


	public InvestmentAccount reactivateAccount(InvestmentAccount account, Date effectiveStart) {
		if (WorkflowStatus.STATUS_ACTIVE.equals(account.getWorkflowStatus().getName())) {
			return account;
		}
		String workflowName = account.getType().isOurAccount() ? "Investment Account Status - Clifton Accounts" : "Investment Account Status - Non-Clifton Accounts";
		String stateName = account.getType().isOurAccount() ? "Terminated - Historical Correction" : WorkflowStatus.STATUS_ACTIVE;
		WorkflowState state = this.workflowDefinitionService.getWorkflowStateByName(this.workflowDefinitionService.getWorkflowByName(workflowName).getId(), stateName);
		account.setWorkflowState(state);
		account.setWorkflowStateEffectiveStartDate(effectiveStart);
		return RequestOverrideHandler.serviceCallWithOverrides(() -> this.investmentAccountService.saveInvestmentAccount(account),
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, true, "workflowStateEffectiveStartDate"));
	}
}
