package com.clifton.ims.tests.workflow.task;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.workflow.task.WorkflowTaskService;
import com.clifton.workflow.task.search.WorkflowTaskSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * @author manderson
 */
public class WorkflowTaskServiceTests extends BaseImsIntegrationTest {


	@Resource
	private WorkflowTaskService workflowTaskService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFindByJsonCustomColumn() {
		WorkflowTaskSearchForm searchForm = new WorkflowTaskSearchForm();
		searchForm.addSearchRestriction("customColumns.ZenDeskNumber", ComparisonConditions.EQUALS, "174686");

		Assertions.assertEquals(3, CollectionUtils.getSize(this.workflowTaskService.getWorkflowTaskList(searchForm)));

		searchForm = new WorkflowTaskSearchForm();
		searchForm.addSearchRestriction("customColumns.ZenDeskNumber", ComparisonConditions.LIKE, "174686");

		Assertions.assertEquals(3, CollectionUtils.getSize(this.workflowTaskService.getWorkflowTaskList(searchForm)));
	}
}
