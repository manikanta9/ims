package com.clifton.ims.tests.core.web.bind;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.client.setup.BusinessClientRelationshipCategory;
import com.clifton.business.client.setup.BusinessClientSetupService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.AssertUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListService;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.protocol.ImsProtocolClient;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.HashMap;


/**
 * @author stevenf on 11/15/2016.
 */
public class ValidateBeanTests extends BaseImsIntegrationTest {

	@Resource
	private BusinessClientService businessClientService;
	@Resource
	private BusinessClientSetupService businessClientSetupService;
	@Resource
	private BusinessCompanyService businessCompanyService;
	@Resource
	private InvestmentManagerAccountService managerAccountService;
	@Resource
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;
	@Resource
	private SystemListService systemListService;

	@Resource
	private ImsProtocolClient imsProtocolClient;


	//Ensures the 'validateBean' method called from 'doBind' in the ValidationServletRequestDatabinder class
	//does not throw a null pointer exception for various column configurations
	//BusinessClientRelationship was previously a known failure.
	@Test
	public void testValidatingServletRequestDataBinder() {
		BusinessClient client = new BusinessClient();
		BusinessClientRelationship relationship = new BusinessClientRelationship();
		InvestmentManagerAccount managerAccount = new InvestmentManagerAccount();
		try {
			BusinessCompany company = this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "Morgan Stanley & Co. Inc.");
			BusinessClientCategory category = this.businessClientSetupService.getBusinessClientCategoryByName(BusinessUtils.CLIENT_CATEGORY_CLIENT_GROUP);
			BusinessClientRelationshipCategory relationshipCategory = this.businessClientSetupService.getBusinessClientRelationshipCategoryByName(BusinessUtils.CLIENT_RELATIONSHIP_CATEGORY_INSTITUTIONAL);

			client.setName("TestClient");
			client.setCategory(category);
			this.businessClientService.saveBusinessClient(client);

			relationship.setName("TestData");
			relationship.setRelationshipCategory(relationshipCategory);
			SystemList systemList = relationship.getRelationshipCategory().getRelationshipTypeSystemList();
			relationship.setRelationshipType(systemListService.getSystemListItemByListAndValue(systemList.getName(), "Corporation"));
			this.businessClientService.saveBusinessClientRelationship(relationship);

			managerAccount.setAccountName("TestAccount");
			managerAccount.setClient(client);
			managerAccount.setManagerCompany(company);
			managerAccount.setCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.CASH);
			managerAccount.setBaseCurrency(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14"));
			this.managerAccountService.saveInvestmentManagerAccount(managerAccount);
		}
		finally {
			if (managerAccount.getId() != null) {
				this.managerAccountService.deleteInvestmentManagerAccount(managerAccount.getId());
			}

			if (relationship.getId() != null) {
				this.businessClientService.deleteBusinessClientRelationship(relationship.getId());
			}

			if (client.getId() != null) {
				this.businessClientService.deleteBusinessClient(client.getId());
			}
		}
	}


	/**
	 * Ensures the disableValidatingBindingValidation request parameter is honored by the {@link com.clifton.core.web.bind.WebBindingHandler}
	 */
	@Test
	public void testDisableValidatingBindingValidation() {
		try {
			HashMap<String, String> params = new HashMap<>();
			params.put("disableValidatingBindingValidation", "true");
			this.imsProtocolClient.request("/investmentAccountSecurityTargetFreezeSave.json", params, Void.TYPE, "", false);
		}
		catch (Exception e) {
			// The error message below is from the InvestmentAccountSecurityTargetFreezeValidator, which should get invoked because disableValidatingBindingValidation=true is sent with the request.
			// If the disableValidatingBindingValidation flag is not handled correctly by IMS, then a data binding error message of the form "The field _____ is required" will come back from IMS.
			AssertUtils.assertEquals("A security target freeze type is required.", ((ImsErrorResponseException) e).getErrorMessageFromIMS(), "Unexpected validation error when using disableValidatingBindingValidation request property: " + e);
		}
	}
}
