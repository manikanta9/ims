package com.clifton.ims.tests.portfolio.cashflow.ldi;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowType;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistoryService;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowTypeSearchForm;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * A suite of tests for verifying the proper functionality of the {@link PortfolioCashFlowService}.
 *
 * @author davidi
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PortfolioLdiCashFlowServiceTests extends BaseImsIntegrationTest {

	@Resource
	PortfolioCashFlowService portfolioCashFlowService;

	@Resource
	PortfolioCashFlowHistoryService portfolioCashFlowHistoryService;

	@Resource
	InvestmentAccountService investmentAccountService;

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	SystemListService<SystemList> systemListService;

	@Resource
	SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// Data entities for tests
	private PortfolioCashFlowGroup grp1;
	private PortfolioCashFlowGroup grp2;
	private PortfolioCashFlowGroup grp3;
	private PortfolioCashFlowGroup grp4;
	private PortfolioCashFlow flow1;
	private PortfolioCashFlow flow2;
	private PortfolioCashFlow flow3;
	private PortfolioCashFlow flow4;

	private List<Integer> groupIdList;
	private static final int QUERY_LIMIT = 10;

	private static final String PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE = "Projected Flow";
	private static final String ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE = "Actual Flow";
	private static final String SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE = "Service Cost";

	private SystemListItem groupTypeNonRolling;


	@BeforeAll
	public void initData() {
		this.groupTypeNonRolling = this.systemListService.getSystemListItemByListAndValue("Portfolio Cash Flow Group Type", "Non-Rolling");
		PortfolioCashFlowType cashFlowType = this.portfolioCashFlowService.getPortfolioCashFlowType((short) 1);
		cashFlowType.setCumulative(true);
		this.portfolioCashFlowService.savePortfolioCashFlowType(cashFlowType);

		this.grp1 = createAndSavePortfolioCashFlowGroup(6, 1927, "01/01/2016", "03/20/2016", "03/21/2016", this.groupTypeNonRolling);
		this.grp2 = createAndSavePortfolioCashFlowGroup(1, 425, "01/01/2016", "03/23/2016", "03/24/2016", this.groupTypeNonRolling);
		this.grp3 = createAndSavePortfolioCashFlowGroup(73, 948, "01/01/2016", "03/24/2016", "03/25/2016", this.groupTypeNonRolling);
		this.grp4 = createAndSavePortfolioCashFlowGroup(13697, 1927, "01/01/2016", "03/26/2016", "03/27/2018", this.groupTypeNonRolling);
		this.groupIdList = new ArrayList<>();
		this.groupIdList.add(this.grp1.getId());
		this.groupIdList.add(this.grp2.getId());
		this.groupIdList.add(this.grp3.getId());
		this.groupIdList.add(this.grp4.getId());

		this.flow1 = createPortfolioCashFlow(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, this.grp1, BigDecimal.valueOf(500.00), 2016, true);
		this.flow2 = createPortfolioCashFlow(ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE, this.grp2, BigDecimal.valueOf(1000.00), 2017, true);
		this.flow3 = createPortfolioCashFlow(ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE, this.grp3, BigDecimal.valueOf(2000.00), 2018, true);
		this.flow4 = createPortfolioCashFlow(SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE, this.grp4, BigDecimal.valueOf(100.00), 2019, true);
	}


	@AfterAll
	public void cleanUp() {
		PortfolioCashFlowType cashFlowType = this.portfolioCashFlowService.getPortfolioCashFlowType((short) 1);
		cashFlowType.setCumulative(false);
		this.portfolioCashFlowService.savePortfolioCashFlowType(cashFlowType);

		deleteCashFlow(this.flow1.getId());
		deleteCashFlow(this.flow2.getId());
		deleteCashFlow(this.flow3.getId());
		deleteCashFlow(this.flow4.getId());

		deleteCashFlowGroup(this.grp1.getId());
		deleteCashFlowGroup(this.grp2.getId());
		deleteCashFlowGroup(this.grp3.getId());
		deleteCashFlowGroup(this.grp4.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////        PortfolioCashFlowType business methods tests        ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetPortfolioCashFlowType() {
		PortfolioCashFlowType portfolioCashFlowType1 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowType((short) 1), 2);
		PortfolioCashFlowType portfolioCashFlowType2 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowType((short) 2), 2);
		PortfolioCashFlowType portfolioCashFlowType3 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowType((short) 3), 2);
		PortfolioCashFlowType nonExistent = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowType((short) 4), 2);

		Assertions.assertEquals((short) 1, portfolioCashFlowType1.getId());
		Assertions.assertEquals((short) 2, portfolioCashFlowType2.getId());
		Assertions.assertEquals((short) 3, portfolioCashFlowType3.getId());
		Assertions.assertNull(nonExistent);
	}


	@Test
	public void testGetPortfolioCashFlowTypeByName() {
		PortfolioCashFlowType portfolioCashFlowType1 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeByName("Projected Flow"), 2);
		PortfolioCashFlowType portfolioCashFlowType2 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeByName("Actual Flow"), 2);
		PortfolioCashFlowType portfolioCashFlowType3 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeByName("Service Cost"), 2);

		Assertions.assertEquals("Projected Flow", portfolioCashFlowType1.getName());
		Assertions.assertEquals("Actual Flow", portfolioCashFlowType2.getName());
		Assertions.assertEquals("Service Cost", portfolioCashFlowType3.getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_projectedFlow_True() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setProjectedFlow(true);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_actualFlow_True() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setActualFlow(true);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_serviceCost_True() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setServiceCost(true);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_id_1() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setId((short) 1);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_id_2() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setId((short) 2);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_id_3() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setId((short) 3);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_name_projected_flow() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setFlowTypeName(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_name_actual_flow() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setFlowTypeName(ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_name_service_costs() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setFlowTypeName(SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE);
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(1, queryResultList.size());
		Assertions.assertEquals(SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE, queryResultList.get(0).getName());
	}


	@Test
	public void testGetPortfolioFlowTypeList_by_name_no_exists() {
		PortfolioCashFlowTypeSearchForm searchForm = new PortfolioCashFlowTypeSearchForm();
		List<PortfolioCashFlowType> queryResultList;

		searchForm.setFlowTypeName("nothing");
		queryResultList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeList(searchForm), 2);
		Assertions.assertEquals(0, queryResultList.size());
	}


	@Test
	public void testSavePortfolioCashFlowType() {
		PortfolioCashFlowType testCashFlowType = new PortfolioCashFlowType();
		testCashFlowType.setActualFlow(true);
		testCashFlowType.setName("noType");
		testCashFlowType.setDescription("noType description");
		PortfolioCashFlowType savedEntity = this.portfolioCashFlowService.savePortfolioCashFlowType(testCashFlowType);
		Assertions.assertNotNull(savedEntity);

		PortfolioCashFlowType retrievedEntity = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowType(savedEntity.getId()), 2);
		Assertions.assertEquals(savedEntity.getId(), retrievedEntity.getId());

		// Test delete also
		this.portfolioCashFlowService.deletePortfolioCashFlowType(savedEntity.getId());
		retrievedEntity = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowType(savedEntity.getId()), 2);
		Assertions.assertNull(retrievedEntity);
	}


	@Test
	public void testSavePortfolioCashFlowType_invalidType_noTypeIndicatorSelected() {
		PortfolioCashFlowType testCashFlowType = new PortfolioCashFlowType();
		testCashFlowType.setName("noType");
		testCashFlowType.setDescription("noType description");
		Assertions.assertThrows(ImsErrorResponseException.class, () -> this.portfolioCashFlowService.savePortfolioCashFlowType(testCashFlowType));
	}


	@Test
	public void testSavePortfolioCashFlowType_invalidType_multipleTypeIndicatorsSelected() {
		PortfolioCashFlowType testCashFlowType = new PortfolioCashFlowType();
		testCashFlowType.setName("noType");
		testCashFlowType.setDescription("noType description");
		testCashFlowType.setActualFlow(true);
		testCashFlowType.setProjectedFlow(true);
		Assertions.assertThrows(ImsErrorResponseException.class, () -> this.portfolioCashFlowService.savePortfolioCashFlowType(testCashFlowType));
	}


	////////////////////////////////////////////////////////////////////////////
	////////       PortfolioCashFlowGroup business methods test         ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSavePortfolioCashFlowGroup_validation_fail_condition() {
		// Should fail to save due to date overlap.
		PortfolioCashFlowGroup grp = BeanUtils.cloneBean(this.grp1, false, false);
		grp.setEffectiveStartDate(DateUtils.toDate("03/25/2016"));

		// It throws a validation exception, but that gets intercepted by IMS and is recast as an ImsErrorResponseException.
		Assertions.assertThrows(ImsErrorResponseException.class, () -> this.portfolioCashFlowService.savePortfolioCashFlowGroup(grp));
	}


	@Test
	public void testGetPortfolioCashFlowGroup() {
		PortfolioCashFlowGroup retrievedGrp = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroup(this.grp1.getId()), 2);
		Assertions.assertEquals(this.grp1.getId(), retrievedGrp.getId());

		retrievedGrp = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroup(this.grp2.getId()), 2);
		Assertions.assertEquals(this.grp2.getId(), retrievedGrp.getId());

		retrievedGrp = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroup(this.grp3.getId()), 2);
		Assertions.assertEquals(this.grp3.getId(), retrievedGrp.getId());

		retrievedGrp = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroup(this.grp4.getId()), 2);
		Assertions.assertEquals(this.grp4.getId(), retrievedGrp.getId());
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_id() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		// Search by ID
		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setId(this.grp3.getId());
		groupList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 2);
		Assertions.assertEquals(1, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp3));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_investmentAccountId() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setInvestmentAccountId(this.grp3.getInvestmentAccount().getId());
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(1, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp3));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_investmentAccountNumber() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setInvestmentAccountNumberEquals(this.grp2.getInvestmentAccount().getNumber());
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(1, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp2));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_investmentSecurityId() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setCurrencyInvestmentSecurityId(this.grp3.getCurrencyInvestmentSecurity().getId());
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(1, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp3));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_currency_symbol() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setCurrencySymbol(this.grp4.getCurrencyInvestmentSecurity().getSymbol());
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(2, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp4));
		Assertions.assertTrue(groupList.contains(this.grp1));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_startDate() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setEffectiveStartDate(this.grp1.getEffectiveStartDate());
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(1, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp1));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_startDate_no_match() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setEffectiveStartDate(DateUtils.toDate("06/01/2018"));
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(0, groupList.size());
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_endDate() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setEffectiveEndDate(this.grp4.getEffectiveEndDate());
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(1, groupList.size());
		Assertions.assertTrue(groupList.contains(this.grp4));
	}


	@Test
	public void testGetPortfolioCashFlowGroupList_search_by_endDate_no_match() {
		PortfolioCashFlowGroupSearchForm searchForm;
		List<PortfolioCashFlowGroup> groupList;

		searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setEffectiveEndDate(DateUtils.toDate("01/21/2012"));
		groupList = filterListToTestScope(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 4));
		Assertions.assertEquals(0, groupList.size());
	}


	////////////////////////////////////////////////////////////////////////////
	////////         PortfolioCashFlow business methods tests           ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetPortfolioCashFlow() {
		PortfolioCashFlow retrievedCashFlow1 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlow(this.flow1.getId()), 2);
		PortfolioCashFlow retrievedCashFlow2 = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlow(this.flow2.getId()), 2);

		Assertions.assertEquals(this.flow1.getId(), retrievedCashFlow1.getId());
		Assertions.assertEquals(this.flow2.getId(), retrievedCashFlow2.getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_id() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setId(this.flow1.getId());
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		cashFlowList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 2);
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow1.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_searchPattern() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setSearchPattern(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE);
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow1.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_flowTypeName() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setFlowTypeName(SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE);
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow4.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_flowTypeId() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setCashFlowTypeId((short) 3);
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow4.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_flowTypeBoolean() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setActualFlow(true);
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(2, cashFlowList.size());
		Assertions.assertTrue(cashFlowList.contains(this.flow2));
		Assertions.assertTrue(cashFlowList.contains(this.flow3));
	}


	@Test
	public void testGetPortfolioCashFlowList_by_cashFlowTypeId() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setCashFlowTypeId(this.flow1.getCashFlowType().getId());
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow1.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_cashFlowGroupId() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setCashFlowGroupId(this.flow1.getCashFlowGroup().getId());
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow1.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_cashFlowDate() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setCashFlowYear(this.flow3.getCashFlowYear());
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow3.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_cashFlowAmountEquals() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setCashFlowAmount(this.flow3.getCashFlowAmount());
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(this.flow3.getId(), cashFlowList.get(0).getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_by_accountId() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;
		final int accountId = 73;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setInvestmentAccountId(73);
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertEquals(accountId, cashFlowList.get(0).getCashFlowGroup().getInvestmentAccount().getId());
	}


	@Test
	public void testGetPortfolioCashFlowList_search_by_flowStartDate() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setFlowEffectiveStartDate(this.grp1.getEffectiveStartDate());
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertTrue(cashFlowList.contains(this.flow1));
	}


	@Test
	public void testGetPortfolioCashFlowList_search_by_flowEffectiveStartDate_no_match() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setFlowEffectiveStartDate(DateUtils.toDate("06/01/2018"));
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(0, cashFlowList.size());
	}


	@Test
	public void testGetPortfolioCashFlowList_search_by_endDate() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setFlowEffectiveEndDate(this.grp4.getEffectiveEndDate());
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(1, cashFlowList.size());
		Assertions.assertTrue(cashFlowList.contains(this.flow4));
	}


	@Test
	public void testGetPortfolioCashFlowList_search_by_flowEndDate_no_match() {
		PortfolioCashFlowSearchForm searchForm;
		List<PortfolioCashFlow> cashFlowList;

		searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setOrderBy("id:desc");
		searchForm.setLimit(QUERY_LIMIT);
		searchForm.setFlowEffectiveEndDate(DateUtils.toDate("01/21/2012"));
		cashFlowList = filterListToTestScopeForFlow(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 4));
		Assertions.assertEquals(0, cashFlowList.size());
	}


	@Test
	public void testSavePortfolioCashFlow_testCalculator_projected_flow() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(1, 1927, "01/01/3039", "10/23/3039", "12/31/3039", this.groupTypeNonRolling);
		final PortfolioCashFlowType cashFlowType = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeByName("Projected Flow"), 4);

		PortfolioCashFlow cashFlow = createPortfolioCashFlow(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, BigDecimal.valueOf(120000.00), 3031, true);
		cashFlow.setCashFlowType(cashFlowType);
		cashFlow = this.portfolioCashFlowService.savePortfolioCashFlow(cashFlow);

		Assertions.assertEquals((short) 1, cashFlow.getStartingMonthNumber());
		Assertions.assertEquals((short) 12, cashFlow.getEndingMonthNumber());

		this.portfolioCashFlowService.deletePortfolioCashFlow(cashFlow.getId());
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_testCalculator_service_cost() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/01/2040", "10/23/2040", "11/30/2041", this.groupTypeNonRolling);
		final PortfolioCashFlowType cashFlowType = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeByName("Service Cost"), 2);

		PortfolioCashFlow cashFlow = createPortfolioCashFlow(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, BigDecimal.valueOf(100000.00), 2040, true);
		cashFlow.setCashFlowType(cashFlowType);
		cashFlow = this.portfolioCashFlowService.savePortfolioCashFlow(cashFlow);

		Assertions.assertEquals((short) 1, cashFlow.getStartingMonthNumber());
		Assertions.assertEquals((short) 12, cashFlow.getEndingMonthNumber());

		this.portfolioCashFlowService.deletePortfolioCashFlow(cashFlow.getId());
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_add_add_multipleValues() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/01/2130", "03/01/2130", "12/31/2130", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2131, 2132, 2133, 2134, 2135);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlows = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlows);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_add_new_minimum_year_entry() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/02/2120", "03/01/2120", "12/31/2120", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount = BigDecimal.valueOf(1500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		PortfolioCashFlow cashFlow = createPortfolioCashFlow(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, amount, 2120, true);
		cashFlow = this.portfolioCashFlowService.savePortfolioCashFlow(cashFlow);

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertTrue(cashFlowList2.contains(cashFlow));
		Assertions.assertEquals(6, cashFlowList2.size());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_delete_minimum_year_entry() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/03/2120", "03/01/2140", "12/31/2140", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);


		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		this.portfolioCashFlowService.deletePortfolioCashFlow(cashFlowList1.get(0).getId());

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertFalse(cashFlowList2.contains(cashFlowList1.get(0)));
		Assertions.assertEquals((short) 2122, cashFlowList2.get(0).getCashFlowYear());
		Assertions.assertEquals(4, cashFlowList2.size());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_delete_non_minimum_year_entry() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/03/2120", "03/01/2150", "12/31/2150", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		this.portfolioCashFlowService.deletePortfolioCashFlow(cashFlowList1.get(2).getId());

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertFalse(cashFlowList2.contains(cashFlowList1.get(2)));
		Assertions.assertEquals((short) 2121, cashFlowList1.get(0).getCashFlowYear());
		Assertions.assertEquals(4, cashFlowList2.size());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_change_minimum_year_entry_date_up() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/02/2120", "03/01/2160", "12/31/2160", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		cashFlowList1.get(0).setCashFlowYear((short) 2130);
		this.portfolioCashFlowService.savePortfolioCashFlow(cashFlowList1.get(0));

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertEquals(5, cashFlowList2.size());
		Assertions.assertEquals((short) 2122, cashFlowList2.get(0).getCashFlowYear());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_change_minimum_year_entry_date_down() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/02/2120", "03/01/2170", "12/31/2170", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		cashFlowList1.get(0).setCashFlowYear((short) 2120);
		this.portfolioCashFlowService.savePortfolioCashFlow(cashFlowList1.get(0));

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertEquals(5, cashFlowList2.size());
		Assertions.assertEquals((short) 2120, cashFlowList2.get(0).getCashFlowYear());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	@Test
	public void testSavePortfolioCashFlow_change_date_to_conflict() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/02/2180", "03/01/2180", "12/31/2180", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		cashFlowList1.get(1).setCashFlowYear((short) 2123);
		Assertions.assertThrows(ImsErrorResponseException.class, () -> this.portfolioCashFlowService.savePortfolioCashFlow(cashFlowList1.get(1)));

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertEquals(5, cashFlowList2.size());
		Assertions.assertEquals((short) 2121, cashFlowList2.get(0).getCashFlowYear());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	public void testSavePortfolioCashFlow_add_new_conflicting_year_entry() {
		final PortfolioCashFlowGroup group = createAndSavePortfolioCashFlowGroup(7, 1927, "01/02/2190", "03/01/2190", "12/31/2190", this.groupTypeNonRolling);
		final List<Integer> cashFlowYearList = CollectionUtils.createList(2121, 2122, 2123, 2124, 2125);
		final BigDecimal amount1 = BigDecimal.valueOf(100000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount2 = BigDecimal.valueOf(200000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount3 = BigDecimal.valueOf(300000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount4 = BigDecimal.valueOf(400000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount5 = BigDecimal.valueOf(500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		final BigDecimal amount = BigDecimal.valueOf(1500000.00).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final List<BigDecimal> cashFlowAmountList = CollectionUtils.createList(amount1, amount2, amount3, amount4, amount5);
		List<PortfolioCashFlow> cashFlowList1 = addMultipleCashFlowsToGroup(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, cashFlowAmountList, cashFlowYearList);
		validateStartingAndEndingMonthNumber(cashFlowList1);

		PortfolioCashFlow cashFlow = createPortfolioCashFlow(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, group, amount, 2121, true);
		Assertions.assertThrows(ImsErrorResponseException.class, () -> this.portfolioCashFlowService.savePortfolioCashFlow(cashFlow));

		List<PortfolioCashFlow> cashFlowList2 = getCashFlowListForGroup(group);
		Assertions.assertFalse(cashFlowList2.contains(cashFlow));
		Assertions.assertEquals((short) 2121, cashFlowList2.get(0).getCashFlowYear());
		Assertions.assertEquals(5, cashFlowList2.size());
		validateStartingAndEndingMonthNumber(cashFlowList2);

		// cleanup
		deleteCashFlows(getCashFlowListForGroup(group));
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(group.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortfolioCashFlowGroup createAndSavePortfolioCashFlowGroup(Integer accountId, Integer currencySecurityId, String asOfDate, String effectiveStartDate, String endDate, SystemListItem groupType) {
		PortfolioCashFlowGroup grp = new PortfolioCashFlowGroup();
		grp.setAsOfDate(DateUtils.toDate(asOfDate));
		grp.setEffectiveStartDate(DateUtils.toDate(effectiveStartDate));
		if (endDate != null) {
			grp.setEffectiveEndDate(DateUtils.toDate(endDate));
		}

		grp.setCurrencyInvestmentSecurity(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentInstrumentService.getInvestmentSecurity(currencySecurityId), 2));
		grp.setInvestmentAccount(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountService.getInvestmentAccount(accountId), 2));
		grp.setCashFlowGroupType(groupType);

		return this.portfolioCashFlowService.savePortfolioCashFlowGroup(grp);
	}


	private PortfolioCashFlow createPortfolioCashFlow(String portfolioCashFlowTypeName, PortfolioCashFlowGroup group, BigDecimal amount, int cashFlowYear, boolean save) {
		PortfolioCashFlow cashFlow = new PortfolioCashFlow();

		PortfolioCashFlowType cashFlowType = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowTypeByName(portfolioCashFlowTypeName), 4);

		cashFlow.setCashFlowType(cashFlowType);
		cashFlow.setCashFlowGroup(group);
		cashFlow.setCashFlowAmount(amount);
		cashFlow.setCashFlowYear((short) cashFlowYear);
		return save ? this.portfolioCashFlowService.savePortfolioCashFlow(cashFlow) : cashFlow;
	}


	private List<PortfolioCashFlow> filterListToTestScopeForFlow(List<PortfolioCashFlow> portfolioCashFlowList) {
		return CollectionUtils.getStream(portfolioCashFlowList)
				.filter(entry -> this.groupIdList.contains(entry.getCashFlowGroup().getId())).collect(Collectors.toList());
	}


	private List<PortfolioCashFlowGroup> filterListToTestScope(List<PortfolioCashFlowGroup> portfolioCashFlowGroupList) {
		return CollectionUtils.getStream(portfolioCashFlowGroupList)
				.filter(entry -> this.groupIdList.contains(entry.getId())).collect(Collectors.toList());
	}


	private List<PortfolioCashFlow> addMultipleCashFlowsToGroup(String cashFlowTypeName, PortfolioCashFlowGroup group, List<BigDecimal> cashFlowAmountList, List<Integer> cashFlowYearList) {
		List<PortfolioCashFlow> cashFlowList = new ArrayList<>();
		for (int index = 0; index < cashFlowYearList.size(); ++index) {
			PortfolioCashFlow cashFlow = createPortfolioCashFlow(cashFlowTypeName, group, cashFlowAmountList.get(index), cashFlowYearList.get(index), true);
			cashFlowList.add(cashFlow);
		}
		return sortPortfolioCashFlowList(cashFlowList);
	}


	/**
	 * Takes a list of PortfolioCashFlow entities, and verifies the startingMonthNumber and endingMonthNumber have been properly calculated.
	 * First item in the list must have the lowest year.
	 */
	private void validateStartingAndEndingMonthNumber(List<PortfolioCashFlow> cashFlowList) {
		Assertions.assertFalse(CollectionUtils.isEmpty(cashFlowList), "No values found in cash flow list.");

		final int endingMonthOffset = 11;
		List<String> flowTypes = CollectionUtils.createList(PROJECTED_FLOW_PORTFOLIO_CASH_FLOW_TYPE, ACTUAL_FLOW_PORTFOLIO_CASH_FLOW_TYPE, SERVICE_COST_PORTFOLIO_CASH_FLOW_TYPE);

		for (String flowTypeName : flowTypes) {
			List<PortfolioCashFlow> sublistByFlowType = CollectionUtils.getFiltered(cashFlowList, entry -> flowTypeName.equals(entry.getCashFlowType().getName()));
			if (CollectionUtils.isEmpty(sublistByFlowType)) {
				continue;
			}
			sortPortfolioCashFlowList(sublistByFlowType);
			final int minimumYear = sublistByFlowType.get(0).getCashFlowYear();

			for (PortfolioCashFlow cashFlow : CollectionUtils.getIterable(sublistByFlowType)) {
				int expectedStartingMonthNumber = calculateStartingMonthNumber(cashFlow, minimumYear);
				int expectedEndingMonthNumber = expectedStartingMonthNumber + endingMonthOffset;
				Assertions.assertEquals(expectedStartingMonthNumber, (int) cashFlow.getStartingMonthNumber(), "Unexpected value for startingMonthNumber of PortfolioCashFlow");
				Assertions.assertEquals(expectedEndingMonthNumber, (int) cashFlow.getEndingMonthNumber(), "Unexpected value for endingMonthNumber of PortfolioCashFlow");
			}
		}
	}


	/**
	 * Calculates and returns a PortfolioCashFlow entity's expected StartingMonthNumber property value.
	 */
	private int calculateStartingMonthNumber(PortfolioCashFlow portfolioCashFlow, int minimumCashFlowYear) {
		return (portfolioCashFlow.getCashFlowYear() - minimumCashFlowYear) * 12 + 1;
	}


	/**
	 * Returns a list of PortfolioCashFlow entries associated with the specified PortfolioCashFlowGroup.
	 */
	private List<PortfolioCashFlow> getCashFlowListForGroup(PortfolioCashFlowGroup group) {
		List<PortfolioCashFlow> cashFlowList = null;
		if (group != null) {
			PortfolioCashFlowSearchForm searchForm = new PortfolioCashFlowSearchForm();
			searchForm.setCashFlowGroupId(group.getId());
			cashFlowList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 2);
			sortPortfolioCashFlowList(cashFlowList);
		}
		return cashFlowList;
	}


	private List<PortfolioCashFlow> sortPortfolioCashFlowList(List<PortfolioCashFlow> cashFlowList) {
		return CollectionUtils.sort(cashFlowList, (entryA, entryB) -> MathUtils.compare(entryA.getCashFlowYear(), entryB.getCashFlowYear()));
	}


	private void deleteCashFlows(List<PortfolioCashFlow> cashFlowList) {
		for (PortfolioCashFlow cashFlow : CollectionUtils.getIterable(cashFlowList)) {
			this.portfolioCashFlowService.deletePortfolioCashFlow(cashFlow.getId());
		}
	}


	private void deleteCashFlow(int cashFlowId) {
		this.portfolioCashFlowService.deletePortfolioCashFlow(cashFlowId);
	}


	private void deleteCashFlowGroup(int cashFlowGroupId) {
		this.portfolioCashFlowHistoryService.clearPortfolioCashFlowHistoryList(cashFlowGroupId);
		this.portfolioCashFlowService.deletePortfolioCashFlowGroup(cashFlowGroupId);
	}
}

