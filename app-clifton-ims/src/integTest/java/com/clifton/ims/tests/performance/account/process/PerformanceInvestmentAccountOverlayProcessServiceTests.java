package com.clifton.ims.tests.performance.account.process;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.PerformanceBenchmarkService;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;
import com.clifton.performance.account.benchmark.process.PerformanceBenchmarkProcessService;
import com.clifton.performance.account.benchmark.search.PerformanceSummaryBenchmarkSearchForm;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayAssetClass;
import com.clifton.product.performance.overlay.ProductPerformanceOverlayService;
import com.clifton.product.performance.overlay.ProductPerformancePortfolioRun;
import com.clifton.product.performance.overlay.process.ProductPerformanceOverlayProcessService;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.clifton.workflow.transition.search.WorkflowTransitionSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>PerformanceInvestmentAccountOverlayProcessServiceTests</code> ...
 *
 * @author manderson
 */

public class PerformanceInvestmentAccountOverlayProcessServiceTests extends BasePerformanceInvestmentAccountProcessServiceTests {

	private static final Date TEST_DATE = DateUtils.toDate("06/30/2014");


	@Resource
	private PerformanceBenchmarkService performanceBenchmarkService;

	@Resource
	private PerformanceBenchmarkProcessService performanceBenchmarkProcessService;

	@Resource
	private ProductPerformanceOverlayService productPerformanceOverlayService;
	@Resource
	private ProductPerformanceOverlayProcessService productPerformanceOverlayProcessService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPIOSServiceAccounts() {
		// Account Number 719061
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("719061", DateUtils.toDate("06/30/2015"));
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);

		// Includes CCY - June G/L small change on last day - believe this is due to Accrual Date convention change
		bean = getPerformanceInvestmentAccount("056901", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean, BigDecimal.valueOf(0.0025));

		// Includes REPO Accrued Interest and G/L Other In Account Return and G/L Override that should remain
		bean = getPerformanceInvestmentAccount("578509", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);

		// Does Not Use Financing Rate
		bean = getPerformanceInvestmentAccount("071000", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);

		// Sub-Account that uses parent account for total portfolio value
		bean = getPerformanceInvestmentAccount("051020", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}


	@Test
	public void testPIOSServiceAccountsRecreate() {
		Date runDate = DateUtils.toDate("08/31/2021");

		// Account number 221700 Overlay no parent
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("221700", runDate);
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount recreatedBean = recreatePerformanceInvestmentAccount(bean);
		validateSummaryPropertiesEqual(bean, recreatedBean);

		// Overlay Sub-Account that uses parent account for total portfolio value
		bean = getPerformanceInvestmentAccount("372023", runDate);
		rebuildSnapshotsForSummary(bean);
		recreatedBean = recreatePerformanceInvestmentAccount(bean);
		validateSummaryPropertiesEqual(bean, recreatedBean);
	}


	@Test
	public void testPIOSServiceAccountWithOverrideRecalculatesReturn() {
		// Account Number 297615
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccount("297615", DateUtils.toDate("06/30/2021"));
		rebuildSnapshotsForSummary(performanceInvestmentAccount);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(performanceInvestmentAccount.getId());
		validateSummaryPropertiesEqual(performanceInvestmentAccount, previewBean);

		Date overriddenMeasureDate = DateUtils.toDate("06/18/2021");
		ProductPerformancePortfolioRun performanceRun = CollectionUtils.getOnlyElement(
				CollectionUtils.getStream(this.productPerformanceOverlayService.getProductPerformanceOverlayRunListByPerformanceSummary(performanceInvestmentAccount.getId()))
						.filter(pRun -> DateUtils.isEqualWithoutTime(overriddenMeasureDate, pRun.getMeasureDate()))
						.collect(Collectors.toList()));
		Assertions.assertNotNull(performanceRun, "Failed to find expected overridden performance portfolio run.");

		performanceRun = this.productPerformanceOverlayService.getProductPerformanceOverlayRun(performanceRun.getId());
		ProductPerformanceOverlayAssetClass assetClassWithOverride = CollectionUtils.getOnlyElement(performanceRun.getPerformanceOverlayAssetClassList());
		assetClassWithOverride.setOverlayTargetOverride(new BigDecimal("-1769559.22")); // calculated value
		// save and reprocess
		if (WorkflowStatus.STATUS_CLOSED.equals(performanceInvestmentAccount.getWorkflowStatus().getName())) {
			WorkflowTransitionSearchForm transitionSearchForm = new WorkflowTransitionSearchForm();
			transitionSearchForm.setWorkflowName("Performance Summary Status");
			transitionSearchForm.setEndWorkflowStateName("Approved By Accounting");
			List<WorkflowTransition> transitionList = this.workflowTransitionService.getWorkflowTransitionList(transitionSearchForm);
			for (WorkflowTransition transition : transitionList) {
				if (transition.getStartWorkflowState().equals(performanceInvestmentAccount.getWorkflowState())) {
					this.workflowTransitionService.executeWorkflowTransitionByTransition("PerformanceInvestmentAccount", performanceInvestmentAccount.getId().longValue(), transition.getId());
					break;
				}
			}
		}
		performanceRun = this.productPerformanceOverlayProcessService.saveProductPerformanceOverlayRun(performanceRun);
		Assertions.assertEquals(new BigDecimal("-12.79186144905312716700"), performanceRun.getAccountReturn());

		assetClassWithOverride = CollectionUtils.getOnlyElement(performanceRun.getPerformanceOverlayAssetClassList());
		assetClassWithOverride.setOverlayTargetOverride(new BigDecimal("11232003"));
		// save and reprocess
		performanceRun = this.productPerformanceOverlayProcessService.saveProductPerformanceOverlayRun(performanceRun);
		Assertions.assertEquals(new BigDecimal("-2.01488680387191660500"), performanceRun.getAccountReturn());
	}


	@Test
	public void testHistoricalReturnCalculations() {
		// Runs about 300 summaries to confirm historical calculations (QTD, YTD, ITD)
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setMeasureDate(DateUtils.toDate("02/28/2017"));
		searchForm.setWorkflowStateNameEquals("Posted");
		List<PerformanceInvestmentAccount> performanceInvestmentAccountList = getPerformanceInvestmentAccountList(searchForm);
		Set<String> skipAccountNumbers = new HashSet<>();
		skipAccountNumbers.add("071000"); // Removed inception date override
		skipAccountNumbers.add("071400"); // Changed options
		skipAccountNumbers.add("071500"); // Changed options
		skipAccountNumbers.add("077100"); // Changed inception date override
		skipAccountNumbers.add("110120"); // Changed inception date override
		skipAccountNumbers.add("110170"); // Changed inception date override
		skipAccountNumbers.add("297601");
		skipAccountNumbers.add("400000"); // Removed inception date override
		skipAccountNumbers.add("221700"); // Removed inception date override
		skipAccountNumbers.add("423000"); // Changed inception date override
		skipAccountNumbers.add("570000");
		skipAccountNumbers.add("340300"); // Changed options
		skipAccountNumbers.add("400300"); // Changed options
		skipAccountNumbers.add("400301"); // Changed options

		for (PerformanceInvestmentAccount performanceInvestmentAccount : CollectionUtils.getIterable(performanceInvestmentAccountList)) {
			if (!skipAccountNumbers.contains(performanceInvestmentAccount.getClientAccount().getNumber())) {
				// As long as earlier summaries weren't updated after this one - test it, otherwise history is being updated
				if (!isPreviousSummaryUpdatedAfter(performanceInvestmentAccount)) {
					PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccountHistoricalOnly(performanceInvestmentAccount.getId());
					validateSummaryPropertiesEqual(performanceInvestmentAccount, previewBean);
				}
			}
		}
	}


	protected boolean isPreviousSummaryUpdatedAfter(PerformanceInvestmentAccount performanceInvestmentAccount) {
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setClientAccountId(performanceInvestmentAccount.getClientAccount().getId());
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN, performanceInvestmentAccount.getUpdateDate()));
		searchForm.addSearchRestriction(new SearchRestriction("measureDate", ComparisonConditions.LESS_THAN, performanceInvestmentAccount.getMeasureDate()));
		return !CollectionUtils.isEmpty(RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> getPerformanceInvestmentAccountList(searchForm), "id"));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBenchmarkReturns() {
		// 445007 Nestle - Canadian Equities
		PerformanceInvestmentAccount summary = getPerformanceInvestmentAccount("445007", TEST_DATE);
		List<PerformanceSummaryBenchmark> originalList = getPerformanceSummaryBenchmarkList(summary.getId());
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForSummary(summary.getId());
		List<PerformanceSummaryBenchmark> newList = getPerformanceSummaryBenchmarkList(summary.getId());
		validateBenchmarkList(originalList, newList);

		// 495200 Pension Fund of the Christian Church - Adjusted MTD (was 0 before)
		summary = getPerformanceInvestmentAccount("495200", TEST_DATE);
		originalList = getPerformanceSummaryBenchmarkList(summary.getId());
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForSummary(summary.getId());
		newList = getPerformanceSummaryBenchmarkList(summary.getId());
		validateBenchmarkList(originalList, newList);

		// 713300 Amherst - Overridden start date (Terminated account)
		summary = getPerformanceInvestmentAccount("713300", DateUtils.toDate("05/31/2012"));
		originalList = getPerformanceSummaryBenchmarkList(summary.getId());
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForSummary(summary.getId());
		newList = getPerformanceSummaryBenchmarkList(summary.getId());
		validateBenchmarkList(originalList, newList);

		// 502000 Pepperdine DE - Sub Account
		summary = getPerformanceInvestmentAccount("502000", TEST_DATE);
		originalList = getPerformanceSummaryBenchmarkList(summary.getId());
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForSummary(summary.getId());
		newList = getPerformanceSummaryBenchmarkList(summary.getId());
		validateBenchmarkList(originalList, newList);

		// Basic Tests
		// 051030
		summary = getPerformanceInvestmentAccount("051030", TEST_DATE);
		originalList = getPerformanceSummaryBenchmarkList(summary.getId());
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForSummary(summary.getId());
		newList = getPerformanceSummaryBenchmarkList(summary.getId());
		validateBenchmarkList(originalList, newList);

		// 057000
		summary = getPerformanceInvestmentAccount("057000", TEST_DATE);
		originalList = getPerformanceSummaryBenchmarkList(summary.getId());
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForSummary(summary.getId());
		newList = getPerformanceSummaryBenchmarkList(summary.getId());
		validateBenchmarkList(originalList, newList, 2); // Historical return for one of the benchmarks was updated slightly so making the test more flexible for this account
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRecalculateBenchmarkReturnFromSelected() {
		// Recalculating from selected would not recalculate MTD values prior to the selected date, but should still include the previous MTD history in calculating QTD, YTD, ITD
		// 078350 Syn. MSCI EAFE (SYN EAFE - MFS CONTRACT security) - Recalculate as of 6/2014
		PerformanceInvestmentAccount summary = getPerformanceInvestmentAccount("078350", TEST_DATE);
		PerformanceSummaryBenchmarkSearchForm searchForm = new PerformanceSummaryBenchmarkSearchForm();
		searchForm.setBenchmarkSecurityId(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SYN EAFE - MFS CONTRACT").getId());
		searchForm.setSummaryId(summary.getId());

		List<PerformanceSummaryBenchmark> originalList = this.performanceBenchmarkService.getPerformanceSummaryBenchmarkList(searchForm);
		AssertUtils.assertTrue(CollectionUtils.getSize(originalList) == 1, "Expected one Summary Benchmark for 078350 SEAFE Security on 6/2014");
		this.performanceBenchmarkProcessService.processPerformanceSummaryBenchmarkListForBenchmark(originalList.get(0).getReferenceTwo().getId(), true, TEST_DATE);

		List<PerformanceSummaryBenchmark> newList = this.performanceBenchmarkService.getPerformanceSummaryBenchmarkList(searchForm);
		validateBenchmarkList(originalList, newList);
	}


	private void validateBenchmarkList(List<PerformanceSummaryBenchmark> originalList, List<PerformanceSummaryBenchmark> newList) {
		validateBenchmarkList(originalList, newList, null);
	}


	private void validateBenchmarkList(List<PerformanceSummaryBenchmark> originalList, List<PerformanceSummaryBenchmark> newList, Integer roundDecimals) {
		// There may be additional ones added later that weren't part of this summary at the time - so as long as originals are still there with correct values, we are OK
		for (PerformanceSummaryBenchmark originalSummaryBenchmark : CollectionUtils.getIterable(originalList)) {
			boolean found = false;
			for (PerformanceSummaryBenchmark newSummaryBenchmark : CollectionUtils.getIterable(newList)) {
				if (originalSummaryBenchmark.equals(newSummaryBenchmark)) {
					found = true;
					validateBenchmarkPropertiesEqual(originalSummaryBenchmark, newSummaryBenchmark, roundDecimals);
					break;
				}
			}
			if (!found) {
				Assertions.fail("Did not find Summary Benchmark [" + originalSummaryBenchmark.getLabel() + "] in new list.");
			}
		}
	}


	private void validateBenchmarkPropertiesEqual(PerformanceSummaryBenchmark originalBean, PerformanceSummaryBenchmark newBean, Integer roundDecimals) {
		// MTD Return - Some cases historical data might have been missing, so value was null before, but now can be
		// calculated - for those - if previously null, then don't validate.
		if (originalBean.getMonthToDateReturn() != null) {
			validateBenchmarkPropertyEqual("monthToDateReturn", originalBean, newBean, roundDecimals);
		}
		validateBenchmarkPropertyEqual("monthToDateReturnOverride", originalBean, newBean, roundDecimals);
		validateBenchmarkPropertyEqual("quarterToDateReturn", originalBean, newBean, roundDecimals);
		validateBenchmarkPropertyEqual("yearToDateReturn", originalBean, newBean, roundDecimals);
		validateBenchmarkPropertyEqual("inceptionToDateReturn", originalBean, newBean, roundDecimals);
	}


	private void validateBenchmarkPropertyEqual(String propertyName, PerformanceSummaryBenchmark originalBean, PerformanceSummaryBenchmark newBean, Integer roundDecimals) {
		// Unless specified - format to 4 decimals
		if (roundDecimals == null) {
			roundDecimals = 4;
		}
		BigDecimal originalValue = MathUtils.round((BigDecimal) BeanUtils.getPropertyValue(originalBean, propertyName), roundDecimals);
		BigDecimal newValue = MathUtils.round((BigDecimal) BeanUtils.getPropertyValue(newBean, propertyName), roundDecimals);
		Assertions.assertEquals(originalValue, newValue, "Performance Summary Benchmark [" + originalBean.getLabel() + "] Property [" + propertyName + "]");
	}


	private List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkList(int summaryId) {
		return this.performanceBenchmarkService.getPerformanceSummaryBenchmarkListBySummary(summaryId);
	}
}
