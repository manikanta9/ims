package com.clifton.ims.tests.dataaccess.search.hibernate;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.hibernate.HibernateReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import com.clifton.core.util.MathUtils;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.ShortType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


/**
 * These tests ensure HibernateUtils methods are working as expected.
 *
 * @author stevenf on 9/29/2016.
 */
@ContextConfiguration
@Transactional
public class HibernateUtilsTests extends BaseInMemoryDatabaseTests {

	@Resource
	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	@Transactional
	public void testFindByFields() {
		ReadOnlyDAO<SecurityResource> securityResourceDAO = getReadOnlyDao(SecurityResource.class);

		SecurityResource securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.name"}, new Object[]{"Business", null}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business", securityResource.getLabelExpanded());
		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.name"}, new Object[]{"BusinessContract", "Business"}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business / Contracts", securityResource.getLabelExpanded());
		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.name", "parent.parent.name"}, new Object[]{"ClientDocuments", "BusinessContract", "Business"}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business / Contracts / Client Documents", securityResource.getLabelExpanded());
		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.name", "parent.parent.name", "parent.parent.parent.name"}, new Object[]{"Trade Confirmations", "ClientDocuments", "BusinessContract", "Business"}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business / Contracts / Client Documents / Trade Confirmations", securityResource.getLabelExpanded());

		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.id"}, new Object[]{"Business", null}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business", securityResource.getLabelExpanded());
		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.id"}, new Object[]{"BusinessContract", 1}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business / Contracts", securityResource.getLabelExpanded());
		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.id", "parent.parent.id"}, new Object[]{"ClientDocuments", 2, 1}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business / Contracts / Client Documents", securityResource.getLabelExpanded());
		securityResource = CollectionUtils.getOnlyElement(securityResourceDAO.findByFields(new String[]{"name", "parent.id", "parent.parent.id", "parent.parent.parent.id"}, new Object[]{"Trade Confirmations", 3, 2, 1}));
		Assertions.assertNotNull(securityResource);
		Assertions.assertEquals("Business / Contracts / Client Documents / Trade Confirmations", securityResource.getLabelExpanded());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetNumericType() {
		Number value = null;

		Assertions.assertEquals(IntegerType.class, HibernateUtils.getNumericType(value).getClass());
		value = 1;
		Assertions.assertEquals(IntegerType.class, HibernateUtils.getNumericType(value).getClass());

		value = MathUtils.SHORT_THREE;
		Assertions.assertEquals(ShortType.class, HibernateUtils.getNumericType(value).getClass());

		value = (long) 50000;
		Assertions.assertEquals(LongType.class, HibernateUtils.getNumericType(value).getClass());

		value = MathUtils.BIG_DECIMAL_PENNY;
		Assertions.assertEquals(BigDecimalType.class, HibernateUtils.getNumericType(value).getClass());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	@Transactional // Required for session retrieval
	public void testGetSessionFactory() {
		ReadOnlyDAO<SecurityGroup> securityGroupDAO = getReadOnlyDao(SecurityGroup.class);
		HibernateReadOnlyDAO<SecurityGroup> hibernateSecurityGroupDAO = (HibernateReadOnlyDAO<SecurityGroup>) securityGroupDAO;
		Criteria criteria = hibernateSecurityGroupDAO.getSessionFactory().getCurrentSession().createCriteria(hibernateSecurityGroupDAO.getConfiguration().getBeanClass());
		Assertions.assertTrue(HibernateUtils.getSessionFactory(criteria) instanceof SessionFactoryImplementor);
	}


	@Test
	@Transactional // Required for session retrieval
	public void getCriteriaEntityName() {
		ReadOnlyDAO<SecurityGroup> securityGroupDAO = getReadOnlyDao(SecurityGroup.class);
		HibernateReadOnlyDAO<SecurityGroup> hibernateSecurityGroupDAO = (HibernateReadOnlyDAO<SecurityGroup>) securityGroupDAO;
		Criteria criteria = hibernateSecurityGroupDAO.getSessionFactory().getCurrentSession().createCriteria(hibernateSecurityGroupDAO.getConfiguration().getBeanClass());
		Assertions.assertEquals(SecurityGroup.class.getName(), HibernateUtils.getCriteriaEntityName(criteria));
	}


	@Test
	public void getIdentifierPropertyName() {
		ReadOnlyDAO<SecurityGroup> securityGroupDAO = getReadOnlyDao(SecurityGroup.class);
		HibernateReadOnlyDAO<SecurityGroup> hibernateSecurityGroupDAO = (HibernateReadOnlyDAO<SecurityGroup>) securityGroupDAO;
		SessionFactory sessionFactory = hibernateSecurityGroupDAO.getSessionFactory();
		Assertions.assertEquals("id", HibernateUtils.getIdentifierPropertyName(sessionFactory, SecurityGroup.class.getName()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void getAssociationClass() {
		ReadOnlyDAO<SecurityGroup> securityGroupDAO = getReadOnlyDao(SecurityGroup.class);
		HibernateReadOnlyDAO<SecurityGroup> hibernateSecurityGroupDAO = (HibernateReadOnlyDAO<SecurityGroup>) securityGroupDAO;
		SessionFactory sessionFactory = hibernateSecurityGroupDAO.getSessionFactory();
		Assertions.assertEquals(SecurityUserGroup.class, HibernateUtils.getAssociationClass(sessionFactory, SecurityGroup.class.getName() + ".userList"));
		Assertions.assertEquals(SecurityUserGroup.class, HibernateUtils.getAssociationClass(sessionFactory, SecurityUser.class.getName() + ".groupList"));
	}


	@Test
	public void getCollectionKeyProperty() {
		ReadOnlyDAO<SecurityGroup> securityGroupDAO = getReadOnlyDao(SecurityGroup.class);
		HibernateReadOnlyDAO<SecurityGroup> hibernateSecurityGroupDAO = (HibernateReadOnlyDAO<SecurityGroup>) securityGroupDAO;
		SessionFactory sessionFactory = hibernateSecurityGroupDAO.getSessionFactory();
		{
			String role = SecurityGroup.class.getName() + ".userList";
			Class<?> associationClass = HibernateUtils.getAssociationClass(sessionFactory, role);
			Assertions.assertEquals("referenceTwo", HibernateUtils.getAssociationKeyProperty(sessionFactory, role, associationClass));
		}
		{
			String role = SecurityUser.class.getName() + ".groupList";
			Class<?> associationClass = HibernateUtils.getAssociationClass(sessionFactory, role);
			Assertions.assertEquals("referenceOne", HibernateUtils.getAssociationKeyProperty(sessionFactory, role, associationClass));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends IdentityObject> ReadOnlyDAO<T> getReadOnlyDao(Class<T> clazz) {
		ReadOnlyDAO<T> dao = this.daoLocator.locate(clazz);
		return dao;
	}
}
