package com.clifton.ims.tests.marketdata.updater.security;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.field.mapping.MarketDataFieldValueMapping;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldValueMappingSearchForm;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityResult;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityRetrieverCommand;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityService;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author mwacker
 */

public class MarketDataInvestmentSecurityServiceTests extends BaseImsIntegrationTest {

	@Resource
	private MarketDataFieldService marketDataFieldService;
	@Resource
	private MarketDataFieldMappingService marketDataFieldMappingService;
	@Resource
	private MarketDataInvestmentSecurityService marketDataInvestmentSecurityService;
	@Resource
	private MarketDataSourceService marketDataSourceService;

	@Resource
	private InvestmentSetupService investmentSetupService;


	@Test
	public void testLoadBondDataFromBloomberg_38378XDB4() {
		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		searchForm.setInvestmentTypeName("Bonds");
		searchForm.setInvestmentTypeSubType("ABS");
		searchForm.setInvestmentTypeSubType2("Agency");
		InvestmentInstrumentHierarchy hierarchy = CollectionUtils.getFirstElementStrict(this.investmentSetupService.getInvestmentInstrumentHierarchyList(searchForm));

		updateCouponTypeMarketDataFieldValueMapping();

		MarketDataInvestmentSecurityRetrieverCommand command = new MarketDataInvestmentSecurityRetrieverCommand();
		command.setInstrumentHierarchyId(hierarchy.getId());
		command.setSymbol("38378XDB4");
		MarketDataInvestmentSecurityResult securityResult = this.marketDataInvestmentSecurityService.getMarketDataInvestmentSecurity(command);

		InvestmentSecurity security = securityResult.getNewSecurity();

		Assertions.assertEquals("38378XDB4", security.getSymbol());
		Assertions.assertEquals("38378XDB4", security.getCusip());
		Assertions.assertEquals("US38378XDB47", security.getIsin());
		Assertions.assertNull(security.getSedol());
		Assertions.assertEquals("United States", security.getInstrument().getCountryOfRisk().getLabel());
		Assertions.assertEquals("United States", security.getInstrument().getSettlementCalendar().getName());
		Assertions.assertEquals("United States of America", security.getBusinessCompany().getLabel());
		Assertions.assertEquals("07/01/2014", DateUtils.fromDate(security.getStartDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals("06/16/2053", DateUtils.fromDate(security.getEndDate(), DateUtils.DATE_FORMAT_INPUT));
		Assertions.assertEquals("US Dollar", security.getInstrument().getTradingCurrency().getName());
		Assertions.assertEquals("1.742000000", getSystemColumnValue(security.getColumnValueList(), "Coupon Amount (%)"));
		Assertions.assertEquals("Monthly", getSystemColumnValue(security.getColumnValueList(), "Coupon Frequency"));
		Assertions.assertEquals("Fixed", getSystemColumnValue(security.getColumnValueList(), "Coupon Type"));
		Assertions.assertEquals("30/360", getSystemColumnValue(security.getColumnValueList(), "Day Count"));
		Assertions.assertEquals("0", getSystemColumnValue(security.getColumnValueList(), "144A"));
		Assertions.assertEquals("MULTI", getSystemColumnValue(security.getColumnValueList(), "Collateral Type"));

		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type I"));
		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type II"));
		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type III"));
		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type IV"));
		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type V"));
		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type VI"));
		Assertions.assertNull(getSystemColumnValue(security.getColumnValueList(), "Mortgage Tranche Type VII"));
	}


	@Test
	public void testMarketDataFieldColumnMappingList() {
		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		searchForm.setInvestmentTypeName("Bonds");
		searchForm.setInvestmentTypeSubType("ABS");
		searchForm.setInvestmentTypeSubType2("Agency");
		InvestmentInstrumentHierarchy hierarchy = CollectionUtils.getFirstElementStrict(this.investmentSetupService.getInvestmentInstrumentHierarchyList(searchForm));

		MarketDataInvestmentSecurityRetrieverCommand command = new MarketDataInvestmentSecurityRetrieverCommand();
		command.setInstrumentHierarchyId(hierarchy.getId());
		command.setSymbol("38378XDB4");
		// If there is a different count, open up the security in IMS, click the down arrow on the Bloomberg button to show field mappings to see the differences
		List<MarketDataFieldColumnMapping> mappingList = this.marketDataInvestmentSecurityService.getMarketDataFieldColumnMappingList(command);
		Assertions.assertEquals(48, mappingList.size());

		Set<String> expectedMappings = CollectionUtils.createHashSet(
				"InvestmentSecurity\tMortgage Tranche Type IV\t\tMortgage Tranche Type IV\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\tMortgage Deal Name\t\tMortgage Deal Name\tMTG_DEAL_NAME",
				"InvestmentSecurity\tMortgage Tranche Type VII\t\tMortgage Tranche Type VII\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\tCoupon Frequency\t\tCoupon Frequency\tCPN_FREQ",
				"InvestmentSecurity\tMortgage Tranche Type V\t\tMortgage Tranche Type V\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\tMortgage Tranche Type I\t\tMortgage Tranche Type I\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\t144A\t\t144A\t144A_FLAG",
				"InvestmentSecurity\tMortgage Tranche Type II\t\tMortgage Tranche Type II\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\tCoupon Type\t\tCoupon Type\tCPN_TYP",
				"InvestmentSecurity\tCollateral Type\t\tMortgage Collateral Type\tMTG_GEN_TICKER",
				"InvestmentSecurity\tMortgage Tranche Type III\t\tMortgage Tranche Type III\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\tMortgage Tranche Type VI\t\tMortgage Tranche Type VI\tMTG_TRANCHE_TYP_LONG",
				"InvestmentSecurity\tDay Count\t\tDay Count\tDAY_CNT_DES",
				"InvestmentSecurity\tCoupon Amount (%)\t\tCoupon\tCPN",
				"InvestmentSecurity\t\tAccrualMethod\t\t",
				"InvestmentSecurity\t\tBigInvestmentSecurityID\t\t",
				"InvestmentSecurity\t\tBusinessCompanyID\tSecurity Company - Standard\tULTIMATE_OBLIGOR_ID,ID_BB_ULTIMATE_PARENT_CO",
				"InvestmentSecurity\t\tBusinessContractID\t\t",
				"InvestmentInstrument\t\tCountryOfRiskID\tCountry of Risk\tCNTRY_OF_RISK",
				"InvestmentSecurity\t\tCusip\tCUSIP\tID_CUSIP",
				"InvestmentSecurity\t\tEarlyTerminationDate\tBonds - Early Termination Date\tCALLED_DT",
				"InvestmentSecurity\t\tEndDate\tMaturity Date\tMATURITY",
				"InvestmentSecurity\t\tFirstDeliveryDate\t\t",
				"InvestmentSecurity\t\tFirstNoticeDate\t\t",
				"InvestmentSecurity\t\tInvestmentInstrumentID\t\t",
				"InvestmentSecurity\t\tInvestmentSecurityDescription\tBond - Description - ABS\tSECURITY_TYP",
				"InvestmentSecurity\t\tInvestmentSecurityID\t\t",
				"InvestmentSecurity\t\tInvestmentSecurityName\tSecurity Name - Specific\tSECURITY_NAME,SECURITY_TYP2",
				"InvestmentSecurity\t\tIsCurrency\t\t",
				"InvestmentSecurity\t\tIsIlliquid\t\t",
				"InvestmentSecurity\t\tIsin\tISIN\tID_ISIN",
				"InvestmentSecurity\t\tLastDeliveryDate\t\t",
				"InvestmentSecurity\t\tOccSymbol\t\t",
				"InvestmentSecurity\t\tPriceMultiplierOverride\t\t",
				"InvestmentSecurity\t\tReferenceInvestmentSecurityID\t\t",
				"InvestmentSecurity\t\tSedol\t\t",
				"InvestmentInstrument\t\tSettlementCalendarID\tBond CDR Code - Settlement\tCDR_SETTLE_CODE,CDR_COUNTRY_CODE",
				"InvestmentSecurity\t\tSettlementCurrencyID\t\t",
				"InvestmentSecurity\t\tStartDate\tIssue Date\tISSUE_DT",
				"InvestmentSecurity\t\tSymbol\t\t",
				"InvestmentInstrument\t\tTradingCurrencyID\tCurrency Denomination\tCRNCY",
				"InvestmentSecurity\t\tUnderlyingSecurityID\t\t",
				"InvestmentSecurity\t\tOptionType\t\t",
				"InvestmentSecurity\t\tOptionStrikePrice\t\t",
				"InvestmentSecurity\t\tFigi\tFinancial Instrument Global Identifier (FIGI)\tID_BB_GLOBAL",
				"InvestmentSecurity\t\tSettlementExpiryTime\t\t",
				"InvestmentSecurity\t\tOptionStyle\t\t"
		);
		Set<String> actualMappings = mappingList.stream().map(this::getMappingString).collect(Collectors.toSet());
		expectedMappings.forEach(expectedMapping -> Assertions.assertTrue(actualMappings.contains(expectedMapping), "Expected to finding mapping: " + expectedMapping));
	}


	private String getMappingString(MarketDataFieldColumnMapping mapping) {
		//}, String tableName, String templateName, String columnName, String fieldName, String externalFieldName){
		StringBuilder sb = new StringBuilder();
		sb.append(mapping.getTableName()).append("\t")
				.append(mapping.getTemplate() != null ? mapping.getTemplate().getName() : "").append("\t")
				.append(mapping.getColumn() != null ? mapping.getColumn().getName() : "").append("\t")
				.append(mapping.getDataField() != null ? mapping.getDataField().getName() : "").append("\t")
				.append(mapping.getDataField() != null ? mapping.getDataField().getExternalFieldName() : "");
		return sb.toString();
	}


	private String getSystemColumnValue(List<SystemColumnValue> columnValueList, String columnName) {
		for (SystemColumnValue columnValue : CollectionUtils.getIterable(columnValueList)) {
			if (columnName.equals(columnValue.getColumn().getName())) {
				return columnValue.getValue();
			}
		}
		return null;
	}


	private void updateCouponTypeMarketDataFieldValueMapping() {
		MarketDataFieldValueMappingSearchForm marketDataFieldValueMappingSearchForm = new MarketDataFieldValueMappingSearchForm();
		marketDataFieldValueMappingSearchForm.setDataFieldName("Coupon Type");
		marketDataFieldValueMappingSearchForm.setDataSourceName("Bloomberg");
		marketDataFieldValueMappingSearchForm.setFromValueEquals("FIXED");
		List<MarketDataFieldValueMapping> fieldMappingList = this.marketDataFieldMappingService.getMarketDataFieldValueMappingList(marketDataFieldValueMappingSearchForm);
		if (CollectionUtils.isEmpty(fieldMappingList)) {
			MarketDataFieldValueMapping fieldValueMapping = new MarketDataFieldValueMapping();
			fieldValueMapping.setDataSource(this.marketDataSourceService.getMarketDataSourceByName("Bloomberg"));
			fieldValueMapping.setDataField(this.marketDataFieldService.getMarketDataFieldByName("Coupon Type"));
			fieldValueMapping.setFromValue("FIXED");
			fieldValueMapping.setToValue("Fixed");
			this.marketDataFieldMappingService.saveMarketDataFieldValueMapping(fieldValueMapping);
		}
	}
}
