package com.clifton.ims.tests.bloomberg.client;


import com.clifton.bloomberg.client.BloombergDataService;
import com.clifton.bloomberg.messages.BloombergMarketSectors;
import com.clifton.bloomberg.messages.BloombergPeriodicity;
import com.clifton.bloomberg.messages.request.BloombergRequestException;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.bloomberg.messages.request.ReferenceDataRequest;
import com.clifton.bloomberg.messages.response.ReferenceDataResponse;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.integration.export.ScheduledExportTests;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Map;


public class BloombergClientTests extends BaseImsIntegrationTest {

	private static final Logger log = LoggerFactory.getLogger(ScheduledExportTests.class);

	@Resource
	private BloombergDataService bloombergDataService;

	@Resource
	private MarketDataFieldService marketDataFieldService;


	@Disabled
	//only works on Chris's bloomberg terminal as these securities and fields are tied to his box
	public void testOverride() {
		ReferenceDataRequest pricingDataRequest = new ReferenceDataRequest("Sw_Market_Val", "Sw_Par_Cpn", "Sw_Cnv_Bpv", "Sw_Net_Acc_Int", "Sw_Cnv_Mod_Dur");
		pricingDataRequest.addSecurityList(Arrays.asList("SL322UHA Corp", "SL322UHE Corp", "SL322UHF Corp", "SL322UHG Corp"));
		pricingDataRequest.addOverride("Settle_Dt", "20111012");
		pricingDataRequest.addOverride("Sw_Curve_Dt", "20111012");
		ReferenceDataResponse response = this.bloombergDataService.getReferenceData(pricingDataRequest, true);
		response.getValues().forEach((securityIdentifier, fieldValueMap) ->
				System.out.println(securityIdentifier + " "
						+ fieldValueMap.get("Sw_Market_Val") + " "
						+ fieldValueMap.get("Sw_Par_Cpn") + " "
						+ fieldValueMap.get("Sw_Cnv_Bpv") + " "
						+ fieldValueMap.get("Sw_Net_Acc_Int") + " "
						+ fieldValueMap.get("Sw_Cnv_Mod_Dur"))
		);
	}


	// This test expects a Map<String, Object> because the Map<Date, Object> private member of the HistoricalDataResponse
	// class does not deserialize correctly (date conversion to a string in the JSON causes the deserialization to fail
	// when attempting to bind back to a date - custom deserializer could be used as an option as well, or change the
	// Map representation in the HistoricalDataResponse object to Map<String, ReferenceDataResponse>
	@Test
	public void testHistoricalDataRequest() throws BloombergRequestException {
		HistoricalDataRequest historicalDataRequest = new HistoricalDataRequest("OPEN", "PX_LAST");
		historicalDataRequest.setSecurity("LCQ12 " + BloombergMarketSectors.COMMODITY.getKeyName());
		historicalDataRequest.setStartDate(DateUtils.toDate("03/28/2011"));
		historicalDataRequest.setEndDate(DateUtils.toDate("03/29/2011"));
		historicalDataRequest.setPeriodicitySelection(BloombergPeriodicity.DAILY);

		Map<String, ReferenceDataResponse> response = this.bloombergDataService.getHistoricalData(historicalDataRequest);
		Assertions.assertEquals(2, response.size());
		response.forEach((dateString, dateReferenceDataResponse) -> dateReferenceDataResponse.getValues().forEach((securityIdentifier, fieldValueMap) -> {
			System.out.println(DateUtils.fromDateShort(DateUtils.toDate(dateString, DateUtils.FIX_DATE_FORMAT_INPUT))
					+ " " + securityIdentifier + " " + fieldValueMap.get("OPEN") + " " + fieldValueMap.get("PX_LAST"));
		}));
	}


	@Test
	public void testClientError() {
		ReferenceDataRequest pricingDataRequest = new ReferenceDataRequest("BID", "PX_LAST", "ASK", "ID_CUSIP", "TICKER");
		pricingDataRequest.addSecurityList(Arrays.asList("IBM Equity", "AAPL Equity", "BSX Equity", "USB Equity"));
		pricingDataRequest.setServiceUrl("");
		try {
			this.bloombergDataService.getReferenceData(pricingDataRequest, false);
			Assertions.fail("Should have thrown exception");
		}
		catch (Exception ex) {
			String errorMessage = ex.getCause() != null && ex.getCause().getCause() != null ? ex.getCause().getCause().getMessage() : ex.getMessage();
			Assertions.assertTrue(errorMessage.contains("Failed to open [] service url.") || errorMessage.contains("Timed out"));
		}

		// do another request the will succeed to clear the error count
		pricingDataRequest = new ReferenceDataRequest("BID", "PX_LAST", "ASK", "ID_CUSIP", "TICKER");
		pricingDataRequest.addSecurityList(Arrays.asList("IBM Equity", "AAPL Equity", "BSX Equity", "USB Equity"));
		ReferenceDataResponse response = this.bloombergDataService.getReferenceData(pricingDataRequest, false);
		Assertions.assertNull(response.getErrorMessage());
	}


	@Test
	public void testBloombergHistoricalLookUp() {
		HistoricalDataRequest pricingDataRequest = new HistoricalDataRequest("PX_BID", "PX_ASK");
		pricingDataRequest.setEndDate(DateUtils.toDate("06/27/2013"));
		pricingDataRequest.setStartDate(DateUtils.toDate("06/27/2013"));
		pricingDataRequest.setSecurity("IBM Equity");

		Map<String, ReferenceDataResponse> response = this.bloombergDataService.getHistoricalData(pricingDataRequest);
		Assertions.assertEquals(response.size(), 1);
		response.forEach((dateString, dateReferenceDataResponse) -> {
			Date date = DateUtils.toDate(dateString, DateUtils.FIX_DATE_FORMAT_INPUT);
			dateReferenceDataResponse.getValues().forEach((securityIdentifier, fieldValueMap) -> {
				Assertions.assertEquals("IBM Equity", securityIdentifier);
				Assertions.assertTrue(fieldValueMap.containsKey("PX_BID"));
				Assertions.assertTrue(fieldValueMap.containsKey("PX_ASK"));
			});
		});
	}


	@Test
	public void testRetrieveMidPriceFromDatabaseForJDUSOption() {
		try {
			MarketDataValueHolder valueHolder = this.marketDataFieldService.getMarketDataValueForDateNormalized(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("JD US 12/20/14 P25").getId(), DateUtils.toDate("10/28/2014"), "Mid Price");
			Assertions.assertTrue(CompareUtils.isEqual(new BigDecimal("2.300000000000000"), valueHolder.getMeasureValue()));
		}
		catch (Exception e) {
			e.printStackTrace();
			Assertions.fail(e.getLocalizedMessage());
		}
	}


	@Test
	public void testPriceClient() throws BloombergRequestException {
		ReferenceDataRequest pricingDataRequest = new ReferenceDataRequest("BID", "PX_LAST", "ASK", "ID_CUSIP", "TICKER");
		pricingDataRequest.addSecurityList(Collections.singletonList("IBM Equity"));

		ReferenceDataResponse response = this.bloombergDataService.getReferenceData(pricingDataRequest, true);
		Assertions.assertEquals(1, response.getValues().size());
		response.getValues().forEach((securityIdentifier, fieldValueMap) -> {
			System.out.println(securityIdentifier + " "
					+ fieldValueMap.get("BID") + " "
					+ fieldValueMap.get("PX_LAST") + " "
					+ fieldValueMap.get("ASK") + " "
					+ fieldValueMap.get("ID_CUSIP") + " "
					+ fieldValueMap.get("TICKER"));
			Assertions.assertEquals("IBM Equity", securityIdentifier);
			Assertions.assertEquals("IBM", fieldValueMap.get("TICKER"));
		});
	}
}
