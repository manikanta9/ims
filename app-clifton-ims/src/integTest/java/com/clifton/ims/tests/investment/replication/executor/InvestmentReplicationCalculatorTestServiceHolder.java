package com.clifton.ims.tests.investment.replication.executor;


import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import com.clifton.system.bean.SystemBeanService;


/**
 * The <code>InvestmentReplicationCalculatorTestServiceHolder</code> holds the service objects that are required by the {@link InvestmentReplicationCalculatorTestExecutor}.
 *
 * @author michaelm
 */
public class InvestmentReplicationCalculatorTestServiceHolder {

	private CalendarSetupService calendarSetupService;
	private InvestmentInstrumentService investmentInstrumentService;
	private SystemBeanService systemBeanService;
	private InvestmentReplicationService investmentReplicationService;
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	private InvestmentReplicationHistoryService investmentReplicationHistoryService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public InvestmentReplicationHistoryRebuildService getInvestmentReplicationHistoryRebuildService() {
		return this.investmentReplicationHistoryRebuildService;
	}


	public void setInvestmentReplicationHistoryRebuildService(InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService) {
		this.investmentReplicationHistoryRebuildService = investmentReplicationHistoryRebuildService;
	}


	public InvestmentReplicationHistoryService getInvestmentReplicationHistoryService() {
		return this.investmentReplicationHistoryService;
	}


	public void setInvestmentReplicationHistoryService(InvestmentReplicationHistoryService investmentReplicationHistoryService) {
		this.investmentReplicationHistoryService = investmentReplicationHistoryService;
	}
}
