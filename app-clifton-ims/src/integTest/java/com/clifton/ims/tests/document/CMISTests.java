package com.clifton.ims.tests.document;

import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @author stevenf
 */
public class CMISTests extends BaseImsIntegrationTest {

	@Resource
	DocumentManagementService documentManagementService;


	@Test
	public void testGetDocumentRecord() {
		Map<String, String> userPassMap = new LinkedHashMap<>();
		userPassMap.put("imstestuser1", "password1");
		userPassMap.put("imsdocumenttestuser", "test_pw2");
		for (Map.Entry<String, String> stringStringEntry : userPassMap.entrySet()) {
			long time = System.currentTimeMillis();
			this.userUtils.switchUser(stringStringEntry.getKey(), stringStringEntry.getValue());
			DocumentRecord documentRecord = this.documentManagementService.getDocumentRecord("BusinessContract", 704);
			Assertions.assertNotNull(documentRecord);
		}
	}
}
