package com.clifton.ims.tests.integration.export;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.ExportDefinitionType;
import com.clifton.export.run.runner.ExportRunnerService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.integration.export.ftp.FakeFtpServerUtils;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.filesystem.FileEntry;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;


public class JPMorganExportTests extends BaseImsIntegrationTest {

	//Ports under 1000 on linux can only be bound to as root
	private static final Integer FTP_CONTROL_PORT = 2021;
	private static final String FTP_USERNAME = "testUsername";
	private static final String FTP_PASSWORD = "testPassword";
	private static final String FTP_REMOTE_FOLDER = "/";
	private static final Integer FTP_POLL_SLEEP_TIME = 1000;
	private static final Integer FTP_POLL_LIMIT_COUNT = 60;

	private static final Date BOEING_FUTURES_TRADE_DATE = DateUtils.toDate("10/31/2014");
	private static final String EXPECTED_JPM_FUTURES_FILE_NAME = "Boeing_Futures_10-31-2014.csv";

	@Resource
	private BusinessCompanyService businessCompanyService;
	@Resource
	private ExportDefinitionService exportDefinitionService;
	@Resource
	private ExportRunnerService exportRunnerService;
	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private FakeFtpServerUtils fakeFtpServerUtils;


	@Test
	public void fullFtpIntegrationTest() throws IOException, InterruptedException {
		ExportDefinitionType opsType = this.exportDefinitionService.getExportDefinitionTypeByName("Operations");
		String filenameTemplate = "CLIFTON_TRADE_IM_${DATE?string(\"yyyyMMddHHmm\")}01.csv";

		//Create the Export Definition
		ExportDefinition exportDefinition = new ExportDefinition();
		exportDefinition.setName("JPM Integration Test - " + RandomUtils.randomNumber());
		exportDefinition.setType(opsType);
		this.exportDefinitionService.saveExportDefinition(exportDefinition);

		//Set the Destination
		createFtpDestination(exportDefinition);

		//Set the Contents
		createContent("Boeing DB", BOEING_FUTURES_TRADE_DATE, filenameTemplate, exportDefinition);

		//Start the FTP server
		FakeFtpServer fakeFtpServer = startFtpServer();

		try {
			//Send the export
			this.exportRunnerService.runManualExport(exportDefinition.getId());

			Assertions.assertTrue(this.fakeFtpServerUtils.serverHasFiles(fakeFtpServer, FTP_REMOTE_FOLDER, FTP_POLL_LIMIT_COUNT, FTP_POLL_SLEEP_TIME));

			@SuppressWarnings({"cast", "unchecked"})
			List<FileEntry> files = (List<FileEntry>) fakeFtpServer.getFileSystem().listFiles("/");
			Assertions.assertEquals(1, CollectionUtils.getSize(files));
			FileEntry fileEntry = files.get(0);

			InputStream actualFileInputStream = fileEntry.createInputStream();

			InputStream expectedFileInputStream = getClass().getResourceAsStream(EXPECTED_JPM_FUTURES_FILE_NAME);

			BufferedReader expectedReader = new BufferedReader(new InputStreamReader(expectedFileInputStream));
			BufferedReader actualReader = new BufferedReader(new InputStreamReader(actualFileInputStream));

			String expectedLine;
			String actualLine;
			int lineNum = 0;
			while ((expectedLine = expectedReader.readLine()) != null) {
				actualLine = actualReader.readLine();

				/*
				 * Assert that the line from the actual file equals the corresponding line of the expected file. For now, we skip the third line
				 * because it contains values that will be different each time (dates, times, etc.)
				 */
				if (lineNum != 2) {
					Assertions.assertEquals(expectedLine, actualLine);
				}
				lineNum++;
			}
		}
		//Cleanup
		finally {
			fakeFtpServer.stop();
		}
	}


	private FakeFtpServer startFtpServer() {
		FakeFtpServer fakeFtpServer = this.fakeFtpServerUtils.createAndInitializeFakeFtpServer(FTP_USERNAME, FTP_PASSWORD, null);
		fakeFtpServer.setServerControlPort(FTP_CONTROL_PORT);
		fakeFtpServer.start();
		return fakeFtpServer;
	}


	private ExportDefinitionDestination createFtpDestination(ExportDefinition exportDefinition) throws UnknownHostException {
		return new ExportFtpDestinationBuilder(InetAddress.getLocalHost().getHostName(), this.systemBeanService, this.exportDefinitionService)
				.exportDefinition(exportDefinition)
				.ftpRemoteFolder(FTP_REMOTE_FOLDER)
				.ftpPort(FTP_CONTROL_PORT)
				.ftpProtocol("FTP")
				.ftpUsername(FTP_USERNAME)
				.ftpPassword(FTP_PASSWORD)
				.build();
	}


	private ExportDefinitionContent createContent(String accountGroupName, Date tradeDate, String filenameTemplate, ExportDefinition exportDefinition) {
		InvestmentAccountGroup accountGroup = this.investmentAccountGroupService.getInvestmentAccountGroupByName(accountGroupName);
		BusinessCompany jpMorgan = this.businessCompanyService.getBusinessCompanyByTypeAndName("Custodian", "JP Morgan Chase");
		int daysBack = DateUtils.getDaysDifference(new Date(), tradeDate);

		SystemBean contentSystemBean = SystemBeanBuilder.newBuilder(null, "Trade Export Generator", this.systemBeanService)
				.addProperty("File Format Type", "JP_MORGAN")
				.addProperty("Client Account Group", accountGroup.getId() + "")
				.addProperty("Custodian", jpMorgan.getId() + "")
				.addProperty("Environment", "PROD")
				.addProperty("Date generation option", "DAYS_BACK")
				.addProperty("Days from today", daysBack + "")
				.addProperty("Reconciled Only", "true")
				.build();

		ExportDefinitionContent content = new ExportDefinitionContent();
		content.setContentSystemBean(contentSystemBean);
		content.setFileNameTemplate(filenameTemplate);
		content.setDefinition(exportDefinition);
		this.exportDefinitionService.saveExportDefinitionContent(content);

		return content;
	}
}
