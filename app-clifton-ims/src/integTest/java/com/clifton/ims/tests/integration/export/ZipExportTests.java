package com.clifton.ims.tests.integration.export;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.dataaccess.file.container.FileContainerNative;
import com.clifton.core.security.ServiceAuthenticationHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.ExportDefinitionType;
import com.clifton.export.run.runner.ExportRunnerService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.integration.export.ftp.FakeFtpServerUtils;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;


public class ZipExportTests extends BaseImsIntegrationTest {

	//Ports under 1000 cannot be bound to by non-root users in linux
	private static final Integer FTP_CONTROL_PORT = 4021;
	private static final String FTP_USERNAME = "testUsername";
	private static final String FTP_PASSWORD = "testPassword";
	private static final String FTP_REMOTE_FOLDER = "/";
	private static final Integer FTP_POLL_SLEEP_TIME = 1000;
	private static final Integer FTP_POLL_LIMIT_COUNT = 60;

	private static final String EXPORT_DEFINITION_TYPE_NAME = "Trading";

	private static final String FILENAME_PREFIX = "PREFIX_";

	//This is a shared network location where files will be placed for pickup by the integration server
	private static final String FILE_BASE_PATH = "//MSP-700-03.paraport.com/Development/Integration Testing/temp/";

	private static final String FILENAME_TEMPLATE_1 = String.format("%.0f", Math.random() * 1000000000) + "CLIFTON_EXPORT_01.txt";
	private static final String FILE_CONTENT_1 = "test content 1";

	private static final String FILENAME_TEMPLATE_2 = String.format("%.0f", Math.random() * 1000000000) + "CLIFTON_EXPORT_02.txt";
	private static final String FILE_CONTENT_2 = "test content 2";

	private static final String FILENAME_TEMPLATE_ZIP = "test.zip";
	private static final String FILENAME_TEMP_ZIP_DIR = "ZipTest";
	private static final String EXPECTED_FILENAME_TEMPLATE_ZIP = FILENAME_PREFIX + "test.zip";


	@Resource
	private ExportDefinitionService exportDefinitionService;

	@Resource
	private ExportRunnerService exportRunnerService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private FakeFtpServerUtils fakeFtpServerUtils;

	/**
	 * Additional context configuration necessary to write to a remote location. The following Components need to be defined in the context.
	 */
	@Configuration
	static class ZipExportTestConfiguration {

		@Autowired
		private Environment environment;


		@Bean
		public FileContainerFactory fileContainerFactory() {
			return new FileContainerFactory();
		}


		@Bean
		public ServiceAuthenticationHandler serviceAuthenticationHandler() {
			ServiceAuthenticationHandler handler = new ServiceAuthenticationHandler();
			handler.setServiceDomain(this.environment.getProperty("application.smb.domain"));
			handler.setServiceUsername(this.environment.getProperty("application.smb.username"));
			handler.setServicePassword(this.environment.getProperty("application.smb.password"));
			return handler;
		}
	}

	/*
	OBJECTIVES:
	1. Verify the file is received
	2. Verify the name of the zip file
	3. Verify the name of each file inside the zip
	4. Verify the contents of each file inside the zip
	 */


	@Test
	public void fullZipExportIntegrationTest() throws IOException, InterruptedException {
		ExportDefinitionType exportDefType = this.exportDefinitionService.getExportDefinitionTypeByName(EXPORT_DEFINITION_TYPE_NAME);

		//First Content File
		FileContainer expectedFile1 = FileContainerFactory.getFileContainer(FILE_BASE_PATH + FILENAME_TEMPLATE_1);
		FileUtils.writeStringToFile(expectedFile1, FILE_CONTENT_1);

		//Second Content File
		FileContainer expectedFile2 = FileContainerFactory.getFileContainer(FILE_BASE_PATH + FILENAME_TEMPLATE_2);
		FileUtils.writeStringToFile(expectedFile2, FILE_CONTENT_2);

		//Create the Export Definition
		ExportDefinition exportDefinition = new ExportDefinition();
		exportDefinition.setName("Zip Export Integration Test - " + RandomUtils.randomNumber());
		exportDefinition.setType(exportDefType);
		exportDefinition.setZipFilesForExport(true);
		exportDefinition.setZippedFileNameTemplate(FILENAME_TEMPLATE_ZIP);
		this.exportDefinitionService.saveExportDefinition(exportDefinition);

		//Set the Destination
		createFtpDestination(exportDefinition);

		//Set the Contents
		createContent(FILENAME_TEMPLATE_1, expectedFile1.getAbsolutePath(), exportDefinition);
		createContent(FILENAME_TEMPLATE_2, expectedFile2.getAbsolutePath(), exportDefinition);

		//Start the FTP server
		FakeFtpServer fakeFtpServer = startFtpServer();

		try {
			//Send the export
			this.exportRunnerService.runManualExport(exportDefinition.getId());

			Assertions.assertTrue(this.fakeFtpServerUtils.serverHasFiles(fakeFtpServer, FTP_REMOTE_FOLDER, FTP_POLL_LIMIT_COUNT, FTP_POLL_SLEEP_TIME));

			@SuppressWarnings({"cast", "unchecked"})
			List<FileEntry> ftpFiles = (List<FileEntry>) fakeFtpServer.getFileSystem().listFiles(FTP_REMOTE_FOLDER);

			//There should be 1 zip file in the root of the FTP server
			Assertions.assertEquals(1, CollectionUtils.getSize(ftpFiles));

			//Get the zip file
			FileEntry zipFileEntry = ftpFiles.get(0);

			List<FileWrapper> actualUnzippedFiles = unzipFiles(zipFileEntry);

			//There should be two files that get extracted from the zip file
			Assertions.assertEquals(2, CollectionUtils.getSize(actualUnzippedFiles));

			FileContainer actualFile1 = FileContainerFactory.getFileContainer(actualUnzippedFiles.get(0).getFile());
			FileContainer actualFile2 = FileContainerFactory.getFileContainer(actualUnzippedFiles.get(1).getFile());

			if (actualFile1.getName().equals(FILENAME_TEMPLATE_2)) {
				actualFile1 = FileContainerFactory.getFileContainer(actualUnzippedFiles.get(1).getFile());
				actualFile2 = FileContainerFactory.getFileContainer(actualUnzippedFiles.get(0).getFile());
			}

			//Verify the file names are as expected
			Assertions.assertTrue(StringUtils.startsWith(actualFile1.getName(), FileUtils.getFileNameWithoutExtension(FILENAME_TEMPLATE_1)));
			Assertions.assertTrue(StringUtils.startsWith(actualFile2.getName(), FileUtils.getFileNameWithoutExtension(FILENAME_TEMPLATE_2)));

			//Compare the file contents
			compareFileContents(expectedFile1, (FileContainerNative) actualFile1);
			compareFileContents(expectedFile2, (FileContainerNative) actualFile2);
		}
		//Cleanup
		finally {
			fakeFtpServer.stop();
		}
	}


	private List<FileWrapper> unzipFiles(FileEntry zipFileEntry) throws IOException {
		//Actual Location for Zip File
		Path zipFileLocation = Files.createTempDirectory(FILENAME_TEMP_ZIP_DIR);

		//Verify the name of the zip file
		Assertions.assertEquals(EXPECTED_FILENAME_TEMPLATE_ZIP, zipFileEntry.getName());

		//Unzip the file
		InputStream zipFileInputStream = zipFileEntry.createInputStream();
		File actualZipFile = FileUtils.convertInputStreamToFile(zipFileLocation.toAbsolutePath().toString().concat("/".concat(FILENAME_TEMPLATE_ZIP)), zipFileInputStream);


		List<FileWrapper> fileList = ZipUtils.unZipFiles(actualZipFile);

		//Cleanup
		actualZipFile.delete();
		zipFileLocation.toFile().delete();

		return fileList;
	}


	private void compareFileContents(FileContainer expected, File actual) throws IOException {
		String actualLine;
		String expectedLine;

		try (InputStream expectedInputStream = expected.getInputStream();
		     InputStream actualInputStream = new FileInputStream(actual);

		     BufferedReader expectedReader = new BufferedReader(new InputStreamReader(expectedInputStream));
		     BufferedReader actualReader = new BufferedReader(new InputStreamReader(actualInputStream))) {

			while ((expectedLine = expectedReader.readLine()) != null) {
				actualLine = actualReader.readLine();

				//The lines between the two files should match
				Assertions.assertEquals(expectedLine, actualLine);
			}
		}
		finally {
			//Cleanup
			expected.deleteFile();
			actual.delete();
		}
	}


	private FakeFtpServer startFtpServer() {
		FakeFtpServer fakeFtpServer = this.fakeFtpServerUtils.createAndInitializeFakeFtpServer(FTP_USERNAME, FTP_PASSWORD, null);
		fakeFtpServer.setServerControlPort(FTP_CONTROL_PORT);
		fakeFtpServer.start();
		return fakeFtpServer;
	}


	private ExportDefinitionDestination createFtpDestination(ExportDefinition exportDefinition) throws UnknownHostException {
		return new ExportFtpDestinationBuilder(InetAddress.getLocalHost().getHostName(), this.systemBeanService, this.exportDefinitionService)
				.exportDefinition(exportDefinition)
				.ftpRemoteFolder(FTP_REMOTE_FOLDER)
				.ftpPort(FTP_CONTROL_PORT)
				.ftpProtocol("FTP")
				.ftpUsername(FTP_USERNAME)
				.ftpPassword(FTP_PASSWORD)
				.filenamePrefix(FILENAME_PREFIX)
				.build();
	}


	private ExportDefinitionContent createContent(String filenameTemplate, String fileLocation, ExportDefinition exportDefinition) {
		SystemBean contentSystemBean = SystemBeanBuilder.newBuilder(null, "Network File Export Generator", this.systemBeanService)
				.addProperty("File Location", fileLocation)
				.build();

		ExportDefinitionContent content = new ExportDefinitionContent();
		content.setContentSystemBean(contentSystemBean);
		content.setFileNameTemplate(filenameTemplate);
		content.setDefinition(exportDefinition);
		this.exportDefinitionService.saveExportDefinitionContent(content);

		return content;
	}
}
