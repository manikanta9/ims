package com.clifton.ims.tests.performance.composite.account.calculators;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>PerformanceCompositeInvestmentAccountProcessServiceTests</code> to test various composite
 * account monthly period return calculators.
 *
 * @author apopp
 */

public class PerformanceCompositeInvestmentAccountProcessServiceTests extends BaseImsIntegrationTest {

	@Resource
	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;

	@Resource
	private PerformanceCompositeSetupService performanceCompositeSetupService;


	@Test
	public void testInterestIncomeCalculation() {
		PerformanceCompositeInvestmentAccountDailyPerformance previousMonthEndValue = getDailyPerformanceForDate("10/30/2015", "689180");
		PerformanceCompositeInvestmentAccountDailyPerformance beginningValue = getDailyPerformanceForDate("11/2/2015", "689180");
		PerformanceCompositeInvestmentAccountDailyPerformance endOfMonthValue = getDailyPerformanceForDate("11/30/2015", "689180");

		BigDecimal result = MathUtils.subtract(endOfMonthValue.getPositionValue(), previousMonthEndValue.getPositionValue());
		ValidationUtils.assertTrue(MathUtils.isEqual(result, new BigDecimal("135983.53")), "Expected [135983.53] but got [" + result + "] for account [689180]");
		ValidationUtils.assertTrue(MathUtils.isEqual(beginningValue.getGainLoss(), new BigDecimal("96433.06")), "Expected [96433.06] but got [" + beginningValue.getGainLoss() + "] for account [689180]");
		ValidationUtils.assertTrue(MathUtils.isEqual(endOfMonthValue.getGainLoss(), new BigDecimal("-39581.17")), "Expected [-39581.17] but got [" + endOfMonthValue.getGainLoss() + "] for account [689180]");
	}


	@Test
	public void testDecemberInterestIncome456606() {
		PerformanceCompositeInvestmentAccountDailyPerformance previousMonthEndValue = getDailyPerformanceForDate("11/30/2015", "456606");
		PerformanceCompositeInvestmentAccountDailyPerformance beginningValue = getDailyPerformanceForDate("12/1/2015", "456606");
		PerformanceCompositeInvestmentAccountDailyPerformance endOfMonthValue = getDailyPerformanceForDate("12/31/2015", "456606");

		PerformanceCompositeInvestmentAccountDailyPerformance tenth = getDailyPerformanceForDate("12/10/2015", "456606");
		PerformanceCompositeInvestmentAccountDailyPerformance twentyThird = getDailyPerformanceForDate("12/23/2015", "456606");

		ValidationUtils.assertTrue(MathUtils.isEqual(tenth.getGainLoss(), new BigDecimal("285711.29")), "Expected [285711.29] but got [" + tenth.getGainLoss() + "] for account [456606]");
		ValidationUtils.assertTrue(MathUtils.isEqual(twentyThird.getGainLoss(), new BigDecimal("241284.35")), "Expected [241284.35] but got [" + twentyThird.getGainLoss() + "] for account [456606]");

		BigDecimal result = MathUtils.subtract(MathUtils.add(endOfMonthValue.getPositionValue(), new BigDecimal("50000000")), previousMonthEndValue.getPositionValue());
		ValidationUtils.assertTrue(MathUtils.isEqual(result, new BigDecimal("1628379.27")), "Expected [1628379.27] but got [" + result + "] for account [456606]");
		ValidationUtils.assertTrue(MathUtils.isEqual(beginningValue.getGainLoss(), new BigDecimal("299295.23")), "Expected [299295.23] but got [" + beginningValue.getGainLoss() + "] for account [456606]");
		ValidationUtils.assertTrue(MathUtils.isEqual(endOfMonthValue.getGainLoss(), new BigDecimal("-134291.35")), "Expected [-134291.35] but got [" + endOfMonthValue.getGainLoss() + "] for account [456606]");
	}


	@Test
	public void testLastDayOfPreviousMonthPerformance() {
		PerformanceCompositeInvestmentAccountDailyPerformance previousMonthEndValue = getDailyPerformanceForDate("10/30/2015", "050905");
		PerformanceCompositeInvestmentAccountDailyPerformance previousMonthEndLastDayValue = getDailyPerformanceForDate("10/31/2015", "11/1/2015", "050905");

		ValidationUtils.assertTrue(MathUtils.isEqual(previousMonthEndValue.getPositionValue(), new BigDecimal("105579727.45")), "Expected [105579727.45] but got [" + previousMonthEndValue.getPositionValue() + "] for account [050905]");
		ValidationUtils.assertTrue(MathUtils.isEqual(previousMonthEndLastDayValue.getPositionValue(), new BigDecimal("105578516.54")), "Expected [105578516.54] but got [" + previousMonthEndLastDayValue.getPositionValue() + "] for account [050905]");
	}


	private PerformanceCompositeInvestmentAccountDailyPerformance getDailyPerformanceForDate(String dateStr, String accountNumber) {
		return getDailyPerformanceForDate(dateStr, dateStr, accountNumber);
	}


	private PerformanceCompositeInvestmentAccountDailyPerformance getDailyPerformanceForDate(String dateStr, String periodDateStr, String accountNumber) {
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
		AccountingPeriod accountingPeriod = this.accountingUtils.getAccountingPeriodForDate(DateUtils.toDate(periodDateStr));

		this.accountingUtils.rebuildAccountingPositionDailySnapshot(clientAccount.getId(), DateUtils.toDate(dateStr), accountingPeriod.getEndDate());

		List<PerformanceCompositeInvestmentAccount> accountAssignmentList = this.performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(clientAccount.getId(), accountingPeriod.getId());
		if (CollectionUtils.isEmpty(accountAssignmentList)) {
			throw new ValidationException("Cannot find performance composite assignment for account [" + accountNumber + "] and period [" + accountingPeriod.getLabel() + "].");
		}
		BeanUtils.sortWithFunction(accountAssignmentList, PerformanceCompositeInvestmentAccount::getCreateDate, true);
		PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount = CollectionUtils.getFirstElementStrict(accountAssignmentList);

		PerformanceCompositeAccountRebuildCommand command = new PerformanceCompositeAccountRebuildCommand();
		command.setMeasureDate(DateUtils.toDate(dateStr));
		command.setPerformanceCompositeInvestmentAccountId(performanceCompositeInvestmentAccount.getId());
		command.setToAccountingPeriod(accountingPeriod);
		command.setPreview(true);
		command.setSynchronous(true);
		return this.performanceCompositePerformanceRebuildService.previewPerformanceCompositeAccountDailyPerformanceRunForMeasureDate(command);
	}
}
