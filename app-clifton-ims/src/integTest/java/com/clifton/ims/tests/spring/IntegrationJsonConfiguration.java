package com.clifton.ims.tests.spring;

import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.converter.json.jackson.strategy.JacksonExternalServiceStrategy;
import com.clifton.core.security.oauth.SecurityOAuthUserTokenService;
import com.clifton.core.security.oauth.client.connection.SecurityOAuth2TokenServiceImpl;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.api.client.ExternalServiceInvocationHandler;
import com.clifton.security.oauth.SecurityOAuthUserTokenServiceImpl;
import com.clifton.security.user.SecurityUserServiceImpl;
import org.aopalliance.intercept.MethodInterceptor;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author StevenF
 */
@PropertySource({"classpath:META-INF/integration-tests.properties"})
@PropertySource({"classpath:META-INF/integration-tests-overrides.properties"})
@ComponentScan(basePackages = {"com.clifton.ims.tests.portal"})
public class IntegrationJsonConfiguration implements BeanFactoryAware, BeanClassLoaderAware {

	private ClassLoader classLoader;

	//Inject value using SpEL
	@Value("${external.service.endpoint.clientPortalApplication}")
	private String externalServiceURL;


	////////////////////////////////////////////////////////////////////////////
	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		bindServices((DefaultListableBeanFactory) beanFactory);
	}


	@Override
	public void setBeanClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}


	public ClassLoader getBeanClassLoader() {
		return this.classLoader;
	}

	////////////////////////////////////////////////////////////////////////////


	@Bean
	public JacksonHandlerImpl externalServiceJacksonHandler() {
		return new JacksonHandlerImpl(new JacksonExternalServiceStrategy());
	}


	@Bean
	public SecurityOAuthUserTokenService securityOAuthUserTokenService() {
		SecurityOAuthUserTokenServiceImpl securityOAuthUserTokenService = new SecurityOAuthUserTokenServiceImpl();
		securityOAuthUserTokenService.setSecurityOAuth2TokenService(new SecurityOAuth2TokenServiceImpl());
		securityOAuthUserTokenService.setSecurityUserService(new SecurityUserServiceImpl());
		return securityOAuthUserTokenService;
	}
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Reflectively and recursively searches for service implementations annotated with {@link Service} in the 'com.clifton' subpackages ending with
	 * 'ServiceImpl'. When such a class is found, a matching interface is looked up in the same package ending with 'Service'. If there is a match, then a
	 * dynamic proxy of that interface is created and bound.
	 */
	private void bindServices(DefaultListableBeanFactory beanFactory) {
		if (CollectionUtils.isEmpty(getServiceImplementationClassSet())) {
			throw new IllegalStateException("Found no Services to bind.");
		}

		Set<Class<?>> implementationClassSet = getServiceImplementationClassSet();

		Map<String, Class<?>> implementationNameToInterfaceClassMap = new HashMap<>();
		Map<String, Class<?>> interfaceNameToImplementationClassMap = new HashMap<>();
		for (Class<?> implementationClass : implementationClassSet) {
			Class<?> interfaceClass = ClassUtils.getInterfaceForServiceImplClass(implementationClass, false);
			if (interfaceClass != null) {
				interfaceNameToImplementationClassMap.put(interfaceClass.getName(), implementationClass);
				implementationNameToInterfaceClassMap.put(implementationClass.getName(), interfaceClass);
			}
		}

		ExternalServiceInvocationHandler invocationHandler = new ExternalServiceInvocationHandler();
		invocationHandler.setExternalServiceURL(getExternalServiceURL());
		invocationHandler.setInterfaceNameToImplementationClassMap(interfaceNameToImplementationClassMap);
		invocationHandler.setJacksonHandler(externalServiceJacksonHandler());
		invocationHandler.setSecurityOAuthUserTokenService(securityOAuthUserTokenService());

		for (Class<?> implementationClass : implementationClassSet) {
			Class<?> interfaceClass = implementationNameToInterfaceClassMap.get(implementationClass.getName());
			//Certain items won't map directly to an interface simple by removing the Impl
			//For now this is sufficient for my purposes though.
			//e.g com.clifton.swift.server.SwiftServerMessageLogServiceImpl
			if (interfaceClass != null) {
				Object proxy = createProxy(interfaceClass, invocationHandler);
				//avoid duplicates (e.g. SecurityAuthorizationSetupService in security and system projects
				if (!beanFactory.containsSingleton(interfaceClass.getName())) {
					beanFactory.registerSingleton(StringUtils.deCapitalize(interfaceClass.getSimpleName()), proxy);
				}
			}
		}
	}


	private Object createProxy(Class<?> serviceInterface, MethodInterceptor methodInterceptor) {
		ProxyFactory proxyFactory = new ProxyFactory(serviceInterface, methodInterceptor);
		return proxyFactory.getProxy(this.classLoader);
	}

	////////////////////////////////////////////////////////////////////////////


	public Set<Class<?>> getServiceImplementationClassSet() {
		return CollectionUtils.getStream(new Reflections("com.clifton.portal",
				new SubTypesScanner(false)).getSubTypesOf(Object.class))
				.filter(service -> service.getName().endsWith("ServiceImpl")).collect(Collectors.toSet());
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getExternalServiceURL() {
		return this.externalServiceURL;
	}


	public void setExternalServiceURL(String externalServiceURL) {
		this.externalServiceURL = externalServiceURL;
	}
}
