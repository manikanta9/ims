package com.clifton.ims.tests.rule.security;


import com.clifton.core.test.util.TestUtils;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.util.templates.ImsTestProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ComplianceRoleRuleSecurityTests extends SecurityRuleTests {

	private static final Logger log = LoggerFactory.getLogger(ComplianceRoleRuleSecurityTests.class);


	@Override
	public void switchUser() {
		String username = ImsTestProperties.TEST_COMPLIANCE_USER;
		String password = ImsTestProperties.TEST_COMPLIANCE_USER_PASSWORD;
		this.userUtils.switchUser(username, password);
	}


	@Override
	public void testIgnoreManualPortfolioRunViolation() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testIgnoreManualPortfolioRunViolation();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [WRITE] permission(s) to [Portfolio Runs] security resource.].]");
	}


	@Override
	public void testSaveManualPortfolioRunViolation() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveManualPortfolioRunViolation();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [WRITE] permission(s) to [Portfolio Runs] security resource.].]");
	}


	@Override
	public void testSaveAndDeletePortfolioRunRuleAssignment() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveAndDeletePortfolioRunRuleAssignment();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [CREATE] permission(s) to [Portfolio Runs] security resource.].]");
	}


	@Override
	public void testSaveAndDeleteTradeRuleAssignment() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveAndDeleteTradeRuleAssignment();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [CREATE] permission(s) to [Trade] security resource.].]");
	}


	@Override
	public void testSaveAndDeleteTradeRealTimeRuleAssignment() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveAndDeleteTradeRealTimeRuleAssignment();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [CREATE] permission(s) to [Trade] security resource.].]");
	}
}
