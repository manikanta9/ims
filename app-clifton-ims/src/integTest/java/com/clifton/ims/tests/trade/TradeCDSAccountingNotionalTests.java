package com.clifton.ims.tests.trade;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public class TradeCDSAccountingNotionalTests extends BaseImsIntegrationTest {

	private TradeType creditDefaultSwaps;

	private TradeType clearedCreditDefaultSwaps;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void beforeTest() {
		this.creditDefaultSwaps = this.tradeUtils.getTradeType(TradeType.CREDIT_DEFAULT_SWAPS);
		this.clearedCreditDefaultSwaps = this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS);
	}


	/**
	 * Sample trade 684097
	 */
	@Test
	public void testSellProtectionUnder100() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurityFromTemplate("70634224", "70634224_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BOA_CUSTODIAN, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.creditDefaultSwaps);
		swap.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		this.investmentInstrumentService.saveInvestmentSecurity(swap);


		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		// Create and execute a buy trade to open a position
		BigDecimal notional = new BigDecimal("11200000");
		Date buyDate = DateUtils.toDate("4/25/2017");
		BigDecimal openPrice = new BigDecimal("98.95");
		Trade buy = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, notional, BigDecimal.ONE, false, openPrice, buyDate, executingBroker, accountInfo)
				.withExchangeRate(new BigDecimal("1")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		Trade trade = this.tradeService.getTrade(buy.getId());

		Assertions.assertEquals((new BigDecimal("117600.00")), trade.getAccountingNotional());
	}


	/**
	 * Sample trade 684941
	 */
	@Test
	public void testSellProtectionOver100() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurityFromTemplate("XI2801J22U0100XXI", "XI2801J22U0100XXI_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, BusinessCompanies.NORTHERN_TRUST, this.clearedCreditDefaultSwaps);


		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		// Create and execute a buy trade to open a position
		BigDecimal notional = new BigDecimal("4000000.00");
		Date buyDate = DateUtils.toDate("4/28/2017");
		BigDecimal openPrice = new BigDecimal("101.730892");
		Trade buy = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, notional, BigDecimal.ONE, false, openPrice, buyDate, executingBroker, accountInfo)
				.withExchangeRate(new BigDecimal("1")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		Trade trade = this.tradeService.getTrade(buy.getId());

		Assertions.assertEquals((new BigDecimal("-69235.68")), trade.getAccountingNotional());
	}


	/**
	 * Sample trade 674647
	 */
	@Test
	public void testBuyProtectionUnder100() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurityFromTemplate("XI2601J26U0100XXI", "XI2601J26U0100XXI_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.clearedCreditDefaultSwaps);


		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BNP_PARIBAS);

		// Create and execute a buy trade to open a position
		BigDecimal notional = new BigDecimal("109900000.00");
		Date buyDate = DateUtils.toDate("03/27/2017");
		BigDecimal openPrice = new BigDecimal("99.45131571");
		Trade buy = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, notional, BigDecimal.ONE, true, openPrice, buyDate, executingBroker, accountInfo)
				.withExchangeRate(new BigDecimal("1")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		Trade trade = this.tradeService.getTrade(buy.getId());

		Assertions.assertEquals((new BigDecimal("-603004.03")), trade.getAccountingNotional());
	}


	/**
	 * Sample trade 674638
	 */
	@Test
	public void testBuyProtectionOver100() {
		int i = 5;
		InvestmentSecurity swap = null;
		while (i < 100 && swap == null) {
			// 5th and 6th digits of security symbol below update every time there is a credit event so search for current credit event number
			String symbol = "XY27" + String.format("%02d", i) + "D21U0500XXI";
			swap = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, null);
			i++;
		}
		ValidationUtils.assertNotNull(swap, "Unable to find security with symbol XY27**D21U0500XXI to copy.");
		swap = this.investmentInstrumentUtils.getCopyOfSecurityFromTemplate(swap.getSymbol(), swap.getSymbol() + "_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));


		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, BusinessCompanies.NORTHERN_TRUST, this.clearedCreditDefaultSwaps);


		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		// Create and execute a buy trade to open a position
		BigDecimal notional = new BigDecimal("282828000.00");
		Date buyDate = DateUtils.toDate("03/27/2017");
		BigDecimal openPrice = new BigDecimal("107.1525");
		Trade buy = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, notional, BigDecimal.ONE, true, openPrice, buyDate, executingBroker, accountInfo)
				.withExchangeRate(new BigDecimal("1"))
				.withCurrentFactor(new BigDecimal("0.99"))
				.withQuantityIntended(new BigDecimal("279999720.00")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		Trade trade = this.tradeService.getTrade(buy.getId());

		Assertions.assertEquals((new BigDecimal("20026979.97")), trade.getAccountingNotional());
	}
}
