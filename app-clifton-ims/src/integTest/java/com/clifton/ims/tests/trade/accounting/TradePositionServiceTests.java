package com.clifton.ims.tests.trade.accounting;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.retriever.InvestmentSecurityEventRetrieverService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentRatesService;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.test.util.RandomUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.accounting.position.TradePositionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class TradePositionServiceTests extends BaseImsIntegrationTest {

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private InvestmentRatesService investmentRatesService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;
	@Resource
	private InvestmentSecurityEventRetrieverService investmentSecurityEventRetrieverService;

	@Resource
	private TradeService tradeService;

	@Resource
	private TradePositionService tradePositionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccruedInterest_Bonds() {
		// T-Bills
		assertAccruedInterestEquals(335_857, null, true);

		// T-Notes: buy and sell
		assertAccruedInterestEquals(335_451, new BigDecimal("24968.99"), true);
		assertAccruedInterestEquals(334_493, new BigDecimal("159041.61"), true);

		// ABS: buy and sell
		assertAccruedInterestEquals(331_152, new BigDecimal("1897.65"));
		assertAccruedInterestEquals(328_222, new BigDecimal("235.18"));

		// TIPS: buy and sell
		assertAccruedInterestEquals(330_327, new BigDecimal("64.80"), true);
		assertAccruedInterestEquals(329_037, new BigDecimal("1127.02"), true);

		// Linkers
		assertAccruedInterestEquals(336_066, new BigDecimal("2066.67"), true);
	}


	@Test
	public void testAccruedInterest_CDS() {
		assertAccruedInterestEquals(334_050, new BigDecimal("89000.00")); // buy with price > 100
		assertAccruedInterestEquals(312_913, new BigDecimal("93074.69")); // buy with price < 100

		assertAccruedInterestEquals(334_052, new BigDecimal("-39308.33")); // sell with price > 100
		assertAccruedInterestEquals(321_295, new BigDecimal("-1166.67")); // sell with price < 100
	}


	@Test
	public void testAccruedInterest_IRS() {
		// buy and sell
		assertAccruedInterestEquals(310_131, new BigDecimal("-1936.91")); // + 2 week days and if holiday find next non-holiday
		assertAccruedInterestEquals(393_180, new BigDecimal("963921.50"));
		assertAccruedInterestEquals(393_108, new BigDecimal("79811.62"));
		assertAccruedInterestEquals(418_964, new BigDecimal("84683.33")); // skip holiday

		assertAccruedInterestEquals(710_961, new BigDecimal("643549.31")); // full close: Pay Fixed and Receive Floating Leg
	}


	@Test
	public void testAccruedInterest_TRS() {
		// TRS accrual is based on closing notional: try to use partial close trades (half or less) so that there's still notional left to be closed
		// for cases when this is not feasible, override trade/settlement dates to 1 day earlier in order to be able to close position before it's closed by the trade and reset
		assertAccruedInterestEquals(676_485, new BigDecimal("15352.89"), DateUtils.toDate("03/30/2017"), DateUtils.toDate("04/04/2017")); // buy to close with positive coupon: receiving
		assertAccruedInterestEquals(638_953, new BigDecimal("682.03"), DateUtils.toDate("12/29/2016"), DateUtils.toDate("01/04/2017")); // buy to close with positive coupon: receiving
		assertAccruedInterestEquals(459_951, new BigDecimal("-871.82")); // buy to close with negative coupon: paying – 6/23/15 TD
		assertAccruedInterestEquals(658_996, new BigDecimal("-14768.73"), DateUtils.toDate("02/27/2017"), DateUtils.toDate("03/02/2017")); // sell to close with negative coupon: paying

		// older tests: no counterparty data
		assertAccruedInterestEquals(305_235, new BigDecimal("-369.47"), null, DateUtils.toDate("08/04/2013")); // sell to close with positive coupon: paying, override settlement date to get correct opening position when calculating accrual
		assertAccruedInterestEquals(305_234, new BigDecimal("986.22"), null, DateUtils.toDate("08/04/2013")); // sell to close with negative coupon: receiving, override settlement date to get correct opening position when calculating accrual
		assertAccruedInterestEquals(337_071, null); // buy to open: no accrual on opening of TRS
		assertAccruedInterestEquals(376_260, new BigDecimal("2281.27"), DateUtils.toDate("07/08/2014"), DateUtils.toDate("07/11/2014")); // buy to close with positive coupon: receiving

		// make sure accrual valuation is using opening position settlement date when AccrualStartConventions.POSITION_OPEN_SETTLEMENT is used for the hierarchy
		assertAccruedInterestEquals(1_979_323, new BigDecimal("46310.55"), DateUtils.toDate("05/24/2019"), DateUtils.toDate("05/29/2019"), true);
		// multiple lots
		Trade copiedTrade = this.tradeUtils.copyTrade(1_979_323, new BigDecimal("100"), DateUtils.toDate("05/01/2019"), true);
		try {
			this.tradeUtils.fullyExecuteTrade(copiedTrade);
			assertAccruedInterestEquals(copiedTrade.getId(), new BigDecimal("46327.08"), DateUtils.toDate("05/24/2019"), DateUtils.toDate("05/29/2019"), true, new BigDecimal("196989"));
		}
		finally {
			this.tradeUtils.unbookAndUnpostTrade(copiedTrade);
			this.tradeUtils.cancelTrade(copiedTrade);
		}
	}


	@Test
	public void testAccruedInterest_TRS_2DayFixingCycle_dailyRate() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.JP_MORGAN_CHASE_BANK_NA, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.tradeUtils.getTradeType(TradeType.TOTAL_RETURN_SWAPS));

		InvestmentInterestRateIndex sofrRateIndex = this.investmentRatesService.getInvestmentInterestRateIndexBySymbol("SOFRRATE");
		InvestmentSecurity trsSecurity = this.investmentInstrumentUtils.getCopyOfSecurity("2181415591", "2181415591_" + RandomUtils.randomNumber());

		trsSecurity.setBusinessCompany(accountInfo.getHoldingCompany());
		trsSecurity.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		trsSecurity.setAccrualMethod(AccrualMethods.DAILY_NON_COMPOUNDING);
		trsSecurity.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);

		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setFkFieldId(trsSecurity.getId());
		List<SystemColumnValue> columnValueList = this.systemColumnValueService.getSystemColumnValueList(searchForm);
		columnValueList.forEach(columnValue -> {
			if ("Reference Rate".equals(columnValue.getColumn().getName())) {
				columnValue.setText(sofrRateIndex.getName());
				columnValue.setValue(sofrRateIndex.getId().toString());
			}
			else if ("Fixing Calendar".equals(columnValue.getColumn().getName())) {
				columnValue.setText(sofrRateIndex.getCalendar().getName());
				columnValue.setValue(sofrRateIndex.getCalendar().getId().toString());
			}
			else if ("Fixing Cycle".equals(columnValue.getColumn().getName())) {
				columnValue.setValue("-2");
			}
		});
		trsSecurity.setColumnValueList(columnValueList);
		trsSecurity = this.investmentInstrumentService.saveInvestmentSecurity(trsSecurity);

		List<InvestmentSecurityEvent> eventList = copySecurityEvents(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("2181415591"), trsSecurity);
		Date interestLegEventData = DateUtils.toDate("01/20/2022");
		Optional<InvestmentSecurityEvent> currentInterestLeg = CollectionUtils.getStream(eventList)
				.filter(event -> InvestmentSecurityEventType.INTEREST_LEG_PAYMENT.equals(event.getType().getName()))
				.filter(event -> DateUtils.isEqualWithoutTime(event.getEventDate(), interestLegEventData))
				.findFirst();
		Assertions.assertTrue(currentInterestLeg.isPresent(), "Unable to find current interest leg payment event.");
		currentInterestLeg.ifPresent(event ->
				addEventRateDetails(event, new BigDecimal("0.4"), new BigDecimal("35"),
						RateChange.forRateWithDate("0.04", "12/20/2021"),
						RateChange.forRateWithDate("0.05", "12/21/2021"),
						RateChange.forRateWithDate("0.04", "01/20/2022"))
		);

		Trade trsTrade = this.tradeUtils.newTotalReturnSwapTradeBuilder(trsSecurity, new BigDecimal("99299"), new BigDecimal("210.8189"), true, DateUtils.toDate("11/30/2021"), accountInfo.getHoldingAccount().getIssuingCompany(), accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trsTrade);

		Optional<InvestmentSecurityEvent> currentDividendLeg = CollectionUtils.getStream(eventList)
				.filter(event -> InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT.equals(event.getType().getName()))
				.filter(event -> DateUtils.isEqualWithoutTime(event.getEventDate(), interestLegEventData))
				.findFirst();
		BigDecimal dividendLegAccrual = BigDecimal.ZERO;
		if (currentDividendLeg.isPresent()) {
			dividendLegAccrual = MathUtils.round(MathUtils.multiply(new BigDecimal("99299"), currentDividendLeg.get().getBeforeEventValue()), 2);
		}

		String[] tradeDates = {
				"11/30/2021",
				"12/1/2021",
				"12/2/2021",
				"12/3/2021",
				"12/4/2021",
				"12/5/2021",
				"12/6/2021",
				"12/7/2021",
				"12/8/2021",
				"12/9/2021",
				"12/10/2021",
				"12/11/2021",
				"12/12/2021",
				"12/13/2021",
				"12/14/2021",
				"12/15/2021",
				"12/16/2021",
				"12/17/2021",
				"12/18/2021",
				"12/19/2021",
				"12/20/2021",
				"12/21/2021",
				"12/22/2021",
				"12/23/2021",
				"12/24/2021",
				"12/25/2021",
				"12/26/2021",
				"12/27/2021",
				"12/28/2021",
				"12/29/2021",
				"12/30/2021",
				"12/31/2021",
				"1/1/2022",
				"1/2/2022",
				"1/3/2022",
				"1/4/2022",
				"1/5/2022",
				"1/6/2022",
				"1/7/2022",
				"1/8/2022",
				"1/9/2022",
				"1/10/2022",
				"1/11/2022",
				"1/12/2022",
				"1/13/2022",
				"1/14/2022",
				"1/15/2022",
				"1/16/2022",
				"1/17/2022",
				"1/18/2022"
		};

		double[] interestLegAccruals = {
				-232.60,
				-930.40,
				-1163.01,
				-1163.01,
				-1163.01,
				-1395.61,
				-1628.21,
				-1860.81,
				-2558.61,
				-2791.21,
				-2791.21,
				-2791.21,
				-3023.82,
				-3256.42,
				-3489.02,
				-4186.82,
				-4419.42,
				-4419.42,
				-4419.42,
				-4652.02,
				-4878.81,
				-5809.21,
				-6041.82,
				-6041.82,
				-6041.82,
				-6041.82,
				-6274.42,
				-6507.02,
				-6739.62,
				-7437.42,
				-7670.02,
				-7670.02,
				-7670.02,
				-7902.62,
				-8135.23,
				-8367.83,
				-9065.63,
				-9298.23,
				-9298.23,
				-9298.23,
				-9530.83,
				-9763.43,
				-9996.04,
				-10926.44,
				-11159.04,
				-11159.04,
				-11159.04,
				-11159.04,
				-11391.64,
				-11624.24
		};

		// Validate total accrual by summing daily interest leg accrual with dividend leg for each settlement date tested
		for (int i = 0; i < tradeDates.length; i++) {
			Date transactionDate = DateUtils.toDate(tradeDates[i]);
			Date settlementDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(transactionDate), 2);
			assertAccruedInterestEquals(trsTrade.getId(), MathUtils.add(dividendLegAccrual, BigDecimal.valueOf(interestLegAccruals[i])), transactionDate, settlementDate, false);
		}
	}


	@Test
	public void testAccruedInterest_TRS_1DayFixingCycle_dailyRate() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.JP_MORGAN_CHASE_BANK_NA, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.tradeUtils.getTradeType(TradeType.TOTAL_RETURN_SWAPS));

		InvestmentInterestRateIndex sofrRateIndex = this.investmentRatesService.getInvestmentInterestRateIndexBySymbol("SOFRRATE");
		InvestmentSecurity trsSecurity = this.investmentInstrumentUtils.getCopyOfSecurity("2182149055", "2182149055_" + RandomUtils.randomNumber());

		trsSecurity.setBusinessCompany(accountInfo.getHoldingCompany());
		trsSecurity.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		trsSecurity.setAccrualMethod(AccrualMethods.DAILY_NON_COMPOUNDING);
		trsSecurity.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);

		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setFkFieldId(trsSecurity.getId());
		List<SystemColumnValue> columnValueList = this.systemColumnValueService.getSystemColumnValueList(searchForm);
		columnValueList.forEach(columnValue -> {
			if ("Reference Rate".equals(columnValue.getColumn().getName())) {
				columnValue.setValue(sofrRateIndex.getId().toString());
			}
			else if ("Fixing Calendar".equals(columnValue.getColumn().getName())) {
				columnValue.setValue(sofrRateIndex.getCalendar().getId().toString());
			}
			else if ("Fixing Cycle".equals(columnValue.getColumn().getName())) {
				columnValue.setValue("-1");
			}
		});
		trsSecurity.setColumnValueList(columnValueList);
		trsSecurity = this.investmentInstrumentService.saveInvestmentSecurity(trsSecurity);

		List<InvestmentSecurityEvent> eventList = generateEventList(trsSecurity, new String[]{"Equity Leg Payment", "Interest Leg Payment", "Dividend Leg Payment"});
		Date interestLegEventData = DateUtils.toDate("01/12/2022");
		Optional<InvestmentSecurityEvent> currentInterestLeg = CollectionUtils.getStream(eventList)
				.filter(event -> InvestmentSecurityEventType.INTEREST_LEG_PAYMENT.equals(event.getType().getName()))
				.filter(event -> DateUtils.isEqualWithoutTime(event.getEventDate(), interestLegEventData))
				.findFirst();
		Assertions.assertTrue(currentInterestLeg.isPresent(), "Unable to find current interest leg payment event.");
		currentInterestLeg.ifPresent(event ->
				addEventRateDetails(event, new BigDecimal("-0.18"), new BigDecimal("-23"),
						RateChange.forRateWithDate("0.03", "10/19/2021"),
						RateChange.forRateWithDate("0.05", "10/22/2021"),
						RateChange.forRateWithDate("0.04", "10/25/2021"),
						RateChange.forRateWithDate("0.05", "10/26/2021"))
		);

		Trade trsTrade = this.tradeUtils.newTotalReturnSwapTradeBuilder(trsSecurity, new BigDecimal("57680"), new BigDecimal("141.1059"), true, DateUtils.toDate("10/08/2021"), accountInfo.getHoldingAccount().getIssuingCompany(), accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trsTrade);

		Optional<InvestmentSecurityEvent> currentDividendLeg = CollectionUtils.getStream(eventList)
				.filter(event -> InvestmentSecurityEventType.DIVIDEND_LEG_PAYMENT.equals(event.getType().getName()))
				.filter(event -> DateUtils.isEqualWithoutTime(event.getEventDate(), interestLegEventData))
				.findFirst();
		BigDecimal dividendLegAccrual = BigDecimal.ZERO;
		if (currentDividendLeg.isPresent()) {
			dividendLegAccrual = MathUtils.round(MathUtils.multiply(new BigDecimal("99233"), currentDividendLeg.get().getBeforeEventValue()), 2);
		}

		String[] tradeDates = {
				"10/12/2021",
				"10/13/2021",
				"10/14/2021",
				"10/15/2021",
				"10/18/2021",
				"10/19/2021",
				"10/20/2021",
				"10/21/2021",
				"10/22/2021",
				"10/25/2021",
				"10/26/2021",
				"10/27/2021",
				"10/28/2021",
				"10/29/2021"
		};

		double[] interestLegAccruals = {
				40.7,
				81.39,
				203.48,
				244.17,
				284.86,
				330.08,
				375.30,
				501.91,
				542.60,
				585.56,
				626.25,
				666.95,
				789.03,
				829.72
		};

		// Validate total accrual by summing daily interest leg accrual with dividend leg for each settlement date tested
		for (int i = 0; i < tradeDates.length; i++) {
			Date transactionDate = DateUtils.toDate(tradeDates[i]);
			Date settlementDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(transactionDate), 2);
			assertAccruedInterestEquals(trsTrade.getId(), MathUtils.add(dividendLegAccrual, BigDecimal.valueOf(interestLegAccruals[i])), transactionDate, settlementDate, false);
		}
	}


	@Test
	public void testAccruedInterest_iBoxxTRS() {
		assertAccruedInterestEquals(691_736, new BigDecimal("16276.36")); // sell to open before 1st coupon payment - 05/23/2017 TD
		assertAccruedInterestEquals(691_956, new BigDecimal("28928.87")); // buy to open before 1st coupon payment - 05/24/2017 TD

		// override trade/settlement dates to 1 day earlier in order to be able to close position before it's closed by the trade and reset
		assertAccruedInterestEquals(636_352, new BigDecimal("-379.54"), DateUtils.toDate("12/20/2016"), DateUtils.toDate("12/21/2016")); // sell to close after 1st coupon payment - 12/21/16 TD
		assertAccruedInterestEquals(715_355, new BigDecimal("-1645.00"), DateUtils.toDate("07/07/2017"), DateUtils.toDate("07/08/2017")); // buy to close after 1st coupon payment - 7/10/17 TD
		assertAccruedInterestEquals(636_352, new BigDecimal("-29682.20"), DateUtils.toDate("12/19/2016"), DateUtils.toDate("12/20/2016")); // sell to close one day before accrual end date
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertAccruedInterestEquals(int tradeId, BigDecimal accruedInterest) {
		assertAccruedInterestEquals(tradeId, accruedInterest, null, null);
	}


	private void assertAccruedInterestEquals(int tradeId, BigDecimal accruedInterest, boolean clearQuantity) {
		assertAccruedInterestEquals(tradeId, accruedInterest, null, null, clearQuantity);
	}


	private void assertAccruedInterestEquals(int tradeId, BigDecimal accruedInterest, Date tradeDateOverride, Date settlementDateOverride) {
		assertAccruedInterestEquals(tradeId, accruedInterest, tradeDateOverride, settlementDateOverride, null, null);
	}


	private void assertAccruedInterestEquals(int tradeId, BigDecimal accruedInterest, Date tradeDateOverride, Date settlementDateOverride, Boolean isBuyOverride) {
		assertAccruedInterestEquals(tradeId, accruedInterest, tradeDateOverride, settlementDateOverride, false, isBuyOverride, null);
	}


	private void assertAccruedInterestEquals(int tradeId, BigDecimal accruedInterest, Date tradeDateOverride, Date settlementDateOverride, Boolean isBuyOverride, BigDecimal quantityOverride) {
		assertAccruedInterestEquals(tradeId, accruedInterest, tradeDateOverride, settlementDateOverride, false, isBuyOverride, quantityOverride);
	}


	private void assertAccruedInterestEquals(int tradeId, BigDecimal accruedInterest, Date tradeDateOverride, Date settlementDateOverride, boolean clearQuantity, Boolean isBuyOverride, BigDecimal quantityOverride) {
		Trade trade = this.tradeService.getTrade(tradeId);
		Assertions.assertNotNull(trade, "Cannot find trade with id = " + tradeId);
		trade.setInvestmentSecurity(this.investmentInstrumentService.getInvestmentSecurity(trade.getInvestmentSecurity().getId()));
		trade.setAccrualAmount1(null);
		trade.setAccrualAmount2(null);
		if (tradeDateOverride != null) {
			trade.setTradeDate(tradeDateOverride);
		}
		if (settlementDateOverride != null) {
			trade.setSettlementDate(settlementDateOverride);
		}
		if (clearQuantity) {
			trade.setQuantityIntended(null);
		}
		if (isBuyOverride != null) {
			trade.setBuy(isBuyOverride);
		}
		if (quantityOverride != null) {
			trade.setQuantityIntended(quantityOverride);
		}

		BigDecimal calculatedInterest;
		try {
			calculatedInterest = RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradePositionService.getTradeAccrualAmount(trade),
					new RequestOverrideHandler.EntityPropertyOverrides(Trade.class, false, "workflowState", "workflowStatus", "executingBrokerCompany"),
					new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, false, "businessClient", "businessContract"));
		}
		catch (Throwable e) {
			throw new RuntimeException("Error in getTradeAccrualAmount for " + trade, e);
		}

		BigDecimal difference = MathUtils.subtract(accruedInterest, calculatedInterest);
		Assertions.assertTrue(MathUtils.isLessThanOrEqual(difference, MathUtils.BIG_DECIMAL_PENNY), () -> String.format("[Trade Date %s] Calculated accrued interest (%s) differs from expected value (%s) by more than a penny.", DateUtils.fromDateShort(trade.getTradeDate()), calculatedInterest, accruedInterest));
	}


	private List<InvestmentSecurityEvent> copySecurityEvents(InvestmentSecurity sourceSecurity, InvestmentSecurity newSecurity) {
		List<InvestmentSecurityEvent> eventList = getSecurityEventListForSecurity(sourceSecurity);
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(eventList), "Unable to find events for security " + sourceSecurity);

		return CollectionUtils.getStream(eventList)
				.map(event -> {
					event.setId(null);
					event.setSecurity(newSecurity);
					return this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
				})
				.collect(Collectors.toList());
	}


	private List<InvestmentSecurityEvent> getSecurityEventListForSecurity(InvestmentSecurity security) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		return this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
	}


	private void addEventRateDetails(InvestmentSecurityEvent event, BigDecimal initialRate, BigDecimal spread, RateChange... rateChanges) {
		event.setBeforeAndAfterEventValue(initialRate);
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);

		// create detail rates
		ArrayUtils.getStream(rateChanges)
				.forEach(rateChange -> {
					InvestmentSecurityEventDetail detail = new InvestmentSecurityEventDetail();
					detail.setEvent(event);
					detail.setReferenceRate(rateChange.rate);
					detail.setEffectiveDate(rateChange.effectiveDate);
					detail.setSpread(spread);
					this.investmentSecurityEventService.saveInvestmentSecurityEventDetail(detail);
				});
	}


	private List<InvestmentSecurityEvent> generateEventList(InvestmentSecurity security, String[] eventTypeNames) {
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypeNames, true, false);
		return getSecurityEventListForSecurity(security);
	}


	private static final class RateChange {

		private final Date effectiveDate;
		private final BigDecimal rate;


		RateChange(Date effectiveDate, BigDecimal rate) {
			this.effectiveDate = effectiveDate;
			this.rate = rate;
		}


		static RateChange forRateWithDate(String rate, String date) {
			return new RateChange(DateUtils.toDate(date), new BigDecimal(rate));
		}
	}
}
