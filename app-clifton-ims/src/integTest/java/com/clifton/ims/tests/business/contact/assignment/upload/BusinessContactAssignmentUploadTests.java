package com.clifton.ims.tests.business.contact.assignment.upload;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactCategory;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.assignment.BusinessContactAssignment;
import com.clifton.business.contact.assignment.BusinessContactAssignmentCategory;
import com.clifton.business.contact.assignment.BusinessContactAssignmentRole;
import com.clifton.business.contact.assignment.BusinessContactAssignmentService;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentCategorySearchForm;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentRoleSearchForm;
import com.clifton.business.contact.assignment.search.BusinessContactAssignmentSearchForm;
import com.clifton.business.contact.search.BusinessContactCategorySearchForm;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.business.contact.upload.BusinessContactAssignmentUploadCommand;
import com.clifton.business.contact.upload.BusinessContactAssignmentUploadService;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.system.schema.SystemSchemaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Tests upload functionality related to {@link com.clifton.business.contact.assignment.BusinessContactAssignment}s.
 *
 * @author michaelm
 * @see com.clifton.business.contact.upload.BusinessContactAssignmentUploadCommand
 * @see com.clifton.business.contact.upload.BusinessContactAssignmentUploadService
 */
public class BusinessContactAssignmentUploadTests extends BaseImsIntegrationTest {

	private static final String INVESTMENT_ACCOUNT_NUMBER = "000002";

	private static final String CONTACT_ASSIGNMENT_CATEGORY_NAME = "Parametric Account Contacts";
	private static final String CONTACT_CATEGORY_NAME = "Parametric";
	private static final String CONTACT_ASSIGNMENT_CATEGORY_ENTITY_TABLE_NAME = "InvestmentAccount";
	private static final String CONTACT_ASSIGNMENT_CATEGORY_ENTITY_LIST_URL = "investmentAccountListFind.json?ourAccount=true";

	private static final String CONTACT_ASSIGNMENT_ROLE_NAME = "PM (Primary)";

	private BusinessContactAssignmentCategory contactAssignmentCategory;
	private BusinessContactAssignmentRole contactAssignmentRole;
	private InvestmentAccount investmentAccount;
	private BusinessContact contactMichaelMajor;
	private BusinessContact contactNickKirsch;

	@Resource
	private BusinessContactService businessContactService;

	@Resource
	private BusinessContactAssignmentService businessContactAssignmentService;

	@Resource
	private BusinessContactAssignmentUploadService businessContactAssignmentUploadService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private SystemSchemaService systemSchemaService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTestData() {
		this.contactAssignmentCategory = getOrCreateContactAssignmentCategory();
		this.contactAssignmentRole = getOrCreateContactAssignmentRole();
		this.investmentAccount = getOrCreateInvestmentAccount();
		this.contactMichaelMajor = getOrCreateContact("Michael", "Major", CONTACT_CATEGORY_NAME);
		this.contactNickKirsch = getOrCreateContact("Nick", "Kirsch", CONTACT_CATEGORY_NAME);
		deleteContactAssignmentsForFkFieldId(this.investmentAccount.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testContactAssignmentUpload_MaxAssigneesPerEntity_SkipActiveExistingAssignee() {
		this.businessContactAssignmentUploadService.uploadBusinessContactAssignmentFile(getUploadCommand());
		BusinessContactAssignment assignment = CollectionUtils.getOnlyElementStrict(getContactAssignmentListByFkFieldId(this.investmentAccount.getId(), true));
		ValidationUtils.assertEquals("Michael Major", assignment.getContact().getNameLabel(),
				"Contact Assignment in upload file should be ignored because it already exists.");
	}


	/**
	 * Ensure that existing PM (Primary) is replaced with the one from the upload by setting endDate of existing assignment and startDate of new assignment.
	 */
	@Test
	public void testContactAssignmentUpload_MaxAssigneesPerEntity_ReplaceExisting() {
		createContactAssignment(this.contactMichaelMajor, this.investmentAccount.getId(), DateUtils.toDate("05/01/2020"), DateUtils.toDate("05/05/2020"));
		createContactAssignment(this.contactNickKirsch, this.investmentAccount.getId(), DateUtils.toDate("05/06/2020"));
		BusinessContactAssignmentUploadCommand uploadCommand = getUploadCommand();
		this.businessContactAssignmentUploadService.uploadBusinessContactAssignmentFile(uploadCommand);
		ValidationUtils.assertEquals("Michael Major", CollectionUtils.getOnlyElementStrict(getContactAssignmentListByFkFieldId(this.investmentAccount.getId(), true)).getContact().getNameLabel(),
				"Contact Assignment in upload file should have been replaced the existing Contact Assignment.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BusinessContactAssignmentCategory getOrCreateContactAssignmentCategory() {
		BusinessContactAssignmentCategorySearchForm searchForm = new BusinessContactAssignmentCategorySearchForm();
		searchForm.setName(CONTACT_ASSIGNMENT_CATEGORY_NAME);
		BusinessContactAssignmentCategory assignmentCategory = CollectionUtils.getFirstElement(this.businessContactAssignmentService.getBusinessContactAssignmentCategoryList(searchForm));
		if (assignmentCategory == null) {
			assignmentCategory = createContactAssignmentCategory();
		}
		return assignmentCategory;
	}


	private BusinessContactAssignmentCategory createContactAssignmentCategory() {
		BusinessContactAssignmentCategory assignmentCategory = new BusinessContactAssignmentCategory();
		assignmentCategory.setName(CONTACT_ASSIGNMENT_CATEGORY_NAME);
		assignmentCategory.setContactCategory(getOrCreateContactCategory());
		assignmentCategory.setEntitySystemTable(this.systemSchemaService.getSystemTableByName(CONTACT_ASSIGNMENT_CATEGORY_ENTITY_TABLE_NAME));
		assignmentCategory.setEntityListUrl(CONTACT_ASSIGNMENT_CATEGORY_ENTITY_LIST_URL);
		return this.businessContactAssignmentService.saveBusinessContactAssignmentCategory(assignmentCategory);
	}


	private BusinessContactCategory getOrCreateContactCategory() {
		BusinessContactCategorySearchForm searchForm = new BusinessContactCategorySearchForm();
		searchForm.setSearchPattern(CONTACT_CATEGORY_NAME);
		BusinessContactCategory contactCategory = CollectionUtils.getFirstElement(this.businessContactService.getBusinessContactCategoryList(searchForm));
		if (contactCategory == null) {
			contactCategory = new BusinessContactCategory();
			contactCategory.setName(CONTACT_CATEGORY_NAME);
			contactCategory = this.businessContactService.saveBusinessContactCategory(contactCategory);
		}
		return contactCategory;
	}


	private BusinessContactAssignmentRole getOrCreateContactAssignmentRole() {
		BusinessContactAssignmentRoleSearchForm searchForm = new BusinessContactAssignmentRoleSearchForm();
		searchForm.setName(CONTACT_ASSIGNMENT_ROLE_NAME);
		BusinessContactAssignmentRole contactAssignmentRole = CollectionUtils.getFirstElement(this.businessContactAssignmentService.getBusinessContactAssignmentRoleList(searchForm));
		if (contactAssignmentRole == null) {
			contactAssignmentRole = createContactAssignmentRole();
		}
		return contactAssignmentRole;
	}


	private BusinessContactAssignmentRole createContactAssignmentRole() {
		BusinessContactAssignmentRole contactAssignmentRole = new BusinessContactAssignmentRole();
		contactAssignmentRole.setName(CONTACT_ASSIGNMENT_ROLE_NAME);
		contactAssignmentRole.setMaxAssigneesPerEntity(1);
		return this.businessContactAssignmentService.saveBusinessContactAssignmentRole(contactAssignmentRole);
	}


	private InvestmentAccount getOrCreateInvestmentAccount() {
		InvestmentAccount investmentAccount;
		try {
			investmentAccount = this.investmentAccountUtils.getClientAccountByNumber(INVESTMENT_ACCOUNT_NUMBER);
		}
		catch (IllegalStateException e) {
			investmentAccount = this.investmentAccountUtils.createClientAccount(this.businessUtils.createBusinessClient());
			investmentAccount.setNumber(INVESTMENT_ACCOUNT_NUMBER);
			investmentAccount = this.investmentAccountService.saveInvestmentAccount(investmentAccount);
		}
		return investmentAccount;
	}


	private BusinessContact getOrCreateContact(String firstName, String lastName, String contactCategoryName) {
		BusinessContactSearchForm searchForm = new BusinessContactSearchForm();
		searchForm.setFirstName(firstName);
		searchForm.setLastName(lastName);
		BusinessContact contact = CollectionUtils.getFirstElement(this.businessContactService.getBusinessContactList(searchForm));
		if (contact == null) {
			contact = new BusinessContact();
			contact.setFirstName(firstName);
			contact.setLastName(lastName);
			BusinessContactCategorySearchForm categorySearchForm = new BusinessContactCategorySearchForm();
			categorySearchForm.setSearchPattern(contactCategoryName);
			contact.setContactCategory(CollectionUtils.getFirstElement(this.businessContactService.getBusinessContactCategoryList(categorySearchForm)));
		}
		return contact;
	}


	private BusinessContactAssignment createContactAssignment(BusinessContact contact, int fkFieldId, Date startDate) {
		return createContactAssignment(contact, fkFieldId, startDate, null);
	}


	private BusinessContactAssignment createContactAssignment(BusinessContact contact, int fkFieldId, Date startDate, Date endDate) {
		BusinessContactAssignment contactAssignment = new BusinessContactAssignment();
		contactAssignment.setContactAssignmentCategory(this.contactAssignmentCategory);
		contactAssignment.setContactAssignmentRole(this.contactAssignmentRole);
		contactAssignment.setContact(contact);
		contactAssignment.setFkFieldId(fkFieldId);
		contactAssignment.setStartDate(startDate);
		contactAssignment.setEndDate(endDate);
		return this.businessContactAssignmentService.saveBusinessContactAssignment(contactAssignment);
	}


	private void deleteContactAssignmentsForFkFieldId(int fkFieldId) {
		CollectionUtils.getStream(getContactAssignmentListByFkFieldId(fkFieldId)).forEach(assignment -> this.businessContactAssignmentService.deleteBusinessContactAssignment(assignment.getId()));
	}


	private List<BusinessContactAssignment> getContactAssignmentListByFkFieldId(int fkFieldId) {
		return getContactAssignmentListByFkFieldId(fkFieldId, false);
	}


	private List<BusinessContactAssignment> getContactAssignmentListByFkFieldId(int fkFieldId, boolean activeOnly) {
		BusinessContactAssignmentSearchForm searchForm = new BusinessContactAssignmentSearchForm();
		searchForm.setFkFieldId(fkFieldId);
		if (activeOnly) {
			searchForm.setActive(true);
		}
		return this.businessContactAssignmentService.getBusinessContactAssignmentList(searchForm);
	}


	private BusinessContactAssignmentUploadCommand getUploadCommand() {
		BusinessContactAssignmentUploadCommand uploadCommand = new BusinessContactAssignmentUploadCommand();
		uploadCommand.setExistingBeans(FileUploadExistingBeanActions.SKIP);
		uploadCommand.setContactAssignmentCategory(this.contactAssignmentCategory);
		uploadCommand.setFile(new MultipartFileImpl("src/integTest/java/com/clifton/ims/tests/business/contact/assignment/upload/TestBusinessContactAssignmentUploadFile.xls"));
		return uploadCommand;
	}
}
