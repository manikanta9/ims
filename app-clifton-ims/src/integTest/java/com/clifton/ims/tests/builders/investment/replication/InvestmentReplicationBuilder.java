package com.clifton.ims.tests.builders.investment.replication;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.investment.replication.InvestmentReplicationUtils;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.ReplicationRebalanceTriggerTypes;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;

import java.util.List;


/**
 * {@link InvestmentReplicationBuilder} provides convenient ways to build {@link com.clifton.investment.replication.InvestmentReplication}s for tests.
 * It accepts primitive data for some FK fields and then checks for an existing Replication with the same name before looking up the entities that the primitive inputs reference.
 * This will avoid unnecessary calls to the server during integration tests.
 *
 * @author michaelm
 */
public class InvestmentReplicationBuilder {

	private final InvestmentReplicationUtils replicationUtils;

	private String name;
	private String description;
	private String replicationTypeName;
	private boolean partiallyAllocated;
	private boolean allowedForMatching;
	private boolean inactive;
	private boolean reverseExposureSign;
	private String dynamicCalculatorBeanName;
	private ReplicationRebalanceTriggerTypes rebalanceTriggerType;
	private List<InvestmentReplicationAllocation> allocationList;


	public InvestmentReplicationBuilder(InvestmentReplicationUtils replicationUtils) {
		this.replicationUtils = replicationUtils;
	}


	public static InvestmentReplicationBuilder ofReplicationUtilsAndName(InvestmentReplicationUtils serviceHolder, String name) {
		return new InvestmentReplicationBuilder(serviceHolder)
				.withName(name);
	}


	public InvestmentReplication build() {
		InvestmentReplication investmentReplication = new InvestmentReplication();
		investmentReplication.setName(this.name);
		investmentReplication.setDescription(this.description);
		investmentReplication.setType(this.replicationTypeName != null ? getReplicationTypeByName(this.replicationTypeName) : getReplicationTypeByName(InvestmentReplicationType.DEFAULT_REPLICATION_TYPE_NAME));
		investmentReplication.setPartiallyAllocated(this.partiallyAllocated);
		investmentReplication.setAllowedForMatching(this.allowedForMatching);
		investmentReplication.setInactive(this.inactive);
		investmentReplication.setReverseExposureSign(this.reverseExposureSign);
		if (!StringUtils.isEmpty(this.dynamicCalculatorBeanName)) {
			investmentReplication.setDynamicCalculatorBean(this.replicationUtils.getSystemBeanService().getSystemBeanByName(this.dynamicCalculatorBeanName));
		}
		investmentReplication.setRebalanceTriggerType(this.rebalanceTriggerType);
		investmentReplication.setAllocationList(this.allocationList);
		// need to set replication on all allocations to avoid NPE within BeanParameterUtils#getAllParametersRecursively
		CollectionUtils.getStream(investmentReplication.getAllocationList()).forEach(allocation -> {
			if (allocation.getReplication() == null) {
				allocation.setReplication(investmentReplication);
			}
		});
		return investmentReplication;
	}


	/**
	 * Checks for an existing {@link InvestmentReplication} with the given {@link #name} without comparing properties.
	 * If there is no existing Replication, a new one is built and saved using the builder input.
	 */
	public InvestmentReplication checkForExistingBuildAndSave() {
		ValidationUtils.assertNotNull(this.name, "Replication Name is required to build an InvestmentReplication");
		InvestmentReplicationSearchForm searchForm = new InvestmentReplicationSearchForm();
		searchForm.setNameEquals(this.name);
		InvestmentReplication investmentReplication = CollectionUtils.getFirstElement(this.replicationUtils.getInvestmentReplicationService().getInvestmentReplicationList(searchForm));
		if (investmentReplication == null) {
			investmentReplication = this.replicationUtils.getInvestmentReplicationService().saveInvestmentReplication(build());
		}
		return investmentReplication;
	}


	private InvestmentReplicationType getReplicationTypeByName(String replicationTypeName) {
		ValidationUtils.assertTrue(this.replicationUtils != null && this.replicationUtils.getInvestmentReplicationService() != null,
				"InvestmentReplicationService is required to build the InvestmentReplication.");
		return this.replicationUtils.getInvestmentReplicationService().getInvestmentReplicationTypeByName(replicationTypeName);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public InvestmentReplicationBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public InvestmentReplicationBuilder withReplicationTypeName(String replicationTypeName) {
		this.replicationTypeName = replicationTypeName;
		return this;
	}


	public InvestmentReplicationBuilder withPartiallyAllocated(boolean partiallyAllocated) {
		this.partiallyAllocated = partiallyAllocated;
		return this;
	}


	public InvestmentReplicationBuilder withAllowedForMatching(boolean allowedForMatching) {
		this.allowedForMatching = allowedForMatching;
		return this;
	}


	public InvestmentReplicationBuilder withInactive(boolean inactive) {
		this.inactive = inactive;
		return this;
	}


	public InvestmentReplicationBuilder withReverseExposureSign(boolean reverseExposureSign) {
		this.reverseExposureSign = reverseExposureSign;
		return this;
	}


	public InvestmentReplicationBuilder withDynamicCalculatorBeanName(String dynamicCalculatorBeanName) {
		this.dynamicCalculatorBeanName = dynamicCalculatorBeanName;
		return this;
	}


	public InvestmentReplicationBuilder withRebalanceTriggerType(ReplicationRebalanceTriggerTypes rebalanceTriggerType) {
		this.rebalanceTriggerType = rebalanceTriggerType;
		return this;
	}


	public InvestmentReplicationBuilder withAllocationList(List<InvestmentReplicationAllocation> allocationList) {
		this.allocationList = allocationList;
		return this;
	}
}
