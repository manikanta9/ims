package com.clifton.ims.tests.spring;

import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.websocket.config.WebSocketConfigurationProperties;
import com.clifton.websocket.config.WebSocketConfigurationRegistryPostProcessor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyResourceConfigurer;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;


/**
 * This class includes tests for validating that Spring {@link ApplicationContext} lifecycle constraints are not violated by the IMS context.
 * <p>
 * In particular, this class includes tests for validating that the {@link BeanFactoryPostProcessor} constraint regarding bean instantiation is satisfied within certain
 * customizable constraints. This constraint states that beans should not be instantiated during the {@link BeanFactoryPostProcessor} phase. From the {@link
 * BeanFactoryPostProcessor} JavaDoc:
 * <p>
 * <i>A BeanFactoryPostProcessor may interact with and modify bean definitions, but never bean instances. Doing so may cause premature bean instantiation, violating the container
 * and causing unintended side-effects. If bean instance interaction is required, consider implementing {@link BeanPostProcessor} instead.</i>
 * <p>
 * The primary side-effect of violating the {@link BeanFactoryPostProcessor} bean instantiation constraint is that portions of {@link BeanPostProcessor} processing may be skipped
 * for all instantiated beans and all of their dependencies. Additionally, further definition modifications from {@link BeanFactoryPostProcessor} objects may be preempted by bean
 * instantiation, causing a mismatch between the bean instance and the bean definition. These issues can be difficult to debug. Additionally, if {@link Autowire#BY_NAME} is enabled
 * then instantiated beans will be dependent upon any beans whose names match their field names, resulting in potentially long dependency chains. Some common side-effects to be
 * aware of include the following:
 * <ul>
 * <li>Explicitly {@link Autowired} properties will fail to autowire during for beans instantiated before the {@link BeanPostProcessor} phase because the {@link
 * AutowiredAnnotationBeanPostProcessor} bean will not yet be registered.
 * <li>References to beans or bean definitions generated in other {@link BeanFactoryPostProcessor} objects will fail to autowire because those beans or bean definitions will not
 * yet exist at the time of bean instantiation.
 * <li>Resolvable bean properties (such as {@code ${my-value}}) might not be resolved since {@link PropertyResourceConfigurer} beans (which are {@link BeanFactoryPostProcessor}
 * beans) may not have been processed yet.
 * </ul>
 * <p>
 * Violating the {@link BeanFactoryPostProcessor} bean instantiation constraint should be avoided as much as possible. However, there are circumstances where this constraint may be
 * violated without issues. Beans which violate this constraint should be tightly monitored. Ideally, these beans should be  simple POJOs and should have no autowired properties of
 * their own. When defining these beans, bean references should be avoided and autowiring should be explicitly disabled for the bean if possible.
 * <p>
 * Some additional information on the {@link ApplicationContext} lifecycle may be found <a href="https://jakubstas.com/spring-professional-study-notes/">here</a>.
 *
 * @author MikeH
 */
public class ImsBeanFactoryLifecycleTests extends BaseImsBeanFactoryTests {

	private static final String ERROR_MSG_DELIMITER = "\n\t";
	private static final String ERROR_MSG_PREFIX = "The Application Context lifecycle Bean Factory Post Processor bean instantiation constraint is violated." +
			" The following disallowed beans were created during the Bean Factory Post Processor phase:" + ERROR_MSG_DELIMITER;

	/**
	 * The collection of allowed bean names for early instantiation. Beans of these names will be allowed for early instantiation.
	 */
	private static final Collection<String> ALLOWED_BEAN_NAMES = Collections.unmodifiableCollection(CollectionUtils.createHashSet(
			// Add allowed bean names here
			"org.springframework.context.annotation.ConfigurationClassPostProcessor.importRegistry",
			"org.springframework.context.event.internalEventListenerFactory",
			"loadTimeWeaver",

			// Allow the database connection to start during the BFPP phase for the JdbcHealthCheckWaitingPostProcessor
			"jdbcTemplate",
			"dataSource",
			"lazyConnectionDataSource",
			"cacheRegionFactory"
	));

	/**
	 * The collection of allowed package prefixes for early instantiation. Beans whose class packages have any of the given prefixes will be allowed for early instantiation.
	 */
	private static final Collection<String> ALLOWED_PACKAGE_PREFIXES = Collections.unmodifiableCollection(CollectionUtils.createList(
			// Add allowed bean class package prefixes here
			"java.util"
	));

	/**
	 * The collection of allowed bean types for early instantiation. Beans whose classes exist in this collection will be allowed for early instantiation.
	 */
	private static final Collection<Class<?>> ALLOWED_TYPES = Collections.unmodifiableCollection(CollectionUtils.createList(
			// Add allowed bean types here
			WebSocketConfigurationRegistryPostProcessor.class,
			WebSocketConfigurationProperties.class,
			DAOConfiguration.class,
			BeanFactoryPostProcessor.class,
			Environment.class
	));

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBeanFactoryLifecycleConstraints() {
		// Retrieve the instantiated beans
		Collection<String> instantiatedBeanList = getContext().getGeneratedSingletonNameList();

		// Get all instantiated beans which do not satisfy criteria
		Collection<String> invalidBeans = CollectionUtils.getFiltered(instantiatedBeanList, beanName -> !isAllowedBean(beanName));

		if (!invalidBeans.isEmpty()) {
			// Generate and display error message
			ConfigurableListableBeanFactory beanFactory = getContext().getBeanFactory();
			String errorMessage = invalidBeans.stream()
					.map(beanName -> String.format("Bean [%s] of type [%s] with possible triggering beans %s",
							beanName, beanFactory.getType(beanName) == null ? "null" : AssertUtils.assertNotNull(beanFactory.getType(beanName), "Bean type is null").getName(), Arrays.toString(beanFactory.getDependentBeans(beanName))))
					.collect(Collectors.joining(ERROR_MSG_DELIMITER, ERROR_MSG_PREFIX, ""));
			AssertUtils.fail(errorMessage);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines whether the given bean satisfies the constraints for being an allowed bean for instantiation before the end of the Application Context Bean Factory Post Processor
	 * lifecycle phase.
	 *
	 * @param beanName the name of the bean to validate
	 * @return {@code true} if the bean satisfies constraints for allowed beans, or {@code false} otherwise
	 */
	private boolean isAllowedBean(String beanName) {
		Object bean = getContext().getBean(beanName);
		Class<?> beanClazz = bean.getClass();
		String beanPkgName = beanClazz.getPackage().getName();
		return ALLOWED_BEAN_NAMES.contains(beanName)
				|| CollectionUtils.anyMatch(ALLOWED_PACKAGE_PREFIXES, beanPkgName::startsWith)
				|| CollectionUtils.anyMatch(ALLOWED_TYPES, type -> type.isAssignableFrom(beanClazz));
	}
}
