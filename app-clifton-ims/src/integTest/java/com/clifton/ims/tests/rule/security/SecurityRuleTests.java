package com.clifton.ims.tests.rule.security;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public abstract class SecurityRuleTests extends BaseImsIntegrationTest {

	public static final String PORTFOLIO_RUN_TABLE_NAME = "PortfolioRun";
	public static final String TRADE_TABLE_NAME = "Trade";

	public PortfolioRun portfolioRun;
	public List<RuleViolation> tradeResults;

	@Resource
	public CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	public PortfolioRunService portfolioRunService;

	@Resource
	public RuleDefinitionService ruleDefinitionService;

	@Resource
	public RuleEvaluatorService ruleEvaluatorService;

	@Resource
	public RuleViolationService ruleViolationService;

	@Resource
	public RuleViolationUtils ruleViolationUtils;

	@Resource
	public WorkflowTransitioner workflowTransitioner;


	//Used to create data with the admin user before switching users to test role security for other actions.
	@BeforeEach
	public void initializeTest() {
		String username = ImsTestProperties.TEST_USER_ADMIN;
		String password = ImsTestProperties.TEST_USER_ADMIN_PASSWORD;
		this.userUtils.switchUser(username, password);

		InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("340010");
		if (mainClientAccount != null) {
			PortfolioRun portfolioRun = this.portfolioRunService.getPortfolioRunByMainRun(mainClientAccount.getId(), DateUtils.toDate("07/15/2015"));
			if (portfolioRun != null) {
				this.portfolioRun = portfolioRun;
			}
		}
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Bonds (Central Govt - Inflation Linked - TIPS) - All (Long Only)", "Permitted Security: Futures (Currency) - All (Long or Short)");
		this.tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "CLK6", new BigDecimal(123), true, new Date());
		this.tradeResults = this.ruleViolationUtils.filterViolationListByTypeName("Compliance Real Time Rules", true, this.tradeResults);

		switchUser();
	}


	public abstract void switchUser();


	@Test
	public void testSaveTradeViolation() {
		for (RuleViolation ruleViolation : this.tradeResults) {
			ruleViolation.setIgnored(true);
			ruleViolation.setIgnoreNote("I'm going to ignore this...");
			this.ruleViolationService.saveRuleViolation(ruleViolation);
		}
	}


	@Test
	public void testIgnoreManualPortfolioRunViolation() {
		RuleAssignment ruleAssignment = this.ruleDefinitionService.getRuleAssignmentGlobalByDefinitionName("Portfolio Run Violations");

		RuleViolation ruleViolation = new RuleViolation();
		ruleViolation.setRuleAssignment(ruleAssignment);
		ruleViolation.setCauseTable(ruleAssignment.getRuleDefinition().getCauseTable());
		ruleViolation.setLinkedFkFieldId(this.portfolioRun.getId());
		ruleViolation.setViolationNote("If I were an elephant, I would never need a manual violation.");
		ruleViolation.setIgnored(true);
		ruleViolation.setIgnoreNote("Ignore him.  He thinks he never forgets anything.");
		this.ruleViolationService.saveRuleViolationManual(ruleViolation);
		Assertions.assertTrue(true);
	}


	@Test
	public void testSaveManualPortfolioRunViolation() {
		RuleAssignment ruleAssignment = this.ruleDefinitionService.getRuleAssignmentGlobalByDefinitionName("Portfolio Run Violations");

		RuleViolation ruleViolation = new RuleViolation();
		ruleViolation.setRuleAssignment(ruleAssignment);
		ruleViolation.setCauseTable(ruleAssignment.getRuleDefinition().getCauseTable());
		ruleViolation.setLinkedFkFieldId(this.portfolioRun.getId());
		ruleViolation.setViolationNote("I feel violated.");
		this.ruleViolationService.saveRuleViolationManual(ruleViolation);
		Assertions.assertTrue(true);
	}


	@Test
	public void testSaveAndDeletePortfolioRunRuleAssignment() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setRuleDefinition(this.ruleDefinitionService.getRuleDefinitionByName("Currency Balance: Single Base Exposure Range"));
		//Don't care what the id is - deleting rule assignment anyway.
		ruleAssignment.setAdditionalScopeFkFieldId((long) 1);
		ruleAssignment.setAdditionalScopeLabel("Does not matter.");
		ruleAssignment.setMinAmount(new BigDecimal(-300000));
		ruleAssignment.setMaxAmount(new BigDecimal(300000));
		ruleAssignment.setNote("Security Test");
		this.ruleDefinitionService.saveRuleAssignment(ruleAssignment);
		RuleAssignmentSearchForm ruleAssignmentSearchForm = new RuleAssignmentSearchForm();
		ruleAssignmentSearchForm.setNote("Security Test");
		ruleAssignmentSearchForm.setLimit(1);
		List<RuleAssignment> ruleAssignmentList = this.ruleDefinitionService.getRuleAssignmentList(ruleAssignmentSearchForm);
		this.ruleDefinitionService.deleteRuleAssignment(ruleAssignmentList.get(0).getId());
		Assertions.assertTrue(true);
	}


	@Test
	public void testSaveAndDeleteTradeRuleAssignment() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setRuleDefinition(this.ruleDefinitionService.getRuleDefinitionByName("Current Security For Instrument"));
		//Don't care what the id is - deleting rule assignment anyway.
		ruleAssignment.setAdditionalScopeFkFieldId((long) 1);
		ruleAssignment.setAdditionalScopeLabel("Does not matter.");
		ruleAssignment.setMinAmount(new BigDecimal(-300000));
		ruleAssignment.setMaxAmount(new BigDecimal(300000));
		ruleAssignment.setNote("Security Test");
		this.ruleDefinitionService.saveRuleAssignment(ruleAssignment);
		RuleAssignmentSearchForm ruleAssignmentSearchForm = new RuleAssignmentSearchForm();
		ruleAssignmentSearchForm.setNote("Security Test");
		ruleAssignmentSearchForm.setLimit(1);
		List<RuleAssignment> ruleAssignmentList = this.ruleDefinitionService.getRuleAssignmentList(ruleAssignmentSearchForm);
		this.ruleDefinitionService.deleteRuleAssignment(ruleAssignmentList.get(0).getId());
		Assertions.assertTrue(true);
	}


	@Test
	public void testSaveAndDeleteTradeRealTimeRuleAssignment() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setRuleDefinition(this.ruleDefinitionService.getRuleDefinitionByName("Compliance Real Time Rules"));
		//Don't care what the id is - deleting rule assignment anyway.
		ruleAssignment.setAdditionalScopeFkFieldId((long) 1);
		ruleAssignment.setAdditionalScopeLabel("Does not matter.");
		ruleAssignment.setMinAmount(new BigDecimal(-300000));
		ruleAssignment.setMaxAmount(new BigDecimal(300000));
		ruleAssignment.setNote("Security Test");
		this.ruleDefinitionService.saveRuleAssignment(ruleAssignment);
		RuleAssignmentSearchForm ruleAssignmentSearchForm = new RuleAssignmentSearchForm();
		ruleAssignmentSearchForm.setNote("Security Test");
		ruleAssignmentSearchForm.setLimit(1);
		List<RuleAssignment> ruleAssignmentList = this.ruleDefinitionService.getRuleAssignmentList(ruleAssignmentSearchForm);
		this.ruleDefinitionService.deleteRuleAssignment(ruleAssignmentList.get(0).getId());
		Assertions.assertTrue(true);
	}
}
