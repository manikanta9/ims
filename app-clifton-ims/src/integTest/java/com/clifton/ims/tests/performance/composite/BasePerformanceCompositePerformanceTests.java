package com.clifton.ims.tests.performance.composite;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.performance.composite.performance.metric.PerformanceMetricFieldTypes;
import com.clifton.performance.composite.performance.metric.PerformanceMetricPeriods;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


/**
 * @author manderson
 */
public abstract class BasePerformanceCompositePerformanceTests extends BaseImsIntegrationTest {

	@Resource
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	@Resource
	private PerformanceCompositeSetupService performanceCompositeSetupService;


	////////////////////////////////////////////////////////////////////////////////
	//////////             Composite Performance Helper Methods           //////////
	////////////////////////////////////////////////////////////////////////////////


	protected PerformanceComposite getCompositeByName(String compositeName) {
		PerformanceCompositeSearchForm searchForm = new PerformanceCompositeSearchForm();
		searchForm.setName(compositeName);
		return CollectionUtils.getOnlyElementStrict(this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm));
	}


	protected PerformanceCompositePerformance getCompositePerformanceByCompositeAndDate(String compositeName, String processDate) {
		return getCompositePerformanceByCompositeAndDate(compositeName, processDate, false);
	}


	protected PerformanceCompositePerformance getCompositePerformanceByCompositeAndDate(String compositeName, String processDate, boolean requirePermissionForDraft) {
		AccountingPeriod processPeriod = this.accountingUtils.getAccountingPeriodForDate(DateUtils.toDate(processDate));
		Assertions.assertNotNull(processPeriod, "Missing accounting period for date " + processDate);
		PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
		searchForm.setPerformanceCompositeName(compositeName);
		searchForm.setAccountingPeriodId(processPeriod.getId());
		if (requirePermissionForDraft) {
			searchForm.setRequireFullControlForDraftStatus(true);
		}
		return CollectionUtils.getFirstElement(BeanUtils.filter(this.performanceCompositePerformanceService.getPerformanceCompositePerformanceList(searchForm), performanceCompositePerformance -> performanceCompositePerformance.getPerformanceComposite().getName(), compositeName));
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////          Composite Account Performance Helper Methods        /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Note: This assumes the account is only assigned to one composite during the period
	 */
	protected PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformance(String compositeCode, String accountNumber, String periodEndDate, boolean skipIfMissing) {
		InvestmentAccount clientAccount = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountUtils.getClientAccountByNumber(accountNumber), 2);
		AccountingPeriod period = this.accountingUtils.getAccountingPeriodForDate(DateUtils.toDate(periodEndDate));

		List<PerformanceCompositeInvestmentAccount> performanceCompositeInvestmentAccountList = this.performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(clientAccount.getId(), period.getId());
		if (!StringUtils.isEmpty(compositeCode)) {
			performanceCompositeInvestmentAccountList = CollectionUtils.getFiltered(this.performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(clientAccount.getId(), period.getId()), compositeAssignment -> StringUtils.isEqualIgnoreCase(compositeCode, compositeAssignment.getPerformanceComposite().getCode()));
		}
		PerformanceCompositeInvestmentAccount accountAssignment = CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(performanceCompositeInvestmentAccountList, PerformanceCompositeInvestmentAccount::getCreateDate, true));
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = null;
		if (accountAssignment != null || !skipIfMissing) {
			Assertions.assertNotNull(accountAssignment, "Cannot find assignment for account " + accountNumber + (StringUtils.isEmpty(compositeCode) ? "" : " and composite code " + compositeCode) + " active during period " + period.getLabel());
			accountPerformance = this.performanceCompositePerformanceService.getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(accountAssignment.getId(), period.getId());
		}

		if (skipIfMissing) {
			return accountPerformance;
		}
		Assertions.assertNotNull(accountPerformance, "Cannot find PerformanceCompositeInvestmentAccountPerformance for account # " + accountNumber + (!StringUtils.isEmpty(compositeCode) ? " and composite " + compositeCode : "") + " and date " + periodEndDate);
		return accountPerformance;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////          Performance Composite Metric Helper Methods         /////////
	////////////////////////////////////////////////////////////////////////////////


	protected void validatePerformanceCompositeMetricValuesEqual(PerformanceCompositeMetric expectedResult, PerformanceCompositeMetric actualResult, Boolean validateMTDOnly, Boolean validateBenchmarksOnly) {
		String messagePrefix = actualResult.getLabel();
		for (PerformanceMetricFieldTypes performanceMetricFieldType : PerformanceMetricFieldTypes.values()) {
			if (validateMetricField(performanceMetricFieldType, validateMTDOnly, validateBenchmarksOnly)) {
				BigDecimal actualValue = (BigDecimal) BeanUtils.getPropertyValue(actualResult, performanceMetricFieldType.getBeanPropertyName());
				BigDecimal previewValue = (BigDecimal) BeanUtils.getPropertyValue(expectedResult, performanceMetricFieldType.getBeanPropertyName());
				Assertions.assertEquals(MathUtils.round(actualValue, 7), MathUtils.round(previewValue, 7), messagePrefix + ": " + performanceMetricFieldType.getBeanPropertyName() + " doesn't match expected");
			}
		}
	}


	/**
	 * @param validateMTDOnly        = true = only MTD values, false = all except MTD values and null = no filter
	 * @param validateBenchmarksOnly = true = only benchmark values, false = all except benchmark values, null = no filter
	 */
	private boolean validateMetricField(PerformanceMetricFieldTypes fieldType, Boolean validateMTDOnly, Boolean validateBenchmarksOnly) {
		if (validateMTDOnly != null) {
			return validateMTDOnly == (fieldType.getPerformanceMetricPeriod() == PerformanceMetricPeriods.MONTH_TO_DATE);
		}
		if (validateBenchmarksOnly != null) {
			return validateBenchmarksOnly == fieldType.isBenchmarkReturn();
		}
		return true;
	}
}
