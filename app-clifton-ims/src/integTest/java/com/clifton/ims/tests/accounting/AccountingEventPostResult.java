package com.clifton.ims.tests.accounting;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.gl.journal.AccountingJournal;


public class AccountingEventPostResult {

	private final AccountingEventJournal eventJournal;
	private final AccountingJournal mainBookedJournal;
	private final AccountingJournal accrualReversalBookedJournal;


	AccountingEventPostResult(AccountingEventJournal eventJournal, AccountingJournal mainBookedJournal, AccountingJournal accrualReversalBookedJournal) {
		this.eventJournal = eventJournal;
		this.mainBookedJournal = mainBookedJournal;
		this.accrualReversalBookedJournal = accrualReversalBookedJournal;
	}


	public AccountingEventJournal getEventJournal() {
		return this.eventJournal;
	}


	public AccountingJournal getMainBookedJournal() {
		return this.mainBookedJournal;
	}


	public AccountingJournal getAccrualReversalBookedJournal() {
		return this.accrualReversalBookedJournal;
	}
}
