package com.clifton.ims.tests.performance.account.process;

import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit.jupiter.DisabledIf;

import java.util.Date;


/**
 * @author manderson
 */
public class PerformanceInvestmentAccountSecurityTargetProcessServiceTests extends BasePerformanceInvestmentAccountProcessServiceTests {

	private static final Date TEST_DATE = DateUtils.toDate("06/30/2019");


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testStockDeltaShiftServiceAccounts() {
		// Account Number W-001671
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("W-001671", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);

		// Account With An assignment (no longer overriding calculation)
		bean = getPerformanceInvestmentAccount("W-012814", DateUtils.toDate("04/30/2019"));
		rebuildSnapshotsForSummary(bean);
		previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}


	@Test
	public void testDynamicPutSellingServiceAccounts() {
		// Account Number W-005678
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("W-005678", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}


	@Test
	public void testOarsServiceAccounts() {
		// Account Number W-000612
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("W-000612", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}


	@Test
	public void testPortfolioDeltaShiftServiceAccounts() {
		// Account Number W-012234
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("W-012234", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}


	@Test
	public void testTaxManagedPortfolioDeltaShiftServiceAccounts() {
		// Account Number W-001952
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("W-001952", TEST_DATE);
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}


	@Test
	@DisabledIf(expression = "#{T(com.clifton.core.util.date.DateUtils).isTodayBefore('01/21/2020')}")
	// TODO REMOVE disabled ANNOTATION AFTER THIS CODE NEEDS TO GET TO PROD AND PERFORMANCE REBUILT FIRST
	public void testDivAndTargetChangeOnSameDate() {
		// Account Number W-001329  PERFORMANC-324
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccount("W-001329", DateUtils.toDate("12/31/2019"));
		rebuildSnapshotsForSummary(bean);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(bean.getId());
		validateSummaryPropertiesEqual(bean, previewBean);
	}
}
