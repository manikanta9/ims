package com.clifton.ims.tests.rule.performance;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.ims.tests.system.query.JdbcUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.violation.RuleViolation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


/**
 * PerformanceCompositeInvestmentAccountPerformanceRuleTests uses preview feature for rule violations on existing composite account performance
 * to validate expected violations and also passed violations across the rule definitions that we have set up.
 */
public class PerformanceCompositeInvestmentAccountPerformanceRuleTests extends RuleTests {

	private static final String RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS = "Client Account Missing Required Base Fields";
	private static final String RULE_DEFINITION_PARTIAL_PERIOD = "Client Account Partial Period";
	private static final String RULE_DEFINITION_POSITION_TRANSFER = "Performance Account: Position Transfer During Period";
	private static final String RULE_DEFINITION_SUB_PERIOD_MISSING_PRICES = "Modified Dietz Monthly Calculator: Missing Prices for Sub Period Evaluations";
	private static final String RULE_DEFINITION_MISSING_BENCHMARK_RETURNS = "Account Performance: Missing Benchmark Period Values";
	private static final String RULE_DEFINITION_MISSING_PREVIOUS_MONTH = "Account Performance: Missing Previous Month Performance Period";
	private static final String RULE_DEFINITION_PREVIOUS_INVALID_STATE = "Account Performance: Previous Performance Invalid State";
	private static final String RULE_DEFINITION_PARTIAL_PERFORMANCE_VALUES = "Account Performance: Partial Performance Values";

	private static final String DE_COMPOSITE_III_CODE = "CGDEFLLCC"; // I.E. USED FOR 800020 ACCOUNT

	@Resource
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	@Resource
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Performance Composite Account Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return CollectionUtils.createHashSet("testProprietaryFundCalculatorConfirmExternalValuesUsedForFinalReturnsRule", "testPreviousMonthEndPositionValueGainLossAdjustmentRule", "testManagementFeeOverrideViolationsRule");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testClientAccountMissingRequiredBaseFieldsRule() {
		validateViolationPassed("078200", "01/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("223500", "08/01/2011", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("078230", "12/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("223500", "09/01/2011", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("455210", "03/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("679500", "04/01/2009", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("522600", "04/01/2009", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("713690", "12/01/2008", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("330000", "10/01/2002", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);

		validateViolationExists("580001", "09/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period Gross Return cannot be null</ul>");

		// These all now pass - there would rarely ever be an exception anymore because positions on date is populated by the system
		validateViolationPassed("078228", "12/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("050901", "12/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("576551", "10/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("668207", "02/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("298150", "02/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("298200", "02/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("298250", "02/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
	}


	@Test
	public void testClientAccountPartialPeriodRule() {
		String measureDate = "12/01/2015";
		String[] passingAccountNumbers = new String[]{"556880", "556800", "391500", "428600", "456000", "456050", "456280", "221770", "495100", "604600", "456300", "800010", "556820", "183050", "663050", "567510", "050905", "800060", "663620", "576610", "567010", "650025", "647000", "647001", "689180", "122511"};

		for (String accountNumber : passingAccountNumbers) {
			validateViolationPassed(accountNumber, measureDate, RULE_DEFINITION_PARTIAL_PERIOD);
		}

		validateViolationPassed(DE_COMPOSITE_III_CODE, "800020", measureDate, RULE_DEFINITION_PARTIAL_PERIOD);

		validateViolationExists("661657", "09/30/2015", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 09/04/2015; ");
		validateViolationExists("456606", "06/30/2015", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 06/30/2015; ");
		validateViolationExists("122506", "06/30/2015", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 06/15/2015; ");
		validateViolationExists("297150", "03/31/2014", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 03/19/2014; ");
		validateViolationExists("689180", "03/31/2013", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 03/28/2013; ");
		validateViolationExists("647000", "12/31/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 12/20/2012; ");
		validateViolationExists("647001", "12/31/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 12/20/2012; ");
		// No longer valid because 12/3 is the first business day of the month validateViolationExists("650025", "12/31/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 12/03/2012; ");
		validateViolationExists("576610", "11/30/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 11/15/2012; ");
		validateViolationExists("663620", "10/31/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 10/11/2012; ");
		validateViolationExists(null, "800020", "01/31/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 01/20/2012; ");
		validateViolationExists("800020-1", "01/31/2012", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 01/20/2012; ");
		validateViolationExists("183050", "08/31/2011", RULE_DEFINITION_PARTIAL_PERIOD, "Positions On: 08/08/2011; ");
	}


	@Test
	public void testPerformanceAccountPositionTransferDuringPeriodRule() {
		// Violation Exists - Transfer to AccountHouston Police Officers
		validateViolationExists("310501", "07/31/2013", RULE_DEFINITION_POSITION_TRANSFER, "Position Transfer From 310500: .* to 310501: .* on 07/08/2013:<br/><span class='amountPositive'>1,448,000.00</span> of 912810FR4 .*<br/><span class='amountPositive'>1,326,000.00</span> of 912810FR4 .*<br/>");

		// No Position Transfers
		validateViolationPassed("050905", "01/31/2016", RULE_DEFINITION_POSITION_TRANSFER);
	}


	@Test
	public void testModifiedDietzMonthlyCalculatorMissingPricesForSubPeriodEvaluationsRule() {
		// NOTE: Until the Modified Dietz Monthly Calculator is used in IMS to generate the account performance, this rule won't evaluate anything
		// Will need to add more tests, especially once we run into a mid month period where prices need to be loaded for something other than month end
		validateViolationPassed("456280", "01/31/2016", RULE_DEFINITION_SUB_PERIOD_MISSING_PRICES);
		validateViolationPassed("456280", "02/29/2016", RULE_DEFINITION_SUB_PERIOD_MISSING_PRICES);
		validateViolationPassed("456300", "08/28/2020", RULE_DEFINITION_SUB_PERIOD_MISSING_PRICES);
	}


	@Test
	public void testAccountPerformanceMissingBenchmarkPeriodValuesRule() {
		// Account Performance with no benchmarks
		validateViolationPassed("606550", "06/30/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS);

		// Account Performance with only a risk free return
		validateViolationPassed("183050", "06/30/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS);

		// Account Performance with only a benchmark and risk free return - OK that others are missing
		validateViolationPassed("221770", "06/30/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS);

		// This should never actually happen - so take a terminated account's performance, manually clear the value and then preview rules
		// Running this through DB directly
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = getCompositePerformanceInvestmentAccountPerformanceForAccountAndDate(null, "567510", "02/29/2016");
		if (accountPerformance.getPeriodBenchmarkReturn() != null) {
			JdbcUtils.executeQuery("UPDATE PerformanceCompositeInvestmentAccountPerformance SET PeriodBenchmarkReturn = NULL, PeriodRiskFreeSecurityReturn = NULL WHERE PerformanceCompositeInvestmentAccountPerformanceID = " + accountPerformance.getId());
			this.systemRequestStatsService.deleteSystemCache("com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance");
		}
		validateViolationExists("567510", "02/29/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS, "Missing Period Return for Investment Security LD12TRUU \\(Bloomberg Barclays U.S. Tr Bills: 1-3 Months TR Index Value Unhedged USD\\)");
		validateViolationExists("567510", "02/29/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS, "Missing Period Return for Investment Security SJCERA Custom Contraction Benchmark");
	}


	@Test
	public void testAccountPerformanceMissingPreviousMonthPerformancePeriodRule() {
		validateViolationExists("221700", "10/31/2011", RULE_DEFINITION_MISSING_PREVIOUS_MONTH, "Missing Previous Months Performance for this Account.  Performance Inception Date will be reset.");
		validateViolationExists("447700", "09/30/2015", RULE_DEFINITION_MISSING_PREVIOUS_MONTH, "Missing Previous Months Performance for this Account.  Performance Inception Date will be reset.");

		validateViolationPassed("441000", "01/31/2016", RULE_DEFINITION_MISSING_PREVIOUS_MONTH);

		// First Month for the Account
		validateViolationPassed(null, "800020", "01/31/2012", RULE_DEFINITION_MISSING_PREVIOUS_MONTH);
	}


	@Test
	public void testAccountPerformancePreviousPerformanceInvalidStateRule() {
		validateViolationPassed(null, "800020", "01/31/2012", RULE_DEFINITION_PREVIOUS_INVALID_STATE);
		validateViolationPassed(DE_COMPOSITE_III_CODE, "800020", "10/31/2015", RULE_DEFINITION_PREVIOUS_INVALID_STATE);

		// Move One Back to Draft, and then Preview Rules on a Later One
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = getCompositePerformanceInvestmentAccountPerformanceForAccountAndDate(DE_COMPOSITE_III_CODE, "800020", "10/31/2015");
		this.workflowTransitioner.transitionForTests("Return for Edits", "Draft", "PerformanceCompositeInvestmentAccountPerformance", BeanUtils.getIdentityAsLong(accountPerformance));

		validateViolationPassed(DE_COMPOSITE_III_CODE, "800020", "10/31/2015", RULE_DEFINITION_PREVIOUS_INVALID_STATE);
		validateViolationExists(DE_COMPOSITE_III_CODE, "800020", "11/30/2015", RULE_DEFINITION_PREVIOUS_INVALID_STATE, "800020: .*: 10/2015 is in Draft state.");
		validateViolationExists(DE_COMPOSITE_III_CODE, "800020", "06/30/2016", RULE_DEFINITION_PREVIOUS_INVALID_STATE, "800020: .*: 10/2015 is in Draft state.");
	}


	@Test
	public void testAccountPerformancePartialPerformanceValuesRule() {
		validateViolationPassed(DE_COMPOSITE_III_CODE, "800020", "05/31/2019", RULE_DEFINITION_PARTIAL_PERFORMANCE_VALUES);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Helper Methods                 ////////////////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Composite Code can be used for accounts that have multiple assignments for a period
	 */
	private PerformanceCompositeInvestmentAccountPerformance getCompositePerformanceInvestmentAccountPerformanceForAccountAndDate(String compositeCode, String accountNumber, String measureDate) {
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
		AccountingPeriod period = this.accountingUtils.getAccountingPeriodForDate(DateUtils.toDate(measureDate));

		List<PerformanceCompositeInvestmentAccount> performanceCompositeInvestmentAccountList = this.performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(clientAccount.getId(), period.getId());
		if (!StringUtils.isEmpty(compositeCode)) {
			performanceCompositeInvestmentAccountList = CollectionUtils.getFiltered(this.performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(clientAccount.getId(), period.getId()), compositeAssignment -> StringUtils.isEqualIgnoreCase(compositeCode, compositeAssignment.getPerformanceComposite().getCode()));
		}
		PerformanceCompositeInvestmentAccount accountAssignment = CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(performanceCompositeInvestmentAccountList, PerformanceCompositeInvestmentAccount::getCreateDate, true));
		Assertions.assertNotNull(accountAssignment, "Cannot find assignment for account " + accountNumber + (StringUtils.isEmpty(compositeCode) ? "" : " and composite code " + compositeCode) + " active during period " + period.getLabel());
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = this.performanceCompositePerformanceService.getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(accountAssignment.getId(), period.getId());
		Assertions.assertNotNull(accountPerformance, "Cannot find PerformanceCompositeInvestmentAccountPerformance for account # " + accountNumber + (!StringUtils.isEmpty(compositeCode) ? " and composite " + compositeCode : "") + " and date " + measureDate);
		return accountPerformance;
	}

	/////////////////////////////////////////////////////////////////////////////
	///////////                Validation Methods                 ///////////////
	/////////////////////////////////////////////////////////////////////////////


	private void validateViolationExists(String accountNumber, String measureDate, String ruleDefinitionName, String violationNoteExpected) {
		validateViolationExists(null, accountNumber, measureDate, ruleDefinitionName, violationNoteExpected);
	}


	private void validateViolationExists(String compositeCode, String accountNumber, String measureDate, String ruleDefinitionName, String violationNoteExpected) {
		validateViolation(getCompositePerformanceInvestmentAccountPerformanceForAccountAndDate(compositeCode, accountNumber, measureDate), this.ruleDefinitionService.getRuleDefinitionByName(ruleDefinitionName), true, null, violationNoteExpected);
	}


	private void validateViolationPassed(String accountNumber, String measureDate, String ruleDefinitionName) {
		validateViolationPassed(null, accountNumber, measureDate, ruleDefinitionName);
	}


	private void validateViolationPassed(String compositeCode, String accountNumber, String measureDate, String ruleDefinitionName) {
		validateViolationPassed(compositeCode, accountNumber, measureDate, ruleDefinitionName, null);
	}


	private void validateViolationPassed(String compositeCode, String accountNumber, String measureDate, String ruleDefinitionName, String ignoreCauseTableName) {
		validateViolation(getCompositePerformanceInvestmentAccountPerformanceForAccountAndDate(compositeCode, accountNumber, measureDate), this.ruleDefinitionService.getRuleDefinitionByName(ruleDefinitionName), false, ignoreCauseTableName);
	}


	/**
	 * Validates Violation with Violation Note exists or not in the Preview list
	 */
	private void validateViolation(PerformanceAccountInfo performanceAccountInfo, RuleDefinition ruleDefinition, boolean exists, String ignoreCauseTableName, String... violationNoteExpected) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(ruleDefinition.getRuleCategory().getCategoryTable().getName(), performanceAccountInfo.getId());
		String definitionName = ruleDefinition.getName();
		if (exists) {
			for (String expectedViolation : violationNoteExpected) {
				Assertions.assertFalse(CollectionUtils.isEmpty(BeanUtils.filter(violationList, violation -> violation.getViolationNote().matches(expectedViolation))), "Did not find Expected Violation with Message: " + expectedViolation + " for Performance " + performanceAccountInfo.getLabel() + " for Rule Definition [" + ruleDefinition.getName() + "] with Category Table Name [" + ruleDefinition.getRuleCategory().getCategoryTable() + "]");
			}
		}
		else {
			RuleViolation firstRuleViolation = null;
			for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
				if (definitionName.equals(violation.getRuleAssignment().getRuleDefinition().getName()) && !"Passed".equals(violation.getIgnoreNote())) {
					if (violation.getCauseTable() == null || ignoreCauseTableName == null || (!ignoreCauseTableName.equals(violation.getCauseTable().getName()))) {
						firstRuleViolation = violation;
						break;
					}
				}
			}
			Assertions.assertNull(firstRuleViolation, "Rule Definition: " + definitionName + " for Performance " + performanceAccountInfo.getLabel() + " should not have been executed, but it did");
		}
	}
}
