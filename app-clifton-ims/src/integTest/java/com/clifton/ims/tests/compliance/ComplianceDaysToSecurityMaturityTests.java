package com.clifton.ims.tests.compliance;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;


public class ComplianceDaysToSecurityMaturityTests extends ComplianceRealTimeTradeTests {


	@Test
	public void testCurrencyForward90Max() {
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BARCLAYS_BANK_PLC);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FORWARDS, false, BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, "Permitted Security: Forwards (Currency) - All (Long or Short)", "Days to Maturity Threshold: Forwards (Currency - All) - (0-90 Days)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FORWARDS, "CNH/USD20170228", new BigDecimal(123), true, DateUtils.toDate("2/10/2016"), executingBroker);
		validateViolationNotes(tradeResults, "Days to Maturity Threshold: Forwards (Currency - All) - (0-90 Days) : Days to Maturity for Security [CNH/USD20170228 (CNH/USD Curr Fwd 02/28/2017) [INACTIVE]] is [384] Days which Violates Rule Max [90] : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FORWARDS, "CNH/USD20170228", new BigDecimal(123), true, DateUtils.toDate("02/15/2017"), executingBroker);
		validateViolationNotes(tradeResults);
	}
}
