package com.clifton.ims.tests.builders.investment.account;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.service.BusinessService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountBusinessService;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityGroup;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public class InvestmentAccountBuilder {

	private final InvestmentAccountService investmentAccountService;

	private InvestmentAccountType type;
	private BusinessCompany issuingCompany;
	private BusinessClient businessClient;
	private BusinessContract businessContract;
	private InvestmentSecurity baseCurrency;
	private BusinessService businessService;
	private List<InvestmentAccountBusinessService> businessServiceList;
	private SecurityGroup teamSecurityGroup;
	private String number;
	private String shortName;
	private boolean usedForSpeculation;
	private Date inceptionDate; // AKA Positions On Date
	private Date performanceInceptionDate; // Usually = Positions On Date, but When we started tracking performance for the account
	private String clientAccountType;
	private SystemListItem externalPortfolioCode;
	private SystemListItem externalProductCode;
	private String discretionType;
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;
	private String description;
	private String name;
	private String label;
	private WorkflowStatus workflowStatus;
	private WorkflowState workflowState;
	private Date workflowStateEffectiveStartDate;
	private BusinessCompany defaultExchangeRateSourceCompany;


	private InvestmentAccountBuilder(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	//TODO - defaults?
	public static InvestmentAccountBuilder investmentAccount(InvestmentAccountService investmentAccountService) {
		return new InvestmentAccountBuilder(investmentAccountService);
	}


	public InvestmentAccount build() {
		InvestmentAccount investmentAccount = new InvestmentAccount();
		investmentAccount.setType(this.type);
		investmentAccount.setIssuingCompany(this.issuingCompany);
		investmentAccount.setBusinessClient(this.businessClient);
		investmentAccount.setBusinessContract(this.businessContract);
		investmentAccount.setBaseCurrency(this.baseCurrency);
		investmentAccount.setBusinessService(this.businessService);
		investmentAccount.setBusinessServiceList(this.businessServiceList);
		investmentAccount.setTeamSecurityGroup(this.teamSecurityGroup);
		investmentAccount.setNumber(this.number);
		investmentAccount.setShortName(this.shortName);
		investmentAccount.setUsedForSpeculation(this.usedForSpeculation);
		investmentAccount.setInceptionDate(this.inceptionDate);
		investmentAccount.setPerformanceInceptionDate(this.performanceInceptionDate);
		investmentAccount.setClientAccountType(this.clientAccountType);
		investmentAccount.setExternalPortfolioCode(this.externalPortfolioCode);
		investmentAccount.setExternalProductCode(this.externalProductCode);
		investmentAccount.setDiscretionType(this.discretionType);
		investmentAccount.setColumnGroupName(this.columnGroupName);
		investmentAccount.setColumnValueList(this.columnValueList);
		investmentAccount.setDescription(this.description);
		investmentAccount.setName(this.name);
		investmentAccount.setLabel(this.label);
		investmentAccount.setWorkflowStatus(this.workflowStatus);
		investmentAccount.setWorkflowState(this.workflowState);
		investmentAccount.setWorkflowStateEffectiveStartDate(this.workflowStateEffectiveStartDate);
		investmentAccount.setDefaultExchangeRateSourceCompany(this.defaultExchangeRateSourceCompany);
		return investmentAccount;
	}


	public InvestmentAccount buildAndSave() {
		InvestmentAccount investmentAccount = build();
		return this.investmentAccountService.saveInvestmentAccount(investmentAccount);
	}


	public InvestmentAccountBuilder but() {
		return investmentAccount(this.investmentAccountService)
				.withType(this.type)
				.withIssuingCompany(this.issuingCompany)
				.withBusinessClient(this.businessClient)
				.withBusinessContract(this.businessContract)
				.withBaseCurrency(this.baseCurrency)
				.withBusinessService(this.businessService)
				.withBusinessServiceList(this.businessServiceList)
				.withTeamSecurityGroup(this.teamSecurityGroup)
				.withNumber(this.number)
				.withShortName(this.shortName)
				.withUsedForSpeculation(this.usedForSpeculation)
				.withInceptionDate(this.inceptionDate)
				.withPerformanceInceptionDate(this.performanceInceptionDate)
				.withClientAccountType(this.clientAccountType)
				.withExternalPortfolioCode(this.externalPortfolioCode)
				.withExternalProductCode(this.externalProductCode)
				.withDiscretionType(this.discretionType)
				.withColumnGroupName(this.columnGroupName)
				.withColumnValueList(this.columnValueList)
				.withDescription(this.description)
				.withName(this.name)
				.withLabel(this.label)
				.withWorkflowStatus(this.workflowStatus)
				.withWorkflowState(this.workflowState)
				.withWorkflowStateEffectiveStartDate(this.workflowStateEffectiveStartDate)
				.withDefaultExchangeRateSourceCompany(this.defaultExchangeRateSourceCompany);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountBuilder withType(InvestmentAccountType type) {
		this.type = type;
		return this;
	}


	public InvestmentAccountBuilder withIssuingCompany(BusinessCompany issuingCompany) {
		this.issuingCompany = issuingCompany;
		return this;
	}


	public InvestmentAccountBuilder withBusinessClient(BusinessClient businessClient) {
		this.businessClient = businessClient;
		return this;
	}


	public InvestmentAccountBuilder withBusinessContract(BusinessContract businessContract) {
		this.businessContract = businessContract;
		return this;
	}


	public InvestmentAccountBuilder withBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
		return this;
	}


	public InvestmentAccountBuilder withBusinessService(BusinessService businessService) {
		this.businessService = businessService;
		return this;
	}


	public InvestmentAccountBuilder withBusinessServiceList(List<InvestmentAccountBusinessService> businessServiceList) {
		this.businessServiceList = businessServiceList;
		return this;
	}


	public InvestmentAccountBuilder withTeamSecurityGroup(SecurityGroup teamSecurityGroup) {
		this.teamSecurityGroup = teamSecurityGroup;
		return this;
	}


	public InvestmentAccountBuilder withNumber(String number) {
		this.number = number;
		return this;
	}


	public InvestmentAccountBuilder withShortName(String shortName) {
		this.shortName = shortName;
		return this;
	}


	public InvestmentAccountBuilder withUsedForSpeculation(boolean usedForSpeculation) {
		this.usedForSpeculation = usedForSpeculation;
		return this;
	}


	public InvestmentAccountBuilder withInceptionDate(Date inceptionDate) {
		this.inceptionDate = inceptionDate;
		return this;
	}


	public InvestmentAccountBuilder withPerformanceInceptionDate(Date performanceInceptionDate) {
		this.performanceInceptionDate = performanceInceptionDate;
		return this;
	}


	public InvestmentAccountBuilder withClientAccountType(String clientAccountType) {
		this.clientAccountType = clientAccountType;
		return this;
	}


	public InvestmentAccountBuilder withExternalPortfolioCode(SystemListItem externalPortfolioCode) {
		this.externalPortfolioCode = externalPortfolioCode;
		return this;
	}


	public InvestmentAccountBuilder withExternalProductCode(SystemListItem externalProductCode) {
		this.externalProductCode = externalProductCode;
		return this;
	}


	public InvestmentAccountBuilder withDiscretionType(String discretionType) {
		this.discretionType = discretionType;
		return this;
	}


	public InvestmentAccountBuilder withColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
		return this;
	}


	public InvestmentAccountBuilder withColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
		return this;
	}


	public InvestmentAccountBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public InvestmentAccountBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public InvestmentAccountBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public InvestmentAccountBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
		return this;
	}


	public InvestmentAccountBuilder withWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
		return this;
	}


	public InvestmentAccountBuilder withWorkflowStateEffectiveStartDate(Date workflowStateEffectiveStartDate) {
		this.workflowStateEffectiveStartDate = workflowStateEffectiveStartDate;
		return this;
	}


	public InvestmentAccountBuilder withDefaultExchangeRateSourceCompany(BusinessCompany exchangeRateSourceCompany) {
		this.defaultExchangeRateSourceCompany = exchangeRateSourceCompany;
		return this;
	}
}
