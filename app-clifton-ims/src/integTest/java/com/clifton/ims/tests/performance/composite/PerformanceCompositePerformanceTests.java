package com.clifton.ims.tests.performance.composite;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceMetricFieldTypes;
import com.clifton.performance.composite.performance.metric.PerformanceMetricPeriods;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeRebuildCommand;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import com.clifton.test.util.RandomUtils;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositePerformanceTests</code> test various composite
 * performance calculations.
 * <p>
 *
 * @author manderson
 */

public class PerformanceCompositePerformanceTests extends BasePerformanceCompositePerformanceTests {

	@Resource
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	@Resource
	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;

	@Resource
	private PerformanceCompositeSetupService performanceCompositeSetupService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSecurityForDraftPerformance() {
		PerformanceComposite testComposite = BeanUtils.cloneBean(getCompositeByName("PIOS Composite"), false, false);
		testComposite.setName("Test Composite - " + RandomUtils.randomName());
		testComposite.setCode(RandomUtils.randomNumber() + "");
		testComposite.setStartDate(DateUtils.addMonths(DateUtils.getFirstDayOfMonth(new Date()), -1));
		testComposite.setEndDate(null);
		this.performanceCompositeSetupService.savePerformanceComposite(testComposite);

		// Create a Performance Composite Performance Record
		PerformanceCompositePerformance testCompositePerformance = new PerformanceCompositePerformance();
		testCompositePerformance.setPerformanceComposite(testComposite);
		testCompositePerformance.setAccountingPeriod(this.accountingUtils.getAccountingPeriodForDate(DateUtils.getLastDayOfPreviousMonth(new Date())));
		this.performanceCompositePerformanceService.savePerformanceCompositePerformance(testCompositePerformance);

		// Should be in draft status now
		String testDate = DateUtils.fromDate(testCompositePerformance.getAccountingPeriod().getEndDate());
		testCompositePerformance = getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate);
		Assertions.assertEquals(WorkflowStatus.STATUS_DRAFT, testCompositePerformance.getWorkflowStatus().getName());

		// Current User Is Admin, so should always be available
		Assertions.assertNotNull(getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate, false));
		Assertions.assertNotNull(getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate, true));

		// Switch to Another User THAT DOESN'T HAVE FULL CONTROL
		this.userUtils.switchUser(ImsTestProperties.TEST_ANALYST_USER, ImsTestProperties.TEST_ANALYST_USER_PASSWORD);

		Assertions.assertNotNull(getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate, false));
		Assertions.assertNull(getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate, true));

		try {
			// Add the User to Performance Admins Group so they now have permission
			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
			this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_ANALYST_USER, "Performance Admins");
			this.userUtils.switchUser(ImsTestProperties.TEST_ANALYST_USER, ImsTestProperties.TEST_ANALYST_USER_PASSWORD);

			// Now user should have permission
			Assertions.assertNotNull(getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate, false));
			Assertions.assertNotNull(getCompositePerformanceByCompositeAndDate(testComposite.getName(), testDate, true));
		}

		finally {
			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
			// Clean Up: Remove User from Group
			this.userUtils.removeUserFromSecurityGroup(ImsTestProperties.TEST_ANALYST_USER, "Performance Admins");
		}
	}


	/**
	 * This test compares results for composites for year of 2015
	 * The purpose of the test is to verify the accounts that are included in the results, therefore it just validates Market Value and Gross MTD Return
	 */
	@Test
	public void testCompositePerformance_CompositeCalculation() {
		List<PerformanceComposite> compositeList = this.performanceCompositeSetupService.getPerformanceCompositeList(new PerformanceCompositeSearchForm());
		Date startDate = DateUtils.toDate("01/01/2015");
		Date endDate = DateUtils.toDate("12/31/2015");


		for (PerformanceComposite performanceComposite : compositeList) {
			if ("PIOS Composite".equalsIgnoreCase(performanceComposite.getName()) || "LDI Composite".equalsIgnoreCase(performanceComposite.getName())) {
				continue;
			}
			if (DateUtils.isOverlapInDates(startDate, endDate, performanceComposite.getStartDate(), performanceComposite.getEndDate())) {
				Date compositeStartDate = startDate;
				Date compositeEndDate = endDate;
				if (DateUtils.isDateAfter(performanceComposite.getStartDate(), startDate)) {
					compositeStartDate = performanceComposite.getStartDate();
				}
				if (performanceComposite.getEndDate() != null && DateUtils.isDateAfter(endDate, performanceComposite.getEndDate())) {
					compositeEndDate = performanceComposite.getEndDate();
				}
				Date date = startDate;
				while (DateUtils.isDateBetween(date, compositeStartDate, compositeEndDate, false)) {
					// Skip if it doesn't exist or hasn't been approved
					PerformanceCompositePerformance actualCompositePerformance = getCompositePerformanceByCompositeAndDate(performanceComposite.getName(), DateUtils.fromDateShort(date));
					if (actualCompositePerformance != null && WorkflowStatus.STATUS_APPROVED.equals(actualCompositePerformance.getWorkflowStatus().getName())) {
						// Preview New Performance Composite Result
						PerformanceCompositePerformance compositePerformance = previewCompositePerformanceByCompositeAndDate(actualCompositePerformance);

						String messagePrefix = actualCompositePerformance.getLabel();

						// Validate Market Value and Gross MTD
						Assertions.assertEquals(MathUtils.round(actualCompositePerformance.getPeriodEndMarketValue(), 0), MathUtils.round(compositePerformance.getPeriodEndMarketValue(), 0), messagePrefix + ": Market Value doesn't match expected");
						Assertions.assertEquals(MathUtils.round(actualCompositePerformance.getPeriodGrossReturn(), 6), MathUtils.round(compositePerformance.getPeriodGrossReturn(), 6), messagePrefix + ": Gross MTD Return doesn't match expected");
					}
					date = DateUtils.addMonths(date, 1);
				}
			}
		}
	}


	@Test
	public void testCompositePerformance_DefensiveEquityCompositeII() {
		String compositeName = "Defensive Equity Composite II";

		validateCompositePerformance(compositeName, "09/01/2011");
		validateCompositePerformance(compositeName, "10/01/2011");
		validateCompositePerformance(compositeName, "11/01/2011");
		validateCompositePerformance(compositeName, "12/01/2011");
		validateCompositePerformance(compositeName, "01/01/2012");
		validateCompositePerformance(compositeName, "02/01/2012");
		validateCompositePerformance(compositeName, "03/01/2012");
		validateCompositePerformance(compositeName, "04/01/2012");
		validateCompositePerformance(compositeName, "05/01/2012");
		validateCompositePerformance(compositeName, "06/01/2012");
		validateCompositePerformance(compositeName, "07/01/2012");
		validateCompositePerformance(compositeName, "08/01/2012");
		validateCompositePerformance(compositeName, "09/01/2012");
		validateCompositePerformance(compositeName, "10/01/2012");
		// Break in Performance - Reset Inception Date
		validateCompositePerformance(compositeName, "01/01/2013");
		validateCompositePerformance(compositeName, "02/01/2013");
		validateCompositePerformance(compositeName, "03/01/2013");
		validateCompositePerformance(compositeName, "04/01/2013");
		validateCompositePerformance(compositeName, "05/01/2013");
		validateCompositePerformance(compositeName, "06/01/2013");
		validateCompositePerformance(compositeName, "07/01/2013");
		validateCompositePerformance(compositeName, "08/01/2013");
		validateCompositePerformance(compositeName, "09/01/2013");
		validateCompositePerformance(compositeName, "10/01/2013");
		validateCompositePerformance(compositeName, "11/01/2013");
		validateCompositePerformance(compositeName, "12/01/2013");
		validateCompositePerformance(compositeName, "01/01/2014");
		validateCompositePerformance(compositeName, "02/01/2014");
		validateCompositePerformance(compositeName, "03/01/2014");
		validateCompositePerformance(compositeName, "04/01/2014");
		validateCompositePerformance(compositeName, "05/01/2014");
		validateCompositePerformance(compositeName, "06/01/2014");
		validateCompositePerformance(compositeName, "07/01/2014");
		validateCompositePerformance(compositeName, "08/01/2014");
		validateCompositePerformance(compositeName, "09/01/2014");
		validateCompositePerformance(compositeName, "10/01/2014");
		validateCompositePerformance(compositeName, "11/01/2014");
		validateCompositePerformance(compositeName, "12/01/2014");
		validateCompositePerformance(compositeName, "01/01/2015");
		validateCompositePerformance(compositeName, "02/01/2015");
		validateCompositePerformance(compositeName, "03/01/2015");
		validateCompositePerformance(compositeName, "04/01/2015");
		validateCompositePerformance(compositeName, "05/01/2015");
		validateCompositePerformance(compositeName, "06/01/2015");
		validateCompositePerformance(compositeName, "07/01/2015");
		validateCompositePerformance(compositeName, "08/01/2015");
		validateCompositePerformance(compositeName, "09/01/2015");
		validateCompositePerformance(compositeName, "10/01/2015");
		validateCompositePerformance(compositeName, "11/01/2015");
		validateCompositePerformance(compositeName, "12/01/2015");
		validateCompositePerformance(compositeName, "01/01/2016");
		validateCompositePerformance(compositeName, "02/01/2016");
		validateCompositePerformance(compositeName, "03/01/2016");
		validateCompositePerformance(compositeName, "04/01/2016");
		validateCompositePerformance(compositeName, "05/01/2016");
		validateCompositePerformance(compositeName, "06/01/2016");

		// Partial Period Check: Accounts 524102 and 524002 both started mid December
		validateCompositePerformance(compositeName, "12/01/2016");
	}


	@Test
	public void testCompositePerformance_Rollup_BalancedRiskCompositeII() {
		String compositeName = "Balanced Risk Composite II";
		validateCompositePerformance(compositeName, "04/01/2012");
		validateCompositePerformance(compositeName, "05/01/2012");
		validateCompositePerformance(compositeName, "06/01/2012");
		validateCompositePerformance(compositeName, "07/01/2012");
		validateCompositePerformance(compositeName, "08/01/2012");
		validateCompositePerformance(compositeName, "09/01/2012");
		validateCompositePerformance(compositeName, "10/01/2012");
		validateCompositePerformance(compositeName, "11/01/2012");
		validateCompositePerformance(compositeName, "12/01/2012");
		validateCompositePerformance(compositeName, "01/01/2013");
		validateCompositePerformance(compositeName, "02/01/2013");
		validateCompositePerformance(compositeName, "03/01/2013");
		validateCompositePerformance(compositeName, "04/01/2013");
		validateCompositePerformance(compositeName, "05/01/2013");
		validateCompositePerformance(compositeName, "06/01/2013");
		validateCompositePerformance(compositeName, "07/01/2013");
		validateCompositePerformance(compositeName, "08/01/2013");
		validateCompositePerformance(compositeName, "09/01/2013");
		validateCompositePerformance(compositeName, "10/01/2013");
		validateCompositePerformance(compositeName, "11/01/2013");
		validateCompositePerformance(compositeName, "12/01/2013");
		validateCompositePerformance(compositeName, "01/01/2014");
	}


	/**
	 * Need to ignore this test until history is fixed.  Sent Lucas a list to review
	 */
	@Disabled
	@Test
	public void testCompositePerformance_LDIComposite() {
		String compositeName = "LDI Composite";
		validateCompositePerformance(compositeName, "10/01/2015");
		validateCompositePerformance(compositeName, "11/01/2015");
		validateCompositePerformance(compositeName, "12/01/2015");
		validateCompositePerformance(compositeName, "01/01/2016");
		validateCompositePerformance(compositeName, "02/01/2016");
	}


	/**
	 * Ignoring this test for now - accounting is working on changes that need to be addressed historically
	 */
	@Disabled
	@Test
	public void testCompositePerformance_ModelFee_UseModelNetReturns() {
		String compositeName = "Legacy Composite";
		validateCompositePerformance(compositeName, "10/01/2015");
		validateCompositePerformance(compositeName, "11/01/2015");
		validateCompositePerformance(compositeName, "12/01/2015");
		validateCompositePerformance(compositeName, "01/01/2016");
		validateCompositePerformance(compositeName, "02/01/2016");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateCompositePerformance(String compositeName, String processDate) {
		validateCompositePerformance(compositeName, processDate, false);
	}


	private void validateCompositePerformance(String compositeName, String processDate, boolean validateMTDOnly) {
		// Ensure performance exists
		PerformanceCompositePerformance actualCompositePerformance = getCompositePerformanceByCompositeAndDate(compositeName, processDate);
		ValidationUtils.assertNotNull(actualCompositePerformance, "Cannot find [" + compositeName + "] performance for [" + processDate + "]");

		// Preview New Performance Composite Result
		PerformanceCompositePerformance compositePerformance = previewCompositePerformanceByCompositeAndDate(actualCompositePerformance);

		String messagePrefix = actualCompositePerformance.getLabel();
		for (PerformanceMetricFieldTypes performanceMetricFieldType : PerformanceMetricFieldTypes.values()) {
			if (!validateMTDOnly || performanceMetricFieldType.getPerformanceMetricPeriod() == PerformanceMetricPeriods.MONTH_TO_DATE) {
				// Once History is filled in will need to verify these values
				if (PerformanceMetricFieldTypes.PERIOD_MODEL_NET_RETURN != performanceMetricFieldType.getDependentFieldType() && PerformanceMetricFieldTypes.PERIOD_MODEL_NET_RETURN != performanceMetricFieldType.getDependentFieldType().getDependentFieldType()) {
					BigDecimal actualValue = (BigDecimal) BeanUtils.getPropertyValue(actualCompositePerformance, performanceMetricFieldType.getBeanPropertyName());
					BigDecimal previewValue = (BigDecimal) BeanUtils.getPropertyValue(compositePerformance, performanceMetricFieldType.getBeanPropertyName());
					Assertions.assertEquals(MathUtils.round(actualValue, 7), MathUtils.round(previewValue, 7), messagePrefix + ": " + performanceMetricFieldType.getBeanPropertyName() + " doesn't match expected");
				}
			}
		}
	}


	private PerformanceCompositePerformance previewCompositePerformanceByCompositeAndDate(PerformanceCompositePerformance compositePerformance) {
		PerformanceCompositeRebuildCommand command = new PerformanceCompositeRebuildCommand();
		command.setPerformanceCompositeId(compositePerformance.getPerformanceComposite().getId());
		command.setToAccountingPeriod(compositePerformance.getAccountingPeriod());
		command.setSynchronous(true);
		command.setPreview(true);
		return this.performanceCompositePerformanceRebuildService.previewPerformanceCompositePerformance(command);
	}
}
