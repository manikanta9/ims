package com.clifton.ims.tests.investment.instrument;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.UUID;


/**
 * The <code>InvestmentInstrumentServiceImplTests</code> ...
 *
 * @author manderson
 */

public class InvestmentInstrumentServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentSetupService investmentSetupService;


	/////////////////////////////////////////////////////////////////////
	// Create/Update/Delete Instruments and Securities Test
	/////////////////////////////////////////////////////////////////////


	@Test
	public void testCreateUpdateDeleteOneToOneInstrument() {
		// Use Existing Benchmarks / Indices / Listed / Priced Daily: LUAATRUU as template
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("LUAATRUU");

		InvestmentSecurity newSecurity = copyAndValidateSecurity(security);
		Integer newSecurityId = newSecurity.getId();
		newSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(newSecurity.getSymbol());
		Assertions.assertEquals(newSecurityId, newSecurity.getId());

		String newName = newSecurity.getName() + "TEST UPDATE";
		newSecurity.setName(newName);
		newSecurity = this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);
		Assertions.assertEquals(newName, newSecurity.getName());

		this.investmentInstrumentService.deleteInvestmentInstrument(newSecurity.getInstrument().getId());
		Assertions.assertNull(this.investmentInstrumentService.getInvestmentSecurity(newSecurityId));
	}


	@Test
	public void testCreateCurrency() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true);

		InvestmentSecurity newSecurity = copyAndValidateSecurity(security);
		Integer newSecurityId = newSecurity.getId();
		newSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(newSecurity.getSymbol());
		Assertions.assertEquals(newSecurityId, newSecurity.getId());
		// unable to delete currency due to circular dependency of instrument trading currency, not fixing for test
	}


	@Test
	public void testCreateUpdateDeleteSecurityWithUnderlyingSecurity() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AACAY");

		InvestmentSecurity newSecurity = copyAndValidateSecurity(security);
		Assertions.assertNotNull(newSecurity.getUnderlyingSecurity());
		Integer newSecurityId = newSecurity.getId();
		newSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(newSecurity.getSymbol());
		Assertions.assertEquals(newSecurityId, newSecurity.getId());

		InvestmentSecurity underlyingSecurity = newSecurity.getUnderlyingSecurity();
		newSecurity.setUnderlyingSecurity(null);
		newSecurity = this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);
		// underlying should remain unchanged from the instrument
		Assertions.assertEquals(underlyingSecurity, newSecurity.getUnderlyingSecurity());
		Assertions.assertEquals(underlyingSecurity.getInstrument(), newSecurity.getInstrument().getUnderlyingInstrument());

		newSecurity.setUnderlyingSecurity(null);
		newSecurity.setUnderlyingSecurityFromSecurityUsed(true);
		newSecurity = this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);
		Assertions.assertNull(newSecurity.getUnderlyingSecurity());
		Assertions.assertNull(newSecurity.getInstrument().getUnderlyingInstrument());

		newSecurity.setUnderlyingSecurity(underlyingSecurity);
		newSecurity.setUnderlyingSecurityFromSecurityUsed(true);
		newSecurity = this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);
		Assertions.assertEquals(underlyingSecurity, newSecurity.getUnderlyingSecurity());
		Assertions.assertEquals(underlyingSecurity.getInstrument(), newSecurity.getInstrument().getUnderlyingInstrument());

		this.investmentInstrumentService.deleteInvestmentInstrument(newSecurity.getInstrument().getId());
	}


	@Test
	public void testCreateUpdateDeleteOneToManyInstrument() {
		// Use Existing Commodities / Options / On Futures: NGH2P 2.25 as template
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("NGH2P 2.25");

		InvestmentInstrument newInstrument = BeanUtils.cloneBean(security.getInstrument(), false, false);
		newInstrument.setId(null);
		newInstrument.setIdentifierPrefix(security.getInstrument().getIdentifierPrefix() + "-" + UUID.randomUUID());

		// Save Instrument First
		this.investmentInstrumentService.saveInvestmentInstrument(newInstrument);

		// Add the New Security
		String uniqueSymbol = "NGH2P 2.25-" + UUID.randomUUID();
		InvestmentSecurity newSecurity = BeanUtils.cloneBean(security, false, false);
		newSecurity.setId(null);
		newSecurity.setInstrument(newInstrument);
		newSecurity.setSymbol(uniqueSymbol);
		newSecurity.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		newSecurity.setColumnValueList(this.investmentInstrumentUtils.getCopyOfSystemColumnValueListForSecurity(security));
		this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);

		// Update the Security
		newSecurity.setName(newSecurity.getName() + "TEST UPDATE");
		this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);

		// Update the Instrument
		newInstrument.setName(newInstrument.getName() + "TEST UPDATE");
		this.investmentInstrumentService.saveInvestmentInstrument(newInstrument);

		// Delete the Security
		this.investmentInstrumentService.deleteInvestmentSecurity(newSecurity.getId());
		// Delete the Instrument
		this.investmentInstrumentService.deleteInvestmentInstrument(newInstrument.getId());
	}


	/////////////////////////////////////////////////////////////////////
	// Move Instrument to New Hierarchy
	/////////////////////////////////////////////////////////////////////


	@Test
	public void testMoveBetweenOneToOneAndOneToManyInstrument() {
		// Attempt to move HG instrument (Commodities / Futures - one to many) to (Equities / Funds / CEF - one to one)
		// Then Reverse Attempt to move CEM instrument (Equities / Funds / CEF - one to one) to (Commodities / Futures - one to many)
		InvestmentSecurity hgz4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("HGZ4");
		ValidationUtils.assertFalse(hgz4.getInstrument().getHierarchy().isOneSecurityPerInstrument(), "Expected Security HGZ4 to fall under one to many hierarchy.");

		InvestmentSecurity cem = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CEM US Equity");
		ValidationUtils.assertTrue(cem.getInstrument().getHierarchy().isOneSecurityPerInstrument(), "Expected Security CEM to fall under one to many hierarchy.");

		validateMoveInvestmentInstrument(hgz4.getInstrument().getId(), cem.getInstrument().getHierarchy().getId(), "Cannot move from hierarchy [Commodities / Futures / Physicals] to hierarchy [Equities / Funds / CEF] because:  both hierarchies do not match on option [Each instrument always has one security: One-to-One].");
		validateMoveInvestmentInstrument(cem.getInstrument().getId(), hgz4.getInstrument().getHierarchy().getId(), "Cannot move from hierarchy [Equities / Funds / CEF] to hierarchy [Commodities / Futures / Physicals] because:  both hierarchies do not match on option [Each instrument always has one security: One-to-One].");
	}


	@Test
	public void testMoveBetweenDifferentEventTypeMappings() {
		// Attempt to move C6662076M (CDX.NA.IG.20) instrument (Fixed Income / Swaps / Credit Default Swaps) to (Fixed Income / Swaps / Total Return Swaps)
		// Then Reverse Attempt to move 8446484B (912828VE7) instrument (Fixed Income / Swaps / Total Return Swaps) to (Fixed Income / Swaps / Credit Default Swaps)
		InvestmentSecurity cds = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("C6662076M");
		ValidationUtils.assertEquals("Credit Default Swaps", cds.getInstrument().getHierarchy().getInvestmentTypeSubType().getName(), "Expected Security C6662076M to be a Credit Default Swaps (Subtype), but was: " + cds.getInstrument().getHierarchy().getInvestmentTypeSubType().getName());

		InvestmentSecurity trs = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("8446484B");
		ValidationUtils.assertEquals("Total Return Swaps", trs.getInstrument().getHierarchy().getInvestmentTypeSubType().getName(), "Expected Security 8446484B to be a Total Return Swaps (Subtype), but was: " + trs.getInstrument().getHierarchy().getInvestmentTypeSubType().getName());


		validateMoveInvestmentInstrument(cds.getInstrument().getId(), trs.getInstrument().getHierarchy().getId(), "Cannot move from hierarchy [Fixed Income / Swaps / Credit Default Swaps / Index] to hierarchy [Fixed Income / Swaps / Total Return Swaps / Single Name / Notes and Bonds Total Return Swaps] because:  from hierarchy has event type mapping for [Premium Leg Payment] with events associated with it on the selected instrument, but to hierarchy does not.");
		validateMoveInvestmentInstrument(trs.getInstrument().getId(), cds.getInstrument().getHierarchy().getId(), "Cannot move from hierarchy [Fixed Income / Swaps / Total Return Swaps / Single Name / Notes and Bonds Total Return Swaps] to hierarchy [Fixed Income / Swaps / Credit Default Swaps / Index] because:  from hierarchy has event type mapping for [Equity Leg Payment] with events associated with it on the selected instrument, but to hierarchy does not.");
	}


	@Test
	public void testMoveBetweenDifferentCustomFieldMappings() {
		// Attempt to move 17239PDH1 instrument (Fixed Income / Physicals / Government Bonds / Municipal Government Bonds) to (Fixed Income / Physicals / Corporate Bonds / Convertible Bonds)
		// Then Reverse Attempt to move 031162AE0 instrument (Fixed Income / Physicals / Corporate Bonds / Convertible Bonds) to (Fixed Income / Physicals / Government Bonds / Municipal Government Bonds)
		InvestmentSecurity muniBond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("17239PDH1");
		ValidationUtils.assertEquals("Government Bonds", muniBond.getInstrument().getHierarchy().getInvestmentTypeSubType().getName(), "Expected Security 17239PDH1 to fall under hierarchy with Government Bonds subtype.");

		InvestmentSecurity corpBond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("031162AE0");
		ValidationUtils.assertEquals("Corporate Bonds", corpBond.getInstrument().getHierarchy().getInvestmentTypeSubType().getName(), "Expected Security 031162AE0 to fall under hierarchy with Corporate Bonds subtype.");

		validateMoveInvestmentInstrument(muniBond.getInstrument().getId(), corpBond.getInstrument().getHierarchy().getId(), "Cannot move from hierarchy [Fixed Income / Physicals / Government Bonds / Municipal Government Bonds] to hierarchy [Fixed Income / Physicals / Corporate Bonds / Convertible Bonds] because: The following custom columns are missing from the to list and cannot be mapped: [Muni Issue Type, Coupon Amount (%)]");
		validateMoveInvestmentInstrument(corpBond.getInstrument().getId(), muniBond.getInstrument().getHierarchy().getId(), "Cannot move from hierarchy [Fixed Income / Physicals / Corporate Bonds / Convertible Bonds] to hierarchy [Fixed Income / Physicals / Government Bonds / Municipal Government Bonds] because: The following custom columns are missing from the to list and cannot be mapped: [Coupon Amount]");
	}


	@Test
	public void testSuccessfulOneToOneMove_NoCustomFields() {
		// Move Benchmarks / Indices / Listed / Priced Monthly: HFRIMI to Benchmarks / Indices / Listed / Priced Quarterly and then back again
		InvestmentSecurity benchmark = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("HFRIMI");
		InvestmentInstrumentHierarchy originalHierarchy = benchmark.getInstrument().getHierarchy();

		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		searchForm.setParentId(originalHierarchy.getParent().getId());
		searchForm.setSearchPattern("Priced Quarterly");
		InvestmentInstrumentHierarchy newHierarchy = CollectionUtils.getOnlyElement(this.investmentSetupService.getInvestmentInstrumentHierarchyList(searchForm));
		Assertions.assertNotNull(newHierarchy);
		validateMoveInvestmentInstrument(benchmark.getInstrument().getId(), newHierarchy.getId(), null);
		validateMoveInvestmentInstrument(benchmark.getInstrument().getId(), originalHierarchy.getId(), null);
	}


	@Test
	public void testSuccessfulOneToManyMove_WithCustomFields() {
		// Move OF-RTA (using security: RTAG5P 1035.00) from Equities / Options / Exchange Traded / On Futures / Listed to Fixed Income / Options / Exchange Traded / On Futures / Listed and then back again
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("RTAG5P 1035.00");
		InvestmentInstrumentHierarchy originalHierarchy = security.getInstrument().getHierarchy();

		List<SystemColumnValue> valueList = getInvestmentSecurityCustomColumnValueList(security);
		int originalSize = valueList.size();
		for (SystemColumnValue value : valueList) {
			Assertions.assertEquals(value.getColumn().getLinkedValue(), "" + originalHierarchy.getId());
		}

		//  Fixed Income / Options / Exchange Traded / On Futures / Listed
		InvestmentInstrumentHierarchy newHierarchy = this.investmentSetupService.getInvestmentInstrumentHierarchy((short) 222);

		validateMoveInvestmentInstrument(security.getInstrument().getId(), newHierarchy.getId(), null);

		valueList = getInvestmentSecurityCustomColumnValueList(security);
		Assertions.assertEquals(originalSize, valueList.size());
		for (SystemColumnValue value : valueList) {
			Assertions.assertEquals(value.getColumn().getLinkedValue(), "" + newHierarchy.getId());
		}

		validateMoveInvestmentInstrument(security.getInstrument().getId(), originalHierarchy.getId(), null);

		valueList = getInvestmentSecurityCustomColumnValueList(security);
		Assertions.assertEquals(originalSize, valueList.size());
		for (SystemColumnValue value : valueList) {
			Assertions.assertEquals(value.getColumn().getLinkedValue(), "" + originalHierarchy.getId());
		}
	}


	private InvestmentSecurity copyAndValidateSecurity(InvestmentSecurity security) {
		String uniqueSymbol = security.getSymbol() + '-' + UUID.randomUUID();
		InvestmentInstrument newInstrument = BeanUtils.cloneBean(security.getInstrument(), false, false);
		newInstrument.setId(null);
		newInstrument.setIdentifierPrefix(uniqueSymbol);

		InvestmentSecurity toSaveSecurity = BeanUtils.cloneBean(security, false, false);
		toSaveSecurity.setId(null);
		toSaveSecurity.setInstrument(newInstrument);
		toSaveSecurity.setSymbol(uniqueSymbol);

		InvestmentSecurity newSecurity = this.investmentInstrumentService.saveInvestmentSecurity(toSaveSecurity);
		Integer newSecurityId = newSecurity.getId();
		Assertions.assertNotNull(newSecurityId);
		Assertions.assertNotEquals(newSecurityId, security.getId());
		Assertions.assertNotEquals(newSecurity.getInstrument(), security.getInstrument());
		Assertions.assertEquals(newSecurity.getUnderlyingSecurity() == null, security.getUnderlyingSecurity() == null);
		Assertions.assertEquals(newSecurity.getInstrument().getUnderlyingInstrument() == null, security.getInstrument().getUnderlyingInstrument() == null);

		if (security.isCurrency()) {
			// trading currency is not on the newSecurity instrument since it was already visited, look up the instrument for validation
			newInstrument = this.investmentInstrumentService.getInvestmentInstrument(newSecurity.getInstrument().getId());
			Assertions.assertEquals(newSecurity, newInstrument.getTradingCurrency());
		}
		else {
			Assertions.assertEquals(security.getInstrument().getTradingCurrency(), newSecurity.getInstrument().getTradingCurrency());
		}

		return newSecurity;
	}


	private void validateMoveInvestmentInstrument(int id, short hierarchyId, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			this.investmentInstrumentService.moveInvestmentInstrument(id, hierarchyId);
		}
		catch (ImsErrorResponseException e) {
			errorMessage = e.getErrorMessageFromIMS();
		}
		if (expectedErrorMessage == null) {
			Assertions.assertNull(errorMessage, "Did not expect error message, but was: " + errorMessage);
		}
		else {
			Assertions.assertTrue(errorMessage != null && errorMessage.endsWith(expectedErrorMessage), "Expected error message to end with: " + expectedErrorMessage + ", but was " + errorMessage);
		}
	}


	private List<SystemColumnValue> getInvestmentSecurityCustomColumnValueList(InvestmentSecurity security) {
		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setFkFieldId(security.getId());
		return this.systemColumnValueService.getSystemColumnValueList(searchForm);
	}
}
