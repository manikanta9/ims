package com.clifton.ims.tests.portfolio.cashflow.ldi.history.executor;

import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.date.DateUtils;
import com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory;
import com.clifton.portfolio.cashflow.ldi.history.rebuild.PortfolioCashFlowHistoryRebuildCommand;
import com.clifton.portfolio.cashflow.ldi.history.search.PortfolioCashFlowHistorySearchForm;

import java.util.Arrays;
import java.util.List;


/**
 * This executor is designed to make tests involving {@link com.clifton.portfolio.cashflow.ldi.history.PortfolioCashFlowHistory} easier to read/write.
 *
 * @author michaelm
 */
public class PortfolioCashflowHistoryCalculatorTestExecutor extends BaseTestExecutor<PortfolioCashFlowHistory> {

	private PortfolioCashFlowHistoryCalculatorTestServiceHolder serviceHolder;

	private int investmentAccountId;


	private PortfolioCashflowHistoryCalculatorTestExecutor(PortfolioCashFlowHistoryCalculatorTestServiceHolder serviceHolder) {
		this.serviceHolder = serviceHolder;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static PortfolioCashflowHistoryCalculatorTestExecutor newExecutorWithAccountId(PortfolioCashFlowHistoryCalculatorTestServiceHolder serviceHolder, int investmentAccountId) {
		return newExecutor(serviceHolder).withAccountId(investmentAccountId);
	}


	public static PortfolioCashflowHistoryCalculatorTestExecutor newExecutor(PortfolioCashFlowHistoryCalculatorTestServiceHolder serviceHolder) {
		return new PortfolioCashflowHistoryCalculatorTestExecutor(serviceHolder);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioCashflowHistoryCalculatorTestExecutor withAccountId(int investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
		return this;
	}


	public PortfolioCashflowHistoryCalculatorTestExecutor withBalanceStartAndEndDates(String startBalanceDate, String endBalanceDate) {
		this.startBalanceDate = DateUtils.toDate(startBalanceDate);
		this.endBalanceDate = DateUtils.toDate(endBalanceDate);
		return this;
	}


	public PortfolioCashflowHistoryCalculatorTestExecutor withExpectedExpectedCashFlowHistoryResults(String... expectedCashFlowHistoryStrings) {
		this.expectedResultStrings = expectedCashFlowHistoryStrings;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Cash Flow History";
	}


	@Override
	protected List<PortfolioCashFlowHistory> executeTest() {
		return executeRebuild();
	}


	@Override
	protected String[] getStringValuesForResultEntity(PortfolioCashFlowHistory cashFlowHistory) {
		return Arrays.asList(
				DateUtils.fromDate(cashFlowHistory.getCashFlowGroupHistory().getStartDate(), DateUtils.DATE_FORMAT_INPUT),
				DateUtils.fromDate(cashFlowHistory.getCashFlowGroupHistory().getEndDate(), DateUtils.DATE_FORMAT_INPUT),
				"History Amount " + cashFlowHistory.getCashFlowHistoryAmount().toPlainString(),
				cashFlowHistory.getCashFlowType().getName(),
				"Adj. Year " + cashFlowHistory.getAdjustedYear(),
				"Start Month " + cashFlowHistory.getStartingMonthNumber(),
				"Discount Month " + cashFlowHistory.getDiscountMonth(),
				"Liability Rate " + cashFlowHistory.getLiabilityDiscountRate().toPlainString(),
				"Liability Amount " + cashFlowHistory.getLiabilityCashFlowHistoryAmount().toPlainString(),
				"DV01 Rate " + cashFlowHistory.getDv01DiscountRate().toPlainString(),
				"DV01 Amount " + cashFlowHistory.getDv01CashFlowHistoryAmount().toPlainString(),
				"DV01 Value " + cashFlowHistory.getDv01Value().toPlainString(),
				"KRD " + cashFlowHistory.getKrdValue().toPlainString()
		).toArray(new String[0]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<PortfolioCashFlowHistory> executeRebuild() {
		PortfolioCashFlowHistoryRebuildCommand rebuildCommand = new PortfolioCashFlowHistoryRebuildCommand();
		rebuildCommand.setInvestmentAccountId(this.investmentAccountId);
		rebuildCommand.setStartBalanceDate(this.startBalanceDate);
		rebuildCommand.setEndBalanceDate(this.endBalanceDate);
		rebuildCommand.setSynchronous(true);
		rebuildCommand.setRebuildExisting(true);
		this.serviceHolder.getPortfolioCashFlowHistoryRebuildService().rebuildPortfolioCashFlowHistory(rebuildCommand);
		PortfolioCashFlowHistorySearchForm cashFlowHistorySearchForm = new PortfolioCashFlowHistorySearchForm();
		cashFlowHistorySearchForm.setInvestmentAccountId(this.investmentAccountId);
		cashFlowHistorySearchForm.setStartDate(this.startBalanceDate);
		cashFlowHistorySearchForm.setEndDate(this.endBalanceDate);
		cashFlowHistorySearchForm.setOrderBy("adjustedYear:asc#cashFlowType");
		return this.serviceHolder.getPortfolioCashFlowHistoryService().getPortfolioCashFlowHistoryList(cashFlowHistorySearchForm);
	}
}
