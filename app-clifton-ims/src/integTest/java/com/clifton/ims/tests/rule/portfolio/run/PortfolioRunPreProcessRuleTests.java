package com.clifton.ims.tests.rule.portfolio.run;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.tests.builders.investment.assetclass.InvestmentAssetClassBuilder;
import com.clifton.ims.tests.builders.investment.assetclass.account.assetclass.InvestmentAccountAssetClassBuilder;
import com.clifton.ims.tests.builders.investment.manager.InvestmentManagerAccountBuilder;
import com.clifton.ims.tests.builders.investment.trade.TradeBuilder;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionAccount;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.test.util.RandomUtils;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class PortfolioRunPreProcessRuleTests extends BasePortfolioRunRuleTests {

	private static boolean testInitialized = false;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<String> getExcludedTestMethodSet() {
		Set<String> excludedTestMethodSet = new HashSet<>();
		excludedTestMethodSet.add("testManagerAccountBalanceIncompletelyProcessedRule");
		excludedTestMethodSet.add("testManagerBalanceAdjustedValuesNotCorrectRule");
		excludedTestMethodSet.add("testUnbookedOrIncompleteTradesGTCACCOUNTSONLYRule"); //logic is reused from another rule that is already tested
		return excludedTestMethodSet;
	}


	@Override
	public Boolean getPreProcess() {
		return true;
	}


	@Override
	public boolean isTestInitialized() {
		return testInitialized;
	}


	@Override
	public void setTestInitialized(boolean initialized) {
		testInitialized = initialized;
	}


	@Override
	protected void applyRuleDefinitionSearchFormOverrides(RuleDefinitionSearchForm ruleDefinitionSearchForm) {
		ruleDefinitionSearchForm.setRuleScopeNames(new String[]{"Overlay Client Accounts", "LDI Client Accounts"});
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testManagerAccountBalanceMissingRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
					.withAccountName(RandomUtils.randomNameAndNumber())
					.withClient(this.accountInfo.getBusinessClient())
					.withManagerCompany(this.accountInfo.getClientCompany())
					.withManagerType(InvestmentManagerAccount.ManagerTypes.STANDARD)
					.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.NONE)
					.withBaseCurrency(this.usd)
					.buildAndSave();

			InvestmentAssetClass investmentAssetClass = InvestmentAssetClassBuilder.investmentAssetClass(this.investmentSetupService)
					.withCash(false)
					.withMainCash(false)
					.withMaster(true)
					.withName(RandomUtils.randomNameAndNumber())
					.buildAndSave();

			InvestmentAccountAssetClassBuilder.investmentAccountAssetClass(this.investmentAccountAssetClassService)
					.withAccount(this.accountInfo.getClientAccount())
					.withAssetClass(investmentAssetClass)
					.withAssetClassPercentage(new BigDecimal(100))
					.withCashPercentage(new BigDecimal(100))
					.withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE)
					.withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE)
					.buildAndSave();

			List<InvestmentManagerAccountAllocation> allocationList = new ArrayList<>();
			allocationList.add(this.investmentManagerAccountUtils.createInvestmentManagerAccountAllocation(investmentAssetClass, new BigDecimal(100)));
			this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount, this.accountInfo.getClientAccount(), allocationList);

			expectedViolationNoteMap.put("testManagerAccountBalancesMissingRule", managerAccount.getAccountName() +
					" is missing an account balance on " + DateUtils.fromDate(this.lastBusinessDay, DateUtils.DATE_FORMAT_INPUT));
		}
		else {
			validateTestMethodResult("testManagerAccountBalancesMissingRule");
		}
	}


	@Test
	public void testManagerAccountOutdatedProxyValueRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
					.withAccountName(RandomUtils.randomNameAndNumber())
					.withClient(this.accountInfo.getBusinessClient())
					.withManagerCompany(this.accountInfo.getClientCompany())
					.withManagerType(InvestmentManagerAccount.ManagerTypes.PROXY)
					.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.NONE)
					.withProxyValue(new BigDecimal(10))
					.withProxyValueDate(DateUtils.addDays(this.lastBusinessDay, -112))
					.withBaseCurrency(this.usd)
					.buildAndSave();

			this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount, this.lastBusinessDay, new BigDecimal(100), new BigDecimal(0));

			InvestmentAssetClass investmentAssetClass = InvestmentAssetClassBuilder.investmentAssetClass(this.investmentSetupService)
					.withCash(false)
					.withMainCash(false)
					.withMaster(true)
					.withName(RandomUtils.randomNameAndNumber())
					.buildAndSave();

			InvestmentAccountAssetClassBuilder.investmentAccountAssetClass(this.investmentAccountAssetClassService)
					.withAccount(this.accountInfo.getClientAccount())
					.withAssetClass(investmentAssetClass)
					.withAssetClassPercentage(new BigDecimal(100))
					.withCashPercentage(new BigDecimal(100))
					.withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE)
					.withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE)
					.buildAndSave();

			List<InvestmentManagerAccountAllocation> allocationList = new ArrayList<>();
			allocationList.add(this.investmentManagerAccountUtils.createInvestmentManagerAccountAllocation(investmentAssetClass, new BigDecimal(100)));
			this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount, this.accountInfo.getClientAccount(), allocationList);

			expectedViolationNoteMap.put("testManagerOutdatedProxyValueRule", "Manager " + managerAccount.getAccountName() + " " +
					"Proxy value was last updated " + (Math.abs(DateUtils.getDaysDifferenceInclusive(managerAccount.getProxyValueDate(), portfolioRun.getBalanceDate()))) +
					" days ago on " + DateUtils.fromDate(DateUtils.addDays(this.lastBusinessDay, -112), DateUtils.DATE_FORMAT_INPUT) + ".<br />" +
					"<span class='violationMessage'>This manager should be updated at least every " + 110 + " days.</span>");
		}
		else {
			validateTestMethodResult("testManagerOutdatedProxyValueRule");
		}
	}


	@Test
	public void testManagerAccountProxyManagerUpdatedWithPreviousDaysMOCAdjustmentRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
					.withAccountName(RandomUtils.randomNameAndNumber())
					.withClient(this.accountInfo.getBusinessClient())
					.withManagerCompany(this.accountInfo.getClientCompany())
					.withManagerType(InvestmentManagerAccount.ManagerTypes.PROXY)
					.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.NONE)
					.withProxyValue(new BigDecimal(10000))
					.withProxyValueDate(this.lastBusinessDay)
					.withBaseCurrency(this.usd)
					.buildAndSave();

			InvestmentManagerAccountBalance balance = this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount, this.lastBusinessDay, BigDecimal.ZERO, new BigDecimal(10000));
			InvestmentManagerAccountBalanceAdjustment adjustment = new InvestmentManagerAccountBalanceAdjustment();
			adjustment.setManagerAccountBalance(balance);
			adjustment.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("Previous Day's MOC Adjustment"));
			adjustment.setSecuritiesValue(new BigDecimal(1000));
			adjustment.setCashValue(BigDecimal.ZERO);
			adjustment.setNote("Test");
			balance.addAdjustment(adjustment);
			this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(balance);

			InvestmentAssetClass investmentAssetClass = InvestmentAssetClassBuilder.investmentAssetClass(this.investmentSetupService)
					.withCash(false)
					.withMainCash(false)
					.withMaster(true)
					.withName(RandomUtils.randomNameAndNumber())
					.buildAndSave();

			InvestmentAccountAssetClassBuilder.investmentAccountAssetClass(this.investmentAccountAssetClassService)
					.withAccount(this.accountInfo.getClientAccount())
					.withAssetClass(investmentAssetClass)
					.withAssetClassPercentage(new BigDecimal(100))
					.withCashPercentage(new BigDecimal(100))
					.withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE)
					.withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE)
					.buildAndSave();

			List<InvestmentManagerAccountAllocation> allocationList = new ArrayList<>();
			allocationList.add(this.investmentManagerAccountUtils.createInvestmentManagerAccountAllocation(investmentAssetClass, new BigDecimal(100)));
			this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount, this.accountInfo.getClientAccount(), allocationList);

			expectedViolationNoteMap.put("testManagerAccountProxyManagerUpdatedWithPreviousDaysMOCAdjustmentRule", "Manager " + managerAccount.getAccountName() + " has a proxy updated and \"Previous Day's MOC Adjustment\" type is present, double check the value.");
		}
		else {
			validateTestMethodResult("testManagerAccountProxyManagerUpdatedWithPreviousDaysMOCAdjustmentRule");
		}
	}


	@Test
	public void testManagerAccountOutdatedCustomCashValueRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
					.withAccountName(RandomUtils.randomNameAndNumber())
					.withClient(this.accountInfo.getBusinessClient())
					.withManagerCompany(this.accountInfo.getClientCompany())
					.withManagerType(InvestmentManagerAccount.ManagerTypes.STANDARD)
					.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.CUSTOM_VALUE)
					.withCustomCashValue(new BigDecimal(10))
					.withCustomCashValueUpdateDate(DateUtils.addDays(this.lastBusinessDay, -91))
					.withBaseCurrency(this.usd)
					.buildAndSave();

			this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount, this.lastBusinessDay, new BigDecimal(100), new BigDecimal(0));

			InvestmentAssetClass investmentAssetClass = InvestmentAssetClassBuilder.investmentAssetClass(this.investmentSetupService)
					.withCash(false)
					.withMainCash(false)
					.withMaster(true)
					.withName(RandomUtils.randomNameAndNumber())
					.buildAndSave();

			InvestmentAccountAssetClassBuilder.investmentAccountAssetClass(this.investmentAccountAssetClassService)
					.withAccount(this.accountInfo.getClientAccount())
					.withAssetClass(investmentAssetClass)
					.withAssetClassPercentage(new BigDecimal(100))
					.withCashPercentage(new BigDecimal(100))
					.withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE)
					.withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE)
					.buildAndSave();

			List<InvestmentManagerAccountAllocation> allocationList = new ArrayList<>();
			allocationList.add(this.investmentManagerAccountUtils.createInvestmentManagerAccountAllocation(investmentAssetClass, new BigDecimal(100)));
			this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount, this.accountInfo.getClientAccount(), allocationList);

			expectedViolationNoteMap.put("testOutdatedCustomCashValueRule", "Manager " + managerAccount.getAccountName() + " " +
					"Custom Cash value was last updated " + (Math.abs(DateUtils.getDaysDifferenceInclusive(managerAccount.getCustomCashValueUpdateDate(), portfolioRun.getBalanceDate()))) +
					" days ago on " + DateUtils.fromDate(DateUtils.addDays(this.lastBusinessDay, -91), DateUtils.DATE_FORMAT_INPUT) + ".<br />" +
					"<span class='violationMessage'>This manager should be updated at least every " + 50 + " days.</span>");
		}
		else {
			validateTestMethodResult("testOutdatedCustomCashValueRule");
		}
	}


	@Test
	public void testUnreconciledPositionQuantityRule() {
		if (!testInitialized) {
			ReconcilePositionAccount reconcilePositionAccount = new ReconcilePositionAccount();
			reconcilePositionAccount.setClientAccount(this.accountInfo.getClientAccount());
			reconcilePositionAccount.setDefinition(this.reconcilePositionService.getReconcilePositionDefinition(1));
			reconcilePositionAccount.setHoldingAccount(this.accountInfo.getHoldingAccount());
			reconcilePositionAccount.setPositionDate(this.lastBusinessDay);
			reconcilePositionAccount.setQuantityReconciled(false);
			reconcilePositionAccount.setReconciled(false);
			this.reconcilePositionService.saveReconcilePositionAccount(reconcilePositionAccount);

			ReconcilePosition reconcilePosition = new ReconcilePosition();
			reconcilePosition.setPositionAccount(reconcilePositionAccount);
			reconcilePosition.setAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH));
			reconcilePosition.setInvestmentSecurity(this.investmentSecurity);
			reconcilePosition.setMatched(false);
			reconcilePosition.setOurQuantity(new BigDecimal(0));
			reconcilePosition.setExternalQuantity(new BigDecimal(10).setScale(10, RoundingMode.UNNECESSARY));
			this.reconcilePositionService.saveReconcilePosition(reconcilePosition);

			expectedViolationNoteMap.put("testSecurityPositionQuantityUnreconciledRule", "Position Quantity for " + this.investmentSecurity.getSymbol() + " " +
					"has not been reconciled for holding account " + this.accountInfo.getHoldingAccount().getName() + ":<br /><span class='violationMessage'>" +
					"Our Quantity: <span class='amountPositive'>" + CoreMathUtils.formatNumberMoney(reconcilePosition.getOurQuantity()) + "</span>, External Quantity: <span class='amountPositive'>" +
					CoreMathUtils.formatNumberMoney(reconcilePosition.getExternalQuantity()) + "</span></span>");
		}
		else {
			validateTestMethodResult("testSecurityPositionQuantityUnreconciledRule");
		}
	}


	@Test
	public void testUnbookedLendingRepoOrRepoMaturityRule() {
		if (!testInitialized) {
			LendingRepo repo = this.lendingRepoUtils.newOpenLendingRepoBuilder(this.investmentSecurity, new BigDecimal(10), this.tradeDate, this.accountInfo)
					.price(new BigDecimal("107.8125"))
					.haircutPercent(new BigDecimal("99"))
					.indexRatio(new BigDecimal("1.0829700000"))
					.interestAmount(new BigDecimal("10.29"))
					.netCash(new BigDecimal("4632738.63"))
					.marketValue(new BigDecimal("4679533.97"))
					.interestRate(new BigDecimal("0.08"))
					.dayCountConvention("Actual/360")
					.active(true)
					.instrumentHierarchy(this.investmentSecurity.getInstrument().getHierarchy())
					.buildAndSave();

			expectedViolationNoteMap.put("testUnbookedLendingRepoOrRepoMaturityRule", "Account #: " + this.accountInfo.getClientAccount().getNumber() +
					" has an unbooked Repo for " + this.investmentSecurity.getSymbol() + " from " + DateUtils.fromDate(repo.getTradeDate(), DateUtils.DATE_FORMAT_INPUT));
		}
		else {
			validateTestMethodResult("testUnbookedLendingRepoOrRepoMaturityRule");
		}
	}


	@Test
	public void testUnbookedOrIncompleteTradeFillsRule() {
		if (!testInitialized) {
			TradeBuilder tradeBuilder = this.tradeUtils.newFuturesTradeBuilder(this.investmentSecurity, this.tradeQuantity, true, this.tradeDate, this.executingBroker, this.accountInfo)
					.withTraderUser(this.securityUserService.getSecurityUserByName(ImsTestProperties.TEST_USER_ADMIN));
			Trade trade = tradeBuilder.buildAndSave();
			this.tradeUtils.fullyExecuteTrade(trade);

			TradeFill tradeFill = this.tradeUtils.getTradeFills(trade).get(0);
			AccountingJournal accountingJournal = this.accountingJournalService.getAccountingJournalBySource(TradeFill.TABLE_NAME, tradeFill.getSourceEntityId());
			this.accountingPostingService.unpostAccountingJournal(accountingJournal.getId());

			expectedViolationNoteMap.put("testUnbookedOrIncompleteTradeFillsRule", "Account #: " + this.accountInfo.getClientAccount().getNumber() +
					" has an unbooked fill for booked Trade for " + this.investmentSecurity.getSymbol() + " from " +
					DateUtils.fromDate(trade.getTradeDate(), DateUtils.DATE_FORMAT_INPUT));
		}
		else {
			validateTestMethodResult("testUnbookedOrIncompleteTradeFillsRule");
		}
	}


	@Test
	public void testUnbookedOrIncompleteTradesRule() {
		if (!testInitialized) {
			TradeBuilder tradeBuilder = this.tradeUtils.newFuturesTradeBuilder(this.investmentSecurity, this.tradeQuantity, true, this.tradeDate, this.executingBroker, this.accountInfo)
					.withTraderUser(this.securityUserService.getSecurityUserByName(ImsTestProperties.TEST_USER_ADMIN));
			Trade trade = tradeBuilder.buildAndSave();

			expectedViolationNoteMap.put("testUnbookedOrIncompleteTradesRule", "Account #: " + this.accountInfo.getClientAccount().getNumber() + " has an Pending Trade for " +
					this.investmentSecurity.getSymbol() + " from " + DateUtils.fromDate(trade.getTradeDate(), DateUtils.DATE_FORMAT_INPUT) +
					". Trade entered by " + trade.getTraderUser().getDisplayName());
		}
		else {
			validateTestMethodResult("testUnbookedOrIncompleteTradesRule");
		}
	}
}
