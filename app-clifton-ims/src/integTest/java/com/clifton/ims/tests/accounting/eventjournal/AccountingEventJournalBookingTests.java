package com.clifton.ims.tests.accounting.eventjournal;

import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingBookingRuleIntegrationTestExecutor;
import com.clifton.ims.tests.accounting.AccountingEventPostResult;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.util.RandomUtils;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.stream.Stream;


/**
 * The <code>AccountingEventJournalBookingTests</code> are integration tests that verify the successful generation and booking for event journals.
 * <p>
 * NOTE:  This test is to make sure that the AccountJournal function properly when lazily loaded.
 *
 * @author mwacker
 */

public class AccountingEventJournalBookingTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;
	@Resource
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;
	@Resource
	private AccountingPostingService accountingPostingService;
	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;
	@Resource
	private MarketDataFieldService marketDataFieldService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSavingWithHigherOrderDeleted() {
		InvestmentSecurityEvent event1 = new InvestmentSecurityEvent();
		event1.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName("Cash Dividend Payment"));
		event1.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		event1.setSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("WM", false));
		event1.setDeclareDate(DateUtils.toDate("02/20/2020"));
		event1.setExDate(DateUtils.toDate("03/05/2020"));
		event1.setRecordDate(DateUtils.toDate("03/06/2020"));
		event1.setPaymentDate(DateUtils.toDate("03/16/2020"));
		event1.setBeforeAndAfterEventValue(new BigDecimal("0.545"));
		event1.setAdditionalSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event1);

		InvestmentSecurityEvent event2 = generateSymbolChangeEvent();

		// Now, can try to generate and post journals for higher-order one, and it should fail
		EventJournalGeneratorCommand generatorCommand = new EventJournalGeneratorCommand();
		generatorCommand.setEventId(event2.getId());
		generatorCommand.setGeneratorType(AccountingEventJournalGeneratorTypes.POST);
		generatorCommand.setSeparateJournalPerHoldingAccount(true);
		generatorCommand.setGenerationDate(DateUtils.toDate("03/07/2020"));
		TestUtils.expectException(ImsErrorResponseException.class, () -> this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(generatorCommand), "cannot be booked before");

		// Then, update event1 to Cancelled status, and higher order one will be allowed to generate first
		event1.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Cancelled"));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event1);
		this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(generatorCommand);

		// Now, make sure order override works
		// First need to set event1 back to approved status
		event1.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event1);

		InvestmentSecurityEvent event_orderOverride = generateSymbolChangeEvent();
		event_orderOverride.setBookingOrderOverride((short) 1);
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event_orderOverride);
		EventJournalGeneratorCommand generatorCommand_orderOverride = new EventJournalGeneratorCommand();
		generatorCommand_orderOverride.setEventId(event_orderOverride.getId());
		generatorCommand_orderOverride.setGeneratorType(AccountingEventJournalGeneratorTypes.POST);
		generatorCommand_orderOverride.setSeparateJournalPerHoldingAccount(true);
		generatorCommand_orderOverride.setGenerationDate(DateUtils.toDate("03/07/2020"));

		this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(generatorCommand_orderOverride);
	}


	/**
	 * Test to verify that that event journal can be successfully generated, booked and unbooked.
	 * <p>
	 * This integration test performs the following steps and verifies each one:
	 * <p>
	 * <ol>
	 * <li>Opens bond positions</li>
	 * <li>Generates and books Cash Coupon Payment event</li>
	 * <li>Un-posts the coupon events</li>
	 * </ol>
	 */
	@Test
	public void testCashCouponPaymentEventBookingAndUnbooking() {
		InvestmentSecurity bond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("912828JE1");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.bonds);

		/*
		 **********************************************************************************
		 * STEP 1: Execute bond trades
		 **********************************************************************************
		 */
		testCouponEventDoTrades(bond, accountInfo);

		/*
		 **********************************************************************************
		 * STEP 2: Create and book coupon event
		 **********************************************************************************
		 */
		InvestmentSecurityEvent couponEvent = new InvestmentSecurityEvent();
		couponEvent.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT));
		couponEvent.setBeforeAndAfterEventValue(new BigDecimal("1.375"));
		couponEvent.setSecurity(bond);
		couponEvent.setAccrualStartDate(DateUtils.toDate("7/15/2013"));
		couponEvent.setAccrualEndDate(DateUtils.toDate("01/14/2014"));
		couponEvent.setExDate(DateUtils.toDate("01/15/2014"));
		couponEvent.setPaymentDate(DateUtils.toDate("01/15/2014"));

		this.investmentSecurityEventService.saveInvestmentSecurityEvent(couponEvent);

		AccountingEventPostResult result = this.accountingUtils.generateAndPostEventJournal(couponEvent, accountInfo.getClientAccount());

		/*
		 **********************************************************************************
		 * STEP 3: Unpost coupon event and accrual reversal
		 **********************************************************************************
		 */
		this.accountingPostingService.unpostAccountingJournal(result.getAccrualReversalBookedJournal().getId());
		this.accountingPostingService.unpostAccountingJournal(result.getMainBookedJournal().getId());
	}


	@Test
	public void testForwardMaturity_TwoLots_Gain() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.forwards);

		InvestmentSecurity forward = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP/USD20191031");
		Trade trade = this.tradeUtils.newForwardsTradeBuilder(forward, new BigDecimal("20321500"), true, DateUtils.toDate("09/26/2019"), accountInfo.getHoldingCompany(), accountInfo)
				.withAverageUnitPrice(new BigDecimal("1.236115"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newForwardsTradeBuilder(forward, new BigDecimal("20321500"), false, DateUtils.toDate("10/29/2019"), accountInfo.getHoldingCompany(), accountInfo)
				.withAverageUnitPrice(new BigDecimal("1.28533"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent maturity = this.investmentSecurityEventService.getInvestmentSecurityEvent(1823545);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, maturity)
				.withExpectedEventJournalDetails(
						"672328	null	CA1915508381	HA724555606	Position	09/26/2019	1	20,321,500	25,119,710.97	1.28533	1.236115			1.28533	null",
						"672329	null	CA1915508381	HA724555606	Position	10/29/2019	1	-20,321,500	-26,119,833.6	1.28533	1.28533			1.28533	null"
				)
				.withExpectedJournalDetails(
						"16129548	16129546	CA1377581543	HA1932879516	Position	GBP/USD20191031	1.28533	-20,321,500	0	10/31/2019	1	0	-25,119,710.97	09/26/2019	10/31/2019	Security Maturity for GBP/USD20191031 on 10/31/2019",
						"16129549	16129548	CA1377581543	HA1527024970	Cash	USD			-25,119,710.97	10/31/2019	1	-25,119,710.97	0	09/26/2019	10/31/2019	Cash expense from USD on maturity of GBP/USD20191031 on 10/31/2019",
						"16129550	16129548	CA1377581543	HA1527024970	Currency	GBP			20,321,500	10/31/2019	1.28533	26,119,833.6	0	09/26/2019	10/31/2019	Currency proceeds from GBP on maturity of GBP/USD20191031 on 10/31/2019",
						"16129551	16129547	CA1377581543	HA1932879516	Position	GBP/USD20191031	1.28533	20,321,500	0	10/31/2019	1	0	26,119,833.6	10/29/2019	10/31/2019	Security Maturity for GBP/USD20191031 on 10/31/2019",
						"16129552	16129551	CA1377581543	HA1527024970	Cash	USD			26,119,833.6	10/31/2019	1	26,119,833.6	0	10/29/2019	10/31/2019	Cash proceeds from USD on maturity of GBP/USD20191031 on 10/31/2019",
						"16129553	16129551	CA1377581543	HA1527024970	Currency	GBP			-20,321,500	10/31/2019	1.28533	-26,119,833.6	0	10/29/2019	10/31/2019	Currency expense from GBP on maturity of GBP/USD20191031 on 10/31/2019",
						"16129554	16129548	CA1377581543	HA1932879516	Realized Gain / Loss	GBP/USD20191031	1.28533	20,321,500	-1,000,122.63	10/31/2019	1	-1,000,122.63	0	09/26/2019	10/31/2019	Realized Gain / Loss gain for GBP/USD20191031"
				).execute();
	}


	@Test
	public void testForwardMaturity_TwoLots_Loss() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.forwards);

		InvestmentSecurity forward = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP/USD20191031");
		Trade trade = this.tradeUtils.newForwardsTradeBuilder(forward, new BigDecimal("20321500"), true, DateUtils.toDate("09/26/2019"), accountInfo.getHoldingCompany(), accountInfo)
				.withAverageUnitPrice(new BigDecimal("1.28533"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newForwardsTradeBuilder(forward, new BigDecimal("20321500"), false, DateUtils.toDate("10/29/2019"), accountInfo.getHoldingCompany(), accountInfo)
				.withAverageUnitPrice(new BigDecimal("1.236115"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent maturity = this.investmentSecurityEventService.getInvestmentSecurityEvent(1823545);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, maturity)
				.withExpectedEventJournalDetails(
						"672332	null	CA1275244233	HA721951658	Position	09/26/2019	1	20,321,500	26,119,833.6	1.236115	1.28533			1.236115	null",
						"672333	null	CA1275244233	HA721951658	Position	10/29/2019	1	-20,321,500	-25,119,710.97	1.236115	1.236115			1.236115	null"
				)
				.withExpectedJournalDetails(
						"16129557	16129555	CA681804561	HA2116455252	Position	GBP/USD20191031	1.236115	-20,321,500	0	10/31/2019	1	0	-26,119,833.6	09/26/2019	10/31/2019	Security Maturity for GBP/USD20191031 on 10/31/2019",
						"16129558	16129557	CA681804561	HA1478340910	Cash	USD			-26,119,833.6	10/31/2019	1	-26,119,833.6	0	09/26/2019	10/31/2019	Cash expense from USD on maturity of GBP/USD20191031 on 10/31/2019",
						"16129559	16129557	CA681804561	HA1478340910	Currency	GBP			20,321,500	10/31/2019	1.236115	25,119,710.97	0	09/26/2019	10/31/2019	Currency proceeds from GBP on maturity of GBP/USD20191031 on 10/31/2019",
						"16129560	16129556	CA681804561	HA2116455252	Position	GBP/USD20191031	1.236115	20,321,500	0	10/31/2019	1	0	25,119,710.97	10/29/2019	10/31/2019	Security Maturity for GBP/USD20191031 on 10/31/2019",
						"16129561	16129560	CA681804561	HA1478340910	Cash	USD			25,119,710.97	10/31/2019	1	25,119,710.97	0	10/29/2019	10/31/2019	Cash proceeds from USD on maturity of GBP/USD20191031 on 10/31/2019",
						"16129562	16129560	CA681804561	HA1478340910	Currency	GBP			-20,321,500	10/31/2019	1.236115	-25,119,710.97	0	10/29/2019	10/31/2019	Currency expense from GBP on maturity of GBP/USD20191031 on 10/31/2019",
						"16129563	16129557	CA681804561	HA2116455252	Realized Gain / Loss	GBP/USD20191031	1.236115	20,321,500	1,000,122.63	10/31/2019	1	1,000,122.63	0	09/26/2019	10/31/2019	Realized Gain / Loss loss for GBP/USD20191031"
				).execute();
	}


	@Test
	public void testStockSplitAndSpinoffWithFractionalSharesRoundingOfTwoLots() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("DWDP");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("140000"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("70.79"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("125000"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("37.31"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Stock Spinoff ID: 1801254
		InvestmentSecurityEvent spinoff = this.investmentSecurityEventService.getInvestmentSecurityEvent(1801254);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, spinoff)
				.withExpectedEventJournalDetails(
						"632302	null	208472035	1306290147	Position	12/12/2017	1	140,000	9,910,600	49.9380268	6,991,323.75	46,666.6666666666	2,919,276.25	0.3333333333	null",
						"632303	null	208472035	1306290147	Position	05/02/2019	1	125,000	4,663,750	26.3199291	3,289,991.14	41,666.6666666667	1,373,758.86	0	null"
				)
				.withExpectedJournalDetails(
						"14770125	14770120	688630872	43277560	Position	DWDP	70.79	-140,000	-9,910,600	06/01/2019	1	-9,910,600	-9,910,600	12/12/2017	06/01/2019	Parent Closing from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770126	14770120	688630872	43277560	Position	DWDP	49.9380268	140,000	6,991,323.75	06/01/2019	1	6,991,323.75	6,991,323.75	12/12/2017	06/01/2019	Parent Reopening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770127	14770120	688630872	43277560	Position	CTVA	62.5559196	46,666.6666666666	2,919,276.25	06/01/2019	1	2,919,276.25	2,919,276.25	12/12/2017	06/01/2019	Spinoff Opening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770128	14770127	688630872	43277560	Position	CTVA	26.97	-0.3333333333	-20.85	06/01/2019	1	-20.85	-20.85	12/12/2017	06/01/2019	Fractional Shares Closing from rounding from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770129	14770123	688630872	43277560	Position	DWDP	37.31	-125,000	-4,663,750	06/01/2019	1	-4,663,750	-4,663,750	05/02/2019	06/01/2019	Parent Closing from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770130	14770123	688630872	43277560	Position	DWDP	26.3199291	125,000	3,289,991.14	06/01/2019	1	3,289,991.14	3,289,991.14	05/02/2019	06/01/2019	Parent Reopening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770131	14770123	688630872	43277560	Position	CTVA	32.9702126	41,666.6666666667	1,373,758.86	06/01/2019	1	1,373,758.86	1,373,758.86	05/02/2019	06/01/2019	Spinoff Opening from 3 to 1 CTVA Stock Spinoff for DWDP on 06/01/2019",
						"14770132	14770128	688630872	43277560	Cash	USD			8.99	06/01/2019	1	8.99	0	12/12/2017	06/01/2019	Cash proceeds from close of CTVA",
						"14770133	14770128	688630872	43277560	Realized Gain / Loss	CTVA	26.97	0.3333333333	11.86	06/01/2019	1	11.86	0	12/12/2017	06/01/2019	Realized Gain / Loss loss for CTVA"
				);
		executor.execute();

		// Stock Split ID: 1800684
		InvestmentSecurityEvent split = this.investmentSecurityEventService.getInvestmentSecurityEvent(1800684);

		executor.withEvent(split)
				.withExpectedEventJournalDetails(
						"632310	14770151	313940095	1609628373	Position	12/12/2017	1	140,000	6,991,323.75	49.9380268	149.8140804	46,666.6666666666	6,991,323.75	0.3333333333	null",
						"632311	14770154	313940095	1609628373	Position	05/02/2019	1	125,000	3,289,991.14	26.3199291	78.9597874	41,666.6666666667	3,289,991.14	0	null"
				)
				.withExpectedJournalDetails(
						"14770203	14770195	855378995	44734229	Position	DWDP	49.9380268	-140,000	-6,991,323.75	06/03/2019	1	-6,991,323.75	-6,991,323.75	12/12/2017	06/03/2019	3 to 1 Stock Split for DWDP on 06/03/2019",
						"14770204	14770189	855378995	44734229	Position	DWDP	149.8140804	46,666.6666666666	6,991,323.75	06/03/2019	1	6,991,323.75	6,991,323.75	12/12/2017	06/03/2019	3 to 1 Stock Split for DWDP on 06/03/2019",
						"14770205	14770204	855378995	44734229	Position	DWDP	76.1	-0.3333333333	-49.94	06/03/2019	1	-49.94	-49.94	12/12/2017	06/03/2019	Fractional Shares Closing from rounding from 3 to 1 Stock Split for DWDP on 06/03/2019",
						"14770206	14770199	855378995	44734229	Position	DWDP	26.3199291	-125,000	-3,289,991.14	06/03/2019	1	-3,289,991.14	-3,289,991.14	05/02/2019	06/03/2019	3 to 1 Stock Split for DWDP on 06/03/2019",
						"14770207	14770192	855378995	44734229	Position	DWDP	78.9597874	41,666.6666666667	3,289,991.14	06/03/2019	1	3,289,991.14	3,289,991.14	05/02/2019	06/03/2019	3 to 1 Stock Split for DWDP on 06/03/2019",
						"14770208	14770205	855378995	44734229	Cash	USD			25.37	06/03/2019	1	25.37	0	12/12/2017	06/03/2019	Cash proceeds from close of DWDP",
						"14770209	14770205	855378995	44734229	Realized Gain / Loss	DWDP	76.1	0.3333333333	24.57	06/03/2019	1	24.57	0	12/12/2017	06/03/2019	Realized Gain / Loss loss for DWDP"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setDefaultElection(true);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"637571	null	956829185	480680393	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"637572	null	956829185	480680393	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null"
				)
				.withExpectedJournalDetails(
						"14910632	14910627	956829185	480680393	Dividend Income	PRGO	48.49	136.5	-6,618.89	06/30/2019	1	-6,618.89	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910633	14910630	956829185	480680393	Dividend Income	PRGO	48.49	30	-1,454.7	06/30/2019	1	-1,454.7	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910634	null	956829185	480680393	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"14910635	14910634	956829185	480680393	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910636	null	956829185	480680393	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910637	14910636	956829185	480680393	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910638	14910637	956829185	480680393	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_FrankingCredit() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout.setDefaultElection(true);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"637571	null	956829185	480680393	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"637572	null	956829185	480680393	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null"
				)
				.withExpectedJournalDetails(
						"17656917	17656912	CA1431614177	HA1751649086	Franking Credits	PRGO	0.05	136.5	6.83	06/30/2019	1	6.83	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17656918	17656912	CA1431614177	HA1751649086	Dividend Income	PRGO	48.49	136.5	-6,625.72	06/30/2019	1	-6,625.72	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17656919	17656915	CA1431614177	HA1751649086	Franking Credits	PRGO	0.05	30	1.5	06/30/2019	1	1.5	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17656920	17656915	CA1431614177	HA1751649086	Dividend Income	PRGO	48.49	30	-1,456.2	06/30/2019	1	-1,456.2	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17656921	null	CA1431614177	HA1751649086	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"14910635	14910634	956829185	480680393	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910636	null	956829185	480680393	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910637	14910636	956829185	480680393	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"14910638	14910637	956829185	480680393	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO"
				).execute();
	}


	@Test
	public void testCashDividendWithDripWithFractionalSharesRoundingOfTwoLots() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MRK");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("604"), true, DateUtils.toDate("04/13/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("57.17"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("141"), true, DateUtils.toDate("06/19/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("85.36"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Cash Dividend (with DRIP)
		InvestmentSecurityEvent drip = new InvestmentSecurityEvent();
		drip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_DIVIDEND_DRIP));
		drip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		drip.setSecurity(stock);
		drip.setBeforeAndAfterEventValue(new BigDecimal("0.35"));
		drip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		drip.setExDate(DateUtils.toDate("07/01/2019"));
		drip.setEventDate(DateUtils.toDate("07/01/2019"));
		drip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		drip.setRecordDate(DateUtils.toDate("07/01/2019"));
		drip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		drip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(drip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(drip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(drip);
		payout.setAfterEventValue(drip.getAfterEventValue());
		payout.setAdditionalPayoutValue(new BigDecimal("87.78"));
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setDefaultElection(true);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, drip)
				.withExpectedEventJournalDetails(
						"637569	null	1308374505	79434643	Position	04/13/2018	1	604		87.78	18,556.69	211.4	18,556.7	0.75	null",
						"637570	null	1308374505	79434643	Position	06/19/2019	1	141		87.78	4,331.94	49.35	4,331.94	0	null"
				)
				.withExpectedJournalDetails(
						"14910620	14910615	1308374505	79434643	Dividend Income	MRK	87.78	211.4	-18,556.7	06/30/2019	1	-18,556.7	0	06/30/2019	07/05/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910621	14910618	1308374505	79434643	Dividend Income	MRK	87.78	49.35	-4,331.94	06/30/2019	1	-4,331.94	0	06/30/2019	07/05/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910622	null	1308374505	79434643	Position Receivable	MRK	87.78	260.75	22,888.64	06/30/2019	1	22,888.64	22,888.64	06/30/2019	07/05/2019	Receivable from 0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"14910623	14910622	1308374505	79434643	Position Receivable	MRK	87.78	-260.75	-22,888.64	06/30/2019	1	-22,888.64	-22,888.64	06/30/2019	07/05/2019	Reversal of Receivable from 0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910624	null	1308374505	79434643	Position	MRK	87.78	260.75	22,888.64	06/30/2019	1	22,888.64	22,888.64	06/30/2019	07/05/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910625	14910624	1308374505	79434643	Position	MRK	87.78	-0.75	-65.84	06/30/2019	1	-65.84	-65.84	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910626	14910625	1308374505	79434643	Cash	USD			65.84	06/30/2019	1	65.84	0	06/30/2019	07/05/2019	Cash proceeds from close of MRK"
				).execute();
	}


	@Test
	public void testCashDividendWithDripWithFractionalSharesRoundingOfTwoLots_FrankingCredit() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MRK");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("604"), true, DateUtils.toDate("04/13/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("57.17"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("141"), true, DateUtils.toDate("06/19/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("85.36"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Cash Dividend (with DRIP)
		InvestmentSecurityEvent drip = new InvestmentSecurityEvent();
		drip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_DIVIDEND_DRIP));
		drip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		drip.setSecurity(stock);
		drip.setBeforeAndAfterEventValue(new BigDecimal("0.35"));
		drip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		drip.setExDate(DateUtils.toDate("07/01/2019"));
		drip.setEventDate(DateUtils.toDate("07/01/2019"));
		drip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		drip.setRecordDate(DateUtils.toDate("07/01/2019"));
		drip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		drip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(drip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(drip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(drip);
		payout.setAfterEventValue(drip.getAfterEventValue());
		payout.setAdditionalPayoutValue(new BigDecimal("87.78"));
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout.setDefaultElection(true);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, drip)
				.withExpectedEventJournalDetails(
						"637569	null	1308374505	79434643	Position	04/13/2018	1	604		87.78	18,556.69	211.4	18,556.7	0.75	null",
						"637570	null	1308374505	79434643	Position	06/19/2019	1	141		87.78	4,331.94	49.35	4,331.94	0	null"
				)
				.withExpectedJournalDetails(
						"17657016	17657011	CA603104963	HA1704578066	Franking Credits	MRK	0.05	211.4	10.57	06/30/2019	1	10.57	0	06/30/2019	07/05/2019	Franking Tax Credit from Cash Dividend (with DRIP) for MRK on 07/05/2019",
						"17657017	17657011	CA603104963	HA1704578066	Dividend Income	MRK	87.78	211.4	-18,567.27	06/30/2019	1	-18,567.27	0	06/30/2019	07/05/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"17657018	17657014	CA603104963	HA1704578066	Franking Credits	MRK	0.05	49.35	2.47	06/30/2019	1	2.47	0	06/30/2019	07/05/2019	Franking Tax Credit from Cash Dividend (with DRIP) for MRK on 07/05/2019",
						"17657019	17657014	CA603104963	HA1704578066	Dividend Income	MRK	87.78	49.35	-4,334.41	06/30/2019	1	-4,334.41	0	06/30/2019	07/05/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"17657020	null	CA603104963	HA1704578066	Position Receivable	MRK	87.78	260.75	22,888.64	06/30/2019	1	22,888.64	22,888.64	06/30/2019	07/05/2019	Receivable from 0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"14910623	14910622	1308374505	79434643	Position Receivable	MRK	87.78	-260.75	-22,888.64	06/30/2019	1	-22,888.64	-22,888.64	06/30/2019	07/05/2019	Reversal of Receivable from 0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910624	null	1308374505	79434643	Position	MRK	87.78	260.75	22,888.64	06/30/2019	1	22,888.64	22,888.64	06/30/2019	07/05/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910625	14910624	1308374505	79434643	Position	MRK	87.78	-0.75	-65.84	06/30/2019	1	-65.84	-65.84	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"14910626	14910625	1308374505	79434643	Cash	USD			65.84	06/30/2019	1	65.84	0	06/30/2019	07/05/2019	Cash proceeds from close of MRK"
				).execute();
	}


	@Test
	public void testCashDividendWithDripWithFractionalSharesRoundingOfTwoLots_noPayoutUseEvent() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MRK");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("604"), true, DateUtils.toDate("04/13/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("57.17"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("141"), true, DateUtils.toDate("06/19/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("85.36"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Cash Dividend (with DRIP)
		InvestmentSecurityEvent drip = new InvestmentSecurityEvent();
		drip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_DIVIDEND_DRIP));
		drip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		drip.setSecurity(stock);
		drip.setBeforeAndAfterEventValue(new BigDecimal("0.35"));
		drip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		drip.setExDate(DateUtils.toDate("07/01/2019"));
		drip.setEventDate(DateUtils.toDate("07/01/2019"));
		drip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		drip.setRecordDate(DateUtils.toDate("07/01/2019"));
		drip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		drip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(drip);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, drip)
				.withExpectedEventJournalDetails(
						"650128	null	1636442829	122873641	Position	04/13/2018	1	604		57.17	211.4				null",
						"650129	null	1636442829	122873641	Position	06/19/2019	1	141		85.36	49.35				null"
				)
				.withExpectedJournalDetails(
						"15273453	15273448	1962214064	387778782	Dividend Receivable	USD			211.4	06/30/2019	1	211.4	0	06/30/2019	07/01/2019	Accrual of Cash Dividend (with DRIP) for MRK to be paid on 07/01/2019",
						"15273454	15273448	1962214064	387778782	Dividend Income	MRK	57.17	604	-211.4	06/30/2019	1	-211.4	0	06/30/2019	07/01/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"15273455	15273451	1962214064	387778782	Dividend Receivable	USD			49.35	06/30/2019	1	49.35	0	06/30/2019	07/01/2019	Accrual of Cash Dividend (with DRIP) for MRK to be paid on 07/01/2019",
						"15273456	15273451	1962214064	387778782	Dividend Income	MRK	85.36	141	-49.35	06/30/2019	1	-49.35	0	06/30/2019	07/01/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"16129638	16129629	CA2146144885	HA335368359	Dividend Receivable	USD			-211.4	07/01/2019	1	-211.4	0	04/13/2018	07/01/2019	Reverse accrual of Cash Dividend (with DRIP) for MRK from 06/30/2019",
						"16129639	16129629	CA2146144885	HA335368359	Cash	USD			211.4	07/01/2019	1	211.4	0	04/13/2018	07/01/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019",
						"16129640	16129632	CA2146144885	HA335368359	Dividend Receivable	USD			-49.35	07/01/2019	1	-49.35	0	06/19/2019	07/01/2019	Reverse accrual of Cash Dividend (with DRIP) for MRK from 06/30/2019",
						"16129641	16129632	CA2146144885	HA335368359	Cash	USD			49.35	07/01/2019	1	49.35	0	06/19/2019	07/01/2019	0.35 Cash Dividend (with DRIP) for MRK on 07/01/2019"
				).execute();
	}


	@Test
	public void testCashDividendWithDripWithFractionalSharesRoundingOfTwoLots_noDefaultPayoutOrElectionProducesValidationError() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MRK");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("604"), true, DateUtils.toDate("04/13/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("57.17"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("141"), true, DateUtils.toDate("06/19/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("85.36"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Cash Dividend (with DRIP)
		InvestmentSecurityEvent drip = new InvestmentSecurityEvent();
		drip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_DIVIDEND_DRIP));
		drip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		drip.setSecurity(stock);
		drip.setBeforeAndAfterEventValue(new BigDecimal("0.35"));
		drip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		drip.setExDate(DateUtils.toDate("07/01/2019"));
		drip.setEventDate(DateUtils.toDate("07/01/2019"));
		drip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		drip.setRecordDate(DateUtils.toDate("07/01/2019"));
		drip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		drip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(drip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(drip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(drip);
		payout.setAdditionalPayoutValue(new BigDecimal("87.78"));
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		try {
			AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, drip).execute();
			Assertions.fail("Expected exception for no payout on generation of event journal");
		}
		catch (ImsErrorResponseException e) {
			Assertions.assertTrue(e.getErrorMessageFromIMS().contains("no Client Specific or Default Election specified"));
		}
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayouts() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.setBeforeEventValue(scrip.getBeforeEventValue());
		payout.setAfterEventValue(payout.getBeforeEventValue());
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeEventValue(new BigDecimal("0.2"));
		payout2.setAfterEventValue(payout2.getBeforeEventValue());
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"645472	null	179044819	1644201794	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"645473	null	179044819	1644201794	Position	12/12/2017	1	1,365		0.2	273				null",
						"645474	null	179044819	1644201794	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null",
						"645475	null	179044819	1644201794	Position	05/02/2019	1	300		0.2	60				null"
				)
				.withExpectedJournalDetails(
						"15091813	15091808	179044819	1644201794	Dividend Receivable	USD			273	06/30/2019	1	273	0	06/30/2019	07/05/2019	Accrual of Scrip Dividend for PRGO to be paid on 07/05/2019",
						"15091814	15091808	179044819	1644201794	Dividend Income	PRGO	0.2	1,365	-273	06/30/2019	1	-273	0	06/30/2019	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"15091815	15091811	179044819	1644201794	Dividend Receivable	USD			60	06/30/2019	1	60	0	06/30/2019	07/05/2019	Accrual of Scrip Dividend for PRGO to be paid on 07/05/2019",
						"15091816	15091811	179044819	1644201794	Dividend Income	PRGO	0.2	300	-60	06/30/2019	1	-60	0	06/30/2019	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"15091817	15091808	179044819	1644201794	Dividend Income	PRGO	48.49	136.5	-6,618.89	06/30/2019	1	-6,618.89	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15091818	15091811	179044819	1644201794	Dividend Income	PRGO	48.49	30	-1,454.7	06/30/2019	1	-1,454.7	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15091819	null	179044819	1644201794	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"16129817	16129816	CA754421295	HA1104655700	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"16129818	null	CA754421295	HA1104655700	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"16129819	16129818	CA754421295	HA1104655700	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"16129820	16129819	CA754421295	HA1104655700	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO",
						"16129821	16129805	CA754421295	HA1104655700	Dividend Receivable	USD			-273	07/05/2019	1	-273	0	12/12/2017	07/05/2019	Reverse accrual of Scrip Dividend for PRGO from 06/30/2019",
						"16129822	16129805	CA754421295	HA1104655700	Cash	USD			273	07/05/2019	1	273	0	12/12/2017	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"16129823	16129808	CA754421295	HA1104655700	Dividend Receivable	USD			-60	07/05/2019	1	-60	0	05/02/2019	07/05/2019	Reverse accrual of Scrip Dividend for PRGO from 06/30/2019",
						"16129824	16129808	CA754421295	HA1104655700	Cash	USD			60	07/05/2019	1	60	0	05/02/2019	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayouts_FrankingCredit() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.setBeforeEventValue(scrip.getBeforeEventValue());
		payout.setAfterEventValue(payout.getBeforeEventValue());
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY_FRANKING_CREDIT));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeEventValue(new BigDecimal("0.2"));
		payout2.setAfterEventValue(payout2.getBeforeEventValue());
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout2.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"713932	null	CA1332843473	HA2020967116	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"713933	null	CA1332843473	HA2020967116	Position	12/12/2017	1	1,365		0.2	273				null",
						"713934	null	CA1332843473	HA2020967116	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null",
						"713935	null	CA1332843473	HA2020967116	Position	05/02/2019	1	300		0.2	60				null"
				)
				.withExpectedJournalDetails(
						"17653730	17653725	CA2062859912	HA2004924135	Dividend Receivable	USD			273	06/30/2019	1	273	0	06/30/2019	07/05/2019	Accrual of Scrip Dividend for PRGO to be paid on 07/05/2019",
						"17653731	17653725	CA2062859912	HA2004924135	Franking Credits	USD	0.05		68.25	06/30/2019	1	68.25	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17653732	17653725	CA2062859912	HA2004924135	Dividend Income	PRGO	0.2	1,365	-341.25	06/30/2019	1	-341.25	0	06/30/2019	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"17653733	17653728	CA2062859912	HA2004924135	Dividend Receivable	USD			60	06/30/2019	1	60	0	06/30/2019	07/05/2019	Accrual of Scrip Dividend for PRGO to be paid on 07/05/2019",
						"17653734	17653728	CA2062859912	HA2004924135	Franking Credits	USD	0.05		15	06/30/2019	1	15	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17653735	17653728	CA2062859912	HA2004924135	Dividend Income	PRGO	0.2	300	-75	06/30/2019	1	-75	0	06/30/2019	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"17653736	17653725	CA2062859912	HA2004924135	Franking Credits	PRGO	0.05	136.5	6.83	06/30/2019	1	6.83	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17653737	17653725	CA2062859912	HA2004924135	Dividend Income	PRGO	48.49	136.5	-6,625.72	06/30/2019	1	-6,625.72	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17653738	17653728	CA2062859912	HA2004924135	Franking Credits	PRGO	0.05	30	1.5	06/30/2019	1	1.5	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17653739	17653728	CA2062859912	HA2004924135	Dividend Income	PRGO	48.49	30	-1,456.2	06/30/2019	1	-1,456.2	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17653740	null	CA2062859912	HA2004924135	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"17653765	17653764	CA7493702	HA490450912	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17653766	null	CA7493702	HA490450912	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17653767	17653766	CA7493702	HA490450912	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17653768	17653767	CA7493702	HA490450912	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO",
						"17653769	17653749	CA7493702	HA490450912	Dividend Receivable	USD			-273	07/05/2019	1	-273	0	12/12/2017	07/05/2019	Reverse accrual of Scrip Dividend for PRGO from 06/30/2019",
						"17653770	17653749	CA7493702	HA490450912	Cash	USD			273	07/05/2019	1	273	0	12/12/2017	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"17653771	17653752	CA7493702	HA490450912	Dividend Receivable	USD			-60	07/05/2019	1	-60	0	05/02/2019	07/05/2019	Reverse accrual of Scrip Dividend for PRGO from 06/30/2019",
						"17653772	17653752	CA7493702	HA490450912	Cash	USD			60	07/05/2019	1	60	0	05/02/2019	07/05/2019	0.2 Scrip Dividend for PRGO on 07/01/2019"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayouts_ExDateAfterEventDate() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/05/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeEventValue(new BigDecimal("0.2"));
		payout2.setAfterEventValue(payout2.getBeforeEventValue());
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"645472	null	179044819	1644201794	Position	12/12/2017	1	1,365		50.17	6,848.21	136.5	6,848.21	0.5	null",
						"645473	null	179044819	1644201794	Position	12/12/2017	1	1,365		0.2	273				null",
						"645474	null	179044819	1644201794	Position	05/02/2019	1	300		50.17	1,505.1	30	1,505.1	0	null",
						"645475	null	179044819	1644201794	Position	05/02/2019	1	300		0.2	60				null"
				)
				.withExpectedJournalDetails(
						"15374623	15374618	222223295	1686304	Dividend Income	PRGO	0.2	1,365	-273	07/01/2019	1	-273	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"15374624	15374618	222223295	1686304	Cash	USD			273	07/01/2019	1	273	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"15374625	15374621	222223295	1686304	Dividend Income	PRGO	0.2	300	-60	07/01/2019	1	-60	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"15374626	15374621	222223295	1686304	Cash	USD			60	07/01/2019	1	60	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayouts_ExDateAfterEventDate_FrankingCredit() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/05/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY_FRANKING_CREDIT));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeAndAfterEventValue(new BigDecimal("0.2"));
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout2.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"645472	null	179044819	1644201794	Position	12/12/2017	1	1,365		50.17	6,848.21	136.5	6,848.21	0.5	null",
						"645473	null	179044819	1644201794	Position	12/12/2017	1	1,365		0.2	273				null",
						"645474	null	179044819	1644201794	Position	05/02/2019	1	300		50.17	1,505.1	30	1,505.1	0	null",
						"645475	null	179044819	1644201794	Position	05/02/2019	1	300		0.2	60				null"
				)
				.withExpectedJournalDetails(
						"17653816	17653811	CA1936544751	HA1756690165	Franking Credits	USD	0.05		68.25	07/01/2019	1	68.25	0	07/01/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17653817	17653811	CA1936544751	HA1756690165	Dividend Income	PRGO	0.2	1,365	-341.25	07/01/2019	1	-341.25	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"17653818	17653811	CA1936544751	HA1756690165	Cash	USD			273	07/01/2019	1	273	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"17653819	17653814	CA1936544751	HA1756690165	Franking Credits	USD	0.05		15	07/01/2019	1	15	0	07/01/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17653820	17653814	CA1936544751	HA1756690165	Dividend Income	PRGO	0.2	300	-75	07/01/2019	1	-75	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019",
						"17653821	17653814	CA1936544751	HA1756690165	Cash	USD			60	07/01/2019	1	60	0	07/01/2019	07/01/2019	0.2 Scrip Dividend for PRGO on 07/01/2019"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayoutsOneMarkedDeleted() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeEventValue(new BigDecimal("0.2"));
		payout2.setAfterEventValue(payout2.getBeforeEventValue());
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout2.setDeleted(true);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"650069	null	1114599636	1319024730	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"650070	null	1114599636	1319024730	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null"
				)
				.withExpectedJournalDetails(
						"15272955	15272950	1114599636	1319024730	Dividend Income	PRGO	48.49	136.5	-6,618.89	06/30/2019	1	-6,618.89	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272956	15272953	1114599636	1319024730	Dividend Income	PRGO	48.49	30	-1,454.7	06/30/2019	1	-1,454.7	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272957	null	1114599636	1319024730	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"15272958	15272957	1114599636	1319024730	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272959	null	1114599636	1319024730	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272960	15272959	1114599636	1319024730	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272961	15272960	1114599636	1319024730	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayoutsOneMarkedDeleted_FrankingCredit() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency (with Franking Credit)"));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeAndAfterEventValue(new BigDecimal("0.2"));
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout2.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout2.setDeleted(true);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"650069	null	1114599636	1319024730	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"650070	null	1114599636	1319024730	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null"
				)
				.withExpectedJournalDetails(
						"17655231	17655226	CA536648728	HA618595588	Franking Credits	PRGO	0.05	136.5	6.83	06/30/2019	1	6.83	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17655232	17655226	CA536648728	HA618595588	Dividend Income	PRGO	48.49	136.5	-6,625.72	06/30/2019	1	-6,625.72	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17655233	17655229	CA536648728	HA618595588	Franking Credits	PRGO	0.05	30	1.5	06/30/2019	1	1.5	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17655234	17655229	CA536648728	HA618595588	Dividend Income	PRGO	48.49	30	-1,456.2	06/30/2019	1	-1,456.2	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17655235	null	CA536648728	HA618595588	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"15272958	15272957	1114599636	1319024730	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272959	null	1114599636	1319024730	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272960	15272959	1114599636	1319024730	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272961	15272960	1114599636	1319024730	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO"
				).execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_regenerationForAddedPayoutSucceeds() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity securityToCopy = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		InvestmentSecurity stock = this.investmentInstrumentUtils.getCopyOfSecurity(securityToCopy.getSymbol(), "PRGO-" + RandomUtils.randomNumber(1, 99999));

		// Create Last Trade Price
		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm();
		marketDataValueSearchForm.setInvestmentSecurityId(securityToCopy.getId());
		marketDataValueSearchForm.setMeasureDate(DateUtils.toDate("07/01/2019"));
		marketDataValueSearchForm.setDataFieldName("Last Trade Price");
		MarketDataValue value = CollectionUtils.getFirstElement(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
		if (value != null) {
			value.setId(null);
			value.setInvestmentSecurity(stock);
			this.marketDataFieldService.saveMarketDataValue(value);
		}

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						// no populator is picked up so the details look different than with payouts
						"650267	null	2029763800	971328208	Position	12/12/2017	1	1,365		90.7	136.5				null",
						"650268	null	2029763800	971328208	Position	05/02/2019	1	300		88.19	30				null"
				);
		executor.preview();

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setDefaultElection(true);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		executor.withExpectedEventJournalDetails(
				"650069	null	1114599636	1319024730	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
				"650070	null	1114599636	1319024730	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null"
		)
				.withExpectedJournalDetails(Stream.of(
						"15272955	15272950	1114599636	1319024730	Dividend Income	PRGO	48.49	136.5	-6,618.89	06/30/2019	1	-6,618.89	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272956	15272953	1114599636	1319024730	Dividend Income	PRGO	48.49	30	-1,454.7	06/30/2019	1	-1,454.7	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272957	null	1114599636	1319024730	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				).map(detailString -> detailString.replaceAll("PRGO", stock.getSymbol())).toArray(String[]::new))
				.withExpectedReversalDetails(Stream.of(
						"15272958	15272957	1114599636	1319024730	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272959	null	1114599636	1319024730	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272960	15272959	1114599636	1319024730	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272961	15272960	1114599636	1319024730	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO"
				).map(detailString -> detailString.replaceAll("PRGO", stock.getSymbol())).toArray(String[]::new))
				.execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_regenerationForAddedPayoutSucceeds_FrankingCredit() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity securityToCopy = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		InvestmentSecurity stock = this.investmentInstrumentUtils.getCopyOfSecurity(securityToCopy.getSymbol(), "PRGO-" + RandomUtils.randomNumber(1, 99999));

		// Create Last Trade Price
		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm();
		marketDataValueSearchForm.setInvestmentSecurityId(securityToCopy.getId());
		marketDataValueSearchForm.setMeasureDate(DateUtils.toDate("07/01/2019"));
		marketDataValueSearchForm.setDataFieldName("Last Trade Price");
		MarketDataValue value = CollectionUtils.getFirstElement(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
		if (value != null) {
			value.setId(null);
			value.setInvestmentSecurity(stock);
			this.marketDataFieldService.saveMarketDataValue(value);
		}

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						// no populator is picked up so the details look different than with payouts
						"650267	null	2029763800	971328208	Position	12/12/2017	1	1,365		90.7	136.5				null",
						"650268	null	2029763800	971328208	Position	05/02/2019	1	300		88.19	30				null"
				);
		executor.preview();

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_FRANKING_CREDIT));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setAdditionalPayoutValue2(new BigDecimal("0.05"));
		payout.setDefaultElection(true);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		executor.withExpectedEventJournalDetails(
				"650069	null	1114599636	1319024730	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
				"650070	null	1114599636	1319024730	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null"
		)
				.withExpectedJournalDetails(Stream.of(
						"17655217	17655212	CA1800665666	HA984701408	Franking Credits	PRGO	0.05	136.5	6.83	06/30/2019	1	6.83	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17655218	17655212	CA1800665666	HA984701408	Dividend Income	PRGO	48.49	136.5	-6,625.72	06/30/2019	1	-6,625.72	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17655219	17655215	CA1800665666	HA984701408	Franking Credits	PRGO	0.05	30	1.5	06/30/2019	1	1.5	0	06/30/2019	07/05/2019	Franking Tax Credit from Scrip Dividend for PRGO on 07/05/2019",
						"17655220	17655215	CA1800665666	HA984701408	Dividend Income	PRGO	48.49	30	-1,456.2	06/30/2019	1	-1,456.2	0	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"17655221	null	CA1800665666	HA984701408	Position Receivable	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019"
				).map(detailString -> detailString.replaceAll("PRGO", stock.getSymbol())).toArray(String[]::new))
				.withExpectedReversalDetails(Stream.of(
						"15272958	15272957	1114599636	1319024730	Position Receivable	PRGO	48.49	-166.5	-8,073.59	06/30/2019	1	-8,073.59	-8,073.59	06/30/2019	07/05/2019	Reversal of Receivable from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272959	null	1114599636	1319024730	Position	PRGO	48.49	166.5	8,073.59	06/30/2019	1	8,073.59	8,073.59	06/30/2019	07/05/2019	0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272960	15272959	1114599636	1319024730	Position	PRGO	48.49	-0.5	-24.25	06/30/2019	1	-24.25	-24.25	06/30/2019	07/05/2019	Fractional Shares Closing from rounding from 0.1 Scrip Dividend for PRGO on 07/01/2019",
						"15272961	15272960	1114599636	1319024730	Cash	USD			24.25	06/30/2019	1	24.25	0	06/30/2019	07/05/2019	Cash proceeds from close of PRGO"
				).map(detailString -> detailString.replaceAll("PRGO", stock.getSymbol())).toArray(String[]::new))
				.execute();
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_regenerationForElectionChangeSucceeds() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity securityToCopy = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		InvestmentSecurity stock = this.investmentInstrumentUtils.getCopyOfSecurity(securityToCopy.getSymbol(), "PRGO-" + RandomUtils.randomNumber(1, 99999));

		// Create Last Trade Price
		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm();
		marketDataValueSearchForm.setInvestmentSecurityId(securityToCopy.getId());
		marketDataValueSearchForm.setMeasureDate(DateUtils.toDate("07/01/2019"));
		marketDataValueSearchForm.setDataFieldName("Last Trade Price");
		MarketDataValue value = CollectionUtils.getFirstElement(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
		if (value != null) {
			value.setId(null);
			value.setInvestmentSecurity(stock);
			this.marketDataFieldService.saveMarketDataValue(value);
		}

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.setBeforeEventValue(scrip.getBeforeEventValue());
		payout.setAfterEventValue(payout.getBeforeEventValue());
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 2);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeEventValue(new BigDecimal("0.2"));
		payout2.setAfterEventValue(payout2.getBeforeEventValue());
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout2.setDefaultElection(true);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						// no populator is picked up so the details look different than with payouts
						"645473	null	179044819	1644201794	Position	12/12/2017	1	1,365		0.2	273				null",
						"645475	null	179044819	1644201794	Position	05/02/2019	1	300		0.2	60				null"
				).preview();

		try {
			InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
			election.setClientInvestmentAccount(accountInfo.getClientAccount());
			election.setSecurityEvent(scrip);
			election.setElectionNumber((short) 1);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);
			Assertions.fail("Expected exception on reprocess of the event after election change");
		}
		catch (ImsErrorResponseException e) {
			Assertions.assertTrue(e.getErrorMessageFromIMS().contains("Unable to update existing un-booked journal detail with re-processed changes because a payout is no longer applicable. Please fix this error, or delete existing journal."), "Expected exception for election change on event journal reprocess");
		}
	}


	@Test
	public void testScripDividendWithFractionalSharesRoundingOfTwoLots_multiplePayouts_regenerationFailsOnDeletedPayout() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity securityToCopy = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PRGO");
		InvestmentSecurity stock = this.investmentInstrumentUtils.getCopyOfSecurity(securityToCopy.getSymbol(), "PRGO-" + RandomUtils.randomNumber(1, 99999));

		// Create Last Trade Price
		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm();
		marketDataValueSearchForm.setInvestmentSecurityId(securityToCopy.getId());
		marketDataValueSearchForm.setMeasureDate(DateUtils.toDate("07/01/2019"));
		marketDataValueSearchForm.setDataFieldName("Last Trade Price");
		MarketDataValue value = CollectionUtils.getFirstElement(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
		if (value != null) {
			value.setId(null);
			value.setInvestmentSecurity(stock);
			this.marketDataFieldService.saveMarketDataValue(value);
		}

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("1365"), true, DateUtils.toDate("12/12/2017"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("90.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("88.19"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		// Scrip Dividend
		InvestmentSecurityEvent scrip = new InvestmentSecurityEvent();
		scrip.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.DIVIDEND_SCRIP));
		scrip.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		scrip.setSecurity(stock);
		scrip.setBeforeAndAfterEventValue(new BigDecimal("0.1"));
		scrip.setDeclareDate(DateUtils.toDate("06/01/2019"));
		scrip.setExDate(DateUtils.toDate("07/01/2019"));
		scrip.setEventDate(DateUtils.toDate("07/01/2019"));
		scrip.setPaymentDate(DateUtils.toDate("07/01/2019"));
		scrip.setRecordDate(DateUtils.toDate("07/01/2019"));
		scrip = this.investmentSecurityEventService.saveInvestmentSecurityEvent(scrip);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(scrip);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Security (Existing)"));
		payout.setPayoutNumber((short) 1);
		payout.setElectionNumber((short) 1);
		payout.setPayoutSecurity(stock);
		payout.copyBeforeAndAfterEventValues(scrip);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setSecurityEvent(scrip);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout2.setPayoutNumber((short) 2);
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeEventValue(new BigDecimal("0.2"));
		payout2.setAfterEventValue(payout2.getBeforeEventValue());
		payout2.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		payout2 = this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setClientInvestmentAccount(accountInfo.getClientAccount());
		election.setSecurityEvent(scrip);
		election.setElectionNumber((short) 1);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventClientElection(election);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, scrip)
				.withExpectedEventJournalDetails(
						"645472	null	179044819	1644201794	Position	12/12/2017	1	1,365		48.49	6,618.89	136.5	6,618.89	0.5	null",
						"645473	null	179044819	1644201794	Position	12/12/2017	1	1,365		0.2	273				null",
						"645474	null	179044819	1644201794	Position	05/02/2019	1	300		48.49	1,454.7	30	1,454.7	0	null",
						"645475	null	179044819	1644201794	Position	05/02/2019	1	300		0.2	60				null"
				).preview();

		try {
			// now mark payout2 as deleted; will regenerate event details to remove this payout
			payout2.setDeleted(true);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);
			Assertions.fail("Expected exception on reprocess of the event after deleted payout");
		}
		catch (ImsErrorResponseException e) {
			Assertions.assertTrue(e.getErrorMessageFromIMS().contains("Unable to update existing un-booked journal detail with re-processed changes because a payout is no longer applicable. Please fix this error, or delete existing journal."), "Expected exception for deleted payout on event journal reprocess");
		}
	}


	@Test
	public void testReturnOfCapital_TwoLots() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("BXBLY");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("647"), true, DateUtils.toDate("10/22/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("14.80789799072643"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("279"), true, DateUtils.toDate("06/19/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("17.9"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent returnOfCapital = new InvestmentSecurityEvent();
		returnOfCapital.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.RETURN_OF_CAPITAL));
		returnOfCapital.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		returnOfCapital.setSecurity(stock);
		returnOfCapital.setBeforeAndAfterEventValue(new BigDecimal("0.164673"));
		returnOfCapital.setDeclareDate(DateUtils.toDate("09/16/2019"));
		returnOfCapital.setExDate(DateUtils.toDate("10/10/2019"));
		returnOfCapital.setEventDate(DateUtils.toDate("10/23/2019"));
		returnOfCapital.setRecordDate(DateUtils.toDate("10/11/2019"));
		returnOfCapital = this.investmentSecurityEventService.saveInvestmentSecurityEvent(returnOfCapital);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(returnOfCapital);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout.copyBeforeAndAfterEventValues(returnOfCapital);
		payout.setAdditionalPayoutDate(DateUtils.toDate("10/23/2019"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, returnOfCapital)
				.withExpectedEventJournalDetails(
						"664633	null	1628761684	816473179	Position	10/22/2018	1	647	9,580.71	0.164673	106.55		9,474.16		null",
						"664634	null	1628761684	816473179	Position	06/19/2019	1	279	4,994.1	0.164673	45.94		4,948.16		null"
				).withExpectedJournalDetails(
				"15693176	15693171	1582806967	1325927596	Position	BXBLY	14.8078979907	-647	-9,580.71	10/10/2019	1	-9,580.71	-9,580.71	10/22/2018	10/23/2019	Parent Closing from 0.164673 Return of Capital for BXBLY on 10/23/2019",
				"15693177	15693171	1582806967	1325927596	Position	BXBLY	14.64321	647	9,474.16	10/10/2019	1	9,474.16	9,474.16	10/22/2018	10/23/2019	Parent Reopening from 0.164673 Return of Capital for BXBLY on 10/23/2019",
				"15693178	15693171	1582806967	1325927596	Payment Receivable	USD			106.55	10/10/2019	1	106.55	0	10/22/2018	10/23/2019	Accrual of Return of Capital for BXBLY to be paid on 10/23/2019",
				"15693179	15693174	1582806967	1325927596	Position	BXBLY	17.9	-279	-4,994.1	10/10/2019	1	-4,994.1	-4,994.1	06/19/2019	10/23/2019	Parent Closing from 0.164673 Return of Capital for BXBLY on 10/23/2019",
				"15693180	15693174	1582806967	1325927596	Position	BXBLY	17.73534	279	4,948.16	10/10/2019	1	4,948.16	4,948.16	06/19/2019	10/23/2019	Parent Reopening from 0.164673 Return of Capital for BXBLY on 10/23/2019",
				"15693181	15693174	1582806967	1325927596	Payment Receivable	USD			45.94	10/10/2019	1	45.94	0	06/19/2019	10/23/2019	Accrual of Return of Capital for BXBLY to be paid on 10/23/2019"
		).withExpectedReversalDetails(
				"15692878	15692867	261957808	120566224	Payment Receivable	USD			-106.55	10/23/2019	1	-106.55	0	10/22/2018	10/23/2019	Reverse accrual of 0.164673 Return of Capital for BXBLY paid on 10/23/2019",
				"15692879	15692867	261957808	120566224	Cash	USD			106.55	10/23/2019	1	106.55	0	10/22/2018	10/23/2019	0.164673 Return of Capital for BXBLY paid on 10/23/2019",
				"15692880	15692870	261957808	120566224	Payment Receivable	USD			-45.94	10/23/2019	1	-45.94	0	06/19/2019	10/23/2019	Reverse accrual of 0.164673 Return of Capital for BXBLY paid on 10/23/2019",
				"15692881	15692870	261957808	120566224	Cash	USD			45.94	10/23/2019	1	45.94	0	06/19/2019	10/23/2019	0.164673 Return of Capital for BXBLY paid on 10/23/2019"
		).execute();
	}


	@Test
	public void testReturnOfCapital_TwoLots_ExDateAfterEventDate() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), true, DateUtils.toDate("12/07/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("200.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("202.05"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent returnOfCapital = new InvestmentSecurityEvent();
		returnOfCapital.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.RETURN_OF_CAPITAL));
		returnOfCapital.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		returnOfCapital.setSecurity(stock);
		returnOfCapital.setBeforeAndAfterEventValue(new BigDecimal("5"));
		returnOfCapital.setDeclareDate(DateUtils.toDate("06/01/2019"));
		returnOfCapital.setExDate(DateUtils.toDate("07/03/2019"));
		returnOfCapital.setEventDate(DateUtils.toDate("07/01/2019"));
		returnOfCapital.setRecordDate(DateUtils.toDate("07/01/2019"));
		returnOfCapital.setEventDescription("Ex Date After Event Date");
		returnOfCapital = this.investmentSecurityEventService.saveInvestmentSecurityEvent(returnOfCapital);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(returnOfCapital);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout.copyBeforeAndAfterEventValues(returnOfCapital);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, returnOfCapital)
				.withExpectedJournalDetails(
						"15691310	15691305	729299903	826096263	Position	AAPL	200.7	-250	-50,175	07/05/2019	1	-50,175	-50,175	12/07/2018	07/05/2019	Parent Closing from 5 Return of Capital for AAPL on 07/05/2019",
						"15691311	15691305	729299903	826096263	Position	AAPL	195.7	250	48,925	07/05/2019	1	48,925	48,925	12/07/2018	07/05/2019	Parent Reopening from 5 Return of Capital for AAPL on 07/05/2019",
						"15691312	15691305	729299903	826096263	Cash	USD			1,250	07/05/2019	1	1,250	0	12/07/2018	07/05/2019	5 Return of Capital for AAPL paid on 07/05/2019",
						"15691313	15691308	729299903	826096263	Position	AAPL	202.05	-300	-60,615	07/05/2019	1	-60,615	-60,615	05/02/2019	07/05/2019	Parent Closing from 5 Return of Capital for AAPL on 07/05/2019",
						"15691314	15691308	729299903	826096263	Position	AAPL	197.05	300	59,115	07/05/2019	1	59,115	59,115	05/02/2019	07/05/2019	Parent Reopening from 5 Return of Capital for AAPL on 07/05/2019",
						"15691315	15691308	729299903	826096263	Cash	USD			1,500	07/05/2019	1	1,500	0	05/02/2019	07/05/2019	5 Return of Capital for AAPL paid on 07/05/2019"
				)
				.withExpectedEventJournalDetails(
						"664487	null	729299903	826096263	Position	12/07/2018	1	250	50,175	5	1,250		48,925		null",
						"664488	null	729299903	826096263	Position	05/02/2019	1	300	60,615	5	1,500		59,115		null"
				).execute();
	}


	@Test
	public void testReturnOfCapital_ForeignCurrency() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("TITC");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("08/29/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("18.2"))
				.withExchangeRate(new BigDecimal("1.2"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent returnOfCapital = new InvestmentSecurityEvent();
		returnOfCapital.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.RETURN_OF_CAPITAL));
		returnOfCapital.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		returnOfCapital.setSecurity(stock);
		returnOfCapital.setBeforeAndAfterEventValue(new BigDecimal("2"));
		returnOfCapital.setDeclareDate(DateUtils.toDate("09/01/2019"));
		returnOfCapital.setExDate(DateUtils.toDate("09/09/2019"));
		returnOfCapital.setEventDate(DateUtils.toDate("09/11/2019"));
		returnOfCapital.setRecordDate(DateUtils.toDate("09/09/2019"));
		returnOfCapital = this.investmentSecurityEventService.saveInvestmentSecurityEvent(returnOfCapital);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(returnOfCapital);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("EUR", true));
		payout.copyBeforeAndAfterEventValues(returnOfCapital);
		payout.setAdditionalPayoutDate(DateUtils.toDate("09/10/2019"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, returnOfCapital)
				.withExpectedEventJournalDetails(
						"658348	null	1385866162	699039536	Position	08/29/2019	1.1057	100	1,820	2	200		1,620		null"
				)
				.withExpectedJournalDetails(
						"15374240	15374238	131030916	947508437	Position	TITC	18.2	-100	-1,820	09/09/2019	1.2	-2,012.37	-1,820	08/29/2019	09/10/2019	Parent Closing from 2 Return of Capital for TITC on 09/10/2019",
						"15374241	15374238	131030916	947508437	Position	TITC	16.2	100	1,620	09/09/2019	1.2	1,791.23	1,620	08/29/2019	09/10/2019	Parent Reopening from 2 Return of Capital for TITC on 09/10/2019",
						"15374242	15374238	131030916	947508437	Payment Receivable	EUR			200	09/09/2019	1.1057	221.14	0	08/29/2019	09/10/2019	Accrual of Return of Capital for TITC to be paid on 09/10/2019"
				)
				.withExpectedReversalDetails(
						"15374250	15374245	30799581	1796788453	Payment Receivable	EUR			-200	09/10/2019	1.1057	-221.14	0	08/29/2019	09/10/2019	Reverse accrual of 2 Return of Capital for TITC paid on 09/10/2019",
						"15374251	15374245	30799581	1796788453	Currency	EUR			200	09/10/2019	1.1057	221.14	0	08/29/2019	09/10/2019	2 Return of Capital for TITC paid on 09/10/2019"
				).execute();
	}


	@Test
	public void testReturnOfCapital_PartialCloseBeforeExDate() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), true, DateUtils.toDate("12/07/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("200.7"))
				.buildAndSave();
		Trade partialClose = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("50"), false, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("202.05"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(partialClose);

		InvestmentSecurityEvent returnOfCapital = new InvestmentSecurityEvent();
		returnOfCapital.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.RETURN_OF_CAPITAL));
		returnOfCapital.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		returnOfCapital.setSecurity(stock);
		returnOfCapital.setBeforeAndAfterEventValue(new BigDecimal("5"));
		returnOfCapital.setDeclareDate(DateUtils.toDate("06/01/2019"));
		returnOfCapital.setExDate(DateUtils.toDate("06/14/2019"));
		returnOfCapital.setEventDate(DateUtils.toDate("07/01/2019"));
		returnOfCapital.setRecordDate(DateUtils.toDate("07/02/2019"));
		returnOfCapital = this.investmentSecurityEventService.saveInvestmentSecurityEvent(returnOfCapital);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setSecurityEvent(returnOfCapital);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName("Currency"));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout.copyBeforeAndAfterEventValues(returnOfCapital);
		payout.setAdditionalPayoutDate(DateUtils.toDate("07/05/2019"));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, returnOfCapital)
				.withExpectedEventJournalDetails(
						"665513	null	1610671912	1913248995	Position	12/07/2018	1	200	40,140	5	1,000		39,140		null"
				)
				.withExpectedJournalDetails(
						"15693999	15693992	1847041744	1249210454	Position	AAPL	200.7	-200	-40,140	06/14/2019	1	-40,140	-40,140	12/07/2018	07/05/2019	Parent Closing from 5 Return of Capital for AAPL on 07/05/2019",
						"15694000	15693992	1847041744	1249210454	Position	AAPL	195.7	200	39,140	06/14/2019	1	39,140	39,140	12/07/2018	07/05/2019	Parent Reopening from 5 Return of Capital for AAPL on 07/05/2019",
						"15694001	15693992	1847041744	1249210454	Payment Receivable	USD			1,000	06/14/2019	1	1,000	0	12/07/2018	07/05/2019	Accrual of Return of Capital for AAPL to be paid on 07/05/2019"
				)
				.withExpectedReversalDetails(
						"15694019	15694009	646813636	34083406	Payment Receivable	USD			-1,000	07/05/2019	1	-1,000	0	12/07/2018	07/05/2019	Reverse accrual of 5 Return of Capital for AAPL paid on 07/05/2019",
						"15694020	15694009	646813636	34083406	Cash	USD			1,000	07/05/2019	1	1,000	0	12/07/2018	07/05/2019	5 Return of Capital for AAPL paid on 07/05/2019"
				).execute();
	}


	@Test
	public void testMerger_CurrencyPayout() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeAndAfterEventValue(BigDecimal.valueOf(15.6));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.setBeforeEventValue(merger.getBeforeEventValue());
		payout.setAfterEventValue(payout.getBeforeEventValue());
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"658348	null	1385866162	699039536	Position	09/20/2019	1	100	2,430	15.6	1,560				null"
				)
				.withExpectedJournalDetails(
						"15701344	15701342	1652338866	1835852842	Cash	USD			1,560	09/25/2019	1	1,560	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"15701345	15701342	1652338866	1835852842	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15701346	15701345	1652338866	1835852842	Realized Gain / Loss	FNSR	24.3	100	870	09/25/2019	1	870	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for FNSR"
				).execute();
	}


	@Test
	public void testMerger_CurrencyPayout_ForeignCurrency() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeAndAfterEventValue(BigDecimal.valueOf(12.3));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP", true));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.setBeforeEventValue(merger.getBeforeEventValue());
		payout.setAfterEventValue(payout.getBeforeEventValue());
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"658348	null	1385866162	699039536	Position	09/20/2019	1.2379	100	2,430	12.3	1,230				null"
				)
				.withExpectedJournalDetails(
						"15700911	15700909	1532125025	2057710433	Currency	GBP			1,230	09/25/2019	1.2379	1,522.62	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"15700912	15700909	1532125025	2057710433	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15700913	15700912	1532125025	2057710433	Realized Gain / Loss	FNSR	24.3	100	907.38	09/25/2019	1	907.38	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for FNSR"
				).execute();
	}


	@Test
	public void testMerger_SecurityPayout() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeEventValue(BigDecimal.ONE);
		merger.setAfterEventValue(BigDecimal.valueOf(0.2218));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IIVI"));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_NEW));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.copyBeforeAndAfterEventValues(merger);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"658348	null	1385866162	699039536	Position	09/20/2019	1	100	2,430	109.558		22.18	2,430	0.18	null"
				)
				.withExpectedJournalDetails(
						"15701152	15701150	531087102	1069064079	Position	IIVI	109.558	22.18	2,430	09/25/2019	1	2,430	2,430	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"15701153	15701152	531087102	1069064079	Position	IIVI	36.72	-0.18	-19.72	09/25/2019	1	-19.72	-19.72	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"15701154	15701153	531087102	1069064079	Cash	USD			6.61	09/25/2019	1	6.61	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"15701155	15701150	531087102	1069064079	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15701156	15701153	531087102	1069064079	Realized Gain / Loss	IIVI	36.72	0.18	13.11	09/25/2019	1	13.11	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityPayout_ForLoss() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeEventValue(BigDecimal.ONE);
		merger.setAfterEventValue(BigDecimal.valueOf(0.2218));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IIVI"));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_NEW));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.copyBeforeAndAfterEventValues(merger);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutNumber((short) 2);
		payout2.setSecurityEvent(merger);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY));
		payout2.setDefaultElection(true);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeAndAfterEventValue(BigDecimal.valueOf(15.6));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"664735	null	1073712643	2017593743	Position	09/20/2019	1	100	2,430	39.2245		22.18	870	0.18	null",
						"664736	null	1073712643	2017593743	Position	09/20/2019	1	100	2,430	15.6	1,560				null"
				)
				.withExpectedJournalDetails(
						"15701853	15701851	30544793	1787745545	Position	IIVI	39.2245	22.18	870	09/25/2019	1	870	870	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"15701854	15701853	30544793	1787745545	Position	IIVI	36.72	-0.18	-7.06	09/25/2019	1	-7.06	-7.06	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"15701855	15701854	30544793	1787745545	Cash	USD			6.61	09/25/2019	1	6.61	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"15701856	15701851	30544793	1787745545	Cash	USD			1,560	09/25/2019	1	1,560	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"15701857	15701851	30544793	1787745545	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15701858	15701854	30544793	1787745545	Realized Gain / Loss	IIVI	36.72	0.18	0.45	09/25/2019	1	0.45	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityPayout_ForGain() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeEventValue(BigDecimal.ONE);
		merger.setAfterEventValue(BigDecimal.valueOf(0.2218));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IIVI"));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_NEW));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.copyBeforeAndAfterEventValues(merger);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutNumber((short) 2);
		payout2.setSecurityEvent(merger);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY));
		payout2.setDefaultElection(true);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true));
		payout2.setBeforeAndAfterEventValue(BigDecimal.valueOf(20.6));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"664727	null	1614164607	1649995062	Position	09/20/2019	1	100	2,430	36.72		22.18	814.45	0.18	null",
						"664728	null	1614164607	1649995062	Position	09/20/2019	1	100	2,430	20.6	2,060				null"
				)
				.withExpectedJournalDetails(
						"15738605	15738603	1770793356	1602038780	Position	IIVI	36.72	22.18	814.45	09/25/2019	1	814.45	814.45	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"15738606	15738605	1770793356	1602038780	Position	IIVI	36.72	-0.18	-6.61	09/25/2019	1	-6.61	-6.61	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"15738607	15738606	1770793356	1602038780	Cash	USD			6.61	09/25/2019	1	6.61	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"15738608	15738603	1770793356	1602038780	Cash	USD			2,060	09/25/2019	1	2,060	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"15738609	15738603	1770793356	1602038780	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15738610	15738609	1770793356	1602038780	Realized Gain / Loss	FNSR	24.3	100	-444.45	09/25/2019	1	-444.45	0	09/20/2019	09/25/2019	Realized Gain / Loss gain for FNSR"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityPayout_ForeignCurrency_ForLoss() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeEventValue(BigDecimal.ONE);
		merger.setAfterEventValue(BigDecimal.valueOf(0.2218));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IIVI"));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_NEW));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.copyBeforeAndAfterEventValues(merger);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutNumber((short) 2);
		payout2.setSecurityEvent(merger);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY));
		payout2.setDefaultElection(true);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP", true));
		payout2.setBeforeAndAfterEventValue(BigDecimal.valueOf(12.56));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"664676	null	279803179	831281717	Position	09/20/2019	1	100	2,430	39.459		22.18	875.2	0.18	null",
						"664677	null	279803179	831281717	Position	09/20/2019	1.2379	100	2,430	12.56	1,256				null"
				)
				.withExpectedJournalDetails(
						"15701575	15701573	1060005483	1448071793	Position	IIVI	39.459	22.18	875.2	09/25/2019	1	875.2	875.2	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"15701576	15701575	1060005483	1448071793	Position	IIVI	36.72	-0.18	-7.1	09/25/2019	1	-7.1	-7.1	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"15701577	15701576	1060005483	1448071793	Cash	USD			6.61	09/25/2019	1	6.61	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"15701578	15701573	1060005483	1448071793	Currency	GBP			1,256	09/25/2019	1.2379	1,554.8	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"15701579	15701573	1060005483	1448071793	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15701580	15701576	1060005483	1448071793	Realized Gain / Loss	IIVI	36.72	0.18	0.49	09/25/2019	1	0.49	0	09/20/2019	09/25/2019	Realized Gain / Loss loss for IIVI"
				).execute();
	}


	@Test
	public void testMerger_CurrencyAndSecurityPayout_ForeignCurrency_ForGain() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FNSR");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("100"), true, DateUtils.toDate("09/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("24.3"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent merger = new InvestmentSecurityEvent();
		merger.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.MERGER));
		merger.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		merger.setSecurity(stock);
		merger.setBeforeEventValue(BigDecimal.ONE);
		merger.setAfterEventValue(BigDecimal.valueOf(0.2218));
		merger.setDeclareDate(DateUtils.toDate("01/07/2019"));
		merger.setExDate(DateUtils.toDate("09/25/2019"));
		merger.setEventDate(merger.getExDate());
		merger.setRecordDate(merger.getExDate());
		merger.setAdditionalSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IIVI"));
		merger = this.investmentSecurityEventService.saveInvestmentSecurityEvent(merger);

		InvestmentSecurityEventPayout payout = new InvestmentSecurityEventPayout();
		payout.setElectionNumber((short) 1);
		payout.setPayoutNumber((short) 1);
		payout.setSecurityEvent(merger);
		payout.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.SECURITY_NEW));
		payout.setDefaultElection(true);
		payout.setPayoutSecurity(merger.getAdditionalSecurity());
		payout.copyBeforeAndAfterEventValues(merger);
		payout.setFractionalSharesMethod(FractionalShares.CASH_IN_LIEU);
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);

		InvestmentSecurityEventPayout payout2 = new InvestmentSecurityEventPayout();
		payout2.setElectionNumber((short) 1);
		payout2.setPayoutNumber((short) 2);
		payout2.setSecurityEvent(merger);
		payout2.setPayoutType(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(InvestmentSecurityEventPayoutType.CURRENCY));
		payout2.setDefaultElection(true);
		payout2.setPayoutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP", true));
		payout2.setBeforeAndAfterEventValue(BigDecimal.valueOf(16.56));
		this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout2);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, merger)
				.withExpectedEventJournalDetails(
						"664731	null	1417298673	1674724748	Position	09/20/2019	1	100	2,430	36.72		22.18	814.45	0.18	null",
						"664732	null	1417298673	1674724748	Position	09/20/2019	1.2379	100	2,430	16.56	1,656				null"
				)
				.withExpectedJournalDetails(
						"15737992	15737990	551033126	2094281031	Position	IIVI	36.72	22.18	814.45	09/25/2019	1	814.45	814.45	09/20/2019	09/25/2019	Position opening of IIVI from Merger on 09/25/2019",
						"15737993	15737992	551033126	2094281031	Position	IIVI	36.72	-0.18	-6.61	09/25/2019	1	-6.61	-6.61	09/20/2019	09/25/2019	Fractional Shares Closing from rounding from 1 to 0.2218 IIVI from Merger for FNSR on 09/25/2019",
						"15737994	15737993	551033126	2094281031	Cash	USD			6.61	09/25/2019	1	6.61	0	09/20/2019	09/25/2019	Cash proceeds from close of IIVI Merger fractional shares",
						"15737995	15737990	551033126	2094281031	Currency	GBP			1,656	09/25/2019	1.2379	2,049.96	0	09/20/2019	09/25/2019	Cash payment from FNSR Merger on 09/25/2019",
						"15737996	15737990	551033126	2094281031	Position	FNSR	24.3	-100	-2,430	09/25/2019	1	-2,430	-2,430	09/20/2019	09/25/2019	Position closing of FNSR from Merger on 09/25/2019",
						"15737997	15737996	551033126	2094281031	Realized Gain / Loss	FNSR	24.3	100	-434.41	09/25/2019	1	-434.41	0	09/20/2019	09/25/2019	Realized Gain / Loss gain for FNSR"
				).execute();
	}


	@Test
	public void testStockDividend_TwoLots_Cash_In_Lieu() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ACSAY");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), true, DateUtils.toDate("12/07/2018"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.7"))
				.buildAndSave();
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("300"), true, DateUtils.toDate("05/02/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.4"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent stockDividend = new InvestmentSecurityEvent();
		stockDividend.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT));
		stockDividend.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		stockDividend.setSecurity(stock);
		stockDividend.setAfterEventValue(new BigDecimal("0.01315789"));
		stockDividend.setDeclareDate(DateUtils.toDate("06/01/2019"));
		stockDividend.setExDate(DateUtils.toDate("06/14/2019"));
		stockDividend.setEventDate(DateUtils.toDate("07/01/2019"));
		stockDividend.setRecordDate(DateUtils.toDate("07/02/2019"));
		stockDividend.setEventDescription("Cash In Lieu");
		stockDividend = this.investmentSecurityEventService.saveInvestmentSecurityEvent(stockDividend);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, stockDividend)
				.withExpectedEventJournalDetails(
						"658525	null	1262566783	345510920	Position	12/07/2018	1	250	1,925	7.7	7.6	253.2894725	1,925	0.2368395	null",
						"658526	null	1262566783	345510920	Position	05/02/2019	1	300	2,220	7.4	7.3039	303.947367	2,220	0	null"
				)
				.withExpectedJournalDetails(
						"15691951	15691946	397106368	2014576889	Position	ACSAY	7.7	-250	-1,925	06/13/2019	1	-1,925	-1,925	12/07/2018	06/13/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15691952	15691946	397106368	2014576889	Position	ACSAY	7.6	250	1,900	06/13/2019	1	1,900	1,900	12/07/2018	06/13/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15691953	15691946	397106368	2014576889	Position Receivable	ACSAY	7.6	3.2894725	25	06/13/2019	1	25	25	12/07/2018	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15691954	15691949	397106368	2014576889	Position	ACSAY	7.4	-300	-2,220	06/13/2019	1	-2,220	-2,220	05/02/2019	06/13/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15691955	15691949	397106368	2014576889	Position	ACSAY	7.3039	300	2,191.17	06/13/2019	1	2,191.17	2,191.17	05/02/2019	06/13/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15691956	15691949	397106368	2014576889	Position Receivable	ACSAY	7.3039	3.947367	28.83	06/13/2019	1	28.83	28.83	05/02/2019	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019"
				)
				.withExpectedReversalDetails(
						"16129756	16129752	CA79748427	HA1903204670	Position Receivable	ACSAY	7.6	-3.2894725	-25	07/01/2019	1	-25	-25	12/07/2018	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129757	16129751	CA79748427	HA1903204670	Position	ACSAY	7.6	-250	-1,900	07/01/2019	1	-1,900	-1,900	12/07/2018	07/01/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129758	16129745	CA79748427	HA1903204670	Position	ACSAY	7.6	253.2894725	1,925	07/01/2019	1	1,925	1,925	12/07/2018	07/01/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						// fractional shares are aggregated by holding account and processed for 1st detail instead of separately for each detail
						"16129759	16129758	CA79748427	HA1903204670	Position	ACSAY	8.27	-0.2368395	-1.8	07/01/2019	1	-1.8	-1.8	12/07/2018	07/01/2019	Fractional Shares Closing from rounding from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129760	16129755	CA79748427	HA1903204670	Position Receivable	ACSAY	7.3039	-3.947367	-28.83	07/01/2019	1	-28.83	-28.83	05/02/2019	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129761	16129754	CA79748427	HA1903204670	Position	ACSAY	7.3039	-300	-2,191.17	07/01/2019	1	-2,191.17	-2,191.17	05/02/2019	07/01/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129762	16129748	CA79748427	HA1903204670	Position	ACSAY	7.3039	303.947367	2,220	07/01/2019	1	2,220	2,220	05/02/2019	07/01/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129763	16129759	CA79748427	HA1903204670	Cash	USD			1.96	07/01/2019	1	1.96	0	12/07/2018	07/01/2019	Cash proceeds from close of ACSAY",
						"16129764	16129759	CA79748427	HA1903204670	Realized Gain / Loss	ACSAY	8.27	0.2368395	-0.16	07/01/2019	1	-0.16	0	12/07/2018	07/01/2019	Realized Gain / Loss gain for ACSAY"
				).execute();
	}


	@Test
	public void testStockDividend_TwoLots_CashInLieu_PartialClose() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ACSAY");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), true, DateUtils.toDate("05/28/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.7"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("200"), true, DateUtils.toDate("05/29/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.8"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent stockDividend = new InvestmentSecurityEvent();
		stockDividend.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT));
		stockDividend.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		stockDividend.setSecurity(stock);
		stockDividend.setAfterEventValue(new BigDecimal("0.01315789"));
		stockDividend.setDeclareDate(DateUtils.toDate("06/01/2019"));
		stockDividend.setExDate(DateUtils.toDate("06/18/2019"));
		stockDividend.setEventDate(DateUtils.toDate("07/01/2019"));
		stockDividend.setRecordDate(DateUtils.toDate("07/02/2019"));
		stockDividend.setEventDescription("Cash In Lieu");
		stockDividend = this.investmentSecurityEventService.saveInvestmentSecurityEvent(stockDividend);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, stockDividend)
				.withGenerationDate(stockDividend.getExDate())
				.withExpectedEventJournalDetails(
						"658534	null	1203457878	1944678253	Position	05/28/2019	1	250	1,925	7.7	7.6	253.2894725	1,925	0.9210505	null",
						"658535	null	1203457878	1944678253	Position	05/29/2019	1	200	1,560	7.8	7.6987	202.631578	1,560	0	null"
				)
				.withExpectedJournalDetails(
						"15492096	15492089	874694386	1999434987	Position	ACSAY	7.7	-250	-1,925	06/17/2019	1	-1,925	-1,925	05/28/2019	06/17/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492097	15492089	874694386	1999434987	Position	ACSAY	7.6	250	1,900	06/17/2019	1	1,900	1,900	05/28/2019	06/17/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492098	15492089	874694386	1999434987	Position Receivable	ACSAY	7.6	3.2894725	25	06/17/2019	1	25	25	05/28/2019	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492099	15492091	874694386	1999434987	Position	ACSAY	7.8	-200	-1,560	06/17/2019	1	-1,560	-1,560	05/29/2019	06/17/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492100	15492091	874694386	1999434987	Position	ACSAY	7.6987	200	1,539.74	06/17/2019	1	1,539.74	1,539.74	05/29/2019	06/17/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492101	15492091	874694386	1999434987	Position Receivable	ACSAY	7.6987	2.631578	20.26	06/17/2019	1	20.26	20.26	05/29/2019	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019"
				);
		executor.execute();

		// partial close (after ex date but before event date)
		Trade partialClose = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("10"), false, DateUtils.toDate("06/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("8.1"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(partialClose);

		executor.withGenerationDate(stockDividend.getEventDate())
				.withExpectedReversalDetails(
						"16129602	16129595	CA124102543	HA1048295531	Position Receivable	ACSAY	7.6	-3.2894725	-25	07/01/2019	1	-25	-25	05/28/2019	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129603	16129594	CA124102543	HA1048295531	Position	ACSAY	7.6	-240	-1,824	07/01/2019	1	-1,824	-1,824	05/28/2019	07/01/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129604	16129589	CA124102543	HA1048295531	Position	ACSAY	7.6	243.2894725	1,849	07/01/2019	1	1,849	1,849	05/28/2019	07/01/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129605	16129604	CA124102543	HA1048295531	Position	ACSAY	8.27	-0.9210505	-7	07/01/2019	1	-7	-7	05/28/2019	07/01/2019	Fractional Shares Closing from rounding from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129606	16129598	CA124102543	HA1048295531	Position Receivable	ACSAY	7.6987	-2.631578	-20.26	07/01/2019	1	-20.26	-20.26	05/29/2019	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129607	16129597	CA124102543	HA1048295531	Position	ACSAY	7.6987	-200	-1,539.74	07/01/2019	1	-1,539.74	-1,539.74	05/29/2019	07/01/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129608	16129591	CA124102543	HA1048295531	Position	ACSAY	7.6987	202.631578	1,560	07/01/2019	1	1,560	1,560	05/29/2019	07/01/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129609	16129605	CA124102543	HA1048295531	Cash	USD			7.62	07/01/2019	1	7.62	0	05/28/2019	07/01/2019	Cash proceeds from close of ACSAY",
						"16129610	16129605	CA124102543	HA1048295531	Realized Gain / Loss	ACSAY	8.27	0.9210505	-0.62	07/01/2019	1	-0.62	0	05/28/2019	07/01/2019	Realized Gain / Loss gain for ACSAY"
				).execute();
	}


	@Test
	public void testStockDividend_TwoLots_CashInLieu_FullyCloseLot1_PartialCloseLot2() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ACSAY");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), true, DateUtils.toDate("05/28/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.7"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("200"), true, DateUtils.toDate("05/29/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.8"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent stockDividend = new InvestmentSecurityEvent();
		stockDividend.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT));
		stockDividend.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		stockDividend.setSecurity(stock);
		stockDividend.setAfterEventValue(new BigDecimal("0.01315789"));
		stockDividend.setDeclareDate(DateUtils.toDate("06/01/2019"));
		stockDividend.setExDate(DateUtils.toDate("06/18/2019"));
		stockDividend.setEventDate(DateUtils.toDate("07/01/2019"));
		stockDividend.setRecordDate(DateUtils.toDate("07/02/2019"));
		stockDividend.setEventDescription("Cash In Lieu");
		stockDividend = this.investmentSecurityEventService.saveInvestmentSecurityEvent(stockDividend);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, stockDividend)
				.withGenerationDate(stockDividend.getExDate())
				.withExpectedEventJournalDetails(
						"658534	null	1203457878	1944678253	Position	05/28/2019	1	250	1,925	7.7	7.6	253.2894725	1,925	0.9210505	null",
						"658535	null	1203457878	1944678253	Position	05/29/2019	1	200	1,560	7.8	7.6987	202.631578	1,560	0	null"
				)
				.withExpectedJournalDetails(
						"15492096	15492089	874694386	1999434987	Position	ACSAY	7.7	-250	-1,925	06/17/2019	1	-1,925	-1,925	05/28/2019	06/17/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492097	15492089	874694386	1999434987	Position	ACSAY	7.6	250	1,900	06/17/2019	1	1,900	1,900	05/28/2019	06/17/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492098	15492089	874694386	1999434987	Position Receivable	ACSAY	7.6	3.2894725	25	06/17/2019	1	25	25	05/28/2019	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492099	15492091	874694386	1999434987	Position	ACSAY	7.8	-200	-1,560	06/17/2019	1	-1,560	-1,560	05/29/2019	06/17/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492100	15492091	874694386	1999434987	Position	ACSAY	7.6987	200	1,539.74	06/17/2019	1	1,539.74	1,539.74	05/29/2019	06/17/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492101	15492091	874694386	1999434987	Position Receivable	ACSAY	7.6987	2.631578	20.26	06/17/2019	1	20.26	20.26	05/29/2019	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019"
				);
		executor.execute();

		// fully close lot 1 (250 shares) and partially close lot 2 (3 shares)
		Trade partialClose = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("253"), false, DateUtils.toDate("06/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("8.1"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(partialClose);

		executor.withGenerationDate(stockDividend.getEventDate())
				.withExpectedReversalDetails(
						// since lot 1 has been fully closed, the only thing to be done is reverse the receivable and open a position
						"16129842	16129831	CA1390053317	HA446112821	Position Receivable	ACSAY	7.6	-3.2894725	-25	07/01/2019	1	-25	-25	05/28/2019	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129843	16129825	CA1390053317	HA446112821	Position	ACSAY	7.6	3.2894725	25	07/01/2019	1	25	25	05/28/2019	07/01/2019	0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129844	16129843	CA1390053317	HA446112821	Position	ACSAY	8.27	-0.9210505	-7	07/01/2019	1	-7	-7	05/28/2019	07/01/2019	Fractional Shares Closing from rounding from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						// since lot 2 has been partially closed, the remaining position and the receivable can still be combined into one position of 199 shares
						"16129845	16129834	CA1390053317	HA446112821	Position Receivable	ACSAY	7.6987	-2.631578	-20.26	07/01/2019	1	-20.26	-20.26	05/29/2019	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129846	16129833	CA1390053317	HA446112821	Position	ACSAY	7.6987	-197	-1,516.64	07/01/2019	1	-1,516.64	-1,516.64	05/29/2019	07/01/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129847	16129827	CA1390053317	HA446112821	Position	ACSAY	7.6987	199.631578	1,536.9	07/01/2019	1	1,536.9	1,536.9	05/29/2019	07/01/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129848	16129844	CA1390053317	HA446112821	Cash	USD			7.62	07/01/2019	1	7.62	0	05/28/2019	07/01/2019	Cash proceeds from close of ACSAY",
						"16129849	16129844	CA1390053317	HA446112821	Realized Gain / Loss	ACSAY	8.27	0.9210505	-0.62	07/01/2019	1	-0.62	0	05/28/2019	07/01/2019	Realized Gain / Loss gain for ACSAY"
				).execute();
	}


	@Test
	public void testStockDividend_OneLot_CashInLieu_FullCloseAfterAccrual() {
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		InvestmentSecurity stock = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ACSAY");

		Trade trade = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), true, DateUtils.toDate("05/28/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("7.7"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent stockDividend = new InvestmentSecurityEvent();
		stockDividend.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.STOCK_DIVIDEND_PAYMENT));
		stockDividend.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		stockDividend.setSecurity(stock);
		stockDividend.setAfterEventValue(new BigDecimal("0.01315789"));
		stockDividend.setDeclareDate(DateUtils.toDate("06/01/2019"));
		stockDividend.setExDate(DateUtils.toDate("06/18/2019"));
		stockDividend.setEventDate(DateUtils.toDate("07/01/2019"));
		stockDividend.setRecordDate(DateUtils.toDate("07/02/2019"));
		stockDividend.setEventDescription("Cash In Lieu");
		stockDividend = this.investmentSecurityEventService.saveInvestmentSecurityEvent(stockDividend);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, stockDividend)
				.withGenerationDate(stockDividend.getExDate())
				.withExpectedEventJournalDetails(
						"658534	null	CA1203457878	HA1944678253	Position	05/28/2019	1	250	1,925	7.7	7.6	253.2894725	1,925	0.2894725	null"
				)
				.withExpectedJournalDetails(
						"15492096	15492089	CA874694386	HA1999434987	Position	ACSAY	7.7	-250	-1,925	06/17/2019	1	-1,925	-1,925	05/28/2019	06/17/2019	Closing from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492097	15492089	CA874694386	HA1999434987	Position	ACSAY	7.6	250	1,900	06/17/2019	1	1,900	1,900	05/28/2019	06/17/2019	Reopening from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"15492098	15492089	CA874694386	HA1999434987	Position Receivable	ACSAY	7.6	3.2894725	25	06/17/2019	1	25	25	05/28/2019	07/01/2019	Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019"
				);
		executor.execute();

		// full close (250 shares)
		Trade partialClose = this.tradeUtils.newStocksTradeBuilder(stock, new BigDecimal("250"), false, DateUtils.toDate("06/20/2019"), this.goldmanSachs, accountInfo)
				.withAverageUnitPrice(new BigDecimal("8.1"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(partialClose);

		executor.withGenerationDate(stockDividend.getEventDate())
				.withExpectedReversalDetails(
						"16129779	16129774	CA119961674	HA1472837342	Position Receivable	ACSAY	7.6	-3.2894725	-25	07/01/2019	1	-25	-25	05/28/2019	07/01/2019	Reversal of Receivable from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129780	16129770	CA119961674	HA1472837342	Position	ACSAY	7.6	3.2894725	25	07/01/2019	1	25	25	05/28/2019	07/01/2019	0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129781	16129780	CA119961674	HA1472837342	Position	ACSAY	8.27	-0.2894725	-2.2	07/01/2019	1	-2.2	-2.2	05/28/2019	07/01/2019	Fractional Shares Closing from rounding from 0.01315789 Stock Dividend Payment for ACSAY on 07/01/2019",
						"16129782	16129781	CA119961674	HA1472837342	Cash	USD			2.39	07/01/2019	1	2.39	0	05/28/2019	07/01/2019	Cash proceeds from close of ACSAY",
						"16129783	16129781	CA119961674	HA1472837342	Realized Gain / Loss	ACSAY	8.27	0.2894725	-0.19	07/01/2019	1	-0.19	0	05/28/2019	07/01/2019	Realized Gain / Loss gain for ACSAY"
				).execute();
	}


	@Test
	public void testInterestShortfallPayment_OneLot() {
		String swapSuffix = DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS);
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurity("SDBB4TC33333JKXXWR.3.0.0", "SDBB4TC33333JKXXWR.3.0.0_" + swapSuffix);

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BOA_CUSTODIAN, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.creditDefaultSwaps);
		swap.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		this.investmentInstrumentService.saveInvestmentSecurity(swap);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		Trade trade = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, new BigDecimal("5500000"), BigDecimal.ONE, false, new BigDecimal("86.8398092"), DateUtils.toDate("01/05/2017"), executingBroker, accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent interestShortfall = new InvestmentSecurityEvent();
		interestShortfall.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.INTEREST_SHORTFALL_PAYMENT));
		interestShortfall.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		interestShortfall.setSecurity(swap);
		interestShortfall.setBeforeAndAfterEventValue(new BigDecimal("21.34012651"));
		interestShortfall.setDeclareDate(DateUtils.toDate("06/25/2019"));
		interestShortfall.setExDate(DateUtils.toDate("08/25/2019"));
		interestShortfall.setEventDate(DateUtils.toDate("09/25/2019"));
		interestShortfall.setRecordDate(DateUtils.toDate("06/25/2019"));
		interestShortfall = this.investmentSecurityEventService.saveInvestmentSecurityEvent(interestShortfall);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, interestShortfall)
				.withExpectedEventJournalDetails(
						"673155	null	CA1952901241	HA31748004	Position	01/05/2017	1		-5,500,000		-117.37			1	null"
				)
				.withExpectedJournalDetails(Stream.of(
						"15967829	15967826	CA1500896635	HA2030988233	Leg Receivable	USD			-117.37	08/24/2019	1	-117.37	0	08/24/2019	09/25/2019	Accrual of Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191118120329 to be paid on 09/25/2019",
						"15967830	15967826	CA1500896635	HA2030988233	Premium Leg	SDBB4TC33333JKXXWR.3.0.0_20191118120329			117.37	08/24/2019	1	117.37	0	08/24/2019	09/25/2019	21.34012651 Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191118120329 on 09/25/2019"
				).map(detailString -> detailString.replaceAll("20191118120329", swapSuffix)).toArray(String[]::new))
				.withExpectedReversalDetails(Stream.of(
						"15967866	15967861	CA1880190515	HA1732216400	Leg Receivable	USD			117.37	09/25/2019	1	117.37	0	09/25/2019	09/25/2019	Reverse accrual of Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191118120329 from 08/24/2019",
						"15967867	15967861	CA1880190515	HA1732216400	Cash	USD			-117.37	09/25/2019	1	-117.37	0	09/25/2019	09/25/2019	21.34012651 Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191118120329 on 09/25/2019"
				).map(detailString -> detailString.replaceAll("20191118120329", swapSuffix)).toArray(String[]::new));
		executor.execute();
	}


	@Test
	public void testInterestShortfallPayment_TwoLots_PartialCloseBeforeExDate() {
		String swapSuffix = DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS);
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurity("SDBB4TC33333JKXXWR.3.0.0", "SDBB4TC33333JKXXWR.3.0.0_" + swapSuffix);

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BOA_CUSTODIAN, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.creditDefaultSwaps);
		swap.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		this.investmentInstrumentService.saveInvestmentSecurity(swap);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		Trade trade = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, new BigDecimal("5500000"), BigDecimal.ONE, false, new BigDecimal("86.8398092"), DateUtils.toDate("01/05/2017"), executingBroker, accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		Trade trade2 = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, new BigDecimal("500000"), BigDecimal.ONE, true, new BigDecimal("86.8398092"), DateUtils.toDate("01/05/2018"), executingBroker, accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade2);

		InvestmentSecurityEvent interestShortfall = new InvestmentSecurityEvent();
		interestShortfall.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.INTEREST_SHORTFALL_PAYMENT));
		interestShortfall.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		interestShortfall.setSecurity(swap);
		interestShortfall.setBeforeAndAfterEventValue(new BigDecimal("21.34012651"));
		interestShortfall.setDeclareDate(DateUtils.toDate("06/25/2019"));
		interestShortfall.setExDate(DateUtils.toDate("08/25/2019"));
		interestShortfall.setEventDate(DateUtils.toDate("09/25/2019"));
		interestShortfall.setRecordDate(DateUtils.toDate("06/25/2019"));
		interestShortfall = this.investmentSecurityEventService.saveInvestmentSecurityEvent(interestShortfall);

		AccountingBookingRuleIntegrationTestExecutor executor = AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, interestShortfall)
				.withExpectedEventJournalDetails(
						"673150	15967791	CA541598782	HA18938386	Position	01/05/2017	1		-5,000,000		-106.7			1	null"
				)
				.withExpectedJournalDetails(Stream.of(
						"15967811	15967808	CA1886022729	HA1298146112	Leg Receivable	USD			-106.7	08/24/2019	1	-106.7	0	08/24/2019	09/25/2019	Accrual of Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191119095017 to be paid on 09/25/2019",
						"15967812	15967808	CA1886022729	HA1298146112	Premium Leg	SDBB4TC33333JKXXWR.3.0.0_20191119095017			106.7	08/24/2019	1	106.7	0	08/24/2019	09/25/2019	21.34012651 Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191119095017 on 09/25/2019"
				).map(detailString -> detailString.replaceAll("20191119095017", swapSuffix)).toArray(String[]::new))
				.withExpectedReversalDetails(Stream.of(
						"15967837	15967832	CA1123915284	HA1816629200	Leg Receivable	USD			106.7	09/25/2019	1	106.7	0	09/25/2019	09/25/2019	Reverse accrual of Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191119095017 from 08/24/2019",
						"15967838	15967832	CA1123915284	HA1816629200	Cash	USD			-106.7	09/25/2019	1	-106.7	0	09/25/2019	09/25/2019	21.34012651 Interest Shortfall Payment for SDBB4TC33333JKXXWR.3.0.0_20191119095017 on 09/25/2019"
				).map(detailString -> detailString.replaceAll("20191119095017", swapSuffix)).toArray(String[]::new));
		executor.execute();
	}


	@Test
	public void testInterestShortfallReimbursementPayment_OneLot() {
		String swapSuffix = DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS);
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurity("SDBB4TC33333JKXXWR.3.0.0", "SDBB4TC33333JKXXWR.3.0.0_" + swapSuffix);

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BOA_CUSTODIAN, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.creditDefaultSwaps);
		swap.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		this.investmentInstrumentService.saveInvestmentSecurity(swap);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		Trade trade = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, new BigDecimal("5500000"), BigDecimal.ONE, false, new BigDecimal("86.8398092"), DateUtils.toDate("01/05/2017"), executingBroker, accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		InvestmentSecurityEvent interestShortfall = new InvestmentSecurityEvent();
		interestShortfall.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.INTEREST_SHORTFALL_REIMBURSEMENT_PAYMENT));
		interestShortfall.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		interestShortfall.setSecurity(swap);
		interestShortfall.setBeforeAndAfterEventValue(new BigDecimal("0.000912629"));
		interestShortfall.setDeclareDate(DateUtils.toDate("06/25/2019"));
		interestShortfall.setExDate(DateUtils.toDate("08/25/2019"));
		interestShortfall.setEventDate(DateUtils.toDate("09/25/2019"));
		interestShortfall.setRecordDate(DateUtils.toDate("06/25/2019"));
		interestShortfall = this.investmentSecurityEventService.saveInvestmentSecurityEvent(interestShortfall);

		AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, interestShortfall)
				.withExpectedEventJournalDetails(
						"673152	null	CA422494888	HA254342284	Position	01/05/2017	1		-5,500,000		0.01			1	null"
				)
				.withExpectedJournalDetails(Stream.of(
						"15967821	15967818	CA1757281255	HA488494278	Leg Receivable	USD			0.01	08/24/2019	1	0.01	0	08/24/2019	09/25/2019	Accrual of Interest Shortfall Reimbursement Payment for SDBB4TC33333JKXXWR.3.0.0_20191119170145 to be paid on 09/25/2019",
						"15967822	15967818	CA1757281255	HA488494278	Premium Leg	SDBB4TC33333JKXXWR.3.0.0_20191119170145			-0.01	08/24/2019	1	-0.01	0	08/24/2019	09/25/2019	0.000912629 Interest Shortfall Reimbursement Payment for SDBB4TC33333JKXXWR.3.0.0_20191119170145 on 09/25/2019"
				).map(detailString -> detailString.replaceAll("20191119170145", swapSuffix)).toArray(String[]::new))
				.withExpectedReversalDetails(Stream.of(
						"15967837	15967832	CA754153909	HA530068802	Leg Receivable	USD			-0.01	09/25/2019	1	-0.01	0	09/25/2019	09/25/2019	Reverse accrual of Interest Shortfall Reimbursement Payment for SDBB4TC33333JKXXWR.3.0.0_20191119170145 from 08/24/2019",
						"15967838	15967832	CA754153909	HA530068802	Cash	USD			0.01	09/25/2019	1	0.01	0	09/25/2019	09/25/2019	0.000912629 Interest Shortfall Reimbursement Payment for SDBB4TC33333JKXXWR.3.0.0_20191119170145 on 09/25/2019"
				).map(detailString -> detailString.replaceAll("20191119170145", swapSuffix)).toArray(String[]::new)).execute();
	}


	@Test
	public void testInterestShortfallReimbursementPayment_TwoLots_PartialCloseBeforeExDate() {
		String swapSuffix = DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS);
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurity("SDBB4TC33333JKXXWR.3.0.0", "SDBB4TC33333JKXXWR.3.0.0_" + swapSuffix);

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BOA_CUSTODIAN, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.creditDefaultSwaps);
		swap.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		this.investmentInstrumentService.saveInvestmentSecurity(swap);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.BOA_CUSTODIAN);

		Trade trade = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, new BigDecimal("5500000"), BigDecimal.ONE, false, new BigDecimal("86.8398092"), DateUtils.toDate("01/05/2017"), executingBroker, accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);
		Trade trade2 = this.tradeUtils.newCreditDefaultSwapTradeBuilder(swap, new BigDecimal("500000"), BigDecimal.ONE, true, new BigDecimal("86.8398092"), DateUtils.toDate("01/05/2018"), executingBroker, accountInfo)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade2);

		AssertionFailedError exception = Assertions.assertThrows(AssertionFailedError.class, () -> {
			InvestmentSecurityEvent interestShortfall = new InvestmentSecurityEvent();
			interestShortfall.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.INTEREST_SHORTFALL_REIMBURSEMENT_PAYMENT));
			interestShortfall.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
			interestShortfall.setSecurity(swap);
			interestShortfall.setBeforeAndAfterEventValue(new BigDecimal("0.000912629"));
			interestShortfall.setDeclareDate(DateUtils.toDate("06/25/2019"));
			interestShortfall.setExDate(DateUtils.toDate("08/25/2019"));
			interestShortfall.setEventDate(DateUtils.toDate("09/25/2019"));
			interestShortfall.setRecordDate(DateUtils.toDate("06/25/2019"));
			interestShortfall = this.investmentSecurityEventService.saveInvestmentSecurityEvent(interestShortfall);

			// no journal because no payment amount = 0
			AccountingBookingRuleIntegrationTestExecutor.newExecutorForAccountAndEvent(this.accountingUtils, accountInfo, interestShortfall).execute();
		});
		Assertions.assertEquals("Event journal was not generated. ==> expected: not <null>", exception.getMessage());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void testCouponEventDoTrades(InvestmentSecurity bond, AccountInfo accountInfo) {
		Date tradeDate = DateUtils.toDate("08/05/2013");
		BigDecimal quantity = new BigDecimal("5580000");
		Trade firstTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, true, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("110.609375")).withExpectedUnitPrice(new BigDecimal("110.609375")).withNotionalMultiplier(new BigDecimal("1.08067")).withAccrualAmount1(new BigDecimal("4956.84")).withSettlementDate(DateUtils.toDate("08/06/2013")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(firstTrade);

		tradeDate = DateUtils.toDate("01/08/2014");
		quantity = new BigDecimal("1060000");
		Trade secondTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, false, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("108.9375")).withExpectedUnitPrice(new BigDecimal("108.9375")).withNotionalMultiplier(new BigDecimal("1.08247")).withAccrualAmount1(new BigDecimal("7631.27")).withSettlementDate(DateUtils.toDate("01/09/2014")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(secondTrade);

		tradeDate = DateUtils.toDate("03/13/2014");
		quantity = new BigDecimal("590000");
		Trade thirdTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, true, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("109.66015625")).withExpectedUnitPrice(new BigDecimal("109.66015625")).withNotionalMultiplier(new BigDecimal("1.08242")).withAccrualAmount1(new BigDecimal("1406.92")).withSettlementDate(DateUtils.toDate("03/14/2014")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(thirdTrade);

		tradeDate = DateUtils.toDate("04/16/2014");
		quantity = new BigDecimal("3520000");
		Trade forthTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, true, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("108.953125")).withExpectedUnitPrice(new BigDecimal("108.953125")).withNotionalMultiplier(new BigDecimal("1.08689")).withAccrualAmount1(new BigDecimal("13369.35")).withSettlementDate(DateUtils.toDate("04/17/2014")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(forthTrade);
	}


	private InvestmentSecurityEvent generateSymbolChangeEvent() {
		InvestmentSecurityEvent event = new InvestmentSecurityEvent();
		event.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName("Symbol Change"));
		event.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName("Approved"));
		event.setSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("WM", false));
		event.setAdditionalSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false));
		event.setDeclareDate(DateUtils.toDate("01/01/2020"));
		event.setEventDate(DateUtils.toDate("03/16/2020"));
		event.setExDate(DateUtils.toDate("03/17/2020"));
		event.setRecordDate(DateUtils.toDate("03/16/2020"));
		event.setBeforeAndAfterEventValue(BigDecimal.ONE);
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		return event;
	}
}
