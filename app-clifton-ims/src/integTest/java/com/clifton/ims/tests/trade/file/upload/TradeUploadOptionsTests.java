package com.clifton.ims.tests.trade.file.upload;

import aj.org.objectweb.asm.Type;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.AccountsCreationCommand;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.util.RandomUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.file.upload.TradeUploadCommand;
import com.clifton.trade.file.upload.TradeUploadService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class TradeUploadOptionsTests extends BaseImsIntegrationTest {

	@Resource
	private TradeService tradeService;

	@Resource
	private TradeUploadService tradeUploadService;


	@Test
	public void testOptionUploadSimple_DoNotCreateNewOption() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 05/06/16 C2095");

		// Set Client Account Only
		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, security.getSymbol(), 6, 7.05, false, 2);
		uploadTrade(values);

		// Same Upload, but Set Holding Account Only
		values = populateTradeData(null, accountInfo.getHoldingAccount().getNumber(), security.getSymbol(), 6, 7.05, false, 2);
		uploadTrade(values);

		// Now the account should have 2 pending trades for client account/holding account/security
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), security.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(tradeList));

		// adding to test booking is successful
		this.tradeUtils.fullyExecuteTrade(tradeList.get(0));
	}


	@Test
	public void testOptionUploadSimple_SetTraderAndTradeDateInFile() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);

		Object[] trade1Values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, "SPXW US 05/06/16 C2095", 6, 7.05, false, 2);
		appendTraderUserAndDateInfo(trade1Values, "imstradertestuser", DateUtils.toDate("04/14/2016"), DateUtils.toDate("04/14/2016"));
		Object[] trade2Values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, "SPXW US 05/13/16 C2130", 14, 4.8, false, 0);
		appendTraderUserAndDateInfo(trade2Values, "imstradertestuser", DateUtils.toDate("04/15/2016"), DateUtils.toDate("04/18/2016"));
		Object[] trade3Values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, "SPXW US 05/13/16 P2015", 14, 10.7, false, 2);
		appendTraderUserAndDateInfo(trade3Values, "imstradertestuser", DateUtils.toDate("04/15/2016"), DateUtils.toDate("04/18/2016"));

		List<Object[]> valueList = new ArrayList<>();
		valueList.add(trade1Values);
		valueList.add(trade2Values);
		valueList.add(trade3Values);
		uploadTrades(valueList, true);

		// Now the account should have 3 pending trades for client account/holding account/security - one trade group
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), null);
		Assertions.assertEquals(3, CollectionUtils.getSize(tradeList));

		TradeGroup tradeGroup = tradeList.get(0).getTradeGroup();
		Assertions.assertEquals("imstradertestuser", tradeGroup.getTraderUser().getUserName());
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(tradeGroup.getTradeDate(), DateUtils.toDate("04/14/2016")));

		for (Trade trade : tradeList) {
			Assertions.assertEquals("imstradertestuser", trade.getTraderUser().getUserName());
			if (StringUtils.isEqual("SPXW US 05/06/16 C2095", trade.getInvestmentSecurity().getSymbol())) {
				Assertions.assertTrue(DateUtils.isEqualWithoutTime(trade.getTradeDate(), DateUtils.toDate("04/14/2016")));
				Assertions.assertTrue(DateUtils.isEqualWithoutTime(trade.getSettlementDate(), DateUtils.toDate("04/14/2016")));
			}
			else {
				Assertions.assertTrue(DateUtils.isEqualWithoutTime(trade.getTradeDate(), DateUtils.toDate("04/15/2016")));
				Assertions.assertTrue(DateUtils.isEqualWithoutTime(trade.getSettlementDate(), DateUtils.toDate("04/18/2016")));
			}
		}
	}


	@Test
	public void testOptionUploadSimple_UseExecutingBrokerDTCNumber() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);

		Object[] trade1Values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, "SPXW US 05/06/16 C2095", 6, 7.05, false, 2);
		// Change to Use JP Morgan Chase Company & DTC Number; clear name for testing DTC lookup
		trade1Values[3] = null;
		trade1Values[36] = "902";
		trade1Values[3] = BusinessCompanies.JP_MORGAN_CHASE.getCompanyName();// add company name since there is a duplicate for DTC number in IMS
		// Leave Using Default
		Object[] trade2Values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, "SPXW US 05/13/16 C2130", 14, 4.8, false, 0);
		// Set company name and GS DTC Number to resolve duplicate DTC match
		Object[] trade3Values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, "SPXW US 05/13/16 P2015", 14, 10.7, false, 2);
		trade3Values[3] = BusinessCompanies.GOLDMAN_SACHS_AND_CO.getCompanyName();
		trade3Values[36] = "0005";

		List<Object[]> valueList = new ArrayList<>();
		valueList.add(trade1Values);
		valueList.add(trade2Values);
		valueList.add(trade3Values);
		uploadTrades(valueList, false);

		// Now the account should have 3 pending trades for client account/holding account/security - one trade group
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), null);
		Assertions.assertEquals(3, CollectionUtils.getSize(tradeList));

		for (Trade trade : tradeList) {
			if (StringUtils.isEqual("SPXW US 05/06/16 C2095", trade.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals(BusinessCompanies.JP_MORGAN_CHASE.getCompanyName(), trade.getExecutingBrokerCompany().getName());
			}
			else {
				Assertions.assertEquals(BusinessCompanies.GOLDMAN_SACHS_AND_CO.getCompanyName(), trade.getExecutingBrokerCompany().getName());
			}
		}
	}


	@Test
	public void testOptionUploadSimple_CreateNewOption() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);
		// NOTE THIS WOULD NOT BE A REAL OPTION SECURITY SYMBOL, BUT ENFORCES THAT RE-RUNNING TESTS WOULD ALWAYS CREATE NEW OPTION SECURITY
		// REAL SYMBOL WOULD BE SPXW US 05/06/16 C3010
		String symbol = "SPXW " + RandomUtils.randomName();

		// Set Client Account Only
		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, symbol, 6, 7.05, false, 2);
		appendCreateSecurityData(values, "SPXW US 05/06/16 C2095", "SPXW US 05/06/16 C3010", "SPXW  160506C03010000", 3010.0, null);
		uploadTrade(values);


		// Make sure Security was created
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		Assertions.assertNotNull(security, "Expected Option Security [" + symbol + "] to be created by Trade Upload");

		// Same Upload, but Set Holding Account Only - this time we don't need to create the security
		values = populateTradeData(null, accountInfo.getHoldingAccount().getNumber(), symbol, 6, 7.05, false, 2);
		uploadTrade(values);

		// Now the account should have 2 pending trades for client account/holding account/security
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), security.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(tradeList));
	}


	@Test
	public void testOptionUploadSimple_CreateNewOption_Listed_NoIdentifierPrefix() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);
		// NOTE THIS WOULD NOT BE A REAL OPTION SECURITY SYMBOL, BUT ENFORCES THAT RE-RUNNING TESTS WOULD ALWAYS CREATE NEW OPTION SECURITY
		// REAL SYMBOL WOULD BE S1K6C Y 3010
		String symbol = "SPXW " + RandomUtils.randomName();

		// Set Client Account Only
		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, symbol, 6, 7.05, false, 2);
		appendCreateSecurityData(values, "SPXW US 05/06/16 C2095", "SPXW US 05/06/16 C3020", "SPXW  160506C03020000", 3020.0, "Options Listed");
		uploadTrade(values);


		// Make sure Security was created
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		Assertions.assertNotNull(security, "Expected Option Security [" + symbol + "] to be created by Trade Upload");
		Assertions.assertEquals("SO-SPXW", security.getInstrument().getIdentifierPrefix());

		// Same Upload, but Set Holding Account Only - this time we don't need to create the security
		values = populateTradeData(null, accountInfo.getHoldingAccount().getNumber(), symbol, 6, 7.05, false, 2);
		uploadTrade(values);

		// Now the account should have 2 pending trades for client account/holding account/security
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), security.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(tradeList));
	}


	@Test
	public void testOptionUploadSimple_CreateNewOption_OTC() {
		InvestmentSecurity templateSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("EEM OTC US 05/20/16 P33 - BARC");
		AccountsCreationCommand accountsCreationCommand = AccountsCreationCommand.ofOtcSecurityAndCustodian(templateSecurity, BusinessCompanies.NORTHERN_TRUST, this.options);
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountsCreationCommand);

		// NOTE THIS WOULD NOT BE A REAL OPTION SECURITY SYMBOL, BUT ENFORCES THAT RE-RUNNING TESTS WOULD ALWAYS CREATE NEW OPTION SECURITY
		String symbol = "EEM OTC " + RandomUtils.randomName();

		// Set Client Account Only
		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), accountInfo.getHoldingAccount().getNumber(), symbol, 6, 7.05, false, 2);
		appendCreateSecurityData(values, templateSecurity.getSymbol(), symbol, null, 33.0, null);
		uploadTrade(values);

		// Make sure Security was created
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		Assertions.assertNotNull(security, "Expected Option Security [" + symbol + "] to be created by Trade Upload");
		// Make sure OTC properties set on security
		Assertions.assertEquals(accountInfo.getHoldingAccount().getBusinessContract(), security.getBusinessContract());
		Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), security.getBusinessCompany().getName());
		Assertions.assertTrue(security.isIlliquid());

		// Now the account should have 1 pending trade for client account/holding account/security
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), security.getId());
		Assertions.assertEquals(1, CollectionUtils.getSize(tradeList));
	}


	@Test
	public void testOptionTradeRollUpload() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);
		testOptionTradeRollUpload(accountInfo, null);
		testOptionTradeRollUpload(accountInfo, Boolean.TRUE);
		testOptionTradeRollUpload(accountInfo, Boolean.FALSE);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void uploadTrade(Object[] values) {
		uploadTrade(values, false);
	}


	private void uploadTrade(Object[] values, boolean doNotSetTraderInfoOnGroup) {
		uploadTrades(Collections.singletonList(values), doNotSetTraderInfoOnGroup);
	}


	private void uploadTrades(List<Object[]> valueList, boolean doNotSetTraderInfoOnGroup) {
		uploadTrades(DATA_COLUMN_LIST, valueList, doNotSetTraderInfoOnGroup);
	}


	private void uploadTrades(DataColumn[] columns, List<Object[]> valueList, boolean doNotSetTraderInfoOnGroup) {
		// Create DataTable for Upload File
		DataTable dataTable = new PagingDataTableImpl(columns);
		for (Object[] values : CollectionUtils.getIterable(valueList)) {
			DataRow row = new DataRowImpl(dataTable, values);
			dataTable.addRow(row);
		}

		TradeUploadCommand upload = new TradeUploadCommand();
		upload.setSimpleTradeType(this.tradeUtils.getTradeType("Options"));
		TradeGroup group = new TradeGroup();
		if (!doNotSetTraderInfoOnGroup) {
			group.setTraderUser(this.userUtils.getCurrentUser());
			group.setTradeDate(DateUtils.toDate("04/15/2016"));
		}
		upload.setTradeGroup(group);
		upload.setSimple(true);
		upload.setCreateMissingSecurities(true);
		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		DataTableFileConfig fileConfig = new DataTableFileConfig(dataTable);
		upload.setFile(new MultipartFileImpl(converter.convert(fileConfig)));
		this.tradeUploadService.uploadTradeUploadFile(upload);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Object[] populateTradeData(String clientAccountNumber, String holdingAccountNumber, String securitySymbol, int quantity, double averageUnitPrice, boolean buy, double commissionPerUnit) {
		Object[] values = new Object[DATA_COLUMN_LIST.length];
		values[0] = clientAccountNumber;
		values[1] = holdingAccountNumber;
		values[2] = securitySymbol;
		values[3] = BusinessCompanies.GOLDMAN_SACHS_AND_CO.getCompanyName();
		values[4] = TradeDestination.MANUAL;
		values[5] = quantity;
		values[6] = averageUnitPrice;
		values[7] = (buy ? "B" : "S");
		values[8] = commissionPerUnit;
		return values;
	}


	private void appendCreateSecurityData(Object[] values, String templateSecuritySymbol, String securityName, String occSymbol, double strikePrice, String assetType) {
		InvestmentSecurity templateSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(templateSecuritySymbol);
		ValidationUtils.assertNotNull(templateSecurity, "Missing Security with Symbol [" + templateSecuritySymbol + "] in the database");

		if (templateSecurity.getBusinessCompany() != null) {
			values[3] = templateSecurity.getBusinessCompany().getName();
		}

		if (!StringUtils.isEmpty(assetType)) {
			values[21] = assetType;
		}
		else {
			values[9] = templateSecurity.getInstrument().getIdentifierPrefix();
		}
		values[10] = securityName;
		values[11] = securityName + "(Copied from " + templateSecuritySymbol + ")";
		values[12] = occSymbol;
		values[13] = templateSecurity.getStartDate();
		values[14] = templateSecurity.getEndDate();
		values[15] = templateSecurity.getLastDeliveryDate();
		values[16] = templateSecurity.getOptionType();
		values[17] = strikePrice;
		values[18] = templateSecurity.getOptionStyle();
		values[19] = templateSecurity.getSettlementExpiryTime();
		if (templateSecurity.getInstrument().getHierarchy().isOtc()) {
			values[32] = true; // Set Illiquid
		}
	}


	private void appendTraderUserAndDateInfo(Object[] values, String traderUserName, Date tradeDate, Date settlementDate) {
		values[33] = traderUserName;
		values[34] = tradeDate;
		values[35] = settlementDate;
	}


	private void testOptionTradeRollUpload(AccountInfo accountInfo, Boolean roll) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 05/06/16 C2095");
		DataColumn[] columns = ArrayUtils.add(DATA_COLUMN_LIST, new DataColumnImpl("Roll", Type.BOOLEAN));

		// Set Client Account Only
		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), null, security.getSymbol(), 6, 7.05, false, 2);
		uploadTrades(columns, Collections.singletonList(ArrayUtils.addAll(values, new Object[]{roll})), false);

		// Same Upload, but Set Holding Account Only
		values = populateTradeData(null, accountInfo.getHoldingAccount().getNumber(), security.getSymbol(), 6, 7.05, false, 2);
		uploadTrades(columns, Collections.singletonList(ArrayUtils.addAll(values, new Object[]{roll})), false);

		// Now the account should have 2 pending trades for client account/holding account/security
		List<Trade> tradeList = this.tradeService.getTradePendingList(accountInfo.getClientAccount().getId(), accountInfo.getHoldingAccount().getId(), security.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(tradeList));

		tradeList.forEach(trade -> {
			Assertions.assertEquals(BooleanUtils.isTrue(roll), trade.isRoll());
			Assertions.assertEquals(BooleanUtils.isTrue(roll) ? TradeGroupType.GroupTypes.ROLL_TRADE_IMPORT.getTypeName() : TradeGroupType.GroupTypes.TRADE_IMPORT.getTypeName(), trade.getTradeGroup().getTradeGroupType().getName());
			this.tradeUtils.fullyExecuteTrade(trade);
		});
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private static final DataColumn[] DATA_COLUMN_LIST = new DataColumn[37];


	static {
		// Basic Trade Info
		DATA_COLUMN_LIST[0] = new DataColumnImpl("ClientInvestmentAccount-Number", Types.NVARCHAR);
		DATA_COLUMN_LIST[1] = new DataColumnImpl("HoldingInvestmentAccount-Number", Types.NVARCHAR);
		DATA_COLUMN_LIST[2] = new DataColumnImpl("InvestmentSecurity-Symbol", Types.NVARCHAR);
		DATA_COLUMN_LIST[3] = new DataColumnImpl("ExecutingBrokerCompany-Name", Types.NVARCHAR);
		DATA_COLUMN_LIST[4] = new DataColumnImpl("TradeDestination-Name", Types.NVARCHAR);
		DATA_COLUMN_LIST[5] = new DataColumnImpl("QuantityIntended", Types.DECIMAL);
		DATA_COLUMN_LIST[6] = new DataColumnImpl("AverageUnitPrice", Types.DECIMAL);
		DATA_COLUMN_LIST[7] = new DataColumnImpl("Buy", Types.NVARCHAR);
		DATA_COLUMN_LIST[8] = new DataColumnImpl("CommissionPerUnit", Types.DECIMAL);

		// Security Setup
		DATA_COLUMN_LIST[9] = new DataColumnImpl("InvestmentSecurity-Instrument-IdentifierPrefix", Types.NVARCHAR);
		DATA_COLUMN_LIST[10] = new DataColumnImpl("InvestmentSecurity-Name", Types.NVARCHAR);
		DATA_COLUMN_LIST[11] = new DataColumnImpl("InvestmentSecurity-Description", Types.NVARCHAR);
		DATA_COLUMN_LIST[12] = new DataColumnImpl("InvestmentSecurity-OccSymbol", Types.NVARCHAR);
		DATA_COLUMN_LIST[13] = new DataColumnImpl("InvestmentSecurity-StartDate", Types.DATE);
		DATA_COLUMN_LIST[14] = new DataColumnImpl("InvestmentSecurity-EndDate", Types.DATE);
		DATA_COLUMN_LIST[15] = new DataColumnImpl("InvestmentSecurity-LastDeliveryDate", Types.DATE);

		// Option Columns
		DATA_COLUMN_LIST[16] = new DataColumnImpl("InvestmentSecurity-OptionType", Types.NVARCHAR);
		DATA_COLUMN_LIST[17] = new DataColumnImpl("InvestmentSecurity-OptionStrikePrice", Types.DECIMAL);
		DATA_COLUMN_LIST[18] = new DataColumnImpl("InvestmentSecurity-OptionStyle", Types.NVARCHAR);
		DATA_COLUMN_LIST[19] = new DataColumnImpl("InvestmentSecurity-SettlementExpiryTime", Types.NVARCHAR);

		// Upload Specific Columns
		DATA_COLUMN_LIST[20] = new DataColumnImpl("HierarchyExpanded", Types.NVARCHAR);
		DATA_COLUMN_LIST[21] = new DataColumnImpl("AssetType", Types.NVARCHAR);

		// Additional Trade Info
		DATA_COLUMN_LIST[22] = new DataColumnImpl("Description", Types.NVARCHAR);
		DATA_COLUMN_LIST[23] = new DataColumnImpl("OriginalFace", Types.DECIMAL);
		DATA_COLUMN_LIST[24] = new DataColumnImpl("CurrentFactor", Types.DECIMAL);
		DATA_COLUMN_LIST[25] = new DataColumnImpl("NotionalMultiplier", Types.DECIMAL);
		DATA_COLUMN_LIST[26] = new DataColumnImpl("ExchangeRate", Types.DECIMAL);
		DATA_COLUMN_LIST[27] = new DataColumnImpl("ExpectedUnitPrice", Types.DECIMAL);
		DATA_COLUMN_LIST[28] = new DataColumnImpl("FeeAmount", Types.DECIMAL);
		DATA_COLUMN_LIST[29] = new DataColumnImpl("CommissionAmount", Types.DECIMAL);
		DATA_COLUMN_LIST[30] = new DataColumnImpl("CurrentFactor", Types.DECIMAL);
		DATA_COLUMN_LIST[31] = new DataColumnImpl("AccountingNotional", Types.DECIMAL);

		// OTC Security Setup
		// Note: Contract and Company Fields Are Populated automatically by the Holding Account
		DATA_COLUMN_LIST[32] = new DataColumnImpl("InvestmentSecurity-Illiquid", Types.BOOLEAN);

		DATA_COLUMN_LIST[33] = new DataColumnImpl("TraderUser-UserName", Types.NVARCHAR);
		DATA_COLUMN_LIST[34] = new DataColumnImpl("TradeDate", Types.DATE);
		DATA_COLUMN_LIST[35] = new DataColumnImpl("SettlementDate", Types.DATE);

		DATA_COLUMN_LIST[36] = new DataColumnImpl("ExecutingBrokerCompany-DTC Number", Types.NVARCHAR);
	}
}
