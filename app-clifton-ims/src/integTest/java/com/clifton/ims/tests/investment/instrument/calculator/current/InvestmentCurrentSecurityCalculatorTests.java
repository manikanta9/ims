package com.clifton.ims.tests.investment.instrument.calculator.current;

import com.clifton.calendar.holiday.CalendarHoliday;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.CalendarHolidayUtils;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.search.CalendarScheduleSearchForm;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.investment.replication.executor.InvestmentReplicationCalculatorTestExecutor;
import com.clifton.ims.tests.investment.replication.executor.InvestmentReplicationCalculatorTestServiceHolder;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * Integration tests for verifying the logic in {@link com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator}s.
 *
 * @author michaelm
 */
public class InvestmentCurrentSecurityCalculatorTests extends BaseImsIntegrationTest {

	@Resource
	private CalendarScheduleService calendarScheduleService;
	@Resource
	private CalendarSetupService calendarSetupService;
	@Resource
	private CalendarHolidayService calendarHolidayService;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private InvestmentReplicationService investmentReplicationService;
	@Resource
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	@Resource
	private InvestmentReplicationHistoryService investmentReplicationHistoryService;


	private static final String GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME = "GBP/USD Currency Forward";

	private static final String SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME = "Schedule Based Current Security Calculator";
	private static final String SCHEDULE_MONTHLY_LAST_BUSINESS_DAY_NAME = "Monthly on the Last Business Day at 3:00 PM";
	private static final String SCHEDULE_MONTHLY_2ND_TO_LAST_BUSINESS_DAY_NAME = "Monthly on the 2nd Last Business Day at 3:00 PM";
	private static final String SCHEDULE_MONTHLY_EVERY_10TH_BUSINESS_DAY_NAME = "Every Month on the 10th Business Day at 12:00 AM";
	private static final String SCHEDULE_MONTHLY_EVERY_THIRD_WEDNESDAY_NAME = "Every Third Wednesday in March, June, September and December at 3:00 PM";


	////////////////////////////////////////////////////////////////////////////
	////////      Schedule Based Current Security Calculator Tests      ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testScheduleBasedCalculator_EveryMonthOnLastDay() {
		CalendarSchedule currentSecuritySchedule = createCalendarSchedule(SCHEDULE_MONTHLY_LAST_BUSINESS_DAY_NAME);
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("Monthly Last Day"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST Monthly Last Day"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("12/25/2019", "03/11/2020")
				.withExpectedResults(
						"02/28/2020     03/11/2020     GBP/USD Curr Fwd 03/31/2020     100.00000",
						"01/31/2020     02/27/2020     GBP/USD Curr Fwd 02/28/2020     100.00000",
						"12/31/2019     01/30/2020     GBP/USD Curr Fwd 01/31/2020     100.00000",
						"12/25/2019     12/30/2019     GBP/USD Curr Fwd 12/31/2019     100.00000"
				).execute();
	}


	@Test
	public void testScheduleBasedCalculator_EveryMonthOnLastDay_Negative5WeekdayEffectiveOnAdjustment() {
		CalendarSchedule currentSecuritySchedule = createCalendarSchedule(SCHEDULE_MONTHLY_LAST_BUSINESS_DAY_NAME);
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly Last -5 Weekday"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
				.addProperty("Effective Business Day Adjustment", Integer.valueOf(-5).toString());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST Monthly Last Day -5 WDs"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("12/25/2019", "03/11/2020")
				.withExpectedResults(
						"02/21/2020     03/11/2020     GBP/USD Curr Fwd 03/31/2020     100.00000",
						"01/24/2020     02/20/2020     GBP/USD Curr Fwd 02/28/2020     100.00000",
						"12/25/2019     01/23/2020     GBP/USD Curr Fwd 01/31/2020     100.00000"
				).execute();
	}


	@Test
	public void testScheduleBasedCalculator_EveryMonthOnLastDay_Negative5WeekdayEffectiveOnAdjustment_HolidayOnJan24() {
		CalendarSchedule currentSecuritySchedule = createCalendarSchedule(SCHEDULE_MONTHLY_LAST_BUSINESS_DAY_NAME);
		CalendarHoliday calendarHoliday = CalendarHolidayUtils.createCalendarHolidayWithDayForSchedule(this.calendarHolidayService, currentSecuritySchedule.getCalendar(), RandomUtils.randomNameWithPrefix("TEST Holiday 01/24/2020"), "01/24/2020");
		try {
			SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly Last Day Holiday on 01/24/2020"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
					.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
					.addProperty("Effective Business Day Adjustment", Integer.valueOf(-5).toString())
					.addProperty("Business Day Calendar", currentSecuritySchedule.getCalendar().getId().toString(), currentSecuritySchedule.getCalendar().getName());

			InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST Last Day Holiday 1/24"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
					.withBalanceStartAndEndDates("12/25/2019", "03/11/2020")
					.withExpectedResults(
							"02/21/2020     03/11/2020     GBP/USD Curr Fwd 03/31/2020     100.00000",
							"01/23/2020     02/20/2020     GBP/USD Curr Fwd 02/28/2020     100.00000",
							"12/25/2019     01/22/2020     GBP/USD Curr Fwd 01/31/2020     100.00000"
					).execute();
		}
		finally {
			CalendarHolidayUtils.removeCalendarHolidayAndDays(this.calendarHolidayService, calendarHoliday.getId());
		}
	}


	@Test
	public void testScheduleBasedCalculator_EveryMonthOnLastDay_Negative5WeekdayEffectiveOnAdjustment_HolidayOnFeb20() {
		CalendarSchedule currentSecuritySchedule = createCalendarSchedule(SCHEDULE_MONTHLY_LAST_BUSINESS_DAY_NAME);
		CalendarHoliday calendarHoliday = CalendarHolidayUtils.createCalendarHolidayWithDayForSchedule(this.calendarHolidayService, currentSecuritySchedule.getCalendar(), RandomUtils.randomNameWithPrefix("TEST Holiday 02/20/2020"), "02/20/2020");
		try {
			SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly Last Day Holiday on 2/20"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
					.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
					.addProperty("Effective Business Day Adjustment", Integer.valueOf(-5).toString())
					.addProperty("Business Day Calendar", currentSecuritySchedule.getCalendar().getId().toString(), currentSecuritySchedule.getCalendar().getName());

			InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST Monthly Holiday 2/20"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
					.withBalanceStartAndEndDates("12/25/2019", "03/11/2020")
					.withExpectedResults(
							"02/21/2020     03/11/2020     GBP/USD Curr Fwd 03/31/2020     100.00000",
							"01/24/2020     02/20/2020     GBP/USD Curr Fwd 02/28/2020     100.00000",
							"12/25/2019     01/23/2020     GBP/USD Curr Fwd 01/31/2020     100.00000"
					).execute();
		}
		finally {
			CalendarHolidayUtils.removeCalendarHolidayAndDays(this.calendarHolidayService, calendarHoliday.getId());
		}
	}


	@Test
	public void testScheduleBasedCalculator_EveryMonthOnLastDay_EffectiveOnScheduleFor2ndToLastBusinessDayOfMonthAt3pm() {
		CalendarSchedule currentSecuritySchedule = createCalendarSchedule(SCHEDULE_MONTHLY_LAST_BUSINESS_DAY_NAME);
		CalendarSchedule effectiveOnSchedule = this.calendarScheduleService.copyCalendarSchedule(currentSecuritySchedule.getId(), RandomUtils.randomNameWithPrefix("TEST " + SCHEDULE_MONTHLY_2ND_TO_LAST_BUSINESS_DAY_NAME));
		effectiveOnSchedule.setAdditionalBusinessDays(-1); // -1 to get 2nd to last day since the currentSecuritySchedule gets the last day
		effectiveOnSchedule = this.calendarScheduleService.saveCalendarSchedule(effectiveOnSchedule);
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly 2nd Last Day Effective On Schedule"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
				.addProperty("Effective On Schedule", effectiveOnSchedule.getId().toString(), effectiveOnSchedule.getName());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST Monthly 2nd Last Day"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("12/25/2019", "03/11/2020")
				.withExpectedResults(
						"02/27/2020     03/11/2020     GBP/USD Curr Fwd 03/31/2020     100.00000",
						"01/30/2020     02/26/2020     GBP/USD Curr Fwd 02/28/2020     100.00000",
						"12/30/2019     01/29/2020     GBP/USD Curr Fwd 01/31/2020     100.00000",
						"12/25/2019     12/27/2019     GBP/USD Curr Fwd 12/31/2019     100.00000"
				).execute();
	}


	@Test
	public void testScheduleBasedCalculator_EveryThirdWednesdayInMarchJuneSeptemberDecember_OccurrenceOverride2() {
		CalendarScheduleSearchForm scheduleSearchForm = new CalendarScheduleSearchForm();
		scheduleSearchForm.setName(SCHEDULE_MONTHLY_EVERY_THIRD_WEDNESDAY_NAME);
		CalendarSchedule currentSecuritySchedule = CollectionUtils.getFirstElementStrict(this.calendarScheduleService.getCalendarScheduleList(scheduleSearchForm));
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly 3rd Wed Override 2"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
				.addProperty("Date Result is Security Last Delivery Date", "true")
				.addProperty("Occurrence Override", Integer.valueOf(2).toString());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST 3rd Wed Override 2"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("12/02/2019", "03/17/2020")
				.withExpectedResults(
						"12/18/2019     03/17/2020     GBP/USD Curr Fwd 06/17/2020     100.00000",
						"12/02/2019     12/17/2019     GBP/USD Curr Fwd 03/18/2020     100.00000"
				).execute();
	}


	@Test
	public void testScheduleBasedCalculator_EveryThirdWednesdayInMarchJuneSeptemberDecember_EffectiveOnSchedule() {
		CalendarSchedule currentSecuritySchedule = getCalendarScheduleByName(SCHEDULE_MONTHLY_EVERY_THIRD_WEDNESDAY_NAME);
		CalendarSchedule effectiveOnSchedule = getCalendarScheduleByName(SCHEDULE_MONTHLY_EVERY_10TH_BUSINESS_DAY_NAME);
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly 3rd Wed Effect. On Sched."), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
				.addProperty("Date Result is Security Last Delivery Date", "true")
				.addProperty("Effective On Schedule", effectiveOnSchedule.getId().toString(), effectiveOnSchedule.getName());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST Monthly 3rd Wed"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("04/01/2019", "03/15/2020")
				.withExpectedResults(
						"03/12/2020     03/13/2020     GBP/USD Curr Fwd 06/17/2020     100.00000",
						"12/12/2019     03/11/2020     GBP/USD Curr Fwd 03/18/2020     100.00000",
						"09/13/2019     12/11/2019     GBP/USD Curr Fwd 12/18/2019     100.00000",
						"06/19/2019     09/12/2019     GBP/USD Curr Fwd 09/18/2019     100.00000",
						"04/01/2019     06/18/2019     GBP/USD Curr Fwd 06/19/2019     100.00000"
				).execute();
	}


	@Test
	public void testScheduleBasedCalculator_EveryThirdWednesdayInMarchJuneSeptemberDecember_OccurrenceOverride2_EffectiveOnSchedule() {
		CalendarSchedule currentSecuritySchedule = getCalendarScheduleByName(SCHEDULE_MONTHLY_EVERY_THIRD_WEDNESDAY_NAME);
		CalendarSchedule effectiveOnSchedule = getCalendarScheduleByName(SCHEDULE_MONTHLY_EVERY_10TH_BUSINESS_DAY_NAME);
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly 3rd Wed Effect. On Sched."), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
				.addProperty("Occurrence Override", Integer.valueOf(2).toString())
				.addProperty("Date Result is Security Last Delivery Date", "true")
				.addProperty("Effective On Schedule", effectiveOnSchedule.getId().toString(), effectiveOnSchedule.getName())
				.addProperty("Effective Business Day Adjustment", Integer.valueOf(-5).toString());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST 3rd Wed Eff. Sched."), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("08/01/2019", "03/04/2020")
				.withExpectedResults(
						"12/05/2019     03/04/2020     GBP/USD Curr Fwd 06/17/2020     100.00000",
						"09/06/2019     12/04/2019     GBP/USD Curr Fwd 03/18/2020     100.00000",
						"08/01/2019     09/05/2019     GBP/USD Curr Fwd 12/18/2019     100.00000"
				).execute();
	}


	@Test
	public void testScheduleBasedCalculator_EveryThirdWednesdayInMarchJuneSeptemberDecember_OccurrenceOverride3() {
		CalendarSchedule currentSecuritySchedule = getCalendarScheduleByName(SCHEDULE_MONTHLY_EVERY_THIRD_WEDNESDAY_NAME);
		SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("TEST Monthly 3rd Wed Override 2"), SCHEDULE_BASED_CALCULATOR_BEAN_TYPE_NAME, this.systemBeanService)
				.addProperty("Current Security Schedule", currentSecuritySchedule.getId().toString(), currentSecuritySchedule.getName())
				.addProperty("Date Result is Security Last Delivery Date", "true")
				.addProperty("Occurrence Override", Integer.valueOf(3).toString());

		InvestmentReplicationCalculatorTestExecutor.newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(getServiceHolder(), RandomUtils.randomNameWithPrefix("TEST 3rd Wed Override 3"), GBP_USD_CURRENCY_FORWARD_INSTRUMENT_NAME, beanBuilder)
				.withBalanceStartAndEndDates("06/02/2019", "12/17/2019")
				.withExpectedResults(
						"09/18/2019     12/17/2019     GBP/USD Curr Fwd 06/17/2020     100.00000",
						"06/19/2019     09/17/2019     GBP/USD Curr Fwd 03/18/2020     100.00000",
						"06/03/2019     06/18/2019     GBP/USD Curr Fwd 12/18/2019     100.00000"
				).execute();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CalendarSchedule createCalendarSchedule(String scheduleName) {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName(RandomUtils.randomNameWithPrefix("TEST " + scheduleName));
		schedule.setDescription(scheduleName);
		schedule.setFrequency(ScheduleFrequencies.MONTHLY);
		schedule.setStartDate(DateUtils.toDate("01/01/2019"));
		schedule.setStartTime(new Time(DateUtils.toDate("2019-01-01 15:00:00", DateUtils.DATE_FORMAT_FULL)));
		schedule.setCalendar(this.calendarSetupService.getDefaultCalendar());
		schedule.setBusinessDayConvention(BusinessDayConventions.PREVIOUS);
		schedule.setDayOfMonth(1);
		schedule.setRecurrence(1);
		schedule.setCountFromLastDayOfMonth(true);
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		return this.calendarScheduleService.saveCalendarSchedule(schedule);
	}


	private CalendarSchedule getCalendarScheduleByName(String scheduleName) {
		CalendarScheduleSearchForm scheduleSearchForm = new CalendarScheduleSearchForm();
		scheduleSearchForm.setName(scheduleName);
		return CollectionUtils.getFirstElement(this.calendarScheduleService.getCalendarScheduleList(scheduleSearchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentReplicationCalculatorTestServiceHolder getServiceHolder() {
		InvestmentReplicationCalculatorTestServiceHolder serviceHolder = new InvestmentReplicationCalculatorTestServiceHolder();
		serviceHolder.setCalendarSetupService(this.calendarSetupService);
		serviceHolder.setInvestmentInstrumentService(this.investmentInstrumentService);
		serviceHolder.setInvestmentReplicationHistoryRebuildService(this.investmentReplicationHistoryRebuildService);
		serviceHolder.setInvestmentInstrumentService(this.investmentInstrumentService);
		serviceHolder.setInvestmentReplicationService(this.investmentReplicationService);
		serviceHolder.setSystemBeanService(this.systemBeanService);
		serviceHolder.setInvestmentReplicationHistoryService(this.investmentReplicationHistoryService);
		return serviceHolder;
	}
}
