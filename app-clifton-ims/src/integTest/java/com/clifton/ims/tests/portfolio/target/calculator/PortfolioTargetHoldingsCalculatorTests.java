package com.clifton.ims.tests.portfolio.target.calculator;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.balance.PortfolioTargetBalance;
import com.clifton.portfolio.target.balance.PortfolioTargetBalanceService;
import com.clifton.portfolio.target.calculator.PortfolioTargetCalculator;
import com.clifton.portfolio.target.calculator.PortfolioTargetHoldingsCalculator;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.bean.SystemBeanUtils;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * {@link PortfolioTargetHoldingsCalculatorTests} validates functionality in the {@link PortfolioTargetHoldingsCalculator}.
 *
 * @author michaelm
 */
public class PortfolioTargetHoldingsCalculatorTests extends BaseImsIntegrationTest {

	// Entity Specific Info
	private static final String DE_FUND_CLIENT_ACCOUNT_NUMBER_STRING = "800020";
	private static final Date TARGET_START_DATE = DateUtils.toDate("12/23/2021");
	// System Bean Names
	private static final String BEAN_NAME_ACCOUNTING_POSITION_MARKET_VALUE = "Accounting Position Market Value Calculator";
	private static final String BEAN_TYPE_NAME_TARGET_HOLDINGS_CALCULATOR = "Holdings Target Calculator";

	@Resource
	private PortfolioTargetBalanceService portfolioTargetBalanceService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentGroupService investmentGroupService;

	////////////////////////////////////////////////////////////////////////////
	////////         Holdings Target Calculator Positions Tests         ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testHoldingsTargetCalculator_Validation() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createEmptyHoldingsTargetCalculator();
		TestUtils.expectException(ValidationException.class, holdingsCalculator::validate
				, "Holdings Target Calculators must have either a Value Calculator or Cash Options.");
	}


	@Test
	public void testHoldingsTargetCalculator_Positions_TotalPortfolioValue() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createHoldingsTargetCalculatorForPositions();
		validateDeFundHoldings("Total Portfolio Value", "6,447,103,330.88", holdingsCalculator);
	}


	@Test
	public void testHoldingsTargetCalculator_Positions_InvestmentSecurity_IVV_VOO() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createHoldingsTargetCalculatorForPositions();
		holdingsCalculator.setInvestmentSecurityIds(new Integer[]{this.investmentInstrumentService.getInvestmentSecurityBySymbol("IVV", false).getId(), this.investmentInstrumentService.getInvestmentSecurityBySymbol("VOO", false).getId()});
		validateDeFundHoldings("Investment Securities IVV and VOO", "3,329,001,262.08", holdingsCalculator);
	}


	@Test
	public void testHoldingsTargetCalculator_Positions_InvestmentSecurityGroup_CallOptions() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createHoldingsTargetCalculatorForPositions();
		holdingsCalculator.setInvestmentSecurityGroupIds(new Short[]{this.investmentSecurityGroupService.getInvestmentSecurityGroupByName("Call Options").getId()});
		validateDeFundHoldings("Call Options Security Group", "-16,046,300.00", holdingsCalculator);
	}


	@Test
	public void testHoldingsTargetCalculator_Positions_InvestmentGroup_OptionsMarketValue() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createHoldingsTargetCalculatorForPositions();
		holdingsCalculator.setInvestmentGroupIds(new Short[]{this.investmentGroupService.getInvestmentGroupByName("Options").getId()});
		validateDeFundHoldings("Options Market Value", "-20,367,700.00", holdingsCalculator);
	}


	@Test
	public void testHoldingsTargetCalculator_Positions_InvestmentGroups_Options_Bonds() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createHoldingsTargetCalculatorForPositions();
		holdingsCalculator.setInvestmentGroupIds(new Short[]{this.investmentGroupService.getInvestmentGroupByName("Options").getId(), this.investmentGroupService.getInvestmentGroupByName("Bonds").getId()});
		validateDeFundHoldings("Options and Bonds", "3,118,102,068.80", holdingsCalculator);
	}

	////////////////////////////////////////////////////////////////////////////
	////////        Holdings Target Calculator Cash Options Tests       ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testHoldingsTargetCalculator_CashOptions_Cash() {
		validateDeFundHoldingsForCashOptions("Cash", "37,940,131.50",
				PortfolioTargetHoldingsCalculator.CashOptions.CASH);
	}


	@Test
	public void testHoldingsTargetCalculator_CashOptions_ReceivableOnly() {
		validateDeFundHoldingsForCashOptions("Dividend Receivable", "11,795,086.06",
				PortfolioTargetHoldingsCalculator.CashOptions.RECEIVABLE_ONLY);
	}


	@Test
	public void testHoldingsTargetCalculator_CashOptions_Cash_ReceivableOnly() {
		validateDeFundHoldingsForCashOptions("Cash & Dividend Receivable", "49,735,217.56",
				PortfolioTargetHoldingsCalculator.CashOptions.CASH, PortfolioTargetHoldingsCalculator.CashOptions.RECEIVABLE_ONLY);
	}


	@Test
	public void testHoldingsTargetCalculator_CashOptions_Collateral() {
		// No collateral for DE Fund on 12/23/2021 TODO find a Collateral use case
		validateDeFundHoldingsForCashOptions("Collateral", "0",
				PortfolioTargetHoldingsCalculator.CashOptions.COLLATERAL);
	}

	////////////////////////////////////////////////////////////////////////////
	//////// Holdings Target Calculator Positions & Cash Options Tests  ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testHoldingsTargetCalculator_Positions_CashOptions() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createHoldingsTargetCalculatorForCashOptions(PortfolioTargetHoldingsCalculator.CashOptions.CASH);
		holdingsCalculator.setPositionValueCalculatorBean(this.systemBeanService.getSystemBeanByName(BEAN_NAME_ACCOUNTING_POSITION_MARKET_VALUE));
		holdingsCalculator.setInvestmentSecurityIds(new Integer[]{this.investmentInstrumentService.getInvestmentSecurityBySymbol("IVV", false).getId(), this.investmentInstrumentService.getInvestmentSecurityBySymbol("VOO", false).getId()});
		holdingsCalculator.setApplyInvestmentFiltersToCash(true);
		validateDeFundHoldings("IVV/VOO Positions & Cash with Security Filters Applied", "3,329,001,262.08", holdingsCalculator);
		// Avoiding the Investment Security related filters causes Cash to be pulled in (this is the default functionality).
		holdingsCalculator.setApplyInvestmentFiltersToCash(false);
		validateDeFundHoldings("IVV/VOO Positions & Cash", "3,366,941,393.58", holdingsCalculator);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void validateDeFundHoldingsForCashOptions(String portfolioTargetName, String expectedBalanceValue, PortfolioTargetHoldingsCalculator.CashOptions... cashOptions) {
		validateDeFundHoldings(portfolioTargetName, expectedBalanceValue, createHoldingsTargetCalculatorForCashOptions(cashOptions));
	}


	private void validateDeFundHoldings(String portfolioTargetName, String expectedBalanceValue, PortfolioTargetHoldingsCalculator holdingsCalculator) {
		PortfolioTargetBalance balance = createTargetAndCalculateBalance(DE_FUND_CLIENT_ACCOUNT_NUMBER_STRING, portfolioTargetName, holdingsCalculator);
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(StringUtils.removeAll(expectedBalanceValue, ",")), balance.getAdjustedValue()),
				String.format("Expected total holdings value [%s] but was [%s] for Target [%s]", expectedBalanceValue, balance.getAdjustedValue(), portfolioTargetName));
	}


	private PortfolioTargetHoldingsCalculator createHoldingsTargetCalculatorForPositions() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createEmptyHoldingsTargetCalculator();
		holdingsCalculator.setPositionValueCalculatorBean(this.systemBeanService.getSystemBeanByName(BEAN_NAME_ACCOUNTING_POSITION_MARKET_VALUE));
		return holdingsCalculator;
	}


	private PortfolioTargetHoldingsCalculator createHoldingsTargetCalculatorForCashOptions(PortfolioTargetHoldingsCalculator.CashOptions... cashOptions) {
		PortfolioTargetHoldingsCalculator holdingsCalculator = createEmptyHoldingsTargetCalculator();
		holdingsCalculator.setCashOptions(cashOptions);
		return holdingsCalculator;
	}


	private PortfolioTargetHoldingsCalculator createEmptyHoldingsTargetCalculator() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = new PortfolioTargetHoldingsCalculator();
		return holdingsCalculator;
	}


	private PortfolioTargetBalance createTargetAndCalculateBalance(String clientAccountNumberString, String targetName, PortfolioTargetCalculator targetCalculator) {
		SystemBeanType holdingsTargetCalculatorBeanType = this.systemBeanService.getSystemBeanTypeByName(BEAN_TYPE_NAME_TARGET_HOLDINGS_CALCULATOR);
		if (!holdingsTargetCalculatorBeanType.isDuplicateBeansAllowed()) {
			holdingsTargetCalculatorBeanType.setDuplicateBeansAllowed(true);
			this.systemBeanService.saveSystemBeanType(holdingsTargetCalculatorBeanType);
		}
		PortfolioTarget target = createPortfolioTarget(clientAccountNumberString, targetName);
		target.setTargetCalculatorBean(SystemBeanUtils.createSystemBeanUsingImplementation(this.systemBeanService, RandomUtils.randomNameWithPrefix(target.getName() + " Holdings Calculator"), targetCalculator));
		return this.portfolioTargetBalanceService.previewPortfolioTargetBalance(target, target.getStartDate());
	}


	private PortfolioTarget createPortfolioTarget(String clientAccountNumber, String targetName) {
		PortfolioTarget portfolioTarget = new PortfolioTarget();
		portfolioTarget.setClientAccount(this.investmentAccountService.getInvestmentAccountByNumber(clientAccountNumber));
		portfolioTarget.setName(targetName);
		portfolioTarget.setTargetPercent(BigDecimal.valueOf(100));
		portfolioTarget.setStartDate(TARGET_START_DATE);
		portfolioTarget.setEndDate(DateUtils.addDays(TARGET_START_DATE, 1));
		return portfolioTarget;
	}
}
