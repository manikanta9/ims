package com.clifton.ims.tests.investment;

import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.AccountingEventJournalType;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorServiceImpl;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.search.InvestmentSubTypeSearchForm;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * InvestmentUtilsTests
 * Created by Mary on 9/28/2015.
 */

public class InvestmentUtilsTests extends BaseImsIntegrationTest {

	@Resource
	private ApplicationContextService applicationContextService;

	@Resource
	private AccountingEventJournalService accountingEventJournalService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;


	////////////////////////////////////////////////////////////////
	///  Security Key Override Tests
	///  Used by TypeBasedRulesBookingProcessor and AccountingEventJournalGeneratorService maps
	///  in context that map security processing by locating their overrides in the map
	///  Which all use InvestmentUtils.getSecurityOverrideKey method, however this test will make sure mappings match data in the system
	///  which helps when set up data is changed so we catch invalid mappings prior to releases


	@Test
	public void testGetSecurityOverrideKey_InContextMaps() {
		List<String> keys = getKeysFromServiceBeanMapProperty("accountingEventJournalGeneratorService", "detailPopulatorMap");

		// AccountingEventJournalGenerator Maps first check event type, if it doesn't exist, then it uses security event type
		Map<String, AccountingEventJournalType> eventJournalTypeMap = BeanUtils.getBeanMap(this.accountingEventJournalService.getAccountingEventJournalTypeList(), AccountingEventJournalType::getName);
		Map<String, InvestmentSecurityEventType> securityEventTypeMap = BeanUtils.getBeanMap(this.investmentSecurityEventService.getInvestmentSecurityEventTypeList(null), InvestmentSecurityEventType::getName);

		// Detail Populator Map:
		for (String key : keys) {
			String eventTypeName = getKeyPrefixFromKey(key);
			if (!eventJournalTypeMap.containsKey(eventTypeName)) {
				if (!securityEventTypeMap.containsKey(eventTypeName)) {
					throw new ValidationException("AccountingEventJournalGeneratorService - detailPopulatorMap - has an entry for Event Type name [" + eventTypeName + "] which does not exist as an AccountingEventJournalType or InvestmentSecurityEventType. Key: " + key);
				}
			}
			validateSecurityOverridesForKey(key, "AccountingEventJournalGeneratorService - detailPopulatorMap");
		}

		keys = getKeysFromServiceBeanMapProperty("accountingEventJournalGeneratorService", "affectedPositionsRetrieverMap");
		// Affected Positions Retriever Map:
		for (String key : keys) {
			String eventTypeName = getKeyPrefixFromKey(key);
			if (!AccountingEventJournalGeneratorServiceImpl.ACCOUNTING_EVENT_JOURNAL_POSITION_RETRIEVER_DEFAULT_KEY.equals(eventTypeName)) {
				AccountingEventJournalType eventType = this.accountingEventJournalService.getAccountingEventJournalTypeByName(eventTypeName);
				// Note: Above Method should throw an exception if can't find by name - but extra validation
				AssertUtils.assertNotNull(eventType, "AccountingEventJournalGeneratorService - affectedPositionsRetrieverMap - has an entry for Accounting Event Journal Type name [" + eventType + "] which does not exist. Key: " + key);
				validateSecurityOverridesForKey(key, "AccountingEventJournalGeneratorService - affectedPositionsRetrieverMap");
			}
		}

		keys = getKeysFromServiceBeanMapProperty("tradeFillBookingProcessor", "bookingRulesMap");
		// Booking Rules Map
		for (String key : keys) {
			String tradeTypeName = getKeyPrefixFromKey(key);
			TradeType tradeType = this.tradeService.getTradeTypeByName(tradeTypeName);
			AssertUtils.assertNotNull(tradeType, "TradeFillBookingProcessor - bookingRulesMap - has an entry for Trade Type name [" + tradeTypeName + "] which does not exist. Key: " + key);
			validateSecurityOverridesForKey(key, "TradeFillBookingProcessor - bookingRulesMap");
		}
	}


	@SuppressWarnings("unchecked")
	private List<String> getKeysFromServiceBeanMapProperty(String contextBeanName, String mapBeanPropertyName) {
		String serviceBeanDetails = this.applicationContextService.getContextBeanDetails(contextBeanName);
		Map<String, Object> jsonResultMap = new JsonStringToMapConverter().convert(serviceBeanDetails);
		Map<String, Object> objectMap = (Map<String, Object>) jsonResultMap.get("data");
		Map<String, Object> beanPropertyNameObjectMap = (Map<String, Object>) objectMap.get(mapBeanPropertyName);
		return beanPropertyNameObjectMap.keySet().stream().map(key -> key.replaceAll("_", " ")).collect(Collectors.toList());
	}


	private String getKeyPrefixFromKey(String key) {
		if (key.contains(": ")) {
			return StringUtils.trim(key.substring(0, key.indexOf(": ")));
		}
		return StringUtils.trim(key);
	}


	private void validateSecurityOverridesForKey(String key, String messagePrefix) {
		if (key.contains("[")) {
			String valueString = key.substring(key.indexOf("[") + 1, key.indexOf("]"));
			String[] values = valueString.split(":");
			if (values.length > 0) {

				String investmentType = values[0];
				if ("Payout".equals(investmentType)) {
					if (values.length > 1) {
						String payoutTypeName = values[1];
						AssertUtils.assertNotNull(this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutTypeByName(payoutTypeName), messagePrefix + " - has an entry for Payout type [" + payoutTypeName + "] which does not exist. Key: " + key);
					}
				}
				else {
					if (investmentType != null) {
						InvestmentType type = this.investmentSetupService.getInvestmentTypeByName(investmentType);
						AssertUtils.assertNotNull(type, messagePrefix + " - has an entry for Investment Type name [" + investmentType + "] which does not exist. Key: " + key);

						if (values.length >= 2) {
							String value2 = values[1];
							InvestmentSubTypeSearchForm subTypeSearchForm = new InvestmentSubTypeSearchForm();
							subTypeSearchForm.setInvestmentTypeId(type.getId());
							subTypeSearchForm.setNameEquals(value2);
							AssertUtils.assertNotEmpty(this.investmentSetupService.getInvestmentTypeSubTypeList(subTypeSearchForm), messagePrefix + " - has an entry for SubType [" + value2 + "] which does not exist for Type [" + investmentType + "]. Key: " + key);

							if (values.length == 3) {
								String subType2 = values[2];
								InvestmentSubTypeSearchForm subType2SearchForm = new InvestmentSubTypeSearchForm();
								subType2SearchForm.setInvestmentTypeId(type.getId());
								subType2SearchForm.setNameEquals(subType2);
								AssertUtils.assertNotEmpty(this.investmentSetupService.getInvestmentTypeSubType2List(subType2SearchForm), messagePrefix + " - has an entry for SubType2 [" + subType2 + "] which does not exist for Type [" + investmentType + "]. Key: " + key);
							}
						}
					}
				}
			}
		}
	}
}
