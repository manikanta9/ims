package com.clifton.ims.tests.accounting.balance;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */

public class AccountingAccountPendingBalanceTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingValuationService accountingValuationService;


	@Test
	public void testPendingBondPositionTransferBetweenAccounts() {
		InvestmentSecurity bond = this.investmentInstrumentUtils.getCopyOfSecurity("912796FP9", "912796FP9_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));

		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.bonds);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.bonds);

		// Create a position for the account
		Date openDate = DateUtils.toDate("8/10/2015");
		BigDecimal positionSize = new BigDecimal("15000000.00");
		Trade buyTrade = this.tradeUtils.newBondTradeBuilder(bond, positionSize, true, openDate, fromAccount)
				.withAverageUnitPrice(new BigDecimal("99.928604167"))
				.withSettlementDate(DateUtils.toDate("8/11/2015"))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		//this.accountingUtils.assertPositionOpen(buyTrade);

		AccountingTransaction positionOpeningDetail = (AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0);

		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		Date transactionDate = DateUtils.toDate("10/1/2015");


		transfer.setFromClientInvestmentAccount(fromAccount.getClientAccount());
		transfer.setFromHoldingInvestmentAccount(fromAccount.getHoldingAccount());
		transfer.setToClientInvestmentAccount(toAccount.getClientAccount());
		transfer.setToHoldingInvestmentAccount(toAccount.getHoldingAccount());
		transfer.setType(this.accountingUtils.getAccountingPositionTransferType(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(transactionDate);
		transfer.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "to client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());

		List<AccountingPositionTransferDetail> transferDetails = new ArrayList<>();

		BigDecimal transferPrice = new BigDecimal("99.928604167");
		//Create another position transfer detail of 10 CCH with no commission override
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-10);
		transferDetail.setExistingPosition(positionOpeningDetail);
		transferDetail.setSecurity(bond);
		//transferDetail.setPositionCostBasis(new BigDecimal("10000"));
		transferDetail.setCostPrice(transferPrice);
		transferDetail.setTransferPrice(transferPrice);
		transferDetail.setQuantity(new BigDecimal("7000000.00"));
		transferDetail.setExchangeRateToBase(new BigDecimal("1"));
		transferDetail.setOriginalPositionOpenDate(openDate);
		transferDetails.add(transferDetail);

		transfer.setDetailList(transferDetails);
		this.accountingUtils.saveAccountingPositionTransfer(transfer);


		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(transfer.getTransactionDate());
		searchForm.setClientInvestmentAccountId(buyTrade.getClientInvestmentAccount().getId());
		searchForm.setHoldingInvestmentAccountId(buyTrade.getHoldingInvestmentAccount().getId());
		searchForm.setCollateralAccountingAccount(false);
		searchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE_AND_PREVIEW);
		searchForm.setPositionAccountingAccount(true);

		List<AccountingBalanceValue> balanceList = this.accountingValuationService.getAccountingBalanceValueList(searchForm);
		AccountingBalanceValue securityBalance = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(balanceList, (AccountingBalanceValue b) -> b.getInvestmentSecurity().getSymbol(), bond.getSymbol()));


		Assertions.assertTrue(MathUtils.isEqual(securityBalance.getQuantity(), new BigDecimal("8000000.00")));
		Assertions.assertTrue(MathUtils.isEqual(securityBalance.getLocalCostBasis(), new BigDecimal("7994288.34")));
		Assertions.assertTrue(MathUtils.isEqual(securityBalance.getBaseCostBasis(), new BigDecimal("7994288.34")));


		searchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE);

		balanceList = this.accountingValuationService.getAccountingBalanceValueList(searchForm);
		securityBalance = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(balanceList, (AccountingBalanceValue b) -> b.getInvestmentSecurity().getSymbol(), bond.getSymbol()));


		Assertions.assertTrue(MathUtils.isEqual(securityBalance.getQuantity(), new BigDecimal("8000000.00")));
		Assertions.assertTrue(MathUtils.isEqual(securityBalance.getLocalCostBasis(), new BigDecimal("7994288.34")));
		Assertions.assertTrue(MathUtils.isEqual(securityBalance.getBaseCostBasis(), new BigDecimal("7994288.34")));
	}
}
