package com.clifton.ims.tests.product.overlay.trade.group.dynamic.option;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.product.overlay.trade.group.dynamic.option.ProductOverlayRunTradeGroupDynamicOptionRoll;
import com.clifton.product.overlay.trade.group.dynamic.option.account.ProductOverlayRunTradeGroupDynamicOptionAccountRoll;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.test.util.RandomUtils;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.dynamic.TradeGroupDynamicService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;


/**
 * The <code>ProductOverlayRunTradeGroupDynamicOptionRollTests</code> tests option roll retrieval (account calculators) for various accounts.
 * Note: This uses historical data/runs for existing accounts.
 * <p>
 * WARNING: THIS TEST MAY CHANGE THE ACCOUNT CALCULATOR SELECTED FOR THE ACCOUNT IN ORDER TO TEST SPECIFIC SCENARIOS REGARDLESS OF WHAT THE ACCOUNT CALCULATOR IS TODAY. IF WE NEED TO KNOW WHAT IT WAS, THIS TEST
 * WILL NEED TO BE UPDATED TO CHANGE THE VALUE BACK AFTER THE TEST FINISHES.
 * <p>
 *
 * @author manderson
 */

public class ProductOverlayRunTradeGroupDynamicOptionRollTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private SystemBeanService systemBeanService;

	@SuppressWarnings("rawtypes")
	@Resource
	private TradeGroupDynamicService tradeGroupDynamicService;

	@Resource
	private TradeGroupService tradeGroupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private static final String ACCOUNT_DETAIL_PRINT_HEADER = "Client #\tHolding #\tExpiring Put Quantity\tExpiring Put Exposure\tExpiring Put Tooltip\tExpiring Call Quantity\tExpiring Call Exposure\tExpiring Call Tooltip\tPut Exposure\tCall Exposure\tPut Matching Exposure\tPut Matching Additional Exposure\tCall Matching Exposure\tCall Matching Additional Exposure\tPut Contract Value\tCall Contract Value";
	private static final String ACCOUNT_DETAIL_PRINT_FORMAT = "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s";


	/**
	 * Uses Default calculator bean: "Calls = Equities (Overlay), Puts = Cash and Cash Equivalents (Overlay)";
	 */
	@Test
	public void test_Default_CallsEquities_PutsCashAndCashEquivalents() {
		ProductOverlayRunTradeGroupDynamicOptionRoll optionRoll = generateOptionRollTradeEntry(new String[]{"647000", "460050", "122506"}, "Calls = Equities (Overlay), Puts = Cash and Cash Equivalents (Overlay)");
		for (ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll : optionRoll.getDetailList()) {
			String clientAccountNumber = accountRoll.getClientAccount().getNumber();
			String expectedResult;
			if ("647000".equals(clientAccountNumber)) {
				expectedResult = "647000\t768-70165-1-4\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-17:::-3357500:0:0:;SPXW US 01/08/16 P1900=-19:::-3610000:0:0:;Total=-36:0:0:-6967500:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-17:::-3303253:0:0:;SPXW US 01/08/16 C2090=-18:::-3497562:0:0:;Total=-35:0:0:-6800815:0:0:;\t-28,862,000\t-27,397,569\t32,392,478\t354,979\t31,056,439\t\t179,000\t194,309";
			}
			else if ("460050".equals(clientAccountNumber)) {
				expectedResult = "460050\t237-70497-2-7\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-7:::-1382500:0:0:;SPXW US 01/08/16 P1900=-6:::-1140000:0:0:;Total=-13:0:0:-2522500:0:0:;\t0\t0\tnull\t-10,303,500\t0\t12,072,955\t110,820\t0\t\t179,000";
			}
			else if ("122506".equals(clientAccountNumber)) {
				expectedResult = "122506\t033-1AAL11\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-26:::-5135000:0:0:;SPXW US 01/08/16 P1900=-4:::-760000:0:0:;Total=-30:0:0:-5895000:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-18:::-3497562:0:0:;SPXW US 01/08/16 C2090=-13:::-2526017:0:0:;Total=-31:0:0:-6023579:0:0:;\t-31,277,500\t-29,340,659\t33,484,784\t1,178,926\t32,639,568\t\t179,000\t194,309";
			}
			else {
				throw new RuntimeException("Missing expected results for account # " + clientAccountNumber);
			}
			validateExpectedResults(accountRoll, expectedResult);
		}
	}


	/**
	 * Calculator Bean: "Calls = Equities (Overlay), Puts = Cash Equivalents (Overlay) - NO CASH"
	 * This tests another common calculation similar to the default, except that Cash Value from the run is not included
	 */
	@Test
	public void test_CallsEquities_PutsCashEquivalents_NoCash() {
		ProductOverlayRunTradeGroupDynamicOptionRoll optionRoll = generateOptionRollTradeEntry(new String[]{"689180", "663525", "576610"}, "Calls = Equities (Overlay), Puts = Cash Equivalents (Overlay) - NO CASH");
		for (ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll : optionRoll.getDetailList()) {
			String clientAccountNumber = accountRoll.getClientAccount().getNumber();
			String expectedResult;
			if ("689180".equals(clientAccountNumber)) {
				expectedResult = "689180\t768-70183-1-2\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-5:::-987500:0:0:;SPXW US 01/08/16 P1900=-5:::-950000:0:0:;Total=-10:0:0:-1937500:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-6:::-1165854:0:0:;SPXW US 01/08/16 C2090=-6:::-1165854:0:0:;Total=-12:0:0:-2331708:0:0:;\t-9,556,000\t-8,938,214\t10,636,334\t\t10,256,824\t\t179,000\t194,309";
			}
			else if ("663525".equals(clientAccountNumber)) {
				expectedResult = "663525\t033-1AADE2\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-16:::-3160000:0:0:;SPXW US 01/08/16 P1900=-17:::-3230000:0:0:;Total=-33:0:0:-6390000:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-16:::-3108944:0:0:;SPXW US 01/08/16 C2090=-16:::-3108944:0:0:;Total=-32:0:0:-6217888:0:0:;\t-25,409,000\t-24,677,243\t29,072,421\t\t27,982,065\t\t179,000\t194,309";
			}
			else if ("576610".equals(clientAccountNumber)) {
				expectedResult = "576610\t768-70156-1-5\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-2:::-395000:0:0:;SPXW US 01/08/16 P1900=-3:::-570000:0:0:;Total=-5:0:0:-965000:0:0:;\t0\t0\tnull\t-3,635,500\t0\t4,169,017\t\t0\t\t179,000";
			}
			else {
				throw new RuntimeException("Missing expected results for account # " + clientAccountNumber);
			}
			validateExpectedResults(accountRoll, expectedResult);
		}
	}


	/**
	 * Calculator Bean: "Calls = 50% TPV, Puts = 50% TPV"
	 * This tests the calculation that calculates matching exposure for Calls and Puts based on 50 % of the Total Portfolio Value (less Option Market Value which is the default option)
	 */
	@Test
	public void test_TotalPortfolioValue_50_50() {
		ProductOverlayRunTradeGroupDynamicOptionRoll optionRoll = generateOptionRollTradeEntry(new String[]{"336000", "336020", "058340"}, "Calls = 50% TPV, Puts = 50% TPV");
		for (ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll : optionRoll.getDetailList()) {
			String clientAccountNumber = accountRoll.getClientAccount().getNumber();
			String expectedResult;
			if ("336000".equals(clientAccountNumber)) {
				expectedResult = "336000\t033-1AAG09\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-9:::-1777500:0:0:;SPXW US 01/08/16 P1900=-10:::-1900000:0:0:;Total=-19:0:0:-3677500:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-9:::-1748781:0:0:;SPXW US 01/08/16 C2090=-10:::-1943090:0:0:;Total=-19:0:0:-3691871:0:0:;\t-14,095,000\t-14,378,866\t0\t16,511,250\t0\t16,511,250\t179,000\t194,309";
			}
			else if ("336020".equals(clientAccountNumber)) {
				expectedResult = "336020\t033-1AAG17\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-22:::-4345000:0:0:;SPXW US 01/08/16 P1900=-23:::-4370000:0:0:;Total=-45:0:0:-8715000:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-22:::-4274798:0:0:;SPXW US 01/08/16 C2090=-23:::-4469107:0:0:;Total=-45:0:0:-8743905:0:0:;\t-33,169,500\t-33,809,766\t0\t38,887,001\t0\t38,887,001\t179,000\t194,309";
			}
			else if ("058340".equals(clientAccountNumber)) {
				expectedResult = "058340\t033-1AAPE9\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-65:::-12837500:0:0:;SPXW US 01/08/16 P1900=-66:::-12540000:0:0:;Total=-131:0:0:-25377500:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-65:::-12630085:0:0:;SPXW US 01/08/16 C2090=-66:::-12824394:0:0:;Total=-131:0:0:-25454479:0:0:;\t-102,260,500\t-103,955,315\t0\t117,138,604\t0\t117,138,604\t179,000\t194,309";
			}
			else {
				throw new RuntimeException("Missing expected results for account # " + clientAccountNumber);
			}
			validateExpectedResults(accountRoll, expectedResult);
		}
	}


	/**
	 * Calculator Bean: "GuideStone: Puts = Cash Equivalents (Overlay), Calls = Equities (Manager Balance)"
	 * This tests uses the Cash Equivalents Overlay Exposure for Puts, but Manager Account Balance for Call Matching Exposure
	 */
	@Test
	public void test_CallsManagerBalance_PutsCashEquivalents() {
		ProductOverlayRunTradeGroupDynamicOptionRoll optionRoll = generateOptionRollTradeEntry(new String[]{"297150"}, "GuideStone: Puts = Cash Equivalents (Overlay), Calls = Equities (Manager Balance)");
		for (ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll : optionRoll.getDetailList()) {
			String clientAccountNumber = accountRoll.getClientAccount().getNumber();
			String expectedResult;
			if ("297150".equals(clientAccountNumber)) {
				expectedResult = "297150\t033-197450\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 P1975=-61:::-12047500:0:0:;SPXW US 01/08/16 P1900=-63:::-11970000:0:0:;Total=-124:0:0:-24017500:0:0:;\t0\t0\tshowCurrent=true;showPending=false;SPXW US 01/08/16 C2130=-11:::-2137399:0:0:;SPXW US 01/08/16 C2090=-12:::-2331708:0:0:;Total=-23:0:0:-4469107:0:0:;\t-98,823,000\t-18,459,355\t111,262,406\t\t0\t21,330,728\t179,000\t194,309";
			}
			else {
				throw new RuntimeException("Missing expected results for account # " + clientAccountNumber);
			}
			validateExpectedResults(accountRoll, expectedResult);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	private ProductOverlayRunTradeGroupDynamicOptionRoll generateOptionRollTradeEntry(String[] accountNumbers, String accountCalculatorBeanName) {
		InvestmentAccountGroup testGroup = setupTestAccountGroupAndAccountOverrides(accountNumbers, accountCalculatorBeanName);
		ProductOverlayRunTradeGroupDynamicOptionRoll rollCommand = new ProductOverlayRunTradeGroupDynamicOptionRoll();
		rollCommand.setTradeGroupType(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.tradeGroupService.getTradeGroupTypeByName("Option Roll"), 2));
		rollCommand.setClientAccountGroup(testGroup);
		rollCommand.setTradeDate(DateUtils.toDate("01/08/2016"));
		rollCommand.setExpiringOptionsDate(DateUtils.toDate("01/08/2016"));
		rollCommand.setBalanceDate(DateUtils.toDate("01/07/2016"));
		rollCommand.setPutSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 02/12/16 P1790"));
		rollCommand.setCallSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 02/12/16 C2005"));
		rollCommand.setOptionInstrument(rollCommand.getPutSecurity().getInstrument());

		rollCommand = (ProductOverlayRunTradeGroupDynamicOptionRoll) this.tradeGroupDynamicService.getTradeGroupDynamicEntry(rollCommand);
		ValidationUtils.assertEquals(accountNumbers.length, CollectionUtils.getSize(rollCommand.getDetailList()), "Expected " + accountNumbers.length + " account roll results in the Option Roll Retrieval");
		return rollCommand;
	}


	private InvestmentAccountGroup setupTestAccountGroupAndAccountOverrides(String[] accountNumbers, String accountCalculatorBeanName) {
		SystemBean accountCalculatorBean = this.systemBeanService.getSystemBeanByName(accountCalculatorBeanName);
		AssertUtils.assertNotNull(accountCalculatorBean, "Missing Option Roll Account Calculator Bean with Name: " + accountCalculatorBeanName);

		InvestmentAccountGroup testGroup = new InvestmentAccountGroup();
		testGroup.setName(RandomUtils.randomName());
		testGroup.setType(this.investmentAccountGroupService.getInvestmentAccountGroupType((short) 1)); // Group Type of "Other" is 1
		this.investmentAccountGroupService.saveInvestmentAccountGroup(testGroup);

		for (String accountNumber : accountNumbers) {
			InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
			updateAccountCalculatorBean(clientAccount, accountCalculatorBean);
			this.investmentAccountGroupService.linkInvestmentAccountGroupToAccount(testGroup.getId(), clientAccount.getId());
		}
		return testGroup;
	}


	private void updateAccountCalculatorBean(InvestmentAccount clientAccount, SystemBean accountCalculatorBean) {
		SystemColumnCustomConfig systemColumnCustomConfig = new SystemColumnCustomConfig();
		systemColumnCustomConfig.setColumnGroupName("PIOS Processing");
		systemColumnCustomConfig.setEntityId(clientAccount.getId());
		systemColumnCustomConfig.setLinkedValue("true");
		systemColumnCustomConfig = this.systemColumnValueService.getSystemColumnCustomConfig(systemColumnCustomConfig);
		if (systemColumnCustomConfig.getColumnValueList() == null) {
			systemColumnCustomConfig.setColumnValueList(new ArrayList<>());
		}
		boolean found = false;
		for (SystemColumnValue systemColumnValue : CollectionUtils.getIterable(systemColumnCustomConfig.getColumnValueList())) {
			if ("Trade Group Option Roll Account Calculator".equals(systemColumnValue.getColumn().getName())) {
				found = true;
				systemColumnValue.setValue(accountCalculatorBean.getId() + "");
				systemColumnValue.setText(accountCalculatorBean.getName());
				break;
			}
		}

		if (!found) {
			SystemColumnValue value = new SystemColumnValue();
			value.setColumn(CollectionUtils.getFirstElement(BeanUtils.filter(systemColumnCustomConfig.getColumnList(), SystemColumnCustom::getName, "Trade Group Option Roll Account Calculator")));
			value.setValue(accountCalculatorBean.getId() + "");
			value.setText(accountCalculatorBean.getName());
			systemColumnCustomConfig.getColumnValueList().add(value);
		}
		this.systemColumnValueService.saveSystemColumnValueList(systemColumnCustomConfig);
	}

	////////////////////////////////////////////////////////////////////////////////


	private void validateExpectedResults(ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll, String expectedResult) {
		String result = getProductOverlayRunTradeGroupDynamicOptionAccountRollAsString(accountRoll);
		if (!StringUtils.isEqual(expectedResult.trim(), result.trim())) {
			StringBuilder message = new StringBuilder(100);
			message.append(StringUtils.NEW_LINE);
			message.append("Expected: ");
			message.append(StringUtils.NEW_LINE);
			message.append(ACCOUNT_DETAIL_PRINT_HEADER);
			message.append(StringUtils.NEW_LINE);
			message.append(expectedResult);
			message.append(StringUtils.NEW_LINE);
			message.append("Actual: ");
			message.append(StringUtils.NEW_LINE);
			message.append(result);
			throw new ValidationException("Option Roll Trade Group Results for Account " + accountRoll.getClientAccount().getNumber() + " do not match expected: " + message.toString());
		}
	}


	private String getProductOverlayRunTradeGroupDynamicOptionAccountRollAsString(ProductOverlayRunTradeGroupDynamicOptionAccountRoll accountRoll) {
		// Holding Account Might Be Null In Some Cases
		String holdingAccountNumber = accountRoll.getHoldingAccount() != null ? accountRoll.getHoldingAccount().getNumber() : "";
		return String.format(ACCOUNT_DETAIL_PRINT_FORMAT, accountRoll.getClientAccount().getNumber(), holdingAccountNumber, CoreMathUtils.formatNumberInteger(accountRoll.getExpiringPutTradedInstrumentQuantity()), accountRoll.getExpiringPutExposure(),
				accountRoll.getExpiringPutTooltipString(), CoreMathUtils.formatNumberInteger(accountRoll.getExpiringCallTradedInstrumentQuantity()), CoreMathUtils.formatNumberInteger(accountRoll.getExpiringCallExposure()),
				accountRoll.getExpiringCallTooltipString(), CoreMathUtils.formatNumberInteger(accountRoll.getPutExposure()), CoreMathUtils.formatNumberInteger(accountRoll.getCallExposure()),
				CoreMathUtils.formatNumberInteger(accountRoll.getPutMatchingExposure()), CoreMathUtils.formatNumberInteger(accountRoll.getPutMatchingAdditionalExposure()), CoreMathUtils.formatNumberInteger(accountRoll.getCallMatchingExposure()), CoreMathUtils.formatNumberInteger(accountRoll.getCallMatchingAdditionalExposure()),
				CoreMathUtils.formatNumberDecimal(accountRoll.getPutContractValue()), CoreMathUtils.formatNumberDecimal(accountRoll.getCallContractValue()));
	}
}
