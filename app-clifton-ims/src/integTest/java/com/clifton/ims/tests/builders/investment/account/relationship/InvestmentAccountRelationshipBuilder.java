package com.clifton.ims.tests.builders.investment.account.relationship;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;

import java.util.Date;


/**
 * @author stevenf
 */
public class InvestmentAccountRelationshipBuilder {

	private final InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountRelationshipPurpose purpose;
	private InvestmentSecurityGroup securityGroup;
	private Date startDate;
	private Date endDate;
	private InvestmentAccountAssetClass accountAssetClass;
	private InvestmentAccount referenceOne;
	private InvestmentAccount referenceTwo;


	private InvestmentAccountRelationshipBuilder(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public static InvestmentAccountRelationshipBuilder investmentAccountRelationship(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		return new InvestmentAccountRelationshipBuilder(investmentAccountRelationshipService);
	}


	public InvestmentAccountRelationship build() {
		InvestmentAccountRelationship investmentAccountRelationship = new InvestmentAccountRelationship();
		investmentAccountRelationship.setPurpose(this.purpose);
		investmentAccountRelationship.setSecurityGroup(this.securityGroup);
		investmentAccountRelationship.setStartDate(this.startDate);
		investmentAccountRelationship.setEndDate(this.endDate);
		investmentAccountRelationship.setAccountAssetClass(this.accountAssetClass);
		investmentAccountRelationship.setReferenceOne(this.referenceOne);
		investmentAccountRelationship.setReferenceTwo(this.referenceTwo);
		return investmentAccountRelationship;
	}


	public InvestmentAccountRelationship buildAndSave() {
		InvestmentAccountRelationship investmentAccountRelationship = build();
		return this.investmentAccountRelationshipService.saveInvestmentAccountRelationship(investmentAccountRelationship);
	}


	public InvestmentAccountRelationshipBuilder but() {
		return investmentAccountRelationship(this.investmentAccountRelationshipService)
				.withPurpose(this.purpose)
				.withSecurityGroup(this.securityGroup)
				.withStartDate(this.startDate)
				.withEndDate(this.endDate)
				.withAccountAssetClass(this.accountAssetClass)
				.withReferenceOne(this.referenceOne)
				.withReferenceTwo(this.referenceTwo);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////
	public InvestmentAccountRelationshipBuilder withPurpose(InvestmentAccountRelationshipPurpose purpose) {
		this.purpose = purpose;
		return this;
	}


	public InvestmentAccountRelationshipBuilder withSecurityGroup(InvestmentSecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
		return this;
	}


	public InvestmentAccountRelationshipBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public InvestmentAccountRelationshipBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public InvestmentAccountRelationshipBuilder withAccountAssetClass(InvestmentAccountAssetClass accountAssetClass) {
		this.accountAssetClass = accountAssetClass;
		return this;
	}


	public InvestmentAccountRelationshipBuilder withReferenceOne(InvestmentAccount referenceOne) {
		this.referenceOne = referenceOne;
		return this;
	}


	public InvestmentAccountRelationshipBuilder withReferenceTwo(InvestmentAccount referenceTwo) {
		this.referenceTwo = referenceTwo;
		return this;
	}
}
