package com.clifton.ims.tests.billing.invoice;


import com.clifton.billing.billingbasis.BillingBasisValuationType;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingScheduleBillingDefinitionDependency;
import com.clifton.billing.definition.search.BillingScheduleBillingDefinitionDependencySearchForm;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceGroup;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.process.schedule.amount.BillingAmountCalculatorUtils;
import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceDetailSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.dw.analytics.DwAnalyticsService;
import com.clifton.dw.analytics.search.AccountingPositionSnapshotSearchForm;
import com.clifton.dw.rebuild.DwRebuildCommand;
import com.clifton.dw.rebuild.DwRebuildService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.system.SystemConditionUtils;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.security.user.SecurityGroup;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionAction;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BillingInvoiceProcessTests</code> voids and creates corrected invoices
 * for specific definitions and validates the result is the same as what it was.
 * <p>
 * IMPORTANT:
 * - We need to use existing definitions, because there is a lot of definition setup
 * and billing basis valuations that can be used and it's not possible to easily "mimic" these manually
 * - Tests will always validate final result against the top parent, so when running more than once we always ensure
 * we aren't comparing this "test" to previous "test" and get a false positive.  This will only work for billing definitions
 * that users did not VOID and Create Corrected manually - if so, that billing definition cannot be used;
 * <p>
 * In most cases we compare to actual, in a few there were some account allocation logic that has been fixed or improved - for those cases
 * the expected break is manually set in the test to confirm new logic.  For these cases, it was decided not to go back and change existing account
 * allocations, but just improve going forward.
 *
 * @author manderson
 */

public class BillingInvoiceProcessTests extends BaseImsIntegrationTest {

	private static final Logger log = LoggerFactory.getLogger(BillingInvoiceProcessTests.class);

	@Resource
	private BillingDefinitionService billingDefinitionService;

	@Resource
	private BillingInvoiceService billingInvoiceService;

	@Resource
	private DwRebuildService dwRebuildService;

	@Resource
	private DwAnalyticsService dwAnalyticsService;

	@Resource
	private RuleDefinitionService ruleDefinitionService;

	@Resource
	private RuleViolationService ruleViolationService;

	@Resource
	private SystemConditionUtils systemConditionUtils;

	@Resource
	private SystemConditionService systemConditionService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;


	private Short billingDefinitionApprovedWorkflowStateId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setupTests() {
		// Clear Ignore Condition for Rule Definition "Billing Definition Account: No Billing Basis"
		// Need this in order to make tests re-runnable and push existing invoices forward and create new ones for the test
		RuleDefinition ruleDefinition = this.ruleDefinitionService.getRuleDefinitionByName("Billing Definition Account: No Billing Basis");
		ruleDefinition.setIgnoreCondition(this.systemConditionService.getSystemConditionByName("Always True Condition"));
		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.ruleDefinitionService.saveRuleDefinition(ruleDefinition), 1);

		// Remove Requirement on Definition Being Approved in order to Approve Invoice
		this.systemConditionUtils.changeSystemConditionEntryBean("Billing Invoice Initial Approval Condition", "Billing Definition Workflow Status Property = Approved", "Always True Comparison");

		// Remove Post Report Action - NOTE: These are NOT added back after the test finishes
		removePostReportAction(this.workflowTransitionService.getWorkflowTransition(100));
		removePostReportAction(this.workflowTransitionService.getWorkflowTransition(131));

		Workflow definitionWorkflow = this.workflowDefinitionService.getWorkflowByName("Billing Definition Status");
		this.billingDefinitionApprovedWorkflowStateId = this.workflowDefinitionService.getWorkflowStateByName(definitionWorkflow.getId(), "Approved").getId();
	}


	private void removePostReportAction(WorkflowTransition transition) {
		boolean found = false;
		for (WorkflowTransitionAction action : transition.getActionList()) {
			if (StringUtils.isEqual("Billing Invoice Post Report Workflow Action", action.getActionSystemBean().getName())) {
				transition.getActionList().remove(action);
				found = true;
				break;
			}
		}
		if (found) {
			this.workflowTransitionService.saveWorkflowTransition(transition);
		}
	}


	@AfterEach
	public void cleanUpTests() {
		// Reset Ignore Condition for Rule Definition "Billing Definition Account: No Billing Basis"
		RuleDefinition ruleDefinition = this.ruleDefinitionService.getRuleDefinitionByName("Billing Definition Account: No Billing Basis");
		ruleDefinition.setIgnoreCondition(this.systemConditionService.getSystemConditionByName("Billing Account - Ignore Condition for No Billing Basis"));
		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.ruleDefinitionService.saveRuleDefinition(ruleDefinition), 1);

		// Put Requirement back on Definition Being Approved in order to Approve Invoice
		this.systemConditionUtils.changeSystemConditionEntryBean("Billing Invoice Initial Approval Condition", "Always True Comparison", "Billing Definition Workflow Status Property = Approved");
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	// 3: McKnight Flat Fee Only
	public void testFlatFeeInvoice() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{3});
	}


	@Test
	//  4, // Continental Period Average: Notional, Syn Exp, and Market Value
	public void testPeriodAverage_VariousBillingBasis() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{4});
	}


	// 38, // BCBS Shared with HMO (40): Notional - Exposure Hedge (Initial) Period Trade Dates
	// 40, // HMO Shared with BCBS (38): Notional - Exposure Hedge (Initial) Period Trade Dates
	@Test
	public void testPeriodTradeDates_HedgingInitial_Shared() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{38, 40});
	}


	// 47, // Alliant Energy Corp: Period End, Period Average, Market Value, External, Synthetic Exposure
	@Test
	public void testPeriodEnd_PeriodAverage_VariousBillingBasis_External() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{47});
	}


	//	98, // Denison - Period Average Synthetic Exposure, 1 account partial period, Account with Sub Accounts
	@Test
	public void testPeriodAverage_SynExp_PartialPeriod_SubAccounts() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{98});
	}


	// 122, // New Orleans FF Shared with 123 - Period End Synthetic Exposure
	// 123, // New Orleans FF Shared with 122 - Period End Synthetic Exposure
	@Test
	public void testPeriodEnd_Shared() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{122, 123});
	}


	// 298: Trustees of the Lawrenceville School-IMA (06/28/2012 - )
	@Test
	public void testPeriodAverage_HedgingCurrent() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{298});
	}


	// 356, // Trinity Health Corp shared with (358) Trinity Health Pension Market Value w/ Cash Period Average
	// 358, // Trinity Health Pension shared with (356) Trinity Health Corp Market Value w/ Cash Period Average
	@Test
	public void testPeriodAverage_MarketValueWithCash_Shared() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{356, 358});
	}


	// 130, // Guidestone Family of Funds - a LOT of accounts, Cash/Collateral, Market Value, Additional One time schedules
	@Test
	public void testManyAccounts_CashCollateral_MarketValue_OneTimeSchedules() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		// Account allocations are different because we stopped applying fixed fees to accounts whose billing wasn't active during the invoice period (Now Account 1147 is excluded)
		Map<Integer, String> accountOverrideMap = new HashMap<>();
		accountOverrideMap.put(1154, "808.27");
		accountOverrideMap.put(1156, "3357.11");
		accountOverrideMap.put(1157, "2672.40");
		accountOverrideMap.put(1158, "2291.22");
		accountOverrideMap.put(2854, "535.78");
		accountOverrideMap.put(1159, "625.24");
		accountOverrideMap.put(1160, "1271.34");
		accountOverrideMap.put(1161, "982.45");
		accountOverrideMap.put(1162, "785.91");
		accountOverrideMap.put(1163, "586.54");
		accountOverrideMap.put(2635, "514.71");
		accountOverrideMap.put(1164, "1069.73");
		accountOverrideMap.put(1165, "1038.34");
		accountOverrideMap.put(1166, "833.07");
		accountOverrideMap.put(1167, "751.99");
		accountOverrideMap.put(1840, "1189.20");
		accountOverrideMap.put(1142, "1280.02");
		accountOverrideMap.put(1144, "1012.07");
		accountOverrideMap.put(1145, "2789.44");
		accountOverrideMap.put(1146, "4582.43");
		accountOverrideMap.put(1147, "");
		accountOverrideMap.put(1148, "2502.74");
		processAndValidateInvoices(invoiceDate, new Integer[]{130}, accountOverrideMap);
	}


	// 698 Adept 17 - Ability to bill on Position Collateral Separately
	@Test
	public void testPositionCollateralMarketValue() {
		// Note: The billing valuation for Position Collateral Should be zero for first quarter
		// Should also add another invoice test when there is an actual value
		Date invoiceDate = DateUtils.toDate("12/31/2016");
		processAndValidateInvoices(invoiceDate, new Integer[]{698});
	}


	// 130, // Guidestone Family of Funds - a LOT of accounts, Cash/Collateral, Market Value, Additional One time schedules
	// CURRENCY TEST
	@Test
	public void testCurrency() {
		// Special Test for Guidestone to verify CCY changes, we don't bill on CCY often and last time was for Guidestone in October 2013 (Note they use a monthly invoice)
		Date invoiceDate = DateUtils.toDate("10/31/2013");
		// Account allocations are different because we stopped applying fixed fees to accounts whose billing wasn't active during the invoice period (Now Acct ID 1147 is excluded)
		Map<Integer, String> accountOverrideMap = new HashMap<>();
		accountOverrideMap.put(1154, "560.98");
		accountOverrideMap.put(1156, "3089.57");
		accountOverrideMap.put(1157, "2394.87");
		accountOverrideMap.put(1158, "2005.52");
		accountOverrideMap.put(1159, "380.61");
		accountOverrideMap.put(1160, "1020.07");
		accountOverrideMap.put(1161, "730.03");
		accountOverrideMap.put(1705, "3198.45");
		accountOverrideMap.put(1162, "533.96");
		accountOverrideMap.put(1163, "340.73");
		accountOverrideMap.put(2635, "269.69");
		accountOverrideMap.put(1164, "817.72");
		accountOverrideMap.put(1165, "780.80");
		accountOverrideMap.put(1166, "578.18");
		accountOverrideMap.put(1167, "498.13");
		accountOverrideMap.put(1840, "853.98");
		accountOverrideMap.put(1142, "997.95");
		accountOverrideMap.put(1144, "756.14");
		accountOverrideMap.put(1145, "2360.29");
		accountOverrideMap.put(1146, "4032.61");
		accountOverrideMap.put(1147, "");
		accountOverrideMap.put(1148, "2245.72");
		processAndValidateInvoices(invoiceDate, new Integer[]{130}, accountOverrideMap);
	}


	// 327, // Clifton Equal Sector 800060 - Period Start External
	// Test is no longer valid as we now have IMS calculated values for funds at the investor level
	// There are currently no other types of invoices that use period start and external
	@Test
	@Disabled
	public void testPeriodStart_External() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{327});
	}


	// 372, // U of MN Synthetic Exposure - Period Month End Average
	@Test
	public void testPeriodMonthEndAverage_SyntheticExposure() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		processAndValidateInvoices(invoiceDate, new Integer[]{372});
	}


	// 403, // Three Sisters (Margaret A. Cargill) - Shared with 404 & 405 - multiple accounts for account allocation test
	// 404, // Three Sisters (Akaloa) - Shared with 403 & 405 - multiple accounts for account allocation test
	// 405, // Three Sisters (Anne Ray) - Shared with 403 & 404 - multiple accounts for account allocation test
	@Test
	public void testSharedWithMultipleAccounts_AccountAllocation() {
		// Billing Schedule "Additional Separate Account Fee" for Billing Definitions 404: Akaloa Resource Foundation-IMA (05/01/2013 - )
		// and 405: 405: Anne Ray Charitable Trust-IMA (05/01/2013 - )
		// was changed to pro-rate for positions on - use a newer invoice

		// NOTE: BILLING ACCOUNT SETUP WAS UPDATED SO NEED TO USE NEW INVOICE
		Date invoiceDate = DateUtils.toDate("03/31/2017");
		processAndValidateInvoices(invoiceDate, new Integer[]{403, 404, 405});
	}


	// Billing Definitions 260 and 261 - PWC (PriceWaterHouse) Test for Dirty Price calculation on Swaps
	@Test
	public void testSharedWithMultipleAccounts_NotionalDirtyPrice() {
		Date invoiceDate = DateUtils.toDate("03/31/2014");


		// Because of issues with the invoice before, these invoices where previously voided and re-calculated/confirmed
		// So we need to validate against the invoice we know was verified, not the root parent like other cases
		processInvoices(invoiceDate, new Integer[]{260, 261});

		List<BillingInvoice> newInvoiceList = getCurrentInvoiceList(invoiceDate, new Integer[]{260, 261});
		for (BillingInvoice invoice : newInvoiceList) {
			if (invoice.getBillingDefinition().getId() == 260) {
				validateBillingInvoice(invoice, this.billingInvoiceService.getBillingInvoice(6040), null);
			}
			else {
				validateBillingInvoice(invoice, this.billingInvoiceService.getBillingInvoice(6041), null);
			}
		}
	}


	// Billing Definitions 622, 623, 624, 625, 626 Duke Test for AUM calculation
	@Test
	public void testSharedWithMultipleAccounts_AUM() {
		Date invoiceDate = DateUtils.toDate("03/31/2016");
		// Because of issues with the invoice before, these invoices where previously voided and re-calculated/confirmed
		// So we need to validate against the invoice we know was verified, not the root parent like other cases
		processAndValidateInvoices(invoiceDate, new Integer[]{622, 623, 624, 625, 626});
	}


	@Test
	public void testPrivateFundInvestors() {
		// Global Defensive Equity - Uses customized Private Fund Investor Valuation Type, and Period Start that accounts for Holidays
		// Need various test dates to verify when month start lands on a weekend, month start lands on a holiday, etc.
		Integer[] billingDefinitions = new Integer[]{674};
		// Beginning of Fund
		Date invoiceDate = DateUtils.toDate("09/30/2015");
		processAndValidateInvoices(invoiceDate, billingDefinitions);

		// November 1st is a weekend
		invoiceDate = DateUtils.toDate("11/30/2015");
		processAndValidateInvoices(invoiceDate, billingDefinitions);

		invoiceDate = DateUtils.toDate("12/31/2015");
		processAndValidateInvoices(invoiceDate, billingDefinitions);

		// January 1st is a Holiday
		invoiceDate = DateUtils.toDate("01/31/2016");
		processAndValidateInvoices(invoiceDate, billingDefinitions);

		// Equal Sector Fund Validates New Schedule Type for Fixed Fee that can allocate fee based on billing basis
		billingDefinitions = new Integer[]{690};
		invoiceDate = DateUtils.toDate("10/31/2012");
		processAndValidateInvoices(invoiceDate, billingDefinitions);
	}


	// Billing Definition 652
	@Test
	public void testManagerBalance() {
		// The Baptist Foundation of Oklahoma - Uses manager balance because part of the billing basis value comes from Seattle and is downloaded there
		Integer[] billingDefinitions = new Integer[]{652};
		// This invoice was originally external, but if reconfigured to use manager balance the result is the same
		// Could continue with this test OR add new test for 9/30/2016 invoice
		Date invoiceDate = DateUtils.toDate("06/30/2016");
		processAndValidateInvoices(invoiceDate, billingDefinitions);
	}


	// Billing Definition 1342
	@Test
	public void testSecurityTargetPeriodWeighted() {
		// Aegis Class, LLC W-000991 3/24/2016 to 03/08/2018
		Integer[] billingDefinitionIds = new Integer[]{1342};
		Date invoiceDate = DateUtils.toDate("03/31/2016"); // First Month - confirm prorating
		processAndValidateInvoices(invoiceDate, billingDefinitionIds);

		// Regular Month - no prorating - no security target changes during the month
		invoiceDate = DateUtils.toDate("12/31/2016");
		processAndValidateInvoices(invoiceDate, billingDefinitionIds);

		// Month with Security Target Changes
		invoiceDate = DateUtils.toDate("01/31/2018");
		processAndValidateInvoices(invoiceDate, billingDefinitionIds);

		// Month Account terminates
		invoiceDate = DateUtils.toDate("03/31/2018");
		processAndValidateInvoices(invoiceDate, billingDefinitionIds);
	}


	/**
	 * Note: The intention of this test is to test BILLING-147 AND BILLING-136
	 * for the use of BillingScheduleBillingDefinitionDependency and a Schedule Waive Condition the depends on Prerequisite Invoice
	 * Until those Jira items are released (IMS 9.11.0 - November 2017) and the billing definitions are re-configured it's not actually testing that
	 * <p>
	 * Sonoma DE Billing Definition 742 (Prerequisite)
	 * Sonoma PIOS Billing Definition 741 (Dependent through Discount Schedule)
	 */
	@Test
	public void testBillingScheduleDependency_WaiveCondition() {
		// First Need to Process 742 - Prerequisite Invoice
		Integer[] billingDefinitions = new Integer[]{742};
		Date invoiceDate = DateUtils.toDate("09/30/2017");
		processAndValidateInvoices(invoiceDate, billingDefinitions);

		// Do Not Need to Re-Process 741 Independently - Since it's a Dependent Invoice, it's already been voided and re-generated, so just validate it
		billingDefinitions = new Integer[]{741};
		List<BillingInvoice> newInvoiceList = getCurrentInvoiceList(DateUtils.toDate("09/30/2017"), billingDefinitions);
		for (BillingInvoice invoice : newInvoiceList) {
			validateBillingInvoice(invoice, null);
		}
	}


	/**
	 * Tests Minimum Fee with Credit - No max on the credit.
	 */
	@Test
	public void testMinimumFeeWithCredit_NoMax() {
		// Note: Depends on Billing Definition 674
		Integer[] billingDefinitions = new Integer[]{752};
		Date invoiceDate = DateUtils.toDate("12/31/2017");
		processAndValidateInvoices(invoiceDate, billingDefinitions);
	}


	/**
	 * Tests Minimum Fee with Credit - Max 5K
	 */
	@Test
	public void testMinimumFeeWithCredit_Max() {
		// Note: Depends on Billing Definition 674
		Integer[] billingDefinitions = new Integer[]{728};
		Date invoiceDate = DateUtils.toDate("03/31/2018");
		processAndValidateInvoices(invoiceDate, billingDefinitions);
	}


	/*
	 * Executing the Re-Process transition on BillingInvoice started causing
	 * an unexpected Hibernate exception following changes on Jira ticket INVESTMENT-482.
	 */
	@Test
	public void testReProcessInvoiceTransition() {
		Date invoiceDate = DateUtils.toDate("12/31/2013");
		Integer billingDefinitionId = 372;
		List<BillingInvoice> invoiceList = getCurrentInvoiceList(invoiceDate, new Integer[]{billingDefinitionId});
		BillingInvoice invoice = CollectionUtils.getOnlyElement(BeanUtils.filter(invoiceList, billingInvoice -> billingInvoice.getBillingDefinition().getId(), billingDefinitionId));
		try {
			transitionInvoiceByTransitionName(invoice, "Re-Process Invoice");
		}
		catch (Exception e) {
			String expectedHibernateMessage = "Batch update returned unexpected row count from update [0]; actual row count: 0; expected: 1";
			Assertions.assertTrue(e.getMessage().contains(expectedHibernateMessage));
			Assertions.fail("Did not expect to encounter Hibernate [" + expectedHibernateMessage + "].");
		}
	}


	/*
	 * Deleting a BillingInvoice is causing an unexpected Hibernate exception regarding re-associating
	 * a transient collection. The collection in issue is our lazy, noop bag mapping, which should be
	 * ignored. Started after Hibernate HHH-9777. Added Hibernate message board post:
	 * https://forum.hibernate.org/viewtopic.php?f=1&t=1042912
	 */
	@Test
	public void testDeleteBillingInvoice() {
		BillingInvoice invoice = new BillingInvoice();
		invoice.setInvoiceType(this.billingInvoiceService.getBillingInvoiceType((short) 2)); // Legal Fee
		invoice.setBusinessCompany(this.businessUtils.createBusinessClient().getCompany());
		invoice.setVendorBusinessCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP));
		invoice.setInvoiceDate(new Date());
		invoice.setInvoicePeriodStartDate(invoice.getInvoiceDate());
		invoice.setInvoicePeriodEndDate(invoice.getInvoiceDate());
		invoice.setTotalAmount(BigDecimal.ONE);

		BillingInvoice saved = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.billingInvoiceService.saveBillingInvoice(invoice), 3);
		try {
			this.billingInvoiceService.deleteBillingInvoice(saved.getId());
		}
		catch (Exception e) {
			String expectedHibernateMessage = "could not reassociate uninitialized transient collection";
			Assertions.assertTrue(e.getMessage().contains(expectedHibernateMessage));
			Assertions.fail("Did not expect to encounter Hibernate [" + expectedHibernateMessage + "].");
		}
	}


	private void processAndValidateInvoices(Date date, Integer[] billingDefinitionIds) {
		processAndValidateInvoices(date, billingDefinitionIds, null);
	}


	/**
	 * @param date
	 * @param billingDefinitionIds:         Billing Definition Ids to process/validate
	 * @param accountAllocationOverrideMap: Map of BillingDefinitionInvestmentAccountID -> Billing Amount to verify account allocations for cases where changes/improves altered existing logic at that time.
	 */
	private void processAndValidateInvoices(Date date, Integer[] billingDefinitionIds, Map<Integer, String> accountAllocationOverrideMap) {
		// Process Invoices - Call Once to re-process all that we need
		// Shared invoices are processed together, so just process all, and then validate all
		processInvoices(date, billingDefinitionIds);

		List<BillingInvoice> newInvoiceList = getCurrentInvoiceList(date, billingDefinitionIds);
		for (BillingInvoice invoice : newInvoiceList) {
			validateBillingInvoice(invoice, accountAllocationOverrideMap);
		}
	}


	private void processInvoices(Date date, Integer[] billingDefinitionIds) {
		// Ensure all applicable definitions are not in applicable state.  Will re-activate any that are shared or dependent on the invoice
		Set<Integer> definitionsCheckedForArchivedState = new HashSet<>();
		for (Integer billingDefinitionId : billingDefinitionIds) {
			transitionBillingDefinitionFromArchived(date, billingDefinitionId, definitionsCheckedForArchivedState);
		}

		// For Shared, they both process together, so keep a list of what has been processed
		List<Integer> definitionsProcessed = new ArrayList<>();

		List<BillingInvoice> invoiceList = getCurrentInvoiceList(date, billingDefinitionIds);

		List<Integer> accountDwRebuiltList = new ArrayList<>();

		for (Integer definitionId : billingDefinitionIds) {
			if (definitionsProcessed.contains(definitionId)) {
				continue;
			}
			BillingInvoice invoice = CollectionUtils.getOnlyElement(BeanUtils.filter(invoiceList, billingInvoice -> billingInvoice.getBillingDefinition().getId(), definitionId));
			Assertions.assertNotNull(invoice, "Missing Valid Invoice for Definition ID [" + definitionId + "] on [" + DateUtils.fromDateShort(date) + "].");

			rebuildDataWarehouseForInvoice(invoice, accountDwRebuiltList);

			if (invoice.getInvoiceGroup() != null) {
				BillingInvoiceGroup group = this.billingInvoiceService.getBillingInvoiceGroup(invoice.getInvoiceGroup().getId());
				transitionInvoiceGroup(group);
				for (BillingInvoice child : group.getInvoiceList()) {
					definitionsProcessed.add((child.getBillingDefinition().getId()));
				}
			}
			else {
				transitionInvoice(invoice);
				definitionsProcessed.add(definitionId);
			}
		}
	}


	private void rebuildDataWarehouseForInvoice(BillingInvoice invoice, List<Integer> accountsProcessedList) {
		log.info("Rebuilding the Data Warehouse for necessary accounts...");

		BillingInvoiceDetailAccountSearchForm searchForm = new BillingInvoiceDetailAccountSearchForm();
		if (invoice.getInvoiceGroup() != null) {
			searchForm.setInvoiceGroupId(invoice.getInvoiceGroup().getId());
		}
		else {
			searchForm.setInvoiceId(invoice.getId());
		}

		// To save time , only Rebuild for those that actually need it - can determine this by the bean type names as we know those that would use snapshots
		List<BillingInvoiceDetailAccount> list = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.billingInvoiceService.getBillingInvoiceDetailAccountList(searchForm), "investmentAccount.id|billingDefinitionInvestmentAccount.billingBasisValuationType.calculatorBean.type.name");
		for (BillingInvoiceDetailAccount da : CollectionUtils.getIterable(list)) {
			if (!accountsProcessedList.contains(da.getInvestmentAccount().getId())) {
				if (da.getBillingDefinitionInvestmentAccount() != null) {
					BillingBasisValuationType vt = da.getBillingDefinitionInvestmentAccount().getBillingBasisValuationType();
					if (vt != null && vt.getCalculatorBean() != null) {
						String beanType = vt.getCalculatorBean().getType().getName();
						if ("Billing Basis Market Value Calculator".equals(beanType) || "Billing Basis Notional Calculator".equals(beanType)) {

							// Also check if DW records exist on the start date, then DW had previously been rebuilt so not necessary
							// Will speed up subsequent tests after first run after DW is restored
							AccountingPositionSnapshotSearchForm snapshotSearchForm = new AccountingPositionSnapshotSearchForm();
							snapshotSearchForm.setClientInvestmentAccountId(da.getInvestmentAccount().getId());
							// If it's the last day of the month - move forward to the next weekday - we keep snapshots on the last day of the month
							Date startDate = invoice.getBillingBasisStartDate();
							if (DateUtils.isLastDayOfMonth(startDate)) {
								startDate = DateUtils.addWeekDays(startDate, 1);
							}
							snapshotSearchForm.setSnapshotDate(startDate);
							if (CollectionUtils.isEmpty(
									RequestOverrideHandler.serviceCallWithOverrides(() -> this.dwAnalyticsService.getAccountingPositionSnapshotList(snapshotSearchForm),
											new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_PROPERTIES_ROOT_PARAMETER, "data"),
											new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_PROPERTIES_PARAMETER, "id")
									)
							)) {
								DwRebuildCommand command = new DwRebuildCommand();
								command.setClientAccountId(da.getInvestmentAccount().getId());
								command.setStartSnapshotDate(invoice.getBillingBasisStartDate());
								command.setEndSnapshotDate(invoice.getBillingBasisEndDate());
								command.setSynchronous(true);
								this.dwRebuildService.rebuildAccountingSnapshots(command);
								accountsProcessedList.add(da.getInvestmentAccount().getId());
							}
						}
					}
				}
			}
		}
	}


	private List<BillingInvoice> getCurrentInvoiceList(Date date, Integer[] billingDefinitionIds) {
		BillingInvoiceSearchForm searchForm = new BillingInvoiceSearchForm();
		searchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CANCELED);
		searchForm.setInvoiceDate(date);
		searchForm.setBillingDefinitionIds(billingDefinitionIds);
		return this.billingInvoiceService.getBillingInvoiceList(searchForm);
	}


	private void transitionInvoiceGroup(BillingInvoiceGroup group) {
		String nextStateName = getNextWorkflowStateName(group.getWorkflowState());
		if (nextStateName != null) {
			for (BillingInvoice invoice : group.getInvoiceList()) {
				ignoreViolations(invoice);
			}
			if (StringUtils.isEqual("Approved by Admin", nextStateName)) {
				transitionAsSecondUser("BillingInvoiceGroup", group, nextStateName);
			}
			else {
				BillingInvoiceGroup toTransition = group;
				RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.workflowTransitionService.executeWorkflowTransition("BillingInvoiceGroup", toTransition.getId(),
						this.workflowDefinitionService.getWorkflowStateByName(toTransition.getWorkflowState().getWorkflow().getId(), nextStateName).getId()), 2); // must be 2 for state id
			}
			group = this.billingInvoiceService.getBillingInvoiceGroup(group.getId());
			transitionInvoiceGroup(group);
		}
	}


	private void transitionBillingDefinitionFromArchived(Date date, Integer billingDefinitionId, Set<Integer> definitionsChecked) {
		if (!definitionsChecked.contains(billingDefinitionId)) {
			definitionsChecked.add(billingDefinitionId);

			BillingDefinition billingDefinition = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.billingDefinitionService.getBillingDefinition(billingDefinitionId), 3);
			// If Billing Definition is in Archived State - reactivate it
			if (StringUtils.isEqualIgnoreCase(BillingDefinition.ARCHIVED_WORKFLOW_STATE_NAME, billingDefinition.getWorkflowState().getName())) {
				this.workflowTransitionService.executeWorkflowTransition(BillingDefinition.BILLING_DEFINITION_TABLE_NAME, billingDefinition.getId(), this.billingDefinitionApprovedWorkflowStateId);
			}

			// Note: This does not check shared because the test should be passing in the ids of the shared definitions

			// Check all dependencies
			BillingScheduleBillingDefinitionDependencySearchForm searchForm = new BillingScheduleBillingDefinitionDependencySearchForm();
			searchForm.setPrerequisiteBillingDefinitionId(billingDefinitionId);

			List<BillingScheduleBillingDefinitionDependency> dependencyList = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.billingDefinitionService.getBillingScheduleBillingDefinitionDependencyList(searchForm), "billingSchedule.billingDefinition.id");
			for (BillingScheduleBillingDefinitionDependency definitionDependency : CollectionUtils.getIterable(dependencyList)) {
				transitionBillingDefinitionFromArchived(date, definitionDependency.getBillingSchedule().getBillingDefinition().getId(), definitionsChecked);
			}
		}
	}


	private void transitionInvoice(BillingInvoice invoice) {
		String nextStateName = getNextWorkflowStateName(invoice.getWorkflowState());
		if (nextStateName != null) {
			ignoreViolations(invoice);
			if (StringUtils.isEqual("Approved by Admin", nextStateName)) {
				transitionAsSecondUser("BillingInvoice", invoice, nextStateName);
			}
			else {
				BillingInvoice toTransition = invoice;
				RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.workflowTransitionService.executeWorkflowTransition("BillingInvoice", toTransition.getId(),
						this.workflowDefinitionService.getWorkflowStateByName(toTransition.getWorkflowState().getWorkflow().getId(), nextStateName).getId()), 2); // must be 2 for state id
			}
			invoice = this.billingInvoiceService.getBillingInvoice(invoice.getId());
			transitionInvoice(invoice);
		}
	}


	/**
	 * Switches the user for specific transitions
	 * Needed because the same user cannot Approve and Approve by Admin
	 */
	private void transitionAsSecondUser(String tableName, WorkflowAware workflowAwareBean, String nextWorkflowStateName) {
		this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_3, SecurityGroup.BILLING_ADMIN);
		this.userUtils.addPermissionToUser(ImsTestProperties.TEST_USER_3, "Workflow");
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_3, ImsTestProperties.TEST_USER_3_PASSWORD);

		try {
			// Transition Invoice
			RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.workflowTransitionService.executeWorkflowTransition(tableName, (Integer) workflowAwareBean.getIdentity(),
					this.workflowDefinitionService.getWorkflowStateByName(workflowAwareBean.getWorkflowState().getWorkflow().getId(), nextWorkflowStateName).getId()), 2); // must be 2 for state id
		}
		finally {
			// Clean up - Switch Back to admin user
			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
			// Clean Up All Security Permissions for the test user
			this.userUtils.removeAllPermissionsForUser(ImsTestProperties.TEST_USER_3);
		}
	}


	private void transitionInvoiceByTransitionName(BillingInvoice invoice, String transitionName) {
		if (transitionName != null) {
			ignoreViolations(invoice);
			List<WorkflowTransition> availableTransitionList = this.workflowTransitionService.getWorkflowTransitionNextListByEntity("BillingInvoice", BeanUtils.getIdentityAsLong(invoice));
			for (WorkflowTransition availableTransition : availableTransitionList) {
				if (transitionName.equals(availableTransition.getName())) {
					this.workflowTransitionService.executeWorkflowTransitionByTransition("BillingInvoice", invoice.getId(), availableTransition.getId());
				}
			}
		}
	}


	private String getNextWorkflowStateName(WorkflowState state) {
		if (WorkflowStatus.STATUS_OPEN.equals(state.getStatus().getName())) {
			throw new ValidationException("Cannot test invoice that is currently in an Open status");
		}
		if ("Valid".equals(state.getName())) {
			return "Approved";
		}
		if ("Approved".equals(state.getName())) {
			return "Approved by Admin";
		}
		if ("Approved by Admin".equals(state.getName())) {
			return "Sent";
		}
		if (WorkflowStatus.STATUS_ACTIVE.equals(state.getStatus().getName()) || WorkflowStatus.STATUS_CLOSED.equals(state.getStatus().getName())) {
			return ("Corrected");
		}
		return null;
	}


	private void ignoreViolations(BillingInvoice invoice) {
		if (invoice != null && invoice.getViolationStatus() != null && invoice.getViolationStatus().isUnresolvedViolationsExist()) {
			RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
			searchForm.setLinkedTableName("BillingInvoice");
			searchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(invoice));
			searchForm.setIgnorable(true);
			searchForm.setIgnored(false);
			List<RuleViolation> violationList = this.ruleViolationService.getRuleViolationList(searchForm);
			// Some require notes, so just manually enter notes/ignore each one
			for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
				violation.setIgnored(true);
				violation.setIgnoreNote("Integration Test - Ignoring All Warnings.");
				RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.ruleViolationService.saveRuleViolation(violation), 1);
			}
		}
	}


	private void validateBillingInvoice(BillingInvoice invoice, Map<Integer, String> accountAllocationOverrideMap) {
		validateBillingInvoice(invoice, invoice.getRootParent(), accountAllocationOverrideMap);
	}


	private void validateBillingInvoice(BillingInvoice invoice, BillingInvoice originalInvoice, Map<Integer, String> accountAllocationOverrideMap) {
		if (originalInvoice.getId() == null) {
			// Every time the test is re-ran the invoice is voided and re-generated - with the previous set as the parent.  Once we get beyond the max levels we loose the root parent's id
			// And to prevent memory errors on trying to load too much data, it's better to just restore and re-try
			Assertions.fail("Original Invoice is missing.  This is likely because the test was re-run too many times and now the root parent is beyond 6 levels above.  Restore the database and try re-running the test.");
		}
		// Populate the Detail List on the Original Invoice For Comparisons
		BillingInvoiceDetailSearchForm searchForm = new BillingInvoiceDetailSearchForm();
		searchForm.setInvoiceId(originalInvoice.getId());
		originalInvoice.setDetailList(this.billingInvoiceService.getBillingInvoiceDetailList(searchForm));

		// Validate Invoice Total
		BigDecimal total = invoice.getTotalAmount(); // Don't need to exclude manual adjustments here because system just created the invoice
		BigDecimal originalTotal = getInvoiceTotalExcludingManualAdjustments(originalInvoice);
		ValidationUtils.assertTrue(MathUtils.isLessThanOrEqual(MathUtils.abs(MathUtils.subtract(total, originalTotal)), BigDecimal.valueOf(5)),
				"Expected [" + CoreMathUtils.formatNumberInteger(originalTotal) + "] for " + invoice.getBillingDefinition().getLabel() + " invoice on " + DateUtils.fromDateShort(invoice.getInvoiceDate())
						+ " but was [" + CoreMathUtils.formatNumberInteger(total) + "]");

		Map<Integer, BigDecimal> accountMap = getBillingInvoiceDetailAccountAmountMap(invoice);
		Map<Integer, BigDecimal> originalAccountMap = getBillingInvoiceDetailAccountAmountMap(originalInvoice);

		StringBuilder misMatches = new StringBuilder();

		List<Integer> checkedAccounts = new ArrayList<>();
		for (Map.Entry<Integer, BigDecimal> integerBigDecimalEntry : originalAccountMap.entrySet()) {
			checkedAccounts.add(integerBigDecimalEntry.getKey());
			BigDecimal originalAmount = integerBigDecimalEntry.getValue();
			// If there is an override to expected account allocation
			if (accountAllocationOverrideMap != null && accountAllocationOverrideMap.containsKey(integerBigDecimalEntry.getKey())) {
				originalAmount = StringUtils.isEmpty(accountAllocationOverrideMap.get(integerBigDecimalEntry.getKey())) ? null : new BigDecimal(accountAllocationOverrideMap.get(integerBigDecimalEntry.getKey()));
			}
			BigDecimal newAmount = accountMap.get(integerBigDecimalEntry.getKey());
			if (!MathUtils.isEqual(originalAmount, newAmount)) {
				// Give it a threshold of 5 just like invoices
				if (MathUtils.isGreaterThan(MathUtils.abs(MathUtils.subtract(newAmount, originalAmount)), BigDecimal.valueOf(5))) {
					misMatches.append("Account ID: ").append(integerBigDecimalEntry.getKey()).append(" is expected to be allocated [").append(CoreMathUtils.formatNumberMoney(originalAmount)).append("], and now is allocated [").append(CoreMathUtils.formatNumberMoney(newAmount)).append("].  ");
				}
			}
		}
		// See if there is anything new that wasn't there before
		for (Map.Entry<Integer, BigDecimal> integerBigDecimalEntry : accountMap.entrySet()) {
			if (!checkedAccounts.contains(integerBigDecimalEntry.getKey())) {
				BigDecimal newAmount = integerBigDecimalEntry.getValue();
				if (!MathUtils.isNullOrZero(newAmount)) {
					misMatches.append("Account ID: ").append(integerBigDecimalEntry.getKey()).append(" was NOT allocated anything before and now is allocated [").append(CoreMathUtils.formatNumberMoney(newAmount)).append("].  ");
				}
			}
		}

		if (!StringUtils.isEmpty(misMatches.toString())) {
			throw new ValidationException("Invoice Account Allocation is different for " + invoice.getBillingDefinition().getLabel() + " invoice on "
					+ DateUtils.fromDateShort(invoice.getInvoiceDate()) + misMatches.toString());
		}
	}


	private BigDecimal getInvoiceTotalExcludingManualAdjustments(BillingInvoice invoice) {
		BillingInvoice hydratedInvoice = CollectionUtils.isEmpty(invoice.getDetailList()) ? RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.billingInvoiceService.getBillingInvoice(invoice.getId()), 4) : invoice;
		return BillingAmountCalculatorUtils.getBillingInvoiceTotalAmountCalculated(hydratedInvoice, null);
	}


	private Map<Integer, BigDecimal> getBillingInvoiceDetailAccountAmountMap(BillingInvoice invoice) {
		BillingInvoiceDetailAccountSearchForm searchForm = new BillingInvoiceDetailAccountSearchForm();
		searchForm.setInvoiceId(invoice.getId());
		List<BillingInvoiceDetailAccount> detailList = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.billingInvoiceService.getBillingInvoiceDetailAccountList(searchForm), "investmentAccount.id|billingAmount|invoiceDetail.manual");
		Map<Integer, BigDecimal> accountMap = new HashMap<>();
		for (BillingInvoiceDetailAccount billingAccount : CollectionUtils.getIterable(detailList)) {
			// Skip Manual Adjustment Records on the Invoice
			if (!billingAccount.getInvoiceDetail().isManual()) {
				BigDecimal amount = accountMap.get(billingAccount.getInvestmentAccount().getId());
				amount = MathUtils.add(amount, billingAccount.getBillingAmount());
				accountMap.put(billingAccount.getInvestmentAccount().getId(), amount);
			}
		}
		return accountMap;
	}
}
