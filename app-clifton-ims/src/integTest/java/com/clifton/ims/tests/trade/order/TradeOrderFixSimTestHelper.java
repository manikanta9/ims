package com.clifton.ims.tests.trade.order;

import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.fix.message.FixMessage;
import com.clifton.fix.quickfix.util.QuickFixUtilServiceImpl;
import com.clifton.ims.tests.utility.PeriodicRepeatingExecutor;
import com.clifton.investment.account.mapping.InvestmentAccountMappingPurposeBuilder;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupItem;
import com.clifton.investment.setup.group.InvestmentGroupMatrix;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.search.InvestmentGroupItemSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.builder.TradeDestinationBuilder;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationMappingBuilder;
import com.clifton.trade.destination.TradeDestinationMappingConfiguration;
import com.clifton.trade.destination.TradeDestinationMappingConfigurationBuilder;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocation;
import com.clifton.trade.order.allocation.TradeOrderAllocationService;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.allocation.search.TradeOrderAllocationSearchForm;
import com.clifton.trade.order.fix.order.TradeOrderFixOrderService;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;

import static com.clifton.core.util.date.DateUtils.FIX_DATE_FORMAT_INPUT;


/**
 * This class is use with the TradeOrderFixSimTests class and provides trade execution support methods.  These were once part of the TradeOrderFixSimTests class, but have been
 * moved to a separate class.
 *
 * @author davidi
 */

@Component
public class TradeOrderFixSimTestHelper {

	private String dataSourceDirectory;
	@Value("${fix.sessionSuffix}")
	private String fixSessionSuffix;

	// How many times to poll for an Order / Allocation state transition.
	@Value("${fix.fixSimPollCount}")
	private int fisSimPollCount;
	// How long to wait, in seconds, between polling cycles.
	@Value("${fix.fixSimPollIntervalSeconds}")
	private int fixSimPollIntervalSeconds;

	private static final String FIX_MSG_TYPE_NEW_ORDER = "D";
	private static final String FIX_MSG_TYPE_EXECUTION_REPORT = "8";
	private static final String FIX_MSG_TYPE_ALLOC_INSTR = "J";
	private static final String FIX_MSG_TYPE_ALLOC_ACK = "P";
	private static final String FIX_MSG_TYPE_ALLOC_REPORT = "AS";

	public static final Set<Integer> COMPARISON_IGNORE_FIELDS_D_MESSAGE = CollectionUtils.createHashSet(9, 10, 34, 50, 52, 57, 58, 60);
	public static final Set<Integer> COMPARISON_IGNORE_FIELDS_J_MESSAGE = CollectionUtils.createHashSet(9, 10, 34, 50, 52, 58, 60);

	@Resource
	private InvestmentGroupService investmentGroupService;
	@Resource
	private TradeDestinationService tradeDestinationService;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private TradeOrderService tradeOrderService;
	@Resource
	private TradeOrderAllocationService tradeOrderAllocationService;
	@Resource
	private TradeOrderFixOrderService tradeOrderFixOrderService;
	@Resource
	private FixSimHelper fixSimHelper;

	private final QuickFixUtilServiceImpl quickFixUtilService = new QuickFixUtilServiceImpl();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Convenience method to consturct a TestContext and automatically populate its comparison ignore fields.
	 *
	 * @return a new TestContext
	 */
	public TestContext createTestContext(String instance, String sessionName ) {
		TestContext testContext = new TestContext();
		testContext.setComparisonIgnoreFieldsNewOrder(COMPARISON_IGNORE_FIELDS_D_MESSAGE);
		testContext.setComparisonIgnoreFieldsAllocInstruction(COMPARISON_IGNORE_FIELDS_J_MESSAGE);
		testContext.setFixSimInstance(instance);
		testContext.setFixSimSessionName(sessionName);
		return testContext;
	}


	/**
	 * Executes a Trade and returns the resulting TradeOrder
	 *
	 * @param trade        - trade to execute
	 * @param groupingType - how orders are to be grouped (default, execute separately, etc.)
	 * @param testContext  - an instance of TestContext containing test-related data.
	 * @return returns the TradeOrder resulting from the trade transaction.
	 */
	public TradeOrder executeFixTradeOrder(Trade trade, TradeOrderGroupingTypes groupingType, com.clifton.ims.tests.trade.order.TestContext testContext) {
		return executeFixTradeOrder(new Trade[]{trade}, groupingType, testContext);
	}


	/**
	 * Executes the Trades and returns the resulting TradeOrder
	 *
	 * @param trades       - list of trades for the order
	 * @param groupingType - how orders are to be grouped (default, execute separately, etc.)
	 * @param testContext  - an instance of TestContext containing test-related data.
	 * @return returns the TradeOrder resulting from the trade transaction.
	 */
	public TradeOrder executeFixTradeOrder(Trade[] trades, TradeOrderGroupingTypes groupingType, com.clifton.ims.tests.trade.order.TestContext testContext) {
		try {
			ArrayList<Integer> tradeIds = new ArrayList<>();
			for (Trade trade : trades) {
				tradeIds.add(trade.getId());
			}

			this.tradeOrderService.executeTradeOrderTradeList(tradeIds.toArray(new Integer[0]), groupingType, false);
			// wait for NewOrderSingle to be sent and saved in Fix DB.
			final int tradeId = tradeIds.get(0);
			awaitForStateChange(() -> {
				TradeOrder tradeOrder = getTradeOrderForStatusVerification(tradeId);
				boolean returnVal = false;
				if (tradeOrder != null && tradeOrder.getStatus() != null && tradeOrder.getStatus().getOrderStatus() == TradeOrderStatuses.SENT) {
					Map<String, List<String>> results = getFixMessageTextByType(tradeOrder, CollectionUtils.createList(FIX_MSG_TYPE_NEW_ORDER));
					if (CollectionUtils.getSize(results.get(FIX_MSG_TYPE_NEW_ORDER)) == 1) {
						returnVal = true;
					}
				}
				return returnVal;
			});

			TradeOrder trdOrder = this.tradeOrderService.getTradeOrderByTrade(tradeId);
			if (trdOrder.getExternalIdentifier() == null) {
				throw new RuntimeException("Order did not receive an external identifier");
			}
			if (testContext.getExpectedTradeOrderStatus() == TradeOrderStatuses.SENT && trdOrder.getStatus().getOrderStatus() == TradeOrderStatuses.SENT) {
				return trdOrder;
			}

			// update execution reports with new identifier and send to FixSim
			updateOrderAndAllocationID(trdOrder, testContext);
			testContext.addFixMessageReplacement(ReplacementMapKeys.EXECUTION_REPORT, testContext.getTemplateFixOrderId(), testContext.getFixOrderId());
			List<String> updatedFixExecReportMessagesList = replaceMessageFieldValues(testContext.getFixMessageMap().get(FIX_MSG_TYPE_EXECUTION_REPORT), testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ORDER_EXECUTION));
			updatedFixExecReportMessagesList = replaceMessageFieldValues(updatedFixExecReportMessagesList, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.EXECUTION_REPORT));

			if (CollectionUtils.getSize(updatedFixExecReportMessagesList) == 0) {
				return trdOrder;
			}
			getFixSimHelper().uploadMessagesToFixSim(updatedFixExecReportMessagesList, testContext.getFixSimInstance(), testContext.getFixSimSessionName());

			// wait for trade order to transition to the specified state (e.g. Filled)
			awaitForStateChange(() -> {
				TradeOrder tradeOrder = getTradeOrderForStatusVerification(tradeId);
				return (tradeOrder != null && tradeOrder.getStatus() != null && tradeOrder.getStatus().getOrderStatus() == testContext.getExpectedTradeOrderStatus());
			});

			// Handle sending of allocation ACK's and AllocationReport
			if (testContext.getExpectedAllocationAckStatus() != null) {
				// wait for AllocationInstruction to be sent and saved in Fix DB.
				awaitForStateChange(() -> {
					TradeOrder tradeOrder = getTradeOrderForStatusVerification(tradeId);
					boolean returnVal = false;
					if (tradeOrder != null && tradeOrder.getAllocationStatus() != null && tradeOrder.getAllocationStatus().getAllocationStatus() == TradeOrderAllocationStatuses.SENT) {
						Map<String, List<String>> results = getFixMessageTextByType(tradeOrder, CollectionUtils.createList(FIX_MSG_TYPE_ALLOC_INSTR));
						if (CollectionUtils.getSize(results.get(FIX_MSG_TYPE_ALLOC_INSTR)) == 1) {
							returnVal = true;
						}
					}
					return returnVal;
				});

				// update ACK messages and send to FixSim
				updateOrderAndAllocationID(this.tradeOrderService.getTradeOrderByTrade(tradeId), testContext);
				testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ACK, testContext.getTemplateFixAllocationId(), testContext.getFixAllocationId());

				List<String> updatedAckMessages = replaceMessageFieldValues(testContext.getFixMessageMap().get(FIX_MSG_TYPE_ALLOC_ACK), testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ALLOC_ALL));
				updatedAckMessages = replaceMessageFieldValues(updatedAckMessages, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ALLOC_ACK));
				getFixSimHelper().uploadMessagesToFixSim(updatedAckMessages, testContext.getFixSimInstance(), testContext.getFixSimSessionName());

				awaitForStateChange(() -> {
					TradeOrder tradeOrder = getTradeOrderForStatusVerification(tradeId);
					return (tradeOrder != null && tradeOrder.getAllocationStatus() != null && tradeOrder.getAllocationStatus().getAllocationStatus() == testContext.getExpectedAllocationAckStatus());
				});

				if (testContext.getExpectedAllocationStatus() != null) {
					// Update allocation identifier and price in template
					String allocationReportTemplate = CollectionUtils.getOnlyElementStrict(testContext.getFixMessageMap().get(FIX_MSG_TYPE_ALLOC_REPORT));

					testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_REPORT, testContext.getTemplateFixOrderId(), testContext.getFixOrderId());
					testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_REPORT, testContext.getTemplateFixAllocationId(), testContext.getFixAllocationId());

					String allocationReportMessage = replaceMessageFieldValues(allocationReportTemplate, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ALLOC_ALL));
					allocationReportMessage = replaceMessageFieldValues(allocationReportMessage, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ALLOC_REPORT));

					getFixSimHelper().uploadMessageToFixSim(allocationReportMessage, testContext.getFixSimInstance(), testContext.getFixSimSessionName());

					awaitForStateChange(() -> {
						TradeOrder tradeOrder = getTradeOrderForStatusVerification(tradeId);
						return (tradeOrder.getAllocationStatus() != null && tradeOrder.getAllocationStatus().getAllocationStatus() == testContext.getExpectedAllocationStatus());
					});
				}
			}

			TradeOrder tradeOrder = this.tradeOrderService.getTradeOrderByTrade(tradeId);
			validateMessages(tradeOrder, testContext);
			return tradeOrder;
		}
		catch (Exception exc) {
			if (exc instanceof InterruptedException) {
				Thread.currentThread().interrupt();
			}
			LogUtils.error(TradeOrderFixSimTestHelper.class, exc.getMessage());
			return null;
		}
	}


	/**
	 * Gets the OrderID and AllocationID given the TradeOrder and CurrentDate and updates the OrderId and AllocationId in the TestContext.
	 */
	public void updateOrderAndAllocationID(TradeOrder tradeOrder, com.clifton.ims.tests.trade.order.TestContext testContext) {
		Assertions.assertNotNull(tradeOrder, "TradeOrder entity cannot be null.");
		Assertions.assertNotNull(testContext, "TestContext entity cannot be null.");
		Assertions.assertNotNull(testContext.getTestDate(), "The test date in the TestContext cannot be null.");
		Date fixMessageDate = DateUtils.clearTime(testContext.getTestDate());

		TradeOrderAllocationSearchForm tradeOrderSearchForm = new TradeOrderAllocationSearchForm();
		tradeOrderSearchForm.setTradeOrderId(tradeOrder.getId());
		List<TradeOrderAllocation> tradeOrderAllocationList = this.tradeOrderAllocationService.getTradeOrderAllocationList(tradeOrderSearchForm);
		TradeOrderAllocation tradeOrderAllocation = CollectionUtils.getOnlyElement(tradeOrderAllocationList);

		testContext.setFixOrderId("O_" + tradeOrder.getId() + "_" + DateUtils.fromDate(fixMessageDate, FIX_DATE_FORMAT_INPUT));
		if (tradeOrderAllocation != null) {
			testContext.setFixAllocationId("A_" + tradeOrderAllocation.getId() + "_" + DateUtils.fromDate(fixMessageDate, FIX_DATE_FORMAT_INPUT));
		}
	}


	/**
	 * Constructs a map of fix messages in text form for a given tradeOrder, keyed by message type tags (e.g. D, J). The typeTagList will restrict the
	 * contents of the map to just the specified message types.
	 */
	public Map<String, List<String>> getFixMessageTextByType(TradeOrder tradeOrder, List<String> typeTagList) {
		if (tradeOrder.getExternalIdentifier() == null) {
			return Collections.emptyMap();
		}
		Map<String, List<String>> messagesByType = new HashMap<>();
		List<FixMessage> fixMessageList = this.tradeOrderFixOrderService.getTradeOrderFixMessageListForOrder(tradeOrder.getExternalIdentifier());
		for (FixMessage message : CollectionUtils.getIterable(fixMessageList)) {
			if (CollectionUtils.isEmpty(typeTagList) || typeTagList.contains(message.getType().getMessageTypeTagValue())) {
				List<String> messageTextList = messagesByType.computeIfAbsent(message.getType().getMessageTypeTagValue(), k -> new ArrayList<>());
				messageTextList.add(message.getText());
			}
		}
		return messagesByType;
	}


	/**
	 * Validates the D and J message contents by comparing the field data in the sent messages to the field data in the template messages.
	 * TagTypeList should contain "D" and "J".
	 */
	public void validateMessages(TradeOrder tradeOrder, com.clifton.ims.tests.trade.order.TestContext testContext) {
		Assertions.assertNotNull(tradeOrder, "TradeOrder entity cannot be null.");
		Assertions.assertNotNull(testContext, "TestContext cannot be null.");
		Map<String, List<String>> savedFixMessages = getFixMessageTextByType(tradeOrder, CollectionUtils.createList(FIX_MSG_TYPE_NEW_ORDER, FIX_MSG_TYPE_ALLOC_INSTR));
		String template = CollectionUtils.getOnlyElementStrict(testContext.getFixMessageMap().get(FIX_MSG_TYPE_NEW_ORDER));
		template = StringUtils.replace(template, testContext.getTemplateFixOrderId(), testContext.getFixOrderId());
		template = replaceMessageFieldValues(template, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ORDER));
		template = replaceMessageFieldValues(template, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ORDER_EXECUTION));

		String savedMessage = CollectionUtils.getOnlyElementStrict(savedFixMessages.get(FIX_MSG_TYPE_NEW_ORDER));
		compareMessages(template, savedMessage, FIX_MSG_TYPE_NEW_ORDER, testContext);

		if (testContext.getExpectedAllocationStatus() != null) {
			template = CollectionUtils.getOnlyElementStrict(testContext.getFixMessageMap().get(FIX_MSG_TYPE_ALLOC_INSTR));
			template = StringUtils.replace(template, testContext.getTemplateFixOrderId(), testContext.getFixOrderId());
			template = StringUtils.replace(template, testContext.getTemplateFixAllocationId(), testContext.getFixAllocationId());
			template = replaceMessageFieldValues(template, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ALLOC_INSTRUCTION));
			template = replaceMessageFieldValues(template, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.ALLOC_ALL));

			savedMessage = CollectionUtils.getOnlyElementStrict(savedFixMessages.get(FIX_MSG_TYPE_ALLOC_INSTR));
			compareMessages(template, savedMessage, FIX_MSG_TYPE_ALLOC_INSTR, testContext);
		}
	}


	public void compareMessages(String template, String messageText, String messageTypeTag, com.clifton.ims.tests.trade.order.TestContext testContext) {
		List<String> errorList = new ArrayList<>();
		Set<Integer> ignoreTags = messageTypeTag.equals(FIX_MSG_TYPE_NEW_ORDER) ? testContext.getComparisonIgnoreFieldsNewOrder() : testContext.getComparisonIgnoreFieldsAllocInstruction();
		Map<Integer, List<String>> templateFieldMap = getFixMessageFieldMap(template, ignoreTags);
		Map<Integer, List<String>> messageFieldMap = getFixMessageFieldMap(messageText, ignoreTags);

		for (Map.Entry<Integer, List<String>> entry : CollectionUtils.getIterable(templateFieldMap.entrySet())) {
			if (!messageFieldMap.containsKey(entry.getKey())) {
				errorList.add("\"" + messageTypeTag + "\" Message is missing field " + entry.getKey() + " which exists in the message template.");
				continue;
			}
			List<String> messageFieldList = messageFieldMap.get(entry.getKey());
			// if values are string-delimited lists, convert to sets for comparisons, as list order may vary.
			if (!CollectionUtils.isEmpty(testContext.getFixTagsWithListValues()) && testContext.getFixTagsWithListValues().contains(entry.getKey())) {
				Assertions.assertEquals(CollectionUtils.getSize(entry.getValue()), CollectionUtils.getSize(messageFieldList), "The message has an unexpected number of repeating fields.");
				for (int index = 0; index < CollectionUtils.getSize(entry.getValue()); ++index) {
					String templateField = entry.getValue().get(index);
					String messageField = messageFieldList.get(index);
					// remove the "tag#=" part of the string
					templateField = templateField.replace(entry.getKey() + "=", "");
					messageField = messageField.replace(entry.getKey() + "=", "");
					Set<String> valueSetExpected = getSetFromDelimitedString(templateField, testContext.getValueListDelimiter());
					Set<String> valueSetMessage = getSetFromDelimitedString(messageField, testContext.getValueListDelimiter());
					if (!valueSetExpected.equals(valueSetMessage)) {
						errorList.add("Values for tag: " + entry.getKey() + " do not match expected values in the \"" + messageTypeTag + "\" message. Expected: " + entry.getValue() + "  Message Field Value: " + messageFieldList);
					}
				}
			}
			else if (!entry.getValue().containsAll(messageFieldList) || !messageFieldList.containsAll(entry.getValue())) {
				errorList.add("Values for tag: " + entry.getKey() + " do not match expected values in the \"" + messageTypeTag + "\" message. Expected: " + entry.getValue() + "  Message Field Value: " + messageFieldList);
			}
		}

		if (messageFieldMap.size() > templateFieldMap.size()) {
			for (Map.Entry<Integer, List<String>> entry : messageFieldMap.entrySet()) {
				if (!templateFieldMap.containsKey(entry.getKey())) {
					errorList.add("The \"" + messageTypeTag + "\"  message contains tag " + entry.getValue() + " which does not exist in the template.");
				}
			}
		}

		Assertions.assertTrue(CollectionUtils.isEmpty(errorList), "Errors found in comparison between template and message (type: \"" + messageTypeTag + "\"): " + errorList);
	}


	private void awaitForStateChange(Callable<Boolean> pollingTask) throws InterruptedException {
		PeriodicRepeatingExecutor.await(pollingTask, getFisSimPollCount(), getFixSimPollIntervalSeconds());
	}


	private TradeOrder getTradeOrderForStatusVerification(Integer tradeId) {
		return RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.tradeOrderService.getTradeOrderByTrade(tradeId),
				"id|externalIdentifier|status.orderStatus|allocationStatus.allocationStatus");
	}


	private Map<Integer, List<String>> getFixMessageFieldMap(String message, Set<Integer> ignoreTags) {
		Map<Integer, List<String>> fixMessageFieldMap = new HashMap<>();

		for (String field : StringUtils.split(message, "\u0001")) {
			String[] tagAndValue = StringUtils.split(field, "=");
			Assertions.assertTrue(tagAndValue.length > 1, "Invalid field tag encountered during parsing of Fix Message: " + field);
			Integer tag = Integer.parseInt(tagAndValue[0]);
			if (!ignoreTags.contains(tag)) {
				List<String> fieldList = fixMessageFieldMap.computeIfAbsent(tag, k -> new ArrayList<>());
				fieldList.add(field);
			}
		}

		return fixMessageFieldMap;
	}


	/**
	 * Takes a text string containing delimited values and extracts those values into a
	 */
	private Set<String> getSetFromDelimitedString(String text, String delimiter) {
		return new HashSet<>(StringUtils.splitOnDelimiter(text, delimiter));
	}


	/**
	 * Updates the FixMessage with new values from a substitution map
	 */
	private String replaceMessageFieldValues(String message, Map<String, String> substitutionMap) {
		String updatedMessage = message;
		for (Map.Entry<String, String> entry : CollectionUtils.getIterable(substitutionMap.entrySet())) {
			updatedMessage = StringUtils.replace(updatedMessage, entry.getKey(), entry.getValue());
		}
		return updatedMessage;
	}


	/**
	 * Updates the FixMessages in the list with new values from a substitution map
	 */
	private List<String> replaceMessageFieldValues(List<String> messageList, Map<String, String> substitutionMap) {
		List<String> updatedMessageList = new ArrayList<>();
		for (String message : CollectionUtils.getIterable(messageList)) {
			updatedMessageList.add(replaceMessageFieldValues(message, substitutionMap));
		}
		return updatedMessageList;
	}


	/**
	 * Add fix messages to ordered message lists referenced via the message type code, and stores the
	 * resulting map in TestContext.FixMessageMap.
	 */
	public void updateFixMessageMap(String fileName, TestContext testContext) {
		Map<String, List<String>> fixMessageMap = new HashMap<>();
		String delimiter = getDataSourceDirectory().endsWith("/") ? "" : "/";
		final String dataSourcePath = new ClassPathResource(getDataSourceDirectory() + delimiter + fileName).getPath();
		ResourceLoader loader = new DefaultResourceLoader();

		List<String> rawMessageList;
		try {
			rawMessageList = Arrays.asList(FileUtils.readFileToString(loader.getResource(dataSourcePath).getFile()).split("\r?\n"));
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}

		rawMessageList = replaceMessageFieldValues(rawMessageList, testContext.getFixMessageReplacementMap().get(ReplacementMapKeys.GLOBAL));

		for (String fixMessage : rawMessageList) {
			String messageCode = this.quickFixUtilService.getMessageTypeCode(fixMessage);
			List<String> messageList = fixMessageMap.computeIfAbsent(messageCode, key -> new ArrayList<>());
			messageList.add(fixMessage);
		}

		testContext.setFixMessageMap(fixMessageMap);
	}


	public String getMaturityMonthYear(InvestmentSecurity security) {
		Integer yearCode = InvestmentUtils.getFutureYear(security);
		Integer monthCode = InvestmentUtils.getFutureMonth(security);
		if (monthCode != null) {
			return yearCode + "" + CoreMathUtils.formatNumber(new BigDecimal(monthCode), "00");
		}
		return null;
	}


	public TradeDestination createEMSXDestination() {
		TradeDestination tradeDestination = this.tradeDestinationService.getTradeDestinationByName("JUNIT EMSX Ticket");
		if (tradeDestination == null) {
			tradeDestination = TradeDestinationBuilder.createEMSXDestination()
					.withId(null)
					.withName("JUNIT EMSX Ticket")
					.toTradeDestination();
			this.tradeDestinationService.saveTradeDestination(tradeDestination);

			TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = TradeDestinationMappingConfigurationBuilder.createBloombergEMSXMappingConfigurationBuilder()
					.withId(null)
					.withName("JUNIT EMSX Ticket Configuration")
					.withDescription("JUNIT EMSX Ticket Configuration")
					.toTradeDestinationMappingConfiguration();
			this.tradeDestinationService.saveTradeDestinationMappingConfiguration(tradeDestinationMappingConfiguration);

			TradeDestinationMapping tradeDestinationMapping = TradeDestinationMappingBuilder.createGoldmanFutures()
					.withId(null)
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withFixPostTradeAllocationTopAccountNumber(null)
					.withOrderAccountMappingPurpose(InvestmentAccountMappingPurposeBuilder.createEMSXTradingAccountNumberBuilder().toInvestmentAccountMappingPurpose())
					.withStartDate(DateUtils.toDate("04/18/2018"))
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createMorganStocks()
					.withId(null)
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withFixPostTradeAllocationTopAccountNumber(null)
					.withOrderAccountMappingPurpose(InvestmentAccountMappingPurposeBuilder.createEMSXTradingAccountNumberBuilder().toInvestmentAccountMappingPurpose())
					.withStartDate(DateUtils.toDate("04/18/2018"))
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);
		}
		return tradeDestination;
	}


	public TradeDestination createBloombergDestination() {
		TradeDestination tradeDestination = this.tradeDestinationService.getTradeDestinationByName("JUNIT Bloomberg Ticket");
		if (tradeDestination == null) {
			tradeDestination = TradeDestinationBuilder.createBloombergDestination()
					.withId(null)
					.withName("JUNIT Bloomberg Ticket")
					.toTradeDestination();
			this.tradeDestinationService.saveTradeDestination(tradeDestination);

			TradeDestinationMappingConfiguration tradeDestinationMappingConfiguration = TradeDestinationMappingConfigurationBuilder.createBloombergFITCDSConfigurationBuilder()
					.withId(null)
					.withName("JUNIT Bloomberg FIT CDS Configuration")
					.withDescription("JUNIT Setting for trading CDS on Bloomberg CDS")
					.withFixSendClearingFirmLEIWithAllocation(true)
					.withSendRestrictiveBrokerList(true)
					.withFixTradeGroupingAllowed(true)
					.toTradeDestinationMappingConfiguration();
			this.tradeDestinationService.saveTradeDestinationMappingConfiguration(tradeDestinationMappingConfiguration);

			TradeDestinationMapping tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withFixPostTradeAllocationTopAccountNumber("PPA")
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			// executing broker mappings
			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withFixPostTradeAllocationTopAccountNumber("PPA")
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createMorganStanley().toBusinessCompany())
					.toTradeDestinationMapping();
			tradeDestinationMapping.setEndDate(new Date());
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createCreditSuisse().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createJPMorganChase().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createBarclaysCapitolInc().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createBNPParibasSecCorp().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createCitigroupGlobalMarkets().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createDeutscheBankSecurities().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createSocieteGenerale().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createBankOfAmericaMerrillLynch().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createStifelNicolaus().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);

			tradeDestinationMapping = TradeDestinationMappingBuilder.createClearedCDS()
					.withTradeDestination(tradeDestination)
					.withMappingConfiguration(tradeDestinationMappingConfiguration)
					.withExecutingBrokerCompany(BusinessCompanyBuilder.createCitadelSecurities().toBusinessCompany())
					.toTradeDestinationMapping();
			this.tradeDestinationService.saveTradeDestinationMapping(tradeDestinationMapping);
		}
		return tradeDestination;
	}


	public String getExchangeMICCode(Trade trade) {
		InvestmentExchange exchange = InvestmentUtils.getSecurityExchange(trade.getInvestmentSecurity());
		return exchange.getMarketIdentifierCode();
	}


	public void updateGroupAssociation() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("IBM");
		InvestmentGroup group = this.investmentGroupService.getInvestmentGroupByName("FIX Contracts");
		ValidationUtils.assertNotNull(group, "Investment Group not found [FIX Contracts]");
		List<InvestmentGroup> groups = this.investmentGroupService.getInvestmentGroupListForSecurity(security.getId());

		if (groups.stream().noneMatch(g -> Objects.equals(g.getId(), group.getId()))) {
			InvestmentGroupItemSearchForm searchForm = new InvestmentGroupItemSearchForm();
			searchForm.setGroupId(group.getId());
			searchForm.setName("All Instruments");
			searchForm.setLimit(1);
			Optional<InvestmentGroupItem> item = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.investmentGroupService.getInvestmentGroupItemList(searchForm).stream().findFirst(), "id");
			ValidationUtils.assertTrue(item.isPresent(), "Investment Group Item not found [All Instruments] for Investment Group [FIX Contracts]");
			InvestmentGroupMatrix investmentGroupMatrix = new InvestmentGroupMatrix();
			item.ifPresent(investmentGroupMatrix::setGroupItem);
			investmentGroupMatrix.setInstrument(security.getInstrument());
			this.investmentGroupService.saveInvestmentGroupMatrix(investmentGroupMatrix);
			this.investmentGroupService.rebuildInvestmentGroupItemInstrumentListByGroup(group.getId());
		}
	}


	/**
	 * Applies default session suffix to the provided text parameter, and returns the suffixed string.
	 */
	public String applyDefaultSessionSuffix(String text) {
		return text + getFixSessionSuffix();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSimHelper getFixSimHelper() {
		return this.fixSimHelper;
	}


	public void setFixSimHelper(FixSimHelper fixSimHelper) {
		this.fixSimHelper = fixSimHelper;
	}


	public String getDataSourceDirectory() {
		return this.dataSourceDirectory;
	}


	public void setDataSourceDirectory(String dataSourceDirectory) {
		this.dataSourceDirectory = dataSourceDirectory;
	}


	public String getFixSessionSuffix() {
		return this.fixSessionSuffix;
	}


	public void setFixSessionSuffix(String fixSessionSuffix) {
		this.fixSessionSuffix = fixSessionSuffix;
	}


	public int getFisSimPollCount() {
		return this.fisSimPollCount;
	}


	public void setFisSimPollCount(int fisSimPollCount) {
		this.fisSimPollCount = fisSimPollCount;
	}


	public int getFixSimPollIntervalSeconds() {
		return this.fixSimPollIntervalSeconds;
	}


	public void setFixSimPollIntervalSeconds(int fixSimPollIntervalSeconds) {
		this.fixSimPollIntervalSeconds = fixSimPollIntervalSeconds;
	}
}







