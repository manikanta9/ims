package com.clifton.ims.tests.accounting.balance.removal;

import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.gl.balance.removal.AccountingBalanceRemovalCommand;
import com.clifton.accounting.gl.balance.removal.AccountingBalanceRemovalService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class AccountingBalanceRemovalServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingBalanceRemovalService accountingBalanceRemovalService;

	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingBalanceRemoval_NoBalance() {
		AccountInfo account = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		AccountingBalanceRemovalCommand removalCommand = new AccountingBalanceRemovalCommand();
		removalCommand.setHoldingInvestmentAccountId(account.getHoldingAccount().getId());
		removalCommand.setRemovalDate(new Date());
		removalCommand.setRemovalNote("Test");
		removalCommand.setDistributePositions(true);
		removalCommand.setDistributeNonPositionAssetsAndLiabilities(true);
		Status removalStatus = this.accountingBalanceRemovalService.processAccountingBalanceRemoval(removalCommand);
		validateRemovalStatus(removalStatus, false, CollectionUtils.createList("Skipped: No positions in the selected account(s) to remove", "Skipped: No non-position assets or liabilities in the selected account(s) to remove"));
	}


	@Test
	public void testAccountingBalanceRemoval_PositionsOnly() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		// Add a Trade for XOM
		Trade trade = this.tradeUtils.newStocksTradeBuilder(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("XOM"), BigDecimal.valueOf(14800), true, DateUtils.toDate("05/02/2016"), this.goldmanSachs, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		AccountingBalanceRemovalCommand removalCommand = new AccountingBalanceRemovalCommand();
		removalCommand.setHoldingInvestmentAccountId(accountInfo.getHoldingAccount().getId());
		removalCommand.setRemovalDate(DateUtils.toDate("05/31/2016"));
		removalCommand.setRemovalNote("Test");
		removalCommand.setDistributePositions(true);
		removalCommand.setDistributeNonPositionAssetsAndLiabilities(false);
		Status removalStatus = this.accountingBalanceRemovalService.processAccountingBalanceRemoval(removalCommand);
		validateRemovalStatus(removalStatus, true, CollectionUtils.createList("Message: Booked and posted 1 transfer(s) for 1 position(s)."));
	}


	@Test
	public void testAccountingBalanceRemoval_All() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.stocks);

		// Add a Trade for XOM
		Trade trade = this.tradeUtils.newStocksTradeBuilder(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("XOM"), BigDecimal.valueOf(14800), true, DateUtils.toDate("05/02/2016"), this.goldmanSachs, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(trade);

		// Cash Div Payment
		EventJournalGeneratorCommand generatorCommand = EventJournalGeneratorCommand.ofEventForClientAccount(1520699, accountInfo.getClientAccount().getId(), AccountingEventJournalGeneratorTypes.POST);
		this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(generatorCommand);

		AccountingBalanceRemovalCommand removalCommand = new AccountingBalanceRemovalCommand();
		removalCommand.setHoldingInvestmentAccountId(accountInfo.getHoldingAccount().getId());
		removalCommand.setRemovalDate(DateUtils.toDate("06/13/2016"));
		removalCommand.setRemovalNote("Test");
		removalCommand.setDistributePositions(true);
		removalCommand.setDistributeNonPositionAssetsAndLiabilities(true);
		Status removalStatus = this.accountingBalanceRemovalService.processAccountingBalanceRemoval(removalCommand);
		validateRemovalStatus(removalStatus, true, CollectionUtils.createList("Message: Booked and posted 1 transfer(s) for 1 position(s).", "Message: Removed all non-position assets and liabilities [Cash] from the accounts."));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateRemovalStatus(Status status, boolean actionPerformed, List<String> detailMessages) {
		Assertions.assertEquals(actionPerformed, status.isActionPerformed());
		Assertions.assertEquals(detailMessages.size(), status.getDetailList().size());

		for (StatusDetail detail : status.getDetailList()) {
			String actualMessage = detail.getCategory() + ": " + detail.getNote();
			Assertions.assertTrue(detailMessages.contains(actualMessage), "Did not expect detail message " + actualMessage);
		}
	}
}
