package com.clifton.ims.tests.compliance;

import com.clifton.business.client.BusinessClient;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.security.user.SecurityGroup;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * Tests that go through activating an account to ensure the compliance rule exists condition works
 */

public class ComplianceAccountActivationTests extends BaseImsIntegrationTest {

	@Resource
	private WorkflowTransitioner workflowTransitioner;


	@Test
	public void testActivateAccountWithoutRules() {
		String errorMessage = "";
		try {
			verifyAccountActivation();
			ValidationUtils.assertTrue(false, "Should not be able to activate account without compliance rules assigned");
		}
		catch (ImsErrorResponseException e) {
			errorMessage = e.getErrorMessageFromIMS();
		}

		ValidationUtils.assertTrue(errorMessage.contains("Compliance rules are required for account to become active"), "Should not be able to activate account without compliance rules assigned");
	}


	@Test
	public void testActivateAccountWithShortLongRules() {
		verifyAccountActivation("Permitted Security: Bonds (ABS) - All (Long Only)");
		ValidationUtils.assertTrue(true, "Should be able to activate account with compliance rules assigned");
	}


	@Test
	public void testActivateAccountWithRollupRules() {
		verifyAccountActivation("Options on Futures Allowed RU");
		ValidationUtils.assertTrue(true, "Should be able to activate account with compliance rules assigned");
	}


	@Test
	public void testActivateAccountWithShortLongAndRollupRules() {
		verifyAccountActivation("Options on Futures Allowed RU", "Permitted Security: Bonds (ABS) - All (Long Only)");
		ValidationUtils.assertTrue(true, "Should be able to activate account with compliance rules assigned");
	}


	private void verifyAccountActivation(String... ruleNames) {
		BusinessClient client = this.businessUtils.createBusinessClient();
		InvestmentAccount clientAccount = this.investmentAccountUtils.createInactiveClientAccount(client);
		this.complianceUtils.assignRulesToAccountByNames(clientAccount, ruleNames);

		this.workflowTransitioner.transitionIfPresent("Submit for Approval", "InvestmentAccount", BeanUtils.getIdentityAsLong(clientAccount));

		this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_3, SecurityGroup.INVESTMENT_ACCOUNT_ADMIN);
		this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_3, SecurityGroup.CLIENT_ADMIN);
		this.userUtils.addPermissionToUser(ImsTestProperties.TEST_USER_3, "Workflow");
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_3, ImsTestProperties.TEST_USER_3_PASSWORD);

		try {
			//Attempt to activate account, this should fail without rules
			this.workflowTransitioner.transitionIfPresent("Activate Account", "InvestmentAccount", BeanUtils.getIdentityAsLong(clientAccount));
		}
		finally {
			// Clean up - Switch Back to admin user
			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
			// Clean Up All Security Permissions for the test user
			this.userUtils.removeAllPermissionsForUser(ImsTestProperties.TEST_USER_3);
		}
	}
}
