package com.clifton.ims.tests.portfolio.run;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.service.BusinessServiceBusinessServiceProcessingType;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.builders.investment.assetclass.InvestmentAssetClassBuilder;
import com.clifton.ims.tests.builders.investment.assetclass.account.assetclass.InvestmentAccountAssetClassBuilder;
import com.clifton.ims.tests.builders.investment.manager.InvestmentManagerAccountBuilder;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.portfolio.run.process.PortfolioRunProcessService;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.test.util.RandomUtils;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>PortfolioRunServiceImplTests</code>
 * <p>
 * Delete method is currently in ProductOverlayService because need to delete supported tables
 * will re-factor this to Portfolio project as LDI is supported
 *
 * @author manderson
 */

public class PortfolioRunServiceImplTests extends BaseImsIntegrationTest {


	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;
	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private PortfolioRunProcessService portfolioRunProcessService;

	@Resource
	private PortfolioRunUtils portfolioRunUtils;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	@Resource
	private PortfolioRunMarginService portfolioRunMarginService;

	@Resource
	private SystemColumnValueService systemColumnValueService;


	@Test
	public void testCreateAndDeleteRun_UserSecurity() {
		// Change to different user to verify security access
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
		this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_2, "Analysts");
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD);

		// Get Last Run for this account - create new version, process, and delete
		PortfolioRun existingRun = this.portfolioRunUtils.getLastRunForAccount("310400");

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(existingRun.getClientInvestmentAccount(), existingRun.getBalanceDate());

		this.portfolioRunProcessService.deletePortfolioRun(run.getId());

		// Clean Up Security
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
		this.userUtils.removeAllPermissionsForUser(ImsTestProperties.TEST_USER_2);
	}

	///////////////////////////////////////////////////////////////////
	// Overlay Tests
	///////////////////////////////////////////////////////////////////


	@Test
	public void testCreateProcessInvalidRun_Overlay_MissingManagerBalances() {
		// Creating a new run for today will fail because manager balances we know won't be loaded yet
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("310400");

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(clientAccount, DateUtils.getPreviousWeekday(DateUtils.addDays(new Date(), 1)));

		processPortfolioRun(run, null);

		validateRun(run.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED, "Invalid", "Invalid");

		// Delete the run
		this.portfolioRunProcessService.deletePortfolioRun(run.getId());
	}


	@Test
	public void testCreateProcessInvalidRun_ThenSuccessful() {
		// Note: This is used to test deleting of rule violations
		InvestmentAccount account = createClientAccount(ServiceProcessingTypes.PORTFOLIO_RUNS);

		InvestmentManagerAccount managerAccount = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
				.withAccountName(RandomUtils.randomNameAndNumber())
				.withClient(account.getBusinessClient())
				.withManagerCompany(account.getBusinessClient().getCompany())
				.withManagerType(InvestmentManagerAccount.ManagerTypes.STANDARD)
				.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.NONE)
				.withBaseCurrency(this.usd)
				.buildAndSave();

		InvestmentManagerAccount managerAccount2 = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
				.withAccountName(RandomUtils.randomNameAndNumber())
				.withClient(account.getBusinessClient())
				.withManagerCompany(account.getBusinessClient().getCompany())
				.withManagerType(InvestmentManagerAccount.ManagerTypes.STANDARD)
				.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.NONE)
				.withBaseCurrency(this.usd)
				.buildAndSave();

		InvestmentAssetClass investmentAssetClass = InvestmentAssetClassBuilder.investmentAssetClass(this.investmentSetupService)
				.withCash(false)
				.withMainCash(false)
				.withMaster(true)
				.withName(RandomUtils.randomNameAndNumber())
				.buildAndSave();

		InvestmentAccountAssetClassBuilder.investmentAccountAssetClass(this.investmentAccountAssetClassService)
				.withAccount(account)
				.withAssetClass(investmentAssetClass)
				.withAssetClassPercentage(new BigDecimal(100))
				.withCashPercentage(new BigDecimal(100))
				.withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE)
				.withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE)
				.buildAndSave();

		List<InvestmentManagerAccountAllocation> allocationList = new ArrayList<>();
		allocationList.add(this.investmentManagerAccountUtils.createInvestmentManagerAccountAllocation(investmentAssetClass, new BigDecimal(100)));
		this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount, account, allocationList);
		this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount2, account, allocationList);

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(account, DateUtils.getPreviousWeekday(new Date()));
		processPortfolioRun(run, null);
		run = validateRun(run.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED, "Invalid", "Invalid");

		// Add manager balance
		this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount, run.getBalanceDate(), new BigDecimal(100), new BigDecimal(0));
		this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount2, run.getBalanceDate(), new BigDecimal(100), new BigDecimal(0));

		processPortfolioRun(run, null);
		run = validateRun(run.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS, "Pending Analyst Approval", "Review");

		// Create a Run for the Next Day
		PortfolioRun nextRun = this.portfolioRunUtils.createPortfolioRun(account, DateUtils.getNextWeekday(run.getBalanceDate()), true);
		processPortfolioRun(nextRun, null);
		nextRun = validateRun(nextRun.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED, "Invalid", "Invalid");

		// Add Manager Balance
		this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount, nextRun.getBalanceDate(), new BigDecimal(100), new BigDecimal(0));
		this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount2, nextRun.getBalanceDate(), new BigDecimal(100), new BigDecimal(0));

		processPortfolioRun(nextRun, null);
		nextRun = validateRun(nextRun.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS, "Pending Analyst Approval", "Review");

		// SET PREVIOUS RUN TO MAIN RUN TO REMOVE VIOLATION
		run.setMainRun(true);
		this.portfolioRunService.savePortfolioRun(run);

		processPortfolioRun(nextRun, null);
		validateRun(nextRun.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS, "Pending Analyst Approval", "Review");


		// Delete the runs
		this.portfolioRunProcessService.deletePortfolioRun(run.getId());
		this.portfolioRunProcessService.deletePortfolioRun(nextRun.getId());
	}


	@Test
	public void testCreateProcessInvalidRun_Overlay_InvalidSetup() {
		InvestmentAccount account = createClientAccount(ServiceProcessingTypes.PORTFOLIO_RUNS);

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(account, DateUtils.getPreviousWeekday(new Date()));

		processPortfolioRun(run, "There are no active Manager Account Assignments assigned to account [" + account.getLabel() + "].  Unable to process Portfolio run for this account.");

		validateRun(run.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED, "Manager Allocations", "Invalid");

		// Delete the run
		this.portfolioRunProcessService.deletePortfolioRun(run.getId());
	}


	@Test
	public void testGroupedMargin_Overlay_InitialMargin() {
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("661624");
		String originalValue = (String) this.systemColumnValueService.getSystemColumnCustomValue(clientAccount.getId(), InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, InvestmentAccount.GROUPED_MARGIN_ACCOUNT);

		Date runDate = DateUtils.toDate("02/10/2021");

		//Get a known run from before the setting was added.
		PortfolioRun originalRun = this.portfolioRunService.getPortfolioRunByMainRun(clientAccount.getId(), runDate);

		PortfolioRunMargin portfolioRunMarginFirst = CollectionUtils.getFirstElement(this.portfolioRunMarginService.getPortfolioRunMarginListByRun(originalRun.getId()));
		if (portfolioRunMarginFirst != null && !MathUtils.isEqual(portfolioRunMarginFirst.getMarginInitialValue(), BigDecimal.ZERO)) {
			try {
				// Create a new run for Feb 10, 2021, check the margin, updated the Grouped Margin custom field and reprocess the run, should have an initial margin of zero.
				updateClientAccountGroupMarginCustomValue(clientAccount, InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, "true", "true");

				PortfolioRun testRun = this.portfolioRunUtils.createPortfolioRun(clientAccount, runDate);
				processPortfolioRun(testRun, null);
				validateRun(testRun.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS, "Pending Analyst Approval", "Review");

				PortfolioRunMargin portfolioRunMarginTest = CollectionUtils.getFirstElement(this.portfolioRunMarginService.getPortfolioRunMarginListByRun(testRun.getId()));

				Assertions.assertNotNull(portfolioRunMarginTest);
				Assertions.assertTrue(MathUtils.isNullOrZero(portfolioRunMarginTest.getMarginInitialValue()));

				// Delete the run
				this.portfolioRunProcessService.deletePortfolioRun(testRun.getId());
			}
			finally {
				//reset everything
				updateClientAccountGroupMarginCustomValue(clientAccount, InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, "true", originalValue);
				String lastValue = (String) this.systemColumnValueService.getSystemColumnCustomValue(clientAccount.getId(), InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, InvestmentAccount.GROUPED_MARGIN_ACCOUNT);
				Assertions.assertEquals(originalValue, lastValue);
			}
		}
	}


	///////////////////////////////////////////////////////////////////
	// LDI Tests
	///////////////////////////////////////////////////////////////////


	@Test
	public void testCreateProcessInvalidRun_LDI_MissingManagerBalances() {
		// Creating a new run for today will fail because manager balances we know won't be loaded yet
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("668452");

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(clientAccount, DateUtils.getPreviousWeekday(DateUtils.addDays(new Date(), 1)));

		processPortfolioRun(run, null);

		validateRun(run.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED, "Invalid", "Invalid");

		// Delete the run
		this.portfolioRunProcessService.deletePortfolioRun(run.getId());
	}


	@Test
	public void testCreateProcessInvalidRun_LDI_InvalidSetup() {
		InvestmentAccount account = createClientAccount(ServiceProcessingTypes.LDI);

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(account, DateUtils.getPreviousWeekday(new Date()));

		processPortfolioRun(run, "There are no Active Key Rate Duration Points set up for this account.");

		validateRun(run.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED, "Manager Allocations", "Invalid");

		// Delete the run
		this.portfolioRunProcessService.deletePortfolioRun(run.getId());
	}


	/**
	 * After updating to Hibernate 5, an intermittent issue was identified trying to transition a PortfolioRun when
	 * the transition contained an action with report generation. For some reason the referenced InvestmentAccount was
	 * being updated (caused by a flush while in transaction due to workflow state change) causing the report to be
	 * locked for accessing the account. A Hibernate issue has been created to address our transient collection mappings
	 * flagging the versioned entity as dirty (https://hibernate.atlassian.net/browse/HHH-10404).
	 */
	@Test
	public void testInvestmentAccountDoesNotGetUpdatedDuringPortfolioRunTransition() {
		InvestmentAccount account = createClientAccount(ServiceProcessingTypes.LDI);
		byte[] beginningRv = account.getRv();

		PortfolioRun run = this.portfolioRunUtils.createPortfolioRun(account, DateUtils.getPreviousWeekday(new Date()));
		try {
			processPortfolioRun(run, "There are no Active Key Rate Duration Points set up for this account.");
			/*
			 * In Hibernate 4.3.11 and 5.0.3, the InvestmentAccount was being updated because the transient collections
			 * of the entity were classifying the entity as dirty (empty PersistentBag vs null). Until a Hibernate fix
			 * is released, we have overridden Hibernate's CollectionType with our own compiled version that validates
			 * the checkability of the collection before checking dirtiness.
			 */
			InvestmentAccount updatedAccount = this.investmentAccountUtils.getClientAccountByNumber(account.getNumber());
			Assertions.assertArrayEquals(beginningRv, updatedAccount.getRv());
		}
		finally {
			// Delete the run
			this.portfolioRunProcessService.deletePortfolioRun(run.getId());
		}
	}


	@Test
	public void testGroupedMargin_LDI_InitialMargin() {
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("305801");
		String originalValue = (String) this.systemColumnValueService.getSystemColumnCustomValue(clientAccount.getId(), InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, InvestmentAccount.GROUPED_MARGIN_ACCOUNT);

		Date runDate = DateUtils.toDate("02/10/2021");

		//Get a known run from before the setting was added.
		PortfolioRun originalRun = this.portfolioRunService.getPortfolioRunByMainRun(clientAccount.getId(), runDate);

		PortfolioRunMargin portfolioRunMarginFirst = CollectionUtils.getFirstElement(this.portfolioRunMarginService.getPortfolioRunMarginListByRun(originalRun.getId()));
		if (portfolioRunMarginFirst != null && !MathUtils.isEqual(portfolioRunMarginFirst.getMarginInitialValue(), BigDecimal.ZERO)) {
			try {
				// Create a new run for Feb 10, 2021, check the margin, updated the Grouped Margin custom field and reprocess the run, should have an initial margin of zero.
				updateClientAccountGroupMarginCustomValue(clientAccount, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME, "LDI", "true");

				PortfolioRun testRun = this.portfolioRunUtils.createPortfolioRun(clientAccount, runDate);
				processPortfolioRun(testRun, null);
				validateRun(testRun.getId(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS, "Pending Analyst Approval", "Review");

				PortfolioRunMargin portfolioRunMarginTest = CollectionUtils.getFirstElement(this.portfolioRunMarginService.getPortfolioRunMarginListByRun(testRun.getId()));

				Assertions.assertNotNull(portfolioRunMarginTest);
				Assertions.assertTrue(MathUtils.isNullOrZero(portfolioRunMarginTest.getMarginInitialValue()));

				// Delete the run
				this.portfolioRunProcessService.deletePortfolioRun(testRun.getId());
			}
			finally {
				//reset everything
				updateClientAccountGroupMarginCustomValue(clientAccount, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME, "LDI", originalValue);
				String lastValue = (String) this.systemColumnValueService.getSystemColumnCustomValue(clientAccount.getId(), InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME, InvestmentAccount.GROUPED_MARGIN_ACCOUNT);
				Assertions.assertEquals(originalValue, lastValue);
			}
		}
	}


	////////////////////////////////////////////////////////////////////
	//////////                Helper Methods                  //////////
	////////////////////////////////////////////////////////////////////


	private void processPortfolioRun(PortfolioRun run, String expectedExceptionMessage) {
		List<WorkflowTransition> transitionList = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(PortfolioRun.TABLE_PORTFOLIO_RUN, BeanUtils.getIdentityAsLong(run));
		WorkflowTransition transition = null;
		for (WorkflowTransition trans : transitionList) {
			if (StringUtils.isEqual("Processing", trans.getEndWorkflowState().getName())) {
				transition = trans;
				break;
			}
		}

		if (transition == null) {
			throw new RuntimeException("Unable to find transition to Processing state from " + run.getWorkflowState().getName());
		}
		String exceptionMessage = null;
		try {
			this.workflowTransitionService.executeWorkflowTransitionByTransition(PortfolioRun.TABLE_PORTFOLIO_RUN, run.getId(), transition.getId());
		}

		catch (Throwable e) {
			if (e instanceof ImsErrorResponseException) {
				exceptionMessage = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			if (exceptionMessage == null) {
				exceptionMessage = e.getMessage();
			}
		}
		Assertions.assertEquals(expectedExceptionMessage, exceptionMessage);
	}


	private PortfolioRun validateRun(int runId, RuleViolationStatus.RuleViolationStatusNames expectedViolationStatus, String expectedWorkflowStateName, String expectedWorkflowStatusName) {
		PortfolioRun run = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioRunService.getPortfolioRun(runId), 3);
		Assertions.assertEquals(expectedViolationStatus, run.getViolationStatus().getRuleViolationStatusName());
		Assertions.assertEquals(expectedWorkflowStateName, run.getWorkflowState().getName());
		Assertions.assertEquals(expectedWorkflowStatusName, run.getWorkflowStatus().getName());
		return run;
	}


	private InvestmentAccount createClientAccount(ServiceProcessingTypes processingType) {
		BusinessClient client = this.businessUtils.createBusinessClient();
		InvestmentAccount clientAccount = this.investmentAccountUtils.createClientAccount(client);

		BusinessServiceBusinessServiceProcessingType businessServiceBusinessServiceProcessingType = this.businessUtils.getBusinessServiceBusinessServiceProcessingTypeForProcessingType(processingType);
		clientAccount.setBusinessService(businessServiceBusinessServiceProcessingType.getBusinessService());
		clientAccount.setServiceProcessingType(businessServiceBusinessServiceProcessingType.getBusinessServiceProcessingType());

		return this.investmentAccountService.saveInvestmentAccount(clientAccount);
	}


	private void updateClientAccountGroupMarginCustomValue(InvestmentAccount clientAccount, String columnGroupName, String linkedValue, String isGroupedMarginValue) {
		SystemColumnCustomConfig systemColumnCustomConfig = new SystemColumnCustomConfig();
		systemColumnCustomConfig.setColumnGroupName(columnGroupName);
		systemColumnCustomConfig.setEntityId(clientAccount.getId());
		systemColumnCustomConfig.setLinkedValue(linkedValue);
		systemColumnCustomConfig = this.systemColumnValueService.getSystemColumnCustomConfig(systemColumnCustomConfig);
		if (systemColumnCustomConfig.getColumnValueList() == null) {
			systemColumnCustomConfig.setColumnValueList(new ArrayList<>());
		}

		SystemColumnValue groupMarginValue = new SystemColumnValue();
		groupMarginValue.setColumn(CollectionUtils.getFirstElement(BeanUtils.filter(systemColumnCustomConfig.getColumnList(), SystemColumnCustom::getName, InvestmentAccount.GROUPED_MARGIN_ACCOUNT)));
		groupMarginValue.setValue(isGroupedMarginValue);
		Integer index = CoreCollectionUtils.getIndexOfFirstPropertyMatch(systemColumnCustomConfig.getColumnValueList(), "column.label", InvestmentAccount.GROUPED_MARGIN_ACCOUNT);
		if (index != null) {
			systemColumnCustomConfig.getColumnValueList().set(index, groupMarginValue);
		}
		else {
			systemColumnCustomConfig.getColumnValueList().add(groupMarginValue);
		}
		this.systemColumnValueService.saveSystemColumnValueList(systemColumnCustomConfig);
	}
}
