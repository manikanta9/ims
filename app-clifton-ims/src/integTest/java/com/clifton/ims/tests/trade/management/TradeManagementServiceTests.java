package com.clifton.ims.tests.trade.management;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.protocol.ImsProtocolClient;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeListActionCommand;
import com.clifton.trade.TradeListActionTypes;
import com.clifton.trade.TradeService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author davidi
 */
public class TradeManagementServiceTests extends BaseImsIntegrationTest {

	@Resource
	private TradeService tradeService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private ImsProtocolClient imsProtocolClient;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testExecuteTradeActionByCommand_FILL() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.stocks);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.stocks);
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL");

		final BigDecimal fillPrice = BigDecimal.valueOf(300.0);
		final BigDecimal quantity1 = BigDecimal.valueOf(10.0);
		final BigDecimal quantity2 = BigDecimal.valueOf(20.0);

		Trade trade1 = this.tradeUtils.newStocksTradeBuilder(security, quantity1, true, DateUtils.toDate("06/25/2020"), accountInfo1.getHoldingCompany(), accountInfo1)
				.withAverageUnitPrice(null)
				.withExpectedUnitPrice(null)
				.buildAndSave();
		trade1 = transitionTradeToAwaitingFillsState(trade1);

		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(security, quantity2, true, DateUtils.toDate("06/25/2020"), accountInfo2.getHoldingCompany(), accountInfo2)
				.withAverageUnitPrice(null)
				.withExpectedUnitPrice(null)
				.buildAndSave();

		trade2 = transitionTradeToAwaitingFillsState(trade2);

		List<Trade> beanList = CollectionUtils.createList(trade1, trade2);
		TradeListActionCommand command = new TradeListActionCommand();
		command.setFillPrice(fillPrice);
		command.setActionType(TradeListActionTypes.FILL);
		command.setBeanList(beanList);

		Map<String, String> params = commandToMap(command);
		params.put("disableValidatingBindingValidation", "true");
		this.imsProtocolClient.request("/tradeListActionCommandExecute.json", params, Status.class, "result", false);

		trade1 = this.tradeService.getTrade(trade1.getId());
		List<TradeFill> tradeFillList1 = trade1.getTradeFillList();
		trade2 = this.tradeService.getTrade(trade2.getId());
		List<TradeFill> tradeFillList2 = trade2.getTradeFillList();

		Assertions.assertEquals(fillPrice, trade1.getAverageUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertNotNull(tradeFillList1);
		Assertions.assertEquals(1, tradeFillList1.size());
		Assertions.assertEquals(quantity1, tradeFillList1.get(0).getQuantity().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(fillPrice, tradeFillList1.get(0).getNotionalUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(TradeService.TRADE_AWAITING_FILLS_STATE_NAME, trade1.getWorkflowState().getName());

		Assertions.assertEquals(fillPrice, trade2.getAverageUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertNotNull(tradeFillList2);
		Assertions.assertEquals(1, tradeFillList2.size());
		Assertions.assertEquals(quantity2, tradeFillList2.get(0).getQuantity().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(fillPrice, tradeFillList2.get(0).getNotionalUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(TradeService.TRADE_AWAITING_FILLS_STATE_NAME, trade2.getWorkflowState().getName());
	}


	@Test
	public void testExecuteTradeAction_FILL_EXECUTE() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.stocks);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.stocks);
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL");

		final BigDecimal fillPrice = BigDecimal.valueOf(300.0);
		final BigDecimal quantity1 = BigDecimal.valueOf(10.0);
		final BigDecimal quantity2 = BigDecimal.valueOf(20.0);

		Trade trade1 = this.tradeUtils.newStocksTradeBuilder(security, quantity1, true, DateUtils.toDate("06/25/2020"), accountInfo1.getHoldingCompany(), accountInfo1)
				.withAverageUnitPrice(null)
				.withExpectedUnitPrice(null)
				.buildAndSave();
		trade1 = transitionTradeToAwaitingFillsState(trade1);

		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(security, quantity2, true, DateUtils.toDate("06/25/2020"), accountInfo2.getHoldingCompany(), accountInfo2)
				.withAverageUnitPrice(null)
				.withExpectedUnitPrice(null)
				.buildAndSave();

		trade2 = transitionTradeToAwaitingFillsState(trade2);

		List<Trade> beanList = CollectionUtils.createList(trade1, trade2);
		TradeListActionCommand command = new TradeListActionCommand();
		command.setFillPrice(fillPrice);
		command.setActionType(TradeListActionTypes.FILL_EXECUTE);
		command.setBeanList(beanList);

		Map<String, String> params = commandToMap(command);
		params.put("disableValidatingBindingValidation", "true");
		this.imsProtocolClient.request("/tradeListActionCommandExecute.json", params, Status.class, "result", false);

		trade1 = this.tradeService.getTrade(trade1.getId());
		List<TradeFill> tradeFillList1 = trade1.getTradeFillList();
		trade2 = this.tradeService.getTrade(trade2.getId());
		List<TradeFill> tradeFillList2 = trade2.getTradeFillList();

		Assertions.assertEquals(fillPrice, trade1.getAverageUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertNotNull(tradeFillList1);
		Assertions.assertEquals(1, tradeFillList1.size());
		Assertions.assertEquals(quantity1, tradeFillList1.get(0).getQuantity().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(fillPrice, tradeFillList1.get(0).getNotionalUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(TradeService.TRADE_EXECUTED_VALID_STATE_NAME, trade1.getWorkflowState().getName());

		Assertions.assertEquals(fillPrice, trade2.getAverageUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertNotNull(tradeFillList2);
		Assertions.assertEquals(1, tradeFillList2.size());
		Assertions.assertEquals(quantity2, tradeFillList2.get(0).getQuantity().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(fillPrice, tradeFillList2.get(0).getNotionalUnitPrice().setScale(1, RoundingMode.HALF_UP));
		Assertions.assertEquals(TradeService.TRADE_EXECUTED_VALID_STATE_NAME, trade2.getWorkflowState().getName());
	}


	@Test
	public void testExecuteTradeActionByCommand_DIFFERING_SECURITIES_ERROR() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.stocks);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.stocks);
		InvestmentSecurity security1 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("AAPL");
		InvestmentSecurity security2 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM");

		final BigDecimal fillPrice = BigDecimal.valueOf(300.0);
		final BigDecimal quantity1 = BigDecimal.valueOf(10.0);
		final BigDecimal quantity2 = BigDecimal.valueOf(20.0);

		Trade trade1 = this.tradeUtils.newStocksTradeBuilder(security1, quantity1, true, DateUtils.toDate("06/25/2020"), accountInfo1.getHoldingCompany(), accountInfo1)
				.withAverageUnitPrice(null)
				.withExpectedUnitPrice(null)
				.buildAndSave();
		trade1 = transitionTradeToAwaitingFillsState(trade1);

		Trade trade2 = this.tradeUtils.newStocksTradeBuilder(security2, quantity2, true, DateUtils.toDate("06/25/2020"), accountInfo2.getHoldingCompany(), accountInfo2)
				.withAverageUnitPrice(null)
				.withExpectedUnitPrice(null)
				.buildAndSave();

		trade2 = transitionTradeToAwaitingFillsState(trade2);

		List<Trade> beanList = CollectionUtils.createList(trade1, trade2);
		TradeListActionCommand command = new TradeListActionCommand();
		command.setFillPrice(fillPrice);
		command.setActionType(TradeListActionTypes.FILL_EXECUTE);
		command.setBeanList(beanList);

		Map<String, String> params = commandToMap(command);
		params.put("disableValidatingBindingValidation", "true");
		try {
			this.imsProtocolClient.request("/tradeListActionCommandExecute.json", params, Status.class, "result", false);
		}
		catch (Exception exc) {
			Assertions.assertEquals(ImsErrorResponseException.class, exc.getClass());
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private Trade transitionTradeToAwaitingFillsState(Trade trade) {
		// note:  newly-created test trades start out in Pending state

		// This code block generates a HTTP: 404 exception, but the technique of manually setting the state to approved and saving the trade does work.
		//WorkflowTransition nextTransition = this.workflowTransitionService.getWorkflowTransitionNext(trade, new StringBuilder());
		//this.workflowTransitionService.executeWorkflowTransitionByTransition("Trade", trade.getId(), nextTransition.getId());
		//return tradeService.getTrade(trade.getId());
		List<String> stateList = CollectionUtils.createList(TradeService.TRADE_APPROVED_STATE_NAME);
		return transitionTradeToState(trade, stateList);
	}


	private Trade transitionTradeToState(Trade trade, List<String> stateNames) {
		for (String stateName : stateNames) {
			trade = applyWorkflowStateToTrade(trade, stateName);
		}
		return trade;
	}


	private Trade applyWorkflowStateToTrade(Trade trade, String workflowStateName) {
		Short workFlowId = trade.getWorkflowState().getWorkflow().getId();
		WorkflowState workflowState = this.workflowDefinitionService.getWorkflowStateByName(workFlowId, workflowStateName);
		trade.setWorkflowState(workflowState);
		trade.setWorkflowStatus(workflowState.getStatus());
		return this.tradeService.saveTrade(trade);
	}


	private Map<String, String> commandToMap(TradeListActionCommand command) {
		HashMap<String, String> params = new HashMap<>();
		params.put("fillPrice", command.getFillPrice().toString());
		params.put("actionType", command.getActionType().toString());

		String tradeData = "";
		for (Trade tradeEntry : command.getBeanList()) {
			if (!StringUtils.isEmpty(tradeData)) {
				tradeData += ",";
			}
			tradeData += "{\"id\":" + tradeEntry.getId() + ",\"class\":\"com.clifton.trade.Trade\"}";
		}
		tradeData = "[" + tradeData + "]";

		params.put("beanList", tradeData);
		return params;
	}
}
