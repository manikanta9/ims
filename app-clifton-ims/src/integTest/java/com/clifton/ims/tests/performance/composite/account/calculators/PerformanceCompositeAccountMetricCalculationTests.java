package com.clifton.ims.tests.performance.composite.account.calculators;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.metric.PerformanceMetricFieldTypes;
import com.clifton.performance.composite.performance.metric.PerformanceMetricPeriods;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;


/**
 * The <code>PerformanceCompositeAccountMetricCalculationTests</code> tests the metrics calculations for the account
 * i.e. QTD, YTD, ITD, One Year, etc.
 * MTD values are NOT recalculated, but historical values are during previews to compare with actual values
 *
 * @author manderson
 */
public class PerformanceCompositeAccountMetricCalculationTests extends BasePerformanceCompositeAccountCalculatorTests {

	@Resource
	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountMetrics() {
		// Has 3 Full Years of History and One Benchmark - Currently a Terminated Account
		String accountNumber = "050920";
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "10/31/2014", 0.002);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "11/30/2014", 0.002);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "12/31/2014", 0.002);

		// One Year of History and Two Benchmarks
		accountNumber = "122511";
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "10/31/2015", 0.017);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "11/30/2015", 0.017);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "12/31/2015", 0.017);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "01/31/2016", 0.017);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "02/29/2016", 0.017);

		// Does Not have a Full Year of History Yet
		accountNumber = "456606";
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "10/31/2015");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "11/30/2015");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "12/31/2015");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "01/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "02/29/2016");

		// Has 10+ Years of History and 1 Benchmark
		accountNumber = "428600";
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "10/31/2015", 0.002);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "11/30/2015", 0.002);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "12/31/2015", 0.002);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "01/31/2016", 0.002);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, accountNumber, "02/29/2016", 0.002);

		// Has 3+ years of history and 2 benchmarks
		accountNumber = "800020";
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(DE_COMPOSITE_III_CODE, accountNumber, "10/31/2015", 0.012);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(DE_COMPOSITE_III_CODE, accountNumber, "11/30/2015", 0.012);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(DE_COMPOSITE_III_CODE, accountNumber, "12/31/2015", 0.012);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(DE_COMPOSITE_III_CODE, accountNumber, "01/31/2016", 0.012);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(DE_COMPOSITE_III_CODE, accountNumber, "02/29/2016", 0.012);
	}


	/**
	 * When there are gaps in performance history, the account should "start over" and inception date is reset
	 */
	@Test
	public void testLinkedReturnsWithGaps() {
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "074500", "10/31/2011", "08/31/2012");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "074500", "10/31/2012", "11/30/2014", 0.075);

		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "664030", "08/31/2007", "07/31/2008");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "664030", "08/31/2008", "01/31/2015", 0.025);

		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "125500", "02/28/2010", "08/31/2012", 0.01);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "425100", "02/28/2011", "02/28/2014", 0.01);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "385705", "08/31/2013", "03/31/2015", 0.003);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "660400", "04/30/2009", "11/30/2014", 0.0048);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "424000", "08/31/2011", "03/31/2015", 0.009);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "372600", "06/30/2012", "03/31/2015", 0.005);
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "123610", "01/31/2011", "03/31/2015", 0.012);
		// Ignoring this for now - this account is associated with multiple composites - known item to resolve: https://wiki.paraport.com/display/Risk/Saugatuck+GIPS+Performance
		//validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues("606203", "01/31/2014", "02/28/2015");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "110120", "02/28/2011", "03/31/2013");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "110100", "12/31/2011", "05/31/2012");
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(null, "385703", "08/31/2013", "03/31/2015", 0.01);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                    Historical Validation                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(String compositeCode, String accountNumber, String startDate, String endDate) {
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(compositeCode, accountNumber, startDate, endDate, null);
	}


	private void validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(String compositeCode, String accountNumber, String startDate, String endDate, Double inceptionToDateThreshold) {
		Date date = DateUtils.toDate(startDate);
		Date end = DateUtils.toDate(endDate);
		while (DateUtils.compare(date, end, false) <= 0) {
			validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(compositeCode, accountNumber, DateUtils.fromDateShort(date), true, inceptionToDateThreshold);
			date = DateUtils.getLastDayOfMonth(DateUtils.addDays(date, 1));
		}
	}


	private void validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(String compositeCode, String accountNumber, String periodEndDate) {
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(compositeCode, accountNumber, periodEndDate, false, null);
	}


	private void validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(String compositeCode, String accountNumber, String periodEndDate, Double inceptionToDateThreshold) {
		validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(compositeCode, accountNumber, periodEndDate, false, inceptionToDateThreshold);
	}


	private void validatePerformanceCompositeInvestmentAccountPerformanceHistoricalValues(String compositeCode, String accountNumber, String periodEndDate, boolean skipIfMissing, Double inceptionToDateThreshold) {
		PerformanceCompositeInvestmentAccountPerformance actualAccountPerformance = getPerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, periodEndDate, skipIfMissing);
		if (actualAccountPerformance == null) {
			return;
		}
		PerformanceCompositeInvestmentAccountPerformance previewAccountPerformance = getHistoricalPreviewResultForPerformanceCompositeInvestmentAccountPerformance(actualAccountPerformance);
		ValidationUtils.assertNotNull(previewAccountPerformance, "Preview PerformanceCompositeInvestmentAccountPerformance is required");

		// If one of the benchmark fields was cleared, but had a benchmark on it before, just skip those values and any dependent values
		Set<PerformanceMetricFieldTypes> ignoreFields = getIgnoreComparisonFields(previewAccountPerformance, actualAccountPerformance);


		String messagePrefix = actualAccountPerformance.getLabel();
		for (PerformanceMetricFieldTypes performanceMetricFieldType : PerformanceMetricFieldTypes.values()) {
			if (PerformanceMetricPeriods.MONTH_TO_DATE != performanceMetricFieldType.getPerformanceMetricPeriod()) {
				// Once History is filled in will need to verify these values
				if (PerformanceMetricFieldTypes.PERIOD_MODEL_NET_RETURN != performanceMetricFieldType.getDependentFieldType() && PerformanceMetricFieldTypes.PERIOD_MODEL_NET_RETURN != performanceMetricFieldType.getDependentFieldType().getDependentFieldType()) {
					BigDecimal actualValue = (BigDecimal) BeanUtils.getPropertyValue(actualAccountPerformance, performanceMetricFieldType.getBeanPropertyName());
					BigDecimal previewValue = (BigDecimal) BeanUtils.getPropertyValue(previewAccountPerformance, performanceMetricFieldType.getBeanPropertyName());
					if (isIgnoreComparisonField(performanceMetricFieldType, ignoreFields)) {
						continue;
					}

					// If both values are null/zero - ignore
					if (!MathUtils.isNullOrZero(actualValue) || !MathUtils.isNullOrZero(previewValue)) {
						// If actual value was null and date is prior to 2014 ignore
						if (MathUtils.isNullOrZero(actualValue) && !DateUtils.isDateAfter(DateUtils.toDate(periodEndDate), DateUtils.toDate("01/01/2015"))) {
							continue;
						}
						int precision = 7;
						// Allow bigger changes - due to original inception date calculations we missing a day (JIRA PERFORMANC-237)
						if (PerformanceMetricPeriods.INCEPTION_TO_DATE == performanceMetricFieldType.getPerformanceMetricPeriod() && inceptionToDateThreshold != null) {
							BigDecimal difference = MathUtils.subtract(actualValue, previewValue);
							if (MathUtils.isGreaterThan(MathUtils.abs(difference), inceptionToDateThreshold)) {
								Assertions.fail(messagePrefix + ": " + performanceMetricFieldType.getBeanPropertyName() + " doesn't match expected and difference " + CoreMathUtils.formatNumberDecimal(difference) + " is greater than expected threshold: " + inceptionToDateThreshold);
							}
						}
						else {
							Assertions.assertEquals(MathUtils.round(actualValue, precision), MathUtils.round(previewValue, precision), messagePrefix + ": " + performanceMetricFieldType.getBeanPropertyName() + " doesn't match expected");
						}
					}
				}
			}
		}
	}


	private PerformanceCompositeInvestmentAccountPerformance getHistoricalPreviewResultForPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		PerformanceCompositeAccountRebuildCommand command = new PerformanceCompositeAccountRebuildCommand();
		command.setToAccountingPeriod(accountPerformance.getAccountingPeriod());
		command.setPerformanceCompositeInvestmentAccountId(accountPerformance.getPerformanceCompositeInvestmentAccount().getId());
		command.setCalculateAccountDailyMonthlyBasePerformance(false);

		return this.performanceCompositePerformanceRebuildService.previewPerformanceCompositeAccountPerformance(command);
	}
}
