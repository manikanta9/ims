package com.clifton.ims.tests.rule.portfolio.run;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.core.util.MathUtils;
import org.jsoup.Jsoup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.File;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author stevenf
 * This is just used for my own testing purposes, runs all rules against the specified number of runs and prints the violations.
 * I 'unignore' when I need it.
 */
@Disabled
public class PortfolioRunRuleViolationPreviewTests extends BaseImsIntegrationTest {

	private static boolean testInitialized = false;

	private static Date balanceDate = DateUtils.toDate("01/27/2017");
	private static List<RuleViolation> ruleViolationList = new ArrayList<>();
	private static Map<String, String> portfolioRunViolationMap = new LinkedHashMap<>();


	@Resource
	public PortfolioRunService portfolioRunService;

	@Resource
	public RuleEvaluatorService ruleEvaluatorService;


	@BeforeEach
	public void getPortfolioRunRuleViolationList() {
		if (!testInitialized) {
			PortfolioRunSearchForm searchForm = new PortfolioRunSearchForm();
			searchForm.setMainRun(true);
			searchForm.setBalanceDate(balanceDate);
			//searchForm.setLimit(10);
			List<PortfolioRun> portfolioRunList = this.portfolioRunService.getPortfolioRunList(searchForm);
			for (PortfolioRun portfolioRun : CollectionUtils.getIterable(portfolioRunList)) {
				ruleViolationList.addAll(this.ruleEvaluatorService.previewRuleViolations("PortfolioRun", portfolioRun.getId()));
			}
			testInitialized = true;
		}
	}


	@Test
	public void printAllManagerAccountSecuritiesBalanceRangeViolations() {
		printRuleViolations("Manager Account: Securities Balance Range");
	}


	@Test
	public void printAllCashTargetChangeSincePreviousDayViolations() {
		printRuleViolations("Cash Target: Change Since Previous Day");
	}


//	@Test
//	public void printAllAssetClassOutdatedBenchmarkDurationViolations() {
//		printRuleViolations("Asset Class: Outdated Benchmark Duration");
//	}
//
//
//	@Test
//	public void printAllAssetClassOutdatedReplicationViolations() {
//		printRuleViolations("Asset Class: Outdated Replication");
//	}
//
//
//	@Test
//	public void printAllAssetClassChangedSincePreviousRunViolations() {
//		printRuleViolations("Asset Class: Changed Since Previous Run");
//	}
//
//
//	@Test
//	public void printAllCounterpartyExposureLimitMarketValueViolations() {
//		printRuleViolations("Counterparty Exposure Limit: Market Value");
//	}
//
//
//	@Test
//	public void printAllCounterpartyExposureLimitNotionalValueViolations() {
//		printRuleViolations("Counterparty Exposure Limit: Notional Value");
//	}
//
//
//	@Test
//	public void printAllCounterpartyExposureLimitOpenTradeEquityValueViolations() {
//		printRuleViolations("Counterparty Exposure Limit: Open Trade Equity Value");
//	}
//
//
//	@Test
//	public void printAllPIOSContractsUnmatchedViolations() {
//		printRuleViolations("Contracts Unmatched: Exclude Bonds And Currency");
//	}
//


	private void printRuleViolations(String ruleName) {
		List<RuleViolation> filteredList = ruleViolationList.stream()
				.filter(r -> r.getRuleAssignment().getRuleDefinition().getName().equals(ruleName))
				.collect(Collectors.toList());

		DataColumn[] columns = new DataColumn[4];
		columns[0] = new DataColumnImpl("Account #", Types.NVARCHAR, "");
		columns[1] = new DataColumnImpl("Account Name", Types.NVARCHAR, "");
		columns[2] = new DataColumnImpl("Team", Types.NVARCHAR, "");
		columns[3] = new DataColumnImpl("Violation Note", Types.NVARCHAR, "");
		DataTable dataTable = new PagingDataTableImpl(columns);
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(filteredList)) {
			if (!"Passed".equals(ruleViolation.getIgnoreNote())) {
				PortfolioRun run = this.portfolioRunService.getPortfolioRun(MathUtils.getNumberAsInteger(ruleViolation.getLinkedFkFieldId()));
				if (run != null) {
					Object[] data = new Object[4];
					data[0] = run.getClientInvestmentAccount().getNumber();
					data[1] = run.getClientInvestmentAccount().getLabel();
					data[2] = run.getClientInvestmentAccount().getTeamSecurityGroup().getName();
					data[3] = Jsoup.parse(ruleViolation.getViolationNote()).text();
					dataTable.addRow(new DataRowImpl(dataTable, data));
				}
			}
		}
		DataTableFileConfig dataTableFileConfig = new DataTableFileConfig(dataTable, DataTableFileConfig.DATA_TABLE_HEADER_EXPORT_STYLE);
		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		File excelFile = converter.convert(dataTableFileConfig);
		try {
			FileUtils.copyFileCreatePath(excelFile, "C:/work/export/" + ruleName.replaceAll("[^a-zA-Z0-9.-]", "_") +
					"_" + balanceDate.toString().replaceAll("[^a-zA-Z0-9.-]", "-") + ".xls");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
