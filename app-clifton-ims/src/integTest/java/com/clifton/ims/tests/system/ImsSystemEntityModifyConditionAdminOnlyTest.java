package com.clifton.ims.tests.system;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.test.SystemEntityModifyConditionAdminOnlyTest;
import com.clifton.test.spring.NoopLifecycleProcessor;
import org.springframework.context.LifecycleProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;


/**
 * The {@link ImsSystemEntityModifyConditionAdminOnlyTest} class implements the {@link SystemEntityModifyConditionAdminOnlyTest} test type for execution within the current
 * environment.
 *
 * @author ignacioa
 */
@ContextConfiguration("classpath:META-INF/spring/app-clifton-ims-context.xml")
@TestPropertySource
@Import(ImsSystemEntityModifyConditionAdminOnlyTest.TestConfiguration.class)
public class ImsSystemEntityModifyConditionAdminOnlyTest extends SystemEntityModifyConditionAdminOnlyTest {

	@Configuration
	@ExcludeFromComponentScan
	public static class TestConfiguration {

		// Simplify start-up process by disabling lifecycle beans
		@Bean(AbstractApplicationContext.LIFECYCLE_PROCESSOR_BEAN_NAME)
		public LifecycleProcessor lifecycleProcessor() {
			return new NoopLifecycleProcessor();
		}
	}
}
