package com.clifton.ims.tests.compliance;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.security.user.SecurityGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


public class ComplianceRuleModifyConditionTests extends BaseImsIntegrationTest {

	@Resource
	SystemConditionService systemConditionService;

	@Resource
	private ComplianceRuleService complianceRuleService;


	@Test
	public void testSetComplianceRuleModifyCondition() {
		ComplianceRule noTradeRule = this.complianceUtils.getComplianceRuleByName("Exclude All Positions");
		SystemCondition systemCondition = this.systemConditionService.getSystemConditionByName("Current User is a Member of Administrator Group");
		noTradeRule.setEntityModifyCondition(systemCondition);
		this.complianceRuleService.saveComplianceRule(noTradeRule);
		ComplianceRule noTradeRuleAdminOnly = this.complianceUtils.getComplianceRuleByName("Exclude All Positions");
		ValidationUtils.assertEquals(noTradeRuleAdminOnly.getEntityModifyCondition().getName(), "Current User is a Member of Administrator Group", "Error setting modify condition");
		noTradeRule.setEntityModifyCondition(this.systemConditionService.getSystemConditionByName("Always True Condition"));
		this.complianceRuleService.saveComplianceRule(noTradeRule);
	}


	@Test
	public void testAdminOnlyComplianceRuleModifyCondition() {
		ComplianceRuleAssignment noTradeGlobalAssignment = this.complianceUtils.getComplianceRuleAssignmentByRuleNameAndAccountName("Exclude All Positions", null);
		ComplianceRule noTradeRule = noTradeGlobalAssignment.getRule();

		// Clear Existing Admin Only Condition
		noTradeRule.setEntityModifyCondition(this.systemConditionService.getSystemConditionByName("Always True Condition"));
		this.complianceRuleService.saveComplianceRule(noTradeRule);

		//Switch to a basic Compliance user and test that they can change rules and assignments
		this.userUtils.addUserToSecurityGroup(ImsTestProperties.TEST_USER_2, SecurityGroup.COMPLIANCE);
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD);

		//Test to make sure changes are allowed without a condition set
		noTradeRule.setBatch(false);
		this.complianceRuleService.saveComplianceRule(noTradeRule);
		noTradeRule.setBatch(true);
		this.complianceRuleService.saveComplianceRule(noTradeRule);
		noTradeGlobalAssignment.setNote("TEST");
		this.complianceRuleService.saveComplianceRuleAssignment(noTradeGlobalAssignment);
		noTradeGlobalAssignment.setNote("");
		this.complianceRuleService.saveComplianceRuleAssignment(noTradeGlobalAssignment);

		//Now make it so only administrators can modify assignments and rules
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
		SystemCondition systemCondition = this.systemConditionService.getSystemConditionByName("Current User is a Member of Administrator Group");
		noTradeRule.setEntityModifyCondition(systemCondition);
		this.complianceRuleService.saveComplianceRule(noTradeRule);
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD);

		Exception error = null;
		try {
			noTradeRule.setBatch(false);
			this.complianceRuleService.saveComplianceRule(noTradeRule);
		}
		catch (Exception e) {
			error = e;
		}
		ValidationUtils.assertNotNull(error, "Error: was able to modify global no trade rule as a non administrator");
		error = null;

		try {
			noTradeGlobalAssignment.setNote("TEST");
			this.complianceRuleService.saveComplianceRuleAssignment(noTradeGlobalAssignment);
		}
		catch (Exception e) {
			error = e;
		}
		ValidationUtils.assertNotNull(error, "Error: was able to modify global no trade rule assignment as a non administrator");


		// Clean up - Switch Back to admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
		// Clean Up All Security Permissions for the test user
		this.userUtils.removeAllPermissionsForUser(ImsTestProperties.TEST_USER_2);

		//Test to make sure changes are allowed with admin only condition set while being admin
		noTradeRule.setBatch(false);
		noTradeRule.setEntityModifyCondition(this.systemConditionService.getSystemConditionByName("Current User is a Member of Administrator Group"));
		this.complianceRuleService.saveComplianceRule(noTradeRule);
		noTradeRule.setBatch(true);
		this.complianceRuleService.saveComplianceRule(noTradeRule);
		noTradeGlobalAssignment.setNote("TEST");
		this.complianceRuleService.saveComplianceRuleAssignment(noTradeGlobalAssignment);
		noTradeGlobalAssignment.setNote("");
		this.complianceRuleService.saveComplianceRuleAssignment(noTradeGlobalAssignment);
	}
}
