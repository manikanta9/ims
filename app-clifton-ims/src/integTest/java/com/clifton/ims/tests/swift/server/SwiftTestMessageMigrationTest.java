package com.clifton.ims.tests.swift.server;

import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;


public class SwiftTestMessageMigrationTest extends BaseSwiftTestMessageTest {

	@Test
	public void swiftSourceSystemTest() {
		ApplicationContext applicationContext = getRunnerContext();
		JdbcTemplate jdbcTemplate = new JdbcTemplate(applicationContext.getBean(DataSource.class));
		List<Map<String, Object>> sourceSystems = jdbcTemplate.queryForList("SELECT * FROM SwiftSourceSystem");
		Assertions.assertNotNull(sourceSystems);
		Assertions.assertFalse(sourceSystems.isEmpty());
		MatcherAssert.assertThat(sourceSystems.get(0).get("SOURCESYSTEMNAME"), IsEqual.equalTo("IMS"));
	}
}
