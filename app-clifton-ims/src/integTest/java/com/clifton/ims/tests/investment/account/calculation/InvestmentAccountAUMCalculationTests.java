package com.clifton.ims.tests.investment.account.calculation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.calculation.InvestmentAccountCalculationService;
import com.clifton.investment.account.calculation.InvestmentAccountCalculationType;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorCommand;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorService;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotService;
import com.clifton.investment.account.calculation.snapshot.search.InvestmentAccountCalculationSnapshotSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class InvestmentAccountAUMCalculationTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentAccountCalculationProcessorService investmentAccountCalculationProcessorService;

	@Resource
	private InvestmentAccountCalculationService investmentAccountCalculationService;

	@Resource
	private InvestmentAccountCalculationSnapshotService investmentAccountCalculationSnapshotService;

	private Short aumCalculationTypeId = null;

	// Comparison from actual to calculated values must be less than 0.001% Difference - allows for rounding differences between old SQL and new Code calculations
	private static final BigDecimal THRESHOLD_PERCENT = new BigDecimal("0.001");

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	// Not an Active Test - Can be used locally for individual dates when making a change to confirm the affect doesn't break other accounts
	public void previewAllForDateTest() {
		String date = "02/28/2018";
		InvestmentAccountCalculationSnapshotSearchForm searchForm = new InvestmentAccountCalculationSnapshotSearchForm();
		searchForm.setSnapshotDate(DateUtils.toDate(date));

		List<InvestmentAccountCalculationSnapshot> snapshotList = this.investmentAccountCalculationSnapshotService.getInvestmentAccountCalculationSnapshotList(searchForm, false);
		StringBuilder results = new StringBuilder(50);
		for (InvestmentAccountCalculationSnapshot snapshot : CollectionUtils.getIterable(snapshotList)) {
			validateInvestmentAccountCalculationSnapshot(snapshot.getInvestmentAccount().getNumber(), date, CoreMathUtils.formatNumber(snapshot.getCalculatedValue(), "####.##"), CoreMathUtils.formatNumber(snapshot.getCalculatedValueBase(), "####.##"), results);
		}
		evaluateResults(results);
	}


	@Test
	public void testNotional() {
		validateInvestmentAccountCalculationSnapshot("250000", "08/31/2017", "41632410.63", "41632410.63");
		validateInvestmentAccountCalculationSnapshot("660590", "08/31/2017", "12277352193.21", "12277352193.21");

		validateInvestmentAccountCalculationSnapshot("250000", "04/30/2017", "12087295.52", "12087295.52");
		validateInvestmentAccountCalculationSnapshot("660590", "04/30/2017", "0", "0");
	}


	@Test
	public void testDurationAdjustedNotional() {
		// Asset class benchmark was update from "50% LBUSTRUU/50% LGC5TRUU" to "LBUSTRUU" February 2021, so updated the expected values
		validateInvestmentAccountCalculationSnapshot("662901", "04/30/2017", "99699817.68", "99699817.68");
		validateInvestmentAccountCalculationSnapshot("662901", "08/31/2017", "105792972.50", "105792972.50");
	}


	@Test
	public void testPositionMarketValue() {
		validateInvestmentAccountCalculationSnapshot("158801", "04/30/2017", "327313013.36", "327313013.36");
		validateInvestmentAccountCalculationSnapshot("502121", "04/30/2017", "46254502.46", "46254502.46");
		validateInvestmentAccountCalculationSnapshot("522454", "04/30/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("522708", "04/30/2017", "1267733.3", "1267733.3");
		validateInvestmentAccountCalculationSnapshot("663131", "04/30/2017", "10022663.06", "10022663.06");

		validateInvestmentAccountCalculationSnapshot("158801", "08/31/2017", "314218574.58", "314218574.58");
		validateInvestmentAccountCalculationSnapshot("502121", "08/31/2017", "49021382.15", "49021382.15");
		validateInvestmentAccountCalculationSnapshot("522454", "08/31/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("522708", "08/31/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("663131", "08/31/2017", "10033660.86", "10033660.86");
	}


	@Test
	public void testMarketValue_ABSCash() {
		validateInvestmentAccountCalculationSnapshot("050905", "08/31/2017", "162377596.08", "162377596.08");
		validateInvestmentAccountCalculationSnapshot("073501", "08/31/2017", "23568241.46", "23568241.46");
		validateInvestmentAccountCalculationSnapshot("556800", "08/31/2017", "1035078.98", "1035078.98");
		validateInvestmentAccountCalculationSnapshot("800010-11", "08/31/2017", "3939280.24", "3939280.24");

		// Contains Negative Cash and Cash Collateral
		validateInvestmentAccountCalculationSnapshot("428600", "08/31/2017", "121067961.10", "121067961.10");
		// Should be zero - only include cash if position value is not zero
		validateInvestmentAccountCalculationSnapshot("382510", "08/31/2017", "0", "0");
		// Receivables
		validateInvestmentAccountCalculationSnapshot("663120", "08/31/2017", "5925533.74", "5925533.74");
		// Cash Collateral
		validateInvestmentAccountCalculationSnapshot("524102", "08/31/2017", "33070693.58", "33070693.58");

		// NOTE: THIS ACCOUNT IS THE ONLY ONE OFF FOR 8/31 AND ITS OFF BY 23.77 IT IS WITHIN THE THRESHOLD SO TEST ALLOWS IT
		validateInvestmentAccountCalculationSnapshot("122511", "08/31/2017", "16344106.11", "16344106.11");

		validateInvestmentAccountCalculationSnapshot("800020-133", "04/30/2017", "9598176.15", "9598176.15");
		validateInvestmentAccountCalculationSnapshot("115200", "04/30/2017", "68789288.37", "68789288.37");
		validateInvestmentAccountCalculationSnapshot("221770", "04/30/2017", "717053.12", "717053.12");
		validateInvestmentAccountCalculationSnapshot("382510", "04/30/2017", "0", "0");
	}


	@Test
	public void testMarketValue_Not_ABSCash() {
		validateInvestmentAccountCalculationSnapshot("668473", "08/14/2019", "51931138.43", "51931138.43");
		validateInvestmentAccountCalculationSnapshot("900030", "09/11/2019", "116465003.35", "116465003.35");
	}


	@Test
	public void testNotionalFutures_MarketValueNonFutures() {
		validateInvestmentAccountCalculationSnapshot("524000", "08/31/2017", "99204346.15", "99204346.15");
		validateInvestmentAccountCalculationSnapshot("524100", "08/31/2017", "386053279.45", "386053279.45");
		validateInvestmentAccountCalculationSnapshot("630000", "08/31/2017", "91560604.6", "91560604.6");
	}


	@Test
	public void testNotionalCurrencyForwards_And_SyntheticExposure() {
		// GBP (Uses Goldman Sachs)
		validateInvestmentAccountCalculationSnapshot("051100", "11/30/2019", "259826835.07", "336086011.17");

		validateInvestmentAccountCalculationSnapshot("051100", "12/31/2019", "206932205.13", "274133438.74");
	}


	@Test
	public void testSyntheticExposure() {
		// Holds Shorts
		validateInvestmentAccountCalculationSnapshot("050952", "08/31/2017", "27442788.75", "27442788.75");
		// Contains Virtual Contracts
		validateInvestmentAccountCalculationSnapshot("050950", "08/31/2017", "28888669.07", "28888669.07");

		// Value comes from Sub-Account values from Main Account Run
		validateInvestmentAccountCalculationSnapshot("161000", "08/31/2017", "14590030.02", "14590030.02");
		// Value comes from Sub-Account values from Main Account Run - But 0 Value
		validateInvestmentAccountCalculationSnapshot("331505", "08/31/2017", "0", "0");

		// CAD (Uses Morgan Stanley FX Rate for Client Account)
		validateInvestmentAccountCalculationSnapshot("040200", "08/31/2017", "203715754.67", "163031295.00");

		// JPY
		validateInvestmentAccountCalculationSnapshot("521802", "08/31/2017", "0", "0");

		// Account that was de-activated as of 9/15 - was active on 8/31 so should still create snapshot
		validateInvestmentAccountCalculationSnapshot("606550", "08/31/2017", "34603100", "34603100");

		// Currency Balance and Short Exposure - should be ABS + ABS
		validateInvestmentAccountCalculationSnapshot("400100", "04/30/2017", "37293707.37", "37293707.37");

		validateInvestmentAccountCalculationSnapshot("050950", "04/30/2017", "45693991.96", "45693991.96");
		// CAD (Uses Morgan Stanley FX Rate for Client Account)
		validateInvestmentAccountCalculationSnapshot("040200", "04/30/2017", "235851790.8", "172532400.00");
		validateInvestmentAccountCalculationSnapshot("079200", "04/30/2017", "82561442.33", "82561442.33");
		validateInvestmentAccountCalculationSnapshot("187148", "04/30/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("187150", "04/30/2017", "40858308.28", "40858308.28");
		validateInvestmentAccountCalculationSnapshot("521000", "04/30/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("662900", "04/30/2017", "694310500", "694310500");
	}


	@Test
	public void testSyntheticExposure_FuturesOnly() {

		validateInvestmentAccountCalculationSnapshot("720030", "04/30/2017", "93004646.05", "93004646.05");

		validateInvestmentAccountCalculationSnapshot("720030", "08/31/2017", "95653221.21", "95653221.21");
	}


	@Test
	public void testLDI_KRDExposure() {
		validateInvestmentAccountCalculationSnapshot("522450", "04/30/2017", "1662914554.76", "1662914554.76");
		validateInvestmentAccountCalculationSnapshot("522460", "04/30/2017", "475228880.19", "475228880.19");
		validateInvestmentAccountCalculationSnapshot("661000", "04/30/2017", "189586128.62", "189586128.62");

		validateInvestmentAccountCalculationSnapshot("400400", "08/31/2017", "132201671.01", "132201671.01");
		validateInvestmentAccountCalculationSnapshot("400420", "08/31/2017", "137059095.86", "137059095.86");
		validateInvestmentAccountCalculationSnapshot("522450", "08/31/2017", "1823943852.62", "1823943852.62");
		validateInvestmentAccountCalculationSnapshot("522460", "08/31/2017", "470575755.33", "470575755.33");
		validateInvestmentAccountCalculationSnapshot("661000", "08/31/2017", "210656090.95", "210656090.95");
	}


	@Test
	public void testLDI_OverlayBalance() {
		validateInvestmentAccountCalculationSnapshot("050951", "08/31/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("182301", "08/31/2017", "0", "0");
		// CAD - Uses Morgan Stanley Rate for Client Account
		validateInvestmentAccountCalculationSnapshot("188451", "08/31/2017", "57097250", "45694249.93");
		validateInvestmentAccountCalculationSnapshot("458810", "08/31/2017", "212342500", "212342500");
		validateInvestmentAccountCalculationSnapshot("661260", "08/31/2017", "39042812.5", "39042812.5");
		validateInvestmentAccountCalculationSnapshot("668000", "08/31/2017", "133801718.75", "133801718.75");

		validateInvestmentAccountCalculationSnapshot("050951", "04/30/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("182301", "04/30/2017", "0", "0");
		// CAD Uses Morgan Stanley Rate for Client Account
		validateInvestmentAccountCalculationSnapshot("188451", "04/30/2017", "75362400", "55129773.23");
		validateInvestmentAccountCalculationSnapshot("458810", "04/30/2017", "183304687.5", "183304687.5");
		validateInvestmentAccountCalculationSnapshot("661260", "04/30/2017", "33638687.5", "33638687.5");
		validateInvestmentAccountCalculationSnapshot("668000", "04/30/2017", "121423156.25", "121423156.25");
	}


	@Test
	public void testManagerCash_OverlayRun() {
		validateInvestmentAccountCalculationSnapshot("071000", "08/31/2017", "9579333.21", "9579333.21");
		validateInvestmentAccountCalculationSnapshot("071400", "08/31/2017", "14999680.08", "14999680.08");
		validateInvestmentAccountCalculationSnapshot("071500", "08/31/2017", "48275898.98", "48275898.98");

		validateInvestmentAccountCalculationSnapshot("071000", "04/30/2017", "18494753.8", "18494753.8");
		validateInvestmentAccountCalculationSnapshot("071400", "04/30/2017", "22865231", "22865231");
		validateInvestmentAccountCalculationSnapshot("071500", "04/30/2017", "96484610", "96484610");
	}


	@Test
	public void testManagerBalance() {
		validateInvestmentAccountCalculationSnapshot("576703", "04/30/2017", "75000000", "75000000");
		validateInvestmentAccountCalculationSnapshot("576703", "08/31/2017", "75000000", "75000000");
	}


	@Test
	public void testOptionsNotional() {
		validateInvestmentAccountCalculationSnapshot("605400", "08/31/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("605411", "08/31/2017", "110729920", "110729920");
		validateInvestmentAccountCalculationSnapshot("605421", "08/31/2017", "47498362.6", "47498362.6");
		validateInvestmentAccountCalculationSnapshot("605500", "08/31/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("605511", "08/31/2017", "188339730", "188339730");
		validateInvestmentAccountCalculationSnapshot("605521", "08/31/2017", "80381844.4", "80381844.4");

		validateInvestmentAccountCalculationSnapshot("605400", "04/30/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("605411", "04/30/2017", "119925260", "119925260");
		validateInvestmentAccountCalculationSnapshot("605421", "04/30/2017", "49575151.2", "49575151.2");
		validateInvestmentAccountCalculationSnapshot("605500", "04/30/2017", "0", "0");
		validateInvestmentAccountCalculationSnapshot("605511", "04/30/2017", "185252340", "185252340");
		validateInvestmentAccountCalculationSnapshot("605521", "04/30/2017", "78704053.6", "78704053.6");
	}


	@Test
	public void testOptionsNotional_PutOrCall() {
		// Note: This account is no longer active, so need to go back to an earlier date
		validateInvestmentAccountCalculationSnapshot("576613", "03/31/2015", "30912000", "30912000");
		// Options have been removed
		validateInvestmentAccountCalculationSnapshot("576613", "06/30/2015", "0", "0");
	}


	@Test
	public void testSwapsAdjustedNotional() {
		// Note: These accounts are no longer active, so need to go back to an earlier date
		validateInvestmentAccountCalculationSnapshot("580001", "08/31/2016", "25000000", "25000000");
		validateInvestmentAccountCalculationSnapshot("580002", "03/31/2015", "50000000", "50000000");
	}


	@Test
	public void testSwapsAdjustedNotional_NetByUnderlying() {
		validateInvestmentAccountCalculationSnapshot("185580", "02/28/2018", "651385449.23", "651385449.23");
		validateInvestmentAccountCalculationSnapshot("187111", "02/28/2018", "1739336139.55", "1739336139.55");
		validateInvestmentAccountCalculationSnapshot("187190", "02/28/2018", "551403064.86", "551403064.86");
		validateInvestmentAccountCalculationSnapshot("222491", "02/28/2018", "322791606.30", "322791606.30");
	}


	@Test
	public void testSwaptionsAdjustedDeltaNotional() {
		validateInvestmentAccountCalculationSnapshot("187323", "02/28/2018", "8467813.22", "8467813.22");
	}


	@Test
	public void testSyntheticExposure_Futures_SwapsAdjustedNotional() {
		validateInvestmentAccountCalculationSnapshot("078161", "08/31/2017", "40172543.54", "40172543.54");
		validateInvestmentAccountCalculationSnapshot("221700", "08/31/2017", "32799811.91", "32799811.91");
		validateInvestmentAccountCalculationSnapshot("221800", "08/31/2017", "545834478.46", "545834478.46");

		validateInvestmentAccountCalculationSnapshot("078161", "04/30/2017", "39733148.77", "39733148.77");
		validateInvestmentAccountCalculationSnapshot("221700", "04/30/2017", "54397866.84", "54397866.84");
		validateInvestmentAccountCalculationSnapshot("221800", "04/30/2017", "444503629.69", "444503629.69");
	}


	@Test
	public void testMacArthurRollup() {
		// Rollup Calculation used for MacArthur Accounts: Synthetic Exposure of Futures + Positions Market Value (Includes Collateral) of US T-Notes/Bonds + Notional of Swaps - TRS + Synthetic Exposure of Currency Forwards + Notional (Dirty) of Swaps CDS + Vega Notional (Variance Swaps)
		validateInvestmentAccountCalculationSnapshot("385400", "10/31/2017", "247418750.32", "247418750.32");
		// validateInvestmentAccountCalculationSnapshot("385416", "10/31/2017", "1968916915", "1968916915");
		validateInvestmentAccountCalculationSnapshot("385417", "10/31/2017", "493929299.04", "493929299.04");
		// validateInvestmentAccountCalculationSnapshot("385420", "10/31/2017", "673536412.18", "673536412.18");
		validateInvestmentAccountCalculationSnapshot("385421", "10/31/2017", "647609019.85", "647609019.85");
		validateInvestmentAccountCalculationSnapshot("385460", "10/31/2017", "36278823.12", "36278823.12");
		validateInvestmentAccountCalculationSnapshot("385470", "10/31/2017", "59059290", "59059290");
		validateInvestmentAccountCalculationSnapshot("385480", "10/31/2017", "671927037.92", "671927037.92");
		validateInvestmentAccountCalculationSnapshot("385490", "10/31/2017", "768047204", "768047204");
	}


	@Test
	public void testExternal() {
		// External tests that a zero record is created, used mostly by DUMAC.  However more of their accounts are being switched to
		// real IMS calculators.  Using 187261 account as a test account since it is currently paused (as of 2/5/2018) so likely
		// won't be switched to a different calculator any time soon.
		validateInvestmentAccountCalculationSnapshot("187261", "08/31/2017", "0", "0");
	}


	@Test
	public void testSecurityTarget() {
		// Saugatuck Account # 1015008759
		validateInvestmentAccountCalculationSnapshot("W-000955", "02/29/2016", "468823.04", "468823.04");
		validateInvestmentAccountCalculationSnapshot("W-000955", "12/31/2016", "589310.46", "589310.46");
		validateInvestmentAccountCalculationSnapshot("W-000955", "10/31/2018", "432379.80", "432379.80");

		// Saugatuck Account # 8156-6769
		validateInvestmentAccountCalculationSnapshot("W-001503", "08/31/2017", "541666.66", "551666.66");
		validateInvestmentAccountCalculationSnapshot("W-001503", "02/28/2018", "2593750.00", "2593750.00");
		validateInvestmentAccountCalculationSnapshot("W-001503", "08/31/2018", "1500000.00", "1500000.00");
	}


	@Test
	public void testPositionMarketValueEquitiesOnly() {
		// Saugatuck Account # 240273
		validateInvestmentAccountCalculationSnapshot("W-001952", "08/31/2018", "11211564.72", "11211564.72");
		validateInvestmentAccountCalculationSnapshot("W-001952", "09/30/2018", "11222682.74", "11222682.74");
		validateInvestmentAccountCalculationSnapshot("W-001952", "10/31/2018", "10457072.88", "10457072.88");
	}


	@Test
	public void testSecurityTargetAndMarketValue() {
		// Saugatuck Account # 033-182841; account was terminated re-activate
		this.investmentAccountUtils.reactivateClientAccount("W-000244", new Date());
		validateInvestmentAccountCalculationSnapshot("W-000244", "09/30/2018", "97166880.36", "97166880.36");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void validateInvestmentAccountCalculationSnapshot(String accountNumber, String snapshotDate, String calculatedValue, String calculatedValueBase) {
		validateInvestmentAccountCalculationSnapshot(accountNumber, snapshotDate, calculatedValue, calculatedValueBase, null);
	}


	protected void validateInvestmentAccountCalculationSnapshot(String accountNumber, String snapshotDate, String calculatedValue, String calculatedValueBase, StringBuilder results) {
		if (this.aumCalculationTypeId == null) {
			List<InvestmentAccountCalculationType> typeList = this.investmentAccountCalculationService.getInvestmentAccountCalculationTypeList();
			InvestmentAccountCalculationType type = CollectionUtils.getOnlyElementStrict(BeanUtils.filter(typeList, InvestmentAccountCalculationType::getName, "AUM"));
			this.aumCalculationTypeId = type.getId();
		}
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
		InvestmentAccountCalculationProcessorCommand command = new InvestmentAccountCalculationProcessorCommand();
		command.setCalculationTypeId(this.aumCalculationTypeId);
		command.setInvestmentAccountId(account.getId());
		command.setSnapshotDate(DateUtils.toDate(snapshotDate));

		BigDecimal expectedCalculatedValue = MathUtils.round(new BigDecimal(calculatedValue), 2);
		BigDecimal expectedCalculatedValueBase = MathUtils.round(new BigDecimal(calculatedValueBase), 2);

		try {
			InvestmentAccountCalculationSnapshot snapshot = this.investmentAccountCalculationProcessorService.previewInvestmentAccountCalculationSnapshot(command);
			BigDecimal actualCalculatedValueBase = MathUtils.multiply(snapshot.getCalculatedValue(), snapshot.getFxRateToBase());

			if (results == null) {
				Assertions.assertTrue(isValueComparisonWithinThreshold(expectedCalculatedValue, snapshot.getCalculatedValue()), snapshot.getLabel() + " Expected Calculated Value: " + CoreMathUtils.formatNumberMoney(expectedCalculatedValue) + ", Actual Calculated Value: " + CoreMathUtils.formatNumberMoney(snapshot.getCalculatedValue()));
				// Database Calculated Column - so not persisted on a "new" preview bean
				Assertions.assertTrue(isValueComparisonWithinThreshold(expectedCalculatedValueBase, actualCalculatedValueBase), snapshot.getLabel() + " Expected Calculated Base Value: " + CoreMathUtils.formatNumberMoney(expectedCalculatedValueBase) + ", Actual Calculated Base Value: " + CoreMathUtils.formatNumberMoney(actualCalculatedValueBase));
			}
			else {
				if (!isValueComparisonWithinThreshold(expectedCalculatedValue, snapshot.getCalculatedValue()) || !isValueComparisonWithinThreshold(expectedCalculatedValueBase, actualCalculatedValueBase)) {
					results.append(StringUtils.NEW_LINE);
					results.append(snapshotDate);
					results.append(StringUtils.TAB);
					results.append(accountNumber);
					results.append(StringUtils.TAB);
					results.append(account.getAumCalculatorBean() == null ? "" : account.getAumCalculatorBean().getLabelShort());
					results.append(StringUtils.TAB);
					results.append("Did Not Match Values");
					results.append(StringUtils.TAB);
					results.append(CoreMathUtils.formatNumberMoney(expectedCalculatedValue));
					results.append(StringUtils.TAB);
					results.append(CoreMathUtils.formatNumberMoney(snapshot.getCalculatedValue()));
					results.append(StringUtils.TAB);
					results.append(CoreMathUtils.formatNumberMoney(expectedCalculatedValueBase));
					results.append(StringUtils.TAB);
					results.append(CoreMathUtils.formatNumberMoney(MathUtils.round(MathUtils.multiply(snapshot.getCalculatedValue(), snapshot.getFxRateToBase()), 2)));
				}
			}
		}
		catch (Exception e) {
			if (results == null) {
				throw e;
			}
			results.append(StringUtils.NEW_LINE);
			results.append(snapshotDate);
			results.append(StringUtils.TAB);
			results.append(accountNumber);
			results.append(StringUtils.TAB);
			results.append(account.getAumCalculatorBean() == null ? "" : account.getAumCalculatorBean().getLabelShort());
			results.append(StringUtils.TAB);
			results.append(ExceptionUtils.getOriginalMessage(e).replace("Error was returned from IMS:", ""));
			results.append(StringUtils.TAB);
			results.append(CoreMathUtils.formatNumberMoney(expectedCalculatedValue));
			results.append(StringUtils.TAB);
			results.append(StringUtils.TAB);
			results.append(CoreMathUtils.formatNumberMoney(expectedCalculatedValueBase));
			results.append(StringUtils.TAB);
		}
	}


	private boolean isValueComparisonWithinThreshold(BigDecimal expectedValue, BigDecimal calculatedValue) {
		BigDecimal difference = MathUtils.subtract(MathUtils.round(expectedValue, 2), MathUtils.round(calculatedValue, 2));
		if (!MathUtils.isNullOrZero(difference)) {
			BigDecimal percentDifference = MathUtils.getPercentChange(expectedValue, calculatedValue, true);
			//System.out.println("Expected vs. Calculated Comparison % Difference: " + CoreMathUtils.formatNumberDecimal(percentDifference));
			return MathUtils.isLessThan(percentDifference, THRESHOLD_PERCENT);
		}
		return true;
	}


	protected void evaluateResults(StringBuilder results) {
		if (results.length() > 0) {
			StringBuilder header = new StringBuilder(100);
			header.append("Date");
			header.append(StringUtils.TAB);
			header.append("Account #");
			header.append(StringUtils.TAB);
			header.append("AUM Calculation Type");
			header.append(StringUtils.TAB);
			header.append("AUM Calculator Bean");
			header.append(StringUtils.TAB);
			header.append("Error Message");
			header.append(StringUtils.TAB);
			header.append("Expected Calculated Value");
			header.append(StringUtils.TAB);
			header.append("Calculated Value");
			header.append(StringUtils.TAB);
			header.append("Expected Base Value");
			header.append(StringUtils.TAB);
			header.append("Base Value");
			System.out.println(new StringBuilder().append(header).append(results));
			Assertions.fail("Invalid calculation results found.  See console for details");
		}
	}
}
