package com.clifton.ims.tests.rule.investment;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.product.manager.ProductManagerService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class InvestmentManagerAccountBalanceRuleTests extends RuleTests {

	@Resource
	InvestmentCalculatorService investmentCalculatorService;
	@Resource
	InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	@Resource
	MarketDataFieldService marketDataFieldService;
	@Resource
	ProductManagerService productManagerService;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Manager Account Balance Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return new HashSet<>();
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCashPercentageAdjustmentsRule() {
		InvestmentManagerAccountBalanceSearchForm investmentManagerAccountBalanceSearchForm = new InvestmentManagerAccountBalanceSearchForm();
		investmentManagerAccountBalanceSearchForm.setAccountNameNumber("M04793");
		investmentManagerAccountBalanceSearchForm.setBalanceDate(DateUtils.toDate("06/27/2011"));
		InvestmentManagerAccountBalance accountBalance =
				CollectionUtils.getFirstElementStrict(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceList(investmentManagerAccountBalanceSearchForm));

		InvestmentManagerAccount managerAccount = accountBalance.getManagerAccount();
		if (managerAccount.getCashPercentManager() != null) {
			InvestmentManagerAccountBalance cashPercentManagerBalance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(
					managerAccount.getCashPercentManager().getId(), accountBalance.getBalanceDate(), false);
			if (cashPercentManagerBalance != null) {
				this.investmentManagerAccountBalanceService.deleteInvestmentManagerAccountBalance(cashPercentManagerBalance.getId());
			}
		}
		//if cashPercentManager was null, let it fail.
		validateTestMethodResultWithExistingAccountBalance(accountBalance.getId(), "Cash Percent Manager " + managerAccount.getCashPercentManager() + " balance missing - cannot determine current cash percentage.");
	}


	@Test
	public void testM2MAdjustmentsRule() {
		InvestmentManagerAccountBalanceSearchForm investmentManagerAccountBalanceSearchForm = new InvestmentManagerAccountBalanceSearchForm();
		investmentManagerAccountBalanceSearchForm.setAccountNameNumber("M04276");
		investmentManagerAccountBalanceSearchForm.setBalanceDate(DateUtils.toDate("11/29/2013"));
		InvestmentManagerAccountBalance accountBalance =
				CollectionUtils.getFirstElementStrict(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceList(investmentManagerAccountBalanceSearchForm));

		String expectedViolationNote = "M2M Balance History Missing for Accounts [[M04276: 428532] - [428530: National Electrical Benefit Fund - Dom Equity] - Gain/Loss (Futures Only)]";
		validateTestMethodResultWithExistingAccountBalance(accountBalance.getId(), expectedViolationNote);
	}


	@Test
	public void testProxyAdjustmentsRule() {
		InvestmentManagerAccountBalanceSearchForm investmentManagerAccountBalanceSearchForm = new InvestmentManagerAccountBalanceSearchForm();
		investmentManagerAccountBalanceSearchForm.setAccountNameNumber("M00359");
		investmentManagerAccountBalanceSearchForm.setBalanceDate(DateUtils.toDate("12/31/2015"));
		InvestmentManagerAccountBalance accountBalance =
				CollectionUtils.getFirstElementStrict(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceList(investmentManagerAccountBalanceSearchForm));

		InvestmentManagerAccount managerAccount = accountBalance.getManagerAccount();
		InvestmentSecurity security = managerAccount.getProxyBenchmarkSecurity();
		String proxyValueDate = DateUtils.fromDateShort(managerAccount.getProxyValueDate());

		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm();
		marketDataValueSearchForm.setInvestmentSecurityId(managerAccount.getProxyBenchmarkSecurity().getId());
		marketDataValueSearchForm.setMeasureDate(investmentManagerAccountBalanceSearchForm.getBalanceDate());
		marketDataValueSearchForm.setDataFieldName("Last Trade Price");
		MarketDataValue marketDataValue = CollectionUtils.getFirstElementStrict(this.marketDataFieldService.getMarketDataValueList(marketDataValueSearchForm));
		try {
			Assertions.assertNotNull(marketDataValue, "Unable to find " + marketDataValueSearchForm.getDataFieldName() + " value for " + managerAccount.getProxyBenchmarkSecurity() + " on date " + marketDataValueSearchForm.getMeasureDate());
			this.marketDataFieldService.deleteMarketDataValue(marketDataValue.getId());

			String expectedViolationNote = "Proxy Benchmark Security [" + security.getLabelWithBigSecurity() +
					"] price data [Last Trade Price] is missing on date [" + proxyValueDate + "]. " + proxyValueDate +
					" is not a holiday for the calendar [" + this.investmentCalculatorService.getInvestmentSecurityCalendar(security.getId()).getName() + "].";
			validateTestMethodResultWithExistingAccountBalance(accountBalance.getId(), expectedViolationNote);
		} finally {
			// add back the removed field value
			marketDataValue.setId(null);
			this.marketDataFieldService.saveMarketDataValue(marketDataValue);
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////             Test Assertion Methods              ////////////////
	/////////////////////////////////////////////////////////////////////////////


	private void validateTestMethodResultWithExistingAccountBalance(int accountBalanceId, String expectedViolationNote) {
		boolean foundMatchingViolation = false;
		//Process the account balance so that violations are created/updated/saved
		this.productManagerService.processProductManagerAccountBalanceAdjustmentListForBalances(new Integer[]{accountBalanceId});

		//Query for the violations - ensure they were recently updated.
		RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
		ruleViolationSearchForm.setLinkedFkFieldId(MathUtils.getNumberAsLong(accountBalanceId));
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm))) {
			if (DateUtils.isDateAfter(ruleViolation.getUpdateDate(), DateUtils.getPreviousWeekday(new Date()))) {
				if (expectedViolationNote.equals(ruleViolation.getViolationNote())) {
					foundMatchingViolation = true;
				}
			}
		}
		Assertions.assertTrue(foundMatchingViolation);
	}
}
