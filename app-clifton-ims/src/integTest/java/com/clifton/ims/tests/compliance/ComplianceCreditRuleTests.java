package com.clifton.ims.tests.compliance;


import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class ComplianceCreditRuleTests extends ComplianceRealTimeTradeTests {

	@Test
	public void testBasicCreditRatingRule() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, true, "Credit Rating A-1 or P-1 at Time of Purchase");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "088023EF9", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "760985A84", new BigDecimal(123), true, DateUtils.toDate("10/25/2016"));
		validateViolationNotes(tradeResults, "Credit Rating A-1 or P-1 at Time of Purchase : Security satisfies at least 1 of credit ratings [P-1 (Moody's: short-term), A-1 (Standard & Poor's: short-term)] : Failed");
	}
}
