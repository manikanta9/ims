package com.clifton.ims.tests.instruction.mt54x;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountRelationshipPurposes;
import com.clifton.ims.tests.investment.instruction.InvestmentInstructionUtils;
import com.clifton.ims.tests.trade.TradeCreator;
import com.clifton.ims.tests.trade.TradeUtils;
import com.clifton.ims.tests.utility.PeriodicRepeatingExecutor;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBusinessCompany;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBusinessCompanyService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.swift.server.SwiftTestMessageServer;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.autoclient.SwiftExpectedMessage;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import com.clifton.test.util.PropertiesLoader;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


public class MT542Test extends BaseImsIntegrationTest {

	private static final String CONTEXT_PROPERTIES_PATH = PropertiesLoader.getPropertiesFileUri();
	private static final String TEST_DATA_PATH = "com/clifton/ims/tests/swift/server/data";
	public static final String SYMBOL = "912810RW0";
	private static final String DELIVERY_NAME = MT542Test.class.getSimpleName();

	private static final String T_BILLS_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":98A::SETT//20180320\n" +
			":98A::TRAD//20180320\n" +
			":35B:ISIN US912810RW09\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/TIPS\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20470215\n" +
			":98A::MATU//20470215\n" +
			":98A::ISSU//20170228\n" +
			":92A::INTR//0,875\n" +
			":36B::SIZE//UNIT/0,01\n" +
			":16S:FIA\n" +
			":22F::PROC//OPEP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/9140000,\n" +
			":97A::SAFE//496015427\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//COLO\n" +
			":16R:SETPRTY\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95R::REAG/USFW/STATE\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD0,\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String T_BILLS_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I542PPSCUS66XXXXN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//1\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			T_BILLS_COMMON_SUFFIX;

	private static final String T_BILLS_RESPONSE = "{1:F21PPSCUS66AXXX0058058413}{4:{177:1708231442}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I542PPSCUS66XXXXN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//1\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			T_BILLS_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	@Resource
	private TradeCreator tradeCreator;

	@Resource
	protected TradeUtils tradeUtils;

	@Resource
	private TradeService tradeService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	@Resource
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	@Resource
	private AccountingPositionService accountingPositionService;

	@Resource
	private AccountingTransactionService accountingTransactionService;

	@Resource
	private AccountingBookingService<?> accountingBookingService;

	@Resource
	private InvestmentInstructionUtils investmentInstructionUtils;

	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	@Resource
	private InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService;


	private static SwiftTestMessageServer swiftTestMessageServer;

	private InvestmentInstructionCategory category;
	private AccountingPositionTransfer accountingPositionTransfer;

	private Set<InvestmentInstructionDeliveryBusinessCompany> investmentInstructionDeliveryBusinessCompanies = new HashSet<>();

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void beforeClass() throws Exception {
		swiftTestMessageServer = new SwiftTestMessageServer(CONTEXT_PROPERTIES_PATH, TEST_DATA_PATH);
		swiftTestMessageServer.start();
	}


	@AfterAll
	public static void afterClass() throws Exception {
		swiftTestMessageServer.shutdown();
	}


	@BeforeEach
	public void before() {
		Assertions.assertNotNull(swiftTestMessageServer.sendStartTestSession());
	}


	@AfterEach
	public void after() {
		swiftTestMessageServer.sendStopTestSession();

		InvestmentInstructionStatus errorStatus = this.investmentInstructionUtils.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);

		List<InvestmentInstructionItem> items = this.investmentInstructionUtils.getInstructionItemList(this.category, new Date(), this.accountingPositionTransfer).stream()
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.COMPLETED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.CANCELED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.ERROR.name()))
				.collect(Collectors.toList());
		for (InvestmentInstructionItem item : items) {
			item.setStatus(errorStatus);
			this.investmentInstructionUtils.saveInvestmentInstructionItem(item);
		}

		InvestmentInstructionDeliveryType deliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, false);
		InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setDeliveryTypeId(deliveryType.getId());
		CollectionUtils.getStream(this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm))
				.forEach(d -> {
					d.setDeliveryType(null);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		this.investmentInstructionDeliveryBusinessCompanies
				.forEach(d -> this.investmentInstructionDeliveryBusinessCompanyService.deleteInvestmentInstructionDeliveryBusinessCompany(d.getId()));
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void positionTransferTest() throws InterruptedException {
		Date tradeDate = DateUtils.toDate("11/30/2017", DateUtils.DATE_FORMAT_INPUT);
		Date todayDate = new Date();
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, BusinessCompanies.STATE_STREET_BANK,
				this.tradeUtils.getTradeType(TradeType.BONDS));
		InvestmentAccount holdingAccount = accountInfo.getHoldingAccount();
		this.investmentAccountUtils.createMarginHoldingAccount(accountInfo, accountInfo.getBusinessClient(), BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		accountInfo.setHoldingAccount(holdingAccount);
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createBondTrade(
				accountInfo,
				SYMBOL,
				DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT),
				new BigDecimal("9140000.00"),
				new BigDecimal("99.8125"),
				new BigDecimal("23998.55"),
				new BigDecimal("1.02248"),
				true,
				TradeDestination.BLOOMBERG_TICKET,
				BusinessCompanies.BARCLAYS_CAPITOL_INC);
		trade = this.approveTrade(trade);

		TradeFill tradeFill = new TradeFill();
		tradeFill.setTrade(trade);
		saveTradeFill(
				tradeFill,
				new BigDecimal("9140000.00"),
				new BigDecimal("99.8125"),
				new BigDecimal("9327944.45"),
				new BigDecimal("99.8125"),
				new BigDecimal("9327944.45")
		);

		trade = finishExecuteTrade(trade);
		trade = bookAndPostTrade(trade);
		Assertions.assertNotNull(trade);

		this.accountingPositionTransfer = createCollateralTransferPost(accountInfo.getClientAccount(), holdingAccount, todayDate,
				tradeDate, trade.getInvestmentSecurity());
		Assertions.assertNotNull(this.accountingPositionTransfer);

		AccountingJournal accountingJournal = this.accountingBookingService.bookAccountingJournal(AccountingJournalType.TRANSFER_JOURNAL,
				this.accountingPositionTransfer.getId(), true);
		Assertions.assertNotNull(accountingJournal);
		// setup test case
		swiftTestMessageServer.sendSwiftTestCase(SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(T_BILLS_REQUEST)
								.withReplacement("97A", ":SAFE//496015427", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.withReplacement("98A", ":TRAD//20180320", ":TRAD//" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
								.withReplacement("98A", ":SETT//20180320", ":SETT//" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(T_BILLS_RESPONSE)
								.withReplacement("97A", ":SAFE//496015427", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withCopyRequestValue("20C")
								.withReplacement("98A", ":TRAD//20180320", ":TRAD//" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
								.withReplacement("98A", ":SETT//20180320", ":SETT//" + LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE))
								.build()
				)
				.build());

		this.category = this.investmentInstructionUtils.getInvestmentInstructionCategoryByName("Position Transfer Counterparty");

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, tradeDate, null).isEmpty(),
				String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, todayDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, tradeDate, this.accountingPositionTransfer).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), tradeDate,
				this.accountingPositionTransfer.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, tradeDate), "Instruction Items are regenerating.");
		List<InvestmentInstructionItem> investmentInstructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, todayDate, this.accountingPositionTransfer))
				.collect(Collectors.toList());
		Assertions.assertFalse(investmentInstructionItems.isEmpty(), "Should have Open Instruction Items.");

		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// setup delivery type.
		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, true);
		Assertions.assertNotNull(investmentInstructionDeliveryType);
		InvestmentInstructionDelivery mellonInstructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
				BusinessCompanies.BANK_OF_NEW_YORK_MELLON_INTERMEDIARY), DELIVERY_NAME + " Mellon");
		if (Objects.isNull(mellonInstructionDelivery)) {
			mellonInstructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
					BusinessCompanies.BANK_OF_NEW_YORK_MELLON_INTERMEDIARY), DELIVERY_NAME + " Mellon", "89-0000-1111", "MELLON");
		}
		Assertions.assertNotNull(mellonInstructionDelivery);
		InvestmentInstructionDelivery stateStreetInstructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
				BusinessCompanies.STATE_STREET_BANK), DELIVERY_NAME + " State");
		if (Objects.isNull(stateStreetInstructionDelivery)) {
			stateStreetInstructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
					BusinessCompanies.STATE_STREET_BANK), DELIVERY_NAME + " State", "89-0000-2222", "STATE");
		}
		Assertions.assertNotNull(stateStreetInstructionDelivery);

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, todayDate, this.accountingPositionTransfer).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		Assertions.assertNotNull(investmentInstruction);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// make sure definitions have a delivery type
		investmentInstructionItems.stream()
				.map(InvestmentInstructionItem::getInstruction)
				.map(InvestmentInstruction::getDefinition)
				.filter(i -> Objects.isNull(i.getDeliveryType()))
				.forEach(d -> {
					d.setDeliveryType(investmentInstructionDeliveryType);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		linkDelivery(this.accountingPositionTransfer.getToHoldingInvestmentAccount().getIssuingCompany(), investmentInstructionDeliveryType, mellonInstructionDelivery);
		linkDelivery(this.accountingPositionTransfer.getFromHoldingInvestmentAccount().getIssuingCompany(), investmentInstructionDeliveryType, stateStreetInstructionDelivery);

		// send instructions
		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, todayDate, this.accountingPositionTransfer))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertNotEquals(0, instructionItems.length, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift to exhaust test case
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for complete status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, todayDate, this.accountingPositionTransfer,
				InvestmentInstructionStatusNames.COMPLETED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, todayDate, this.accountingPositionTransfer, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private void linkDelivery(BusinessCompany businessCompany, InvestmentInstructionDeliveryType investmentInstructionDeliveryType, InvestmentInstructionDelivery instructionDelivery) {
		InvestmentInstructionDeliveryBusinessCompanySearchForm linkSearchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		linkSearchForm.setBusinessCompanyId(businessCompany.getId());
		linkSearchForm.setTypeId(investmentInstructionDeliveryType.getId());
		linkSearchForm.setDeliveryCurrencyId(this.usd.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> links = this.investmentInstructionDeliveryBusinessCompanyService.getInvestmentInstructionDeliveryBusinessCompanyList(linkSearchForm);
		if (links.isEmpty()) {
			InvestmentInstructionDeliveryBusinessCompany link = new InvestmentInstructionDeliveryBusinessCompany();
			link.setDeliveryCurrency(this.usd);
			link.setInvestmentInstructionDeliveryType(investmentInstructionDeliveryType);
			link.setReferenceOne(instructionDelivery);
			link.setReferenceTwo(businessCompany);
			this.investmentInstructionDeliveryBusinessCompanyService.saveInvestmentInstructionDeliveryBusinessCompany(link);
			this.investmentInstructionDeliveryBusinessCompanies.add(link);
		}
	}


	private Trade approveTrade(Trade trade) {
		this.tradeUtils.approveTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private Trade finishExecuteTrade(Trade trade) {
		this.tradeUtils.finishExecutionTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private Trade bookAndPostTrade(Trade trade) {
		this.tradeUtils.bookAndPostTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private TradeFill saveTradeFill(TradeFill tradeFill, BigDecimal quantity, BigDecimal notionalUnitPrice, BigDecimal notionalTotalPrice, BigDecimal paymentUnitPrice, BigDecimal paymentTotalPrice) {
		tradeFill.setQuantity(quantity);
		tradeFill.setNotionalUnitPrice(notionalUnitPrice);
		tradeFill.setNotionalTotalPrice(notionalTotalPrice);
		tradeFill.setPaymentUnitPrice(paymentUnitPrice);
		tradeFill.setPaymentTotalPrice(paymentTotalPrice);
		return this.tradeService.saveTradeFill(tradeFill);
	}


	private AccountingPositionTransfer createCollateralTransferPost(InvestmentAccount clientAccount, InvestmentAccount holdingAccount, Date transactionDate, Date tradeDate, InvestmentSecurity investmentSecurity) {
		AccountingPositionTransferType accountingPositionTransferType = this.accountingPositionTransferService.getAccountingPositionTransferTypeByName(AccountingPositionTransferType.COLLATERAL_TRANSFER_POST);
		Assertions.assertNotNull(accountingPositionTransferType);

		AccountingPositionTransfer accountingPositionTransfer = new AccountingPositionTransfer();
		accountingPositionTransfer.setType(accountingPositionTransferType);
		accountingPositionTransfer.setTransactionDate(transactionDate);
		accountingPositionTransfer.setSettlementDate(transactionDate);
		accountingPositionTransfer.setCollateralTransfer(true);
		accountingPositionTransfer.setNote("com.clifton.ims.tests.instruction.mt54x.MT542Test.positionTransferTest");
		accountingPositionTransfer.setUseOriginalTransactionDate(true);

		accountingPositionTransfer.setFromClientInvestmentAccount(clientAccount);
		InvestmentAccount fromAccount = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccount.getId(),
				InvestmentAccountRelationshipPurposes.CUSTODIAN.getName(), null);
		Assertions.assertNotNull(fromAccount);
		accountingPositionTransfer.setFromHoldingInvestmentAccount(fromAccount);

		accountingPositionTransfer.setToClientInvestmentAccount(clientAccount);
		InvestmentAccount toAccount = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccount.getId(),
				InvestmentAccountRelationshipPurposes.TRADING_OPTIONS.getName(), null);
		Assertions.assertNotNull(toAccount);
		accountingPositionTransfer.setToHoldingInvestmentAccount(toAccount);

		List<AccountingPosition> accountingPositionList = this.accountingPositionService.getAccountingPositionList(clientAccount.getId(),
				holdingAccount.getId(), investmentSecurity.getId(), tradeDate);
		Assertions.assertEquals(1, accountingPositionList.size());
		AccountingPosition accountingPosition = accountingPositionList.get(0);
		AccountingTransaction accountingTransaction = this.accountingTransactionService.getAccountingTransaction(accountingPosition.getId());
		Assertions.assertNotNull(accountingTransaction);

		AccountingPositionTransferDetail accountingPositionTransferDetail = new AccountingPositionTransferDetail();
		accountingPositionTransferDetail.setExistingPosition(accountingTransaction);
		accountingPositionTransferDetail.setSecurity(investmentSecurity);
		accountingPositionTransferDetail.setOriginalPositionOpenDate(tradeDate);
		accountingPositionTransferDetail.setExchangeRateToBase(accountingTransaction.getExchangeRateToBase());
		accountingPositionTransferDetail.setCostPrice(accountingPosition.getPrice());
		accountingPositionTransferDetail.setTransferPrice(accountingPosition.getPrice());
		accountingPositionTransferDetail.setQuantity(accountingPosition.getQuantity());
		accountingPositionTransferDetail.setPositionCostBasis(accountingPosition.getPositionCostBasis());
		accountingPositionTransfer.setDetailList(Collections.singletonList(accountingPositionTransferDetail));

		this.accountingPositionTransferService.saveAccountingPositionTransfer(accountingPositionTransfer);

		return accountingPositionTransfer;
	}
}
