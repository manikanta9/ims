package com.clifton.ims.tests.compliance;


import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class ComplianceShortLongTests extends ComplianceRealTimeTradeTests {

	@Test
	public void testSingleAccountFutures() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLK6");
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Bonds (Central Govt - Inflation Linked - TIPS) - All (Long Only)", "Permitted Security: Futures (Currency) - All (Long or Short)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, security.getSymbol(), new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Exclude All Positions : 123 Contracts for " + security.getSymbol() + " open on " + DateUtils.fromDateShort(new Date()) + " : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "CDU4", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testSingleAccountBonds() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.BONDS, "Permitted Security: Bonds (Central Govt - Inflation Linked - TIPS) - All (Long Only)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.BONDS, "912828S50", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testExhaustiveLong() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testShortPosition() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only) : -123 Contracts for NQM15 open on " + DateUtils.fromDateShort(new Date()) + " : Long Allowed : Failed");
	}


	@Test
	public void testLongToShort() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		//Now bring it back down with sells
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM5", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);

		//We just closed our position on NQM15.  Let's try another sell which will bring us into a short position. This is not allowed in this case
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only) : -123 Contracts for NQM15 open on " + DateUtils.fromDateShort(new Date()) + " : Long Allowed : Failed");
	}


	@Test
	public void testShortToLongWithRejects() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only)");

		//Try to short a couple times up front. Shouldn't be able to
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only) : -123 Contracts for NQM15 open on " + DateUtils.fromDateShort(new Date()) + " : Long Allowed : Failed");
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only) : -246 Contracts for NQM15 open on " + DateUtils.fromDateShort(new Date()) + " : Long Allowed : Failed");

		//Several buys, long should be allowed
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		//Sell some
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);

		//Buy Some
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		//Close position by selling all
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);

		//Go short, should not be allowed: AUTO REJECT THIS TRADE
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, true, "NQM15", new BigDecimal(123), false, new Date(), null);
		validateViolationNotes(tradeResults, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only) : -123 Contracts for NQM15 open on " + DateUtils.fromDateShort(new Date()) + " : Long Allowed : Failed");

		//Buy
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		//Sell
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);

		//Go short, should not be allowed
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "NQM15", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults, "Permitted Security: Futures (Equity - Index) - Domestic (Long Only) : -123 Contracts for NQM15 open on " + DateUtils.fromDateShort(new Date()) + " : Long Allowed : Failed");
	}


	@Test
	public void testExclusionGroup() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLK6");
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, "Permitted Security: Bonds (Central Govt - Inflation Linked - TIPS) - All (Long Only)", "Permitted Security: Futures (Currency) - All (Long or Short)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, security.getSymbol(), new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Exclude All Positions : 123 Contracts for " + security.getSymbol() + " open on " + DateUtils.fromDateShort(new Date()) + " : Failed");

		//We have verified the account fails the real run, lets add it to th exclusion group to make sure it passes
		this.investmentAccountUtils.addInvestmentAccountToAccountGroupByName(accountInfo.getClientAccount(), "Compliance Exclusion Group");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, security.getSymbol(), new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	@Disabled
	public void testOptionsWithUnderlying() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "SP Options");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, "S4J5P Y 1980", new BigDecimal(1), true, new Date(), this.goldmanSachs);
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, "BAC US 01/18/14 P2", new BigDecimal(1), true, new Date(), this.goldmanSachs);
		validateViolationNotes(tradeResults, "Exclude All Positions : 1 Contracts for BAC US 01/18/14 P2 open on " + DateUtils.fromDateShort(new Date()) + " : Failed");

		this.complianceUtils.assignRuleToAccountByName(accountInfo.getClientAccount(), "Options");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, "BAC US 01/18/14 P2", new BigDecimal(1), true, new Date(), this.goldmanSachs);
		validateViolationNotes(tradeResults);
	}
}
