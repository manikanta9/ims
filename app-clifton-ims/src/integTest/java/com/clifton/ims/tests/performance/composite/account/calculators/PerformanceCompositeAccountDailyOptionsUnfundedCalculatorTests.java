package com.clifton.ims.tests.performance.composite.account.calculators;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountDailyOptionsUnfundedCalculatorTests</code> tests the PerformanceCompositeAccountDailyOptionsUnfundedCalculator
 * <p>
 * Note: Need to override Period End Market Value for some months because Accounting was using bad logic
 * and was always using the same underlying security in valuing all positions instead of the actual underlying
 * For some months this didn't matter if they all happen to use the same underlying
 * These differences are small enough that the return values fall within 0.005 threshold for most gross/net differences
 *
 * @author manderson
 */
public class PerformanceCompositeAccountDailyOptionsUnfundedCalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Test
	public void test_account_310501() {
		String accountNumber = "310501";

		// NOTE: NO WEEKLY EXPOSURE BREAKS

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("152416950.00")); // Accounting Value: 152,832,960.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("-681813.93"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("152694900.00")); // Accounting Value: 152,910,135.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("662674.00"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("149601900.00")); // Accounting Value: 150,229,590.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("874674.64"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.14712127")); // Accounting Value: -0.14198660
		overrideValueMap.put("periodGainLoss", new BigDecimal("-193204.54"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("141592850.00")); // Accounting Value: 141,818,250.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("779319.11"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-934282.99"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_663050() {
		String accountNumber = "663050";

		// March 2020 THIS account has switched calculators recently. Historically - use the composite calculator...
		setAccountPerformanceCalculationType(accountNumber, "10/31/2015", "Options Unfunded Calculator (Sub Periods)");

		// NOTE: THIS ACCOUNT APPEARS TO HAVE WEEKLY EXPOSURE CHANGES SO ONCE VERIFIED AND PROCESSED ON PRODUCTION, SHOULD ADD A MONTH WITH THOSE DETAILS TO BE TESTED/VALIDATED

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("1047218500.00")); // Accounting Value: 1,050,076,800.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("-5123072.36"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("971135000")); // Accounting Value: 972,591,675.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("4200877.62"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("1064514200.00")); // Actual Value: 1,068,980,620.00
		overrideValueMap.put("periodGainLoss", new BigDecimal("5904025.64"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-1073060.33"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("6434054.62"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-8287207.44"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("2879354.53"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "04/30/2016", overrideValueMap);

		// Daily Values Should Match - Lucas added overrides to account for excluding Bonds, which is now supported
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "05/31/2016");
		validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, "05/31/2016", true);

		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "09/30/2017");
		validatePerformanceCompositeInvestmentAccountDailyPerformance(accountNumber, "09/30/2017");
	}


	@Test
	public void test_account_183050() {
		String accountNumber = "183050";

		// THIS ACCOUNT USES STATIC 50MM POSITION VALUE
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-266534.88"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("209501.51"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("291363.42"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-45067.22"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("273994.28"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-373895.07"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_606550() {
		String accountNumber = "606550";

		// This account was changed recently.  So either they'll need to re-calc history in which case this can be moved to a different calculator test
		// or switch the calculator back.
		setAccountPerformanceCalculationType(accountNumber, "05/31/2016", "Options Unfunded Calculator (Static 50 MM Target)");

		// THIS ACCOUNT USES STATIC 50MM POSITION VALUE - NEW ACCOUNT IN MAY
		// Daily Values Should Match - Lucas added overrides to account for excluding Bonds, which is now supported
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "05/31/2016");
		validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, "05/31/2016", true);
	}


	@Test
	public void test_account_423900() {
		// Options Unfunded Daily Calculation
		String accountNumber = "423900";

		// New calculation - until selected on Prod will need this.
		setAccountPerformanceCalculationType(accountNumber, "09/30/2017", "Options Unfunded Daily Calculator (Linked)");

		// Confirmed with spreadsheet values - see: JIRA PERFORMANC-274
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-0.7994"));
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.8125"));
		overrideValueMap.put("periodGainLoss", new BigDecimal("-1972713.70"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "09/30/2017", overrideValueMap);
	}
}
