package com.clifton.ims.tests.accounting.balance;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;


/**
 * @author nickk
 */

public class AccountingBalancePendingBalanceTests extends BaseImsIntegrationTest {

	private static final Date POSITION_OPEN_DATE = DateUtils.clearTime(DateUtils.toDate("09/27/2018"));
	private static final Date BALANCE_DATE = DateUtils.clearTime(DateUtils.toDate("09/28/2018"));

	private static final String POSITION_SECURITY_SYMBOL = "SPXW US 10/26/18 P2835";

	@Resource
	private AccountingBalanceService accountingBalanceService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;

	private AtomicReference<AccountInfo> accountInfo = new AtomicReference<>();


	@BeforeEach
	public void setup() {
		this.accountInfo.updateAndGet(currentValue -> {
			if (currentValue == null) {
				currentValue = openPosition(POSITION_SECURITY_SYMBOL, BigDecimal.TEN, false);
			}
			return currentValue;
		});
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingBalanceListNoPending() {
		String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse";
		validatePositionAccountBalanceList(false, expected);
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////            Transfer Tests                         ///////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingBalanceListMergePendingOpenNewPositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer("SPXW US 10/26/18 P2720", BigDecimal.ONE.negate(), true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListMergePendingOpenOnExistingPositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer(POSITION_SECURITY_SYMBOL, BigDecimal.ONE.negate(), true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-11\t-10,750\t-10,750\t-10,750\t-10,750\ttrue\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListMergePendingPartialClosePositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer(POSITION_SECURITY_SYMBOL, BigDecimal.ONE, true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-9\t-9,585\t-9,585\t-9,585\t-9,585\ttrue\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t1\t1,065\t1,065\t1,065\t1,065\ttrue\ttrue";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListMergePendingFullyClosePositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer(POSITION_SECURITY_SYMBOL, BigDecimal.TEN, true);
		try {
			String expected = "";
			String pendingExpected = "";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingOpenNewPositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer("SPXW US 10/26/18 P2720", BigDecimal.ONE.negate(), true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingOpenOnExistingPositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer(POSITION_SECURITY_SYMBOL, BigDecimal.ONE.negate(), true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingPartialClosePositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer(POSITION_SECURITY_SYMBOL, BigDecimal.ONE, true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t1\t1,065\t1,065\t1,065\t1,065\ttrue\ttrue";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t1\t1,065\t1,065\t1,065\t1,065\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingFullyClosePositionTransfer() {
		AccountingPositionTransfer transfer = createTransfer(POSITION_SECURITY_SYMBOL, BigDecimal.TEN, true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t10\t10,650\t10,650\t10,650\t10,650\ttrue\ttrue";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t10\t10,650\t10,650\t10,650\t10,650\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.accountingPositionTransferService.deleteAccountingPositionTransfer(transfer.getId());
		}
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////               Trade Tests                         ///////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testAccountingBalanceListMergePendingOpenNewPositionTrade() {
		Trade trade = createTrade("SPXW US 10/26/18 P2720", BigDecimal.ONE, false);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListMergePendingOpenOnExistingPositionTrade() {
		Trade trade = createTrade(POSITION_SECURITY_SYMBOL, BigDecimal.ONE, false);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-11\t-10,750\t-10,750\t-10,750\t-10,750\ttrue\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListMergePendingPartialClosePositionTrade() {
		Trade trade = createTrade(POSITION_SECURITY_SYMBOL, BigDecimal.ONE, true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-9\t-9,585\t-9,585\t-9,585\t-9,585\ttrue\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t1\t1,065\t1,065\t1,065\t1,065\ttrue\ttrue";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListMergePendingFullyClosePositionTrade() {
		Trade trade = createTrade(POSITION_SECURITY_SYMBOL, BigDecimal.TEN, true);
		try {
			String expected = "";
			String pendingExpected = "";
			validatePositionAccountBalanceList(true, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingOpenNewPositionTrade() {
		Trade trade = createTrade("SPXW US 10/26/18 P2720", BigDecimal.ONE, false);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2720\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingOpenOnExistingPositionTrade() {
		Trade trade = createTrade(POSITION_SECURITY_SYMBOL, BigDecimal.ONE, false);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-1\t-100\t-100\t-100\t-100\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingPartialClosePositionTrade() {
		Trade trade = createTrade(POSITION_SECURITY_SYMBOL, BigDecimal.ONE, true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t1\t1,065\t1,065\t1,065\t1,065\ttrue\ttrue";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t1\t1,065\t1,065\t1,065\t1,065\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}


	@Test
	public void testAccountingBalanceListNoMergePendingFullyClosePositionTrade() {
		Trade trade = createTrade(POSITION_SECURITY_SYMBOL, BigDecimal.TEN, true);
		try {
			String expected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t-10\t-10,650\t-10,650\t-10,650\t-10,650\tfalse\tfalse\n" +
					getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t10\t10,650\t10,650\t10,650\t10,650\ttrue\ttrue";
			String pendingExpected = getPositionPrefix() + "\tPosition\tSPXW US 10/26/18 P2835\t10\t10,650\t10,650\t10,650\t10,650\ttrue\ttrue";
			validatePositionAccountBalanceList(false, expected, pendingExpected);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private AccountInfo openPosition(String securitySymbol, BigDecimal quantity, boolean buy) {
		InvestmentSecurity option = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(securitySymbol);

		// Create new accounts
		AccountInfo account = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);

		// Create a position for the account
		Trade openTrade = this.tradeUtils.newOptionsTradeBuilder(option, quantity, buy, POSITION_OPEN_DATE, this.goldmanSachs, account)
				.withAverageUnitPrice(new BigDecimal("10.65"))
				.withSettlementDate(DateUtils.addDays(POSITION_OPEN_DATE, 1))
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(openTrade);
		return account;
	}


	private String getPositionPrefix() {
		return this.accountInfo.get().getClientAccount().getNumber() + "\t" + this.accountInfo.get().getHoldingAccount().getNumber();
	}


	private void validatePositionAccountBalanceList(boolean mergePending, String expected) {
		validatePositionAccountBalanceList(mergePending, expected, null);
	}


	private void validatePositionAccountBalanceList(boolean mergePending, String expected, String pendingExpected) {
		AccountingBalanceSearchForm balanceSearchForm = AccountingBalanceSearchForm.onTransactionDate(BALANCE_DATE);
		balanceSearchForm.setClientInvestmentAccountId(this.accountInfo.get().getClientAccount().getId());
		balanceSearchForm.setHoldingInvestmentAccountId(this.accountInfo.get().getHoldingAccount().getId());
		balanceSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS_EXCLUDE_COLLATERAL_AND_RECEIVABLE);
		balanceSearchForm.setPendingActivityRequest(mergePending ? PendingActivityRequests.POSITIONS_WITH_MERGE_AND_PREVIEW : PendingActivityRequests.POSITIONS_WITH_PREVIEW);
		List<AccountingBalance> balanceList = this.accountingBalanceService.getAccountingBalanceList(balanceSearchForm);
		assertAccountingBalanceList(expected, pendingExpected, balanceList);
	}


	private void assertAccountingBalanceList(String expected, String pendingExpected, List<AccountingBalance> balanceList) {
		balanceList.sort(Comparator.comparing(balance -> balance.getInvestmentSecurity().getSymbol()));
		String actual = balanceList.stream()
				.map(AccountingBalance::toStringFormatted)
				.collect(Collectors.joining("\n"));
		if (!CompareUtils.isEqual(expected, actual)) {
			Assertions.fail("AccountingBalance list does not match expected.\nExpected:\n" + expected + "\nActual:\n" + actual);
		}
		if (pendingExpected != null) {
			String pendingActual = balanceList.stream()
					.filter(AccountingBalance::isPendingActivityIncluded)
					.map(AccountingBalance::getPendingBalance)
					.filter(Objects::nonNull)
					.map(AccountingBalance::toStringFormatted)
					.collect(Collectors.joining("\n"));
			if (!CompareUtils.isEqual(pendingExpected, pendingActual)) {
				Assertions.fail("AccountingBalance pending list does not match expected.\nExpected:\n" + pendingExpected + "\nActual:\n" + pendingActual);
			}
		}
	}


	private AccountingPositionTransfer createTransfer(String securitySymbol, BigDecimal quantity, boolean to) {
		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		if (to) {
			transfer.setType(this.accountingUtils.getAccountingPositionTransferType(AccountingUtils.TO_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
			transfer.setToClientInvestmentAccount(this.accountInfo.get().getClientAccount());
			transfer.setToHoldingInvestmentAccount(this.accountInfo.get().getHoldingAccount());
			transfer.setNote("Transfer to client account " + transfer.getToClientInvestmentAccount().getName() + " and holding account " + transfer.getToHoldingInvestmentAccount().getName());
		}
		else {
			transfer.setType(this.accountingUtils.getAccountingPositionTransferType(AccountingUtils.FROM_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
			transfer.setFromClientInvestmentAccount(this.accountInfo.get().getClientAccount());
			transfer.setFromHoldingInvestmentAccount(this.accountInfo.get().getHoldingAccount());
			transfer.setNote("Transfer from client account " + transfer.getFromClientInvestmentAccount().getName() + " and holding account " + transfer.getFromHoldingInvestmentAccount().getName());
		}
		transfer.setTransactionDate(BALANCE_DATE);
		transfer.setSettlementDate(BALANCE_DATE);

		AccountingPositionTransferDetail detail = new AccountingPositionTransferDetail();
		if (!to) {
			detail.setExistingPosition(new AccountingTransaction());
			detail.getExistingPosition().setId(9325529L);
		}
		detail.setSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(securitySymbol));
		detail.setQuantity(quantity);
		detail.setTransferPrice(BigDecimal.ONE);
		detail.setExchangeRateToBase(BigDecimal.ONE);
		detail.setOriginalPositionOpenDate(POSITION_OPEN_DATE);
		transfer.setDetailList(Collections.singletonList(detail));
		return this.accountingUtils.saveAccountingPositionTransfer(transfer);
	}


	private Trade createTrade(String securitySymbol, BigDecimal quantity, boolean buy) {
		InvestmentSecurity option = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(securitySymbol);
		return this.tradeUtils.newOptionsTradeBuilder(option, quantity, buy, BALANCE_DATE, this.goldmanSachs, this.accountInfo.get())
				.withAverageUnitPrice(BigDecimal.ONE)
				.buildAndSave();
	}
}
