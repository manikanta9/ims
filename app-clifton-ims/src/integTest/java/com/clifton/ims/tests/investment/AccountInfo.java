package com.clifton.ims.tests.investment;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;


/**
 * The <code>AccountInfo</code> is a container of account information.
 *
 * @author jgommels
 */
public class AccountInfo {

	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;
	private InvestmentAccount custodianAccount;
	private InvestmentAccount repoHoldingAccount;
	private InvestmentAccount swapsHoldingAccount;


	public static AccountInfo copy(AccountInfo accountInfo) {
		AccountInfo clonedInfo = new AccountInfo();
		clonedInfo.setHoldingAccount(accountInfo.getHoldingAccount());
		clonedInfo.setClientAccount(accountInfo.getClientAccount());
		clonedInfo.setCustodianAccount(accountInfo.getCustodianAccount());
		clonedInfo.setRepoHoldingAccount(accountInfo.getRepoHoldingAccount());
		clonedInfo.setSwapsHoldingAccount(accountInfo.getSwapsHoldingAccount());
		return clonedInfo;
	}


////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getClientCompany() {
		return this.clientAccount.getIssuingCompany();
	}


	public BusinessClient getBusinessClient() {
		return this.clientAccount.getBusinessClient();
	}


	public BusinessCompany getHoldingCompany() {
		return this.holdingAccount.getIssuingCompany();
	}


	public BusinessCompany getRepoHoldingCompany() {
		return this.repoHoldingAccount.getIssuingCompany();
	}


	public BusinessCompany getSwapsHoldingCompany() {
		return this.swapsHoldingAccount.getIssuingCompany();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public InvestmentAccount getCustodianAccount() {
		return this.custodianAccount;
	}


	public void setCustodianAccount(InvestmentAccount custodianAccount) {
		this.custodianAccount = custodianAccount;
	}


	public InvestmentAccount getRepoHoldingAccount() {
		return this.repoHoldingAccount;
	}


	public void setRepoHoldingAccount(InvestmentAccount repoHoldingAccount) {
		this.repoHoldingAccount = repoHoldingAccount;
	}


	public InvestmentAccount getSwapsHoldingAccount() {
		return this.swapsHoldingAccount;
	}


	public void setSwapsHoldingAccount(InvestmentAccount swapsHoldingAccount) {
		this.swapsHoldingAccount = swapsHoldingAccount;
	}
}
