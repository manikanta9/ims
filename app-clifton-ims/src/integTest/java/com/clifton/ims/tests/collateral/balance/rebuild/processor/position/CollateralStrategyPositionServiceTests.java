package com.clifton.ims.tests.collateral.balance.rebuild.processor.position;

import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPosition;
import com.clifton.collateral.balance.rebuild.processor.position.options.CollateralOptionsStrategyPositionService;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.CSVFileToDataTableConverter;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.clifton.collateral.CollateralType.COLLATERAL_TYPE_OPTIONS_MARGIN;


/**
 * @author terrys
 */
public class CollateralStrategyPositionServiceTests extends BaseImsIntegrationTest {

	private static final ClassPathResource inputFile = new ClassPathResource("com/clifton/ims/tests/collateral/balance/rebuild/processor/position/ParametricDefensiveEquityMarginPositions.csv");

	@Resource
	public CollateralOptionsStrategyPositionService collateralOptionsStrategyPositionService;

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	@Test
	public void getCollateralStrategyPositionListTest() throws IOException {
		Map<String, InvestmentSecurity> securityMap = new HashMap<>();
		Date positionDate = DateUtils.toDate("08/01/2019");

		CSVFileToDataTableConverter csvFileToDataTableConverter = new CSVFileToDataTableConverter();
		DataTable dataTable = csvFileToDataTableConverter.convert(inputFile.getFile());

		Map<Long, List<CollateralOptionsStrategyTestPosition>> expectedPositionByTransactionId = new HashMap<>();
		for (int r = 0; r < dataTable.getTotalRowCount(); r++) {
			CollateralOptionsStrategyTestPosition collateralOptionsStrategyPosition = convert(positionDate, dataTable.getRow(r), securityMap);
			List<CollateralOptionsStrategyTestPosition> expectedPositionList = expectedPositionByTransactionId.computeIfAbsent(collateralOptionsStrategyPosition.getAccountingTransactionId(), k -> {
				List<CollateralOptionsStrategyTestPosition> list = new ArrayList<>();
				expectedPositionByTransactionId.put(collateralOptionsStrategyPosition.getAccountingTransactionId(), list);
				return list;
			});
			expectedPositionList.add(collateralOptionsStrategyPosition);
		}

		Map<Long, List<CollateralOptionsStrategyTestPosition>> positionByTransactionId = this.collateralOptionsStrategyPositionService.getCollateralOptionsStrategyPositionList(COLLATERAL_TYPE_OPTIONS_MARGIN, 6309, positionDate, false).stream()
				.map(CollateralOptionsStrategyTestPosition::new)
				.collect(Collectors.groupingBy(CollateralOptionsStrategyPosition::getAccountingTransactionId));

		Assertions.assertTrue(expectedPositionByTransactionId.keySet().containsAll(positionByTransactionId.keySet()));
		Assertions.assertTrue(positionByTransactionId.keySet().containsAll(expectedPositionByTransactionId.keySet()));
		for (Map.Entry<Long, List<CollateralOptionsStrategyTestPosition>> entry : expectedPositionByTransactionId.entrySet()) {
			List<CollateralOptionsStrategyTestPosition> expectedTransactionsList = entry.getValue();
			List<CollateralOptionsStrategyTestPosition> returnedTransactionsList = positionByTransactionId.get(entry.getKey());
			Assertions.assertEquals(expectedTransactionsList.size(), returnedTransactionsList.size());
			Assertions.assertTrue(expectedTransactionsList.containsAll(returnedTransactionsList));
		}
	}

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////


	private CollateralOptionsStrategyTestPosition convert(Date positionDate, DataRow dataRow, Map<String, InvestmentSecurity> securityMap) {
		Map<String, Object> values = dataRow.getRowValueMap();
		InvestmentSecurity security = securityMap.computeIfAbsent(values.get("Security").toString(), k -> {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setName(values.get("Security").toString());
			List<InvestmentSecurity> securities = this.investmentInstrumentService.getInvestmentSecurityList(searchForm);
			Assertions.assertFalse(securities.isEmpty());
			Assertions.assertEquals(1, securities.size());
			return securities.get(0);
		});
		return new CollateralOptionsStrategyTestPosition(
				positionDate,
				security,
				new BigDecimal("100"),
				new BigDecimal(values.get("Quantity").toString()),
				values.get("Strategy").toString(),
				new BigDecimal(values.get("Strike Value").toString()),
				new BigDecimal(values.get("Underlying Price").toString()),
				new BigDecimal(values.get("Underlying Value").toString()),
				new BigDecimal(values.get("Market Price").toString()),
				new BigDecimal(values.get("Option Proceeds").toString()),
				Long.parseLong(values.get("Transaction ID").toString())
		);
	}

	/////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////

	private static class CollateralOptionsStrategyTestPosition extends CollateralOptionsStrategyPosition {

		public CollateralOptionsStrategyTestPosition(CollateralOptionsStrategyPosition collateralPosition) {
			super(collateralPosition);
		}


		public CollateralOptionsStrategyTestPosition(Date positionDate, InvestmentSecurity investmentSecurity, BigDecimal priceMultiplier, BigDecimal remainingQuantity, String positionGroupId, BigDecimal strikeValue, BigDecimal underlyingPrice, BigDecimal underlyingValue, BigDecimal optionPrice, BigDecimal optionProceeds, Long accountingTransactionId) {
			super(positionDate, investmentSecurity, priceMultiplier, remainingQuantity, positionGroupId, strikeValue, underlyingPrice, underlyingValue, optionPrice, optionProceeds, accountingTransactionId);
		}


		@Override
		public boolean equals(Object o) {
			if (o instanceof CollateralOptionsStrategyTestPosition) {
				CollateralOptionsStrategyTestPosition that = (CollateralOptionsStrategyTestPosition) o;
				return
						StringUtils.isEqual(getPositionGroupId(), that.getPositionGroupId())
								&& MathUtils.isEqual(getAccountingTransactionId(), that.getAccountingTransactionId())
								&& MathUtils.isEqual(getOptionPrice(), that.getOptionPrice())
								&& MathUtils.isEqual(getOptionProceeds(), that.getOptionProceeds())
								&& MathUtils.isEqual(getPriceMultiplier(), that.getPriceMultiplier())
								&& MathUtils.isEqual(getRemainingQuantity(), that.getRemainingQuantity())
								&& MathUtils.isEqual(getRequiredCollateral(), that.getRequiredCollateral())
								&& MathUtils.isEqual(getStrikePrice(), that.getStrikePrice())
								&& MathUtils.isEqual(getStrikeValue(), that.getStrikeValue())
								&& MathUtils.isEqual(getUnderlyingPrice(), that.getUnderlyingPrice())
								&& MathUtils.isEqual(getUnderlyingValue(), that.getUnderlyingValue());
			}
			return false;
		}


		@Override
		public int hashCode() {
			return Objects.hash(getStrikePrice(), getStrikeValue(), getUnderlyingPrice(), getUnderlyingValue(), getOptionPrice(), getOptionProceeds(), getAccountingTransactionId());
		}


		@Override
		public String toString() {
			return "{" +
					", positionDate=" + DateUtils.fromDateShort(getPositionDate()) +
					", investmentSecurity=" + getInvestmentSecurity().getSymbol() +
					", remainingQuantity=" + getRemainingQuantity() +
					", transactionId=" + getAccountingTransactionId() +
					", positionGroupId='" + getPositionGroupId() +
					'}';
		}
	}
}
