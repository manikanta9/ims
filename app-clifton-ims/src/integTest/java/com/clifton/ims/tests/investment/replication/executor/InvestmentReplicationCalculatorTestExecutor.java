package com.clifton.ims.tests.investment.replication.executor;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.test.executor.BaseTestExecutor;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.investment.replication.history.InvestmentReplicationAllocationHistory;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildCommand;
import com.clifton.investment.replication.history.search.InvestmentReplicationAllocationHistorySearchForm;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


/**
 * This executor is designed to make tests involving {@link InvestmentReplication}s easier to read/write.
 *
 * @author michaelm
 */
public class InvestmentReplicationCalculatorTestExecutor extends BaseTestExecutor<InvestmentReplicationAllocationHistory> {

	private InvestmentReplicationCalculatorTestServiceHolder serviceHolder;

	private String replicationName;
	private InvestmentReplication replication;
	private String allocationInstrumentName;
	private SystemBeanBuilder securityCalculatorSystemBeanBuilder;


	private InvestmentReplicationCalculatorTestExecutor(InvestmentReplicationCalculatorTestServiceHolder serviceHolder) {
		this.serviceHolder = serviceHolder;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static InvestmentReplicationCalculatorTestExecutor newExecutorForReplicationNameAndInstrumentNameWithSecurityCalculatorSystemBeanBuilder(InvestmentReplicationCalculatorTestServiceHolder serviceHolder,
	                                                                                                                                                String replicationName,
	                                                                                                                                                String allocationInstrumentName,
	                                                                                                                                                SystemBeanBuilder securityCalculatorSystemBeanBuilder) {
		return newExecutor(serviceHolder).withReplicationName(replicationName).withAllocationInstrumentName(allocationInstrumentName).withSecurityCalculatorSystemBeanBuilder(securityCalculatorSystemBeanBuilder);
	}


	public static InvestmentReplicationCalculatorTestExecutor newExecutorWithReplication(InvestmentReplicationCalculatorTestServiceHolder serviceHolder, InvestmentReplication replication) {
		return newExecutor(serviceHolder).withReplication(replication);
	}


	public static InvestmentReplicationCalculatorTestExecutor newExecutor(InvestmentReplicationCalculatorTestServiceHolder serviceHolder) {
		return new InvestmentReplicationCalculatorTestExecutor(serviceHolder);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationCalculatorTestExecutor withReplicationName(String replicationName) {
		this.replicationName = replicationName;
		return this;
	}


	public InvestmentReplicationCalculatorTestExecutor withReplication(InvestmentReplication replication) {
		this.replication = replication;
		return this;
	}


	public InvestmentReplicationCalculatorTestExecutor withAllocationInstrumentName(String allocationInstrumentName) {
		this.allocationInstrumentName = allocationInstrumentName;
		return this;
	}


	public InvestmentReplicationCalculatorTestExecutor withSecurityCalculatorSystemBeanBuilder(SystemBeanBuilder securityCalculatorSystemBeanBuilder) {
		this.securityCalculatorSystemBeanBuilder = securityCalculatorSystemBeanBuilder;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Allocation History";
	}


	@Override
	protected List<InvestmentReplicationAllocationHistory> executeTest() {
		return executeRebuild(createOrGetReplication());
	}


	@Override
	protected String[] getStringValuesForResultEntity(InvestmentReplicationAllocationHistory allocationHistory) {
		return Arrays.asList(
				DateUtils.fromDate(allocationHistory.getReplicationHistory().getStartDate(), DateUtils.DATE_FORMAT_INPUT),
				DateUtils.fromDate(allocationHistory.getReplicationHistory().getEndDate(), DateUtils.DATE_FORMAT_INPUT),
				allocationHistory.getCurrentInvestmentSecurity().getName(),
				allocationHistory.getReplicationAllocation().getAllocationWeight().toString()
		).toArray(new String[0]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SystemBean createOrGetCurrentSecurityCalculator() {
		if (this.securityCalculatorSystemBeanBuilder == null) {
			return null;
		}
		else {
			try {
				return this.securityCalculatorSystemBeanBuilder.buildAndSave();
			}
			catch (Exception e) {
				String existingCalculatorNameString = StringUtils.substringBetween(e.getCause().getMessage(), "[", "]");
				String[] duplicateCalculatorNames = existingCalculatorNameString == null ? null : existingCalculatorNameString.split(",");
				SystemBean calculator = this.securityCalculatorSystemBeanBuilder.build();
				int duplicateCalculatorCount = ArrayUtils.getLength(duplicateCalculatorNames);
				if (duplicateCalculatorCount == 0) {
					throw new ValidationException(String.format("Unable to create Current Security Calculator bean with type [%s] and name [%s].", calculator.getType().getName(), calculator.getName()), e);
				}
				ValidationUtils.assertTrue(ArrayUtils.isSingleElement(duplicateCalculatorNames), String.format("Unable to create Current Security Calculator bean with type [%s] and name [%s] because the following duplicate calculator beans exist [%s].", calculator.getType().getName(), calculator.getName(), duplicateCalculatorNames));
				return getExistingCalculatorByName(this.securityCalculatorSystemBeanBuilder.build().getType().getId(), duplicateCalculatorNames[0]);
			}
		}
	}


	private SystemBean getExistingCalculatorByName(int calculatorTypeId, String calculatorName) {
		SystemBeanSearchForm beanSearchForm = new SystemBeanSearchForm();
		beanSearchForm.setTypeId(calculatorTypeId);
		beanSearchForm.addSearchRestriction("name", ComparisonConditions.EQUALS, calculatorName);
		return CollectionUtils.getFirstElementStrict(this.serviceHolder.getSystemBeanService().getSystemBeanList(beanSearchForm));
	}


	private InvestmentReplication createOrGetReplication() {
		if (this.replication != null) {
			return this.replication;
		}
		ValidationUtils.assertNotNull(this.replicationName, "Replication or Replication Name is required for test execution.");
		InvestmentReplicationType replicationType = this.serviceHolder.getInvestmentReplicationService().getInvestmentReplicationTypeByName(InvestmentReplicationType.DEFAULT_REPLICATION_TYPE_NAME);
		try {
			this.replication = createReplication(replicationType);
		}
		catch (Exception e) {
			InvestmentReplicationSearchForm replicationSearchForm = new InvestmentReplicationSearchForm();
			replicationSearchForm.setNameEquals(this.replicationName);
			replicationSearchForm.setTypeId(replicationType.getId());
			this.replication = CollectionUtils.getFirstElement(this.serviceHolder.getInvestmentReplicationService().getInvestmentReplicationList(replicationSearchForm));
			if (this.replication == null) {
				throw new ValidationException(String.format("Unable to create new Replication or get existing with name [%s] and type [%s].", this.replicationName, replicationType.getName()), e);
			}
		}
		this.replication.setAllocationList(createAllocationList(replication));
		return this.replication;
	}


	private InvestmentReplication createReplication(InvestmentReplicationType replicationType) {
		InvestmentReplication replication = new InvestmentReplication();
		replication.setName(this.replicationName);
		replication.setType(replicationType);
		replication.setAllocationList(createAllocationList(replication));
		replication.setInactive(true); // to avoid rebuilding for previous business day in InvestmentReplicationAllocationObserver
		return this.serviceHolder.getInvestmentReplicationService().saveInvestmentReplication(replication);
	}


	private List<InvestmentReplicationAllocation> createAllocationList(InvestmentReplication replication) {
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();
		ValidationUtils.assertNotNull(this.allocationInstrumentName, "Invalid test executor configuration: Instrument Name is required for creating allocations.");
		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName(this.allocationInstrumentName);
		allocation.setReplicationTypeOverride(replication.getType());
		List<InvestmentInstrument> instrumentList = this.serviceHolder.getInvestmentInstrumentService().getInvestmentInstrumentList(instrumentSearchForm);
		allocation.setReplicationInstrument(CollectionUtils.getFirstElementStrict(instrumentList));
		allocation.setOrder((short) 0);
		allocation.setAllocationWeight(BigDecimal.valueOf(100));
		allocation.setCurrentSecurityCalculatorBean(createOrGetCurrentSecurityCalculator());
		allocation.setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_ALL);
		return Arrays.asList(allocation);
	}


	private List<InvestmentReplicationAllocationHistory> executeRebuild(InvestmentReplication replication) {
		InvestmentReplicationHistoryRebuildCommand replicationHistoryRebuildCommand = new InvestmentReplicationHistoryRebuildCommand();
		replicationHistoryRebuildCommand.setReplicationId(replication.getId());
		replicationHistoryRebuildCommand.setRebuildExisting(true);
		replicationHistoryRebuildCommand.setUseCurrentlyActiveAllocations(true);
		replicationHistoryRebuildCommand.setStartBalanceDate(this.startBalanceDate);
		replicationHistoryRebuildCommand.setEndBalanceDate(this.endBalanceDate);
		replicationHistoryRebuildCommand.setSynchronous(true);
		this.serviceHolder.getInvestmentReplicationHistoryRebuildService().rebuildInvestmentReplicationHistory(replicationHistoryRebuildCommand);
		InvestmentReplicationAllocationHistorySearchForm allocationHistorySearchForm = new InvestmentReplicationAllocationHistorySearchForm();
		allocationHistorySearchForm.setReplicationId(replication.getId());
		allocationHistorySearchForm.setActiveOnStartDate(this.startBalanceDate);
		allocationHistorySearchForm.setActiveOnEndDate(this.endBalanceDate);
		return this.serviceHolder.getInvestmentReplicationHistoryService().getInvestmentReplicationAllocationHistoryList(allocationHistorySearchForm);
	}
}
