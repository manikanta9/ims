package com.clifton.ims.tests.system.query;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.SystemQueryType;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemTable;
import com.clifton.test.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;


public class SystemQueryBuilder {

	private final String systemQueryName;
	private final SystemTable table;
	private final String sql;
	private final List<SystemQueryParameter> parameterList = new ArrayList<>();
	private final SystemQueryService systemQueryService;


	private SystemQueryBuilder(String systemQueryName, SystemTable table, String sql, SystemQueryService systemQueryService) {
		this.systemQueryName = systemQueryName;
		this.table = table;
		this.sql = sql;
		this.systemQueryService = systemQueryService;
	}


	public static SystemQueryBuilder newBuilder(SystemTable table, String sql, SystemQueryService systemQueryService) {
		return new SystemQueryBuilder(RandomUtils.randomNameAndNumber(), table, sql, systemQueryService);
	}


	public SystemQueryBuilder addParameter(String name, int order, SystemDataType dataType) {
		SystemQueryParameter parameter = new SystemQueryParameter();
		parameter.setName(name);
		parameter.setDescription(name);
		parameter.setOrder(order);
		parameter.setParameterOrder(order);
		parameter.setDataType(dataType);

		this.parameterList.add(parameter);
		return this;
	}


	public SystemQuery build() {
		SystemQuery systemQuery = new SystemQuery();
		systemQuery.setName(this.systemQueryName);
		systemQuery.setTable(this.table);
		systemQuery.setSqlStatement(this.sql);
		systemQuery.setQueryType(getSystemQueryType(null));
		if (systemQuery.getQueryCategory() == null) {
			systemQuery.setQueryCategory(this.systemQueryService.getSystemQueryCategory((short) 1));
		}
		return systemQuery;
	}


	public SystemQuery buildAndSave() {
		SystemQuery systemQuery = build();
		this.systemQueryService.saveSystemQuery(systemQuery);

		for (SystemQueryParameter parameter : CollectionUtils.getIterable(this.parameterList)) {
			parameter.setQuery(systemQuery);
			this.systemQueryService.saveSystemQueryParameter(parameter);
		}

		return systemQuery;
	}


	/**
	 * Returns the query type for the given query type name.  If queryTypeName is blank, defaults to the "Select" query type.
	 */
	private SystemQueryType getSystemQueryType(String queryTypeName) {
		if (StringUtils.isEmpty(queryTypeName)) {
			queryTypeName = "Select";
		}
		List<SystemQueryType> queryTypeList = this.systemQueryService.getSystemQueryTypeList();
		return CollectionUtils.getFirstElementStrict(BeanUtils.filter(queryTypeList, SystemQueryType::getName, queryTypeName));
	}
}
