package com.clifton.ims.tests.accounting.position;

import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * Tests for AccountingPosition queries via the AccountingPositionService using the AccountingPositionCommand Class.
 *
 * @author davidi
 */
public class AccountingPositionServiceQueryTests extends BaseImsIntegrationTest {

	@Resource
	AccountingPositionService accountingPositionService;

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetAccountingPositionListUsingCommand_single_position_with_existingPositionId() {
		final long expectedPositionId = 14807092;
		final Date transactionDate = DateUtils.toDate("03/11/2020");

		final AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setExistingPositionId(expectedPositionId);
		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);

		final int expectedListSize = 1;
		Assertions.assertEquals(expectedListSize, positionList.size());
		Assertions.assertEquals(expectedPositionId, positionList.get(0).getId());
	}


	@Test
	public void testGetAccountingPositionListUsingCommand_single_position_id_within_existingPositionIds() {
		final long expectedPositionId = 14807092;
		final Date transactionDate = DateUtils.toDate("03/11/2020");

		final AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		final Long[] positionIds = {expectedPositionId};
		command.setExistingPositionIds(positionIds);
		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);

		final int expectedListSize = 1;
		Assertions.assertEquals(expectedListSize, positionList.size());
		Assertions.assertEquals(expectedPositionId, positionList.get(0).getId());
	}


	@Test
	public void testGetAccountingPositionListUsingCommand_multiple_position_ids_within_existingPositionIds() {
		final long expectedPositionId = 14807092;
		final long expectedPositionId2 = 16704136;
		final Date transactionDate = DateUtils.toDate("03/11/2020");

		final AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		final Long[] positionIds = {expectedPositionId, expectedPositionId2};
		command.setExistingPositionIds(positionIds);
		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
		CollectionUtils.sort(positionList, this::comparePositionId);

		final int expectedListSize = 2;
		Assertions.assertEquals(expectedListSize, positionList.size());
		Assertions.assertEquals(expectedPositionId, positionList.get(0).getId());
		Assertions.assertEquals(expectedPositionId2, positionList.get(1).getId());
	}


	@Test
	public void testGetAccountingPositionListUsingCommand_multiple_position_ids_transaction_date_filtering() {
		final long expectedPositionId = 14807092;  //transaction date 06/28/2019
		final long expectedPositionId2 = 16704136; //transaction date 01/16/2020  -- will be filtered out
		final Date transactionDate = DateUtils.toDate("10/01/2019");

		final AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		final Long[] positionIds = {expectedPositionId, expectedPositionId2};
		command.setExistingPositionIds(positionIds);
		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
		CollectionUtils.sort(positionList, this::comparePositionId);

		final int expectedListSize = 1;
		Assertions.assertEquals(expectedListSize, positionList.size());
		Assertions.assertEquals(expectedPositionId, positionList.get(0).getId());
	}


	@Test
	public void testGetAccountingPositionListUsingCommand_multiple_position_ids_filter_by_security() {
		final long expectedPositionId = 14807092;  // this one should be returned, it has the matching investment security
		final long expectedPositionId2 = 16704136;
		final InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828WU0", false);
		final Date transactionDate = DateUtils.toDate("03/11/2020");

		final AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		final Long[] positionIds = {expectedPositionId, expectedPositionId2};
		command.setExistingPositionIds(positionIds);
		command.setInvestmentSecurityId(investmentSecurity.getId());

		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
		CollectionUtils.sort(positionList, this::comparePositionId);

		final int expectedListSize = 1;
		Assertions.assertEquals(expectedListSize, positionList.size());
		Assertions.assertEquals(expectedPositionId, positionList.get(0).getId());
	}


	@Test
	public void testGetAccountingPositionListUsingCommand_get_by_account_security_transaction_date() {
		final long expectedPositionId = 14717702; // transaction date is: 2019-06-18
		final long expectedPositionId1 = 14807092;  // transaction date is: 2019-06-28
		final InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828WU0", false);
		final Date transactionDate = DateUtils.toDate("06/28/2019");  // will query positions on or before 06/28/2019
		final int clientAccountID = 3166; // Marist
		final int holdingAccountID = 3208;  //Marist College

		final AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(transactionDate);
		command.setInvestmentSecurityId(investmentSecurity.getId());
		command.setClientInvestmentAccountId(clientAccountID);
		command.setHoldingInvestmentAccountId(holdingAccountID);

		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
		CollectionUtils.sort(positionList, this::comparePositionId);

		final int expectedListSize = 2;
		Assertions.assertEquals(expectedListSize, positionList.size());
		Assertions.assertEquals(expectedPositionId, positionList.get(0).getId());
		Assertions.assertEquals(expectedPositionId1, positionList.get(1).getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int comparePositionId(AccountingPosition position1, AccountingPosition position2) {
		return CompareUtils.compare(position1.getId(), position2.getId());
	}
}
