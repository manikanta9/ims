package com.clifton.ims.tests.system.validation;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactCategory;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.BusinessContractType;
import com.clifton.business.contract.search.BusinessContractTypeSearchForm;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.UUID;


/**
 * The <code>DataAccessExceptionConverterTest</code> class tests converting database exceptions to corresponding {$link ValidationException}
 * instances with user friendly error messages.
 * <p>
 *
 * @author manderson
 */

@Disabled //CURRENTLY CAUSING INFINITE LOOP DURING DESERIALIZATION, WILL UNIGNORE WHEN THIS IS RESOLVED
public class DataAccessExceptionConverterTests extends BaseImsIntegrationTest {

	@Resource
	private BusinessContactService businessContactService;

	@Resource
	private BusinessContractService businessContractService;

	@Resource
	private CalendarHolidayService calendarHolidayService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	private static final short BENCHMARKS_HIERARCHY = 1;
	private static final int GSCI_INSTRUMENT_STRUCTURE = 2; // Goldman Sachs Commodity Index Structure


	@Test
	public void testDeleteFKConstraintError_NoDetails() {
		// There are some cases where calling delete method actually calls additional delete methods and those additional calls
		// Are what violate the FK constraint.  In this case, we expect user friendly generic exception without the individual rows that were violated
		// Because this is so rare - will use existing data and will try to delete what we know is used
		// Deleting InvestmentInstrumentStructure, deletes the InvestmentInstrumentStructureWeights
		// Which is allowed only if there are no InvestmentSecurityStructureWeights using it.
		// This method relies of FK constraint for validation
		// Try to delete the category
		String msg = null;
		try {
			this.investmentInstrumentStructureService.deleteInvestmentInstrumentStructure(GSCI_INSTRUMENT_STRUCTURE);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("Cannot delete selected Investment Instrument Structure Weight row because it is referenced by 'Investment Security Structure Weight' row(s).", msg);
	}


	@Test
	public void testDeleteFKConstraintError_WithDetails() {
		String uniqueId = UUID.randomUUID().toString();

		// Create a New Category
		BusinessContactCategory category = new BusinessContactCategory();
		category.setName("Test Category " + uniqueId);
		this.businessContactService.saveBusinessContactCategory(category);

		// Add a New Contact
		BusinessContact contact = new BusinessContact();
		contact.setLastName("Test Contact " + uniqueId);
		contact.setContactCategory(category);
		this.businessContactService.saveBusinessContact(contact);

		// Try to delete the category
		String msg = null;
		try {
			this.businessContactService.deleteBusinessContactCategory(category.getId());
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("Cannot delete selected Business Contact Category row because it is referenced by the following 'Business Contact' row(s): '" + contact.getLabel() + "'.", msg);
	}


	@Test
	public void testInsertFKConstraintError() {
		String uniqueId = UUID.randomUUID().toString();

		// Create a New Category
		BusinessContactCategory category = new BusinessContactCategory();
		category.setName("Test Category " + uniqueId);
		this.businessContactService.saveBusinessContactCategory(category);

		// Delete the New Category
		this.businessContactService.deleteBusinessContactCategory(category.getId());

		// Add a New Contact - Referencing Newly Deleted Category
		BusinessContact contact = new BusinessContact();
		contact.setLastName("Test Contact " + uniqueId);
		contact.setContactCategory(category);

		String msg = null;
		try {
			this.businessContactService.saveBusinessContact(contact);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("Cannot find 'BusinessContactCategory' entity for id = " + category.getId(), msg);
	}


	@Test
	public void testDuplicateKey_OneField() {
		String uniqueId = UUID.randomUUID().toString();

		// Create a New Category
		BusinessContactCategory category = new BusinessContactCategory();
		category.setName("Test Category " + uniqueId);
		this.businessContactService.saveBusinessContactCategory(category);

		// Create Second Category with same name
		BusinessContactCategory category2 = new BusinessContactCategory();
		category2.setName("Test Category " + uniqueId);

		String msg = null;
		try {
			this.businessContactService.saveBusinessContactCategory(category2);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("The field 'Contact Category Name' does not allow duplicate values. Entity: " + category.getName(), msg);
	}


	@Test
	public void testDuplicateKey_MultipleFields() {
		// Find an existing Holiday
		CalendarHolidayDay calendarHoliday = CollectionUtils.getFirstElement(this.calendarHolidayService.getCalendarHolidayDayList(new CalendarHolidayDaySearchForm()));

		// Create a Duplicate of it
		CalendarHolidayDay copy = com.clifton.core.beans.BeanUtils.cloneBean(calendarHoliday, false, false);
		copy.setId(null);

		String msg = null;
		try {
			this.calendarHolidayService.saveCalendarHolidayDay(copy);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("The field combination 'Calendar Day ID', 'Calendar ID' does not allow duplicate values. Entity: " + copy.getLabel(), msg);
	}


	/**
	 * Note: This is actually returning message from ValidatingServletRequestDataBinder so it's not really testing the DataTruncation at the database level.
	 * See next test for test case that uses copy method to avoid validation during binding
	 */
	@Test
	public void testDataTruncationError_String_ValidationDuringBinding() {

		// Add a New Category - Name has a max of 50 Characters
		BusinessContactCategory category = new BusinessContactCategory();
		category.setName("Test Category With Name that is TOO Long - 1234567890");
		String msg = null;
		try {
			this.businessContactService.saveBusinessContactCategory(category);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("The maximum length for field 'ContactCategoryName' is 50.", msg);
	}


	/**
	 * Note: Need to use copy method to prevent validation during binding to pick up values that are too long
	 */
	@Test
	public void testDataTruncationError_String() {
		// Use an existing contract type to copy
		BusinessContractType contractType = CollectionUtils.getFirstElement(this.businessContractService.getBusinessContractTypeList(new BusinessContractTypeSearchForm()));
		Assertions.assertNotNull(contractType, "There are no existing contract types in the system to copy");
		String longName = "Test Contract Type With Name that is TOO Long - 1234567890";
		String msg = null;
		try {
			this.businessContractService.copyBusinessContractType(contractType.getId(), longName);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("Error in insert({id=null,label=Test Contract Type With Name that is TOO Long - 1234567890}): String or binary data would be truncated. ContractTypeName has max length of 50. Value length 58 is too long. [Test Contract Type With Name that is TOO Long - 1234567890]\n", msg);
	}


	@Test
	public void testDataTruncationError_Decimal() {
		String name = "Test - " + UUID.randomUUID();
		InvestmentInstrumentHierarchy hierarchy = this.investmentSetupService.copyInvestmentInstrumentHierarchy(BENCHMARKS_HIERARCHY, BENCHMARKS_HIERARCHY, name, name, name);
		hierarchy.setPriceMultiplier(new BigDecimal(10000000000.0));

		String msg = null;
		try {
			this.investmentSetupService.saveInvestmentInstrumentHierarchy(hierarchy);
		}
		catch (Throwable e) {
			msg = getErrorMessage(e);
		}
		Assertions.assertEquals("Error in update({id=" + hierarchy.getId() + ",label=" + name + "}): Arithmetic overflow error converting numeric to data type numeric. PriceMultiplier integer portion has max length of 9.  Value with integer portion length 11 is too large. [10000000000]", StringUtils.trim(msg));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String getErrorMessage(Throwable exception) {
		String msg = null;
		if (exception instanceof ImsErrorResponseException) {
			msg = ((ImsErrorResponseException) exception).getErrorMessageFromIMS();
		}
		if (msg == null) {
			msg = exception.getMessage();
		}
		return msg;
	}
}
