package com.clifton.ims.tests.accounting;


import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * The <code>GeneralLedgerDescriptions</code> holds a list of static String constants that define possible expected strings in the general ledger descriptions.
 *
 * @author jgommels
 */
public class GeneralLedgerDescriptions {

	//Assets
	public static final String CASH_PROCEEDS_FROM_CLOSE = "Cash proceeds from close of %s";

	//Expenses
	public static final String CASH_EXPENSE_FROM_OPEN = "Cash expense from opening of %s";
	public static final String CASH_EXPENSE_FROM_CLOSE = "Cash expense from close of %s";
	public static final String COMMISSION_EXPENSE = "Commission expense for %s";
	public static final String EXECUTING_COMMISSION_EXPENSE = "Executing Commission expense for %s";
	public static final String CLEARING_COMMISSION_EXPENSE = "Clearing Commission expense for %s";
	public static final String EXCHANGE_FEE_EXPENSE = "Exchange Fee expense for %s";
	public static final String MANAGEMENT_FEE_EXPENSE = "Management Fee expense for %s";
	public static final String NFA_FEES_EXPENSE = "NFA Fees expense for %s";
	public static final String SEC_FEES_EXPENSE = "SEC Fees expense for %s";
	public static final String TERMINATION_FEE = "Termination Fee expense for %s";
	public static final String TRANSACTION_FEE_OTHER = "Transaction Fee (Other) expense for %s";

	//Revenue
	public static final String ACCRUED_INTEREST_LEG = "Accrued Interest Leg for %s";
	public static final String ACCRUED_PREMIUM_LEG = "Accrued Premium Leg for %s";

	//Currencies
	public static final String CURRENCY_PROCEEDS_FROM_OPEN = "Currency proceeds from opening of %s";
	public static final String CURRENCY_PROCEEDS_FROM_CLOSE = "Currency proceeds from close of %s";
	public static final String CURRENCY_EXPENSE_FROM_OPEN = "Currency expense from opening of %s";
	public static final String CURRENCY_EXPENSE_FROM_CLOSE = "Currency expense from close of %s";
	public static final String CURRENCY_TRANSLATION_GAIN_LOSS = "Currency Translation Gain / Loss for %s";

	//Positions
	public static final String POSITION_OPENING = "Position opening of %s";
	public static final String POSITION_OPENING_AT_ORIGINAL_NOTIONAL = "Position opening at Original Notional for %s";
	public static final String POSITION_OPENING_AT_NEW_ORIGINAL_NOTIONAL = "Position opening at new Original Notional for %s";
	public static final String POSITION_OPENING_FROM_TRANSFER = "Position opening from transfer from %s";
	public static final String FULL_POSITION_CLOSE = "Full position close for %s";
	public static final String FULL_POSITION_CLOSE_AT_ORIGINAL_NOTIONAL = "Full position close  at Original Notional for %s";
	public static final String POSITION_CLOSE_FROM_TRANSFER_TO_EXTERNAL_ACCOUNT = "Position close from transfer to external account";
	public static final String PARTIAL_POSITION_CLOSE = "Partial position close for %s";
	public static final String POSITION_CLOSE_FROM_TRANSFER = "Position close from transfer to %s";

	//Events
	public static final String SECURITY_MATURITY = "Security Maturity for %s on %s";

	//Miscellaneous
	public static final String CASH_PROCEEDS_FROM_OPEN = "Cash proceeds from opening of %s";
	public static final String REALIZED_GAIN_LOSS_GAIN = "Realized Gain / Loss gain for %s";
	public static final String REALIZED_GAIN_LOSS_LOSS = "Realized Gain / Loss loss for %s";
	public static final String REDUCING_NOTIONAL = "Reducing Original Notional to Current Notional for %s";


	public static String format(String description, InvestmentSecurity security) {
		return String.format(description, security.getSymbol());
	}


	public static String format(String description, InvestmentAccount account) {
		return String.format(description, account.getLabel());
	}


	public static String format(String description, InvestmentSecurity security, Date date) {
		return String.format(description, security.getSymbol(), DateUtils.fromDateShort(date));
	}
}
