package com.clifton.ims.tests;


import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.business.BusinessUtils;
import com.clifton.ims.tests.compliance.ComplianceUtils;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.ims.tests.investment.InvestmentInstrumentUtils;
import com.clifton.ims.tests.investment.manager.InvestmentManagerAccountUtils;
import com.clifton.ims.tests.lending.repo.LendingRepoUtils;
import com.clifton.ims.tests.spring.ImsIntegrationConfiguration;
import com.clifton.ims.tests.trade.TradeUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.test.BaseIntegrationTest;
import com.clifton.trade.TradeType;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;


@ContextConfiguration(classes = {ImsIntegrationConfiguration.class})
public abstract class BaseImsIntegrationTest extends BaseIntegrationTest {

	//////////////////////////////////////////////////////////////////
	//						Test Util Classes
	//////////////////////////////////////////////////////////////////
	@Resource
	protected AccountingUtils accountingUtils;

	@Resource
	protected BusinessUtils businessUtils;

	@Resource
	protected ComplianceUtils complianceUtils;

	@Resource
	protected InvestmentManagerAccountUtils investmentManagerAccountUtils;

	@Resource
	protected InvestmentAccountUtils investmentAccountUtils;

	@Resource
	protected InvestmentInstrumentUtils investmentInstrumentUtils;

	@Resource
	protected LendingRepoUtils lendingRepoUtils;

	@Resource
	protected TradeUtils tradeUtils;

	//////////////////////////////////////////////////////////////////
	//						IMS Service Classes
	//////////////////////////////////////////////////////////////////

	@Resource
	protected AccountingAccountService accountingAccountService;

	@Resource
	protected InvestmentInstrumentService investmentInstrumentService;


	@Resource
	protected SystemColumnValueService systemColumnValueService;

	@Resource
	protected SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected InvestmentSecurity usd;
	protected InvestmentSecurity eur;

	protected TradeType futures;
	protected TradeType bonds;
	protected TradeType options;
	protected TradeType clearedInterestRateSwaps;
	protected TradeType creditDefaultSwaps;
	protected TradeType totalReturnSwaps;
	protected TradeType currency;
	protected TradeType stocks;
	protected TradeType forwards;

	protected BusinessCompany goldmanSachs;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void baseBeforeTestImpl() {
		super.baseBeforeTestImpl();

		this.usd = (this.usd != null) ? this.usd : this.investmentInstrumentUtils.getCurrencyBySymbol("USD");
		this.eur = (this.eur != null) ? this.eur : this.investmentInstrumentUtils.getCurrencyBySymbol("EUR");
		this.futures = (this.futures != null) ? this.futures : this.tradeUtils.getTradeType(TradeType.FUTURES);
		this.bonds = (this.bonds != null) ? this.bonds : this.tradeUtils.getTradeType(TradeType.BONDS);
		this.options = (this.options != null) ? this.options : this.tradeUtils.getTradeType(TradeType.OPTIONS);
		this.currency = (this.currency != null) ? this.currency : this.tradeUtils.getTradeType(TradeType.CURRENCY);
		this.totalReturnSwaps = (this.totalReturnSwaps != null) ? this.totalReturnSwaps : this.tradeUtils.getTradeType(TradeType.TOTAL_RETURN_SWAPS);
		this.clearedInterestRateSwaps = (this.clearedInterestRateSwaps != null) ? this.clearedInterestRateSwaps : this.tradeUtils.getTradeType(TradeType.CLEARED_INTEREST_RATE_SWAPS);
		this.creditDefaultSwaps = (this.creditDefaultSwaps != null) ? this.creditDefaultSwaps : this.tradeUtils.getTradeType(TradeType.CREDIT_DEFAULT_SWAPS);
		this.stocks = (this.stocks != null) ? this.stocks : this.tradeUtils.getTradeType(TradeType.STOCKS);
		this.forwards = (this.forwards != null) ? this.forwards : this.tradeUtils.getTradeType(TradeType.FORWARDS);
		this.goldmanSachs = (this.goldmanSachs != null) ? this.goldmanSachs : this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
	}
}
