package com.clifton.ims.tests.rule.trade;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimit;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitService;
import com.clifton.compliance.old.exchangelimit.CompliancePositionExchangeLimitTypes;
import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitSearchForm;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.search.ComplianceRuleOldSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.ims.tests.system.query.JdbcUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanPropertyTypeSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionBroker;
import com.clifton.trade.restriction.TradeRestrictionBrokerService;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.type.TradeRestrictionTypeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class TradeRuleTests extends RuleTests {

	@Resource
	ComplianceOldService complianceOldService;

	@Resource
	CompliancePositionExchangeLimitService compliancePositionExchangeLimitService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private TradeRestrictionBrokerService tradeRestrictionBrokerService;

	@Resource
	private TradeRestrictionService tradeRestrictionService;

	@Resource
	private TradeRestrictionTypeService tradeRestrictionTypeService;

	@Resource
	private InvestmentCalendarService investmentCalendarService;

	@Resource
	private SystemBeanService systemBeanService;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Trade Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		Set<String> excludedTestMethodSet = new HashSet<>();
		//Handled by ComplianceSystemRuleEvaluatorTests
		excludedTestMethodSet.add("testComplianceRealTimeRulesRule");
		//Handled by ComplianceShortLongTests
		excludedTestMethodSet.add("testShortLongChangeRule");
		//Handled by ComplianceCurrentContractRuleTests
		excludedTestMethodSet.add("testCurrentSecurityForInstrumentRule");
		//No bean for rule yet - need to add test once rule is enabled
		excludedTestMethodSet.add("testTradeFillPriceOutsideOfThresholdRule");
		// Disabled currently (as of 8/28/20) - temporarily enabling in the test, so we can exclude it from this validation
		excludedTestMethodSet.add("testSpotCurrencyTradesWithFXConnectAreNotAllowedInOTCClearedAccountsRule");
		return excludedTestMethodSet;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	@Disabled
	public void testTradeFillPriceOutsideOfThresholdRule() {
		AccountInfo bondsAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.bonds, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_COMPLIANCE);

		RuleAssignment assignment = new RuleAssignment();
		assignment.setRuleDefinition(this.ruleDefinitionService.getRuleDefinitionByName("Trade Fill Price Outside of Threshold"));
		assignment.setNote("Default");
		assignment.setAdditionalScopeFkFieldId(BeanUtils.getIdentityAsLong(bondsAccountInfo.getClientAccount()));
		this.ruleDefinitionService.saveRuleAssignment(assignment);

		SystemBean ruleEvaluatorBean = this.systemBeanService.getSystemBeanByName("Violation: Trade Fill Price Outside of Threshold Rule Evaluator");
		SystemBeanPropertyTypeSearchForm sf = new SystemBeanPropertyTypeSearchForm();
		sf.setTypeId(ruleEvaluatorBean.getType().getId());
		sf.setSearchPattern("Price Threshold");
		SystemBeanPropertyType thresholdPropertyType = CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyTypeList(sf));

		SystemBeanProperty threshold = new SystemBeanProperty();
		threshold.setBean(ruleEvaluatorBean);
		threshold.setType(thresholdPropertyType);
		threshold.setValue("10");
		ruleEvaluatorBean.setPropertyList(Collections.singletonList(threshold));
		this.systemBeanService.saveSystemBean(ruleEvaluatorBean);
		try {
			Date dateToUse = new Date();

			InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("38378KES4");

			// First, check trade fill that has price too high
			Trade buyAboveThreshold = this.tradeUtils.newBondTradeBuilder(security, BigDecimal.TEN, false, dateToUse, bondsAccountInfo, new BigDecimal(10))
					.addFill(BigDecimal.TEN, new BigDecimal(21))
					.buildAndSave();

			validateViolationForExistingTrade(buyAboveThreshold.getId(), "Violation: Trade Fill Price Outside of Threshold Rule Evaluator", true, "Actual price differed from expected by 11.");

			// Then, check trade fill with price too low
			Trade buyBelowThreshold = this.tradeUtils.newBondTradeBuilder(security, BigDecimal.TEN, false, dateToUse, bondsAccountInfo, new BigDecimal(20))
					.addFill(BigDecimal.TEN, new BigDecimal(9))
					.buildAndSave();

			validateViolationForExistingTrade(buyBelowThreshold.getId(), "Violation: Trade Fill Price Outside of Threshold Rule Evaluator", true, "Actual price differed from expected by 11.");

			// Now, make sure violation isn't generated for trade with price difference within tolerance
			Trade buyValid = this.tradeUtils.newBondTradeBuilder(security, BigDecimal.TEN, false, dateToUse, bondsAccountInfo, new BigDecimal(10))
					.addFill(BigDecimal.TEN, new BigDecimal(15))
					.buildAndSave();

			validateViolationForExistingTrade(buyValid.getId(), "Violation: Trade Fill Price Outside of Threshold Rule Evaluator", false);

			// Update bean property for percent to make sure that validation method works.
			SystemBeanPropertyTypeSearchForm sf2 = new SystemBeanPropertyTypeSearchForm();
			sf2.setTypeId(ruleEvaluatorBean.getType().getId());
			sf2.setSearchPattern("Use Percent");
			SystemBeanPropertyType percentPropertyType = CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyTypeList(sf2));

			SystemBeanProperty percent = new SystemBeanProperty();
			percent.setBean(ruleEvaluatorBean);
			percent.setType(percentPropertyType);
			percent.setValue("true");
			ruleEvaluatorBean.setPropertyList(CollectionUtils.createList(threshold, percent));
			this.systemBeanService.saveSystemBean(ruleEvaluatorBean);

			// Then, check trade fill with price too low
			Trade buyPercent = this.tradeUtils.newBondTradeBuilder(security, BigDecimal.TEN, false, dateToUse, bondsAccountInfo, new BigDecimal(10))
					.addFill(BigDecimal.TEN, new BigDecimal(12))
					.buildAndSave();

			validateViolationForExistingTrade(buyPercent.getId(), "Violation: Trade Fill Price Outside of Threshold Rule Evaluator", true, "Actual price differed from expected by 20.");
		}
		finally {
			ruleEvaluatorBean.setPropertyList(new ArrayList<>());
			this.systemBeanService.saveSystemBean(ruleEvaluatorBean);
			this.ruleDefinitionService.deleteRuleAssignment(assignment.getId());
		}
	}


	//For whatever reason when attempting to fully execute the trade, and set an expected exception I couldn't get this to work, so doing it manually.
	@Test
	public void testTradingRulesClientApprovedContractsRule() {
		AccountInfo bondsAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.bonds, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_COMPLIANCE);

		Date tradeDate = DateUtils.toDate("11/20/2015");
		BigDecimal quantity = new BigDecimal("2000");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("38378KES4");

		Trade buy = this.tradeUtils.newBondTradeBuilder(security, quantity, false, tradeDate, bondsAccountInfo)
				.addFill(quantity, BigDecimal.ONE)
				.buildAndSave();
		this.tradeUtils.validateTrade(buy);

		boolean foundViolation = false;
		String expectedViolationNote = "Investment instrument GNR 2013-45 AB (Fixed Income / Physicals / Collateralized Securities / CMBS / Agency) is not approved for account " + bondsAccountInfo.getHoldingAccount().getLabel();
		List<RuleViolation> ruleViolationList = this.ruleViolationUtils.getRuleViolationList(TRADE_TABLE_NAME, buy.getId());
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			if (ruleViolation.getViolationNote().contains(expectedViolationNote)) {
				foundViolation = true;
				break;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	@Test
	public void testTradeRuleAccountWithUnapprovedEventsRule() {
		// Using trade info from above test, but on today's date to avoid error saving event
		AccountInfo bondsAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.bonds, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_COMPLIANCE);
		Date dateToUse = new Date();

		InvestmentEvent event = new InvestmentEvent();
		event.setInvestmentAccount(bondsAccountInfo.getClientAccount());
		event.setEventDate(DateUtils.clearTime(dateToUse));
		event.setEventType(this.investmentCalendarService.getInvestmentEventTypeByName("Contact Client Reminder"));

		this.investmentCalendarService.saveInvestmentEvent(event, true);

		BigDecimal quantity = new BigDecimal("2000");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("38378KES4");

		Trade buy = this.tradeUtils.newBondTradeBuilder(security, quantity, false, dateToUse, bondsAccountInfo)
				.addFill(quantity, BigDecimal.ONE)
				.withCurrentFactor(BigDecimal.valueOf(0.0143364526D))
				.buildAndSave();

		validateViolationForExistingTrade(buy.getId(), "Trade Rule: Account With Unapproved Events", true, "There are 1 Investment Calendar Events in an Unapproved State");

		// Now make sure trade is excluded from rule evaluation if its group is excluded from the rule
		TradeGroup group = this.tradeUtils.newTradeGroup(Collections.singletonList(buy));

		Trade buyWithGroup = this.tradeUtils.newBondTradeBuilder(security, quantity, false, dateToUse, bondsAccountInfo)
				.addFill(quantity, BigDecimal.ONE)
				.withCurrentFactor(BigDecimal.valueOf(0.0143364526D))
				.withTradeGroup(group)
				.buildAndSave();

		SystemBean ruleEvaluator = this.systemBeanService.getSystemBeanByName("Trade Violation: Unapproved Event For Client Account");
		SystemBeanProperty excludedTypes = new SystemBeanProperty();

		SystemBeanPropertyTypeSearchForm sf = new SystemBeanPropertyTypeSearchForm();
		sf.setSearchPattern("Exclude Trade Group Types");
		sf.setTypeId(ruleEvaluator.getType().getId());

		excludedTypes.setType(CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyTypeList(sf)));
		excludedTypes.setValue(group.getTradeGroupType().getId().toString());

		SystemBeanPropertyTypeSearchForm sf2 = new SystemBeanPropertyTypeSearchForm();
		sf2.setSearchPattern("Unapproved Workflow Statuses");
		sf2.setTypeId(ruleEvaluator.getType().getId());

		SystemBeanProperty unapprovedStatues = new SystemBeanProperty();
		unapprovedStatues.setType(CollectionUtils.getOnlyElement(this.systemBeanService.getSystemBeanPropertyTypeList(sf2)));
		unapprovedStatues.setValue("Draft::Pending");

		ruleEvaluator.setPropertyList(CollectionUtils.createList(excludedTypes, unapprovedStatues));

		this.systemBeanService.saveSystemBean(ruleEvaluator);

		validateViolationForExistingTrade(buyWithGroup.getId(), "Trade Rule: Account With Unapproved Events", false);
	}


	@Test
	public void testNovationTradeWithoutCounterpartyRule() {
		AccountInfo swapAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.creditDefaultSwaps, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_COMPLIANCE);
		Date dateToUse = new Date();

		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MY95G");

		Trade buy = this.tradeUtils.newCreditDefaultSwapTradeBuilder(security, new BigDecimal(10), new BigDecimal(2), true, new BigDecimal(10),
				dateToUse, this.businessUtils.getBusinessCompany(BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC), swapAccountInfo)
				.withClientAccount(this.investmentAccountUtils.getClientAccountByNumber("185666"))
				.withHoldingAccount(this.investmentAccountUtils.getHoldingAccountByNumber("TDE-MSCS"))
				.withNovation(true)
				.buildAndSave();

		validateViolationForExistingTrade(buy.getId(), "Novation Trade Without Counterparty", true, "The trade is marked as novation, but there is no counterparty assigned.");

		//Now make sure trade is accepted with business company assigned
		Trade buy2 = this.tradeUtils.newCreditDefaultSwapTradeBuilder(security, new BigDecimal(5), new BigDecimal(4), true, new BigDecimal(5),
				dateToUse, this.businessUtils.getBusinessCompany(BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC), swapAccountInfo)
				.withClientAccount(this.investmentAccountUtils.getClientAccountByNumber("185666"))
				.withHoldingAccount(this.investmentAccountUtils.getHoldingAccountByNumber("TDE-MSCS"))
				.withNovation(true)
				.withAssignmentCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.buildAndSave();

		validateViolationForExistingTrade(buy2.getId(), "Novation Trade Without Counterparty", false);
	}


	@Test
	public void testForeignTradeExchangeRateEqualsOneRule() {

		AccountInfo currencyAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.currency, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_COMPLIANCE);
		BigDecimal quantity = BigDecimal.TEN;

		// get a security who's instrument's currency is not that of the account (USD)
		InvestmentSecurity foreignSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP", true);

		Trade invalidTrade = this.tradeUtils.newCurrencyTradeBuilder(foreignSecurity, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, BigDecimal.ONE, true, new Date(), this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO), currencyAccountInfo)
				.buildAndSave();

		validateViolationForExistingTrade(invalidTrade.getId(), "Foreign Trade Exchange Rate Equals One Rule Evaluator", true, "The base currency of the account is different than that of the traded security, but has an FX Rate of one.");

		// Now check to make sure a case that shouldn't generate an error actually doesn't
		InvestmentSecurity domesticSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true);

		Trade validTrade = this.tradeUtils.newCurrencyTradeBuilder(domesticSecurity, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, BigDecimal.ONE, true, new Date(), this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO), currencyAccountInfo)
				.buildAndSave();

		validateViolationForExistingTrade(validTrade.getId(), "Foreign Trade Exchange Rate Equals One Rule Evaluator", false);

		// One more test to make sure that a valid trade with exchangeRate != 1 is allowed
		Trade validForeignTrade = this.tradeUtils.newCurrencyTradeBuilder(foreignSecurity, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, BigDecimal.TEN, true, new Date(), this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO), currencyAccountInfo)
				.buildAndSave();

		validateViolationForExistingTrade(validForeignTrade.getId(), "Foreign Trade Exchange Rate Equals One Rule Evaluator", false);
	}


	@Test
	public void testDomesticTradeExchangeRateDoesNotEqualOneRule() {

		AccountInfo currencyAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.currency, InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_COMPLIANCE);
		BigDecimal quantity = BigDecimal.TEN;

		// get a security who's instrument's currency is the same as the account (USD)
		InvestmentSecurity domesticSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("USD", true);

		Trade invalidTrade = this.tradeUtils.newCurrencyTradeBuilder(domesticSecurity, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, BigDecimal.TEN, true, new Date(), this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO), currencyAccountInfo)
				.buildAndSave();

		validateViolationForExistingTrade(invalidTrade.getId(), "Domestic Trade Exchange Rate Does Not Equal One", true, "The base currency of the account is the same as that of the traded security, but exchange rate is not equal to one.");

		// Now check to make sure a case that shouldn't generate an error actually doesn't

		Trade validTrade = this.tradeUtils.newCurrencyTradeBuilder(domesticSecurity, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, BigDecimal.ONE, true, new Date(), this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO), currencyAccountInfo)
				.buildAndSave();

		validateViolationForExistingTrade(validTrade.getId(), "Domestic Trade Exchange Rate Does Not Equal One", false);

		// One more test to make sure that a valid foreign trade is allowed
		InvestmentSecurity foreignSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("GBP", true);

		Trade validForeignTrade = this.tradeUtils.newCurrencyTradeBuilder(foreignSecurity, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, BigDecimal.TEN, true, new Date(), this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO), currencyAccountInfo)
				.buildAndSave();

		validateViolationForExistingTrade(validForeignTrade.getId(), "Domestic Trade Exchange Rate Does Not Equal One", false);
	}


	@Test
	public void testComplianceExchangeLimitRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		Date tradeDate = DateUtils.toDate("12/01/2015");
		BigDecimal quantity = new BigDecimal("101");
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("CCH14", "TEST_FH14", "TEST_F");

		CompliancePositionExchangeLimitSearchForm compliancePositionExchangeLimitSearchForm = new CompliancePositionExchangeLimitSearchForm();
		compliancePositionExchangeLimitSearchForm.setInstrumentId(security.getInstrument().getId());
		compliancePositionExchangeLimitSearchForm.setAccountabilityLimit(false);
		CompliancePositionExchangeLimit compliancePositionExchangeLimit =
				CollectionUtils.getFirstElement(this.compliancePositionExchangeLimitService.getCompliancePositionExchangeLimitList(compliancePositionExchangeLimitSearchForm));
		if (compliancePositionExchangeLimit == null) {
			compliancePositionExchangeLimit = new CompliancePositionExchangeLimit();
		}
		compliancePositionExchangeLimit.setInstrument(security.getInstrument());
		compliancePositionExchangeLimit.setExchangeLimit(100);
		compliancePositionExchangeLimit.setAccountabilityLimit(false);
		compliancePositionExchangeLimit.setLimitType(CompliancePositionExchangeLimitTypes.ALL_MONTHS);
		this.compliancePositionExchangeLimitService.saveCompliancePositionExchangeLimit(compliancePositionExchangeLimit);
		TestUtils.expectException(AssertionError.class, () -> {
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, this.goldmanSachs, futuresAccountInfo)
					.buildAndSave();
			try {
				this.tradeUtils.fullyExecuteTrade(buy);
			}
			finally {
				this.tradeUtils.cancelTrade(buy);
			}
		}, "Expected no non-ignored system violations but encountered violation: 'This trade will increase our positions against the TEST_F: Copy of [CC]: All Months ( - ) limit from <span class='amountPositive'>0%</span> to <span class='amountPositive'>101%</span> which is over the <span class='amountPositive'>100%</span> threshold'.");
	}


	@Test
	public void testTradeDateIsNotABusinessDayRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		Date tradeDate = DateUtils.toDate("12/15/2013");
		BigDecimal quantity = new BigDecimal("1");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, this.goldmanSachs, futuresAccountInfo)
				.addFill(quantity, BigDecimal.ONE)
				.buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy, "The selected trade date 12/15/2013 is not a business day for the calendar ICE Futures US");
	}


	@Test
	public void testTradeDateInTheFutureRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		Date tradeDate = DateUtils.toDate("12/16/2025");
		BigDecimal quantity = new BigDecimal("1");

		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLZ1");
		Date lastDeliveryDate = security.getLastDeliveryDate();
		Date endDate = security.getEndDate();

		//Modify dates for rule.
		security.setEndDate(DateUtils.toDate("12/16/2026"));
		security.setLastDeliveryDate(DateUtils.toDate("12/16/2026"));
		this.investmentInstrumentService.saveInvestmentSecurity(security);
		security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLZ1");

		Trade buy = null;
		try {
			buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, this.goldmanSachs, futuresAccountInfo)
					.addFill(quantity, BigDecimal.ONE)
					.buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy, "Trade date is in the future.");
		}
		finally {
			//Set dates back.
			security.setEndDate(endDate);
			security.setLastDeliveryDate(lastDeliveryDate);
			this.investmentInstrumentService.saveInvestmentSecurity(security);
			try {
				if (buy != null) {
					this.tradeUtils.cancelTrade(buy);
				}
			}
			catch (Exception e) {
				for (TradeFill tradeFill : buy.getTradeFillList()) {
					this.tradeUtils.deleteTradeFill(tradeFill);
				}
				this.tradeUtils.cancelTrade(buy);
			}
		}
	}


	@Test
	public void testTradingRulesNotionalLimitRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		Date tradeDate = DateUtils.toDate("08/28/2019");

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName("S&P Midcap 400 Mini Futures (FA)");
		InvestmentInstrument instrument = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));
		if (instrument != null) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInstrumentId(instrument.getId());
			searchForm.setActiveOnDate(tradeDate);
			searchForm.setLimit(1);
			searchForm.setOrderBy("endDate:asc");
			InvestmentSecurity security = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));
			if (security != null) {
				ComplianceRuleOld complianceRuleOld = new ComplianceRuleOld();
				complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Notional Limit"));
				complianceRuleOld.setClientInvestmentAccount(futuresAccountInfo.getClientAccount());
				complianceRuleOld.setInvestmentInstrument(instrument);
				complianceRuleOld.setMaxBuyQuantity(BigDecimal.valueOf(100));
				complianceRuleOld.setMaxSellQuantity(BigDecimal.valueOf(100));
				complianceRuleOld.setMaxBuyAccountingNotional(BigDecimal.valueOf(9900));
				complianceRuleOld.setMaxSellAccountingNotional(BigDecimal.valueOf(9900));
				complianceRuleOld.setIgnorable(true);
				this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

				this.complianceUtils.createApprovedContractsOldComplianceRule(futuresAccountInfo, security.getInstrument());
				this.complianceUtils.createTradeAllComplianceRule(futuresAccountInfo);

				Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(100), true, tradeDate, this.goldmanSachs, futuresAccountInfo)
						.addFill(BigDecimal.valueOf(100), BigDecimal.ONE)
						.buildAndSave();
				this.tradeUtils.fullyExecuteTrade(buy, "The notional value of the trade <span class='amountPositive'>10,000.00</span> exceeds the buy limit of <span class='amountPositive'>9,900.00</span>");
			}
			else {
				Assertions.fail("Failed to locate security for test.");
			}
		}
		else {
			Assertions.fail("Failed to locate security instrument for test.");
		}
	}


	@Test
	public void testTradingRulesFatFingerLimitRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName("S&P Midcap 400 Mini Futures (FA)");
		InvestmentInstrument instrument = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));
		if (instrument != null) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInstrumentId(instrument.getId());
			searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN, new Date()));
			searchForm.setOrderBy("endDate:desc");
			searchForm.setLimit(1);
			InvestmentSecurity security = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));
			if (security != null) {
				ComplianceRuleOldSearchForm complianceRuleOldSearchForm = new ComplianceRuleOldSearchForm();
				complianceRuleOldSearchForm.setTypeId(this.complianceOldService.getComplianceRuleTypeOldByName("Fat Finger Trader Limit").getId());
				complianceRuleOldSearchForm.setInvestmentInstrumentId(instrument.getId());
				ComplianceRuleOld complianceRuleOld = CollectionUtils.getFirstElement(this.complianceOldService.getComplianceRuleOldList(complianceRuleOldSearchForm));
				if (complianceRuleOld == null) {
					complianceRuleOld = new ComplianceRuleOld();
				}
				complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Fat Finger Trader Limit"));
				complianceRuleOld.setInvestmentInstrument(instrument);
				complianceRuleOld.setMaxBuyQuantity(BigDecimal.valueOf(100));
				complianceRuleOld.setMaxSellQuantity(BigDecimal.valueOf(100));
				complianceRuleOld.setMaxBuyAccountingNotional(BigDecimal.valueOf(10100));
				complianceRuleOld.setMaxSellAccountingNotional(BigDecimal.valueOf(10100));
				this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

				this.complianceUtils.createApprovedContractsOldComplianceRule(futuresAccountInfo, security.getInstrument());
				this.complianceUtils.createTradeAllComplianceRule(futuresAccountInfo);

				TestUtils.expectException(AssertionError.class, () -> {
					Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(101), true, new Date(), this.goldmanSachs, futuresAccountInfo)
							.addFill(BigDecimal.valueOf(101), BigDecimal.ONE)
							.buildAndSave();
					this.tradeUtils.fullyExecuteTrade(buy);
				}, "Expected no non-ignored system violations but encountered violation: 'The order to buy <span class='amountPositive'>101.00</span> " + security.getSymbol() + " Contracts exceeds the DMA order limit of <span class='amountPositive'>100.00</span>.  This order must be executed in multiple tranches and/or via an algo'.");
			}
			else {
				Assertions.fail("Failed to locate security for test.");
			}
		}
		else {
			Assertions.fail("Failed to locate security instrument for test.");
		}
	}


	@Test
	public void testTradingRulesShortLongAllowedRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		Date tradeDate = DateUtils.toDate("08/28/2019");

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName("S&P Midcap 400 Mini Futures (FA)");
		InvestmentInstrument instrument = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));
		if (instrument != null) {
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInstrumentId(instrument.getId());
			searchForm.setLimit(1);
			searchForm.setActiveOnDate(tradeDate);
			searchForm.setOrderBy("endDate:asc");
			InvestmentSecurity security = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));
			if (security != null) {
				ComplianceRuleOld complianceRuleOld = new ComplianceRuleOld();
				complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Short/Long"));
				complianceRuleOld.setClientInvestmentAccount(futuresAccountInfo.getClientAccount());
				complianceRuleOld.setInvestmentInstrument(instrument);
				complianceRuleOld.setLongPositionAllowed(true);
				complianceRuleOld.setShortPositionAllowed(false);
				complianceRuleOld.setIgnorable(true);
				this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

				this.complianceUtils.createApprovedContractsOldComplianceRule(futuresAccountInfo, security.getInstrument());
				this.complianceUtils.createTradeAllComplianceRule(futuresAccountInfo);

				Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(100), true, tradeDate, this.goldmanSachs, futuresAccountInfo)
						.addFill(BigDecimal.valueOf(100), BigDecimal.ONE)
						.buildAndSave();
				this.tradeUtils.fullyExecuteTrade(buy);

				Trade sell = this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(100), false, tradeDate, this.goldmanSachs, futuresAccountInfo)
						.addFill(BigDecimal.valueOf(100), BigDecimal.ONE)
						.buildAndSave();
				this.tradeUtils.fullyExecuteTrade(sell);

				sell = this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(100), false, tradeDate, this.goldmanSachs, futuresAccountInfo)
						.addFill(BigDecimal.valueOf(100), BigDecimal.ONE)
						.buildAndSave();
				this.tradeUtils.fullyExecuteTrade(sell, "This trade will generate a short position of <span class='amountNegative'>-100</span> Contracts which is not allowed.");
			}
			else {
				Assertions.fail("Failed to locate security for test.");
			}
		}
		else {
			Assertions.fail("Failed to locate security instrument for test.");
		}
	}


	@Test
	public void testSpotCurrencyTradesWithFXConnectAreNotAllowedInOTCClearedAccountsRule() {
		RuleDefinition rule = this.ruleDefinitionService.getRuleDefinitionByName("Spot Currency Trades with FX Connect are not allowed in OTC Cleared accounts");
		rule.setInactive(false);
		this.ruleDefinitionService.saveRuleDefinition(rule);

		try {
			AccountInfo currencyAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.currency);

			Date tradeDate = DateUtils.toDate("03/04/2016");
			BigDecimal quantity = new BigDecimal("100");

			InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("EUR", true);

			TestUtils.expectException(AssertionError.class, () -> {
				Trade buy = this.tradeUtils.newCurrencyTradeBuilder(security, currencyAccountInfo.getClientAccount().getBaseCurrency(), quantity, new BigDecimal("1.2"), true, tradeDate, this.goldmanSachs, currencyAccountInfo, "FX Connect Ticket")
						.buildAndSave();
				this.tradeUtils.fullyExecuteTrade(buy);
			}, "Cannot trade spot currency via FX Connect Ticket in account " + currencyAccountInfo.getHoldingAccount().getLabel() + ".  Need to execute manually via the phone.");
		}
		finally {
			rule.setInactive(true);
			this.ruleDefinitionService.saveRuleDefinition(rule);
		}
	}


	@Test
	public void testManualBuyOfBondsWithoutNoteRule() {
		// Create new accounts
		AccountInfo account = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.bonds);

		// Create a position for the account
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		InvestmentSecurity bond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("912796FF1");
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName("Manual");
		Trade buyTrade = this.tradeUtils.newBondTradeBuilder(bond, new BigDecimal("1500000.00"), true, DateUtils.toDate("01/27/2015"), account)
				.withExecutingBroker(broker)
				.withTradeDestination(destination)
				.withDescription(null)
				.buildAndSave();
		TestUtils.expectException(AssertionError.class, () -> this.tradeUtils.fullyExecuteTrade(buyTrade), "Expected no non-ignored system violations but encountered violation: 'Note field requires \"evidence of seeking best execution\" when Buying Bonds via Manual destination.'.");
	}


	@Test
	public void testPortfolioRunTradeContractDifferenceSameDirectionRule() {
		String ruleDefinitionName = "Portfolio Run Trade Contract Difference (Same Direction)";
		// Can't test by entering a new trade because the trade needs to come from the Trade Creation screen of the run in order to apply
		// So need to use existing trades
		// Trades NOT from Portfolio Runs - Shouldn't apply because not in scope
		validateViolationForExistingTrade(748518, ruleDefinitionName, false);
		validateViolationForExistingTrade(748605, ruleDefinitionName, false);
		validateViolationForExistingTrade(748738, ruleDefinitionName, false);

		// Not a Future, so shouldn't apply
		validateViolationForExistingTrade(754195, ruleDefinitionName, false);

		// Passed Trades without Contract Splitting
		validateViolationForExistingTrade(750633, ruleDefinitionName, false);
		validateViolationForExistingTrade(750639, ruleDefinitionName, false);

		// Violated Trades without Contract Splitting
		validateViolationForExistingTrade(748312, ruleDefinitionName, true, "The trade entered <span class='amountNegative'>-65.00</span> is in the same direction of the system suggested trade <span class='amountNegative'>-135.60</span> and is outside the suggested band of <span class='amountPositive'>5.00</span> for International Equity - Equity Replication - MSCI EAFE Mini Futures (MFS).");
		// Suggested Trade of 0
		validateViolationForExistingTrade(732016, ruleDefinitionName, true, "The trade entered <span class='amountNegative'>-23.00</span> is in the same direction of the system suggested trade <span class='amountPositive'>0.00</span> and is outside the suggested band of <span class='amountPositive'>5.00</span> for Commodity - Live Cattle - Commodity Replication - Live Cattle Futures (LC).");

		// Passed Trades from Runs with contract splitting
		// Need to mark the trade allocation as un-processed in order for the split to remain as it was entered
		JdbcUtils.executeQuery("UPDATE TradeAssetClassPositionAllocation SET IsProcessed = 0 WHERE TradeID = 747807");
		validateViolationForExistingTrade(747807, ruleDefinitionName, false);

		// Now mark the trade allocation as processed - which then triggers it as a violation (because account doesn't use explicit, so once they are processed the system tries to allocate current contracts itself which changes the current target trade amount)
		JdbcUtils.executeQuery("UPDATE TradeAssetClassPositionAllocation SET IsProcessed = 1 WHERE TradeID = 747807");
		validateViolationForExistingTrade(747807, ruleDefinitionName, true, "The trade entered <span class='amountNegative'>-69.00</span> is in the same direction of the system suggested trade <span class='amountNegative'>-127.09</span> and is outside the suggested band of <span class='amountPositive'>5.00</span> for Domestic Equity - Equity Replication - S&P 500 Mini Futures (ES).");
	}


	@Test
	public void testPortfolioRunTradeContractDifferenceOppositeDirectionRule() {
		String ruleDefinitionName = "Portfolio Run Trade Contract Difference (Opposite Direction)";
		// Can't test by entering a new trade because the trade needs to come from the Trade Creation screen of the run in order to apply
		// So need to use existing trades
		// Trades NOT from Portfolio Runs - Shouldn't apply because not in scope
		validateViolationForExistingTrade(748518, ruleDefinitionName, false);
		validateViolationForExistingTrade(748605, ruleDefinitionName, false);
		validateViolationForExistingTrade(748738, ruleDefinitionName, false);

		// Passed Trades from Runs without contract splitting
		validateViolationForExistingTrade(748312, ruleDefinitionName, false);
		// Contract Difference (i.e. Suggested Trade is Zero - so we ignore)
		validateViolationForExistingTrade(732016, ruleDefinitionName, false);

		// Violated Trades from Runs without contract splitting
		validateViolationForExistingTrade(732015, ruleDefinitionName, true, "The trade entered <span class='amountPositive'>309.00</span> is in the opposite direction of the system suggested trade <span class='amountNegative'>-5.34</span> and is outside the suggested band of <span class='amountPositive'>1.00</span> for Cash - Currency Replication - British Pound (BP).");

		// Passed Trades from Runs with contract splitting
		// Need to mark the trade allocation as un-processed in order for the split to remain as it was entered
		JdbcUtils.executeQuery("UPDATE TradeAssetClassPositionAllocation SET IsProcessed = 0 WHERE TradeID = 747807");
		validateViolationForExistingTrade(747807, ruleDefinitionName, false);

		// Now mark the trade allocation as processed - which then triggers it as a violation (because account doesn't use explicit, so once they are processed the system tries to allocate current contracts itself which changes the current target trade amount)
		JdbcUtils.executeQuery("UPDATE TradeAssetClassPositionAllocation SET IsProcessed = 1 WHERE TradeID = 747807");
		validateViolationForExistingTrade(747807, ruleDefinitionName, true, "The trade entered <span class='amountNegative'>-16.00</span> is in the opposite direction of the system suggested trade <span class='amountPositive'>43.87</span> and is outside the suggested band of <span class='amountPositive'>1.00</span> for Global Equity - Equity Replication - S&P 500 Mini Futures (ES).");
	}


	@Test
	public void testTradeRestrictionBrokerRuleEvaluatorRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		InvestmentAccount clientAccount = futuresAccountInfo.getClientAccount();

		Date tradeDate = DateUtils.toDate("06/27/2018");
		BigDecimal quantity = new BigDecimal("2.00");
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC);
		BusinessCompany broker2 = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName("Manual");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLZ8");

		TradeRestrictionBroker tradeRestrictionBroker = new TradeRestrictionBroker();
		tradeRestrictionBroker.setBrokerCompany(broker);
		tradeRestrictionBroker.setClientAccount(clientAccount);
		tradeRestrictionBroker.setInclude(true);
		this.tradeRestrictionBrokerService.saveTradeRestrictionBroker(tradeRestrictionBroker);

		Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, broker2, futuresAccountInfo)
				.withTradeDestination(destination)
				.buildAndSave();

		String ruleMessage = "Trades for Account #: " + clientAccount.getLabel() + " cannot be traded with broker " + broker2.getName() + ". This broker is is not configured as a permitted broker in the TradeRestrictionBroker table.";
		TestUtils.expectException(AssertionError.class, () -> {
			this.tradeUtils.fullyExecuteTrade(buy);

			this.tradeRestrictionBrokerService.deleteTradeRestrictionBroker(tradeRestrictionBroker);

			// Cannot include variants of the this as separate tests as we would fail the testRuleTestsExistForRulesThatDoNotApply() test,
			// so we execute them within this test.
			testTradeRestrictionBrokerRuleEvaluatorRule_broker_in_excludes();
			testTradeRestrictionBrokerRuleEvaluatorRule_no_violations();
		}, ruleMessage);
	}


	@Test
	public void testTradeRestrictionRuleEvaluatorRule() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		InvestmentAccount clientAccount = futuresAccountInfo.getClientAccount();

		Date tradeDate = DateUtils.clearTime(new Date());
		tradeDate = DateUtils.addDays(tradeDate, 1);
		BigDecimal quantity = new BigDecimal("2.00");
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName("Manual");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLZ8");
		security.setEndDate(DateUtils.addMonths(tradeDate, 2));

		TradeRestriction tradeRestriction = new TradeRestriction();
		tradeRestriction.setClientInvestmentAccount(clientAccount);
		tradeRestriction.setInvestmentSecurity(security);
		tradeRestriction.setRestrictionType(this.tradeRestrictionTypeService.getTradeRestrictionTypeByName("No Buy"));
		tradeRestriction.setStartDate(tradeDate);

		this.tradeRestrictionService.saveTradeRestriction(tradeRestriction);

		Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, broker, futuresAccountInfo)
				.withTradeDestination(destination)
				.buildAndSave();

		String ruleMessage = "No Buy starting on " + DateUtils.fromDate(tradeDate, "MMM d, y h:mm:ss a");
		TestUtils.expectException(AssertionError.class, () -> {
			this.tradeUtils.fullyExecuteTrade(buy);

			this.tradeRestrictionService.deactivateTradeRestriction(tradeRestriction.getId(), "used for testing");
		}, ruleMessage);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	// Note:  the tests in this section are called from within the main test since the RuleTest framework does not
	// recognize test function names that do not match the rule name.


	private void testTradeRestrictionBrokerRuleEvaluatorRule_broker_in_excludes() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		InvestmentAccount clientAccount = futuresAccountInfo.getClientAccount();

		Date tradeDate = DateUtils.toDate("06/27/2018");
		BigDecimal quantity = new BigDecimal("2.00");
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName("Manual");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLZ8");

		TradeRestrictionBroker tradeRestrictionBroker = new TradeRestrictionBroker();
		tradeRestrictionBroker.setBrokerCompany(broker);
		tradeRestrictionBroker.setClientAccount(futuresAccountInfo.getClientAccount());
		tradeRestrictionBroker.setInclude(false);
		this.tradeRestrictionBrokerService.saveTradeRestrictionBroker(tradeRestrictionBroker);

		Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, broker, futuresAccountInfo)
				.withTradeDestination(destination)
				.buildAndSave();

		String ruleMessage = "Trades for Account #: " + clientAccount.getLabel() + " cannot be traded with broker " + broker.getName() + ". This broker is is not configured as a permitted broker in the TradeRestrictionBroker table.";
		TestUtils.expectException(AssertionError.class, () -> {
			this.tradeUtils.fullyExecuteTrade(buy);

			this.tradeRestrictionBrokerService.deleteTradeRestrictionBroker(tradeRestrictionBroker);
		}, ruleMessage);
	}


	private void testTradeRestrictionBrokerRuleEvaluatorRule_no_violations() {
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		Date tradeDate = DateUtils.toDate("06/27/2018");
		BigDecimal quantity = new BigDecimal("2.00");
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName("Manual");
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CLZ8");

		TradeRestrictionBroker tradeRestrictionBroker = new TradeRestrictionBroker();
		tradeRestrictionBroker.setBrokerCompany(broker);
		tradeRestrictionBroker.setClientAccount(futuresAccountInfo.getClientAccount());
		tradeRestrictionBroker.setInclude(true);
		this.tradeRestrictionBrokerService.saveTradeRestrictionBroker(tradeRestrictionBroker);

		Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, quantity, true, tradeDate, broker, futuresAccountInfo)
				.withTradeDestination(destination)
				.buildAndSave();

		this.tradeUtils.fullyExecuteTrade(buy);

		this.tradeRestrictionBrokerService.deleteTradeRestrictionBroker(tradeRestrictionBroker);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates Violation with Violation Note exists or not in the Preview list
	 */
	private void validateViolationForExistingTrade(int tradeId, String ruleDefinitionName, boolean exists, String... violationNoteExpected) {
		Trade trade = this.tradeUtils.getTrade(tradeId);
		ValidationUtils.assertNotNull(trade, "Missing Trade with ID " + tradeId + " from the database.");
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations("Trade", trade.getId());

		if (exists) {
			Assertions.assertFalse(ArrayUtils.isEmpty(violationNoteExpected), "The 'exists' parameter is true, but the caller did not provide a validation note.  If exists is true, you must provide a validation note.");
			for (String expectedViolation : violationNoteExpected) {
				Assertions.assertFalse(CollectionUtils.isEmpty(BeanUtils.filter(violationList, RuleViolation::getViolationNote, expectedViolation)), "Did not find Expected Violation with Message: " + expectedViolation + " for Trade " + trade.getLabel() + " for Rule Definition [" + ruleDefinitionName + "]");
			}
		}
		else {
			RuleViolation firstRuleViolation = null;
			for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
				if (ruleDefinitionName.equals(violation.getRuleAssignment().getRuleDefinition().getName()) && !"Passed".equals(violation.getIgnoreNote())) {
					firstRuleViolation = violation;
					break;
				}
			}
			Assertions.assertNull(firstRuleViolation, "Rule Definition: " + ruleDefinitionName + " for Trade " + trade.getLabel() + " should not have been executed, but it did");
		}
	}
}
