package com.clifton.ims.tests.collateral.balance;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
class CollateralBalanceServiceImplTest extends BaseImsIntegrationTest {

	@Resource
	private CollateralBalanceService collateralBalanceService;

	@Resource
	private CollateralService collateralService;

	@Resource
	private InvestmentAccountService investmentAccountService;


	@Test
	public void testAccruedInterestIncludesReceivablesAmount() {
		CollateralType collateralType = this.collateralService.getCollateralTypeByName(CollateralType.COLLATERAL_TYPE_OTC);
		Assertions.assertNotNull(collateralType);
		Assertions.assertTrue(collateralType.isUseSettlementDate());
		InvestmentAccount holdingAccount = this.investmentAccountService.getInvestmentAccountByNumber("NUSC-BBNK");
		Assertions.assertNotNull(holdingAccount);
		Date positionDate = DateUtils.toDate("02/3/2020");
		List<AccountingPositionDaily> positionDailyList = this.collateralBalanceService.getCollateralAccountingPositionDailyLiveList(collateralType.getName(), holdingAccount.getId(), positionDate, false);
		Assertions.assertEquals(2, positionDailyList.size());
		Optional<AccountingPositionDaily> accountingPositionDaily = positionDailyList.stream().filter(d -> d.getInvestmentSecurity().getSymbol().equals("36667485B")).findFirst();
		Assertions.assertTrue(accountingPositionDaily.isPresent());
		MatcherAssert.assertThat(accountingPositionDaily.get().getRemainingQuantity(), IsEqual.equalTo(new BigDecimal("30844.1327340000")));
		MatcherAssert.assertThat(accountingPositionDaily.get().getAccrual1Local(), IsEqual.equalTo(new BigDecimal("-3533.15")));
	}


	@Test
	public void testAccruedInterestIncludesReceivablesAmountAndPayments() {
		CollateralType collateralType = this.collateralService.getCollateralTypeByName(CollateralType.COLLATERAL_TYPE_OTC);
		Assertions.assertNotNull(collateralType);
		Assertions.assertTrue(collateralType.isUseSettlementDate());
		InvestmentAccount holdingAccount = this.investmentAccountService.getInvestmentAccountByNumber("NUSC-BBNK");
		Assertions.assertNotNull(holdingAccount);
		Date positionDate = DateUtils.toDate("02/4/2020");
		List<AccountingPositionDaily> positionDailyList = this.collateralBalanceService.getCollateralAccountingPositionDailyLiveList(collateralType.getName(), holdingAccount.getId(), positionDate, false);
		Assertions.assertEquals(4, positionDailyList.size());
		Map<BigDecimal, AccountingPositionDaily> accountingPositionDailyMap = positionDailyList.stream()
				.filter(d -> d.getInvestmentSecurity().getSymbol().equals("36667485B"))
				.filter(d -> !MathUtils.isNullOrZero(d.getRemainingQuantity()))
				.collect(Collectors.toMap(AccountingPositionDaily::getRemainingQuantity, Function.identity()));
		Assertions.assertTrue(accountingPositionDailyMap.containsKey(new BigDecimal("30844.1327340000")));

		MatcherAssert.assertThat(accountingPositionDailyMap.get(new BigDecimal("30844.1327340000")).getAccrual1Local(), IsEqual.equalTo(new BigDecimal("-7193.59")));
	}
}
