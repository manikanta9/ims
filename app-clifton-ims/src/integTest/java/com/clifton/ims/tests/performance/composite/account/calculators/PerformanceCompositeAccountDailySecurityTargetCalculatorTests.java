package com.clifton.ims.tests.performance.composite.account.calculators;

import org.junit.jupiter.api.Test;


/**
 * Tests GIPS Performance calculations for Accounts that use Security Targets
 *
 * @author manderson
 */
public class PerformanceCompositeAccountDailySecurityTargetCalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Test
	public void validateSecurityTargetAllOptions() {
		String accountNumber = "W-000010";
		String compositeCode = "PRAO1028DUFB";
		// Account is assigned to multiple composites so we need to include the composite code to find the correct one
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "01/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "02/29/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "03/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "04/30/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "05/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "06/30/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "07/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "08/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "09/30/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "10/31/2016");
	}


	@Test
	public void validateSecurityTargetPutsOnly_IVV_SPX_SPY_XSP() {
		String accountNumber = "W-000010";
		String compositeCode = "PRALPS1028UF";
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "01/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "02/29/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "03/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "04/30/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "05/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "06/30/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "07/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "08/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "09/30/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "10/31/2016");
	}


	@Test
	public void validateSecurityTargetCallsOnly_EFA() {
		String accountNumber = "W-000018";
		String compositeCode = "PRANUSEDUB";
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "06/30/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "07/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "08/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "09/30/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "10/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "11/30/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "12/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "01/31/2011");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "02/28/2011");
	}


	@Test
	public void validateSecurityTargetCallsOnly_IVV_SPX_SPY_XSP() {
		String accountNumber = "W-000018";
		String compositeCode = "PRALCEDUB";
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "06/30/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "07/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "08/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "09/30/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "10/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "11/30/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "12/31/2010");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "01/31/2011");
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, "02/28/2011");
	}


	@Test
	public void validateSecurityTargetTargetValueChangeAndCallsOnly() {
		String accountNumber = "W-000003";
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "06/30/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "07/31/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "08/31/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "09/30/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "10/31/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "11/30/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "12/31/2013");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "01/31/2014");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "02/28/2014");
	}
}
