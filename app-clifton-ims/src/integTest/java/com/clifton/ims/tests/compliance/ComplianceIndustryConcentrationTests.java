package com.clifton.ims.tests.compliance;


import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class ComplianceIndustryConcentrationTests extends ComplianceRealTimeTradeTests {


	@Test
	public void testIndustryConcentrationBuildUp() {
		Date createDate = new Date();

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUNDS, "Permitted Security: Futures (Currency) - All (Long or Short)", "Permitted Security: Futures (Fixed Income) - All (Long or Short)", "Permitted Security: Swaps - All (Long or Short)", "Permitted Security: Swaps - (Commodity - Total Return - TRS) - All (Long or Short)", "Permitted Security: Futures (Equity - Index) - All (Long or Short)", "Permitted Security: Currency - All (Long or Short)", "Permitted Security: Funds (ETF) - All (Long Only)", "Permitted Security: Notes (ETN) - All (Long Only)", "Permitted Security: Bonds - All (Long Only)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "PCY", new BigDecimal(41595), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "JNK", new BigDecimal(43670), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "LQD", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);

		//Now that the account is vested lets test our industry concentration
		this.complianceUtils.assignRulesToAccountByNames(accountInfo.getClientAccount(), "Max 25% in a single industry (Ex. Govt & Futures)");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "LQD", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults, true, "Max 25\\% in a single industry \\(Ex. Govt & Futures\\) : Net Assets: \\$[0-9,.]+ : Failed (\\s?\\[Industry Corp/Pref-High Yield, Limit Value: 25.00000, Result: [0-9]+.[0-9]+\\]|\\s?\\[Industry Emerging Market-Debt, Limit Value: 25.00000, Result: [0-9]+.[0-9]+\\]|\\s?\\[Industry Corp/Pref-Inv Grade, Limit Value: 25.00000, Result: [0-9.]+\\]){3}");
	}


	@Test
	public void testIndustryConcentrationSellVsBuyPendingTrade() {
		Date createDate = new Date();

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUNDS, "Permitted Security: Futures (Currency) - All (Long or Short)", "Permitted Security: Futures (Fixed Income) - All (Long or Short)", "Permitted Security: Swaps - All (Long or Short)", "Permitted Security: Swaps - (Commodity - Total Return - TRS) - All (Long or Short)", "Permitted Security: Futures (Equity - Index) - All (Long or Short)", "Permitted Security: Currency - All (Long or Short)", "Permitted Security: Funds (ETF) - All (Long Only)", "Permitted Security: Notes (ETN) - All (Long Only)", "Permitted Security: Bonds - All (Long Only)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "PCY", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "JNK", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "LQD", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "VNQ", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "SUB", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "MUB", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "SHY", new BigDecimal(1800), true, createDate);
		validateViolationNotes(tradeResults);

		//Now that the account is vested lets test our industry concentration
		this.complianceUtils.assignRulesToAccountByNames(accountInfo.getClientAccount(), "Max 25% in a single industry (Ex. Govt & Futures)");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "LQD", new BigDecimal(1000), false, createDate);
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "LQD", new BigDecimal(1000), true, createDate);
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "LQD", new BigDecimal(2000), true, createDate);

		validateViolationNotes(tradeResults, true, "Max 25\\% in a single industry \\(Ex. Govt & Futures\\) : Net Assets: \\$[0-9,.]+ : Failed "
				+ "("
				+ "\\s?\\[Industry Corp/Pref-Inv Grade, Limit Value: 25.00000, Result: 3[0-9].[0-9]+\\]" // The failing industry concentration; expect 30% or greater concentration
				+ "|\\s?\\[Industry Technology, Limit Value: 25.00000, Result: [0-9]+.[0-9]+\\]"
				+ "|\\s?\\[Industry Refining & Marketing, Limit Value: 25.00000, Result: [0-9]+\\.[0-9]+\\]"
				+ "|\\s?\\[Industry Corp/Pref-High Yield, Limit Value: 25.00000, Result: [0-9]+\\.[0-9]+\\]"
				+ "|\\s?\\[Industry Sector Fund-Real Estate, Limit Value: 25.00000, Result: [0-9]+\\.[0-9]+\\]"
				+ "|\\s?\\[Industry Coal Operations, Limit Value: 25.00000, Result: [0-9]+\\.[0-9]+\\]"
				+ "|\\s?\\[Industry Emerging Market-Debt, Limit Value: 25.00000, Result: [0-9]+\\.[0-9]+\\]"
				+ "){7}"
		);
	}
}
