package com.clifton.ims.tests.lending.repo;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.LendingRepoType;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Date;


@Component
public class LendingRepoUtils {

	private static final String LENDING_REPO_TABLE_NAME = "LendingRepo";
	private final SecurityUser admin;
	@Resource
	private LendingRepoService lendingRepoService;

	//	@Inject
	//	private WorkflowTransitionService workflowTransitionService;
	@Resource
	private WorkflowTransitioner workflowTransitioner;
	@Resource
	private RuleViolationUtils ruleViolationUtils;
	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;
	@Resource
	private CalendarSetupService calendarSetupService;


	@Inject
	public LendingRepoUtils(SecurityUserService securityUserService) {
		this.admin = securityUserService.getSecurityUserByName(SecurityUser.SYSTEM_USER);
	}


	public LendingRepoBuilder newOpenLendingRepoBuilder(InvestmentSecurity security, BigDecimal originalFace, Date date, AccountInfo accountInfo) {
		return newLendingRepoBuilder(this.lendingRepoService.getLendingRepoTypeByName(LendingRepoTypes.OPEN.getName()), security, false, 1, originalFace, BigDecimal.ONE, BigDecimal.ONE, date,
				accountInfo);
	}


	public void executeRepo(LendingRepo repo, boolean matureRepo, String... warningDescriptionsToIgnore) {

		//Validate the trade
		this.workflowTransitioner.transitionIfPresent("Validate REPO", LENDING_REPO_TABLE_NAME, BeanUtils.getIdentityAsLong(repo));

		//TODO May want to make this behavior more customizable
		//Ignore 'non-current' security warnings for now
		this.ruleViolationUtils.ignoreViolation(LENDING_REPO_TABLE_NAME, repo.getId(), "not the current security for instrument", false);

		//Ignore expected system warnings
		for (String warningDescription : warningDescriptionsToIgnore) {
			this.ruleViolationUtils.ignoreViolation(LENDING_REPO_TABLE_NAME, repo.getId(), warningDescription, true);
		}

		//Validate the trade again now that system warnings have been ignored
		this.workflowTransitioner.transitionIfPresent("Validate REPO", LENDING_REPO_TABLE_NAME, BeanUtils.getIdentityAsLong(repo));

		//Assert no unignored system warnings
		this.ruleViolationUtils.assertNoRuleViolations(LENDING_REPO_TABLE_NAME, repo.getId());

		//Transition through rest of states
		this.workflowTransitioner.transitionIfPresent("Approve REPO", LENDING_REPO_TABLE_NAME, BeanUtils.getIdentityAsLong(repo));
		this.workflowTransitioner.transitionIfPresent("Book and Post REPO Opening", LENDING_REPO_TABLE_NAME, BeanUtils.getIdentityAsLong(repo));

		if (matureRepo) {
			this.workflowTransitioner.transitionIfPresent("Mature REPO", LENDING_REPO_TABLE_NAME, BeanUtils.getIdentityAsLong(repo));
		}
	}


	public void confirmLendingRepo(int id, String confirmationNote, boolean confirmed) {
		this.lendingRepoService.confirmLendingRepo(ArrayUtils.toIntegerArray(id), confirmationNote);
	}


	private LendingRepoBuilder newLendingRepoBuilder(LendingRepoType lendingRepoType, InvestmentSecurity security, boolean reverse, int term, BigDecimal originalFace, BigDecimal price,
	                                                 BigDecimal haircutPercent, Date date, AccountInfo accountInfo) {

		date = this.calendarBusinessDayService.getNextBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.addDays(date, -1), this.calendarSetupService.getCalendarByName(LendingRepoService.LENDING_REPO_CALENDAR_NAME).getId()));

		LendingRepoBuilder builder = new LendingRepoBuilder(this.lendingRepoService);
		builder.investmentSecurity(security);
		builder.reverse(reverse);
		builder.term(term);

		builder.quantityIntended(originalFace);
		builder.originalFace(originalFace);
		builder.price(price);
		builder.haircutPercent(haircutPercent);
		builder.clientInvestmentAccount(accountInfo.getClientAccount());
		builder.holdingInvestmentAccount(accountInfo.getHoldingAccount());
		builder.repoInvestmentAccount(accountInfo.getRepoHoldingAccount());
		builder.counterparty(accountInfo.getRepoHoldingAccount().getIssuingCompany());

		builder.type(lendingRepoType);

		builder.traderUser(this.admin);
		builder.tradeDate(date);
		builder.settlementDate(date);

		return builder;
	}
}
