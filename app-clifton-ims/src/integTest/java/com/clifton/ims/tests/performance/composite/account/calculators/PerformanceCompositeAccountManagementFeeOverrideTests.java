package com.clifton.ims.tests.performance.composite.account.calculators;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * The <code>PerformanceCompositeAccountManagementFeeOverrideTests</code> tests the evaluation of the management fee override calculator
 * <p>
 * It will run the preview feature, but only validate the management fee override value
 *
 * @author manderson
 */
public class PerformanceCompositeAccountManagementFeeOverrideTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Test
	public void test_NoOverride() {
		String accountNumber = "495100";

		validatePerformanceManagementFeeOverride(accountNumber, "01/31/2017", null);
		validatePerformanceManagementFeeOverride(accountNumber, "02/28/2017", null);
		validatePerformanceManagementFeeOverride(accountNumber, "03/31/2017", null);
	}


	@Test
	public void test_ZeroNegativeOverrideAndRollBalanceForward() {
		// Tests that the first month's override does not go negative, but is set to zero
		String accountNumber = "124401";

		// Would normally calculate negative value - that value should be zeroed out
		validatePerformanceManagementFeeOverride(accountNumber, "04/30/2017", BigDecimal.ZERO);
		// And then the subsequent adjustment is applied to second month of quarter
		validatePerformanceManagementFeeOverride(accountNumber, "05/31/2017", new BigDecimal("1565.9"));
		// And then the third month there is no adjustment
		validatePerformanceManagementFeeOverride(accountNumber, "06/30/2017", null);


		// Tests that the first month's override does not go negative, but is set to zero
		accountNumber = "668452";
		validatePerformanceManagementFeeOverride(accountNumber, "07/31/2016", BigDecimal.ZERO);

		// And then the subsequent adjustment is applied to second month of quarter
		validatePerformanceManagementFeeOverride(accountNumber, "08/31/2016", new BigDecimal("8417.26"));
	}


	@Test
	public void test_OverrideAppliedFirstMonthOfQuarterOnly() {
		// Tests that the first month's override is appropriately applied
		String accountNumber = "428600";

		validatePerformanceManagementFeeOverride(accountNumber, "01/31/2017", new BigDecimal("15888.67"));
		// And then the subsequent months have no override
		validatePerformanceManagementFeeOverride(accountNumber, "02/28/2017", null);
		validatePerformanceManagementFeeOverride(accountNumber, "03/31/2017", null);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validatePerformanceManagementFeeOverride(String accountNumber, String periodEndDate, BigDecimal expectedManagementFeeOverride) {
		PerformanceCompositeInvestmentAccountPerformance actualAccountPerformance = getPerformanceCompositeInvestmentAccountPerformance(null, accountNumber, periodEndDate, false);
		PerformanceCompositeInvestmentAccountPerformance previewAccountPerformance = getPreviewResultForPerformanceCompositeInvestmentAccountPerformance(actualAccountPerformance, false);

		if (expectedManagementFeeOverride == null) {
			Assertions.assertNull(previewAccountPerformance.getManagementFeeOverride(), "Expected Management Fee Override to be NULL - which means NO override, but it was: " + CoreMathUtils.formatNumberInteger(previewAccountPerformance.getManagementFeeOverride()));
		}
		else if (previewAccountPerformance.getManagementFeeOverride() == null) {
			Assertions.fail("Previewed Management Fee Override is NULL which means it didn't calculate anything, but was expecting it to be " + CoreMathUtils.formatNumberInteger(expectedManagementFeeOverride));
		}
		else {
			Assertions.assertEquals(CoreMathUtils.formatNumberInteger(expectedManagementFeeOverride), CoreMathUtils.formatNumberInteger(previewAccountPerformance.getManagementFeeOverride()));
		}
	}
}
