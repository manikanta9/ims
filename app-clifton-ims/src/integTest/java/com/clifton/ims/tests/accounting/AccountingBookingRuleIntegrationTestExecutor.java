package com.clifton.ims.tests.accounting;

import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import org.junit.jupiter.api.Assertions;

import java.util.Date;


/**
 * This executor is designed to make booking rule integration tests easier to read/write.
 *
 * @author michaelm
 */
public class AccountingBookingRuleIntegrationTestExecutor {

	private final AccountingUtils accountingUtils;

	private AccountInfo accountInfo;
	private InvestmentSecurityEvent event;
	private String[] expectedEventJournalDetails;
	private String[] expectedJournalDetails;
	private String[] expectedReversalDetails;
	/**
	 * @see com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand#generationDate
	 */
	private Date generationDate;


	private AccountingBookingRuleIntegrationTestExecutor(AccountingUtils accountingUtils) {
		this.accountingUtils = accountingUtils;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountingBookingRuleIntegrationTestExecutor newExecutorForAccountAndEvent(AccountingUtils accountingUtils, AccountInfo accountInfo, InvestmentSecurityEvent event) {
		return new AccountingBookingRuleIntegrationTestExecutor(accountingUtils).withAccountInfo(accountInfo).withEvent(event);
	}


	public static AccountingBookingRuleIntegrationTestExecutor newExecutor(AccountingUtils accountingUtils) {
		return new AccountingBookingRuleIntegrationTestExecutor(accountingUtils);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBookingRuleIntegrationTestExecutor withAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
		return this;
	}


	public AccountingBookingRuleIntegrationTestExecutor withEvent(InvestmentSecurityEvent event) {
		this.event = event;
		return this;
	}


	public AccountingBookingRuleIntegrationTestExecutor withExpectedEventJournalDetails(String... expectedEventJournalDetails) {
		this.expectedEventJournalDetails = expectedEventJournalDetails;
		return this;
	}


	public AccountingBookingRuleIntegrationTestExecutor withExpectedJournalDetails(String... expectedJournalDetails) {
		this.expectedJournalDetails = expectedJournalDetails;
		return this;
	}


	public AccountingBookingRuleIntegrationTestExecutor withExpectedReversalDetails(String... expectedReversalDetails) {
		this.expectedReversalDetails = expectedReversalDetails;
		return this;
	}


	public AccountingBookingRuleIntegrationTestExecutor withGenerationDate(Date generationDate) {
		this.generationDate = generationDate;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Verifies that every journal detail from the specified journal as well as the count of details
	 * matches the expected formatted journal detail representation.
	 */
	public void execute() {
		this.execute(false);
	}


	public void executeAndPrintTransactions() {
		this.execute(true);
	}


	public void preview() {
		this.preview(false);
	}


	public void previewAndPrintTransactions() {
		this.preview(true);
	}


	private void execute(boolean printResults) {
		AccountingEventPostResult result = this.accountingUtils.generateAndPostEventJournal(this.event, this.accountInfo.getClientAccount(), this.generationDate);

		AccountingEventJournal eventJournal = result.getEventJournal();
		assertEventJournalDetails(eventJournal, printResults);

		AccountingJournal journal = result.getMainBookedJournal();
		Assertions.assertNotNull(journal, "Event journal booked journal was not generated.");
		AccountingUtils.assertJournalDetailList(journal, this.expectedJournalDetails, printResults);

		AccountingJournal reversal = result.getAccrualReversalBookedJournal();
		if (reversal != null && !CollectionUtils.isEmptyCollectionOrArray(this.expectedReversalDetails)) {
			AccountingUtils.assertJournalDetailList(reversal, this.expectedReversalDetails, printResults);
		}
	}


	private void preview(boolean printResults) {
		AccountingEventJournal eventJournal = this.accountingUtils.generateEventJournal(this.event, this.accountInfo.getClientAccount());
		assertEventJournalDetails(eventJournal, printResults);
	}


	private void assertEventJournalDetails(AccountingEventJournal eventJournal, boolean printResults) {
		Assertions.assertNotNull(eventJournal, "Event journal was not generated.");
		String[] actualDetailStrings = CollectionUtils.getConverted(eventJournal.getDetailList(), detail -> {
			detail.setJournal(eventJournal);
			return detail.toStringFormatted(false);
		}).toArray(new String[0]);
		AccountingUtils.assertJournalDetailList(actualDetailStrings, this.expectedEventJournalDetails, printResults);
	}
}
