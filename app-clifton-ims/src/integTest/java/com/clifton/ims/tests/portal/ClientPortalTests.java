package com.clifton.ims.tests.portal;


import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.spring.IntegrationJsonConfiguration;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.search.PortalEntitySourceSectionSearchForm;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileExtended;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.setup.search.PortalFileCategorySearchForm;
import com.clifton.portal.file.upload.PortalFileUploadService;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.Date;
import java.util.List;


/**
 * @author StevenF
 */

//Ignoring until we do cross app tests on bamboo?
@Disabled
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {IntegrationJsonConfiguration.class})
public class ClientPortalTests {

	@Resource
	PortalEntityService portalEntityService;

	@Resource
	PortalEntitySetupService portalEntitySetupService;

	@Resource
	PortalFileSetupService portalFileSetupService;

	@Resource
	PortalFileService portalFileService;

	@Resource
	PortalFileUploadService portalFileUploadService;

	@Resource
	PortalSecurityUserService portalSecurityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////            PortalEntityService Test Methods             ///////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void getPortalEntityList() {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setEntityTypeId(Short.valueOf("1"));
		List<PortalEntity> portalEntityList = this.portalEntityService.getPortalEntityList(searchForm);
		Assertions.assertEquals(10, portalEntityList.size());
	}


	@Test
	public void savePortalEntity() {
		PortalEntity portalEntity = new PortalEntity();
		portalEntity.setName("Best.Portal.Entity.Ever.");
		portalEntity.setDescription("No one, and I mean no one, has ever created a better portal entity.");
		portalEntity.setStartDate(new Date(-1));
		portalEntity.setSourceFkFieldId(Integer.valueOf(String.valueOf(Math.round(Math.random() * 1000))));
		portalEntity.setFolderName("Stuff Goes Here");
		portalEntity.setUpdateDate(new Date());
		portalEntity.setEntitySourceSection(getPortalEntitySourceSectionByName("Portfolio Run"));
		portalEntity = this.portalEntityService.savePortalEntity(portalEntity);
		Assertions.assertNotNull(portalEntity.getId());
	}


	@Test
	public void getPortalEntityFieldValueListByEntity() {
		String fieldValue = "";
		PortalEntity portalEntity = getPortalEntityByTypeIdAndName(Short.valueOf("1"), "Jack Hansen");
		if (portalEntity != null) {
			List<PortalEntityFieldValue> portalEntityFieldValueList = this.portalEntityService.getPortalEntityFieldValueListByEntity(portalEntity.getId());
			for (PortalEntityFieldValue portalEntityFieldValue : CollectionUtils.getIterable(portalEntityFieldValueList)) {
				if ("Professional Designations".equals(portalEntityFieldValue.getPortalEntityFieldType().getName())) {
					fieldValue = portalEntityFieldValue.getValue();
				}
			}
		}
		Assertions.assertEquals("CFA", fieldValue);
	}


	@Test
	public void getPortalEntityRollupListByParent() {
		int size = 0;
		PortalEntity portalEntity = getPortalEntityByTypeIdAndName(Short.valueOf("6"), "Mercer Funds");
		if (portalEntity != null) {
			List<PortalEntityRollup> portalEntityRollupList = this.portalEntityService.getPortalEntityRollupListByParent(portalEntity.getId());
			size = portalEntityRollupList.size();
		}
		Assertions.assertTrue(0 < size);
	}

	////////////////////////////////////////////////////////////////////////////
	////////           PortalFileSetupService Test Methods           ///////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void getPortalFileCategoryList() {
		PortalFileCategorySearchForm searchForm = new PortalFileCategorySearchForm();
		searchForm.setFolderName("Documentation");
		List<PortalFileCategory> portalFileCategoryList = this.portalFileSetupService.getPortalFileCategoryList(searchForm);
		Assertions.assertEquals("Documentation", portalFileCategoryList.get(0).getFolderName());
	}


	@Test
	@Disabled
	public void getPortalFileCategoryForUploadList() {
	}


	////////////////////////////////////////////////////////////////////////////
	////////             PortalFileService Test Methods              ///////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	@Disabled //How to validate, we don't return the list, should we?
	public void approvePortalFileList() {
		this.portalFileService.approvePortalFileList(new Integer[]{1, 2, 3}, true);
	}


	@Test
	@Disabled
	public void getPortalFileBySource() {
	}


	@Test
	public void savePortalFile() {
		PortalEntity portalEntity = getPortalEntityByTypeIdAndName(Short.valueOf("6"), "GuideStone Fund Group");

		PortalFile portalFile = new PortalFile();
		portalFile.setSourceFkFieldId("AI".hashCode());
		portalFile.setFileCategory(this.portalFileSetupService.getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, "IMA"));
		portalFile.setPostPortalEntity(portalEntity);
		portalFile.setReportDate(new Date());
		portalFile = this.portalFileService.savePortalFile(portalFile);
		Assertions.assertNotNull(portalFile.getId());
	}


	@Test
	@Disabled
	public void deletePortalFile() {
	}


	@Test
	public void getPortalFileExtendedList() {
		PortalFileExtendedSearchForm searchForm = new PortalFileExtendedSearchForm();
		searchForm.setPostPortalEntityId(1382);
		List<PortalFileExtended> portalFileExtendedList = this.portalFileService.getPortalFileExtendedList(searchForm);
		Assertions.assertNotNull(portalFileExtendedList);
	}


	@Test
	public void uploadPortalFile() throws Exception {
		uploadPortalFile("classpath:1.pdf");
	}


	private PortalFile uploadPortalFile(String fileName) throws Exception {
		ResourceLoader loader = new DefaultResourceLoader();
		InputStream inputStream = loader.getResource(fileName).getInputStream();

		PortalFile portalFile = new PortalFile();
		portalFile.setReportDate(new Date());
		portalFile.setPostPortalEntity(this.portalEntityService.getPortalEntity(Short.valueOf("1")));
		portalFile.setFileCategory(this.portalFileSetupService.getPortalFileCategory(Short.valueOf("3")));
		portalFile.setFile(new MockMultipartFile("uploadPortalFileTestFile.pdf", "uploadPortalFileTestFile.pdf", null, inputStream));
		portalFile = this.portalFileUploadService.uploadPortalFile(portalFile, FileDuplicateActions.ERROR);
		Assertions.assertNotNull(portalFile.getId());
		return portalFile;
	}


	@Test
	public void downloadPortalFile() throws Exception {
		PortalFile portalFile = uploadPortalFile("classpath:1.pdf");
		FileWrapper fileWrapper = this.portalFileService.downloadPortalFile(portalFile.getId(), Short.valueOf("1"));
		Assertions.assertNotNull(fileWrapper.getFile());
	}


	@Test
	public void downloadPortalFileList() throws Exception {
		Integer[] portalFileIds = new Integer[3];
		for (int i = 0; i < 3; i++) {
			portalFileIds[i] = uploadPortalFile("classpath:" + (i + 1) + ".pdf").getId();
		}
		FileWrapper fileWrapper = this.portalFileService.downloadPortalFileList(portalFileIds, Short.valueOf("1"), false);
		Assertions.assertNotNull(fileWrapper.getFile());
	}


	@Test
	@Disabled
	//Ignoring for now - Mary is making a new list save that will have a request mapping - this doesn't for now.
	public void setSavePortalSecurityUser() {
		PortalSecurityUser user = this.portalSecurityUserService.getPortalSecurityUser(Short.valueOf("1"));
		this.portalSecurityUserService.savePortalSecurityUser(user);
	}


	////////////////////////////////////////////////////////////////////////////////
	private PortalEntity getPortalEntityByTypeIdAndName(short typeId, String name) {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setEntityTypeId(typeId);
		List<PortalEntity> portalEntityList = this.portalEntityService.getPortalEntityList(searchForm);
		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			if (name.equals(portalEntity.getName())) {
				return portalEntity;
			}
		}
		return null;
	}


	private PortalEntitySourceSection getPortalEntitySourceSectionByName(String name) {
		PortalEntitySourceSectionSearchForm searchForm = new PortalEntitySourceSectionSearchForm();
		searchForm.setName("Portfolio Run");
		List<PortalEntitySourceSection> sourceSectionList = this.portalEntitySetupService.getPortalEntitySourceSectionList(searchForm);
		Assertions.assertTrue(sourceSectionList != null && !sourceSectionList.isEmpty());
		return sourceSectionList.get(0);
	}
}
