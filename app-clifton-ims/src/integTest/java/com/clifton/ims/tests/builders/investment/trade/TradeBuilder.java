package com.clifton.ims.tests.builders.investment.trade;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeOpenCloseType;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.group.TradeGroup;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


@SuppressWarnings("hiding")
public class TradeBuilder {

	private final TradeService tradeService;
	private final Trade trade = new Trade();
	private final List<TradeFill> fills = new ArrayList<>();

	// References to TradeOpenCloseTypes that will be looked up via TradeService if null
	public static final AtomicReference<TradeOpenCloseType> BUY = new AtomicReference<>(),
			BUY_TO_OPEN = new AtomicReference<>(),
			BUY_TO_CLOSE = new AtomicReference<>(),
			SELL = new AtomicReference<>(),
			SELL_TO_OPEN = new AtomicReference<>(),
			SELL_TO_CLOSE = new AtomicReference<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TradeBuilder(TradeService tradeService) {
		this.tradeService = tradeService;
		setTradeOpenCloseType(BUY, (short) 1);
		setTradeOpenCloseType(BUY_TO_OPEN, (short) 2);
		setTradeOpenCloseType(BUY_TO_CLOSE, (short) 3);
		setTradeOpenCloseType(SELL, (short) 4);
		setTradeOpenCloseType(SELL_TO_OPEN, (short) 5);
		setTradeOpenCloseType(SELL_TO_CLOSE, (short) 6);
	}


	private void setTradeOpenCloseType(AtomicReference<TradeOpenCloseType> typeReference, short id) {
		if (typeReference.get() == null && this.tradeService != null) {
			typeReference.compareAndSet(null, this.tradeService.getTradeOpenCloseType(id));
		}
	}


	public static TradeBuilder trade(TradeService tradeService) {
		return new TradeBuilder(tradeService);
	}


	public Trade build() {
		if (!CollectionUtils.isEmpty(this.fills)) {
			// set shallow clone of trade on each fill to avoid recursion in serialization
			this.fills.forEach(fill -> fill.setTrade(BeanUtils.cloneBean(this.trade, false, false)));
			this.trade.setTradeFillList(this.fills);
		}

		return this.trade;
	}


	public Trade buildAndSave() {
		Trade saved = this.tradeService.saveTrade(this.trade);
		if (!CollectionUtils.isEmpty(this.fills)) {
			for (TradeFill fill : CollectionUtils.getIterable(this.fills)) {
				fill.setTrade(saved);
				this.tradeService.saveTradeFill(fill);
			}
			// fetch trade to get the fills
			saved = this.tradeService.getTrade(saved.getId());
		}
		return saved;
	}


	public TradeBuilder addFill(BigDecimal quantity, BigDecimal price) {
		TradeFill fill = new TradeFill();
		fill.setQuantity(quantity);
		fill.setNotionalUnitPrice(price);
		this.fills.add(fill);

		return this;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public TradeBuilder withAccountingNotional(BigDecimal notional) {
		this.trade.setAccountingNotional(notional);
		return this;
	}


	public TradeBuilder withAccrualAmount1(BigDecimal accrualAmount1) {
		this.trade.setAccrualAmount1(accrualAmount1);
		return this;
	}


	public TradeBuilder withAccrualAmount2(BigDecimal accrualAmount2) {
		this.trade.setAccrualAmount2(accrualAmount2);
		return this;
	}


	public TradeBuilder withCurrentFactor(BigDecimal currentFactor) {
		this.trade.setCurrentFactor(currentFactor);
		return this;
	}


	public TradeBuilder withInvestmentSecurity(InvestmentSecurity security) {
		this.trade.setInvestmentSecurity(security);
		return this;
	}


	public TradeBuilder withBuy(boolean buy) {
		this.trade.setBuy(buy);
		this.trade.setOpenCloseType(buy ? BUY.get() : SELL.get());
		return this;
	}


	public TradeBuilder withTradeDestination(TradeDestination tradeDestination) {
		this.trade.setTradeDestination(tradeDestination);
		return this;
	}


	public TradeBuilder withQuantityIntended(BigDecimal quantityIntended) {
		this.trade.setQuantityIntended(quantityIntended);
		return this;
	}


	public TradeBuilder withAverageUnitPrice(BigDecimal averageUnitPrice) {
		this.trade.setAverageUnitPrice(averageUnitPrice);
		return this;
	}


	public TradeBuilder withExpectedUnitPrice(BigDecimal expectedUnitPrice) {
		this.trade.setExpectedUnitPrice(expectedUnitPrice);
		return this;
	}


	public TradeBuilder withNotionalMultiplier(BigDecimal notionalMultiplier) {
		this.trade.setNotionalMultiplier(notionalMultiplier);
		return this;
	}


	public TradeBuilder withOriginalFace(BigDecimal originalFace) {
		this.trade.setOriginalFace(originalFace);
		return this;
	}


	public TradeBuilder withExchangeRate(BigDecimal exchangeRate) {
		this.trade.setExchangeRateToBase(exchangeRate);
		return this;
	}


	public TradeBuilder withPayingSecurity(InvestmentSecurity payingSecurity) {
		this.trade.setPayingSecurity(payingSecurity);
		return this;
	}


	public TradeBuilder withClientAccount(InvestmentAccount clientAccount) {
		this.trade.setClientInvestmentAccount(clientAccount);
		return this;
	}


	public TradeBuilder withHoldingAccount(InvestmentAccount holdingAccount) {
		this.trade.setHoldingInvestmentAccount(holdingAccount);
		return this;
	}


	public TradeBuilder withExecutingBroker(BusinessCompany executingBroker) {
		this.trade.setExecutingBrokerCompany(executingBroker);
		return this;
	}


	public TradeBuilder withTradeExecutionType(TradeExecutionType tradeExecutionType) {
		this.trade.setTradeExecutionType(tradeExecutionType);
		return this;
	}


	public TradeBuilder withExecutingSponsor(BusinessCompany executingSponsor) {
		this.trade.setExecutingSponsorCompany(executingSponsor);
		return this;
	}


	public TradeBuilder withSettlementDate(Date settlementDate) {
		this.trade.setSettlementDate(settlementDate);
		return this;
	}


	public TradeBuilder withTradeDate(Date tradeDate) {
		this.trade.setTradeDate(tradeDate);
		return this;
	}


	public TradeBuilder withTradeType(TradeType tradeType) {
		this.trade.setTradeType(tradeType);
		return this;
	}


	public TradeBuilder withTraderUser(SecurityUser traderUser) {
		this.trade.setTraderUser(traderUser);
		return this;
	}


	public TradeBuilder withCommissionAmount(BigDecimal commissionAmount) {
		this.trade.setCommissionAmount(commissionAmount);
		return this;
	}


	public TradeBuilder withCommissionPerUnit(BigDecimal commissionPerUnit) {
		this.trade.setCommissionPerUnit(commissionPerUnit);
		return this;
	}


	public TradeBuilder withFeeAmount(BigDecimal feeAmount) {
		this.trade.setFeeAmount(feeAmount);
		return this;
	}


	public TradeBuilder withEnableCommissionOverride(boolean enableCommissionOverride) {
		this.trade.setEnableCommissionOverride(enableCommissionOverride);
		return this;
	}


	public TradeBuilder withDescription(String description) {
		this.trade.setDescription(description);
		return this;
	}


	public TradeBuilder withBlockTrade(boolean blockTrade) {
		this.trade.setBlockTrade(blockTrade);
		return this;
	}


	public TradeBuilder withOpenCloseType(AtomicReference<TradeOpenCloseType> openCloseTypeReference) {
		this.trade.setOpenCloseType(openCloseTypeReference.get());
		return this;
	}


	public TradeBuilder withTradeGroup(TradeGroup group) {
		this.trade.setTradeGroup(group);
		return this;
	}


	public TradeBuilder withNovation(boolean novation) {
		this.trade.setNovation(novation);
		return this;
	}


	public TradeBuilder withAssignmentCompany(BusinessCompany company) {
		this.trade.setAssignmentCompany(company);
		return this;
	}
}
