package com.clifton.ims.tests.lending.repo;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.lending.repo.upload.LendingRepoRollUploadCommand;
import com.clifton.lending.repo.upload.LendingRepoUploadService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * Placeholder to test Lending Repo Uploads locally
 * Put the current upload file into this directory under SWIB Collateral File.xlsx and then enable this test and run it. Prints out the total time to make it easier to review performance stats
 */
@Disabled("Must be run with specific file for the back up in time.  Can be used for local testing/debugging.")
public class LendingRepoUploadTest extends BaseImsIntegrationTest {


	@Resource
	private LendingRepoUploadService lendingRepoUploadService;


	@Test
	public void testRepoUploadFile() {
		long start = System.currentTimeMillis();

		LendingRepoRollUploadCommand command = new LendingRepoRollUploadCommand();
		command.setFile(new MultipartFileImpl("src/integTest/java/com/clifton/ims/tests/lending/repo/SWIB Collateral File.xlsx"));
		command.setPartialUploadAllowed(false);
		//command.setPositionRetrievalOptionRecompile(true);

		this.lendingRepoUploadService.uploadLendingRepoRollUploadFile(command);

		System.out.println("UPLOAD TIME " + DateUtils.getTimeDifference(System.currentTimeMillis() - start, true));
	}
}
