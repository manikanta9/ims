package com.clifton.ims.tests.trade.order;

import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * A Test Context used in conjunction with the TradeOrderFixSimTests to easily move test configuration data from the tests to the execution methods in TradeOrderFixSimTestHelper.
 * <p>
 * TestDate - current test date (e.g. today).
 * FixOrderId - stores the FIX OrderID value for the test Fix Transaction.
 * FixAllocationId - stores the FIX AllocationID value for the test Fix Transaction.
 * TemplateFixOrderId - stores the FIX OrderID value currently found in the fix message templates.
 * TemplateFixAllocationId - stores the FIX AllocationID value currently found in the fix message templates.
 * FixMessageMap - map of lists of orderd messages keyed on the message type symbol. Derived from test data.
 * AllocationReportTemplate is the message template that will be used to create an AllocationReportt message to send to FixSim service for replay.
 * ExpectedQuantity is the price returned by FixSim which should be the average price for the trade order and should be applied to fills.  This can be utilized by the test  after the FIX Transaction to calculate fill prices, etc.
 * OrderReplacementValues is a map of a string key and replacement string for values in the FixNewOrderSingle message template
 * ExecutionReportReplacementValues is a map of a string key and replacement string value where the value is to be substituted for each occurrence of the key in the ExecutionReport messages.
 * AllocationReplacementValues is a map of a string key and replacement string value where the value is to be substituted for each occurrence of the key in the Allocation messages such as acknowledgments (P), excluding the AllocationReport.
 * AllocationReportReplacementValues is a map of a string key and replacement string value where the value is to be substituted for each occurrence of the key in the AllocationReport message.
 * GlobalReplacementValues is a map of a string key and a replacement string value where the value is to be substituted for each occurrence of the key in all messages.
 * AllocationReportPriceTags is a list of integer values indicating tags that have prices that should be replaced with an average price from the allocation instruction message.
 * FixSimInstance contains the FixSim Instance value for FixSim (e.g. "PARAMETRIC").
 * FixSimSessionName contains the Session name to be used by this test (e.g. "REDI Multi-Broker")
 * ExpectedTradeOrderStatus - the final expected order status of the execution phase of the test
 * ExpectedAllocationStatus - the final expected allocation status of the order
 * ExpectedAllocationAckStatus - the last expected status of the allocation ack messages (P) in the allocation transaction.
 * ResponseMessageDelay -  a waiting period, in milliseconds, used to delay the sending a response.
 * FixTagsWithListValues - allows a test to specify tag numbers whose values are delimited strings and must be compared using value sets instead of straight string comparisons.
 * ValuesListDelimiter - delimiter used in multi-value fields specified in FixTagsWithListValues.  The default delimiter value is ";".
 * ComparisonIgnoreFieldsNewOrder - a set of Integers indicating tag numbers to ignore when comparing a NewOrderSingle (D) message to its template.
 * ComparisonIgnoreFieldsAllocInstruction - a set of Integers indicating tag numbers to ignore when comparing an AllocationInstruction (J) message to its template.
 */
class TestContext {

	private Date testDate;
	private String fixOrderId;
	private String fixAllocationId;
	private String templateFixOrderId;
	private String templateFixAllocationId;
	private Map<String, List<String>> fixMessageMap;
	private Map<Enum<?>, Map<String, String>> fixMessageReplacementMap;
	private String fixSimInstance;
	private String fixSimSessionName;
	private TradeOrderStatuses expectedTradeOrderStatus;
	private TradeOrderAllocationStatuses expectedAllocationStatus;
	private TradeOrderAllocationStatuses expectedAllocationAckStatus;
	private int responseMessageDelay;
	private List<Integer> fixTagsWithListValues;
	private String valueListDelimiter;
	private Set<Integer> comparisonIgnoreFieldsNewOrder;
	private Set<Integer> comparisonIgnoreFieldsAllocInstruction;

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	public TestContext() {
		this.testDate = new Date();
		this.fixMessageReplacementMap = new HashMap<>();

		this.fixMessageReplacementMap.put(ReplacementMapKeys.GLOBAL, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.ORDER, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.ORDER_EXECUTION, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.EXECUTION_REPORT, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.ALLOC_INSTRUCTION, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.ALLOC_ALL, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.ALLOC_ACK, new HashMap<>());
		this.fixMessageReplacementMap.put(ReplacementMapKeys.ALLOC_REPORT, new HashMap<>());
		this.fixTagsWithListValues = new ArrayList<>();
		this.valueListDelimiter = ";";
		this.comparisonIgnoreFieldsNewOrder = new HashSet<>();
		this.comparisonIgnoreFieldsAllocInstruction = new HashSet<>();
	}

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	/**
	 * Convenience method to allow tests to easily populate the replacement map.
	 */
	public void addFixMessageReplacement(Enum<?> categoryKey, String replacementKey, String replacementValue) {
		this.getFixMessageReplacementMap().get(categoryKey).put(replacementKey, replacementValue);
	}

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	public Date getTestDate() {
		return this.testDate;
	}


	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}


	public String getFixOrderId() {
		return this.fixOrderId;
	}


	public void setFixOrderId(String fixOrderId) {
		this.fixOrderId = fixOrderId;
	}


	public String getFixAllocationId() {
		return this.fixAllocationId;
	}


	public void setFixAllocationId(String fixAllocationId) {
		this.fixAllocationId = fixAllocationId;
	}


	public String getTemplateFixOrderId() {
		return this.templateFixOrderId;
	}


	public void setTemplateFixOrderId(String templateFixOrderId) {
		this.templateFixOrderId = templateFixOrderId;
	}


	public String getTemplateFixAllocationId() {
		return this.templateFixAllocationId;
	}


	public void setTemplateFixAllocationId(String templateFixAllocationId) {
		this.templateFixAllocationId = templateFixAllocationId;
	}


	public Map<String, List<String>> getFixMessageMap() {
		return this.fixMessageMap;
	}


	public void setFixMessageMap(Map<String, List<String>> fixMessageMap) {
		this.fixMessageMap = fixMessageMap;
	}


	public Map<Enum<?>, Map<String, String>> getFixMessageReplacementMap() {
		return this.fixMessageReplacementMap;
	}


	public void setFixMessageReplacementMap(Map<Enum<?>, Map<String, String>> fixMessageReplacementMap) {
		this.fixMessageReplacementMap = fixMessageReplacementMap;
	}


	public String getFixSimInstance() {
		return this.fixSimInstance;
	}


	public void setFixSimInstance(String fixSimInstance) {
		this.fixSimInstance = fixSimInstance;
	}


	public String getFixSimSessionName() {
		return this.fixSimSessionName;
	}


	public void setFixSimSessionName(String fixSimSessionName) {
		this.fixSimSessionName = fixSimSessionName;
	}


	public TradeOrderStatuses getExpectedTradeOrderStatus() {
		return this.expectedTradeOrderStatus;
	}


	public void setExpectedTradeOrderStatus(TradeOrderStatuses expectedTradeOrderStatus) {
		this.expectedTradeOrderStatus = expectedTradeOrderStatus;
	}


	public TradeOrderAllocationStatuses getExpectedAllocationStatus() {
		return this.expectedAllocationStatus;
	}


	public void setExpectedAllocationStatus(TradeOrderAllocationStatuses expectedAllocationStatus) {
		this.expectedAllocationStatus = expectedAllocationStatus;
	}


	public TradeOrderAllocationStatuses getExpectedAllocationAckStatus() {
		return this.expectedAllocationAckStatus;
	}


	public void setExpectedAllocationAckStatus(TradeOrderAllocationStatuses expectedAllocationAckStatus) {
		this.expectedAllocationAckStatus = expectedAllocationAckStatus;
	}


	public int getResponseMessageDelay() {
		return this.responseMessageDelay;
	}


	public void setResponseMessageDelay(int responseMessageDelay) {
		this.responseMessageDelay = responseMessageDelay;
	}


	public List<Integer> getFixTagsWithListValues() {
		return this.fixTagsWithListValues;
	}


	public void setFixTagsWithListValues(List<Integer> fixTagsWithListValues) {
		this.fixTagsWithListValues = fixTagsWithListValues;
	}


	public String getValueListDelimiter() {
		return this.valueListDelimiter;
	}


	public void setValueListDelimiter(String valueListDelimiter) {
		this.valueListDelimiter = valueListDelimiter;
	}


	public Set<Integer> getComparisonIgnoreFieldsNewOrder() {
		return this.comparisonIgnoreFieldsNewOrder;
	}


	public void setComparisonIgnoreFieldsNewOrder(Set<Integer> comparisonIgnoreFieldsNewOrder) {
		this.comparisonIgnoreFieldsNewOrder = comparisonIgnoreFieldsNewOrder;
	}


	public Set<Integer> getComparisonIgnoreFieldsAllocInstruction() {
		return this.comparisonIgnoreFieldsAllocInstruction;
	}


	public void setComparisonIgnoreFieldsAllocInstruction(Set<Integer> comparisonIgnoreFieldsAllocInstruction) {
		this.comparisonIgnoreFieldsAllocInstruction = comparisonIgnoreFieldsAllocInstruction;
	}
}
