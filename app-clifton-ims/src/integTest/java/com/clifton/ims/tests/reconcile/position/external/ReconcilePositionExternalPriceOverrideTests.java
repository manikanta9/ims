package com.clifton.ims.tests.reconcile.position.external;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.rebuild.AccountingM2MRebuildService;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseSearchForm;
import com.clifton.accounting.m2m.search.AccountingM2MDailySearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionService;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;
import com.clifton.reconcile.position.external.ReconcilePositionExternalPriceOverride;
import com.clifton.reconcile.position.search.ReconcilePositionSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionExternalPriceOverrideTests extends BaseImsIntegrationTest {

	@Resource
	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;

	@Resource
	private ReconcilePositionService reconcilePositionService;

	@Resource
	private AccountingM2MService accountingM2MService;

	@Resource
	private AccountingM2MRebuildService accountingM2MRebuildService;

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private InvestmentSetupService investmentSetupService;


	@Test
	public void testPriceOverrideCreateAndDelete() {
		ReconcilePositionExternalPriceOverride override = null;
		AccountingM2MDaily m2mDaily = null;
		AccountingM2MDaily originalM2M = null;
		try {
			ReconcilePosition position = getSingleSecurityPosition();
			InvestmentSecurity security = position.getInvestmentSecurity();
			m2mDaily = getAccountingM2MDaily(position);
			if (m2mDaily != null) {
				// Only test if we find it
				originalM2M = new AccountingM2MDaily();
				BeanUtils.copyProperties(m2mDaily, originalM2M);

				m2mDaily = unbookM2MEntry(m2mDaily);
				override = createPriceOverride(security, position);

				m2mDaily = this.accountingM2MService.getAccountingM2MDailyPopulated(m2mDaily.getId());

				Assertions.assertEquals(1, m2mDaily.getExpenseList().size());

				AccountingM2MDailyExpense expense = m2mDaily.getExpenseList().get(0);
				Assertions.assertEquals(override.getId(), expense.getSourceFkFieldId());
				Assertions.assertEquals("M2M Price Adjustment", expense.getType().getName());
			}
		}
		finally {
			if (override != null) {
				this.reconcilePositionExternalDailyService.deleteReconcilePositionExternalPriceOverride(override.getId());
			}
			if (m2mDaily != null && originalM2M != null) {
				m2mDaily = this.accountingM2MService.getAccountingM2MDailyPopulated(m2mDaily.getId());
				Assertions.assertEquals(0, m2mDaily.getExpenseList().size());

				if (originalM2M.isBooked()) {
					bookM2M(m2mDaily);
				}
				else if (originalM2M.isReconciled()) {
					reconcileM2M(m2mDaily);
				}
			}
		}
	}


	@Test
	public void testPriceOverrideCreateAndDeleteWithUnbookingM2M() {
		ReconcilePositionExternalPriceOverride override = null;
		AccountingM2MDaily m2mDaily = null;
		AccountingM2MDaily originalM2M = null;
		try {
			ReconcilePosition position = getSingleSecurityPosition();
			InvestmentSecurity security = position.getInvestmentSecurity();
			m2mDaily = getAccountingM2MDaily(position);
			if (m2mDaily != null) {
				// Only test if we find it
				originalM2M = new AccountingM2MDaily();
				BeanUtils.copyProperties(m2mDaily, originalM2M);

				if (!m2mDaily.isBooked()) {
					m2mDaily = bookM2M(m2mDaily);
				}
				override = createPriceOverride(security, position);

				m2mDaily = this.accountingM2MService.getAccountingM2MDailyPopulated(m2mDaily.getId());

				Assertions.assertEquals(0, m2mDaily.getExpenseList().size());
			}
		}
		finally {
			if (override != null) {
				this.reconcilePositionExternalDailyService.deleteReconcilePositionExternalPriceOverride(override.getId());
			}
			if (m2mDaily != null && originalM2M != null) {
				if (originalM2M.isBooked()) {
					bookM2M(m2mDaily);
				}
				else if (originalM2M.isReconciled()) {
					reconcileM2M(m2mDaily);
				}
			}
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	private ReconcilePositionExternalPriceOverride createPriceOverride(InvestmentSecurity security, ReconcilePosition position) {
		Date positionDate = position.getPositionAccount().getPositionDate();

		ReconcilePositionExternalPriceOverride override = new ReconcilePositionExternalPriceOverride();
		override.setHoldingCompany(position.getPositionAccount().getHoldingAccount().getIssuingCompany());
		override.setInvestmentSecurity(security);
		override.setPositionDate(positionDate);
		override.setExternalPrice(position.getExternalPrice());
		override.setOverridePrice(MathUtils.add(position.getExternalPrice(), new BigDecimal(".1")));
		override.setNote("Stale Price");

		this.reconcilePositionExternalDailyService.saveReconcilePositionExternalPriceOverride(override);
		return override;
	}


	private ReconcilePosition getSingleSecurityPosition() {
		Date previousBusinessDay = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);

		ReconcilePositionSearchForm searchForm = new ReconcilePositionSearchForm();
		searchForm.setOrderBy("positionDate:desc");
		searchForm.addSearchRestriction(new SearchRestriction("positionDate", ComparisonConditions.LESS_THAN_OR_EQUALS, previousBusinessDay));
		searchForm.setInvestmentSecurityTypeId(this.investmentSetupService.getInvestmentTypeByName(InvestmentType.FUTURES).getId());
		searchForm.setExcludeClosedPositions(true);
		searchForm.setExternalPriceNotNull(true);
		searchForm.setLimit(1);
		return CollectionUtils.getFirstElement(this.reconcilePositionService.getReconcilePositionList(searchForm));
	}


	private AccountingM2MDaily unbookM2MEntry(AccountingM2MDaily m2mDaily) {
		this.accountingM2MRebuildService.unpostAccountingM2MDaily(m2mDaily.getMarkDate(), m2mDaily.getHoldingInvestmentAccount().getType().getId(), null, null, m2mDaily.getClientInvestmentAccount().getId(), true, true);
		return this.accountingM2MService.getAccountingM2MDailyPopulated(m2mDaily.getId());
	}


	private AccountingM2MDaily reconcileM2M(AccountingM2MDaily m2mDaily) {
		m2mDaily.setReconciled(true);
		this.accountingM2MService.saveAccountingM2MDaily(m2mDaily);
		return m2mDaily;
	}


	private AccountingM2MDaily bookM2M(AccountingM2MDaily m2mDaily) {
		AccountingM2MDaily reconciledM2mDaily = !m2mDaily.isReconciled() ? reconcileM2M(m2mDaily) : m2mDaily;

		this.accountingM2MRebuildService.bookAndPostAccountingM2MDaily(reconciledM2mDaily.getMarkDate(), reconciledM2mDaily.getHoldingInvestmentAccount().getType().getId(), null, null, reconciledM2mDaily.getClientInvestmentAccount().getId(),
				false, false);
		return this.accountingM2MService.getAccountingM2MDailyPopulated(reconciledM2mDaily.getId());
	}


	private AccountingM2MDaily getAccountingM2MDaily(ReconcilePosition position) {
		AccountingM2MDailySearchForm searchForm = new AccountingM2MDailySearchForm();
		searchForm.setHoldingInvestmentAccountId(position.getPositionAccount().getHoldingAccount().getId());
		searchForm.setMarkDate(position.getPositionAccount().getPositionDate());

		boolean m2mIsInLocalCurrency = Boolean.parseBoolean((String) this.systemColumnValueService.getSystemColumnCustomValue(position.getPositionAccount().getHoldingAccount().getId(), InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME, InvestmentAccount.HOLDING_ACCOUNT_M2M_LOCAL_CURRENCY));
		if (m2mIsInLocalCurrency) {
			searchForm.setMarkCurrencyId(position.getPositionAccount().getHoldingAccount().getBaseCurrency().getId());
		}
		AccountingM2MDaily m2mDaily = CollectionUtils.getFirstElement(this.accountingM2MService.getAccountingM2MDailyList(searchForm));

		if (m2mDaily != null) {
			AccountingM2MDailyExpenseSearchForm expenseSearchForm = new AccountingM2MDailyExpenseSearchForm();
			expenseSearchForm.setM2mDailyId(m2mDaily.getId());
			m2mDaily.setExpenseList(this.accountingM2MService.getAccountingM2MDailyExpenseList(expenseSearchForm));
		}
		return m2mDaily;
	}
}
