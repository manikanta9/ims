package com.clifton.ims.tests.compliance;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleSearchForm;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.bean.search.SystemBeanPropertyTypeSearchForm;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ComplianceTransactionNotionalLimitTests extends ComplianceRealTimeTradeTests {

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private ComplianceRuleService complianceRuleService;

	@Resource
	private InvestmentSetupService investmentSetupService;


	@Test
	public void testFuturesTransactionNotionalLimit() {
		ComplianceRule rule;
		SystemBean bean = null;
		try {
			bean = this.systemBeanService.getSystemBeanByName("Futures Transaction 35,000,000 Notional Limit Rule : null");
			bean = this.systemBeanService.getSystemBean(bean.getId());
			ComplianceRuleSearchForm ruleSearchForm = new ComplianceRuleSearchForm();
			ruleSearchForm.setName("Futures Transaction 35,000,000 Notional Limit Rule");
			//Just check to see if it exists.
			CollectionUtils.getFirstElementStrict(this.complianceRuleService.getComplianceRuleList(ruleSearchForm));
		}
		catch (Exception e) {
			if (bean == null) {
				SystemBeanType beanType = this.systemBeanService.getSystemBeanTypeByName("Transaction Notional Limit Rule");

				SystemBeanPropertyTypeSearchForm propertyTypeSearchForm = new SystemBeanPropertyTypeSearchForm();
				propertyTypeSearchForm.setSearchPattern("Max Notional");
				propertyTypeSearchForm.setTypeId(beanType.getId());
				SystemBeanPropertyType propertyType = CollectionUtils.getOnlyElementStrict(this.systemBeanService.getSystemBeanPropertyTypeList(propertyTypeSearchForm));

				SystemBeanProperty property = new SystemBeanProperty();
				property.setType(propertyType);
				property.setValue("35000000");

				List<SystemBeanProperty> propertyList = new ArrayList<>();
				propertyList.add(property);

				bean = new SystemBean();
				bean.setType(beanType);
				bean.setName("Doesn't matter; overwritten when compliance rule is saved - not sure why that needs to be done.");
				bean.setDescription("Transaction notional in base currency cannot exceed this maximum for futures.");
				bean.setPropertyList(propertyList);
			}
			ComplianceRuleType ruleType = this.complianceRuleService.getComplianceRuleTypeByName("Transaction Notional Limit");

			rule = new ComplianceRule();
			rule.setName("Futures Transaction 35,000,000 Notional Limit Rule");
			rule.setDescription("Transaction notional in base currency cannot exceed this maximum for futures.");
			rule.setRuleType(ruleType);
			rule.setRuleEvaluatorBean(bean);
			rule.setInvestmentType(this.investmentSetupService.getInvestmentTypeByName(InvestmentType.FUTURES));
			rule.setBatch(true);
			rule.setRealTime(true);
			rule.setCurrencyType(InvestmentCurrencyTypes.DOMESTIC);
			this.complianceRuleService.saveComplianceRule(rule);
		}

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUTURES, true, "Futures Transaction 35,000,000 Notional Limit Rule");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "CLK6", new BigDecimal(34999), false, new Date());
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUTURES, "CLK6", new BigDecimal(35001), false, new Date());
		validateViolationNotes(tradeResults, "Futures Transaction 35,000,000 Notional Limit Rule : Notional value of '35,001,000.00' exceeds maximum notional value of '35,000,000.00' : Failed");
	}
}
