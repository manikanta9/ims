package com.clifton.ims.tests.system;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionEntry;
import com.clifton.system.condition.SystemConditionService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>SystemConditionUtils</code> can be used to easily change condition entries on existing conditions for testing purposes
 *
 * @author manderson
 */
@Component
public class SystemConditionUtils {

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private SystemConditionService systemConditionService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * This can be useful for multi level conditions where one of the entries we don't want to evaluate for testing purposes
	 * For example: Billing Invoice Approval requires the Billing Definition to be Approved.  For testing, we don't care about that
	 * so before the test, we change that specific entry for the bean to be Always True - and then after the test we switch it back to require Approval
	 */
	public void changeSystemConditionEntryBean(String conditionName, String fromSystemBeanName, String toSystemBeanName) {
		SystemCondition condition = this.systemConditionService.getSystemConditionByName(conditionName);
		ValidationUtils.assertNotNull(condition, "Could not find condition with name " + conditionName);
		// Note: these are strict retrievals and will throw exception if missing
		SystemBean fromSystemBean = this.systemBeanService.getSystemBeanByName(fromSystemBeanName);
		SystemBean toSystemBean = this.systemBeanService.getSystemBeanByName(toSystemBeanName);

		// Get the condition with entry list populated
		condition = this.systemConditionService.getSystemConditionPopulated(condition.getId());
		changeSystemConditionEntryBeanImpl(condition.getConditionEntry().getChildEntryList(), fromSystemBean, toSystemBean);
	}


	private void changeSystemConditionEntryBeanImpl(List<SystemConditionEntry> entryList, SystemBean fromSystemBean, SystemBean toSystemBean) {
		for (SystemConditionEntry conditionEntry : CollectionUtils.getIterable(entryList)) {
			if (!CollectionUtils.isEmpty(conditionEntry.getChildEntryList())) {
				changeSystemConditionEntryBeanImpl(conditionEntry.getChildEntryList(), fromSystemBean, toSystemBean);
			}
			if (CompareUtils.isEqual(fromSystemBean, conditionEntry.getBean())) {
				conditionEntry.setBean(toSystemBean);
				this.systemConditionService.saveSystemConditionEntry(conditionEntry);
			}
		}
	}
}
