package com.clifton.ims.tests.rule;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


@Component
public class RuleViolationUtils {

	@Resource
	private RuleDefinitionService ruleDefinitionService;

	@Resource
	private RuleViolationService ruleViolationService;


	public List<RuleViolation> getRuleViolationList(String tableName, Number fkFieldId) {
		RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
		searchForm.setLinkedTableName(tableName);
		searchForm.setLinkedFkFieldId(MathUtils.getNumberAsLong(fkFieldId));
		return this.ruleViolationService.getRuleViolationList(searchForm);
	}


	public void assertNoRuleViolations(String tableName, Number fkFieldId) {
		List<RuleViolation> ruleViolationList = getRuleViolationList(tableName, fkFieldId);
		for (RuleViolation violation : CollectionUtils.getIterable(ruleViolationList)) {
			if (!violation.isIgnored()) {
				throw new AssertionError("Expected no non-ignored system violations but encountered violation: '" + violation.getViolationNote() + "'.");
			}
		}
	}


	public void assertRuleViolations(String tableName, IdentityObject entity, List<String> expectedViolationDescriptions) {
		List<RuleViolation> violationsNotYetMatched = getRuleViolationList(tableName, BeanUtils.getIdentityAsLong(entity));

		for (String expectedViolationDescription : CollectionUtils.getIterable(expectedViolationDescriptions)) {
			Iterator<RuleViolation> violationIterator = CollectionUtils.getIterable(violationsNotYetMatched).iterator();

			boolean violationFound = false;
			while (violationIterator.hasNext()) {
				RuleViolation violation = violationIterator.next();
				if (violation.getViolationNote().contains(expectedViolationDescription)) {
					violationFound = true;
					violationIterator.remove();
					break;
				}
			}

			if (!violationFound) {
				throw new AssertionError("Expected system violation '" + expectedViolationDescription + "' but this violation was not present.");
			}
		}

		if (!violationsNotYetMatched.isEmpty()) {
			throw new AssertionError("There were " + violationsNotYetMatched.size() + " unexpected violations: " + violationsNotYetMatched);
		}
	}


	/**
	 * Ignores the first violation to contain the contents of <code>expectedViolationDescription</code>. The violation only needs to contain the contents; it does not have to be an exact match.
	 *
	 * @param tableName                           the table name the violation is associated with (e.g. Trade)
	 * @param fkFieldId                           the record ID (e.g. Trade ID)
	 * @param expectedViolationNote               the violation description to ignore (actual violation only needs to contain this description - does not have to be exact match)
	 * @param throwExceptionIfViolationNotPresent whether to throw an exception if the violation is not present
	 * @return whether or not the violation was present and ignored
	 */
	public boolean ignoreViolation(String tableName, Number fkFieldId, String expectedViolationNote, boolean throwExceptionIfViolationNotPresent) {
		return ignoreViolations(tableName, fkFieldId, throwExceptionIfViolationNotPresent, expectedViolationNote);
	}


	/**
	 * Ignores the violations that contain the contents of one of the <code>expectedViolationDescriptions</code>. The violation only needs to contain the contents; it does not have to be an exact match.
	 *
	 * @param tableName                           the table name the violation is associated with (e.g. Trade)
	 * @param fkFieldId                           the record ID (e.g. Trade ID)
	 * @param throwExceptionIfViolationNotPresent whether to throw an exception if the violation is not present
	 * @param expectedViolationNotes              the violation description to ignore (actual violation only needs to contain this description - does not have to be exact match)
	 * @return whether or not a violation was present and ignored
	 */
	public boolean ignoreViolations(String tableName, Number fkFieldId, boolean throwExceptionIfViolationNotPresent, String... expectedViolationNotes) {
		if (ArrayUtils.isEmpty(expectedViolationNotes)) {
			return false;
		}
		List<RuleViolation> ruleViolationList = getRuleViolationList(tableName, fkFieldId);
		boolean violationIgnored = false;
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			for (String expectedViolationNote : expectedViolationNotes) {
				if (ruleViolation.getViolationNote().contains(expectedViolationNote)) {
					ruleViolation.setIgnored(true);
					ruleViolation.setIgnoreNote("I'm going to ignore this.");
					RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.ruleViolationService.saveRuleViolation(ruleViolation), 1);
					violationIgnored = true;
				}
			}
		}

		if (!violationIgnored && throwExceptionIfViolationNotPresent) {
			throw new AssertionError("Expected violation(s) '" + StringUtils.join(expectedViolationNotes, ",") + "'.");
		}

		return violationIgnored;
	}


	/**
	 * Ignores all violations that exist for the entity
	 */
	public void ignoreAllViolationsIfExist(String tableName, Number fkFieldId) {
		List<RuleViolation> ruleViolationList = getRuleViolationList(tableName, fkFieldId);
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(ruleViolationList)) {
			if (ruleViolation.getRuleAssignment().getRuleDefinition().isIgnorable() && !ruleViolation.isIgnored()) {
				ruleViolation.setIgnored(true);
				RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.ruleViolationService.saveRuleViolation(ruleViolation), 1);
			}
		}
	}


	/**
	 * Filtering system violation list by cause table
	 */
	public List<RuleViolation> filterViolationListByTypeName(String definitionTypeName, boolean inclusion, List<RuleViolation> ruleViolationList) {
		return BeanUtils.filter(ruleViolationList, ruleViolation -> inclusion == StringUtils.isEqual(definitionTypeName, ruleViolation.getRuleAssignment().getRuleDefinition().getName()));
	}


	public void createRuleAssignment(String ruleDefinitionName, BigDecimal ruleAmount, BigDecimal minAmount, BigDecimal maxAmount, IdentityObject additionalScopeEntity, IdentityObject entity) {
		RuleDefinition ruleDefinition = this.ruleDefinitionService.getRuleDefinitionByName(ruleDefinitionName);
		RuleAssignmentSearchForm ruleAssignmentSearchForm = new RuleAssignmentSearchForm();
		if (additionalScopeEntity != null) {
			ruleAssignmentSearchForm.setAdditionalScopeFkFieldId(BeanUtils.getIdentityAsLong(additionalScopeEntity));
		}
		ruleAssignmentSearchForm.setRuleDefinitionId(ruleDefinition.getId());
		ruleAssignmentSearchForm.setNullAdditionalScopeFkFieldId(additionalScopeEntity == null);
		ruleAssignmentSearchForm.setNullEntityFkFieldId(entity == null);
		ruleAssignmentSearchForm.setExcluded(false);
		ruleAssignmentSearchForm.setLimit(1);
		ruleAssignmentSearchForm.setOrderBy("additionalScopeFkFieldId:desc#entityFkFieldId:desc");

		RuleAssignment ruleAssignment = new RuleAssignment();
		List<RuleAssignment> ruleAssignmentList = this.ruleDefinitionService.getRuleAssignmentList(ruleAssignmentSearchForm);
		if (CollectionUtils.getSize(ruleAssignmentList) == 1) {
			ruleAssignment = CollectionUtils.getFirstElement(ruleAssignmentList);
			//If the search returned the default global assignment and the scope of the assignment to be created is not global
			//then just create a new assignment, otherwise override the global values.
			if (ruleAssignment == null || ((additionalScopeEntity != null || entity != null) && ruleAssignment.getAdditionalScopeFkFieldId() == null && ruleAssignment.getEntityFkFieldId() == null)) {
				ruleAssignment = new RuleAssignment();
			}
		}
		ruleAssignment.setRuleDefinition(ruleDefinition);
		ruleAssignment.setNote("Integration Test Assignment");
		if (additionalScopeEntity != null) {
			ruleAssignment.setAdditionalScopeFkFieldId(BeanUtils.getIdentityAsLong(additionalScopeEntity));
		}
		if (entity != null) {
			ruleAssignment.setEntityFkFieldId(BeanUtils.getIdentityAsLong(entity));
		}
		if (ruleAmount != null) {
			ruleAssignment.setRuleAmount(ruleAmount);
		}
		if (minAmount != null) {
			ruleAssignment.setMinAmount(minAmount);
		}
		if (maxAmount != null) {
			ruleAssignment.setMaxAmount(maxAmount);
		}
		if (ruleAssignment.getEndDate() != null) {
			//Set end date to tomorrow; we can't set to null because we don't serialize nulls;
			ruleAssignment.setEndDate(DateUtils.addDays(new Date(), 1));
		}
		this.ruleDefinitionService.saveRuleAssignment(ruleAssignment);
	}
}
