package com.clifton.ims.tests.compliance;


import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.type.ComplianceRuleType;
import com.clifton.compliance.run.ComplianceRun;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunDetail;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.test.protocol.ImsGsonFactory;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.google.gson.Gson;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class ComplianceRunTests extends BaseImsIntegrationTest {

	private static final String EXPECTED_RESULTS_FILE = "Compliance_Rule_Run_Expected.csv";
	private static final BigDecimal RESULT_VALUE_DIFFERENCE_THRESHOLD = new BigDecimal(500);


	/**
	 * This 'test' will simply generate results for each account in the provided list and print them
	 * in the format to be used in the Compliance_Rule_Run_Expected.csv for the other test in this class.
	 * <p>
	 * This is useful when you want to add test cases for a specific account
	 * that is known to have accurate calculations or if underlying valuation models change
	 * <p>
	 * Uncomment @Test to populate (buildDir)/resources/test/com/clifton/ims/tests/compliance/Compliance_Rule_Run_Expected.csv.
	 * Then copy the file to (testSrc)/java/com/clifton/ims/tests/compliance/Compliance_Rule_Run_Expected.csv.
	 */
//	@Test
	public void generateResultsForTests() throws IOException {
		List<String> accountNumberList = new ArrayList<String>() {{
			add("900100");
			add("900200");
			add("524500");
//			add("900030");
//			add("391320");
//			add("391360");
//			add("391240");
//			add("391280");
//			add("391200");
//			add("391120");
//			add("391160");
		}};

		Date processDate = DateUtils.toDate("05/01/2015");
		int totalBusinessDaysBackToProcess = 50; //generate results for 90 business days back
		int skipIncrement = 3; //Skip every business day so we don't need to check

		File output = new File(getClass().getResource(EXPECTED_RESULTS_FILE).getFile());
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(output))) {
			Gson gson = ImsGsonFactory.newImsGson(null).getGson();
			for (String accountNumber : CollectionUtils.getIterable(accountNumberList)) {
				for (int i = 0; i < totalBusinessDaysBackToProcess; i++) {
					for (int j = 0; j < skipIncrement; j++) {
						processDate = DateUtils.getPreviousWeekday(processDate);
					}
					//Run the preview
					InvestmentAccount clientAccount = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountUtils.getClientAccountByNumber(accountNumber), 2);
					List<ComplianceRuleRun> ruleRuns = this.complianceUtils.processRunPreview(clientAccount, processDate);
					//Delete unneeded entities before serializing to json string
					ruleRuns = sanitizeGeneratedRunResults(ruleRuns, processDate, accountNumber);
					//Copy these results to your test file
					String ruleRunString = gson.toJson(ruleRuns);
					if (!"[]".equals(ruleRunString)) {
//						System.out.println(ruleRunString);
						bw.write(ruleRunString + "\n");
					}
				}
			}
		}
	}


	private List<ComplianceRuleRun> sanitizeGeneratedRunResults(List<ComplianceRuleRun> ruleRunList, Date processDate, String accountNumber) {
		List<ComplianceRuleRun> sanitizedRuleRunList = new ArrayList<>();
		for (ComplianceRuleRun ruleRun : CollectionUtils.getIterable(ruleRunList)) {
			if (!ruleRun.getRuleType().isInvestmentSecuritySpecific()) {
				sanitizedRuleRunList.add(ruleRun);
			}
			if (ruleRun.getComplianceRule() != null) {
				ruleRun.getComplianceRule().setRuleEvaluatorBean(null);
				ruleRun.getComplianceRule().setRuleType(null);
				ruleRun.getComplianceRule().setInvestmentHierarchy(null);
				ruleRun.getComplianceRule().setInvestmentInstrument(null);
				ruleRun.getComplianceRule().setInvestmentType(null);
				ruleRun.getComplianceRule().setInvestmentTypeSubType(null);
				ruleRun.getComplianceRule().setDescription(null);
				ruleRun.getComplianceRule().setLabel("");
				ruleRun.getComplianceRule().setComplianceRuleLimitType(null);
				ruleRun.setChildren(null);
				sanitizeBaseEntityFields(ruleRun.getComplianceRule());
			}
			InvestmentAccount investmentAccount = new InvestmentAccount();
			investmentAccount.setNumber(accountNumber);
			ruleRun.setComplianceRun(new ComplianceRun());
			ruleRun.getComplianceRun().setClientInvestmentAccount(investmentAccount);
			ruleRun.getComplianceRun().setRunDate(processDate);
			ruleRun.getRuleType().setRuleEvaluatorType(null);
			ruleRun.getRuleType().setDescription(null);
			ruleRun.getRuleType().setLabel("");
			sanitizeBaseEntityFields(ruleRun.getRuleType());
			sanitizeBaseEntityFields(ruleRun.getComplianceRun());
		}
		return sanitizedRuleRunList;
	}


	private void sanitizeBaseEntityFields(BaseEntity<?> baseEntity) {
		baseEntity.setCreateDate(null);
		baseEntity.setUpdateDate(null);
		baseEntity.setRv(null);
	}


	@Test
	public void complianceRunPreviewBatchComparisonTest() throws IOException {
		Gson gson = ImsGsonFactory.newImsGson(null).getGson();

		List<String> errorMessageList = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(getClass().getResource(EXPECTED_RESULTS_FILE).getFile()))) {
			String line;
			//For each json string that represents historical values
			while ((line = br.readLine()) != null) {
				//Load up historical run list
				List<ComplianceRuleRun> historicalRuleRuns = Arrays.asList(gson.fromJson(line, ComplianceRuleRun[].class));

				//Grab out a run to see what account and process date this list is for
				ComplianceRuleRun firstRun = CollectionUtils.getFirstElement(historicalRuleRuns);

				if (firstRun != null) {
					try {
						//Process latest code with a preview for account on date
						InvestmentAccount clientAccount = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountUtils.getClientAccountByNumber(firstRun.getComplianceRun().getClientInvestmentAccount().getNumber()), 2);
						List<ComplianceRuleRun> ruleRuns = this.complianceUtils.processRunPreview(clientAccount, firstRun.getComplianceRun().getRunDate());

						//Make sure all historical values are accurate
						errorMessageList.addAll(validatePreviewWithHistoricalValues(historicalRuleRuns, ruleRuns));
					}
					catch (Exception e) {
						errorMessageList.add(String.format("An unexpected error occurred: %s\nCaused by: %s", e.getMessage(), ExceptionUtils.getFullStackTrace(e)));
					}
				}
			}
		}

		// Display errors if any occurred
		if (!errorMessageList.isEmpty()) {
			Assertions.fail(errorMessageList.stream().collect(Collectors.joining("\n\t", "Historical run validation failed:\n\t", StringUtils.EMPTY_STRING)));
		}
	}


	private List<String> validatePreviewWithHistoricalValues(List<ComplianceRuleRun> historicalRunList, List<ComplianceRuleRun> previewRunList) {
		List<String> errorMessageList = new ArrayList<>();
		for (ComplianceRuleRun historicalRun : CollectionUtils.getIterable(historicalRunList)) {

			//For current historical run find it's matching run in the preview results
			ComplianceRuleRun previewMatchForHistorical = findMatchingRunForHistorical(historicalRun, previewRunList);

			try {
				// result values for preview can vary, see if the difference between the historic run and the current are similar
				BigDecimal resultValueDifference = MathUtils.abs(MathUtils.subtract(historicalRun.getResultValue(), previewMatchForHistorical.getResultValue()));
				if (MathUtils.isGreaterThan(resultValueDifference, RESULT_VALUE_DIFFERENCE_THRESHOLD)) {
					Assertions.fail(String.format("Property [resultValue] difference exceeds threshold of [%s]. Expected: [%s]. Actual: [%s].", RESULT_VALUE_DIFFERENCE_THRESHOLD, historicalRun.getResultValue(), previewMatchForHistorical.getResultValue()));
				}
				scaleRunValuesForComparison(historicalRun);
				scaleRunValuesForComparison(previewMatchForHistorical);
				//Auto throw exception of not equal
				CoreCompareUtils.isNonNullPropertiesEqual(historicalRun, previewMatchForHistorical, true, new Class[]{InvestmentAccount.class, ComplianceRule.class, List.class, ComplianceRuleType.class, ComplianceRun.class, ComplianceRuleRunDetail.class}, "description", "rv", "createUserId", "updateUserId", "newBean", "label", "labelShort", "active", "leaf", "children");
			}
			catch (Exception e) {
				errorMessageList.add(String.format("Rule [%s] for account [%s] does not match historic run result for date [%s]: %s", historicalRun.getComplianceRule().getName(), historicalRun.getComplianceRun().getClientInvestmentAccount().getNumber(), historicalRun.getComplianceRun().getRunDate(), e.getMessage()));
			}
		}
		return errorMessageList;
	}


	private void scaleRunValuesForComparison(ComplianceRuleRun run) {
		if (run.getResultValue() != null) {
			// value is set to null as they are compared individually
			run.setResultValue(null);
		}
		if (run.getPercentOfAssets() != null) {
			run.setPercentOfAssets(MathUtils.round(run.getPercentOfAssets(), 0));
		}
	}


	private ComplianceRuleRun findMatchingRunForHistorical(ComplianceRuleRun historicalRun, List<ComplianceRuleRun> previewRunList) {
		ComplianceRuleRun matchedRun = null;
		for (ComplianceRuleRun previewRun : CollectionUtils.getIterable(previewRunList)) {
			//Test percentage rules, specificity rules have other test cases
			if (previewRun.getRuleType().isInvestmentSecuritySpecific()) {
				continue;
			}
			//Match if account number, run date, rule type name, and rule name are the same
			if (historicalRun.getComplianceRun().getClientInvestmentAccount().getNumber().equals(previewRun.getComplianceRun().getClientInvestmentAccount().getNumber())
					&& historicalRun.getComplianceRun().getRunDate().equals(previewRun.getComplianceRun().getRunDate())
					&& historicalRun.getRuleType().getName().equals(previewRun.getRuleType().getName())
					&& historicalRun.getComplianceRule().getName().equals(previewRun.getComplianceRule().getName())) {
				matchedRun = previewRun;
				break;
			}
		}
		ValidationUtils.assertNotNull(matchedRun, "Should not get here. Couldn't find a rule run for historical run [" + historicalRun + "]");
		return matchedRun;
	}
}
