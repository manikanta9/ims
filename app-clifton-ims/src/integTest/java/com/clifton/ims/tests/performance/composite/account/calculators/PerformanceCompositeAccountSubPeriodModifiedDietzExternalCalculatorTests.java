package com.clifton.ims.tests.performance.composite.account.calculators;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


/**
 * The <code>PerformanceCompositeAccountSubPeriodModifiedDietzExternalCalculatorTests</code> tests the PerformanceCompositeAccountSubPeriodModifiedDietzExternalCalculator
 * <p>
 * NOTE THIS CALCULATOR IS NOT TESTING CURRENTLY - THERE IS EXTERNAL DATA THAT IS NEEDED AND SINCE IT APPEARS IT'S NOT BEING USED YET IT'S NOT POSSIBLE TO COMPARE TO ACTUAL
 *
 * @author manderson
 */
public class PerformanceCompositeAccountSubPeriodModifiedDietzExternalCalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {

	@Disabled
	@Test
	public void test_account_604600() {
		String accountNumber = "604600";
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "12/31/2015");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "01/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "02/29/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "03/31/2016");
	}
}
