package com.clifton.ims.tests.investment.account.relationship;


import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountRelationshipPurposes;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;


/**
 * The <code>InvestmentAccountRelationshipConfigCacheTests</code>
 * tests Adding, Updating, Removing Relationships and ensure cache is properly cleared
 *
 * @author manderson
 */

public class InvestmentAccountRelationshipConfigCacheTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;


	// Test Adding, Updating, Removing Relationships and ensure cache is properly cleared
	@Test
	public void testAccountRelationships() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		int clientAccountId = accountInfo.getClientAccount().getId();
		int holdingAccountId = accountInfo.getHoldingAccount().getId();
		Date today = new Date();

		InvestmentAccount account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_FUTURES.getName(),
				today);
		Assertions.assertTrue((account != null && account.getId().equals(holdingAccountId)), "Client/Holding Account Relationship should be valid today for Trading: Futures");

		account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_OPTIONS.getName(), today);
		Assertions.assertNull(account, "Client/Holding Account Relationship should NOT be valid today for Trading: Options");

		// Add relationship for Trading: Options
		InvestmentAccountRelationship relationship = this.investmentAccountUtils.createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(),
				InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);

		// Now should be active
		account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_OPTIONS.getName(), today);
		Assertions.assertTrue((account != null && account.getId().equals(holdingAccountId)), "Client/Holding Account Relationship should be valid today for Trading: Options");

		// Change relationship start date to tomorrow
		relationship.setStartDate(DateUtils.addDays(today, 1));
		this.investmentAccountRelationshipService.saveInvestmentAccountRelationship(relationship);

		// Now should not be active today
		account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_OPTIONS.getName(), today);
		Assertions.assertNull(account, "Client/Holding Account Relationship should NOT be valid today for Trading: Options");

		// But should be active for tomorrow
		account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_OPTIONS.getName(),
				DateUtils.addDays(today, 1));
		Assertions.assertTrue((account != null && account.getId().equals(holdingAccountId)), "Client/Holding Account Relationship should be valid TOMORROW for Trading: Options");

		// Delete the relationship
		this.investmentAccountRelationshipService.deleteInvestmentAccountRelationship(relationship.getId());

		// Should NOT be active ever = but check tomorrow since that was last active
		account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_OPTIONS.getName(),
				DateUtils.addDays(today, 1));
		Assertions.assertNull(account, "Client/Holding Account Relationship should NOT be valid today for Trading: Options");

		// Relationship for Futures should still be active
		account = this.investmentAccountRelationshipService.getInvestmentAccountRelatedForPurpose(clientAccountId, InvestmentAccountRelationshipPurposes.TRADING_FUTURES.getName(), today);
		Assertions.assertTrue((account != null && account.getId().equals(holdingAccountId)), "Client/Holding Account Relationship should be valid today for Trading: Futures");
	}
}
