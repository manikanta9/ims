package com.clifton.ims.tests.builders.investment.business.contract;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.business.contract.BusinessContractPartyRole;
import com.clifton.business.contract.BusinessContractService;


/**
 * @author stevenf
 */
public class BusinessContractPartyBuilder {

	private final BusinessContractService businessContractService;

	private String comments;
	private BusinessContractPartyRole role;
	private BusinessContact contact;
	private BusinessCompany company;
	private BusinessContract contract;


	private BusinessContractPartyBuilder(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public static BusinessContractPartyBuilder businessContractParty(BusinessContractService businessContractService) {
		return new BusinessContractPartyBuilder(businessContractService);
	}


	public BusinessContractParty build() {
		BusinessContractParty businessContractParty = new BusinessContractParty();
		businessContractParty.setComments(this.comments);
		businessContractParty.setRole(this.role);
		businessContractParty.setContact(this.contact);
		businessContractParty.setCompany(this.company);
		businessContractParty.setContract(this.contract);
		return businessContractParty;
	}


	public BusinessContractParty buildAndSave() {
		BusinessContractParty businessContractParty = build();
		return this.businessContractService.saveBusinessContractParty(businessContractParty);
	}


	public BusinessContractPartyBuilder but() {
		return businessContractParty(this.businessContractService)
				.withComments(this.comments)
				.withRole(this.role)
				.withContact(this.contact)
				.withCompany(this.company)
				.withContract(this.contract);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public BusinessContractPartyBuilder withComments(String comments) {
		this.comments = comments;
		return this;
	}


	public BusinessContractPartyBuilder withRole(BusinessContractPartyRole role) {
		this.role = role;
		return this;
	}


	public BusinessContractPartyBuilder withContact(BusinessContact contact) {
		this.contact = contact;
		return this;
	}


	public BusinessContractPartyBuilder withCompany(BusinessCompany company) {
		this.company = company;
		return this;
	}


	public BusinessContractPartyBuilder withContract(BusinessContract contract) {
		this.contract = contract;
		return this;
	}
}
