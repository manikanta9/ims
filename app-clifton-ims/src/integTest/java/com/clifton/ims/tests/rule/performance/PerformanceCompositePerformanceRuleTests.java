package com.clifton.ims.tests.rule.performance;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.ims.tests.system.query.JdbcUtils;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.violation.RuleViolation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * PerformanceCompositePerformanceRuleTests uses preview feature for rule violations on existing composite performance
 * to validate expected violations and also passed violations across the rule definitions that we have set up.
 */
public class PerformanceCompositePerformanceRuleTests extends RuleTests {

	private static final String RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS = "Composite Performance Missing Required Base Fields";
	private static final String RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE = "Composite Performance Valid Account Performance";
	private static final String RULE_DEFINITION_VALID_ACCOUNT_PERIOD = "Composite Performance Valid Account Period";
	private static final String RULE_DEFINITION_MISSING_BENCHMARK_RETURNS = "Composite Performance: Missing Benchmark Period Values";
	private static final String RULE_DEFINITION_MISSING_PREVIOUS_MONTH = "Composite Performance: Missing Previous Month Performance Period";
	private static final String RULE_DEFINITION_PREVIOUS_INVALID_STATE = "Composite Performance: Previous Performance Invalid State";


	@Resource
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Performance Composite Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return new HashSet<>();
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCompositePerformanceMissingRequiredBaseFieldsRule() {
		validateViolationExists("Commodity Composite XXI", "11/01/2012", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "12/01/2012", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "01/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "02/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "03/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "04/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "05/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "06/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "07/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "08/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "09/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "10/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "11/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "12/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "01/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "02/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "03/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "04/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "05/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "06/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "07/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "08/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "09/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "10/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "11/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "12/01/2014", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "01/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "02/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXI", "03/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "04/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "05/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "06/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "07/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "08/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "09/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");
		validateViolationExists("Commodity Composite XXIII", "10/01/2013", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS, "The Following Required Fields Are Invalid:<br/><ul><li>Period End Market Value cannot be null<li>Period Gross Return cannot be null</ul>");


		validateViolationPassed("Defensive Equity Options Funded Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("US Low Volatility Equity Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Defensive Equity Composite III", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Defensive Equity Strategy Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Elevated Beta VRP Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Commodity Composite III", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Equal Sector Commodity Composite I", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Equal Sector Commodity Composite II", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Dynamic Put Selling Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Absolute Return VRP Overlay 0.5 Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("TIPS Portfolio Management Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Low Beta VRP Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Defensive Equity Composite VI", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Global Defensive Equity Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Equal Sector Commodity Composite III", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Defensive Equity Enhanced Large Cap Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Commodity Composite I", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Commodity Composite II", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Commodity Strategy Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Global Balanced Risk Contraction Portfolio Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Enhanced Intermediate Government Credit Fixed Income Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Enhanced U.S. Large Cap Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Enhanced U.S. Small Cap Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Legacy Composite", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Defensive Equity Composite II", "12/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Defensive Equity Composite II", "11/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Enhanced U.S. Small Cap Composite", "11/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
		validateViolationPassed("Legacy Composite", "11/01/2015", RULE_DEFINITION_MISSING_REQUIRED_BASE_FIELDS);
	}


	@Test
	public void testCompositePerformanceValidAccountPerformanceRule() {
		// This account was moved to a client group and the violation no longer applies to it.
		// validateViolationExists("Defensive Equity Emerging Markets Composite (unfunded)", "08/01/2016", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE, "[663156: U of MN Foundation-Emerging DE-Performance Only: 08/2016] - Partial Period Positions Off All Period.Account was excluded from composite performance");

		validateViolationPassed("Commodity Composite II", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Defensive Equity Composite II", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Defensive Equity Composite III", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Defensive Equity Strategy Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Commodity Composite III", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("US Low Volatility Equity Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Defensive Equity Composite VI", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Defensive Equity Enhanced Large Cap Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Absolute Return VRP Overlay 0.5 Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Dynamic Put Selling Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Equal Sector Commodity Composite II", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Equal Sector Commodity Composite I", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Equal Sector Commodity Composite III", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Enhanced U.S. Small Cap Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Enhanced Intermediate Government Credit Fixed Income Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Global Balanced Risk Contraction Portfolio Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Commodity Composite I", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("TIPS Portfolio Management Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Defensive Equity Options Funded Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Global Defensive Equity Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Elevated Beta VRP Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
		validateViolationPassed("Low Beta VRP Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE);
	}


	@Test
	public void testCompositePerformanceValidAccountPeriodRule() {
		// This account was moved to a client group and the violation no longer applies to it.
		// validateViolationExists("Defensive Equity Emerging Markets Composite (unfunded)", "08/01/2016", RULE_DEFINITION_VALID_ACCOUNT_PERFORMANCE, "[663156: U of MN Foundation-Emerging DE-Performance Only: 08/2016] - Partial Period Positions Off All Period.Account was excluded from composite performance");

		validateViolationPassed("Commodity Composite II", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Defensive Equity Composite II", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Defensive Equity Composite III", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Defensive Equity Strategy Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Commodity Composite III", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("US Low Volatility Equity Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Defensive Equity Composite VI", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Defensive Equity Enhanced Large Cap Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Absolute Return VRP Overlay 0.5 Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Dynamic Put Selling Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Equal Sector Commodity Composite II", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Equal Sector Commodity Composite I", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Equal Sector Commodity Composite III", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Enhanced U.S. Small Cap Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Enhanced Intermediate Government Credit Fixed Income Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Global Balanced Risk Contraction Portfolio Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Commodity Composite I", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("TIPS Portfolio Management Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Defensive Equity Options Funded Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Global Defensive Equity Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Elevated Beta VRP Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
		validateViolationPassed("Low Beta VRP Composite", "12/01/2015", RULE_DEFINITION_VALID_ACCOUNT_PERIOD);
	}


	@Test
	public void testCompositePerformanceMissingBenchmarkPeriodValuesRule() {
		// Composite Performance with no benchmarks
		validateViolationPassed("Absolute Return VRP Overlay 0.5 Composite", "06/30/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS);

		// Composite Performance with only a risk free return
		validateViolationPassed("Dynamic Put Selling Composite", "06/30/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS);

		// Composite Performance with all 3 populated
		validateViolationPassed("Defensive Equity Composite VI", "06/30/2016", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS);

		// This should never actually happen - so take a terminated Composite's performance, manually clear the value and then preview rules
		// Running this through DB directly
		PerformanceCompositePerformance compositePerformance = getCompositePerformanceForCompositeAndDate("Balanced Risk Composite I", "07/31/2015");
		if (compositePerformance.getPeriodBenchmarkReturn() != null) {
			JdbcUtils.executeQuery("UPDATE PerformanceCompositePerformance SET PeriodBenchmarkReturn = NULL, PeriodRiskFreeSecurityReturn = NULL WHERE PerformanceCompositePerformanceID = " + compositePerformance.getId());
			this.systemRequestStatsService.deleteSystemCache("com.clifton.performance.composite.performance.PerformanceCompositePerformance");
		}
		validateViolationExists("Balanced Risk Composite I", "07/31/2015", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS, "Missing Period Return for Investment Security GIPS 90-Day T-Bill");
		validateViolationExists("Balanced Risk Composite I", "07/31/2015", RULE_DEFINITION_MISSING_BENCHMARK_RETURNS, "Missing Period Return for Investment Security G0O1 (BofA Merrill Lynch US 3-Month Treasury Bill Index)");
	}


	@Test
	public void testCompositePerformanceMissingPreviousMonthPerformancePeriodRule() {
		validateViolationExists("Defensive Equity Composite II", "01/31/2013", RULE_DEFINITION_MISSING_PREVIOUS_MONTH, "Missing Previous Months Performance for this Composite.  Performance Inception Date will be reset.");
		validateViolationExists("Enhanced Core Fixed Income (ECFI) Composite", "09/30/2015", RULE_DEFINITION_MISSING_PREVIOUS_MONTH, "Missing Previous Months Performance for this Composite.  Performance Inception Date will be reset.");

		validateViolationPassed("Defensive Equity Composite II", "01/31/2016", RULE_DEFINITION_MISSING_PREVIOUS_MONTH);

		// First Month for the Composite
		validateViolationPassed("Defensive Equity Composite II", "09/30/2011", RULE_DEFINITION_MISSING_PREVIOUS_MONTH);
	}


	@Test
	public void testCompositePerformancePreviousPerformanceInvalidStateRule() {
		validateViolationPassed("Defensive Equity Composite II", "01/31/2012", RULE_DEFINITION_PREVIOUS_INVALID_STATE);
		validateViolationPassed("Defensive Equity Composite II", "10/31/2015", RULE_DEFINITION_PREVIOUS_INVALID_STATE);

		// Move One Back to Draft, and then Preview Rules on a Later One
		PerformanceCompositePerformance compositePerformance = getCompositePerformanceForCompositeAndDate("Defensive Equity Composite II", "10/31/2015");
		this.workflowTransitioner.transitionForTests("Return for Edits", "Draft", "PerformanceCompositePerformance", BeanUtils.getIdentityAsLong(compositePerformance));

		validateViolationPassed("Defensive Equity Composite II", "10/31/2015", RULE_DEFINITION_PREVIOUS_INVALID_STATE);
		validateViolationExists("Defensive Equity Composite II", "11/30/2015", RULE_DEFINITION_PREVIOUS_INVALID_STATE, "Defensive Equity Composite II: 10/2015 is in Draft state.");
		validateViolationExists("Defensive Equity Composite II", "06/30/2016", RULE_DEFINITION_PREVIOUS_INVALID_STATE, "Defensive Equity Composite II: 10/2015 is in Draft state.");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Helper Methods                 ////////////////
	/////////////////////////////////////////////////////////////////////////////


	private PerformanceCompositePerformance getCompositePerformanceForCompositeAndDate(String compositeName, String measureDate) {
		AccountingPeriod accountingPeriod = this.accountingUtils.getAccountingPeriodForDate(DateUtils.toDate(measureDate));
		PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
		searchForm.setPerformanceCompositeNameEquals(compositeName);
		searchForm.setAccountingPeriodId(accountingPeriod.getId());
		PerformanceCompositePerformance compositePerformance = CollectionUtils.getOnlyElement(this.performanceCompositePerformanceService.getPerformanceCompositePerformanceList(searchForm));
		if (compositePerformance == null) {
			throw new ValidationException("Missing composite performance for " + compositeName + " and period " + accountingPeriod.getLabel());
		}
		return compositePerformance;
	}


	/////////////////////////////////////////////////////////////////////////////
	///////////                Validation Methods                 ///////////////
	/////////////////////////////////////////////////////////////////////////////
	private void validateViolationExists(String compositeName, String measureDate, String ruleDefinitionName, String violationNoteExpected) {
		validateViolation(getCompositePerformanceForCompositeAndDate(compositeName, measureDate), this.ruleDefinitionService.getRuleDefinitionByName(ruleDefinitionName), true, violationNoteExpected);
	}


	private void validateViolationPassed(String compositeName, String measureDate, String ruleDefinitionName) {
		validateViolation(getCompositePerformanceForCompositeAndDate(compositeName, measureDate), this.ruleDefinitionService.getRuleDefinitionByName(ruleDefinitionName), false);
	}


	/**
	 * Validates Violation with Violation Note exists or not in the Preview list
	 */
	private void validateViolation(PerformanceCompositePerformance performanceCompositePerformance, RuleDefinition ruleDefinition, boolean exists, String... violationNoteExpected) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(ruleDefinition.getRuleCategory().getCategoryTable().getName(), performanceCompositePerformance.getId());
		String definitionName = ruleDefinition.getName();
		if (exists) {
			for (String expectedViolation : violationNoteExpected) {
				Assertions.assertFalse(CollectionUtils.isEmpty(BeanUtils.filter(violationList, RuleViolation::getViolationNote, expectedViolation)), "Did not find Expected Violation with Message: " + expectedViolation + " for Performance " + performanceCompositePerformance.getLabel() + " for Rule Definition [" + ruleDefinition.getName() + "] with Category Table Name [" + ruleDefinition.getRuleCategory().getCategoryTable() + "]");
			}
		}
		else {
			RuleViolation firstRuleViolation = null;
			for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
				if (definitionName.equals(violation.getRuleAssignment().getRuleDefinition().getName()) && !"Passed".equals(violation.getIgnoreNote())) {
					firstRuleViolation = violation;
					break;
				}
			}
			Assertions.assertNull(firstRuleViolation, "Rule Definition: " + definitionName + " for Performance " + performanceCompositePerformance.getLabel() + " should not have been executed, but it did");
		}
	}
}
