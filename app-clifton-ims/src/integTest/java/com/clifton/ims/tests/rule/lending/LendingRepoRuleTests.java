package com.clifton.ims.tests.rule.lending;


import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.search.ComplianceRuleOldSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class LendingRepoRuleTests extends RuleTests {

	@Resource
	ComplianceOldService complianceOldService;
	@Resource
	LendingRepoService lendingRepoService;


	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "REPO Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return new HashSet<>();
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testTradingRulesREPONotionalLimitRule() {
		AccountInfo repoAccountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.DEUTSCHE_BANK_SECURITIES);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828HN3", false);//CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));

		ComplianceRuleOld complianceRuleOld = new ComplianceRuleOld();
		complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Notional Limit"));
		complianceRuleOld.setClientInvestmentAccount(repoAccountInfo.getClientAccount());
		complianceRuleOld.setInvestmentInstrument(security.getInstrument());
		complianceRuleOld.setMaxBuyQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxBuyAccountingNotional(BigDecimal.valueOf(9900));
		this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

		this.complianceUtils.createApprovedContractsOldComplianceRule(repoAccountInfo, security.getInstrument());
		this.complianceUtils.createTradeAllComplianceRule(repoAccountInfo);

		LendingRepo lendingRepo = this.lendingRepoUtils.newOpenLendingRepoBuilder(security, new BigDecimal(10000), new Date(), repoAccountInfo)
				.price(new BigDecimal("100"))
				.haircutPercent(new BigDecimal("99"))
				.indexRatio(new BigDecimal("1"))
				.interestAmount(new BigDecimal("10"))
				.interestRate(new BigDecimal("1"))
				//make it a buy
				.reverse(true)
				.dayCountConvention("Actual/360")
				.active(true)
				.instrumentHierarchy(security.getInstrument().getHierarchy())
				.buildAndSave();

		boolean foundViolation = false;
		RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
		ruleViolationSearchForm.setRuleDefinitionName("Trading Rules (REPO): Notional Limit");
		ruleViolationSearchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(lendingRepo));
		// The string below should also be tested for if the RuleViolation#equals method is refactored so that the expected violations aren't considered equal.
		// see https://stash.paraport.com/projects/IMS/repos/ims/pull-requests/6914/diff#app-clifton-ims-tests/test/java/com/clifton/ims/tests/rule/lending/LendingRepoRuleTests.java
		// String expectedViolation = "The notional value of the trade <span class='amountPositive'>10,000.00</span> exceeds the buy limit of <span class='amountPositive'>9,900.00</span>";
		String expectedViolation = "The order to buy <span class='amountPositive'>10,000.00</span> 912828HN3 Face exceeds the DMA order limit of <span class='amountPositive'>100.00</span>.  This order must be executed in multiple tranches and/or via an algo";
		List<RuleViolation> violationList = this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm);
		Assertions.assertEquals(1, violationList.size(), "Only one violation should exist, see comment above to determine if test should be updated.");
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(violationList)) {
			if (ruleViolation.getViolationNote().equals(expectedViolation)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	@Test
	public void testTradingRulesREPOFatFingerLimitRule() {
		AccountInfo repoAccountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.DEUTSCHE_BANK_SECURITIES);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828LA6", false);//CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));

		ComplianceRuleOldSearchForm complianceRuleOldSearchForm = new ComplianceRuleOldSearchForm();
		complianceRuleOldSearchForm.setTypeId(this.complianceOldService.getComplianceRuleTypeOldByName("Fat Finger Trader Limit").getId());
		complianceRuleOldSearchForm.setInvestmentInstrumentId(security.getInstrument().getId());
		ComplianceRuleOld complianceRuleOld = CollectionUtils.getFirstElement(this.complianceOldService.getComplianceRuleOldList(complianceRuleOldSearchForm));
		if (complianceRuleOld == null) {
			complianceRuleOld = new ComplianceRuleOld();
		}
		complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Fat Finger Trader Limit"));
		complianceRuleOld.setInvestmentInstrument(security.getInstrument());
		complianceRuleOld.setMaxBuyQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxSellQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxBuyAccountingNotional(BigDecimal.valueOf(10000));
		complianceRuleOld.setMaxSellAccountingNotional(BigDecimal.valueOf(10000));
		this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

		this.complianceUtils.createApprovedContractsOldComplianceRule(repoAccountInfo, security.getInstrument());
		this.complianceUtils.createTradeAllComplianceRule(repoAccountInfo);

		LendingRepo lendingRepo = this.lendingRepoUtils.newOpenLendingRepoBuilder(security, new BigDecimal(10), new Date(), repoAccountInfo)
				.price(new BigDecimal("10125467"))
				.haircutPercent(new BigDecimal("98"))
				.indexRatio(new BigDecimal("1.3"))
				.quantityIntended(new BigDecimal("1000"))
				.interestRate(new BigDecimal("1"))
				//make it a buy
				.reverse(true)
				.dayCountConvention("Actual/360")
				.active(true)
				.instrumentHierarchy(security.getInstrument().getHierarchy())
				.buildAndSave();

		boolean foundViolation = false;

		String expectedViolation = "The order to buy <span class='amountPositive'>1,000.00</span> 912828LA6 Face exceeds the DMA order limit of <span class='amountPositive'>100.00</span>.  This order must be executed in multiple tranches and/or via an algo";
		RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
		ruleViolationSearchForm.setRuleDefinitionName("Trading Rules (REPO): Fat Finger Limit");
		ruleViolationSearchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(lendingRepo));
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm))) {
			if (ruleViolation.getViolationNote().equals(expectedViolation)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	@Test
	public void testTradingRulesREPOShortLongAllowedRule() {
		AccountInfo repoAccountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.DEUTSCHE_BANK_SECURITIES);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828QV5", false);//CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(searchForm));

		ComplianceRuleOld complianceRuleOld = new ComplianceRuleOld();
		complianceRuleOld.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Short/Long"));
		complianceRuleOld.setClientInvestmentAccount(repoAccountInfo.getClientAccount());
		complianceRuleOld.setInvestmentInstrument(security.getInstrument());
		complianceRuleOld.setMaxBuyQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxSellQuantity(BigDecimal.valueOf(100));
		complianceRuleOld.setMaxBuyAccountingNotional(BigDecimal.valueOf(10000));
		complianceRuleOld.setMaxSellAccountingNotional(BigDecimal.valueOf(10000));
		this.complianceOldService.saveComplianceRuleOld(complianceRuleOld);

		this.complianceUtils.createApprovedContractsOldComplianceRule(repoAccountInfo, security.getInstrument());
		this.complianceUtils.createTradeAllComplianceRule(repoAccountInfo);

		LendingRepo lendingRepo = this.lendingRepoUtils.newOpenLendingRepoBuilder(security, new BigDecimal(10), new Date(), repoAccountInfo)
				.price(new BigDecimal("100"))
				.haircutPercent(new BigDecimal("99"))
				.indexRatio(new BigDecimal("1"))
				.interestAmount(new BigDecimal("10"))
				.netCash(new BigDecimal("10000"))
				.marketValue(new BigDecimal("10000"))
				.interestRate(new BigDecimal("1"))
				//make it a sell
				.reverse(false)
				.dayCountConvention("Actual/360")
				.active(true)
				.instrumentHierarchy(security.getInstrument().getHierarchy())
				.buildAndSave();

		boolean foundViolation = false;
		String expectedViolation = "This trade will generate a short position of <span class='amountNegative'>-10</span> Face which is not allowed.";
		RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
		ruleViolationSearchForm.setRuleDefinitionName("Trading Rules (REPO): Short/Long Allowed");
		ruleViolationSearchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(lendingRepo));
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm))) {
			if (ruleViolation.getViolationNote().equals(expectedViolation)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}


	@Test
	public void testProhibitREPOsRule() {
		AccountInfo repoAccountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.DEUTSCHE_BANK_SECURITIES);
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912828QV5", false);

		LendingRepo lendingRepo = this.lendingRepoUtils.newOpenLendingRepoBuilder(security, new BigDecimal(10), new Date(), repoAccountInfo)
				.price(new BigDecimal("10125467"))
				.haircutPercent(new BigDecimal("98"))
				.indexRatio(new BigDecimal("1.3"))
				.quantityIntended(new BigDecimal("1000"))
				.interestRate(new BigDecimal("1"))
				//make it a buy
				.reverse(true)
				.dayCountConvention("Actual/360")
				.active(true)
				.instrumentHierarchy(security.getInstrument().getHierarchy())
				.buildAndSave();

		boolean foundViolation = false;

		String expectedViolation = "REPO Trading is prohibited. Please contact Compliance to enable REPO trading on this account.";
		RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
		ruleViolationSearchForm.setRuleDefinitionName("Prohibit REPOs");
		ruleViolationSearchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(lendingRepo));
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm))) {
			if (ruleViolation.getViolationNote().equals(expectedViolation)) {
				foundViolation = true;
			}
		}
		Assertions.assertTrue(foundViolation);
	}
}
