package com.clifton.ims.tests.accounting.position;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyRebuildCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;


public class AccountingPositionDailyServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingPositionDailyService accountingPositionDailyService;


	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetAccountingPositionDailyLiveList_TRS() {
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		InvestmentSecurity trs = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("Q6E8U");
		searchForm.setInvestmentSecurityId(trs.getId());
		searchForm.setSnapshotDate(DateUtils.toDate("04/27/2015"));
		List<AccountingPositionDaily> resultList = this.accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);

		Assertions.assertEquals(1, resultList.size());
		AccountingPositionDaily result = resultList.get(0);
		Assertions.assertEquals(new BigDecimal("26334032.30"), result.getNotionalValueLocal());
		// Accrual is calculated from settlement of opening transaction and not ex date difference of 1 day ($210.85)
		Assertions.assertEquals(new BigDecimal("1321000.01"), result.getOpenTradeEquityLocal());
		Assertions.assertEquals(new BigDecimal("-10964.24"), result.getAccrualLocal());

		compareAccountingPositionDailyLiveWithSnapshot(resultList, searchForm);
	}


	@Test
	public void testGetAccountingPositionDailyLiveList_BoundCouponOnWeekendShouldBeIncludedOnMonday() {
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		searchForm.setInvestmentSecurityId(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("912810QF8").getId());
		searchForm.setClientAccountId(this.investmentAccountUtils.getClientAccountByNumber("578509").getId());
		searchForm.setSnapshotDate(DateUtils.toDate("08/17/2015"));
		List<AccountingPositionDaily> resultList = this.accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);

		Assertions.assertEquals(5, resultList.size());
		BigDecimal result = BigDecimal.ZERO;
		for (AccountingPositionDaily position : resultList) {
			result = MathUtils.add(result, position.getDailyGainLossBase());
		}
		Assertions.assertEquals(new BigDecimal("78684.77"), result);

		compareAccountingPositionDailyLiveWithSnapshot(resultList, searchForm);
	}


	@Test
	public void testGetAccountingPositionDailyLiveList_BoundCouponOnSaturdayShouldBeIncludedOnSunday() {
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		searchForm.setInvestmentSecurityId(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("89233P6T8").getId());
		searchForm.setClientAccountId(this.investmentAccountUtils.getClientAccountByNumber("604600").getId());
		searchForm.setSnapshotDate(DateUtils.toDate("7/31/2016"));
		List<AccountingPositionDaily> resultList = this.accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);

		Assertions.assertEquals(1, resultList.size());
		BigDecimal result = resultList.get(0).getDailyGainLossBase();
		Assertions.assertEquals(new BigDecimal("17.24"), result);

		compareAccountingPositionDailyLiveWithSnapshot(resultList, searchForm);
	}


	@Test
	public void testGetAccountingPositionDailyLiveList_ForeignFund_NoAccruals() {
		// Jira: ACCOUNTING-533  Issue was with Base OTE and Prior Base OTE
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("XBAU");
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setSnapshotDate(DateUtils.toDate("02/01/2017"));
		List<AccountingPositionDaily> resultList = this.accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);

		Assertions.assertEquals(1, resultList.size());
		AccountingPositionDaily result = resultList.get(0);
		Assertions.assertEquals(new BigDecimal("69347719.36"), result.getNotionalValueLocal());

		Assertions.assertEquals(new BigDecimal("0.00"), result.getOpenTradeEquityLocal());

		Assertions.assertEquals(new BigDecimal("-141281.15"), result.getOpenTradeEquityBase());
		Assertions.assertEquals(new BigDecimal("166554.78"), result.getPriorOpenTradeEquityBase());

		Assertions.assertEquals(new BigDecimal("0.00"), result.getAccrualLocal());
		Assertions.assertEquals(new BigDecimal("0.00"), result.getAccrualBase());

		Assertions.assertEquals(new BigDecimal("-28115.84"), result.getDailyGainLossLocal());
		Assertions.assertEquals(new BigDecimal("-307835.93"), result.getDailyGainLossBase());

		compareAccountingPositionDailyLiveWithSnapshot(resultList, searchForm);
	}


	@Test
	public void testGetAccountingPositionDailyLiveList_InternationalLinkers_WithAccruals() {
		// Jira: ACCOUNTING-533  Issue was with Base OTE and Prior Base OTE
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("FR0012558310");
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setSnapshotDate(DateUtils.toDate("02/29/2016"));
		List<AccountingPositionDaily> resultList = this.accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);

		Assertions.assertEquals(2, resultList.size());
		for (AccountingPositionDaily result : resultList) {
			if (MathUtils.isEqual(new BigDecimal("350000"), result.getRemainingQuantity())) {
				Assertions.assertEquals(new BigDecimal("364123.04"), result.getNotionalValueLocal());

				Assertions.assertEquals(new BigDecimal("-17279.10"), result.getOpenTradeEquityLocal());

				Assertions.assertEquals(new BigDecimal("-17419.38"), result.getOpenTradeEquityBase());
				Assertions.assertEquals(new BigDecimal("-14586.22"), result.getPriorOpenTradeEquityBase());

				Assertions.assertEquals(new BigDecimal("349.60"), result.getAccrualLocal());
				Assertions.assertEquals(new BigDecimal("379.86"), result.getAccrualBase());

				Assertions.assertEquals(new BigDecimal("-558.16"), result.getDailyGainLossLocal());
				Assertions.assertEquals(new BigDecimal("-2833.16"), result.getDailyGainLossBase());
			}
			else if (MathUtils.isEqual(new BigDecimal("249000"), result.getRemainingQuantity())) {
				Assertions.assertEquals(new BigDecimal("259047.53"), result.getNotionalValueLocal());

				Assertions.assertEquals(new BigDecimal("4300.30"), result.getOpenTradeEquityLocal());

				Assertions.assertEquals(new BigDecimal("-3984.62"), result.getOpenTradeEquityBase());
				Assertions.assertEquals(new BigDecimal("-1969.02"), result.getPriorOpenTradeEquityBase());

				Assertions.assertEquals(new BigDecimal("248.71"), result.getAccrualLocal());
				Assertions.assertEquals(new BigDecimal("270.24"), result.getAccrualBase());

				Assertions.assertEquals(new BigDecimal("-397.10"), result.getDailyGainLossLocal());
				Assertions.assertEquals(new BigDecimal("-2015.60"), result.getDailyGainLossBase());
			}
			else {
				throw new ValidationException("Expected 2 lots, one with quantity of 350,000 and the other with 249,000, but found one with: " + CoreMathUtils.formatNumberMoney(result.getRemainingQuantity()));
			}
		}

		compareAccountingPositionDailyLiveWithSnapshot(resultList, searchForm);
	}


	@Test
	public void testGetAccountingPositionDailyLiveList_ForeignCreditDefaultSwaps() {
		// Jira: ACCOUNTING-533  Issue was with Base OTE and Prior Base OTE
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("7M8LJ");
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setSnapshotDate(DateUtils.toDate("02/28/2017"));
		List<AccountingPositionDaily> resultList = this.accountingPositionDailyService.getAccountingPositionDailyLiveList(searchForm);

		Assertions.assertEquals(1, resultList.size());
		AccountingPositionDaily result = resultList.get(0);
		Assertions.assertEquals(new BigDecimal("1193409.27"), result.getNotionalValueLocal());

		Assertions.assertEquals(new BigDecimal("4506.97"), result.getOpenTradeEquityLocal());

		Assertions.assertEquals(new BigDecimal("-77380.18"), result.getOpenTradeEquityBase());
		Assertions.assertEquals(new BigDecimal("-93919.94"), result.getPriorOpenTradeEquityBase());

		Assertions.assertEquals(new BigDecimal("83819.44"), result.getAccrualLocal());
		Assertions.assertEquals(new BigDecimal("88785.74"), result.getAccrualBase());

		Assertions.assertEquals(new BigDecimal("15108.20"), result.getDailyGainLossLocal());
		Assertions.assertEquals(new BigDecimal("16539.76"), result.getDailyGainLossBase());

		compareAccountingPositionDailyLiveWithSnapshot(resultList, searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void compareAccountingPositionDailyLiveWithSnapshot(List<AccountingPositionDaily> accountingPositionDailyLiveList, AccountingPositionDailyLiveSearchForm liveSearchForm) {
		AccountingPositionDailyRebuildCommand rebuildCommand = new AccountingPositionDailyRebuildCommand();
		rebuildCommand.setSnapshotDate(liveSearchForm.getSnapshotDate());
		rebuildCommand.setInvestmentSecurityId(liveSearchForm.getInvestmentSecurityId());
		rebuildCommand.setClientAccountId(liveSearchForm.getClientAccountId());
		rebuildCommand.setSynchronous(true);
		this.accountingPositionDailyService.rebuildAccountingPositionDaily(rebuildCommand);

		AccountingPositionDailySearchForm searchForm = new AccountingPositionDailySearchForm();
		searchForm.setInvestmentSecurityId(rebuildCommand.getInvestmentSecurityId());
		searchForm.setPositionDate(rebuildCommand.getStartSnapshotDate());
		searchForm.setClientInvestmentAccountId(rebuildCommand.getClientAccountId());
		List<AccountingPositionDaily> accountingPositionDailyList = this.accountingPositionDailyService.getAccountingPositionDailyList(searchForm);

		Assertions.assertEquals(accountingPositionDailyLiveList.size(), accountingPositionDailyList.size());

		for (AccountingPositionDaily accountingPositionDailyLive : accountingPositionDailyLiveList) {
			boolean found = false;
			for (AccountingPositionDaily accountingPositionDaily : accountingPositionDailyList) {
				if (MathUtils.isEqual(accountingPositionDaily.getAccountingTransaction().getId(), accountingPositionDailyLive.getAccountingTransaction().getId())) {
					found = true;
					validatePositionDailyEqualsPositionDailyLive(accountingPositionDailyLive, accountingPositionDaily);
					break;
				}
			}
			if (!found) {
				throw new ValidationException("Did not find snapshot record that matches live record for accounting transaction id " + accountingPositionDailyLive.getAccountingTransaction().getId());
			}
		}
	}


	private void validatePositionDailyEqualsPositionDailyLive(AccountingPositionDaily accountingPositionDailyLive, AccountingPositionDaily accountingPositionDaily) {
		// Note: Include Calculated Getters, but Exclude Accrual 1 and 2 - they don't match, but total accruals do match
		List<String> differences = CoreCompareUtils.getNoEqualProperties(accountingPositionDailyLive, accountingPositionDaily, false, "id", "identity", "newBean", "accrual1Local", "accrual2Local", "equityLegPaymentLocal", "accrual1PaymentLocal", "accrual1Base", "accrual2Base", "equityLegPaymentBase", "accrualPayment1Base");
		AssertUtils.assertEmpty(differences, "Found differences between Live and Snapshot Position " + getAccountingPositionDailyFriendlyLabel(accountingPositionDailyLive) + ": " + StringUtils.collectionToCommaDelimitedString(differences));
	}


	private String getAccountingPositionDailyFriendlyLabel(AccountingPositionDaily accountingPositionDaily) {
		return "TransactionID: " + accountingPositionDaily.getAccountingTransaction().getId() + " Client Account: " + accountingPositionDaily.getAccountingTransaction().getClientInvestmentAccount().getNumber() + " Security: " + accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getSymbol() + " Date: " + DateUtils.fromDateShort(accountingPositionDaily.getPositionDate()) + " Remaining Quantity: " + CoreMathUtils.formatNumberInteger(accountingPositionDaily.getRemainingQuantity());
	}
}
