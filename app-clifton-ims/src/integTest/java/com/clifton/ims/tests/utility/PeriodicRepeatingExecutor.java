package com.clifton.ims.tests.utility;

import com.clifton.core.concurrent.ConcurrentUtils;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


public class PeriodicRepeatingExecutor {

	private static final ScheduledExecutorService SCHEDULED_EXECUTOR_SERVICE = ConcurrentUtils.newScheduledExecutorService("PeriodicRepeatingExecutor", 2);


	static {
		Runtime.getRuntime().addShutdownHook(new Thread(() -> ConcurrentUtils.shutdownExecutorService(SCHEDULED_EXECUTOR_SERVICE)));
	}


	public static void await(Callable<Boolean> repeatedTask, int iterations, int waitSeconds) throws InterruptedException {
		RepeatedRunnable repeatedRunnable = new RepeatedRunnable(iterations, repeatedTask);
		ScheduledFuture<?> futureTradeOrder = SCHEDULED_EXECUTOR_SERVICE.scheduleWithFixedDelay(repeatedRunnable, waitSeconds, waitSeconds, TimeUnit.SECONDS);
		repeatedRunnable.await(futureTradeOrder);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class RepeatedRunnable implements Runnable {

		private final Callable<Boolean> repeatedTask;

		private final CountDownLatch countDownLatch;


		RepeatedRunnable(int iterations, Callable<Boolean> repeatedTask) throws InterruptedException {
			this.repeatedTask = repeatedTask;
			this.countDownLatch = new CountDownLatch(iterations);
		}


		public void await(ScheduledFuture<?> future) throws InterruptedException {
			this.countDownLatch.await();
			future.cancel(false);
		}


		@Override
		public void run() {
			try {
				boolean status = this.repeatedTask.call();
				if (status) {
					complete();
				}
				else {
					this.countDownLatch.countDown();
				}
			}
			catch (Exception e) {
				complete();
			}
		}


		private void complete() {
			while (this.countDownLatch.getCount() > 0) {
				this.countDownLatch.countDown();
			}
		}
	}
}
