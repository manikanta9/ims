package com.clifton.ims.tests.investment.account.search;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * Tests of the Search functionality of the InvestmentAccountServiceSearch tests.
 * Note: these tests are currently focused on queries with using the dtcParticipant searchform parameter.
 * These tests can be expanded as needed in the future to test other SearchForm properties as well.
 *
 * @author davidi
 */
public class InvestmentAccountServiceSearchTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentAccountService investmentAccountService;


	@Test
	public void testGetInvestmentAccountList_dtcParticipant_true_ourAccount_true() {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setDtcParticipant(true);
		searchForm.setStart(0);
		searchForm.setLimit(100);

		List<InvestmentAccount> clientAccountList = this.investmentAccountService.getInvestmentAccountList(searchForm);

		for (InvestmentAccount clientAccount : CollectionUtils.getIterable(clientAccountList)) {
			Assertions.assertTrue(hasHoldingAccountWithIssuerDtcEntry(clientAccount),
					"Missing holding account with issuer that is DTC Participant for client account: " + clientAccount.getLabel());
		}
	}


	@Test
	public void testGetInvestmentAccountList_dtcParticipant_false_ourAccount_true() {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setDtcParticipant(false);
		searchForm.setStart(0);
		searchForm.setLimit(100);

		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm);

		for (InvestmentAccount account : CollectionUtils.getIterable(accountList)) {
			Assertions.assertFalse(hasHoldingAccountWithIssuerDtcEntry(account),
					"Holding account found with issuer that is DTC Participant for client account: " + account.getLabel());
		}
	}


	@Test
	public void testGetInvestmentAccountList_dtcParticipant_true_ourAccount_false() {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(false);
		searchForm.setDtcParticipant(true);
		searchForm.setStart(0);
		searchForm.setLimit(100);

		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm);

		for (InvestmentAccount account : CollectionUtils.getIterable(accountList)) {
			Assertions.assertTrue(account.getIssuingCompany().isDTCParticipant(),
					"Holding account with issuer that is not a DTC Participant found: " + account.getLabel());
		}
	}


	@Test
	public void testGetInvestmentAccountList_dtcParticipant_false_ourAccount_false() {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(false);
		searchForm.setDtcParticipant(false);
		searchForm.setStart(0);
		searchForm.setLimit(100);

		List<InvestmentAccount> accountList = this.investmentAccountService.getInvestmentAccountList(searchForm);

		for (InvestmentAccount account : CollectionUtils.getIterable(accountList)) {
			Assertions.assertFalse(account.getIssuingCompany().isDTCParticipant(),
					"Holding account found with issuer that is DTC Participant: " + account.getLabel());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Determines if all Client account has a holding account with an issuing company that is a DTC participant.
	 */
	private boolean hasHoldingAccountWithIssuerDtcEntry(InvestmentAccount clientAccount) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(false);
		searchForm.setMainAccountId(clientAccount.getId());
		List<InvestmentAccount> holdingAccountList = this.investmentAccountService.getInvestmentAccountList(searchForm);
		for (InvestmentAccount holdingAccount : CollectionUtils.getIterable(holdingAccountList)) {
			if (!StringUtils.isEmpty(holdingAccount.getIssuingCompany().getDtcNumber())) {
				return true;
			}
		}

		return false;
	}
}
