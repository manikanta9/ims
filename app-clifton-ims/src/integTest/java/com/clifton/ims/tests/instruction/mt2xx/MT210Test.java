package com.clifton.ims.tests.instruction.mt2xx;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountRelationshipPurposes;
import com.clifton.ims.tests.investment.instruction.InvestmentInstructionUtils;
import com.clifton.ims.tests.trade.TradeCreator;
import com.clifton.ims.tests.utility.PeriodicRepeatingExecutor;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBusinessCompany;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryBusinessCompanyService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryBusinessCompanySearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.swift.server.SwiftTestMessageServer;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.autoclient.SwiftExpectedMessage;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import com.clifton.test.util.PropertiesLoader;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


public class MT210Test extends BaseImsIntegrationTest {

	private static final String CONTEXT_PROPERTIES_PATH = PropertiesLoader.getPropertiesFileUri();
	private static final String TEST_DATA_PATH = "com/clifton/ims/tests/swift/server/data";
	private static final String DELIVERY_NAME = MT210Test.class.getSimpleName();

	private static final String TEST_REQUEST_INSTRUCTION_MESSAGE = "{1:F01PPSCUS66AXXX0000000000}{2:I210CNORUS44XETDN}{4:\r\n" +
			":20:74977\r\n" +
			":25:ARFD02\r\n" +
			":30:171018\r\n" +
			":21:MARG\r\n" +
			":32B:USD1410,\r\n" +
			":52A:MSNYUS33\r\n" +
			":56A:IRVTUS3NXXX\r\n" +
			"-}";

	private static final String TEST_RESPONSE_INSTRUCTION_MESSAGE = "{1:F21PPSCUS66AXXX0072074855}{4:{177:1710180637}{451:0}}{1:F01PPSCUS66AXXX0072074855}{2:I210CNORUS44XETDN}{4:\r\n" +
			":20:74977\r\n" +
			":25:ARFD02\r\n" +
			":30:171018\r\n" +
			":21:MARG\r\n" +
			":32B:USD1410,\r\n" +
			":52A:MSNYUS33\r\n" +
			":56A:IRVTUS3NXXX\r\n" +
			"-}{5:{MAC:00000000}{CHK:A9BD48ED991F}}";

	private static final String TEST_REQUEST_INSTRUCTION_CANCEL_MESSAGE = "{1:F01PPSCUS66AXXX0000000000}{2:I292NWNBSGS1XXXXN}{4:\r\n" +
			":20:1234\r\n" +
			":21:123\r\n" +
			":11S:210\r\n" +
			"170803\r\n" +
			":20:123\r\n" +
			":30:170803\r\n" +
			":21:MARG\r\n" +
			":32B:USD123,456\r\n" +
			"-}";

	private static final String TEST_RESPONSE_INSTRUCTION_CANCEL_MESSAGE = "{1:F21PPSCUS66AXXX0072074855}{4:{177:1710180637}{451:0}}{1:F01PPSCUS66AXXX0000000000}{2:I292NWNBSGS1XXXXN}{4:\r\n" +
			":20:1234\r\n" +
			":21:123\r\n" +
			":11S:210\r\n" +
			"170803\r\n" +
			":20:123\r\n" +
			":30:170803\r\n" +
			":21:MARG\r\n" +
			":32B:USD123,456\r\n" +
			"-}{5:{MAC:00000000}{CHK:F50716A9AAFD}}";

	private static final Date tradeDate = DateUtils.toDate("09/27/2017", DateUtils.DATE_FORMAT_INPUT);
	private static final Date m2mDate = DateUtils.toDate("10/17/2017", DateUtils.DATE_FORMAT_INPUT);

	private static SwiftTestMessageServer swiftTestMessageServer;

	@Resource
	private TradeCreator tradeCreator;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentInstructionUtils investmentInstructionUtils;

	@Resource
	private AccountingUtils accountingUtils;

	@Resource
	private InvestmentInstructionService investmentInstructionService;

	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	@Resource
	private InvestmentInstructionDeliveryBusinessCompanyService investmentInstructionDeliveryBusinessCompanyService;


	private InvestmentInstructionCategory category;
	private Set<InvestmentInstructionDeliveryBusinessCompany> investmentInstructionDeliveryBusinessCompanies = new HashSet<>();

	private AccountingM2MDaily accountingM2MDaily;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void beforeClass() throws Exception {
		swiftTestMessageServer = new SwiftTestMessageServer(CONTEXT_PROPERTIES_PATH, TEST_DATA_PATH);
		swiftTestMessageServer.start();
	}


	@AfterAll
	public static void afterClass() throws Exception {
		swiftTestMessageServer.shutdown();
	}


	@BeforeEach
	public void before() {
		Assertions.assertNotNull(swiftTestMessageServer.sendStartTestSession());
		this.category = this.investmentInstructionUtils.getInvestmentInstructionCategoryByName("M2M Counterparty");
		Assertions.assertNotNull(this.category);
	}


	@AfterEach
	public void after() {
		swiftTestMessageServer.sendStopTestSession();

		final InvestmentInstructionStatus errorStatus = this.investmentInstructionUtils.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);

		this.investmentInstructionUtils.getInstructionItemList(this.category, m2mDate, this.accountingM2MDaily).stream()
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.COMPLETED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.CANCELED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.ERROR.name()))
				.forEach(i -> {
					i.setStatus(errorStatus);
					this.investmentInstructionUtils.saveInvestmentInstructionItem(i);
				});

		InvestmentInstructionDeliveryType deliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, false);
		InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setDeliveryTypeId(deliveryType.getId());
		CollectionUtils.getStream(this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm))
				.forEach(d -> {
					d.setDeliveryType(null);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		this.investmentInstructionDeliveryBusinessCompanies
				.forEach(d -> this.investmentInstructionDeliveryBusinessCompanyService.deleteInvestmentInstructionDeliveryBusinessCompany(d.getId()));
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void futuresM2MTest() throws InterruptedException {
		// TradeType.OPTIONS:  the account relationship cannot be 'Futures Broker (Hold)'.
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getHoldingAccount(), accountInfo.getCustodianAccount(), InvestmentAccountRelationshipPurposes.MARK_TO_MARKET);
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.MARK_TO_MARKET);
		// add 'Futures Broker' relationship
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_FUTURES);
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(
				accountInfo,
				"ESZ7",
				DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT),
				169.00000000,
				true,
				TradeDestination.MANUAL,
				BusinessCompanies.MORGAN_STANLEY_AND_CO_INC);
		Assertions.assertNotNull(trade);

		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create().
				with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(TEST_REQUEST_INSTRUCTION_MESSAGE)
								.withCopyRequestValue("20")
								.withReplacement("25", "ARFD02", accountInfo.getCustodianAccount().getNumber())
								.withReplacement("30", "171018", LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")))
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(TEST_RESPONSE_INSTRUCTION_MESSAGE)
								.withCopyRequestValue("20")
								.withReplacement("25", "ARFD02", accountInfo.getCustodianAccount().getNumber())
								.withReplacement("30", "171018", LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")))
								.build()
				).build();
		swiftTestMessageServer.sendSwiftTestCase(swiftTestCase);

		trade = approveTrade(trade);
		fullyExecuteTrade(trade);

		// create mark-to-market record
		String status = this.accountingUtils.createMarkToMarket(m2mDate, accountInfo.getHoldingAccount(),
				accountInfo.getHoldingAccount().getIssuingCompany(), accountInfo.getHoldingAccount().getType());
		MatcherAssert.assertThat(status, IsEqual.equalTo("Processing Complete. Total accounts: 1; Failed accounts: 0; Processed accounts: 1"));

		// make sure they were created with assumed values.
		List<AccountingM2MDaily> accountingM2MDailyList = this.accountingUtils.getAccountingM2MDailyList(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), m2mDate);
		Assertions.assertEquals(1, accountingM2MDailyList.size());

		// update mark-to-market amounts
		this.accountingM2MDaily = accountingM2MDailyList.get(0);
		this.accountingUtils.updateAccountingM2MDailyAmounts(this.accountingM2MDaily, new BigDecimal("1410.00"));

		// reconcile mark-to-market record
		int reconciled = this.accountingUtils.reconcileAccountingM2MDaily(m2mDate, new BigDecimal("0.50"), accountInfo.getHoldingAccount().getType());
		Assertions.assertTrue(reconciled > 0);

		// post mark-to-market record
		int bookedCount = this.accountingUtils.bookAndPostAccountingM2MDaily(m2mDate, accountInfo.getHoldingAccount().getType(), accountInfo.getClientAccount());
		Assertions.assertTrue(bookedCount > 0);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, m2mDate, null).isEmpty(),
				String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), m2mDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, m2mDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, m2mDate, this.accountingM2MDaily).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] mark-to-market: [%s]", this.category.getLabel(), m2mDate, this.accountingM2MDaily.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, m2mDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		List<InvestmentInstructionItem> investmentInstructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, m2mDate, this.accountingM2MDaily))
				.collect(Collectors.toList());
		Assertions.assertFalse(investmentInstructionItems.isEmpty(), "Should have Open Instruction Items.");

		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, true);
		Assertions.assertNotNull(investmentInstructionDeliveryType);
		InvestmentInstructionDelivery instructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils
				.getBusinessCompany(BusinessCompanies.BANK_OF_NEW_YORK_MELLON_INTERMEDIARY), DELIVERY_NAME);
		if (Objects.isNull(instructionDelivery)) {
			instructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils
					.getBusinessCompany(BusinessCompanies.BANK_OF_NEW_YORK_MELLON_INTERMEDIARY), DELIVERY_NAME, "89-0000-6978", "NOTUSED");
		}
		Assertions.assertNotNull(instructionDelivery);

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, m2mDate, this.accountingM2MDaily).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		Assertions.assertNotNull(investmentInstruction);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// make sure definitions have a delivery type
		investmentInstructionItems.stream()
				.map(InvestmentInstructionItem::getInstruction)
				.map(InvestmentInstruction::getDefinition)
				.filter(i -> Objects.isNull(i.getDeliveryType()))
				.forEach(d -> {
					d.setDeliveryType(investmentInstructionDeliveryType);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		// link between type and business company (morgan stanley).
		InvestmentInstructionDeliveryBusinessCompanySearchForm linkSearchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		linkSearchForm.setBusinessCompanyId(accountInfo.getHoldingAccount().getIssuingCompany().getId());
		linkSearchForm.setTypeId(investmentInstructionDeliveryType.getId());
		linkSearchForm.setDeliveryCurrencyId(this.usd.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> links = this.investmentInstructionDeliveryBusinessCompanyService.getInvestmentInstructionDeliveryBusinessCompanyList(linkSearchForm);
		if (links.isEmpty()) {
			InvestmentInstructionDeliveryBusinessCompany link = new InvestmentInstructionDeliveryBusinessCompany();
			link.setDeliveryCurrency(this.usd);
			link.setInvestmentInstructionDeliveryType(investmentInstructionDeliveryType);
			link.setReferenceOne(instructionDelivery);
			link.setReferenceTwo(accountInfo.getHoldingAccount().getIssuingCompany());
			this.investmentInstructionDeliveryBusinessCompanyService.saveInvestmentInstructionDeliveryBusinessCompany(link);
			this.investmentInstructionDeliveryBusinessCompanies.add(link);
		}

		// run instruction items
		int[] instructionItemIDs = investmentInstructionItems.stream().mapToInt(InvestmentInstructionItem::getId).toArray();
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItemIDs);

		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, m2mDate, this.accountingM2MDaily, InvestmentInstructionStatusNames.SENT, instructionItemIDs), 10, 1);

		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(10, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for IMS to process the response from the fake Swift Server.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, m2mDate, this.accountingM2MDaily, InvestmentInstructionStatusNames.COMPLETED, instructionItemIDs), 10, 1);
		Set<String> instructionStatuses = this.investmentInstructionUtils.getInstructionItemList(this.category, m2mDate, this.accountingM2MDaily).stream()
				.map(InvestmentInstructionItem::getStatus)
				.map(InvestmentInstructionStatus::getName)
				.collect(Collectors.toSet());
		Assertions.assertTrue(instructionStatuses.size() == 1
				&& instructionStatuses.contains(InvestmentInstructionStatusNames.COMPLETED.name()), "All instructions items should be [COMPLETED] " + instructionStatuses);
	}


	@Test
	public void futuresM2MCancelTest() throws InterruptedException {
		// TradeType.OPTIONS:  the account relationship cannot be 'Futures Broker (Hold)'.
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getHoldingAccount(), accountInfo.getCustodianAccount(), InvestmentAccountRelationshipPurposes.MARK_TO_MARKET);
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.MARK_TO_MARKET);
		// add 'Futures Broker' relationship
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_FUTURES);
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(
				accountInfo,
				"ESZ7",
				DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT),
				169.00000000,
				true,
				TradeDestination.MANUAL,
				BusinessCompanies.MORGAN_STANLEY_AND_CO_INC);
		Assertions.assertNotNull(trade);

		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(TEST_REQUEST_INSTRUCTION_MESSAGE)
								.withCopyRequestValue("20")
								.withReplacement("25", "ARFD02", accountInfo.getCustodianAccount().getNumber())
								.withReplacement("30", "171018", LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")))
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(TEST_RESPONSE_INSTRUCTION_MESSAGE)
								.withCopyRequestValue("20")
								.withReplacement("25", "ARFD02", accountInfo.getCustodianAccount().getNumber())
								.withReplacement("30", "171018", LocalDate.now().format(DateTimeFormatter.ofPattern("yyMMdd")))
								.build()
				)
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(TEST_REQUEST_INSTRUCTION_CANCEL_MESSAGE)
								.withCopyRequestValue("30")
								.withCopyRequestValue("20", true)
								.withCopyRequestValue("21", true)
								.withCopyRequestValue("20", true)
								.withCopyRequestValue("32B")
								.withCopyRequestValue("11S")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(TEST_RESPONSE_INSTRUCTION_CANCEL_MESSAGE)
								.withCopyRequestValue("30")
								.withCopyRequestValue("20", true)
								.withCopyRequestValue("21", true)
								.withCopyRequestValue("20", true)
								.withCopyRequestValue("32B")
								.withCopyRequestValue("11S")
								.build()
				)
				.build();
		swiftTestMessageServer.sendSwiftTestCase(swiftTestCase);

		trade = approveTrade(trade);
		fullyExecuteTrade(trade);

		// create mark-to-market record
		String status = this.accountingUtils.createMarkToMarket(m2mDate, accountInfo.getHoldingAccount(),
				accountInfo.getHoldingAccount().getIssuingCompany(), accountInfo.getHoldingAccount().getType());
		MatcherAssert.assertThat(status, IsEqual.equalTo("Processing Complete. Total accounts: 1; Failed accounts: 0; Processed accounts: 1"));

		// make sure they were created with assumed values.
		List<AccountingM2MDaily> accountingM2MDailyList = this.accountingUtils.getAccountingM2MDailyList(accountInfo.getClientAccount(), accountInfo.getHoldingAccount(), m2mDate);
		Assertions.assertEquals(1, accountingM2MDailyList.size());

		// update mark-to-market amounts
		this.accountingM2MDaily = accountingM2MDailyList.get(0);
		this.accountingUtils.updateAccountingM2MDailyAmounts(this.accountingM2MDaily, new BigDecimal("1410.00"));

		// reconcile mark-to-market record
		int reconciled = this.accountingUtils.reconcileAccountingM2MDaily(m2mDate, new BigDecimal("0.50"), accountInfo.getHoldingAccount().getType());
		Assertions.assertTrue(reconciled > 0);

		// post mark-to-market record
		int bookedCount = this.accountingUtils.bookAndPostAccountingM2MDaily(m2mDate, accountInfo.getHoldingAccount().getType(), accountInfo.getClientAccount());
		Assertions.assertTrue(bookedCount > 0);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, m2mDate, null).isEmpty(),
				String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), m2mDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, m2mDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, m2mDate, this.accountingM2MDaily).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] mark-to-market: [%s]", this.category.getLabel(), m2mDate, this.accountingM2MDaily.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, m2mDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		List<InvestmentInstructionItem> investmentInstructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, m2mDate, this.accountingM2MDaily))
				.collect(Collectors.toList());
		Assertions.assertFalse(investmentInstructionItems.isEmpty(), "Should have Open Instruction Items.");

		InvestmentInstructionDeliveryType investmentInstructionDeliveryType = this.investmentInstructionUtils.getInvestmentInstructionDeliveryType(DELIVERY_NAME, true);
		Assertions.assertNotNull(investmentInstructionDeliveryType);
		InvestmentInstructionDelivery instructionDelivery = this.investmentInstructionUtils.getInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
				BusinessCompanies.BANK_OF_NEW_YORK_MELLON_INTERMEDIARY), DELIVERY_NAME);
		if (Objects.isNull(instructionDelivery)) {
			instructionDelivery = this.investmentInstructionUtils.createInvestmentInstructionDelivery(this.businessUtils.getBusinessCompany(
					BusinessCompanies.BANK_OF_NEW_YORK_MELLON_INTERMEDIARY), DELIVERY_NAME, "89-0000-6978", "NOTUSED");
		}
		Assertions.assertNotNull(instructionDelivery);

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, m2mDate, this.accountingM2MDaily).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		Assertions.assertNotNull(investmentInstruction);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		// make sure definitions have a delivery type
		investmentInstructionItems.stream()
				.map(InvestmentInstructionItem::getInstruction)
				.map(InvestmentInstruction::getDefinition)
				.filter(i -> Objects.isNull(i.getDeliveryType()))
				.forEach(d -> {
					d.setDeliveryType(investmentInstructionDeliveryType);
					this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(d);
				});

		// link between type and business company (morgan stanley).
		InvestmentInstructionDeliveryBusinessCompanySearchForm linkSearchForm = new InvestmentInstructionDeliveryBusinessCompanySearchForm();
		linkSearchForm.setBusinessCompanyId(accountInfo.getHoldingAccount().getIssuingCompany().getId());
		linkSearchForm.setTypeId(investmentInstructionDeliveryType.getId());
		linkSearchForm.setDeliveryCurrencyId(this.usd.getId());
		List<InvestmentInstructionDeliveryBusinessCompany> links = this.investmentInstructionDeliveryBusinessCompanyService.getInvestmentInstructionDeliveryBusinessCompanyList(linkSearchForm);
		if (links.isEmpty()) {
			InvestmentInstructionDeliveryBusinessCompany link = new InvestmentInstructionDeliveryBusinessCompany();
			link.setDeliveryCurrency(this.usd);
			link.setInvestmentInstructionDeliveryType(investmentInstructionDeliveryType);
			link.setReferenceOne(instructionDelivery);
			link.setReferenceTwo(accountInfo.getHoldingAccount().getIssuingCompany());
			this.investmentInstructionDeliveryBusinessCompanyService.saveInvestmentInstructionDeliveryBusinessCompany(link);
			this.investmentInstructionDeliveryBusinessCompanies.add(link);
		}

		// run instruction items
		int[] instructionItemIDs = investmentInstructionItems.stream().mapToInt(InvestmentInstructionItem::getId).toArray();
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItemIDs);

		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, m2mDate, this.accountingM2MDaily, InvestmentInstructionStatusNames.SENT, instructionItemIDs), 10, 1);

		// wait for IMS to process the response from the fake Swift Server.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, m2mDate, this.accountingM2MDaily, InvestmentInstructionStatusNames.COMPLETED, instructionItemIDs), 10, 1);
		Set<String> instructionStatuses = this.investmentInstructionUtils.getInstructionItemList(this.category, m2mDate, this.accountingM2MDaily).stream()
				.map(InvestmentInstructionItem::getStatus)
				.map(InvestmentInstructionStatus::getName)
				.collect(Collectors.toSet());
		Assertions.assertTrue(instructionStatuses.size() == 1
				&& instructionStatuses.contains(InvestmentInstructionStatusNames.COMPLETED.name()), "All instructions items should be [COMPLETED] " + instructionStatuses);

		// get items to cancel.
		Integer[] items = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("AccountingM2MDaily", this.accountingM2MDaily.getId()).stream()
				.map(InvestmentInstructionItem::getId)
				.toArray(Integer[]::new);

		// cancel items.
		this.investmentInstructionService.cancelInvestmentInstructionItemList(items);
		int[] pendingCancelItems = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("AccountingM2MDaily", this.accountingM2MDaily.getId()).stream()
				.filter(i -> Objects.equals(InvestmentInstructionStatusNames.PENDING_CANCEL, i.getStatus().getInvestmentInstructionStatusName()))
				.mapToInt(InvestmentInstructionItem::getId)
				.toArray();
		Assertions.assertTrue(pendingCancelItems.length > 0);

		// send PENDING_CANCEL items.
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItemIDs);

		// wait for swift server to send all responses.
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for cancelled status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, m2mDate, this.accountingM2MDaily, InvestmentInstructionStatusNames.CANCELED, instructionItemIDs), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, m2mDate, this.accountingM2MDaily, InvestmentInstructionStatusNames.CANCELED, instructionItemIDs), "All instructions items should be [CANCELLED].");
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public Trade approveTrade(Trade trade) {
		this.tradeUtils.approveTrade(trade);

		return this.tradeService.getTrade(trade.getId());
	}


	public Trade fullyExecuteTrade(Trade trade) {
		this.tradeUtils.fullyExecuteTrade(trade);

		return this.tradeService.getTrade(trade.getId());
	}
}
