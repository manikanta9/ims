package com.clifton.ims.tests.system.note;

import com.clifton.business.client.BusinessClient;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.document.system.note.DocumentSystemNoteService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.note.upload.SystemNoteUploadCommand;
import com.clifton.system.note.upload.SystemNoteUploadService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.util.RandomUtils;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * SystemNoteSecurityTests tests the dynamic security for SystemNotes
 * <p>
 * NOTE: Uses Analyst Group as example for tests - these users have access to Investment Account Notes (InvestmentAccountNote resource), but not BusinessClient notes (BusinessClient table)
 *
 * @author manderson
 */

public class SystemNoteSecurityTests extends BaseImsIntegrationTest {

	@Resource
	private SystemNoteService systemNoteService;

	@Resource
	private SystemNoteUploadService systemNoteUploadService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@Resource
	private DocumentSystemNoteService documentSystemNoteService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static final String INVESTMENT_ACCOUNT_TABLE = "InvestmentAccount";
	private static final String BUSINESS_CLIENT_TABLE = "BusinessClient";
	private static final String BUSINESS_CONTACT_TABLE = "BusinessContact";
	private static final String BUSINESS_CLIENT_RELATIONSHIP_TABLE = "BusinessClientRelationship";

	private static final String TEST_ACCOUNT_1_NUMBER = "221800";
	private static final String TEST_ACCOUNT_2_NUMBER = "221700";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void systemNoteType_AccessAllowed_NoteSpecificSecurityResource() {
		SystemNoteType noteType = setupNoteType(INVESTMENT_ACCOUNT_TABLE);
		this.systemNoteService.saveSystemNoteType(noteType);
		noteType.setBlankNoteAllowed(true);
		this.systemNoteService.saveSystemNoteType(noteType);
		this.systemNoteService.deleteSystemNoteType(noteType.getId());
	}


	@Test
	public void systemNoteType_AccessAllowed_TableSecurityResource() {
		SystemNoteType noteType = setupNoteType(BUSINESS_CONTACT_TABLE);
		this.systemNoteService.saveSystemNoteType(noteType);
		noteType.setBlankNoteAllowed(true);
		this.systemNoteService.saveSystemNoteType(noteType);
		this.systemNoteService.deleteSystemNoteType(noteType.getId());
	}


	@Test
	public void systemNoteType_AccessDenied_NoteSpecificSecurityResource() {
		SystemNoteType noteType = setupNoteType(BUSINESS_CLIENT_RELATIONSHIP_TABLE);
		Throwable ex = null;
		try {
			this.systemNoteService.saveSystemNoteType(noteType);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [CREATE] permission(s) to [BusinessClientRelationshipNote] security resource.", ex);
	}


	@Test
	public void systemNoteType_AccessDenied_TableSecurityResource() {
		SystemNoteType noteType = setupNoteType(BUSINESS_CLIENT_TABLE);
		Throwable ex = null;
		try {
			this.systemNoteService.saveSystemNoteType(noteType);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);
	}

	//////////////////////////////////////////////////////////


	@Test
	public void systemNote_AccessAllowed() {
		SystemNoteType noteType = setupNoteType(INVESTMENT_ACCOUNT_TABLE);
		this.saveNoteTypeAsAdmin(noteType);

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(BeanUtils.getIdentityAsLong(this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_1_NUMBER)));
		note.setText("Test note");
		this.systemNoteService.saveSystemNote(note);

		note.setText("Update test note");
		this.systemNoteService.saveSystemNote(note);

		//  Add Support for Link to Multiple
		noteType.setLinkToMultipleAllowed(true);
		this.saveNoteTypeAsAdmin(noteType);

		this.systemNoteService.saveSystemNoteLink(note.getId(), this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_2_NUMBER).getId());

		this.systemNoteService.deleteSystemNoteLink(note.getId(), this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_1_NUMBER).getId());

		this.systemNoteService.deleteSystemNote(note.getId());
	}


	@Test
	public void systemNote_AccessDenied() {
		SystemNoteType noteType = setupNoteType(BUSINESS_CLIENT_TABLE);
		this.saveNoteTypeAsAdmin(noteType);

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(BeanUtils.getIdentityAsLong(getClient(TEST_ACCOUNT_1_NUMBER)));
		note.setText("Test note");

		Throwable ex = null;
		try {
			this.systemNoteService.saveSystemNote(note);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);

		switchToAdminUser();
		this.systemNoteService.saveSystemNote(note);
		switchToAnalystUser();

		note.setText("Update test note");
		ex = null;
		try {
			this.systemNoteService.saveSystemNote(note);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);

		//  Add Support for Link to Multiple
		noteType.setLinkToMultipleAllowed(true);
		this.saveNoteTypeAsAdmin(noteType);

		ex = null;
		try {
			this.systemNoteService.saveSystemNoteLink(note.getId(), this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_2_NUMBER).getId());
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);

		switchToAdminUser();
		this.systemNoteService.saveSystemNoteLink(note.getId(), this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_2_NUMBER).getId());
		switchToAnalystUser();


		ex = null;
		try {
			this.systemNoteService.deleteSystemNoteLink(note.getId(), this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_1_NUMBER).getId());
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);


		ex = null;
		try {
			this.systemNoteService.deleteSystemNote(note.getId());
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);

		switchToAdminUser();
		this.systemNoteService.deleteSystemNote(note.getId());
	}

	//////////////////////////////////////////////////////////


	@Test
	public void systemNote_upload_AccessAllowed() {
		SystemNoteUploadCommand upload = new SystemNoteUploadCommand();
		upload.setFkFieldTableName(INVESTMENT_ACCOUNT_TABLE);
		upload.setFkFieldId(this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_1_NUMBER).getId());

		// Empty File - which is fine, just testing security
		Throwable ex = null;
		try {
			this.systemNoteUploadService.uploadSystemNoteFile(upload);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("File selection is required to process an upload.", ex);
	}


	@Test
	public void systemNote_upload_AccessDenied() {
		SystemNoteUploadCommand upload = new SystemNoteUploadCommand();
		upload.setFkFieldTableName(BUSINESS_CLIENT_TABLE);
		upload.setFkFieldId(getClient(TEST_ACCOUNT_1_NUMBER).getId());

		// Empty File - which is fine, just testing security
		Throwable ex = null;
		try {
			this.systemNoteUploadService.uploadSystemNoteFile(upload);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);
	}


	//////////////////////////////////////////////////////////


	@Test
	public void systemNote_documents_AccessAllowed() {
		SystemNoteType noteType = setupNoteType(INVESTMENT_ACCOUNT_TABLE);
		noteType.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
		this.saveNoteTypeAsAdmin(noteType);

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(BeanUtils.getIdentityAsLong(this.investmentAccountUtils.getClientAccountByNumber(TEST_ACCOUNT_1_NUMBER)));
		note.setText("Test note");
		// Empty File - which is fine, just testing security
		Throwable ex = null;
		try {
			this.documentSystemNoteService.uploadDocumentSystemNote(note, null);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("File is Required.", ex);
	}


	@Test
	public void systemNote_documents_AccessDenied() {
		SystemNoteType noteType = setupNoteType(BUSINESS_CLIENT_TABLE);
		noteType.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
		this.saveNoteTypeAsAdmin(noteType);

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(BeanUtils.getIdentityAsLong(getClient(TEST_ACCOUNT_1_NUMBER)));
		note.setText("Test note");
		// Empty File - which is fine, just testing security
		Throwable ex = null;
		try {
			this.documentSystemNoteService.uploadDocumentSystemNote(note, null);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [WRITE] permission(s) to [BusinessClient] security resource.", ex);
	}


	@Test
	public void systemNote_documents_AccessDenied_NoteSpecificSecurityResource() {
		SystemNoteType noteType = setupNoteType(BUSINESS_CLIENT_RELATIONSHIP_TABLE);
		noteType.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.SINGLE);
		this.saveNoteTypeAsAdmin(noteType);

		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(BeanUtils.getIdentityAsLong(getClient(TEST_ACCOUNT_1_NUMBER).getClientRelationship()));
		note.setText("Test note");
		// Empty File - which is fine, just testing security
		Throwable ex = null;
		try {
			this.documentSystemNoteService.uploadDocumentSystemNote(note, null);
		}
		catch (Throwable e) {
			ex = e;
		}
		validateExpectedErrorMessage("Access Denied. You do not have [CREATE] permission(s) to [BusinessClientRelationshipNote] security resource.", ex);
	}


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	@BeforeEach
	public void switchToAnalystUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_ANALYST_USER, ImsTestProperties.TEST_ANALYST_USER_PASSWORD);
	}


	public void switchToAdminUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Helper Methods                              ////////
	////////////////////////////////////////////////////////////////////////////


	private SystemNoteType setupNoteType(String tableName) {
		SystemNoteType noteType = new SystemNoteType();
		noteType.setName(tableName + ": " + RandomUtils.randomName());
		noteType.setTable(this.systemSchemaService.getSystemTableByName(tableName));
		noteType.setOrder(1000);
		noteType.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.NONE);
		return noteType;
	}


	private void saveNoteTypeAsAdmin(SystemNoteType bean) {
		switchToAdminUser();
		this.systemNoteService.saveSystemNoteType(bean);
		switchToAnalystUser();
	}


	private void validateExpectedErrorMessage(String message, Throwable e) {
		String actualMsg = null;
		if (e != null) {
			if (e instanceof ImsErrorResponseException) {
				actualMsg = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			if (actualMsg == null) {
				actualMsg = e.getMessage();
			}
		}
		if (actualMsg != null && actualMsg.startsWith("Unexpected Error Occurred: [")) {
			actualMsg = actualMsg.substring(28, actualMsg.length() - 2);
		}
		Assertions.assertEquals(message, actualMsg);
	}


	private BusinessClient getClient(String accountNumber) {
		return this.investmentAccountUtils.getClientAccountByNumber(accountNumber).getBusinessClient();
	}
}
