package com.clifton.ims.tests.integration.export.aws.s3;

import com.adobe.testing.s3mock.junit5.S3MockExtension;
import com.adobe.testing.s3mock.util.HashUtil;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.status.Status;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.ExportDefinitionType;
import com.clifton.export.run.runner.ExportRunnerService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;


import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;



/**
 * Tests for AWS S3 export functionality
 */
//TODO: Re-enable when keys have been setup and S3 exports are fully functional
@Disabled
public class AWSS3ExportTests extends BaseImsIntegrationTest {

	@RegisterExtension
	static final S3MockExtension S3_MOCK =
			S3MockExtension.builder().silent().withSecureConnection(true).build();

	private static final Integer POLL_SLEEP_TIME = 1000;
	private static final Integer POLL_LIMIT_COUNT = 60;

	private static final String EXPORT_DEFINITION_TYPE_NAME = "Internal";

	private static final String BUCKET = "ppacoresoftsyssandbox-priv-mpls-testing";
	private static final String REGION = "us-east-1";



	//This is a shared network location where files will be placed for pickup by the integration server
	private static final String FILE_BASE_PATH = "//MSP-700-03.paraport.com/Development/Integration Testing/temp/";

	private static final String FILENAME_TEMPLATE_1 = String.format("%.0f", Math.random() * 1000000000) + "CLIFTON_EXPORT_01.txt";
	private static final String FILE_CONTENT_1 = "test content 1";

	private static final String FILENAME_TEMPLATE_2 = String.format("%.0f", Math.random() * 1000000000) + "CLIFTON_EXPORT_02.txt";
	private static final String FILE_CONTENT_2 = "test content 2";


	private static short STRING_ID = 1;
	private static short INTEGER_ID = 2;

	private final String ENDPOINT = S3_MOCK.getServiceEndpoint();
	private final AmazonS3 s3Client = S3_MOCK.createS3Client(REGION);

	private ExportDefinitionType exportDefinitionType;

	@Resource
	private ExportDefinitionService exportDefinitionService;
	@Resource
	private ExportRunnerService exportRunnerService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private S3MockUtils s3MockUtils;


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////                                    TESTS                                           //////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAWSS3() throws IOException, InterruptedException {
		testSendObject();
	}


	/**
	 * Sending file to bucket
	 */
	private void testSendObject() throws IOException, InterruptedException {
		List<ExportDefinitionDestination> destinationList = new ArrayList<>();
		List<ExportDefinitionContent> contentList = new ArrayList<>();
		String randomString = Integer.toString(RandomUtils.randomNumber());
		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(getExportDefinitionType(), randomString);
		try {
			//Set the Destination
			AWSS3ExportDestinationBuilder builder = new AWSS3ExportDestinationBuilder(this.systemBeanService, this.exportDefinitionService)
					.exportDefinition(exportDefinition)
					.bucket(BUCKET)
					.region(REGION)
					.endpoint(ENDPOINT);
			destinationList.add(builder.build());

			//Content File
			FileContainer expectedFile1 = FileContainerFactory.getFileContainer(FILE_BASE_PATH + FILENAME_TEMPLATE_1);
			FileUtils.writeStringToFile(expectedFile1, FILE_CONTENT_1);

			//Set the Contents
			createContent(FILENAME_TEMPLATE_1, expectedFile1.getAbsolutePath(), exportDefinition);

			//Create the bucket
			s3Client.createBucket(BUCKET);

			//Send the export
			send(exportDefinition.getId());

			// Poll until the file is in the bucket (1 minute max)
			Assertions.assertTrue(this.s3MockUtils.bucketHasObjects(s3Client, BUCKET, FILENAME_TEMPLATE_1, POLL_LIMIT_COUNT, POLL_SLEEP_TIME));

			S3Object s3Object = s3Client.getObject(BUCKET,FILENAME_TEMPLATE_1);

			// Needs to catch a NoSuchAlgorithmException if no md5 can be found
			try {
				compareFileContents(expectedFile1, s3Object);
			}
			catch (NoSuchAlgorithmException e){
				throw new RuntimeException(e.getMessage());
			}
		}
		finally {
			//cleanupEntities(exportDefinition, destinationList, contentList);
		}
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////                                    HELPERS                                   ///////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	private void compareFileContents(FileContainer expected, S3Object actual) throws IOException, NoSuchAlgorithmException {
		final File uploadFile = new File(expected.getPath());
		try (S3ObjectInputStream objectContent = actual.getObjectContent();
		     final InputStream uploadFileIs = new FileInputStream(uploadFile);){
			final String uploadHash = HashUtil.getDigest(uploadFileIs);
			final String downloadedHash = HashUtil.getDigest(objectContent);
			Assertions.assertEquals(uploadHash, downloadedHash,
					"Up- and downloaded Files should have equal Hashes");
		}
		finally {
			//Cleanup
			expected.deleteFile();
		}
	}



	private Status send(Integer exportDefinitionId) {
		//Run the export
		return this.exportRunnerService.runManualExport(exportDefinitionId);
	}


	private ExportDefinition createExportDefinition(ExportDefinitionType exportDefType, String random) {
		//Create the Export Definition
		ExportDefinition exportDefinition = new ExportDefinition();
		exportDefinition.setName("AWS S3 Export Integration Test - " + random);
		exportDefinition.setType(exportDefType);
		this.exportDefinitionService.saveExportDefinition(exportDefinition);
		return exportDefinition;
	}


	private ExportDefinitionContent createContent(String filenameTemplate, String fileLocation, ExportDefinition exportDefinition) {
		SystemBean contentSystemBean = SystemBeanBuilder.newBuilder(null, "Network File Export Generator", this.systemBeanService)
				.addProperty("File Location", fileLocation)
				.build();

		ExportDefinitionContent content = new ExportDefinitionContent();
		content.setContentSystemBean(contentSystemBean);
		content.setFileNameTemplate(filenameTemplate);
		content.setDefinition(exportDefinition);
		this.exportDefinitionService.saveExportDefinitionContent(content);

		return content;
	}


	private void cleanupEntities(ExportDefinition definition, List<ExportDefinitionDestination> exportDefinitionDestinations, List<ExportDefinitionContent> exportDefinitionContents) {
		//Delete the contents
		for (ExportDefinitionContent content : exportDefinitionContents) {
			this.exportDefinitionService.deleteExportDefinitionContent(content.getId());
		}
		//Delete the destinations
		for (ExportDefinitionDestination destination : exportDefinitionDestinations) {
			this.exportDefinitionService.deleteExportDefinitionDestinationAndLink(destination.getId(), definition.getId());
		}
		this.exportDefinitionService.deleteExportDefinitionAndRunHistory(definition.getId());
	}




	//TODO: replace with optionals?
	private ExportDefinitionType getExportDefinitionType() {
		return this.exportDefinitionType == null ?
				this.exportDefinitionType = this.exportDefinitionService.getExportDefinitionTypeByName(EXPORT_DEFINITION_TYPE_NAME)
				: this.exportDefinitionType;
	}

}

