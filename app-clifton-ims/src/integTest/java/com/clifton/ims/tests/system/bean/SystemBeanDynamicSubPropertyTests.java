package com.clifton.ims.tests.system.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.system.query.SystemQueryBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


public class SystemBeanDynamicSubPropertyTests extends BaseImsIntegrationTest {

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private SystemQueryService systemQueryService;

	@Resource
	private SystemSchemaService systemSchemaService;


	@Test
	@Disabled
	// cannot run this test until the serialization is corrected
	public void testSubPropertySave() {
		List<SystemDataType> dataTypeList = this.systemSchemaService.getSystemDataTypeList();
		SystemTable table = this.systemSchemaService.getSystemTableByName("Trade");

		SystemQuery query = SystemQueryBuilder.newBuilder(table, "SELECT TOP 1 * FROM Trade", this.systemQueryService)
				.addParameter("Date", 1, BeanUtils.filter(dataTypeList, SystemDataType::getName, DataTypeNames.DATE.toString()).get(0))
				.addParameter("Account List", 2, BeanUtils.filter(dataTypeList, SystemDataType::getName, DataTypeNames.INTEGER.toString()).get(0))
				.buildAndSave();

		SystemBean exportSystemBean = SystemBeanBuilder.newBuilder(RandomUtils.randomNameAndNumber(), "Query Export Generator", this.systemBeanService)
				.addProperty("System Query", query.getId().toString())
				.addProperty("Export Format", "PDF")
				.buildAndSave();

		List<SystemBeanPropertyType> propertyTypeList = this.systemBeanService.getSystemBeanPropertyTypeListByBean(exportSystemBean);
		Assertions.assertEquals(5, propertyTypeList.size());

		//		SystemBeanPropertyType systemQueryField = propertyTypeList.get(0);
		//		SystemBeanPropertyType exportFormatField = propertyTypeList.get(1);
		SystemBeanPropertyType subDateField = propertyTypeList.get(3);
		SystemBeanPropertyType subAccountListField = propertyTypeList.get(4);

		SystemBeanProperty subDateProperty = new SystemBeanProperty();
		subDateProperty.setType(subDateField);
		subDateProperty.setValue("BUSINESS_DAYS_BACK:1");

		exportSystemBean.getPropertyList().add(subDateProperty);

		// clear the id's because virtual properties cannot sub id's, need to use the virtualId field
		subDateField.setId(null);
		subAccountListField.setId(null);

		SystemBeanProperty subAccountListProperty = new SystemBeanProperty();
		subAccountListProperty.setType(subDateField);
		subAccountListProperty.setValue("10");

		exportSystemBean.getPropertyList().add(subAccountListProperty);

		this.systemBeanService.saveSystemBean(exportSystemBean);
		@SuppressWarnings("unused")
		List<SystemBeanProperty> propertyList = this.systemBeanService.getSystemBeanPropertyListByBean(exportSystemBean);
	}


	@Test
	public void testSubPropertyTypeList() {
		List<SystemDataType> dataTypeList = this.systemSchemaService.getSystemDataTypeList();
		SystemTable table = this.systemSchemaService.getSystemTableByName("Trade");

		SystemQuery query = SystemQueryBuilder.newBuilder(table, "SELECT TOP 1 * FROM Trade", this.systemQueryService)
				.addParameter("Date", 1, BeanUtils.filter(dataTypeList, SystemDataType::getName, DataTypeNames.DATE.toString()).get(0))
				.addParameter("Account List", 2, BeanUtils.filter(dataTypeList, SystemDataType::getName, DataTypeNames.INTEGER.toString()).get(0))
				.buildAndSave();

		List<SystemQueryParameter> queryParameterList = this.systemQueryService.getSystemQueryParameterListByQuery(query.getId());

		SystemBean exportSystemBean = SystemBeanBuilder.newBuilder(RandomUtils.randomNameAndNumber(), "Query Export Generator", this.systemBeanService)
				.addProperty("System Query", query.getId().toString())
				.addProperty("Export Format", "PDF")
				.buildAndSave();

		List<SystemBeanPropertyType> propertyTypeList = this.systemBeanService.getSystemBeanPropertyTypeListByBean(exportSystemBean);
		Assertions.assertEquals(7, propertyTypeList.size());

		SystemBeanPropertyType systemQueryField = propertyTypeList.get(0);
		SystemBeanPropertyType exportFormatField = propertyTypeList.get(1);
		SystemBeanPropertyType includeHeader = propertyTypeList.get(2);
		SystemBeanPropertyType jsonField = propertyTypeList.get(3);
		SystemBeanPropertyType queryTimeOut = propertyTypeList.get(4);
		SystemBeanPropertyType subDateField = propertyTypeList.get(5);
		SystemBeanPropertyType subAccountListField = propertyTypeList.get(6);

		Assertions.assertEquals("System Query", systemQueryField.getName());
		Assertions.assertEquals("Export Format", exportFormatField.getName());
		Assertions.assertEquals("Do Not Include Header", includeHeader.getName());
		Assertions.assertEquals("System Query Parameter Values", jsonField.getName());
		Assertions.assertEquals("jsonParameterValue", jsonField.getSystemPropertyName());
		Assertions.assertEquals("queryTimeoutSeconds", queryTimeOut.getSystemPropertyName());
		Assertions.assertEquals("Date", subDateField.getName());
		Assertions.assertEquals("Account List", subAccountListField.getName());

		// check that the query field is configured to trigger a field reset
		Assertions.assertTrue(systemQueryField.isResetDynamicFieldsOnChange());

		// check that the sub fields are test to target the json field
		Assertions.assertEquals(jsonField.getId(), subDateField.getTargetSystemUserInterfaceAware().getId());
		Assertions.assertEquals(jsonField.getId(), subAccountListField.getTargetSystemUserInterfaceAware().getId());

		// check that the sub fields are virtual
		Assertions.assertTrue(subDateField.isVirtualProperty());
		Assertions.assertTrue(subAccountListField.isVirtualProperty());

		// check that the sub fields are virtual
		Assertions.assertEquals(queryParameterList.get(0).getId(), subDateField.getVirtualTypeId());
		Assertions.assertEquals(queryParameterList.get(1).getId(), subAccountListField.getVirtualTypeId());

		Assertions.assertTrue(subDateField.isVirtualProperty());
		Assertions.assertTrue(subAccountListField.isVirtualProperty());

		// check that the date field string
		String expectDateFieldString = "{displayField: 'name',valueField: 'value', xtype:'combo', mode:'local', store: {xtype: 'arraystore',fields:['name', 'value', 'description'],data: Clifton.calendar.date.COMMON_DATE_GENERATION_OPTIONS}}";
		Assertions.assertEquals(expectDateFieldString, subDateField.getUserInterfaceConfig());
	}
}
