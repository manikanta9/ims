package com.clifton.ims.tests.investment.instrument.calculator;

import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */

public class InvestmentCalculatorServiceTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentCalculatorService investmentCalculatorService;


	@Test
	public void testInvestmentInterestForDays() {
		Assertions.assertEquals(new BigDecimal("107.06"), this.investmentCalculatorService.getInvestmentInterestForDays(new BigDecimal("21411664.57"), new BigDecimal("0.18"), DateUtils.toDate("06/03/2015"), 1, DayCountConventions.ACTUAL_THREESIXTY.getAccrualString()));
		Assertions.assertEquals(new BigDecimal("145.20"), this.investmentCalculatorService.getInvestmentInterestForDays(new BigDecimal("1455942.38"), new BigDecimal("0.52"), DateUtils.toDate("12/10/2013"), 7, DayCountConventions.ACTUAL_THREESIXTYFIVE.getAccrualString()));
	}


	@Test
	public void testDirtyPrice_ZeroCouponSwap() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ZCSBRL-20180222-20270104-9.845");

		// Portfolio Run on 8/17/18 Account # 227000
		// Symbol = ZCSBRL-20180222-20270104-9.845
		Date testDate = DateUtils.toDate("08/17/2018");
		BigDecimal result = this.investmentCalculatorService.getInvestmentSecurityDirtyPrice(security.getId(), BigDecimal.valueOf(64982814.70), new BigDecimal("93.139107532948"), testDate);
		Assertions.assertEquals(new BigDecimal("93.837421258"), MathUtils.round(result, 9));
	}
}
