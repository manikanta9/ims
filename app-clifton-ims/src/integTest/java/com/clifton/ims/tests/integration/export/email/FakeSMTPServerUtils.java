package com.clifton.ims.tests.integration.export.email;

import com.clifton.core.email.FakeSMTPServer;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;


@Component
public class FakeSMTPServerUtils {

	/**
	 * Polls the server every given number of seconds up to a given limit.
	 * Returns true when messages are found otherwise returns false.
	 */
	public boolean serverHasMessages(FakeSMTPServer fakeSMTPServer, int pollLimitCount, int sleepTime) throws InterruptedException {
		int counter = 0;

		while (counter < pollLimitCount) {
			if (!CollectionUtils.isEmpty(fakeSMTPServer.getMessages())) {
				return true;
			}
			else {
				counter += 1;
				Thread.sleep(sleepTime);
			}
		}
		return false;
	}
}
