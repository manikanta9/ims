package com.clifton.ims.tests.investment.account;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceLevelTypes;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.search.BusinessServiceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountBusinessService;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Collections;


/**
 * The <code>InvestmentAccountWorkflowTests</code>
 * tests transitioning a client account through certain workflow states.
 *
 * @author mwacker
 */

public class InvestmentAccountWorkflowTests extends BaseImsIntegrationTest {

	@Resource
	private WorkflowTransitioner workflowTransitioner;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private BusinessServiceService businessServiceService;


	public void switchToAdminUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
	}


	public void switchToOperationsAdminUser() {
		this.userUtils.switchUser(ImsTestProperties.TEST_CLIENT_ADMIN_USER, ImsTestProperties.TEST_CLIENT_ADMIN_USER_PASSWORD);
	}


	@Test
	public void testActivationOfClientAccount() {
		switchToAdminUser();

		BusinessClient client = this.businessUtils.createBusinessClient();
		InvestmentAccount clientAccount = this.investmentAccountUtils.createInactiveClientAccount(client);

		/*
		 * Add BusinessService and service component. Doing so would produce error on changing the workflow state with a different user
		 * because hibernate will flush changes before we expect them.
		 */
		BusinessServiceSearchForm businessServiceSearchForm = new BusinessServiceSearchForm();
		businessServiceSearchForm.setSearchPattern("PIOS (Overlay)");
		BusinessService service = CollectionUtils.getFirstElement(this.businessServiceService.getBusinessServiceList(businessServiceSearchForm));
		if (service != null) {
			clientAccount.setBusinessService(service);

			businessServiceSearchForm.setSearchPattern(null);
			businessServiceSearchForm.setParentId(service.getId());
			businessServiceSearchForm.setServiceLevelType(BusinessServiceLevelTypes.SERVICE_COMPONENT);
			BusinessService componentService = CollectionUtils.getFirstElement(this.businessServiceService.getBusinessServiceList(businessServiceSearchForm));

			InvestmentAccountBusinessService investmentAccountBusinessService = new InvestmentAccountBusinessService();
			investmentAccountBusinessService.setBusinessService(componentService);
			investmentAccountBusinessService.setInvestmentAccount(clientAccount);
			investmentAccountBusinessService.setStartDate(DateUtils.toDate("10/12/2015"));
			clientAccount.setBusinessServiceList(Collections.singletonList(investmentAccountBusinessService));

			this.investmentAccountService.saveInvestmentAccount(clientAccount);
		}

		transitionInvestmentAccount("Submit for Approval", clientAccount);

		this.complianceUtils.createTradeAllRule(clientAccount);

		switchToOperationsAdminUser();
		try {
			InvestmentAccount lookedUpClientInvestmentAccount = this.investmentAccountService.getInvestmentAccount(clientAccount.getId());
			WorkflowState state = this.workflowDefinitionService.getWorkflowStateByName(lookedUpClientInvestmentAccount.getWorkflowState().getWorkflow().getId(), "Active");
			lookedUpClientInvestmentAccount.setWorkflowStateEffectiveStartDate(null); // clear effective start date.
			lookedUpClientInvestmentAccount.setWorkflowState(state);
			this.investmentAccountService.saveInvestmentAccount(lookedUpClientInvestmentAccount);
		}
		finally {
			switchToAdminUser();
		}
	}


	private void transitionInvestmentAccount(String transitionName, InvestmentAccount clientAccount) {
		String tableName = "InvestmentAccount";
		this.workflowTransitioner.transitionIfPresent(transitionName, tableName, BeanUtils.getIdentityAsLong(clientAccount));
	}
}
