package com.clifton.ims.tests.trade.order;

/**
 * Keys used within the TradeOrderFixSimTests and TradOrderFixSimTestHelper to identify FixMessage template categories for dynamically replacing fix tag data within message
 * templates that match the category.
 *
 * @author davidi
 */
public enum ReplacementMapKeys {
	GLOBAL,
	ORDER,
	ORDER_EXECUTION,
	EXECUTION_REPORT,
	ALLOC_ALL,
	ALLOC_INSTRUCTION,
	ALLOC_ACK,
	ALLOC_REPORT
}
