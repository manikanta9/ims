package com.clifton.ims.tests.investment.manager;


import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>InvestmentManagerAccountUtils</code> provides convenience methods for creating manager accounts, account balances, etc.
 *
 * @author jgommels
 */
@Component
public class InvestmentManagerAccountUtils {

	private final InvestmentManagerAccountService investmentManagerAccountService;
	private final InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private SystemBeanService systemBeanService;


	@Inject
	public InvestmentManagerAccountUtils(InvestmentManagerAccountService investmentManagerAccountService, InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public void saveInvestmentManagerAccount(InvestmentManagerAccount investmentManagerAccount) {
		this.investmentManagerAccountService.saveInvestmentManagerAccount(investmentManagerAccount);
	}


	public void deleteInvestmentManagerAccount(int id) {
		this.investmentManagerAccountService.deleteInvestmentManagerAccount(id);
	}


	public InvestmentManagerAccountBalance createInvestmentManagerAccountBalance(InvestmentManagerAccount managerAccount, Date balanceDate, BigDecimal cashValue, BigDecimal securitiesValue) {
		InvestmentManagerAccountBalance investmentManagerAccountBalance = new InvestmentManagerAccountBalance();
		investmentManagerAccountBalance.setManagerAccount(managerAccount);
		investmentManagerAccountBalance.setBalanceDate(balanceDate);
		investmentManagerAccountBalance.setCashValue(cashValue);
		investmentManagerAccountBalance.setSecuritiesValue(securitiesValue);
		return saveInvestmentManagerAccountBalance(investmentManagerAccountBalance);
	}


	public InvestmentManagerAccountBalance saveInvestmentManagerAccountBalance(InvestmentManagerAccountBalance investmentManagerAccountBalance) {
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(investmentManagerAccountBalance);
		return this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(investmentManagerAccountBalance.getId());
	}


	public void deleteInvestmentManagerAccountBalance(int id) {
		this.investmentManagerAccountBalanceService.deleteInvestmentManagerAccountBalance(id);
	}


	public InvestmentManagerAccountAllocation createInvestmentManagerAccountAllocation(InvestmentAssetClass assetClass, BigDecimal allocationPercent) {
		InvestmentManagerAccountAllocation investmentManagerAccountAllocation = new InvestmentManagerAccountAllocation();
		investmentManagerAccountAllocation.setAllocationPercent(allocationPercent);
		investmentManagerAccountAllocation.setAssetClass(assetClass);
		return investmentManagerAccountAllocation;
	}


	public InvestmentManagerAccountAssignment createInvestmentManagerAccountAssignment(InvestmentManagerAccount managerAccount, InvestmentAccount clientAccount, List<InvestmentManagerAccountAllocation> allocationList) {
		InvestmentManagerAccountAssignment investmentManagerAccountAssignment = new InvestmentManagerAccountAssignment();
		investmentManagerAccountAssignment.setReferenceOne(managerAccount);
		investmentManagerAccountAssignment.setReferenceTwo(clientAccount);
		investmentManagerAccountAssignment.setAllocationList(allocationList);
		this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(investmentManagerAccountAssignment);
		return this.investmentManagerAccountService.getInvestmentManagerAccountAssignment(investmentManagerAccountAssignment.getId());
	}
}
