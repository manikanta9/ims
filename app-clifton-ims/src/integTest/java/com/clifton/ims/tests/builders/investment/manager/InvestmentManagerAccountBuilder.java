package com.clifton.ims.tests.builders.investment.manager;


import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAdjustmentM2M;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.LinkedManagerTypes;
import com.clifton.investment.manager.custodian.InvestmentManagerCustodianAccount;
import com.clifton.investment.rates.InvestmentInterestRateIndex;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


@SuppressWarnings("hiding")
public class InvestmentManagerAccountBuilder {

	private final InvestmentManagerAccountService investmentManagerAccountService;

	private BusinessClient client;
	private BusinessCompany managerCompany;
	private String accountNumber;
	private String accountName;
	private InvestmentSecurity baseCurrency;
	private InvestmentManagerCustodianAccount managerCustodianAccount;
	private boolean zeroBalanceIfNoDownload;
	private InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType;
	private BigDecimal customCashValue;
	private Date customCashValueUpdateDate;
	private InvestmentManagerAccount cashPercentManager;
	private List<InvestmentManagerAccountAdjustmentM2M> adjustmentM2MList;
	private boolean inactive;
	private InvestmentManagerAccount.ManagerTypes managerType;
	private LinkedManagerTypes linkedManagerType;
	private InvestmentAccount linkedInvestmentAccount;
	private InvestmentSecurity benchmarkSecurity;
	private boolean proxyValueGrowthInverted;
	private BigDecimal proxyValue;
	private Date proxyValueDate;
	private InvestmentSecurity proxyBenchmarkSecurity;
	private String proxyBenchmarkSecurityDisplayName;
	private BigDecimal proxySecurityQuantity;
	private InvestmentInterestRateIndex proxyBenchmarkRateIndex;
	private boolean proxySecuritiesOnly;
	private boolean proxyToLastKnownPrice;
	List<InvestmentManagerAccountRollup> rollupManagerList;


	private InvestmentManagerAccountBuilder(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public static InvestmentManagerAccountBuilder investmentManagerAccount(InvestmentManagerAccountService investmentManagerAccountService) {
		return new InvestmentManagerAccountBuilder(investmentManagerAccountService);
	}


	public InvestmentManagerAccount build() {
		InvestmentManagerAccount investmentManagerAccount = new InvestmentManagerAccount();
		investmentManagerAccount.setAccountName(this.accountName);
		investmentManagerAccount.setAccountNumber(this.accountNumber);
		investmentManagerAccount.setAdjustmentM2MList(this.adjustmentM2MList);
		investmentManagerAccount.setBenchmarkSecurity(this.benchmarkSecurity);
		investmentManagerAccount.setCashAdjustmentType(this.cashAdjustmentType);
		investmentManagerAccount.setCashPercentManager(this.cashPercentManager);
		investmentManagerAccount.setClient(this.client);
		investmentManagerAccount.setBaseCurrency(this.baseCurrency);
		investmentManagerAccount.setCustodianAccount(this.managerCustodianAccount);
		investmentManagerAccount.setCustomCashValue(this.customCashValue);
		investmentManagerAccount.setCustomCashValueUpdateDate(this.customCashValueUpdateDate);
		investmentManagerAccount.setInactive(this.inactive);
		investmentManagerAccount.setLinkedInvestmentAccount(this.linkedInvestmentAccount);
		investmentManagerAccount.setLinkedManagerType(this.linkedManagerType);
		investmentManagerAccount.setManagerCompany(this.managerCompany);
		investmentManagerAccount.setManagerType(this.managerType);
		investmentManagerAccount.setProxyBenchmarkRateIndex(this.proxyBenchmarkRateIndex);
		investmentManagerAccount.setProxyBenchmarkSecurity(this.proxyBenchmarkSecurity);
		investmentManagerAccount.setProxyBenchmarkSecurityDisplayName(this.proxyBenchmarkSecurityDisplayName);
		investmentManagerAccount.setProxySecuritiesOnly(this.proxySecuritiesOnly);
		investmentManagerAccount.setProxySecurityQuantity(this.proxySecurityQuantity);
		investmentManagerAccount.setProxyToLastKnownPrice(this.proxyToLastKnownPrice);
		investmentManagerAccount.setProxyValue(this.proxyValue);
		investmentManagerAccount.setProxyValueDate(this.proxyValueDate);
		investmentManagerAccount.setProxyValueGrowthInverted(this.proxyValueGrowthInverted);
		investmentManagerAccount.setRollupManagerList(this.rollupManagerList);
		investmentManagerAccount.setZeroBalanceIfNoDownload(this.zeroBalanceIfNoDownload);
		return investmentManagerAccount;
	}


	public InvestmentManagerAccount buildAndSave() {
		InvestmentManagerAccount investmentManagerAccount = build();
		this.investmentManagerAccountService.saveInvestmentManagerAccount(investmentManagerAccount);
		//refresh it to ensure proper loading of all properties.
		investmentManagerAccount = this.investmentManagerAccountService.getInvestmentManagerAccount(investmentManagerAccount.getId());
		return investmentManagerAccount;
	}


	public InvestmentManagerAccountBuilder but() {
		return investmentManagerAccount(this.investmentManagerAccountService)
				.withAccountName(this.accountName)
				.withAccountNumber(this.accountNumber)
				.withAdjustmentM2MList(this.adjustmentM2MList)
				.withBenchmarkSecurity(this.benchmarkSecurity)
				.withCashAdjustmentType(this.cashAdjustmentType)
				.withCashPercentManager(this.cashPercentManager)
				.withClient(this.client)
				.withBaseCurrency(this.baseCurrency)
				.withCustodianAccount(this.managerCustodianAccount)
				.withCustomCashValue(this.customCashValue)
				.withCustomCashValueUpdateDate(this.customCashValueUpdateDate)
				.withInactive(this.inactive)
				.withLinkedInvestmentAccount(this.linkedInvestmentAccount)
				.withLinkedManagerType(this.linkedManagerType)
				.withManagerCompany(this.managerCompany)
				.withManagerType(this.managerType)
				.withProxyBenchmarkRateIndex(this.proxyBenchmarkRateIndex)
				.withProxyBenchmarkSecurity(this.proxyBenchmarkSecurity)
				.withProxyBenchmarkSecurityDisplayName(this.proxyBenchmarkSecurityDisplayName)
				.withProxySecuritiesOnly(this.proxySecuritiesOnly)
				.withProxySecurityQuantity(this.proxySecurityQuantity)
				.withProxyToLastKnownPrice(this.proxyToLastKnownPrice)
				.withProxyValue(this.proxyValue)
				.withProxyValueDate(this.proxyValueDate)
				.withProxyValueGrowthInverted(this.proxyValueGrowthInverted)
				.withRollupManagerList(this.rollupManagerList)
				.withZeroBalanceIfNoDownload(this.zeroBalanceIfNoDownload);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBuilder withClient(BusinessClient client) {
		this.client = client;
		return this;
	}


	public InvestmentManagerAccountBuilder withManagerCompany(BusinessCompany managerCompany) {
		this.managerCompany = managerCompany;
		return this;
	}


	public InvestmentManagerAccountBuilder withAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
		return this;
	}


	public InvestmentManagerAccountBuilder withAccountName(String accountName) {
		this.accountName = accountName;
		return this;
	}


	public InvestmentManagerAccountBuilder withBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
		return this;
	}


	public InvestmentManagerAccountBuilder withCustodianAccount(InvestmentManagerCustodianAccount custodianAccount) {
		this.managerCustodianAccount = custodianAccount;
		return this;
	}


	public InvestmentManagerAccountBuilder withZeroBalanceIfNoDownload(boolean zeroBalanceIfNoDownload) {
		this.zeroBalanceIfNoDownload = zeroBalanceIfNoDownload;
		return this;
	}


	public InvestmentManagerAccountBuilder withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType cashAdjustmentType) {
		this.cashAdjustmentType = cashAdjustmentType;
		return this;
	}


	public InvestmentManagerAccountBuilder withCustomCashValue(BigDecimal customCashValue) {
		this.customCashValue = customCashValue;
		return this;
	}


	public InvestmentManagerAccountBuilder withCustomCashValueUpdateDate(Date customCashValueUpdateDate) {
		this.customCashValueUpdateDate = customCashValueUpdateDate;
		return this;
	}


	public InvestmentManagerAccountBuilder withCashPercentManager(InvestmentManagerAccount cashPercentManager) {
		this.cashPercentManager = cashPercentManager;
		return this;
	}


	public InvestmentManagerAccountBuilder withAdjustmentM2MList(List<InvestmentManagerAccountAdjustmentM2M> adjustmentM2MList) {
		this.adjustmentM2MList = adjustmentM2MList;
		return this;
	}


	public InvestmentManagerAccountBuilder withInactive(boolean inactive) {
		this.inactive = inactive;
		return this;
	}


	public InvestmentManagerAccountBuilder withManagerType(InvestmentManagerAccount.ManagerTypes managerType) {
		this.managerType = managerType;
		return this;
	}


	public InvestmentManagerAccountBuilder withLinkedManagerType(LinkedManagerTypes linkedManagerType) {
		this.linkedManagerType = linkedManagerType;
		return this;
	}


	public InvestmentManagerAccountBuilder withLinkedInvestmentAccount(InvestmentAccount linkedInvestmentAccount) {
		this.linkedInvestmentAccount = linkedInvestmentAccount;
		return this;
	}


	public InvestmentManagerAccountBuilder withBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyValueGrowthInverted(boolean proxyValueGrowthInverted) {
		this.proxyValueGrowthInverted = proxyValueGrowthInverted;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyValue(BigDecimal proxyValue) {
		this.proxyValue = proxyValue;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyValueDate(Date proxyValueDate) {
		this.proxyValueDate = proxyValueDate;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyBenchmarkSecurity(InvestmentSecurity proxyBenchmarkSecurity) {
		this.proxyBenchmarkSecurity = proxyBenchmarkSecurity;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyBenchmarkSecurityDisplayName(String proxyBenchmarkSecurityDisplayName) {
		this.proxyBenchmarkSecurityDisplayName = proxyBenchmarkSecurityDisplayName;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxySecurityQuantity(BigDecimal proxySecurityQuantity) {
		this.proxySecurityQuantity = proxySecurityQuantity;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyBenchmarkRateIndex(InvestmentInterestRateIndex proxyBenchmarkRateIndex) {
		this.proxyBenchmarkRateIndex = proxyBenchmarkRateIndex;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxySecuritiesOnly(boolean proxySecuritiesOnly) {
		this.proxySecuritiesOnly = proxySecuritiesOnly;
		return this;
	}


	public InvestmentManagerAccountBuilder withProxyToLastKnownPrice(boolean proxyToLastKnownPrice) {
		this.proxyToLastKnownPrice = proxyToLastKnownPrice;
		return this;
	}


	public InvestmentManagerAccountBuilder withRollupManagerList(List<InvestmentManagerAccountRollup> rollupManagerList) {
		this.rollupManagerList = rollupManagerList;
		return this;
	}
}
