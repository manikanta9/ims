package com.clifton.ims.tests.trade.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


public class TradeSearchFormTests extends BaseImsIntegrationTest {

	@Resource
	private TradeService tradeService;


	@Test
	public void testFilterByCustodian() {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setCustodianCompanyId(4079); //JP Morgan Chase
		searchForm.setTradeDate(DateUtils.toDate("8/7/2014"));
		List<Trade> trades = this.tradeService.getTradeList(searchForm);

		trades = BeanUtils.sortWithFunction(trades, Trade::getId, true);

		Assertions.assertEquals(12, trades.size());
		Assertions.assertEquals(trades.get(0).getId().intValue(), 380240);
		Assertions.assertEquals(trades.get(1).getId().intValue(), 380241);
		Assertions.assertEquals(trades.get(2).getId().intValue(), 380245);
	}
}
