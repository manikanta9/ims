package com.clifton.ims.tests.spring;

import com.clifton.test.spring.BeanFactoryWatchingApplicationContext;
import org.junit.jupiter.api.BeforeEach;


/**
 * The {@code BaseImsBeanFactoryTests} type is a base test class for testing against a partially-prepared application context.
 * <p>
 * Sub-types of this class will be provided with an application context pre-populated with its primary beans and bean definitions. This context can be used to validate details of
 * generated beans such as properties and instantiation orders.
 *
 * @author MikeH
 */
public abstract class BaseImsBeanFactoryTests {

	private static final String CONTEXT_RESOURCE_PATH = "classpath:" + BaseImsBeanFactoryTests.class.getName().replaceAll("\\.", "/") + "-context.xml";

	private BeanFactoryWatchingApplicationContext context;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void initializeTestContext() {
		// Initialize context
		setContext(new BeanFactoryWatchingApplicationContext(CONTEXT_RESOURCE_PATH));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BeanFactoryWatchingApplicationContext getContext() {
		return this.context;
	}


	public void setContext(BeanFactoryWatchingApplicationContext context) {
		this.context = context;
	}
}
