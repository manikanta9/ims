package com.clifton.ims.tests.trade.file.upload;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.file.upload.TradeUploadCommand;
import com.clifton.trade.file.upload.TradeUploadService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;
import java.util.UUID;


/**
 * @author manderson
 */
public class TradeUploadTests extends BaseImsIntegrationTest {

	@Resource
	private TradeService tradeService;

	@Resource
	private TradeGroupService tradeGroupService;

	@Resource
	private TradeUploadService tradeUploadService;


	@Test
	public void testUploadSimple_WrongTradeType() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		// Security is a Cleared Credit Security - Should NOT be able to upload as a CDS Trade Type
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("XY2107D18U0500XXI");

		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), accountInfo.getHoldingAccount().getNumber(), security.getSymbol(), 5300000, 104.9906192, null, true, null, null, null);

		// Create DataTable for Upload File
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(dataTable, values);
		dataTable.addRow(row);

		TradeUploadCommand upload = new TradeUploadCommand();
		upload.setSimpleTradeType(this.tradeUtils.getTradeType("Swaps: CDS"));
		upload.setSimpleTradeDestination(this.tradeUtils.getManualTradeDestination());
		upload.setSimple(true);
		upload.setSimpleExecutingBrokerCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL));
		TradeGroup group = new TradeGroup();
		group.setTraderUser(this.userUtils.getCurrentUser());
		group.setTradeDate(DateUtils.toDate("03/19/2018"));
		upload.setTradeGroup(group);
		upload.setSimpleSettlementDate(DateUtils.toDate("03/19/2018"));

		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		DataTableFileConfig fileConfig = new DataTableFileConfig(dataTable);
		upload.setFile(new MultipartFileImpl(converter.convert(fileConfig)));

		String errorMessage = null;
		try {
			this.tradeUploadService.uploadTradeUploadFile(upload);
		}
		catch (Exception e) {
			if (e instanceof ImsErrorResponseException) {
				errorMessage = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			if (errorMessage == null) {
				errorMessage = e.getMessage();
			}
			if (errorMessage != null && errorMessage.startsWith("Unexpected Error Occurred: [")) {
				errorMessage = errorMessage.substring(28, errorMessage.length() - 2);
			}
		}
		String expectedError = "Incorrect Trade Type [Swaps: CDS]. Expected Trade type of [Cleared: CDS] in order to create trade for security [" + security.getLabel() + "].";
		if (StringUtils.isEmpty(errorMessage)) {
			Assertions.fail("Expected exception with message [" + expectedError + "] to be thrown during the trade upload, but no Validation Exception was thrown.");
		}
		Assertions.assertEquals(expectedError, errorMessage);
	}


	@Test
	public void testUploadSimple_ClearedCDS_OriginalFace() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		BigDecimal originalFace = BigDecimal.valueOf(27100000);
		Trade trade = executeSingleTradeUpload(accountInfo, "TX2805D22E0500XXI", null, originalFace.intValue(), 102.995296, null, "08/28/2020", "09/30/2020");
		validateAndCancelTrade(trade, originalFace, BigDecimal.valueOf(0.94666), BigDecimal.valueOf(25654486));
	}


	@Test
	public void testUploadSimple_ClearedCDS_QuantityIntended() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		BigDecimal quantityIntended = BigDecimal.valueOf(27100000);
		Trade trade = executeSingleTradeUpload(accountInfo, "TX2805D22E0500XXI", quantityIntended.intValue(), null, 102.995296, null, "08/28/2020", "09/30/2020");
		validateAndCancelTrade(trade, BigDecimal.valueOf(28626962.16), BigDecimal.valueOf(0.94666), quantityIntended);
	}


	@Test
	public void testUploadSimple_ClearedCDS_OriginalFace_OverrideCurrentFactor() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		BigDecimal originalFace = BigDecimal.valueOf(27100000);
		BigDecimal currentFactor = BigDecimal.valueOf(0.9);
		Trade trade = executeSingleTradeUpload(accountInfo, "TX2805D22E0500XXI", null, originalFace.intValue(), 102.995296, currentFactor, "08/28/2020", "09/30/2020");
		validateAndCancelTrade(trade, originalFace, currentFactor, BigDecimal.valueOf(24390000));
	}


	@Test
	public void testUploadSimple_ClearedCDS_QuantityIntended_OverrideCurrentFactor() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		BigDecimal quantityIntended = BigDecimal.valueOf(27100000);
		BigDecimal currentFactor = BigDecimal.valueOf(0.9);
		Trade trade = executeSingleTradeUpload(accountInfo, "TX2805D22E0500XXI", quantityIntended.intValue(), null, 102.995296, currentFactor, "08/28/2020", "09/30/2020");
		validateAndCancelTrade(trade, BigDecimal.valueOf(30111111.11), currentFactor, quantityIntended);
	}


	@Test
	public void testUploadSimple_SettlementCurrency() {
		// Test 1: No Default Settlement CCY (Should be USD)
		testSettlementCurrency("USD", null, null);
		// Test 2: Simple Settlement CCY = EUR
		testSettlementCurrency("EUR", "EUR", null);
		// Test 3: File Symbol = EUR
		testSettlementCurrency("EUR", null, "EUR");
		// Test 4: File Symbol = EUR, Simple = USD
		testSettlementCurrency("EUR", "USD", "EUR");
	}


	@Test
	public void testRollTradeUpload() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		Date tradeDate = DateUtils.toDate("11/20/2019");
		InvestmentSecurity llf0 = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly("LLF20");

		// default non-roll trade in Trade Import group type
		testFuturesTradeImport(accountInfo, llf0, tradeDate, null);
		// non-roll trade in specified Trade Import group type
		testFuturesTradeImport(accountInfo, llf0, tradeDate, this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.TRADE_IMPORT.getTypeName()));
		// roll trade in specified Roll Trade Import group type
		testFuturesTradeImport(accountInfo, llf0, tradeDate, this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.ROLL_TRADE_IMPORT.getTypeName()));
		// non-roll trade in default Trade Import group type
		testFuturesTradeImport(accountInfo, llf0, tradeDate, Boolean.FALSE, null);
		// roll trade in system created Roll Trade Import group type
		testFuturesTradeImport(accountInfo, llf0, tradeDate, Boolean.TRUE, null);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private Trade executeSingleTradeUpload(AccountInfo accountInfo, String securitySymbol, Integer quantityIntended, Integer originalFace, double averageUnitPrice, BigDecimal currentFactor, String tradeDateString, String settlementDateString) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(securitySymbol);

		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), accountInfo.getHoldingAccount().getNumber(), security.getSymbol(), quantityIntended, averageUnitPrice, currentFactor, false, "USD", null, originalFace);

		// Create DataTable for Upload File
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(dataTable, values);
		dataTable.addRow(row);

		TradeGroup tradeGroup = uploadTrade(tradeDateString, settlementDateString, dataTable);
		return lookupTrade(accountInfo, security, tradeGroup);
	}


	private TradeGroup uploadTrade(String tradeDateString, String settlementDateString, DataTable dataTable) {
		TradeUploadCommand upload = new TradeUploadCommand();
		upload.setSimpleTradeType(this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		upload.setSimpleTradeDestination(this.tradeUtils.getManualTradeDestination());
		upload.setSimple(true);
		upload.setSimpleExecutingBrokerCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL));

		TradeGroup group = new TradeGroup();
		group.setTraderUser(this.userUtils.getCurrentUser());
		group.setTradeDate(DateUtils.toDate(tradeDateString));
		upload.setTradeGroup(group);
		upload.setSimpleSettlementDate(DateUtils.toDate(settlementDateString));

		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		DataTableFileConfig fileConfig = new DataTableFileConfig(dataTable);
		upload.setFile(new MultipartFileImpl(converter.convert(fileConfig)));
		this.tradeUploadService.uploadTradeUploadFile(upload);

		return group;
	}


	private Trade lookupTrade(AccountInfo accountInfo, InvestmentSecurity investmentSecurity, TradeGroup tradeGroup) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setClientInvestmentAccountId(accountInfo.getClientAccount().getId());
		searchForm.setSecurityId(investmentSecurity.getId());
		searchForm.setTradeDate(tradeGroup.getTradeDate());
		searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
		if (tradeGroup.getTradeGroupType() != null) {
			searchForm.setTradeGroupTypeId(tradeGroup.getTradeGroupType().getId());
		}
		searchForm.setOrderBy("createDate:desc");
		return CollectionUtils.getFirstElement(this.tradeService.getTradeList(searchForm));
	}


	private void validateAndCancelTrade(Trade trade, BigDecimal expectedOriginalFace, BigDecimal expectedFactor, BigDecimal expectedQuantity) {
		Assertions.assertTrue(MathUtils.isEqual(trade.getOriginalFace(), expectedOriginalFace), String.format("Expected Original Face to be %s but was %s", expectedOriginalFace.toPlainString(), trade.getOriginalFace().toPlainString()));
		Assertions.assertTrue(MathUtils.isEqual(trade.getCurrentFactor(), expectedFactor), String.format("Expected Current Factor to be %s but was %s", expectedFactor.toPlainString(), trade.getCurrentFactor().toPlainString()));
		Assertions.assertTrue(MathUtils.isEqual(trade.getQuantityIntended(), expectedQuantity), String.format("Expected Quantity Intended to be %s but was %s", expectedQuantity.toPlainString(), trade.getQuantityIntended().toPlainString()));
		this.tradeUtils.cancelTrade(trade);
	}


	private void testFuturesTradeImport(AccountInfo accountInfo, InvestmentSecurity security, Date tradeDate, TradeGroupType tradeGroupType) {
		testFuturesTradeImport(accountInfo, security, tradeDate, null, tradeGroupType);
	}


	private void testFuturesTradeImport(AccountInfo accountInfo, InvestmentSecurity security, Date tradeDate, Boolean rollTrade, TradeGroupType tradeGroupType) {
		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), accountInfo.getHoldingAccount().getNumber(), security.getSymbol(), 1, 1, null, true, null, rollTrade, null);

		// Create DataTable for Upload File
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(dataTable, values);
		dataTable.addRow(row);

		TradeUploadCommand upload = new TradeUploadCommand();
		upload.setSimpleTradeType(this.tradeUtils.getTradeType("Futures"));
		upload.setSimpleTradeDestination(this.tradeUtils.getManualTradeDestination());
		upload.setSimple(true);
		upload.setSimpleExecutingBrokerCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO));

		TradeGroup group = new TradeGroup();
		group.setTraderUser(this.userUtils.getCurrentUser());
		group.setTradeDate(tradeDate);
		if (tradeGroupType != null) {
			group.setTradeGroupType(tradeGroupType);
		}
		upload.setTradeGroup(group);

		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		DataTableFileConfig fileConfig = new DataTableFileConfig(dataTable);
		upload.setFile(new MultipartFileImpl(converter.convert(fileConfig)));
		this.tradeUploadService.uploadTradeUploadFile(upload);

		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setClientInvestmentAccountId(accountInfo.getClientAccount().getId());
		searchForm.setSecurityId(security.getId());
		searchForm.setTradeDate(tradeDate);
		searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
		if (tradeGroupType != null) {
			searchForm.setTradeGroupTypeId(tradeGroupType.getId());
		}
		searchForm.setOrderBy("createDate:desc");
		// Should only be one
		Trade trade = CollectionUtils.getFirstElement(this.tradeService.getTradeList(searchForm));
		Assertions.assertNotNull(trade, "Expected the upload to produce a single trade");
		boolean expectRollTrade = ((rollTrade == null) ? false : rollTrade) || (tradeGroupType == null ? false : tradeGroupType.isRoll());
		Assertions.assertEquals(expectRollTrade, trade.isRoll(), "The created trade from upload does not match the expected roll value");
		this.tradeUtils.cancelTrade(trade);
	}


	private void testSettlementCurrency(String expectedCcySymbol, String simpleCcySymbol, String fileCcySymbol) {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.JP_MORGAN_CHASE_BANK_NA, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, this.tradeUtils.getTradeType(TradeType.TOTAL_RETURN_SWAPS));
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("A2328931", "A2328931-" + UUID.randomUUID());
		security.setBusinessContract(accountInfo.getHoldingAccount().getBusinessContract());
		this.investmentInstrumentService.saveInvestmentSecurity(security);

		Object[] values = populateTradeData(accountInfo.getClientAccount().getNumber(), accountInfo.getHoldingAccount().getNumber(), security.getSymbol(), 100000, 119, null, true, fileCcySymbol, null, null);

		// Create DataTable for Upload File
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(dataTable, values);
		dataTable.addRow(row);

		TradeUploadCommand upload = new TradeUploadCommand();
		upload.setSimpleTradeType(this.tradeUtils.getTradeType("Swaps: TRS"));
		upload.setSimpleTradeDestination(this.tradeUtils.getManualTradeDestination());
		upload.setSimple(true);
		upload.setSimpleExecutingBrokerCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.JP_MORGAN_CHASE_BANK_NA));
		if (simpleCcySymbol != null) {
			upload.setSimplePayingSecurity(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(simpleCcySymbol, true));
		}
		TradeGroup group = new TradeGroup();
		group.setTraderUser(this.userUtils.getCurrentUser());
		group.setTradeDate(DateUtils.toDate("05/1/2019"));
		upload.setTradeGroup(group);

		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		DataTableFileConfig fileConfig = new DataTableFileConfig(dataTable);
		upload.setFile(new MultipartFileImpl(converter.convert(fileConfig)));
		this.tradeUploadService.uploadTradeUploadFile(upload);

		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setClientInvestmentAccountId(accountInfo.getClientAccount().getId());
		searchForm.setSecurityId(security.getId());
		// Should only be one
		Trade trade = CollectionUtils.getFirstElement(this.tradeService.getTradeList(searchForm));
		Assertions.assertNotNull(trade);
		Assertions.assertEquals(expectedCcySymbol, trade.getPayingSecurity().getSymbol());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private Object[] populateTradeData(String clientAccountNumber, String holdingAccountNumber, String securitySymbol, Integer quantity, double averageUnitPrice, BigDecimal currentFactor, boolean buy, String settlementCurrencySymbol, Boolean rollTrade, Integer originalFace) {
		Object[] values = new Object[DATA_COLUMN_LIST.length];
		values[0] = clientAccountNumber;
		values[1] = holdingAccountNumber;
		values[2] = securitySymbol;
		values[3] = quantity;
		values[4] = averageUnitPrice;
		values[5] = (buy ? "B" : "S");
		values[6] = settlementCurrencySymbol;
		if (rollTrade != null) {
			values[7] = rollTrade;
		}
		values[8] = originalFace;
		values[9] = currentFactor;
		return values;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private static final DataColumn[] DATA_COLUMN_LIST = new DataColumn[10];


	static {
		// Basic Trade Info
		DATA_COLUMN_LIST[0] = new DataColumnImpl("ClientInvestmentAccount-Number", Types.NVARCHAR);
		DATA_COLUMN_LIST[1] = new DataColumnImpl("HoldingInvestmentAccount-Number", Types.NVARCHAR);
		DATA_COLUMN_LIST[2] = new DataColumnImpl("InvestmentSecurity-Symbol", Types.NVARCHAR);
		DATA_COLUMN_LIST[3] = new DataColumnImpl("QuantityIntended", Types.DECIMAL);
		DATA_COLUMN_LIST[4] = new DataColumnImpl("AverageUnitPrice", Types.DECIMAL);
		DATA_COLUMN_LIST[5] = new DataColumnImpl("Buy", Types.NVARCHAR);
		DATA_COLUMN_LIST[6] = new DataColumnImpl("PayingSecurity-Symbol", Types.NVARCHAR);
		DATA_COLUMN_LIST[7] = new DataColumnImpl("Roll", Types.BIT);
		DATA_COLUMN_LIST[8] = new DataColumnImpl("OriginalFace", Types.DECIMAL);
		DATA_COLUMN_LIST[9] = new DataColumnImpl("CurrentFactor", Types.DECIMAL);
	}
}
