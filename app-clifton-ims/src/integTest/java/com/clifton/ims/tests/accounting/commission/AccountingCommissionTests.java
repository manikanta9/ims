package com.clifton.ims.tests.accounting.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.commission.AccountingCommissionApplyMethods;
import com.clifton.accounting.commission.AccountingCommissionDefinition;
import com.clifton.accounting.commission.AccountingCommissionService;
import com.clifton.accounting.commission.AccountingCommissionTier;
import com.clifton.accounting.commission.AccountingCommissionTypes;
import com.clifton.accounting.commission.calculation.AccountingCommissionCalculationMethods;
import com.clifton.accounting.commission.search.AccountingCommissionDefinitionSearchForm;
import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.accounting.gl.journal.AccountingJournal;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionType;
import com.clifton.accounting.gl.journal.transactiontype.AccountingTransactionTypeService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingJournalDetailBuilder;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.accounting.GeneralLedgerDescriptions;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.AccountsCreationCommand;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.test.util.ConcurrentExecutionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeType;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;


public class AccountingCommissionTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingAccountService accountingAccountService;

	@Resource
	private AccountingCommissionService accountingCommissionService;

	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	@Resource
	private AccountingEventJournalService accountingEventJournalService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private TradeGroupService tradeGroupService;

	@Resource
	private AccountingTransactionTypeService accountingTransactionTypeService;


	@Test
	public void testRoundTurnCommissionForFutures() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a buy trade
		BigDecimal buyQuantity = new BigDecimal("20");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);

		// Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(buyQuantity)
				.opening(true)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.POSITION_OPENING, cch14)
				.build());

		// Validate the buy trade
		AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);

		// Create and execute a sell trade
		BigDecimal sellQuantity = new BigDecimal("10");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		BigDecimal expectedExecutionCommission = new BigDecimal("3"); //1.5 * 2 = 3
		BigDecimal expectedClearingCommission = new BigDecimal("6.3"); // 3.15 * 2 = 6.3
		BigDecimal expectedCommissionDebit = new BigDecimal("93"); // 10 * (3 + 6.3) = 93

		detailBuilder = AccountingJournalDetailBuilder.builder(sell);

		// Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingCommission)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(sellQuantity.multiply(expectedClearingCommission))
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutionCommission)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(sellQuantity.multiply(expectedExecutionCommission))
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(expectedCommissionDebit.negate())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, cch14)
				.build());

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
	}


	@Test
	public void testRoundTurnCommissionWithBrokersChangeForFutures() {
		InvestmentSecurity edm2 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("S X4");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		// Create and execute a buy trade
		BusinessCompany executingBrokerForBuy = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		BigDecimal buyQuantity = new BigDecimal("600");
		Date buyDate = DateUtils.toDate("12/2/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(edm2, buyQuantity, true, buyDate, executingBrokerForBuy, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		// Validate the journal details from the buy
		List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);

		List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);

		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(buyQuantity)
				.opening(true)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.POSITION_OPENING, edm2)
				.build());

		// Validate the buy trade
		AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);

		// Create and execute a sell trade
		BusinessCompany executingBrokerForSell = this.businessUtils.getBusinessCompany(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC);
		BigDecimal sellQuantity = new BigDecimal("600");
		Date sellDate = DateUtils.toDate("12/3/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(edm2, sellQuantity, false, sellDate, executingBrokerForSell, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		// Validate the journal details from the sell
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		/*
		 * The buy and sell will use round-turn CommissionDefinitions
		 */
		BigDecimal expectedCommissionPrice = new BigDecimal("6.7"); // (0.93 + 2.42) * 2 = 6.7
		BigDecimal expectedExecutionCommission = new BigDecimal("1.86"); // 0.93 * 2 = 1.86
		BigDecimal expectedClearingCommission = new BigDecimal("4.84"); // 2.42 * 2 = 2.42

		BigDecimal expectedCommissionDebit = MathUtils.multiply(expectedCommissionPrice, new BigDecimal(600));
		List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();

		detailBuilder = AccountingJournalDetailBuilder.builder(sell);
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.FULL_POSITION_CLOSE, edm2)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingCommission)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(expectedClearingCommission.multiply(sellQuantity))
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, edm2)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutionCommission)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(expectedExecutionCommission.multiply(sellQuantity))
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, edm2)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(expectedCommissionDebit.negate())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, edm2)
				.build());

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
	}


	@Test
	public void testRoundTurnWithClosingMultipleLots() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a buy trade
		BigDecimal buy1Quantity = new BigDecimal("10");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy1 = this.tradeUtils.newFuturesTradeBuilder(cch14, buy1Quantity, true, buyDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy1);

		this.accountingUtils.assertPositionOpen(buy1);

		BigDecimal buy2Quantity = new BigDecimal("15");
		Trade buy2 = this.tradeUtils.newFuturesTradeBuilder(cch14, buy2Quantity, true, DateUtils.toDate("12/17/2013"), executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy2);
		this.accountingUtils.assertPositionOpen(buy2);

		BigDecimal sellQuantity = new BigDecimal("30");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, DateUtils.toDate("12/18/2013"), executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		List<TradeFill> fills = this.tradeUtils.getTradeFills(sell);
		Assertions.assertTrue(MathUtils.isEqual(sell.getQuantityIntended(), fills.stream()
				.map(TradeFill::getQuantity)
				.reduce(BigDecimal.ZERO, BigDecimal::add)));

		List<? extends AccountingJournalDetailDefinition> actualSellDetails = fills.stream()
				.flatMap(f -> this.accountingUtils.getTradeFillDetails(f).stream())
				.collect(Collectors.toList());

		AccountingUtils.assertJournalDetailList(actualSellDetails, new String[]{
				"16128095	16128092	CA1592231876	HA461933090	Position	CCH14	1	-10	0	12/18/2013	1	0	-100	12/16/2013	12/18/2013	Full position close for CCH14",
				"16128096	16128093	CA1592231876	HA461933090	Position	CCH14	1	-15	0	12/18/2013	1	0	-150	12/17/2013	12/18/2013	Full position close for CCH14",
				"16128097	16128095	CA1592231876	HA461933090	Clearing Commission	CCH14	6.3	10	63	12/18/2013	1	63	0	12/16/2013	12/18/2013	Clearing Commission expense for CCH14",
				"16128098	16128095	CA1592231876	HA461933090	Executing Commission	CCH14	3	10	30	12/18/2013	1	30	0	12/16/2013	12/18/2013	Executing Commission expense for CCH14",
				"16128099	16128096	CA1592231876	HA461933090	Clearing Commission	CCH14	6.3	15	94.5	12/18/2013	1	94.5	0	12/17/2013	12/18/2013	Clearing Commission expense for CCH14",
				"16128100	16128096	CA1592231876	HA461933090	Executing Commission	CCH14	3	15	45	12/18/2013	1	45	0	12/17/2013	12/18/2013	Executing Commission expense for CCH14",
				"16128101	16128095	CA1592231876	HA461933090	Cash	USD			-93	12/18/2013	1	-93	0	12/16/2013	12/18/2013	Cash expense from close of CCH14",
				"16128102	16128096	CA1592231876	HA461933090	Cash	USD			-139.5	12/18/2013	1	-139.5	0	12/17/2013	12/18/2013	Cash expense from close of CCH14",
				"16128103	null	CA1592231876	HA461933090	Position	CCH14	1	-5	0	12/18/2013	1	0	-50	12/18/2013	12/18/2013	Position opening of CCH14",
		}, false);
	}


	@Test
	public void testMultipleLotsAndFlatFees() {
		InvestmentSecurity ubm = getUbmSecurity();
		InvestmentSecurity swissFranc = this.investmentInstrumentUtils.getCurrencyBySymbol("CHF");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		AccountingCommissionDefinitionSearchForm commissionSearchForm = new AccountingCommissionDefinitionSearchForm();
		commissionSearchForm.setInvestmentSecurityId(ubm.getId());
		commissionSearchForm.setAccountingAccountName(AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
		BigDecimal commissionAmountInFrancs = new BigDecimal("17.91");
		AccountingCommissionDefinition ubm4CommissionDefinition = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchForm));
		if (ubm4CommissionDefinition == null) {
			ubm4CommissionDefinition = new AccountingCommissionDefinition();
			ubm4CommissionDefinition.setInvestmentSecurity(ubm);
			ubm4CommissionDefinition.setExpenseAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE));
			ubm4CommissionDefinition.setTransactionType(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));
			ubm4CommissionDefinition.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
			ubm4CommissionDefinition.setCommissionType(AccountingCommissionTypes.SIMPLE);
			ubm4CommissionDefinition.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
			ubm4CommissionDefinition.setCommissionCurrency(swissFranc);
			ubm4CommissionDefinition.setCommissionAmount(commissionAmountInFrancs);
			ubm4CommissionDefinition = this.accountingCommissionService.saveAccountingCommissionDefinition(ubm4CommissionDefinition);
		}

		AccountingCommissionDefinition ubm4CommissionDefinitionClearing = createZeroCommissionDefinition(ubm, null, AccountingAccount.CLEARING_COMMISSION_EXPENSE);

		try {
			// Create and execute a buy trade to open a position
			BigDecimal positionQuantity1 = new BigDecimal("5");
			BigDecimal averageUnitPrice = new BigDecimal("125.00");
			Date buyDate = DateUtils.toDate("12/10/2013");
			Trade buy1 = this.tradeUtils.newFuturesTradeBuilder(ubm, positionQuantity1, true, buyDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy1);

			Date buyDate2 = DateUtils.toDate("12/11/2013");
			BigDecimal positionQuantity2 = new BigDecimal("5");
			Trade buy2 = this.tradeUtils.newFuturesTradeBuilder(ubm, positionQuantity2, true, buyDate2, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy2);

			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> actualBuyDetails1 = this.accountingUtils.getFirstTradeFillDetails(buy1, true);

			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(actualBuyDetails1, new String[]{
					"16128183	null	CA1066650345	HA1873846371	Position	UBM14	125	5	0	12/10/2013	1.37715	0	625,000	12/10/2013	12/10/2013	Position opening of UBM14",
					"16128184	16128183	CA1066650345	HA1873846371	Executing Commission	UBM14	2.93	5	14.65	12/10/2013	1.37715	20.18	0	12/10/2013	12/10/2013	Executing Commission expense for UBM14",
					"16128185	16128183	CA1066650345	HA1873846371	Currency	CHF			-17.91	12/10/2013	1.1268875366	-20.18	0	12/10/2013	12/10/2013	Currency expense from opening of UBM14"
			}, false);

			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> actualBuyDetails2 = this.accountingUtils.getFirstTradeFillDetails(buy2, true);

			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(actualBuyDetails2, new String[]{
					"16128362	null	CA372636347	HA1044907402	Position	UBM14	125	5	0	12/11/2013	1.37905	0	625,000	12/11/2013	12/11/2013	Position opening of UBM14",
					"16128363	16128362	CA372636347	HA1044907402	Executing Commission	UBM14	2.932	5	14.66	12/11/2013	1.37905	20.22	0	12/11/2013	12/11/2013	Executing Commission expense for UBM14",
					"16128364	16128362	CA372636347	HA1044907402	Currency	CHF			-17.91	12/11/2013	1.1289230075	-20.22	0	12/11/2013	12/11/2013	Currency expense from opening of UBM14"
			}, false);


			// Create and execute a sell trade to close the position
			Date sellDate = DateUtils.toDate("12/12/2013");
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(ubm, MathUtils.add(positionQuantity1, positionQuantity2), false, sellDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);

			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(actualSellDetails, new String[]{
					"16128541	16128535	CA586296745	HA1528215680	Position	UBM14	125	-5	0	12/12/2013	1.37715	0	-625,000	12/10/2013	12/12/2013	Full position close for UBM14",
					"16128542	16128538	CA586296745	HA1528215680	Position	UBM14	125	-5	0	12/12/2013	1.37905	0	-625,000	12/11/2013	12/12/2013	Full position close for UBM14",
					"16128543	16128541	CA586296745	HA1528215680	Executing Commission	UBM14	1.464	5	7.32	12/12/2013	1.37535	10.07	0	12/10/2013	12/12/2013	Executing Commission expense for UBM14",
					"16128544	16128541	CA586296745	HA1528215680	Currency	CHF			-8.96	12/12/2013	1.1242270939	-10.07	0	12/10/2013	12/12/2013	Currency expense from close of UBM14",
					"16128545	16128542	CA586296745	HA1528215680	Executing Commission	UBM14	1.462	5	7.31	12/12/2013	1.37535	10.05	0	12/11/2013	12/12/2013	Executing Commission expense for UBM14",
					"16128546	16128542	CA586296745	HA1528215680	Currency	CHF			-8.94	12/12/2013	1.1242270939	-10.05	0	12/11/2013	12/12/2013	Currency expense from close of UBM14"
			}, false);
		}
		//Cleanup - delete AccountingCommissionDefinition that was created for test
		finally {
			if (ubm4CommissionDefinition != null && ubm4CommissionDefinition.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(ubm4CommissionDefinition.getId());
			}
			if (ubm4CommissionDefinitionClearing != null && ubm4CommissionDefinitionClearing.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(ubm4CommissionDefinitionClearing.getId());
			}
		}
	}


	@Test
	public void testEarlyTerminationForTRS() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("478078");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(AccountsCreationCommand.ofOtcSecurityAndCustodian(swap, BusinessCompanies.NORTHERN_TRUST, this.totalReturnSwaps));
		BusinessCompany counterParty = swap.getBusinessCompany();

		// Open a position with a buy trade
		BigDecimal buyQuantity = new BigDecimal("29583");
		BigDecimal buyPrice = new BigDecimal("613.491");
		Date buyDate = DateUtils.toDate("01/31/2013");
		Trade buy = this.tradeUtils.newTotalReturnSwapTradeBuilder(swap, buyQuantity, buyPrice, true, buyDate, counterParty, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		// Validate the journal details from the buy
		List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);

		List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);

		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(buyPrice)
				.quantity(buyQuantity)
				.opening(true)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.POSITION_OPENING, swap)
				.build());

		// Validate the buy trade
		AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);


		BigDecimal sellQuantity = new BigDecimal("29583");
		BigDecimal sellPrice = new BigDecimal("574.952");
		Date sellDate = DateUtils.toDate("07/31/2013");


		// process TRS resets between buy and sell trades
		this.accountingUtils.postOtcTrsSwapEventsForDateRange(swap, accountInfo.getClientAccount(), buyDate, sellDate);


		// Cause an early termination with a sell trade
		Trade sell = this.tradeUtils.newTotalReturnSwapTradeBuilder(swap, sellQuantity, sellPrice, false, sellDate, counterParty, accountInfo)
				.withSettlementDate(DateUtils.toDate("08/05/2013")).withAccrualAmount1(new BigDecimal("-14221.80")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		// Validate the journal details from the sell
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
		detailBuilder = AccountingJournalDetailBuilder.builder(sell);
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(sellPrice)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.FULL_POSITION_CLOSE, swap)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_INTEREST_LEG))
				.price(null)
				.quantity(null)
				.opening(true)
				.localDebitCredit(new BigDecimal("14221.80"))
				.description(GeneralLedgerDescriptions.ACCRUED_INTEREST_LEG, swap)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.price(sellPrice)
				.quantity(sellQuantity)
				.opening(false)
				.localDebitCredit(new BigDecimal("-605031.52"))
				.description(GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_GAIN, swap)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_TERMINATION_FEE))
				.price(new BigDecimal("1.724856"))
				.opening(true)
				.localDebitCredit(new BigDecimal("51026.42"))
				.description(GeneralLedgerDescriptions.TERMINATION_FEE, swap)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.holdingInvestmentAccount(accountInfo.getCustodianAccount())
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("539783.30"))
				.description(GeneralLedgerDescriptions.CASH_PROCEEDS_FROM_CLOSE, swap)
				.build());

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
	}


	@Test
	public void testLmeMaturityCommission() {
		InvestmentSecurity lpz3 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("LPZ13");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a buy trade
		BigDecimal buyQuantity = new BigDecimal("10");
		Date buyDate = DateUtils.toDate("12/10/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(lpz3, buyQuantity, true, buyDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		// Assert the position open for the buy
		this.accountingUtils.assertPositionOpen(buy);

		// Create and execute a sell trade
		BigDecimal sellQuantity = new BigDecimal("10");
		Date sellDate = DateUtils.toDate("12/11/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(lpz3, sellQuantity, false, sellDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		// Assert the position open for the sell (LMEs are treated a bit differently - a buy and a sell results in two lots remaining open)
		this.accountingUtils.assertPositionOpen(sell);

		// Get the security maturity event
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(lpz3.getId());
		searchForm.setTypeId(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.SECURITY_MATURITY).getId());
		InvestmentSecurityEvent maturityEvent = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm).get(0);
		Date maturityDate = maturityEvent.getEventDate();

		// Generate the event journal for the security maturity event (only for this client account)
		List<? extends AccountingJournalDetailDefinition> maturityDetails = this.accountingUtils.generateAndPostEventJournal(maturityEvent, accountInfo.getClientAccount())
				.getMainBookedJournal()
				.getJournalDetailList();

		List<AccountingJournalDetailDefinition> expectedDetails = new ArrayList<>();
		AccountingJournalDetailBuilder builder = AccountingJournalDetailBuilder.builder();

		expectedDetails.add(builder.clientInvestmentAccount(accountInfo.getClientAccount())
				.holdingInvestmentAccount(accountInfo.getHoldingAccount())
				.executingCompany(executingBroker)
				.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.security(lpz3)
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.localDebitCredit(BigDecimal.ZERO)
				.opening(false)
				.transactionDate(maturityDate)
				.description(GeneralLedgerDescriptions.format(GeneralLedgerDescriptions.SECURITY_MATURITY, lpz3, maturityDate))
				.build());

		expectedDetails.add(builder.clientInvestmentAccount(accountInfo.getClientAccount()).quantity(buyQuantity).build());

		expectedDetails.add(builder.clientInvestmentAccount(accountInfo.getClientAccount())
				.holdingInvestmentAccount(accountInfo.getHoldingAccount())
				.executingCompany(executingBroker)
				.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.security(lpz3)
				.price(new BigDecimal("9.9"))
				//9.9 x 10 = 99
				.localDebitCredit(new BigDecimal("99"))
				.opening(true)
				.description(GeneralLedgerDescriptions.format(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, lpz3))
				.build());

		expectedDetails.add(builder.clientInvestmentAccount(accountInfo.getClientAccount())
				.holdingInvestmentAccount(accountInfo.getHoldingAccount())
				.executingCompany(executingBroker)
				.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("-99"))
				.description(GeneralLedgerDescriptions.format(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, lpz3))
				.build());

		// Validate the security maturity
		AccountingUtils.assertJournalDetailList(expectedDetails, maturityDetails);
	}


	private InvestmentSecurity getUbmSecurity() {
		InvestmentSecurity ubm;
		ubm = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("UBM4");
		if (ubm == null) {
			this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("UBM14");
		}

		return ubm;
	}


	/**
	 * This test creates a new AccountingCommissionDefinition that is specific to the UBM4 future with a different currency than the base currency. After the test completes,
	 * the new commission definition is deleted.
	 */
	@Test
	public void testDifferentCommissionCurrency() {
		InvestmentSecurity ubm = getUbmSecurity();
		InvestmentSecurity swissFranc = this.investmentInstrumentUtils.getCurrencyBySymbol("CHF");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		AccountingCommissionDefinitionSearchForm commissionSearchForm = new AccountingCommissionDefinitionSearchForm();
		commissionSearchForm.setInvestmentSecurityId(ubm.getId());
		commissionSearchForm.setAccountingAccountName(AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
		BigDecimal commissionAmountInFrancs = new BigDecimal("17.91");
		AccountingCommissionDefinition ubm4CommissionDefinition = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchForm));
		if (ubm4CommissionDefinition == null) {
			ubm4CommissionDefinition = new AccountingCommissionDefinition();
			ubm4CommissionDefinition.setInvestmentSecurity(ubm);
			ubm4CommissionDefinition.setExpenseAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE));
			ubm4CommissionDefinition.setTransactionType(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));
			ubm4CommissionDefinition.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
			ubm4CommissionDefinition.setCommissionType(AccountingCommissionTypes.SIMPLE);
			ubm4CommissionDefinition.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
			ubm4CommissionDefinition.setCommissionCurrency(swissFranc);
			ubm4CommissionDefinition.setCommissionAmount(commissionAmountInFrancs);
			ubm4CommissionDefinition = this.accountingCommissionService.saveAccountingCommissionDefinition(ubm4CommissionDefinition);
		}

		AccountingCommissionDefinition ubm4CommissionDefinitionClearing = createZeroCommissionDefinition(ubm, null, AccountingAccount.CLEARING_COMMISSION_EXPENSE);

		try {
			// Create and execute a buy trade to open a position
			BigDecimal positionQuantity = new BigDecimal("5");
			BigDecimal averageUnitPrice = new BigDecimal("125.00");
			Date buyDate = DateUtils.toDate("12/10/2013");
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(ubm, positionQuantity, true, buyDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy);

			AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);

			// Build the expected list of details
			List<AccountingJournalDetailDefinition> expectedBuyDetails = new ArrayList<>();
			expectedBuyDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
					.price(averageUnitPrice)
					.quantity(positionQuantity)
					.opening(true)
					.localDebitCredit(BigDecimal.ZERO)
					.baseDebitCredit(BigDecimal.ZERO)
					.description(GeneralLedgerDescriptions.POSITION_OPENING, ubm)
					.build());

			expectedBuyDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.security(ubm)
					.price(new BigDecimal("2.93"))
					.quantity(positionQuantity)
					.localDebitCredit(new BigDecimal("14.65"))
					.baseDebitCredit(new BigDecimal("20.18"))
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, ubm)
					.build());

			expectedBuyDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
					.security(swissFranc)
					.price(null)
					.quantity(null)
					.localDebitCredit(commissionAmountInFrancs.negate())
					.baseDebitCredit(new BigDecimal("-20.18"))
					.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_OPEN, ubm)
					.build());

			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> actualBuyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);

			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(expectedBuyDetails, actualBuyDetails);

			// Create and execute a sell trade to close the position
			Date sellDate = DateUtils.toDate("12/11/2013");
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(ubm, positionQuantity, false, sellDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);

			detailBuilder = AccountingJournalDetailBuilder.builder(sell);

			// Build the expected list of details
			List<AccountingJournalDetailDefinition> expectedSellDetails = new ArrayList<>();
			expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
					.price(averageUnitPrice)
					.quantity(positionQuantity.negate())
					.opening(false)
					.localDebitCredit(BigDecimal.ZERO)
					.baseDebitCredit(BigDecimal.ZERO)
					.description(GeneralLedgerDescriptions.FULL_POSITION_CLOSE, ubm)
					.build());

			expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.security(ubm)
					.price(new BigDecimal("2.932"))
					.quantity(positionQuantity)
					.localDebitCredit(new BigDecimal("14.66"))
					.baseDebitCredit(new BigDecimal("20.22"))
					.opening(true)
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, ubm)
					.build());

			expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
					.security(swissFranc)
					.price(null)
					.quantity(null)
					.localDebitCredit(commissionAmountInFrancs.negate())
					.baseDebitCredit(new BigDecimal("-20.22"))
					.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_CLOSE, ubm)
					.build());

			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(expectedSellDetails, actualSellDetails);
		}
		//Cleanup - delete AccountingCommissionDefinition that was created for test
		finally {
			if (ubm4CommissionDefinition != null && ubm4CommissionDefinition.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(ubm4CommissionDefinition.getId());
			}
			if (ubm4CommissionDefinitionClearing != null && ubm4CommissionDefinitionClearing.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(ubm4CommissionDefinitionClearing.getId());
			}
		}
	}


	@Test
	public void testHalfTurnTieredNotionalClearingCommission() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ5");
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		final AccountingCommissionDefinition commissionDefinition = addTieredNotionalCommissionDefinition(security, AccountingCommissionTypes.TIERED_NOTIONAL, AccountingCommissionApplyMethods.HALF_TURN, AccountingAccount.CLEARING_COMMISSION_EXPENSE);
		final AccountingCommissionDefinition commissionDefinitionExecuting = createZeroCommissionDefinition(security, null, AccountingAccount.EXECUTING_COMMISSION_EXPENSE);

		try {
			// Create and execute a buy trade to open a position
			BigDecimal positionQuantity = new BigDecimal("2000");
			BigDecimal averageUnitPrice = new BigDecimal("1992");
			Date buyDate = DateUtils.toDate("09/29/2015");
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, positionQuantity, true, buyDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy);

			// Build the expected list of details
			BigDecimal expectedCommission = new BigDecimal("670");
			BigDecimal expectedPrice = MathUtils.divide(expectedCommission, positionQuantity); // 670 / 2000

			List<? extends AccountingJournalDetailDefinition> actualBuyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);
			this.assertTradeJournalDetailListForTieredNotionalCommission(buy, actualBuyDetails, null, expectedPrice, null, expectedCommission);

			// Create and execute a sell trade to close the position
			Date sellDate = DateUtils.toDate("09/30/2015");
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(security, positionQuantity, false, sellDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);

			List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);
			assertTradeJournalDetailListForTieredNotionalCommission(sell, actualSellDetails, null, expectedPrice, null, expectedCommission);
		}
		//Cleanup - delete AccountingCommissionDefinition that was created for test
		finally {
			if (commissionDefinition.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(commissionDefinition.getId());
			}
			if (commissionDefinitionExecuting.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(commissionDefinitionExecuting.getId());
			}
		}
	}


	@Test
	public void testRoundTurnTieredNotionalExecutingCommission() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ5");
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		final AccountingCommissionDefinition commissionDefinition = addTieredNotionalCommissionDefinition(security, AccountingCommissionTypes.TIERED_NOTIONAL, AccountingCommissionApplyMethods.ROUND_TURN, AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
		final AccountingCommissionDefinition commissionDefinitionExecuting = createZeroCommissionDefinition(security, null, AccountingAccount.CLEARING_COMMISSION_EXPENSE);

		try {
			// Create and execute a buy trade to open a position
			BigDecimal positionQuantity = new BigDecimal("2000");
			BigDecimal averageUnitPrice = new BigDecimal("1992");
			Date buyDate = DateUtils.toDate("09/29/2015");
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, positionQuantity, true, buyDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy);

			// Build the expected list of details
			BigDecimal expectedCommission = MathUtils.multiply(new BigDecimal("670"), new BigDecimal("2"));
			BigDecimal expectedPrice = MathUtils.divide(expectedCommission, positionQuantity);

			List<? extends AccountingJournalDetailDefinition> actualBuyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);
			assertTradeJournalDetailListForTieredNotionalCommission(buy, actualBuyDetails, expectedPrice, null, null, BigDecimal.ZERO);

			// Create and execute a sell trade to close the position
			Date sellDate = DateUtils.toDate("09/30/2015");
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(security, positionQuantity, false, sellDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);

			List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);
			assertTradeJournalDetailListForTieredNotionalCommission(sell, actualSellDetails, expectedPrice, null, null, expectedCommission);
		}
		//Cleanup - delete AccountingCommissionDefinition that was created for test
		finally {
			if (commissionDefinition.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(commissionDefinition.getId());
			}
			if (commissionDefinitionExecuting.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(commissionDefinitionExecuting.getId());
			}
		}
	}


	@Test
	public void testHalfTurnTieredNotionalExecutingAndClearingCommission() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ5");
		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		final AccountingCommissionDefinition executingDefinition = addTieredNotionalCommissionDefinition(security, AccountingCommissionTypes.TIERED_NOTIONAL, AccountingCommissionApplyMethods.HALF_TURN, AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
		final AccountingCommissionDefinition executingDefinitionClearing = addTieredNotionalCommissionDefinition(security, AccountingCommissionTypes.TIERED_NOTIONAL, AccountingCommissionApplyMethods.HALF_TURN, AccountingAccount.CLEARING_COMMISSION_EXPENSE);
		try {
			// Create and execute a buy trade to open a position
			BigDecimal positionQuantity = new BigDecimal("2000");
			BigDecimal averageUnitPrice = new BigDecimal("1992");
			Date buyDate = DateUtils.toDate("09/29/2015");
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(security, positionQuantity, true, buyDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy);

			// Build the expected list of details
			BigDecimal expectedCommission = MathUtils.multiply(new BigDecimal("670"), new BigDecimal("2"));
			BigDecimal expectedPrice = MathUtils.divide(MathUtils.divide(expectedCommission, positionQuantity), new BigDecimal("2"));

			List<? extends AccountingJournalDetailDefinition> actualBuyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);
			assertTradeJournalDetailListForTieredNotionalCommission(buy, actualBuyDetails, expectedPrice, expectedPrice, null, expectedCommission);
			// Create and execute a sell trade to close the position
			Date sellDate = DateUtils.toDate("09/30/2015");
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(security, positionQuantity, false, sellDate, executingBroker, accountInfo)
					.withAverageUnitPrice(averageUnitPrice).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);

			List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);
			assertTradeJournalDetailListForTieredNotionalCommission(sell, actualSellDetails, expectedPrice, expectedPrice, null, expectedCommission);
		}
		//Cleanup - delete AccountingCommissionDefinition that was created for test
		finally {
			if (executingDefinition.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(executingDefinition.getId());
			}
			if (executingDefinitionClearing.getId() != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(executingDefinitionClearing.getId());
			}
		}
	}


	//Used to create more specific instances of commission definitions that will result in ZERO commission becing calculated for the given account
	private AccountingCommissionDefinition createZeroCommissionDefinition(InvestmentSecurity security, InvestmentType type, String accountingAccountName) {
		AccountingCommissionDefinitionSearchForm commissionSearchForm = new AccountingCommissionDefinitionSearchForm();
		if (security != null) {
			commissionSearchForm.setInvestmentSecurityId(security.getId());
		}
		if (type != null) {
			commissionSearchForm.setInvestmentTypeId(type.getId());
		}
		commissionSearchForm.setAccountingAccountName(accountingAccountName);
		commissionSearchForm.setCommissionType(AccountingCommissionTypes.SIMPLE);
		commissionSearchForm.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);

		AccountingCommissionDefinition commissionDefinition = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchForm));
		if (commissionDefinition != null) {
			throw new IllegalStateException("Test did not expect a valid commission definition for security: " + (security != null ? security.getId() : "null"));
		}

		commissionDefinition = new AccountingCommissionDefinition();
		commissionDefinition.setExpenseAccountingAccount(this.accountingAccountService.getAccountingAccountByName(accountingAccountName));
		commissionDefinition.setInvestmentSecurity(security);
		commissionDefinition.setTransactionType(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));
		commissionDefinition.setInvestmentType(type);
		commissionDefinition.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
		commissionDefinition.setCommissionType(AccountingCommissionTypes.SIMPLE);
		commissionDefinition.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
		commissionDefinition.setCommissionAmount(BigDecimal.ZERO);

		return this.accountingCommissionService.saveAccountingCommissionDefinition(commissionDefinition);
	}


	private AccountingCommissionDefinition addTieredNotionalCommissionDefinition(InvestmentSecurity security, AccountingCommissionTypes commissionType, AccountingCommissionApplyMethods applyMethod, String accountingAccountName) {
		AccountingCommissionDefinitionSearchForm commissionSearchForm = new AccountingCommissionDefinitionSearchForm();
		commissionSearchForm.setInvestmentSecurityId(security.getId());
		commissionSearchForm.setAccountingAccountName(accountingAccountName);
		commissionSearchForm.setStartDate(DateUtils.toDate("09/29/2015"));
		commissionSearchForm.setEndDate(DateUtils.addDays(commissionSearchForm.getStartDate(), 2));

		AccountingCommissionDefinition commissionDefinition = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchForm));
		if (commissionDefinition != null) {
			throw new IllegalStateException("Test did not expect a valid commission definition for security: " + security.getId() + " and account: " + accountingAccountName);
		}

		List<AccountingCommissionTier> tiers = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			AccountingCommissionTier tier = new AccountingCommissionTier();
			tier.setThreshold(new BigDecimal(i * 100000000));
			tier.setFeeAmount(new BigDecimal(Integer.valueOf(i * 330 + 10).toString()));
			tiers.add(tier);
		}

		commissionDefinition = new AccountingCommissionDefinition();
		commissionDefinition.setExpenseAccountingAccount(this.accountingAccountService.getAccountingAccountByName(accountingAccountName));
		commissionDefinition.setInvestmentSecurity(security);
		commissionDefinition.setTransactionType(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE));
		commissionDefinition.setCommissionApplyMethod(applyMethod);
		commissionDefinition.setCommissionType(commissionType);
		commissionDefinition.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.FLAT_AMOUNT);
		commissionDefinition.setCommissionTierList(tiers);
		commissionDefinition.setStartDate(commissionSearchForm.getStartDate());
		commissionDefinition.setEndDate(commissionSearchForm.getEndDate());

		this.accountingCommissionService.saveAccountingCommissionDefinition(commissionDefinition);
		commissionDefinition = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchForm));
		return commissionDefinition;
	}


	private void assertTradeJournalDetailListForTieredNotionalCommission(Trade trade, List<? extends AccountingJournalDetailDefinition> actualJournalDetailList, BigDecimal expectedExecutingPrice, BigDecimal expectedClearingPrice, BigDecimal expectedExchangePrice, BigDecimal expectedTotalCommissionAmount) {
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(trade);
		List<AccountingJournalDetailDefinition> expectedDetails = new ArrayList<>();
		expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(trade.getAverageUnitPrice())
				.quantity(trade.isBuy() ? trade.getQuantity() : trade.getQuantity().negate())
				.opening(trade.isBuy())
				.localDebitCredit(BigDecimal.ZERO)
				.baseDebitCredit(BigDecimal.ZERO)
				.description(trade.isBuy() ? GeneralLedgerDescriptions.POSITION_OPENING : GeneralLedgerDescriptions.FULL_POSITION_CLOSE, trade.getInvestmentSecurity())
				.build());

		if (!expectedTotalCommissionAmount.equals(BigDecimal.ZERO)) {
			if (expectedClearingPrice != null) {
				BigDecimal expectedClearingCommission = MathUtils.multiply(expectedClearingPrice, trade.getQuantity());
				expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
						.security(trade.getInvestmentSecurity())
						.price(expectedClearingPrice.setScale(3, RoundingMode.UP))
						.quantity(trade.getQuantity())
						.localDebitCredit(expectedClearingCommission)
						.baseDebitCredit(expectedClearingCommission)
						.opening(true)
						.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, trade.getInvestmentSecurity())
						.build());
			}

			if (expectedExecutingPrice != null) {
				BigDecimal expectedExecutingCommission = MathUtils.multiply(expectedExecutingPrice, trade.getQuantity());
				expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
						.security(trade.getInvestmentSecurity())
						.price(expectedExecutingPrice.setScale(3, RoundingMode.UP))
						.quantity(trade.getQuantity())
						.localDebitCredit(expectedExecutingCommission)
						.baseDebitCredit(expectedExecutingCommission)
						.opening(true)
						.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, trade.getInvestmentSecurity())
						.build());
			}


			if (expectedExchangePrice != null) {
				BigDecimal expectedExchangeFee = MathUtils.multiply(expectedExchangePrice, trade.getQuantity());
				expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXCHANGE_FEE_EXPENSE))
						.security(trade.getInvestmentSecurity())
						.price(expectedExchangePrice.setScale(2, RoundingMode.UP))
						.quantity(trade.getQuantity())
						.localDebitCredit(expectedExchangeFee)
						.baseDebitCredit(expectedExchangeFee)
						.opening(true)
						.description(GeneralLedgerDescriptions.EXCHANGE_FEE_EXPENSE, trade.getInvestmentSecurity())
						.build());
			}

			expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
					.security(trade.getPayingSecurity())
					.price(null)
					.quantity(null)
					.localDebitCredit(expectedTotalCommissionAmount.negate())
					.baseDebitCredit(expectedTotalCommissionAmount.negate())
					.description(trade.isBuy() ? GeneralLedgerDescriptions.CASH_EXPENSE_FROM_OPEN : GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, trade.getInvestmentSecurity())
					.build());
		}


		AccountingUtils.assertJournalDetailList(expectedDetails, actualJournalDetailList);
	}


	@Disabled // Fails with DB deadlock searching for Trade while executing rule evaluations
	@Test
	public void testHalfTurnExchangeForPhysicalCommissionMultipleThreads() {
		int taskCount = 10;
		CountDownLatch latch = new CountDownLatch(taskCount);
		Lock lock = new ReentrantLock();
		Callable<Boolean> task = () -> {
			testHalfTurnExchangeForPhysicalCommission(lock, latch);
			return Boolean.TRUE;
		};
		List<ConcurrentExecutionUtils.ConcurrentExecutionResponse<Boolean>> results = ConcurrentExecutionUtils.executeConcurrently(task);
		for (ConcurrentExecutionUtils.ConcurrentExecutionResponse<Boolean> result : results) {
			if (!result.isSuccessful()) {
				Assertions.fail(ExceptionUtils.toString(result.getError()));
			}
		}
	}


	@Test
	public void testHalfTurnExchangeForPhysicalCommission() throws InterruptedException {
		testHalfTurnExchangeForPhysicalCommission(null, null);
	}


	private AccountingCommissionDefinition createExchangeForPhysicalDefinition(InvestmentSecurity security, BigDecimal commissionAmount, String accountingAccountName, Date startDate, Date endDate) {
		AccountingCommissionDefinition definition = new AccountingCommissionDefinition();
		definition.setInvestmentSecurity(security);
		definition.setExpenseAccountingAccount(this.accountingUtils.getAccountingAccount(accountingAccountName));
		definition.setTransactionType(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.EXCHANGE_FOR_PHYSICAL));
		definition.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
		definition.setCommissionType(AccountingCommissionTypes.SIMPLE);
		definition.setCommissionCalculationMethod(AccountingCommissionCalculationMethods.PER_UNIT);
		definition.setCommissionAmount(commissionAmount);
		definition.setStartDate(startDate);
		definition.setEndDate(endDate);
		return definition;
	}


	private void testHalfTurnExchangeForPhysicalCommission(Lock lock, CountDownLatch latch) throws InterruptedException {

		Date startDate = DateUtils.toDate("09/27/2013");
		Date endDate = DateUtils.addDays(startDate, 2);
		// securities
		InvestmentSecurity futureSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ13");
		InvestmentSecurity fundSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("VOO");

		// Create accounts
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo fundsAccountInfo = this.investmentAccountUtils.createHoldingAccount(futuresAccountInfo, BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.tradeUtils.getTradeType(TradeType.FUNDS));

		// set up commission definition for EFP
		BigDecimal executingCommissionPerUnit = BigDecimal.valueOf(0.1d);
		AccountingCommissionDefinition commissionDefinitionExecuting = createExchangeForPhysicalDefinition(futureSecurity, executingCommissionPerUnit, AccountingAccount.EXECUTING_COMMISSION_EXPENSE, startDate, endDate);

		BigDecimal clearingCommissionPerUnit = BigDecimal.valueOf(0.2d);
		AccountingCommissionDefinition commissionDefinitionClearing = createExchangeForPhysicalDefinition(futureSecurity, clearingCommissionPerUnit, AccountingAccount.CLEARING_COMMISSION_EXPENSE, startDate, endDate);

		//Prevents executing commission on buy
		AccountingCommissionDefinition zeroDefinition = createZeroCommissionDefinition(fundSecurity, null, AccountingAccount.EXECUTING_COMMISSION_EXPENSE);

		boolean commissionDefinitionCreated = false;
		boolean commissionDefinitionCreatedClearing = false;
		if (lock != null) {
			lock.lock();
		}
		try {
			// make sure the Commission Definition does not already exist before adding it.
			AccountingCommissionDefinitionSearchForm commissionSearchForm = new AccountingCommissionDefinitionSearchForm();
			commissionSearchForm.setInvestmentSecurityId(commissionDefinitionExecuting.getInvestmentSecurity().getId());
			commissionSearchForm.setAccountingTransactionTypeId(commissionDefinitionExecuting.getTransactionType().getId());
			commissionSearchForm.setStartDate(commissionDefinitionExecuting.getStartDate());
			commissionSearchForm.setEndDate(commissionDefinitionExecuting.getEndDate());
			AccountingCommissionDefinition foundDefinition = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchForm));
			if (foundDefinition == null) {
				this.accountingCommissionService.saveAccountingCommissionDefinition(commissionDefinitionExecuting);
				commissionDefinitionCreated = true;
			}

			// make sure the Commission Definition does not already exist before adding it.
			AccountingCommissionDefinitionSearchForm commissionSearchFormClearing = new AccountingCommissionDefinitionSearchForm();
			commissionSearchFormClearing.setAccountingAccountName(AccountingAccount.CLEARING_COMMISSION_EXPENSE);
			commissionSearchFormClearing.setInvestmentSecurityId(commissionDefinitionClearing.getInvestmentSecurity().getId());
			commissionSearchFormClearing.setAccountingTransactionTypeId(commissionDefinitionClearing.getTransactionType().getId());
			commissionSearchFormClearing.setStartDate(commissionDefinitionClearing.getStartDate());
			commissionSearchFormClearing.setEndDate(commissionDefinitionClearing.getEndDate());
			AccountingCommissionDefinition foundDefinitionClearing = CollectionUtils.getOnlyElement(this.accountingCommissionService.getAccountingCommissionDefinitionList(commissionSearchFormClearing));
			if (foundDefinitionClearing == null) {
				this.accountingCommissionService.saveAccountingCommissionDefinition(commissionDefinitionClearing);
				commissionDefinitionCreatedClearing = true;
			}
		}
		finally {
			if (lock != null) {
				lock.unlock();
			}
		}

		try {
			// Prepare EFP TradeGroup
			TradeGroup efpGroup = tradeEfp(futuresAccountInfo, futureSecurity, fundsAccountInfo, fundSecurity, false, startDate);
			if (latch != null) {
				latch.countDown();
			}
			validateEfpTradeGroup(efpGroup, true, executingCommissionPerUnit, clearingCommissionPerUnit, null, null);
		}
		//Cleanup - delete AccountingCommissionDefinition that was created for test
		finally {
			if (commissionDefinitionCreated || commissionDefinitionCreatedClearing) {
				if (latch != null) {
					latch.await();
				}
				if (lock != null) {
					lock.lock();
				}
				try {
					if (commissionDefinitionCreated) {
						this.accountingCommissionService.deleteAccountingCommissionDefinition(commissionDefinitionExecuting.getId());
					}
					if (commissionDefinitionCreatedClearing) {
						this.accountingCommissionService.deleteAccountingCommissionDefinition(commissionDefinitionClearing.getId());
					}
					this.accountingCommissionService.deleteAccountingCommissionDefinition(zeroDefinition.getId());
				}
				finally {
					if (lock != null) {
						lock.unlock();
					}
				}
			}
		}
	}


	private TradeGroup tradeEfp(AccountInfo futureAccountInfo, InvestmentSecurity future, AccountInfo fundAccountInfo, InvestmentSecurity fund, boolean buyFuture, Date tradeDate) {
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		// Create EFP trade list
		Trade futureTrade = this.tradeUtils.newFuturesTradeBuilder(future, new BigDecimal("417"), buyFuture, tradeDate, executingBroker, futureAccountInfo)
				.withAverageUnitPrice(new BigDecimal("1913.72"))
				.build();
		Trade fundTrade = this.tradeUtils.newTradeBuilder(this.tradeUtils.getTradeType(TradeType.FUNDS), fund, new BigDecimal("227715"), new BigDecimal("227715"), !buyFuture, tradeDate, executingBroker, fundAccountInfo)
				.withCommissionPerUnit(BigDecimal.ZERO)
				.build();
		List<Trade> tradeList = new ArrayList<>(2);
		tradeList.add(futureTrade);
		tradeList.add(fundTrade);

		// Prepare EFP TradeGroup
		TradeGroup efpGroup = new TradeGroup();
		efpGroup.setInvestmentSecurity(future);
		efpGroup.setSecondaryInvestmentSecurity(fund);
		efpGroup.setTradeDate(tradeDate);
		efpGroup.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.EFP.getTypeName()));
		efpGroup.setTradeList(tradeList);
		efpGroup.setTraderUser(futureTrade.getTraderUser());

		// save TradeGroup and refresh the sell trade
		efpGroup = this.tradeGroupService.saveTradeGroup(efpGroup);
		futureTrade = efpGroup.getTradeList().get(0);
		fundTrade = efpGroup.getTradeList().get(1);

		// fully execute trades
		this.tradeUtils.fullyExecuteTrade(futureTrade);
		this.tradeUtils.fullyExecuteTrade(fundTrade);

		return efpGroup;
	}


	private void validateEfpTradeGroup(TradeGroup efpTradeGroup, boolean positionOpening, BigDecimal expectedFutureExecutingCommissionRate, BigDecimal expectedFutureClearingCommissionRate, BigDecimal expectedFutureExchangeFeeRate, BigDecimal expectedFundSecFeeRate) {
		Trade futureTrade = efpTradeGroup.getTradeList().get(0);
		Trade fundTrade = efpTradeGroup.getTradeList().get(1);
		// validate Futures sell details
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(futureTrade);
		List<AccountingJournalDetailDefinition> expectedFuturesSellDetails = new ArrayList<>();
		expectedFuturesSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(futureTrade.getAverageUnitPrice())
				.quantity(positionOpening ? futureTrade.getQuantityIntended().negate() : futureTrade.getQuantityIntended())
				.opening(positionOpening)
				.localDebitCredit(BigDecimal.ZERO)
				.baseDebitCredit(BigDecimal.ZERO)
				.description(positionOpening ? GeneralLedgerDescriptions.POSITION_OPENING : GeneralLedgerDescriptions.FULL_POSITION_CLOSE, futureTrade.getInvestmentSecurity())
				.build());

		BigDecimal totalCommissionAndFee = BigDecimal.ZERO;
		if (expectedFutureClearingCommissionRate != null) {
			BigDecimal clearingCommission = MathUtils.multiply(futureTrade.getQuantityIntended(), expectedFutureClearingCommissionRate);
			totalCommissionAndFee = totalCommissionAndFee.add(clearingCommission);
			expectedFuturesSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
					.security(futureTrade.getInvestmentSecurity())
					.price(expectedFutureClearingCommissionRate)
					.quantity(futureTrade.getQuantityIntended())
					.opening(true)
					.localDebitCredit(clearingCommission)
					.baseDebitCredit(clearingCommission)
					.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, futureTrade.getInvestmentSecurity())
					.build());
		}

		if (expectedFutureExecutingCommissionRate != null) {
			BigDecimal executingCommission = MathUtils.multiply(futureTrade.getQuantityIntended(), expectedFutureExecutingCommissionRate);
			totalCommissionAndFee = totalCommissionAndFee.add(executingCommission);
			expectedFuturesSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.security(futureTrade.getInvestmentSecurity())
					.price(expectedFutureExecutingCommissionRate)
					.quantity(futureTrade.getQuantityIntended())
					.opening(true)
					.localDebitCredit(executingCommission)
					.baseDebitCredit(executingCommission)
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, futureTrade.getInvestmentSecurity())
					.build());
		}

		if (expectedFutureExchangeFeeRate != null) {
			BigDecimal exchangeFee = MathUtils.multiply(futureTrade.getQuantityIntended(), expectedFutureExchangeFeeRate);
			totalCommissionAndFee = totalCommissionAndFee.add(exchangeFee);
			expectedFuturesSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXCHANGE_FEE_EXPENSE))
					.security(futureTrade.getInvestmentSecurity())
					.price(expectedFutureExchangeFeeRate)
					.quantity(futureTrade.getQuantityIntended())
					.opening(true)
					.localDebitCredit(exchangeFee)
					.baseDebitCredit(exchangeFee)
					.description(GeneralLedgerDescriptions.EXCHANGE_FEE_EXPENSE, futureTrade.getInvestmentSecurity())
					.build());
		}

		expectedFuturesSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(totalCommissionAndFee.negate())
				.baseDebitCredit(totalCommissionAndFee.negate())
				.description(positionOpening ? GeneralLedgerDescriptions.CASH_EXPENSE_FROM_OPEN : GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, futureTrade.getInvestmentSecurity())
				.build());

		List<? extends AccountingJournalDetailDefinition> actualFuturesSellDetails = this.accountingUtils.getFirstTradeFillDetails(futureTrade, true);
		AccountingUtils.assertJournalDetailList(expectedFuturesSellDetails, actualFuturesSellDetails);

		// validate Funds buy
		detailBuilder = AccountingJournalDetailBuilder.builder(fundTrade);
		List<AccountingJournalDetailDefinition> expectedFundsBuyDetails = new ArrayList<>();
		BigDecimal payment = MathUtils.round(MathUtils.multiply(fundTrade.getAverageUnitPrice(), fundTrade.getQuantityIntended()), 2);
		expectedFundsBuyDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(fundTrade.getAverageUnitPrice())
				.quantity(positionOpening ? fundTrade.getQuantityIntended() : fundTrade.getQuantityIntended().negate())
				.opening(positionOpening)
				.localDebitCredit(positionOpening ? payment : payment.negate())
				.baseDebitCredit(positionOpening ? payment : payment.negate())
				.description(positionOpening ? GeneralLedgerDescriptions.POSITION_OPENING : GeneralLedgerDescriptions.FULL_POSITION_CLOSE, fundTrade.getInvestmentSecurity())
				.build());
		if (!positionOpening) {
			final BigDecimal secFee = MathUtils.multiply(expectedFundSecFeeRate, fundTrade.getAccountingNotional(), 2);
			payment = MathUtils.subtract(payment, secFee);
			expectedFundsBuyDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_SEC_FEES))
					.price(MathUtils.roundToSmallestPrecision(MathUtils.divide(secFee, fundTrade.getQuantityIntended()), (testValue) -> MathUtils.isEqual(secFee, MathUtils.multiply(testValue, fundTrade.getQuantity(), secFee.scale()))))
					.quantity(fundTrade.getQuantityIntended())
					.opening(true)
					.localDebitCredit(secFee)
					.baseDebitCredit(secFee)
					.description(GeneralLedgerDescriptions.SEC_FEES_EXPENSE, fundTrade.getInvestmentSecurity())
					.build());
		}
		expectedFundsBuyDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.opening(true)
				.localDebitCredit(positionOpening ? payment.negate() : payment)
				.baseDebitCredit(positionOpening ? payment.negate() : payment)
				.description(positionOpening ? GeneralLedgerDescriptions.CASH_EXPENSE_FROM_OPEN : GeneralLedgerDescriptions.CASH_PROCEEDS_FROM_CLOSE, fundTrade.getInvestmentSecurity())
				.build());

		List<? extends AccountingJournalDetailDefinition> actualFundsBuyDetails = this.accountingUtils.getFirstTradeFillDetails(fundTrade, true);
		AccountingUtils.assertJournalDetailList(expectedFundsBuyDetails, actualFundsBuyDetails);
	}


	/**
	 * This test should closely mimic {@link AccountingCommissionTests#testRoundTurnCommissionForFutures()} but for half-turn, block trade commission definition.
	 */
	@Test
	public void testBlockTradeCommissionApplication() {
		Date buyDate = DateUtils.toDate("12/16/2013");
		InvestmentType futuresType = this.investmentSetupService.getInvestmentTypeByName(InvestmentType.FUTURES);

		AccountingCommissionDefinitionSearchForm definitionSearchForm = new AccountingCommissionDefinitionSearchForm();
		definitionSearchForm.setAccountingAccountName(AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
		definitionSearchForm.setAccountingTransactionTypeId(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE).getId());
		definitionSearchForm.setInvestmentTypeId(futuresType.getId());
		definitionSearchForm.setInstrumentHierarchyId(Short.valueOf("3"));
		definitionSearchForm.setInvestmentInstrumentId(891);
		definitionSearchForm.setActiveOnDate(buyDate);

		// Use CCH14 commission definition active 03/01/2011 - 06/30/2016 as a template
		BigDecimal executingCommissionAmount = new BigDecimal("0.75");
		AccountingCommissionDefinition cch14BlockCommissionDefinitionExecuting = copyDefinition(CollectionUtils.getFirstElementStrict(this.accountingCommissionService.getAccountingCommissionDefinitionList(definitionSearchForm)), executingCommissionAmount, AccountingAccount.EXECUTING_COMMISSION_EXPENSE, this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.BLOCK_TRADE), AccountingCommissionApplyMethods.HALF_TURN);
		cch14BlockCommissionDefinitionExecuting = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14BlockCommissionDefinitionExecuting);

		BigDecimal clearingCommissionAmount = new BigDecimal("1.25");
		AccountingCommissionDefinition cch14BlockCommissionDefinitionClearing = copyDefinition(cch14BlockCommissionDefinitionExecuting, clearingCommissionAmount, AccountingAccount.CLEARING_COMMISSION_EXPENSE, null, null);
		cch14BlockCommissionDefinitionClearing = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14BlockCommissionDefinitionClearing);

		BigDecimal exchangeFeeAmount = new BigDecimal("1.10");
		AccountingCommissionDefinition cch14BlockCommissionDefinitionExchange = copyDefinition(cch14BlockCommissionDefinitionExecuting, exchangeFeeAmount, AccountingAccount.EXCHANGE_FEE_EXPENSE, null, null);
		cch14BlockCommissionDefinitionExchange = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14BlockCommissionDefinitionExchange);

		try {
			// Create new accounts for trading
			AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
			InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");
			BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

			// Create and execute a buy trade
			BigDecimal buyQuantity = new BigDecimal("20");
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, executingBroker, accountInfo).withBlockTrade(true).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy);
			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);
			// Build the expected list of details
			BigDecimal expectedCommissionPrice = new BigDecimal("3.10"); // CommissionDefinition: (1.25 + 0.75 + 1.10) = 3.10
			BigDecimal expectedCommissionDebit = MathUtils.multiply(expectedCommissionPrice, buyQuantity); // 20 * 3.10 = 62
			List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
			AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
					.price(BigDecimal.ONE)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(BigDecimal.ZERO)
					.description(GeneralLedgerDescriptions.POSITION_OPENING, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
					.price(clearingCommissionAmount)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(clearingCommissionAmount, buyQuantity))
					.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXCHANGE_FEE_EXPENSE))
					.price(exchangeFeeAmount)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(exchangeFeeAmount, buyQuantity))
					.description(GeneralLedgerDescriptions.EXCHANGE_FEE_EXPENSE, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.price(executingCommissionAmount)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(executingCommissionAmount, buyQuantity))
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
					.security(this.usd)
					.price(null)
					.quantity(null)
					.localDebitCredit(expectedCommissionDebit.negate())
					.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_OPEN, cch14)
					.build());
			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);

			// Create and execute a sell trade
			BigDecimal sellQuantity = new BigDecimal("10");
			Date sellDate = DateUtils.toDate("12/17/2013");
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo).withBlockTrade(true).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);
			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);
			// Build the expected list of details
			expectedCommissionDebit = MathUtils.multiply(expectedCommissionPrice, sellQuantity);
			List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
			detailBuilder = AccountingJournalDetailBuilder.builder(sell);
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
					.price(BigDecimal.ONE)
					.quantity(sellQuantity.negate())
					.opening(false)
					.localDebitCredit(BigDecimal.ZERO)
					.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
					.price(clearingCommissionAmount)
					.quantity(sellQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(clearingCommissionAmount, sellQuantity))
					.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXCHANGE_FEE_EXPENSE))
					.price(exchangeFeeAmount)
					.quantity(sellQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(exchangeFeeAmount, sellQuantity))
					.description(GeneralLedgerDescriptions.EXCHANGE_FEE_EXPENSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.price(executingCommissionAmount)
					.quantity(sellQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(executingCommissionAmount, sellQuantity))
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
					.security(this.usd)
					.price(null)
					.quantity(null)
					.localDebitCredit(expectedCommissionDebit.negate())
					.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, cch14)
					.build());
			// Validate the sell trade
			AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
		}
		finally {
			// remove created block trade commission definition
			this.accountingCommissionService.deleteAccountingCommissionDefinition(cch14BlockCommissionDefinitionExecuting.getId());
			this.accountingCommissionService.deleteAccountingCommissionDefinition(cch14BlockCommissionDefinitionClearing.getId());
			this.accountingCommissionService.deleteAccountingCommissionDefinition(cch14BlockCommissionDefinitionExchange.getId());
		}
	}


	@Test
	public void testTradeCommissionByExecutingBroker() {
		Date buyDate = DateUtils.toDate("12/16/2013");
		InvestmentType futuresType = this.investmentSetupService.getInvestmentTypeByName(InvestmentType.FUTURES);

		AccountingCommissionDefinitionSearchForm definitionSearchForm = new AccountingCommissionDefinitionSearchForm();
		definitionSearchForm.setAccountingAccountName(AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
		definitionSearchForm.setAccountingTransactionTypeId(this.accountingTransactionTypeService.getAccountingTransactionTypeByName(AccountingTransactionType.TRADE).getId());
		definitionSearchForm.setInvestmentTypeId(futuresType.getId());
		definitionSearchForm.setInstrumentHierarchyId(Short.valueOf("3"));
		definitionSearchForm.setInvestmentInstrumentId(891);
		definitionSearchForm.setActiveOnDate(buyDate);

		// Use CCH14 commission definition active 03/01/2011 - 06/30/2016 as a template
		AccountingCommissionDefinition cch14OriginalCommissionDefinitionExecuting = CollectionUtils.getFirstElementStrict(this.accountingCommissionService.getAccountingCommissionDefinitionList(definitionSearchForm));
		// make CCH14 half-turn for testing
		AccountingCommissionApplyMethods originalApplyMethod = cch14OriginalCommissionDefinitionExecuting.getCommissionApplyMethod();
		cch14OriginalCommissionDefinitionExecuting.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
		cch14OriginalCommissionDefinitionExecuting = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14OriginalCommissionDefinitionExecuting);

		definitionSearchForm.setAccountingAccountName(AccountingAccount.CLEARING_COMMISSION_EXPENSE);
		AccountingCommissionDefinition cch14OriginalCommissionDefinitionClearing = CollectionUtils.getFirstElementStrict(this.accountingCommissionService.getAccountingCommissionDefinitionList(definitionSearchForm));
		// make CCH14 half-turn for testing
		cch14OriginalCommissionDefinitionClearing.setCommissionApplyMethod(AccountingCommissionApplyMethods.HALF_TURN);
		cch14OriginalCommissionDefinitionClearing = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14OriginalCommissionDefinitionClearing);

		BigDecimal executingCommissionAmount = new BigDecimal("1.5");
		AccountingCommissionDefinition cch14ExecutingBrokerCommissionDefinitionExecuting = copyDefinition(cch14OriginalCommissionDefinitionExecuting, executingCommissionAmount, AccountingAccount.EXECUTING_COMMISSION_EXPENSE, null, AccountingCommissionApplyMethods.HALF_TURN);
		cch14ExecutingBrokerCommissionDefinitionExecuting.setExecutingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO));
		cch14ExecutingBrokerCommissionDefinitionExecuting = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14ExecutingBrokerCommissionDefinitionExecuting);

		BigDecimal clearingCommissionAmount = new BigDecimal("0.5");
		AccountingCommissionDefinition cch14ExecutingBrokerCommissionDefinitionClearing = copyDefinition(cch14ExecutingBrokerCommissionDefinitionExecuting, clearingCommissionAmount, AccountingAccount.CLEARING_COMMISSION_EXPENSE, null, null);
		cch14ExecutingBrokerCommissionDefinitionClearing = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14ExecutingBrokerCommissionDefinitionClearing);

		BigDecimal exchangeFeeAmount = new BigDecimal("1");
		AccountingCommissionDefinition cch14ExecutingBrokerCommissionDefinitionExchange = copyDefinition(cch14ExecutingBrokerCommissionDefinitionExecuting, exchangeFeeAmount, AccountingAccount.EXCHANGE_FEE_EXPENSE, null, null);
		cch14ExecutingBrokerCommissionDefinitionExchange = this.accountingCommissionService.saveAccountingCommissionDefinition(cch14ExecutingBrokerCommissionDefinitionExchange);

		try {
			// Create new accounts for trading
			AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
			InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");
			BusinessCompany goldmanSachs = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

			// Create and execute a buy trade with Goldman Sachs as the executing broker - will use CCH14 Half Turn Commission Definition for Goldman Sachs as the Executing Company
			BigDecimal buyQuantity = new BigDecimal("20");
			Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, goldmanSachs, accountInfo).withBlockTrade(true).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(buy);
			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);
			// Build the expected list of details
			BigDecimal expectedCommissionPrice = new BigDecimal("3"); // CommissionDefinition: (0.5 + 1.5 + 1) = 3
			BigDecimal expectedCommissionDebit = MathUtils.multiply(expectedCommissionPrice, buyQuantity); // 20 * 3 = 60
			List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
			AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
					.price(BigDecimal.ONE)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(BigDecimal.ZERO)
					.description(GeneralLedgerDescriptions.POSITION_OPENING, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
					.price(clearingCommissionAmount)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(clearingCommissionAmount, buyQuantity))
					.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXCHANGE_FEE_EXPENSE))
					.price(exchangeFeeAmount)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(exchangeFeeAmount, buyQuantity))
					.description(GeneralLedgerDescriptions.EXCHANGE_FEE_EXPENSE, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.price(executingCommissionAmount)
					.quantity(buyQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(executingCommissionAmount, buyQuantity))
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
					.security(this.usd)
					.price(null)
					.quantity(null)
					.localDebitCredit(expectedCommissionDebit.negate())
					.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_OPEN, cch14)
					.build());
			// Validate the buy trade
			AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);

			// Create and execute a sell trade using Citigroup as the executing broker - will use CCH14 Half Turn Commission Definition
			BigDecimal sellQuantity = new BigDecimal("10");
			Date sellDate = DateUtils.toDate("12/17/2013");
			BusinessCompany citigroup = this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP);
			Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, citigroup, accountInfo).withBlockTrade(true).buildAndSave();
			this.tradeUtils.fullyExecuteTrade(sell);
			// Get the list of details for the trade after it is executed
			List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);
			// Build the expected list of details
			expectedCommissionPrice = new BigDecimal("4.65"); // 1.5 + 3.15 + 0 = 4.65
			expectedCommissionDebit = MathUtils.multiply(expectedCommissionPrice, sellQuantity);
			List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
			detailBuilder = AccountingJournalDetailBuilder.builder(sell);
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
					.price(BigDecimal.ONE)
					.quantity(sellQuantity.negate())
					.opening(false)
					.localDebitCredit(BigDecimal.ZERO)
					.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
					.price(cch14OriginalCommissionDefinitionClearing.getCommissionAmount())
					.quantity(sellQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(cch14OriginalCommissionDefinitionClearing.getCommissionAmount(), sellQuantity))
					.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
					.price(cch14OriginalCommissionDefinitionExecuting.getCommissionAmount())
					.quantity(sellQuantity)
					.opening(true)
					.localDebitCredit(MathUtils.multiply(cch14OriginalCommissionDefinitionExecuting.getCommissionAmount(), sellQuantity))
					.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
					.build());
			expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
					.security(this.usd)
					.price(null)
					.quantity(null)
					.localDebitCredit(expectedCommissionDebit.negate())
					.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, cch14)
					.build());
			// Validate the sell trade
			AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
		}
		finally {
			// Reset the CCH14 commission definition
			cch14OriginalCommissionDefinitionExecuting.setCommissionApplyMethod(originalApplyMethod);
			this.accountingCommissionService.saveAccountingCommissionDefinition(cch14OriginalCommissionDefinitionExecuting);
			cch14OriginalCommissionDefinitionClearing.setCommissionApplyMethod(originalApplyMethod);
			this.accountingCommissionService.saveAccountingCommissionDefinition(cch14OriginalCommissionDefinitionClearing);
			// remove created trade commission definition
			this.accountingCommissionService.deleteAccountingCommissionDefinition(cch14ExecutingBrokerCommissionDefinitionExecuting.getId());
			this.accountingCommissionService.deleteAccountingCommissionDefinition(cch14ExecutingBrokerCommissionDefinitionClearing.getId());
			this.accountingCommissionService.deleteAccountingCommissionDefinition(cch14ExecutingBrokerCommissionDefinitionExchange.getId());
		}
	}


	/**
	 * Copies all of the properties of the given definition except for identifying info (id, rv, etc) and amount fields.
	 * Applies the given Amount, AccountingAccount, TransactionType, and CommissionApplyMethod to the created definition.
	 * <p>
	 * DOES NOT save the new definition.
	 */
	private AccountingCommissionDefinition copyDefinition(AccountingCommissionDefinition definition, BigDecimal newCommissionAmount, String newAccountingAccountName, AccountingTransactionType transactionType, AccountingCommissionApplyMethods commissionApplyMethod) {
		AccountingCommissionDefinition newDefinition = new AccountingCommissionDefinition();
		BeanUtils.copyProperties(definition, newDefinition, new String[]{"id", "rv", "createUserId", "createDate", "updateUserId", "updateDate", "newBean", "active", "expenseAccountingAccount"});
		newDefinition.setCommissionAmount(newCommissionAmount);
		newDefinition.setExpenseAccountingAccount(this.accountingAccountService.getAccountingAccountByName(newAccountingAccountName));
		if (transactionType != null) {
			newDefinition.setTransactionType(transactionType);
		}
		if (commissionApplyMethod != null) {
			newDefinition.setCommissionApplyMethod(commissionApplyMethod);
		}
		return newDefinition;
	}


	@Test
	public void testEfpTradeSecFeeFromAlternateTransactionType() {
		testEfpTradeSecFeeCommissionApplication(false);
	}


	@Test
	public void testEfpTradeSecFeeFromEfpTransactionType() {
		testEfpTradeSecFeeCommissionApplication(true);
	}


	private void testEfpTradeSecFeeCommissionApplication(boolean useEfpDefinition) {
		AccountingCommissionDefinition zeroDefinition = null;
		Date tradeDate = DateUtils.toDate("08/29/2017");
		// Currently an EFP SEC Fees commission definition (ID: 1025) exists that duplicates the TRADE SEC Fees (ID: 1031).
		// Test that this definition is not necessary by using the alternate transaction type.
		AccountingCommissionDefinition efpSecFeeCommissionDefinition = this.accountingCommissionService.getAccountingCommissionDefinition(1025);
		Date endDate = efpSecFeeCommissionDefinition.getEndDate();
		efpSecFeeCommissionDefinition.setEndDate(DateUtils.toDate("08/28/2017"));
		if (!useEfpDefinition) {
			this.accountingCommissionService.saveAccountingCommissionDefinition(efpSecFeeCommissionDefinition);
		}
		try {
			// securities
			InvestmentSecurity futureSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESU7");
			InvestmentSecurity fundSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("VOO");

			zeroDefinition = createZeroCommissionDefinition(fundSecurity, this.investmentSetupService.getInvestmentTypeByName(InvestmentType.FUNDS), AccountingAccount.EXECUTING_COMMISSION_EXPENSE);
			// Create accounts
			AccountInfo futureAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
			AccountInfo fundAccountInfo = this.investmentAccountUtils.createHoldingAccount(futureAccountInfo, BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.tradeUtils.getTradeType(TradeType.FUNDS));

			TradeGroup openingEfpTradeGroup = tradeEfp(futureAccountInfo, futureSecurity, fundAccountInfo, fundSecurity, false, tradeDate);
			validateEfpTradeGroup(openingEfpTradeGroup, true, null, new BigDecimal("0.65"), new BigDecimal("2.16"), null); // Commission Definition ID: 1011

			TradeGroup closingEfpTradeGroup = tradeEfp(futureAccountInfo, futureSecurity, fundAccountInfo, fundSecurity, true, tradeDate);
			validateEfpTradeGroup(closingEfpTradeGroup, false, null, new BigDecimal("0.65"), new BigDecimal("2.16"), new BigDecimal("0.0000231"));
		}
		finally {
			if (!useEfpDefinition) {
				// Add back the EFP SEC fee definition
				efpSecFeeCommissionDefinition.setEndDate(endDate);
				this.accountingCommissionService.saveAccountingCommissionDefinition(efpSecFeeCommissionDefinition);
			}
			if (zeroDefinition != null) {
				this.accountingCommissionService.deleteAccountingCommissionDefinition(zeroDefinition.getId());
			}
		}
	}


	@Test
	public void testTradeCommissionForLmeMaturity() {
		// Create new accounts for trading
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		InvestmentSecurity lau7 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("LAU7");
		BusinessCompany goldmanSachs = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		Date tradeDate = DateUtils.toDate("09/18/2017");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(lau7, BigDecimal.ONE, true, tradeDate, goldmanSachs, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);
		BigDecimal lastTransactionPrice = new BigDecimal("0.9");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(lau7, BigDecimal.ONE, false, tradeDate, goldmanSachs, accountInfo).withAverageUnitPrice(lastTransactionPrice).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
		eventSearchForm.setTypeId((short) 6); // Security Maturity
		eventSearchForm.setSecurityId(lau7.getId());
		InvestmentSecurityEvent maturityEvent = CollectionUtils.getFirstElementStrict(this.investmentSecurityEventService.getInvestmentSecurityEventList(eventSearchForm));

		EventJournalGeneratorCommand generatorCommand = new EventJournalGeneratorCommand();
		generatorCommand.setClientAccountId(accountInfo.getClientAccount().getId());
		generatorCommand.setSecurityId(lau7.getId());
		generatorCommand.setEventId(maturityEvent.getId());
		generatorCommand.setGeneratorType(AccountingEventJournalGeneratorTypes.POST);
		Assertions.assertTrue(this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(generatorCommand) > 0);

		AccountingEventJournalSearchForm journalSearchForm = new AccountingEventJournalSearchForm();
		journalSearchForm.setClientInvestmentAccountId(accountInfo.getClientAccount().getId());
		journalSearchForm.setInvestmentSecurityId(lau7.getId());
		List<AccountingEventJournal> journalList = this.accountingEventJournalService.getAccountingEventJournalList(journalSearchForm);
		Assertions.assertEquals(1, journalList.size());
		AccountingJournal journal = this.accountingUtils.getJournal(CollectionUtils.getFirstElementStrict(journalList));
		List<? extends AccountingJournalDetailDefinition> detailList = journal.getJournalDetailList();

		List<AccountingJournalDetailDefinition> expectedDetailList = new ArrayList<>();
		Date transactionDate = DateUtils.toDate("09/20/2017");
		BigDecimal loss = new BigDecimal("2.5");
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(sell).transactionDate(transactionDate).settlementDate(transactionDate);
		expectedDetailList.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				// If positions offset, the last transaction price is used for event detail prices
				// see AccountingEventJournalDetailPriceLastClosePopulator.
				.price(lastTransactionPrice)
				.quantity(BigDecimal.ONE)
				.description("Security Maturity for " + lau7.getSymbol() + " on 09/20/2017")
				.build());
		expectedDetailList.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.quantity(BigDecimal.ONE.negate())
				.description("Security Maturity for " + lau7.getSymbol() + " on 09/20/2017")
				.build());
		expectedDetailList.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.quantity(BigDecimal.ONE)
				.opening(false)
				.baseDebitCredit(loss)
				.localDebitCredit(loss)
				.description(GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_LOSS, lau7)
				.build());
		expectedDetailList.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.opening(true)
				.price(null)
				.quantity(null)
				.baseDebitCredit(loss.negate())
				.localDebitCredit(loss.negate())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, lau7)
				.build());
		AccountingUtils.assertJournalDetailList(expectedDetailList, detailList);
	}
}
