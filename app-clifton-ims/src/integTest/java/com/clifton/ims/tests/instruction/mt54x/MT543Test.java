package com.clifton.ims.tests.instruction.mt54x;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.instruction.InvestmentInstructionUtils;
import com.clifton.ims.tests.trade.TradeCreator;
import com.clifton.ims.tests.utility.PeriodicRepeatingExecutor;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.swift.server.SwiftTestMessageServer;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.autoclient.SwiftExpectedMessage;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import com.clifton.test.util.PropertiesLoader;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


public class MT543Test extends BaseImsIntegrationTest {

	private static final String CONTEXT_PROPERTIES_PATH = PropertiesLoader.getPropertiesFileUri();
	private static final String TEST_DATA_PATH = "com/clifton/ims/tests/swift/server/data";

	private static final String FUTURES_COMMON_SUFFIX = ":16R:TRADDET\n" +
			":94B::TRAD//EXCH/XCBT\n" +
			":98A::SETT//20171017\n" +
			":98A::TRAD//20171017\n" +
			":90B::DEAL//ACTU/USD125,390625\n" +
			":35B:/TS/TYZ17\n" +
			"US 10YR NOTE (CBT)Dec17\n" +
			":16R:FIA\n" +
			":12A::CLAS/ISIT/FUT\n" +
			":11A::DENO//USD\n" +
			":98A::EXPI//20171219\n" +
			":36B::SIZE//UNIT/1000,\n" +
			":16S:FIA\n" +
			":22F::PROC//CLOP\n" +
			":16S:TRADDET\n" +
			":16R:FIAC\n" +
			":36B::SETT//UNIT/6,\n" +
			":97A::SAFE//9H3I\n" +
			":16S:FIAC\n" +
			":16R:SETDET\n" +
			":22F::SETR//TRAD\n" +
			":16R:SETPRTY\n" +
			":95Q::PSET//XX\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::REAG//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:SETPRTY\n" +
			":95P::BUYR//GOLDUS33\n" +
			":16S:SETPRTY\n" +
			":16R:AMT\n" +
			":19A::SETT//USD10,56\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::DEAL//USD0,\n" +
			":16S:AMT\n" +
			":16R:AMT\n" +
			":19A::EXEC//USD10,56\n" +
			":16S:AMT\n" +
			":16S:SETDET\n" +
			"-}";

	private static final String FUTURES_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I543SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//74630\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX;

	private static final String FUTURES_RESPONSE = "{1:F21PPSCUS66AXXX0072074508}{4:{177:1710171454}{451:0}}{1:F01PPSCUS66AXXX0072074508}{2:I543SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//74630\n" +
			":23G:NEWM\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:A1C6427CBB69}}";

	private static final String FUTURES_CANCEL_REQUEST = "{1:F01PPSCUS66AXXX0000000000}{2:I543SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//77777\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//74630\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX;

	private static final String FUTURES_CANCEL_RESPONSE = "{1:F21PPSCUS66AXXX0072074508}{4:{177:1710171454}{451:0}}{1:F01PPSCUS66AXXX0072074508}{2:I543SBOSUS3UXIMSN}{4:\n" +
			":16R:GENL\n" +
			":20C::SEME//77777\n" +
			":23G:CANC\n" +
			":16R:LINK\n" +
			":20C::PREV//74630\n" +
			":16S:LINK\n" +
			":16S:GENL\n" +
			FUTURES_COMMON_SUFFIX +
			"{5:{MAC:00000000}{CHK:A1C6427CBB69}}";

	private static SwiftTestMessageServer swiftTestMessageServer;

	@Resource
	private TradeCreator tradeCreator;

	@Resource
	private TradeService tradeService;

	@Resource
	private InvestmentInstructionUtils investmentInstructionUtils;

	@Resource
	private InvestmentInstructionService investmentInstructionService;

	private Trade trade;

	private Date tradeDate = DateUtils.toDate("10/17/2017", DateUtils.DATE_FORMAT_INPUT);

	private InvestmentInstructionCategory category;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public static void beforeClass() throws Exception {
		swiftTestMessageServer = new SwiftTestMessageServer(CONTEXT_PROPERTIES_PATH, TEST_DATA_PATH);
		swiftTestMessageServer.start();
	}


	@AfterAll
	public static void afterClass() throws Exception {
		swiftTestMessageServer.shutdown();
	}


	@BeforeEach
	public void before() {
		Assertions.assertNotNull(swiftTestMessageServer.sendStartTestSession());
		this.category = this.investmentInstructionUtils.getInvestmentInstructionCategoryByName("Trade Counterparty");
		Assertions.assertNotNull(this.category);
	}


	@AfterEach
	public void after() {
		swiftTestMessageServer.sendStopTestSession();

		InvestmentInstructionStatus errorStatus = this.investmentInstructionUtils.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR);

		List<InvestmentInstructionItem> items = this.investmentInstructionUtils.getInstructionItemList(this.category, this.tradeDate, this.trade).stream()
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.COMPLETED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.CANCELED.name()))
				.filter(i -> !StringUtils.isEqual(i.getStatus().getName(), InvestmentInstructionStatusNames.ERROR.name()))
				.collect(Collectors.toList());
		for (InvestmentInstructionItem item : items) {
			item.setStatus(errorStatus);
			this.investmentInstructionUtils.saveInvestmentInstructionItem(item);
		}
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Test
	public void futuresTest() throws Exception {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.STATE_STREET_BANK,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		this.trade = this.tradeCreator.createFuturesTrade(
				accountInfo,
				"TYZ17",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				6.00000000,
				false,
				TradeDestination.MANUAL,
				BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		this.trade = this.approveTrade(this.trade);

		// update the trade fill average price.
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(this.trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		TradeFill tradeFill = tradeFills.get(0);
		tradeFill.setQuantity(new BigDecimal("6"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("125.390625"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("752343.78"));
		this.tradeService.saveTradeFill(tradeFill);
		Assertions.assertNotNull(this.trade);

		this.trade = this.fullyExecuteTrade(this.trade);

		// setup test case
		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create().
				with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_REQUEST)
								.withReplacement("97A", ":SAFE//9H3I", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("22F", ":PROC//CLOP", ":PROC//OPEP")
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_RESPONSE)
								.withReplacement("97A", ":SAFE//9H3I", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("22F", ":PROC//CLOP", ":PROC//OPEP")
								.withCopyRequestValue("20C")
								.build()
				).build();
		swiftTestMessageServer.sendSwiftTestCase(swiftTestCase);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, this.trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, this.trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, this.trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		Assertions.assertNotNull(investmentInstruction);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, this.trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertFalse(instructionItems.length == 0, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(10, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 10, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");
	}


	@Test
	public void futuresCancelTest() throws Exception {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.STATE_STREET_BANK,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		this.trade = this.tradeCreator.createFuturesTrade(
				accountInfo,
				"TYZ17",
				DateUtils.fromDate(this.tradeDate, DateUtils.DATE_FORMAT_INPUT),
				6.00000000,
				false,
				TradeDestination.MANUAL,
				BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		this.trade = this.approveTrade(this.trade);

		// update the trade fill average price.
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(this.trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		TradeFill tradeFill = tradeFills.get(0);
		tradeFill.setQuantity(new BigDecimal("6"));
		tradeFill.setNotionalUnitPrice(new BigDecimal("125.390625"));
		tradeFill.setNotionalTotalPrice(new BigDecimal("752343.78"));
		this.tradeService.saveTradeFill(tradeFill);
		Assertions.assertNotNull(this.trade);

		this.trade = this.fullyExecuteTrade(this.trade);

		// setup test case
		SwiftTestCase swiftTestCase = SwiftTestCase.SwiftTestCaseBuilder.
				create()
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_REQUEST)
								.withReplacement("97A", ":SAFE//9H3I", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("22F", ":PROC//CLOP", ":PROC//OPEP")
								.withCopyRequestValue("20C")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_RESPONSE)
								.withReplacement("97A", ":SAFE//9H3I", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("22F", ":PROC//CLOP", ":PROC//OPEP")
								.withCopyRequestValue("20C")
								.build()
				)
				.with(
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_CANCEL_REQUEST)
								.withReplacement("97A", ":SAFE//9H3I", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("22F", ":PROC//CLOP", ":PROC//OPEP")
								.withCopyRequestValue("20C", "SEME")
								.withCopyRequestValue("20C", "PREV")
								.build(),
						SwiftExpectedMessage.SwiftExpectedMessageBuilder
								.create(FUTURES_CANCEL_RESPONSE)
								.withReplacement("97A", ":SAFE//9H3I", ":SAFE//" + accountInfo.getCustodianAccount().getNumber())
								.withReplacement("22F", ":PROC//CLOP", ":PROC//OPEP")
								.withCopyRequestValue("20C", "SEME")
								.withCopyRequestValue("20C", "PREV")
								.build()
				)
				.build();
		swiftTestMessageServer.sendSwiftTestCase(swiftTestCase);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, null).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s].", this.category.getLabel(), this.tradeDate));

		// creates instruction items
		this.investmentInstructionUtils.processInvestmentInstructionList(this.category, this.tradeDate);

		Assertions.assertTrue(this.investmentInstructionUtils.getSendingInstructionItemList(this.category, this.tradeDate, this.trade).isEmpty(), String.format("Instruction Items are in sending status for category: [%s] date: [%s] trade: [%s]", this.category.getLabel(), this.tradeDate, this.trade.getIdentity()));
		Assertions.assertFalse(this.investmentInstructionUtils.isRegenerating(this.category, this.tradeDate), "Instruction Items are regenerating.");
		Assertions.assertFalse(CollectionUtils.isEmpty(this.investmentInstructionUtils.getInstructionDefinitionList(this.category)), "Should have active instruction definitions.");

		// make sure swift contact exists.
		InvestmentInstruction investmentInstruction = this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, this.trade).stream()
				.map(InvestmentInstructionItem::getInstruction)
				.findFirst()
				.orElse(null);
		Assertions.assertNotNull(investmentInstruction);
		this.investmentInstructionUtils.createSwiftInstructionContact(investmentInstruction);

		int[] instructionItems = CollectionUtils.getStream(this.investmentInstructionUtils.getOpenInstructionItems(this.category, this.tradeDate, this.trade))
				.mapToInt(InvestmentInstructionItem::getId).toArray();
		Assertions.assertFalse(instructionItems.length == 0, "Should have Open Instruction Items.");
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), 10, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.trade, InvestmentInstructionStatusNames.COMPLETED, instructionItems), "All instructions items should be [COMPLETED].");

		// get items to cancel.
		Integer[] items = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("Trade", this.trade.getId()).stream()
				.map(InvestmentInstructionItem::getId)
				.toArray(Integer[]::new);

		// cancel items.
		this.investmentInstructionService.cancelInvestmentInstructionItemList(items);
		int[] pendingCancelItems = this.investmentInstructionService.getInvestmentInstructionItemListForEntity("Trade", this.trade.getId()).stream()
				.filter(i -> InvestmentInstructionStatusNames.PENDING_CANCEL == i.getStatus().getInvestmentInstructionStatusName())
				.mapToInt(InvestmentInstructionItem::getId)
				.toArray();
		Assertions.assertTrue(pendingCancelItems.length > 0);

		// send PENDING_CANCEL items.
		this.investmentInstructionUtils.sendInvestmentInstructionItemList(instructionItems);

		// wait for swift server to send all responses.
		SwiftTestStatusMessage statusMessage = swiftTestMessageServer.awaitStatusMessage(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, Duration.of(20, ChronoUnit.SECONDS));
		Assertions.assertEquals(SwiftTestMessageStatuses.LAST_TEMPLATE_RESPONSE, statusMessage.getStatus(), statusMessage.getPayload());

		// wait for cancelled status.
		PeriodicRepeatingExecutor.await(() -> this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.trade, InvestmentInstructionStatusNames.CANCELED, instructionItems), 20, 1);
		Assertions.assertTrue(this.investmentInstructionUtils.areInstructionItemsWithStatus(this.category, this.tradeDate, this.trade, InvestmentInstructionStatusNames.CANCELED, instructionItems), "All instructions items should be [CANCELLED].");
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	private Trade approveTrade(Trade trade) {
		this.tradeUtils.approveTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}


	private Trade fullyExecuteTrade(Trade trade) {
		this.tradeUtils.fullyExecuteTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}
}
