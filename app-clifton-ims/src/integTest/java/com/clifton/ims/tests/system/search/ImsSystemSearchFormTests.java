package com.clifton.ims.tests.system.search;


import com.clifton.test.system.search.BaseSystemSearchFormTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration(locations = "classpath:com/clifton/ims/tests/core/dataaccess/SessionFactoryTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ImsSystemSearchFormTests extends BaseSystemSearchFormTests {

	private static final String[] PACKAGES_TO_EXCLUDE_FROM_HIBERNATE_MAPPING_TEST = {"com.clifton.integration", "com.clifton.reconcile", "com.clifton.reconciliation"};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String[] getPackagesToExcludeFromHibernateMappingTest() {
		return PACKAGES_TO_EXCLUDE_FROM_HIBERNATE_MAPPING_TEST;
	}
}
