package com.clifton.ims.tests.portfolio.cashflow.ldi.upload;

import com.clifton.business.client.BusinessClient;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.bind.WebBindingHandler;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlow;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowGroup;
import com.clifton.portfolio.cashflow.ldi.PortfolioCashFlowService;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowGroupSearchForm;
import com.clifton.portfolio.cashflow.ldi.search.PortfolioCashFlowSearchForm;
import com.clifton.portfolio.cashflow.ldi.upload.PortfolioCashFlowUploadCommand;
import com.clifton.portfolio.cashflow.ldi.upload.PortfolioCashFlowUploadService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.core.util.MathUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;


/**
 * A set of tests to verify that the PortfolioCashFlow Upload feature is functional.
 *
 * @author davidi
 */
public class PortfolioCashFlowUploadServiceTests extends BaseImsIntegrationTest {

	@Resource
	InvestmentAccountService investmentAccountService;

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	PortfolioCashFlowService portfolioCashFlowService;

	@Resource
	PortfolioCashFlowUploadService portfolioCashFlowUploadService;

	@Resource
	InvestmentAccountUtils investmentAccountUtils;

	@Resource
	SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testUploadPortfolioCashFlowUploadFile_multiple() {
		BusinessClient client = this.businessUtils.createBusinessClient();
		InvestmentAccount testAccount = this.investmentAccountUtils.createClientAccount(client);

		PortfolioCashFlowUploadCommand uploadCommand = new PortfolioCashFlowUploadCommand();
		uploadCommand.setInvestmentAccount(testAccount);
		uploadCommand.setFile(new MultipartFileImpl("src/integTest/java/com/clifton/ims/tests/portfolio/cashflow/ldi/upload/MultiplePortfolioCashFlowUploadFile.xls"));
		uploadCommand.setApplyUnmappedColumnNamesAsSeparateCashFlowTypes(true);
		uploadCommand.setEndActiveGroup(true);
		uploadSampleFileForCommand(uploadCommand);

		List<PortfolioCashFlowGroup> groupList = getPortfolioCashFlowGroupListForAccount(testAccount.getNumber(), "USD", "Rolling");
		Assertions.assertEquals(1, groupList.size());

		PortfolioCashFlowGroup group = groupList.get(0);
		Assertions.assertEquals("USD", group.getCurrencyInvestmentSecurity().getSymbol());
		Assertions.assertEquals(DateUtils.toDate("01/01/2019"), group.getAsOfDate());
		Assertions.assertEquals(DateUtils.toDate("03/01/2019"), group.getEffectiveStartDate());
		Assertions.assertEquals(DateUtils.toDate("12/31/2019"), group.getEffectiveEndDate());
		Assertions.assertEquals("Rolling", group.getCashFlowGroupType().getValue());

		List<PortfolioCashFlow> portfolioCashFlowList = getPortfolioCashFlowListForGroup(groupList.get(0));
		Assertions.assertEquals(15, portfolioCashFlowList.size());

		List<PortfolioCashFlow> projectedFlowSublist = CollectionUtils.getFiltered(portfolioCashFlowList, entry -> "Projected Flow".equals(entry.getCashFlowType().getName()));
		sortPortfolioCashFlowList(projectedFlowSublist);
		Assertions.assertEquals(5, projectedFlowSublist.size());
		validateCashFlowData(projectedFlowSublist.get(0), group, "Projected Flow", BigDecimal.valueOf(1000.00), 2019, 1);
		validateCashFlowData(projectedFlowSublist.get(1), group, "Projected Flow", BigDecimal.valueOf(1100.00), 2020, 13);
		validateCashFlowData(projectedFlowSublist.get(2), group, "Projected Flow", BigDecimal.valueOf(1200.00), 2021, 25);
		validateCashFlowData(projectedFlowSublist.get(3), group, "Projected Flow", BigDecimal.valueOf(1300.00), 2022, 37);
		validateCashFlowData(projectedFlowSublist.get(4), group, "Projected Flow", BigDecimal.valueOf(1400.00), 2023, 49);


		List<PortfolioCashFlow> actualFlowSublist = CollectionUtils.getFiltered(portfolioCashFlowList, entry -> "Actual Flow".equals(entry.getCashFlowType().getName()));
		sortPortfolioCashFlowList(actualFlowSublist);
		Assertions.assertEquals(5, actualFlowSublist.size());
		validateCashFlowData(actualFlowSublist.get(0), group, "Actual Flow", BigDecimal.valueOf(3000.00), 2019, 1);
		validateCashFlowData(actualFlowSublist.get(1), group, "Actual Flow", BigDecimal.valueOf(3100.00), 2020, 13);
		validateCashFlowData(actualFlowSublist.get(2), group, "Actual Flow", BigDecimal.valueOf(3200.00), 2021, 25);
		validateCashFlowData(actualFlowSublist.get(3), group, "Actual Flow", BigDecimal.valueOf(3300.00), 2022, 37);
		validateCashFlowData(actualFlowSublist.get(4), group, "Actual Flow", BigDecimal.valueOf(3400.00), 2023, 49);


		List<PortfolioCashFlow> serviceCostSublist = CollectionUtils.getFiltered(portfolioCashFlowList, entry -> "Service Cost".equals(entry.getCashFlowType().getName()));
		sortPortfolioCashFlowList(serviceCostSublist);
		Assertions.assertEquals(5, serviceCostSublist.size());
		validateCashFlowData(serviceCostSublist.get(0), group, "Service Cost", BigDecimal.valueOf(2000.00), 2019, 1);
		validateCashFlowData(serviceCostSublist.get(1), group, "Service Cost", BigDecimal.valueOf(2100.00), 2020, 13);
		validateCashFlowData(serviceCostSublist.get(2), group, "Service Cost", BigDecimal.valueOf(2200.00), 2021, 25);
		validateCashFlowData(serviceCostSublist.get(3), group, "Service Cost", BigDecimal.valueOf(2300.00), 2022, 37);
		validateCashFlowData(serviceCostSublist.get(4), group, "Service Cost", BigDecimal.valueOf(2400.00), 2023, 49);
	}


	@Test
	public void testUploadPortfolioCashFlowUploadFile() {
		BusinessClient client = this.businessUtils.createBusinessClient();
		InvestmentAccount testAccount = this.investmentAccountUtils.createClientAccount(client);
		PortfolioCashFlowUploadCommand uploadCommand = new PortfolioCashFlowUploadCommand();
		uploadCommand.setInvestmentAccount(testAccount);
		uploadCommand.setFile(new MultipartFileImpl("src/integTest/java/com/clifton/ims/tests/portfolio/cashflow/ldi/upload/SimplePortfolioCashFlowUploadFile.xls"));
		uploadCommand.setApplyUnmappedColumnNamesAsSeparateCashFlowTypes(false);
		uploadCommand.setEndActiveGroup(true);
		uploadSampleFileForCommand(uploadCommand);

		List<PortfolioCashFlowGroup> groupList = getPortfolioCashFlowGroupListForAccount(testAccount.getNumber(), "USD", "Rolling");
		Assertions.assertEquals(1, groupList.size());

		PortfolioCashFlowGroup group = groupList.get(0);
		Assertions.assertEquals("USD", group.getCurrencyInvestmentSecurity().getSymbol());
		Assertions.assertEquals(DateUtils.toDate("01/01/2021"), group.getAsOfDate());
		Assertions.assertEquals(DateUtils.toDate("03/01/2021"), group.getEffectiveStartDate());
		Assertions.assertNull(group.getEffectiveEndDate());
		Assertions.assertEquals("Rolling", group.getCashFlowGroupType().getValue());

		List<PortfolioCashFlow> portfolioCashFlowList = getPortfolioCashFlowListForGroup(groupList.get(0));
		Assertions.assertEquals(5, portfolioCashFlowList.size());

		sortPortfolioCashFlowList(portfolioCashFlowList);

		// validateCashFlowData(PortfolioCashFlow cashFlow, PortfolioCashFlowGroup expectedCashFlowGroup, String expectedCashFlowTypeName, BigDecimal expectedCashFlowAmount, int expectedCashFlowYear, int expectedStartingMonthNumber)
		validateCashFlowData(portfolioCashFlowList.get(0), group, "Projected Flow", BigDecimal.valueOf(1000.00), 2020, 1);
		validateCashFlowData(portfolioCashFlowList.get(1), group, "Projected Flow", BigDecimal.valueOf(2000.00), 2021, 13);
		validateCashFlowData(portfolioCashFlowList.get(2), group, "Projected Flow", BigDecimal.valueOf(3000.00), 2022, 25);
		validateCashFlowData(portfolioCashFlowList.get(3), group, "Projected Flow", BigDecimal.valueOf(4000.00), 2023, 37);
		validateCashFlowData(portfolioCashFlowList.get(4), group, "Projected Flow", BigDecimal.valueOf(5000.00), 2024, 49);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<PortfolioCashFlowGroup> getPortfolioCashFlowGroupListForAccount(String accountNumber, String currencySymbol, String groupTypeName) {
		PortfolioCashFlowGroupSearchForm searchForm = new PortfolioCashFlowGroupSearchForm();
		searchForm.setInvestmentAccountNumberEquals(accountNumber);
		if (StringUtils.isNotEmpty(currencySymbol)) {
			searchForm.setCurrencySymbol(currencySymbol);
		}
		if (StringUtils.isNotEmpty(groupTypeName)) {
			searchForm.setGroupType(groupTypeName);
		}
		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowGroupList(searchForm), 3);
	}


	private List<PortfolioCashFlow> getPortfolioCashFlowListForGroup(PortfolioCashFlowGroup portfolioCashFlowGroup) {
		PortfolioCashFlowSearchForm searchForm = new PortfolioCashFlowSearchForm();
		searchForm.setCashFlowGroupId(portfolioCashFlowGroup.getId());

		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.portfolioCashFlowService.getPortfolioCashFlowList(searchForm), 3);
	}


	private void validateCashFlowData(PortfolioCashFlow cashFlow, PortfolioCashFlowGroup expectedCashFlowGroup, String expectedCashFlowTypeName, BigDecimal expectedCashFlowAmount, int expectedCashFlowYear, int expectedStartingMonthNumber) {
		Assertions.assertEquals(expectedCashFlowGroup, cashFlow.getCashFlowGroup());
		Assertions.assertEquals(expectedCashFlowTypeName, cashFlow.getCashFlowType().getName());
		Assertions.assertEquals(expectedCashFlowAmount.setScale(2, RoundingMode.HALF_UP), cashFlow.getCashFlowAmount().setScale(2, RoundingMode.HALF_UP));
		Assertions.assertEquals((short) expectedCashFlowYear, cashFlow.getCashFlowYear());
		Assertions.assertEquals((short) expectedStartingMonthNumber, cashFlow.getStartingMonthNumber());
	}


	private List<PortfolioCashFlow> sortPortfolioCashFlowList(List<PortfolioCashFlow> cashFlowList) {
		return CollectionUtils.sort(cashFlowList, (entryA, entryB) -> MathUtils.compare(entryA.getCashFlowYear(), entryB.getCashFlowYear()));
	}


	private void uploadSampleFileForCommand(PortfolioCashFlowUploadCommand uploadCommand) {
		RequestOverrideHandler.serviceCallWithOverrides(() -> {
					this.portfolioCashFlowUploadService.uploadPortfolioCashFlowUploadFile(uploadCommand);
					return true;
				},
				new RequestOverrideHandler.EntityPropertyOverrides[]{
						// exclude investmentAccount.aumCalculatorBean because it is a calculated getter
						new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, false, "aumCalculatorBean"),
						new RequestOverrideHandler.EntityPropertyOverrides(SystemBean.class, false, "entityModifyCondition")
				},
				new RequestOverrideHandler.RequestParameterOverrides(WebBindingHandler.ENABLE_VALIDATING_BINDING_PARAMETER, "true"),
				new RequestOverrideHandler.RequestParameterOverrides(WebBindingHandler.DISABLE_VALIDATING_BINDING_VALIDATION_PARAMETER, "true"));
	}
}
