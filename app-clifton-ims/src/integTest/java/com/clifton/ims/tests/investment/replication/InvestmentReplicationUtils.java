package com.clifton.ims.tests.investment.replication;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityTargetAdjustmentTypes;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * {@link InvestmentReplicationUtils} provides convenience methods for working with {@link com.clifton.investment.replication.InvestmentReplication}s.
 *
 * @author michaelm
 */
@Component
public class InvestmentReplicationUtils {

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private InvestmentReplicationService investmentReplicationService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityGroupService investmentSecurityGroupService;


	////////////////////////////////////////////////////////////////////////////
	////////              InvestmentReplication Utilities               ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentReplication createReplicationWithRandomTestNamePrefix(String replicationName, String replicationTypeName, String dynamicCalculatorBeanName) {
		return createReplicationWithRandomTestNamePrefix(replicationName, replicationTypeName, dynamicCalculatorBeanName, null);
	}


	public InvestmentReplication createReplicationWithRandomTestNamePrefix(String replicationName, String replicationTypeName, String dynamicCalculatorBeanName, String[] childAllocationNames) {
		InvestmentReplication replication = new InvestmentReplication();
		replication.setName(RandomUtils.randomNameWithPrefix("TEST " + replicationName));
		replication.setType(this.investmentReplicationService.getInvestmentReplicationTypeByName(replicationTypeName));
		replication.setDynamicCalculatorBean(this.systemBeanService.getSystemBeanByName(dynamicCalculatorBeanName));
		if (!ArrayUtils.isEmpty(childAllocationNames)) {
			replication.setAllocationList(getDefaultChildAllocations(replication, childAllocationNames));
		}
		replication.setInactive(true); // to avoid rebuilding for previous business day in InvestmentReplicationAllocationObserver
		return this.investmentReplicationService.saveInvestmentReplication(replication);
	}


	public InvestmentReplication getExistingReplicationByName(String replicationName) {
		InvestmentReplicationSearchForm searchForm = new InvestmentReplicationSearchForm();
		searchForm.setNameEquals(replicationName);
		return CollectionUtils.getFirstElement(this.investmentReplicationService.getInvestmentReplicationList(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////         InvestmentReplicationAllocation Utilities          ////////
	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentReplicationAllocation> getDefaultChildAllocations(InvestmentReplication parentReplication, String[] childAllocationNames) {
		InvestmentReplicationSearchForm searchForm = new InvestmentReplicationSearchForm();
		searchForm.setNames(childAllocationNames);
		List<InvestmentReplication> childReplications = this.investmentReplicationService.getInvestmentReplicationList(searchForm);
		List<InvestmentReplicationAllocation> allocationList = new ArrayList<>();
		allocationList.add(createChildAllocation(parentReplication, childReplications.get(0), BigDecimal.valueOf(75)));
		allocationList.add(createChildAllocation(parentReplication, childReplications.get(1), BigDecimal.valueOf(25)));
		return allocationList;
	}


	public InvestmentReplicationAllocation createSecurityUnderlyingReplicationAllocation(String underlyingSymbol, String securityGroupName) {
		InvestmentReplicationAllocation replicationAllocation = new InvestmentReplicationAllocation();
		replicationAllocation.setAllocationWeight(BigDecimal.valueOf(100));
		replicationAllocation.setPortfolioSelectedCurrentSecurity(true);
		replicationAllocation.setSecurityUnderlying(true);
		replicationAllocation.setCurrentSecurityCalculatorBean(this.systemBeanService.getSystemBeanByName("Default Current Contract Calculator"));
		replicationAllocation.setCurrentSecurityTargetAdjustmentType(InvestmentCurrentSecurityTargetAdjustmentTypes.DIFFERENCE_ALL);
		replicationAllocation.setRebalanceAllocationMax(BigDecimal.valueOf(2));
		replicationAllocation.setRebalanceAllocationMin(BigDecimal.valueOf(2));
		replicationAllocation.setReplicationSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol(underlyingSymbol, false));
		replicationAllocation.setReplicationSecurityGroup(this.investmentSecurityGroupService.getInvestmentSecurityGroupByName(securityGroupName));
		return replicationAllocation;
	}


	private InvestmentReplicationAllocation createChildAllocation(InvestmentReplication parentReplication, InvestmentReplication childReplication, BigDecimal allocationWeight) {
		InvestmentReplicationAllocation allocation = new InvestmentReplicationAllocation();
		allocation.setReplication(parentReplication);
		allocation.setChildReplication(childReplication);
		allocation.setAllocationWeight(allocationWeight);
		allocation.setReplicationTypeOverride(childReplication.getType()); // avoid stack overflow with recursive calls during serialization
		return allocation;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}
}
