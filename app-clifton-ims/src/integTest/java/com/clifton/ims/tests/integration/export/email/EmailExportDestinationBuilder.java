package com.clifton.ims.tests.integration.export.email;

import com.clifton.business.contact.BusinessContact;
import com.clifton.core.util.StringUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author theodorez on 10/1/2015.
 */
public class EmailExportDestinationBuilder {

	private ExportDefinition exportDefinition;
	private List<Integer> toContactIds;
	private List<Integer> ccContactIds;
	private List<Integer> bccContactIds;
	private String randomString;
	private String emailText;
	private String emailSubject;

	private SystemBeanBuilder beanBuilder;


	private ExportDefinitionService exportDefinitionService;


	public EmailExportDestinationBuilder(SystemBeanService systemBeanService, ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
		this.toContactIds = new ArrayList<>();
		this.ccContactIds = new ArrayList<>();
		this.bccContactIds = new ArrayList<>();
		this.beanBuilder = SystemBeanBuilder.newBuilder(null, "Email", systemBeanService);
	}


	public EmailExportDestinationBuilder exportDefinition(ExportDefinition exportDefinition) {
		this.exportDefinition = exportDefinition;
		return this;
	}


	public EmailExportDestinationBuilder from(BusinessContact from) {
		return from(from.getId());
	}


	public EmailExportDestinationBuilder from(Integer fromId) {
		this.beanBuilder.addProperty("From Contact", fromId.toString());
		return this;
	}


	public EmailExportDestinationBuilder addToContact(BusinessContact contact) {
		return addToContactId(contact.getId());
	}


	public EmailExportDestinationBuilder addToContactId(Integer contactId) {
		this.toContactIds.add(contactId);
		toContactIds(this.toContactIds);
		return this;
	}


	public EmailExportDestinationBuilder toContacts(List<BusinessContact> toContacts) {
		List<Integer> contactIds = new ArrayList<>();
		for (BusinessContact contact : toContacts) {
			contactIds.add(contact.getId());
		}
		return toContactIds(contactIds);
	}


	public EmailExportDestinationBuilder toContactIds(List<Integer> toContactIds) {
		this.beanBuilder.addProperty("To Contacts", StringUtils.collectionToCommaDelimitedString(toContactIds));
		this.toContactIds = toContactIds;
		return this;
	}


	public EmailExportDestinationBuilder addCcContact(BusinessContact contact) {
		return addCcContactId(contact.getId());
	}


	public EmailExportDestinationBuilder addCcContactId(Integer contactId) {
		this.ccContactIds.add(contactId);
		ccContactIds(this.ccContactIds);
		return this;
	}


	public EmailExportDestinationBuilder ccContacts(List<BusinessContact> ccContacts) {
		List<Integer> contactIds = new ArrayList<>();
		for (BusinessContact contact : ccContacts) {
			contactIds.add(contact.getId());
		}
		return ccContactIds(contactIds);
	}


	public EmailExportDestinationBuilder ccContactIds(List<Integer> ccContactIds) {
		this.beanBuilder.addProperty("CC Contacts", StringUtils.collectionToDelimitedString(ccContactIds, ","));
		this.ccContactIds = ccContactIds;
		return this;
	}


	public EmailExportDestinationBuilder addBccContact(BusinessContact contact) {
		return addBccContactId(contact.getId());
	}


	public EmailExportDestinationBuilder addBccContactId(Integer contactId) {
		this.bccContactIds.add(contactId);
		bccContactIds(this.bccContactIds);
		return this;
	}


	public EmailExportDestinationBuilder bccContacts(List<BusinessContact> bccContacts) {
		List<Integer> contactIds = new ArrayList<>();
		for (BusinessContact contact : bccContacts) {
			contactIds.add(contact.getId());
		}
		return bccContactIds(contactIds);
	}


	public EmailExportDestinationBuilder bccContactIds(List<Integer> bccContactIds) {
		this.beanBuilder.addProperty("BCC Contacts", StringUtils.collectionToCommaDelimitedString(bccContactIds));
		this.bccContactIds = bccContactIds;
		return this;
	}


	public EmailExportDestinationBuilder emailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
		this.beanBuilder.addProperty("Subject", emailSubject);
		return this;
	}


	public EmailExportDestinationBuilder emailText(String emailText) {
		this.emailText = emailText;
		this.beanBuilder.addProperty("Text", emailText);
		return this;
	}


	public EmailExportDestinationBuilder sender(BusinessContact sender) {
		return sender(sender.getId());
	}


	public EmailExportDestinationBuilder sender(Integer senderId) {
		this.beanBuilder.addProperty("Sender Contact", senderId.toString());
		return this;
	}


	public EmailExportDestinationBuilder hostname(String hostname) {
		this.beanBuilder.addProperty("SMTP Host Url", hostname);
		return this;
	}


	public EmailExportDestinationBuilder port(Integer port) {
		this.beanBuilder.addProperty("SMTP Host Port", port.toString());
		return this;
	}


	public EmailExportDestinationBuilder randomString(String string) {
		this.randomString = string;
		return this;
	}


	public List<Integer> getToContactIds() {
		return this.toContactIds;
	}


	public List<Integer> getCcContactIds() {
		return this.ccContactIds;
	}


	public List<Integer> getBccContactIds() {
		return this.bccContactIds;
	}


	public ExportDefinitionDestination build() {
		if (!StringUtils.isEmpty(this.randomString)) {
			emailSubject(this.emailSubject + this.randomString);
			emailText(this.emailText + this.randomString);
		}
		SystemBean bean = this.beanBuilder.build();
		bean.setName("Test destination - " + RandomUtils.randomNumber());
		ExportDefinitionDestination destination = new ExportDefinitionDestination();
		destination.setDestinationSystemBean(bean);
		destination.setDefinition(this.exportDefinition);
		this.exportDefinitionService.saveExportDefinitionDestination(destination);
		return destination;
	}
}
