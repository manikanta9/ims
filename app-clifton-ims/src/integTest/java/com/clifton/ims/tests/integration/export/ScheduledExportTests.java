package com.clifton.ims.tests.integration.export;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.calendar.schedule.BusinessDayConventions;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.config.RunnerConfigService;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.ExportDefinitionType;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunSourceTypes;
import com.clifton.export.run.ExportRunStatus.ExportRunStatusNames;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class ScheduledExportTests extends BaseImsIntegrationTest {

	// Make sure we get an initial time at least a minute into the future to avoid the task getting missed by the runner handler.
	private static final int INITIAL_SCHEDULE_TIME_SEC = 70;
	private static final int RETRY_INTERVAL_SEC = 60;
	private static final int RETRY_COUNT = 1;
	private static final int TIME_BUFFER_SEC = 5;

	// sleep time set to fraction of retry interval for case where the execution and lookup are staggered by the system
	private static final int POLL_LIMIT_COUNT = 3; // wait a maximum of 5x RETRY_INTERVAL_SEC

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private CalendarScheduleService calendarScheduleService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private ExportDefinitionService exportDefinitionService;

	@Resource
	private ExportRunService exportRunService;

	@Resource
	private RunnerConfigService runnerConfigService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private SystemConditionService systemConditionService;


	/**
	 * This integration test creates a dummy export that is expected to fail. The export is configured to retry 3 times. This test
	 * verifies that the initial schedule and the subsequent retries all execute on time.
	 */
	@Test
	public void testScheduledExportWithRetries() throws InterruptedException {
		Date scheduledRunTime = DateUtils.addSeconds(new Date(), INITIAL_SCHEDULE_TIME_SEC);
		ExportDefinitionType opsType = this.exportDefinitionService.getExportDefinitionTypeByName("Operations");

		CalendarSchedule calendarSchedule = createSchedule(scheduledRunTime);

		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(calendarSchedule, opsType);

		//Set the Destination
		createFtpDestination(exportDefinition);

		//Set the Contents
		createContent(exportDefinition);

		//Rerun the scheduler to trigger the schedule to be scheduled
		this.runnerConfigService.restartRunnerHandler();

		List<ExportRunHistory> runHistoryList = getHistories(exportDefinition, new Date());
		Assertions.assertNotNull(runHistoryList, "Run History list is empty!");
		Assertions.assertEquals(1 + RETRY_COUNT, runHistoryList.size(), "The number of run histories is not what it should be!");
		runHistoryList = BeanUtils.sortWithFunction(runHistoryList, ExportRunHistory::getId, true);

		//Assert the initial scheduled run
		ExportRunHistory firstScheduledRun = runHistoryList.get(0);
		assertRunHistory(firstScheduledRun, ExportRunSourceTypes.SCHEDULE, ExportRunStatusNames.FAILED_ERROR, scheduledRunTime, 0);

		//Assert all of the retry runs
		Date lastRunEndDate = firstScheduledRun.getEndDate();
		for (int retryAttempt = 1; retryAttempt <= RETRY_COUNT; retryAttempt++) {
			ExportRunHistory runHistory = runHistoryList.get(retryAttempt);
			Date expectedRunDate = DateUtils.addSeconds(lastRunEndDate, RETRY_INTERVAL_SEC);
			assertRunHistory(runHistory, ExportRunSourceTypes.RETRY, ExportRunStatusNames.FAILED_ERROR, expectedRunDate, retryAttempt);
			lastRunEndDate = runHistory.getEndDate();
		}
	}


	private List<ExportRunHistory> getHistories(ExportDefinition definition, Date date) throws InterruptedException {
		int counter = 0;
		Date day = DateUtils.clearTime(date);
		do {
			counter++;
			// the first sleep will be longer since we do not expect the retries to complete for ((RETRY_INTERVAL_SEC * RETRY_COUNT) + INITIAL_SCHEDULE_TIME_SEC)), subsequent wait 1/20 RETRY_INTERVAL_SEC
			TimeUnit.SECONDS.sleep(counter == 1 ? INITIAL_SCHEDULE_TIME_SEC + (RETRY_COUNT * RETRY_INTERVAL_SEC) : RETRY_INTERVAL_SEC / 20);
			List<ExportRunHistory> histories = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.exportRunService.getExportRunHistoryListByDate(definition.getId(), day), "id|exportRunSourceType|status|retryAttemptNumber|scheduledDate|startDate|endDate");
			//Poll until the number of retries desired is reached, will be 1 more than retry count to account for initial run
			if (!CollectionUtils.isEmpty(histories) && (histories.size() >= (1 + RETRY_COUNT))) {
				return histories;
			}
		}
		while (counter < POLL_LIMIT_COUNT);
		return null;
	}


	private void assertRunHistory(ExportRunHistory runHistory, ExportRunSourceTypes expectedRunSourceType, ExportRunStatusNames expectedStatus, Date expectedRunDateTime, int expectedRetryAttempt) {
		Assertions.assertEquals(expectedRunSourceType, runHistory.getExportRunSourceType());
		Assertions.assertEquals(this.exportRunService.getExportRunStatusByName(expectedStatus), runHistory.getStatus());
		Assertions.assertEquals((short) expectedRetryAttempt, runHistory.getRetryAttemptNumber().shortValue());

		Date minRunDate = DateUtils.addSeconds(expectedRunDateTime, -TIME_BUFFER_SEC);
		Date maxRunDate = DateUtils.addSeconds(expectedRunDateTime, TIME_BUFFER_SEC);

		assertTime(runHistory.getScheduledDate(), minRunDate, maxRunDate);
		assertTime(runHistory.getStartDate(), minRunDate, maxRunDate);
		assertTime(runHistory.getEndDate(), minRunDate, maxRunDate);
	}


	private void assertTime(Date time, Date startTime, Date endTime) {
		String errorMsg = DateUtils.fromDate(time, Time.TIME_FORMAT_FULL) + " is not between " + DateUtils.fromDate(startTime, Time.TIME_FORMAT_FULL) + " and "
				+ DateUtils.fromDate(endTime, Time.TIME_FORMAT_FULL);
		Assertions.assertTrue(DateUtils.isDateBetween(time, startTime, endTime, true), errorMsg);
	}


	private ExportDefinition createExportDefinition(CalendarSchedule calendarSchedule, ExportDefinitionType type) {
		ExportDefinition exportDefinition = new ExportDefinition();
		exportDefinition.setName("JPM Integration Test - " + RandomUtils.randomNumber());
		exportDefinition.setType(type);
		exportDefinition.setRecurrenceSchedule(calendarSchedule);
		exportDefinition.setRetryCount(RETRY_COUNT);
		exportDefinition.setRetryDelayInSeconds(RETRY_INTERVAL_SEC);
		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.exportDefinitionService.saveExportDefinition(exportDefinition), 2);
	}


	private ExportDefinitionDestination createFtpDestination(ExportDefinition exportDefinition) {
		return new ExportFtpDestinationBuilder("fakehost", this.systemBeanService, this.exportDefinitionService)
				.ftpUsername("fakeusername")
				.ftpPassword("fakepassword")
				.ftpRemoteFolder("/")
				.ftpProtocol("FTP")
				.exportDefinition(exportDefinition)
				.build();
	}


	private ExportDefinitionContent createContent(ExportDefinition exportDefinition) {
		BusinessCompany custodian = this.businessCompanyService.getBusinessCompanyByTypeAndName("Custodian", "Amalgamated Bank of New York");

		SystemBean contentSystemBean = SystemBeanBuilder.newBuilder(null, "Trade Export Generator", this.systemBeanService)
				.addProperty("File Format Type", "JP_MORGAN")
				.addProperty("Custodian", custodian.getId() + "")
				.addProperty("Date generation option", "DAYS_BACK")
				.addProperty("Days from today", 1 + "")
				.addProperty("Reconciled Only", "false")
				.build();

		ExportDefinitionContent content = new ExportDefinitionContent();
		content.setContentSystemBean(contentSystemBean);
		content.setFileNameTemplate("test.csv");
		content.setDefinition(exportDefinition);

		//This is what should cause the export to fail (intentionally). Will likely need to change this later to just use the 'Reconciled' condition and add unreconciled trades
		content.setContentReadySystemCondition(this.systemConditionService.getSystemConditionByName("Export: Client Portal File Content Ready"));

		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.exportDefinitionService.saveExportDefinitionContent(content), 1);
	}


	private CalendarSchedule createSchedule(Date scheduledRunDate) {
		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setName("Integration Test Schedule-" + RandomUtils.randomNumber(0, 9999) + ": One-time test schedule");
		schedule.setDescription("This schedule was created by an integration test.");
		schedule.setFrequency(ScheduleFrequencies.ONCE);
		schedule.setCalendar(this.calendarSetupService.getCalendarByName("System Calendar"));
		schedule.setBusinessDayConvention(BusinessDayConventions.UNADJUSTED);
		schedule.setStartDate(DateUtils.clearTime(scheduledRunDate));
		schedule.setStartTime(new Time(scheduledRunDate));
		schedule.setCalendarScheduleType(this.calendarScheduleService.getCalendarScheduleTypeByName("Standard Schedules"));
		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.calendarScheduleService.saveCalendarSchedule(schedule), 2);
	}
}
