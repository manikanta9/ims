package com.clifton.ims.tests.trade.file.upload;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.trade.TradeCreator;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.execution.TradeExecutionType;
import com.clifton.trade.file.upload.TradeUploadCommand;
import com.clifton.trade.file.upload.TradeUploadService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.search.TradeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */
public class TradeUploadCustomForwardsConverterTests extends BaseImsIntegrationTest {

	@Resource
	private TradeService tradeService;

	@Resource
	private TradeUploadService tradeUploadService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	TradeCreator tradeCreator;


	@Test
	public void testCurrencyTradeUpload() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.FORWARDS));
		List<Trade> tradeList = null;
		try {
			TradeUploadCommand upload = getTradeUpload(getTradeData(accountInfo.getClientAccount()), TradeDestination.FX_CONNECT_TICKET);
			cancelPendingForwardsTrades(upload);
			tradeList = uploadTrades(upload);
			validateTrades(accountInfo, tradeList);
		}
		finally {
			if (!CollectionUtils.isEmpty(tradeList)) {
				cancelTrades(tradeList);
			}
		}
	}


	@Test
	public void testClsCurrencyTradeUpload_verifyBroker() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.FORWARDS));
		InvestmentAccount clientAccount = accountInfo.getClientAccount();
		// CLS Holding Account
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, accountInfo.getCustodianAccount(), this.tradeUtils.getTradeType(TradeType.FORWARDS), true);
		InvestmentAccount clsHoldingAccount = accountInfo.getHoldingAccount();
		List<Trade> tradeList = null;

		try {
			TradeUploadCommand upload = getTradeUpload(getBrokerTestTradeDataCls(clientAccount, clsHoldingAccount), null);
			cancelPendingForwardsTrades(upload);
			tradeList = uploadTrades(upload);

			Assertions.assertEquals(3, tradeList.size());
			validateAccountingNotionalRounding(tradeList);

			String tradeAccountInfo = accountInfo.getClientAccount().getNumber() + "-" + accountInfo.getHoldingAccount().getNumber();
			Date tradeDate = tradeList.get(0).getTradeDate();
			Assertions.assertEquals(tradeAccountInfo + ": BUY 5,925,600 of EUR/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(0).getLabel());
			Assertions.assertEquals(tradeAccountInfo + ": SELL 1,925,600 of CAD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(1).getLabel());
			Assertions.assertEquals(tradeAccountInfo + ": SELL 1,925,600 of CAD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(2).getLabel());

			Assertions.assertNull(tradeList.get(0).getExecutingBrokerCompany(), "CLS Trade \"" + tradeList.get(0).getLabel() + "\" should not have an executing broker set.");
			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(1).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(1).getExecutingBrokerCompany().getName());
			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(2).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(2).getExecutingBrokerCompany().getName());
		}
		finally {
			if (!CollectionUtils.isEmpty(tradeList)) {
				cancelTrades(tradeList);
			}
		}
	}


	@Test
	public void testNonClsCurrencyTradeUpload_verifyBroker() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.FORWARDS));
		InvestmentAccount clientAccount = accountInfo.getClientAccount();
		InvestmentAccount holdingAccount = accountInfo.getHoldingAccount();
		List<Trade> tradeList = null;

		try {
			TradeUploadCommand upload = getTradeUpload(getBrokerTestTradeDataNonCls(clientAccount, holdingAccount), null);
			cancelPendingForwardsTrades(upload);
			tradeList = uploadTrades(upload);

			Assertions.assertEquals(5, tradeList.size());
			validateAccountingNotionalRounding(tradeList);

			String tradeAccountInfo = accountInfo.getClientAccount().getNumber() + "-" + accountInfo.getHoldingAccount().getNumber();
			Date tradeDate = tradeList.get(0).getTradeDate();
			Assertions.assertEquals(tradeAccountInfo + ": BUY 1,134,000 of EUR/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(0).getLabel());
			Assertions.assertEquals(tradeAccountInfo + ": SELL 2,134,000 of CAD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(1).getLabel());
			Assertions.assertEquals(tradeAccountInfo + ": BUY 3,134,000 of EUR/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(2).getLabel());
			Assertions.assertEquals(tradeAccountInfo + ": BUY 4,134,000 of EUR/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(3).getLabel());
			Assertions.assertEquals(tradeAccountInfo + ": SELL 5,134,000 of CAD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(4).getLabel());

			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(0).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(0).getExecutingBrokerCompany().getName());
			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(1).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(1).getExecutingBrokerCompany().getName());
			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(2).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(2).getExecutingBrokerCompany().getName());
			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(3).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(3).getExecutingBrokerCompany().getName());
			Assertions.assertEquals(BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName(), tradeList.get(4).getExecutingBrokerCompany().getName(), "Unexpected executing broker: " + tradeList.get(4).getExecutingBrokerCompany().getName());
		}
		finally {
			if (!CollectionUtils.isEmpty(tradeList)) {
				cancelTrades(tradeList);
			}
		}
	}


	@Test
	public void testForwardTradeRollUpload() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.FORWARDS));
		testForwardTradeRollUpload(accountInfo, null);
		testForwardTradeRollUpload(accountInfo, Boolean.TRUE);
		testForwardTradeRollUpload(accountInfo, Boolean.FALSE);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void validateTrades(AccountInfo accountInfo, List<Trade> tradeList) {
		Assertions.assertEquals(16, tradeList.size());

		validateAccountingNotionalRounding(tradeList);

		String tradeAccountInfo = accountInfo.getClientAccount().getNumber() + "-" + accountInfo.getHoldingAccount().getNumber();
		Date tradeDate = tradeList.get(0).getTradeDate();
		Assertions.assertEquals(tradeAccountInfo + ": BUY 370,852.27 of SGD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(0).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 746,820.2 of CHF/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(1).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 928,079.7 of GBP/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(2).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 2,340,569.63 of EUR/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(3).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 2,177,708,854 of JPY/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(4).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 996,268.5 of AUD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(5).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 1,827,133.67 of CAD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(6).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 5,260,412.49 of HKD/USD20160405 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(7).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 371,268.63 of SGD/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(8).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 744,779.28 of CHF/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(9).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 925,056.63 of GBP/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(10).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 2,313,653 of EUR/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(11).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": BUY 2,160,355,915 of JPY/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(12).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 971,053.13 of AUD/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(13).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 1,808,978 of CAD/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(14).getLabel());
		Assertions.assertEquals(tradeAccountInfo + ": SELL 5,207,689.75 of HKD/USD20160506 on " + DateUtils.fromDateShort(tradeDate), tradeList.get(15).getLabel());
	}


	private void validateAccountingNotionalRounding(List<Trade> tradeList) {
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			InvestmentSecurity forwardSecurity = trade.getInvestmentSecurity();

			boolean payingEqualsTrading = trade.getPayingSecurity().equals(forwardSecurity.getInstrument().getTradingCurrency());
			InvestmentSecurity quantityCCY = payingEqualsTrading ? trade.getInvestmentSecurity().getInstrument().getTradingCurrency() : trade.getPayingSecurity();
			InvestmentSecurity notionalCCY = payingEqualsTrading ? trade.getPayingSecurity() : trade.getInvestmentSecurity().getInstrument().getTradingCurrency();

			Assertions.assertTrue(trade.getQuantityIntended() == null || MathUtils.isEqual(InvestmentCalculatorUtils.getNotionalCalculator(quantityCCY).round(trade.getQuantityIntended()), trade.getQuantityIntended()), "Accounting notional [" + CoreMathUtils.formatNumber(trade.getQuantityIntended(), MathUtils.NUMBER_FORMAT_DECIMAL_MAX) + "] for [" + trade.getInvestmentSecurity().getSymbol() + "] is not rounded correctly.");
			Assertions.assertTrue(trade.getAccountingNotional() == null || MathUtils.isEqual(InvestmentCalculatorUtils.getNotionalCalculator(notionalCCY).round(trade.getAccountingNotional()), trade.getAccountingNotional()), "Accounting notional [" + CoreMathUtils.formatNumber(trade.getAccountingNotional(), MathUtils.NUMBER_FORMAT_DECIMAL_MAX) + "] for [" + trade.getInvestmentSecurity().getSymbol() + "] is not rounded correctly.");
		}
	}


	private void cancelPendingForwardsTrades(TradeUploadCommand upload) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setTradeDate(upload.getTradeGroup().getTradeDate());
		searchForm.setTradeTypeId(upload.getSimpleTradeType().getId());
		if (upload.getSimpleTradeDestination() != null) {
			searchForm.setTradeDestinationId(upload.getSimpleTradeDestination().getId());
		}
		searchForm.setWorkflowStateName("Pending");
		cancelTrades(this.tradeService.getTradeList(searchForm));
	}


	private void cancelTrades(List<Trade> tradeList) {
		List<String> errors = new ArrayList<>();
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			try {
				this.tradeUtils.cancelTrade(trade);
			}
			catch (Exception e) {
				errors.add(ExceptionUtils.getDetailedMessage(e));
			}
		}
		Assertions.assertEquals(0, errors.size(), "Failed to cancel trades.\n" + StringUtils.join(errors, "\n\n"));
	}


	private List<Trade> uploadTrades(TradeUploadCommand upload) {
		this.tradeUploadService.uploadTradeUploadFile(upload);

		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setTradeDate(upload.getTradeGroup().getTradeDate());
		searchForm.setTradeTypeId(upload.getSimpleTradeType().getId());
		if (upload.getSimpleTradeDestination() != null) {
			searchForm.setTradeDestinationId(upload.getSimpleTradeDestination().getId());
		}
		searchForm.setWorkflowStateName("Pending");
		return this.tradeService.getTradeList(searchForm);
	}


	private TradeUploadCommand getTradeUpload(DataTable tradeData, String tradeDestinationName) {
		TradeUploadCommand upload = new TradeUploadCommand();
		upload.setSimpleTradeType(this.tradeUtils.getTradeType("Forwards"));
		TradeGroup group = new TradeGroup();
		TradeGroupType tradeGroupType = new TradeGroupType();
		tradeGroupType.setName("Trade Import");
		group.setTradeGroupType(tradeGroupType);
		group.setTraderUser(this.userUtils.getCurrentUser());
		group.setTradeDate(DateUtils.toDate("03/28/2016"));
		upload.setTradeGroup(group);
		upload.setSimple(true);
		upload.setCustom(true);

		if (!StringUtils.isEmpty(tradeDestinationName)) {
			upload.setSimpleTradeDestination(this.tradeDestinationService.getTradeDestinationByName(tradeDestinationName));
		}

		DataTableToExcelFileConverter converter = new DataTableToExcelFileConverter();
		DataTableFileConfig fileConfig = new DataTableFileConfig(tradeData);
		upload.setFile(new MultipartFileImpl(converter.convert(fileConfig)));

		return upload;
	}


	private DataTable getTradeData(InvestmentAccount clientAccount) {
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "SGD", new BigDecimal("370852.27"), "USD", "SGD/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CHF", new BigDecimal("746820.2"), "USD", "CHF/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "GBP", new BigDecimal("928079.7"), "USD", "GBP/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "EUR", new BigDecimal("2340569.63"), "USD", "EUR/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "JPY", new BigDecimal("2177708854"), "USD", "JPY/USD20160404", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "AUD", new BigDecimal("996268.5"), "USD", "AUD/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CAD", new BigDecimal("1827133.67"), "USD", "CAD/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "HKD", new BigDecimal("5260412.49"), "USD", "HKD/USD20160405", true, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "SGD", new BigDecimal("371268.6326"), "USD", "SGD/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CHF", new BigDecimal("744779.2795"), "USD", "CHF/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "GBP", new BigDecimal("925056.6299"), "USD", "GBP/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "EUR", new BigDecimal("2313652.996"), "USD", "EUR/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "USD", new BigDecimal("19098873.4"), "JPY", "JPY/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "AUD", new BigDecimal("971053.1295"), "USD", "AUD/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CAD", new BigDecimal("1808977.996"), "USD", "CAD/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "HKD", new BigDecimal("5207689.752"), "USD", "HKD/USD20160506", false, TradeExecutionType.NON_CLS_TRADE, null, null, null)));

		return dataTable;
	}


	private DataTable getBrokerTestTradeDataCls(InvestmentAccount clientAccount, InvestmentAccount holdingAccount) {
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "EUR", new BigDecimal("5925600"), "USD", "EUR/USD20160404", true, TradeExecutionType.CLS_TRADE, TradeDestination.FX_CONNECT_TICKET, holdingAccount.getNumber(), null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CAD", new BigDecimal("1925600"), "USD", "CAD/USD20160404", false, TradeExecutionType.CLS_TRADE, TradeDestination.FX_CONNECT_TICKET, holdingAccount.getNumber(), BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName())));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CAD", new BigDecimal("1925600"), "USD", "CAD/USD20160404", false, TradeExecutionType.CLS_TRADE, TradeDestination.MANUAL, holdingAccount.getNumber(), BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName())));
		return dataTable;
	}


	private DataTable getBrokerTestTradeDataNonCls(InvestmentAccount clientAccount, InvestmentAccount holdingAccount) {
		DataTable dataTable = new PagingDataTableImpl(DATA_COLUMN_LIST);
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "EUR", new BigDecimal("1134000"), "USD", "EUR/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, TradeDestination.FX_CONNECT_TICKET, holdingAccount.getNumber(), null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CAD", new BigDecimal("2134000"), "USD", "CAD/USD20160404", false, TradeExecutionType.NON_CLS_TRADE, TradeDestination.FX_CONNECT_TICKET, holdingAccount.getNumber(), BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName())));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "EUR", new BigDecimal("3134000"), "USD", "EUR/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, TradeDestination.FX_CONNECT_TICKET, null, null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "EUR", new BigDecimal("4134000"), "USD", "EUR/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, TradeDestination.MANUAL, holdingAccount.getNumber(), null)));
		dataTable.addRow(new DataRowImpl(dataTable, populateTradeDataRow(clientAccount, "CAD", new BigDecimal("5134000"), "USD", "CAD/USD20160404", false, TradeExecutionType.NON_CLS_TRADE, TradeDestination.MANUAL, holdingAccount.getNumber(), BusinessCompanies.BARCLAYS_BANK_PLC.getCompanyName())));
		return dataTable;
	}


	private Object[] populateTradeDataRow(InvestmentAccount clientAccount, String givenCurrencySymbol, BigDecimal givenAmount, String settlementCurrencySymbol, String forwardSymbol, boolean buy,
	                                      String tradeExecutionTypeName, String tradeDestinationName, String holdingInvestmentAccountNumber, String executingBrokerCompanyName) {
		Object[] values = new Object[DATA_COLUMN_LIST.length];
		values[0] = clientAccount.getNumber();
		values[1] = (buy ? "B" : "S");
		values[2] = givenCurrencySymbol;
		values[3] = givenAmount;
		values[4] = settlementCurrencySymbol;
		values[5] = forwardSymbol;
		values[6] = tradeExecutionTypeName;
		values[7] = tradeDestinationName;
		values[8] = holdingInvestmentAccountNumber;
		values[9] = executingBrokerCompanyName;

		return values;
	}


	private void testForwardTradeRollUpload(AccountInfo accountInfo, Boolean roll) {
		List<Trade> tradeList = null;
		try {
			DataColumn[] rollColumns = ArrayUtils.add(DATA_COLUMN_LIST, new DataColumnImpl("Roll", Types.NVARCHAR));
			DataTable dataTable = new PagingDataTableImpl(rollColumns);
			Object[] tradeValues = populateTradeDataRow(accountInfo.getClientAccount(), "SGD", new BigDecimal("370852.27"), "USD", "SGD/USD20160404", true, TradeExecutionType.NON_CLS_TRADE, null, null, null);
			dataTable.addRow(new DataRowImpl(dataTable, ArrayUtils.addAll(tradeValues, new Object[]{roll})));
			TradeUploadCommand upload = getTradeUpload(dataTable, TradeDestination.FX_CONNECT_TICKET);
			cancelPendingForwardsTrades(upload);
			tradeList = uploadTrades(upload);
			Assertions.assertEquals(1, tradeList.size());

			validateAccountingNotionalRounding(tradeList);

			String tradeAccountInfo = accountInfo.getClientAccount().getNumber() + "-" + accountInfo.getHoldingAccount().getNumber();
			Trade trade = tradeList.get(0);
			Date tradeDate = trade.getTradeDate();
			Assertions.assertEquals(tradeAccountInfo + ": BUY 370,852.27 of SGD/USD20160404 on " + DateUtils.fromDateShort(tradeDate), trade.getLabel());
			Assertions.assertEquals(BooleanUtils.isTrue(roll), trade.isRoll());
			Assertions.assertEquals(BooleanUtils.isTrue(roll) ? TradeGroupType.GroupTypes.ROLL_TRADE_IMPORT.getTypeName() : TradeGroupType.GroupTypes.TRADE_IMPORT.getTypeName(), trade.getTradeGroup().getTradeGroupType().getName());
		}
		finally {
			if (!CollectionUtils.isEmpty(tradeList)) {
				cancelTrades(tradeList);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private static final DataColumn[] DATA_COLUMN_LIST = new DataColumn[]{
			new DataColumnImpl("ClientInvestmentAccountNumber", Types.NVARCHAR),
			new DataColumnImpl("Buy", Types.NVARCHAR),
			new DataColumnImpl("GivenCurrencySymbol", Types.NVARCHAR),
			new DataColumnImpl("GivenAmount", Types.DECIMAL),
			new DataColumnImpl("SettlementCurrencySymbol", Types.NVARCHAR),
			new DataColumnImpl("ForwardSecuritySymbol", Types.NVARCHAR),
			new DataColumnImpl("TradeExecutionTypeName", Types.NVARCHAR),
			new DataColumnImpl("TradeDestinationName", Types.NVARCHAR),
			new DataColumnImpl("HoldingInvestmentAccount-Number", Types.NVARCHAR),
			new DataColumnImpl("ExecutingBrokerCompanyName", Types.NVARCHAR),
	};
}
