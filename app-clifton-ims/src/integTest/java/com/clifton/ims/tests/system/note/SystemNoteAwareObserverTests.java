package com.clifton.ims.tests.system.note;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.document.DocumentAttachmentSupportedTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.note.search.SystemNoteSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * SystemNoteAwareObserverTests tests the deletion of the note or link to a note happens when the related entity is deleted
 * <p>
 *
 * @author manderson
 */

public class SystemNoteAwareObserverTests extends BaseImsIntegrationTest {

	@Resource
	private BusinessClientService businessClientService;

	@Resource
	private SystemNoteService systemNoteService;

	@Resource
	private SystemSchemaService systemSchemaService;


	private static final String BUSINESS_CLIENT_TABLE = "BusinessClient";


	//////////////////////////////////////////////////////////


	@Test
	public void testDeleteNotesAndLinks() {
		SystemNoteType specificNoteType = setupNoteType(BUSINESS_CLIENT_TABLE, false);
		SystemNoteType linkToMultipleNoteType = setupNoteType(BUSINESS_CLIENT_TABLE, true);

		// Create New Clients
		BusinessClient client1 = this.businessUtils.createBusinessClient();
		BusinessClient client2 = this.businessUtils.createBusinessClient();

		// Create New Notes
		SystemNote note1 = saveNote(specificNoteType, client1);
		SystemNote note2 = saveNote(specificNoteType, client2);
		SystemNote linkedNote = saveNote(linkToMultipleNoteType, client1);
		this.systemNoteService.saveSystemNoteLink(linkedNote.getId(), client2.getId());

		// Ensure the Correct Notes are Retrieved
		SystemNoteSearchForm searchForm1 = new SystemNoteSearchForm();
		searchForm1.setEntityTableName(BUSINESS_CLIENT_TABLE);
		searchForm1.setFkFieldId(BeanUtils.getIdentityAsLong(client1));
		List<SystemNote> client1Notes = this.systemNoteService.getSystemNoteListForEntity(searchForm1);
		Assertions.assertEquals(2, CollectionUtils.getSize(client1Notes));

		SystemNoteSearchForm searchForm2 = new SystemNoteSearchForm();
		searchForm2.setEntityTableName(BUSINESS_CLIENT_TABLE);
		searchForm2.setFkFieldId(BeanUtils.getIdentityAsLong(client2));
		List<SystemNote> client2Notes = this.systemNoteService.getSystemNoteListForEntity(searchForm2);
		Assertions.assertEquals(2, CollectionUtils.getSize(client2Notes));

		// Delete the First Client
		this.businessClientService.deleteBusinessClient(client1.getId());

		// First Client Should No Longer Have Any Notes
		client1Notes = this.systemNoteService.getSystemNoteListForEntity(searchForm1);
		Assertions.assertEquals(0, CollectionUtils.getSize(client1Notes));

		// Second Client Should Still have 2
		client2Notes = this.systemNoteService.getSystemNoteListForEntity(searchForm2);
		Assertions.assertEquals(2, CollectionUtils.getSize(client2Notes));

		// Linked note should have second client directly linked
		linkedNote = this.systemNoteService.getSystemNote(linkedNote.getId());
		Assertions.assertEquals(linkedNote.getFkFieldId(), BeanUtils.getIdentityAsLong(client2));

		// Delete the Second Client
		this.businessClientService.deleteBusinessClient(client2.getId());

		// Now, Second Client's Notes should be deleted
		client2Notes = this.systemNoteService.getSystemNoteListForEntity(searchForm2);
		Assertions.assertEquals(0, CollectionUtils.getSize(client2Notes));
	}


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	//////////////////////////////////////////////////////////
	//  Helper Methods
	//////////////////////////////////////////////////////////


	private SystemNoteType setupNoteType(String tableName, boolean linkToMultipleAllowed) {
		SystemNoteType noteType = new SystemNoteType();
		noteType.setName(tableName + ": " + RandomUtils.randomName());
		noteType.setTable(this.systemSchemaService.getSystemTableByName(tableName));
		noteType.setOrder(1000);
		noteType.setAttachmentSupportedType(DocumentAttachmentSupportedTypes.NONE);
		noteType.setLinkToMultipleAllowed(linkToMultipleAllowed);
		this.systemNoteService.saveSystemNoteType(noteType);
		return noteType;
	}


	private SystemNote saveNote(SystemNoteType noteType, BusinessClient client) {
		SystemNote note = new SystemNote();
		note.setNoteType(noteType);
		note.setFkFieldId(BeanUtils.getIdentityAsLong(client));
		note.setText("Test Note");
		this.systemNoteService.saveSystemNote(note);
		return note;
	}
}
