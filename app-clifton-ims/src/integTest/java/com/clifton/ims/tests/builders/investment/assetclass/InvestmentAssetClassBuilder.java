package com.clifton.ims.tests.builders.investment.assetclass;

import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentSetupService;


/**
 * @author stevenf
 */
public class InvestmentAssetClassBuilder {

	private final InvestmentSetupService investmentSetupService;

	private InvestmentAssetClass masterAssetClass;
	private boolean master;
	private String shortLabel;
	private boolean cash;
	private boolean mainCash;
	private String label;
	private String name;
	private String description;


	private InvestmentAssetClassBuilder(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public static InvestmentAssetClassBuilder investmentAssetClass(InvestmentSetupService investmentSetupService) {
		return new InvestmentAssetClassBuilder(investmentSetupService);
	}


	public InvestmentAssetClass build() {
		InvestmentAssetClass investmentAssetClass = new InvestmentAssetClass();
		investmentAssetClass.setMasterAssetClass(this.masterAssetClass);
		investmentAssetClass.setMaster(this.master);
		investmentAssetClass.setShortLabel(this.shortLabel);
		investmentAssetClass.setCash(this.cash);
		investmentAssetClass.setMainCash(this.mainCash);
		investmentAssetClass.setLabel(this.label);
		investmentAssetClass.setName(this.name);
		investmentAssetClass.setDescription(this.description);
		return investmentAssetClass;
	}


	public InvestmentAssetClass buildAndSave() {
		InvestmentAssetClass investmentAssetClass = build();
		this.investmentSetupService.saveInvestmentAssetClass(investmentAssetClass);
		//refresh it to ensure proper loading of all properties.
		investmentAssetClass = this.investmentSetupService.getInvestmentAssetClass(investmentAssetClass.getId());
		return investmentAssetClass;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentAssetClassBuilder withMasterAssetClass(InvestmentAssetClass masterAssetClass) {
		this.masterAssetClass = masterAssetClass;
		return this;
	}


	public InvestmentAssetClassBuilder withMaster(boolean master) {
		this.master = master;
		return this;
	}


	public InvestmentAssetClassBuilder withShortLabel(String shortLabel) {
		this.shortLabel = shortLabel;
		return this;
	}


	public InvestmentAssetClassBuilder withCash(boolean cash) {
		this.cash = cash;
		return this;
	}


	public InvestmentAssetClassBuilder withMainCash(boolean mainCash) {
		this.mainCash = mainCash;
		return this;
	}


	public InvestmentAssetClassBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public InvestmentAssetClassBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public InvestmentAssetClassBuilder withDescription(String description) {
		this.description = description;
		return this;
	}
}
