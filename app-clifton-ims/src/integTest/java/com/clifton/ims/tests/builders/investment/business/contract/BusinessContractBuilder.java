package com.clifton.ims.tests.builders.investment.business.contract;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.contract.BusinessContractTemplate;
import com.clifton.business.contract.BusinessContractType;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.test.util.RandomUtils;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public class BusinessContractBuilder {

	private final BusinessContractService businessContractService;

	private BusinessCompany company;
	private BusinessContractType contractType;
	private BusinessContractTemplate contractTemplate;
	private WorkflowState workflowState;
	private WorkflowStatus workflowStatus;
	private Date effectiveDate;
	private Date signedDate;
	private boolean internalApproved;
	private boolean legalApproved;
	private boolean clientApproved;
	private String documentFileType;
	private Date documentUpdateDate;
	private String documentUpdateUser;
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;
	private BusinessContract parent;
	private String label;
	private String name;
	private String description;


	private BusinessContractBuilder(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public static BusinessContractBuilder businessContract(BusinessContractService businessContractService) {
		return new BusinessContractBuilder(businessContractService)
				.withName(RandomUtils.randomNameAndNumber());
	}


	public BusinessContract build() {
		BusinessContract businessContract = new BusinessContract();
		businessContract.setCompany(this.company);
		businessContract.setContractType(this.contractType);
		businessContract.setContractTemplate(this.contractTemplate);
		businessContract.setWorkflowState(this.workflowState);
		businessContract.setWorkflowStatus(this.workflowStatus);
		businessContract.setEffectiveDate(this.effectiveDate);
		businessContract.setSignedDate(this.signedDate);
		businessContract.setInternalApproved(this.internalApproved);
		businessContract.setLegalApproved(this.legalApproved);
		businessContract.setClientApproved(this.clientApproved);
		businessContract.setDocumentFileType(this.documentFileType);
		businessContract.setDocumentUpdateDate(this.documentUpdateDate);
		businessContract.setDocumentUpdateUser(this.documentUpdateUser);
		businessContract.setColumnGroupName(this.columnGroupName);
		businessContract.setColumnValueList(this.columnValueList);
		businessContract.setParent(this.parent);
		businessContract.setLabel(this.label);
		businessContract.setName(this.name);
		businessContract.setDescription(this.description);
		return businessContract;
	}


	public BusinessContract buildAndSave() {
		BusinessContract businessContract = build();
		return this.businessContractService.saveBusinessContract(businessContract);
	}


	public BusinessContractBuilder but() {
		return businessContract(this.businessContractService)
				.withCompany(this.company)
				.withContractType(this.contractType)
				.withContractTemplate(this.contractTemplate)
				.withWorkflowState(this.workflowState)
				.withWorkflowStatus(this.workflowStatus)
				.withEffectiveDate(this.effectiveDate)
				.withSignedDate(this.signedDate)
				.withInternalApproved(this.internalApproved)
				.withLegalApproved(this.legalApproved)
				.withClientApproved(this.clientApproved)
				.withDocumentFileType(this.documentFileType)
				.withDocumentUpdateDate(this.documentUpdateDate)
				.withDocumentUpdateUser(this.documentUpdateUser)
				.withColumnGroupName(this.columnGroupName)
				.withColumnValueList(this.columnValueList)
				.withParent(this.parent)
				.withLabel(this.label)
				.withName(this.name)
				.withDescription(this.description);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public BusinessContractBuilder withCompany(BusinessCompany company) {
		this.company = company;
		return this;
	}


	public BusinessContractBuilder withContractType(BusinessContractType contractType) {
		this.contractType = contractType;
		return this;
	}


	public BusinessContractBuilder withContractTemplate(BusinessContractTemplate contractTemplate) {
		this.contractTemplate = contractTemplate;
		return this;
	}


	public BusinessContractBuilder withWorkflowState(WorkflowState workflowState) {
		this.workflowState = workflowState;
		return this;
	}


	public BusinessContractBuilder withWorkflowStatus(WorkflowStatus workflowStatus) {
		this.workflowStatus = workflowStatus;
		return this;
	}


	public BusinessContractBuilder withEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
		return this;
	}


	public BusinessContractBuilder withSignedDate(Date signedDate) {
		this.signedDate = signedDate;
		return this;
	}


	public BusinessContractBuilder withInternalApproved(boolean internalApproved) {
		this.internalApproved = internalApproved;
		return this;
	}


	public BusinessContractBuilder withLegalApproved(boolean legalApproved) {
		this.legalApproved = legalApproved;
		return this;
	}


	public BusinessContractBuilder withClientApproved(boolean clientApproved) {
		this.clientApproved = clientApproved;
		return this;
	}


	public BusinessContractBuilder withDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
		return this;
	}


	public BusinessContractBuilder withDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
		return this;
	}


	public BusinessContractBuilder withDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
		return this;
	}


	public BusinessContractBuilder withColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
		return this;
	}


	public BusinessContractBuilder withColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
		return this;
	}


	public BusinessContractBuilder withParent(BusinessContract parent) {
		this.parent = parent;
		return this;
	}


	public BusinessContractBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public BusinessContractBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public BusinessContractBuilder withDescription(String description) {
		this.description = description;
		return this;
	}
}
