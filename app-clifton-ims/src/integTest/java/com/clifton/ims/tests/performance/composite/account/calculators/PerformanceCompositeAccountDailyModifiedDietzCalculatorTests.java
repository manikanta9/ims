package com.clifton.ims.tests.performance.composite.account.calculators;

import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountDailyModifiedDietzCalculatorTests</code> tests the PerformanceCompositeAccountDailyModifiedDietzCalculator
 *
 * @author manderson
 */
public class PerformanceCompositeAccountDailyModifiedDietzCalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Override
	protected boolean isUseThreshold(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		// Started using IMS in March 2016
		return DateUtils.compare(accountPerformance.getMeasureDate(), DateUtils.toDate("03/01/2016"), false) < 0;
	}


	@Test
	public void test_account_050905() {
		String accountNumber = "050905";

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("481510.44"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-212959.41"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-938471.87"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("1638463.02"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("1552345.71"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		validatePerformanceCompositeInvestmentAccountDailyPerformance(accountNumber, "03/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "03/31/2016");

		// Ignoring Daily Check: Benchmark returns on a few days that are saved differ from new preview value (Manually updated)- confirmed new preview value is correct MTD still matches
		// validatePerformanceCompositeInvestmentAccountDailyPerformance(accountNumber, "04/30/2016");
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("managementFeeOverride", new BigDecimal("5728.77")); // New Feature to "True Up" Revenue
		overrideValueMap.put("periodNetReturn", new BigDecimal("0.28613209")); // Actual: 0.28613192 now slightly adjusted because of the true up value
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "04/30/2016", overrideValueMap);

		// Should back out 4/30 gain loss from 5/2
		// We want to ignore overrides because we need overridden gain/loss to match newly calculated gain/loss
		// Ignoring Daily Check: Benchmark returns on a few days that are saved differ from new preview value (Manually updated)- confirmed new preview value is correct MTD still matches
		// validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, "05/31/2016", true);
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "05/31/2016", true);
	}


	@Test
	public void test_account_495100() {
		String accountNumber = "495100";

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("1144667.02"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-29172.00")); // Accounting Value: Missing
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("-21393.00")); // Accounting Value: Missing
		overrideValueMap.put("periodGainLoss", new BigDecimal("-5584972.00"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("-230607.31"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		// Ignore daily user entered overrides here because the system will calculate 1/31 values instead of using 1/29 override
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", true);
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "02/29/2016");

		validatePerformanceCompositeInvestmentAccountDailyPerformance(accountNumber, "03/31/2016");
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "03/31/2016");
	}


	@Test
	public void test_account_456606() {
		String accountNumber = "456606";

		// Account was originally including net cash flow amount for last day of previous month
		// With PERFORMANCE-196 This is no longer an issue, so ignore the override in preview calculations and validate that it matches real results
		validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, "03/31/2016", true);
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", true);
	}


	@Test
	public void test_account_112950() {
		String accountNumber = "112950";

		// Includes CASH interest expense in gain/loss on 5/31
		validatePerformanceCompositeInvestmentAccountPerformance(accountNumber, "05/31/2016");
	}


	@Test
	public void test_account_456605() {
		String accountNumber = "456605";

		// Includes CASH interest expense in gain/loss on 10/1 (Saturday)
		validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, "10/31/2016", true);
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2016", true);
	}


	public void test_account_604600() {
		String accountNumber = "604600";

		// Includes Interest Income on 5/2 in gain/loss - ignore overrides since accounting overrode value to match how should calculate now
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "05/31/2016", true);
		validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, "05/02/2016", true);
	}
}
