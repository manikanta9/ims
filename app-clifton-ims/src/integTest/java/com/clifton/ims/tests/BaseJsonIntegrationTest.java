package com.clifton.ims.tests;


import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.business.BusinessUtils;
import com.clifton.ims.tests.compliance.ComplianceUtils;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.ims.tests.investment.InvestmentInstrumentUtils;
import com.clifton.ims.tests.investment.manager.InvestmentManagerAccountUtils;
import com.clifton.ims.tests.lending.repo.LendingRepoUtils;
import com.clifton.ims.tests.spring.IntegrationJsonConfiguration;
import com.clifton.ims.tests.trade.TradeUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.test.junit.DefaultImsTestAuthenticator;
import com.clifton.test.junit.TestWatcherLogger;
import com.clifton.test.util.UserUtils;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ExtendWith({SpringExtension.class, TestWatcherLogger.class, DefaultImsTestAuthenticator.class})
@ContextConfiguration(classes = {IntegrationJsonConfiguration.class})
public abstract class BaseJsonIntegrationTest {

	//////////////////////////////////////////////////////////////////
	//						Test Util Classes
	//////////////////////////////////////////////////////////////////
	@Resource
	protected AccountingUtils accountingUtils;

	@Resource
	protected BusinessUtils businessUtils;

	@Resource
	protected ComplianceUtils complianceUtils;

	@Resource
	protected InvestmentManagerAccountUtils investmentManagerAccountUtils;

	@Resource
	protected InvestmentAccountUtils investmentAccountUtils;

	@Resource
	protected InvestmentInstrumentUtils investmentInstrumentUtils;

	@Resource
	protected LendingRepoUtils lendingRepoUtils;

	@Resource
	protected TradeUtils tradeUtils;

	@Resource
	protected UserUtils userUtils;

	//////////////////////////////////////////////////////////////////
	//						IMS Service Classes
	//////////////////////////////////////////////////////////////////

	@Resource
	protected AccountingAccountService accountingAccountService;

	@Resource
	protected InvestmentInstrumentService investmentInstrumentService;

	@Resource
	protected SecurityUserService securityUserService;

	@Resource
	protected SecurityAuthorizationSetupService securityAuthorizationSetupService;

	@Resource
	protected SystemColumnValueService systemColumnValueService;

	//////////////////////////////////////////////////////////////////

	protected InvestmentSecurity usd;
	protected InvestmentSecurity eur;

	protected TradeType futures;
	protected TradeType bonds;
	protected TradeType options;
	protected TradeType clearedInterestRateSwaps;
	protected TradeType totalReturnSwaps;
	protected TradeType currency;

	protected BusinessCompany goldmanSachs;

	//TODO May want to consider better way of handling this to allow testing of non-admin users
	protected SecurityUser admin;

	//////////////////////////////////////////////////////////////////


	/**
	 * Do not override. This method must be public void and needs to run before every test. If you need additional before logic
	 * define a public void method with a different name annotated with @BeforeEach
	 */
	@BeforeEach
	public void baseBeforeTest() {
//		if (!ImsTestProperties.TEST_USER_ADMIN.equals(this.userUtils.getCurrentUserName())) {
//			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
//		}

		this.admin = (this.admin != null && SecurityUser.SYSTEM_USER.equals(this.admin.getUserName())) ? this.admin : this.securityUserService.getSecurityUserByName(SecurityUser.SYSTEM_USER);
		this.usd = (this.usd != null) ? this.usd : this.investmentInstrumentUtils.getCurrencyBySymbol("USD");
		this.eur = (this.eur != null) ? this.eur : this.investmentInstrumentUtils.getCurrencyBySymbol("EUR");
		this.futures = (this.futures != null) ? this.futures : this.tradeUtils.getTradeType(TradeType.FUTURES);
		this.bonds = (this.bonds != null) ? this.bonds : this.tradeUtils.getTradeType(TradeType.BONDS);
		this.options = (this.options != null) ? this.options : this.tradeUtils.getTradeType(TradeType.OPTIONS);
		this.currency = (this.currency != null) ? this.currency : this.tradeUtils.getTradeType(TradeType.CURRENCY);
		this.totalReturnSwaps = (this.totalReturnSwaps != null) ? this.totalReturnSwaps : this.tradeUtils.getTradeType(TradeType.TOTAL_RETURN_SWAPS);
		this.clearedInterestRateSwaps = (this.clearedInterestRateSwaps != null) ? this.clearedInterestRateSwaps : this.tradeUtils.getTradeType(TradeType.CLEARED_INTEREST_RATE_SWAPS);
		this.goldmanSachs = (this.goldmanSachs != null) ? this.goldmanSachs : this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
	}
}
