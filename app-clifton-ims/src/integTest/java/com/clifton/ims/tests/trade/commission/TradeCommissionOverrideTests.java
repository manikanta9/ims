package com.clifton.ims.tests.trade.commission;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingJournalDetailBuilder;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.accounting.GeneralLedgerDescriptions;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.util.ConcurrentExecutionUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.accounting.commission.TradeCommission;
import com.clifton.trade.accounting.commission.TradeCommissionOverride;
import com.clifton.trade.accounting.commission.TradeCommissionService;
import com.clifton.trade.accounting.commission.TradeCommissionSources;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


public class TradeCommissionOverrideTests extends BaseImsIntegrationTest {

	@Resource
	private TradeCommissionService tradeCommissionService;

	private TradeType clearedCreditDefaultSwaps;


	@BeforeEach
	public void beforeTest() {
		this.clearedCreditDefaultSwaps = this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS);
	}


	/**
	 * This test does the following:
	 * <p>
	 * <ul>
	 * <li>Creates a buy trade (to open a position) with three fills and a flat fee override set on the trade</li>
	 * <li>Creates a second override for NFA Fees through TradeCommissionOverride</li>
	 * <li>Executes the buy trade</li>
	 * <li>Creates a sell trade (to partially close the position) with one fill and "per unit" commission override set on the trade</li>
	 * <li>Creates a second override for Management Fee through TradeCommissionOverride</li>
	 * <li>Executes the sell trade</li>
	 * <li>Validates that the commission amounts, etc. are correct for each of these steps</li>
	 * </ul>
	 */
	@Test
	public void testTradeCommissionOverrides() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create a buy trade
		BigDecimal buyQuantity = new BigDecimal("100");
		BigDecimal fill1Quantity = new BigDecimal("41");
		BigDecimal fill2Quantity = new BigDecimal("39");
		BigDecimal fill3Quantity = new BigDecimal("20");
		BigDecimal feeOverrideAmount = new BigDecimal("-30");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, executingBroker, accountInfo)
				.addFill(fill1Quantity, BigDecimal.ONE)
				.addFill(fill2Quantity, BigDecimal.ONE)
				.addFill(fill3Quantity, BigDecimal.ONE)
				.withFeeAmount(feeOverrideAmount)
				.withEnableCommissionOverride(true)
				.buildAndSave();

		List<TradeCommission<?>> commissions = this.tradeCommissionService.getTradeCommissionListExpandedByTrade(buy.getId());
		Assertions.assertEquals(1, commissions.size());

		TradeCommission<Integer> expectedCommission = new TradeCommission<>();
		expectedCommission.setSource(TradeCommissionSources.OVERRIDE);
		expectedCommission.setExpenseAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_NFA_FEES));
		expectedCommission.setTotalAmount(feeOverrideAmount);
		CoreCompareUtils.isNonNullPropertiesEqual(expectedCommission, commissions.get(0), true, null);

		// Execute the buy, thus opening a position
		this.tradeUtils.fullyExecuteTrade(buy);

		// Validate the first fill
		List<? extends AccountingJournalDetailDefinition> fill1Details = this.accountingUtils.getTradeFillDetails(buy.getTradeFillList().get(0));
		AccountingUtils.assertJournalDetailList(fill1Details,
				"16348936	null	CA275961689	HA1901660624	Position	CCH14	1	41	0	12/16/2013	1	0	410	12/16/2013	12/16/2013	Position opening of CCH14",
				"16348937	16348936	CA275961689	HA1901660624	NFA Fees	CCH14	0.3	41	12.3	12/16/2013	1	12.3	0	12/16/2013	12/16/2013	NFA Fees expense for CCH14",
				"16348938	16348936	CA275961689	HA1901660624	Cash	USD			-12.3	12/16/2013	1	-12.3	0	12/16/2013	12/16/2013	Cash expense from opening of CCH14"
		);

		// Validate the second fill
		List<? extends AccountingJournalDetailDefinition> fill2Details = this.accountingUtils.getTradeFillDetails(buy.getTradeFillList().get(1));
		AccountingUtils.assertJournalDetailList(fill2Details,
				"16349080	null	CA103441693	HA1479817186	Position	CCH14	1	39	0	12/16/2013	1	0	390	12/16/2013	12/16/2013	Position opening of CCH14",
				"16349081	16349080	CA103441693	HA1479817186	NFA Fees	CCH14	0.3	39	11.7	12/16/2013	1	11.7	0	12/16/2013	12/16/2013	NFA Fees expense for CCH14",
				"16349082	16349080	CA103441693	HA1479817186	Cash	USD			-11.7	12/16/2013	1	-11.7	0	12/16/2013	12/16/2013	Cash expense from opening of CCH14"
		);

		// Validate the third fill
		List<? extends AccountingJournalDetailDefinition> fill3Details = this.accountingUtils.getTradeFillDetails(buy.getTradeFillList().get(2));
		AccountingUtils.assertJournalDetailList(fill3Details,
				"16349224	null	CA1890280775	HA737513824	Position	CCH14	1	20	0	12/16/2013	1	0	200	12/16/2013	12/16/2013	Position opening of CCH14",
				"16349225	16349224	CA1890280775	HA737513824	NFA Fees	CCH14	0.3	20	6	12/16/2013	1	6	0	12/16/2013	12/16/2013	NFA Fees expense for CCH14",
				"16349226	16349224	CA1890280775	HA737513824	Cash	USD			-6	12/16/2013	1	-6	0	12/16/2013	12/16/2013	Cash expense from opening of CCH14"
		);

		// Create a sell trade
		BigDecimal sellQuantity = new BigDecimal("60");
		BigDecimal commissionOverridePerUnit = new BigDecimal("2.5");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo)
				.withCommissionPerUnit(commissionOverridePerUnit)
				.withEnableCommissionOverride(true)
				.buildAndSave();

		// Create a flat override for Management Fee for the sell trade
		TradeCommissionOverride managementFeeOverride = new TradeCommissionOverride();
		String overrideNote = "This is a test";
		managementFeeOverride.setAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_MANAGEMENT_FEE));
		managementFeeOverride.setTotalAmount(new BigDecimal("-50"));
		managementFeeOverride.setOverrideNote(overrideNote);
		managementFeeOverride.setTrade(sell);
		this.tradeCommissionService.saveTradeCommissionOverride(managementFeeOverride);

		// Execute the sell, thus partially closing the overall position
		this.tradeUtils.fullyExecuteTrade(sell);

		// Get the actual details for the sell
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(sellDetails,
				"16349376	16349367	CA1919942798	HA268057854	Position	CCH14	1	-41	0	12/17/2013	1	0	-410	12/16/2013	12/17/2013	Full position close for CCH14",
				"16349377	16349370	CA1919942798	HA268057854	Position	CCH14	1	-19	0	12/17/2013	1	0	-190	12/16/2013	12/17/2013	Partial position close for CCH14",
				"16349378	16349376	CA1919942798	HA268057854	Management Fee	CCH14	0.8334	41	34.17	12/17/2013	1	34.17	0	12/16/2013	12/17/2013	Management Fee expense for CCH14",
				"16349379	16349377	CA1919942798	HA268057854	Management Fee	CCH14	0.833	19	15.83	12/17/2013	1	15.83	0	12/16/2013	12/17/2013	Management Fee expense for CCH14",
				"16349380	16349376	CA1919942798	HA268057854	Commission	CCH14	2.5	41	102.5	12/17/2013	1	102.5	0	12/16/2013	12/17/2013	Commission expense for CCH14",
				"16349381	16349377	CA1919942798	HA268057854	Commission	CCH14	2.5	19	47.5	12/17/2013	1	47.5	0	12/16/2013	12/17/2013	Commission expense for CCH14",
				"16349382	16349376	CA1919942798	HA268057854	Cash	USD			-136.67	12/17/2013	1	-136.67	0	12/16/2013	12/17/2013	Cash expense from close of CCH14",
				"16349383	16349377	CA1919942798	HA268057854	Cash	USD			-63.33	12/17/2013	1	-63.33	0	12/16/2013	12/17/2013	Cash expense from close of CCH14"
		);
	}


	/**
	 * This test verifies that TradeCommissionOverrides are created for a new Trade when the enableCommissionOverride flag is not set.
	 * <p>
	 * <ul>
	 * <li>Creates a buy trade (to open a position) with overrides taking effect</li>
	 * <li>Executes the buy trade</li>
	 * <li>Creates a sell trade (to partially close the position) with one fill and commission override taking effect on the trade</li>
	 * <li>Executes the sell trade</li>
	 * <li>Validates that the commission amounts, etc. are correct for each of these steps</li>
	 * </ul>
	 */
	@Test
	public void testTradeCommissionOverrideWithoutEnableCommissionOnNewTradeOverrides() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create a buy trade
		BigDecimal buyQuantity = new BigDecimal("100");
		BigDecimal feeOverrideAmount = new BigDecimal("-30");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, executingBroker, accountInfo)
				.withAverageUnitPrice(BigDecimal.ONE)
				.withFeeAmount(feeOverrideAmount)
				.buildAndSave();

		List<TradeCommission<?>> commissions = this.tradeCommissionService.getTradeCommissionListExpandedByTrade(buy.getId());
		Assertions.assertEquals(1, commissions.size());

		// Execute the buy, thus opening a position
		this.tradeUtils.fullyExecuteTrade(buy);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);

		// Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedDetails = new ArrayList<>();
		expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(buyQuantity)
				.opening(true)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.POSITION_OPENING, cch14)
				.build());
		expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_NFA_FEES))
				.price(MathUtils.divide(feeOverrideAmount.negate(), buyQuantity))
				.quantity(buyQuantity)
				.opening(true)
				.localDebitCredit(feeOverrideAmount.negate())
				.baseDebitCredit(feeOverrideAmount.negate())
				.description(GeneralLedgerDescriptions.NFA_FEES_EXPENSE, cch14)
				.build());
		expectedDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.opening(true)
				.localDebitCredit(feeOverrideAmount)
				.baseDebitCredit(feeOverrideAmount)
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_OPEN, cch14)
				.build());

		// Validate the details
		List<? extends AccountingJournalDetailDefinition> actualDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);
		AccountingUtils.assertJournalDetailList(expectedDetails, actualDetails);

		// Create a sell trade
		BigDecimal sellQuantity = new BigDecimal("60");
		BigDecimal commissionOverridePerUnit = new BigDecimal("2.5");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo)
				.withCommissionPerUnit(commissionOverridePerUnit)
				.buildAndSave();

		commissions = this.tradeCommissionService.getTradeCommissionListExpandedByTrade(sell.getId());
		Assertions.assertEquals(1, commissions.size());

		// Execute the sell, thus partially closing the overall position
		this.tradeUtils.fullyExecuteTrade(sell);

		detailBuilder = AccountingJournalDetailBuilder.builder(sell);
		List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, cch14)
				.build());
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_COMMISSION))
				.price(commissionOverridePerUnit)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(new BigDecimal("150")) // 2.5 * 60 = 150
				.description(GeneralLedgerDescriptions.COMMISSION_EXPENSE, cch14)
				.build());
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("-150")) // commissions only
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, cch14)
				.build());

		// Get the actual details for the sell
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
	}


	/**
	 * Tests creating and booking a trade with a Commission override of 0 and a Fee override of 0 set on the Trade itself. Normally this trade should result
	 * in a Commission and Transaction Fee, but the override of 0 will prevent those journal details from being created.
	 */
	//TODO Make this test more realistic
	//@Test
	public void testZeroCommissionAndFeeOverrides() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("TX1802D17E0500XXI");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.clearedCreditDefaultSwaps);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL);

		// Create and execute a buy trade with a commission and fee of 0
		BigDecimal notionalForBuy = new BigDecimal("2000000");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, notionalForBuy, BigDecimal.ONE, true, new BigDecimal(100), buyDate, executingBroker, accountInfo)
				.withCommissionAmount(BigDecimal.ZERO)
				.withFeeAmount(BigDecimal.ZERO)
				.withEnableCommissionOverride(true)
				.buildAndSave();

		this.tradeUtils.fullyExecuteTrade(buy);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);

		// Build the expected list of details
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);
		List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(buy.getAverageUnitPrice())
				.quantity(notionalForBuy)
				.opening(true)
				.localDebitCredit(new BigDecimal("1980000"))
				.baseDebitCredit(new BigDecimal("2723292"))
				.description(GeneralLedgerDescriptions.POSITION_OPENING, swap)
				.build());

		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_PREMIUM_LEG))
				.price(null)
				.quantity(null)
				.opening(true)
				.localDebitCredit(new BigDecimal("-200000"))
				.baseDebitCredit(new BigDecimal("-275080"))
				.description(GeneralLedgerDescriptions.ACCRUED_INTEREST_LEG, swap)
				.build());

		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(this.eur)
				.localDebitCredit(new BigDecimal("-1780000"))
				.baseDebitCredit(new BigDecimal("-2448212"))
				.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_OPEN, swap)
				.build());

		// Validate the buy trade
		AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);
	}


	@Test
	public void testCommissionOverrideWithCurrency() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a buy trade
		BigDecimal buyQuantity = new BigDecimal("20");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, executingBroker, accountInfo).buildAndSave();

		// Create a commission override for SEC Fees in Euros
		BigDecimal secFeeOverrideAmountForBuy = new BigDecimal("-3.14");
		TradeCommissionOverride secFeeOverrideForBuy = new TradeCommissionOverride();
		secFeeOverrideForBuy.setAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_SEC_FEES));
		secFeeOverrideForBuy.setTrade(buy);
		secFeeOverrideForBuy.setCommissionCurrency(this.eur);
		secFeeOverrideForBuy.setTotalAmount(secFeeOverrideAmountForBuy);
		this.tradeCommissionService.saveTradeCommissionOverride(secFeeOverrideForBuy);

		this.tradeUtils.fullyExecuteTrade(buy);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> buyDetails = this.accountingUtils.getFirstTradeFillDetails(buy, true);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(buy);

		// Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedListForBuy = new ArrayList<>();
		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(buyQuantity)
				.opening(true)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.POSITION_OPENING, cch14)
				.build());

		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_SEC_FEES))
				//Exchange rate from EUR to USD on 12/16/2013 is 1.3754.  1.3754 x 3.14 ~= 4.32
				.localDebitCredit(new BigDecimal("4.32"))
				//4.32 / 20 ~= 0.22
				.price(new BigDecimal("0.216"))
				.description(GeneralLedgerDescriptions.SEC_FEES_EXPENSE, cch14)
				.build());

		expectedListForBuy.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(this.eur)
				.price(null)
				.quantity(null)
				.localDebitCredit(secFeeOverrideAmountForBuy)
				.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_OPEN, cch14)
				.build());

		// Validate the buy trade
		AccountingUtils.assertJournalDetailList(expectedListForBuy, buyDetails);

		// Create and execute a sell trade
		BigDecimal sellQuantity = new BigDecimal("10");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo).buildAndSave();

		// Create a commission override for SEC Fees in Euros
		BigDecimal secFeeOverrideAmountForSell = new BigDecimal("-1.59");
		TradeCommissionOverride secFeeOverrideForSell = new TradeCommissionOverride();
		secFeeOverrideForSell.setAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_SEC_FEES));
		secFeeOverrideForSell.setTrade(sell);
		secFeeOverrideForSell.setCommissionCurrency(this.eur);
		secFeeOverrideForSell.setTotalAmount(secFeeOverrideAmountForSell);
		this.tradeCommissionService.saveTradeCommissionOverride(secFeeOverrideForSell);

		this.tradeUtils.fullyExecuteTrade(sell);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		BigDecimal expectedClearingPrice = new BigDecimal("6.3"); //3.15 * 2
		BigDecimal expectedExecutingPrice = new BigDecimal("3"); //1.5 * 2
		BigDecimal expectedCommissionDebit = new BigDecimal("93"); // 10 * 9.3 = 93

		detailBuilder = AccountingJournalDetailBuilder.builder(sell);

		// Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_SEC_FEES))
				//Exchange rate from EUR to USD on 12/17/2013 is 1.37305.  1.37305 x 1.59 ~= 2.18
				.localDebitCredit(new BigDecimal("2.18"))
				//2.18 / 10 ~= 0.22
				.price(new BigDecimal("0.218"))
				.opening(true)
				.quantity(sellQuantity)
				.description(GeneralLedgerDescriptions.SEC_FEES_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(this.eur)
				.price(null)
				.quantity(null)
				.localDebitCredit(secFeeOverrideAmountForSell)
				.description(GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_CLOSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.security(cch14)
				.price(expectedClearingPrice)
				.quantity(sellQuantity)
				.localDebitCredit(MathUtils.multiply(expectedClearingPrice, sellQuantity))
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.security(cch14)
				.price(expectedExecutingPrice)
				.quantity(sellQuantity)
				.localDebitCredit(MathUtils.multiply(expectedExecutingPrice, sellQuantity))
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(expectedCommissionDebit.negate())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, cch14)
				.build());

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
	}


	@Test
	public void testCommissionOverrideWithCurrencyMultipleThreads() {
		Callable<Boolean> task = () -> {
			testCommissionOverrideWithCurrency();
			return Boolean.TRUE;
		};
		List<ConcurrentExecutionUtils.ConcurrentExecutionResponse<Boolean>> results = ConcurrentExecutionUtils.executeConcurrently(task);
		List<String> errors = new ArrayList<>();
		for (ConcurrentExecutionUtils.ConcurrentExecutionResponse<Boolean> result : results) {
			if (result.isSuccessful()) {
				Assertions.assertTrue(result.getResult());
			}
			else {
				String message = ExceptionUtils.getOriginalMessage(result.getError());
				if (!message.contains("deadlock")) {
					errors.add(message);
				}
			}
		}
		Assertions.assertEquals(0, errors.size(), "Expected 0 failures, but " + errors.size() + " of " + results.size() + " failed. Errors: " + StringUtils.join(errors, "\n"));
	}


	@Test
	public void testCommissionOverrideWithMoreThanTwoDecimalPrecision() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create a buy trade
		BigDecimal buyQuantity = new BigDecimal("100");
		BigDecimal fill1Quantity = new BigDecimal("41");
		BigDecimal fill2Quantity = new BigDecimal("39");
		BigDecimal fill3Quantity = new BigDecimal("20");
		BigDecimal commissionPerUnitOverride = new BigDecimal("0.005");
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buy = this.tradeUtils.newFuturesTradeBuilder(cch14, buyQuantity, true, buyDate, executingBroker, accountInfo)
				.addFill(fill1Quantity, BigDecimal.ONE)
				.addFill(fill2Quantity, BigDecimal.ONE)
				.addFill(fill3Quantity, BigDecimal.ONE)
				.withCommissionPerUnit(commissionPerUnitOverride)
				.withEnableCommissionOverride(true)
				.buildAndSave();

		List<TradeCommission<?>> commissions = this.tradeCommissionService.getTradeCommissionListExpandedByTrade(buy.getId());
		Assertions.assertEquals(1, commissions.size());

		TradeCommission<Integer> expectedCommission = new TradeCommission<>();
		expectedCommission.setSource(TradeCommissionSources.OVERRIDE);
		expectedCommission.setExpenseAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_COMMISSION));
		expectedCommission.setAmountPerUnit(commissionPerUnitOverride);
		CoreCompareUtils.isNonNullPropertiesEqual(expectedCommission, commissions.get(0), true, null);

		// Execute the buy, thus opening a position
		this.tradeUtils.fullyExecuteTrade(buy);

		// Validate the first fill
		List<? extends AccountingJournalDetailDefinition> fill1Details = this.accountingUtils.getTradeFillDetails(buy.getTradeFillList().get(0));
		AccountingUtils.assertJournalDetailList(fill1Details,
				"16348927	null	CA973739696	HA1866420090	Position	CCH14	1	41	0	12/16/2013	1	0	410	12/16/2013	12/16/2013	Position opening of CCH14",
				"16348928	16348927	CA973739696	HA1866420090	Commission	CCH14	0.005	41	0.21	12/16/2013	1	0.21	0	12/16/2013	12/16/2013	Commission expense for CCH14",
				"16348929	16348927	CA973739696	HA1866420090	Cash	USD			-0.21	12/16/2013	1	-0.21	0	12/16/2013	12/16/2013	Cash expense from opening of CCH14"
		);

		// Validate the second fill
		List<? extends AccountingJournalDetailDefinition> fill2Details = this.accountingUtils.getTradeFillDetails(buy.getTradeFillList().get(1));
		AccountingUtils.assertJournalDetailList(fill2Details,
				"16349071	null	CA2024299155	HA1867012964	Position	CCH14	1	39	0	12/16/2013	1	0	390	12/16/2013	12/16/2013	Position opening of CCH14",
				"16349072	16349071	CA2024299155	HA1867012964	Commission	CCH14	0.005	39	0.2	12/16/2013	1	0.2	0	12/16/2013	12/16/2013	Commission expense for CCH14",
				"16349073	16349071	CA2024299155	HA1867012964	Cash	USD			-0.2	12/16/2013	1	-0.2	0	12/16/2013	12/16/2013	Cash expense from opening of CCH14"
		);

		// Validate the third fill
		List<? extends AccountingJournalDetailDefinition> fill3Details = this.accountingUtils.getTradeFillDetails(buy.getTradeFillList().get(2));
		AccountingUtils.assertJournalDetailList(fill3Details,
				"16349215	null	CA1367871507	HA1933191651	Position	CCH14	1	20	0	12/16/2013	1	0	200	12/16/2013	12/16/2013	Position opening of CCH14",
				"16349216	16349215	CA1367871507	HA1933191651	Commission	CCH14	0.005	20	0.1	12/16/2013	1	0.1	0	12/16/2013	12/16/2013	Commission expense for CCH14",
				"16349217	16349215	CA1367871507	HA1933191651	Cash	USD			-0.1	12/16/2013	1	-0.1	0	12/16/2013	12/16/2013	Cash expense from opening of CCH14"
		);

		// Create a sell trade
		BigDecimal sellQuantity = new BigDecimal("60");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo)
				.withCommissionPerUnit(commissionPerUnitOverride)
				.withEnableCommissionOverride(true)
				.buildAndSave();

		// Create a flat override for Management Fee for the sell trade
		TradeCommissionOverride managementFeeOverride = new TradeCommissionOverride();
		String overrideNote = "This is a test";
		managementFeeOverride.setAccountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXPENSE_MANAGEMENT_FEE));
		managementFeeOverride.setTotalAmount(new BigDecimal("-50"));
		managementFeeOverride.setOverrideNote(overrideNote);
		managementFeeOverride.setTrade(sell);
		this.tradeCommissionService.saveTradeCommissionOverride(managementFeeOverride);

		// Execute the sell, thus partially closing the overall position
		this.tradeUtils.fullyExecuteTrade(sell);

		// Get the actual details for the sell
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(sellDetails,
				"16349359	16349350	CA464513772	HA1901475788	Position	CCH14	1	-41	0	12/17/2013	1	0	-410	12/16/2013	12/17/2013	Full position close for CCH14",
				"16349360	16349353	CA464513772	HA1901475788	Position	CCH14	1	-19	0	12/17/2013	1	0	-190	12/16/2013	12/17/2013	Partial position close for CCH14",
				"16349361	16349359	CA464513772	HA1901475788	Management Fee	CCH14	0.8334	41	34.17	12/17/2013	1	34.17	0	12/16/2013	12/17/2013	Management Fee expense for CCH14",
				"16349362	16349360	CA464513772	HA1901475788	Management Fee	CCH14	0.833	19	15.83	12/17/2013	1	15.83	0	12/16/2013	12/17/2013	Management Fee expense for CCH14",
				"16349363	16349359	CA464513772	HA1901475788	Commission	CCH14	0.005	41	0.21	12/17/2013	1	0.21	0	12/16/2013	12/17/2013	Commission expense for CCH14",
				"16349364	16349360	CA464513772	HA1901475788	Commission	CCH14	0.005	19	0.1	12/17/2013	1	0.1	0	12/16/2013	12/17/2013	Commission expense for CCH14",
				"16349365	16349359	CA464513772	HA1901475788	Cash	USD			-34.38	12/17/2013	1	-34.38	0	12/16/2013	12/17/2013	Cash expense from close of CCH14",
				"16349366	16349360	CA464513772	HA1901475788	Cash	USD			-15.93	12/17/2013	1	-15.93	0	12/16/2013	12/17/2013	Cash expense from close of CCH14"
		);
	}
}
