package com.clifton.ims.tests.workflow.transition;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.email.FakeSMTPServer;
import com.clifton.core.util.FunctionUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.EmailHandlerImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.integration.export.email.FakeSMTPServerUtils;
import com.clifton.workflow.task.WorkflowTask;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.EmailSendingWorkflowActionHandler;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Collections;


/**
 * @author lnaylor
 */
public class EmailSendingWorkflowActionTests extends BaseImsIntegrationTest {

	private static final Integer POLL_SLEEP_TIME = 1000;
	private static final Integer POLL_LIMIT_COUNT = 60;

	private static final Integer SMTP_PORT = 2500;

	private static final String EMAIL_A = "lnaylor@paraport.com";

	private static final String HOST_NAME = FunctionUtils.uncheckedSupplier(() -> InetAddress.getLocalHost().getHostName()).get();

	private final Lazy<FakeSMTPServer> fakeSmtpServerLazy = new Lazy<>(() -> new FakeSMTPServer(SMTP_PORT));

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Resource
	private FakeSMTPServerUtils fakeSMTPServerUtils;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() throws IOException {
		getFakeSmtpServer().start();
	}


	@AfterEach
	public void cleanup() {
		getFakeSmtpServer().clearMessages();
		getFakeSmtpServer().stop();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testProcessAction() throws Exception {
		EmailHandler emailHandler = new EmailHandlerImpl(HOST_NAME, SMTP_PORT, "smtp", "smtp", "noreply@paraport.com");
		EmailSendingWorkflowActionHandler<WorkflowTask> emailSendingWorkflowActionHandler = new EmailSendingWorkflowActionHandler<>();
		emailSendingWorkflowActionHandler.setEmailHandler(emailHandler);
		emailSendingWorkflowActionHandler.setTemplateConverter(new FreemarkerTemplateConverter());

		emailSendingWorkflowActionHandler.setToEmails(Collections.singletonList(EMAIL_A));
		emailSendingWorkflowActionHandler.setFromEmail(EMAIL_A);
		emailSendingWorkflowActionHandler.setSubject("SUBJECT");
		emailSendingWorkflowActionHandler.setText("TEXT");

		WorkflowTask task = new WorkflowTask();
		WorkflowTransition transition = new WorkflowTransition();

		emailSendingWorkflowActionHandler.processAction(task, transition);
		Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(getFakeSmtpServer(), POLL_LIMIT_COUNT, POLL_SLEEP_TIME), "No messages were found in the SMTP server.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FakeSMTPServer getFakeSmtpServer() {
		return this.fakeSmtpServerLazy.get();
	}
}
