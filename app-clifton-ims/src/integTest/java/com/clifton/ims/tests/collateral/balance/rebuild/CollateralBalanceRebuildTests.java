package com.clifton.ims.tests.collateral.balance.rebuild;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildCommand;
import com.clifton.collateral.balance.rebuild.CollateralBalanceRebuildService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.config.RunnerConfigSearchForm;
import com.clifton.core.util.runner.config.RunnerConfigService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * @author stevenf
 */
public class CollateralBalanceRebuildTests extends BaseImsIntegrationTest {

	@Resource
	CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private CollateralService collateralService;

	@Resource
	private CollateralBalanceRebuildService collateralBalanceRebuildService;

	@Resource
	private RunnerConfigService runnerConfigService;


	@Test
	public void testRebuildCollateralBalance() {
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("051400");
		InvestmentAccount holdingAccount = this.investmentAccountUtils.getHoldingAccountByNumber("A17-BNYM");

		CollateralBalanceRebuildCommand rebuildCommand = new CollateralBalanceRebuildCommand();
		rebuildCommand.setBalanceDate(DateUtils.toDate("07/01/2019"));
		rebuildCommand.setCollateralType(this.collateralService.getCollateralTypeByName("OTC Collateral"));
		this.collateralBalanceRebuildService.rebuildCollateralBalance(rebuildCommand);

		rebuildCommand.setBusinessClientId(clientAccount.getBusinessClient().getId());
		this.collateralBalanceRebuildService.rebuildCollateralBalance(rebuildCommand);

		rebuildCommand.setBusinessClientId(null);
		rebuildCommand.setHoldingAccountId(holdingAccount.getId());
		this.collateralBalanceRebuildService.rebuildCollateralBalance(rebuildCommand);

		rebuildCommand.setHoldingAccountId(null);
		rebuildCommand.setIssuingCompanyId(holdingAccount.getIssuingCompany().getId());
		this.collateralBalanceRebuildService.rebuildCollateralBalance(rebuildCommand);

		RunnerConfigSearchForm searchForm = new RunnerConfigSearchForm();
		searchForm.setType("COLLATERAL-BALANCE");

		List<String> runnerConfigTypeIDList = new ArrayList<>();
		this.runnerConfigService.getRunnerConfigList(searchForm)
				.forEach(runnerConfig -> runnerConfigTypeIDList.add(runnerConfig.getTypeId()));
		Assertions.assertTrue(runnerConfigTypeIDList.contains("_OTC Collateral_" + DateUtils.fromDate(rebuildCommand.getBalanceDate())));
		Assertions.assertTrue(runnerConfigTypeIDList.contains("CLIENT_" + clientAccount.getBusinessClient().getId() + "_OTC Collateral_" + DateUtils.fromDate(rebuildCommand.getBalanceDate())));
		Assertions.assertTrue(runnerConfigTypeIDList.contains("HOLDER_" + holdingAccount.getId() + "_OTC Collateral_" + DateUtils.fromDate(rebuildCommand.getBalanceDate())));
		Assertions.assertTrue(runnerConfigTypeIDList.contains("ISSUER_" + holdingAccount.getIssuingCompany().getId() + "_OTC Collateral_" + DateUtils.fromDate(rebuildCommand.getBalanceDate())));
	}
}
