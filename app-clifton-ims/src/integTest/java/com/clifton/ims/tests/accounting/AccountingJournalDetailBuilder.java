package com.clifton.ims.tests.accounting;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>AccountingJournalDetailBuilder</code> is a builder for building {@link AccountingJournalDetailDefinition}s. It is primarily intended
 * to be used as a builder of an expected journal detail in an integration test and used in conjunction with methods like
 * {@link AccountingUtils#assertJournalDetail(AccountingJournalDetailDefinition, AccountingJournalDetailDefinition)}.
 *
 * @author jgommels
 */
@SuppressWarnings("hiding")
public class AccountingJournalDetailBuilder {

	private AccountingAccount glAccount;
	private InvestmentAccount clientAccount;
	private InvestmentAccount holdingAccount;
	private InvestmentSecurity security;
	private BusinessCompany executingCompany;
	private BigDecimal price;
	private BigDecimal quantity;
	private BigDecimal localDebitCredit;
	private BigDecimal baseDebitCredit;
	private boolean opening;
	private String description;
	private Date transactionDate;
	private Date settlementDate;


	private AccountingJournalDetailBuilder() {

	}


	public static AccountingJournalDetailBuilder builder() {
		return new AccountingJournalDetailBuilder();
	}


	public static AccountingJournalDetailBuilder builder(Trade trade) {
		AccountingJournalDetailBuilder builder = new AccountingJournalDetailBuilder();
		builder.clientAccount = trade.getClientInvestmentAccount();
		builder.holdingAccount = trade.getHoldingInvestmentAccount();
		builder.security = trade.getInvestmentSecurity();
		builder.executingCompany = trade.getExecutingBrokerCompany();
		builder.transactionDate = trade.getTradeDate();
		builder.settlementDate = trade.getSettlementDate();
		return builder;
	}


	public static AccountingJournalDetailBuilder builder(AccountingJournalDetailDefinition journalDetail) {
		AccountingJournalDetailBuilder builder = new AccountingJournalDetailBuilder();
		builder.glAccount = journalDetail.getAccountingAccount();
		builder.clientAccount = journalDetail.getClientInvestmentAccount();
		builder.holdingAccount = journalDetail.getHoldingInvestmentAccount();
		builder.security = journalDetail.getInvestmentSecurity();
		builder.executingCompany = journalDetail.getExecutingCompany();
		builder.price = journalDetail.getPrice();
		builder.quantity = journalDetail.getQuantity();
		builder.localDebitCredit = journalDetail.getLocalDebitCredit();
		builder.baseDebitCredit = journalDetail.getBaseDebitCredit();
		builder.opening = journalDetail.isOpening();
		builder.description = journalDetail.getDescription();
		builder.transactionDate = journalDetail.getTransactionDate();
		builder.settlementDate = journalDetail.getSettlementDate();
		return builder;
	}


	public AccountingJournalDetailDefinition build() {
		AccountingJournalDetailDefinition journalDetail = new AccountingTransaction();
		journalDetail.setAccountingAccount(this.glAccount);
		journalDetail.setClientInvestmentAccount(this.clientAccount);
		journalDetail.setHoldingInvestmentAccount(this.holdingAccount);
		journalDetail.setExecutingCompany(this.executingCompany);
		journalDetail.setInvestmentSecurity(this.security);
		journalDetail.setSettlementDate(this.settlementDate);
		journalDetail.setTransactionDate(this.transactionDate);
		journalDetail.setPrice(this.price);
		journalDetail.setLocalDebitCredit(this.localDebitCredit);
		journalDetail.setBaseDebitCredit(this.baseDebitCredit);
		journalDetail.setOpening(this.opening);
		journalDetail.setQuantity(this.quantity);
		journalDetail.setDescription(this.description);

		return journalDetail;
	}


	public AccountingJournalDetailBuilder accountingAccount(AccountingAccount glAccount) {
		this.glAccount = glAccount;
		return this;
	}


	public AccountingJournalDetailBuilder clientInvestmentAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
		return this;
	}


	public AccountingJournalDetailBuilder holdingInvestmentAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
		return this;
	}


	public AccountingJournalDetailBuilder executingCompany(BusinessCompany executingCompany) {
		this.executingCompany = executingCompany;
		return this;
	}


	public AccountingJournalDetailBuilder security(InvestmentSecurity security) {
		this.security = security;
		return this;
	}


	public AccountingJournalDetailBuilder price(BigDecimal price) {
		this.price = price;
		return this;
	}


	public AccountingJournalDetailBuilder quantity(BigDecimal quantity) {
		this.quantity = quantity;
		return this;
	}


	public AccountingJournalDetailBuilder localDebitCredit(BigDecimal localDebitCredit) {
		this.localDebitCredit = localDebitCredit;
		return this;
	}


	public AccountingJournalDetailBuilder baseDebitCredit(BigDecimal baseDebitCredit) {
		this.baseDebitCredit = baseDebitCredit;
		return this;
	}


	public AccountingJournalDetailBuilder opening(boolean opening) {
		this.opening = opening;
		return this;
	}


	public AccountingJournalDetailBuilder description(String description) {
		this.description = description;
		return this;
	}


	public AccountingJournalDetailBuilder description(String description, InvestmentSecurity security) {
		this.description = GeneralLedgerDescriptions.format(description, security);
		return this;
	}


	public AccountingJournalDetailBuilder description(String description, InvestmentAccount account) {
		this.description = GeneralLedgerDescriptions.format(description, account);
		return this;
	}


	public AccountingJournalDetailBuilder transactionDate(Date date) {
		this.transactionDate = date;
		return this;
	}


	public AccountingJournalDetailBuilder settlementDate(Date date) {
		this.settlementDate = date;
		return this;
	}
}
