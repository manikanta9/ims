package com.clifton.ims.tests.system.column;

import com.clifton.ims.tests.spring.ImsIntegrationConfiguration;
import com.clifton.test.system.column.BaseSystemColumnTests;
import org.springframework.test.context.ContextConfiguration;


/**
 * @author manderson
 */
@ContextConfiguration(classes = {ImsIntegrationConfiguration.class})
public class SystemColumnTests extends BaseSystemColumnTests {

	// Nothing here
}
