package com.clifton.ims.tests.accounting.positiontransfer.upload;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.accounting.positiontransfer.upload.AccountingPositionTransferUploadCustomCommand;
import com.clifton.accounting.positiontransfer.upload.AccountingPositionTransferUploadService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverterWithDynamicColumnCreation;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingJournalDetailBuilder;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.accounting.GeneralLedgerDescriptions;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * This test executes nearly the same tests as the other position transfer tests, but
 * uses a dynamically generated temporary XML file upload to perform the transfer.
 *
 * @author NickK
 */
public class AccountingPositionTransferUploadTests extends BaseImsIntegrationTest {

	private static InvestmentSecurity TPZ5;
	private static InvestmentSecurity CCH14;

	@Resource
	private AccountingPositionTransferUploadService accountingPositionTransferUploadService;

	@Resource
	private AccountingPositionTransferService accountingPositionTransferService;


	@BeforeEach
	public void beforeTest() {
		if (TPZ5 == null) {
			TPZ5 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("TPZ5");
		}
		if (CCH14 == null) {
			CCH14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");
		}
	}


	@Test
	public void testRoundTurnCommissionWithPositionTransferForFutures() {
		// Create new accounts
		AccountInfo toAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(toAccountInfo);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccountInfo, CCH14.getInstrument());

		Date transactionDate = DateUtils.toDate("12/3/2013");
		BigDecimal transferQuantity = BigDecimal.valueOf(30.0);

		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setToClientInvestmentAccountNumber(toAccountInfo.getClientAccount().getNumber());
		command.setToHoldingInvestmentAccountNumber(toAccountInfo.getHoldingAccount().getNumber());
		command.setTypeName(AccountingUtils.TO_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(transactionDate);
		command.setNote("Transfer to client account " + toAccountInfo.getClientAccount().getName() + " and holding account " + toAccountInfo.getHoldingAccount().getName());
		command.setSecuritySymbol(CCH14.getSymbol());
		command.setQuantity(transferQuantity);
		command.setCostPrice(BigDecimal.ONE);
		command.setOriginalPositionOpenDate(DateUtils.toDate("12/2/2013"));
		command.setCommissionPerUnitOverride(BigDecimal.ZERO);
		command.setTransferPrice(command.getCostPrice());
		command.setExchangeRateToBase(BigDecimal.ONE);

		// Upload and Book transfer
		uploadTransferWithUploadTemplateList(Collections.singletonList(command));
		AccountingPositionTransfer transfer = bookTransfer(null, toAccountInfo, transactionDate, AccountingUtils.TO_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);

		// Build the expected list of details for the transfer
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.security(CCH14);
		List<AccountingJournalDetailDefinition> expectedTransferDetails = new ArrayList<>();
		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.quantity(transferQuantity)
				.opening(true)
				.clientInvestmentAccount(toAccountInfo.getClientAccount())
				.holdingInvestmentAccount(toAccountInfo.getHoldingAccount())
				.description("Position opening from transfer from external account")
				.build());

		List<? extends AccountingJournalDetailDefinition> actualTransferDetails = this.accountingUtils.getPositionTransferDetails(transfer);

		AccountingUtils.assertJournalDetailList(expectedTransferDetails, actualTransferDetails);
	}


	@Test
	public void testTransferBetweenHoldingAccounts() {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createTradeAllComplianceRule(toAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, CCH14.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, CCH14.getInstrument());

		// Create a position for 'fromAccount'
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("12/16/2013");
		BigDecimal positionSize = new BigDecimal("20");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(CCH14, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		//Get the position to transfer
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(fromAccount, CCH14, openDate);
		Assertions.assertEquals(1, positionList.size());

		//Transfer the position to the other account
		Date transactionDate = DateUtils.toDate("12/17/2013");

		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setFromClientInvestmentAccountNumber(fromAccount.getClientAccount().getNumber());
		command.setFromHoldingInvestmentAccountNumber(fromAccount.getHoldingAccount().getNumber());
		command.setToClientInvestmentAccountNumber(toAccount.getClientAccount().getNumber());
		command.setToHoldingInvestmentAccountNumber(toAccount.getHoldingAccount().getNumber());
		command.setTypeName(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(transactionDate);
		command.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "\nto client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());
		command.setSecuritySymbol(positionList.get(0).getInvestmentSecurity().getSymbol());
		command.setQuantity(positionSize);
		command.setTransferPrice(positionList.get(0).getPrice());

		// Upload and Book transfer
		uploadTransferWithUploadTemplateList(Collections.singletonList(command));
		AccountingPositionTransfer transfer = bookTransfer(fromAccount, toAccount, transactionDate, AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);

		// Build the expected list of details for the transfer
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.security(CCH14);
		List<AccountingJournalDetailDefinition> expectedTransferDetails = new ArrayList<>();
		BigDecimal expectedClearingPrice = new BigDecimal("3.15");
		BigDecimal expectedExecutingPrice = new BigDecimal("1.5");
		BigDecimal expectedCommissionDebitForTransfer = new BigDecimal("93"); //4.65 x 20 = 93

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(positionSize.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.quantity(positionSize)
				.opening(true)
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingPrice)
				.localDebitCredit(MathUtils.multiply(expectedClearingPrice, positionSize))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutingPrice)
				.localDebitCredit(MathUtils.multiply(expectedExecutingPrice, positionSize))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(expectedCommissionDebitForTransfer.negate())
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		List<? extends AccountingJournalDetailDefinition> actualTransferDetails = this.accountingUtils.getPositionTransferDetails(transfer);

		AccountingUtils.assertJournalDetailList(expectedTransferDetails, actualTransferDetails);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a sell trade
		BigDecimal sellQuantity = new BigDecimal("5");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(CCH14, sellQuantity, false, sellDate, executingBroker, toAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		BigDecimal expectedCommissionDebitForSell = new BigDecimal("23.25"); // 4.65 x 5 = 23.25

		//Build the expected list of details for the sell (position close)
		detailBuilder = AccountingJournalDetailBuilder.builder(sell);
		List<AccountingJournalDetailDefinition> expectedSellDetails = new ArrayList<>();
		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingPrice)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(MathUtils.multiply(expectedClearingPrice, sellQuantity))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutingPrice)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(MathUtils.multiply(expectedExecutingPrice, sellQuantity))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.opening(true)
				.localDebitCredit(expectedCommissionDebitForSell.negate())
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, CCH14)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		// Validate the sell (position close)
		AccountingUtils.assertJournalDetailList(expectedSellDetails, actualSellDetails);
	}


	@Test
	public void testTransferWithCommissionOverride() {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, CCH14.getInstrument());

		// Create a position for the account
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("11/5/2013");
		BigDecimal positionSize = new BigDecimal("30");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(CCH14, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		AccountingTransaction positionOpeningDetail = (AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0);

		Date transactionDate = DateUtils.toDate("12/3/2013");
		BigDecimal commissionOverride = new BigDecimal("7.0");

		//Create a position transfer detail of 10 CCH14 with a commission override of 7.0
		List<AccountingPositionTransferCommand> uploadTemplateList = new ArrayList<>();
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setFromClientInvestmentAccountNumber(fromAccount.getClientAccount().getNumber());
		command.setFromHoldingInvestmentAccountNumber(fromAccount.getHoldingAccount().getNumber());
		command.setTypeName(AccountingUtils.FROM_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(transactionDate);
		command.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName());
		command.setSecuritySymbol(positionOpeningDetail.getInvestmentSecurity().getSymbol());
		command.setQuantity(new BigDecimal(10));
		command.setTransferPrice(positionOpeningDetail.getPrice());
		command.setCommissionPerUnitOverride(commissionOverride);
		uploadTemplateList.add(command);
		//Create another position transfer detail of 10 CCH14 with no commission override
		AccountingPositionTransferCommand command2 = new AccountingPositionTransferCommand();
		BeanUtils.copyProperties(command, command2, new String[]{"commissionPerUnitOverride"});
		uploadTemplateList.add(command2);

		// Upload and Book transfer
		uploadTransferWithUploadTemplateList(uploadTemplateList);
		AccountingPositionTransfer transfer = bookTransfer(fromAccount, null, transactionDate, AccountingUtils.FROM_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);

		List<? extends AccountingJournalDetailDefinition> actualJournalDetails = this.accountingUtils.getJournal(transfer).getJournalDetailList();

		AccountingUtils.assertJournalDetailList(actualJournalDetails,
				"16346110	16346109	CA1541565645	HA1679217164	Position	CCH14	1	-10	0	12/03/2013	1	0	-100	11/05/2013	12/03/2013	Position close from transfer to external account",
				"16346111	16346109	CA1541565645	HA1679217164	Position	CCH14	1	-10	0	12/03/2013	1	0	-100	11/05/2013	12/03/2013	Position close from transfer to external account",
				"16346112	16346110	CA1541565645	HA1679217164	Commission	CCH14	7	10	70	12/03/2013	1	70	0	11/05/2013	12/03/2013	Commission expense for CCH14",
				"16346113	16346110	CA1541565645	HA1679217164	Cash	USD			-70	12/03/2013	1	-70	0	11/05/2013	12/03/2013	Cash expense from close of CCH14",
				"16346114	16346111	CA1541565645	HA1679217164	Clearing Commission	CCH14	3.15	10	31.5	12/03/2013	1	31.5	0	11/05/2013	12/03/2013	Clearing Commission expense for CCH14",
				"16346115	16346111	CA1541565645	HA1679217164	Executing Commission	CCH14	1.5	10	15	12/03/2013	1	15	0	11/05/2013	12/03/2013	Executing Commission expense for CCH14",
				"16346116	16346111	CA1541565645	HA1679217164	Cash	USD			-46.5	12/03/2013	1	-46.5	0	11/05/2013	12/03/2013	Cash expense from close of CCH14"
		);
	}


	@Test
	public void testTransferOfForeignFuturePositionBetweenAccounts() {
		InvestmentSecurity zm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("Z M4");

		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createTradeAllComplianceRule(toAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, zm4.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, zm4.getInstrument());

		// Create a position for the account
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("5/23/2014");
		BigDecimal positionSize = new BigDecimal("60");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(zm4, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		AccountingTransaction positionOpeningDetail = (AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0);

		Date transactionDate = DateUtils.toDate("5/27/2014");

		BigDecimal transferPrice = new BigDecimal("6829.50");

		//Create a position transfer detail of 10 CCH14 with a commission override of 7.0
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setFromClientInvestmentAccountNumber(fromAccount.getClientAccount().getNumber());
		command.setFromHoldingInvestmentAccountNumber(fromAccount.getHoldingAccount().getNumber());
		command.setToClientInvestmentAccountNumber(toAccount.getClientAccount().getNumber());
		command.setToHoldingInvestmentAccountNumber(toAccount.getHoldingAccount().getNumber());
		command.setTypeName(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(transactionDate);
		command.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "to client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());
		command.setSecuritySymbol(positionOpeningDetail.getInvestmentSecurity().getSymbol());
		command.setQuantity(positionSize);
		command.setTransferPrice(transferPrice);

		// Upload and Book transfer
		uploadTransferWithUploadTemplateList(Collections.singletonList(command));
		AccountingPositionTransfer transfer = bookTransfer(fromAccount, toAccount, transactionDate, AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.transactionDate(transactionDate);

		List<AccountingJournalDetailDefinition> expectedJournalDetails = new ArrayList<>();

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.security(zm4)
				.price(transferPrice)
				.quantity(positionSize.negate())
				.opening(false)
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.quantity(positionSize)
				.opening(true)
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.localDebitCredit(new BigDecimal("-4097100"))
				.baseDebitCredit(new BigDecimal("-6900130.97"))
				.opening(false)
				.description(GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_GAIN, zm4)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(new BigDecimal("0.6"))
				.localDebitCredit(new BigDecimal("36"))
				.baseDebitCredit(new BigDecimal("60.63"))
				.opening(true)
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, zm4)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(new BigDecimal("0.48"))
				.localDebitCredit(new BigDecimal("28.8"))
				.baseDebitCredit(new BigDecimal("48.5"))
				.opening(true)
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, zm4)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(this.investmentInstrumentUtils.getCurrencyBySymbol("GBP"))
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("4097035.2"))
				.baseDebitCredit(new BigDecimal("6900021.84"))
				.description(GeneralLedgerDescriptions.CURRENCY_PROCEEDS_FROM_CLOSE, zm4)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		List<? extends AccountingJournalDetailDefinition> actualJournalDetails = this.accountingUtils.getJournal(transfer).getJournalDetailList();

		AccountingUtils.assertJournalDetailList(expectedJournalDetails, actualJournalDetails);
	}


	@Test
	public void testTransferOfMultipleForeignFutureSellLotPositionsBetweenAccounts() {
		testTransferOfMultipleForeignFutureLotPositionsBetweenAccounts(false);
	}


	@Test
	public void testTransferOfMultipleForeignFutureBuyLotPositionsBetweenAccounts() {
		testTransferOfMultipleForeignFutureLotPositionsBetweenAccounts(true);
	}


	private void testTransferOfMultipleForeignFutureLotPositionsBetweenAccounts(boolean buy) {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, this.futures);

		List<AccountingTransaction> positions = openTpz5Trades(fromAccount, buy);

		//Create compliance rules to allow securities
		this.complianceUtils.createTradeAllComplianceRule(fromAccount);
		this.complianceUtils.createTradeAllComplianceRule(toAccount);
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, TPZ5.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, TPZ5.getInstrument());

		Date transactionDate = DateUtils.toDate("10/05/2015");
		BigDecimal transferPrice = new BigDecimal("1441.5");

		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("36962G5Y6");
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, security.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, security.getInstrument());

		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setFromClientInvestmentAccountNumber(fromAccount.getClientAccount().getNumber());
		command.setFromHoldingInvestmentAccountNumber(fromAccount.getHoldingAccount().getNumber());
		command.setToClientInvestmentAccountNumber(toAccount.getClientAccount().getNumber());
		command.setToHoldingInvestmentAccountNumber(toAccount.getHoldingAccount().getNumber());
		command.setTypeName(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);
		command.setTransactionDate(transactionDate);
		command.setSettlementDate(transactionDate);
		command.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "to client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());
		command.setSecuritySymbol(positions.get(0).getInvestmentSecurity().getSymbol());
		// Specify the quantity of both lots so both lots are selected for transfer.
		command.setQuantity(MathUtils.add(positions.get(0).getQuantity(), positions.get(1).getQuantity()));
		command.setLotSelectionMethod("FIFO");
		command.setTransferPrice(transferPrice);

		// Upload and Book transfer
		uploadTransferWithUploadTemplateList(Collections.singletonList(command));
		AccountingPositionTransfer transfer = bookTransfer(fromAccount, toAccount, transactionDate, AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.transactionDate(transactionDate);

		List<AccountingJournalDetailDefinition> expectedJournalDetails = new ArrayList<>();
		BigDecimal firstLotQuantity = new BigDecimal("121");
		BigDecimal secondLogQuantity = new BigDecimal("36");

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.security(TPZ5)
				.price(transferPrice)
				.quantity(buy ? firstLotQuantity.negate() : firstLotQuantity)
				.opening(false)
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.quantity(buy ? firstLotQuantity : firstLotQuantity.negate())
				.opening(true)
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.security(TPZ5)
				.price(transferPrice)
				.quantity(buy ? secondLogQuantity.negate() : secondLogQuantity)
				.opening(false)
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.quantity(buy ? secondLogQuantity : secondLogQuantity.negate())
				.opening(true)
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.quantity(firstLotQuantity)
				.localDebitCredit(new BigDecimal(buy ? "-7623000.00" : "7623000.00"))
				.baseDebitCredit(new BigDecimal(buy ? "-63379.68" : "63379.68"))
				.opening(false)
				.description(buy ? GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_GAIN : GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_LOSS, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.quantity(secondLogQuantity)
				.localDebitCredit(new BigDecimal(buy ? "-11700000.00" : "11700000.00"))
				.baseDebitCredit(new BigDecimal(buy ? "-97589.35" : "97589.35"))
				.opening(false)
				.description(buy ? GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_GAIN : GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_LOSS, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(firstLotQuantity)
				.localDebitCredit(new BigDecimal("21175"))
				.baseDebitCredit(new BigDecimal("176.05"))
				.opening(true)
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(firstLotQuantity)
				.localDebitCredit(new BigDecimal("21175"))
				.baseDebitCredit(new BigDecimal("176.05"))
				.opening(true)
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, TPZ5)
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(secondLogQuantity)
				.localDebitCredit(new BigDecimal("6300.00"))
				.baseDebitCredit(new BigDecimal("52.55"))
				.opening(true)
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(new BigDecimal("175"))
				.quantity(secondLogQuantity)
				.localDebitCredit(new BigDecimal("6300.00"))
				.baseDebitCredit(new BigDecimal("52.55"))
				.opening(true)
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		InvestmentSecurity ccy = this.investmentInstrumentUtils.getCurrencyBySymbol("JPY");
		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(ccy)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal(buy ? "7580650.00" : "-7665350.00"))
				.baseDebitCredit(new BigDecimal(buy ? "63027.58" : "-63731.78"))
				.description(buy ? GeneralLedgerDescriptions.CURRENCY_PROCEEDS_FROM_CLOSE : GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_CLOSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP))
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(ccy)
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal(buy ? "11687400.00" : "-11712600.00"))
				.baseDebitCredit(new BigDecimal(buy ? "97484.25" : "-97694.45"))
				.description(buy ? GeneralLedgerDescriptions.CURRENCY_PROCEEDS_FROM_CLOSE : GeneralLedgerDescriptions.CURRENCY_EXPENSE_FROM_CLOSE, TPZ5)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		List<? extends AccountingJournalDetailDefinition> actualJournalDetails = this.accountingUtils.getJournal(transfer).getJournalDetailList();

		AccountingUtils.assertJournalDetailList(expectedJournalDetails, actualJournalDetails);
	}


	@Test
	public void testHavingNoTransferDateResultsInValidationException() {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setTransactionDate(null);
		command.setSettlementDate(null);
		command.setSecuritySymbol(CCH14.getSymbol());
		command.setQuantity(null);
		command.setTransferPrice(BigDecimal.ONE);

		uploadResultingInValidationException(command, "Cannot process position transfer upload: The transfer import contained a position transfer command row without a Transaction Date defined. The Transaction Date can also be specified on the position transfer upload window.");
	}


	@Test
	public void testHavingIncorrectSecuritySymbolResultsInValidationException() {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setTransactionDate(DateUtils.toDate("12/17/2013"));
		command.setSettlementDate(DateUtils.toDate("12/17/2013"));
		command.setSecuritySymbol(TPZ5.getSymbol());
		command.setQuantity(null);
		command.setTransferPrice(BigDecimal.ONE);

		uploadResultingInValidationException(command, "Cannot process position transfer upload: Cannot find Active Security on 12/17/2013 with Symbol [TPZ15].");
	}


	@Test
	public void testHavingNoQuantityResultsInValidationException() {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setTransactionDate(DateUtils.toDate("12/17/2013"));
		command.setSettlementDate(DateUtils.toDate("12/17/2013"));
		command.setSecuritySymbol(CCH14.getSymbol());
		command.setQuantity(null);
		command.setTransferPrice(BigDecimal.ONE);

		uploadResultingInValidationException(command, "Cannot process position transfer upload: Position transfer command/row has no defined amount to transfer. Expected one of Quantity, Quantity Percent, Original Face, or Original Face Percent to be defined with a value greater than 0.");
	}


	@Test
	public void testHavingIncorrectOriginalFaceResultsInValidationException() {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setTransactionDate(DateUtils.toDate("12/17/2013"));
		command.setSettlementDate(DateUtils.toDate("12/17/2013"));
		command.setSecuritySymbol(CCH14.getSymbol());
		command.setOriginalFace(BigDecimal.ONE);
		command.setTransferPrice(BigDecimal.ONE);

		uploadResultingInValidationException(command, "Cannot process position transfer upload: Position transfer command/row has a specified OriginalFace or Original Face Percent for a Security with Symbol [CCH14], which does not allow Factor Change Events.");
	}


	@Test
	public void testHavingOriginalFacePercentWithNoRoundingResultsInValidationException() {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setTransactionDate(DateUtils.toDate("12/17/2013"));
		command.setSettlementDate(DateUtils.toDate("12/17/2013"));
		command.setSecuritySymbol(CCH14.getSymbol());
		command.setOriginalFacePercent(BigDecimal.ONE);
		command.setTransferPrice(BigDecimal.ONE);

		uploadResultingInValidationException(command, "Cannot process position transfer upload: Cannot calculate an Original Face Percent amount without Original Face Rounding defined.");
	}


	@Test
	public void testHavingIncorrectOriginalFacePercentResultsInValidationException() {
		AccountingPositionTransferCommand command = new AccountingPositionTransferCommand();
		command.setTransactionDate(DateUtils.toDate("12/17/2013"));
		command.setSettlementDate(DateUtils.toDate("12/17/2013"));
		command.setSecuritySymbol(CCH14.getSymbol());
		command.setOriginalFacePercent(BigDecimal.ONE);
		command.setOriginalFaceRounding(new BigDecimal("1000"));
		command.setTransferPrice(BigDecimal.ONE);

		uploadResultingInValidationException(command, "Cannot process position transfer upload: Position transfer command/row has a specified OriginalFace or Original Face Percent for a Security with Symbol [CCH14], which does not allow Factor Change Events.");
	}


	private void uploadTransferWithUploadTemplateList(List<AccountingPositionTransferCommand> commandList) {
		AccountingPositionTransferUploadCustomCommand uploadCommand = new AccountingPositionTransferUploadCustomCommand();
		populateUploadFile(uploadCommand, commandList);
		this.accountingPositionTransferUploadService.uploadAccountingPositionTransferFile(uploadCommand);
	}


	private void populateUploadFile(AccountingPositionTransferUploadCustomCommand uploadCommand, List<AccountingPositionTransferCommand> uploadRowList) {
		BeanCollectionToDataTableConverterWithDynamicColumnCreation<AccountingPositionTransferCommand> dataTableConverter = new BeanCollectionToDataTableConverterWithDynamicColumnCreation<AccountingPositionTransferCommand>(new DataTableConverterConfiguration(), false);
		DataTable importData = dataTableConverter.convert(uploadRowList);
		DataTableToExcelFileConverter dataFileConverter = new DataTableToExcelFileConverter("xls");
		DataTableFileConfig fileConfig = new DataTableFileConfig(importData);
		try {
			uploadCommand.setFile(new MultipartFileImpl(dataFileConverter.convert(fileConfig)));
		}
		catch (Throwable e) {
			throw new RuntimeException("Error setting upload file: " + e.getMessage(), e);
		}
	}


	private AccountingPositionTransfer bookTransfer(AccountInfo fromAccount, AccountInfo toAccount, Date transactionDate, String transferTypeName) {
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.setTransferTypeName(transferTypeName);
		searchForm.setTransactionDate(transactionDate);
		searchForm.setSettlementDate(transactionDate);
		if (fromAccount != null) {
			searchForm.setFromClientInvestmentAccountId(fromAccount.getClientAccount().getId());
			searchForm.setFromHoldingInvestmentAccountId(fromAccount.getHoldingAccount().getId());
		}
		if (toAccount != null) {
			searchForm.setToClientInvestmentAccountId(toAccount.getClientAccount().getId());
			searchForm.setToHoldingInvestmentAccountId(toAccount.getHoldingAccount().getId());
		}
		searchForm.setJournalNotBooked(Boolean.TRUE);
		AccountingPositionTransfer transfer = CollectionUtils.getOnlyElement(this.accountingPositionTransferService.getAccountingPositionTransferList(searchForm));
		Assertions.assertNotNull(transfer);
		return this.accountingUtils.bookAccountingPositionTransfer(transfer);
	}


	private List<AccountingTransaction> openTpz5Trades(AccountInfo fromAccount, boolean buy) {
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.CITIGROUP);
		Date openDate = DateUtils.toDate("9/08/2015");
		BigDecimal positionSize = new BigDecimal("121");

		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(TPZ5, positionSize, buy, openDate, broker, fromAccount)
				.withExchangeRate(new BigDecimal("0.00831427"))
				.withAverageUnitPrice(new BigDecimal("1435.2")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		List<AccountingTransaction> result = new ArrayList<>();
		result.add((AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0));

		broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		openDate = DateUtils.toDate("9/30/2015");
		positionSize = new BigDecimal("36");

		buyTrade = this.tradeUtils.newFuturesTradeBuilder(TPZ5, positionSize, buy, openDate, broker, fromAccount)
				.withExchangeRate(new BigDecimal("0.00834097"))
				.withAverageUnitPrice(new BigDecimal("1409")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);
		result.add((AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0));

		return result;
	}


	private void uploadResultingInValidationException(AccountingPositionTransferCommand command, String message) {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);

		// Create a position for 'fromAccount'
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("12/16/2013");
		BigDecimal positionSize = new BigDecimal("20");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(CCH14, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		//Get the position to transfer
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(fromAccount, CCH14, openDate);
		Assertions.assertEquals(1, positionList.size());

		command.setFromClientInvestmentAccountNumber(fromAccount.getClientAccount().getNumber());
		command.setFromHoldingInvestmentAccountNumber(fromAccount.getHoldingAccount().getNumber());
		command.setToClientInvestmentAccountNumber(toAccount.getClientAccount().getNumber());
		command.setToHoldingInvestmentAccountNumber(toAccount.getHoldingAccount().getNumber());
		command.setTypeName(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME);
		command.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "\nto client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());

		String exceptionMessage = null;
		try {
			// Upload and Book transfer
			uploadTransferWithUploadTemplateList(Collections.singletonList(command));
		}
		catch (Throwable e) {
			if (e instanceof ImsErrorResponseException) {
				exceptionMessage = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			if (exceptionMessage == null) {
				exceptionMessage = e.getMessage();
			}
		}
		Assertions.assertEquals(message, exceptionMessage);
	}
}
