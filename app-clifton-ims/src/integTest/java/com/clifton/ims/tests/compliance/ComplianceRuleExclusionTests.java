package com.clifton.ims.tests.compliance;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.search.BusinessServiceSearchForm;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleSearchForm;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author mitchellf
 */
public class ComplianceRuleExclusionTests extends ComplianceRealTimeTradeTests {

	private static final String EXCLUDED_RULE_NAME = "Position Value Constraint - Trade Delta (15-40): Options - All (Sells Only)";
	private static final String CLIENT_ACCOUNT_NUMBER = "W-012801";
	private static final Date TRADE_DATE = DateUtils.toDate("10/25/2019");
	private static final String SECURITY_1_SYMBOL = "FB US 11/01/19 C202.5";
	private static final String SECURITY_2_SYMBOL = "FB US 11/01/19 C205";

	private static final String BUSINESS_SERVICE_NAME = "Dynamic Put Selling (unfunded)";
	private static final String DAYS_FROM_EXPIRATION_RULE_NAME = "Position Value Constraint - Days from Expiration (27-35): Options - All";
	private static final String EXCLUSION_CLIENT_ACCOUNT_NAME = "The Mark Ealey Irrevocable Trust 2012 DTD 12/26/2012 - DPS";
	private static final String EXCLUSION_CLIENT_ACCOUNT_NUMBER = "W-014620";
	private static final String HOLDING_ACCOUNT_NUMBER = "Y919075";
	private static final String SECURITY_3_SYMBOL = "AAPL US 07/17/20 C315";


	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private ComplianceRuleService complianceRuleService;

	@Resource
	private BusinessServiceService businessServiceService;

	@Resource
	private SystemRequestStatsService systemRequestStatsService;


	@Test
	public void testCreateTradeWithExclusion() {
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber(CLIENT_ACCOUNT_NUMBER);
		InvestmentAccount holdingAccount = this.investmentAccountUtils.getHoldingAccountByNumber("5803-1373");
		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setClientAccount(account);
		accountInfo.setHoldingAccount(holdingAccount);

		CompanySearchForm companySearchForm = new CompanySearchForm();
		companySearchForm.setName("Charles Schwab & Co Inc");
		BusinessCompany executingBroker = CollectionUtils.getOnlyElement(this.businessCompanyService.getBusinessCompanyList(companySearchForm));
		BigDecimal quantity = new BigDecimal("12");

		List<RuleViolation> ruleViolations = this.complianceUtils.executeRulesAgainstTrade(accountInfo, "Options", SECURITY_1_SYMBOL, quantity, false, TRADE_DATE, executingBroker);

		validateViolationNoteExists(ruleViolations, EXCLUDED_RULE_NAME, true);

		ComplianceRuleAssignment exclusionAssignment = this.complianceUtils.excludeRuleFromAccountByName(account, EXCLUDED_RULE_NAME);

		try {
			// re-run trade, hopefully now without the same violation
			List<RuleViolation> violationsWithExclusion = this.complianceUtils.executeRulesAgainstTrade(accountInfo, "Options", SECURITY_1_SYMBOL, quantity, false, TRADE_DATE, executingBroker);
			validateViolationNoteDoesNotExist(violationsWithExclusion, EXCLUDED_RULE_NAME, true);
		}
		finally {
			// cleanup
			if (exclusionAssignment.getId() != null) {
				this.complianceUtils.removeComplianceRuleAssignment(exclusionAssignment);
			}
		}
	}


	@Test
	public void testCreateTradeWithBusinessServiceExclusion() {
		// this test will function essentially the same as the test for individual exclusions, but will verify that a business service exclusion is applied.
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber(CLIENT_ACCOUNT_NUMBER);
		InvestmentAccount holdingAccount = this.investmentAccountUtils.getHoldingAccountByNumber("5803-1373");
		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setClientAccount(account);
		accountInfo.setHoldingAccount(holdingAccount);

		CompanySearchForm companySearchForm = new CompanySearchForm();
		companySearchForm.setName("Charles Schwab & Co Inc");
		BusinessCompany executingBroker = CollectionUtils.getOnlyElement(this.businessCompanyService.getBusinessCompanyList(companySearchForm));
		BigDecimal quantity = new BigDecimal("12");

		List<RuleViolation> ruleViolations = this.complianceUtils.executeRulesAgainstTrade(accountInfo, "Options", SECURITY_2_SYMBOL, quantity, false, TRADE_DATE, executingBroker);
		validateViolationNoteExists(ruleViolations, EXCLUDED_RULE_NAME, true);

		// Remove existing Business Service assignment so it doesn't conflict, But save start date to use later
		Date startDate = this.complianceUtils.getComplianceRuleAssignmentByRuleNameAndBusinessService(EXCLUDED_RULE_NAME, account.getBusinessService().getId()).getStartDate();
		this.complianceUtils.removeComplianceRuleAssignment(account.getBusinessService(), EXCLUDED_RULE_NAME);
		ObjectWrapper<ComplianceRuleAssignment> businessServiceExclusion = new ObjectWrapper<>();
		try {
			// Create a global assignment with the given rule
			this.complianceUtils.executeWithGlobalRuleAssignment(EXCLUDED_RULE_NAME, startDate, () -> {
				// Generate a trade to verify that the global assignment has taken effect
				List<RuleViolation> violationsWithGlobalRule = this.complianceUtils.executeRulesAgainstTrade(accountInfo, "Options", SECURITY_2_SYMBOL, quantity, false, TRADE_DATE, executingBroker);
				validateViolationNoteExists(violationsWithGlobalRule, EXCLUDED_RULE_NAME, true);

				// Now create an exclusion assignment for the applicable Business Service
				businessServiceExclusion.setObject(this.complianceUtils.excludeRuleFromBusinessServiceByNameOnDate(account.getBusinessService(), EXCLUDED_RULE_NAME, TRADE_DATE));

				// Generate another trade and verify that the exclusion was applied
				List<RuleViolation> violationsWithExclusion = this.complianceUtils.executeRulesAgainstTrade(accountInfo, "Options", SECURITY_2_SYMBOL, quantity, false, TRADE_DATE, executingBroker);
				validateViolationNoteDoesNotExist(violationsWithExclusion, EXCLUDED_RULE_NAME, true);
				return true;
			});
		}
		finally {
			// cleanup
			this.complianceUtils.removeComplianceRuleAssignment(businessServiceExclusion.getObject());
			// recreate deleted assignment for other tests
			this.complianceUtils.assignRuleToBusinessServiceByNameOnDate(account.getBusinessService(), EXCLUDED_RULE_NAME, startDate);
		}
	}


	// This test exists primarily to verify cache functionality for security-specific assignments
	@Test
	public void testRemoveEndDateForBusinessServiceAssignmentWithClientAccountExclusion() {
		// Verify business service assignment for Position Value Constraint - Days from Expiration (27-35): Options - All on Dynamic Put Selling with end date 7/2/20
		ComplianceRuleSearchForm complianceRuleSearchForm = new ComplianceRuleSearchForm();
		complianceRuleSearchForm.setName(DAYS_FROM_EXPIRATION_RULE_NAME);
		ComplianceRule rule = CollectionUtils.getOnlyElement(this.complianceRuleService.getComplianceRuleList(complianceRuleSearchForm));
		Date tradeDate = DateUtils.toDate("07/16/2020");
		BusinessServiceSearchForm businessServiceSearchForm = new BusinessServiceSearchForm();
		businessServiceSearchForm.setName(BUSINESS_SERVICE_NAME);
		BusinessService businessService = CollectionUtils.getOnlyElement(this.businessServiceService.getBusinessServiceList(businessServiceSearchForm));

		ComplianceRuleAssignment businessServiceAssignment = this.complianceUtils.getComplianceRuleAssignmentByRuleNameAndBusinessService(DAYS_FROM_EXPIRATION_RULE_NAME, businessService.getId());
		if (businessServiceAssignment == null) {
			// create assignment - it exists currently, but in the future it may be removed
			businessServiceAssignment = new ComplianceRuleAssignment();
			businessServiceAssignment.setRule(rule);
			businessServiceAssignment.setBusinessService(businessService);
			businessServiceAssignment.setStartDate(DateUtils.toDate("06/05/2019"));
		}
		businessServiceAssignment.setExclusionAssignment(false);
		businessServiceAssignment.setEndDate(DateUtils.toDate("07/02/2020"));
		this.complianceRuleService.saveComplianceRuleAssignment(businessServiceAssignment);
		try {
			// Verify exclusion assignment for same rule and client account w-014620
			ComplianceRuleAssignment exclusionAssignment = this.complianceUtils.getComplianceRuleAssignmentByRuleNameAndAccountName(DAYS_FROM_EXPIRATION_RULE_NAME, EXCLUSION_CLIENT_ACCOUNT_NAME);
			InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(EXCLUSION_CLIENT_ACCOUNT_NUMBER);

			if (exclusionAssignment == null) {
				// create assignment - again, it exists currently, but in the future it may be removed
				exclusionAssignment = new ComplianceRuleAssignment();
				exclusionAssignment.setClientInvestmentAccount(clientAccount);
				exclusionAssignment.setRule(rule);
				exclusionAssignment.setStartDate(tradeDate);
			}
			exclusionAssignment.setExclusionAssignment(true);
			this.complianceRuleService.saveComplianceRuleAssignment(exclusionAssignment);

			//  Create trade on 7/16 for AAPL 7/17/20 C315
			AccountInfo accountInfo = new AccountInfo();
			accountInfo.setClientAccount(clientAccount);
			accountInfo.setHoldingAccount(this.investmentAccountUtils.getHoldingAccountByNumber(HOLDING_ACCOUNT_NUMBER));

			// To test cache functionality, need to clear here
			this.systemRequestStatsService.deleteSystemCacheAll();
			List<RuleViolation> violations = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, false, SECURITY_3_SYMBOL, BigDecimal.ONE, true, tradeDate, this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "UBS Financial Services, Inc."));

			// Compliance run preview, verify that rule violation does not exist
			validateViolationNoteDoesNotExist(violations, DAYS_FROM_EXPIRATION_RULE_NAME, true);

			// Remove end date on business service assignment
			businessServiceAssignment.setEndDate(null);
			this.complianceRuleService.saveComplianceRuleAssignment(businessServiceAssignment);
			violations = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, false, SECURITY_3_SYMBOL, BigDecimal.ONE, true, tradeDate, this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "UBS Financial Services, Inc."));
			validateViolationNoteDoesNotExist(violations, DAYS_FROM_EXPIRATION_RULE_NAME, true);

			// // Now, just to make sure the rule is functioning properly, we'll delete the exclusion and hopefully a violation will show up
			this.complianceRuleService.deleteComplianceRuleAssignment(exclusionAssignment.getId());
			violations = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.OPTIONS, false, SECURITY_3_SYMBOL, BigDecimal.ONE, true, tradeDate, this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "UBS Financial Services, Inc."));
			validateViolationNoteExists(violations, DAYS_FROM_EXPIRATION_RULE_NAME, true);
		}
		finally {
			businessServiceAssignment.setExclusionAssignment(true);
			this.complianceRuleService.saveComplianceRuleAssignment(businessServiceAssignment);
		}
	}
}
