package com.clifton.ims.tests.integration.export.email;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.core.email.FakeSMTPServer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.FunctionUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.ExportDefinitionType;
import com.clifton.export.run.runner.ExportRunnerService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryService;
import com.clifton.test.util.RandomUtils;
import net.kemitix.wiser.assertions.WiserAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.subethamail.wiser.WiserMessage;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;


/**
 * Tests for email export functionality
 */
public class EmailExportTests extends BaseImsIntegrationTest {

	private static final Integer POLL_SLEEP_TIME = 1000;
	private static final Integer POLL_LIMIT_COUNT = 60;

	private static final Integer SMTP_PORT = 2500;

	private static final String EXPORT_DEFINITION_TYPE_NAME = "Internal";

	private static final String EMAIL_A = "mwacker@paraport.com";
	private static final String EMAIL_B = "tzwieg@paraport.com";
	private static final String EMAIL_C = "manderson@paraport.com";

	private static final String SUBJECT_PREFIX = "Test Subject - ";
	private static final String TEXT_PREFIX = "Some text here - ";

	private static final String FILENAME_TEMPLATE = "CLIFTON_EXPORT_01.csv";
	private static final String QUERY_NAME = "REDI Alias";
	private static final String QUERY_EXPORT_FORMAT = "CSV";

	private static final String HOST_NAME = FunctionUtils.uncheckedSupplier(() -> InetAddress.getLocalHost().getHostName()).get();

	private final Lazy<FakeSMTPServer> fakeSmtpServerLazy = new Lazy<>(() -> new FakeSMTPServer(SMTP_PORT));
	private final Lazy<BusinessContact> contactALazy = new Lazy<>(() -> getBusinessContactByEmail(EMAIL_A));
	private final Lazy<BusinessContact> contactBLazy = new Lazy<>(() -> getBusinessContactByEmail(EMAIL_B));
	private final Lazy<BusinessContact> contactCLazy = new Lazy<>(() -> getBusinessContactByEmail(EMAIL_C));
	private final Lazy<SystemQuery> systemQueryLazy = new Lazy<>(() -> this.systemQueryService.getSystemQueryByName(QUERY_NAME));
	private final Lazy<ExportDefinitionType> exportDefinitionTypeLazy = new Lazy<>(() -> this.exportDefinitionService.getExportDefinitionTypeByName(EXPORT_DEFINITION_TYPE_NAME));

	@Resource
	private ExportDefinitionService exportDefinitionService;
	@Resource
	private ExportRunnerService exportRunnerService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private BusinessContactService businessContactService;
	@Resource
	private SystemQueryService systemQueryService;
	@Resource
	private FakeSMTPServerUtils fakeSMTPServerUtils;


	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////                                    TESTS                                           //////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Sending from contact A to contact B
	 */
	@Test
	public void testSendReceiveEmail() {
		List<ExportDefinitionDestination> destinationList = new ArrayList<>();
		List<ExportDefinitionContent> contentList = new ArrayList<>();

		String randomString = Integer.toString(RandomUtils.randomNumber());
		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(getExportDefinitionType(), randomString);
		try {
			//Set the Destination
			EmailExportDestinationBuilder builder = new EmailExportDestinationBuilder(this.systemBeanService, this.exportDefinitionService)
					.exportDefinition(exportDefinition)
					.from(getContactA())
					.addToContact(getContactB())
					.emailSubject(SUBJECT_PREFIX)
					.emailText(TEXT_PREFIX)
					.randomString(randomString)
					.hostname(HOST_NAME)
					.port(SMTP_PORT);
			destinationList.add(builder.build());

			//Set the Contents
			contentList.add(createExportDefinitionContent(exportDefinition));
			//Send the export
			send(exportDefinition.getId());

			assertSmtpServerHasMessages();
			WiserAssertions.assertReceivedMessage(getFakeSmtpServer())
					.from(getContactA().getEmailAddress())
					.to(getContactB().getEmailAddress())
					.withSubjectContains(SUBJECT_PREFIX)
					.withContentContains(TEXT_PREFIX);
		}
		finally {
			cleanupEntities(exportDefinition, destinationList, contentList);
		}
	}


	/**
	 * Send an email to Contact A without a From Contact
	 */
	@Test
	public void testSendEmailWithoutFromContact() {
		List<ExportDefinitionDestination> destinationList = new ArrayList<>();
		List<ExportDefinitionContent> contentList = new ArrayList<>();

		String randomString = Integer.toString(RandomUtils.randomNumber());

		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(getExportDefinitionType(), randomString);
		try {
			//Setup the destination
			EmailExportDestinationBuilder builder = new EmailExportDestinationBuilder(this.systemBeanService, this.exportDefinitionService)
					.exportDefinition(exportDefinition)
					.addToContact(getContactA())
					.emailSubject(SUBJECT_PREFIX)
					.emailText(TEXT_PREFIX)
					.randomString(randomString)
					.hostname(HOST_NAME)
					.port(SMTP_PORT);
			destinationList.add(builder.build());

			//Set the Contents
			contentList.add(createExportDefinitionContent(exportDefinition));

			send(exportDefinition.getId());

			assertSmtpServerHasMessages();
			WiserAssertions.assertReceivedMessage(getFakeSmtpServer())
					.from("noreply@paraport.com")
					.to(getContactA().getEmailAddress())
					.withSubjectContains(SUBJECT_PREFIX)
					.withContentContains(TEXT_PREFIX);
		}
		finally {
			cleanupEntities(exportDefinition, destinationList, contentList);
		}
	}


	/**
	 * Send email from Contact A to Contact B cc'ing Contact C
	 */
	@Test
	public void testSendEmailCC() {
		List<ExportDefinitionDestination> destinationList = new ArrayList<>();
		List<ExportDefinitionContent> contentList = new ArrayList<>();

		String randomString = Integer.toString(RandomUtils.randomNumber());

		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(getExportDefinitionType(), randomString);

		try {
			//Setup the destination
			EmailExportDestinationBuilder builder = new EmailExportDestinationBuilder(this.systemBeanService, this.exportDefinitionService)
					.exportDefinition(exportDefinition)
					.from(getContactA())
					.addToContact(getContactB())
					.addCcContact(getContactC())
					.emailSubject(SUBJECT_PREFIX)
					.emailText(TEXT_PREFIX)
					.randomString(randomString)
					.hostname(HOST_NAME)
					.port(SMTP_PORT);
			destinationList.add(builder.build());

			//Set the Contents
			contentList.add(createExportDefinitionContent(exportDefinition));

			send(exportDefinition.getId());

			assertSmtpServerHasMessages();
			List<WiserMessage> messages = getFakeSmtpServer().getMessages();
			for (WiserMessage message : messages) {
				String data = new String(message.getData());
				Assertions.assertTrue(data.contains("Cc: " + getContactC().getEmailAddress()));
			}

			WiserAssertions.assertReceivedMessage(getFakeSmtpServer())
					.from(getContactA().getEmailAddress())
					.to(getContactB().getEmailAddress())
					.to(getContactC().getEmailAddress())
					.withSubjectContains(SUBJECT_PREFIX)
					.withContentContains(TEXT_PREFIX);
		}
		finally {
			cleanupEntities(exportDefinition, destinationList, contentList);
		}
	}


	/**
	 * Send from contact A to contact B, BCC contact C
	 */
	@Test
	public void testSendEmailBCC() {
		List<ExportDefinitionDestination> destinationList = new ArrayList<>();
		List<ExportDefinitionContent> contentList = new ArrayList<>();

		String randomString = Integer.toString(RandomUtils.randomNumber());

		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(getExportDefinitionType(), randomString);

		try {
			//Setup the destination
			EmailExportDestinationBuilder builder = new EmailExportDestinationBuilder(this.systemBeanService, this.exportDefinitionService)
					.exportDefinition(exportDefinition)
					.from(getContactA())
					.addToContact(getContactB())
					.addBccContact(getContactC())
					.emailSubject(SUBJECT_PREFIX)
					.emailText(TEXT_PREFIX)
					.randomString(randomString)
					.hostname(HOST_NAME)
					.port(SMTP_PORT);
			destinationList.add(builder.build());

			//Set the Contents
			contentList.add(createExportDefinitionContent(exportDefinition));

			send(exportDefinition.getId());

			assertSmtpServerHasMessages();
			WiserAssertions.assertReceivedMessage(getFakeSmtpServer())
					.from(getContactA().getEmailAddress())
					.to(getContactB().getEmailAddress())
					.to(getContactC().getEmailAddress())
					.withSubjectContains(SUBJECT_PREFIX)
					.withContentContains(TEXT_PREFIX);
		}
		finally {
			cleanupEntities(exportDefinition, destinationList, contentList);
		}
	}


	/**
	 * Test sending email from contact B on behalf of contact A
	 */
	@Test
	public void testSendEmailOnBehalfOf() {
		List<ExportDefinitionDestination> destinationList = new ArrayList<>();
		List<ExportDefinitionContent> contentList = new ArrayList<>();

		String randomString = Integer.toString(RandomUtils.randomNumber());

		//Create the Export Definition
		ExportDefinition exportDefinition = createExportDefinition(getExportDefinitionType(), randomString);

		try {
			//Setup the destination
			EmailExportDestinationBuilder builder = new EmailExportDestinationBuilder(this.systemBeanService, this.exportDefinitionService)
					.exportDefinition(exportDefinition)
					.from(getContactA())
					.sender(getContactB())
					.addToContact(getContactC())
					.emailSubject(SUBJECT_PREFIX)
					.emailText(TEXT_PREFIX)
					.randomString(randomString)
					.hostname(HOST_NAME)
					.port(SMTP_PORT);
			destinationList.add(builder.build());

			//Set the Contents
			contentList.add(createExportDefinitionContent(exportDefinition));

			send(exportDefinition.getId());

			assertSmtpServerHasMessages();
			List<WiserMessage> messages = getFakeSmtpServer().getMessages();
			for (WiserMessage message : messages) {
				String data = new String(message.getData());
				Assertions.assertTrue(data.contains("Sender: \"" + getContactB().getNameLabel()));
			}

			WiserAssertions.assertReceivedMessage(getFakeSmtpServer())
					.from(getContactA().getEmailAddress())
					.to(getContactC().getEmailAddress())
					.withSubjectContains(SUBJECT_PREFIX)
					.withContentContains(TEXT_PREFIX);
		}
		finally {
			cleanupEntities(exportDefinition, destinationList, contentList);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////                                    HELPERS                                   ///////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() throws IOException {
		getFakeSmtpServer().start();
	}


	@AfterEach
	public void cleanup() {
		getFakeSmtpServer().stop();
	}


	private void send(Integer exportDefinitionId) {
		//Run the export
		this.exportRunnerService.runManualExport(exportDefinitionId);
	}


	private ExportDefinition createExportDefinition(ExportDefinitionType exportDefType, String random) {
		//Create the Export Definition
		ExportDefinition exportDefinition = new ExportDefinition();
		exportDefinition.setName("Email Export Integration Test - " + random);
		exportDefinition.setType(exportDefType);
		this.exportDefinitionService.saveExportDefinition(exportDefinition);
		return exportDefinition;
	}


	private ExportDefinitionContent createExportDefinitionContent(ExportDefinition exportDefinition) {
		SystemBean contentSystemBean = SystemBeanBuilder.newBuilder(null, "Query Export Generator", this.systemBeanService)
				.addProperty("System Query", getSystemQuery().getId().toString())
				.addProperty("Export Format", QUERY_EXPORT_FORMAT)
				.build();

		ExportDefinitionContent content = new ExportDefinitionContent();
		content.setContentSystemBean(contentSystemBean);
		content.setFileNameTemplate(FILENAME_TEMPLATE);
		content.setDefinition(exportDefinition);
		this.exportDefinitionService.saveExportDefinitionContent(content);

		return content;
	}


	private BusinessContact getBusinessContactByEmail(String email) {
		BusinessContactSearchForm businessContactSearchForm = new BusinessContactSearchForm();
		businessContactSearchForm.setEmailAddress(email);
		return CollectionUtils.getOnlyElement(this.businessContactService.getBusinessContactList(businessContactSearchForm));
	}


	private void cleanupEntities(ExportDefinition definition, List<ExportDefinitionDestination> exportDefinitionDestinations, List<ExportDefinitionContent> exportDefinitionContents) {
		//Clear the messages from the server
		getFakeSmtpServer().clearMessages();
		//Delete the contents
//		for (ExportDefinitionContent content : exportDefinitionContents) {
//			this.exportDefinitionService.deleteExportDefinitionContent(content.getId());
//		}
//		//Delete the destinations
//		for (ExportDefinitionDestination destination : exportDefinitionDestinations) {
//			this.exportDefinitionService.deleteExportDefinitionDestinationAndLink(destination.getId(), definition.getId());
//		}
//		//Delete the run history
//		this.exportDefinitionService.deleteExportRunHistoryForDefinition(definition.getId());
//		//Delete the definition
//		this.exportDefinitionService.deleteExportDefinition(definition.getId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertSmtpServerHasMessages() {
		try {
			Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(getFakeSmtpServer(), POLL_LIMIT_COUNT, POLL_SLEEP_TIME), "No messages were found in the SMTP server.");
		}
		catch (Exception e) {
			Assertions.fail("An error occurred while attempting to assert that the SMTP server contained messages.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FakeSMTPServer getFakeSmtpServer() {
		return this.fakeSmtpServerLazy.get();
	}


	public BusinessContact getContactA() {
		return this.contactALazy.get();
	}


	public BusinessContact getContactB() {
		return this.contactBLazy.get();
	}


	public BusinessContact getContactC() {
		return this.contactCLazy.get();
	}


	public SystemQuery getSystemQuery() {
		return this.systemQueryLazy.get();
	}


	public ExportDefinitionType getExportDefinitionType() {
		return this.exportDefinitionTypeLazy.get();
	}
}
