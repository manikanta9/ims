package com.clifton.ims.tests.product.manager;


import com.clifton.business.client.BusinessClient;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.AggregationOperations;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.InvestmentEventManagerAdjustment;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccount.CashAdjustmentType;
import com.clifton.investment.manager.InvestmentManagerAccount.ManagerTypes;
import com.clifton.investment.manager.InvestmentManagerAccountRollup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.product.manager.ProductManagerBalanceProcessingTypes;
import com.clifton.product.manager.ProductManagerService;
import com.clifton.product.manager.job.AccountBalanceCommand;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * The <code>ProductManagerServiceTests</code> tests actions performed for reloading/processing/adjusting manager account balances.
 * <p>
 *
 * @author manderson
 */

public class ProductManagerServiceTests extends BaseImsIntegrationTest {

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private InvestmentCalendarService investmentCalendarService;

	@Resource
	private InvestmentManagerAccountService investmentManagerAccountService;

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private ProductManagerService productManagerService;


	/**
	 * If a balance is reloaded and there is a change to the original cash/securities the existing balance should be deleted first, and the new balance inserted.
	 * <p>
	 * NOTE: Issue with Hibernate Deletes, MUST flush the session after deleting and before inserting new balance record.  Hibernate will perform inserts before deletes and thus cause a UX violation
	 */
	@Test
	public void testReloadManagerBalance() {
		BusinessClient businessClient = this.businessUtils.createBusinessClient();

		InvestmentManagerAccount manager = setupManagerAccount(businessClient, null, ManagerTypes.PROXY, DateUtils.toDate("07/07/2014"), 10000.00, true);

		// Load New Balance Record
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setClientId(manager.getClient().getId());

		// Initial Load
		Date balanceDate = DateUtils.toDate("07/11/2014");
		AccountBalanceCommand command = AccountBalanceCommand.ofSynchronous(searchForm, balanceDate, ProductManagerBalanceProcessingTypes.LOAD_PROCESS);
		this.productManagerService.processProductManagerAccountBalanceList(command);

		// Get the Balance And Validate
		InvestmentManagerAccountBalance balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(manager.getId(), balanceDate, true);
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, balance.getCashValue()), "Expected Original Cash to Be 0.");
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(10000.00), balance.getSecuritiesValue()), "Expected Original Securities to Be 10,000.");

		// Change Proxy Value and Reload Balance
		manager.setProxyValue(BigDecimal.valueOf(11000.00));
		this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);

		// Re-Load it
		this.productManagerService.processProductManagerAccountBalanceListForBalances(new Integer[]{balance.getId()});

		// Validate Initial Balance
		balance = validateManagerAccountBalance(manager, balanceDate, 0.0, 11000.00, 0.0, 11000.00, 0.0, 11000.0);

		// Add a Manual Adjustment
		InvestmentManagerAccountBalanceAdjustment adj = new InvestmentManagerAccountBalanceAdjustment();
		adj.setCashValue(BigDecimal.valueOf(2500));
		adj.setSecuritiesValue(BigDecimal.ZERO);
		adj.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("Daily Manual Bank Override"));
		adj.setNote("Test");
		balance.setAdjustmentList(CollectionUtils.createList(adj));
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(balance);

		// Validate Balance After Adjustment
		validateManagerAccountBalance(manager, balanceDate, 0.0, 11000.00, 2500.0, 11000.00, 2500.0, 11000.00);

		// Update Proxy Value
		manager.setProxyValue(BigDecimal.valueOf(9000.00));
		this.investmentManagerAccountService.saveInvestmentManagerAccount(manager);

		// Reload Balance
		this.productManagerService.processProductManagerAccountBalanceListForBalances(new Integer[]{balance.getId()});

		// Validate Balance After Proxy Change (Should Keep Manual Adjustment)
		validateManagerAccountBalance(manager, balanceDate, 0.0, 9000.00, 2500.0, 9000.00, 2500.0, 9000.00);
	}


	/**
	 * This test creates 4 managers and validates adjustments and adjusted values are processed correctly when an event is added to the calendar:
	 * <p>
	 * 1 Proxy Manager (Event Adjustment)
	 * 2. Regular Manager used as a child of a rollup (Event Adjustment)
	 * 3. Proxy Manager used as a child of a rollup (NO Event Adjustment)
	 * 4. Rollup Manager of 2 & 3
	 * Note: Processing manager accounts appears to accurately include event adjustments, but this also tests adding events to the calendar for today which should automatically update manager balances for previous business day
	 */
	@Test
	public void testManagerAccountBalanceWithInvestmentCalendarAdjustment() {
		BusinessClient businessClient = this.businessUtils.createBusinessClient();
		InvestmentAccount account = this.investmentAccountUtils.createClientAccount(businessClient);

		Date previousBusinessDay = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);

		// Setup Manager Accounts
		InvestmentManagerAccount manager1 = setupManagerAccount(businessClient, "Manager 1", ManagerTypes.PROXY, previousBusinessDay, 1000000.00, true);
		InvestmentManagerAccount manager2 = setupManagerAccount(businessClient, "Manager 2", ManagerTypes.STANDARD, null, null, true);
		InvestmentManagerAccount manager3 = setupManagerAccount(businessClient, "Manager 3", ManagerTypes.PROXY, previousBusinessDay, 500000.00, true);
		InvestmentManagerAccount manager4 = setupManagerAccount(businessClient, "Manager 4", ManagerTypes.ROLLUP, null, null, false);

		addRollupManagerLink(manager4, manager2, -100.00);
		addRollupManagerLink(manager4, manager3, -100.00);
		this.investmentManagerAccountService.saveInvestmentManagerAccount(manager4);

		// Manager 2 is a Standard Manager, so Create Balance Manually and Process
		InvestmentManagerAccountBalance balance2 = new InvestmentManagerAccountBalance();
		balance2.setManagerAccount(manager2);
		balance2.setBalanceDate(previousBusinessDay);
		balance2.setCashValue(BigDecimal.valueOf(2000000.00));
		balance2.setSecuritiesValue(BigDecimal.valueOf(20000.00));
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(balance2);
		this.productManagerService.processProductManagerAccountBalanceAdjustmentListForBalances(new Integer[]{balance2.getId()});

		// Load Process the rest of the Manager Balances
		InvestmentManagerAccountSearchForm searchForm = new InvestmentManagerAccountSearchForm();
		searchForm.setClientId(businessClient.getId());
		AccountBalanceCommand command = AccountBalanceCommand.ofSynchronous(searchForm, previousBusinessDay, ProductManagerBalanceProcessingTypes.LOAD_PROCESS);
		this.productManagerService.processProductManagerAccountBalanceList(command);

		// Add Calendar Event for Today for Managers 1 & 2
		InvestmentEvent event = new InvestmentEvent();
		event.setEventDate(new Date());
		event.setEventType(this.investmentCalendarService.getInvestmentEventTypeByName("Cash Flow"));
		event.setInvestmentAccount(account);
		event.setSummary("Test Event");
		event.setManagerAdjustmentList(new ArrayList<>());

		InvestmentEventManagerAdjustment eventManagerAdjustment1 = new InvestmentEventManagerAdjustment();
		eventManagerAdjustment1.setManagerAccount(manager1);
		eventManagerAdjustment1.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("Contribution/Distribution"));
		eventManagerAdjustment1.setCashValue(BigDecimal.valueOf(20000000));
		eventManagerAdjustment1.setSecuritiesValue(BigDecimal.valueOf(200000));
		event.getManagerAdjustmentList().add(eventManagerAdjustment1);

		InvestmentEventManagerAdjustment eventManagerAdjustment2 = new InvestmentEventManagerAdjustment();
		eventManagerAdjustment2.setManagerAccount(manager2);
		eventManagerAdjustment2.setAdjustmentType(this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceAdjustmentTypeByName("Contribution/Distribution"));
		eventManagerAdjustment2.setCashValue(BigDecimal.valueOf(10000000));
		event.getManagerAdjustmentList().add(eventManagerAdjustment2);

		// Saving the event should automatically update the manager balances
		this.investmentCalendarService.saveInvestmentEvent(event, true);

		// Add sleep for a few seconds to allow the adjustments to be processed for the rollup manager. The manager is built via a runner; see ProductManagerAccountBalanceObserver
		try {
			TimeUnit.SECONDS.sleep(3);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}

		// Validate Balances and Adjustments
		validateManagerAccountBalance(manager1, previousBusinessDay, 0.0, 1000000.00, 20000000.0, 1200000.00, 20000000.0, 1200000.00);
		validateManagerAccountBalance(manager2, previousBusinessDay, 2000000.00, 20000.00, 12000000.00, 20000.00, 12000000.00, 20000.00);
		validateManagerAccountBalance(manager3, previousBusinessDay, 0.0, 500000.00, 0.0, 500000.00, 0.0, 500000.00);
		validateManagerAccountBalance(manager4, previousBusinessDay, -12000000.00, -520000.00, -12000000.00, -520000.00, -12000000.00, -520000.00);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentManagerAccount setupManagerAccount(BusinessClient businessClient, String managerName, ManagerTypes managerType, Date proxyDate, Double proxyValue, boolean saveManager) {
		InvestmentManagerAccount managerAccount = new InvestmentManagerAccount();
		managerAccount.setClient(businessClient);
		managerAccount.setManagerCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.CLIFTON));
		managerAccount.setAccountName(managerName);
		managerAccount.setCashAdjustmentType(CashAdjustmentType.NONE);
		managerAccount.setManagerType(managerType);
		managerAccount.setBaseCurrency(this.usd);
		if (managerType == ManagerTypes.PROXY) {
			managerAccount.setProxyValue(BigDecimal.valueOf(proxyValue));
			managerAccount.setProxyValueDate(proxyDate);
		}

		if (managerType == ManagerTypes.ROLLUP) {
			managerAccount.setRollupAggregationType(AggregationOperations.SUM);
		}

		if (saveManager) {
			this.investmentManagerAccountService.saveInvestmentManagerAccount(managerAccount);
		}
		return managerAccount;
	}


	private void addRollupManagerLink(InvestmentManagerAccount parentManager, InvestmentManagerAccount childManager, double allocationPercent) {
		InvestmentManagerAccountRollup rollup = new InvestmentManagerAccountRollup();
		rollup.setReferenceTwo(childManager);
		rollup.setAllocationPercent(BigDecimal.valueOf(allocationPercent));

		if (parentManager.getRollupManagerList() == null) {
			parentManager.setRollupManagerList(new ArrayList<>());
		}
		parentManager.getRollupManagerList().add(rollup);
	}


	/**
	 * Validates the balance exists with expected cash/securities values
	 *
	 * @return the populated balance record, so if there are adjustments they can be validated as well, but this will validate if balance is adjusted and at least one adjustment exists
	 */
	private InvestmentManagerAccountBalance validateManagerAccountBalance(InvestmentManagerAccount managerAccount, Date balanceDate, double originalCash, double originalSecurities, double adjustedCash, double adjustedSecurities, double mocCash, double mocSecurities) {
		InvestmentManagerAccountBalance balance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(managerAccount.getId(), balanceDate, true);
		String msgPrefix = "Balance for Manager [" + managerAccount.getLabel() + "] on [" + DateUtils.fromDateShort(balanceDate) + "]: ";
		AssertUtils.assertNotNull(balance, msgPrefix + "Missing");
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(originalCash), balance.getCashValue()), msgPrefix + "Expected Original Cash to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(originalCash)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getCashValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(originalSecurities), balance.getSecuritiesValue()), msgPrefix + "Expected Original Securities to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(originalSecurities)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getSecuritiesValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(adjustedCash), balance.getAdjustedCashValue()), msgPrefix + "Expected Adjusted Cash to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(adjustedCash)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getAdjustedCashValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(adjustedSecurities), balance.getAdjustedSecuritiesValue()), msgPrefix + "Expected Adjusted Securities to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(adjustedSecurities)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getAdjustedSecuritiesValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(mocCash), balance.getMarketOnCloseCashValue()), msgPrefix + "Expected MOC Cash to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(mocCash)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getMarketOnCloseCashValue()));
		ValidationUtils.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(mocSecurities), balance.getMarketOnCloseSecuritiesValue()), msgPrefix + "Expected MOC Securities to Be " + CoreMathUtils.formatNumberMoney(BigDecimal.valueOf(mocSecurities)) + " but was " + CoreMathUtils.formatNumberMoney(balance.getMarketOnCloseSecuritiesValue()));
		if (balance.isAdjusted()) {
			ValidationUtils.assertNotEmpty(balance.getAdjustmentList(), msgPrefix + " is adjusted but doesn't have any adjustments.");
		}
		return balance;
	}
}
