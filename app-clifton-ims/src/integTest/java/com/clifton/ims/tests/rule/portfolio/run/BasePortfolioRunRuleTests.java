package com.clifton.ims.tests.rule.portfolio.run;


import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContractTypes;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.builders.investment.assetclass.InvestmentAssetClassBuilder;
import com.clifton.ims.tests.builders.investment.assetclass.account.assetclass.InvestmentAccountAssetClassBuilder;
import com.clifton.ims.tests.builders.investment.manager.InvestmentManagerAccountBuilder;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountCreationCommand;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountRelationshipPurposes;
import com.clifton.ims.tests.investment.InvestmentInstrumentUtils;
import com.clifton.ims.tests.portfolio.run.PortfolioRunUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.portfolio.run.process.PortfolioRunProcessService;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.reconcile.position.ReconcilePositionService;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.junit.jupiter.api.BeforeEach;

import javax.annotation.Resource;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public abstract class BasePortfolioRunRuleTests extends RuleTests {

	public AccountInfo accountInfo;
	public Date lastBusinessDay;
	public InvestmentSecurity investmentSecurity;
	public BusinessCompany executingBroker;
	public Date tradeDate;
	public BigDecimal tradeQuantity;

	//These static variables are used across tests.
	public static PortfolioRun portfolioRun;

	@Resource
	protected AccountingM2MService accountingM2MService;
	@Resource
	protected AccountingPeriodService accountingPeriodService;
	@Resource
	protected AccountingJournalService accountingJournalService;
	@Resource
	protected AccountingPostingService accountingPostingService;
	@Resource
	protected InvestmentAccountAssetClassService investmentAccountAssetClassService;
	@Resource
	protected InvestmentAccountService investmentAccountService;
	@Resource
	protected InvestmentCalculatorService investmentCalculatorService;
	@Resource
	protected InvestmentCalendarService investmentCalendarService;
	@Resource
	protected InvestmentInstrumentUtils investmentInstrumentUtils;
	@Resource
	protected InvestmentManagerAccountService investmentManagerAccountService;
	@Resource
	protected InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	@Resource
	protected InvestmentSetupService investmentSetupService;
	@Resource
	protected PortfolioRunProcessService portfolioRunProcessService;
	@Resource
	protected PortfolioRunMarginService portfolioRunMarginService;
	@Resource
	protected PortfolioRunService portfolioRunService;
	@Resource
	protected PortfolioRunUtils portfolioRunUtils;
	@Resource
	protected ProductOverlayService productOverlayService;
	@Resource
	protected ReconcilePositionService reconcilePositionService;
	@Resource
	protected RuleEvaluatorService ruleEvaluatorService;
	@Resource
	protected RuleViolationUtils ruleViolationUtils;
	@Resource
	protected SystemBeanService systemBeanService;
	@Resource
	protected WorkflowDefinitionService workflowDefinitionService;


	public abstract boolean isTestInitialized();


	public abstract void setTestInitialized(boolean testInitialized);


	@Override
	public String getCategoryName() {
		return "Portfolio Run Rules";
	}


	/*
	This method is called from the 'initializeTests' method annotated with '@Before'

	It creates a new PortfolioRun with a new client account and relevant related account and management accounts.
	It then iterates all methods annotated with '@Test' and invokes each.
	Each test should create any necessary test data based on the 'testInitialized' variable.
	Once initialization is complete the 'triggerRuleViolations' method will be called.  It should be
	implemented to trigger the event that will cause rule violations based on the created data (e.g. process the PortfolioRun).
	Once the event has been triggered, all violations will be retrieved for the entity (e.g. for the PortfolioRun) and
	stored in a static list.  Each test will then be executed by jUnit and they will attempt to retrieve the expected violation message
	from the static list of violation messages.

	Example method:

		public void testExampleViolationRule() {
			if (!testInitialized) {
				String expectedViolationNote = createExampleViolationData();
				expectedViolationNoteMap.put("testExampleViolationRule", expectedViolationNote);
			}
			else {
				//Defined in 'RuleTests' it asserts the message exists in the static map of violations.
				validateTestMethodResult("testExampleViolationRule");
			}
		}


	Additional validation may be done in the tests, but at the most basic level the message
	should be validated. - Steve
	 */
	@BeforeEach
	@Override
	public void initializeTest() {
		if (!isTestInitialized()) {
			super.initializeTest();

			updateRuleDefinitions();

			portfolioRun = createPortfolioRun();

			//invoke all methods during initialization to create data necessary to trigger rule violations.
			createTestData();

			Integer linkedFkFieldId = triggerRuleViolations();
			RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
			ruleViolationSearchForm.setLinkedFkFieldId(MathUtils.getNumberAsLong(linkedFkFieldId));
			ruleViolationList = this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm);

			setTestInitialized(true);
		}
	}


	private void updateRuleDefinitions() {
		RuleDefinition ruleDefinition = this.ruleDefinitionService.getRuleDefinitionByName("Manager Account: Total Market Value vs. Previous Run Total Market Value");
		SystemBean evaluatorBean = this.systemBeanService.getSystemBean(ruleDefinition.getEvaluatorBean().getId());

		int businessCompanyIdsIndex = -1;
		List<SystemBeanProperty> systemBeanPropertyList = evaluatorBean.getPropertyList();
		for (int i = 0; i < systemBeanPropertyList.size(); i++) {
			SystemBeanProperty beanProperty = systemBeanPropertyList.get(i);
			if ("businessCompanyIds".equals(beanProperty.getType().getSystemPropertyName())) {
				businessCompanyIdsIndex = i;
			}
		}
		if (businessCompanyIdsIndex >= 0) {
			systemBeanPropertyList.remove(businessCompanyIdsIndex);
		}
		evaluatorBean.setPropertyList(systemBeanPropertyList);
		this.systemBeanService.saveSystemBean(evaluatorBean);
	}


	protected void createTestData() {
		for (Method method : testMethodSet) {
			try {
				method.invoke(this);
			}
			catch (IllegalAccessException | InvocationTargetException e) {
				testExceptionsMap.put(method.getName(), e);
			}
		}
	}


	protected Integer triggerRuleViolations() {
		this.workflowTransitioner.transitionIfPresent("Process Run", PORTFOLIO_RUN_TABLE_NAME, BeanUtils.getIdentityAsLong(portfolioRun));
		return portfolioRun.getId();
	}


	private PortfolioRun createPortfolioRun() {
		this.lastBusinessDay = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
		this.investmentSecurity = this.investmentInstrumentUtils.getCopyOfSecurity("LPZ13", "LPZ13-" + new Date().getTime());
		this.investmentSecurity.getInstrument().setInactive(false);
		this.investmentInstrumentService.saveInvestmentInstrument(this.investmentSecurity.getInstrument());
		this.investmentSecurity.setEndDate(DateUtils.addWeekDays(this.lastBusinessDay, 5));
		this.investmentSecurity.setLastDeliveryDate(DateUtils.addWeekDays(this.lastBusinessDay, 5));
		this.investmentInstrumentService.saveInvestmentSecurity(this.investmentSecurity);
		this.accountInfo = this.investmentAccountUtils.createAccountsForPortfolioRun(BusinessCompanies.GOLDMAN_SACHS_AND_CO,
				BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.DEUTSCHE_BANK_SECURITIES, ServiceProcessingTypes.PORTFOLIO_RUNS, this.futures);

		//A change Terry made to the createAccountsForTrading method caused my swaps account needed for the unbookedClearedM2MRule to be null.
		//rather than changing back I just create the account here as otherwise may have conflicting code?
		this.accountInfo.setSwapsHoldingAccount(this.investmentAccountUtils.createInvestmentAccount(AccountCreationCommand.ofActive(InvestmentAccountType.OTC_CLEARED, this.businessUtils.createBusinessClient(), BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessContractTypes.ADDENDUM_CLEARED_DERIVATIVES)));
		this.investmentAccountUtils.createAccountRelationship(this.accountInfo.getClientAccount(), this.accountInfo.getSwapsHoldingAccount(), InvestmentAccountRelationshipPurposes.MARK_TO_MARKET);


		this.executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		this.tradeDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(this.lastBusinessDay, this.investmentCalculatorService.getInvestmentSecurityCalendar(this.investmentSecurity.getId()).getId()), -1);
		this.tradeQuantity = new BigDecimal("10");

		//Create a new run for the day prior to today - this is to allow proper testing of rules that compare against the previous run
		PortfolioRun previousPortfolioRun = this.portfolioRunUtils.createPortfolioRun(this.accountInfo.getClientAccount(),
				this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(this.lastBusinessDay), -1), true);

		//Create a manager account for the previous run
		InvestmentManagerAccount previousRunManagerAccount = createManagerAccountWithAssignmentAndBalance(this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(this.lastBusinessDay), -1));

		//process the previous run
		this.workflowTransitioner.transitionIfPresent("Process Run", PORTFOLIO_RUN_TABLE_NAME, BeanUtils.getIdentityAsLong(previousPortfolioRun));

		//set the manager account to inactive - to test added/removed manager account rule
		InvestmentManagerAccountAssignment investmentManagerAccountAssignment =
				this.investmentManagerAccountService.getInvestmentManagerAccountAssignmentByManagerAndAccount(previousRunManagerAccount.getId(),
						this.accountInfo.getClientAccount().getId());
		investmentManagerAccountAssignment.setInactive(true);
		this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(investmentManagerAccountAssignment);
		previousRunManagerAccount.setInactive(true);
		this.investmentManagerAccountUtils.saveInvestmentManagerAccount(previousRunManagerAccount);

		//Create the new run that all data for rule testing will be created for now.
		PortfolioRun portfolioRun = this.portfolioRunUtils.createPortfolioRun(this.accountInfo.getClientAccount(), this.lastBusinessDay, true);
		//Create a new manager account for this run.
		createManagerAccountWithAssignmentAndBalance(this.lastBusinessDay);
		return portfolioRun;
	}


	//TODO - break this method up or move?
	public InvestmentManagerAccount createManagerAccountWithAssignmentAndBalance(Date balanceDate) {
		InvestmentManagerAccount managerAccount = InvestmentManagerAccountBuilder.investmentManagerAccount(this.investmentManagerAccountService)
				.withAccountName(RandomUtils.randomNameAndNumber())
				.withClient(this.accountInfo.getBusinessClient())
				.withManagerCompany(this.accountInfo.getClientCompany())
				.withManagerType(InvestmentManagerAccount.ManagerTypes.STANDARD)
				.withCashAdjustmentType(InvestmentManagerAccount.CashAdjustmentType.NONE)
				.withBaseCurrency(this.usd)
				.buildAndSave();


		//Create a manager account balance for the previous run
		this.investmentManagerAccountUtils.createInvestmentManagerAccountBalance(managerAccount,
				balanceDate, new BigDecimal(1000000), new BigDecimal(2000000));
		//Create an investment asset class for the previous run

		InvestmentAssetClass investmentAssetClass = InvestmentAssetClassBuilder.investmentAssetClass(this.investmentSetupService)
				.withName("AssetClass-" + new Date().getTime())
				.withMaster(true)
				.withCash(false)
				.withMainCash(false)
				.buildAndSave();

		InvestmentAccountAssetClassBuilder.investmentAccountAssetClass(this.investmentAccountAssetClassService)
				.withAccount(this.accountInfo.getClientAccount())
				.withAssetClass(investmentAssetClass)
				.withAssetClassPercentage(new BigDecimal(100))
				.withCashPercentage(new BigDecimal(100))
				.withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources.TARGET_EXPOSURE)
				.withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes.NONE)
				.buildAndSave();

		//Create investment manager allocation and assignment for previous run.
		List<InvestmentManagerAccountAllocation> investmentManagerAccountAllocationList = new ArrayList<>();
		investmentManagerAccountAllocationList.add(this.investmentManagerAccountUtils.createInvestmentManagerAccountAllocation(investmentAssetClass, new BigDecimal(100)));
		this.investmentManagerAccountUtils.createInvestmentManagerAccountAssignment(managerAccount,
				this.accountInfo.getClientAccount(), investmentManagerAccountAllocationList);
		return managerAccount;
	}
}
