package com.clifton.ims.tests.rule.portfolio.run;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.system.query.JdbcUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassSearchForm;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.ManagerCashOverlayTypes;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.manager.ManagerCustomCashAllocationTypes;
import com.clifton.investment.manager.ManagerCustomCashOverlayTypes;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassCashAdjustment;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.search.RuleAssignmentSearchForm;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


public class PortfolioRunPostProcessRuleTests extends BasePortfolioRunRuleTests {

	private static boolean testInitialized = false;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public Set<String> getExcludedTestMethodSet() {
		Set<String> excludedTestMethodSet = new HashSet<>();
		//Globally excluded
		excludedTestMethodSet.add("testContractsUnmatchedRule");
		//System Defined
		excludedTestMethodSet.add("testRunValidationViolationRule");

		// Globally Excluded - and also compares event date to TODAY so results aren't the same for a run if it's run or previewed on different days
		excludedTestMethodSet.add("testCashDividendPaymentExDateRule");
		excludedTestMethodSet.add("testCashDividendPaymentPaymentDateRule");

		return excludedTestMethodSet;
	}


	@Override
	public Boolean getPreProcess() {
		return false;
	}


	@Override
	public boolean isTestInitialized() {
		return testInitialized;
	}


	@Override
	public void setTestInitialized(boolean initialized) {
		testInitialized = initialized;
	}


	@Override
	protected void applyRuleDefinitionSearchFormOverrides(RuleDefinitionSearchForm ruleDefinitionSearchForm) {
		ruleDefinitionSearchForm.setRuleScopeNames(new String[]{"Overlay Client Accounts", "LDI Client Accounts"});
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPortfolioRunFundCashIsNegativeRule() {
		InvestmentAccount client = this.investmentAccountService.getInvestmentAccountByNumber("386000");
		PortfolioRun run = this.portfolioRunService.getPortfolioRunByMainRun(client.getId(), DateUtils.toDate("03/12/2021"));
		ProductOverlayAssetClassCashAdjustment adj = new ProductOverlayAssetClassCashAdjustment();
		adj.setAmount(new BigDecimal("1000000000"));
		List<ProductOverlayAssetClass> acList = this.productOverlayService.getProductOverlayAssetClassListByRun(run.getId());
		ProductOverlayAssetClass ac = CollectionUtils.getOnlyElement(acList.stream().filter(assetClass -> "Domestic Equity Large Cap".equals(assetClass.getLabel())).collect(Collectors.toList()));
		adj.setOverlayAssetClass(ac);
		adj.setFromCashType(ManagerCashTypes.FUND);
		adj.setToCashType(ManagerCashTypes.CLIENT_DIRECTED);
		adj.setNote("Test");
		this.productOverlayService.saveProductOverlayAssetClassCashAdjustmentAllowNegative(adj);
		validateTestMethodResultWithExistingRun(client.getNumber(), DateUtils.fromDate(run.getBalanceDate(), DateUtils.DATE_FORMAT_FULL), "testPortfolioRunFundCashIsNegativeRule", "Total fund cash for 386000: MCERA on Mar 12, 2021 is negative.");
	}


	@Test
	public void testAccountWithUnapprovedEventsRule() {
		if (testInitialized) {
			InvestmentEvent event = new InvestmentEvent();
			event.setEventType(this.investmentCalendarService.getInvestmentEventTypeByName("Contact Client Reminder"));
			event.setInvestmentAccount(portfolioRun.getClientInvestmentAccount());
			event.setEventDate(DateUtils.clearTime(portfolioRun.getBalanceDate()));
			event.setWorkflowStatus(this.workflowDefinitionService.getWorkflowStatusByName("Draft"));
			RequestOverrideHandler.serviceCallWithOverrides(() -> this.investmentCalendarService.saveInvestmentEvent(event, true), new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, false, "aumCalculatorBean"));
		}
		String expectedViolation = "There are 1 Investment Calendar Events in an Unapproved State";
		validateTestMethodResultWithExistingRun(portfolioRun.getClientInvestmentAccount().getNumber(), DateUtils.fromDate(portfolioRun.getBalanceDate(), DateUtils.DATE_FORMAT_INPUT), "testAccountWithUnapprovedEventsRule", expectedViolation);
	}


	@Test
	public void testAssetClassChangedSincePreviousRunRule() {
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber("040001");
		PortfolioRun run = this.portfolioRunService.getPortfolioRunByMainRun(account.getId(), DateUtils.toDate("04/14/2016"));
		ProductOverlayAssetClass overlayAssetClass = CollectionUtils.getFirstElement(this.productOverlayService.getProductOverlayAssetClassListByRun(run.getId()));
		InvestmentAccountAssetClass assetClass;
		if (overlayAssetClass != null) {
			assetClass = overlayAssetClass.getAccountAssetClass();
			if (!testInitialized) {
				assetClass.setCashPercentage(MathUtils.add(assetClass.getCashPercentage(), BigDecimal.ONE));
				this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(assetClass);
				assetClass = this.investmentAccountAssetClassService.getInvestmentAccountAssetClass(assetClass.getId());
			}
			String time = DateUtils.fromDate(assetClass.getUpdateDate(), "hh:mm");
			String date = DateUtils.fromDate(assetClass.getUpdateDate(), "MM/dd/yyyy");
			String userName = this.securityUserService.getSecurityUser(assetClass.getUpdateUserId()).getDisplayName();
			String expectedViolation = assetClass.getLabel() + " was updated at " + time + " by " + userName + " on " + date;
			validateTestMethodResultWithExistingRun("040001", "04/14/2016", "testAssetClassChangedSincePreviousRunRule", expectedViolation);
		}
	}


	@Test
	public void testAssetClassLargeChangeInBenchmarkDurationSincePreviousDayRule() {
		validateTestMethodResultWithExistingRun("730100", "05/26/2014", "testAssetClassBenchmarkDurationValueLargeChangeSincePreviousDayRule",
				"Benchmark Security in Deflation Hedging Class had significant duration change: LUATTRUU<br /><span class='violationMessage'>Previous duration value of <span class='amountPositive'>1.85</span> is now <span class='amountPositive'>5.26</span>, a change of <span class='amountPositive'>184.32%</span> is greater than maximum <span class='amountPositive'>10%</span></span>");
	}


	@Test
	public void testAssetClassOutdatedBenchmarkDurationRule() {
		validateTestMethodResultWithExistingRun("050920", "01/08/2016", "testAssetClassOutdatedBenchmarkDurationRule",
				"Benchmark Security in Fixed asset class has an outdated duration: USH15<br /><span class='violationMessage'>Benchmark duration is outdated by 324 day(s) which is greater than the allowed duration of 35</span>");
	}


	@Test
	public void testAssetClassOutdatedReplicationRule() {
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber("050980");
		PortfolioRun run = this.portfolioRunService.getPortfolioRunByMainRun(account.getId(), DateUtils.toDate("06/08/2016"));
		ProductOverlayAssetClassReplication assetClassReplication = CollectionUtils.getFirstElement(this.productOverlayService.getProductOverlayAssetClassReplicationListByRun(run.getId()));
		InvestmentReplication replication;

		if (assetClassReplication != null) {
			replication = assetClassReplication.getReplication();
			if (!testInitialized) {
				JdbcUtils.executeQuery("Update InvestmentReplication SET UpdateDate = '1/1/2014' WHERE InvestmentReplicationID = '" + replication.getId() + "';");
				this.systemRequestStatsService.deleteSystemCache("com.clifton.investment.replication.InvestmentReplication");
				this.systemRequestStatsService.deleteSystemCache("com.clifton.product.overlay.ProductOverlayAssetClassReplication");
			}
			String expectedViolation = "Replication " + replication.getLabel() + " is outdated:<br /><span class='violationMessage'>" +
					"Outdated by 889 days which is greater than 380 days</span>";
			validateTestMethodResultWithExistingRun("050980", "06/08/2016", "testAssetClassOutdatedReplicationRule", expectedViolation);
		}
	}


	@Test
	public void testAssetClassReplicationDeviationAmountFromTargetRangeRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("050952");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Asset Class Replication: Deviation Amount From Target Range", null, BigDecimal.valueOf(-300000.0d),
						BigDecimal.valueOf(300000.0d), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("050952", "09/30/2015", "testAssetClassReplicationDeviationAmountFromTargetRangeRule",
				"Foreign Currency Asset Class Overlay Exposure deviation from target: <span class='amount(Positive|Negative)'>-?[0-9]+,?[0-9]+.[0-9]+</span>");
	}


	@Test
	public void testAssetClassReplicationConsecutive0AllocationWithContractsRule() {
		// Expect violation on the second day
		validateTestMethodResultWithExistingRun("051102", "02/20/2020", "testAssetClassReplicationConsecutive0AllocationWithContractsRule",
				"Equity Hedge - DKK - Currency Replication (Excludes Other) - DKK/GBP Currency Forward has zero allocation weights for consecutive days but still holds -882947.95 contract(s).");
	}


	@Test
	public void testAssetClassReplicationDeviationFromTargetRangeRule() {
		InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("386601");
		if (mainClientAccount != null) {
			this.ruleViolationUtils.createRuleAssignment("Asset Class Replication: Deviation % From Target Range", null, BigDecimal.valueOf(-1.0d),
					BigDecimal.valueOf(1.0d), mainClientAccount, null);
		}
		validateTestMethodResultWithExistingRun("386601", "03/11/2016", "testAssetClassReplicationDeviationFromTargetRangeViolationRule",
				"US Equity Hedge Asset Class Overlay Exposure deviation from target: <span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span>");
	}


	@Test
	public void testCashTargetBalanceRangeRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("331500");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Cash Target: Balance Range", null, BigDecimal.valueOf(500000000),
						BigDecimal.valueOf(1000000000), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("331500", "5/24/2018", "testCashTargetBalanceRangeRule",
				"Cash Target Balance <span class='amountPositive'>477,208,633.94</span> is less than minimum of <span class='amountPositive'>500,000,000.00</span>");
	}


	@Test
	public void testCashExposureAmountRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("221800");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Cash Exposure Amount", null, BigDecimal.valueOf(-10000000),
						BigDecimal.valueOf(10000000), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("221800", "10/22/2015", "testCashExposureAmountRule",
				"Cash exposure of <span class='amountNegative'>-14,574,040.72</span> is less than minimum <span class='amountNegative'>-10,000,000.00</span>");
	}


	@Test
	public void testCashExposureOfTotalPortfolioValueRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("221800");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Cash Exposure % of Total Portfolio Value", null, BigDecimal.ZERO,
						BigDecimal.valueOf(0.1d), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("221800", "10/22/2015", "testCashExposureOfTotalPortfolioValueRule",
				"Cash exposure of <span class='amountNegative'>-0.16%</span> of Total Portfolio Value, is less than minimum <span class='amountPositive'>0%</span>");
	}


	//Need to create data dynamically
	@Disabled
	@Test
	public void testCounterpartyExposureLimitMarketValueRule() {
		validateTestMethodResultWithExistingRun("221800", "06/01/2015", "testCounterpartyExposureLimitMarketValueRule",
				"Market Value Limit Violated:<br />\n" +
						"<span style='padding-left:20px;'>\n" +
						"[13.88]% is greater than required maximum [5.00]%.\n" +
						"</span>");
	}


	//Need to create data dynamically
	@Disabled
	@Test
	public void testCounterpartyExposureLimitNotionalValueRule() {
		validateTestMethodResultWithExistingRun("221800", "06/01/2015", "testCounterpartyExposureLimitNotionalValueRule",
				"Notional Value Limit Violated:<br />\n" +
						"<span style='padding-left:20px;'>\n" +
						"[1,861.40]% is greater than required maximum [5.00]%.\n" +
						"</span>");
	}


	//Need to create data dynamically
	@Disabled
	@Test
	public void testCounterpartyExposureLimitOpenTradeEquityRule() {
		validateTestMethodResultWithExistingRun("221800", "06/01/2015", "testCounterpartyExposureLimitOpenTradeEquityRule",
				"Open Trade Equity Value Limit Violated:<br />" +
						"<span style='padding-left:20px;'>" +
						"[5.22]% is greater than required maximum [5.00]%." +
						"</span>");
	}


	@Test
	public void testCurrencyBalanceSingleBaseExposureRangeRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("340010");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Currency Balance: Single Base Exposure Range", null, BigDecimal.valueOf(-300000),
						BigDecimal.valueOf(300000), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("340010", "07/15/2015", "testCurrencyBalanceSingleBaseExposureRangeRule",
				"JPY Currency Balance of JPY Denominated Positions:<br /><span class='violationMessage'><span class='amount(Positive|Negative)'>-?[0-9]+,?[0-9]+.[0-9]+</span> is greater than maximum <span class='amountPositive'>300,000.00</span></span>");
	}


	@Test
	public void testCurrencyBalanceSingleBaseExposureOfReplicationRangeRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("340010");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Currency Balance: Single Base Exposure % Of Replication Range", null, BigDecimal.valueOf(-3),
						BigDecimal.valueOf(3), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("340010", "07/15/2015", "testCurrencyBalanceSingleBaseExposureOfReplicationRangeRule",
				"JPY Currency Balance as a % of JPY Denominated Positions:<br /><span class='violationMessage'><span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span> is greater than maximum <span class='amountPositive'>3%</span></span>");
	}


	@Test
	public void testCurrencyBalanceTotalBaseExposureRangeRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("340010");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Currency Balance: Total Base Exposure Range", null, BigDecimal.valueOf(-500000),
						BigDecimal.valueOf(500000), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("340010", "07/15/2015", "testCurrencyBalanceTotalBaseExposureRangeRule",
				"Total Base Currency Balance <span class='amount(Positive|Negative)'>-?[0-9]+,?[0-9]+.[0-9]+</span> is greater than the required maximum of <span class='amountPositive'>500,000.00</span>");
	}


	@Test
	public void testCurrencyBalanceTotalBaseExposureOfAssetClassOverlayTargetRangeViolationRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("459000");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Currency Balance Total Base Exposure % of Asset Class Overlay Target Range Violation", null, BigDecimal.valueOf(-2.5),
						BigDecimal.valueOf(2.5), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("459000", "02/25/2016", "testCurrencyBalanceTotalBaseExposureOfAssetClassOverlayTargetRangeViolationRule",
				"Total Base Currency Balance <span class='amountPositive'>655,398.57</span> as a percentage of Global Fixed Income Overlay Target <span class='amountPositive'>22,794,018.37</span>:<br /><span class='violationMessage'><span class='amountPositive'>2.88%</span> is greater than the required maximum of <span class='amountPositive'>2.5%</span>");
	}


	@Test
	public void testCurrencyBalanceTotalBaseExposureOfCashRangeRule() {
		if (testInitialized) {
			//Ensure there is an included assignment for the test.
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("340010");
			if (mainClientAccount != null) {
				this.ruleViolationUtils.createRuleAssignment("Currency Balance: Total Base Exposure % Of Cash Range", null, BigDecimal.valueOf(-2),
						BigDecimal.valueOf(2), mainClientAccount, null);
			}
		}
		validateTestMethodResultWithExistingRun("340010", "07/15/2015", "testCurrencyBalanceTotalBaseExposureOfCashRangeRule",
				"Total Base Currency Balance <span class='amount(Positive|Negative)'>-?[0-9]+,?[0-9]+.[0-9]+</span> as a percentage of Cash Total <span class='amount(Positive|Negative)'>-?[0-9]+,?[0-9]+,?[0-9]+.[0-9]+</span>:<br /><span class='violationMessage'><span class='amount(Positive|Negative)'>-?[0-9]+.[0-9]+%</span> is greater than the required maximum of <span class='amountPositive'>2%</span>");
	}


	@Test
	public void testCurrencyBalanceNoCurrencyFutureRule() {
		validateTestMethodResultWithExistingRun("662950", "09/04/2015", "testCurrencyBalanceNoCurrencyFutureRule",
				"Currency balance found for 'CAD' without corresponding future on account '662950: The University of Oklahoma Foundation, Inc.'");
	}


	@Test
	public void testDailyGainLossVsOurAccountValueChangeRule() {
		validateTestMethodResultWithExistingRun("454000", "05/02/2016", "testDailyGainLossVsOurAccountValueChangeRule",
				"Parametric Account Value Difference is <span class='amountNegative'>-2.27%</span>:<br /><span class='violationMessage'>Parametric Change <span class='amountNegative'>-100,075.90</span> vs. Daily Gain/Loss <span class='amountPositive'>0.00</span></span>");
	}


	@Disabled //TODO - fix this to be dynamic
	@Test
	public void testExplicitPositionAllocationChangeSincePreviousDayRule() {
		validateTestMethodResultWithExistingRun("297425", "06/18/2015", "testExplicitPositionAllocationChangeSincePreviousDayRule",
				"Position Allocation [Materials - CDU5 (C$ CURRENCY FUT   Sep15) (06/18/2015 - 07/08/2015)] has explicit position allocation that has changed since previous day.  Previous allocation amount [17.00]. Current allocation amount [21.00].");
	}


	@Test
	public void testExplicitPositionAllocationUnableToFulfillRule() {
		validateTestMethodResultWithExistingRun("576700", "11/05/2014", "testExplicitPositionAllocationUnableToFulfillRule",
				"Domestic Equity Large Cap - ESZ14 (S&P500 EMINI FUT  Dec14) [INACTIVE] (11/05/2014 - 12/11/2014) allocation was unable to be fulfilled:<br /><span class='violationMessage'>Actual Allocation - <span class='amountPositive'>0.00</span><br />Virtual Allocation - <span class='amountNegative'>-3.00</span></span>");
	}


	@Test
	public void testManagerAccountAddedRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			expectedViolationNoteMap.put("testManagerAccountAddedRule", managerAccount.getAccountName() + " Manager Account was Added/Activated");
		}
		else {
			validateTestMethodResult("testManagerAccountAddedRule");
		}
	}


	@Test
	public void testManagerAccountBalanceAdjustedFromEventRule() {
		validateTestMethodResultWithExistingRun("221800", "08/13/2015", "testManagerAccountBalanceAdjustedFromEventRule",
				"Hamilton Lane [Adjustment Automatically applied from previous day [MOC] adjustment]:<br /><span class='violationMessage'>Securities <span class='amountPositive'>2,870,952.75</span>, Cash <span class='amountPositive'>0.00</span> </span>");
	}


	@Test
	public void testManagerAccountBalanceManuallyAdjustedRule() {
		validateTestMethodResultWithExistingRun("567200", "11/03/2015", "testManagerAccountBalanceManuallyAdjustedRule",
				"Parametric - Overlay [Margin Variation Payable]:<br /><span class='violationMessage'>Securities <span class='amountPositive'>0.00</span>, Cash <span class='amountNegative'>-249,119.99</span>  by James Thorson</span>");
	}


	@Test
	public void testManagerAccountRebalanceRecommendedRule() {
		validateTestMethodResultWithExistingRun("495000", "10/15/2015", "testManagerAccountRebalanceRecommendedRule",
				"Manager Account Domestic Equity Large Cap Growth is off target for group PFCC Large Cap Value/Growth:<br /><span class='violationMessage'><span class='amountNegative'>-223,245,676.70</span> is less than minimum of <span class='amountNegative'>-15,627,197.39</span></span>");
	}


	@Test
	public void testManagerAccountBalanceRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Balance Range", null, BigDecimal.valueOf(100), BigDecimal.valueOf(150), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountBalanceRangeRule", "Manager " + managerAccount.getAccountName() + " Total Balance:<br /><span class='violationMessage'><span class='amountPositive'>3,000,000.00</span> is greater than maximum of <span class='amountPositive'>150.00</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountBalanceRangeRule");
		}
	}


	@Test
	public void testManagerAccountCashBalanceRangeRule() {
		validateTestMethodResultWithExistingRun("158799", "06/17/2019", "testManagerAccountCashBalanceRangeRule",
				"EUR Hedge - Cash Manager Cash Balance:<br /><span class='violationMessage'><span class='amountNegative'>-7,910.96</span> is less than minimum of <span class='amountPositive'>0.00</span></span>");
	}


	@Test
	public void testManagerAccountCashPercentageRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Cash Percentage Range", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountCashPercentageRangeRule", "Manager " + managerAccount.getAccountName() + " Cash Percent of Balance:<br /><span class='violationMessage'><span class='amountPositive'>33.33%</span> is greater than maximum of <span class='amountPositive'>1%</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountCashPercentageRangeRule");
		}
	}


	@Test
	public void testManagerAccountDailyChangeVsBenchmarkRule() {
		String clientAccountNumber = "455000";
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(clientAccountNumber);
		InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
		searchForm.setAssetClassName("Fixed Income");
		searchForm.setAccountId(clientAccount.getId());
		InvestmentAccountAssetClass assetClass = CollectionUtils.getFirstElementStrict(this.investmentAccountAssetClassService.getInvestmentAccountAssetClassList(searchForm));
		if (!testInitialized) {
			disableExcludedRuleAssignmentForRuleAndClientAccount("Manager Account: Daily Change vs. Benchmark", clientAccount.getId());
		}
		validateTestMethodResultWithExistingRun(clientAccountNumber, "10/15/2015", "testManagerAccountDailyChangeVsBenchmarkRule",
				"Anchorage Illiquid Offshore - Fixed Income Changed <span class='amountPositive'>6.75%</span>:<br /><span class='violationMessage'>Difference is <span class='amountPositive'>6.94%</span> from " + assetClass.getBenchmarkSecurity().getSymbol() + " which is greater than maximum allowed <span class='amountPositive'>3%</span></span>");
	}


	private void disableExcludedRuleAssignmentForRuleAndClientAccount(String ruleName, int clientAccountId) {
		RuleAssignmentSearchForm searchForm = new RuleAssignmentSearchForm();
		searchForm.setRuleDefinitionName(ruleName);
		searchForm.setAdditionalScopeFkFieldIdEquals(MathUtils.getNumberAsLong(clientAccountId));
		searchForm.setNullEntityFkFieldId(true);
		RuleAssignment ruleAssignment = CollectionUtils.getFirstElementStrict(this.ruleDefinitionService.getRuleAssignmentList(searchForm));
		if (ruleAssignment.isExcluded()) {
			ruleAssignment.setEndDate(DateUtils.toDate("01/01/2010"));
		}
		this.ruleDefinitionService.saveRuleAssignment(ruleAssignment);
	}


	@Test
	public void testManagerAccountTotalMarketValueVsPreviousRunTotalMarketValueRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Total Market Value vs. Previous Run Total Market Value", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountTotalMarketValueVsPreviousRunTotalMarketValueRule", managerAccount.getAccountName() + " changed <span class='amountPositive'>100%</span>:<br /><span class='violationMessage'>Market Value <span class='amountPositive'>3,000,000.00</span> vs. Previous Run Total Market Value <span class='amountPositive'>0.00</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountTotalMarketValueVsPreviousRunTotalMarketValueRule");
		}
	}


	@Test
	public void testManagerAccountSecuritiesBalanceRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Securities Balance Range", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountSecuritiesBalanceRangeRule", "Manager " + managerAccount.getAccountName() + " Securities Balance:<br /><span class='violationMessage'><span class='amountPositive'>2,000,000.00</span> is greater than maximum of <span class='amountPositive'>1.00</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountSecuritiesBalanceRangeRule");
		}
	}


	@Test
	public void testManagerAccountSecuritiesPercentageRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Securities Percentage Range", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountSecuritiesPercentageRangeRule", "Manager " + managerAccount.getAccountName() + " Securities Percent of Balance:<br /><span class='violationMessage'><span class='amountPositive'>66.67%</span> is greater than maximum of <span class='amountPositive'>1%</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountSecuritiesPercentageRangeRule");
		}
	}


	@Test
	public void testManagerAccountTotalPortfolioCashPercentageRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Total Portfolio Cash Percentage Range", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountTotalPortfolioCashPercentageRangeRule", "Manager " + managerAccount.getAccountName() + " Cash Balance Percent of Total Portfolio Value:<br /><span class='violationMessage'><span class='amountPositive'>4.55%</span> is greater than maximum of <span class='amountPositive'>1%</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountTotalPortfolioCashPercentageRangeRule");
		}
	}


	@Test
	public void testManagerAccountTotalPortfolioPercentageRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Total Portfolio Percentage Range", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountTotalPortfolioPercentageRangeRule", "Manager " + managerAccount.getAccountName() + " Total Balance Percent of Total Portfolio Value:<br /><span class='violationMessage'><span class='amountPositive'>13.64%</span> is greater than maximum of <span class='amountPositive'>1%</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountTotalPortfolioPercentageRangeRule");
		}
	}


	@Test
	public void testManagerAccountTotalPortfolioSecuritiesPercentageRangeRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			this.ruleViolationUtils.createRuleAssignment("Manager Account: Total Portfolio Securities Percentage Range", null, BigDecimal.valueOf(-1), BigDecimal.valueOf(1), this.accountInfo.getClientAccount(), managerAccount);
			expectedViolationNoteMap.put("testManagerAccountTotalPortfolioSecuritiesPercentageRangeRule", "Manager " + managerAccount.getAccountName() + " Securities Balance Percent of Total Portfolio Value:<br /><span class='violationMessage'><span class='amountPositive'>9.09%</span> is greater than maximum of <span class='amountPositive'>1%</span></span>");
		}
		else {
			validateTestMethodResult("testManagerAccountTotalPortfolioSecuritiesPercentageRangeRule");
		}
	}


	@Test
	public void testManagerAccountBalanceCustomCashOverlayAllocationRule() {
		if (!testInitialized) {
			InvestmentManagerAccount managerAccount = createManagerAccountWithAssignmentAndBalance(portfolioRun.getBalanceDate());
			InvestmentManagerAccountAssignment assignment = this.investmentManagerAccountService.getInvestmentManagerAccountAssignmentByManagerAndAccount(managerAccount.getId(), portfolioRun.getClientInvestmentAccount().getId());
			assignment.setCashOverlayType(ManagerCashOverlayTypes.FUND);
			assignment.setCashType(ManagerCashTypes.MANAGER);
			assignment.setCustomCashAllocationType(ManagerCustomCashAllocationTypes.MANAGER_ASSET_CLASSES);
			assignment.setCustomCashOverlayType(ManagerCustomCashOverlayTypes.CUSTOM_PERCENT);
			InvestmentManagerAccountAllocation customCashAllocation = new InvestmentManagerAccountAllocation();
			customCashAllocation.setCustomAllocationValue(BigDecimal.valueOf(75));
			customCashAllocation.setAssetClass(assignment.getAllocationList().get(0).getAssetClass());
			customCashAllocation.setCashType(ManagerCashTypes.TRANSITION);
			customCashAllocation.setNote("TESTING");
			customCashAllocation.setCustomCashOverlay(true);
			assignment.setCustomCashOverlayAllocationList(CollectionUtils.createList(customCashAllocation));
			this.investmentManagerAccountService.saveInvestmentManagerAccountAssignment(assignment);
			// Actual Rule Violation is a Full Table, this just looks for specific points of data within the violation note
			expectedViolationNoteMap.put("testManagerAccountBalanceCustomCashOverlayAllocationRule", "Custom Cash Overlay Allocation applied to.*" + managerAccount.getLabel() + "(.*(\\n))*.*" + customCashAllocation.getAssetClass().getLabel() + "(.*(\\n))*.*Transition(.*(\\n))*.*750,000.00(.*(\\n))*.*TESTING(.*(\\n))*.*Fund Cash Weights .*default(.*(\\n))*.*Manager .*default(.*(\\n))*.*62,500.00(.*(\\n))*.*812,500.00(.*(\\n))*.*");
		}
		else {
			validateTestMethodResult("testManagerAccountBalanceCustomCashOverlayAllocationRule");
		}
	}


	@Test
	public void testMarginAnalysisOurAccountValueRangeRule() {
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber("051060");
		PortfolioRun run = this.portfolioRunService.getPortfolioRunByMainRun(account.getId(), DateUtils.toDate("03/08/2016"));
		PortfolioRunMargin runMargin = CollectionUtils.getFirstElement(this.portfolioRunMarginService.getPortfolioRunMarginListByRun(run.getId()));
		if (runMargin != null) {
			if (!testInitialized) {
				JdbcUtils.executeQuery("UPDATE PortfolioRunMargin SET OurAccountValue = '0' WHERE PortfolioRunMarginID = '" + runMargin.getId() + "';");
				this.systemRequestStatsService.deleteSystemCache("com.clifton.portfolio.run.margin.PortfolioRunMargin");
				this.ruleViolationUtils.createRuleAssignment("Margin Analysis \"Our Account\" Value Range", BigDecimal.ZERO, null, null, account, null);
			}
			String expectedViolation = "Broker Account " + runMargin.getLabelShort() + " Margin Value <span class='amountPositive'>0.00</span>";
			validateTestMethodResultWithExistingRun("051060", "03/08/2016", "testMarginAnalysisOurAccountValueRangeRule", expectedViolation);
		}
	}


	@Test
	public void testPortfolioNAVvsGLAssetsDifferenceRule() {
		validateTestMethodResultWithExistingRun("800050", "10/15/2013", "testPIOSNAVvsGLAssetsDifferenceRule",
				"Portfolio NAV <span class='amountPositive'>11,721,084.87</span> does not match General Ledger Assets <span class='amountPositive'>11,509,552.97</span>.<br /><span class='violationMessage'>Difference: <span class='amountPositive'>211,531.90</span></span>");
	}


	@Test
	public void testContractsUnmatchedExcludeBondsAndCurrencyRule() {
		//Ensure there is an included assignment for the test.
		InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("456606");
		if (mainClientAccount != null) {
			this.ruleViolationUtils.createRuleAssignment("Contracts Unmatched: Exclude Bonds And Currency", null, null,
					null, mainClientAccount, null);
		}

		validateTestMethodResultWithExistingRun("456606", "10/08/2015", "testContractsUnmatchedExcludeBondsAndCurrencyRule",
				"Total Contracts: <span class='amountPositive'>[0-9,]{1,12}.00</span>, PIOS Contracts: <span class='amountPositive'>221,499,045.00</span><br /><span class='violationMessage'>Security SPXW US 10/09/15 P1840: Actual = <span class='amountNegative'>-140.00</span>, PIOS =  Missing<br />Security SPXW US 10/09/15 P1820: Actual = <span class='amountNegative'>-132.00</span>, PIOS =  Missing<br /></span>", false);
	}


	@Test
	public void testContractsUnmatchedExcludeCollateralAndCurrencyRule() {
		validateTestMethodResultWithExistingRun("340010", "05/05/2017", "testContractsUnmatchedExcludeCollateralAndCurrencyRule",
				"Total Contracts: <span class='amount(Positive|Negative)'>[-0-9,]{2,10}.00</span>, PIOS Contracts: <span class='amountPositive'>253.00</span><br /><span class='violationMessage'>Security 1EM7C 2430: Actual = <span class='amountNegative'>-43.00</span>, PIOS =  Missing<br />Security 4EK7C 2420: Actual = <span class='amountNegative'>-46.00</span>, PIOS =  Missing<br />Security 2EK7C 2385: Actual = <span class='amountNegative'>-47.00</span>, PIOS =  Missing<br />Security 3EK7C 2395: Actual = <span class='amountNegative'>-45.00</span>, PIOS =  Missing<br /></span>");
	}


	@Test
	public void testCashTargetChangeSincePreviousDayRule() {
		validateTestMethodResultWithExistingRun("605000", "11/19/2015", "testCashTargetChangeSincePreviousDayRule",
				"Cash Target Value changed <span class='amountPositive'>3.02%</span><br />\n" +
						"<span style='padding-left:20px;'>\n" +
						"Cash Target Value <span class='amountPositive'>283,885,187.32</span> vs. Previous Run Cash Target Value <span class='amountPositive'>275,557,395.99</span>\n" +
						"</span>");
	}


	@Test
	public void testSyntheticallyAdjustedPositionsExposureRangeRule() {
		validateTestMethodResultWithExistingRun("079800", "12/13/2013", "testSyntheticallyAdjustedPositionsExposureRangeRule",
				"Total Exposure for Synthetically Adjusted Positions <span class='amountPositive'>19,033,720.64</span> is " +
						"<span class='amountPositive'>15.06%</span> of the total portfolio value 126365098.45<br />" +
						"<span class='violationMessage'><span class='amountPositive'>15.06%</span> is greater than maximum of " +
						"<span class='amountPositive'>15%</span></span>", false);
	}


	@Test
	public void testPortfolioLDIRunUnexpectedKRDValueDateRule() {
		validateTestMethodResultWithExistingRun("719064", "2/4/2015", "testPortfolioLDIRunKRDDateRule",
				"Security TYH15 (US 10YR NOTE (CBT)Mar15) is using KRD Value Date of 12/31/2014 instead of expected 01/31/2015.<br /><span class='violationMessage'>This security is currently being used for the following: TYH15 (US 10YR NOTE (CBT)Mar15)</span>");
	}


	@Test
	public void testPortfolioLDIRunVariousKRDValueDatesRule() {
		validateTestMethodResultWithExistingRun("719064", "2/4/2015", "testPortfolioLDIRunKRDDateRule",
				"Various KRD Value Dates are being used across Manager Benchmarks and Position Exposure: 01/31/2015, 12/31/2014");
	}


	@Test
	public void testAssetClassRebalanceRecommendedRule() {
		validateTestMethodResultWithExistingRun("225980", "3/31/2015", "testAssetClassRebalanceRecommendedRule",
				"Public Inflation Sensitive - Commodities is off target: REBALANCE<br /><span class='violationMessage'>Off target <span class='amountNegative'>-118,023.76</span> which is below <span class='amountNegative'>-88,627.14</span></span>");
	}


	@Test
	public void testAssetClassMiniRebalanceRecommendedRule() {
		validateTestMethodResultWithExistingRun("446000", "10/07/2015", "testRebalancingMiniRebalanceRecommendedRule",
				"Total Rebalance adjusted cash outside Account specified range:<br /><span class='violationMessage'><span class='amountPositive'>14,205,401.09</span> is greater than <span class='amountPositive'>1.00</span></span>");
	}


	@Test
	public void testManagerAccountRemovedRule() {
		validateTestMethodResultWithExistingRun("221800", "09/07/2015", "testManagerAccountRemovedRule",
				"DFA International Manager Account was Removed/De-activated");
	}


	@Test
	public void testExplicitPositionAllocationUnusedRule() {
		validateTestMethodResultWithExistingRun("445500", "09/02/2015", "testExplicitPositionAllocationUnusedRule",
				"Global Equity - MESU15 (mini MSCI Emg Mkt Sep15) [INACTIVE] (08/25/2015 - 09/21/2015) allocation is not present:<br /><span class='violationMessage'>Security is not present on the run in that asset class.</span>");
	}


	@Test
	public void testLargeChangeInSecurityDurationSincePreviousDayRule() {
		validateTestMethodResultWithExistingRun("495200", "11/25/2015", "testLargeChangeInSecurityDurationSincePreviousDayRule",
				"Security in Global Fixed Income Asset Class and FIX BC Global Agg Replication had significant duration change: OEZ15<br /><span class='violationMessage'>Previous duration value of <span class='amountPositive'>4.89</span> is now <span class='amountPositive'>4.40</span>, a change of <span class='amountNegative'>-10.02%</span> is less than minimum <span class='amountNegative'>-10%</span> Please verify the duration value entered.</span>");
	}


	@Test
	public void testUnbookedClearedM2MRule() {
		if (!testInitialized) {
			AccountingM2MDaily accountingM2MDaily = new AccountingM2MDaily();
			accountingM2MDaily.setMarkDate(this.lastBusinessDay);
			accountingM2MDaily.setTransferAmount(new BigDecimal(10));
			accountingM2MDaily.setClientInvestmentAccount(this.accountInfo.getClientAccount());
			accountingM2MDaily.setHoldingInvestmentAccount(this.accountInfo.getSwapsHoldingAccount());
			this.accountingM2MService.saveAccountingM2MDaily(accountingM2MDaily);

			expectedViolationNoteMap.put("testUnbookedClearedM2MRule", Pattern.quote("Client Account [" + this.accountInfo.getClientAccount().getLabel() +
					"] has an unbooked M2M [" + accountingM2MDaily.getLabel() + "]."));
		}
		else {
			validateTestMethodResult("testUnbookedClearedM2MRule");
		}
	}


	@Test
	public void testVariationMarginRangeViolationRule() {
		validateTestMethodResultWithExistingRun("400200", "08/13/2015", "testVariationMarginRangeViolationRule",
				"Variation Margin Percentage Dropped Below <span class='amountPositive'>5%</span>:<br><span class='violationMessage'>Broker Variation margin <span class='amountPositive'>3.37%</span> is less than required minimum <span class='amountPositive'>5%</span></span>");
	}


	@Test
	public void testManualRebalanceRule() {
		validateTestMethodResultWithExistingRun("221800", "11/27/2017", "testManualRebalanceRule",
				"Manual rebalance was processed by Max Chisaka");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateTestMethodResultWithExistingRun(String accountNumber, String balanceDate, String testMethodName, String expectedViolationNote) {
		validateTestMethodResultWithExistingRun(accountNumber, balanceDate, testMethodName, expectedViolationNote, true);
	}


	private void validateTestMethodResultWithExistingRun(String accountNumber, String balanceDate, String testMethodName, String expectedViolationNote, boolean byMainRun) {
		//TODO - currently uses an existing account to trigger violations - ideally would create data dynamically using the generated accounts/runs as is the case for most tests.
		if (!testInitialized) {
			expectedViolationNoteMap.put(testMethodName, expectedViolationNote);
		}
		else {
			InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
			if (mainClientAccount != null) {
				PortfolioRun run;
				if (byMainRun) {
					run = this.portfolioRunService.getPortfolioRunByMainRun(mainClientAccount.getId(), DateUtils.toDate(balanceDate));
				}
				else {
					run = this.portfolioRunService.getPortfolioRunLastByMOC(mainClientAccount.getId(), DateUtils.toDate(balanceDate));
				}
				ruleViolationList.addAll(this.ruleEvaluatorService.previewRuleViolations("PortfolioRun", run.getId()));
				validateTestMethodResult(testMethodName);
			}
		}
	}
}
