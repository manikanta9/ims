package com.clifton.ims.tests.rule.security;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AnalystRoleRuleSecurityTests extends SecurityRuleTests {

	private static final Logger log = LoggerFactory.getLogger(AnalystRoleRuleSecurityTests.class);


	@Override
	public void switchUser() {
		//switch to non-admin test user with 'Analysts' security group access for testing
		String username = ImsTestProperties.TEST_ANALYST_USER;
		String password = ImsTestProperties.TEST_ANALYST_USER_PASSWORD;
		this.userUtils.switchUser(username, password);
	}


	@Override
	public void testSaveTradeViolation() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveTradeViolation();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [WRITE] permission(s) to [TradingRules] security resource.].]");
	}


	@Test
	public void testIgnoreViolation() {
		InvestmentAccount mainClientAccount = this.investmentAccountUtils.getClientAccountByNumber("662950");
		if (mainClientAccount != null) {
			PortfolioRun run = this.portfolioRunService.getPortfolioRunByMainRun(mainClientAccount.getId(), DateUtils.toDate("08/11/2015"));
			RuleViolationSearchForm ruleViolationSearchForm = new RuleViolationSearchForm();
			ruleViolationSearchForm.setLinkedFkFieldId(BeanUtils.getIdentityAsLong(run));
			ruleViolationSearchForm.setRuleDefinitionName("Manager Account Balance: Manually Adjusted");
			ruleViolationSearchForm.setLimit(1);
			RuleViolation ruleViolation = CollectionUtils.getFirstElement(this.ruleViolationService.getRuleViolationList(ruleViolationSearchForm));
			if (ruleViolation != null) {
				ruleViolation.setIgnored(!ruleViolation.isIgnored());
				this.ruleViolationService.saveRuleViolation(ruleViolation);
			}
		}
	}


	@Override
	public void testSaveAndDeleteTradeRuleAssignment() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveAndDeleteTradeRuleAssignment();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [CREATE] permission(s) to [Trade] security resource.].]");
	}


	@Override
	public void testSaveAndDeleteTradeRealTimeRuleAssignment() {
		TestUtils.expectException(ImsErrorResponseException.class, () -> {
			super.testSaveAndDeleteTradeRealTimeRuleAssignment();
		}, "[Unexpected Error Occurred: [Access Denied. You do not have [CREATE] permission(s) to [Trade] security resource.].]");
	}
}
