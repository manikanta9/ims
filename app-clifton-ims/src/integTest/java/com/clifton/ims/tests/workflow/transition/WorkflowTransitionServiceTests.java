package com.clifton.ims.tests.workflow.transition;

import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.util.templates.ImsTestProperties;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * WorkflowTransitionServiceTests tests methods in WorkflowTransitionService
 * <p>
 * Note: Tests utilize Client Relationship Status Workflow as this is the most "simplistic" workflow we have now
 *
 * @author manderson
 */

public class WorkflowTransitionServiceTests extends BaseImsIntegrationTest {


	@Resource
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testWorkflowTransitionWriteAccessRequired_Allowed() {
		// First - Create a New BusinessClientRelationship object
		BusinessClientRelationship relationship = this.businessUtils.createBusinessClientRelationship();

		// Make sure it's WRITE ACCESS
		WorkflowTransition transition = updateWorkflowTransitionReadAccess(BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), "Active", false);

		// THEN GIVE USER WRITE ACCESS
		switchUserAccess(ImsTestProperties.TEST_USER_2, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, false);

		// TRANSITION Relationship to Active
		executeTransitionAsUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), transition.getId(), null);
	}


	@Test
	public void testWorkflowTransitionWriteAccessRequired_Denied() {
		// First - Create a New BusinessClientRelationship object
		BusinessClientRelationship relationship = this.businessUtils.createBusinessClientRelationship();

		// Make sure it's WRITE ACCESS
		WorkflowTransition transition = updateWorkflowTransitionReadAccess(BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), "Active", false);

		// THEN GIVE USER READ ACCESS
		switchUserAccess(ImsTestProperties.TEST_USER_2, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, true);

		// TRANSITION Relationship to Active
		String expectedErrorMessage = "Access Denied. You do not have [WRITE] permission(s) to [BusinessClientRelationship] security resource.";
		executeTransitionAsUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), transition.getId(), expectedErrorMessage);
	}


	@Test
	public void testWorkflowTransitionReadAccessRequired_Allowed() {
		// First - Create a New BusinessClientRelationship object
		BusinessClientRelationship relationship = this.businessUtils.createBusinessClientRelationship();

		// Make sure it's READ ACCESS
		WorkflowTransition transition = updateWorkflowTransitionReadAccess(BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), "Active", true);

		// THEN GIVE USER READ ACCESS
		switchUserAccess(ImsTestProperties.TEST_USER_2, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, true);

		// TRANSITION Relationship to Active
		executeTransitionAsUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), transition.getId(), null);
	}


	@Test
	public void testWorkflowTransitionReadAccessRequired_Denied() {
		// First - Create a New BusinessClientRelationship object
		BusinessClientRelationship relationship = this.businessUtils.createBusinessClientRelationship();

		// Make sure it's READ ACCESS
		WorkflowTransition transition = updateWorkflowTransitionReadAccess(BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), "Active", true);

		// THEN REMOVE ALL USER ACCESS
		this.userUtils.removeAllPermissionsForUser(ImsTestProperties.TEST_USER_2);

		// TRANSITION Relationship to Active
		String expectedErrorMessage = "Access Denied. You do not have [READ] permission(s) to [BusinessClientRelationship] security resource.";
		executeTransitionAsUser(ImsTestProperties.TEST_USER_2, ImsTestProperties.TEST_USER_2_PASSWORD, BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, relationship.getId(), transition.getId(), expectedErrorMessage);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private WorkflowTransition updateWorkflowTransitionReadAccess(String tableName, int entityId, String endStateName, boolean readAccessIsSufficient) {
		WorkflowTransition updateTransition = null;
		List<WorkflowTransition> transitionList = this.workflowTransitionService.getWorkflowTransitionNextListByEntity(tableName, MathUtils.getNumberAsLong(entityId));
		for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
			if (transition.getEndWorkflowState().getName().equals(endStateName)) {
				updateTransition = transition;
				break;
			}
		}
		if (updateTransition != null) {
			updateWorkflowTransitionReadAccess(updateTransition, readAccessIsSufficient);
		}
		else {
			throw new IllegalStateException("Cannot find valid transition to " + endStateName);
		}
		return updateTransition;
	}


	private void updateWorkflowTransitionReadAccess(WorkflowTransition transition, boolean readAccessIsSufficient) {
		if (transition.isReadAccessSufficientToExecute() != readAccessIsSufficient) {
			// If we need to save it - read it again to ensure action lists are on the bean for saving
			transition = this.workflowTransitionService.getWorkflowTransition(transition.getId());
			transition.setReadAccessSufficientToExecute(readAccessIsSufficient);
			this.workflowTransitionService.saveWorkflowTransition(transition);
		}
	}


	private void switchUserAccess(String userName, String tableName, boolean readOnly) {
		this.userUtils.removeAllPermissionsForUser(userName);
		if (readOnly) {
			this.userUtils.addPermissionReadOnlyToUser(userName, tableName);
		}
		else {
			this.userUtils.addPermissionToUser(userName, tableName);
		}
	}


	/**
	 * Attempt to execute transition as user, but always switch back to admin user when done
	 */
	private void executeTransitionAsUser(String userName, String password, String tableName, int entityId, int transitionId, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			this.userUtils.switchUser(userName, password);
			this.workflowTransitionService.executeWorkflowTransitionByTransition(tableName, entityId, transitionId);
		}
		catch (ImsErrorResponseException e) {
			errorMessage = e.getErrorMessageFromIMS();
		}
		finally {
			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
		}
		if (expectedErrorMessage == null) {
			Assertions.assertNull(errorMessage, "Did not expect error message, but was: " + errorMessage);
		}
		else {
			Assertions.assertTrue(errorMessage != null && errorMessage.contains(expectedErrorMessage), "Expected error message to end with: " + expectedErrorMessage + ", but was " + errorMessage);
		}
	}
}
