package com.clifton.ims.tests.investment.instruction;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.core.email.FakeSMTPServer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.integration.export.ExportFtpDestinationBuilder;
import com.clifton.ims.tests.integration.export.email.FakeSMTPServerUtils;
import com.clifton.ims.tests.integration.export.ftp.FakeFtpServerUtils;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionSendingService;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.run.InvestmentInstructionRun;
import com.clifton.investment.instruction.run.InvestmentInstructionRunService;
import com.clifton.investment.instruction.run.search.InvestmentInstructionRunSearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionContactGroup;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.test.util.RandomUtils;
import net.kemitix.wiser.assertions.WiserAssertions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.subethamail.wiser.WiserMessage;

import javax.annotation.Resource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;


@Disabled
public class InvestmentInstructionTests extends BaseImsIntegrationTest {


	@Resource
	private BusinessContactService businessContactService;
	@Resource
	private ExportDefinitionService exportDefinitionService;
	@Resource
	private InvestmentInstructionRunService investmentInstructionRunService;
	@Resource
	private InvestmentInstructionSendingService investmentInstructionSendingService;
	@Resource
	private InvestmentInstructionService investmentInstructionService;
	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;
	@Resource
	private InvestmentInstructionStatusService investmentInstructionStatusService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private SystemConditionService systemConditionService;
	@Resource
	private FakeSMTPServerUtils fakeSMTPServerUtils;
	@Resource
	private FakeFtpServerUtils fakeFtpServerUtils;

	//SMTP SERVER SETTINGS
	private static final Integer SMTP_PORT = 2600;
	private static final Integer SMTP_POLL_SLEEP_TIME = 1000;
	private static final Integer SMTP_POLL_LIMIT_COUNT = 60;

	//FTP SERVER SETTINGS
	//Ports under 1000 cannot be bound to by non-root users in linux
	private static final Integer FTP_CONTROL_PORT = 3021;
	private static final String FTP_USERNAME = "testUsername";
	private static final String FTP_PASSWORD = "testPassword";
	private static final String FTP_REMOTE_FOLDER = "/";
	private static final Integer FTP_POLL_SLEEP_TIME = 1000;
	private static final Integer FTP_POLL_LIMIT_COUNT = 60;

	private static final Integer INSTRUCTION_STATUS_POLL_COUNT = 10;
	private static final Integer INSTRUCTION_STATUS_POLL_SLEEP_TIME = 2;


	private static final String DEFAULT_FROM_EMAIL = "TCGOps@paraport.com";

	private static final String RECIPIENT_CONTACT_A_EMAIL = "tzwieg@paraport.com";
	private static final String RECIPIENT_CONTACT_B_EMAIL = "mwacker@paraport.com";

	//Can be reused across tests
	private BusinessContact contactA;
	private BusinessContact contactB;


	/**
	 * OBJECTIVES
	 * 1. Send instruction to Test Contact A
	 */
	@Test
	public void testSendInvestmentInstructionEmail() throws UnknownHostException, InterruptedException {
		Integer smtpPort = SMTP_PORT;
		FakeSMTPServer emailServer = new FakeSMTPServer(smtpPort);
		emailServer.start();
		try {
			int instructionId = 25047;
			//Set instruction and items to OPEN so they can be sent again
			InvestmentInstruction instruction = resetInvestmentInstructionToOpen(instructionId);

			//Set the instruction definition to use the test contact group
			InvestmentInstructionDefinition definition = instruction.getDefinition();
			definition.setContactGroup(createInvestmentInstructionContactGroup());
			definition.setMessageSubjectContent("Test " + definition.getContactGroup().getName());
			definition.setFromContact(getBusinessContactByEmail("tcgops@paraport.com"));
			this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(definition);

			InvestmentInstructionContact contact = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactA())
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();

			this.investmentInstructionSendingService.sendInvestmentInstruction(instructionId, null);

			Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(emailServer, SMTP_POLL_LIMIT_COUNT, SMTP_POLL_SLEEP_TIME));

			WiserAssertions.assertReceivedMessage(emailServer)
					.from(DEFAULT_FROM_EMAIL)
					.to(RECIPIENT_CONTACT_A_EMAIL);

			validateFinalInstructionStatus(instructionId, this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
		}
		finally {
			emailServer.stop();
		}
	}


	/**
	 * OBJECTIVES
	 * 1. Send instruction to Test Contact A and Test Contact B
	 */
	@Test
	public void testSendInvestmentInstructionEmailMultiple() throws UnknownHostException, InterruptedException {
		Integer smtpPort = SMTP_PORT + 1;
		FakeSMTPServer emailServer = new FakeSMTPServer(smtpPort);
		emailServer.start();
		try {
			int instructionId = 25051;
			//Set instruction and items to OPEN so they can be sent again
			InvestmentInstruction instruction = resetInvestmentInstructionToOpen(instructionId);

			//Set the instruction definition to use the test contact group
			InvestmentInstructionDefinition definition = instruction.getDefinition();
			definition.setContactGroup(createInvestmentInstructionContactGroup());
			definition.setMessageSubjectContent("Test " + definition.getContactGroup().getName());
			definition.setFromContact(getBusinessContactByEmail("tcgops@paraport.com"));
			this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(definition);

			InvestmentInstructionContact contactA = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactA())
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();

			InvestmentInstructionContact contactB = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactB())
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();

			this.investmentInstructionSendingService.sendInvestmentInstruction(instructionId, null);

			Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(emailServer, SMTP_POLL_LIMIT_COUNT, SMTP_POLL_SLEEP_TIME));

			WiserAssertions.assertReceivedMessage(emailServer)
					.from(DEFAULT_FROM_EMAIL)
					.to(RECIPIENT_CONTACT_A_EMAIL)
					.to(RECIPIENT_CONTACT_B_EMAIL);

			validateFinalInstructionStatus(instructionId, this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
		}
		finally {
			emailServer.stop();
		}
	}


	/**
	 * OBJECTIVES
	 * 1. Ensure Instruction is only sent to Contact A - Contact B will have an end date prior to today
	 */
	@Test
	public void testSendInvestmentInstructionEmailWithExpiredContact() throws UnknownHostException, InterruptedException {
		Integer smtpPort = SMTP_PORT + 2;
		FakeSMTPServer emailServer = new FakeSMTPServer(smtpPort);
		emailServer.start();
		try {
			int instructionId = 25061;
			//Set instruction and items to OPEN so they can be sent again
			InvestmentInstruction instruction = resetInvestmentInstructionToOpen(instructionId);

			//Set the instruction definition to use the test contact group
			InvestmentInstructionDefinition definition = instruction.getDefinition();
			definition.setContactGroup(createInvestmentInstructionContactGroup());
			definition.setMessageSubjectContent("Test " + definition.getContactGroup().getName());
			definition.setFromContact(getBusinessContactByEmail("tcgops@paraport.com"));
			this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(definition);

			InvestmentInstructionContact contactA = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactA())
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();
			InvestmentInstructionContact contactB = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactB())
					.startDate(instruction.getInstructionDate())
					.endDate(DateUtils.addDays(new Date(), -2))
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();

			this.investmentInstructionSendingService.sendInvestmentInstruction(instructionId, null);

			Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(emailServer, SMTP_POLL_LIMIT_COUNT, SMTP_POLL_SLEEP_TIME));

			WiserAssertions.assertReceivedMessage(emailServer)
					.from(DEFAULT_FROM_EMAIL)
					.to(RECIPIENT_CONTACT_A_EMAIL);

			List<WiserMessage> messages = emailServer.getMessages();
			for (WiserMessage message : messages) {
				AssertUtils.assertFalse(StringUtils.isEqual(RECIPIENT_CONTACT_B_EMAIL, message.getEnvelopeReceiver()), "Contact B was sent the instruction even though it expired on " + DateUtils.addDays(new Date(), -2));
			}

			validateFinalInstructionStatus(instructionId, this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
		}
		finally {
			emailServer.stop();
		}
	}


	@Test
	public void testSendInvestmentInstructionFtp() throws UnknownHostException, InterruptedException {
		FakeFtpServer fakeFtpServer = this.fakeFtpServerUtils.createAndInitializeFakeFtpServer(FTP_USERNAME, FTP_PASSWORD, FTP_REMOTE_FOLDER);
		fakeFtpServer.setServerControlPort(FTP_CONTROL_PORT);
		fakeFtpServer.start();
		try {
			testSendInvestmentInstructionFtpOnly(fakeFtpServer);
			testSendInvestmentInstructionFtpAndEmail(fakeFtpServer);
			testSendInvestmentInstructionFtpAndEmailWithFailure(fakeFtpServer);
		}
		finally {
			fakeFtpServer.stop();
		}
	}


	/**
	 * Test sending a single instruction to a FTP destination
	 */
	private void testSendInvestmentInstructionFtpOnly(FakeFtpServer fakeFtpServer) throws UnknownHostException, InterruptedException {
		int instructionId = 25071;
		//Set instruction and items to OPEN so they can be sent again
		InvestmentInstruction instruction = resetInvestmentInstructionToOpen(instructionId);

		//Set the instruction definition to use the test contact group
		InvestmentInstructionDefinition definition = instruction.getDefinition();
		definition.setContactGroup(createInvestmentInstructionContactGroup());
		definition.setMessageSubjectContent("Test " + definition.getContactGroup().getName());
		definition.setFromContact(getBusinessContactByEmail("tcgops@paraport.com"));
		this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(definition);

		SystemCondition sendCondition = this.systemConditionService.getSystemConditionByName("Always True Condition");

		ExportDefinitionDestination ftpDestination = new ExportFtpDestinationBuilder(InetAddress.getLocalHost().getHostName(), this.systemBeanService, this.exportDefinitionService)
				.destinationName("Investment Instruction FTP Destination - " + RandomUtils.randomNumber())
				.sharedDestination(true)
				.sendCondition(sendCondition)
				.ftpRemoteFolder(FTP_REMOTE_FOLDER)
				.ftpProtocol("FTP")
				.ftpUsername(FTP_USERNAME)
				.ftpPassword(FTP_PASSWORD)
				.build();

		InvestmentInstructionContact contact = new InvestmentInstructionContactBuilder("FTP", this.systemBeanService, this.investmentInstructionSetupService)
				.sharedFtpDestination(ftpDestination)
				.startDate(instruction.getInstructionDate())
				.contactGroup(instruction.getDefinition().getContactGroup())
				.company(instruction.getRecipientCompany())
				.build();

		this.investmentInstructionSendingService.sendInvestmentInstruction(instructionId, null);

		Assertions.assertTrue(this.fakeFtpServerUtils.serverHasFiles(fakeFtpServer, FTP_REMOTE_FOLDER, FTP_POLL_LIMIT_COUNT, FTP_POLL_SLEEP_TIME));

		@SuppressWarnings({"cast", "unchecked"})
		List<FileEntry> ftpFiles = (List<FileEntry>) fakeFtpServer.getFileSystem().listFiles(FTP_REMOTE_FOLDER);
		Assertions.assertEquals(1, CollectionUtils.getSize(ftpFiles));

		this.fakeFtpServerUtils.clearFilesFromFolder(fakeFtpServer, FTP_REMOTE_FOLDER);
	}


	/**
	 * Test sending a single instruction to both an Email and FTP destination
	 */
	private void testSendInvestmentInstructionFtpAndEmail(FakeFtpServer fakeFtpServer) throws UnknownHostException, InterruptedException {
		Integer smtpPort = SMTP_PORT + 3;
		FakeSMTPServer emailServer = new FakeSMTPServer(smtpPort);
		emailServer.start();
		try {
			int instructionId = 25043;
			//Set instruction and items to OPEN so they can be sent again
			InvestmentInstruction instruction = resetInvestmentInstructionToOpen(instructionId);

			//Set the instruction definition to use the test contact group
			InvestmentInstructionDefinition definition = instruction.getDefinition();
			definition.setContactGroup(createInvestmentInstructionContactGroup());
			definition.setMessageSubjectContent("Test " + definition.getContactGroup().getName());
			definition.setFromContact(getBusinessContactByEmail("tcgops@paraport.com"));
			this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(definition);

			InvestmentInstructionContact emailContact = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactA())
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();

			SystemCondition sendCondition = this.systemConditionService.getSystemConditionByName("Always True Condition");

			ExportDefinitionDestination ftpDestination = new ExportFtpDestinationBuilder(InetAddress.getLocalHost().getHostName(), this.systemBeanService, this.exportDefinitionService)
					.destinationName("Investment Instruction FTP Destination - " + RandomUtils.randomNumber())
					.sharedDestination(true)
					.sendCondition(sendCondition)
					.ftpRemoteFolder(FTP_REMOTE_FOLDER)
					.ftpProtocol("FTP")
					.ftpUsername(FTP_USERNAME)
					.ftpPassword(FTP_PASSWORD)
					.build();

			InvestmentInstructionContact ftpContact = new InvestmentInstructionContactBuilder("FTP", this.systemBeanService, this.investmentInstructionSetupService)
					.sharedFtpDestination(ftpDestination)
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.build();


			this.investmentInstructionSendingService.sendInvestmentInstruction(instructionId, null);

			Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(emailServer, SMTP_POLL_LIMIT_COUNT, SMTP_POLL_SLEEP_TIME));
			Assertions.assertTrue(this.fakeFtpServerUtils.serverHasFiles(fakeFtpServer, FTP_REMOTE_FOLDER, FTP_POLL_LIMIT_COUNT, FTP_POLL_SLEEP_TIME));

			@SuppressWarnings({"cast", "unchecked"})
			List<FileEntry> ftpFiles = (List<FileEntry>) fakeFtpServer.getFileSystem().listFiles(FTP_REMOTE_FOLDER);
			Assertions.assertEquals(1, CollectionUtils.getSize(ftpFiles));

			WiserAssertions.assertReceivedMessage(emailServer)
					.from(DEFAULT_FROM_EMAIL)
					.to(RECIPIENT_CONTACT_A_EMAIL);

			validateFinalInstructionStatus(instructionId, this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.COMPLETED));
			this.fakeFtpServerUtils.clearFilesFromFolder(fakeFtpServer, FTP_REMOTE_FOLDER);
		}
		finally {
			emailServer.stop();
		}
	}


	/**
	 * Test sending a single instruction to both an Email and FTP destination with one failure
	 */
	private void testSendInvestmentInstructionFtpAndEmailWithFailure(FakeFtpServer fakeFtpServer) throws UnknownHostException, InterruptedException {
		Integer smtpPort = SMTP_PORT + 4;
		FakeSMTPServer emailServer = new FakeSMTPServer(smtpPort);
		emailServer.start();
		try {
			int instructionId = 25034;
			//Set instruction and items to OPEN so they can be sent again
			InvestmentInstruction instruction = resetInvestmentInstructionToOpen(instructionId);

			//Set the instruction definition to use the test contact group
			InvestmentInstructionDefinition definition = instruction.getDefinition();
			definition.setContactGroup(createInvestmentInstructionContactGroup());
			definition.setMessageSubjectContent("Test " + definition.getContactGroup().getName());
			//definition.setFromContact(getBusinessContactByEmail("tcgops@paraport.com"));
			this.investmentInstructionSetupService.saveInvestmentInstructionDefinition(definition);

			SystemCondition sendCondition = this.systemConditionService.getSystemConditionByName("Always True Condition");

			ExportDefinitionDestination ftpDestination = new ExportFtpDestinationBuilder(InetAddress.getLocalHost().getHostName(), this.systemBeanService, this.exportDefinitionService)
					.destinationName("Investment Instruction FTP Destination - " + RandomUtils.randomNumber())
					.sharedDestination(true)
					.sendCondition(sendCondition)
					.ftpRemoteFolder(FTP_REMOTE_FOLDER)
					.ftpProtocol("FTP")
					.ftpUsername(FTP_USERNAME)
					.ftpPassword("1234")
					.build();

			InvestmentInstructionContact ftpContact = new InvestmentInstructionContactBuilder("FTP", this.systemBeanService, this.investmentInstructionSetupService)
					.sharedFtpDestination(ftpDestination)
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.build();

			InvestmentInstructionContact emailContact = new InvestmentInstructionContactBuilder("Email", this.systemBeanService, this.investmentInstructionSetupService)
					.contact(getContactA())
					.startDate(instruction.getInstructionDate())
					.contactGroup(instruction.getDefinition().getContactGroup())
					.company(instruction.getRecipientCompany())
					.hostname(InetAddress.getLocalHost().getHostName())
					.port(smtpPort)
					.build();


			this.investmentInstructionSendingService.sendInvestmentInstruction(instructionId, null);

			Assertions.assertTrue(this.fakeSMTPServerUtils.serverHasMessages(emailServer, SMTP_POLL_LIMIT_COUNT, SMTP_POLL_SLEEP_TIME));
			this.fakeFtpServerUtils.serverHasFiles(fakeFtpServer, FTP_REMOTE_FOLDER, FTP_POLL_LIMIT_COUNT, FTP_POLL_SLEEP_TIME);

			WiserAssertions.assertReceivedMessage(emailServer)
					.from(DEFAULT_FROM_EMAIL)
					.to(RECIPIENT_CONTACT_A_EMAIL);

			validateFinalInstructionStatus(instructionId, this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR));
			this.fakeFtpServerUtils.clearFilesFromFolder(fakeFtpServer, FTP_REMOTE_FOLDER);
		}
		finally {
			emailServer.stop();
		}
	}


	////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////                                    HELPERS                                   ///////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////


	private void validateFinalInstructionStatus(int instructionId, InvestmentInstructionStatus anticipatedStatus) throws InterruptedException {
		InvestmentInstruction instruction = this.investmentInstructionService.getInvestmentInstruction(instructionId);
		for (int counter = 0; counter < INSTRUCTION_STATUS_POLL_COUNT; counter++) {
			instruction = this.investmentInstructionService.getInvestmentInstruction(instructionId);
			if (instruction.getStatus().compareStatusCode(anticipatedStatus) != 0) {
				Thread.sleep(INSTRUCTION_STATUS_POLL_SLEEP_TIME);
			}
			else {
				break;
			}
		}
		AssertUtils.assertTrue(instruction.getStatus().compareStatusCode(anticipatedStatus) == 0, "Instruction not " + anticipatedStatus.getName() + ". Status is currently " + instruction.getStatus().getInvestmentInstructionStatusName());
	}


	public InvestmentInstruction resetInvestmentInstructionToOpen(int instructionId) {
		InvestmentInstruction instruction = this.investmentInstructionService.getInvestmentInstruction(instructionId);
		AssertUtils.assertNotNull(instruction, "Instruction id: " + instructionId + " not found");
		//Set all of the item statuses to OPEN
		for (InvestmentInstructionItem item : CollectionUtils.getIterable(instruction.getItemList())) {
			item.setStatus(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN));
			this.investmentInstructionService.saveInvestmentInstructionItem(item);
		}
		instruction.setStatus(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN));
		this.investmentInstructionService.saveInvestmentInstruction(instruction, false);

		//Set all the old run statuses to ERROR (To keep track of them and still allow new runs to be ran)
		InvestmentInstructionRunSearchForm runSearchForm = new InvestmentInstructionRunSearchForm();
		runSearchForm.setInstructionId(instructionId);
		List<InvestmentInstructionRun> oldRuns = this.investmentInstructionRunService.getInvestmentInstructionRunList(runSearchForm);
		for (InvestmentInstructionRun run : oldRuns) {
			run.setRunStatus(this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.ERROR));
			this.investmentInstructionRunService.saveInvestmentInstructionRun(run);
		}

		return instruction;
	}


	public InvestmentInstructionContactGroup createInvestmentInstructionContactGroup() {
		InvestmentInstructionContactGroup group = new InvestmentInstructionContactGroup();
		group.setName("Test Group - " + RandomUtils.randomNumber());
		this.investmentInstructionSetupService.saveInvestmentInstructionContactGroup(group);
		return group;
	}


	private BusinessContact getBusinessContactByEmail(String email) {
		BusinessContactSearchForm businessContactSearchForm = new BusinessContactSearchForm();
		businessContactSearchForm.setEmailAddress(email);
		return CollectionUtils.getFirstElementStrict(this.businessContactService.getBusinessContactList(businessContactSearchForm));
	}


	//TODO: replace with optionals?
	private BusinessContact getContactA() {
		return this.contactA == null ? this.contactA = getBusinessContactByEmail(RECIPIENT_CONTACT_A_EMAIL) : this.contactA;
	}


	//TODO: replace with optionals?
	private BusinessContact getContactB() {
		return this.contactB == null ? this.contactB = getBusinessContactByEmail(RECIPIENT_CONTACT_B_EMAIL) : this.contactB;
	}
}
