package com.clifton.ims.tests.system.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.system.search.SystemSearchSearchForm;
import com.clifton.system.search.SystemSearchService;
import com.clifton.system.search.provider.search.SystemSearchResult;
import com.clifton.system.search.setup.SystemSearchArea;
import com.clifton.system.search.setup.SystemSearchAreaEntity;
import com.clifton.system.search.setup.SystemSearchSetupService;
import com.clifton.system.search.setup.search.SystemSearchAnalyzerSearchForm;
import com.clifton.system.search.setup.search.SystemSearchAreaSearchForm;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


public class SystemSearchTests extends BaseImsIntegrationTest {

	@Resource
	SystemSearchService systemSearchService;

	@Resource
	SystemSearchSetupService systemSearchSetupService;


	@Test
	public void testSearchAreaEntityCache() {
		String randomNameNumber = RandomUtils.randomNameAndNumber();
		System.out.println(randomNameNumber);
		//Search for "Search for this random string", and nothing should be returned
		validateSearchQueryResultWithDepth(randomNameNumber, "", 1, true);

		//Create Association to [randomNameNumber]
		//Get Client Relationship Search Area
		SystemSearchAreaSearchForm searchForm = new SystemSearchAreaSearchForm();
		searchForm.setName("Client Relationships");
		SystemSearchArea clientRelationshipArea = CollectionUtils.getOnlyElementStrict(this.systemSearchSetupService.getSystemSearchAreaList(searchForm));

		//Load the caches to make sure we can reproduce the cache issue
		this.systemSearchService.reindexSystemSearchArea(clientRelationshipArea.getId());

		//Get the Exact Match Caseless search analyzer
		SystemSearchAnalyzerSearchForm searchAnalyzerSearchForm = new SystemSearchAnalyzerSearchForm();
		searchAnalyzerSearchForm.setName("Exact Match Caseless");

		//Build our new search area entity relationship
		SystemSearchAreaEntity searchAreaEntity = new SystemSearchAreaEntity();
		searchAreaEntity.setSystemSearchArea(clientRelationshipArea);
		searchAreaEntity.setSystemSearchAnalyzer(CollectionUtils.getOnlyElement(this.systemSearchSetupService.getSystemSearchAnalyzerList(searchAnalyzerSearchForm)));
		searchAreaEntity.setFkFieldId(141); //University of St. Thomas
		searchAreaEntity.setSearchRank(100);
		searchAreaEntity.setSearchValue(randomNameNumber);
		searchAreaEntity.setName("University of Saint Thomas");

		//save new area entity
		this.systemSearchSetupService.saveSystemSearchAreaEntity(searchAreaEntity);

		//Reindex to pick up the new area entity changes
		this.systemSearchService.reindexSystemSearchArea(clientRelationshipArea.getId());

		//This should pull back "University of St. Thomas" now
		validateSearchQueryExactResult(randomNameNumber, "University of St. Thomas");
	}


	@Test
	public void testBasicSystemSearch() {
		validateSearchQueryExactResult("SPXW US 08/29/14 P1850", "SPXW US 08/29/14 P1850 [INACTIVE]");
		validateSearchQueryExactResult("AAPL", "AAPL (Apple Inc)");
		validateSearchQueryExactResult("SPX FLEX 06/09/15 P1780.12", "SPX FLEX 06/09/15 P1780.12 (SPX FLEX 06/09/15 1780.12 Put) [INACTIVE]");

		validateSearchQueryExactResult("90 DAY T-BILL", "90 Day T-Bill");
		validateSearchQueryExactResult("90 t-bill", "90 Day T-Bill");
		validateSearchQueryExactResult("90 day bill", "90 Day T-Bill");

		validateSearchQueryExactResult("Saint Ignatius College Prep", "St. Ignatius College Prep");
		validateSearchQueryExactResult("Saint Olaf College", "St. Olaf College");

		validateSearchQueryExactResult("Saint Paul Teacher's Retirement", "St. Paul Teacher's Retirement");
		validateSearchQueryExactResult("Saint Luke's Health System", "St. Luke's Health System");

		validateSearchQueryExactResult("The Saint Louis Archdiocesan Fund", "The St. Louis Archdiocesan Fund");
		validateSearchQueryExactResult("University of Saint Thomas", "University of St. Thomas");
		validateSearchQueryExactResult("Washington University in Saint Louis", "Washington University in St. Louis");

		validateSearchQueryExactResult("jobs", "Batch Jobs Window");
	}


	////////////////////////////////////////////////////////////////////////////


	/**
	 * Given a search string determine if it produced the expected label result as the first result in the list.
	 *
	 * @param searchQuery
	 * @param expectedLabelResult
	 */
	private void validateSearchQueryExactResult(String searchQuery, String expectedLabelResult) {
		validateSearchQueryResultWithDepth(searchQuery, expectedLabelResult, 1, false);
	}


	/**
	 * Given a search string determine if it produced the expected label result within the max depth of the returned
	 * list of search results.
	 *
	 * @param searchQuery
	 * @param expectedLabelResult
	 * @param maxDepth
	 */
	private void validateSearchQueryResultWithDepth(String searchQuery, String expectedLabelResult, int maxDepth, boolean emptyResult) {
		boolean pass = false;

		SystemSearchSearchForm searchForm = new SystemSearchSearchForm();
		searchForm.setSearchPattern(searchQuery);
		searchForm.setLimit(100);
		List<SystemSearchResult> systemSearchResultList = this.systemSearchService.getSystemSearchList(searchForm);

		if (emptyResult && CollectionUtils.isEmpty(systemSearchResultList)) {
			return;
		}

		int cnt = 0;
		while (cnt < CollectionUtils.getSize(systemSearchResultList) && cnt < maxDepth) {
			if (StringUtils.isEqual(expectedLabelResult, systemSearchResultList.get(cnt).getLabel())) {
				pass = true;
				break;
			}

			cnt++;
		}

		ValidationUtils.assertTrue(pass, "Query [" + searchQuery + "] did not find [" + expectedLabelResult + "] within the expected depth [" + maxDepth + "]. Found: [" + StringUtils.collectionToCommaDelimitedString(CollectionUtils.createList(BeanUtils.getPropertyValues(systemSearchResultList, SystemSearchResult::getLabel))) + "]");
	}
}
