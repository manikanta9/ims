package com.clifton.ims.tests.performance.account.process;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Function;


/**
 * @author manderson
 */
public abstract class BasePerformanceInvestmentAccountProcessServiceTests extends BaseImsIntegrationTest {


	@Resource
	private PerformanceInvestmentAccountService performanceInvestmentAccountService;

	@Resource
	private PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////////
	/////////////                     Helper Methods                   /////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void rebuildSnapshotsForSummary(PerformanceInvestmentAccount performanceInvestmentAccount) {
		// Do Last Day of Previous Month Separately - Only checks if snapshots exist on start date, so when rebuilding from month to month previous month prevents current month from rebuilding
		this.accountingUtils.rebuildAccountingPositionDailySnapshot(performanceInvestmentAccount.getClientAccount().getId(), DateUtils.addDays(performanceInvestmentAccount.getAccountingPeriod().getStartDate(), -1), DateUtils.addDays(performanceInvestmentAccount.getAccountingPeriod().getStartDate(), -1));
		this.accountingUtils.rebuildAccountingPositionDailySnapshot(performanceInvestmentAccount.getClientAccount().getId(), performanceInvestmentAccount.getAccountingPeriod().getStartDate(), DateUtils.getNextWeekday(performanceInvestmentAccount.getAccountingPeriod().getEndDate()));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected PerformanceInvestmentAccount previewPerformanceInvestmentAccount(int id) {
		return this.performanceInvestmentAccountProcessService.previewPerformanceInvestmentAccount(id);
	}


	protected PerformanceInvestmentAccount recreatePerformanceInvestmentAccount(PerformanceInvestmentAccount performanceInvestmentAccount) {
		if (WorkflowStatus.STATUS_CLOSED.equals(performanceInvestmentAccount.getWorkflowStatus().getName())) {
			// transition existing PSum to open for deletion
			Workflow pSumWorkflow = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.workflowDefinitionService.getWorkflowByName("Performance Summary Status"), 2);
			WorkflowState openState = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.workflowDefinitionService.getWorkflowStateByName(pSumWorkflow.getId(), "Approved By Accounting"), 2);
			RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.workflowTransitionService.executeWorkflowTransition("PerformanceInvestmentAccount", performanceInvestmentAccount.getId().longValue(), openState.getId()), 2);
		}
		this.performanceInvestmentAccountProcessService.deletePerformanceInvestmentAccount(performanceInvestmentAccount.getId());
		PerformanceInvestmentAccount newPerformanceInvestmentAccount = new PerformanceInvestmentAccount();
		newPerformanceInvestmentAccount.setClientAccount(performanceInvestmentAccount.getClientAccount());
		newPerformanceInvestmentAccount.setAccountingPeriod(performanceInvestmentAccount.getAccountingPeriod());
		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.performanceInvestmentAccountService.savePerformanceInvestmentAccount(newPerformanceInvestmentAccount), 2);
		this.performanceInvestmentAccountProcessService.processPerformanceInvestmentAccount(newPerformanceInvestmentAccount.getId());
		return this.performanceInvestmentAccountService.getPerformanceInvestmentAccount(newPerformanceInvestmentAccount.getId());
	}


	protected PerformanceInvestmentAccount previewPerformanceInvestmentAccountHistoricalOnly(int id) {
		return this.performanceInvestmentAccountProcessService.previewPerformanceInvestmentAccountHistoricalOnly(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected PerformanceInvestmentAccount getPerformanceInvestmentAccount(String accountNumber, Date date) {
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setClientAccountId(getClientAccountIdByNumber(accountNumber));
		searchForm.setMeasureDate(date);
		return CollectionUtils.getOnlyElement(RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> getPerformanceInvestmentAccountList(searchForm), 3));
	}


	private Integer getClientAccountIdByNumber(String clientAccountNumber) {
		return RequestOverrideHandler.serviceCallWithOverrides(() -> this.investmentAccountUtils.getClientAccountByNumber(clientAccountNumber).getId(),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_PROPERTIES_ROOT_PARAMETER, "data"),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_PROPERTIES_PARAMETER, "id")
		);
	}


	protected List<PerformanceInvestmentAccount> getPerformanceInvestmentAccountList(PerformanceInvestmentAccountSearchForm searchForm) {
		return this.performanceInvestmentAccountService.getPerformanceInvestmentAccountList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void validateSummaryPropertiesEqual(PerformanceInvestmentAccount originalBean, PerformanceInvestmentAccount newBean) {
		validateSummaryPropertiesEqual(originalBean, newBean, null);
	}


	protected void validateSummaryPropertiesEqual(PerformanceInvestmentAccount originalBean, PerformanceInvestmentAccount newBean, BigDecimal accountThreshold) {
		StringBuilder errorBuilder = new StringBuilder();
		validateSummaryPropertyEqual("monthToDateReturn", originalBean, newBean, PerformanceInvestmentAccount::getMonthToDateReturn, accountThreshold, errorBuilder);
		validateSummaryPropertyEqual("quarterToDateReturn", originalBean, newBean, PerformanceInvestmentAccount::getQuarterToDateReturn, accountThreshold, errorBuilder);
		validateSummaryPropertyEqual("yearToDateReturn", originalBean, newBean, PerformanceInvestmentAccount::getYearToDateReturn, accountThreshold, errorBuilder);
		validateSummaryPropertyEqual("inceptionToDateReturn", originalBean, newBean, PerformanceInvestmentAccount::getInceptionToDateReturn, accountThreshold, errorBuilder);
		// Until history is back filled only validate if was saved
		if (originalBean.getInceptionToDateBenchmarkReturnCumulative() != null) {
			validateSummaryPropertyEqual("inceptionToDateReturnCumulative", originalBean, newBean, PerformanceInvestmentAccount::getInceptionToDateReturnCumulative, accountThreshold, errorBuilder);
		}

		validateSummaryPropertyEqual("monthToDateBenchmarkReturn", originalBean, newBean, PerformanceInvestmentAccount::getMonthToDateBenchmarkReturn, null, errorBuilder);
		validateSummaryPropertyEqual("quarterToDateBenchmarkReturn", originalBean, newBean, PerformanceInvestmentAccount::getQuarterToDateBenchmarkReturn, null, errorBuilder);
		validateSummaryPropertyEqual("yearToDateBenchmarkReturn", originalBean, newBean, PerformanceInvestmentAccount::getYearToDateBenchmarkReturn, null, errorBuilder);
		validateSummaryPropertyEqual("inceptionToDateBenchmarkReturn", originalBean, newBean, PerformanceInvestmentAccount::getInceptionToDateBenchmarkReturn, null, errorBuilder);
		// Until history is back filled only validate if was saved
		if (originalBean.getInceptionToDateBenchmarkReturnCumulative() != null) {
			validateSummaryPropertyEqual("inceptionToDateBenchmarkReturnCumulative", originalBean, newBean, PerformanceInvestmentAccount::getInceptionToDateBenchmarkReturnCumulative, null, errorBuilder);
		}

		validateSummaryPropertyEqual("monthToDatePortfolioReturn", originalBean, newBean, PerformanceInvestmentAccount::getMonthToDatePortfolioReturn, accountThreshold, errorBuilder);
		validateSummaryPropertyEqual("quarterToDatePortfolioReturn", originalBean, newBean, PerformanceInvestmentAccount::getQuarterToDatePortfolioReturn, accountThreshold, errorBuilder);
		validateSummaryPropertyEqual("yearToDatePortfolioReturn", originalBean, newBean, PerformanceInvestmentAccount::getYearToDatePortfolioReturn, accountThreshold, errorBuilder);
		validateSummaryPropertyEqual("inceptionToDatePortfolioReturn", originalBean, newBean, PerformanceInvestmentAccount::getInceptionToDatePortfolioReturn, accountThreshold, errorBuilder);
		// Until history is back filled only validate if was saved
		if (originalBean.getInceptionToDatePortfolioReturnCumulative() != null) {
			validateSummaryPropertyEqual("inceptionToDatePortfolioReturnCumulative", originalBean, newBean, PerformanceInvestmentAccount::getInceptionToDatePortfolioReturnCumulative, accountThreshold, errorBuilder);
		}
		Assertions.assertEquals(0, errorBuilder.length(), errorBuilder.toString());
	}


	protected void validateSummaryPropertyEqual(String propertyName, PerformanceInvestmentAccount originalBean, PerformanceInvestmentAccount newBean, Function<PerformanceInvestmentAccount, BigDecimal> valueGetter, BigDecimal threshold, StringBuilder errorBuilder) {
		// Format to 4 decimals
		BigDecimal originalVal = valueGetter.apply(originalBean);
		BigDecimal newVal = valueGetter.apply(newBean);
		validateSummaryPropertyEqual("Performance Summary [" + originalBean.getLabel() + "] ", propertyName, originalVal, newVal, threshold, errorBuilder);
	}


	protected void validateSummaryPropertyEqual(String errorPrefix, String propertyName, BigDecimal originalValue, BigDecimal newValue, BigDecimal threshold, StringBuilder errorBuilder) {
		if (threshold == null) {
			threshold = BigDecimal.ZERO;
		}
		// Format to 4 decimals
		BigDecimal difference = MathUtils.round(MathUtils.subtract(newValue, originalValue), 4);
		if (!MathUtils.isNullOrZero(difference)) {
			if (MathUtils.isGreaterThan(MathUtils.abs(difference), threshold)) {
				errorBuilder.append(errorPrefix).append("Property [").append(propertyName)
						.append("]: Expected ").append(CoreMathUtils.formatNumber(originalValue, "#,###.####"))
						.append(", but was ").append(CoreMathUtils.formatNumber(newValue, "#,###.####"))
						.append(". Difference not within given threshold of ").append(CoreMathUtils.formatNumberDecimal(threshold))
						.append('\n');
			}
		}
	}
}
