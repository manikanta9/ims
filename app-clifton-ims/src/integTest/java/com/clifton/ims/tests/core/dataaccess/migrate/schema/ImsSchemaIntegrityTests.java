package com.clifton.ims.tests.core.dataaccess.migrate.schema;

import com.clifton.test.core.dataaccess.migrate.schema.BaseSchemaIntegrityTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author NickK
 */
@ContextConfiguration(locations = "classpath:com/clifton/ims/tests/core/dataaccess/SessionFactoryTests-context.xml")
@ExtendWith(SpringExtension.class)
public class ImsSchemaIntegrityTests extends BaseSchemaIntegrityTests {

	// nothing here
}
