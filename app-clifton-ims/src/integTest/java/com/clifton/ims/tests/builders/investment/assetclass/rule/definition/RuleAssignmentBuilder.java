package com.clifton.ims.tests.builders.investment.assetclass.rule.definition;

import com.clifton.rule.definition.RuleAssignment;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.RuleDefinitionService;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author stevenf
 */
public class RuleAssignmentBuilder {

	private final RuleDefinitionService ruleDefinitionService;

	private RuleDefinition ruleDefinition;
	private String ruleDefinitionName;

	private boolean excluded;

	private Long additionalScopeFkFieldId;
	private String additionalScopeLabel;

	private Long entityFkFieldId;
	private String entityLabel;

	private BigDecimal maxAmount;
	private BigDecimal minAmount;
	private BigDecimal ruleAmount;

	private Date endDate;
	private Date startDate;

	private String note;


	private RuleAssignmentBuilder(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public static RuleAssignmentBuilder ruleAssignment(RuleDefinitionService ruleDefinitionService) {
		return new RuleAssignmentBuilder(ruleDefinitionService);
	}


	public RuleAssignment build() {
		RuleAssignment ruleAssignment = new RuleAssignment();
		ruleAssignment.setRuleAmount(this.ruleAmount);
		this.ruleDefinition = this.ruleDefinition == null ? (this.ruleDefinitionName != null) ? this.ruleDefinitionService.getRuleDefinitionByName(this.ruleDefinitionName) : null : this.ruleDefinition;
		ruleAssignment.setRuleDefinition(this.ruleDefinition);
		ruleAssignment.setAdditionalScopeFkFieldId(this.additionalScopeFkFieldId);
		ruleAssignment.setAdditionalScopeLabel(this.additionalScopeLabel);
		ruleAssignment.setEntityFkFieldId(this.entityFkFieldId);
		ruleAssignment.setEntityLabel(this.entityLabel);
		ruleAssignment.setExcluded(this.excluded);
		ruleAssignment.setEndDate(this.endDate);
		ruleAssignment.setStartDate(this.startDate);
		ruleAssignment.setMaxAmount(this.maxAmount);
		ruleAssignment.setMinAmount(this.minAmount);
		ruleAssignment.setNote(this.note);
		return ruleAssignment;
	}


	public RuleAssignment buildAndSave() {
		RuleAssignment ruleAssignment = build();
		return this.ruleDefinitionService.saveRuleAssignment(ruleAssignment);
	}


	public RuleAssignmentBuilder createRuleAssignment() {
		//TODO - defaults?
		return this;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public RuleAssignmentBuilder withNote(String note) {
		this.note = note;
		return this;
	}


	public RuleAssignmentBuilder withRuleAmount(BigDecimal ruleAmount) {
		this.ruleAmount = ruleAmount;
		return this;
	}


	public RuleAssignmentBuilder withRuleDefinition(RuleDefinition ruleDefinition) {
		this.ruleDefinition = ruleDefinition;
		return this;
	}


	public RuleAssignmentBuilder withRuleDefinitionName(String ruleDefinitionName) {
		this.ruleDefinitionName = ruleDefinitionName;
		return this;
	}


	public RuleAssignmentBuilder withAdditionalScopeFkFieldId(Long additionalScopeFkFieldId) {
		this.additionalScopeFkFieldId = additionalScopeFkFieldId;
		return this;
	}


	public RuleAssignmentBuilder withAdditionalScopeLabel(String additionalScopeLabel) {
		this.additionalScopeLabel = additionalScopeLabel;
		return this;
	}


	public RuleAssignmentBuilder withEntityFkFieldId(Long entityFkFieldId) {
		this.entityFkFieldId = entityFkFieldId;
		return this;
	}


	public RuleAssignmentBuilder withEntityLabel(String entityLabel) {
		this.entityLabel = entityLabel;
		return this;
	}


	public RuleAssignmentBuilder withExcluded(boolean excluded) {
		this.excluded = excluded;
		return this;
	}


	public RuleAssignmentBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}


	public RuleAssignmentBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public RuleAssignmentBuilder withMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
		return this;
	}


	public RuleAssignmentBuilder withMinAmount(BigDecimal minAmount) {
		this.minAmount = minAmount;
		return this;
	}
}
