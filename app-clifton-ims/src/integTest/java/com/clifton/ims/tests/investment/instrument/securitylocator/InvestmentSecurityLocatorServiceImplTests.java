package com.clifton.ims.tests.investment.instrument.securitylocator;


import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class InvestmentSecurityLocatorServiceImplTests extends BaseImsIntegrationTest {

	@Test
	public void testSecurityLookupBySymbol() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL UW Equity", null);
		Assertions.assertNotNull(security, "Cannot find 'AAPL UW Equity'");
		Assertions.assertEquals("AAPL", security.getSymbol());

		security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL UW", null);
		Assertions.assertNotNull(security, "Cannot find 'AAPL UW'");
		Assertions.assertEquals("AAPL", security.getSymbol());

		security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL Equity", null);
		Assertions.assertNotNull(security, "Cannot find 'AAPL Equity'");
		Assertions.assertEquals("AAPL", security.getSymbol());

		security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", null);
		Assertions.assertNotNull(security, "Cannot find 'AAPL'");
		Assertions.assertEquals("AAPL", security.getSymbol());

		security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL UW Govt", null);
		Assertions.assertNull(security, "Should not be able to find 'AAPL UW Govt'");
	}
}
