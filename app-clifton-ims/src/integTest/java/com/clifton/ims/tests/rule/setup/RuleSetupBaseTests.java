package com.clifton.ims.tests.rule.setup;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.search.RuleCategorySearchForm;
import com.clifton.system.schema.column.SystemColumnService;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


/**
 * Test the setup of rule related entities to ensure they follow our conventions
 */
public abstract class RuleSetupBaseTests extends BaseImsIntegrationTest {

	protected final String RULE_CATEGORY_NAME_SUFFIX = "Rules";
	protected final String RULE_CATEGORY_EVALUATOR_CONTEXT_BEAN_SUFFIX = "Rule Evaluator Context";

	//First Word Doesn't Match first word of Category Table Name
	protected static final Set<String> EXCLUDED_CATEGORY_CATEGORY_TABLE_VERIFICATION =
			CollectionUtils.createHashSet("Manager Account Balance Rules", "REPO Rules", "Transfer Rules");

	//Transfer rules specify AccountingPositionTransfer, but use a converterBean...
	//so the additional scope does not exist on the category table, but does on the converted object.
	protected static final Set<String> EXCLUDED_CATEGORY_CATEGORY_ADDITIONAL_SCOPE_VERIFICATION =
			CollectionUtils.createHashSet("Transfer Rules");

	protected static final Set<String> EXCLUDED_TABLE_VERIFICATION =
			CollectionUtils.createHashSet("AccountingPositionDaily", "ProductOverlayManagerAccountRebalance", "PortfolioRunMargin", "BillingDefinitionInvestmentAccount", "BillingBasisSnapshot");

	@Resource
	public RuleDefinitionService ruleDefinitionService;

	@SuppressWarnings("rawtypes")
	@Resource
	public SystemColumnService systemColumnService;


	@Test
	public void testRuleSetup() {
		StringBuilder violationBuilder = new StringBuilder();
		List<RuleCategory> ruleCategoryList = getRuleCategoryList();
		StringBuilder ruleSetupViolationBuilder = new StringBuilder();
		for (RuleCategory ruleCategory : CollectionUtils.getIterable(ruleCategoryList)) {
			StringBuilder categoryViolationBuilder = new StringBuilder();
			try {
				verifyRuleCategoryName(ruleCategory);
			}
			catch (Exception e) {
				appendToBuilder(categoryViolationBuilder, "Verify Rule Category Name", e.getMessage());
			}
			try {
				if (!isRuleCategoryCategoryTableVerificationSkipped(ruleCategory)) {
					verifyRuleCategoryTable(ruleCategory);
				}
			}
			catch (Exception e) {
				appendToBuilder(categoryViolationBuilder, "Verify Rule Category Table", e.getMessage());
			}
			try {
				verifyRuleCategoryEvaluatorContextBean(ruleCategory);
			}
			catch (Exception e) {
				appendToBuilder(categoryViolationBuilder, "Verify Rule Category Evaluator Context Bean", e.getMessage());
			}
			try {
				verifyRuleCategorySecurityResource(ruleCategory);
			}
			catch (Exception e) {
				appendToBuilder(categoryViolationBuilder, "Verify Rule Category Security Resource", e.getMessage());
			}
			try {
				if (!isRuleCategoryCategoryAdditionalScopesVerificationSkipped(ruleCategory)) {
					verifyRuleCategoryAdditionalScopes(ruleCategory);
				}
			}
			catch (Exception e) {
				appendToBuilder(categoryViolationBuilder, "Verify Rule Category Additional Scopes", e.getMessage());
			}

			StringBuilder ruleDefinitionViolationBuilder = verifyRuleDefinitionListForCategory(ruleCategory);
			if (ruleDefinitionViolationBuilder != null) {
				//There was one or more Rule Definition Violations
				appendToBuilder(categoryViolationBuilder, "Rule Definition", "{" + ruleDefinitionViolationBuilder.toString() + "}");
			}

			if (!StringUtils.isEmpty(categoryViolationBuilder.toString())) {
				//There was a violation in this Rule Category
				appendToBuilder(ruleSetupViolationBuilder, ruleCategory.getName(), "{" + categoryViolationBuilder.toString() + "}");
			}
		}
		ValidationUtils.assertTrue(StringUtils.isEmpty(ruleSetupViolationBuilder.toString()), new GsonBuilder().setPrettyPrinting().create().toJson(new JsonParser().parse("{\"Rule Category\":{" + ruleSetupViolationBuilder.toString() + "}}")));
	}


	protected abstract void verifyRuleCategoryName(RuleCategory ruleCategory);


	protected abstract void verifyRuleCategoryTable(RuleCategory ruleCategory);


	protected abstract void verifyRuleCategoryEvaluatorContextBean(RuleCategory ruleCategory);


	protected abstract void verifyRuleCategorySecurityResource(RuleCategory ruleCategory);


	protected abstract void verifyRuleCategoryAdditionalScopes(RuleCategory ruleCategory);


	protected abstract StringBuilder verifyRuleDefinitionListForCategory(RuleCategory ruleCategory);


	private boolean isRuleCategoryCategoryTableVerificationSkipped(RuleCategory ruleCategory) {
		return EXCLUDED_CATEGORY_CATEGORY_TABLE_VERIFICATION.contains(ruleCategory.getName());
	}


	private boolean isRuleCategoryCategoryAdditionalScopesVerificationSkipped(RuleCategory ruleCategory) {
		return EXCLUDED_CATEGORY_CATEGORY_ADDITIONAL_SCOPE_VERIFICATION.contains(ruleCategory.getName());
	}


	private List<RuleCategory> getRuleCategoryList() {
		RuleCategorySearchForm searchForm = new RuleCategorySearchForm();
		return this.ruleDefinitionService.getRuleCategoryList(searchForm);
	}


	protected void appendToBuilder(StringBuilder builder, String name, String message) {
		String result = "\"" + name + "\"" + ":" + ((message.startsWith("{")) ? message : "\"" + message + "\"");
		if (StringUtils.isEmpty(builder.toString())) {
			builder.append(result);
		}
		else {
			builder.append(",").append(result);
		}
	}
}
