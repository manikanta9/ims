package com.clifton.ims.tests.integration.export.ftp;

import com.clifton.core.util.CollectionUtils;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class FakeFtpServerUtils {


	/**
	 * Creates a new {@link FakeFtpServer} with a user account using the given username and password
	 * with a UnixFileSystem with a given directory root. If no directoryRoot is specified uses "/".
	 */
	public FakeFtpServer createAndInitializeFakeFtpServer(String username, String password, String directoryRoot) {
		if (directoryRoot == null) {
			directoryRoot = "/";
		}
		FakeFtpServer fakeFtpServer = new FakeFtpServer();
		fakeFtpServer.addUserAccount(new UserAccount(username, password, directoryRoot));

		FileSystem fileSystem = new UnixFakeFileSystem();
		fileSystem.add(new DirectoryEntry(directoryRoot));
		fakeFtpServer.setFileSystem(fileSystem);

		return fakeFtpServer;
	}


	/**
	 * Removes all files from the fake FTP server
	 */
	public void clearFilesFromFolder(FakeFtpServer fakeFtpServer, String remoteFolder) {
		@SuppressWarnings({"unchecked"})
		List<FileEntry> fileList = fakeFtpServer.getFileSystem().listFiles(remoteFolder);
		for (FileEntry file : CollectionUtils.getIterable(fileList)) {
			fakeFtpServer.getFileSystem().delete(file.getPath());
		}
	}


	/**
	 * Polls the FTP server every given number of ms (sleep time) until poll limit is reached
	 */
	public boolean serverHasFiles(FakeFtpServer fakeFtpServer, String folder, int pollLimitCount, int sleepTime) throws InterruptedException {
		int counter = 0;
		while (counter < pollLimitCount) {
			if (!CollectionUtils.isEmpty(fakeFtpServer.getFileSystem().listFiles(folder))) {
				return true;
			}
			else {
				counter += 1;
				Thread.sleep(sleepTime);
			}
		}
		return false;
	}
}
