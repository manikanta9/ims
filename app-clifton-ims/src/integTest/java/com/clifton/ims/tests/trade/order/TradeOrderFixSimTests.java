package com.clifton.ims.tests.trade.order;

import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.fix.quickfix.converter.QuickFixConverterUtils;
import com.clifton.fix.session.FixSessionService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.builders.investment.trade.TradeBuilder;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountRelationshipPurposes;
import com.clifton.ims.tests.trade.TradeCreator;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.connection.TradeDestinationConnection;
import com.clifton.trade.destination.connection.TradeDestinationConnectionSearchForm;
import com.clifton.trade.destination.connection.TradeDestinationConnectionService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchForm;
import com.clifton.trade.order.TradeOrder;
import com.clifton.trade.order.TradeOrderService;
import com.clifton.trade.order.TradeOrderStatuses;
import com.clifton.trade.order.allocation.TradeOrderAllocationStatuses;
import com.clifton.trade.order.grouping.TradeOrderGroupingTypes;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * TradeOrderFixSimTests are tests that were formerly run under the TradeOrderFixTests, which has been updated to
 * use FixSim as the remote FIX end point.
 */
public class TradeOrderFixSimTests extends BaseImsIntegrationTest {

	// Note: should be "PARAMETRIC_DEV"  for integration tests, and be set to "PARAMETRIC_QA" for local testing on your computer in "build.my.properties" file.
	// Also for local testing set "build.my.properties" migration settings (in the FIX Application project) to:  migration.after.restore.sql.file=META-INF/schema/sql/After_Restore_Database_SQL_QA.sql in build file for
	@Value("${fix.fixSimInstance}")
	private String fixSimInstance;

	// Note: should be "_DEV" for scheduled integration tests, and set to  "_QA" for local testing on your computer in "build.my.properties" file
	@Value("${fix.sessionSuffix}")
	private String fixSessionSuffix;

	// The path to the directory that contains the FIX Messages templates for the test.
	@Value("${fix.dataSourceDirectory}")
	private String dataSourceDirectory;

	// Determines how much of a difference is allowed between FixSim and local FIX sequence numbers.  If exceeded, we attempt to synchronize the FixSim session sequence numbers to ours.
	@Value("${fix.sequenceNumberDifferenceTolerance}")
	private int sequenceNumberDifferenceTolerance;

	// FixSim Sessions
	private static final String REDI_MULTI_BROKER_SESSION = "REDI Multi-Broker";
	private static final String FX_CONNECT_BROKER_SESSION = "FX Connect";
	private static final String BLOOMBERG_MINNEAPOLIS_SESSION = "Bloomberg (Parametric Minneapolis Fixed Income)";
	private static final String CITI_DESK_SESSION = "CITI CARE";
	private static final String BLOOMBERG_EMSX_SESSION = "Bloomberg EMSX";
	private static final String WEX_SESSION = "WEX (Wolverine Execution)";

	// FixSim sessions currently in use by these tests.  Add more as needed to accommodate any additional test.
	private final static Map<String, Boolean> FIXSIM_SESSIONS = new HashMap<>();


	public static final Set<Integer> COMPARISON_IGNORE_FIELDS_D_MESSAGE = CollectionUtils.createHashSet(9, 10, 34, 50, 52, 57, 58, 60);
	public static final Set<Integer> COMPARISON_IGNORE_FIELDS_J_MESSAGE = CollectionUtils.createHashSet(9, 10, 34, 50, 52, 58, 60);
	private static boolean isInitialized = false;

	@Resource
	private BusinessCompanyService businessCompanyService;
	@Resource
	private FixSessionService fixSessionService;
	@Resource
	private InvestmentAccountService investmentAccountService;
	@Resource
	private TradeCreator tradeCreator;
	@Resource
	private TradeService tradeService;
	@Resource
	private TradeOrderService tradeOrderService;
	@Resource
	private TradeDestinationService tradeDestinationService;
	@Resource
	private TradeDestinationConnectionService tradeDestinationConnectionService;
	@Resource
	private TradeOrderFixSimTestHelper tradeOrderFixSimTestHelper;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void initializeTestEnvironment() {
		FIXSIM_SESSIONS.put(BLOOMBERG_MINNEAPOLIS_SESSION, Boolean.FALSE);
		FIXSIM_SESSIONS.put(BLOOMBERG_EMSX_SESSION, Boolean.FALSE);
		FIXSIM_SESSIONS.put(CITI_DESK_SESSION, Boolean.FALSE);
		FIXSIM_SESSIONS.put(FX_CONNECT_BROKER_SESSION, Boolean.FALSE);
		FIXSIM_SESSIONS.put(REDI_MULTI_BROKER_SESSION, Boolean.FALSE);
		FIXSIM_SESSIONS.put(WEX_SESSION, Boolean.FALSE);

		getTradeOrderFixSimTestHelper().setDataSourceDirectory(getDataSourceDirectory());
		getTradeOrderFixSimTestHelper().setFixSessionSuffix(getFixSessionSuffix());

		// Apply the fixConnectionSuffix value to each FixDestinationName property value for all TradeDestinationConnections.
		TradeDestinationConnectionSearchForm tradeDestSearchForm = new TradeDestinationConnectionSearchForm();
		tradeDestSearchForm.setLimit(100);
		List<TradeDestinationConnection> tradeDestinationConnectionList = this.tradeDestinationConnectionService.getTradeDestinationConnectionList(tradeDestSearchForm);
		for (TradeDestinationConnection connection : CollectionUtils.getIterable(tradeDestinationConnectionList)) {
			String fixDestinationName = connection.getFixDestinationName();
			if (fixDestinationName != null && !fixDestinationName.endsWith(this.fixSessionSuffix)) {
				int endIndex = connection.getFixDestinationName().length();
				fixDestinationName = StringUtils.substring(fixDestinationName, 0, Math.min(endIndex, 45)) + this.fixSessionSuffix;
				connection.setFixDestinationName(fixDestinationName);
				this.tradeDestinationConnectionService.saveTradeDestinationConnection(connection);
			}
		}

		// Reset all FixSim Sessions
		for (String sessionName : FIXSIM_SESSIONS.keySet()) {
			getTradeOrderFixSimTestHelper().getFixSimHelper().resetFixSimSession(getFixSimInstance(), sessionName, getFixSessionSuffix(), getSequenceNumberDifferenceTolerance());
		}
	}


	@BeforeEach
	public void InitData() {
		// this is only executed once
		if (!isInitialized) {
			initializeTestEnvironment();
			isInitialized = true;
		}

		// Confirm Fix App is Running
		try {
			this.fixSessionService.getFixSessionDefinition((short) 1);
		}
		catch (Throwable e) {
			throw new ValidationException("Cannot find FixSessionDefinition.  Make sure your Fix Web App is Running.", e);
		}

		// EMSX - Stocks
		TradeDestinationMappingSearchCommand searchCommand = new TradeDestinationMappingSearchCommand();
		searchCommand.setTradeTypeId((this.tradeService.getTradeTypeByName("Stocks")).getId());
		searchCommand.setTradeDestinationId((this.tradeDestinationService.getTradeDestinationByName("Bloomberg EMSX ALGO")).getId());
		searchCommand.setExecutingBrokerCompanyId((this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "Morgan Stanley & Co. Inc.")).getId());
		TradeDestinationMapping emsxStocksMapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(searchCommand);
		if (emsxStocksMapping == null) {
			throw new RuntimeException("Cannot find TradeDestinationMapping for Bloomberg EMSX Ticket, Stocks and broker Morgan Stanley");
		}
		else if (!emsxStocksMapping.isActive()) {
			emsxStocksMapping.setEndDate(DateUtils.addYears(new Date(), 500));
			this.tradeDestinationService.saveTradeDestinationMapping(emsxStocksMapping);
		}

		// WEX - Stocks
		searchCommand = new TradeDestinationMappingSearchCommand();
		searchCommand.setTradeTypeId((this.tradeService.getTradeTypeByName("Stocks")).getId());
		searchCommand.setTradeDestinationId((this.tradeDestinationService.getTradeDestinationByName("WEX Ticket")).getId());
		searchCommand.setExecutingBrokerCompanyId((this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "Wolverine Execution Services, LLC")).getId());
		TradeDestinationMapping wexStocksMapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(searchCommand);
		if (wexStocksMapping == null) {
			throw new RuntimeException("Cannot find TradeDestinationMapping for Wex Ticket, Stocks and Wolverine Execution Services, LLC");
		}
		else if (!wexStocksMapping.isActive() || !StringUtils.isEqual("PARA-WEX", wexStocksMapping.getFixPostTradeAllocationTopAccountNumber())) {
			wexStocksMapping.setEndDate(DateUtils.addYears(new Date(), 500));
			wexStocksMapping.setFixPostTradeAllocationTopAccountNumber("PARA-WEX");
			this.tradeDestinationService.saveTradeDestinationMapping(wexStocksMapping);
		}

		// WEX - Options
		searchCommand = new TradeDestinationMappingSearchCommand();
		searchCommand.setTradeTypeId((this.tradeService.getTradeTypeByName("Options")).getId());
		searchCommand.setTradeDestinationId((this.tradeDestinationService.getTradeDestinationByName("WEX Ticket")).getId());
		searchCommand.setExecutingBrokerCompanyId((this.businessCompanyService.getBusinessCompanyByTypeAndName("Broker", "Wolverine Execution Services, LLC")).getId());
		TradeDestinationMapping wexOptionsMapping = this.tradeDestinationService.getTradeDestinationMappingByCommand(searchCommand);
		if (wexOptionsMapping == null) {
			throw new RuntimeException("Cannot find TradeDestinationMapping for WEX Ticket, Options and Wolverine Execution Services, LLC");
		}
		else if (!wexOptionsMapping.isActive()) {
			wexOptionsMapping.setEndDate(DateUtils.addYears(new Date(), 500));
			this.tradeDestinationService.saveTradeDestinationMapping(wexOptionsMapping);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (13) (A)
	 * <- Single Order (S)
	 * <- Execution Report (3) (S)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (2) (A)
	 * <- Allocation Report (A)
	 * <- Allocation Instruction (S)
	 * <- Allocation Report (S)
	 * <p>
	 * File:  CLIFTONMB44_TEST_USER_1_ESM16_INDEX_36_00000000.dat
	 */
	@Test
	public void testRediGoldmanFutures() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0001";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.MELLON_BANK_NA,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(accountInfo, "ESM16 Index", "06/02/2016", 36.00000000, true, "REDI Ticket", BusinessCompanies.GOLDMAN_SACHS_AND_CO, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(REDI_MULTI_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "REDITKTS", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("REDITKTS"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONMB44", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONMB44"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20160705", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20160705", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201606", "200=" + getTradeOrderFixSimTestHelper().getMaturityMonthYear(trade.getInvestmentSecurity()));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "79=157-85082", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "64=20160705", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "75=20160705", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=157-85082", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_185691_20160602");
		testContext.setTemplateFixAllocationId("A_179368_20160602");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.ACCEPTED);
		testContext.setExpectedAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);
		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONMB44_TEST_USER_1_ESM16_INDEX_36_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.COMPLETE));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		BigDecimal totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(36.00000000).compareTo(totalQuantity));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(2096.75).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(2096.75).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (5) (A)
	 */
	@Test
	public void testFxConnectBarclayForward() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0002";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "FX Connect", "test_user_1");
		Trade trade = this.tradeCreator.createForwardTrade(accountInfo, "INR/USD20150617", "USD", "06/10/2016", new BigDecimal("1098853.65000000"),
				new BigDecimal("17173.35"), new BigDecimal("63.985977778000000"), true, "FX Connect Ticket", BusinessCompanies.BARCLAYS_BANK_PLC, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(FX_CONNECT_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "FXCCXUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("FXCCXUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "glfixhub.bos.clifton", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("glfixhub.bos.clifton"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20150610", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=900020", "1=" + accountInfo.getClientAccount().getNumber());
		testContext.setTemplateFixOrderId("O_137482_20150610");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("GLFIXHUB_BOS_CLIFTON_TEST_USER_1_INR_USD_1098853_65000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(63.93).compareTo(tradeFills.get(0).getNotionalUnitPrice()));

		// trade average unit price, qty
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(63.93).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(1098853.6500000000).compareTo(trade.getQuantityIntended()));
		Assertions.assertEquals(0, BigDecimal.valueOf(17188.39).compareTo(trade.getAccountingNotional()));
		// trade order price, qty
		Assertions.assertEquals(0, BigDecimal.valueOf(63.93).compareTo(tradeOrder.getAveragePrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(1098853.6500000000).compareTo(tradeOrder.getQuantityFilled()));
		Assertions.assertEquals(0, BigDecimal.valueOf(17188.39).compareTo(trade.getAccountingNotional()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (5) (A)
	 */
	@Test
	public void testFxConnectCompetitiveForward() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0003";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BOA_CUSTODIAN, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.BARCLAYS_BANK_PLC, accountInfo.getCustodianAccount(),
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL, accountInfo.getCustodianAccount(),
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.JP_MORGAN_CHASE, accountInfo.getCustodianAccount(),
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.MORGAN_STANLEY_AND_CO_INTERNATIONAL_PLC, accountInfo.getCustodianAccount(),
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		accountInfo = this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.UBS, accountInfo.getCustodianAccount(),
				this.tradeUtils.getTradeType(TradeType.FORWARDS));

		this.tradeUtils.createSecuritySystemUser("imstestuser1", "FX Connect", "test_user_1");
		Trade trade = this.tradeCreator.createForwardTrade(accountInfo, "GBP/USD20160921", "GBP", "07/08/2016", new BigDecimal("25000000.00000000"), null, null, false, "FX Connect Ticket", null, true, false, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(FX_CONNECT_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "FXCCXUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("FXCCXUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "glfixhub.bos.clifton", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("glfixhub.bos.clifton"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=187066", "1=" + accountInfo.getClientAccount().getNumber());
		testContext.setTemplateFixOrderId("O_190892_20160708");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("GLFIXHUB_BOS_CLIFTON_TEST_USER_1_USD_GBP_25000000_00000000.dat", testContext);
		testContext.getFixTagsWithListValues().add(448);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(1.296330000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()));

		// trade average unit price, qty
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(1.29633).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(19285212.87).compareTo(trade.getQuantityIntended()));
		Assertions.assertEquals(0, BigDecimal.valueOf(25000000.0000000000).compareTo(trade.getAccountingNotional()));
		// trade order price, qty
		Assertions.assertEquals(0, BigDecimal.valueOf(1.296330000000000).compareTo(tradeOrder.getAveragePrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(25000000.0000000000).compareTo(tradeOrder.getQuantityFilled()));
	}


	@Test
	public void testFxCAD_USD_201706212_Forward() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0004";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.FORWARDS));

		this.tradeUtils.createSecuritySystemUser("imstestuser1", "FX Connect", "test_user_1");
		Trade trade = this.tradeCreator.createForwardTrade(accountInfo, "CAD/USD20170621", "CAD", "04/28/2017", new BigDecimal("542000.00000000"), new BigDecimal("739146.29"), new BigDecimal("1.363738543768100"), false, "FX Connect Ticket",
				BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL, true, false, testIdentifier);

		trade = this.approveTrade(trade);

		TestContext testContext = createContext(FX_CONNECT_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "FXCCXUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("FXCCXUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "glfixhub.bos.clifton", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("glfixhub.bos.clifton"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=385400", "1=" + accountInfo.getClientAccount().getNumber());
		testContext.setTemplateFixOrderId("O_232093_20170428");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("GLFIXHUB_BOS_CLIFTON_TEST_USER_1_USD_CAD_542000_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));

		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(1.36445).compareTo(trade.getAverageUnitPrice()));
		//Assertions.assertEquals(0, BigDecimal.valueOf(1.0).compareTo(trade.getExchangeRateToBase()));
		Assertions.assertEquals(0, BigDecimal.valueOf(542000.00).compareTo(trade.getAccountingNotional()));
		Assertions.assertEquals(0, BigDecimal.valueOf(739531.90).compareTo(trade.getQuantityIntended()));
		// trade order price, qty
		Assertions.assertEquals(0, BigDecimal.valueOf(1.36445).compareTo(tradeOrder.getAveragePrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(542000.00).compareTo(tradeOrder.getQuantityFilled()));
	}


	@Test
	public void testFxUSD_GBP_20190712_Forward_CLS_SELL() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0005";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BANK_ON_NEW_YORK_MELLON, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL,
				this.tradeUtils.getTradeType(TradeType.FORWARDS), true);
		this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.GOLDMAN_SACHS_AND_CO, accountInfo.getCustodianAccount(), this.tradeUtils.getTradeType(TradeType.FORWARDS));
		this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.MORGAN_STANLEY_AND_CO_INTERNATIONAL_PLC, accountInfo.getCustodianAccount(), this.tradeUtils.getTradeType(TradeType.FORWARDS));
		this.tradeCreator.setClsMemberTagOnBroker(accountInfo.getHoldingAccount().getIssuingCompany());

		this.tradeUtils.createSecuritySystemUser("imstestuser1", "FX Connect", "test_user_1");
		Trade trade = this.tradeCreator.createForwardTrade(accountInfo, "USD/GBP20190930", "GBP", "07/17/2019", new BigDecimal("120000.00000000"),
				new BigDecimal("83333.33333333"), new BigDecimal("1.44"), false, "FX Connect Ticket",
				null, false, true, testIdentifier);

		trade = this.approveTrade(trade);

		TestContext testContext = createContext(FX_CONNECT_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "FXCCXUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("FXCCXUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "glfixhub.bos.clifton", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("glfixhub.bos.clifton"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=051450", accountInfo.getClientAccount().getNumber());
		testContext.setTemplateFixOrderId("O_388118_20190717");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.SENT);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("GLFIXHUB_BOS_CLIFTON_TEST_USER_1_USD_GBP_120000_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.SENT));

		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(1.44).compareTo(trade.getExpectedUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(83333.33).compareTo(trade.getAccountingNotional()));
		Assertions.assertEquals(0, BigDecimal.valueOf(120000.00).compareTo(trade.getQuantityIntended()));
		Assertions.assertEquals(0, BigDecimal.valueOf(120000.00).compareTo(tradeOrder.getQuantity()));
	}


	@Test
	public void testFxUSD_GBP_20190712_Forward_CLS_BUY() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0006";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BANK_ON_NEW_YORK_MELLON, BusinessCompanies.BANK_ON_NEW_YORK_MELLON, BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL,
				this.tradeUtils.getTradeType(TradeType.FORWARDS), true);
		this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.GOLDMAN_SACHS_AND_CO, accountInfo.getCustodianAccount(), this.tradeUtils.getTradeType(TradeType.FORWARDS));
		this.investmentAccountUtils.createAccountsForTrading(accountInfo.getClientAccount(), BusinessCompanies.MORGAN_STANLEY_AND_CO_INTERNATIONAL_PLC, accountInfo.getCustodianAccount(), this.tradeUtils.getTradeType(TradeType.FORWARDS));
		this.tradeCreator.setClsMemberTagOnBroker(accountInfo.getHoldingAccount().getIssuingCompany());

		this.tradeUtils.createSecuritySystemUser("imstestuser1", "FX Connect", "test_user_1");
		Trade trade = this.tradeCreator.createForwardTrade(accountInfo, "USD/GBP20190930", "GBP", "07/17/2019", new BigDecimal("200000.00"),
				new BigDecimal("166666.67"), new BigDecimal("1.20"), true, "FX Connect Ticket",
				null, false, true, testIdentifier);

		trade = this.approveTrade(trade);

		TestContext testContext = createContext(FX_CONNECT_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "FXCCXUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("FXCCXUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "glfixhub.bos.clifton", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("glfixhub.bos.clifton"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=051450", accountInfo.getClientAccount().getNumber());
		testContext.setTemplateFixOrderId("O_388125_20190717");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.SENT);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("GLFIXHUB_BOS_CLIFTON_TEST_USER_1_USD_GBP_200000_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.SENT));

		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(1.20).compareTo(trade.getExpectedUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(166666.67).compareTo(trade.getAccountingNotional()));
		Assertions.assertEquals(0, BigDecimal.valueOf(200000.00).compareTo(trade.getQuantityIntended()));
		Assertions.assertEquals(0, BigDecimal.valueOf(200000.00).compareTo(tradeOrder.getQuantity()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (2) (A)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (1) (A)
	 * <- Allocation Report (A)
	 * <p>
	 * CLIFTONPARA_912796JX8_50000000_00000000.dat
	 */
	@Test
	public void testBloombergGoldmanBond() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0007";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.BONDS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");
		// Java Bug - BigDecimal will return 5E+7 but DecimalFormat.parse drops the E+7.
		BigDecimal quantity = new BigDecimal("50000000.00000000");
		Trade trade = this.tradeCreator.createBondTrade(accountInfo, "912796JX8", "07/05/2016", quantity, true, "Bloomberg Ticket", testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_MINNEAPOLIS_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLP", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLP"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONPARA", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONPARA"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20160720", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20160719", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "79=WAF09/17-02572", "79=" + trade.getHoldingInvestmentAccount().getNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "541=20170622", "541=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "64=20160720", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "75=20160719", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=WAF09/17-02572", "79=" + trade.getHoldingInvestmentAccount().getNumber());
		testContext.setTemplateFixOrderId("O_192223_20160719");
		testContext.setTemplateFixAllocationId("A_186578_20160719");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.RECEIVED);
		testContext.setExpectedAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONPARA_912796JX8_50000000_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.COMPLETE));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		BigDecimal totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(50000000.00000000).compareTo(totalQuantity));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(99.545986111000000).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(99.545986111000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (2) (A)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (2) (A)
	 * <- Allocation Report (A)
	 * <p>
	 * File:  PPAMPLSU_TEST_USER_1_IBN16_18_00000000.dat
	 */
	@Test
	public void testRediCitiDeskFutures() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0008";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, BusinessCompanies.MELLON_BANK_NA, this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(accountInfo, "IBN16", "07/05/2016", 18.00000000, true, "CITI Desk", BusinessCompanies.CITIGROUP, testIdentifier);
		trade = this.approveTrade(trade);

		TradeType tradeType = this.tradeService.getTradeTypeByName(TradeType.FUTURES);
		TradeDestinationMappingSearchForm searchForm = new TradeDestinationMappingSearchForm();
		searchForm.setTradeTypeId(tradeType.getId());
		searchForm.setExecutingBrokerCompanyId(trade.getExecutingBrokerCompany().getId());
		searchForm.setTradeDestinationName("CITI Desk");
		TradeDestinationMapping tradeDestinationMapping = CollectionUtils.getFirstElementStrict(this.tradeDestinationService.getTradeDestinationMappingList(searchForm));

		TestContext testContext = createContext(CITI_DESK_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CITICFOXU", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CITICFOXU"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSU", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSU"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20160712", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20160712", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=123456", "1=" + tradeDestinationMapping.getFixPostTradeAllocationTopAccountNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "64=20160712", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "75=20160712", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=276-9186E", "79=" + trade.getHoldingInvestmentAccount().getNumber());
		testContext.setTemplateFixOrderId("O_191331_20160712");
		testContext.setTemplateFixAllocationId("A_184723_20160712");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.ACCEPTED);
		testContext.setExpectedAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSU_TEST_USER_1_IBN16_18_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.COMPLETE));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		BigDecimal totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(18.00000000).compareTo(totalQuantity));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(8504.000000000000000).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(8504.000000000000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * Test for XPM0 Trade ID  2269665  that users are reporting no fills.  Unable to re-create, but leaving test here to confirm.
	 * File:  PPAMPLSU_TEST_USER_1_XPM0_23_00000000.dat
	 * <p>
	 * Production issue could not be reproduced with the previous TradeOrderFix test, but the problem encountered is manifesting itself here, and
	 * it appears the Allocation Report in the data is not being processed correctly.  This could be due to bad data sent in allocation instruction
	 * or received in allocation report or both.
	 */
	@Test
	public void testRediCitiDeskFutures_2() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0009";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, BusinessCompanies.GOLDMAN_SACHS_AND_CO,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(accountInfo, "XPM20", "04/09/2020", 23.00000000, true, "CITI Desk", BusinessCompanies.CITIGROUP, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(CITI_DESK_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CITICFOXU", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CITICFOXU"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSU", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSU"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20200409", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20200409", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=123456", "1=APS121");
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "64=20200409", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "75=20200409", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=157-74523", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.getComparisonIgnoreFieldsAllocInstruction().add(524);

		testContext.setTemplateFixOrderId("O_463348_20200408");
		testContext.setTemplateFixAllocationId("A_393540_20200408");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.ACCEPTED);
		testContext.setExpectedAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSU_TEST_USER_1_XPM0_23_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.COMPLETE));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		BigDecimal totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(23.00000000).compareTo(totalQuantity));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(5215.391304350000000).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(5215.3913043500000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (13) (A)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (2) (A)
	 * <- Allocation Report (A)
	 * <p>
	 * File:  CLIFTONMB44_TEST_USER_1_MFSU16_INDEX_42_00000000.dat
	 */
	@Test
	void testRediCitiFutures() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0010";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, BusinessCompanies.MELLON_BANK_NA,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFuturesTrade(accountInfo, "MFSU16", "08/16/2016", 42.0000000000, true, "REDI Ticket", BusinessCompanies.CITIGROUP, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(REDI_MULTI_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "REDITKTS", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("REDITKTS"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONMB44", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONMB44"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "200=201609", "200=" + getTradeOrderFixSimTestHelper().getMaturityMonthYear(trade.getInvestmentSecurity()));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=276-9182D", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_196046_20160816");
		testContext.setTemplateFixAllocationId("A_190156_20160816");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.ACCEPTED);
		testContext.setExpectedAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONMB44_TEST_USER_1_MFSU16_INDEX_42_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.COMPLETE));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		BigDecimal totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(42.0000000000).compareTo(totalQuantity));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(1707.457142860000000).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(1707.457142860000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (5) (A)
	 * <p>
	 * CLIFTONPARA_2I65BZDF6_18100000_00000000.dat
	 */
	@Test
	public void testBloombergGoldmanCreditDefaultSwapSef() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0011";

		TradeDestination tradeDestination = getTradeOrderFixSimTestHelper().createBloombergDestination();
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO,
				this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");
		BigDecimal quantity = new BigDecimal("18100000.00000000");
		Trade trade = this.tradeCreator.createClearedCreditDefaultSwap(accountInfo, "XM2501J21U0100XXI", "07/05/2016", quantity, tradeDestination.getName(), BusinessCompanies.CREDIT_SUISSE_SECURITIES, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_MINNEAPOLIS_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLP", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLP"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONPARA", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONPARA"));
		testContext.setTemplateFixOrderId("O_190387_20160705");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONPARA_2I65BZDF6_18100000_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(92.13).compareTo(tradeFills.get(0).getNotionalUnitPrice()));

		// trade average unit price, qty
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(92.13).compareTo(trade.getAverageUnitPrice()));
		// trade order price, qty
		Assertions.assertEquals(0, BigDecimal.valueOf(92.13).compareTo(tradeOrder.getAveragePrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(18100000.0000000000).compareTo(tradeOrder.getQuantityFilled()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (5) (A)
	 * <p>
	 * CLIFTONPARA_2I65BZDF6_19100000_00000000.dat
	 */
	@Test
	public void testBloombergGoldmanCreditDefaultSwap() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0012";

		//TradeDestination tradeDestination = createBloombergDestination();
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO,
				this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");
		BigDecimal quantity = new BigDecimal("19100000.00000000");
		Trade trade = this.tradeCreator.createClearedCreditDefaultSwap(accountInfo, "XM2501J21U0100XXI", "07/06/2016", quantity, "Bloomberg Ticket", testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_MINNEAPOLIS_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLP", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLP"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONPARA", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONPARA"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "79=601305794", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_190387_20160706");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		testContext.getComparisonIgnoreFieldsNewOrder().add(524);
		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONPARA_2I65BZDF6_19100000_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(92.13).compareTo(tradeFills.get(0).getNotionalUnitPrice()));

		// trade average unit price, qty
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(92.13).compareTo(trade.getAverageUnitPrice()));
		// trade order price, qty
		Assertions.assertEquals(0, BigDecimal.valueOf(92.13).compareTo(tradeOrder.getAveragePrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(19100000.0000000000).compareTo(tradeOrder.getQuantityFilled()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (12) (A)
	 * <- Single Order (S)
	 * <- Execution Report (3) (S)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (2) (A)
	 * <- Allocation Instruction (S)
	 * <p>
	 * File:  CLIFTONMB44_TEST_USER_1_464287457_1130_00000000.dat
	 */
	@Test
	public void testRediGoldmanFund() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0013";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE,
				BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, this.tradeUtils.getTradeType(TradeType.FUNDS));

		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE,
				BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, this.tradeUtils.getTradeType(TradeType.FUNDS));

		// These trades now require that the holding account have a related account with the relationship purpose:  Executing Funds
		InvestmentAccount brokerAccount = accountInfo2.getHoldingAccount();
		brokerAccount.setType(this.investmentAccountService.getInvestmentAccountTypeByName(InvestmentAccountType.BROKER));
		this.investmentAccountService.saveInvestmentAccount(brokerAccount);
		this.investmentAccountUtils.createAccountRelationship(accountInfo.getHoldingAccount(), brokerAccount, InvestmentAccountRelationshipPurposes.EXECUTING_FUNDS);

		//this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createFundsTrade(accountInfo, "SHY", "07/05/2016", 1130.00000000, true, 0.00, "REDI Ticket", BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(REDI_MULTI_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "REDITKTS", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("REDITKTS"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONMB44", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONMB44"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20160627", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20160622", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "64=20160627", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "75=20160622", "75=" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=276-9182D", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_188469_20160622");
		testContext.setTemplateFixAllocationId("A_181978_20160622");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.RECEIVED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONMB44_TEST_USER_1_2971524_1130_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.RECEIVED));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(1.000000000000000).compareTo(trade.getAverageUnitPrice()));
		Assertions.assertEquals(0, BigDecimal.valueOf(85.049100000000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (13) (A)
	 * <- Single Order (S)
	 * <- Execution Report (3) (S)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (2) (A)
	 * <- Allocation Report (A)
	 * <- Allocation Instruction (S)
	 * <- Allocation Report (S)
	 * <p>
	 * CLIFTONMB44_TEST_USER_1_MFSU16_INDEX_78_00000000.dat
	 */
	@Test
	public void testRediGoldmanFutureMultipleTrades() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0014";

		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.MELLON_BANK_NA,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade1 = this.tradeCreator.createFuturesTrade(accountInfo1, "MFSU16 Index", "07/05/2016", 25.00000000, true, "REDI Ticket", BusinessCompanies.GOLDMAN_SACHS_AND_CO, testIdentifier);
		trade1 = this.approveTrade(trade1);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.MELLON_BANK_NA,
				this.tradeUtils.getTradeType(TradeType.FUTURES));
		Trade trade2 = this.tradeCreator.createFuturesTrade(accountInfo2, "MFSU16 Index", "07/05/2016", 53.00000000, true, "REDI Ticket", BusinessCompanies.GOLDMAN_SACHS_AND_CO, testIdentifier);
		trade2 = this.approveTrade(trade2);

		TradeType tradeType = this.tradeService.getTradeTypeByName(TradeType.FUTURES);
		TradeDestinationMappingSearchForm searchForm = new TradeDestinationMappingSearchForm();
		searchForm.setTradeTypeId(tradeType.getId());
		searchForm.setExecutingBrokerCompanyId(trade1.getExecutingBrokerCompany().getId());
		searchForm.setTradeDestinationName("REDI Ticket");
		TradeDestinationMapping tradeDestinationMapping = CollectionUtils.getFirstElementStrict(this.tradeDestinationService.getTradeDestinationMappingList(searchForm));

		TestContext testContext = createContext(REDI_MULTI_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "REDITKTS", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("REDITKTS"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONMB44", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONMB44"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=C0320613", "1=" + tradeDestinationMapping.getFixPostTradeAllocationTopAccountNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20160727", "64=" + DateUtils.fromDate(trade1.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "75=20160727", "75=" + DateUtils.fromDate(trade1.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=052-BAG5G", "79=" + accountInfo2.getHoldingAccount().getNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "79=157-00087", "79=" + accountInfo1.getHoldingAccount().getNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "64=20160727", "64=" + DateUtils.fromDate(trade1.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "75=20160727", "75=" + DateUtils.fromDate(trade1.getTradeDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "200=201609", "200=" + getTradeOrderFixSimTestHelper().getMaturityMonthYear(trade1.getInvestmentSecurity()));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ALLOC_ALL, "200=201609", "200=" + getTradeOrderFixSimTestHelper().getMaturityMonthYear(trade1.getInvestmentSecurity()));
		testContext.getComparisonIgnoreFieldsAllocInstruction().add(57);
		testContext.setTemplateFixOrderId("O_193093_20160727");
		testContext.setTemplateFixAllocationId("A_187416_20160727");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);
		testContext.setExpectedAllocationAckStatus(TradeOrderAllocationStatuses.ACCEPTED);
		testContext.setExpectedAllocationStatus(TradeOrderAllocationStatuses.COMPLETE);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONMB44_TEST_USER_1_MFSU16_INDEX_78_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(new Trade[]{trade1, trade2}, TradeOrderGroupingTypes.DEFAULT, testContext);

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		TradeOrder tmp = this.tradeOrderService.getTradeOrderByTrade(trade2.getId());
		Assertions.assertNotNull(tmp, "Expected a TradeOrder for the Trade.");
		MatcherAssert.assertThat(tradeOrder.getId(), IsEqual.equalTo(tmp.getId()));

		// filled status
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		// allocated status
		Assertions.assertNotNull(tradeOrder.getAllocationStatus(), "Expected an allocation status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getAllocationStatus().getAllocationStatus(), IsEqual.equalTo(TradeOrderAllocationStatuses.COMPLETE));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade1.getId());
		BigDecimal totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(25.00000000).compareTo(totalQuantity), MathUtils.subtract(BigDecimal.valueOf(25.00000000), totalQuantity).toPlainString());

		// trade average unit price
		trade1 = this.tradeService.getTrade(trade1.getId());
		// Was: 1653.768000000000000, Now: 1653.766666000000000 Because the option to force average pricing was turned on
		Assertions.assertEquals(0, BigDecimal.valueOf(1653.766666000000000).compareTo(trade1.getAverageUnitPrice()));

		// trade fill quantities
		tradeFills = this.tradeService.getTradeFillListByTrade(trade2.getId());
		totalQuantity = tradeFills.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		Assertions.assertEquals(0, BigDecimal.valueOf(53.00000000).compareTo(totalQuantity));

		// trade average unit price
		trade2 = this.tradeService.getTrade(trade2.getId());
		// Was: 1653.766037735849057, Now: 1653.766666000000000 Because the option to force average pricing was turned on
		Assertions.assertEquals(0, new BigDecimal("1653.766666000000000").compareTo(trade2.getAverageUnitPrice()));

		Assertions.assertEquals(0, new BigDecimal("1653.766666000000000").compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Single Order (A)
	 * <- Execution Report (5) (A)
	 * <- Execution Report (5) (A)
	 */
	@Test
	public void testRediGoldmanOptions() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0015";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "REDI+", "test_user_1");
		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "SPXW US 07/03/17 C2470", "06/28/2017", new BigDecimal("2.0000000000"), true,
				"REDI Ticket", BusinessCompanies.GOLDMAN_SACHS_AND_CO, TradeBuilder.BUY_TO_CLOSE, testIdentifier);
		trade = this.approveTrade(trade);

		TestContext testContext = createContext(REDI_MULTI_BROKER_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "REDITKTS", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("REDITKTS"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLIFTONMB44", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLIFTONMB44"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=20170703", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "8135=GS", "8135=GSCO");
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "79=157-57198", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_240994_20170628");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLIFTONMB44_TEST_USER_1_SPXW_2_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		// filled status
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(0.300000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), tradeFills.get(0).getNotionalUnitPrice().toPlainString());

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(0.300000000000000).compareTo(trade.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade.getAverageUnitPrice().toPlainString(), BigDecimal.valueOf(0.300000000000000).toPlainString()));
		Assertions.assertEquals(0, BigDecimal.valueOf(0.300000000000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * Single Equity Option trade via WEX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testWex_EquityOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0016";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "WEX (Wolverine Execution)", "test_user_1");

		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "EFA US 04/20/18 C73", "04/11/2018", new BigDecimal("2.0000000000"), false,
				"WEX Ticket", BusinessCompanies.WEX, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(WEX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "WEXOEUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("WEXOEUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201805", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=PARA-WEX", "1=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_290769_20180411");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSUAT_TEST_USER_1_EFA_2_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("2.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("0.010000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");
	}


	/**
	 * Grouped Option trade via WEX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testWex_EquityOption_Trade_Grouped() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0017";

		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "WEX (Wolverine Execution)", "test_user_1");

		Trade trade1 = this.tradeCreator.createOptionsTrade(accountInfo1, "EFA US 04/27/18 C72", "04/17/2018", new BigDecimal("1.0000000000"), false,
				"WEX Ticket", BusinessCompanies.WEX, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade1 = approveTrade(trade1);
		Trade trade2 = this.tradeCreator.createOptionsTrade(accountInfo2, "EFA US 04/27/18 C72", "04/17/2018", new BigDecimal("2.0000000000"), false,
				"WEX Ticket", BusinessCompanies.WEX, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade2 = approveTrade(trade2);

		TestContext testContext = createContext(WEX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "WEXOEUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("WEXOEUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201804", "200=" + DateUtils.fromDate(trade1.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.setTemplateFixOrderId("O_291409_20180417");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSUAT_TEST_USER_1_EFA_3_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(new Trade[]{trade1, trade2}, TradeOrderGroupingTypes.DEFAULT, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("3.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("0.100000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");
	}


	/**
	 * Single Weekly Index Option trade via WEX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (Received) (A)
	 * <- Allocation Acknowledgement (Accepted) (A)
	 */
	@Test
	public void testWex_Weekly_IndexOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0018";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "WEX (Wolverine Execution)", "test_user_1");

		InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
		instrumentSearchForm.setName("S&P 500 Weekly Index Options");
		instrumentSearchForm.setLimit(1);
		List<InvestmentInstrument> instruments = this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm);

		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setInstrumentId(instruments.iterator().next().getId());
		securitySearchForm.setName("SPXW");
		securitySearchForm.setActive(true);
		securitySearchForm.setLimit(1);
		List<InvestmentSecurity> securityList = this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm);
		InvestmentSecurity security = securityList.iterator().next();
		Assertions.assertTrue(security.getSymbol().startsWith("SPXW"));
		BigDecimal strikePrice = QuickFixConverterUtils.getScaledBigDecimal(security.getOptionStrikePrice());
		String putCall = security.getOptionType().toString();
		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, security.getSymbol(), "04/19/2018", new BigDecimal("5.0000000000"), false,
				"WEX Ticket", BusinessCompanies.WEX, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(WEX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "WEXOEUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("WEXOEUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201805", "200=" + DateUtils.fromDate(security.getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "202=2800.00000000", "202=" + strikePrice);
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "201=1", "201=" + ("PUT".equalsIgnoreCase(putCall) ? 0 : 1));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "461=OCEXCS", "461=" + ("PUT".equalsIgnoreCase(putCall) ? "OPEXCS" : "OCEXCS"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "205=02", ("205=" + DateUtils.fromDate(security.getLastDeliveryDate(), "dd")));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=PARA-WEX", "1=" + accountInfo.getHoldingAccount().getNumber());
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "64=20180420", "64=" + DateUtils.fromDate(trade.getSettlementDate(), DateUtils.FIX_DATE_FORMAT_INPUT));
		testContext.setTemplateFixOrderId("O_291464_20180419");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSUAT_TEST_USER_1_SPXW_5_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("5.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("0.450000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");
	}


	/**
	 * Single Index Option trade via WEX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (Received) (A)
	 * <- Allocation Acknowledgement (Accepted) (A)
	 */
	@Test
	public void testWex_IndexOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0019";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "WEX (Wolverine Execution)", "test_user_1");

		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "SPX US 05/18/18 C2765", "04/17/2018", new BigDecimal("4.0000000000"), false,
				"WEX Ticket", BusinessCompanies.WEX, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(WEX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "WEXOEUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("WEXOEUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201805", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=PARA-WEX", "1=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_291410_20180417");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSUAT_TEST_USER_1_SPX_4_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("4.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("12.600000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");
	}


	/**
	 * Stock trade via WEX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 * -> Allocation Instruction (A)
	 * <- Allocation Acknowledgement (Received) (A)
	 * <- Allocation Acknowledgement (Accepted) (A)
	 * <p>
	 * File:  PPAMPLSUAT_TEST_USER_1_IBM_10_00000000.dat
	 */
	@Test
	public void testWex_Stock_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0020";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "WEX (Wolverine Execution)", "test_user_1");

		Trade trade = this.tradeCreator.createStocksTrade(accountInfo, "IBM", "04/17/2018", 10.0, true, 0.00, "WEX Ticket", BusinessCompanies.WEX, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(WEX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "WEXOEUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("WEXOEUAT"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "PPAMPLSUAT", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("PPAMPLSUAT"));
		testContext.setTemplateFixOrderId("O_291413_20180417");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("PPAMPLSUAT_TEST_USER_1_IBM_10_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("10.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("160.960000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");
	}


	/**
	 * Single Equity Option trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_EquityOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0021";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "EFA US 04/27/18 C72", "06/18/2018", new BigDecimal("2.0000000000"), false,
				"Bloomberg EMSX ALGO", BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		String exchangeMICCode = getTradeOrderFixSimTestHelper().getExchangeMICCode(trade);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201804", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "207=XCBO", "207=" + exchangeMICCode);
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=12345", "1=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_291409_20180618");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_EFA_2_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("2.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("71.250000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), String.format("TradeFills Notional Unit Price: Actual %s versus Expected %s", tradeFills.get(0).getNotionalUnitPrice(), new BigDecimal("71.250000000000000").toPlainString()));
		Assertions.assertEquals(0, BigDecimal.valueOf(2.000000000000000).compareTo(tradeFills.get(0).getQuantity()), String.format("TradeFills Quantity Actual %s versus Expected %s", tradeFills.get(0).getQuantity(), new BigDecimal("2.000000000000000").toPlainString()));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(trade.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade.getAverageUnitPrice().toPlainString(), new BigDecimal("71.250000000000000").toPlainString()));
	}


	/**
	 * Grouped Equity Option trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_EquityOptions_Grouped_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0022";

		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		Trade trade1 = this.tradeCreator.createOptionsTrade(accountInfo1, "EFA US 04/27/18 C72", "06/18/2018", new BigDecimal("5.0000000000"), false,
				"Bloomberg EMSX ALGO", BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade1 = approveTrade(trade1);

		Trade trade2 = this.tradeCreator.createOptionsTrade(accountInfo2, "EFA US 04/27/18 C72", "06/18/2018", new BigDecimal("6.0000000000"), false,
				"Bloomberg EMSX ALGO", BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade2 = approveTrade(trade2);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201804", "200=" + DateUtils.fromDate(trade1.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "207=XCBO", "207=" + getTradeOrderFixSimTestHelper().getExchangeMICCode(trade1));
		testContext.setTemplateFixOrderId("O_291410_20180618");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_EFA_11_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(new Trade[]{trade1, trade2}, TradeOrderGroupingTypes.DEFAULT, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("11.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("71.250000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade1.getId());
		Assertions.assertEquals(new BigDecimal("5.0000000000"), trade1.getQuantity(), "Expected trade quantity does not match actual trade quantity.");
		Assertions.assertEquals(1, tradeFills.size(), "Expected trade fill size does not match actual trade fill size.");
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), String.format("Actual %s versus Expected %s", tradeFills.get(0).getNotionalUnitPrice(), new BigDecimal("71.250000000000000").toPlainString()));

		List<TradeFill> tradeFills2 = this.tradeService.getTradeFillListByTrade(trade2.getId());
		Assertions.assertEquals(new BigDecimal("6.0000000000"), trade2.getQuantity(), "Expected trade quantity does not match actual trade quantity.");
		Assertions.assertEquals(1, tradeFills2.size(), "Expected trade fill size does not match actual trade fill size.");
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(tradeFills2.get(0).getNotionalUnitPrice()), String.format("Actual %s versus Expected %s", tradeFills2.get(0).getNotionalUnitPrice(), new BigDecimal("71.250000000000000").toPlainString()));

		// trade average unit price - taken from the allocation report.
		trade1 = this.tradeService.getTrade(trade1.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(trade1.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade1.getAverageUnitPrice().toPlainString(), new BigDecimal("71.250000000000000").toPlainString()));
		// trade average unit price - taken from the allocation report.
		trade2 = this.tradeService.getTrade(trade1.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(trade2.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade1.getAverageUnitPrice().toPlainString(), new BigDecimal("71.250000000000000").toPlainString()));
		Assertions.assertEquals(0, BigDecimal.valueOf(71.250000000000000).compareTo(tradeOrder.getAveragePrice()));
	}


	/**
	 * Single Weekly Index Option trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_WeeklyIndexOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0023";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "SPXW US 05/02/18 C2730", "06/18/2018", new BigDecimal("15.0000000000"), false,
				"Bloomberg EMSX ALGO", BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201805", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=12345", "1=" + accountInfo.getHoldingAccount().getNumber());

		testContext.setTemplateFixOrderId("O_291431_20180418");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_SPXW_15_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("15.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("10.500000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size());
		Assertions.assertEquals(0, BigDecimal.valueOf(10.500000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), String.format("TradeFills Notional Unit Price: Actual %s versus Expected %s", tradeFills.get(0).getNotionalUnitPrice(), new BigDecimal("10.500000000000000").toPlainString()));
		Assertions.assertEquals(0, BigDecimal.valueOf(15.0000000000).compareTo(tradeFills.get(0).getQuantity()), String.format("TradeFills Quantity Actual %s versus Expected %s", tradeFills.get(0).getQuantity(), new BigDecimal("15.0000000000").toPlainString()));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(10.500000000000000).compareTo(trade.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade.getAverageUnitPrice().toPlainString(), new BigDecimal("10.500000000000000").toPlainString()));
	}


	/**
	 * Single Index Option trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_IndexOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0024";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "SPX US 06/15/18 C2625", "06/18/2018", new BigDecimal("12.0000000000"), false,
				"Bloomberg EMSX ALGO", BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201805", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=12345", "1=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_291411_20180618");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_SPX_12_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("12.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("10.500000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size(), "Expected trade fill size does not match actual trade fill size.");
		Assertions.assertEquals(0, BigDecimal.valueOf(10.500000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), String.format("TradeFills Notional Unit Price: Actual %s versus Expected %s", tradeFills.get(0).getNotionalUnitPrice(), new BigDecimal("10.500000000000000").toPlainString()));
		Assertions.assertEquals(0, BigDecimal.valueOf(12.000000000000000).compareTo(tradeFills.get(0).getQuantity()), String.format("TradeFills Quantity Actual %s versus Expected %s", tradeFills.get(0).getQuantity(), new BigDecimal("12.000000000000000").toPlainString()));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(10.500000000000000).compareTo(trade.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade.getAverageUnitPrice().toPlainString(), new BigDecimal("10.500000000000000").toPlainString()));
	}


	/**
	 * Single Stock trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_Stock_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0025";

		TradeDestination tradeDestination = getTradeOrderFixSimTestHelper().createEMSXDestination();
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		getTradeOrderFixSimTestHelper().updateGroupAssociation();

		Trade trade = this.tradeCreator.createStocksTrade(accountInfo, "IBM", "06/18/2018", 13.0000000000d, true, 0.00,
				tradeDestination.getName(), BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "79=7A 45315", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_291412_20180618");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_2005973_13_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("13.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("160.990000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size(), "Expected trade fill size does not match actual trade fill size.");
		Assertions.assertEquals(0, BigDecimal.valueOf(160.990000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), String.format("Actual %s versus Expected %s", tradeFills.get(0).getNotionalUnitPrice(), new BigDecimal("160.990000000000000").toPlainString()));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(160.990000000000000).compareTo(trade.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade.getAverageUnitPrice().toPlainString(), new BigDecimal("160.990000000000000").toPlainString()));
	}


	/**
	 * Single Future Option trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_FuturesOption_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0026";

		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.OPTIONS));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		Trade trade = this.tradeCreator.createOptionsTrade(accountInfo, "S K8C 1030", "06/18/2018", new BigDecimal("16.0000000000"), false,
				"Bloomberg EMSX ALGO", BusinessCompanies.MORGAN_STANLEY_CAPITAL_SERVICES_INC, TradeBuilder.SELL_TO_OPEN, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "200=201804", "200=" + DateUtils.fromDate(trade.getInvestmentSecurity().getEndDate(), "yyyyMM"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "1=12345", "1=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_291433_20180418");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_S_K8C_16_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");

		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("16.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("1050.000000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade quantities
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(new BigDecimal("16.0000000000"), trade.getQuantity(), "Unexpected trade quantity.");
	}


	/**
	 * Single Future trade via EMSX
	 * IMS <-> FIX (A) asynchronous (S) synchronous
	 * -> Single Order (A)
	 * <- Execution Report (New) (A)
	 * <- Execution Report (Filled) (A)
	 */
	@Test
	public void testEMSX_Futures_Trade() {
		final String testIdentifier = "TRADEORDER_INEGRATION_TEST_0027";

		TradeDestination tradeDestination = getTradeOrderFixSimTestHelper().createEMSXDestination();
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_EXECUTION_AND_CLEARING, BusinessCompanies.JP_MORGAN_CHASE, this.tradeUtils.getTradeType(TradeType.FUTURES));
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "Bloomberg", "test_user_1");

		Trade trade = this.tradeCreator.createFuturesTrade(accountInfo, "C K18", "06/18/2018", 17.0000000000d, true,
				tradeDestination.getName(), BusinessCompanies.GOLDMAN_SACHS_AND_CO, testIdentifier);
		trade = approveTrade(trade);

		TestContext testContext = createContext(BLOOMBERG_EMSX_SESSION);
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "BLPTEST", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("BLPTEST"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.GLOBAL, "CLFTPARAEMSX", getTradeOrderFixSimTestHelper().applyDefaultSessionSuffix("CLFTPARAEMSX"));
		testContext.addFixMessageReplacement(ReplacementMapKeys.ORDER_EXECUTION, "79=052-BAJ44", "79=" + accountInfo.getHoldingAccount().getNumber());
		testContext.setTemplateFixOrderId("O_291432_20180418");
		testContext.setExpectedTradeOrderStatus(TradeOrderStatuses.FILLED);

		getTradeOrderFixSimTestHelper().updateFixMessageMap("CLFTPARAEMSX_TEST_USER_1_C_K18_COMDTY_17_00000000.dat", testContext);

		// execute single trade
		TradeOrder tradeOrder = getTradeOrderFixSimTestHelper().executeFixTradeOrder(trade, TradeOrderGroupingTypes.EXECUTE_SEPARATELY, testContext);

		Assertions.assertNotNull(tradeOrder, "A null value was returned from getTradeOrderFixSimTesHelper().executeFixTradeOrder, instead of a valid instance");
		Assertions.assertNotNull(tradeOrder, "Expected a TradeOrder for the Trade.");
		Assertions.assertNotNull(tradeOrder.getStatus(), "Expected a status for the TradeOrder.");
		MatcherAssert.assertThat(tradeOrder.getStatus().getOrderStatus(), IsEqual.equalTo(TradeOrderStatuses.FILLED));
		Assertions.assertEquals(new BigDecimal("17.0000000000"), tradeOrder.getQuantityFilled(), "Expected Quantity Filled does not match actual quantity filled");
		Assertions.assertEquals(new BigDecimal("10.500000000000000"), tradeOrder.getAveragePrice(), "Expected AveragePrice does not match actual Average Price");

		// trade fill quantities
		List<TradeFill> tradeFills = this.tradeService.getTradeFillListByTrade(trade.getId());
		Assertions.assertEquals(1, tradeFills.size(), "Expected trade fill size does not match actual trade fill size.");
		Assertions.assertEquals(0, BigDecimal.valueOf(10.500000000000000).compareTo(tradeFills.get(0).getNotionalUnitPrice()), String.format("Actual %s versus Expected %s", tradeFills.get(0).getNotionalUnitPrice(), new BigDecimal("1050.000000000000000").toPlainString()));

		// trade average unit price - taken from the allocation report.
		trade = this.tradeService.getTrade(trade.getId());
		Assertions.assertEquals(0, BigDecimal.valueOf(10.500000000000000).compareTo(trade.getAverageUnitPrice()), String.format("Actual %s versus Expected %s", trade.getAverageUnitPrice().toPlainString(), new BigDecimal("1050.000000000000000").toPlainString()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TestContext createContext(String sessionName) {
		if (BooleanUtils.isTrue(FIXSIM_SESSIONS.get(sessionName))) {
			// was previously used, reset
			getTradeOrderFixSimTestHelper().getFixSimHelper().resetFixSimSession(getFixSimInstance(), sessionName, getFixSessionSuffix(), getSequenceNumberDifferenceTolerance());
		}
		FIXSIM_SESSIONS.replace(sessionName, true);
		return getTradeOrderFixSimTestHelper().createTestContext(this.fixSimInstance, sessionName);
	}


	private Trade approveTrade(Trade trade) {
		this.tradeUtils.approveTrade(trade);
		return this.tradeService.getTrade(trade.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFixSessionSuffix() {
		return this.fixSessionSuffix;
	}


	public void setFixSessionSuffix(String fixSessionSuffix) {
		this.fixSessionSuffix = fixSessionSuffix;
	}


	public String getFixSimInstance() {
		return this.fixSimInstance;
	}


	public void setFixSimInstance(String fixSimInstance) {
		this.fixSimInstance = fixSimInstance;
	}


	public String getDataSourceDirectory() {
		return this.dataSourceDirectory;
	}


	public void setDataSourceDirectory(String dataSourceDirectory) {
		this.dataSourceDirectory = dataSourceDirectory;
	}


	public int getSequenceNumberDifferenceTolerance() {
		return this.sequenceNumberDifferenceTolerance;
	}


	public void setSequenceNumberDifferenceTolerance(int sequenceNumberDifferenceTolerance) {
		this.sequenceNumberDifferenceTolerance = sequenceNumberDifferenceTolerance;
	}


	public TradeOrderFixSimTestHelper getTradeOrderFixSimTestHelper() {
		return this.tradeOrderFixSimTestHelper;
	}
}
