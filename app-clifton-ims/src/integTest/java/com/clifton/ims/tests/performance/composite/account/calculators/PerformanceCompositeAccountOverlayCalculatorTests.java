package com.clifton.ims.tests.performance.composite.account.calculators;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountOverlayCalculatorTests</code> tests the PerformanceCompositeAccountOverlayCalculator
 * which just copies Portfolio Return and Last Run's Overlay Target for the main run for the account.
 *
 * @author manderson
 */
public class PerformanceCompositeAccountOverlayCalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Test
	public void test_730100() {
		String accountNumber = "730100";

		// These are all overridden because it was using the last run on the p-sum - not really the last run of the month

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("2733310.01"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2015", overrideValueMap);

		overrideValueMap.put("periodEndMarketValue", new BigDecimal("2241031.00"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/28/2015", overrideValueMap);

		overrideValueMap.put("periodEndMarketValue", new BigDecimal("2233785.00"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2015", overrideValueMap);
	}


	@Test
	public void test_713600() {
		// Includes Benchmark Return
		String accountNumber = "713600";

		// These are all overridden because it was using the last run on the p-sum - not really the last run of the month

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("668100192.81"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2015", overrideValueMap);

		overrideValueMap.put("periodEndMarketValue", new BigDecimal("700479675.76"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/28/2015", overrideValueMap);

		overrideValueMap.put("periodEndMarketValue", new BigDecimal("671478183.44"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2015", overrideValueMap);
	}
}
