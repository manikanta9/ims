package com.clifton.ims.tests;

import com.clifton.accounting.eventjournal.eventrollup.AccountingSecurityEventRollup;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionInfo;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.positiontransfer.command.AccountingPositionTransferCommand;
import com.clifton.accounting.positiontransfer.upload.AccountingPositionTransferUploadCustomCommand;
import com.clifton.bloomberg.messages.request.HistoricalDataRequest;
import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractParty;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.fix.identifier.FixIdentifier;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.manager.InvestmentManagerAccountAllocation;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.upload.LendingRepoRollUploadCommand;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.otc.swap.irs.OtcInterestRateSwapEvent;
import com.clifton.otc.swap.trs.OtcTotalReturnSwapEvent;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.product.overlay.trade.group.dynamic.option.ProductOverlayRunTradeGroupDynamicOptionRoll;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListEntity;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.test.protocol.ImsProtocolClientConfiguration;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.file.upload.TradeUploadCommand;
import com.clifton.trade.roll.TradeRollPosition;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>IntegrationTestConfiguration</code> houses configuration defaults for how the core clifton-test project should act
 * in regards to handling various aspects of integration tests for IMS.
 * <p>
 * These defaults revolve around default properties to save and exclude along with various adapter overrides.  It also allows specifying various
 * additional parameters to add to requests for specific URLs.
 *
 * @author apopp
 */
public class ImsIntegrationTestConfiguration implements ImsProtocolClientConfiguration {

	private static final Set<Class<?>> EXCLUDED_CLASSES = CollectionUtils.createHashSet(MultipartFile.class, SystemListItem.class, SystemColumn.class);
	private static final Map<Class<?>, Set<String>> EXCLUDED_PROPERTIES_MAP = new HashMap<>();
	private static final Map<Class<?>, Set<String>> SAVE_NULL_PROPERTIES_MAP = new HashMap<>();
	private static final Map<Class<?>, Set<String>> NESTED_SAVE_PROPERTIES_MAP = new HashMap<>();
	private static final Set<Class<?>> OVERRIDE_CLASSES_ONLY_SEND_ID_WHEN_PRESENT = CollectionUtils.createHashSet(TradeUploadCommand.class);
	private static final Map<Class<?>, JsonDeserializer<?>> TYPE_ADAPTER_MAP = new HashMap<>();


	static {
		EXCLUDED_PROPERTIES_MAP.put(BusinessContract.class, CollectionUtils.createHashSet("securityResource"));
		EXCLUDED_PROPERTIES_MAP.put(BusinessContractParty.class, CollectionUtils.createHashSet("securityResource"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentAccountAssetClass.class, CollectionUtils.createHashSet("clientInvestmentAccount"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentExchange.class, CollectionUtils.createHashSet("openTimeLocal", "closeTimeLocal"));
		EXCLUDED_PROPERTIES_MAP.put(HistoricalDataRequest.class, CollectionUtils.createHashSet("responseClass", "security", "startDate", "endDate", "serviceUrl", "periodicitySelection", "operation"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentAssetClass.class, CollectionUtils.createHashSet("level"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentAccountRelationship.class, CollectionUtils.createHashSet("RelatedAccountID", "referenceTwo.RelatedAccountID"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentManagerAccountAllocation.class, CollectionUtils.createHashSet("absAllocationPercent", "assetClass.level"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentManagerAccountBalanceAdjustment.class, CollectionUtils.createHashSet("autoLoaded", "nextDayAdjustmentApplied"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentReplicationAllocation.class, CollectionUtils.createHashSet("coalesceInvestmentReplicationTypeOverride", "rebalancingUsed", "allocationUniqueString", "allocationLabel"));
		EXCLUDED_PROPERTIES_MAP.put(LendingRepo.class, CollectionUtils.createHashSet("investmentSecurity", "clientInvestmentAccount", "holdingInvestmentAccount", "settlementCurrency"));
		EXCLUDED_PROPERTIES_MAP.put(SystemBeanProperty.class, CollectionUtils.createHashSet("parameter", "multipleValuesAllowed"));
		EXCLUDED_PROPERTIES_MAP.put(SystemBeanPropertyType.class, CollectionUtils.createHashSet("systemUserInterfaceValueConfig", "usedByLabel", "userInterfaceConfig", "virtualProperty"));
		EXCLUDED_PROPERTIES_MAP.put(Trade.class, CollectionUtils.createHashSet("settlementCurrency", "tradeFillList", "sourceEntityId", "collateral", "labelShort", "fixTrade", "transactionDate", "createOrder", "uniqueLabel"));
		EXCLUDED_PROPERTIES_MAP.put(TradeFill.class, CollectionUtils.createHashSet("investmentSecurity", "settlementCurrency", "clientInvestmentAccount", "holdingInvestmentAccount", "traderUser", "buy", "tradeDate", "settlementDate"));
		EXCLUDED_PROPERTIES_MAP.put(TradeRollPosition.class, CollectionUtils.createHashSet("longPosition", "quantity"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentInstruction.class, CollectionUtils.createHashSet("definition", "entityModifyCondition", "reportGroupingUniqueKey"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentInstructionDefinition.class, CollectionUtils.createHashSet("selectorBean", "entityModifyCondition"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentInstructionItem.class, CollectionUtils.createHashSet("reportGroupingUniqueKey"));
		EXCLUDED_PROPERTIES_MAP.put(AccountingPositionTransferUploadCustomCommand.class, CollectionUtils.createHashSet("uploadBeanClass"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentSpecificityEntryProperty.class, CollectionUtils.createHashSet("multipleValuesAllowed"));
		EXCLUDED_PROPERTIES_MAP.put(BusinessCompanyContact.class, CollectionUtils.createHashSet("coalesceContactCompany", "entityModifyCondition"));
		EXCLUDED_PROPERTIES_MAP.put(BusinessContactType.class, CollectionUtils.createHashSet("entityModifyCondition"));
		EXCLUDED_PROPERTIES_MAP.put(BusinessContact.class, CollectionUtils.createHashSet("entityModifyCondition"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentTargetExposureAdjustment.class, CollectionUtils.createHashSet("jsonValue"));
		EXCLUDED_PROPERTIES_MAP.put(InvestmentReplicationAllocation.class, CollectionUtils.createHashSet("replication"));
		EXCLUDED_PROPERTIES_MAP.put(LendingRepoRollUploadCommand.class, CollectionUtils.createHashSet("uploadBeanClass"));
	}


	static {
		SAVE_NULL_PROPERTIES_MAP.put(PerformanceCompositeInvestmentAccount.class, CollectionUtils.createHashSet("accountPerformanceCalculatorBean"));
		SAVE_NULL_PROPERTIES_MAP.put(InvestmentInstructionDefinition.class, CollectionUtils.createHashSet("deliveryType"));
		SAVE_NULL_PROPERTIES_MAP.put(BusinessClient.class, CollectionUtils.createHashSet("terminateDate"));
		SAVE_NULL_PROPERTIES_MAP.put(InvestmentSecurity.class, CollectionUtils.createHashSet("underlyingSecurity"));
		SAVE_NULL_PROPERTIES_MAP.put(SystemTable.class, CollectionUtils.createHashSet("maxQuerySizeLimit"));
		SAVE_NULL_PROPERTIES_MAP.put(ComplianceRuleAssignment.class, CollectionUtils.createHashSet("startDate", "endDate"));
	}


	static {
		NESTED_SAVE_PROPERTIES_MAP.put(BusinessClient.class, CollectionUtils.createHashSet("company"));
		NESTED_SAVE_PROPERTIES_MAP.put(BusinessClientRelationship.class, CollectionUtils.createHashSet("company"));
		NESTED_SAVE_PROPERTIES_MAP.put(LendingRepo.class, CollectionUtils.createHashSet("repoDefinition"));
		NESTED_SAVE_PROPERTIES_MAP.put(SecurityPermissionAssignment.class, CollectionUtils.createHashSet("securityPermission"));
	}


	static {
		TYPE_ADAPTER_MAP.put(AccountingJournalDetailDefinition.class, new AccountingJournalDetailDefinitionAdapter());
		TYPE_ADAPTER_MAP.put(AccountingTransactionInfo.class, new AccountingTransactionInfoAdapter());
		TYPE_ADAPTER_MAP.put(Class.class, new ClassTypeAdapter());
		TYPE_ADAPTER_MAP.put(SystemList.class, new SystemListAdapter());
		TYPE_ADAPTER_MAP.put(SystemColumn.class, new SystemColumnAdapter());
		TYPE_ADAPTER_MAP.put(NotificationDefinition.class, new NotificationDefinitionAdapter());
		TYPE_ADAPTER_MAP.put(MarketDataLive.class, new MarketDataLiveAdapter());
		TYPE_ADAPTER_MAP.put(OtcTotalReturnSwapEvent.class, new AccountingSecurityEventRollupAdapter());
		TYPE_ADAPTER_MAP.put(OtcInterestRateSwapEvent.class, new AccountingSecurityEventRollupAdapter());
		TYPE_ADAPTER_MAP.put(CustomJsonString.class, new CustomJsonStringAdapter());
		TYPE_ADAPTER_MAP.put(FixIdentifier.class, new FixIdentifierAdapter());
	}


	/**
	 * Use {@link ThreadLocalContextHandler} that can be used by IMS Integration
	 * Test Framework to support concurrent test execution.
	 */
	private final ContextHandler contextHandler = new ThreadLocalContextHandler();


	@Override
	public Map<Class<?>, Set<String>> getNestedSavePropertiesMap() {
		return NESTED_SAVE_PROPERTIES_MAP;
	}


	@Override
	public Set<Class<?>> getOverrideClassesOnlySendIdWhenPresent() {
		return OVERRIDE_CLASSES_ONLY_SEND_ID_WHEN_PRESENT;
	}


	@Override
	public Map<Class<?>, Set<String>> getExcludedPropertiesMap() {
		return EXCLUDED_PROPERTIES_MAP;
	}


	@Override
	public Map<Class<?>, Set<String>> getSaveNullPropertiesMap() {
		return SAVE_NULL_PROPERTIES_MAP;
	}


	@Override
	public Set<Class<?>> getExcludedClasses() {
		return EXCLUDED_CLASSES;
	}


	@Override
	public Map<Class<?>, JsonDeserializer<?>> getTypeAdapterMap() {
		return TYPE_ADAPTER_MAP;
	}


	private String getStringParameter(String name, List<NameValuePair> params) {
		NameValuePair tableNameParameter = HttpUtils.getParam(name, params);
		return tableNameParameter != null ? tableNameParameter.getValue() : null;
	}


	@Override
	public void addParameterOverridesForPath(String path, List<NameValuePair> params) {
		if (path.contains("accountingEventJournalListForPreview")) {
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES_ROOT, "accountingEventJournalDetailList"));
		}
		else if (path.contains("accountingJournalBook") && "Task Journal".equals(getStringParameter("journalTypeName", params))) {
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES, "id"));
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES_ROOT, "data"));
		}
		else if ("/tradeGroupDynamicEntry.json".equals(path) && "Option Roll".equals(getStringParameter("tradeGroupType.name", params))) {
			params.add(new BasicNameValuePair(ENABLE_VALIDATING_BINDING, "true"));
			params.add(new BasicNameValuePair(DTO_CLASS_FOR_BINDING, ProductOverlayRunTradeGroupDynamicOptionRoll.class.getName()));
			params.add(new BasicNameValuePair(REQUESTED_MAX_DEPTH, "7"));
		}
		else if ("/tradeUploadFileUpload.json".equals(path)) {
			params.add(new BasicNameValuePair(ENABLE_VALIDATING_BINDING, "true"));
			params.add(new BasicNameValuePair(DISABLE_VALIDATING_BINDING_VALIDATING, "true"));
		}
		else if ("/businessContactAssignmentFileUpload.json".equals(path)) {
			params.add(new BasicNameValuePair(ENABLE_VALIDATING_BINDING, "true"));
			params.add(new BasicNameValuePair(DISABLE_VALIDATING_BINDING_VALIDATING, "true"));
		}
		else if ("/accountingPositionTransferFileUpload.json".equals(path)) {
			params.add(new BasicNameValuePair(ENABLE_VALIDATING_BINDING, "true"));
			params.add(new BasicNameValuePair(DISABLE_VALIDATING_BINDING_VALIDATING, "true"));
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES, "uploadResultsString"));
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES_ROOT, "data"));
		}
		else if ("/accountingPositionTransferListGenerate.json".equals(path)) {
			params.add(new BasicNameValuePair(ENABLE_VALIDATING_BINDING, "true"));
			params.add(new BasicNameValuePair(DTO_CLASS_FOR_BINDING, AccountingPositionTransferCommand.class.getName()));
			// limit response data
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES, "resultString"));
			params.add(new BasicNameValuePair(REQUESTED_PROPERTIES_ROOT, "data"));
		}
		else if ("/notificationDefinition.json".equals(path)) {
			params.add(new BasicNameValuePair(DTO_CLASS_FOR_BINDING, NotificationDefinitionBatch.class.getName()));
		}
	}


	@Override
	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	/**
	 * The <code>AccountingJournalDetailDefinitionAdapter</code> is a JsonDeserializer that instructs Gson to deserialize to AccountingTransaction when an AccountingJournalDetailDefinition is encountered.
	 * This is needed because AccountingJournalDetailDefinition is an interface and thus cannot be instantiated.
	 *
	 * @author jgommels
	 */
	static class AccountingJournalDetailDefinitionAdapter implements JsonDeserializer<AccountingJournalDetailDefinition> {

		@Override
		public AccountingJournalDetailDefinition deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return context.deserialize(json, AccountingTransaction.class);
		}
	}

	/**
	 * The <code>AccountingJournalDetailDefinitionAdapter</code> is a JsonDeserializer that instructs Gson to deserialize to AccountingTransaction when an AccountingTransactionInfo is encountered.
	 * This is needed because AccountingTransactionInfo is an interface and thus cannot be instantiated.
	 *
	 * @author jgommels
	 */
	static class AccountingTransactionInfoAdapter implements JsonDeserializer<AccountingTransactionInfo> {

		@Override
		public AccountingTransactionInfo deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return context.deserialize(json, AccountingTransaction.class);
		}
	}

	/**
	 * The <code>ClassTypeAdapter</code> is a JsonDeserializer that instructs Gson to deserialize a generic Class<?> object
	 *
	 * @author stevenf
	 */
	static class ClassTypeAdapter implements JsonDeserializer<Class<?>> {

		@Override
		public Class<?> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			try {
				return Class.forName(json.getAsString());
			}
			catch (ClassNotFoundException e) {
				throw new RuntimeException(e);
			}
		}
	}

	static class CustomJsonStringAdapter implements JsonDeserializer<CustomJsonString> {

		@Override
		public CustomJsonString deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return new CustomJsonString(json.toString());
		}
	}


	/**
	 * The <code>SystemListAdapter</code> is a JsonDeserializer that instructs Gson to deserialize SystemList to SystemListEntity.
	 * This is needed because SystemList is an abstract class and thus cannot be instantiated.
	 *
	 * @author jgommels
	 */
	static class SystemListAdapter implements JsonDeserializer<SystemList> {

		@Override
		public SystemList deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return context.deserialize(json, SystemListEntity.class);
		}
	}

	static class SystemColumnAdapter implements JsonDeserializer<SystemColumn> {

		@Override
		public SystemColumn deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			return context.deserialize(json, SystemColumnCustom.class);
		}
	}

	static class NotificationDefinitionAdapter implements JsonDeserializer<NotificationDefinition> {

		@Override
		public NotificationDefinition deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			NotificationDefinition result = null;
			if (json.isJsonObject()) {
				JsonObject notificationDefinitionObject = json.getAsJsonObject();
				if (notificationDefinitionObject.getAsJsonPrimitive("instant").getAsBoolean()) {
					result = context.deserialize(json, NotificationDefinitionInstant.class);
				}
				else {
					result = context.deserialize(json, NotificationDefinitionBatch.class);
				}
			}
			return result;
		}
	}

	/**
	 * Adapter to property deserialize {@link MarketDataLive} value based on data type.
	 */
	static final class MarketDataLiveAdapter implements JsonDeserializer<MarketDataLive> {

		@Override
		public MarketDataLive deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
			if (json.isJsonObject()) {
				JsonObject liveObject = json.getAsJsonObject();
				int securityId = liveObject.getAsJsonPrimitive("securityId").getAsInt();
				Date measureDate = DateUtils.toDate(liveObject.getAsJsonPrimitive("measureDate").getAsString());
				boolean numeric = liveObject.getAsJsonPrimitive("numeric").getAsBoolean();
				JsonPrimitive valueJson = liveObject.getAsJsonPrimitive("value");
				if (numeric) {
					return new MarketDataLive(securityId, valueJson.getAsBigDecimal(), measureDate);
				}
				else {
					String fieldName = liveObject.getAsJsonPrimitive("fieldName").getAsString();
					JsonPrimitive dataTypeNameJson = liveObject.getAsJsonPrimitive("dataTypeName");
					DataTypeNames dataTypeName = DataTypeNames.valueOf(dataTypeNameJson.getAsString());
					switch (dataTypeName) {
						case DATE: {
							return new MarketDataLive(securityId, fieldName, dataTypeName, DateUtils.toDate(valueJson.getAsString()), measureDate);
						}
						case BOOLEAN: {
							return new MarketDataLive(securityId, fieldName, dataTypeName, valueJson.getAsBoolean(), measureDate);
						}
						default:
							return new MarketDataLive(securityId, fieldName, dataTypeName, valueJson.getAsString(), measureDate);
					}
				}
			}
			return null;
		}
	}


	static class FixIdentifierAdapter implements JsonDeserializer<FixIdentifier> {

		@Override
		public FixIdentifier deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
			FixIdentifier fixIdentifier = new FixIdentifier();
			JsonObject jsonObject = json.getAsJsonObject();
			fixIdentifier.setId(jsonObject.get("id").getAsInt());
			fixIdentifier.setUniqueStringIdentifier(jsonObject.get("uniqueStringIdentifier").getAsString());
			fixIdentifier.setNewlyCreated(jsonObject.get("newBean").getAsBoolean());
			fixIdentifier.setIdentifierGenerated(jsonObject.get("identifierGenerated").getAsBoolean());
			fixIdentifier.setIdentifierGenerated(jsonObject.get("dropCopyIdentifier").getAsBoolean());
			fixIdentifier.setSourceSystemFkFieldId(jsonObject.get("sourceSystemFKFieldId") != null ? jsonObject.get("sourceSystemFKFieldId").getAsLong() : null);
			fixIdentifier.setPersistent(jsonObject.get("persistent").getAsBoolean());
			return fixIdentifier;
		}
	}


	/**
	 * <code>AccountingSecurityEventRollupAdapter</code> adds functionality to property deserialize {@link AccountingSecurityEventRollup}s with property event legs and payments.
	 */
	static final class AccountingSecurityEventRollupAdapter implements JsonDeserializer<AccountingSecurityEventRollup> {

		@Override
		public AccountingSecurityEventRollup deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) {
			AccountingSecurityEventRollup result = null;
			if (json.isJsonObject()) {
				JsonObject rollupEventJson = json.getAsJsonObject();
				Map<String, String> paymentMappings = new HashMap<>();
				if (OtcInterestRateSwapEvent.class.isAssignableFrom((Class<?>) typeOfT)) {
					OtcInterestRateSwapEvent interestRateSwapEvent = new OtcInterestRateSwapEvent();
					interestRateSwapEvent.setFixedLeg(getMemberByName(context, rollupEventJson, "floatingLeg", InvestmentSecurityEvent.class));
					interestRateSwapEvent.setFloatingLeg(getMemberByName(context, rollupEventJson, "fixedLeg", InvestmentSecurityEvent.class));
					interestRateSwapEvent.setFloatingDetailList(Collections.singletonList(getMemberByName(context, rollupEventJson, "floatingDetail", InvestmentSecurityEventDetail.class)));
					paymentMappings.put(interestRateSwapEvent.getEventLegTypes()[0], "floatingPayment");
					paymentMappings.put(interestRateSwapEvent.getEventLegTypes()[1], "fixedPayment");
					result = interestRateSwapEvent;
				}
				else if (OtcTotalReturnSwapEvent.class.isAssignableFrom((Class<?>) typeOfT)) {
					OtcTotalReturnSwapEvent totalReturnSwapEvent = new OtcTotalReturnSwapEvent();
					totalReturnSwapEvent.setEquityLeg(getMemberByName(context, rollupEventJson, "equityLeg", InvestmentSecurityEvent.class));
					totalReturnSwapEvent.setInterestLeg(getMemberByName(context, rollupEventJson, "interestLeg", InvestmentSecurityEvent.class));
					totalReturnSwapEvent.setDividendLeg(getMemberByName(context, rollupEventJson, "dividendLeg", InvestmentSecurityEvent.class));
					paymentMappings.put(totalReturnSwapEvent.getEventLegTypes()[0], "equityPayment");
					paymentMappings.put(totalReturnSwapEvent.getEventLegTypes()[1], "interestPayment");
					paymentMappings.put(totalReturnSwapEvent.getEventLegTypes()[2], "dividendPayment");
					result = totalReturnSwapEvent;
				}

				if (result != null) {
					result.setId(getMemberIntByName(rollupEventJson, "id"));
					result.setSecurity(getMemberByName(context, rollupEventJson, "security", InvestmentSecurity.class));
					result.setFirstEventJournalDetailId(getMemberIntByName(rollupEventJson, "firstEventJournalDetailId"));
					result.setPaymentPreview(getMemberBooleanByName(rollupEventJson, "paymentPreview"));
					result.setPreviewSkipMessage(getMemberStringByName(rollupEventJson, "previewSkipMessage"));

					for (Map.Entry<String, String> paymentEntry : paymentMappings.entrySet()) {
						String propertyName = paymentEntry.getValue();
						String basePropertyName = propertyName + "Base";
						result.setEventPayment(paymentEntry.getKey(), getMemberBigDecimalByName(rollupEventJson, propertyName));
						result.setEventPaymentBase(paymentEntry.getKey(), getMemberBigDecimalByName(rollupEventJson, basePropertyName));
					}
				}
			}
			return result;
		}


		private <E extends Serializable> E getMemberByName(JsonDeserializationContext context, JsonObject object, String memberName, Class<E> type) {
			return object.has(memberName) ? context.deserialize(object.getAsJsonObject(memberName), type) : null;
		}


		private int getMemberIntByName(JsonObject object, String memberName) {
			return object.has(memberName) ? object.getAsJsonPrimitive(memberName).getAsInt() : 0;
		}


		private BigDecimal getMemberBigDecimalByName(JsonObject object, String memberName) {
			return object.has(memberName) ? object.getAsJsonPrimitive(memberName).getAsBigDecimal() : null;
		}


		private boolean getMemberBooleanByName(JsonObject object, String memberName) {
			return object.has(memberName) ? object.getAsJsonPrimitive(memberName).getAsBoolean() : false;
		}


		private String getMemberStringByName(JsonObject object, String memberName) {
			return object.has(memberName) ? object.getAsJsonPrimitive(memberName).getAsString() : null;
		}
	}
}
