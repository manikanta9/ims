package com.clifton.ims.tests.system.bean;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SystemBeanBuilder {

	private final String systemBeanName;
	private final SystemBeanType systemBeanType;
	private final List<SystemBeanPropertyType> propertyTypes;
	private final SystemBeanService systemBeanService;

	private final Map<String, SystemBeanProperty> propertiesMap = new HashMap<>();


	private SystemBeanBuilder(String systemBeanName, SystemBeanType systemBeanType, List<SystemBeanPropertyType> propertyTypes, SystemBeanService systemBeanService) {
		this.systemBeanName = systemBeanName;
		this.systemBeanType = systemBeanType;
		this.propertyTypes = propertyTypes;
		this.systemBeanService = systemBeanService;
	}


	public static SystemBeanBuilder newBuilder(String systemBeanName, String systemBeanTypeName, SystemBeanService systemBeanService) {
		SystemBeanType systemBeanType = systemBeanService.getSystemBeanTypeByName(systemBeanTypeName);
		List<SystemBeanPropertyType> propertyTypes = systemBeanService.getSystemBeanPropertyTypeListByType(systemBeanType.getId());
		return new SystemBeanBuilder(systemBeanName, systemBeanType, propertyTypes, systemBeanService);
	}


	public SystemBeanBuilder addProperty(String name, String value) {
		return addProperty(name, value, null);
	}


	public SystemBeanBuilder addProperty(String name, String value, String text) {
		SystemBeanPropertyType propertyType = getPropertyType(name);
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(propertyType);
		property.setValue(value);
		property.setText(text);
		this.propertiesMap.put(name, property);

		return this;
	}


	public SystemBeanBuilder addProperty(SystemBeanPropertyType propertyType, String value) {
		SystemBeanProperty property = new SystemBeanProperty();
		property.setType(propertyType);
		property.setValue(value);
		this.propertiesMap.put(propertyType.getName(), property);

		return this;
	}


	public SystemBean build() {
		SystemBean systemBean = new SystemBean();
		systemBean.setName(this.systemBeanName);
		systemBean.setType(this.systemBeanType);
		systemBean.setPropertyList(new ArrayList<>(this.propertiesMap.values()));

		return systemBean;
	}


	public SystemBean buildAndSave() {
		SystemBean systemBean = build();
		this.systemBeanService.saveSystemBean(systemBean);
		return systemBean;
	}


	public SystemBean buildAndGetOrSave() {
		try {
			return buildAndSave();
		}
		catch (Exception e) {
			String existingBeanNameString = StringUtils.substringBetween(e.getCause().getMessage(), "[", "]");
			String[] duplicateBeanNames = existingBeanNameString == null ? null : existingBeanNameString.split(",");
			if (ArrayUtils.isEmpty(duplicateBeanNames)) {
				throw e;
			}
			SystemBean systemBean = build();
			int duplicateCalculatorCount = ArrayUtils.getLength(duplicateBeanNames);
			if (duplicateCalculatorCount == 0) {
				throw new ValidationException(String.format("Unable to create System Bean with type [%s] and name [%s].", systemBean.getType().getName(), systemBean.getName()), e);
			}
			ValidationUtils.assertEquals(duplicateCalculatorCount, 1, String.format("Unable to create System Bean with type [%s] and name [%s] because the following duplicate beans exist [%s].", systemBean.getType().getName(), systemBean.getName(), duplicateBeanNames));
			return getExistingCalculatorByName(build().getType().getId(), duplicateBeanNames[0]);
		}
	}


	private SystemBean getExistingCalculatorByName(int calculatorTypeId, String calculatorName) {
		SystemBeanSearchForm beanSearchForm = new SystemBeanSearchForm();
		beanSearchForm.setTypeId(calculatorTypeId);
		beanSearchForm.addSearchRestriction("name", ComparisonConditions.EQUALS, calculatorName);
		return CollectionUtils.getFirstElementStrict(this.systemBeanService.getSystemBeanList(beanSearchForm));
	}


	private SystemBeanPropertyType getPropertyType(String propertyTypeName) {
		for (SystemBeanPropertyType propertyType : this.propertyTypes) {
			if (propertyTypeName.equalsIgnoreCase(propertyType.getName())) {
				return propertyType;
			}
		}

		throw new ValidationException("The property type with name [" + propertyTypeName + "] does not exist for the system bean type with name [" + this.systemBeanType.getName() + "]");
	}
}
