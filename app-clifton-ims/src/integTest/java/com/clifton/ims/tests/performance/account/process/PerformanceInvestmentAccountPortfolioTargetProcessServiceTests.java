package com.clifton.ims.tests.performance.account.process;

import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountPortfolioTargetProcessServiceTests</code> tests performance summaries for PortfolioTarget run processing
 *
 * @author nickk
 */
public class PerformanceInvestmentAccountPortfolioTargetProcessServiceTests extends BasePerformanceInvestmentAccountProcessServiceTests {

	@Test
	public void testPortfolioTargetRunsPerformanceAccounts() {
		Date periodDate = DateUtils.toDate("11/30/2021");

		// Account Number 051521 - Account migrated to target processing on 11/18/2021; has Gain/Loss overrides
		PerformanceInvestmentAccount performanceSummary = getPerformanceInvestmentAccount("051521", periodDate);
		rebuildSnapshotsForSummary(performanceSummary);
		PerformanceInvestmentAccount previewBean = previewPerformanceInvestmentAccount(performanceSummary.getId());
		validateSummaryPropertiesEqual(performanceSummary, previewBean);
	}
}
