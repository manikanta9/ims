package com.clifton.ims.tests.investment.instruction;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.SimpleEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.ims.tests.workflow.WorkflowTransitioner;
import com.clifton.investment.instruction.InvestmentInstruction;
import com.clifton.investment.instruction.InvestmentInstructionItem;
import com.clifton.investment.instruction.InvestmentInstructionSendingService;
import com.clifton.investment.instruction.InvestmentInstructionService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDelivery;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryService;
import com.clifton.investment.instruction.delivery.InvestmentInstructionDeliveryType;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliverySearchForm;
import com.clifton.investment.instruction.delivery.search.InvestmentInstructionDeliveryTypeSearchForm;
import com.clifton.investment.instruction.process.InvestmentInstructionProcessorService;
import com.clifton.investment.instruction.search.InvestmentInstructionItemSearchForm;
import com.clifton.investment.instruction.search.InvestmentInstructionSearchForm;
import com.clifton.investment.instruction.setup.InvestmentInstructionCategory;
import com.clifton.investment.instruction.setup.InvestmentInstructionContact;
import com.clifton.investment.instruction.setup.InvestmentInstructionDefinition;
import com.clifton.investment.instruction.setup.InvestmentInstructionSetupService;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionContactSearchForm;
import com.clifton.investment.instruction.setup.search.InvestmentInstructionDefinitionSearchForm;
import com.clifton.investment.instruction.status.InvestmentInstructionStatus;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusNames;
import com.clifton.investment.instruction.status.InvestmentInstructionStatusService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Component
public class InvestmentInstructionUtils {

	public static final String INSTRUCTION_SWIFT = "Instruction SWIFT";

	@Resource
	private InvestmentInstructionSetupService investmentInstructionSetupService;

	@Resource
	private InvestmentInstructionStatusService investmentInstructionStatusService;

	@Resource
	private InvestmentInstructionProcessorService investmentInstructionProcessorService;

	@Resource
	private InvestmentInstructionSendingService investmentInstructionSendingService;

	@Resource
	private InvestmentInstructionService investmentInstructionService;

	@Resource
	private SystemBeanService systemBeanService;

	@Resource
	private InvestmentInstructionDeliveryService investmentInstructionDeliveryService;

	@Resource
	private WorkflowTransitioner workflowTransitioner;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean areInstructionItemsWithStatus(InvestmentInstructionCategory category, Date instructionDate, SimpleEntity<Integer> fkObject, InvestmentInstructionStatusNames status, int... items) {
		List<Integer> itemList = Arrays.stream(Objects.isNull(items) ? new int[0] : items).boxed().collect(Collectors.toList());
		Set<InvestmentInstructionStatusNames> instructionStatuses = getInstructionItemList(category, instructionDate, fkObject).stream()
				.filter(i -> itemList.contains(i.getId()))
				.map(InvestmentInstructionItem::getStatus)
				.map(InvestmentInstructionStatus::getInvestmentInstructionStatusName)
				.filter(Predicate.isEqual(status))
				.collect(Collectors.toSet());
		return instructionStatuses.size() == itemList.size();
	}


	public List<InvestmentInstructionItem> getOpenInstructionItems(InvestmentInstructionCategory category, Date instructionDate, SimpleEntity<Integer> fkObject) {
		InvestmentInstructionStatus openStatus = this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames.OPEN);
		Assertions.assertNotNull(openStatus);
		InvestmentInstructionItemSearchForm itemSearchForm = new InvestmentInstructionItemSearchForm();
		itemSearchForm.setCategoryId(category.getId());
		itemSearchForm.setInstructionDate(instructionDate);
		itemSearchForm.setItemStatus(openStatus.getId());
		itemSearchForm.setFkFieldId(fkObject.getId());
		return this.investmentInstructionService.getInvestmentInstructionItemList(itemSearchForm);
	}


	public boolean isRegenerating(InvestmentInstructionCategory category, Date instructionDate) {
		InvestmentInstructionSearchForm searchForm = new InvestmentInstructionSearchForm();
		searchForm.setInstructionCategoryId(category.getId());
		searchForm.setInstructionDate(instructionDate);
		for (InvestmentInstruction investmentInstruction : CollectionUtils.getIterable(this.investmentInstructionService.getInvestmentInstructionList(searchForm))) {
			if (investmentInstruction.isRegenerating()) {
				return true;
			}
		}
		return false;
	}


	public List<InvestmentInstructionDefinition> getInstructionDefinitionList(InvestmentInstructionCategory category) {
		InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setInstructionCategoryId(category.getId());
		searchForm.setWorkflowStatusNameEquals(WorkflowStatus.STATUS_ACTIVE);
		return this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm);
	}


	public List<InvestmentInstructionItem> getSendingInstructionItemList(InvestmentInstructionCategory category, Date instructionDate, SimpleEntity<Integer> fkObject) {
		InvestmentInstructionItemSearchForm sendingItemForm = new InvestmentInstructionItemSearchForm();
		sendingItemForm.setCategoryId(category.getId());
		sendingItemForm.setInstructionDate(instructionDate);
		if (fkObject != null) {
			sendingItemForm.setFkFieldId(fkObject.getId());
		}
		sendingItemForm.setInstructionItemStatusList(new String[]{InvestmentInstructionStatusNames.SEND.toString(), InvestmentInstructionStatusNames.SENDING.toString(), InvestmentInstructionStatusNames.SENT.toString(), InvestmentInstructionStatusNames.SENT_CANCEL.toString()});
		return this.investmentInstructionService.getInvestmentInstructionItemList(sendingItemForm);
	}


	public List<InvestmentInstructionItem> getInstructionItemList(InvestmentInstructionCategory category, Date instructionDate, SimpleEntity<Integer> fkObject) {
		InvestmentInstructionItemSearchForm itemSearchForm = new InvestmentInstructionItemSearchForm();
		itemSearchForm.setCategoryId(category.getId());
		itemSearchForm.setInstructionDate(instructionDate);
		itemSearchForm.setFkFieldId(fkObject.getId());
		return this.investmentInstructionService.getInvestmentInstructionItemList(itemSearchForm);
	}


	public boolean hasSwiftInstructionContact(InvestmentInstruction instruction) {
		InvestmentInstructionDefinition definition = instruction.getDefinition();
		Assertions.assertNotNull(definition);

		InvestmentInstructionContactSearchForm investmentInstructionContactSearchForm = new InvestmentInstructionContactSearchForm();
		investmentInstructionContactSearchForm.setContactGroupId(definition.getContactGroup().getId());
		investmentInstructionContactSearchForm.setCompanyId(instruction.getRecipientCompany().getId());
		investmentInstructionContactSearchForm.setDestinationSystemBeanTypeName(INSTRUCTION_SWIFT);
		List<InvestmentInstructionContact> investmentInstructionContactList = this.investmentInstructionSetupService.getInvestmentInstructionContactList(investmentInstructionContactSearchForm);
		return !investmentInstructionContactList.isEmpty();
	}


	public void createSwiftInstructionContact(InvestmentInstruction instruction) {
		if (!hasSwiftInstructionContact(instruction)) {
			InvestmentInstructionDefinition definition = instruction.getDefinition();
			Assertions.assertNotNull(definition);

			InvestmentInstructionContactSearchForm investmentInstructionContactSearchForm = new InvestmentInstructionContactSearchForm();
			investmentInstructionContactSearchForm.setContactGroupId(definition.getContactGroup().getId());
			investmentInstructionContactSearchForm.setCompanyId(instruction.getRecipientCompany().getId());
			List<InvestmentInstructionContact> investmentInstructionContactList = this.investmentInstructionSetupService.getInvestmentInstructionContactList(investmentInstructionContactSearchForm);
			investmentInstructionContactSearchForm.setDestinationSystemBeanTypeName(INSTRUCTION_SWIFT);

			SystemBeanType instructionSwiftSystemType = this.systemBeanService.getSystemBeanTypeByName(INSTRUCTION_SWIFT);
			Assertions.assertNotNull(instructionSwiftSystemType);

			Optional<InvestmentInstructionContact> swiftContact = investmentInstructionContactList.stream()
					.filter(c -> Objects.equals(instructionSwiftSystemType, c.getDestinationSystemBean().getType()))
					.findFirst();

			if (!swiftContact.isPresent()) {
				InvestmentInstructionContact contact = new InvestmentInstructionContactBuilder("SWIFT", this.systemBeanService, this.investmentInstructionSetupService)
						.startDate(new Date())
						.contactGroup(definition.getContactGroup())
						.company(instruction.getRecipientCompany())
						.build();
				this.investmentInstructionSetupService.saveInvestmentInstructionContact(contact);
			}

			SystemBeanType instructionFaxSystemType = this.systemBeanService.getSystemBeanTypeByName("Instruction FAX");
			Assertions.assertNotNull(instructionFaxSystemType);

			Optional<InvestmentInstructionContact> faxContact = investmentInstructionContactList.stream()
					.filter(c -> Objects.equals(instructionFaxSystemType, c.getDestinationSystemBean().getType()))
					.findFirst();
			faxContact.ifPresent(investmentInstructionContact -> this.investmentInstructionSetupService.deleteInvestmentInstructionContact(investmentInstructionContact.getId()));
		}
	}


	public void activateInstructionDefinition(String categoryName, String definitionName) {
		InvestmentInstructionCategory category = getInvestmentInstructionCategoryByName(categoryName);
		Assertions.assertNotNull(category);

		InvestmentInstructionDefinitionSearchForm searchForm = new InvestmentInstructionDefinitionSearchForm();
		searchForm.setInstructionCategoryId(category.getId());
		searchForm.setName(definitionName);
		List<InvestmentInstructionDefinition> definitionList = this.investmentInstructionSetupService.getInvestmentInstructionDefinitionList(searchForm);
		InvestmentInstructionDefinition definition = CollectionUtils.getFirstElementStrict(definitionList);
		Assertions.assertFalse(definitionList.size() > 1);
		if (!WorkflowStatus.STATUS_ACTIVE.equals(definition.getWorkflowStatus().getName())) {
			this.workflowTransitioner.transitionForTests("Definition Activation for Integration Tests", "Active", "InvestmentInstructionDefinition", BeanUtils.getIdentityAsLong(definition));
		}
	}


	public InvestmentInstructionDeliveryType getInvestmentInstructionDeliveryType(String deliveryTypeName, boolean create) {
		Assertions.assertNotNull(deliveryTypeName);
		InvestmentInstructionDeliveryTypeSearchForm searchForm = new InvestmentInstructionDeliveryTypeSearchForm();
		searchForm.setName(deliveryTypeName);
		return this.investmentInstructionDeliveryService.getInvestmentInstructionDeliveryTypeList(searchForm).stream()
				.findFirst()
				.orElseGet(() -> {
					if (create) {
						InvestmentInstructionDeliveryType deliveryType = new InvestmentInstructionDeliveryType();
						deliveryType.setName(deliveryTypeName);
						deliveryType.setDescription(deliveryTypeName);
						this.investmentInstructionDeliveryService.saveInvestmentInstructionDeliveryType(deliveryType);
						return deliveryType;
					}
					return null;
				});
	}


	public InvestmentInstructionDelivery getInvestmentInstructionDelivery(BusinessCompany deliveryCompany, String deliveryName) {
		InvestmentInstructionDeliverySearchForm searchForm = new InvestmentInstructionDeliverySearchForm();
		searchForm.setDeliveryCompanyId(deliveryCompany.getId());
		return this.investmentInstructionDeliveryService.getInvestmentInstructionDeliveryList(searchForm).stream()
				.filter(d -> Objects.equals(deliveryName, d.getName()))
				.findFirst()
				.orElse(null);
	}


	public InvestmentInstructionDelivery createInvestmentInstructionDelivery(BusinessCompany deliveryCompany, String deliveryName, String deliveryAccountNumber, String aba) {
		InvestmentInstructionDelivery delivery = new InvestmentInstructionDelivery();
		delivery.setName(deliveryName);
		delivery.setDescription(deliveryName);
		delivery.setDeliveryAccountNumber(deliveryAccountNumber);
		delivery.setDeliveryCompany(deliveryCompany);
		delivery.setDeliveryABA(aba);
		this.investmentInstructionDeliveryService.saveInvestmentInstructionDelivery(delivery);
		return delivery;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstructionCategory getInvestmentInstructionCategoryByName(String categoryName) {
		return this.investmentInstructionSetupService.getInvestmentInstructionCategoryByName(categoryName);
	}


	public InvestmentInstructionStatus getInvestmentInstructionStatusByName(InvestmentInstructionStatusNames statusName) {
		return this.investmentInstructionStatusService.getInvestmentInstructionStatusByName(statusName);
	}


	public Status processInvestmentInstructionList(InvestmentInstructionCategory category, Date instructionDate) {
		return this.investmentInstructionProcessorService.processInvestmentInstructionList(category.getId(), instructionDate, true);
	}


	public void sendInvestmentInstructionItemList(int[] instructionItems) {
		this.investmentInstructionSendingService.sendInvestmentInstructionItemList(instructionItems, null);
	}


	public InvestmentInstructionItem saveInvestmentInstructionItem(InvestmentInstructionItem item) {
		return this.investmentInstructionService.saveInvestmentInstructionItem(item);
	}
}
