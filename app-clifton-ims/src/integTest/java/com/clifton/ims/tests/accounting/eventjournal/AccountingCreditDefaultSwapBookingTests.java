package com.clifton.ims.tests.accounting.eventjournal;

import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.test.util.RandomUtils;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingCreditDefaultSwapBookingTests</code> are integration tests that verify the booking logic for trades and credit events pertaining to Credit Default Swaps.
 *
 * @author jgommels
 */
public class AccountingCreditDefaultSwapBookingTests extends BaseImsIntegrationTest {

	private TradeType clearedCreditDefaultSwaps;
	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void beforeTest() {
		this.clearedCreditDefaultSwaps = this.tradeUtils.getTradeType(TradeType.CLEARED_CREDIT_DEFAULT_SWAPS);
	}


	/**
	 * Tests a CDS position open and full close with a factor of 1.
	 */
	@Test
	public void testClearedCdsFullCloseWithFactorOfOne() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("TX1802D17E0500XXI");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.clearedCreditDefaultSwaps);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL);

		// Create and execute a buy trade to open a position
		BigDecimal notional = new BigDecimal("7400000");
		Date buyDate = DateUtils.toDate("7/29/2013");
		BigDecimal openPrice = new BigDecimal("105.65438735");
		Trade buy = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, notional, BigDecimal.ONE, true, openPrice, buyDate, executingBroker, accountInfo).withExchangeRate(new BigDecimal("1.32535")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buy);

		List<? extends AccountingJournalDetailDefinition> actualList = this.accountingUtils.getFirstTradeFillDetails(buy, true);
		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"16126478	null	CA99633427	HA1886343084	Position	TX1802D17E0500XXI	105.65438735	7,400,000	-418,424.66	07/29/2013	1.32535	-554,559.12	-418,424.66	07/29/2013	07/29/2013	Position opening of TX1802D17E0500XXI",
				"16126479	16126478	CA99633427	HA1886343084	Premium Leg	TX1802D17E0500XXI			-42,138.89	07/29/2013	1.32535	-55,848.78	0	07/29/2013	07/29/2013	Accrued Premium Leg for TX1802D17E0500XXI",
				"16126480	16126478	CA99633427	HA1886343084	Clearing Commission	TX1802D17E0500XXI	0.000025	7,400,000	185	07/29/2013	1.32535	245.19	0	07/29/2013	07/29/2013	Clearing Commission expense for TX1802D17E0500XXI",
				"16126481	16126478	CA99633427	HA1886343084	Transaction Fee (Other)	TX1802D17E0500XXI	0.000005	7,400,000	37	07/29/2013	1.32535	49.04	0	07/29/2013	07/29/2013	Transaction Fee (Other) expense for TX1802D17E0500XXI",
				"16126482	16126478	CA99633427	HA1886343084	Currency	EUR			460,341.55	07/29/2013	1.32535	610,113.67	0	07/29/2013	07/29/2013	Currency proceeds from opening of TX1802D17E0500XXI"
		}, false);

		// Create and execute a sell trade to close the position
		Date sellDate = DateUtils.toDate("9/19/2013");
		BigDecimal sellPrice = new BigDecimal("107.06985845");
		Trade sell = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, notional, BigDecimal.ONE, false, sellPrice, sellDate, executingBroker, accountInfo).withExchangeRate(new BigDecimal("1.3348")).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		actualList = this.accountingUtils.getFirstTradeFillDetails(sell, true);
		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"26409805	26409800	CA197640189	HA293343525	Position	TX1802D17E0500XXI	107.06985845	-7,400,000	418,424.66	09/19/2013	1.32535	554,559.12	418,424.66	07/29/2013	09/19/2013	Full position close for TX1802D17E0500XXI",
				"26409806	26409805	CA197640189	HA293343525	Premium Leg	TX1802D17E0500XXI			1,027.78	09/19/2013	1.3348	1,371.88	0	07/29/2013	09/19/2013	Accrued Premium Leg for TX1802D17E0500XXI",
				"26409807	26409805	CA197640189	HA293343525	Realized Gain / Loss	TX1802D17E0500XXI	107.06985845	7,400,000	104,744.87	09/19/2013	1.3348	139,813.45	0	07/29/2013	09/19/2013	Realized Gain / Loss loss for TX1802D17E0500XXI",
				"26409808	26409805	CA197640189	HA293343525	Currency Translation Gain / Loss	TX1802D17E0500XXI			0	09/19/2013	1.3348	3,954.12	0	07/29/2013	09/19/2013	Currency Translation Gain / Loss for TX1802D17E0500XXI",
				"26409809	26409805	CA197640189	HA293343525	Clearing Commission	TX1802D17E0500XXI	0.000025	7,400,000	185	09/19/2013	1.3348	246.94	0	07/29/2013	09/19/2013	Clearing Commission expense for TX1802D17E0500XXI",
				"26409810	26409805	CA197640189	HA293343525	Transaction Fee (Other)	TX1802D17E0500XXI	0.000005	7,400,000	37	09/19/2013	1.3348	49.39	0	07/29/2013	09/19/2013	Transaction Fee (Other) expense for TX1802D17E0500XXI",
				"26409811	26409805	CA197640189	HA293343525	Currency	EUR			-524,419.31	09/19/2013	1.3348	-699,994.9	0	07/29/2013	09/19/2013	Currency expense from close of TX1802D17E0500XXI"
		}, false);
	}


	/**
	 * This integration test opens a Short position in a CDS, then partially closes it with a Buy, then opens another lot with a Sell.
	 * It is intended to almost exactly match a real set of trades that actually occurred.
	 */
	@Test
	public void testThreeTransactionsWithFactorOfOne() {
		InvestmentSecurity swap = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("XI2201J19U0100XXI");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.clearedCreditDefaultSwaps);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL);

		/*
		 **********************************************************************************
		 * First execute and validate a Sell trade of a swap, opening up a Short position.
		 **********************************************************************************
		 */

		// Create and execute a Sell trade to open a Short position
		BigDecimal firstTransactionNotional = new BigDecimal("7450000.00");
		Date firstTransactionDate = DateUtils.toDate("3/20/2014");
		BigDecimal firstTransactionPrice = new BigDecimal("101.40968029");
		Trade firstTransaction = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, firstTransactionNotional, BigDecimal.ONE, false, firstTransactionPrice, firstTransactionDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(firstTransaction);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> actualList = this.accountingUtils.getFirstTradeFillDetails(firstTransaction, true);

		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"16126551	null	CA817857912	HA1652447491	Position	XI2201J19U0100XXI	101.40968029	-7,450,000	105,021.18	03/20/2014	1	105,021.18	105,021.18	03/20/2014	03/20/2014	Position opening of XI2201J19U0100XXI",
				"16126552	16126551	CA817857912	HA1652447491	Premium Leg	XI2201J19U0100XXI			413.89	03/20/2014	1	413.89	0	03/20/2014	03/20/2014	Accrued Premium Leg for XI2201J19U0100XXI",
				"16126553	16126551	CA817857912	HA1652447491	Clearing Commission	XI2201J19U0100XXI	0.000025	7,450,000	186.25	03/20/2014	1	186.25	0	03/20/2014	03/20/2014	Clearing Commission expense for XI2201J19U0100XXI",
				"16126554	16126551	CA817857912	HA1652447491	Transaction Fee (Other)	XI2201J19U0100XXI	0.000006	7,450,000	44.7	03/20/2014	1	44.7	0	03/20/2014	03/20/2014	Transaction Fee (Other) expense for XI2201J19U0100XXI",
				"16126555	16126551	CA817857912	HA1652447491	Cash	USD			-105,666.02	03/20/2014	1	-105,666.02	0	03/20/2014	03/20/2014	Cash expense from opening of XI2201J19U0100XXI"
		}, false);
		/*
		 ****************************************************************************************************************************
		 * Execute and validate a Buy trade of the swap, thus partially closing the position. Since this is a CDS, it should result in
		 * a partial close, followed by a full close of the remaining amount, followed by an opening.
		 ****************************************************************************************************************************
		 */

		// Create and execute a Buy trade to partially close the position
		BigDecimal secondTransactionNotional = new BigDecimal("280000");
		Date secondTransactionDate = DateUtils.toDate("4/1/2014");
		BigDecimal secondTransactionPrice = new BigDecimal("101.59717527");
		Trade secondTransaction = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, secondTransactionNotional, BigDecimal.ONE, true, secondTransactionPrice, secondTransactionDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(secondTransaction);

		// Get the list of details for the trade after it is executed
		actualList = this.accountingUtils.getFirstTradeFillDetails(secondTransaction, true);
		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"16345548	16345543	CA740405551	HA341456646	Position	XI2201J19U0100XXI	101.59717527	280,000	-3,947.1	04/01/2014	1	-3,947.1	-3,947.1	03/20/2014	04/01/2014	Partial position close for XI2201J19U0100XXI",
				"16345549	16345543	CA740405551	HA341456646	Position	XI2201J19U0100XXI	101.40968029	7,170,000	-101,074.08	04/01/2014	1	-101,074.08	-101,074.08	03/20/2014	04/01/2014	Full position close  of XI2201J19U0100XXI",
				"16345550	16345543	CA740405551	HA341456646	Position	XI2201J19U0100XXI	101.40968029	-7,170,000	101,074.08	04/01/2014	1	101,074.08	101,074.08	03/20/2014	04/01/2014	Position opening at new Original Notional for XI2201J19U0100XXI",
				"16345551	16345549	CA740405551	HA341456646	Cash	USD			114,517.45	04/01/2014	1	114,517.45	0	03/20/2014	04/01/2014	Cash proceeds from close of XI2201J19U0100XXI",
				"16345552	16345550	CA740405551	HA341456646	Cash	USD			-114,517.45	04/01/2014	1	-114,517.45	0	03/20/2014	04/01/2014	Cash expense from opening of XI2201J19U0100XXI",
				"16345553	16345548	CA740405551	HA341456646	Premium Leg	XI2201J19U0100XXI			-108.89	04/01/2014	1	-108.89	0	03/20/2014	04/01/2014	Accrued Premium Leg for XI2201J19U0100XXI",
				"16345554	16345548	CA740405551	HA341456646	Realized Gain / Loss	XI2201J19U0100XXI	101.59717527	280,000	-524.99	04/01/2014	1	-524.99	0	03/20/2014	04/01/2014	Realized Gain / Loss gain for XI2201J19U0100XXI",
				"16345555	16345548	CA740405551	HA341456646	Clearing Commission	XI2201J19U0100XXI	0.000025	280,000	7	04/01/2014	1	7	0	03/20/2014	04/01/2014	Clearing Commission expense for XI2201J19U0100XXI",
				"16345556	16345548	CA740405551	HA341456646	Transaction Fee (Other)	XI2201J19U0100XXI	0.000006	280,000	1.68	04/01/2014	1	1.68	0	03/20/2014	04/01/2014	Transaction Fee (Other) expense for XI2201J19U0100XXI",
				"16345557	16345548	CA740405551	HA341456646	Cash	USD			4,572.3	04/01/2014	1	4,572.3	0	03/20/2014	04/01/2014	Cash proceeds from close of XI2201J19U0100XXI"
		}, false);

		/*
		 **********************************************************************************
		 * Finally, execute and validate another Sell trade of the swap to open a new lot.
		 **********************************************************************************
		 */

		// Create and execute a Sell trade to open a new lot
		BigDecimal thirdTransactionNotional = new BigDecimal("690000.00");
		Date thirdTransactionDate = DateUtils.toDate("5/30/2014");
		BigDecimal thirdTransactionPrice = new BigDecimal("101.8329824");
		Trade thirdTransaction = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, thirdTransactionNotional, BigDecimal.ONE, false, thirdTransactionPrice, thirdTransactionDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(thirdTransaction);

		// Get the list of details for the trade after it is executed
		actualList = this.accountingUtils.getFirstTradeFillDetails(thirdTransaction, true);

		AccountingUtils.assertJournalDetailList(actualList, new String[]{
				"16126564	null	CA817857912	HA1652447491	Position	XI2201J19U0100XXI	101.8329824	-690,000	12,647.58	05/30/2014	1	12,647.58	12,647.58	05/30/2014	05/30/2014	Position opening of XI2201J19U0100XXI",
				"16126565	16126564	CA817857912	HA1652447491	Premium Leg	XI2201J19U0100XXI			1,399.17	05/30/2014	1	1,399.17	0	05/30/2014	05/30/2014	Accrued Premium Leg for XI2201J19U0100XXI",
				"16126566	16126564	CA817857912	HA1652447491	Clearing Commission	XI2201J19U0100XXI	0.000025	690,000	17.25	05/30/2014	1	17.25	0	05/30/2014	05/30/2014	Clearing Commission expense for XI2201J19U0100XXI",
				"16126567	16126564	CA817857912	HA1652447491	Transaction Fee (Other)	XI2201J19U0100XXI	0.000006	690,000	4.14	05/30/2014	1	4.14	0	05/30/2014	05/30/2014	Transaction Fee (Other) expense for XI2201J19U0100XXI",
				"16126568	16126564	CA817857912	HA1652447491	Cash	USD			-14,068.14	05/30/2014	1	-14,068.14	0	05/30/2014	05/30/2014	Cash expense from opening of XI2201J19U0100XXI"
		}, false);
	}


	/**
	 * This integration test performs the following steps and verifies each one:
	 * <p>
	 * <ol>
	 * <li>Opens a Long CDS Position with a factor of 1</li>
	 * <li>Generates a Credit Event for the CDS, resulting in a notional reduction</li>
	 * <li>Partially closes the Long position</li>
	 * <li>Fully closes the Long position and opens a Short position</li>
	 * <li>Generates another Credit Event for the CDS, resulting in another notional reduction</li>
	 * <li>Partially closes the Short position</li>
	 * <li>Fully closes the Short position</li>
	 * </ol>
	 */
	@Test
	public void testFiveTradesAndTwoCreditEvents() {
		String newSwapSymbol = RandomUtils.randomNameAndNumber().toUpperCase();
		InvestmentSecurity swap = this.investmentInstrumentUtils.getCopyOfSecurityAndUnderlyingFromTemplate("XI2201J19U0100XXI", "CDXIG522", newSwapSymbol);
		InvestmentSecurity underlyingSecurity = swap.getUnderlyingSecurity();

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.clearedCreditDefaultSwaps);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL);

		/*
		 **********************************************************************************
		 * STEP 1: Execute and validate a Buy trade of a swap, opening up a Long position.
		 **********************************************************************************
		 */

		BigDecimal firstTradeNotional = new BigDecimal("1000000.00");
		String firstTradeDateString = "4/7/2014";
		Date firstTradeDate = DateUtils.toDate(firstTradeDateString);
		BigDecimal firstTradePrice = new BigDecimal("101.1");
		Trade firstTrade = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, firstTradeNotional, BigDecimal.ONE, true, firstTradePrice, firstTradeDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(firstTrade);

		List<? extends AccountingJournalDetailDefinition> actualList = this.accountingUtils.getFirstTradeFillDetails(firstTrade, true);

		String[] expectedResults = Arrays.stream(
						new String[]{
								"16126851	null	CA953544306	HA2115135629	Position	AMEMARTENHALTEN-27	101.1	1,000,000	-11,000	04/07/2014	1	-11,000	-11,000	04/07/2014	04/07/2014	Position opening of AMEMARTENHALTEN-27",
								"16126852	16126851	CA953544306	HA2115135629	Premium Leg	AMEMARTENHALTEN-27			-555.56	04/07/2014	1	-555.56	0	04/07/2014	04/07/2014	Accrued Premium Leg for AMEMARTENHALTEN-27",
								"16126853	16126851	CA953544306	HA2115135629	Clearing Commission	AMEMARTENHALTEN-27	0.000025	1,000,000	25	04/07/2014	1	25	0	04/07/2014	04/07/2014	Clearing Commission expense for AMEMARTENHALTEN-27",
								"16126854	16126851	CA953544306	HA2115135629	Cash	USD			11,530.56	04/07/2014	1	11,530.56	0	04/07/2014	04/07/2014	Cash proceeds from opening of AMEMARTENHALTEN-27"
						})
				.map(s -> s.replaceAll("AMEMARTENHALTEN-27", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);

		/*
		 **********************************************************************************
		 * STEP 2: Execute and validate a Credit Event for the swap, thus reducing the notional value of the position.
		 **********************************************************************************
		 */
		BigDecimal newFactor = new BigDecimal("0.99");
		BigDecimal recoveryRate = new BigDecimal("8.5");
		Date creditEventDate = DateUtils.toDate("4/14/2014");
		Date creditEventPaymentDate = DateUtils.toDate("4/15/2014");

		InvestmentSecurityEvent creditEvent = new InvestmentSecurityEvent();
		creditEvent.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CREDIT_EVENT));
		creditEvent.setSecurity(underlyingSecurity);
		creditEvent.setBeforeEventValue(BigDecimal.ONE);
		creditEvent.setAfterEventValue(newFactor);
		creditEvent.setAdditionalEventValue(recoveryRate);
		creditEvent.setDeclareDate(DateUtils.toDate("03/19/2014"));
		creditEvent.setRecordDate(DateUtils.toDate("06/19/2014"));
		creditEvent.setEventDate(creditEventDate);
		creditEvent.setAdditionalDate(creditEventDate);
		creditEvent.setExDate(creditEventPaymentDate);
		creditEvent.setEventDescription("Fake Credit Event generated by integration test.");
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(creditEvent);// should create 2 events: underlying and copy it to the swap

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(swap.getId());
		searchForm.setTypeId(creditEvent.getType().getId());
		searchForm.setEventDate(creditEvent.getEventDate());
		searchForm.setEventDescription(creditEvent.getEventDescription());
		List<InvestmentSecurityEvent> eventList = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		ValidationUtils.assertEquals(1, CollectionUtils.getSize(eventList), "Must have exactly one security event " + creditEvent + " for security " + underlyingSecurity);
		creditEvent = eventList.get(0);

		actualList = this.accountingUtils.generateAndPostEventJournal(creditEvent, accountInfo.getClientAccount()).getMainBookedJournal().getJournalDetailList();
		expectedResults = Arrays.stream(
						new String[]{
								"16346465	16346461	CA1931163545	HA1977020257	Position	FIRELSANHALDETH-386	101.1	-10,000	110	04/14/2014	1	110	110	04/07/2014	04/14/2014	1 to 0.99 Credit Event for FIRELSANHALDETH-386 on 04/14/2014",
								"16346466	16346465	CA1931163545	HA1977020257	Realized Gain / Loss	FIRELSANHALDETH-386	101.1		-10,110	04/14/2014	1	-10,110	0	04/07/2014	04/14/2014	Realized Gain / Loss from Credit Event for FIRELSANHALDETH-386 on 04/14/2014",
								"16346467	16346465	CA1931163545	HA1977020257	Principal Losses	FIRELSANHALDETH-386	101.1		850	04/14/2014	1	850	0	04/07/2014	04/14/2014	Principal Losses from Credit Event for FIRELSANHALDETH-386",
								"16346468	16346461	CA1931163545	HA1977020257	Premium Leg	FIRELSANHALDETH-386			7.22	04/14/2014	1	7.22	0	04/07/2014	04/14/2014	Premium Leg from Credit Event for FIRELSANHALDETH-386 on 04/14/2014",
								"16346469	16346461	CA1931163545	HA1977020257	Cash	USD			-7.22	04/14/2014	1	-7.22	0	04/07/2014	04/14/2014	Cash expense from Credit Event of FIRELSANHALDETH-386",
								"16346470	16346465	CA1931163545	HA1977020257	Cash	USD			9,150	04/14/2014	1	9,150	0	04/07/2014	04/14/2014	Cash proceeds from Credit Event of FIRELSANHALDETH-386"
						})
				.map(s -> s.replaceAll("FIRELSANHALDETH-386", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);

		/*
		 **********************************************************************************
		 * STEP 3: Execute and validate a Sell trade of the swap, partially closing the Long position.
		 **********************************************************************************
		 */

		BigDecimal secondTradeNotional = new BigDecimal("500000.00");
		Date secondTradeDate = DateUtils.toDate("4/21/2014");
		BigDecimal secondTradePrice = new BigDecimal("101.2");
		Trade secondTrade = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, secondTradeNotional, newFactor, false, secondTradePrice, secondTradeDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(secondTrade);

		actualList = this.accountingUtils.getFirstTradeFillDetails(secondTrade, true);
		expectedResults = Arrays.stream(
						new String[]{
								"16347100	16347090	CA1117517861	HA1753589919	Position	FIRTHUSTHIDRIM-421	101.2	-495,000	5,445	04/21/2014	1	5,445	5,445	04/07/2014	04/21/2014	Partial position close  at Original Notional for FIRTHUSTHIDRIM-421",
								"16347101	16347090	CA1117517861	HA1753589919	Position	FIRTHUSTHIDRIM-421	101.1	-495,000	5,445	04/21/2014	1	5,445	5,445	04/07/2014	04/21/2014	Full position close  of FIRTHUSTHIDRIM-421",
								"16347102	16347090	CA1117517861	HA1753589919	Position	FIRTHUSTHIDRIM-421	101.1	500,000	-5,500	04/21/2014	1	-5,500	-5,500	04/07/2014	04/21/2014	Position opening at new Original Notional for FIRTHUSTHIDRIM-421",
								"16347103	16347102	CA1117517861	HA1753589919	Position	FIRTHUSTHIDRIM-421	101.1	-5,000	55	04/21/2014	1	55	55	04/07/2014	04/21/2014	Reducing Original Notional to Current Notional for FIRTHUSTHIDRIM-421",
								"16347104	16347101	CA1117517861	HA1753589919	Cash	USD			-5,940	04/21/2014	1	-5,940	0	04/07/2014	04/21/2014	Cash expense from close of FIRTHUSTHIDRIM-421",
								"16347105	16347102	CA1117517861	HA1753589919	Cash	USD			6,000	04/21/2014	1	6,000	0	04/07/2014	04/21/2014	Cash proceeds from opening of FIRTHUSTHIDRIM-421",
								"16347106	16347103	CA1117517861	HA1753589919	Cash	USD			-60	04/21/2014	1	-60	0	04/07/2014	04/21/2014	Cash expense from close of FIRTHUSTHIDRIM-421",
								"16347107	16347100	CA1117517861	HA1753589919	Premium Leg	FIRTHUSTHIDRIM-421			472.22	04/21/2014	1	472.22	0	04/07/2014	04/21/2014	Accrued Premium Leg for FIRTHUSTHIDRIM-421",
								"16347108	16347100	CA1117517861	HA1753589919	Realized Gain / Loss	FIRTHUSTHIDRIM-421	101.2	495,000	495	04/21/2014	1	495	0	04/07/2014	04/21/2014	Realized Gain / Loss loss for FIRTHUSTHIDRIM-421",
								"16347109	16347100	CA1117517861	HA1753589919	Clearing Commission	FIRTHUSTHIDRIM-421	0.00002525	495,000	12.5	04/21/2014	1	12.5	0	04/07/2014	04/21/2014	Clearing Commission expense for FIRTHUSTHIDRIM-421",
								"16347110	16347100	CA1117517861	HA1753589919	Cash	USD			-6,424.72	04/21/2014	1	-6,424.72	0	04/07/2014	04/21/2014	Cash expense from close of FIRTHUSTHIDRIM-421"
						})
				.map(s -> s.replaceAll("FIRTHUSTHIDRIM-421", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);

		/*
		 **********************************************************************************
		 * STEP 4: Execute and validate a Sell trade of the swap that will fully close the existing Long position and open a Short position.
		 **********************************************************************************
		 */

		BigDecimal thirdTradeNotional = new BigDecimal("1000000.00");
		Date thirdTradeDate = DateUtils.toDate("4/22/2014");
		BigDecimal thirdTradePrice = new BigDecimal("101.3");
		Trade thirdTrade = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, thirdTradeNotional, newFactor, false, thirdTradePrice, thirdTradeDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(thirdTrade);

		actualList = this.accountingUtils.getFirstTradeFillDetails(thirdTrade, true);
		expectedResults = Arrays.stream(
						new String[]{
								"16347132	null	CA227332758	HA1387787972	Position	LUEQUEMAHHAL-270	101.3	-500,000	6,500	04/22/2014	1	6,500	6,500	04/22/2014	04/22/2014	Position opening at new Original Notional for LUEQUEMAHHAL-270",
								"16347133	16347123	CA227332758	HA1387787972	Position	LUEQUEMAHHAL-270	101.3	-495,000	5,445	04/22/2014	1	5,445	5,445	04/07/2014	04/22/2014	Full position close  at Original Notional for LUEQUEMAHHAL-270",
								"16347134	16347132	CA227332758	HA1387787972	Position	LUEQUEMAHHAL-270	101.3	5,000	-65	04/22/2014	1	-65	-65	04/22/2014	04/22/2014	Reducing Original Notional to Current Notional for LUEQUEMAHHAL-270",
								"16347135	16347134	CA227332758	HA1387787972	Cash	USD			65	04/22/2014	1	65	0	04/22/2014	04/22/2014	Cash proceeds from close of LUEQUEMAHHAL-270",
								"16347136	16347132	CA227332758	HA1387787972	Premium Leg	LUEQUEMAHHAL-270			486.11	04/22/2014	1	486.11	0	04/22/2014	04/22/2014	Accrued Premium Leg for LUEQUEMAHHAL-270",
								"16347137	16347133	CA227332758	HA1387787972	Premium Leg	LUEQUEMAHHAL-270			486.11	04/22/2014	1	486.11	0	04/07/2014	04/22/2014	Accrued Premium Leg for LUEQUEMAHHAL-270",
								"16347138	16347133	CA227332758	HA1387787972	Realized Gain / Loss	LUEQUEMAHHAL-270	101.3	495,000	990	04/22/2014	1	990	0	04/07/2014	04/22/2014	Realized Gain / Loss loss for LUEQUEMAHHAL-270",
								"16347139	16347132	CA227332758	HA1387787972	Clearing Commission	LUEQUEMAHHAL-270	0.000025	500,000	12.5	04/22/2014	1	12.5	0	04/22/2014	04/22/2014	Clearing Commission expense for LUEQUEMAHHAL-270",
								"16347140	16347133	CA227332758	HA1387787972	Clearing Commission	LUEQUEMAHHAL-270	0.00002525	495,000	12.5	04/22/2014	1	12.5	0	04/07/2014	04/22/2014	Clearing Commission expense for LUEQUEMAHHAL-270",
								"16347141	16347132	CA227332758	HA1387787972	Cash	USD			-6,998.61	04/22/2014	1	-6,998.61	0	04/22/2014	04/22/2014	Cash expense from opening of LUEQUEMAHHAL-270",
								"16347142	16347133	CA227332758	HA1387787972	Cash	USD			-6,933.61	04/22/2014	1	-6,933.61	0	04/07/2014	04/22/2014	Cash expense from close of LUEQUEMAHHAL-270",
						})
				.map(s -> s.replaceAll("LUEQUEMAHHAL-270", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);

		/*
		 **********************************************************************************
		 * STEP 5: Execute and validate another Credit Event for the swap, thus reducing the notional value of the position.
		 **********************************************************************************
		 */
		BigDecimal secondCreditEventFactor = new BigDecimal("0.98");
		BigDecimal secondCreditEventRecoveryRate = new BigDecimal("8.5");
		Date secondCreditEventDate = DateUtils.toDate("4/24/2014");
		Date secondCreditEventPaymentDate = DateUtils.toDate("4/25/2014");

		creditEvent = new InvestmentSecurityEvent();
		creditEvent.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CREDIT_EVENT));
		creditEvent.setSecurity(underlyingSecurity);
		creditEvent.setBeforeEventValue(newFactor);
		creditEvent.setAfterEventValue(secondCreditEventFactor);
		creditEvent.setAdditionalEventValue(secondCreditEventRecoveryRate);
		creditEvent.setDeclareDate(DateUtils.toDate("03/19/2014"));
		creditEvent.setRecordDate(DateUtils.toDate("06/19/2014"));
		creditEvent.setEventDate(secondCreditEventDate);
		creditEvent.setAdditionalDate(secondCreditEventDate);
		creditEvent.setExDate(secondCreditEventPaymentDate);
		creditEvent.setEventDescription("Fake Credit Event generated by integration test.");
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(creditEvent);// should create 2 events: underlying and copy it to the swap

		searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(swap.getId());
		searchForm.setTypeId(creditEvent.getType().getId());
		searchForm.setEventDate(creditEvent.getEventDate());
		searchForm.setEventDescription(creditEvent.getEventDescription());
		eventList = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		ValidationUtils.assertEquals(1, CollectionUtils.getSize(eventList), "Must have exactly one security event " + creditEvent + " for security " + underlyingSecurity);
		creditEvent = eventList.get(0);

		actualList = this.accountingUtils.generateAndPostEventJournal(creditEvent, accountInfo.getClientAccount()).getMainBookedJournal().getJournalDetailList();
		expectedResults = Arrays.stream(
						new String[]{
								"16347175	16347164	CA29983692	HA1503385938	Position	ANSRAETHUS-126	101.3	5,000	-65	04/24/2014	1	-65	-65	04/22/2014	04/24/2014	0.99 to 0.98 Credit Event for ANSRAETHUS-126 on 04/24/2014",
								"16347176	16347175	CA29983692	HA1503385938	Realized Gain / Loss	ANSRAETHUS-126	101.3		5,065	04/24/2014	1	5,065	0	04/22/2014	04/24/2014	Realized Gain / Loss from Credit Event for ANSRAETHUS-126 on 04/24/2014",
								"16347177	16347175	CA29983692	HA1503385938	Principal Losses	ANSRAETHUS-126	101.3		-425	04/24/2014	1	-425	0	04/22/2014	04/24/2014	Principal Losses from Credit Event for ANSRAETHUS-126",
								"16347178	16347164	CA29983692	HA1503385938	Premium Leg	ANSRAETHUS-126			-5	04/24/2014	1	-5	0	04/22/2014	04/24/2014	Premium Leg from Credit Event for ANSRAETHUS-126 on 04/24/2014",
								"16347179	16347164	CA29983692	HA1503385938	Cash	USD			5	04/24/2014	1	5	0	04/22/2014	04/24/2014	Cash proceeds from Credit Event of ANSRAETHUS-126",
								"16347180	16347175	CA29983692	HA1503385938	Cash	USD			-4,575	04/24/2014	1	-4,575	0	04/22/2014	04/24/2014	Cash expense from Credit Event of ANSRAETHUS-126"
						})
				.map(s -> s.replaceAll("ANSRAETHUS-126", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);

		/*
		 **********************************************************************************
		 * STEP 6: Execute and validate a Buy trade for the swap, thus partially closing the position.
		 **********************************************************************************
		 */

		BigDecimal fourthTradeNotional = new BigDecimal("300000.00");
		Date fourthTradeDate = DateUtils.toDate("4/28/2014");
		BigDecimal fourthTradePrice = new BigDecimal("101.4");
		Trade fourthTrade = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, fourthTradeNotional, secondCreditEventFactor, true, fourthTradePrice, fourthTradeDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(fourthTrade);

		actualList = this.accountingUtils.getFirstTradeFillDetails(fourthTrade, true);
		expectedResults = Arrays.stream(
						new String[]{
								"16347219	16347202	CA584144231	HA2052283020	Position	DAESANTHERTHUSTEN-1	101.4	294,000	-3,822	04/28/2014	1	-3,822	-3,822	04/22/2014	04/28/2014	Partial position close  at Original Notional for DAESANTHERTHUSTEN-1",
								"16347220	16347202	CA584144231	HA2052283020	Position	DAESANTHERTHUSTEN-1	101.3	196,000	-2,548	04/28/2014	1	-2,548	-2,548	04/22/2014	04/28/2014	Full position close  at new Original Notional for DAESANTHERTHUSTEN-1",
								"16347221	16347202	CA584144231	HA2052283020	Position	DAESANTHERTHUSTEN-1	101.3	-200,000	2,600	04/28/2014	1	2,600	2,600	04/22/2014	04/28/2014	Position opening at new Original Notional for DAESANTHERTHUSTEN-1",
								"16347222	16347221	CA584144231	HA2052283020	Position	DAESANTHERTHUSTEN-1	101.3	4,000	-52	04/28/2014	1	-52	-52	04/22/2014	04/28/2014	Reducing Original Notional to Current Notional for DAESANTHERTHUSTEN-1",
								"16347223	16347220	CA584144231	HA2052283020	Cash	USD			2,744	04/28/2014	1	2,744	0	04/22/2014	04/28/2014	Cash proceeds from close of DAESANTHERTHUSTEN-1",
								"16347224	16347221	CA584144231	HA2052283020	Cash	USD			-2,800	04/28/2014	1	-2,800	0	04/22/2014	04/28/2014	Cash expense from opening of DAESANTHERTHUSTEN-1",
								"16347225	16347222	CA584144231	HA2052283020	Cash	USD			56	04/28/2014	1	56	0	04/22/2014	04/28/2014	Cash proceeds from close of DAESANTHERTHUSTEN-1",
								"16347226	16347223	CA584144231	HA2052283020	Premium Leg	DAESANTHERTHUSTEN-1			-341.67	04/28/2014	1	-341.67	0	04/22/2014	04/28/2014	Accrued Premium Leg for DAESANTHERTHUSTEN-1",
								"16347227	16347224	CA584144231	HA2052283020	Realized Gain / Loss	DAESANTHERTHUSTEN-1	101.4	294,000	-294	04/28/2014	1	-294	0	04/22/2014	04/28/2014	Realized Gain / Loss gain for DAESANTHERTHUSTEN-1",
								"16347225	16347225	CA584144231	HA2052283020	Clearing Commission	DAESANTHERTHUSTEN-1	0.0000255	294,000	7.5	04/28/2014	1	7.5	0	04/22/2014	04/28/2014	Clearing Commission expense for DAESANTHERTHUSTEN-1",
								"16347226	16347226	CA584144231	HA2052283020	Cash	USD			4,450.17	04/28/2014	1	4,450.17	0	04/22/2014	04/28/2014	Cash proceeds from close of DAESANTHERTHUSTEN-1"
						})
				.map(s -> s.replaceAll("DAESANTHERTHUSTEN-1", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);

		/*
		 **********************************************************************************
		 * STEP 7: Execute and validate a Buy trade for the swap, thus partially closing the position.
		 **********************************************************************************
		 */

		BigDecimal fifthTradeNotional = new BigDecimal("200000.00");
		Date fifthTradeDate = DateUtils.toDate("4/29/2014");
		BigDecimal fifthTradePrice = new BigDecimal("101.5");
		Trade fifthTrade = this.tradeUtils.newClearedCreditDefaultSwapTradeBuilder(swap, fifthTradeNotional, secondCreditEventFactor, true, fifthTradePrice, fifthTradeDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(fifthTrade);

		actualList = this.accountingUtils.getFirstTradeFillDetails(fifthTrade, true);
		expectedResults = Arrays.stream(
						new String[]{
								"16126891	16126885	CA953544306	HA2115135629	Position	AMEMARTENHALTEN-27	101.5	196,000	-2,548	04/29/2014	1	-2,548	-2,548	04/22/2014	04/29/2014	Full position close  at Original Notional for AMEMARTENHALTEN-27",
								"16126892	16126891	CA953544306	HA2115135629	Premium Leg	AMEMARTENHALTEN-27			-233.33	04/29/2014	1	-233.33	0	04/22/2014	04/29/2014	Accrued Premium Leg for AMEMARTENHALTEN-27",
								"16126893	16126891	CA953544306	HA2115135629	Realized Gain / Loss	AMEMARTENHALTEN-27	101.5	196,000	-392	04/29/2014	1	-392	0	04/22/2014	04/29/2014	Realized Gain / Loss gain for AMEMARTENHALTEN-27",
								"16126894	16126891	CA953544306	HA2115135629	Clearing Commission	AMEMARTENHALTEN-27	0.0000255	196,000	5	04/29/2014	1	5	0	04/22/2014	04/29/2014	Clearing Commission expense for AMEMARTENHALTEN-27",
								"16126895	16126891	CA953544306	HA2115135629	Cash	USD			3,168.33	04/29/2014	1	3,168.33	0	04/22/2014	04/29/2014	Cash proceeds from close of AMEMARTENHALTEN-27"
						})
				.map(s -> s.replaceAll("AMEMARTENHALTEN-27", newSwapSymbol))
				.toArray(String[]::new);
		AccountingUtils.assertJournalDetailList(actualList, expectedResults, false);
	}
}
