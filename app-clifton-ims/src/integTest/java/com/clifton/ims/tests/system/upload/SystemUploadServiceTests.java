package com.clifton.ims.tests.system.upload;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.system.upload.SystemUploadCommand;
import com.clifton.system.upload.SystemUploadService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * The SystemUploadServiceTests tests generic upload data for {@link com.clifton.marketdata.field.MarketDataValue} objects
 * in various formats
 * <p>
 * SET TO IGNORE FOR NOW UNTIL INTEGRATION TESTS CAN HANDLE FILES
 *
 * @author manderson
 */

public class SystemUploadServiceTests extends BaseImsIntegrationTest {

	@Resource
	private SystemUploadService systemUploadService;


	@Test
	public void testUploadFile_xls() {
		SystemUploadCommand upload = setupSystemUploadBean("MarketDataValueTestUpload_xls.xls");
		this.systemUploadService.uploadSystemUploadFile(upload);
	}


	@Test
	public void testUploadFile_xlsx() {
		SystemUploadCommand upload = setupSystemUploadBean("MarketDataValueTestUpload_xlsx.xlsx");
		this.systemUploadService.uploadSystemUploadFile(upload);
	}


	@Test
	public void testUploadFile_xlsx_too_large() {
		SystemUploadCommand upload = setupSystemUploadBean("MarketDataValueTestUpload_TooLarge.xlsx");
		String expected = "Error was returned from IMS: Error reading file [MarketDataValueTestUpload_TooLarge.xlsx] with error: File 'MarketDataValueTestUpload_TooLarge.xlsx' is too large to convert to a workbook for processing (Max size allowed is 10 MB).  Please try the following:<br>1. If you are using an xlsx file, save your file as an xls file and try again.<br>2. Open the file and manually delete any empty rows at the end of the file<br>3. Split your file into smaller files.";
		String actual = "";
		boolean error = false;
		try {
			this.systemUploadService.uploadSystemUploadFile(upload);
		}
		catch (Exception e) {
			error = true;
			actual = ExceptionUtils.getOriginalMessage(e);
		}
		AssertUtils.assertTrue(error, "Expected error for an upload file that is too large, but didn't get one.");
		Assertions.assertEquals(expected, actual);
	}


	private SystemUploadCommand setupSystemUploadBean(String fileName) {
		SystemUploadCommand upload = new SystemUploadCommand();
		upload.setTableName("MarketDataValue");
		upload.setExistingBeans(FileUploadExistingBeanActions.UPDATE);
		upload.setPartialUploadAllowed(true);
		try {
			upload.setFile(new MultipartFileImpl(FileUtils.getClasspathResourceAsFile(fileName, this.getClass())));
		}
		catch (Throwable e) {
			throw new RuntimeException("Error setting upload file for [" + fileName + "]: " + e.getMessage(), e);
		}
		return upload;
	}
}
