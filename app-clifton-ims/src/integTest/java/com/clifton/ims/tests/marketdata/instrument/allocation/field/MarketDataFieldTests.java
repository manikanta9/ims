package com.clifton.ims.tests.marketdata.instrument.allocation.field;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.search.InvestmentAccountGroupSearchForm;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class MarketDataFieldTests extends BaseImsIntegrationTest {

	@Resource
	private MarketDataLiveService marketDataLiveService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private InvestmentGroupService investmentGroupService;


	@Test
	public void testMidCalculatedForEEM_Stock_Option() {
		validateOptionMarketDataLiveValue("EEM US 06/16/17 P25", "Mid - Calculated",
				liveValue -> MathUtils.isGreaterThan(liveValue.asNumericValue(), BigDecimal.ZERO));
	}


	@Test
	public void testExpirationDateForEEM_Stock_Option() {
		validateOptionMarketDataLiveValue("EEM US 06/16/17 P25", "Options - Expiration Date",
				liveValue -> DateUtils.isDateAfterOrEqual((Date) liveValue.getValue(), DateUtils.clearTime(new Date())));
	}


	@Test
	public void testLivePriceLookupForListedWeek2Options() {
		InvestmentAccountGroupSearchForm accountGroupSearchForm = new InvestmentAccountGroupSearchForm();
		accountGroupSearchForm.setCategoryHierarchyName("Option Rolls");
		accountGroupSearchForm.setCategoryName("Investment Account Group Tags");
		List<InvestmentAccountGroup> accountGroupList = this.investmentAccountGroupService.getInvestmentAccountGroupList(accountGroupSearchForm);

		if (!CollectionUtils.isEmpty(accountGroupList)) {
			Optional<InvestmentAccountGroup> spGroup = accountGroupList.stream().filter(group -> group.getName().startsWith("SP-")).findAny();
			if (spGroup.isPresent()) {
				InvestmentGroup group = this.investmentGroupService.getInvestmentGroupByName("Option Roll: " + spGroup.get().getName());
				InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
				instrumentSearchForm.setInactive(false);
				instrumentSearchForm.setTradingDisallowed(false);
				instrumentSearchForm.setOneSecurityPerInstrument(false);
				instrumentSearchForm.setInvestmentGroupId(group.getId());
				instrumentSearchForm.setHierarchyName("Listed");
				instrumentSearchForm.setIdentifierPrefix("2E"); // Mini SPX Futures 2nd Week
				List<InvestmentInstrument> instrumentList = this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm);

				List<InvestmentInstrument> weekTwoInstrumentList = CollectionUtils.getStream(instrumentList).filter(instrument -> instrument.getName().contains("2nd")).collect(Collectors.toList());
				List<InvestmentSecurity> securityList = new ArrayList<>();
				for (InvestmentInstrument instrument : CollectionUtils.getIterable(weekTwoInstrumentList)) {
					SecuritySearchForm securitySearchForm = new SecuritySearchForm();
					securitySearchForm.setActive(true);
					securitySearchForm.setInstrumentId(instrument.getId());
					CollectionUtils.getStream(this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm)).forEach(securityList::add);
				}
				int[] securityIds = getStreamOfOptionsExpiringAfterToday(securityList)
						.mapToInt(InvestmentSecurity::getId)
						.toArray();
				if (securityIds.length > 0) {
					MarketDataLiveCommand command = new MarketDataLiveCommand();
					command.setSecurityIds(securityIds);
					command.setMaxAgeInSeconds(10000);
					List<MarketDataLive> livePriceList = this.marketDataLiveService.getMarketDataLivePricesWithCommand(command);
					Assertions.assertEquals(securityIds.length, livePriceList.size());
					// Validate there is a price for each security
					List<Integer> securityIdList = Arrays.stream(securityIds).boxed().collect(Collectors.toList());
					livePriceList.forEach(liveData -> securityIdList.remove(securityIdList.indexOf(liveData.getSecurityId())));
					Assertions.assertTrue(securityIdList.isEmpty());
				}
			}
		}
	}


	private Stream<InvestmentSecurity> getStreamOfOptionsExpiringAfterToday(List<InvestmentSecurity> optionList) {
		Date today = DateUtils.clearTime(new Date());
		return CollectionUtils.getStream(optionList)
				.filter(option -> option.getLastDeliveryDate().after(today));
	}


	private void validateOptionMarketDataLiveValue(String optionSymbol, String marketDataFieldName, Predicate<MarketDataLive> assertionCondition) {
		InvestmentSecurity option = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(optionSymbol);
		if (!option.isActive()) {
			SecuritySearchForm securitySearchForm = new SecuritySearchForm();
			securitySearchForm.setInstrumentId(option.getInstrument().getId());
			securitySearchForm.setActive(Boolean.TRUE);
			Optional<InvestmentSecurity> nonExpiredMatch = getStreamOfOptionsExpiringAfterToday(this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm))
					.findFirst();
			if (nonExpiredMatch.isPresent()) {
				option = nonExpiredMatch.get();
			}
		}
		MarketDataLiveCommand command = new MarketDataLiveCommand();
		command.setMaxAgeInSeconds(10000);
		MarketDataLive liveValue = this.marketDataLiveService.getMarketDataLiveFieldValue(option.getId(), marketDataFieldName, command);
		Assertions.assertNotNull(liveValue, "Cannot find live " + marketDataFieldName + " value for " + option.getSymbol() + " - verify dates - if security is expired live value look up will not work");
		Assertions.assertTrue(assertionCondition.test(liveValue), "Cannot find " + marketDataFieldName + " value for " + option.getSymbol() + ", Value found [" + liveValue.getValue() + "]");
	}
}
