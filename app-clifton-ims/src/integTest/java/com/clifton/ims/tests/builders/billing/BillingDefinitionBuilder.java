package com.clifton.ims.tests.builders.billing;

import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.invoice.BillingInvoiceType;
import com.clifton.business.contract.BusinessContract;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.Date;


/**
 * @author manderson
 */
public class BillingDefinitionBuilder {


	private final BillingDefinitionService billingDefinitionService;

	private BusinessContract contract;
	private InvestmentSecurity billingCurrency;
	private BillingFrequencies billingFrequency;
	private BillingInvoiceType invoiceType;
	private Date startDate;
	private Date endDate;
	private Date secondYearStartDate;


	private BillingDefinitionBuilder(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public static BillingDefinitionBuilder billingDefinition(BillingDefinitionService billingDefinitionService) {
		return new BillingDefinitionBuilder(billingDefinitionService);
	}


	public BillingDefinition build() {
		BillingDefinition billingDefinition = new BillingDefinition();
		billingDefinition.setBusinessCompany(this.contract.getCompany());
		billingDefinition.setBillingCurrency(this.billingCurrency);
		billingDefinition.setBillingContract(this.contract);
		billingDefinition.setBillingFrequency(this.billingFrequency);
		billingDefinition.setInvoiceType(this.invoiceType);
		billingDefinition.setStartDate(this.startDate);
		billingDefinition.setSecondYearStartDate(this.secondYearStartDate);
		billingDefinition.setEndDate(this.endDate);
		return billingDefinition;
	}


	public BillingDefinition buildAndSave() {
		BillingDefinition billingDefinition = build();
		return this.billingDefinitionService.saveBillingDefinition(billingDefinition);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public BillingDefinitionBuilder withContract(BusinessContract contract) {
		this.contract = contract;
		return this;
	}


	public BillingDefinitionBuilder withBillingCurrency(InvestmentSecurity billingCurrency) {
		this.billingCurrency = billingCurrency;
		return this;
	}


	public BillingDefinitionBuilder withBillingFrequency(BillingFrequencies billingFrequency) {
		this.billingFrequency = billingFrequency;
		return this;
	}


	public BillingDefinitionBuilder withInvoiceType(BillingInvoiceType invoiceType) {
		this.invoiceType = invoiceType;
		return this;
	}


	public BillingDefinitionBuilder withStartDate(Date startDate) {
		this.startDate = startDate;
		return this;
	}


	public BillingDefinitionBuilder withSecondYearStartDate(Date secondYearStartDate) {
		this.secondYearStartDate = secondYearStartDate;
		return this;
	}


	public BillingDefinitionBuilder withEndDate(Date endDate) {
		this.endDate = endDate;
		return this;
	}
}
