package com.clifton.ims.tests.accounting.eventjournal;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalDetail;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingEventJournalTests</code> ...
 *
 * @author manderson
 */

public class AccountingEventJournalTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingEventJournalService accountingEventJournalService;

	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPremiumLegPaymentPreview_OTC_CDS() {
		EventJournalGeneratorCommand command = new EventJournalGeneratorCommand();
		command.setEventId(1356497); // 1% for 91 days Premium Leg Payment for SDB4060245819.2 on 09/21/2015
		List<AccountingEventJournalDetail> detailList = this.accountingEventJournalGeneratorService.getAccountingEventJournalListForPreview(command);

		Assertions.assertEquals(1, CollectionUtils.getSize(detailList));
		AccountingEventJournalDetail detail = detailList.get(0);
		Assertions.assertTrue(MathUtils.isNullOrZero(detail.getPreviewAmount()));
		// returned amounts are rounded to 10 decimals: TODO: look into this
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("-1198.17"), detail.getTransactionAmount()));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1.12225"), detail.getExchangeRateToBase()));
		Assertions.assertEquals("Journal Detail Already Generated and Booked with amount -1,198.17. Event Journal processing no longer includes this position and transaction amount is now considered to be 0.", detail.getSkipMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	// Event Edit Tests
	// Editing the Security Event with an un-booked journal should automatically update the event journal with the recalculated values.
	// NOTE: THIS TEST FIRST COPIES REAL EVENT FROM 12/25 to use on 12/26 and
	// uses that event for testing so that event journals booked for real event on 12/25 do not cause a conflict with editing the event


	@Test
	public void testEditCashCouponPaymentSecurityEventForUnbookedJournal() {
		// Bond 31396WNZ5 with Cash Coupon Payment Event on 12/25
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("31396WNZ5");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.STATE_STREET_BANK, this.tradeUtils.getTradeType(TradeType.BONDS));

		// Create and execute a buy trade
		BigDecimal quantity = BigDecimal.valueOf(69778.62);
		Date buyDate = DateUtils.toDate("12/16/2013");
		Trade buyTrade = this.tradeUtils.newBondTradeBuilder(security, quantity, true, buyDate, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		// Copy the Cash Coupon Payment Event for 12/25, New Event Date is set to 12/26 and return the newly created event
		InvestmentSecurityEvent event = copySecurityEventForDateToNextDay(security, "12/25/2013", InvestmentSecurityEventType.CASH_COUPON_PAYMENT);

		// Process Event for this new position
		AccountingEventJournal journal = generateAccountingEventJournalForEvent(accountInfo, event);
		ValidationUtils.assertTrue(CollectionUtils.getSize(journal.getDetailList()) == 1, "Expected One Detail for Journal");
		AccountingEventJournalDetail detail = journal.getDetailList().get(0);
		String transactionAmountString = CoreMathUtils.formatNumberMoney(detail.getTransactionAmount());

		ValidationUtils.assertEquals("24.19", transactionAmountString, "Expected Transaction Amount of [24.19] but was [" + transactionAmountString + "]");

		// Edit & Save the Event
		event.setBeforeAndAfterEventValue(BigDecimal.valueOf(0.75));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);

		// Pull Journal Again with detail list populated - ensure ids are the same, but amounts updated
		AccountingEventJournal journalUpdated = this.accountingEventJournalService.getAccountingEventJournal(journal.getId());
		ValidationUtils.assertTrue(CollectionUtils.getSize(journalUpdated.getDetailList()) == 1, "Expected One Detail for Journal");
		AccountingEventJournalDetail detailUpdated = journalUpdated.getDetailList().get(0);
		String updatedTransactionAmountString = CoreMathUtils.formatNumberMoney(detailUpdated.getTransactionAmount());

		ValidationUtils.assertEquals(detail.getId(), detailUpdated.getId(), "Updated Journal Detail should have the same IDs, with updated transaction amount.");
		ValidationUtils.assertEquals("43.61", updatedTransactionAmountString, "Expected Transaction Amount of [43.61] but was [" + updatedTransactionAmountString + "]");
	}


	// TRS Resets are really two events in one, so want to make sure both events update journals when necessary
	public void testEditTotalReturnSwapResetSecurityEventForUnbookedJournal() {
		// TODO
	}

	////////////////////////////////////////////////////////////////////
	//  Helper Methods


	private InvestmentSecurityEvent copySecurityEventForDateToNextDay(InvestmentSecurity security, String eventDate, String eventTypeName) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setEventDate(DateUtils.toDate(eventDate));
		searchForm.setTypeId(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(eventTypeName).getId());
		List<InvestmentSecurityEvent> eventList = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		ValidationUtils.assertTrue(CollectionUtils.getSize(eventList) == 1, "Expected one " + InvestmentSecurityEventType.CASH_COUPON_PAYMENT + " event for security 31396WNZ5 on 12/25/2013");
		InvestmentSecurityEvent event = eventList.get(0);

		// Create a copy of the event on the next day so we don't run into conflicts with anything using the real event
		InvestmentSecurityEvent newEvent = BeanUtils.cloneBean(event, false, false);
		newEvent.setId(null);
		newEvent.setEventDate(DateUtils.addDays(event.getEventDate(), 1));
		newEvent.setBeforeAndAfterEventValue(MathUtils.round(newEvent.getBeforeEventValue(), newEvent.getType().getDecimalPrecision()));
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(newEvent);

		return newEvent;
	}


	private AccountingEventJournal generateAccountingEventJournalForEvent(AccountInfo accountInfo, InvestmentSecurityEvent event) {
		// Process Event for this new position
		EventJournalGeneratorCommand command = EventJournalGeneratorCommand.ofEvent(event.getId(), AccountingEventJournalGeneratorTypes.GENERATE_PREVIEWED);
		command.setClientAccountId(accountInfo.getClientAccount().getId());

		this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(command);

		AccountingEventJournalSearchForm journalSearchForm = new AccountingEventJournalSearchForm();
		journalSearchForm.setClientInvestmentAccountId(accountInfo.getClientAccount().getId());
		journalSearchForm.setSecurityEventId(event.getId());
		List<AccountingEventJournal> journalList = this.accountingEventJournalService.getAccountingEventJournalList(journalSearchForm);
		ValidationUtils.assertTrue(CollectionUtils.getSize(journalList) == 1, "Expected one AccountingEventJournal generated for event " + event.getLabel());

		// Pull journal again with detail list populated - Should only have one detail
		return this.accountingEventJournalService.getAccountingEventJournal(journalList.get(0).getId());
	}
}
