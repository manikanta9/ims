package com.clifton.ims.tests.core.cache;

import com.clifton.core.cache.CacheStats;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.notification.definition.NotificationDefinition;
import com.clifton.notification.definition.NotificationDefinitionBatch;
import com.clifton.notification.definition.NotificationDefinitionInstant;
import com.clifton.notification.definition.NotificationDefinitionService;
import com.clifton.notification.definition.search.NotificationDefinitionSearchForm;
import com.clifton.system.list.SystemList;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.list.SystemListService;
import com.clifton.system.list.search.SystemListItemSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>CacheTests</code> ensure that hibernate caching is working as expected.
 *
 * @author michaelm
 */
public class CacheTests<T extends SystemList, N extends NotificationDefinition> extends BaseImsIntegrationTest {

	@Resource
	SystemListService<T> systemListService;

	@Resource
	NotificationDefinitionService<N> notificationDefinitionService;

	@Resource
	SystemRequestStatsService systemRequestStatsService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testHierarchyCachedInSecondLevelCache() {
		SystemListItemSearchForm form = new SystemListItemSearchForm();
		form.setListName("States");
		List<SystemListItem> itemList = this.systemListService.getSystemListItemList(form);
		CacheStats cacheStats = systemRequestStatsService.getSystemCacheStatsForName(SystemList.class.getName());
		Assertions.assertTrue(cacheStats.getSize() > 0, "Hierarchy was not cached in second level cache.");
	}


	@Test
	public void testHierarchyCacheUpdateOnUpdate() {
		NotificationDefinitionSearchForm form = new NotificationDefinitionSearchForm();
		short typeId = 9;
		form.setTypeId(typeId);
		List<NotificationDefinition> definitions = this.notificationDefinitionService.getNotificationDefinitionGenericList(form);
		NotificationDefinition definitionToUpdate = CollectionUtils.getFirstElementStrict(definitions);
		String originalDefinitionName = definitionToUpdate.getName();
		try {
			definitionToUpdate.setName("Test ".concat(originalDefinitionName));
			saveNotificationDefinition(definitionToUpdate);
			NotificationDefinition definitionFromCache = this.notificationDefinitionService.getNotificationDefinition(definitionToUpdate.getId());
			Assertions.assertNotEquals(originalDefinitionName, definitionFromCache.getName(), "Stale data in cache after updating an entity.");
		}
		catch (Exception e) {
			throw e;
		}
		finally {
			definitionToUpdate.setName(originalDefinitionName);
			saveNotificationDefinition(definitionToUpdate);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void saveNotificationDefinition(NotificationDefinition notificationDefinition) {
		if (notificationDefinition.isInstant()) {
			this.notificationDefinitionService.saveNotificationDefinitionInstant(((NotificationDefinitionInstant) notificationDefinition));
		}
		else {
			this.notificationDefinitionService.saveNotificationDefinitionBatch(((NotificationDefinitionBatch) notificationDefinition));
		}
	}
}
