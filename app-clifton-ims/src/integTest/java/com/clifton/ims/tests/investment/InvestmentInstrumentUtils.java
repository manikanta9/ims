package com.clifton.ims.tests.investment;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.datasource.MarketDataSourceSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.test.util.RandomUtils;
import com.clifton.core.util.MathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


@Component
public class InvestmentInstrumentUtils {

	private static final Logger log = LoggerFactory.getLogger(InvestmentInstrumentUtils.class);

	@Resource
	InvestmentInstrumentCopyService investmentInstrumentCopyService;

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private SystemColumnValueService systemColumnValueService;

	@Resource
	MarketDataSourceService marketDataSourceService;


	/**
	 * @throws RuntimeException when the currency cannot be found for the specified symbol.
	 */
	public InvestmentSecurity getCurrencyBySymbol(String symbol) {
		InvestmentSecurity result = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, true);
		if (result == null) {
			throw new RuntimeException("Could not find currency with symbol " + symbol);
		}
		return result;
	}


	/**
	 * @throws RuntimeException when the security cannot be found for the specified symbol.
	 */
	public InvestmentSecurity getInvestmentSecurityBySymbol(String symbol) {
		return getInvestmentSecurityBySymbol(symbol, null);
	}


	public InvestmentSecurity getInvestmentSecurityBySymbol(String symbol, Boolean currency) {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, currency);

		if (security == null && symbol.length() > 3) {
			// try to find futures that had their symbol archived: "Z M4" to "Z M14"
			if (MathUtils.isNumber(symbol.substring(symbol.length() - 1))) {
				// last character is a number (year)
				if (!MathUtils.isNumber(symbol.substring(symbol.length() - 2, symbol.length() - 1))) {
					// second to last character is not a number (month)
					String newSymbol = symbol.substring(0, symbol.length() - 1) + "1" + symbol.substring(symbol.length() - 1);
					security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(newSymbol, currency);
					if (security != null) {
						if (!InvestmentUtils.isSecurityOfType(security, InvestmentType.FUTURES)) {
							security = null;
						}
						else {
							log.info("Could not find security with symbol {} but found one with symbol {}", symbol, security.getSymbol());
						}
					}
				}
			}
		}

		//If security is null and symbol is over 11 characters, try to find a security with the last 11 characters of the symbol
		if (security == null && symbol.length() > 11) {
			int startIndex = symbol.length() - 11;
			String shortSymbol = symbol.substring(startIndex);
			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setSymbol(shortSymbol);
			List<InvestmentSecurity> result = this.investmentInstrumentService.getInvestmentSecurityList(searchForm);

			if (CollectionUtils.getSize(result) == 1) {
				InvestmentSecurity firstResult = result.get(0);
				InvestmentInstrumentHierarchy hierarchy = firstResult.getInstrument().getHierarchy();
				if (!hierarchy.isOtc() && InvestmentUtils.isSecurityOfType(firstResult, InvestmentType.SWAPS)) {
					security = firstResult;
					log.info("Could not find security with symbol {} but found one with symbol {}", symbol, security.getSymbol());
				}
			}
		}

		//If security is still null, then throw exception
		if (security == null) {
			throw new RuntimeException("Could not find a security with symbol " + symbol);
		}

		return security;
	}


	public InvestmentSecurity getCopyOfSecurityAndUnderlyingFromTemplate(String symbol, String underlyingSymbol, String newSymbol) {
		InvestmentSecurity newSecurity = getCopyOfSecurityFromTemplate(symbol, newSymbol);
		InvestmentSecurity underlyingSecurity = getCopyOfSecurityFromTemplate(underlyingSymbol, RandomUtils.randomNameAndNumber());
		newSecurity.setUnderlyingSecurity(underlyingSecurity);
		newSecurity.setUnderlyingSecurityFromSecurityUsed(true);
		return this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);
	}


	public InvestmentSecurity getCopyOfSecurityFromTemplate(String symbol, String newSymbol) {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(symbol);
		InvestmentSecurity newSecurity = createCopyOfSecurity(security, newSymbol, null);
		newSecurity = this.investmentInstrumentCopyService.saveInvestmentSecurityFromTemplate(newSecurity, security.getId());
		copySecurityEvents(symbol, newSymbol);
		copyMarketDataSourceSecurities(security, newSecurity);
		return newSecurity;
	}


	public InvestmentSecurity getCopyOfSecurity(String symbol, String newSymbol) {
		return getCopyOfSecurity(symbol, newSymbol, null);
	}


	public InvestmentSecurity getCopyOfSecurity(String symbol, String newSymbol, String newInstrumentPrefix) {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setSymbol(newSymbol);
		InvestmentSecurity newSecurity = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentSecurityList(securitySearchForm));
		if (newSecurity == null) {
			InvestmentSecurity security = getInvestmentSecurityBySymbol(symbol);
			newSecurity = createCopyOfSecurity(security, newSymbol, newInstrumentPrefix);
			newSecurity = this.investmentInstrumentService.saveInvestmentSecurity(newSecurity);
			copyMarketDataSourceSecurities(security, newSecurity);
		}
		return newSecurity;
	}


	private InvestmentSecurity createCopyOfSecurity(InvestmentSecurity security, String newSymbol, String newInstrumentPrefix) {
		InvestmentSecurity newSecurity = BeanUtils.cloneBean(security, false, false);

		// Add custom field values so they are saved when security is created (new validation will prevent creating security without required custom fields)
		newSecurity.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		newSecurity.setColumnValueList(getCopyOfSystemColumnValueListForSecurity(security));

		newSecurity.setId(null);
		newSecurity.setSymbol(newSymbol);
		newSecurity.setCusip(null);
		if (newInstrumentPrefix != null) {
			InstrumentSearchForm instrumentSearchForm = new InstrumentSearchForm();
			instrumentSearchForm.setName("Copy of [" + security.getInstrument().getIdentifierPrefix() + "]");
			InvestmentInstrument instrument = CollectionUtils.getFirstElement(this.investmentInstrumentService.getInvestmentInstrumentList(instrumentSearchForm));
			if (instrument == null) {
				instrument = security.getInstrument();
				instrument.setId(null);
				instrument.setName("Copy of [" + instrument.getIdentifierPrefix() + "]");
				instrument.setIdentifierPrefix(newInstrumentPrefix);
				this.investmentInstrumentService.saveInvestmentInstrument(instrument);
				newSecurity.setInstrument(instrument);
			}
		}
		return newSecurity;
	}


	private void copySecurityEvents(String fromSymbol, String toSymbol) {
		InvestmentSecurity fromSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(fromSymbol);
		InvestmentSecurity toSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(toSymbol);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(fromSecurity.getId());

		for (InvestmentSecurityEvent securityEvent : CollectionUtils.getIterable(this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm))) {
			securityEvent.setId(null);
			securityEvent.setSecurity(toSecurity);
			securityEvent.setBeforeAndAfterEventValue(MathUtils.round(securityEvent.getBeforeEventValue(), 8));
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(securityEvent);
		}
	}


	private void copyMarketDataSourceSecurities(InvestmentSecurity fromSecurity, InvestmentSecurity toSecurity) {
		List<MarketDataSourceSecurity> marketDataSourceSecurityList = this.marketDataSourceService.getMarketDataSourceSecurityListBySecurity(fromSecurity.getId());
		for (MarketDataSourceSecurity dataSourceSecurity : CollectionUtils.getIterable(marketDataSourceSecurityList)) {
			dataSourceSecurity.setId(null);
			dataSourceSecurity.setSecurity(toSecurity);
			this.marketDataSourceService.saveMarketDataSourceSecurity(dataSourceSecurity);
		}
	}


	public List<SystemColumnValue> getCopyOfSystemColumnValueListForSecurity(InvestmentSecurity security) {
		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setFkFieldId(security.getId());
		List<SystemColumnValue> columnValueList = this.systemColumnValueService.getSystemColumnValueList(searchForm);
		if (!CollectionUtils.isEmpty(columnValueList)) {
			columnValueList.forEach(systemColumnValue -> systemColumnValue.setId(null));
		}
		return columnValueList;
	}
}
