package com.clifton.ims.tests.spring;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.websocket.management.WebSocketMessageBrokerStatsMonitor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.ReflectionsException;
import org.reflections.scanners.MemberUsageScanner;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.StringJoiner;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * The {@code ImsBeanAutowiredPropertyTests} class includes tests for validating autowired properties of context beans.
 * <p>
 * These tests aid in verifying that autowired properties are not superfluous when using Spring's {@link AutowireCapableBeanFactory#AUTOWIRE_BY_NAME autowire-by-name} autowire
 * mode. Autowired properties which are not used but which have getters and setters may not be caught by static analysis tools for unused fields since publicly visible getters and
 * setters suffice as "usages" by most standards. These tests help to catch these cases by searching for unused property getters in all context beans.
 * <p>
 * These tests may also help avoid other superfluous properties that would not be caught by static analysis tools, such as properties are set but simply never used.
 *
 * @author MikeH
 */
public class ImsBeanAutowiredPropertyTests extends BaseImsBeanFactoryTests {

	private static final String ERROR_MSG_DELIM = "\n\t";
	private static final String ERROR_MSG_SUB_DELIM = "\n\t\t";
	private static final String ERROR_MSG_PREFIX = "Unused getters were found in context beans. These may be indicative of unused autowired properties. Such getters should be used or removed:" + ERROR_MSG_DELIM;

	private static final Reflections METHOD_USAGE_INDEX = new Reflections(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION, new MemberUsageScanner());

	/**
	 * The collection of skipped bean types when searching for unused accessors. These bean types may, for example, have properties that are only used when serialized for client
	 * use.
	 */
	private static final Collection<Class<?>> SKIPPED_BEAN_TYPE_LIST = Collections.unmodifiableCollection(CollectionUtils.createHashSet(
			WebSocketMessageBrokerStatsMonitor.class
	));

	/**
	 * The collection of skipped bean packages when searching for unused accessors. These packages may, for example, be generated from OpenAPI and we are not able to remove the
	 * unused getters.
	 */
	private static final Collection<String> SKIPPED_BEAN_PACKAGE_LIST = Collections.unmodifiableCollection(CollectionUtils.createHashSet(
			"com.clifton.fix.api.client"
	));

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates that all property accessors within context beans are used.
	 */
	@Test
	public void testBeanAccessorsUsed() {
		Collection<Class<?>> beanClassList = getApplicableBeanTypes();
		Collection<GetterHolder> getterMethodHolderList = beanClassList.stream()
				.map(this::getGetterMethods)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());

		// Validate getter usage
		Collection<GetterHolder> unusedGetterHolderList = CollectionUtils.getFiltered(getterMethodHolderList, getterHolder -> !isMethodUsed(getterHolder.getMethod()));

		// Display violations
		if (!unusedGetterHolderList.isEmpty()) {
			Assertions.fail(generateViolationMessage(unusedGetterHolderList));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates the violation message from the given list of unused methods.
	 */
	private String generateViolationMessage(Collection<GetterHolder> unusedGetterHolderList) {
		// Partition by class
		Map<Class<?>, List<Method>> unusedMethodListByClass = unusedGetterHolderList.stream()
				.collect(Collectors.groupingBy(
						GetterHolder::getClazz,
						() -> new TreeMap<Class<?>, List<Method>>(Comparator.comparing(Class::getName)),
						Collectors.collectingAndThen(Collectors.toList(), getterHolderList -> CollectionUtils.getConverted(getterHolderList, GetterHolder::getMethod)))
				);

		// Generate violation message by class
		List<String> classViolationMessageList = new ArrayList<>();
		for (Map.Entry<Class<?>, List<Method>> violationListByClass : unusedMethodListByClass.entrySet()) {
			Class<?> violationClass = violationListByClass.getKey();
			StringJoiner singleViolationMessageSj = new StringJoiner(ERROR_MSG_SUB_DELIM, violationClass.getName() + ":" + ERROR_MSG_SUB_DELIM, StringUtils.EMPTY_STRING);
			violationListByClass.getValue().stream()
					.map(Method::getName)
					.sorted()
					.forEach(singleViolationMessageSj::add);
			classViolationMessageList.add(singleViolationMessageSj.toString());
		}

		// Generate full violation message
		StringJoiner fullViolationMessageSj = new StringJoiner(ERROR_MSG_DELIM, ERROR_MSG_PREFIX, StringUtils.EMPTY_STRING);
		classViolationMessageList.forEach(fullViolationMessageSj::add);
		return fullViolationMessageSj.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets all unique bean types within the context that apply within the context of the rule.
	 */
	private Collection<Class<?>> getApplicableBeanTypes() {
		Iterator<String> beanNamesIterator = getContext().getBeanFactory().getBeanNamesIterator();
		Spliterator<String> beanNamesSpliterator = Spliterators.spliteratorUnknownSize(beanNamesIterator, Spliterator.ORDERED);
		return StreamSupport.stream(beanNamesSpliterator, false)
				.map(beanName -> getContext().getAutowireCapableBeanFactory().getType(beanName))
				.distinct()
				.filter(Objects::nonNull) // Skip beans whose type cannot be determined before instantiation
				.filter(beanClass -> !SKIPPED_BEAN_TYPE_LIST.contains(beanClass))
				.filter(beanClass -> !isBeanPackageSkipped(beanClass))
				.collect(Collectors.toList());
	}


	/**
	 * Return true if the given beanClass belongs to a package in SKIPPED_BEAN_PACKAGE_LIST
	 */
	private boolean isBeanPackageSkipped(Class<?> beanClass) {
		for (String packageName : SKIPPED_BEAN_PACKAGE_LIST) {
			if (beanClass.getPackage() != null && beanClass.getPackage().getName().startsWith(packageName)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Get all bean property getter method holders for the given class.
	 */
	private Collection<GetterHolder> getGetterMethods(Class<?> clazz) {
		Collection<GetterHolder> getterHolderList = new HashSet<>();
		for (PropertyDescriptor pd : BeanUtils.getPropertyDescriptors(clazz)) {
			Method method = pd.getReadMethod();
			boolean isCheckedGetterMethod = method != null
					&& method.getDeclaringClass().getPackage().getName().startsWith(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION)
					&& !Modifier.isAbstract(method.getModifiers())
					&& !clazz.getProtectionDomain().getCodeSource().getLocation().getPath().endsWith("-test-classes.jar") // Skip test classes
					&& ClassUtils.getClassField(clazz, pd.getName(), true, true) != null; // Only include methods with backing fields for autowiring
			if (isCheckedGetterMethod) {
				getterHolderList.add(new GetterHolder(clazz, method));
			}
		}
		return getterHolderList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@code true} if the method can be marked as used, or {@code false} otherwise.
	 * <p>
	 * This method skips validation on inherited methods, only targeting methods which are definitively originally declared directly on the owner class. Inherited methods should be
	 * validated via the method declaration directly on their top-most owning type.
	 */
	private boolean isMethodUsed(Method method) {
		// Guard-clause: Check usages for original method declarations only
		if (isInheritedMethod(method)) {
			return true;
		}

		try {
			Set<Member> methodUsageList = METHOD_USAGE_INDEX.getMethodUsage(method);
			return !methodUsageList.isEmpty();
		}
		catch (ReflectionsException e) {
			if (e.getMessage().startsWith("Can't resolve member named ")) {
				/*
				 * Ignore a org.reflections bug resolving referencing methods within anonymous classes. This indicates that a reference has been found but cannot be
				 * successfully resolved into a Member object.
				 */
				return true;
			}
			else {
				throw e;
			}
		}
	}


	/**
	 * Determines if the method is inherited from a super-type, including all superclasses and interfaces.
	 * <p>
	 * Methods are considered <i>inherited</i> if a matching method exists within any ancestors, including interfaces, that has the same name and parameters as the given method.
	 * These are similar constraints to those necessary for the presence of {@link Override} annotations.
	 */
	private boolean isInheritedMethod(Method method) {
		// Get all inherited types
		Collection<Class<?>> inheritedTypeList = new HashSet<>();
		Class<?> clazz = method.getDeclaringClass();
		while (clazz != null) {
			if (clazz != method.getDeclaringClass()) {
				inheritedTypeList.add(clazz);
			}
			inheritedTypeList.addAll(ClassUtils.getAllInterfaces(clazz));
			clazz = clazz.getSuperclass();
		}

		// Seek inherited types for matching method
		boolean inherited = inheritedTypeList.stream()
				.map(inheritedType -> MethodUtils.getDeclaredMethod(inheritedType, method.getName(), method.getParameterTypes()))
				.anyMatch(Objects::nonNull);

		return inherited;
	}


	/**
	 * The {@link GetterHolder} is a simple wrapper type to encapsulate relevant property descriptor data.
	 */
	private static class GetterHolder {

		private final Class<?> clazz;
		private final Method method;


		public GetterHolder(Class<?> clazz, Method method) {
			this.clazz = clazz;
			this.method = method;
		}


		public Class<?> getClazz() {
			return this.clazz;
		}


		public Method getMethod() {
			return this.method;
		}
	}
}
