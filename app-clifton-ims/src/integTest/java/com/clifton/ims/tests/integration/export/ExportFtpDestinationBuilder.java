package com.clifton.ims.tests.integration.export;


import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.test.util.RandomUtils;


public class ExportFtpDestinationBuilder {

	private ExportDefinitionService exportDefinitionService;
	private SystemBeanBuilder systemBeanBuilder;
	private ExportDefinitionDestination exportDefinitionDestination;

	///////////////////////////////////////////////////////////////////////////////////////////////


	public ExportFtpDestinationBuilder(String ftpUrl, SystemBeanService systemBeanService, ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
		this.systemBeanBuilder = SystemBeanBuilder.newBuilder(ftpUrl + " - " + RandomUtils.randomNumber(), "Ftp", systemBeanService)
				.addProperty("Ftp URL", ftpUrl);
		this.exportDefinitionDestination = new ExportDefinitionDestination();
	}


	public ExportDefinitionDestination build() {
		this.exportDefinitionDestination.setDestinationSystemBean(this.systemBeanBuilder.build());
		this.exportDefinitionService.saveExportDefinitionDestination(this.exportDefinitionDestination);
		return this.exportDefinitionDestination;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////


	public ExportFtpDestinationBuilder filenamePrefix(String prefix) {
		this.systemBeanBuilder.addProperty("Filename Prefix", prefix);
		return this;
	}


	public ExportFtpDestinationBuilder ftpUsername(String username) {
		this.systemBeanBuilder.addProperty("User Name", username);
		return this;
	}


	public ExportFtpDestinationBuilder ftpPassword(String password) {
		this.systemBeanBuilder.addProperty("Password Secret", null, password);
		return this;
	}


	public ExportFtpDestinationBuilder ftpRemoteFolder(String remoteFolder) {
		this.systemBeanBuilder.addProperty("Remote Folder", remoteFolder);
		return this;
	}


	public ExportFtpDestinationBuilder ftpPort(int port) {
		this.systemBeanBuilder.addProperty("Ftp Port", Integer.toString(port));
		return this;
	}


	public ExportFtpDestinationBuilder ftpProtocol(String protocol) {
		this.systemBeanBuilder.addProperty("FTP Protocol", protocol);
		return this;
	}


	public ExportFtpDestinationBuilder exportDefinition(ExportDefinition definition) {
		this.exportDefinitionDestination.setDefinition(definition);
		return this;
	}


	public ExportFtpDestinationBuilder destinationDisabled(boolean disabled) {
		this.exportDefinitionDestination.setDisabled(disabled);
		return this;
	}


	public ExportFtpDestinationBuilder destinationName(String name) {
		this.exportDefinitionDestination.setDestinationName(name);
		return this;
	}


	public ExportFtpDestinationBuilder sharedDestination(boolean shared) {
		this.exportDefinitionDestination.setSharedDestination(shared);
		return this;
	}


	public ExportFtpDestinationBuilder entityModifyCondition(SystemCondition entityModifyCondition) {
		this.exportDefinitionDestination.setEntityModifyCondition(entityModifyCondition);
		return this;
	}


	public ExportFtpDestinationBuilder sendCondition(SystemCondition sendCondition) {
		this.exportDefinitionDestination.setSendCondition(sendCondition);
		return this;
	}
}
