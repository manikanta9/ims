package com.clifton.ims.tests.marketdata.instrument.allocation;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.system.query.JdbcUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationCalculatedFieldTypes;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldTypes;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocationService;
import com.clifton.marketdata.instrument.allocation.calculators.BaseMarketDataInvestmentSecurityAllocationCalculator;
import com.clifton.marketdata.updater.MarketDataUpdaterService;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Tests the calculations of a few securities in each custom allocation hierarchy that we have.
 * NOTE: This dynamically picks 10 days from history to verify.  If necessary, can specify dates to compare specifically.
 * Except for Replication Synthetic Folder which is month end values only - and dependS on replications so compares specific month end value dates that have already been verified.
 *
 * @author manderson
 */

public class MarketDataInvestmentSecurityAllocationServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentInstrumentCopyService investmentInstrumentCopyService;

	@Resource
	private MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService;

	@Resource
	private MarketDataSourceService marketDataSourceService;

	@Resource
	private MarketDataFieldService marketDataFieldService;

	@Resource
	private MarketDataFieldMappingService marketDataFieldMappingService;

	@Resource
	private MarketDataUpdaterService marketDataUpdaterService;

	@Resource
	public SystemRequestStatsService systemRequestStatsService;

	private static final Logger log = LoggerFactory.getLogger(MarketDataInvestmentSecurityAllocationServiceImplTests.class);


	@Test
	public void testWeightedReturnRebalancedDaily() {
		// Currency As A Benchmark
		// Use Default Start Date of 01/03/2012
		InvestmentSecurity benchmark = getCustomAllocationSecurity("CHF BENCHMARK", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN_ADJUST_DAILY);
		InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);
		// Try Preview:
		validatePreviewCalculationResult(benchmark, "05/01/2015", "100.0805152979", //
				"CHF: % Change: 0.5797101449, Return Weight: 100, Weighted % Change: 0.57971014492754049546" +  //
						" CHF Price Lookup Value [1.0673497705] on [04/30/2015]" + //
						" CHF Price Lookup Value [1.0735373054] on [05/01/2015]" + //
						" Previous Value [99.5036823567] on [04/30/2015], Total % Change: 0.5797101449");

		// Use Default Start Date of 01/03/2011
		benchmark = getCustomAllocationSecurity("CAD BENCHMARK", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN_ADJUST_DAILY);
		copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);

		// Use Default Start Date of 11/30/2010
		benchmark = getCustomAllocationSecurity("GSCIUBSF2", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN_ADJUST_DAILY);
		copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);
		validatePreviewCalculationResult(benchmark, "01/07/2014", "96.7709641524", //
				"SG2MCITR: % Change: 0.1389607086, Return Weight: 50, Weighted % Change: 0.06948035432453488209" + //
						" SG2MCITR Price Lookup Value [653.0623] on [01/06/2014]" + //
						" SG2MCITR Price Lookup Value [653.9698] on [01/07/2014]" + //
						" BCOMF2T: % Change: -0.2479426638, Return Weight: 50, Weighted % Change: -0.12397133189693264040" + //
						" BCOMF2T Price Lookup Value [537.5033] on [01/06/2014]" + //
						" BCOMF2T Price Lookup Value [536.1706] on [01/07/2014]" + //
						" Previous Value [96.8237243464] on [01/06/2014], Total % Change: -0.0544909776");
	}


	@Test
	public void testWeightedReturnFromStart() {
		// Use Default Start Date of 11/29/2013
		InvestmentSecurity benchmark = getCustomAllocationSecurity("PTS BOND BLEND", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN);
		InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);
		validatePreviewCalculationResult(benchmark, "12/31/2013", "99.6735638093", //
				"LT10TRUU: % Change: -0.8160904768, Return Weight: 40, Weighted % Change: -0.32643619072098726691" +  //
						" LT10TRUU Price Lookup Value [466.86] on [12/23/2013]" + //
						" LT10TRUU Price Lookup Value [463.05] on [12/31/2013]" + //
						" Previous Value [100] on [12/23/2013], Total % Change: -0.3264361907");

		// Use Default Start Date of 09/30/2014
		benchmark = getCustomAllocationSecurity("VERIZONLDI", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN);
		copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);
		validatePreviewCalculationResult(benchmark, "12/15/2015", "101.8232870299", //
				"LUGITRUU: % Change: 2.2025049853, Return Weight: 21.54, Weighted % Change: 0.47441957383881081377" + //
						" LUGITRUU Price Lookup Value [1,785.24] on [09/30/2014]" + //
						" LUGITRUU Price Lookup Value [1,824.56] on [12/15/2015]" + //
						" LULCTRUU: % Change: -0.8497288715, Return Weight: 67.14, Weighted % Change: -0.57050796432925933472" + //
						" LULCTRUU Price Lookup Value [3,336.3701] on [09/30/2014]" + //
						" LULCTRUU Price Lookup Value [3,308.02] on [12/15/2015]" + //
						" LGL1TRUU: % Change: 7.4614549739, Return Weight: 12.4, Weighted % Change: 0.92522041675987525315" + //
						" LGL1TRUU Price Lookup Value [3,142.1499] on [09/30/2014]" + //
						" LGL1TRUU Price Lookup Value [3,376.6] on [12/15/2015]" + //
						" LS49TRUU: % Change: 0.1745031508, Return Weight: -10.68, Weighted % Change: -0.01863693650024163972" + //
						" LS49TRUU Price Lookup Value [206.3] on [09/30/2014]" + //
						" LS49TRUU Price Lookup Value [206.66] on [12/15/2015]" + //
						" BSEPTRUU: % Change: 10.5499160434, Return Weight: 9.6, Weighted % Change: 1.01279194016180661898" + //
						" BSEPTRUU Price Lookup Value [524.08] on [09/30/2014]" + //
						" BSEPTRUU Price Lookup Value [579.37] on [12/15/2015]" + //
						" Previous Value [100] on [09/30/2014], Total % Change: 1.8232870299");
	}


	@Test
	public void testSharePriceBaskets() {
		// Use Override Start Date of 12/31/2014
		Date startDate = DateUtils.toDate("12/31/2014");
		InvestmentSecurity benchmark = getCustomAllocationSecurity("MLBXPBR2", InvestmentSecurityAllocationTypes.SHARE_PRICE);
		InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, startDate);
		validateResults(benchmark, copy, startDate);
		validatePreviewCalculationResult(benchmark, "07/01/2015", "83.3897648432",
				" SGECNGP Price: 53.42679, Shares: 0.1108890505, Allocation Value: 5.9244460164" + //
						" SG1MCLP Price: 348.8694, Shares: 0.0041051006, Allocation Value: 1.4321439687" + //
						" SG1MBRP Price: 735.2865, Shares: 0.0019775438, Allocation Value: 1.4540612712" + //
						" SG1MHUP Price: 965.1177, Shares: 0.0060672486, Allocation Value: 5.8556090239" + //
						" SG1MGOP Price: 589.2789, Shares: 0.0101031899, Allocation Value: 5.9535966501" + //
						" SG1MLCP Price: 136.5048, Shares: 0.0223184302, Allocation Value: 3.0465728454" + //
						" SG1MLHP Price: 59.0525, Shares: 0.0500665847, Allocation Value: 2.9565569901" + //
						" SG1MFCP Price: 209.4123, Shares: 0.0036270551, Allocation Value: 0.7595499421" + //
						" SG1MWHP Price: 15.6048, Shares: 0.1829505996, Allocation Value: 2.8549075168" + //
						" SG1MCNP Price: 36.8509, Shares: 0.081155304, Allocation Value: 2.9906459913" + //
						" SG1MSOP Price: 353.8646, Shares: 0.0083783137, Allocation Value: 2.9647886302" + //
						" BCOMBO3 Price: 80.0407, Shares: 0.0366845308, Allocation Value: 2.9362555235" + //
						" SG1MIAP Price: 36.43588, Shares: 0.1675417911, Allocation Value: 6.1045325961" + //
						" SG1MICP Price: 354.8567, Shares: 0.0168647852, Allocation Value: 5.9845820236" + //
						" SG1MIZP Price: 83.0545, Shares: 0.0367754907, Allocation Value: 3.0543699925" + //
						" SG1MIKP Price: 146.9756, Shares: 0.020402779, Allocation Value: 2.9987106804" + //
						" SG1MILP Price: 315.5122, Shares: 0.0047976863, Allocation Value: 1.5137285607" + //
						" BCOMSN Price: 244.7198, Shares: 0.0031901069, Allocation Value: 0.780682318" + //
						" SG1MGCP Price: 177.6901, Shares: 0.0335528092, Allocation Value: 5.9620020176" + //
						" SG1MSIP Price: 182.8854, Shares: 0.0326580488, Allocation Value: 5.9726803118" + //
						" SPGCPLP Price: 242.5366, Shares: 0.0062054038, Allocation Value: 1.5050375302" + //
						" SPGCPAP Price: 465.1349, Shares: 0.0033473006, Allocation Value: 1.5569463276" + //
						" SG1MSBP Price: 91.6039, Shares: 0.0325304751, Allocation Value: 2.9799183851" + //
						" SG1MCTP Price: 27.4736, Shares: 0.0540670578, Allocation Value: 1.4854167178" + //
						" SG1MKCP Price: 16.2341, Shares: 0.176497106, Allocation Value: 2.8652716678" + //
						" SG1MCCP Price: 94.134, Shares: 0.0159002204, Allocation Value: 1.4967513443");

		// Use Override Start Date of 12/31/2014
		benchmark = getCustomAllocationSecurity("GSGLDMAT", InvestmentSecurityAllocationTypes.SHARE_PRICE);

		// NOTE: STOCKS NEED CALENDAR OVERRIDE - MNOD is traded on LI Exchange, But Uses a Different Calendar
		// Until that is resolved - for testing Anything after 4/6 won't calculate (4/3 is Good Friday, 4/6 Easter Monday)
		benchmark.setEndDate(DateUtils.toDate("04/02/2015"));

		copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, startDate);
		validateResults(benchmark, copy, startDate);
	}


	@Test
	public void testReplicationSynthetics() {
		// 7/31: Underlying Return for security allocation SYN S&P MIDCAP was changed - open question to be discussed if the system should actually rebuild
		// the data automatically.  Since it was updated in November for July it wouldn't get picked up by any daily batch jobs, so for now just ignore this particular date
		// NOTE BOTH OF THESE SECURITIES RELY ON SYN S&P MIDCAP
		List<Date> ignoreDates = CollectionUtils.createList(DateUtils.toDate("07/31/2015"));

		// The switch happened as of October 2017.  Once we have enough history (6+ months) can just move the test dates forward.
		// Configuration Changed from RTA to RTY - so in order to calculate history change underlying back to RTA
		// Because we need to save the underlying instrument from the security (instrument.underlyingInstrument) do through sql
		JdbcUtils.executeQuery("UPDATE InvestmentInstrument SET UnderlyingInstrumentID = 481 WHERE InvestmentInstrumentID = 10307");
		this.systemRequestStatsService.deleteSystemCache("com.clifton.investment.instrument.InvestmentInstrument");

		try {
			// Use Calculation Start Date of 03/01/2015
			Date startDate = DateUtils.toDate("03/01/2015");
			InvestmentSecurity benchmark = getCustomAllocationSecurity("SYN RUSSELL 3000", InvestmentSecurityAllocationTypes.MONTHLY_RETURN_WEIGHTED_AVERAGE);
			InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, startDate);
			validateResults(benchmark, copy, startDate, ignoreDates);
			validatePreviewCalculationResult(benchmark, "01/31/2016", "-5.515447",
					"SPTR-SYN Monthly Return: -5.11 Return Weight Value: 83.83 Allocation Value: -4.28371" + //
							" SYN S&P MIDCAP Monthly Return: -5.58 Return Weight Value: 5.81 Allocation Value: -0.3242" + //
							" SYN RUSSELL 2000 Monthly Return: -8.76 Return Weight Value: 10.36 Allocation Value: -0.90754");
		}
		finally {
			// Reset Underlying to RTY
			JdbcUtils.executeQuery("UPDATE InvestmentInstrument SET UnderlyingInstrumentID = 17375 WHERE InvestmentInstrumentID = 10307");
			this.systemRequestStatsService.deleteSystemCache("com.clifton.investment.instrument.InvestmentInstrument");
		}
	}


	@Test
	public void testReplicationSynthetics_Currency() {
		// Use Calculation Start Date of 10/31/2015
		Date startDate = DateUtils.toDate("10/31/2015");
		InvestmentSecurity benchmark = getCustomAllocationSecurity("CURRENCY SYN EUR", InvestmentSecurityAllocationTypes.MONTHLY_RETURN_WEIGHTED_AVERAGE);
		InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, startDate);
		validateResults(benchmark, copy, startDate);
		validatePreviewCalculationResult(benchmark, "01/31/2016", "-0.4363402535", "ECH16 Monthly Return: -0.43634 Return Weight Value: 100 Allocation Value: -0.43634");
	}


	@Test
	public void testWeightedReturnRebalancedMonthly() {
		// Use Default Start Date of 1/31/1990
		InvestmentSecurity benchmark = getCustomAllocationSecurity("BALANCED RISK COMPOSITE II", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN_ADJUST_MONTHLY);
		InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);
		validatePreviewCalculationResult(benchmark, "01/04/2000", "269.6621243018",
				"LEGATRUU: % Change: 0.5740783491, Return Weight: 40, Weighted % Change: 0.22963133962072414335" + //
						" LEGATRUU Price Lookup Value [213.5597] on [12/31/1999]" + //
						" LEGATRUU Price Lookup Value [214.7857] on [01/04/2000]" + //
						" NDDUWI: % Change: -2.9850980682, Return Weight: 60, Weighted % Change: -1.79105884093914581026" + //
						" NDDUWI Price Lookup Value [2,865.199] on [12/31/1999]" + //
						" NDDUWI Price Lookup Value [2,779.67] on [01/04/2000]" + //
						" Previous Value [273.9394908489] on [12/31/1999], Total % Change: -1.5614275013");

		// Use Default Start Date of 02/28/1989
		benchmark = getCustomAllocationSecurity("DEFENSIVE EQUITY COMPOSITE VI", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN_ADJUST_MONTHLY);
		copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, null);
		validateResults(benchmark, copy, null);
		validatePreviewCalculationResult(benchmark, "03/31/2016", "324.7707940059",
				"SPTR: % Change: 6.7838157582, Return Weight: 17, Weighted % Change: 1.15324867888832186131" + //
						" SPTR Price Lookup Value [3,627.059] on [02/29/2016]" + //
						" SPTR Price Lookup Value [3,873.112] on [03/31/2016]" + //
						" SBMMTB3: % Change: 0.0233087585, Return Weight: 83, Weighted % Change: 0.01934626958730933786" + //
						" SBMMTB3 Price Lookup Value [622.9418] on [02/29/2016]" + //
						" SBMMTB3 Price Lookup Value [623.087] on [03/31/2016]" + //
						" Previous Value [321.0066858237] on [02/29/2016], Total % Change: 1.1725949485");


		// Test Future Date to ensure price date look ups work correctly
		try {
			validatePreviewCalculationResult(benchmark, DateUtils.fromDateShort(DateUtils.getLastDayOfMonth(new Date())), "", "Missing Previous Value or Allocation Prices");
		}
		catch (Throwable e) {
			String error;
			if (e instanceof ImsErrorResponseException) {
				error = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			else {
				error = ExceptionUtils.getOriginalMessage(e);
			}
			Assertions.assertTrue(error.contains("Missing price"));
		}
	}


	@Test
	public void testWeightedReturnRebalancedMonthly_TestMissingHistory() {
		// Use Default Start Date of 1/31/1990
		InvestmentSecurity benchmark = getCustomAllocationSecurity("BALANCED RISK COMPOSITE II", InvestmentSecurityAllocationTypes.WEIGHTED_RETURN_ADJUST_MONTHLY);

		boolean error = false;
		try {
			InvestmentSecurity copy = copyCustomAllocationSecurityAndBuildMarketData(benchmark, DateUtils.toDate("5/01/2016"));
			// Don't care about actual result, but validates we don't run into infinite loop/stack overflow since we are missing market data from 1990 to rebuild start of 5/1/2016
		}
		catch (Exception e) {
			error = true;
			Assertions.assertTrue(ExceptionUtils.getOriginalMessage(e).contains("Potential Error – too many missing values prior to current calculation dates. Please manually rebuild the security with start date of inception or last value date"));
		}
		Assertions.assertTrue(error, "Expected missing history to fail rebuild.");
	}


	//////////////////////////////////////////////////////////////////////


	private InvestmentSecurity getCustomAllocationSecurity(String symbol, InvestmentSecurityAllocationTypes securityAllocationType) {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setSymbolExact(symbol);
		securitySearchForm.setSecurityAllocationType(securityAllocationType);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbolOnly(symbol);
		if (security == null || security.getInstrument().getHierarchy().getSecurityAllocationType() != securityAllocationType) {
			throw new ValidationException("Cannot find security with symbol [" + symbol + "] using allocation type [" + securityAllocationType.name() + "]");
		}
		return security;
	}


	/**
	 * Will rebuild for 1 Year of Data From Start Date - If start date is null, uses security start date
	 */
	private InvestmentSecurity copyCustomAllocationSecurityAndBuildMarketData(InvestmentSecurity security, Date rebuildStartDate) {
		InvestmentSecurity newSecurity = BeanUtils.cloneBean(security, false, false);
		newSecurity.setId(null);
		newSecurity.setSymbol(security.getSymbol() + " - " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_PRECISE_NO_SEPARATORS));
		newSecurity.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
		searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setFkFieldId(security.getId());
		newSecurity.setColumnValueList(this.systemColumnValueService.getSystemColumnValueList(searchForm));
		this.investmentInstrumentCopyService.saveInvestmentSecurityFromTemplate(newSecurity, security.getId());
		newSecurity = getCustomAllocationSecurity(newSecurity.getSymbol(), security.getInstrument().getHierarchy().getSecurityAllocationType());
		if (rebuildStartDate == null) {
			rebuildStartDate = security.getStartDate();
		}
		this.marketDataUpdaterService.rebuildMarketDataAllocatedValueForSecurity(newSecurity.getId(), getCustomSecurityAllocationDataSource().getId(), rebuildStartDate, getRebuildEndDate(rebuildStartDate), false);
		return newSecurity;
	}


	private Date getRebuildEndDate(Date rebuildStartDate) {
		Date endDate = DateUtils.addYears(rebuildStartDate, 1);
		Date maxEndDate = DateUtils.addDays(new Date(), -45); // Max of 45 days ago to account for monthly returns that depend on data that is delayed
		if (DateUtils.isDateAfterOrEqual(endDate, maxEndDate)) {
			return maxEndDate;
		}
		return endDate;
	}


	private void validatePreviewCalculationResult(InvestmentSecurity security, String previewDate, String calculatedValue, String expectedResult) {
		String previewResult = this.marketDataInvestmentSecurityAllocationService.previewMarketDataAllocatedValueForSecurity(security.getId(), DateUtils.toDate(previewDate), getCustomSecurityAllocationDataSource().getId());
		expectedResult = "Calculation of [" + security.getSymbol() + "] on [" + previewDate + "]: " + expectedResult + " Result: " + calculatedValue;
		Assertions.assertEquals(expectedResult.replaceAll("\\s+", " ").trim(), previewResult.replaceAll("\\s+", " ").trim());
	}


	private void validateResults(InvestmentSecurity originalSecurity, InvestmentSecurity newSecurity, Date rebuildStartDate) {
		validateResults(originalSecurity, newSecurity, rebuildStartDate, null);
	}


	private void validateResults(InvestmentSecurity originalSecurity, InvestmentSecurity newSecurity, Date rebuildStartDate, List<Date> ignoreDates) {
		List<MarketDataValue> originalList = getValidateValues(originalSecurity, rebuildStartDate);

		StringBuilder sb = new StringBuilder("Validated " + originalSecurity.getSymbol() + " on : ");
		List<MarketDataValue> newList = getMarketDataValueList(newSecurity, BeanUtils.getPropertyValues(originalList, "measureDate", Date.class));

		for (MarketDataValue originalValue : CollectionUtils.getIterable(BeanUtils.sortWithFunction(originalList, MarketDataValue::getMeasureDate, true))) {
			boolean found = false;
			for (MarketDataValue newValue : CollectionUtils.getIterable(newList)) {
				if (DateUtils.compare(originalValue.getMeasureDate(), newValue.getMeasureDate(), false) == 0) {
					found = true;
					if (CollectionUtils.isEmpty(ignoreDates) || !ignoreDates.contains(originalValue.getMeasureDate())) {
						Assertions.assertEquals(MathUtils.round(originalValue.getMeasureValue(), 10), MathUtils.round(newValue.getMeasureValue(), 10), "Incorrect value for [" + newSecurity.getSymbol() + "] calculated on " + DateUtils.fromDateShort(originalValue.getMeasureDate()) + ": ");
						break;
					}
				}
			}
			Assertions.assertTrue(found, "Missing value for[" + newSecurity.getSymbol() + "] of " + CoreMathUtils.formatNumberDecimal(originalValue.getMeasureValue()) + "] on [" + DateUtils.fromDateShort(originalValue.getMeasureDate()) + "] ");
			sb.append(DateUtils.fromDateShort(originalValue.getMeasureDate())).append(" | ");
		}
		log.info(sb.toString());
	}


	private List<MarketDataValue> getValidateValues(InvestmentSecurity security, Date startDate) {
		if (startDate == null) {
			startDate = security.getStartDate();
		}
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setDataSourceId(getCustomSecurityAllocationDataSource().getId());
		searchForm.setDataFieldId(getCustomSecurityMarketDataField(security).getId());
		searchForm.setOrderBy("measureDate:ASC");
		searchForm.setMinMeasureDate(startDate);
		searchForm.setMaxMeasureDate(getRebuildEndDate(startDate));
		if (security.getEndDate() != null && DateUtils.compare(security.getEndDate(), searchForm.getMaxMeasureDate(), false) < 0) {
			searchForm.setMaxMeasureDate(security.getEndDate());
		}

		List<MarketDataValue> valueList = this.marketDataFieldService.getMarketDataValueList(searchForm);
		List<MarketDataValue> filteredList = new ArrayList<>();

		if (security.getInstrument().getHierarchy().getSecurityAllocationType().getCalculatedFieldType().isMonthEndOnly()) {
			// Add all results - monthly values for up to one year
			filteredList = valueList;
		}
		else {
			for (int i = 0; i < CollectionUtils.getSize(valueList); i = i + 10) {
				filteredList.add(valueList.get(i));
			}
		}
		return filteredList;
	}


	private List<MarketDataValue> getMarketDataValueList(InvestmentSecurity security, Date[] dates) {
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setMeasureDates(dates);
		searchForm.setDataSourceId(getCustomSecurityAllocationDataSource().getId());
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setDataFieldId(getCustomSecurityMarketDataField(security).getId());
		return this.marketDataFieldService.getMarketDataValueList(searchForm);
	}


	private MarketDataSource getCustomSecurityAllocationDataSource() {
		return this.marketDataSourceService.getMarketDataSourceByName("Parametric Clifton");
	}


	private MarketDataField getCustomSecurityMarketDataField(InvestmentSecurity security) {
		MarketDataField field;
		if (InvestmentSecurityAllocationCalculatedFieldTypes.MONTHLY_RETURN == security.getInstrument().getHierarchy().getSecurityAllocationType().getCalculatedFieldType()) {
			field = this.marketDataFieldService.getMarketDataFieldByName(BaseMarketDataInvestmentSecurityAllocationCalculator.DEFAULT_MONTHLY_RETURN_DATA_FIELD_NAME);
		}
		else {
			MarketDataPriceFieldMapping priceFieldMapping = this.marketDataFieldMappingService.getMarketDataPriceFieldMappingByInstrument(security.getInstrument().getId());
			field = MarketDataPriceFieldTypes.LATEST.getPriceField(priceFieldMapping, this.marketDataFieldService);
		}
		if (field == null) {
			throw new ValidationException("Cannot determine market data field to use/validate for allocated security " + security.getLabel());
		}
		return field;
	}
}
