package com.clifton.ims.tests.rule.setup;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.CoreMethodUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.definition.search.RuleDefinitionSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.SystemTable;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.List;


/**
 * Test the setup of rule related entities to ensure they follow our conventions
 */
public class RuleDefinitionTests extends RuleCategoryTests {

	@Resource
	private SystemBeanService systemBeanService;


	@Override
	protected void verifyCauseTable(RuleDefinition ruleDefinition) {
		if (ruleDefinition.getCauseTable() != null && !EXCLUDED_TABLE_VERIFICATION.contains(ruleDefinition.getCauseTable().getName())) {
			ValidationUtils.assertNotNull(ruleDefinition.getCauseTable().getDetailScreenClass(), "Rule Definition Cause Table [" + ruleDefinition.getCauseTable().getName() + "] Detail Screen Class Not Set. Cannot dynamically link to entities in UI without it.");
		}
		if (ruleDefinition.getEntityTable() != null && !EXCLUDED_TABLE_VERIFICATION.contains(ruleDefinition.getEntityTable().getName())) {
			ValidationUtils.assertNotNull(ruleDefinition.getEntityTable().getDetailScreenClass(), "Rule Definition Entity Table [" + ruleDefinition.getEntityTable().getName() + "] Detail Screen Class Not Set. Cannot dynamically link to entities in UI without it.");
		}
	}


	@Override
	protected void verifyFreemarkerNoteTemplate(RuleDefinition ruleDefinition) {
		//Verify Violation Note Template Syntax is Valid
		String template = ruleDefinition.getViolationNoteTemplate();
		ValidationUtils.assertNotNull(template, "Rule Definition [" + ruleDefinition.getName() + "] Violation Note Template is NULL.");
	}


	@Override
	protected void verifyName(RuleDefinition ruleDefinition) {
		//There appears to be no current convention
	}


	@Override
	protected void verifyEvaluatorBean(RuleDefinition ruleDefinition) {
		SystemBean evaluatorBean = ruleDefinition.getEvaluatorBean();
		//If Null Then Manual or SystemDefined must be checked on the Rule Definition
		ValidationUtils.assertTrue(evaluatorBean != null || ruleDefinition.isManual() || ruleDefinition.isSystemDefined(), "Rule Definition [" + ruleDefinition.getName() + "] Evaluator Bean is NULL but Rule Definition Manual is set to False. Evaluator Bean can only be NULL for Manual Rule Definition.");

		if (evaluatorBean != null && "Rule Evaluator Using System Condition".equals(evaluatorBean.getType().getName())) {
			//Verify specific aspects of this type in accordance to our rule conventions
			verifyEvaluatorBeanTypeProperties(ruleDefinition, evaluatorBean, this.systemBeanService.getSystemBeanPropertyListByBeanId(evaluatorBean.getId()));
		}
	}


	private void verifyEvaluatorBeanTypeProperties(RuleDefinition ruleDefinition, SystemBean evaluatorBean, List<SystemBeanProperty> propertyList) {
		//Verify Characteristic of Specific Types of Evaluator Beans

		SystemBeanProperty dependentEntityListContextLookupMethodProperty = CollectionUtils.getOnlyElement(BeanUtils.filter(propertyList, systemBeanProperty -> systemBeanProperty.getType().getName(), "Dependent Entity List Context Lookup Method"));

		if (dependentEntityListContextLookupMethodProperty != null) {
			String dependentEntityListContextLookupMethod = dependentEntityListContextLookupMethodProperty.getValue();
			if (!StringUtils.isEmpty(dependentEntityListContextLookupMethod)) {
				//If Dependent Entity List Context Lookup Method exists we must verify that this method exists on the Rule Definitions Category Evaluator Context Bean
				//The method should not only exist but must accept a single parameter of type Category Table
				String contextBeanClassName = ruleDefinition.getRuleCategory().getRuleEvaluatorContextBean().getType().getClassName();
				Class<?> evaluatorContextBeanClass = CoreClassUtils.getClass(contextBeanClassName);
				List<Method> methodList = CoreMethodUtils.getMethodList(evaluatorContextBeanClass, dependentEntityListContextLookupMethod);
				//Check if a method exists with this name
				ValidationUtils.assertTrue(!CollectionUtils.isEmpty(methodList), "Rule Definition [" + ruleDefinition.getName() + "] is using Evaluator Bean [" + evaluatorBean.getName() + "] which is of type [" + evaluatorBean.getType().getName() + "] which has defined a [" + dependentEntityListContextLookupMethodProperty.getType().getName() + "] of [" + dependentEntityListContextLookupMethod + "] but this Method does not exist on this Rule Definitions Category Evaluator Context Bean [" + contextBeanClassName + "]");
				methodList = BeanUtils.filter(methodList, Method::getParameterCount, 1);
				//Ensure that any methods that exist with this name have only a single parameter
				ValidationUtils.assertTrue(!CollectionUtils.isEmpty(methodList), "Rule Definition [" + ruleDefinition.getName() + "] is using Evaluator Bean [" + evaluatorBean.getName() + "] which is of type [" + evaluatorBean.getType().getName() + "] which has defined a [" + dependentEntityListContextLookupMethodProperty.getType().getName() + "] of [" + dependentEntityListContextLookupMethod + "], this Method Exists on this Rule Definitions Category Evaluator Context Bean but it accepts more than 1 parameter. It must only accept 1 parameter from Rule Category Table [" + ruleDefinition.getRuleCategory().getCategoryTable().getName() + "]");
				//Ensure that this is the only method by this name
				Method contextMethodForEntityList = CollectionUtils.getOnlyElementStrict(methodList);
				SystemTable categoryTable = ruleDefinition.getRuleCategory().getCategoryTable();
				//Ensure that the method parameter accepts a bean that is of the type associated to the Category Table
				Class<?> dtoBeanForCategoryTable = findClassBySimpleName(categoryTable.getName());
				AssertUtils.assertNotNull(dtoBeanForCategoryTable, "Could not find class for name " + categoryTable.getName());
				//Retrieve what type the method actually accepts
				Class<?> methodParameterTypeClass = contextMethodForEntityList.getParameterTypes()[0];
				//Ensure that actual method parameter is assignable from the bean we expect to pass into the context
				ValidationUtils.assertTrue(methodParameterTypeClass.isAssignableFrom(dtoBeanForCategoryTable), "Rule Definition [" + ruleDefinition.getName() + "] is using Evaluator Bean [" + evaluatorBean.getName() + "] which is of type [" + evaluatorBean.getType().getName() + "] which has defined a [" + dependentEntityListContextLookupMethodProperty.getType().getName() + "] of [" + dependentEntityListContextLookupMethod + "], this Method Exists on this Rule Definitions Category Evaluator Context Bean but it's single parameter type [" + methodParameterTypeClass.getSimpleName() + "] is not assignable from our Rule Category Table DTO Bean [" + dtoBeanForCategoryTable.getSimpleName() + "]");
			}
		}
	}


	private Class<?> findClassBySimpleName(String simpleName) {
		for (Package p : Package.getPackages()) {
			try {
				return CoreClassUtils.getClass(p.getName() + "." + simpleName);
			}
			catch (Exception e) {
				//
			}
		}
		return null;
	}


	@Override
	protected List<RuleDefinition> getRuleDefinitionListForVerification(RuleCategory ruleCategory) {
		RuleDefinitionSearchForm searchForm = new RuleDefinitionSearchForm();
		searchForm.setRuleCategoryId(ruleCategory.getId());
		return this.ruleDefinitionService.getRuleDefinitionList(searchForm);
	}
}
