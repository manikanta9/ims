package com.clifton.ims.tests.swift.server;

import com.clifton.swift.server.SwiftTestMessageServer;
import com.clifton.swift.server.message.SwiftTestMessageStatuses;
import com.clifton.swift.server.message.SwiftTestStatusMessage;
import com.clifton.swift.server.runner.autoclient.SwiftTestCase;
import com.clifton.test.util.PropertiesLoader;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;

import java.time.Duration;


/**
 * @author nickk
 */
@TestPropertySource(properties = "application.name=SWIFT")
public abstract class BaseSwiftTestMessageTest {

	private static final String CONTEXT_PROPERTIES_PATH = PropertiesLoader.getPropertiesFileUri();
	private static final String TEST_DATA_PATH = "com/clifton/ims/tests/swift/server/data";
	private final SwiftTestMessageServer server = new SwiftTestMessageServer(CONTEXT_PROPERTIES_PATH, TEST_DATA_PATH);


	@BeforeEach
	public void startup() throws Exception {
		this.server.start();
		Assertions.assertNotNull(this.server.sendStartTestSession());
	}


	@AfterEach
	public void shutdown() throws Exception {
		this.server.shutdown();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected ApplicationContext getRunnerContext() {
		return this.server.getApplicationContext();
	}


	protected void setTestCase(SwiftTestCase swiftTestCase) {
		this.server.sendSwiftTestCase(swiftTestCase);
	}


	protected SwiftTestStatusMessage awaitStatusMessage(SwiftTestMessageStatuses swiftTestMessageStatus, Duration duration) {
		try {
			return this.server.awaitStatusMessage(swiftTestMessageStatus, duration);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new RuntimeException("Interrupted waiting for status message: " + swiftTestMessageStatus);
		}
	}
}
