package com.clifton.ims.tests.investment.replication.calculator.dynamic;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.investment.replication.InvestmentReplicationUtils;
import com.clifton.ims.tests.investment.replication.executor.InvestmentReplicationCalculatorTestExecutor;
import com.clifton.ims.tests.investment.replication.executor.InvestmentReplicationCalculatorTestServiceHolder;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.dynamic.InvestmentReplicationCalculatorBlended;
import com.clifton.investment.replication.history.InvestmentReplicationHistoryService;
import com.clifton.investment.replication.history.rebuild.InvestmentReplicationHistoryRebuildService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * Integration tests for verifying {@link com.clifton.investment.replication.calculators.InvestmentReplicationCalculator} calculations.
 *
 * @author michaelm
 */
public class InvestmentReplicationCalculatorTests extends BaseImsIntegrationTest {

	@Resource
	private CalendarSetupService calendarSetupService;
	@Resource
	private InvestmentInstrumentService investmentInstrumentService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private InvestmentReplicationService investmentReplicationService;
	@Resource
	private InvestmentReplicationHistoryRebuildService investmentReplicationHistoryRebuildService;
	@Resource
	private InvestmentReplicationHistoryService investmentReplicationHistoryService;
	@Resource
	private InvestmentReplicationUtils investmentReplicationUtils;


	private static final String COPY_CURRENT_SECURITY_PROPERTY_TYPE_NAME = "Copy Current Security from Child Allocation Hist.";

	////////////////////////////////////////////////////////////////////////////
	////////       InvestmentReplicationCalculatorBlended Tests         ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBlendedReplicationCalculator_75_25() {
		InvestmentReplication replication = getReplicationWithChildren("Blended");
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("05/19/2020", "05/19/2020")
				.withExpectedResults(
						"05/19/2020     05/19/2020     AUD/EUR Curr Fwd 05/29/2020     25.00000",
						"05/19/2020     05/19/2020     AUD/USD Curr Fwd 05/29/2020     8.33333",
						"05/19/2020     05/19/2020     AUD/EUR Curr Fwd 06/30/2020     25.00000",
						"05/19/2020     05/19/2020     AUD/USD Curr Fwd 06/30/2020     8.33333",
						"05/19/2020     05/19/2020     AUD/EUR Curr Fwd 07/31/2020     25.00000",
						"05/19/2020     05/19/2020     AUD/USD Curr Fwd 07/31/2020     8.33333"
				).execute();
	}


	@Test
	public void testBlendedReplicationCalculator_50_25() {
		InvestmentReplication replication = getReplicationWithChildren("Blended");
		replication.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(50));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("05/19/2020", "05/19/2020")
				.withExpectedResults(
						"05/19/2020     05/19/2020     AUD/USD Curr Fwd 05/29/2020     8.33333",
						"05/19/2020     05/19/2020     AUD/EUR Curr Fwd 05/29/2020     16.66667",
						"05/19/2020     05/19/2020     AUD/USD Curr Fwd 06/30/2020     8.33333",
						"05/19/2020     05/19/2020     AUD/EUR Curr Fwd 06/30/2020     16.66667",
						"05/19/2020     05/19/2020     AUD/USD Curr Fwd 07/31/2020     8.33333",
						"05/19/2020     05/19/2020     AUD/EUR Curr Fwd 07/31/2020     16.66667"
				).execute();
	}


	@Test
	public void testBlendedReplicationCalculator_50_50() {
		InvestmentReplication replication = getReplicationWithChildren("Blended");
		replication.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(50));
		replication.getAllocationList().get(1).setAllocationWeight(BigDecimal.valueOf(50));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("05/14/2020", "05/19/2020")
				.withExpectedResults(
						"05/14/2020     05/19/2020     AUD/EUR Curr Fwd 05/29/2020     16.66667",
						"05/14/2020     05/19/2020     AUD/USD Curr Fwd 05/29/2020     16.66667",
						"05/14/2020     05/19/2020     AUD/EUR Curr Fwd 06/30/2020     16.66667",
						"05/14/2020     05/19/2020     AUD/USD Curr Fwd 06/30/2020     16.66667",
						"05/14/2020     05/19/2020     AUD/EUR Curr Fwd 07/31/2020     16.66667",
						"05/14/2020     05/19/2020     AUD/USD Curr Fwd 07/31/2020     16.66667"
				).execute();
	}


	@Test
	public void testBlendedReplicationCalculator_Treasury_75_25() {
		InvestmentReplication replication = getReplicationWithChildren("Blended", ArrayUtils.toArray("100% 2yr Treasury (TU)", "100% 10yr Treasury (TY)"));
		replication.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(75));
		replication.getAllocationList().get(1).setAllocationWeight(BigDecimal.valueOf(25));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("01/01/2016", "06/18/2020")
				.withExpectedResults(
						"05/21/2020     06/18/2020     US 10YR NOTE (CBT)Sep20     75.00000",
						"05/21/2020     06/18/2020     US 2YR NOTE (CBT) Sep20     25.00000",
						"02/21/2020     05/20/2020     US 10YR NOTE (CBT)Jun20     75.00000",
						"02/21/2020     05/20/2020     US 2YR NOTE (CBT) Jun20     25.00000",
						"11/22/2019     02/20/2020     US 10YR NOTE (CBT)Mar20     75.00000",
						"11/22/2019     02/20/2020     US 2YR NOTE (CBT) Mar20     25.00000",
						"08/26/2019     11/21/2019     US 10YR NOTE (CBT)Dec19     75.00000",
						"08/26/2019     11/21/2019     US 2YR NOTE (CBT) Dec19     25.00000",
						"05/24/2019     08/23/2019     US 10YR NOTE (CBT)Sep19     75.00000",
						"05/24/2019     08/23/2019     US 2YR NOTE (CBT) Sep19     25.00000",
						"02/22/2019     05/23/2019     US 10YR NOTE (CBT)Jun19     75.00000",
						"02/22/2019     05/23/2019     US 2YR NOTE (CBT) Jun19     25.00000",
						"02/19/2019     02/21/2019     US 10YR NOTE (CBT)Mar19     75.00000",
						"02/19/2019     02/21/2019     US 2YR NOTE (CBT) Mar19     25.00000",
						"02/18/2019     02/18/2019     US 10YR NOTE (CBT)Jun19     75.00000",
						"02/18/2019     02/18/2019     US 2YR NOTE (CBT) Mar19     25.00000",
						"11/30/2018     02/15/2019     US 10YR NOTE (CBT)Mar19     75.00000",
						"11/30/2018     02/15/2019     US 2YR NOTE (CBT) Mar19     25.00000",
						"08/31/2018     11/29/2018     US 10YR NOTE (CBT)Dec18     75.00000",
						"08/31/2018     11/29/2018     US 2YR NOTE (CBT) Dec18     25.00000",
						"05/31/2018     08/30/2018     US 10YR NOTE (CBT)Sep18     75.00000",
						"05/31/2018     08/30/2018     US 2YR NOTE (CBT) Sep18     25.00000",
						"02/28/2018     05/30/2018     US 10YR NOTE (CBT)Jun18     75.00000",
						"02/28/2018     05/30/2018     US 2YR NOTE (CBT) Jun18     25.00000",
						"11/30/2017     02/27/2018     US 10YR NOTE (CBT)Mar18     75.00000",
						"11/30/2017     02/27/2018     US 2YR NOTE (CBT) Mar18     25.00000",
						"08/31/2017     11/29/2017     US 10YR NOTE (CBT)Dec17     75.00000",
						"08/31/2017     11/29/2017     US 2YR NOTE (CBT) Dec17     25.00000",
						"05/31/2017     08/30/2017     US 10YR NOTE (CBT)Sep17     75.00000",
						"05/31/2017     08/30/2017     US 2YR NOTE (CBT) Sep17     25.00000",
						"05/25/2017     05/30/2017     US 10YR NOTE (CBT)Jun17     75.00000",
						"05/25/2017     05/30/2017     US 2YR NOTE (CBT) Jun17     25.00000",
						"02/28/2017     05/24/2017     US 10YR NOTE (CBT)Jun17     75.00000",
						"02/28/2017     05/24/2017     US 2YR NOTE (CBT) Jun17     25.00000",
						"02/23/2017     02/27/2017     US 10YR NOTE (CBT)Mar17     75.00000",
						"02/23/2017     02/27/2017     US 2YR NOTE (CBT) Jun17     25.00000",
						"11/30/2016     02/22/2017     US 10YR NOTE (CBT)Mar17     75.00000",
						"11/30/2016     02/22/2017     US 2YR NOTE (CBT) Mar17     25.00000",
						"08/31/2016     11/29/2016     US 10YR NOTE (CBT)Dec16     75.00000",
						"08/31/2016     11/29/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"05/31/2016     08/30/2016     US 10YR NOTE (CBT)Sep16     75.00000",
						"05/31/2016     08/30/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"02/29/2016     05/30/2016     US 10YR NOTE (CBT)Jun16     75.00000",
						"02/29/2016     05/30/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"02/22/2016     02/26/2016     US 10YR NOTE (CBT) Mar16     75.00000",
						"02/22/2016     02/26/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"01/01/2016     02/19/2016     US LONG BOND(CBT) Mar16     75.00000",
						"01/01/2016     02/19/2016     US 2YR NOTE (CBT) Mar17     25.00000"
				).execute();
	}


	@Test
	public void testBlendedReplicationCalculator_RebuildDynamically_Treasury_75_25() {
		InvestmentReplication replication = getReplicationWithChildren("Blended", ArrayUtils.toArray("100% 2yr Treasury (TU)", "100% 10yr Treasury (TY)"));
		SystemBean calculatorBean = SystemBeanBuilder.newBuilder(RandomUtils.randomNameWithPrefix("Blended Replication Calculator"), InvestmentReplicationCalculatorBlended.BLENDED_REPLICATION_SYSTEM_BEAN_TYPE_NAME, this.systemBeanService)
				.buildAndGetOrSave();
		replication.setDynamicCalculatorBean(calculatorBean);
		replication.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(75));
		replication.getAllocationList().get(1).setAllocationWeight(BigDecimal.valueOf(25));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("01/01/2016", "03/19/2020")
				.withExpectedResults(
						"02/28/2020     03/19/2020     US 10YR NOTE (CBT)Jun20     75.00000",
						"02/28/2020     03/19/2020     US 2YR NOTE (CBT) Jun20     25.00000",
						"11/29/2019     02/27/2020     US 10YR NOTE (CBT)Mar20     75.00000",
						"11/29/2019     02/27/2020     US 2YR NOTE (CBT) Mar20     25.00000",
						"08/30/2019     11/28/2019     US 10YR NOTE (CBT)Dec19     75.00000",
						"08/30/2019     11/28/2019     US 2YR NOTE (CBT) Dec19     25.00000",
						"05/31/2019     08/29/2019     US 10YR NOTE (CBT)Sep19     75.00000",
						"05/31/2019     08/29/2019     US 2YR NOTE (CBT) Sep19     25.00000",
						"02/28/2019     05/30/2019     US 10YR NOTE (CBT)Jun19     75.00000",
						"02/28/2019     05/30/2019     US 2YR NOTE (CBT) Jun19     25.00000",
						"11/30/2018     02/27/2019     US 10YR NOTE (CBT)Mar19     75.00000",
						"11/30/2018     02/27/2019     US 2YR NOTE (CBT) Mar19     25.00000",
						"08/31/2018     11/29/2018     US 10YR NOTE (CBT)Dec18     75.00000",
						"08/31/2018     11/29/2018     US 2YR NOTE (CBT) Dec18     25.00000",
						"05/31/2018     08/30/2018     US 10YR NOTE (CBT)Sep18     75.00000",
						"05/31/2018     08/30/2018     US 2YR NOTE (CBT) Sep18     25.00000",
						"02/28/2018     05/30/2018     US 10YR NOTE (CBT)Jun18     75.00000",
						"02/28/2018     05/30/2018     US 2YR NOTE (CBT) Jun18     25.00000",
						"11/30/2017     02/27/2018     US 10YR NOTE (CBT)Mar18     75.00000",
						"11/30/2017     02/27/2018     US 2YR NOTE (CBT) Mar18     25.00000",
						"08/31/2017     11/29/2017     US 10YR NOTE (CBT)Dec17     75.00000",
						"08/31/2017     11/29/2017     US 2YR NOTE (CBT) Dec17     25.00000",
						"05/31/2017     08/30/2017     US 10YR NOTE (CBT)Sep17     75.00000",
						"05/31/2017     08/30/2017     US 2YR NOTE (CBT) Sep17     25.00000",
						"05/25/2017     05/30/2017     US 10YR NOTE (CBT)Jun17     75.00000",
						"05/25/2017     05/30/2017     US 2YR NOTE (CBT) Jun17     25.00000",
						"02/28/2017     05/24/2017     US 10YR NOTE (CBT)Jun17     75.00000",
						"02/28/2017     05/24/2017     US 2YR NOTE (CBT) Jun17     25.00000",
						"02/23/2017     02/27/2017     US 10YR NOTE (CBT)Mar17     75.00000",
						"02/23/2017     02/27/2017     US 2YR NOTE (CBT) Jun17     25.00000",
						"11/30/2016     02/22/2017     US 10YR NOTE (CBT)Mar17     75.00000",
						"11/30/2016     02/22/2017     US 2YR NOTE (CBT) Mar17     25.00000",
						"08/31/2016     11/29/2016     US 10YR NOTE (CBT)Dec16     75.00000",
						"08/31/2016     11/29/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"05/31/2016     08/30/2016     US 10YR NOTE (CBT)Sep16     75.00000",
						"05/31/2016     08/30/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"02/29/2016     05/30/2016     US 10YR NOTE (CBT)Jun16     75.00000",
						"02/29/2016     05/30/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"02/22/2016     02/26/2016     US 10YR NOTE (CBT) Mar16     75.00000",
						"02/22/2016     02/26/2016     US 2YR NOTE (CBT) Mar17     25.00000",
						"01/01/2016     02/19/2016     US LONG BOND(CBT) Mar16     75.00000",
						"01/01/2016     02/19/2016     US 2YR NOTE (CBT) Mar17     25.00000"
				).execute();
	}


	@Test
	public void testBlendedReplicationCalculator_MergeAllocationsWithSameSecurity() {
		InvestmentReplication replication = getReplicationWithChildren("Blended", ArrayUtils.toArray("One Contract - DE S&P 500 E-Mini", "50% One Contract - DE S&P 500 E-Mini"));
		replication.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(75));
		replication.getAllocationList().get(1).setAllocationWeight(BigDecimal.valueOf(25));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("05/19/2020", "06/18/2020")
				.withExpectedResults(
						"06/11/2020     06/18/2020     S&P500 EMINI FUT  Sep20     62.50000",
						"05/19/2020     06/10/2020     S&P500 EMINI FUT  Jun20     62.50000"
				).execute();
	}


	@Test
	public void testBlendedReplicationCalculator_ChangeChildAllocation() {
		InvestmentReplication replication = getReplicationWithChildren("Blended", ArrayUtils.toArray("One Contract - DE S&P 500 E-Mini", "50% One Contract - DE S&P 500 E-Mini"));
		replication.getAllocationList().get(0).setAllocationWeight(BigDecimal.valueOf(75));
		replication.getAllocationList().get(1).setAllocationWeight(BigDecimal.valueOf(25));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("06/11/2020", "06/18/2020")
				.withExpectedResults(
						"06/11/2020     06/18/2020     S&P500 EMINI FUT  Sep20     62.50000"
				).execute();

		replication.setAllocationList(this.investmentReplicationUtils.getDefaultChildAllocations(replication, ArrayUtils.toArray("One Contract - DE S&P 500 E-Mini", "100% 10yr Treasury (TY)")));
		replication = this.investmentReplicationService.saveInvestmentReplication(replication);
		InvestmentReplicationCalculatorTestExecutor.newExecutorWithReplication(getServiceHolder(), replication)
				.withBalanceStartAndEndDates("06/18/2020", "06/18/2020")
				.withExpectedResults(
						"06/18/2020     06/18/2020     US 10YR NOTE (CBT)Sep20     75.00000",
						"06/18/2020     06/18/2020     S&P500 EMINI FUT  Sep20     25.00000",
						"06/11/2020     06/18/2020     S&P500 EMINI FUT  Sep20     62.50000" // end date of this history was adjusted after child allocations were changed and rebuilt
				).execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentReplication getReplicationWithChildren(String replicationName) {
		return getReplicationWithChildren(replicationName, ArrayUtils.toArray("3 Tranche (Equal Weight) Forwards: AUD/EUR", "3 Tranche (Equal Weight) Forwards: AUD/USD"));
	}


	private InvestmentReplication getReplicationWithChildren(String replicationName, String[] childAllocationNames) {
		return this.investmentReplicationUtils.createReplicationWithRandomTestNamePrefix(replicationName, "Blended", "Default Blended Replication Calculator", childAllocationNames);
	}


	private InvestmentReplicationCalculatorTestServiceHolder getServiceHolder() {
		InvestmentReplicationCalculatorTestServiceHolder serviceHolder = new InvestmentReplicationCalculatorTestServiceHolder();
		serviceHolder.setCalendarSetupService(this.calendarSetupService);
		serviceHolder.setInvestmentInstrumentService(this.investmentInstrumentService);
		serviceHolder.setInvestmentReplicationHistoryRebuildService(this.investmentReplicationHistoryRebuildService);
		serviceHolder.setInvestmentInstrumentService(this.investmentInstrumentService);
		serviceHolder.setInvestmentReplicationService(this.investmentReplicationService);
		serviceHolder.setSystemBeanService(this.systemBeanService);
		serviceHolder.setInvestmentReplicationHistoryService(this.investmentReplicationHistoryService);
		return serviceHolder;
	}
}
