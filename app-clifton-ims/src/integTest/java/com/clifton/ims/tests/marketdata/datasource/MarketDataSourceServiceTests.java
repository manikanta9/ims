package com.clifton.ims.tests.marketdata.datasource;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.StringUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


public class MarketDataSourceServiceTests extends BaseImsIntegrationTest {

	@Resource
	private MarketDataSourceService marketDataSourceService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	static final Map<String, String> EXPECTED_DATA_SOURCE_MAP = new HashMap<>();


	static {
		EXPECTED_DATA_SOURCE_MAP.put("Bank of America, N.A.::Custodian", "Bank of America, N.A.");
		EXPECTED_DATA_SOURCE_MAP.put("Bloomberg::Vendor", "Bloomberg");
		EXPECTED_DATA_SOURCE_MAP.put("BofA Securities Inc::Broker", "Bank of America Corp");
		EXPECTED_DATA_SOURCE_MAP.put("BondEdge::Vendor", "BondEdge");
		EXPECTED_DATA_SOURCE_MAP.put("Chicago Mercantile Exchange::Securities Exchange", "Chicago Mercantile Exchange");
		EXPECTED_DATA_SOURCE_MAP.put("Citibank N.A.::Broker", "Citibank N.A.");
		EXPECTED_DATA_SOURCE_MAP.put("Citigroup Global Markets Inc.::Broker", "Citigroup");
		EXPECTED_DATA_SOURCE_MAP.put("Citigroup Global Markets Inc./Salomon Brothers::Broker", "Citigroup");
		EXPECTED_DATA_SOURCE_MAP.put("Citigroup Japan Holdings GK::Other Issuer", "Citigroup");
		EXPECTED_DATA_SOURCE_MAP.put("Citicorp::Other Issuer", "Citigroup");
		EXPECTED_DATA_SOURCE_MAP.put("Citigroup Global Markets Inc/Chicago::Broker", "Citigroup");
		EXPECTED_DATA_SOURCE_MAP.put("Citi Private Bank::Broker", "Citigroup");
		EXPECTED_DATA_SOURCE_MAP.put("Deutsche Bank AG::Broker", "Deutsche Bank AG");
		EXPECTED_DATA_SOURCE_MAP.put("FX Connect::Vendor", "FX Connect");
		EXPECTED_DATA_SOURCE_MAP.put("Goldman Sachs & Co.::Broker", "Goldman Sachs");
		EXPECTED_DATA_SOURCE_MAP.put("Goldman Sachs Bank::Custodian", "Goldman Sachs");
		EXPECTED_DATA_SOURCE_MAP.put("Goldman Sachs Execution & Clearing::Broker", "Goldman Sachs");
		EXPECTED_DATA_SOURCE_MAP.put("Goldman Sachs Bank USA::Broker", "Goldman Sachs");
		EXPECTED_DATA_SOURCE_MAP.put("Goldman Sachs International::Broker", "Goldman Sachs");
		EXPECTED_DATA_SOURCE_MAP.put("Interactive Data Holdings Corp::Other Issuer", "Intercontinental Exchange");
		EXPECTED_DATA_SOURCE_MAP.put("J.P. Morgan Securities LLC::Broker", "JP Morgan Chase & Co.");
		EXPECTED_DATA_SOURCE_MAP.put("Markit Portfolio Valuations::Vendor", "Markit Portfolio Valuations");
		EXPECTED_DATA_SOURCE_MAP.put("Morgan Stanley & Co. International PLC::Broker", "Morgan Stanley & Co. Inc.");
		EXPECTED_DATA_SOURCE_MAP.put("Morgan Stanley Capital Services, Inc.::Broker", "Morgan Stanley & Co. Inc.");
		EXPECTED_DATA_SOURCE_MAP.put("Graystone Consulting Ltd::Consultant", "Morgan Stanley & Co. Inc.");
		EXPECTED_DATA_SOURCE_MAP.put("Newedge USA, LLC::Broker", "Newedge USA, LLC");
		EXPECTED_DATA_SOURCE_MAP.put("Northern Trust Hedge Fund Services::3rd Party Administrator", "Northern Trust Hedge Fund Services");
		EXPECTED_DATA_SOURCE_MAP.put("Parametric Clifton Funds::Broker", "Parametric Clifton Funds");
		EXPECTED_DATA_SOURCE_MAP.put("Parametric Seattle Custodian::Custodian", "Parametric Seattle Custodian");
		EXPECTED_DATA_SOURCE_MAP.put("Societe Generale::Broker", "Societe Generale");
		EXPECTED_DATA_SOURCE_MAP.put("UBS Securities, LLC::Broker", "UBS Securities, LLC");
		EXPECTED_DATA_SOURCE_MAP.put("WM/Reuters::Vendor", "WM/Reuters");
		EXPECTED_DATA_SOURCE_MAP.put("Wolverine Execution Services, LLC::Broker", "Wolverine Execution Services, LLC");
	}


	@Test
	public void testMarketDataSourceServiceLookUp() {
		EXPECTED_DATA_SOURCE_MAP.forEach(this::testMarketDataSourceServiceLookUpExecute);
	}


	private void testMarketDataSourceServiceLookUpExecute(String companyName, String expectedDataSourceName) {
		MarketDataSource expectedDataSource = RequestOverrideHandler.serviceCallWithOverrides(() -> this.marketDataSourceService.getMarketDataSourceByName(expectedDataSourceName),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_PROPERTIES_ROOT_PARAMETER, "data")
		);

		String[] companyNameAndType = StringUtils.split(companyName, "::");
		BusinessCompany loopUpCompany = this.businessCompanyService.getBusinessCompanyByTypeAndName(companyNameAndType[1], companyNameAndType[0]);

		MarketDataSource foundMarketDataSource = this.marketDataSourceService.getMarketDataSourceByCompany(loopUpCompany);

		Assertions.assertTrue(foundMarketDataSource != null && foundMarketDataSource.equals(expectedDataSource), "Expected to find " + expectedDataSourceName + " datasource for company " + companyName + ", but instead " + ((foundMarketDataSource == null) ? " didn't find any " : " found " + foundMarketDataSource.getName()));
	}
}
