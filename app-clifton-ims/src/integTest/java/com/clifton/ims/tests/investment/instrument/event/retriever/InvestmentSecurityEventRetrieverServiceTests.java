package com.clifton.ims.tests.investment.instrument.event.retriever;


import com.clifton.accounting.eventjournal.AccountingEventJournal;
import com.clifton.accounting.eventjournal.AccountingEventJournalService;
import com.clifton.accounting.eventjournal.search.AccountingEventJournalSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.InvestmentSecurityEventTypeHierarchy;
import com.clifton.investment.instrument.event.retriever.InvestmentSecurityEventRetrieverService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.test.util.RandomUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class InvestmentSecurityEventRetrieverServiceTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentSecurityEventRetrieverService investmentSecurityEventRetrieverService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private AccountingEventJournalService accountingEventJournalService;

	@Resource
	private InvestmentSetupService investmentSetupService;


	private static final String SYMBOL_DATE_FORMAT = "yyyyMMddHHmmss.SSS";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testEventCopyThroughInstrumentUnderlying() {
		List<InvestmentSecurityEventTypeHierarchy> typeHierarchiesWithCopyFromUnderlying = this.investmentSecurityEventService.getInvestmentSecurityEventTypeHierarchyListByEventType((short) 9);
		typeHierarchiesWithCopyFromUnderlying = BeanUtils.filter(typeHierarchiesWithCopyFromUnderlying, InvestmentSecurityEventTypeHierarchy::isCopiedFromUnderlying, true);

		//disable credit event copy from underlying feature for tests
		setEventTypeCopy(typeHierarchiesWithCopyFromUnderlying, false);

		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		searchForm.setEventSameForInstrumentSecurities(true);
		List<InvestmentInstrumentHierarchy> instrumentHierarchyCopyList = this.investmentSetupService.getInvestmentInstrumentHierarchyList(searchForm);

		//Disable hierarchy event copy for tests
		disableHierarchyListCopyFeature(instrumentHierarchyCopyList);

		try {
			InvestmentSecurity underlyingBenchmark = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CY180985");
			InvestmentSecurity creditDefaultSwap1 = this.investmentInstrumentUtils.getCopyOfSecurity("70305133", "70305133" + RandomUtils.randomNumber());
			InvestmentSecurity creditDefaultSwap2 = this.investmentInstrumentUtils.getCopyOfSecurity("70507564", "70507564" + RandomUtils.randomNumber());
			InvestmentSecurity totalReturnSwap = this.investmentInstrumentUtils.getCopyOfSecurity("EA25322KIM", "EA25322KIM" + RandomUtils.randomNumber());

			deleteAllInvestmentSecurityEventsForSecurity(underlyingBenchmark, true);
			deleteAllInvestmentSecurityEventsForSecurity(creditDefaultSwap1, true);
			deleteAllInvestmentSecurityEventsForSecurity(creditDefaultSwap2, true);
			deleteAllInvestmentSecurityEventsForSecurity(totalReturnSwap, true);

			InvestmentSecurityEventType creditEventType = this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CREDIT_EVENT);
			InvestmentSecurityEventTypeHierarchy eventTypeHierarchy = this.investmentSecurityEventService.getInvestmentSecurityEventTypeHierarchyByEventTypeAndHierarchy(creditEventType.getId(), creditDefaultSwap1.getInstrument().getHierarchy().getId());
			eventTypeHierarchy.setCopiedFromUnderlying(false);
			this.investmentSecurityEventService.saveInvestmentSecurityEventTypeHierarchy(eventTypeHierarchy);

			if (this.investmentSecurityEventService.getInvestmentSecurityEventTypeHierarchyByEventTypeAndHierarchy(creditEventType.getId(), underlyingBenchmark.getInstrument().getHierarchy().getId()) == null) {
				InvestmentSecurityEventTypeHierarchy underlyingEventTypeHierarchy = new InvestmentSecurityEventTypeHierarchy();
				underlyingEventTypeHierarchy.setReferenceTwo(underlyingBenchmark.getInstrument().getHierarchy());
				underlyingEventTypeHierarchy.setReferenceOne(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CREDIT_EVENT));
				this.investmentSecurityEventService.saveInvestmentSecurityEventTypeHierarchy(underlyingEventTypeHierarchy);
			}
			InvestmentInstrumentHierarchy hierarchy = creditDefaultSwap1.getInstrument().getHierarchy();
			hierarchy.setEventSameForInstrumentSecurities(false);
			this.investmentSetupService.saveInvestmentInstrumentHierarchy(hierarchy);

			//Add event to underlyingBenchmark, Validate only it has it
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(createInvestmentSecurityEvent(underlyingBenchmark, DateUtils.toDate("03/19/2014"), InvestmentSecurityEventType.CREDIT_EVENT));
			Assertions.assertTrue(CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(underlyingBenchmark.getId())) == 1 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(creditDefaultSwap1.getId())) == 0 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(creditDefaultSwap2.getId())) == 0 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(totalReturnSwap.getId())) == 0);

			//Now enable copy from underlying on credit event type for credit default swap hierarchy
			eventTypeHierarchy = this.investmentSecurityEventService.getInvestmentSecurityEventTypeHierarchyByEventTypeAndHierarchy(creditEventType.getId(), creditDefaultSwap1.getInstrument().getHierarchy().getId());
			eventTypeHierarchy.setCopiedFromUnderlying(true);
			this.investmentSecurityEventService.saveInvestmentSecurityEventTypeHierarchy(eventTypeHierarchy);

			//Add event to underlyingBenchmark, Validate it gets it along with the credit default swaps and not the total return swap
			this.investmentSecurityEventService.saveInvestmentSecurityEvent(createInvestmentSecurityEvent(underlyingBenchmark, DateUtils.toDate("02/19/2014"), InvestmentSecurityEventType.CREDIT_EVENT));
			Assertions.assertTrue(CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(underlyingBenchmark.getId())) == 2 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(creditDefaultSwap1.getId())) == 1 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(creditDefaultSwap2.getId())) == 1 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(totalReturnSwap.getId())) == 0);

			//Change hierarchy back for other tests
			hierarchy.setEventSameForInstrumentSecurities(true);
			this.investmentSetupService.saveInvestmentInstrumentHierarchy(hierarchy);

			//disable credit event copy from underlying feature for tests
			setEventTypeCopy(typeHierarchiesWithCopyFromUnderlying, true);
		}
		finally {
			//re-enable hierarchy event copy for tests
			enableHierarchyListCopyFeature(instrumentHierarchyCopyList);
		}
	}


	private void disableHierarchyListCopyFeature(List<InvestmentInstrumentHierarchy> hierarchyList) {
		for (InvestmentInstrumentHierarchy hierarchy : CollectionUtils.getIterable(hierarchyList)) {
			hierarchy.setEventSameForInstrumentSecurities(false);
			this.investmentSetupService.saveInvestmentInstrumentHierarchy(hierarchy);
		}
	}


	private void enableHierarchyListCopyFeature(List<InvestmentInstrumentHierarchy> hierarchyList) {
		for (InvestmentInstrumentHierarchy hierarchy : CollectionUtils.getIterable(hierarchyList)) {
			hierarchy.setEventSameForInstrumentSecurities(true);
			this.investmentSetupService.saveInvestmentInstrumentHierarchy(hierarchy);
		}
	}


	private void setEventTypeCopy(List<InvestmentSecurityEventTypeHierarchy> typeHierarchyList, boolean enable) {
		for (InvestmentSecurityEventTypeHierarchy eventTypeHierarchy : CollectionUtils.getIterable(typeHierarchyList)) {
			eventTypeHierarchy.setCopiedFromUnderlying(enable);
			this.investmentSecurityEventService.saveInvestmentSecurityEventTypeHierarchy(eventTypeHierarchy);
		}
	}


	@Test
	public void testEventCopyObserverOnInactiveToActiveSecurity() {
		//Create two securities under the same instrument
		InvestmentSecurity securityOne = this.investmentInstrumentUtils.getCopyOfSecurity("9SENW", "9SENW" + RandomUtils.randomNumber());
		InvestmentSecurity securityTwo = this.investmentInstrumentUtils.getCopyOfSecurity("9SENV", "9SENV" + RandomUtils.randomNumber());

		securityOne.setStartDate(DateUtils.toDate("03/19/2000"));
		securityTwo.setStartDate(DateUtils.toDate("03/19/2000"));

		this.investmentInstrumentService.saveInvestmentSecurity(securityOne);
		this.investmentInstrumentService.saveInvestmentSecurity(securityTwo);
		deleteAllInvestmentSecurityEventsForSecurity(securityTwo);
		deleteAllInvestmentSecurityEventsForSecurity(securityOne);

		//Add event to one, Validate both have it
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(createInvestmentSecurityEvent(securityOne, DateUtils.toDate("03/19/2005")));
		Assertions.assertTrue(CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityOne.getId())) == 1 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityTwo.getId())) == 1);

		//Change active date on one security to the future (Making it inactive)
		securityOne.setStartDate(DateUtils.toDate("03/19/2015"));
		this.investmentInstrumentService.saveInvestmentSecurity(securityOne);

		//Add event on active security
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(createInvestmentSecurityEvent(securityTwo, DateUtils.toDate("03/19/2006")));

		//Validate the inactive security did not receive it
		Assertions.assertTrue(CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityOne.getId())) == 1 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityTwo.getId())) == 2);

		//Make inactive security active
		securityOne.setStartDate(DateUtils.toDate("03/19/2000"));
		this.investmentInstrumentService.saveInvestmentSecurity(securityOne);

		Assertions.assertTrue(CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityOne.getId())) == 1 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityTwo.getId())) == 2);

		//Add event to newly active security
		this.investmentSecurityEventService.saveInvestmentSecurityEvent(createInvestmentSecurityEvent(securityOne, DateUtils.toDate("03/19/2006")));

		//Validate that it didn't get copied to the other
		Assertions.assertTrue(CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityOne.getId())) == 2 && CollectionUtils.getSize(getInvestmentSecurityEventListBySecurity(securityTwo.getId())) == 2);
	}


	private void deleteAllInvestmentSecurityEventsForSecurity(InvestmentSecurity security) {
		deleteAllInvestmentSecurityEventsForSecurity(security, false);
	}


	private void deleteAllInvestmentSecurityEventsForSecurity(InvestmentSecurity security, boolean deleteByEventId) {
		for (InvestmentSecurityEvent event : CollectionUtils.getIterable(getInvestmentSecurityEventListBySecurity(security.getId()))) {
			AccountingEventJournalSearchForm searchForm = new AccountingEventJournalSearchForm();
			if (deleteByEventId) {
				searchForm.setSecurityEventId(event.getId());
			}
			else {
				searchForm.setSecurityInstrumentHierarchyId(security.getInstrument().getHierarchy().getId());
				searchForm.setEventDate(event.getEventDate());
			}

			for (AccountingEventJournal eventJournal : CollectionUtils.getIterable(this.accountingEventJournalService.getAccountingEventJournalList(searchForm))) {
				this.accountingEventJournalService.deleteAccountingEventJournalWithForcedUnposting(eventJournal.getId());
			}
			this.investmentSecurityEventService.deleteInvestmentSecurityEvent(event.getId());
		}
	}


	private List<InvestmentSecurityEvent> getInvestmentSecurityEventListBySecurity(Integer securityId) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(securityId);
		return this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
	}


	private InvestmentSecurityEvent createInvestmentSecurityEvent(InvestmentSecurity security, Date date) {
		return createInvestmentSecurityEvent(security, date, InvestmentSecurityEventType.PREMIUM_LEG_PAYMENT);
	}


	private InvestmentSecurityEvent createInvestmentSecurityEvent(InvestmentSecurity security, Date date, String eventTypeName) {
		InvestmentSecurityEventType eventType = this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(eventTypeName);
		InvestmentSecurityEvent securityEvent = new InvestmentSecurityEvent();
		securityEvent.setSecurity(security);
		securityEvent.setType(eventType);
		securityEvent.setAccrualEndDate(date);
		securityEvent.setAccrualStartDate(date);
		securityEvent.setEventDate(date);
		securityEvent.setExDate(date);
		securityEvent.setPaymentDate(date);
		securityEvent.setBeforeAndAfterEventValue(BigDecimal.ONE);
		return securityEvent;
	}


	@Test
	public void testFixLeg_USD3L_20140603_20340603_3_169() {

		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("USD3L-20140603-20340603-3.169", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Fixed Leg Payment";
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Fixed Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(40, events.size());

		Assertions.assertEquals("12/05/2033	06/05/2034	06/05/2034	06/05/2034", formatInterestEvent(events.get(0), true));
		Assertions.assertEquals("06/03/2033	12/05/2033	12/05/2033	12/05/2033", formatInterestEvent(events.get(1), true));
		Assertions.assertEquals("12/03/2032	06/03/2033	06/03/2033	06/03/2033", formatInterestEvent(events.get(2), true));
		Assertions.assertEquals("06/03/2032	12/03/2032	12/03/2032	12/03/2032", formatInterestEvent(events.get(3), true));
		Assertions.assertEquals("12/03/2031	06/03/2032	06/03/2032	06/03/2032", formatInterestEvent(events.get(4), true));
		Assertions.assertEquals("06/03/2031	12/03/2031	12/03/2031	12/03/2031", formatInterestEvent(events.get(5), true));
		Assertions.assertEquals("12/03/2030	06/03/2031	06/03/2031	06/03/2031", formatInterestEvent(events.get(6), true));
		Assertions.assertEquals("06/03/2030	12/03/2030	12/03/2030	12/03/2030", formatInterestEvent(events.get(7), true));
		Assertions.assertEquals("12/03/2029	06/03/2030	06/03/2030	06/03/2030", formatInterestEvent(events.get(8), true));
		Assertions.assertEquals("06/04/2029	12/03/2029	12/03/2029	12/03/2029", formatInterestEvent(events.get(9), true));
		Assertions.assertEquals("12/04/2028	06/04/2029	06/04/2029	06/04/2029", formatInterestEvent(events.get(10), true));
		Assertions.assertEquals("06/05/2028	12/04/2028	12/04/2028	12/04/2028", formatInterestEvent(events.get(11), true));
		Assertions.assertEquals("12/03/2027	06/05/2028	06/05/2028	06/05/2028", formatInterestEvent(events.get(12), true));
		Assertions.assertEquals("06/03/2027	12/03/2027	12/03/2027	12/03/2027", formatInterestEvent(events.get(13), true));
		Assertions.assertEquals("12/03/2026	06/03/2027	06/03/2027	06/03/2027", formatInterestEvent(events.get(14), true));
		Assertions.assertEquals("06/03/2026	12/03/2026	12/03/2026	12/03/2026", formatInterestEvent(events.get(15), true));
		Assertions.assertEquals("12/03/2025	06/03/2026	06/03/2026	06/03/2026", formatInterestEvent(events.get(16), true));
		Assertions.assertEquals("06/03/2025	12/03/2025	12/03/2025	12/03/2025", formatInterestEvent(events.get(17), true));
		Assertions.assertEquals("12/03/2024	06/03/2025	06/03/2025	06/03/2025", formatInterestEvent(events.get(18), true));
		Assertions.assertEquals("06/03/2024	12/03/2024	12/03/2024	12/03/2024", formatInterestEvent(events.get(19), true));
		Assertions.assertEquals("12/04/2023	06/03/2024	06/03/2024	06/03/2024", formatInterestEvent(events.get(20), true));
		Assertions.assertEquals("06/05/2023	12/04/2023	12/04/2023	12/04/2023", formatInterestEvent(events.get(21), true));
		Assertions.assertEquals("12/05/2022	06/05/2023	06/05/2023	06/05/2023", formatInterestEvent(events.get(22), true));
		Assertions.assertEquals("06/06/2022	12/05/2022	12/05/2022	12/05/2022", formatInterestEvent(events.get(23), true));
		Assertions.assertEquals("12/03/2021	06/06/2022	06/06/2022	06/06/2022", formatInterestEvent(events.get(24), true));
		Assertions.assertEquals("06/03/2021	12/03/2021	12/03/2021	12/03/2021", formatInterestEvent(events.get(25), true));
		Assertions.assertEquals("12/03/2020	06/03/2021	06/03/2021	06/03/2021", formatInterestEvent(events.get(26), true));
		Assertions.assertEquals("06/03/2020	12/03/2020	12/03/2020	12/03/2020", formatInterestEvent(events.get(27), true));
		Assertions.assertEquals("12/03/2019	06/03/2020	06/03/2020	06/03/2020", formatInterestEvent(events.get(28), true));
		Assertions.assertEquals("06/03/2019	12/03/2019	12/03/2019	12/03/2019", formatInterestEvent(events.get(29), true));
		Assertions.assertEquals("12/03/2018	06/03/2019	06/03/2019	06/03/2019", formatInterestEvent(events.get(30), true));
		Assertions.assertEquals("06/04/2018	12/03/2018	12/03/2018	12/03/2018", formatInterestEvent(events.get(31), true));
		Assertions.assertEquals("12/04/2017	06/04/2018	06/04/2018	06/04/2018", formatInterestEvent(events.get(32), true));
		Assertions.assertEquals("06/05/2017	12/04/2017	12/04/2017	12/04/2017", formatInterestEvent(events.get(33), true));
		Assertions.assertEquals("12/05/2016	06/05/2017	06/05/2017	06/05/2017", formatInterestEvent(events.get(34), true));
		Assertions.assertEquals("06/03/2016	12/05/2016	12/05/2016	12/05/2016", formatInterestEvent(events.get(35), true));
		Assertions.assertEquals("12/03/2015	06/03/2016	06/03/2016	06/03/2016", formatInterestEvent(events.get(36), true));
		Assertions.assertEquals("06/03/2015	12/03/2015	12/03/2015	12/03/2015", formatInterestEvent(events.get(37), true));
		Assertions.assertEquals("12/03/2014	06/03/2015	06/03/2015	06/03/2015", formatInterestEvent(events.get(38), true));
		Assertions.assertEquals("06/03/2014	12/03/2014	12/03/2014	12/03/2014", formatInterestEvent(events.get(39), true));
	}


	@Test
	public void testFloatingLeg_USD3L_20160803_20260803_1_4265() {

		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("USD3L-20160803-20260803-1.4265", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Floating Leg Payment";
		Assertions.assertNull(this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, false));

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Floating Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(40, events.size());

		Assertions.assertEquals("05/05/2026	08/03/2026	08/03/2026	08/03/2026	05/07/2026", formatInterestEvent(events.get(0), false));
		Assertions.assertEquals("02/03/2026	05/05/2026	05/05/2026	05/05/2026	02/05/2026", formatInterestEvent(events.get(1), false));
		Assertions.assertEquals("11/03/2025	02/03/2026	02/03/2026	02/03/2026	11/05/2025", formatInterestEvent(events.get(2), false));
		Assertions.assertEquals("08/04/2025	11/03/2025	11/03/2025	11/03/2025	08/06/2025", formatInterestEvent(events.get(3), false));
		Assertions.assertEquals("05/06/2025	08/04/2025	08/04/2025	08/04/2025	05/08/2025", formatInterestEvent(events.get(4), false));
		Assertions.assertEquals("02/03/2025	05/06/2025	05/06/2025	05/06/2025	02/05/2025", formatInterestEvent(events.get(5), false));
		Assertions.assertEquals("11/04/2024	02/03/2025	02/03/2025	02/03/2025	11/06/2024", formatInterestEvent(events.get(6), false));
		Assertions.assertEquals("08/05/2024	11/04/2024	11/04/2024	11/04/2024	08/07/2024", formatInterestEvent(events.get(7), false));
		Assertions.assertEquals("05/03/2024	08/05/2024	08/05/2024	08/05/2024	05/08/2024", formatInterestEvent(events.get(8), false));
		Assertions.assertEquals("02/05/2024	05/03/2024	05/03/2024	05/03/2024	02/07/2024", formatInterestEvent(events.get(9), false));
		Assertions.assertEquals("11/03/2023	02/05/2024	02/05/2024	02/05/2024	11/07/2023", formatInterestEvent(events.get(10), false));
		Assertions.assertEquals("08/03/2023	11/03/2023	11/03/2023	11/03/2023	08/07/2023", formatInterestEvent(events.get(11), false));
		Assertions.assertEquals("05/03/2023	08/03/2023	08/03/2023	08/03/2023	05/05/2023", formatInterestEvent(events.get(12), false));
		Assertions.assertEquals("02/03/2023	05/03/2023	05/03/2023	05/03/2023	02/07/2023", formatInterestEvent(events.get(13), false));
		Assertions.assertEquals("11/03/2022	02/03/2023	02/03/2023	02/03/2023	11/07/2022", formatInterestEvent(events.get(14), false));
		Assertions.assertEquals("08/03/2022	11/03/2022	11/03/2022	11/03/2022	08/05/2022", formatInterestEvent(events.get(15), false));
		Assertions.assertEquals("05/03/2022	08/03/2022	08/03/2022	08/03/2022	05/05/2022", formatInterestEvent(events.get(16), false));
		Assertions.assertEquals("02/03/2022	05/03/2022	05/03/2022	05/03/2022	02/07/2022", formatInterestEvent(events.get(17), false));
		Assertions.assertEquals("11/03/2021	02/03/2022	02/03/2022	02/03/2022	11/05/2021", formatInterestEvent(events.get(18), false));
		Assertions.assertEquals("08/03/2021	11/03/2021	11/03/2021	11/03/2021	08/05/2021", formatInterestEvent(events.get(19), false));
		Assertions.assertEquals("05/04/2021	08/03/2021	08/03/2021	08/03/2021	05/06/2021", formatInterestEvent(events.get(20), false));
		Assertions.assertEquals("02/03/2021	05/04/2021	05/04/2021	05/04/2021	02/05/2021", formatInterestEvent(events.get(21), false));
		Assertions.assertEquals("11/03/2020	02/03/2021	02/03/2021	02/03/2021	11/05/2020", formatInterestEvent(events.get(22), false));
		Assertions.assertEquals("08/03/2020	11/03/2020	11/03/2020	11/03/2020	08/05/2020", formatInterestEvent(events.get(23), false));
		Assertions.assertEquals("05/04/2020	08/03/2020	08/03/2020	08/03/2020	null", formatInterestEvent(events.get(24), false));
		Assertions.assertEquals("02/03/2020	05/04/2020	05/04/2020	05/04/2020	02/05/2020", formatInterestEvent(events.get(25), false));
		Assertions.assertEquals("11/04/2019	02/03/2020	02/03/2020	02/03/2020	11/06/2019", formatInterestEvent(events.get(26), false));
		Assertions.assertEquals("08/05/2019	11/04/2019	11/04/2019	11/04/2019	08/07/2019", formatInterestEvent(events.get(27), false));
		Assertions.assertEquals("05/03/2019	08/05/2019	08/05/2019	08/05/2019	05/08/2019", formatInterestEvent(events.get(28), false));
		Assertions.assertEquals("02/04/2019	05/03/2019	05/03/2019	05/03/2019	02/06/2019", formatInterestEvent(events.get(29), false));
		Assertions.assertEquals("11/05/2018	02/04/2019	02/04/2019	02/04/2019	11/07/2018", formatInterestEvent(events.get(30), false));
		Assertions.assertEquals("08/03/2018	11/05/2018	11/05/2018	11/05/2018	08/07/2018", formatInterestEvent(events.get(31), false));
		Assertions.assertEquals("05/03/2018	08/03/2018	08/03/2018	08/03/2018	05/08/2018", formatInterestEvent(events.get(32), false));
		Assertions.assertEquals("02/05/2018	05/03/2018	05/03/2018	05/03/2018	02/07/2018", formatInterestEvent(events.get(33), false));
		Assertions.assertEquals("11/03/2017	02/05/2018	02/05/2018	02/05/2018	11/07/2017", formatInterestEvent(events.get(34), false));
		Assertions.assertEquals("08/03/2017	11/03/2017	11/03/2017	11/03/2017	08/07/2017", formatInterestEvent(events.get(35), false));
		Assertions.assertEquals("05/03/2017	08/03/2017	08/03/2017	08/03/2017	05/05/2017", formatInterestEvent(events.get(36), false));
		Assertions.assertEquals("02/03/2017	05/03/2017	05/03/2017	05/03/2017	02/07/2017", formatInterestEvent(events.get(37), false));
		Assertions.assertEquals("11/03/2016	02/03/2017	02/03/2017	02/03/2017	11/07/2016", formatInterestEvent(events.get(38), false));
		Assertions.assertEquals("08/03/2016	11/03/2016	11/03/2016	11/03/2016	08/05/2016", formatInterestEvent(events.get(39), false));
	}


	@Test
	public void testFloatingLeg_USD3L_20140603_20340603_3_169() {

		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("USD3L-20140603-20340603-3.169", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Floating Leg Payment";
		Assertions.assertNull(this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, false));

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Floating Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(80, events.size());

		Assertions.assertEquals("03/03/2034	06/05/2034	06/05/2034	06/05/2034", formatInterestEvent(events.get(0), true));
		Assertions.assertEquals("12/05/2033	03/03/2034	03/03/2034	03/03/2034", formatInterestEvent(events.get(1), true));
		Assertions.assertEquals("09/06/2033	12/05/2033	12/05/2033	12/05/2033", formatInterestEvent(events.get(2), true));
		Assertions.assertEquals("06/03/2033	09/06/2033	09/06/2033	09/06/2033", formatInterestEvent(events.get(3), true));
		Assertions.assertEquals("03/03/2033	06/03/2033	06/03/2033	06/03/2033", formatInterestEvent(events.get(4), true));
		Assertions.assertEquals("12/03/2032	03/03/2033	03/03/2033	03/03/2033", formatInterestEvent(events.get(5), true));
		Assertions.assertEquals("09/03/2032	12/03/2032	12/03/2032	12/03/2032", formatInterestEvent(events.get(6), true));
		Assertions.assertEquals("06/03/2032	09/03/2032	09/03/2032	09/03/2032", formatInterestEvent(events.get(7), true));
		Assertions.assertEquals("03/03/2032	06/03/2032	06/03/2032	06/03/2032", formatInterestEvent(events.get(8), true));
		Assertions.assertEquals("12/03/2031	03/03/2032	03/03/2032	03/03/2032", formatInterestEvent(events.get(9), true));
		Assertions.assertEquals("09/03/2031	12/03/2031	12/03/2031	12/03/2031", formatInterestEvent(events.get(10), true));
		Assertions.assertEquals("06/03/2031	09/03/2031	09/03/2031	09/03/2031", formatInterestEvent(events.get(11), true));
		Assertions.assertEquals("03/03/2031	06/03/2031	06/03/2031	06/03/2031", formatInterestEvent(events.get(12), true));
		Assertions.assertEquals("12/03/2030	03/03/2031	03/03/2031	03/03/2031", formatInterestEvent(events.get(13), true));
		Assertions.assertEquals("09/03/2030	12/03/2030	12/03/2030	12/03/2030", formatInterestEvent(events.get(14), true));
		Assertions.assertEquals("06/03/2030	09/03/2030	09/03/2030	09/03/2030", formatInterestEvent(events.get(15), true));
		Assertions.assertEquals("03/04/2030	06/03/2030	06/03/2030	06/03/2030", formatInterestEvent(events.get(16), true));
		Assertions.assertEquals("12/03/2029	03/04/2030	03/04/2030	03/04/2030", formatInterestEvent(events.get(17), true));
		Assertions.assertEquals("09/04/2029	12/03/2029	12/03/2029	12/03/2029", formatInterestEvent(events.get(18), true));
		Assertions.assertEquals("06/04/2029	09/04/2029	09/04/2029	09/04/2029", formatInterestEvent(events.get(19), true));
		Assertions.assertEquals("03/05/2029	06/04/2029	06/04/2029	06/04/2029", formatInterestEvent(events.get(20), true));
		Assertions.assertEquals("12/04/2028	03/05/2029	03/05/2029	03/05/2029", formatInterestEvent(events.get(21), true));
		Assertions.assertEquals("09/05/2028	12/04/2028	12/04/2028	12/04/2028", formatInterestEvent(events.get(22), true));
		Assertions.assertEquals("06/05/2028	09/05/2028	09/05/2028	09/05/2028", formatInterestEvent(events.get(23), true));
		Assertions.assertEquals("03/03/2028	06/05/2028	06/05/2028	06/05/2028", formatInterestEvent(events.get(24), true));
		Assertions.assertEquals("12/03/2027	03/03/2028	03/03/2028	03/03/2028", formatInterestEvent(events.get(25), true));
		Assertions.assertEquals("09/03/2027	12/03/2027	12/03/2027	12/03/2027", formatInterestEvent(events.get(26), true));
		Assertions.assertEquals("06/03/2027	09/03/2027	09/03/2027	09/03/2027", formatInterestEvent(events.get(27), true));
		Assertions.assertEquals("03/03/2027	06/03/2027	06/03/2027	06/03/2027", formatInterestEvent(events.get(28), true));
		Assertions.assertEquals("12/03/2026	03/03/2027	03/03/2027	03/03/2027", formatInterestEvent(events.get(29), true));
		Assertions.assertEquals("09/03/2026	12/03/2026	12/03/2026	12/03/2026", formatInterestEvent(events.get(30), true));
		Assertions.assertEquals("06/03/2026	09/03/2026	09/03/2026	09/03/2026", formatInterestEvent(events.get(31), true));
		Assertions.assertEquals("03/03/2026	06/03/2026	06/03/2026	06/03/2026", formatInterestEvent(events.get(32), true));
		Assertions.assertEquals("12/03/2025	03/03/2026	03/03/2026	03/03/2026", formatInterestEvent(events.get(33), true));
		Assertions.assertEquals("09/03/2025	12/03/2025	12/03/2025	12/03/2025", formatInterestEvent(events.get(34), true));
		Assertions.assertEquals("06/03/2025	09/03/2025	09/03/2025	09/03/2025", formatInterestEvent(events.get(35), true));
		Assertions.assertEquals("03/03/2025	06/03/2025	06/03/2025	06/03/2025", formatInterestEvent(events.get(36), true));
		Assertions.assertEquals("12/03/2024	03/03/2025	03/03/2025	03/03/2025", formatInterestEvent(events.get(37), true));
		Assertions.assertEquals("09/03/2024	12/03/2024	12/03/2024	12/03/2024", formatInterestEvent(events.get(38), true));
		Assertions.assertEquals("06/03/2024	09/03/2024	09/03/2024	09/03/2024", formatInterestEvent(events.get(39), true));
		Assertions.assertEquals("03/04/2024	06/03/2024	06/03/2024	06/03/2024", formatInterestEvent(events.get(40), true));
		Assertions.assertEquals("12/04/2023	03/04/2024	03/04/2024	03/04/2024", formatInterestEvent(events.get(41), true));
		Assertions.assertEquals("09/05/2023	12/04/2023	12/04/2023	12/04/2023", formatInterestEvent(events.get(42), true));
		Assertions.assertEquals("06/05/2023	09/05/2023	09/05/2023	09/05/2023", formatInterestEvent(events.get(43), true));
		Assertions.assertEquals("03/03/2023	06/05/2023	06/05/2023	06/05/2023", formatInterestEvent(events.get(44), true));
		Assertions.assertEquals("12/05/2022	03/03/2023	03/03/2023	03/03/2023", formatInterestEvent(events.get(45), true));
		Assertions.assertEquals("09/06/2022	12/05/2022	12/05/2022	12/05/2022", formatInterestEvent(events.get(46), true));
		Assertions.assertEquals("06/06/2022	09/06/2022	09/06/2022	09/06/2022", formatInterestEvent(events.get(47), true));
		Assertions.assertEquals("03/03/2022	06/06/2022	06/06/2022	06/06/2022", formatInterestEvent(events.get(48), true));
		Assertions.assertEquals("12/03/2021	03/03/2022	03/03/2022	03/03/2022", formatInterestEvent(events.get(49), true));
		Assertions.assertEquals("09/03/2021	12/03/2021	12/03/2021	12/03/2021", formatInterestEvent(events.get(50), true));
		Assertions.assertEquals("06/03/2021	09/03/2021	09/03/2021	09/03/2021", formatInterestEvent(events.get(51), true));
		Assertions.assertEquals("03/03/2021	06/03/2021	06/03/2021	06/03/2021", formatInterestEvent(events.get(52), true));
		Assertions.assertEquals("12/03/2020	03/03/2021	03/03/2021	03/03/2021", formatInterestEvent(events.get(53), true));
		Assertions.assertEquals("09/03/2020	12/03/2020	12/03/2020	12/03/2020", formatInterestEvent(events.get(54), true));
		Assertions.assertEquals("06/03/2020	09/03/2020	09/03/2020	09/03/2020", formatInterestEvent(events.get(55), true));
		Assertions.assertEquals("03/03/2020	06/03/2020	06/03/2020	06/03/2020", formatInterestEvent(events.get(56), true));
		Assertions.assertEquals("12/03/2019	03/03/2020	03/03/2020	03/03/2020", formatInterestEvent(events.get(57), true));
		Assertions.assertEquals("09/03/2019	12/03/2019	12/03/2019	12/03/2019", formatInterestEvent(events.get(58), true));
		Assertions.assertEquals("06/03/2019	09/03/2019	09/03/2019	09/03/2019", formatInterestEvent(events.get(59), true));
		Assertions.assertEquals("03/04/2019	06/03/2019	06/03/2019	06/03/2019", formatInterestEvent(events.get(60), true));
		Assertions.assertEquals("12/03/2018	03/04/2019	03/04/2019	03/04/2019", formatInterestEvent(events.get(61), true));
		Assertions.assertEquals("09/04/2018	12/03/2018	12/03/2018	12/03/2018", formatInterestEvent(events.get(62), true));
		Assertions.assertEquals("06/04/2018	09/04/2018	09/04/2018	09/04/2018", formatInterestEvent(events.get(63), true));
		Assertions.assertEquals("03/05/2018	06/04/2018	06/04/2018	06/04/2018", formatInterestEvent(events.get(64), true));
		Assertions.assertEquals("12/04/2017	03/05/2018	03/05/2018	03/05/2018", formatInterestEvent(events.get(65), true));
		Assertions.assertEquals("09/05/2017	12/04/2017	12/04/2017	12/04/2017", formatInterestEvent(events.get(66), true));
		Assertions.assertEquals("06/05/2017	09/05/2017	09/05/2017	09/05/2017", formatInterestEvent(events.get(67), true));
		Assertions.assertEquals("03/03/2017	06/05/2017	06/05/2017	06/05/2017", formatInterestEvent(events.get(68), true));
		Assertions.assertEquals("12/05/2016	03/03/2017	03/03/2017	03/03/2017", formatInterestEvent(events.get(69), true));
		Assertions.assertEquals("09/06/2016	12/05/2016	12/05/2016	12/05/2016", formatInterestEvent(events.get(70), true));
		Assertions.assertEquals("06/03/2016	09/06/2016	09/06/2016	09/06/2016", formatInterestEvent(events.get(71), true));
		Assertions.assertEquals("03/03/2016	06/03/2016	06/03/2016	06/03/2016", formatInterestEvent(events.get(72), true));
		Assertions.assertEquals("12/03/2015	03/03/2016	03/03/2016	03/03/2016", formatInterestEvent(events.get(73), true));
		Assertions.assertEquals("09/03/2015	12/03/2015	12/03/2015	12/03/2015", formatInterestEvent(events.get(74), true));
		Assertions.assertEquals("06/03/2015	09/03/2015	09/03/2015	09/03/2015", formatInterestEvent(events.get(75), true));
		Assertions.assertEquals("03/03/2015	06/03/2015	06/03/2015	06/03/2015", formatInterestEvent(events.get(76), true));
		Assertions.assertEquals("12/03/2014	03/03/2015	03/03/2015	03/03/2015", formatInterestEvent(events.get(77), true));
		Assertions.assertEquals("09/03/2014	12/03/2014	12/03/2014	12/03/2014", formatInterestEvent(events.get(78), true));
		Assertions.assertEquals("06/03/2014	09/03/2014	09/03/2014	09/03/2014", formatInterestEvent(events.get(79), true));
	}


	@Test
	public void testFixLeg_GBP6L_20130926_20230926_2_7() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("GBP6L-20130926-20230926-2.7", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Fixed Leg Payment";
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Fixed Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(20, events.size());

		Assertions.assertEquals("03/27/2023	09/26/2023	09/26/2023	09/26/2023", formatInterestEvent(events.get(0), true));
		Assertions.assertEquals("09/26/2022	03/27/2023	03/27/2023	03/27/2023", formatInterestEvent(events.get(1), true));
		Assertions.assertEquals("03/28/2022	09/26/2022	09/26/2022	09/26/2022", formatInterestEvent(events.get(2), true));
		Assertions.assertEquals("09/27/2021	03/28/2022	03/28/2022	03/28/2022", formatInterestEvent(events.get(3), true));
		Assertions.assertEquals("03/26/2021	09/27/2021	09/27/2021	09/27/2021", formatInterestEvent(events.get(4), true));
		Assertions.assertEquals("09/28/2020	03/26/2021	03/26/2021	03/26/2021", formatInterestEvent(events.get(5), true));
		Assertions.assertEquals("03/26/2020	09/28/2020	09/28/2020	09/28/2020", formatInterestEvent(events.get(6), true));
		Assertions.assertEquals("09/26/2019	03/26/2020	03/26/2020	03/26/2020", formatInterestEvent(events.get(7), true));
		Assertions.assertEquals("03/26/2019	09/26/2019	09/26/2019	09/26/2019", formatInterestEvent(events.get(8), true));
		Assertions.assertEquals("09/26/2018	03/26/2019	03/26/2019	03/26/2019", formatInterestEvent(events.get(9), true));
		Assertions.assertEquals("03/26/2018	09/26/2018	09/26/2018	09/26/2018", formatInterestEvent(events.get(10), true));
		Assertions.assertEquals("09/26/2017	03/26/2018	03/26/2018	03/26/2018", formatInterestEvent(events.get(11), true));
		Assertions.assertEquals("03/27/2017	09/26/2017	09/26/2017	09/26/2017", formatInterestEvent(events.get(12), true));
		Assertions.assertEquals("09/26/2016	03/27/2017	03/27/2017	03/27/2017", formatInterestEvent(events.get(13), true));
		Assertions.assertEquals("03/29/2016	09/26/2016	09/26/2016	09/26/2016", formatInterestEvent(events.get(14), true));
		Assertions.assertEquals("09/28/2015	03/29/2016	03/29/2016	03/29/2016", formatInterestEvent(events.get(15), true));
		Assertions.assertEquals("03/26/2015	09/28/2015	09/28/2015	09/28/2015", formatInterestEvent(events.get(16), true));
		Assertions.assertEquals("09/26/2014	03/26/2015	03/26/2015	03/26/2015", formatInterestEvent(events.get(17), true));
		Assertions.assertEquals("03/26/2014	09/26/2014	09/26/2014	09/26/2014", formatInterestEvent(events.get(18), true));
		Assertions.assertEquals("09/26/2013	03/26/2014	03/26/2014	03/26/2014", formatInterestEvent(events.get(19), true));
	}


	@Test
	public void testFloatingLeg_GBP6L_20130926_20230926_2_7() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("GBP6L-20130926-20230926-2.7", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Floating Leg Payment";
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Floating Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(20, events.size());

		Assertions.assertEquals("03/27/2023	09/26/2023	09/26/2023	09/26/2023", formatInterestEvent(events.get(0), true));
		Assertions.assertEquals("09/26/2022	03/27/2023	03/27/2023	03/27/2023", formatInterestEvent(events.get(1), true));
		Assertions.assertEquals("03/28/2022	09/26/2022	09/26/2022	09/26/2022", formatInterestEvent(events.get(2), true));
		Assertions.assertEquals("09/27/2021	03/28/2022	03/28/2022	03/28/2022", formatInterestEvent(events.get(3), true));
		Assertions.assertEquals("03/26/2021	09/27/2021	09/27/2021	09/27/2021", formatInterestEvent(events.get(4), true));
		Assertions.assertEquals("09/28/2020	03/26/2021	03/26/2021	03/26/2021", formatInterestEvent(events.get(5), true));
		Assertions.assertEquals("03/26/2020	09/28/2020	09/28/2020	09/28/2020", formatInterestEvent(events.get(6), true));
		Assertions.assertEquals("09/26/2019	03/26/2020	03/26/2020	03/26/2020", formatInterestEvent(events.get(7), true));
		Assertions.assertEquals("03/26/2019	09/26/2019	09/26/2019	09/26/2019", formatInterestEvent(events.get(8), true));
		Assertions.assertEquals("09/26/2018	03/26/2019	03/26/2019	03/26/2019", formatInterestEvent(events.get(9), true));
		Assertions.assertEquals("03/26/2018	09/26/2018	09/26/2018	09/26/2018", formatInterestEvent(events.get(10), true));
		Assertions.assertEquals("09/26/2017	03/26/2018	03/26/2018	03/26/2018", formatInterestEvent(events.get(11), true));
		Assertions.assertEquals("03/27/2017	09/26/2017	09/26/2017	09/26/2017", formatInterestEvent(events.get(12), true));
		Assertions.assertEquals("09/26/2016	03/27/2017	03/27/2017	03/27/2017", formatInterestEvent(events.get(13), true));
		Assertions.assertEquals("03/29/2016	09/26/2016	09/26/2016	09/26/2016", formatInterestEvent(events.get(14), true));
		Assertions.assertEquals("09/28/2015	03/29/2016	03/29/2016	03/29/2016", formatInterestEvent(events.get(15), true));
		Assertions.assertEquals("03/26/2015	09/28/2015	09/28/2015	09/28/2015", formatInterestEvent(events.get(16), true));
		Assertions.assertEquals("09/26/2014	03/26/2015	03/26/2015	03/26/2015", formatInterestEvent(events.get(17), true));
		Assertions.assertEquals("03/26/2014	09/26/2014	09/26/2014	09/26/2014", formatInterestEvent(events.get(18), true));
		Assertions.assertEquals("09/26/2013	03/26/2014	03/26/2014	03/26/2014", formatInterestEvent(events.get(19), true));
	}


	@Test
	public void testFixLeg_EUR6E_20130930_20230930_2_123() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("EUR6E-20130930-20230930-2.123", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Fixed Leg Payment";
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Fixed Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(10, events.size());

		Assertions.assertEquals("09/30/2022	09/29/2023	09/29/2023	09/29/2023", formatInterestEvent(events.get(0), true));
		Assertions.assertEquals("09/30/2021	09/30/2022	09/30/2022	09/30/2022", formatInterestEvent(events.get(1), true));
		Assertions.assertEquals("09/30/2020	09/30/2021	09/30/2021	09/30/2021", formatInterestEvent(events.get(2), true));
		Assertions.assertEquals("09/30/2019	09/30/2020	09/30/2020	09/30/2020", formatInterestEvent(events.get(3), true));
		Assertions.assertEquals("09/28/2018	09/30/2019	09/30/2019	09/30/2019", formatInterestEvent(events.get(4), true));
		Assertions.assertEquals("09/29/2017	09/28/2018	09/28/2018	09/28/2018", formatInterestEvent(events.get(5), true));
		Assertions.assertEquals("09/30/2016	09/29/2017	09/29/2017	09/29/2017", formatInterestEvent(events.get(6), true));
		Assertions.assertEquals("09/30/2015	09/30/2016	09/30/2016	09/30/2016", formatInterestEvent(events.get(7), true));
		Assertions.assertEquals("09/30/2014	09/30/2015	09/30/2015	09/30/2015", formatInterestEvent(events.get(8), true));
		Assertions.assertEquals("09/30/2013	09/30/2014	09/30/2014	09/30/2014", formatInterestEvent(events.get(9), true));
	}


	@Test
	public void testFloatingLeg_EUR6E_20130930_20230930_2_123() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("EUR6E-20130930-20230930-2.123", "SWAP_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[1];
		eventTypes[0] = "Floating Leg Payment";
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Floating Leg Payment");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(20, events.size());

		Assertions.assertEquals("03/31/2023	09/29/2023	09/29/2023	09/29/2023", formatInterestEvent(events.get(0), true));
		Assertions.assertEquals("09/30/2022	03/31/2023	03/31/2023	03/31/2023", formatInterestEvent(events.get(1), true));
		Assertions.assertEquals("03/31/2022	09/30/2022	09/30/2022	09/30/2022", formatInterestEvent(events.get(2), true));
		Assertions.assertEquals("09/30/2021	03/31/2022	03/31/2022	03/31/2022", formatInterestEvent(events.get(3), true));
		Assertions.assertEquals("03/31/2021	09/30/2021	09/30/2021	09/30/2021", formatInterestEvent(events.get(4), true));
		Assertions.assertEquals("09/30/2020	03/31/2021	03/31/2021	03/31/2021", formatInterestEvent(events.get(5), true));
		Assertions.assertEquals("03/31/2020	09/30/2020	09/30/2020	09/30/2020", formatInterestEvent(events.get(6), true));
		Assertions.assertEquals("09/30/2019	03/31/2020	03/31/2020	03/31/2020", formatInterestEvent(events.get(7), true));
		Assertions.assertEquals("03/29/2019	09/30/2019	09/30/2019	09/30/2019", formatInterestEvent(events.get(8), true));
		Assertions.assertEquals("09/28/2018	03/29/2019	03/29/2019	03/29/2019", formatInterestEvent(events.get(9), true));
		Assertions.assertEquals("03/29/2018\t09/28/2018	09/28/2018	09/28/2018", formatInterestEvent(events.get(10), true));
		Assertions.assertEquals("09/29/2017	03/29/2018	03/29/2018	03/29/2018", formatInterestEvent(events.get(11), true));
		Assertions.assertEquals("03/31/2017	09/29/2017	09/29/2017	09/29/2017", formatInterestEvent(events.get(12), true));
		Assertions.assertEquals("09/30/2016	03/31/2017	03/31/2017	03/31/2017", formatInterestEvent(events.get(13), true));
		Assertions.assertEquals("03/31/2016	09/30/2016	09/30/2016	09/30/2016", formatInterestEvent(events.get(14), true));
		Assertions.assertEquals("09/30/2015	03/31/2016	03/31/2016	03/31/2016", formatInterestEvent(events.get(15), true));
		Assertions.assertEquals("03/31/2015	09/30/2015	09/30/2015	09/30/2015", formatInterestEvent(events.get(16), true));
		Assertions.assertEquals("09/30/2014	03/31/2015	03/31/2015	03/31/2015", formatInterestEvent(events.get(17), true));
		Assertions.assertEquals("03/31/2014	09/30/2014	09/30/2014	09/30/2014", formatInterestEvent(events.get(18), true));
		Assertions.assertEquals("09/30/2013	03/31/2014	03/31/2014	03/31/2014", formatInterestEvent(events.get(19), true));
	}


	@Test
	public void testTRS_201907710() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("201907710", "SWAP_201907710_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[]{"Equity Leg Payment", "Interest Leg Payment"};
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Equity Leg Payment");
		searchForm.setOrderBy("eventDate:ASC");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("04/29/2016	06/01/2016	05/31/2016	06/03/2016", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("05/31/2016	07/01/2016	06/30/2016	07/06/2016", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("06/30/2016	07/30/2016	07/29/2016	08/03/2016", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("07/29/2016	09/01/2016	08/31/2016	09/06/2016", formatEquityEvent(events.get(3)));
		Assertions.assertEquals("08/31/2016	10/01/2016	09/30/2016	10/05/2016", formatEquityEvent(events.get(4)));
		Assertions.assertEquals("09/30/2016	11/01/2016	10/31/2016	11/03/2016", formatEquityEvent(events.get(5)));
		Assertions.assertEquals("10/31/2016	12/01/2016	11/30/2016	12/05/2016", formatEquityEvent(events.get(6)));
		Assertions.assertEquals("11/30/2016	12/31/2016	12/30/2016	01/05/2017", formatEquityEvent(events.get(7)));
		Assertions.assertEquals("12/30/2016	02/01/2017	01/31/2017	02/03/2017", formatEquityEvent(events.get(8)));
		Assertions.assertEquals("01/31/2017	03/01/2017	02/28/2017	03/03/2017", formatEquityEvent(events.get(9)));
		Assertions.assertEquals("02/28/2017	04/01/2017	03/31/2017	04/05/2017", formatEquityEvent(events.get(10)));
		Assertions.assertEquals("03/31/2017	04/29/2017	04/28/2017	05/03/2017", formatEquityEvent(events.get(11)));

		searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Interest Leg Payment");
		searchForm.setOrderBy("eventDate:ASC");
		events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("05/04/2016	06/03/2016	06/01/2016	06/03/2016	05/05/2016", formatInterestEvent(events.get(0), false));
		Assertions.assertEquals("06/03/2016	07/06/2016	07/01/2016	07/06/2016	06/06/2016", formatInterestEvent(events.get(1), false));
		Assertions.assertEquals("07/06/2016	08/03/2016	08/01/2016	08/03/2016	07/07/2016", formatInterestEvent(events.get(2), false));
		Assertions.assertEquals("08/03/2016	09/06/2016	09/01/2016	09/06/2016	08/04/2016", formatInterestEvent(events.get(3), false));
		Assertions.assertEquals("09/06/2016	10/05/2016	10/01/2016	10/05/2016	09/07/2016", formatInterestEvent(events.get(4), false));
		Assertions.assertEquals("10/05/2016	11/03/2016	11/01/2016	11/03/2016	10/06/2016", formatInterestEvent(events.get(5), false));
		Assertions.assertEquals("11/03/2016	12/05/2016	12/01/2016	12/05/2016	11/04/2016", formatInterestEvent(events.get(6), false));
		Assertions.assertEquals("12/05/2016	01/05/2017	01/01/2017	01/05/2017	12/06/2016", formatInterestEvent(events.get(7), false));
		Assertions.assertEquals("01/05/2017	02/03/2017	02/01/2017	02/03/2017	01/06/2017", formatInterestEvent(events.get(8), false));
		Assertions.assertEquals("02/03/2017	03/03/2017	03/01/2017	03/03/2017	02/06/2017", formatInterestEvent(events.get(9), false));
		Assertions.assertEquals("03/03/2017	04/05/2017	04/01/2017	04/05/2017	03/06/2017", formatInterestEvent(events.get(10), false));
		Assertions.assertEquals("04/05/2017	05/03/2017	05/01/2017	05/03/2017	04/06/2017", formatInterestEvent(events.get(11), false));
	}


	@Test
	public void testTRS_TBD_GS_TAIWAN() {
		InvestmentSecurity security = this.investmentInstrumentUtils.getCopyOfSecurity("SDB3221174767", "SWAP_SDB3221174767_" + DateUtils.fromDate(new Date(), SYMBOL_DATE_FORMAT));

		String[] eventTypes = new String[]{"Equity Leg Payment", "Interest Leg Payment"};
		this.investmentSecurityEventRetrieverService.loadInvestmentSecurityEventHistory(security.getId(), eventTypes, true, true);

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Equity Leg Payment");
		searchForm.setOrderBy("eventDate:ASC");
		List<InvestmentSecurityEvent> events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("04/29/2016	06/01/2016	05/31/2016	06/03/2016", formatEquityEvent(events.get(0)));
		Assertions.assertEquals("05/31/2016	07/01/2016	06/30/2016	07/06/2016", formatEquityEvent(events.get(1)));
		Assertions.assertEquals("06/30/2016	07/30/2016	07/29/2016	08/03/2016", formatEquityEvent(events.get(2)));
		Assertions.assertEquals("07/29/2016	09/01/2016	08/31/2016	09/06/2016", formatEquityEvent(events.get(3)));
		Assertions.assertEquals("08/31/2016	10/01/2016	09/30/2016	10/05/2016", formatEquityEvent(events.get(4)));
		Assertions.assertEquals("09/30/2016	11/01/2016	10/31/2016	11/03/2016", formatEquityEvent(events.get(5)));
		Assertions.assertEquals("10/31/2016	12/01/2016	11/30/2016	12/05/2016", formatEquityEvent(events.get(6)));
		Assertions.assertEquals("11/30/2016	12/31/2016	12/30/2016	01/05/2017", formatEquityEvent(events.get(7)));
		Assertions.assertEquals("12/30/2016	02/01/2017	01/31/2017	02/03/2017", formatEquityEvent(events.get(8)));
		Assertions.assertEquals("01/31/2017	02/28/2017	02/27/2017	03/02/2017", formatEquityEvent(events.get(9)));
		Assertions.assertEquals("02/27/2017	04/01/2017	03/31/2017	04/05/2017", formatEquityEvent(events.get(10)));
		Assertions.assertEquals("03/31/2017	04/29/2017	04/28/2017	05/03/2017", formatEquityEvent(events.get(11)));

		searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeName("Interest Leg Payment");
		searchForm.setOrderBy("eventDate:ASC");
		events = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertEquals(12, events.size());

		Assertions.assertEquals("05/04/2016	06/03/2016	06/01/2016	06/03/2016	05/04/2016", formatInterestEvent(events.get(0), false));
		Assertions.assertEquals("06/03/2016	07/06/2016	07/01/2016	07/06/2016	06/03/2016", formatInterestEvent(events.get(1), false));
		Assertions.assertEquals("07/06/2016	08/03/2016	08/01/2016	08/03/2016	07/06/2016", formatInterestEvent(events.get(2), false));
		Assertions.assertEquals("08/03/2016	09/06/2016	09/01/2016	09/06/2016	08/03/2016", formatInterestEvent(events.get(3), false));
		Assertions.assertEquals("09/06/2016	10/05/2016	10/01/2016	10/05/2016	09/06/2016", formatInterestEvent(events.get(4), false));
		Assertions.assertEquals("10/05/2016	11/03/2016	11/01/2016	11/03/2016	10/05/2016", formatInterestEvent(events.get(5), false));
		Assertions.assertEquals("11/03/2016	12/05/2016	12/01/2016	12/05/2016	11/03/2016", formatInterestEvent(events.get(6), false));
		Assertions.assertEquals("12/05/2016	01/05/2017	01/01/2017	01/05/2017	12/05/2016", formatInterestEvent(events.get(7), false));
		Assertions.assertEquals("01/05/2017	02/03/2017	02/01/2017	02/03/2017	01/05/2017", formatInterestEvent(events.get(8), false));
		Assertions.assertEquals("02/03/2017	03/02/2017	03/01/2017	03/02/2017	02/03/2017", formatInterestEvent(events.get(9), false));
		Assertions.assertEquals("03/02/2017	04/05/2017	04/01/2017	04/05/2017	03/02/2017", formatInterestEvent(events.get(10), false));
		Assertions.assertEquals("04/05/2017	05/03/2017	05/01/2017	05/03/2017	04/05/2017", formatInterestEvent(events.get(11), false));
	}


	private String formatEquityEvent(InvestmentSecurityEvent e) {
		return DateUtils.fromDate(e.getDeclareDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getExDate(), DateUtils.DATE_FORMAT_INPUT) + "\t"
				+ DateUtils.fromDate(e.getRecordDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getEventDate(), DateUtils.DATE_FORMAT_INPUT);
	}


	/**
	 * Start Date + End Date + Ex Date + Payment Date
	 */
	private String formatInterestEvent(InvestmentSecurityEvent e, boolean excludeAdditionalDate) {
		return DateUtils.fromDate(e.getAccrualStartDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getAccrualEndDate(), DateUtils.DATE_FORMAT_INPUT) + "\t"
				+ DateUtils.fromDate(e.getExDate(), DateUtils.DATE_FORMAT_INPUT) + "\t" + DateUtils.fromDate(e.getPaymentDate(), DateUtils.DATE_FORMAT_INPUT)
				+ (excludeAdditionalDate ? "" : "\t" + DateUtils.fromDate(e.getAdditionalDate(), DateUtils.DATE_FORMAT_INPUT));
	}
}
