package com.clifton.ims.tests.integration.export.aws.s3;

import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.util.RandomUtils;



/**
 * @author rtschumper on 9/1/2020.
 */
public class AWSS3ExportDestinationBuilder {

	private ExportDefinition exportDefinition;


	private SystemBeanBuilder beanBuilder;


	private ExportDefinitionService exportDefinitionService;


	public AWSS3ExportDestinationBuilder(SystemBeanService systemBeanService, ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
		this.beanBuilder = SystemBeanBuilder.newBuilder(null, "AWS S3", systemBeanService);
	}


	public AWSS3ExportDestinationBuilder exportDefinition(ExportDefinition exportDefinition) {
		this.exportDefinition = exportDefinition;
		return this;
	}


	public AWSS3ExportDestinationBuilder bucket(String bucket) {
		this.beanBuilder.addProperty("Destination AWS S3 Bucket", bucket);
		return this;
	}


	public AWSS3ExportDestinationBuilder region(String region) {
		this.beanBuilder.addProperty("Destination AWS S3 Region", region);
		return this;
	}

	public AWSS3ExportDestinationBuilder endpoint(String endpoint) {
		this.beanBuilder.addProperty("AWS S3 Endpoint", endpoint);
		return this;
	}


	public ExportDefinitionDestination build() {
		SystemBean bean = this.beanBuilder.build();
		bean.setName("Test destination - " + RandomUtils.randomNumber());
		ExportDefinitionDestination destination = new ExportDefinitionDestination();
		destination.setDestinationSystemBean(bean);
		destination.setDefinition(this.exportDefinition);
		this.exportDefinitionService.saveExportDefinitionDestination(destination);
		return destination;
	}
}
