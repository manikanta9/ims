package com.clifton.ims.tests.performance.composite;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * The <code>PerformanceCompositePerformanceBenchmarkRebuildTests</code> tests the re-process benchmarks only feature on
 * account and composite performance.  This allows these values to be updated without requiring the performance be moved back to Draft.
 *
 * @author manderson
 */
public class PerformanceCompositePerformanceBenchmarkRebuildTests extends BasePerformanceCompositePerformanceTests {


	@Resource
	private MarketDataFieldService marketDataFieldService;

	@Resource
	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPerformanceBenchmarkReProcessing() {
		String testDate = "06/30/2017";

		// TIPS Portfolio Management Composite II - Ended on 6/30/2017
		PerformanceCompositePerformance compositePerformance = getCompositePerformanceByCompositeAndDate("TIPS Portfolio Management Composite II", testDate);
		// Assignment only for 124401
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = getPerformanceCompositeInvestmentAccountPerformance(null, "124401", testDate, false);

		boolean originalPriceRestored = true;
		MarketDataValue benchmarkPrice = getMarketDataValueLastTradePriceForSecurity(compositePerformance.getBenchmarkSecurity().getId(), testDate);
		BigDecimal originalBenchmarkPrice = benchmarkPrice.getMeasureValue();
		try {
			// 5% change
			benchmarkPrice.setMeasureValue(BigDecimal.valueOf(270.5));
			this.marketDataFieldService.saveMarketDataValue(benchmarkPrice);
			originalPriceRestored = false;

			PerformanceCompositeAccountRebuildCommand accountRebuildCommand = new PerformanceCompositeAccountRebuildCommand();
			accountRebuildCommand.setPerformanceCompositeId(compositePerformance.getPerformanceComposite().getId());
			accountRebuildCommand.setToAccountingPeriod(compositePerformance.getAccountingPeriod());
			accountRebuildCommand.setFromAccountingPeriod(compositePerformance.getAccountingPeriod());
			accountRebuildCommand.setMetricProcessingType(PerformanceCompositeMetricProcessingTypes.BENCHMARKS_ONLY);
			accountRebuildCommand.setProcessCompositeBenchmarks(true);
			accountRebuildCommand.setSynchronous(true);

			// Process Both At Once
			this.performanceCompositePerformanceRebuildService.rebuildPerformanceCompositeAccountPerformance(accountRebuildCommand);


			// Updated Performance Records
			PerformanceCompositePerformance updatedCompositePerformance = getCompositePerformanceByCompositeAndDate("TIPS Portfolio Management Composite II", testDate);
			// Assignment only for 124401
			PerformanceCompositeInvestmentAccountPerformance updatedAccountPerformance = getPerformanceCompositeInvestmentAccountPerformance(null, "124401", testDate, false);

			// All fields except benchmarks should be equal
			validatePerformanceCompositeMetricValuesEqual(compositePerformance, updatedCompositePerformance, null, false);
			validatePerformanceCompositeMetricValuesEqual(accountPerformance, updatedAccountPerformance, null, false);

			Assertions.assertEquals(BigDecimal.valueOf(2.3469), MathUtils.round(updatedCompositePerformance.getPeriodBenchmarkReturn(), 4));
			Assertions.assertEquals(BigDecimal.valueOf(2.3469), MathUtils.round(updatedAccountPerformance.getPeriodBenchmarkReturn(), 4));

			// Confirm one Linked Return
			Assertions.assertEquals(BigDecimal.valueOf(2.7355), MathUtils.round(updatedCompositePerformance.getQuarterToDateBenchmarkReturn(), 4));
			Assertions.assertEquals(BigDecimal.valueOf(2.7355), MathUtils.round(updatedAccountPerformance.getQuarterToDateBenchmarkReturn(), 4));

			// Set price back
			benchmarkPrice.setMeasureValue(originalBenchmarkPrice);
			this.marketDataFieldService.saveMarketDataValue(benchmarkPrice);
			originalPriceRestored = true;

			// Re-Process Again
			accountRebuildCommand = new PerformanceCompositeAccountRebuildCommand();
			accountRebuildCommand.setPerformanceCompositeId(compositePerformance.getPerformanceComposite().getId());
			accountRebuildCommand.setToAccountingPeriod(compositePerformance.getAccountingPeriod());
			accountRebuildCommand.setFromAccountingPeriod(compositePerformance.getAccountingPeriod());
			accountRebuildCommand.setMetricProcessingType(PerformanceCompositeMetricProcessingTypes.BENCHMARKS_ONLY);
			accountRebuildCommand.setProcessCompositeBenchmarks(true);
			accountRebuildCommand.setSynchronous(true);

			// Process Both At Once
			this.performanceCompositePerformanceRebuildService.rebuildPerformanceCompositeAccountPerformance(accountRebuildCommand);

			// Updated Performance Records
			updatedCompositePerformance = getCompositePerformanceByCompositeAndDate("TIPS Portfolio Management Composite II", testDate);
			// Assignment only for 124401
			updatedAccountPerformance = getPerformanceCompositeInvestmentAccountPerformance(null, "124401", testDate, false);

			// Now everything else should match what it was originally
			validatePerformanceCompositeMetricValuesEqual(compositePerformance, updatedCompositePerformance, null, null);
			validatePerformanceCompositeMetricValuesEqual(accountPerformance, updatedAccountPerformance, null, null);
		}
		finally {
			if (!originalPriceRestored) {
				benchmarkPrice.setMeasureValue(originalBenchmarkPrice);
				this.marketDataFieldService.saveMarketDataValue(benchmarkPrice);
			}
		}
	}


	private MarketDataValue getMarketDataValueLastTradePriceForSecurity(int securityId, String priceDate) {
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setMeasureDate(DateUtils.toDate(priceDate));
		searchForm.setInvestmentSecurityId(securityId);
		searchForm.setDataFieldName(MarketDataField.FIELD_LAST_TRADE_PRICE);
		return CollectionUtils.getFirstElement(this.marketDataFieldService.getMarketDataValueList(searchForm));
	}
}
