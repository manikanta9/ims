package com.clifton.ims.tests.business;


import com.clifton.business.company.BusinessCompany;


/**
 * The <code>BusinessCompanies</code> is an enum that lists various {@link BusinessCompany} instances that are commonly used in integration tests.
 *
 * @author jgommels
 */
public enum BusinessCompanies {
	//
	CLIFTON("Our Companies", "Parametric Minneapolis"),

	//
	BOA("Broker", "Bank of America Merrill Lynch"),

	//
	BOA_CUSTODIAN("Custodian", "Bank of America, N.A."),

	//
	CITIGROUP("Broker", "Citigroup Global Markets Inc."),

	//
	CITIBANK_NA("Broker", "Citibank N.A."),

	//
	DEUTSCHE_BANK_AG("Broker", "Deutsche Bank AG"),

	//
	DEUTSCHE_BANK_SECURITIES("Broker", "Deutsche Bank Securities Inc."),

	//
	GOLDMAN_SACHS_AND_CO("Broker", "Goldman Sachs & Co."),

	//
	GOLDMAN_SACHS_INTERNATIONAL("Broker", "Goldman Sachs International"),

	//
	GOLDMAN_SACHS_EXECUTION_AND_CLEARING("Broker", "Goldman Sachs Execution & Clearing"),

	//
	JP_MORGAN_CHASE("Custodian", "JP Morgan Chase"),

	//
	JP_MORGAN_CHASE_BANK_NA("Broker", "JPMorgan Chase Bank NA"),

	//
	MORGAN_STANLEY_AND_CO_INC("Broker", "Morgan Stanley & Co. Inc."),

	//
	MORGAN_STANLEY_CAPITAL_SERVICES_INC("Broker", "Morgan Stanley Capital Services, Inc."),

	//
	MORGAN_STANLEY_AND_CO_INTERNATIONAL_PLC("Broker", "Morgan Stanley & Co. International PLC"),

	//
	NEWEDGE_USA_LLC("Broker", "Newedge USA, LLC"),

	//
	NORTHERN_TRUST("Custodian", "Northern Trust Co. IL"),

	//
	STATE_STREET_BANK("Custodian", "State Street Bank"),

	//
	MELLON_BANK_NA("Custodian", "Mellon Bank, N.A."),

	//
	BARCLAYS_BANK_PLC("Broker", "Barclays Bank PLC"),

	//
	BARCLAYS_CAPITOL_INC("Broker", "Barclays Capital Inc."),

	//
	CREDIT_SUISSE_SECURITIES("Broker", "Credit Suisse Securities (USA) LLC"),

	//
	UBS("Broker", "UBS AG"),

	//
	BANK_ON_NEW_YORK_MELLON("Custodian", "Bank of New York Mellon"),

	//
	BANK_OF_NEW_YORK_MELLON_INTERMEDIARY("Custodian", "Bank of New York Mellon Intermediary"),

	//
	HSBC_BANK_USA_NATIONAL_ASSOCIATION("Broker", "HSBC Bank USA NA/New York NY"),

	//
	BNP_PARIBAS("Broker", "BNP Paribas Securities Corp."),

	//
	WEX("Broker", "Wolverine Execution Services, LLC");

	private final String companyType;
	private final String companyName;


	BusinessCompanies(String companyType, String companyName) {
		this.companyType = companyType;
		this.companyName = companyName;
	}


	public String getCompanyType() {
		return this.companyType;
	}


	public String getCompanyName() {
		return this.companyName;
	}
}
