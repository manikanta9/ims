package com.clifton.ims.tests.core.dataaccess.datasource.stats;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.datasource.stats.DatabasePoolService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author davidi
 */
public class DatabasePoolServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	DatabasePoolService databasePoolService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetDatabasePoolNameList() {
		List<NamedEntityWithoutLabel<String>> dataSourceNames = this.databasePoolService.getDatabasePoolNameList();
		Assertions.assertNotNull(dataSourceNames);
		Assertions.assertTrue(!dataSourceNames.isEmpty());

		Set<String> extractedNames = CollectionUtils.getStream(dataSourceNames).map(NamedEntityWithoutLabel::getName).collect(Collectors.toSet());
		Set<String> expectedSet = CollectionUtils.createHashSet("dataSource", "dwDataSource", "swiftDataSource", "archiveDataSource");

		Assertions.assertTrue(extractedNames.containsAll(expectedSet));
	}
}
