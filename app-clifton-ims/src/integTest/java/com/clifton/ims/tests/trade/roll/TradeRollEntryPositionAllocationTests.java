package com.clifton.ims.tests.trade.roll;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass.PerformanceOverlayTargetSources;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationSearchForm;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InvestmentAssetClassSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;
import com.clifton.trade.assetclass.process.TradeAssetClassPositionAllocationProcessService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroup.TradeGroupActions;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.search.TradeGroupSearchForm;
import com.clifton.trade.roll.TradeRoll;
import com.clifton.trade.roll.TradeRollPosition;
import com.clifton.trade.roll.TradeRollService;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>TradeRollEntryPositionAllocationTests</code> tests creating Roll Trades/Tail Trades
 * and the {@link TradeAssetClassPositionAllocation} entries for the roll. It also processes those trade asset class allocations
 * for {@link InvestmentAccountAssetClassPositionAllocation} entries for following day explicit split updates
 *
 * @author manderson
 */
public class TradeRollEntryPositionAllocationTests extends BaseImsIntegrationTest {

	private static final Map<String, InvestmentAssetClass> assetClassNameToAssetClassMap = new HashMap<>();
	private static InvestmentSecurity ES_FROM_FUTURE_SECURITY;
	private static InvestmentSecurity ES_TO_FUTURE_SECURITY;
	private static InvestmentSecurity PT_FROM_FUTURE_SECURITY;
	private static InvestmentSecurity PT_TO_FUTURE_SECURITY;

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	@Resource
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;

	@Resource
	private InvestmentReplicationService investmentReplicationService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private RuleViolationUtils ruleViolationUtils;

	@Resource
	private TradeAssetClassService tradeAssetClassService;

	@Resource
	private TradeAssetClassPositionAllocationProcessService tradeAssetClassPositionAllocationProcessService;

	@Resource
	private TradeGroupService tradeGroupService;

	@Resource
	private TradeRollService tradeRollService;

	///////////////////////////////////////////////////////////

	private static final String ASSET_CLASS_1 = "Domestic Equity";
	private static final String ASSET_CLASS_2 = "International Equity";
	private static final String ASSET_CLASS_3 = "Fixed Income Core";
	private static final String ASSET_CLASS_4 = "Fixed Income - Long Duration";
	private static final String ASSET_CLASS_CASH = "Cash";

	private static final String REPLICATION_1 = "One Contract - DE S&P 500 E-Mini";
	private static final String REPLICATION_2 = "IE EAFE - MFS Only";
	private static final String REPLICATION_3 = "FIX BCA PIOS";
	private static final String REPLICATION_4 = "FIX BCLGC";


	////////////////////////////////////////////////////////////
	//  Tests for rolling - nothing explicit


	@BeforeEach
	public void initializeSecurities() {
		if (ES_FROM_FUTURE_SECURITY == null) {
			ES_FROM_FUTURE_SECURITY = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESU14");
		}
		if (ES_TO_FUTURE_SECURITY == null) {
			ES_TO_FUTURE_SECURITY = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ14");
		}
		if (PT_FROM_FUTURE_SECURITY == null) {
			PT_FROM_FUTURE_SECURITY = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PTH14");
		}
		if (PT_TO_FUTURE_SECURITY == null) {
			PT_TO_FUTURE_SECURITY = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("PTM14");
		}
	}


	@Test
	public void testRollAccount_NotExplicit_Full_Roll() {
		// No explicit contract allocations
		AccountInfo accountInfo = setupAccount();

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 100.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 100.0, 0);
		validateNoTradeAssetClassPositionAllocationsForRoll(roll);

		// Setup Initial Trades - Short Position
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 60.0, false);

		// Roll the Security
		roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 60.0, 0);
		validateNoTradeAssetClassPositionAllocationsForRoll(roll);
	}


	@Test
	public void testRollAccount_NotExplicit_Full_Roll_With_Tail() {
		// No explicit contract allocations
		AccountInfo accountInfo = setupAccount();

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 100.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 80.0, 20.0);

		validateNoTradeAssetClassPositionAllocationsForRoll(roll);

		// Setup Initial Trades - Short Position
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 60.0, false);

		// Roll the Security
		roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 55.0, 5.0);
		validateNoTradeAssetClassPositionAllocationsForRoll(roll);
	}


	////////////////////////////////////////////////////////////
	//  Tests mocked up from Trading Examples


	@Test
	public void testRollAccount_Explicit_PartialRoll() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "06/13/2014", null, 100.00);
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_2, null, PT_FROM_FUTURE_SECURITY, "12/13/2013", null, 10.00);

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 150.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 10.0, 0);

		String expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 10 of ESU14 on 09/12/2014_-7]" + //
						"[ESZ14_Domestic Equity_BUY 10 of ESZ14 on 09/12/2014_7]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Setup Initial Trades - Short Position
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 60.0, false);

		// Roll the Security
		roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 6.0, 0);

		expectedAllocations = //
				"[PTH14_International Equity_BUY 6 of PTH14 on 03/17/2014_-1]" + //
						"[PTM14_International Equity_SELL 6 of PTM14 on 03/17/2014_1]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);
	}


	@Test
	public void testRollAccount_Explicit_PartialRoll_Multiple_HoldLong() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "06/13/2014", null, 100.00);

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 150.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 10.0, 0, true, true);
		String expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 10 of ESU14 on 09/12/2014_-7]" + //
						"[ESZ14_Domestic Equity_BUY 10 of ESZ14 on 09/12/2014_7]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[ESU14_Domestic Equity_(09/12/2014 - )_93]" + //
						"[ESZ14_Domestic Equity_(09/12/2014 - )_7]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/12/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);

		roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/15/2014", true, 15.0, 0, true, true);
		expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 15 of ESU14 on 09/15/2014_-10]" + //
						"[ESZ14_Domestic Equity_BUY 15 of ESZ14 on 09/15/2014_10]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		expectedFinalResult = //
				"[ESU14_Domestic Equity_(09/15/2014 - )_83]" + //
						"[ESZ14_Domestic Equity_(09/15/2014 - )_17]";
		finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/15/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_FullRollWithTail_HoldLong() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "06/13/2014", null, 100.00);

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 150.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 145.0, 5.0, true, true);
		String expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 145 of ESU14 on 09/12/2014_-97]" + //
						"[ESU14_Domestic Equity_SELL 5 of ESU14 on 09/12/2014_-3]" + //
						"[ESZ14_Domestic Equity_BUY 145 of ESZ14 on 09/12/2014_97]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[ESZ14_Domestic Equity_(09/12/2014 - )_97]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/12/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_PartialRollWithTail_HoldLong() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "06/13/2014", null, 100.00);

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 150.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 10.0, 5.0, true, true);
		String expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 10 of ESU14 on 09/12/2014_-7]" + //
						"[ESU14_Domestic Equity_SELL 5 of ESU14 on 09/12/2014_-3]" + //
						"[ESZ14_Domestic Equity_BUY 10 of ESZ14 on 09/12/2014_7]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[ESU14_Domestic Equity_(09/12/2014 - )_90]" + //
						"[ESZ14_Domestic Equity_(09/12/2014 - )_7]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/12/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_PartialRoll_Multiple_HoldShort() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_2, null, PT_FROM_FUTURE_SECURITY, "12/13/2013", null, 10.00);

		// Setup Initial Trades - Short Position
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 60.0, false);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 6.0, 0, true, true);

		String expectedAllocations = //
				"[PTH14_International Equity_BUY 6 of PTH14 on 03/17/2014_-1]" + //
						"[PTM14_International Equity_SELL 6 of PTM14 on 03/17/2014_1]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[PTH14_International Equity_(03/17/2014 - )_9]" + //
						"[PTM14_International Equity_(03/17/2014 - )_1]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("03/17/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);

		roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/18/2014", false, 6.0, 0, true, true);

		expectedAllocations = //
				"[PTH14_International Equity_BUY 6 of PTH14 on 03/18/2014_-1]" + //
						"[PTM14_International Equity_SELL 6 of PTM14 on 03/18/2014_1]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		expectedFinalResult = //
				"[PTH14_International Equity_(03/18/2014 - )_8]" + //
						"[PTM14_International Equity_(03/18/2014 - )_2]";
		finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("03/18/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_PartialRollWithTail_HoldShort() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_2, null, PT_FROM_FUTURE_SECURITY, "12/13/2013", null, 10.00);

		// Setup Initial Trades - Short Position
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 60.0, false);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 20.0, 6.0, true, true);

		String expectedAllocations = //
				"[PTH14_International Equity_BUY 20 of PTH14 on 03/17/2014_-3]" + //
						"[PTH14_International Equity_BUY 6 of PTH14 on 03/17/2014_-1]" + //
						"[PTM14_International Equity_SELL 20 of PTM14 on 03/17/2014_3]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[PTH14_International Equity_(03/17/2014 - )_6]" + //
						"[PTM14_International Equity_(03/17/2014 - )_3]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("03/17/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_FullRollWithTail_HoldShort() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_2, null, PT_FROM_FUTURE_SECURITY, "12/13/2013", null, 10.00);

		// Setup Initial Trades - Short Position
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 60.0, false);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 50.0, 10.0, true, true);

		String expectedAllocations = //
				"[PTH14_International Equity_BUY 50 of PTH14 on 03/17/2014_-8]" + //
						"[PTH14_International Equity_BUY 10 of PTH14 on 03/17/2014_-2]" + //
						"[PTM14_International Equity_SELL 50 of PTM14 on 03/17/2014_8]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[PTM14_International Equity_(03/17/2014 - )_8]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("03/17/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_PartialRoll_MultipleSameDay_CancelFirstRoll() {
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "06/13/2014", null, 100.00);

		// Setup Initial Trades - Long Position
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 150.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 10.0, 0);
		String expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 10 of ESU14 on 09/12/2014_-7]" + //
						"[ESZ14_Domestic Equity_BUY 10 of ESZ14 on 09/12/2014_7]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Cancel the first roll
		cancelRoll(roll);

		TradeRoll roll2 = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 15.0, 0, true, true);
		expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 15 of ESU14 on 09/12/2014_-10]" + //
						"[ESZ14_Domestic Equity_BUY 15 of ESZ14 on 09/12/2014_10]";
		validateTradeAssetClassPositionAllocationsForRoll(roll2, expectedAllocations);

		String expectedFinalResult = //
				"[ESU14_Domestic Equity_(09/12/2014 - )_90]" + //
						"[ESZ14_Domestic Equity_(09/12/2014 - )_10]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/12/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	////////////////////////////////////////////////////////////
	//  Tests based on real trades in IMS
	// NOTE: The "tradeFuture" calls are reduced to  no more than 4 trades
	// so the ending quantity is the same, but we don't have process all of the trades


	@Test
	public void testRollAccount_Explicit_Full_Roll_HoldLong() {
		// 576700_ESU14_TO_ESZ14
		// Split in 4 Asset Classes - Explicit in 3 and 4th gets the rest
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "06/13/2014", null, 31.00);
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_2, null, ES_FROM_FUTURE_SECURITY, "06/27/2014", null, 30.00);
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_3, null, ES_FROM_FUTURE_SECURITY, "07/31/2014", null, 53.00);

		// Setup Initial Trades
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 53.0, true);
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/27/2014", 55.0, false);
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "07/02/2014", 15.0, true);
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "08/11/2014", 129.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", true, 142.0, 0, true, true);

		String expectedAllocations = //
				"[ESU14_Domestic Equity_SELL 142 of ESU14 on 09/12/2014_-31]" + //
						"[ESU14_International Equity_SELL 142 of ESU14 on 09/12/2014_-30]" + //
						"[ESU14_Fixed Income Core_SELL 142 of ESU14 on 09/12/2014_-53]" + //
						"[ESZ14_Domestic Equity_BUY 142 of ESZ14 on 09/12/2014_31]" + //
						"[ESZ14_International Equity_BUY 142 of ESZ14 on 09/12/2014_30]" + //
						"[ESZ14_Fixed Income Core_BUY 142 of ESZ14 on 09/12/2014_53]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = //
				"[ESZ14_Domestic Equity_(09/12/2014 - )_31]" + //
						"[ESZ14_International Equity_(09/12/2014 - )_30]" + //
						"[ESZ14_Fixed Income Core_(09/12/2014 - )_53]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/12/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_Full_Roll_HoldShort() {
		// 445500_ESU14_TO_ESZ14
		// Split in 2 Asset Classes - Explicit in 1 and 2nd gets the rest
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, ES_FROM_FUTURE_SECURITY, "07/31/2014", null, 5.00);

		// Setup Initial Trades
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "06/13/2014", 60.0, false);
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "07/23/2014", 20.0, true);
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "08/29/2014", 13.0, false);
		tradeFuture(accountInfo, ES_FROM_FUTURE_SECURITY, "09/09/2014", 5.0, true);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, ES_FROM_FUTURE_SECURITY, ES_TO_FUTURE_SECURITY, "09/12/2014", false, 48.0, 0, true, true);

		String expectedAllocations = //
				"[ESU14_Domestic Equity_BUY 48 of ESU14 on 09/12/2014_-5]" + //
						"[ESZ14_Domestic Equity_SELL 48 of ESZ14 on 09/12/2014_5]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = "[ESZ14_Domestic Equity_(09/12/2014 - )_5]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("09/12/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_2_Rolls_SameDay_HoldShort() {
		// 667600 Ventura PTH14 to PTM14 - Full roll after 2 partial rolls on same trade date
		// Split in 2 Asset Classes - Explicit in 1 and 2nd gets the rest
		AccountInfo accountInfo = setupAccount();
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, PT_FROM_FUTURE_SECURITY, "01/31/2014", null, -14.00);

		// Setup Initial Trades
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "12/13/2013", 79.0, true);
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "01/09/2014", 4.0, false);
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "01/23/2014", 9.0, true);
		tradeFuture(accountInfo, PT_FROM_FUTURE_SECURITY, "01/31/2014", 97.0, false);

		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 9.0, 0, true, false);

		String expectedAllocations = //
				"[PTH14_Domestic Equity_BUY 9 of PTH14 on 03/17/2014_10]" + //
						"[PTM14_Domestic Equity_SELL 9 of PTM14 on 03/17/2014_-10]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Roll the rest
		roll = rollFuture(accountInfo, PT_FROM_FUTURE_SECURITY, PT_TO_FUTURE_SECURITY, "03/17/2014", false, 4.0, 0, true, true);
		expectedAllocations = //
				"[PTH14_Domestic Equity_BUY 4 of PTH14 on 03/17/2014_4]" + //
						"[PTM14_Domestic Equity_SELL 4 of PTM14 on 03/17/2014_-4]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = "[PTM14_Domestic Equity_(03/17/2014 - )_-14]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("03/17/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_Zero_Multiple_Rolls_Multiple_Days_HoldLong() {
		// 667600 Ventura MESZ14 to MESH15 - Full roll after 7 partial rolls over 4 days
		// Split in 2 Asset Classes - Explicit in 1, but allocated 0
		AccountInfo accountInfo = setupAccount();
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MESZ14");
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_2, null, security, "12/05/2014", null, 0.00);

		// Setup Initial Trades
		tradeFuture(accountInfo, security, "09/11/2014", 1.0, false);
		tradeFuture(accountInfo, security, "09/18/2014", 22.0, true);

		InvestmentSecurity toSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MESH15");
		// Roll the Security
		TradeRoll roll = rollFuture(accountInfo, security, toSecurity, "12/08/2014", true, 1.0, 0, true, false);

		String expectedAllocations = //
				"[MESZ14_International Equity_SELL 1 of MESZ14 on 12/08/2014_0]" + //
						"[MESH15_International Equity_BUY 1 of MESH15 on 12/08/2014_0]";

		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Roll AGAIN - Same Day
		roll = rollFuture(accountInfo, security, toSecurity, "12/08/2014", true, 1.0, 0, true, true);
		// Same Trade - Same result
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = "[MESZ14_International Equity_(12/05/2014 - )_0]" + //
				"[MESH15_International Equity_(12/08/2014 - )_0]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("12/08/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);

		// Roll Again - Next Day
		roll = rollFuture(accountInfo, security, toSecurity, "12/09/2014", true, 5.0, 0, true, true);
		expectedAllocations = //
				"[MESZ14_International Equity_SELL 5 of MESZ14 on 12/09/2014_0]" + //
						"[MESH15_International Equity_BUY 5 of MESH15 on 12/09/2014_0]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// QUESTION - NO MORE CHANGES TO ACTUAL ALLOCATIONS - HOW DO WE KNOW WHEN 0 MEANS ZERO AND NOT JUST TO END IT????
		finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("12/09/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);

		// Roll Again
		roll = rollFuture(accountInfo, security, toSecurity, "12/10/2014", true, 3.0, 0, true, false);
		expectedAllocations = //
				"[MESZ14_International Equity_SELL 3 of MESZ14 on 12/10/2014_0]" + //
						"[MESH15_International Equity_BUY 3 of MESH15 on 12/10/2014_0]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Roll Again
		roll = rollFuture(accountInfo, security, toSecurity, "12/10/2014", true, 3.0, 0, true, false);
		expectedAllocations = //
				"[MESZ14_International Equity_SELL 3 of MESZ14 on 12/10/2014_0]" + //
						"[MESH15_International Equity_BUY 3 of MESH15 on 12/10/2014_0]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Roll Again
		roll = rollFuture(accountInfo, security, toSecurity, "12/11/2014", true, 2.0, 0, true, false);
		expectedAllocations = //
				"[MESZ14_International Equity_SELL 2 of MESZ14 on 12/11/2014_0]" + //
						"[MESH15_International Equity_BUY 2 of MESH15 on 12/11/2014_0]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// Roll Again
		roll = rollFuture(accountInfo, security, toSecurity, "12/11/2014", true, 6.0, 0, true, true);
		expectedAllocations = //
				"[MESZ14_International Equity_SELL 6 of MESZ14 on 12/11/2014_0]" + //
						"[MESH15_International Equity_BUY 6 of MESH15 on 12/11/2014_0]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// AT THIS POINT YOU WOULD WANT MESZ14 TO BE REMOVED FROM THE LIST - HOW CAN WE DO THIS?  CHECK ACTUAL POSITION QUANTITY IF 0 AND IF NOT ZERO, KEEP THE 0 ELSE END IT???
		finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("12/11/2014"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	@Test
	public void testRollAccount_Explicit_Multiple_Rolls_One_Day_HoldLong() {
		// 125300 MFSH8 to MFSM8 - Full roll after 6 partial rolls over 1 day
		// Split in 2 Asset Classes - Explicit in 1, allocated 515 of 983
		// After all the rolls are complete, all 515 explicit should be defined for the new security
		AccountInfo accountInfo = setupAccount();
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MFSH8");
		setupAssetClassPositionAllocation(accountInfo, ASSET_CLASS_1, null, security, "02/02/2018", null, 515.00);

		// Setup Initial Trades
		tradeFuture(accountInfo, security, "02/02/2018", 983.0, true);

		InvestmentSecurity toSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("MFSM8");

		// 1. Roll 123
		TradeRoll roll = rollFuture(accountInfo, security, toSecurity, "03/12/2018", true, 123.0, 0, true, false);
		String expectedAllocations = "[MFSH18_Domestic Equity_SELL 123 of MFSH18 on 03/12/2018_-64][MFSM18_Domestic Equity_BUY 123 of MFSM18 on 03/12/2018_64]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// 2. Roll 123 - Same Day
		roll = rollFuture(accountInfo, security, toSecurity, "03/12/2018", true, 123.0, 0, true, false);
		expectedAllocations = "[MFSH18_Domestic Equity_SELL 123 of MFSH18 on 03/12/2018_-65][MFSM18_Domestic Equity_BUY 123 of MFSM18 on 03/12/2018_65]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// 3. Roll 125 - Same Day
		roll = rollFuture(accountInfo, security, toSecurity, "03/12/2018", true, 125.0, 0, true, false);
		expectedAllocations = "[MFSH18_Domestic Equity_SELL 125 of MFSH18 on 03/12/2018_-65][MFSM18_Domestic Equity_BUY 125 of MFSM18 on 03/12/2018_65]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// 4. Roll 125 - Same Day
		roll = rollFuture(accountInfo, security, toSecurity, "03/12/2018", true, 125.0, 0, true, false);
		expectedAllocations = "[MFSH18_Domestic Equity_SELL 125 of MFSH18 on 03/12/2018_-66][MFSM18_Domestic Equity_BUY 125 of MFSM18 on 03/12/2018_66]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// 5. Roll 244 - Same Day
		roll = rollFuture(accountInfo, security, toSecurity, "03/12/2018", true, 244.0, 0, true, false);
		expectedAllocations = "[MFSH18_Domestic Equity_SELL 244 of MFSH18 on 03/12/2018_-128][MFSM18_Domestic Equity_BUY 244 of MFSM18 on 03/12/2018_128]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		// 6. Roll FINAL 243 - Same Day
		roll = rollFuture(accountInfo, security, toSecurity, "03/12/2018", true, 243.0, 0, true, true);
		expectedAllocations = "[MFSH18_Domestic Equity_SELL 243 of MFSH18 on 03/12/2018_-127][MFSM18_Domestic Equity_BUY 243 of MFSM18 on 03/12/2018_127]";
		validateTradeAssetClassPositionAllocationsForRoll(roll, expectedAllocations);

		String expectedFinalResult = "[MFSM18_Domestic Equity_(03/12/2018 - )_515]";
		String finalResult = getInvestmentAccountAssetClassPositionAllocationListAsString(accountInfo.getClientAccount().getId(), DateUtils.toDate("03/12/2018"));
		Assertions.assertEquals(expectedFinalResult, finalResult);
	}


	///////////////////////////////////////////////////////////
	/////   Helper Methods
	///////////////////////////////////////////////////////////


	/**
	 * Sets up the Account, Asset Classes, etc. needed
	 */
	private AccountInfo setupAccount() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.FUTURES));

		setupAssetClass(accountInfo, ASSET_CLASS_1, REPLICATION_1);
		setupAssetClass(accountInfo, ASSET_CLASS_2, REPLICATION_2);
		setupAssetClass(accountInfo, ASSET_CLASS_3, REPLICATION_3);
		setupAssetClass(accountInfo, ASSET_CLASS_4, REPLICATION_4);
		setupAssetClass(accountInfo, ASSET_CLASS_CASH, null);

		return accountInfo;
	}


	/**
	 * Creates asset classes for specified account using asset class name and replication name
	 * We use existing ones, so if name changes then we need to update the names here, but for the sake of this processing we don't
	 * care if the security actually exists in the replication, we just assume it does.
	 *
	 * @param accountInfo
	 * @param assetClassName
	 * @param replicationName
	 */
	private void setupAssetClass(AccountInfo accountInfo, String assetClassName, String replicationName) {
		InvestmentAssetClass assetClass = getInvestmentAssetClassByName(assetClassName);
		InvestmentReplication replication = (StringUtils.isEmpty(replicationName) ? null : getInvestmentReplicationByName(replicationName));

		InvestmentAccountAssetClass aac = new InvestmentAccountAssetClass();
		aac.setAccount(accountInfo.getClientAccount());
		aac.setAssetClass(assetClass);
		aac.setAssetClassPercentage(assetClass.isCash() ? BigDecimal.ZERO : BigDecimal.valueOf(25));
		aac.setCashPercentage(aac.getAssetClassPercentage());
		aac.setPrimaryReplication(replication);
		aac.setPerformanceOverlayTargetSource(PerformanceOverlayTargetSources.TARGET_EXPOSURE);
		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(aac), 1);
	}


	private void setupAssetClassPositionAllocation(AccountInfo accountInfo, String assetClassName, String replicationName, InvestmentSecurity security, String startDate, String endDate, double allocationQuantity) {
		InvestmentAccountAssetClassPositionAllocation pa = new InvestmentAccountAssetClassPositionAllocation();
		pa.setAccountAssetClass(getInvestmentAccountAssetClass(accountInfo, assetClassName));
		if (!StringUtils.isEmpty(replicationName)) {
			pa.setReplication(getInvestmentReplicationByName(replicationName));
		}
		pa.setSecurity(security);
		if (!StringUtils.isEmpty(startDate)) {
			pa.setStartDate(DateUtils.toDate(startDate));
		}
		if (!StringUtils.isEmpty(endDate)) {
			pa.setEndDate(DateUtils.toDate(endDate));
		}
		pa.setAllocationQuantity(BigDecimal.valueOf(allocationQuantity));
		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountAssetClassPositionAllocationService.saveInvestmentAccountAssetClassPositionAllocation(pa), 1);
	}


	private void tradeFuture(AccountInfo accountInfo, InvestmentSecurity security, String tradeDate, double quantity, boolean buy) {
		BusinessCompany executingBroker = this.tradeUtils.getDefaultExecutingBroker();
		Trade trade = this.tradeUtils.newFuturesTradeBuilder(security, BigDecimal.valueOf(quantity), buy, DateUtils.toDate(tradeDate), executingBroker, accountInfo).buildAndSave();
		this.ruleViolationUtils.ignoreAllViolationsIfExist("Trade", trade.getId());
		this.tradeUtils.fullyExecuteTrade(trade);
	}


	private TradeRoll rollFuture(AccountInfo accountInfo, InvestmentSecurity fromSecurity, InvestmentSecurity toSecurity, String rollDate, boolean longPosition, double rollQuantity, double tailQuantity) {
		return rollFuture(accountInfo, fromSecurity, toSecurity, rollDate, longPosition, rollQuantity, tailQuantity, false, false);
	}


	private TradeRoll rollFuture(AccountInfo accountInfo, InvestmentSecurity fromSecurity, InvestmentSecurity toSecurity, String rollDate, boolean longPosition, double rollQuantity, double tailQuantity,
	                             boolean fullyExecuteRoll, boolean processAllocations) {
		TradeRoll roll = new TradeRoll();
		roll.setExecutingBrokerCompany(this.tradeUtils.getDefaultExecutingBroker());
		roll.setHoldingCompany(this.tradeUtils.getDefaultExecutingBroker());
		roll.setSecurity(fromSecurity);
		roll.setNewSecurity(toSecurity);
		roll.setTradeDate(DateUtils.toDate(rollDate));
		roll.setSettlementDate(DateUtils.getNextWeekday(roll.getTradeDate()));
		roll.setLongPositions(longPosition);
		roll.setTradeDestination(this.tradeUtils.getManualTradeDestination());
		roll.setTraderUser(this.userUtils.getCurrentUser());

		TradeRoll entryRoll = roll;
		TradeRoll rollEntry = RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradeRollService.getTradeRollEntry(entryRoll),
				new RequestOverrideHandler.EntityPropertyOverrides[]{
						new RequestOverrideHandler.EntityPropertyOverrides(InvestmentSecurity.class, false, "instrument", "underlyingSecurity", "settlementCurrency", "bigSecurity")
				},
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_MAX_DEPTH_PARAMETER, "4"),
				new RequestOverrideHandler.RequestParameterOverrides("enableValidatingBinding", "true"),
				new RequestOverrideHandler.RequestParameterOverrides("disableValidatingBindingValidation", "true")
		);

		boolean found = false;
		for (TradeRollPosition trp : CollectionUtils.getIterable(rollEntry.getPositionList())) {
			if (trp.getClientAccount().equals(accountInfo.getClientAccount())) {
				trp.setRollQuantity(BigDecimal.valueOf(rollQuantity));
				trp.setTailQuantity(BigDecimal.valueOf(tailQuantity));
				found = true;
			}
			else {
				trp.setRollQuantity(BigDecimal.ZERO);
				trp.setTailQuantity(BigDecimal.ZERO);
			}
		}

		if (!found) {
			throw new ValidationException("Could not find existing position to enter rolls for");
		}
		else {
			rollEntry.setPositionList(CollectionUtils.getStream(rollEntry.getPositionList()).filter(trp -> !MathUtils.isNullOrZero(trp.getRollQuantity())).collect(Collectors.toList()));
		}
		RequestOverrideHandler.serviceCallWithOverrides(() -> {
					this.tradeRollService.saveTradeRollEntry(rollEntry);
					return null;
				},
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_GLOBAL_EXCLUSION_PARAMETER, "tradeRoll"),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_MAX_DEPTH_PARAMETER, "1")
		);

		TradeGroupSearchForm searchForm = new TradeGroupSearchForm();
		searchForm.setTradeGroupTypeName(TradeGroupType.GroupTypes.ROLL.getTypeName());
		searchForm.setTradeDate(roll.getTradeDate());
		searchForm.setInvestmentSecurityId(roll.getSecurity().getId());
		searchForm.setSecondaryInvestmentSecurityId(roll.getNewSecurity().getId());
		searchForm.setOrderBy("createDate:desc");
		searchForm.setLimit(1);

		TradeGroup rollGroup = CollectionUtils.getFirstElement(
				RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.tradeGroupService.getTradeGroupList(searchForm), 2));
		Assertions.assertNotNull(rollGroup, "Roll Group is missing");
		roll = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.tradeRollService.getTradeRollGroup(rollGroup.getId()), 4);

		if (fullyExecuteRoll) {
			fullyExecuteRoll(roll);
		}

		if (processAllocations) {
			this.tradeAssetClassPositionAllocationProcessService.processTradeAssetClassPositionAllocationList(accountInfo.getClientAccount().getId(), DateUtils.toDate(rollDate));
		}
		return roll;
	}


	private void fullyExecuteRoll(TradeRoll roll) {
		List<Trade> tradeList = getTradesForRoll(roll);
		for (Trade trade : tradeList) {
			this.ruleViolationUtils.ignoreAllViolationsIfExist("Trade", trade.getId());
			this.tradeUtils.validateTrade(trade);
		}

		roll.setTradeGroupAction(TradeGroupActions.APPROVE);
		saveTradeRoll(roll);

		roll.setFromSecurityPrice(BigDecimal.valueOf(100));
		roll.setToSecurityPrice(BigDecimal.valueOf(100));
		roll.setTradeGroupAction(TradeGroupActions.FILL_EXECUTE);
		saveTradeRoll(roll);

		roll.setTradeGroupAction(TradeGroupActions.BOOK);
		saveTradeRoll(roll);
	}


	private void cancelRoll(TradeRoll roll) {
		roll.setTradeGroupAction(TradeGroupActions.CANCEL);
		saveTradeRoll(roll);
	}


	private void saveTradeRoll(TradeRoll tradeRoll) {
		RequestOverrideHandler.serviceCallWithOverrides(() -> {
					this.tradeRollService.saveTradeRollGroup(tradeRoll);
					return null;
				},
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_GLOBAL_EXCLUSION_PARAMETER, "tradeRoll"),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_MAX_DEPTH_PARAMETER, "1")
		);
	}


	private List<Trade> getTradesForRoll(TradeRoll roll) {
		List<Trade> tradeList = new ArrayList<>(roll.getRollTradeGroup().getTradeList());
		if (roll.getTailTradeGroup() != null) {
			tradeList.addAll(roll.getTailTradeGroup().getTradeList());
		}
		return tradeList;
	}


	private InvestmentAssetClass getInvestmentAssetClassByName(String name) {
		InvestmentAssetClass assetClass = assetClassNameToAssetClassMap.computeIfAbsent(name, key -> {
					InvestmentAssetClassSearchForm searchForm = new InvestmentAssetClassSearchForm();
					searchForm.setNullParent(true);
					searchForm.setName(name);
					return CollectionUtils.getOnlyElement(this.investmentSetupService.getInvestmentAssetClassList(searchForm));
				}
		);
		ValidationUtils.assertNotNull(assetClass, "Asset Class [" + name + "] is missing from the database.  Please update the test to use updated asset class name.");
		return assetClass;
	}


	private InvestmentAccountAssetClass getInvestmentAccountAssetClass(AccountInfo accountInfo, String assetClassName) {
		List<InvestmentAccountAssetClass> accountAssetClassList = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.investmentAccountAssetClassService.getInvestmentAccountAssetClassListByAccount(accountInfo.getClientAccount().getId()), "name|id|account.id|assetClass.id|assetClass.name");
		return CollectionUtils.getOnlyElement(BeanUtils.filter(accountAssetClassList, accountAssetClass -> accountAssetClass.getAssetClass().getName(), assetClassName));
	}


	private InvestmentReplication getInvestmentReplicationByName(String name) {
		InvestmentReplicationSearchForm searchForm = new InvestmentReplicationSearchForm();
		searchForm.setName(name);

		List<InvestmentReplication> list = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.investmentReplicationService.getInvestmentReplicationList(searchForm), "name|id");
		// Search for does a like check, so filter on equals...
		list = BeanUtils.filter(list, InvestmentReplication::getName, name);
		int size = CollectionUtils.getSize(list);
		if (size == 1) {
			return list.get(0);
		}
		throw new ValidationException(size == 0 ? "Replication [" + name + "] is missing from the database.  Please update the test to use updated replication name." : "Found " + size
				+ "replications with name [" + name + "] in the database.  Please update the test to use unique replication name.");
	}


	private String getInvestmentAccountAssetClassPositionAllocationListAsString(Integer clientAccountId, Date activeOnDate) {
		List<InvestmentAccountAssetClassPositionAllocation> list = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.investmentAccountAssetClassPositionAllocationService.getInvestmentAccountAssetClassPositionAllocationList(clientAccountId, activeOnDate), 4);

		if (CollectionUtils.isEmpty(list)) {
			return "[NONE]";
		}
		StringBuilder sb = new StringBuilder(16);
		list = BeanUtils.sortWithFunctions(list, CollectionUtils.createList(
				investmentAccountAssetClassPositionAllocation -> investmentAccountAssetClassPositionAllocation.getSecurity().getId(),
				investmentAccountAssetClassPositionAllocation -> investmentAccountAssetClassPositionAllocation.getAccountAssetClass().getId(),
				investmentAccountAssetClassPositionAllocation -> investmentAccountAssetClassPositionAllocation.getReplication().getId()),
				CollectionUtils.createList(true, true, true));

		for (InvestmentAccountAssetClassPositionAllocation alloc : list) {
			sb.append("[").append(alloc.getSecurity().getSymbol()).append("_").append(alloc.getAccountAssetClass().getLabel()).append("_").append(alloc.getReplication() != null ? alloc.getReplication().getName() + "_" : "").append(alloc.getDateLabel()).append("_").append(CoreMathUtils.formatNumberInteger(alloc.getAllocationQuantity())).append("]");
		}
		return sb.toString();
	}


	private String getTradeAssetClassPositionAllocationListAsString(TradeRoll roll) {
		TradeAssetClassPositionAllocationSearchForm searchForm = new TradeAssetClassPositionAllocationSearchForm();
		if (roll.getTailTradeGroup() != null) {
			searchForm.setTradeGroupIds(new Integer[]{roll.getRollTradeGroup().getId(), roll.getTailTradeGroup().getId()});
		}
		else {
			searchForm.setTradeGroupId(roll.getRollTradeGroup().getId());
		}
		List<TradeAssetClassPositionAllocation> list = RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradeAssetClassService.getTradeAssetClassPositionAllocationList(searchForm),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_MAX_DEPTH_PARAMETER, "4"),
				new RequestOverrideHandler.RequestParameterOverrides(RequestOverrideHandler.REQUESTED_GLOBAL_EXCLUSION_PARAMETER, "tradeAssetClassPositionAllocationSearchForm")
		);
		if (CollectionUtils.isEmpty(list)) {
			return "[NONE]";
		}
		StringBuilder sb = new StringBuilder(16);

		list = BeanUtils.sortWithFunctions(list, CollectionUtils.createList(
				tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getSecurity().getId(),
				tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getAccountAssetClass().getId(),
				tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getReplication() == null ? null : tradeAssetClassPositionAllocation.getReplication().getId(),
				TradeAssetClassPositionAllocation::getAllocationQuantity),
				CollectionUtils.createList(true, true, true, true));

		for (TradeAssetClassPositionAllocation alloc : list) {
			sb.append("[").append(alloc.getSecurity().getSymbol()).append("_").append(alloc.getAccountAssetClass().getLabel()).append("_").append(alloc.getReplication() != null ? alloc.getReplication().getName() + "_" : "").append(alloc.getTrade().getLabelShort()).append("_").append(CoreMathUtils.formatNumberInteger(alloc.getAllocationQuantity())).append("]");
		}
		return sb.toString();
	}


	private void validateNoTradeAssetClassPositionAllocationsForRoll(TradeRoll roll) {
		validateTradeAssetClassPositionAllocationsForRoll(roll, "[NONE]");
	}


	private void validateTradeAssetClassPositionAllocationsForRoll(TradeRoll roll, String expected) {
		String result = getTradeAssetClassPositionAllocationListAsString(roll);
		Assertions.assertEquals(expected, result);
	}
}
