package com.clifton.ims.tests.trade.group;

import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorService;
import com.clifton.accounting.eventjournal.generator.AccountingEventJournalGeneratorTypes;
import com.clifton.accounting.eventjournal.generator.EventJournalGeneratorCommand;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.pending.PendingActivityRequests;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.builders.investment.trade.TradeBuilder;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.copy.InvestmentInstrumentCopyService;
import com.clifton.investment.instrument.copy.InvestmentSecurityCopyCommand;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.execution.TradeExecutionTypeService;
import com.clifton.trade.group.TradeEntry;
import com.clifton.trade.group.TradeEntryDetail;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.dynamic.TradeGroupDynamicService;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRoll;
import com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRollDetail;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author NickK
 */

public class TradeGroupTests extends BaseImsIntegrationTest {

	@Resource
	private AccountingBalanceService accountingBalanceService;
	@Resource
	private AccountingEventJournalGeneratorService accountingEventJournalGeneratorService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private TradeGroupService tradeGroupService;
	@Resource
	private TradeService tradeService;
	@Resource
	private TradeDestinationService tradeDestinationService;
	@Resource
	private TradeExecutionTypeService tradeExecutionTypeService;
	@Resource
	private MarketDataFieldService marketDataFieldService;
	@Resource
	private MarketDataSourceService marketDataSourceService;
	@Resource
	private AccountingValuationService accountingValuationService;
	@Resource
	private InvestmentInstrumentCopyService investmentInstrumentCopyService;
	@Resource
	private TradeGroupDynamicService<TradeGroupDynamicForwardRollDetail> tradeGroupDynamicService;
	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveExchangeForPhysicalWithNoTradeFills() {
		saveExchangeForPhysicalWithTradeFills(false);
	}


	@Test
	public void testSaveExchangeForPhysicalWithTradeFills() {
		saveExchangeForPhysicalWithTradeFills(true);
	}


	private void saveExchangeForPhysicalWithTradeFills(boolean withTradeFills) {
		// securities
		InvestmentSecurity futureSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESZ13");
		InvestmentSecurity fundSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("VOO");

		// Create accounts
		AccountInfo futuresAccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo fundsAccountInfo = this.investmentAccountUtils.createHoldingAccount(futuresAccountInfo, BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.tradeUtils.getTradeType(TradeType.FUNDS));

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create EFP trade list
		BigDecimal futuresQuantity = new BigDecimal("417");
		BigDecimal futuresPrice = BigDecimal.valueOf(1913.72);
		Date buyDate = DateUtils.toDate("09/27/2013");
		TradeBuilder futuresSellBuilder = this.tradeUtils.newFuturesTradeBuilder(futureSecurity, futuresQuantity, false, buyDate, executingBroker, futuresAccountInfo)
				.withAverageUnitPrice(futuresPrice);
		if (withTradeFills) {
			futuresSellBuilder.addFill(BigDecimal.valueOf(50), BigDecimal.valueOf(1913.5))
					.addFill(BigDecimal.valueOf(367), BigDecimal.valueOf(1913.75));
		}
		Trade futuresSell = futuresSellBuilder.build();

		BigDecimal fundsQuantity = new BigDecimal("227715");
		BigDecimal fundsPrice = BigDecimal.valueOf(176.1484);
		Trade fundsBuy = this.tradeUtils.newTradeBuilder(this.tradeUtils.getTradeType(TradeType.FUNDS), fundSecurity, fundsQuantity, fundsPrice, true, buyDate, executingBroker, fundsAccountInfo)
				.withCommissionPerUnit(BigDecimal.ZERO)
				.build();
		List<Trade> tradeList = new ArrayList<>(2);
		tradeList.add(futuresSell);
		tradeList.add(fundsBuy);

		// Prepare EFP TradeGroup
		final TradeGroup efpGroup = createTradeGroup(TradeGroupType.GroupTypes.EFP.getTypeName(), tradeList);
		efpGroup.setInvestmentSecurity(futureSecurity);
		efpGroup.setSecondaryInvestmentSecurity(fundSecurity);

		// save TradeGroup and refresh the sell trade
		TradeGroup savedEfpGroup = RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradeGroupService.saveTradeGroup(efpGroup),
				new RequestOverrideHandler.EntityPropertyOverrides(Trade.class, true, "tradeFillList"));
		futuresSell = savedEfpGroup.getTradeList().get(0);
		fundsBuy = savedEfpGroup.getTradeList().get(1);

		// Assert Status'
		Assertions.assertEquals(WorkflowStatus.STATUS_PENDING, futuresSell.getWorkflowStatus().getName(), "Expected trade's status to be " + WorkflowStatus.STATUS_PENDING);
		Assertions.assertEquals(WorkflowStatus.STATUS_PENDING, fundsBuy.getWorkflowStatus().getName(), "Expected trade's status to be " + WorkflowStatus.STATUS_PENDING);

		// Assert fills
		if (withTradeFills) {
			List<TradeFill> fillList = this.tradeUtils.getTradeFills(futuresSell);
			Assertions.assertEquals(2, fillList.size(), "Expected future's trade fill list to be of size 2.");
		}
		else {
			Assertions.assertNull(futuresSell.getTradeFillList());
		}
	}


	@Test
	public void testAssetBackedBondMultiClientTrade() {
		TradeType bondsTradeType = this.tradeUtils.getTradeType(TradeType.BONDS);
		AccountInfo client1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.NORTHERN_TRUST, bondsTradeType);
		AccountInfo client2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.NORTHERN_TRUST, bondsTradeType);
		AccountInfo client3 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.NORTHERN_TRUST, bondsTradeType);

		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("38378BH51");

		// generate open positions
		TradeEntry openEntry = new TradeEntry();
		openEntry.setTraderUser(this.tradeUtils.getDefaultTrader());
		openEntry.setInvestmentSecurity(security);
		openEntry.setPayingSecurity(security.getInstrument().getTradingCurrency());
		openEntry.setTradeType(bondsTradeType);
		openEntry.setBuy(true);
		openEntry.setTradeDestination(this.tradeUtils.getManualTradeDestination());
		openEntry.setExecutingBrokerCompany(this.tradeUtils.getDefaultExecutingBroker());
		openEntry.setCurrentFactor(new BigDecimal("0.181965582"));
		openEntry.setAverageUnitPrice(new BigDecimal("100.64899"));
		openEntry.setTradeDate(DateUtils.toDate("02/25/2020"));
		openEntry.setSettlementDate(this.tradeUtils.getSettlementDate(security, openEntry.getTradeDate()));
		openEntry.setOriginalFace(new BigDecimal("3900000"));
		openEntry.setQuantityIntended(new BigDecimal("709665.77"));
		openEntry.setAccrualAmount(new BigDecimal("1197.56"));
		openEntry.setAccountingNotional(new BigDecimal("714271.43"));
		openEntry.setDescription("Testing for penny rounding");
		openEntry.setTradeDetailList(new ArrayList<>());

		TradeEntryDetail openDetail1 = new TradeEntryDetail();
		openDetail1.setClientInvestmentAccount(client1.getClientAccount());
		openDetail1.setHoldingInvestmentAccount(client1.getHoldingAccount());
		openDetail1.setOriginalFace(new BigDecimal("850000"));
		openEntry.getTradeDetailList().add(openDetail1);

		TradeEntryDetail openDetail2 = new TradeEntryDetail();
		openDetail2.setClientInvestmentAccount(client2.getClientAccount());
		openDetail2.setHoldingInvestmentAccount(client2.getHoldingAccount());
		openDetail2.setOriginalFace(new BigDecimal("850000"));
		openEntry.getTradeDetailList().add(openDetail2);

		TradeEntryDetail openDetail3 = new TradeEntryDetail();
		openDetail3.setClientInvestmentAccount(client3.getClientAccount());
		openDetail3.setHoldingInvestmentAccount(client3.getHoldingAccount());
		openDetail3.setOriginalFace(new BigDecimal("2200000"));
		openEntry.getTradeDetailList().add(openDetail3);

		processTradeEntryTrades(openEntry, false);

		// process events: cash coupon and factor change
		Date closeDate = DateUtils.toDate("03/16/2020");
		InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
		eventSearchForm.setSecurityId(security.getId());
		// eventSearchForm.setTypeName(InvestmentSecurityEventType.FACTOR_CHANGE);
		eventSearchForm.setAfterEventDate(openEntry.getTradeDate());
		eventSearchForm.setBeforeEventDate(closeDate);
		List<InvestmentSecurityEvent> eventList = this.investmentSecurityEventService.getInvestmentSecurityEventList(eventSearchForm);
		Assertions.assertEquals(2, eventList.size());
		for (InvestmentSecurityEvent event : eventList) {
			EventJournalGeneratorCommand command = new EventJournalGeneratorCommand();
			command.setEventId(event.getId());
			command.setGeneratorType(AccountingEventJournalGeneratorTypes.POST);
			int count = this.accountingEventJournalGeneratorService.generateAccountingEventJournalList(command);
			Assertions.assertTrue(count > 0);
		}

		// close positions
		TradeEntry closeEntry = new TradeEntry();
		BeanUtils.copyProperties(openEntry, closeEntry);
		closeEntry.setBuy(false);
		closeEntry.setCurrentFactor(new BigDecimal("0.1810004283"));
		closeEntry.setAverageUnitPrice(new BigDecimal("99.71875"));
		closeEntry.setTradeDate(closeDate);
		closeEntry.setSettlementDate(this.tradeUtils.getSettlementDate(security, closeDate));
		closeEntry.setQuantityIntended(new BigDecimal("705901.67"));
		closeEntry.setAccrualAmount(new BigDecimal("794.14"));
		closeEntry.setAccountingNotional(new BigDecimal("703916.32"));

		processTradeEntryTrades(closeEntry, true);
	}


	@Test
	public void testTradeGroupDynamicEntry_forwardWithoutNotional() {


		// Create currency forward trade for correct acct/security
		InvestmentSecurity originalForward = this.investmentInstrumentService.getInvestmentSecurityBySymbol("MXN/USD20201216", false);
		InvestmentSecurityCopyCommand command = new InvestmentSecurityCopyCommand();
		BeanUtils.copyProperties(originalForward, command);
		command.setSecurityToCopy(originalForward);
		command.setSettlementDate(DateUtils.toDate("12/18/2020"));
		InvestmentSecurity forward = this.investmentInstrumentCopyService.saveInvestmentSecurityForwardWithCommand(command);
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("185460");
		InvestmentSecurity mxn = this.investmentInstrumentService.getInvestmentSecurityBySymbol("MXN", true);
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.JP_MORGAN_CHASE_BANK_NA);

		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setClientAccount(clientAccount);

		Trade trade = this.tradeUtils.newForwardsTradeBuilder(forward, null, true, DateUtils.toDate("12/07/2020"), broker, accountInfo)
				.withAccountingNotional(new BigDecimal(100000))
				.withTradeDestination(this.tradeDestinationService.getTradeDestinationByName("FX Connect Ticket"))
				.withSettlementDate(DateUtils.toDate("12/18/2020"))
				.withTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionTypeByName("Non-CLS"))
				.withPayingSecurity(mxn)
				.buildAndSave();

		// Ensure no Settlement Amount (quantity Intended)
		ValidationUtils.assertNull(trade.getQuantityIntended(), "Trade was not properly initialized for test.");

		AccountingBalanceSearchForm balanceSearchForm = new AccountingBalanceSearchForm();

		balanceSearchForm.setClientInvestmentAccountId(clientAccount.getId());
		balanceSearchForm.setTransactionDate(DateUtils.toDate("12/07/2020"));
		balanceSearchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE);
		balanceSearchForm.setInvestmentSecurityId(forward.getId());

		List<AccountingBalanceValue> maturingPositionsList = this.accountingValuationService.getAccountingBalanceValueList(balanceSearchForm);

		ValidationUtils.assertEmpty(maturingPositionsList, "Found unexpected positions for new security.");

		TradeGroupDynamicForwardRoll dynamic = new TradeGroupDynamicForwardRoll();
		dynamic.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName("Forward Roll"));
		dynamic.setTradeType(this.tradeService.getTradeTypeByName("Forwards"));
		dynamic.setClientAccountGroup(this.investmentAccountGroupService.getInvestmentAccountGroupByName("DUMAC Currency Hedging Forwards"));
		dynamic.setStartMaturityDate(DateUtils.toDate("12/11/2020"));
		dynamic.setEndMaturityDate(DateUtils.toDate("12/30/2020"));
		dynamic.setIncludePendingTransactions(true);
		dynamic.setNewMaturityDate(DateUtils.toDate("03/17/2020"));
		dynamic.setDefaultRollToBaseCurrency(true);
		dynamic.setDefaultRollToSameBroker(true);
		dynamic.setTradeExecutionType(this.tradeExecutionTypeService.getTradeExecutionTypeByName("Non-CLS"));
		dynamic.setTradeDestination(this.tradeDestinationService.getTradeDestinationByName("FX Connect Ticket"));
		dynamic.setTradeDate(DateUtils.toDate("12/10/2020"));

		TradeGroupDynamicForwardRoll roll = (TradeGroupDynamicForwardRoll) RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradeGroupDynamicService.getTradeGroupDynamicEntry(dynamic),
				new RequestOverrideHandler.EntityPropertyOverrides[]{
						new RequestOverrideHandler.EntityPropertyOverrides(SystemBean.class, false, "entityModifyCondition"),
						new RequestOverrideHandler.EntityPropertyOverrides(SystemBeanType.class, false, "entityModifyCondition")
				},
				new RequestOverrideHandler.RequestParameterOverrides("dtoClassForBinding", "com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRoll"),
				new RequestOverrideHandler.RequestParameterOverrides("enableValidatingBinding", "true"),
				new RequestOverrideHandler.RequestParameterOverrides("requestedPropertiesToExcludeGlobally", "tradeList")
		);

		ValidationUtils.assertEmpty(roll.getDetailList()
				.stream()
				.filter(detail -> detail.getMaturingSecurity().getId().equals(forward.getId()))
				.collect(Collectors.toList()), "Found unexpected forward roll detail");

		MarketDataValue newPrice = new MarketDataValue();
		newPrice.setMeasureValue(new BigDecimal("100"));
		newPrice.setInvestmentSecurity(forward);
		newPrice.setMeasureDate(DateUtils.toDate("12/06/2020"));
		newPrice.setMeasureTime(TimeUtils.toTime("23:59:59"));
		newPrice.setDataField(this.marketDataFieldService.getMarketDataFieldByName(MarketDataField.FIELD_LAST_TRADE_PRICE));
		newPrice.setDataSource(this.marketDataSourceService.getMarketDataSourceByName(MarketDataSource.MARKET_DATA_SOURCE_NAME_PARAMETRIC_CLIFTON));
		this.marketDataFieldService.saveMarketDataValue(newPrice);

		balanceSearchForm = new AccountingBalanceSearchForm();

		balanceSearchForm.setClientInvestmentAccountId(clientAccount.getId());
		balanceSearchForm.setTransactionDate(DateUtils.toDate("12/07/2020"));
		balanceSearchForm.setPendingActivityRequest(PendingActivityRequests.POSITIONS_WITH_MERGE);
		balanceSearchForm.setInvestmentSecurityId(forward.getId());

		maturingPositionsList = this.accountingValuationService.getAccountingBalanceValueList(balanceSearchForm);

		// The maturing position and the detail on the Forward Roll below will not show up due to current limitations on TradeFillUtilsHandlerImpl
		//  When fixed, we can uncomment the validation for maturing positions and the roll below

		ValidationUtils.assertEquals(maturingPositionsList.size(), 0, "Found unexpect positions for new security.");
		// ValidationUtils.assertTrue(MathUtils.isEqual(CollectionUtils.getOnlyElement(maturingPositionsList).getPrice(), new BigDecimal("100")), "Position did not have correct price.");
		// ValidationUtils.assertTrue(MathUtils.isEqual(CollectionUtils.getOnlyElement(maturingPositionsList).getLocalCostBasis(), new BigDecimal("100000")), "Position did not have cost basis.");

		roll = (TradeGroupDynamicForwardRoll) RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradeGroupDynamicService.getTradeGroupDynamicEntry(dynamic),
				new RequestOverrideHandler.EntityPropertyOverrides[]{
						new RequestOverrideHandler.EntityPropertyOverrides(SystemBean.class, false, "entityModifyCondition"),
						new RequestOverrideHandler.EntityPropertyOverrides(SystemBeanType.class, false, "entityModifyCondition")
				},
				new RequestOverrideHandler.RequestParameterOverrides("dtoClassForBinding", "com.clifton.trade.group.dynamic.roll.TradeGroupDynamicForwardRoll"),
				new RequestOverrideHandler.RequestParameterOverrides("enableValidatingBinding", "true"),
				new RequestOverrideHandler.RequestParameterOverrides("requestedPropertiesToExcludeGlobally", "tradeList")
		);

		// ValidationUtils.assertNotEmpty(roll.getDetailList()
		ValidationUtils.assertEmpty(roll.getDetailList()
				.stream()
				.filter(detail -> detail.getMaturingSecurity().getId().equals(forward.getId()))
				.collect(Collectors.toList()), "Found unexpected forward roll detail");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private TradeGroup createTradeGroup(String groupTypeName, List<Trade> tradeList) {
		TradeGroupType groupType = this.tradeGroupService.getTradeGroupTypeByName(groupTypeName);
		TradeGroup tradeGroup = new TradeGroup();
		tradeGroup.setTradeGroupType(groupType);
		tradeGroup.setTradeList(tradeList);
		Trade firstTrade = CollectionUtils.getFirstElement(tradeList);
		if (firstTrade != null) {
			tradeGroup.setTraderUser(firstTrade.getTraderUser());
			tradeGroup.setTradeDate(firstTrade.getTradeDate());
		}
		return tradeGroup;
	}


	private void processTradeEntryTrades(TradeEntry entry, boolean close) {
		this.tradeGroupService.saveTradeEntry(entry);

		Integer[] clientIds = entry.getTradeDetailList().stream()
				.map(TradeEntryDetail::getClientInvestmentAccount)
				.map(InvestmentAccount::getId)
				.toArray(Integer[]::new);

		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setSecurityId(entry.getInvestmentSecurity().getId());
		tradeSearchForm.setTradeDate(entry.getTradeDate());
		tradeSearchForm.setClientInvestmentAccountIds(clientIds);
		tradeSearchForm.setBuy(!close);
		List<Trade> tradeList = this.tradeService.getTradeList(tradeSearchForm);
		Assertions.assertEquals(clientIds.length, tradeList.size());

		for (Trade trade : tradeList) {
			this.tradeUtils.fullyExecuteTrade(trade);
		}

		AccountingBalanceCommand balanceCommand = AccountingBalanceCommand.forClientAccountsOnTransactionDate(clientIds, entry.getTradeDate())
				.withInvestmentSecurityId(entry.getInvestmentSecurity().getId())
				.withAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
		List<AccountingBalance> balanceList = this.accountingBalanceService.getAccountingBalanceList(balanceCommand);
		Assertions.assertEquals(close ? 0 : clientIds.length, balanceList.size());
	}
}
