package com.clifton.ims.tests.builders.investment.manager.balance;

import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceAdjustment;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.rule.violation.status.RuleViolationStatus;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public class InvestmentManagerAccountBalanceBuilder {

	private final InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private InvestmentManagerAccount managerAccount;
	private Date balanceDate;
	private BigDecimal cashValue;
	private BigDecimal securitiesValue;
	// adjusted amounts are same as non-adjusted when there are no adjustments
	// adjustment details are captured by InvestmentManagerAccountBalanceAdjustment(s) and may include m2m, manual overrides, etc.
	// this does NOT include MOC adjustments
	private BigDecimal adjustedCashValue;
	private BigDecimal adjustedSecuritiesValue;
	// adjusted amounts are same as non-adjusted when there are no adjustments
	// MOC Balances = Adjusted Value +/- MOC Adjustments.  If no MOC Adjustments
	// then the MOC value is the same as the adjusted value.
	private BigDecimal marketOnCloseCashValue;
	private BigDecimal marketOnCloseSecuritiesValue;
	private RuleViolationStatus violationStatus;
	private String note;
	private List<InvestmentManagerAccountBalanceAdjustment> adjustmentList;


	private InvestmentManagerAccountBalanceBuilder(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public static InvestmentManagerAccountBalanceBuilder investmentManagerAccountBalance(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		return new InvestmentManagerAccountBalanceBuilder(investmentManagerAccountBalanceService);
	}


	public InvestmentManagerAccountBalanceBuilder but() {
		return investmentManagerAccountBalance(this.investmentManagerAccountBalanceService)
				.withManagerAccount(this.managerAccount)
				.withBalanceDate(this.balanceDate)
				.withCashValue(this.cashValue)
				.withSecuritiesValue(this.securitiesValue)
				.withAdjustedCashValue(this.adjustedCashValue)
				.withAdjustedSecuritiesValue(this.adjustedSecuritiesValue)
				.withMarketOnCloseCashValue(this.marketOnCloseCashValue)
				.withMarketOnCloseSecuritiesValue(this.marketOnCloseSecuritiesValue)
				.withViolationStatus(this.violationStatus)
				.withNote(this.note)
				.withAdjustmentList(this.adjustmentList);
	}


	public InvestmentManagerAccountBalance build() {
		InvestmentManagerAccountBalance investmentManagerAccountBalance = new InvestmentManagerAccountBalance();
		investmentManagerAccountBalance.setManagerAccount(this.managerAccount);
		investmentManagerAccountBalance.setBalanceDate(this.balanceDate);
		investmentManagerAccountBalance.setCashValue(this.cashValue);
		investmentManagerAccountBalance.setSecuritiesValue(this.securitiesValue);
		investmentManagerAccountBalance.setAdjustedCashValue(this.adjustedCashValue);
		investmentManagerAccountBalance.setAdjustedSecuritiesValue(this.adjustedSecuritiesValue);
		investmentManagerAccountBalance.setMarketOnCloseCashValue(this.marketOnCloseCashValue);
		investmentManagerAccountBalance.setMarketOnCloseSecuritiesValue(this.marketOnCloseSecuritiesValue);
		investmentManagerAccountBalance.setViolationStatus(this.violationStatus);
		investmentManagerAccountBalance.setNote(this.note);
		investmentManagerAccountBalance.setAdjustmentList(this.adjustmentList);
		return investmentManagerAccountBalance;
	}


	public InvestmentManagerAccountBalance buildAndSave() {
		InvestmentManagerAccountBalance investmentManagerAccountBalance = build();
		this.investmentManagerAccountBalanceService.saveInvestmentManagerAccountBalance(investmentManagerAccountBalance);
		//refresh it to ensure proper loading of all properties.
		investmentManagerAccountBalance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalance(investmentManagerAccountBalance.getId());
		return investmentManagerAccountBalance;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceBuilder withManagerAccount(InvestmentManagerAccount managerAccount) {
		this.managerAccount = managerAccount;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withSecuritiesValue(BigDecimal securitiesValue) {
		this.securitiesValue = securitiesValue;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withAdjustedCashValue(BigDecimal adjustedCashValue) {
		this.adjustedCashValue = adjustedCashValue;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withAdjustedSecuritiesValue(BigDecimal adjustedSecuritiesValue) {
		this.adjustedSecuritiesValue = adjustedSecuritiesValue;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withMarketOnCloseCashValue(BigDecimal marketOnCloseCashValue) {
		this.marketOnCloseCashValue = marketOnCloseCashValue;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withMarketOnCloseSecuritiesValue(BigDecimal marketOnCloseSecuritiesValue) {
		this.marketOnCloseSecuritiesValue = marketOnCloseSecuritiesValue;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withNote(String note) {
		this.note = note;
		return this;
	}


	public InvestmentManagerAccountBalanceBuilder withAdjustmentList(List<InvestmentManagerAccountBalanceAdjustment> adjustmentList) {
		this.adjustmentList = adjustmentList;
		return this;
	}
}
