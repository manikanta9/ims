package com.clifton.ims.tests.lending.repo;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.DayCountConventions;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.lending.repo.LendingRepoDefinition;
import com.clifton.lending.repo.LendingRepoService;
import com.clifton.lending.repo.LendingRepoType;
import com.clifton.security.user.SecurityUser;

import java.math.BigDecimal;
import java.util.Date;


@SuppressWarnings("hiding")
public class LendingRepoBuilder {

	private final LendingRepoService lendingRepoService;

	//////////////////////////////
	private BusinessCompany counterparty;
	private InvestmentAccount clientInvestmentAccount;
	private InvestmentAccount holdingInvestmentAccount;
	private InvestmentAccount repoInvestmentAccount;
	private InvestmentAccount triPartyInvestmentAccount;
	private InvestmentSecurity investmentSecurity;
	private InvestmentInstrumentHierarchy instrumentHierarchy; // NOTE: do we need this field or is it redundant to investmentSecurity?

	private boolean reverse;
	private boolean active;
	private String dayCountConvention;

	//////////////////////////////

	private LendingRepoType type;
	private SecurityUser traderUser;

	private int term;
	private BigDecimal interestRate;
	private String description;

	private BigDecimal price;
	private BigDecimal haircutPercent;

	private BigDecimal marketValue;
	private BigDecimal netCash;
	private BigDecimal interestAmount;
	private BigDecimal exchangeRateToBase;
	private BigDecimal indexRatio;

	private BigDecimal quantityIntended;
	private BigDecimal originalFace;

	private Date tradeDate;
	private Date settlementDate;
	private Date maturityDate;
	private Date maturitySettlementDate;

	private boolean roll;


	LendingRepoBuilder(LendingRepoService lendingRepoService) {
		this.lendingRepoService = lendingRepoService;
	}


	public LendingRepo build() {
		LendingRepoDefinition definition = new LendingRepoDefinition();
		definition.setCounterparty(this.counterparty);
		definition.setClientInvestmentAccount(this.clientInvestmentAccount);
		definition.setHoldingInvestmentAccount(this.holdingInvestmentAccount);
		definition.setRepoInvestmentAccount(this.repoInvestmentAccount);
		definition.setTriPartyInvestmentAccount(this.triPartyInvestmentAccount);
		definition.setInvestmentSecurity(this.investmentSecurity);
		definition.setInstrumentHierarchy(this.instrumentHierarchy); // NOTE: do we need this field or is it redundant to investmentSecurity?

		definition.setReverse(this.reverse);
		definition.setActive(this.active);
		definition.setDayCountConvention(this.dayCountConvention);

		//////////////////////////////
		LendingRepo repo = new LendingRepo();
		repo.setType(this.type);
		repo.setRepoDefinition(definition);
		repo.setTraderUser(this.traderUser);

		repo.setTerm(this.term);
		repo.setInterestRate(this.interestRate);
		repo.setDescription(this.description);

		repo.setPrice(this.price);
		repo.setHaircutPercent(this.haircutPercent);

		repo.setMarketValue(this.marketValue);
		repo.setNetCash(this.netCash);
		repo.setInterestAmount(this.interestAmount);
		repo.setExchangeRateToBase(this.exchangeRateToBase);
		repo.setIndexRatio(this.indexRatio);

		repo.setQuantityIntended(this.quantityIntended);
		repo.setOriginalFace(this.originalFace);

		repo.setTradeDate(this.tradeDate);
		repo.setSettlementDate(this.settlementDate);
		repo.setMaturityDate(this.maturityDate);
		repo.setMaturitySettlementDate(this.maturitySettlementDate);

		if (this.roll) {
			LendingRepo parentLendingRepo = new LendingRepo();
			// make a separate copy of the repo definition so changes to dayCountConvention will only be applied to the parent.
			LendingRepoDefinition parentLendingRepoDefinition = new LendingRepoDefinition();
			BeanUtils.copyProperties(repo, parentLendingRepo);
			BeanUtils.copyProperties(repo.getRepoDefinition(), parentLendingRepoDefinition);

			parentLendingRepo.setRepoDefinition(parentLendingRepoDefinition);
			parentLendingRepo.setParentIdentifier(null);
			if (parentLendingRepo.getRepoDefinition().getDayCountConvention() == null) {
				parentLendingRepo.getRepoDefinition().setDayCountConvention(DayCountConventions.ACTUAL_THREESIXTY.getAccrualString());
			}
			parentLendingRepo = this.lendingRepoService.saveLendingRepo(parentLendingRepo);
			repo.setParentIdentifier(parentLendingRepo.getId());
		}

		return repo;
	}


	public LendingRepo buildAndSave() {
		LendingRepo repo = build();
		repo = this.lendingRepoService.saveLendingRepo(repo);

		return repo;
	}


	public LendingRepoBuilder type(LendingRepoType type) {
		this.type = type;
		return this;
	}


	public LendingRepoBuilder traderUser(SecurityUser traderUser) {
		this.traderUser = traderUser;
		return this;
	}


	public LendingRepoBuilder term(int term) {
		this.term = term;
		return this;
	}


	public LendingRepoBuilder interestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
		return this;
	}


	public LendingRepoBuilder description(String description) {
		this.description = description;
		return this;
	}


	public LendingRepoBuilder price(BigDecimal price) {
		this.price = price;
		return this;
	}


	public LendingRepoBuilder haircutPercent(BigDecimal haircutPercent) {
		this.haircutPercent = haircutPercent;
		return this;
	}


	public LendingRepoBuilder marketValue(BigDecimal marketValue) {
		this.marketValue = marketValue;
		return this;
	}


	public LendingRepoBuilder netCash(BigDecimal netCash) {
		this.netCash = netCash;
		return this;
	}


	public LendingRepoBuilder interestAmount(BigDecimal interestAmount) {
		this.interestAmount = interestAmount;
		return this;
	}


	public LendingRepoBuilder exchangeRateToBase(BigDecimal exchangeRateToBase) {
		this.exchangeRateToBase = exchangeRateToBase;
		return this;
	}


	public LendingRepoBuilder indexRatio(BigDecimal indexRatio) {
		this.indexRatio = indexRatio;
		return this;
	}


	public LendingRepoBuilder quantityIntended(BigDecimal quantityIntended) {
		this.quantityIntended = quantityIntended;
		return this;
	}


	public LendingRepoBuilder originalFace(BigDecimal originalFace) {
		this.originalFace = originalFace;
		return this;
	}


	public LendingRepoBuilder tradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
		return this;
	}


	public LendingRepoBuilder settlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
		return this;
	}


	public LendingRepoBuilder maturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
		return this;
	}


	public LendingRepoBuilder maturitySettlementDate(Date maturitySettlementDate) {
		this.maturitySettlementDate = maturitySettlementDate;
		return this;
	}


	public LendingRepoBuilder roll(boolean roll) {
		this.roll = roll;
		return this;
	}


	public LendingRepoBuilder counterparty(BusinessCompany counterparty) {
		this.counterparty = counterparty;
		return this;
	}


	public LendingRepoBuilder clientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
		return this;
	}


	public LendingRepoBuilder holdingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
		return this;
	}


	public LendingRepoBuilder repoInvestmentAccount(InvestmentAccount repoInvestmentAccount) {
		this.repoInvestmentAccount = repoInvestmentAccount;
		return this;
	}


	public LendingRepoBuilder triPartyInvestmentAccount(InvestmentAccount triPartyInvestmentAccount) {
		this.triPartyInvestmentAccount = triPartyInvestmentAccount;
		return this;
	}


	public LendingRepoBuilder investmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
		return this;
	}


	public LendingRepoBuilder instrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
		return this;
	}


	public LendingRepoBuilder reverse(boolean reverse) {
		this.reverse = reverse;
		return this;
	}


	public LendingRepoBuilder active(boolean active) {
		this.active = active;
		return this;
	}


	public LendingRepoBuilder dayCountConvention(String dayCountConvention) {
		this.dayCountConvention = dayCountConvention;
		return this;
	}
}
