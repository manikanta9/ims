package com.clifton.ims.tests.rule.billing.invoice;


import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.billingbasis.search.BillingBasisSnapshotSearchForm;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.ims.tests.system.query.JdbcUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.rule.violation.RuleViolation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


public class BillingInvoiceRuleTests extends RuleTests {

	@Resource
	private BillingBasisService billingBasisService;

	@Resource
	private BillingInvoiceService billingInvoiceService;

	@Resource
	private BillingDefinitionService billingDefinitionService;

	@Resource
	private InvestmentAccountService investmentAccountService;


	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Billing Invoice Management Fee Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		Set<String> excludedTestMethodSet = new HashSet<>();
		//These tests are/will be handled in BillingInvoiceProcessTests
		excludedTestMethodSet.add("testBillingDefinitionAccountNoBillingBasisRule");
		excludedTestMethodSet.add("testBillingDefinitionAccountZeroBillingBasisRule");
		excludedTestMethodSet.add("testBillingBasisMissingSourceDataRule");
		excludedTestMethodSet.add("testBillingBasisMissingMainPortfolioRunOnDateRule");
		excludedTestMethodSet.add("testBillingBasisMissingManagerBalanceOnDateRule");
		excludedTestMethodSet.add("testBillingScheduleWaivedRule");
		excludedTestMethodSet.add("testBillingScheduleProcessingRule");
		excludedTestMethodSet.add("testBillingBasisGreaterThanMaxTierRule");
		excludedTestMethodSet.add("testBillingInvoiceTotalDoesNotMatchAccountAllocationRule");
		excludedTestMethodSet.add("testBillingBasisSnapshotSecurityExcludedRule");
		return excludedTestMethodSet;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBillingDefinitionNotActiveDuringInvoicePeriodRule() {
		BillingInvoiceSearchForm billingInvoiceSearchForm = new BillingInvoiceSearchForm();
		billingInvoiceSearchForm.setCompanyName("The Progress Common Trust");
		billingInvoiceSearchForm.setInvoiceDate(DateUtils.toDate("12/31/2013"));
		BillingInvoice billingInvoice = CollectionUtils.getFirstElementStrict(this.billingInvoiceService.getBillingInvoiceList(billingInvoiceSearchForm));
		//Load the full billingDefinition
		this.workflowTransitioner.transitionIfPresent("Re-Activate Definition", "BillingDefinition", BeanUtils.getIdentityAsLong(billingInvoice.getBillingDefinition()));
		this.workflowTransitioner.transitionIfPresent("Return for Edits", "BillingDefinition", BeanUtils.getIdentityAsLong(billingInvoice.getBillingDefinition()));
		BillingDefinition billingDefinition = this.billingDefinitionService.getBillingDefinition(billingInvoice.getBillingDefinition().getId());
		//Save the existing endDate so it can be reset.
		Date billingDefinitionEndDate = billingDefinition.getEndDate();
		billingDefinition.setEndDate(DateUtils.addDays(billingDefinition.getStartDate(), 1));
		this.billingDefinitionService.saveBillingDefinition(billingDefinition);
		try {
			validateViolationExists(billingInvoice.getId(), "Billing Definition [184: The Progress Common Trust- IMA (" +
					DateUtils.fromDateShort(billingDefinition.getStartDate()) + " - " +
					DateUtils.fromDateShort(DateUtils.addDays(billingDefinition.getStartDate(), 1))
					+ ")] is not active during Invoice Period [10/01/2013 - 12/31/2013].");
		}
		finally {
			billingDefinition.setEndDate(billingDefinitionEndDate);
			this.billingDefinitionService.saveBillingDefinition(billingDefinition);
		}
	}


	@Test
	public void testBillingBasisMissingFinalAUMValueOnDateRule() {
		validateViolationDoesNotExist(9211, "Billing Basis: Missing Final AUM Value on Date");

		// For testing, need to move positions on date for the account earlier so the violation will trigger, then set it back
		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber("222525");
		Date originalPositionsOnDate = clientAccount.getInceptionDate();
		try {
			clientAccount.setInceptionDate(DateUtils.toDate("10/15/2015"));
			this.investmentAccountService.saveInvestmentAccount(clientAccount);
			validateViolationExists(8187, "Final AUM value missing for Client Account account 222525: ERP of Duke University-Direct China A-H Share Arbitrage Futures on: 10/31/2015");
		}
		finally {
			clientAccount.setInceptionDate(originalPositionsOnDate);
			this.investmentAccountService.saveInvestmentAccount(clientAccount);
		}
	}


	@Test
	public void testBillingBasisManagerBalanceValueOnDateWhenPositionsOffRule() {
		// There isn't a real case of this, so we are going to make it look like there is to confirm the logic of the test
		// Invoice # 12556 Billing Basis record for Manager M17833 on 6/19/2017 - positions are off for account 502122 and balance is zero
		// Confirm no violation
		int invoiceId = 12556;
		validateViolationDoesNotExist(invoiceId, "Billing Basis: Manager Balance Value On Date When Positions Off");

		// Set value on the billing basis snapshot
		BillingBasisSnapshotSearchForm searchForm = new BillingBasisSnapshotSearchForm();
		searchForm.setBillingInvoiceId(invoiceId);
		searchForm.setSnapshotDate(DateUtils.toDate("06/19/2017"));
		searchForm.setInvestmentManagerAccountId(17833);
		BillingBasisSnapshot snapshot = CollectionUtils.getFirstElementStrict(this.billingBasisService.getBillingBasisSnapshotList(searchForm));
		try {
			JdbcUtils.executeQuery("UPDATE BillingBasisSnapshot SET SnapshotValue = 2000 WHERE BillingBasisSnapshotID = '" + snapshot.getId() + "';");
			validateViolationExists(invoiceId, "Billing basis value present on 06/19/2017 for 502122: Pepperdine University-Options Hedging Manager Balance - Period Average [M17833: Option Billing Exposure] (Fee Schedule) but the account has Positions Off.");
		}
		finally {
			// Clear value on the billing basis snapshot
			JdbcUtils.executeQuery("UPDATE BillingBasisSnapshot SET SnapshotValue = 0 WHERE BillingBasisSnapshotID = '" + snapshot.getId() + "';");
		}
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////             Test Assertion Methods              ////////////////
	/////////////////////////////////////////////////////////////////////////////


	private void validateViolationExists(int billingInvoiceId, String expectedViolationNote) {
		boolean foundExpectedViolationNote = false;
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(this.ruleEvaluatorService.previewRuleViolations("BillingInvoice", billingInvoiceId))) {
			if (expectedViolationNote.equals(ruleViolation.getViolationNote())) {
				foundExpectedViolationNote = true;
			}
		}
		Assertions.assertTrue(foundExpectedViolationNote);
	}


	private void validateViolationDoesNotExist(int billingInvoiceId, String definitionName) {
		for (RuleViolation ruleViolation : CollectionUtils.getIterable(this.ruleEvaluatorService.previewRuleViolations("BillingInvoice", billingInvoiceId))) {
			if (StringUtils.isEqual("Passed", ruleViolation.getIgnoreNote())) {
				continue;
			}
			Assertions.assertFalse(StringUtils.isEqual(definitionName, ruleViolation.getRuleAssignment().getRuleDefinition().getName()), "Did not expect to find any violations for rule definition [" + definitionName + "] but found at least 1: " + ruleViolation.getViolationNote());
		}
	}
}
