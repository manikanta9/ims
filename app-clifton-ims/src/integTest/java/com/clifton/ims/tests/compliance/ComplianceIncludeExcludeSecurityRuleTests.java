package com.clifton.ims.tests.compliance;


import com.clifton.business.company.BusinessCompany;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleSearchForm;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.evaluator.impl.IncludeExcludeSecurityRule;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.investment.setup.group.search.InvestmentSecurityGroupSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.trade.TradeType;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class ComplianceIncludeExcludeSecurityRuleTests extends ComplianceRealTimeTradeTests {

	@Resource
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	@Resource
	private ComplianceRuleService complianceRuleService;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private InvestmentAccountService investmentAccountService;

	private static final Short SELL_RESTRICTION_ID = 2;
	private static final Short BUY_RESTRICTION_ID = 3;


	@Test
	public void testBasicExcludeRule() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUNDS, true, "Prohibited Security (Specific): Parametric Currency Fund (Funds - ETF & ETN)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "EEM", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Prohibited Security (Specific): Parametric Currency Fund (Funds - ETF & ETN) : EEM (iShares MSCI Emerging Markets ETF) is contained in exclusion group ETF and ETN : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "ANWPX", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testBasicIncludeRule() {
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.FUNDS, true, "Permitted Security (Specific): PBR (Funds - ETF)");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "AUNZ", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		//Should pass, rule only applies to ETF scope
		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "ANWPX", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.FUNDS, "EWT", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Permitted Security (Specific): PBR (Funds - ETF) : EWT (iShares MSCI Taiwan ETF) is not included in group Approved ETF's for the PBR Fund : Failed");
	}


	@Test
	public void testExcludeActualSecurity() {

		setup("Test Exclusion Group Actual", "false", "ADS GR", "Exclude Rule Evaluator Bean Actual", "Exclude Rule Actual", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, null);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "Exclude Rule Actual");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Exclude Rule Actual : ADS (adidas AG) is contained in exclusion group Test Exclusion Group Actual : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testIncludeActualSecurity() {

		setup("Test Inclusion Group Actual", "true", "ADS GR", "Inclusion Rule Evaluator Bean Actual", "Include Rule Actual", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, null);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "Include Rule Actual");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Include Rule Actual : ADDYY (adidas AG) is not included in group Test Inclusion Group Actual : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testExcludingUnderlyingSecurity() {

		setup("Test Exclusion Group Underlying", "false", "ADS GR", "Rule Evaluator Bean", "Exclude Rule Underlying", IncludeExcludeSecurityRule.SecuritySelectionOptions.UNDERLYING_SECURITY, null);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "Exclude Rule Underlying");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Exclude Rule Underlying : ADDYY (adidas AG) is contained in exclusion group Test Exclusion Group Underlying : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testIncludingUnderlyingSecurity() {

		setup("Test Inclusion Group Underlying", "true", "ADS GR", "Inclusion Rule Evaluator Bean", "Include Rule Underlying", IncludeExcludeSecurityRule.SecuritySelectionOptions.UNDERLYING_SECURITY, null);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "Include Rule Underlying");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Include Rule Underlying : AAPL (Apple Inc) is not included in group Test Inclusion Group Underlying : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testIncludingBothSecurities() {

		setup("Test Inclusion Group Both", "true", "ADS GR", "Inclusion Rule Evaluator Bean Both", "Include Rule Both", IncludeExcludeSecurityRule.SecuritySelectionOptions.BOTH, null);

		// Both should fail because if either the underlying or the primary is in the group, it should be excluded.
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "Include Rule Both");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Include Rule Both : AAPL (Apple Inc) is not included in group Test Inclusion Group Both : Failed");
	}


	@Test
	public void testExcludingBothSecurities() {

		setup("Test Exclusion Group Both", "false", "ADS GR", "Exclusion Rule Evaluator Bean Both", "Exclude Rule Both", IncludeExcludeSecurityRule.SecuritySelectionOptions.BOTH, null);

		// Both should fail because if either the underlying or the primary is in the group, it should be excluded.
		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "Exclude Rule Both");
		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Exclude Rule Both : ADS (adidas AG) is contained in exclusion group Test Exclusion Group Both : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, "Exclude Rule Both : ADDYY (adidas AG) is contained in exclusion group Test Exclusion Group Both : Failed");

		tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "AAPL", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testTradeRestrictionNotAllowed_buy() {
		setup("Test Group No Buy", "false", "ADS GR", "No Buy Evaluator Bean", "No Buy Rule", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, BUY_RESTRICTION_ID);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "No Buy Rule");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, false, true, "is not allowed because it fails the Restriction of type [No Buy]. : Failed");
		validateViolationNotes(tradeResults, false, true, "is not allowed because it fails the Restriction of type [No Buy]. : Failed");
	}


	@Test
	public void testTradeRestrictionNotAllowed_buyInclude() {
		setup("Test Include Group No Buy", "true", "ADS GR", "No Buy Include Evaluator Bean", "No Buy Include Rule", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, BUY_RESTRICTION_ID);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "No Buy Include Rule");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults, false, true, "is not allowed because it fails the Restriction of type [No Buy]. : Failed");
	}


	@Test
	public void testTradeRestrictionNotAllowed_sell() {
		setup("Test Group No Sell", "false", "ADS GR", "No Sell Evaluator Bean", "No Sell Rule", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, SELL_RESTRICTION_ID);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "No Sell Rule");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults, false, true, "is not allowed because it fails the Restriction of type [No Sell]. : Failed");
	}


	@Test
	public void testTradeRestrictionAllowed_buy() {
		setup("Test Group No Buy", "false", "ADS GR", "No Buy Evaluator Bean", "No Buy Rule", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, BUY_RESTRICTION_ID);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "No Buy Rule");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testTradeRestrictionAllowed_buyInclude() {
		setup("Test Include Group No Buy", "true", "ADS GR", "No Buy Include Evaluator Bean", "No Buy Include Rule", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, BUY_RESTRICTION_ID);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "No Buy Include Rule");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADS GR", new BigDecimal(123), false, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testTradeRestrictionAllowed_sell() {
		setup("Test Group No Sell", "false", "ADS GR", "No Sell Evaluator Bean", "No Sell Rule", IncludeExcludeSecurityRule.SecuritySelectionOptions.ACTUAL_SECURITY, SELL_RESTRICTION_ID);

		AccountInfo accountInfo = this.complianceUtils.createAccountInfoAndAssignRules(TradeType.STOCKS, true, "No Sell Rule");

		List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.STOCKS, "ADDYY", new BigDecimal(123), true, new Date());
		validateViolationNotes(tradeResults);
	}


	@Test
	public void testExcludingWithConstituents() {

		Date evalDate = DateUtils.toDate("03/08/2021");

		setup("Test Exclusion Group Constituents", "false", "007GB6_SUBLT2_USD", "Exclude Rule Evaluator Bean Constituents", "Exclude Rule Constituents", IncludeExcludeSecurityRule.SecuritySelectionOptions.ALL, null);

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber("187071");
		InvestmentAccount holdingAccount = this.investmentAccountService.getInvestmentAccountByNumber("LTP-CTNA");

		List<ComplianceRuleAssignment> assignmentList = this.complianceUtils.assignRulesToAccountByNames(clientAccount, "Exclude Rule Constituents");
		try {
			AccountInfo accountInfo = new AccountInfo();
			accountInfo.setClientAccount(clientAccount);
			accountInfo.setHoldingAccount(holdingAccount);
			accountInfo.setSwapsHoldingAccount(holdingAccount);

			BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.CITIBANK_NA);

			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.CREDIT_DEFAULT_SWAPS, "CA1120246286AB", new BigDecimal(123), true, evalDate, executingBroker);
			validateViolationNotes(tradeResults, false, true, "is contained in exclusion group Test Exclusion Group Constituents : Failed");

			tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.CREDIT_DEFAULT_SWAPS, "CA1114707380", new BigDecimal(123), true, evalDate, executingBroker);

			validateViolationNotes(tradeResults);
		}
		finally {
			this.complianceUtils.removeComplianceRuleAssignment(CollectionUtils.getOnlyElement(assignmentList));
		}
	}


	@Test
	public void testIncludingWithConstituents() {
		Date evalDate = DateUtils.toDate("03/08/2021");

		setup("Test Inclusion Group Constituents", "true", "007GB6_SUBLT2_USD", "Include Rule Evaluator Bean Constituents", "Include Rule Constituents", IncludeExcludeSecurityRule.SecuritySelectionOptions.ALL, null);

		InvestmentAccount clientAccount = this.investmentAccountService.getInvestmentAccountByNumber("187071");
		InvestmentAccount holdingAccount = this.investmentAccountService.getInvestmentAccountByNumber("LTP-CTNA");

		List<ComplianceRuleAssignment> assignmentList = this.complianceUtils.assignRulesToAccountByNames(clientAccount, "Include Rule Constituents");
		try {
			AccountInfo accountInfo = new AccountInfo();
			accountInfo.setClientAccount(clientAccount);
			accountInfo.setHoldingAccount(holdingAccount);
			accountInfo.setSwapsHoldingAccount(holdingAccount);

			BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.CITIBANK_NA);

			List<RuleViolation> tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.CREDIT_DEFAULT_SWAPS, "CA1114707380", new BigDecimal(123), true, evalDate, executingBroker);
			validateViolationNotes(tradeResults, "Include Rule Constituents : CA1114707380 (LTP - BESPOKE-HONOLULU CITI 6/21 30-100%) [INACTIVE] is not included in group Test Inclusion Group Constituents : Failed");

			tradeResults = this.complianceUtils.executeRulesAgainstTrade(accountInfo, TradeType.CREDIT_DEFAULT_SWAPS, "CA1120246286AB", new BigDecimal(123), true, evalDate, executingBroker);

			validateViolationNotes(tradeResults);
		}
		finally {
			this.complianceUtils.removeComplianceRuleAssignment(CollectionUtils.getOnlyElement(assignmentList));
		}
	}


	private void setup(String groupName, String include, String securitySymbol, String beanName, String ruleName, IncludeExcludeSecurityRule.SecuritySelectionOptions option, Short tradeRestrictionTypeId) {

		InvestmentSecurityGroupSearchForm securityGroupSearchFrom = new InvestmentSecurityGroupSearchForm();
		securityGroupSearchFrom.setName(groupName);
		List<InvestmentSecurityGroup> groups = this.investmentSecurityGroupService.getInvestmentSecurityGroupList(securityGroupSearchFrom);
		InvestmentSecurityGroup group;

		if (!CollectionUtils.isEmpty(groups)) {
			group = CollectionUtils.getOnlyElement(groups);
		}
		else {
			group = new InvestmentSecurityGroup();
			group.setName(groupName);
			this.investmentSecurityGroupService.saveInvestmentSecurityGroup(group);
			InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(securitySymbol);
			this.investmentSecurityGroupService.linkInvestmentSecurityGroupToSecurity(group.getId(), security.getId());
		}

		SystemBeanSearchForm sf = new SystemBeanSearchForm();
		sf.setName(beanName);
		List<SystemBean> beans = this.systemBeanService.getSystemBeanList(sf);

		if (CollectionUtils.isEmpty(beans)) {
			SystemBeanBuilder beanBuilder = SystemBeanBuilder.newBuilder(beanName, "Include Exclude Security Evaluator", this.systemBeanService)
					.addProperty("Security Selection Option", option.toString(), option.getLabel())
					.addProperty("Security Group", group.getId().toString(), group.getName())
					.addProperty("Inclusion", include, include);

			if (tradeRestrictionTypeId != null) {
				beanBuilder = beanBuilder.addProperty("Trade Restriction Type", tradeRestrictionTypeId.toString());
			}

			SystemBean ruleEvaluatorBean = beanBuilder.build();

			ComplianceRuleSearchForm crsf = new ComplianceRuleSearchForm();
			crsf.setName(ruleName);
			List<ComplianceRule> rules = this.complianceRuleService.getComplianceRuleList(crsf);

			if (CollectionUtils.isEmpty(rules)) {
				ComplianceRule rule = new ComplianceRule();
				rule.setRuleType(this.complianceRuleService.getComplianceRuleTypeByName("Include Exclude Security Rule"));
				rule.setName(ruleName);
				rule.setDescription("Test Rule");
				rule.setRuleEvaluatorBean(ruleEvaluatorBean);
				rule.setRealTime(true);

				this.complianceRuleService.saveComplianceRule(rule);
			}
		}
	}
}
