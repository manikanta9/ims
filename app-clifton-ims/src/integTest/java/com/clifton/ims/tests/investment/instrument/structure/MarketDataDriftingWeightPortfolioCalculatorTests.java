package com.clifton.ims.tests.investment.instrument.structure;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.structure.InvestmentInstrumentSecurityStructure;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureTypes;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureWeight;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.instrument.structure.MarketDataDriftingWeightPortfolioCalculator;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author manderson
 */
public class MarketDataDriftingWeightPortfolioCalculatorTests extends BaseImsIntegrationTest {


	@Resource
	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private MarketDataSourceService marketDataSourceService;

	@Resource
	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testModelPortfolio_DriftingWeightsCalculation() {
		// Create a New Instrument
		InvestmentInstrument investmentInstrument = createModelPortfolioInstrument("SARP TEST");
		// Create a New Instrument Structure
		InvestmentInstrumentStructure instrumentStructure = new InvestmentInstrumentStructure();
		instrumentStructure.setInstrument(investmentInstrument);
		instrumentStructure.setInstrumentStructureType(InvestmentInstrumentStructureTypes.MODEL_PORTFOLIO);
		instrumentStructure.setStartDate(DateUtils.toDate("06/04/2018"));
		instrumentStructure.setAdditionalMultiplier(BigDecimal.ONE);
		instrumentStructure.setCurrentWeightCalculatorBean(this.systemBeanService.getSystemBeanByName("Drifting Weight Calculator"));
		instrumentStructure.setInstrumentStructureWeightList(new ArrayList<>());
		instrumentStructure.getInstrumentStructureWeightList().add(createInvestmentInstrumentStructureWeight("LXM17", 10, 50));
		instrumentStructure.getInstrumentStructureWeightList().add(createInvestmentInstrumentStructureWeight("XPM17", 20, -50));
		this.investmentInstrumentStructureService.saveInvestmentInstrumentStructure(instrumentStructure);

		// Create New Security
		InvestmentSecurity investmentSecurity = createInvestmentSecurity(investmentInstrument, instrumentStructure.getStartDate(), "10000", "Goldman Sachs");

		// Create New Security Structure - Just need to populate the current security calculators
		InvestmentInstrumentSecurityStructure securityStructure = this.investmentInstrumentStructureService.getInvestmentInstrumentSecurityStructure(instrumentStructure.getId(), investmentSecurity.getId());
		for (InvestmentSecurityStructureWeight securityStructureWeight : securityStructure.getSecurityStructureWeightList()) {
			if (StringUtils.isEqual("LX", securityStructureWeight.getInstrumentWeight().getInstrument().getIdentifierPrefix())) {
				securityStructureWeight.setCurrentSecurityCalculatorBean(this.systemBeanService.getSystemBeanByName("Early (-3BD) - Target Current Opening Difference: H-J-K-M-N-Q-U-V-X-Z-F-G Year Switch: V"));
			}
			else {
				securityStructureWeight.setCurrentSecurityCalculatorBean(this.systemBeanService.getSystemBeanByName("Default Futures Roll Schedule Current Security Calculator"));
			}
		}
		this.investmentInstrumentStructureService.saveInvestmentInstrumentSecurityStructure(securityStructure);

		// For the First 3 Weeks
		this.investmentInstrumentStructureService.rebuildInvestmentSecurityStructureAllocation(instrumentStructure.getId(), investmentSecurity.getId(), investmentSecurity.getStartDate(), DateUtils.toDate("06/25/2018"), true);

		// Validate Various Days Values During that Period:

		// Day 0: 06/04/2018
		List<InvestmentSecurityStructureAllocation> allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/04/2018"), false);
		validateAllocationList("LXQ18\t5,000.00\t1\t5,000.00\t0.00\t0.00\t50\t\r\n" +
				"XPM18\t-6,539.79\t0.76455\t-5,000.00\t0.00\t0.00\t-50\t", allocationList);


		// Day 1: 06/05/2018
		allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/05/2018"), false);
		validateAllocationList("LXQ18\t5,118.36\t1\t5,118.36\t118.36\t0.00\t50.47786\t\r\n" +
				"XPM18\t-6,511.57\t0.76035\t-4,951.07\t21.45\t0.00\t-48.82805", allocationList);

		// Day 2: 06/06/2018
		allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/06/2018"), false);
		validateAllocationList("LXQ18\t5,107.96\t1\t5,107.96\t107.96\t0.00\t50.52572\t\r\n" +
				"XPM18\t-6,537.62\t0.76675\t-5,012.72\t1.67\t0.00\t-49.58359\t", allocationList);

		// Day 3: 06/07/2018
		allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/07/2018"), false);
		validateAllocationList("LXQ18\t5,094.36\t1\t5,094.36\t94.36\t0.00\t50.61712\t\r\n" +
				"XPM18\t-6,578.86\t0.76415\t-5,027.24\t-29.86\t0.00\t-49.95013\t", allocationList);

		// Monday After weekend 06/11/2018
		allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/11/2018"), false);
		validateAllocationList("LXQ18\t5,126.35\t1\t5,126.35\t126.35\t0.00\t50.72733\t\r\n" +
				"XPM18\t-6,566.93\t0.76115\t-4,998.42\t-20.65\t0.00\t-49.46137\t", allocationList);

		// Day Of XP Roll: 06/18/2018
		allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/18/2018"), false);
		validateAllocationList("LXQ18\t4,907.23\t1\t4,907.23\t-92.77\t0.00\t49.90929\t\r\n" +
				"XPM18\t-6,640.72\t0.74245\t-4,930.40\t-74.94\t0.00\t-50.145\t\n", allocationList);

		// Day of New XP Security: 06/19/2018
		allocationList = this.investmentInstrumentStructureService.getInvestmentSecurityStructureAllocationListForSecurityAndDate(instrumentStructure.getId(), investmentSecurity.getId(), DateUtils.toDate("06/19/2018"), false);
		validateAllocationList("LXQ18\t4,801.66\t1\t4,801.66\t-198.34\t0.00\t49.32065\t\r\n" +
				"XPU18\t-6,628.67\t0.73675\t-4,883.67\t8.88\t-74.94\t-50.16298\t\n", allocationList);
	}


	private void validateAllocationList(String expected, List<InvestmentSecurityStructureAllocation> allocationList) {
		StringBuilder allocationListString = new StringBuilder(50);
		// Should already be sorted in proper order
		for (InvestmentSecurityStructureAllocation allocation : CollectionUtils.getIterable(allocationList)) {
			allocationListString.append(allocation.getCurrentSecurity().getSymbol()).append(StringUtils.TAB);
			allocationListString.append(CoreMathUtils.formatNumberMoney(allocation.getAllocationValueLocal())).append(StringUtils.TAB);
			allocationListString.append(CoreMathUtils.formatNumberDecimal(allocation.getFxRateToBase())).append(StringUtils.TAB);
			allocationListString.append(CoreMathUtils.formatNumberMoney(allocation.getAllocationValueBase())).append(StringUtils.TAB);
			allocationListString.append(CoreMathUtils.formatNumberMoney(allocation.getAdditionalAmount1())).append(StringUtils.TAB);
			allocationListString.append(CoreMathUtils.formatNumberMoney(allocation.getAdditionalAmount2())).append(StringUtils.TAB);
			allocationListString.append(CoreMathUtils.formatNumberDecimal(allocation.getAllocationWeight())).append(StringUtils.TAB);
			allocationListString.append(StringUtils.NEW_LINE);
		}
		//System.out.println(allocationListString);
		Assertions.assertEquals(expected.trim(), allocationListString.toString().trim());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private InvestmentInstrument createModelPortfolioInstrument(String namePrefix) {
		InvestmentInstrument investmentInstrument = new InvestmentInstrument();

		InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
		searchForm.setInvestmentTypeName(InvestmentType.BENCHMARKS);

		searchForm.setSearchPattern("Model Portfolio");
		searchForm.setAssignmentAllowed(true);
		InvestmentInstrumentHierarchy hierarchy = CollectionUtils.getFirstElementStrict(this.investmentSetupService.getInvestmentInstrumentHierarchyList(searchForm));

		investmentInstrument.setHierarchy(hierarchy);
		investmentInstrument.setName(namePrefix + " - " + UUID.randomUUID());
		investmentInstrument.setIdentifierPrefix(investmentInstrument.getName());
		investmentInstrument.setTradingCurrency(this.usd);

		this.investmentInstrumentService.saveInvestmentInstrument(investmentInstrument);
		return investmentInstrument;
	}


	private InvestmentInstrumentStructureWeight createInvestmentInstrumentStructureWeight(String symbol, int order, double weight) {
		InvestmentInstrumentStructureWeight instrumentWeight = new InvestmentInstrumentStructureWeight();
		// Note: using example security to get the instrument
		instrumentWeight.setInstrument(this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol).getInstrument());
		instrumentWeight.setOrder(order);
		instrumentWeight.setWeight(BigDecimal.valueOf(weight));
		return instrumentWeight;
	}


	private InvestmentSecurity createInvestmentSecurity(InvestmentInstrument investmentInstrument, Date startDate, String initialStructureValue, String datasourceName) {
		// Create a New Security
		InvestmentSecurity security = new InvestmentSecurity();
		security.setInstrument(investmentInstrument);
		security.setSymbol(investmentInstrument.getIdentifierPrefix());
		security.setName(investmentInstrument.getName());
		security.setStartDate(startDate);

		SystemColumnCustomConfig customConfig = new SystemColumnCustomConfig();
		customConfig.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		customConfig.setLinkedValue(investmentInstrument.getHierarchy().getId() + "");
		customConfig = this.systemColumnValueService.getSystemColumnCustomConfig(customConfig);

		List<SystemColumnValue> columnValueList = new ArrayList<>();
		for (SystemColumnCustom columnCustom : customConfig.getColumnList()) {
			if (StringUtils.isEqual(MarketDataDriftingWeightPortfolioCalculator.SECURITY_CUSTOM_COLUMN_INITIAL_STRUCTURE_VALUE, columnCustom.getName())) {
				SystemColumnValue columnValue = new SystemColumnValue();
				columnValue.setColumn(columnCustom);
				columnValue.setValue(initialStructureValue);
				columnValue.setText(initialStructureValue);
				columnValueList.add(columnValue);
			}
			if (StringUtils.isEqual(MarketDataDriftingWeightPortfolioCalculator.SECURITY_CUSTOM_COLUMN_FX_SOURCE, columnCustom.getName())) {
				SystemColumnValue columnValue = new SystemColumnValue();
				columnValue.setColumn(columnCustom);
				MarketDataSource dataSource = this.marketDataSourceService.getMarketDataSourceByName(datasourceName);
				columnValue.setValue(dataSource.getId() + "");
				columnValue.setText(datasourceName);
				columnValueList.add(columnValue);
			}
		}
		security.setColumnValueList(columnValueList);
		security.setColumnGroupName(customConfig.getColumnGroupName());

		return this.investmentInstrumentService.saveInvestmentSecurity(security);
	}
}
