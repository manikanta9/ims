package com.clifton.ims.tests.trade.order;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.fix.fixsim.model.SessionInfo;
import com.clifton.fix.session.FixSession;
import com.clifton.fix.session.FixSessionService;
import com.clifton.fix.session.search.FixSessionSearchForm;
import com.clifton.fix.util.FixSimApiHandler;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;


/**
 * A class that provides instance methods which aid in the construction of automated tests that utilize FixSim sessions amd the FixSim API.
 *
 * @author davidi
 */

@Component
public class FixSimHelper {

	@Value("${fix.fixSimApiKey}")
	private String fixSimApiKey;
	private FixSimApiHandler fixSimApiHandler;

	@Resource
	FixSessionService fixSessionService;

	////////////////////////////////////////////////////////////////////////////


	@PostConstruct
	public void Init() {
		this.fixSimApiHandler = new FixSimApiHandler();
		this.fixSimApiHandler.setApiKey(this.fixSimApiKey);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sends a FIX Message via the API to a FixSimSession.  FixSim will replay the uploded messages to the fix engine via the specified test session.
	 */
	public void uploadMessageToFixSim(String message, String fixSimInstance, String fixSimSessionName) {
		uploadMessagesToFixSim(CollectionUtils.createList(message), fixSimInstance, fixSimSessionName);
	}


	/**
	 * Sends a sequence of FIX Messages via the API to a FixSimSession.  FixSim will replay the uploded messages to the fix engine via the specified test session.
	 */
	public void uploadMessagesToFixSim(List<String> messageList, String fixSimInstance, String fixSimSessionName) {
		getFixSimApiHandler().sendRawMessages(messageList, fixSimInstance, fixSimSessionName);
	}


	/**
	 * This resets the FixSimSession.  For best results, reset the FixSim session before doing a test. Reusing the same
	 * FixSim session for multiple tests without resetting it has caused some tests to fail.
	 * <p>
	 * returns:  true if sequence numbers were re-synced and a recovery period is required
	 * false if sequence numbers were not adjusted.
	 */
	public boolean resetFixSimSession(String fixSimInstance, String fixSimSessionName, String fixSessionSuffix, int sequenceNumberDifferenceTolerance) {
		getFixSimApiHandler().deleteFixSimOrdersInForSession(fixSimInstance, fixSimSessionName);
		getFixSimApiHandler().deleteFixSimOrdersOutForSession(fixSimInstance, fixSimSessionName);

		String localFixSessionName = getSessionNameWithSuffix(fixSimSessionName, fixSessionSuffix);
		SessionInfo sessionInfo = getFixSimApiHandler().getFixSimSession(fixSimInstance, fixSimSessionName);
		Assertions.assertNotNull(sessionInfo, "Cannot attain session information from FixSim for session: " + fixSimSessionName);
		FixSessionSearchForm fixSessionSearchForm = new FixSessionSearchForm();
		fixSessionSearchForm.setDefinitionName(localFixSessionName);
		FixSession fixSession = CollectionUtils.getOnlyElementStrict(this.fixSessionService.getFixSessionList(fixSessionSearchForm, true, true));
		boolean isRequiresRecoveryPeriod = false;
		Integer localIncomingSeq = fixSession.getLiveIncomingSequenceNumber() != null ? fixSession.getLiveIncomingSequenceNumber() : fixSession.getIncomingSequenceNumber();
		Integer localOutgoingSeq = fixSession.getLiveOutgoingSequenceNumber() != null ? fixSession.getLiveOutgoingSequenceNumber() : fixSession.getOutgoingSequenceNumber();
		Integer fixSimOutgoingSeq = sessionInfo.getSendingSeqNum();
		Integer fixSimIncomingSeq = sessionInfo.getRcvdSeqNum();
		if (fixSimIncomingSeq == null ||
				fixSimOutgoingSeq == null ||
				(Math.abs(localIncomingSeq - fixSimOutgoingSeq) > sequenceNumberDifferenceTolerance) ||
				(Math.abs(localOutgoingSeq - fixSimIncomingSeq) > sequenceNumberDifferenceTolerance)) {
			// Sync remote sequence numbers to ours
			getFixSimApiHandler().updateFixSimSessionSequenceNumbers(fixSimInstance, fixSimSessionName, localIncomingSeq, localOutgoingSeq);
			isRequiresRecoveryPeriod = true;
		}
		return isRequiresRecoveryPeriod;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Suffixes the FullSessionName with the FIX_SESSION_SUFFIX, such that the final session name will be less than or equal to 50 characters in length.
	 */
	private String getSessionNameWithSuffix(String fullSessionName, String suffix) {
		int endIndex = fullSessionName.length();
		return StringUtils.substring(fullSessionName, 0, Math.min(endIndex, 45)) + suffix;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FixSimApiHandler getFixSimApiHandler() {
		return this.fixSimApiHandler;
	}


	public void setFixSimApiHandler(FixSimApiHandler fixSimApiHandler) {
		this.fixSimApiHandler = fixSimApiHandler;
	}
}
