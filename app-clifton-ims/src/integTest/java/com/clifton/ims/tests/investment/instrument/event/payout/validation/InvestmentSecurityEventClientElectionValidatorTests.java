package com.clifton.ims.tests.investment.instrument.event.payout.validation;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventClientElection;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.payout.validation.InvestmentSecurityEventClientElectionValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Tests to confirm proper validation of payout elections.
 *
 * @author davidi
 */
public class InvestmentSecurityEventClientElectionValidatorTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private InvestmentSecurityEventClientElectionValidator investmentSecurityEventClientElectionValidator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testValidate_valid_election_DtcOnlyPayout() {
		initValidator();

		//Cash Dividend Payment for CUK on 03/13/2020
		final int securityEventId = 1841722;
		final String clientAccountNumber = "057020";  //compatible account with CUK security
		final boolean dtcOnly = true;

		InvestmentSecurityEvent event = this.investmentSecurityEventService.getInvestmentSecurityEvent(securityEventId);
		Assertions.assertNotNull(event, "Cannot find InvestmentSecurityEvent with ID: " + securityEventId);

		InvestmentSecurityEventClientElection election = createSecurityEventElection(event, 1, BigDecimal.ONE, BigDecimal.TEN, clientAccountNumber);
		updatePayout(securityEventId, election.getElectionNumber(), dtcOnly);

		// client account and holding account terminated, activate holding account for validation
		this.investmentAccountUtils.reactivateHoldingAccount("941845136", new Date());

		// expect no validation failures
		this.investmentSecurityEventClientElectionValidator.validate(election, null);
	}


	@Test
	public void testValidate_valid_election_not_DtcOnlyPayout() {
		initValidator();

		//Cash Dividend Payment for CUK on 03/13/2020
		final int securityEventId = 1841722;
		final String clientAccountNumber = "057020";  //compatible account with CUK security
		final boolean dtcOnly = false;

		InvestmentSecurityEvent event = this.investmentSecurityEventService.getInvestmentSecurityEvent(securityEventId);
		Assertions.assertNotNull(event, "Cannot find InvestmentSecurityEvent with ID: " + securityEventId);

		InvestmentSecurityEventClientElection election = createSecurityEventElection(event, 1, BigDecimal.ONE, BigDecimal.TEN, clientAccountNumber);
		updatePayout(securityEventId, election.getElectionNumber(), dtcOnly);

		// expect no validation failures
		this.investmentSecurityEventClientElectionValidator.validate(election, null);
	}


	@Test
	public void testValidate_election_DtcOnlyPayout_no_dtc_holdingaccounts() {
		initValidator();

		//Cash Dividend Payment for CUK on 03/13/2020
		final int securityEventId = 1841722;
		final String clientAccountNumber = "050900";  //non-dtc holding accounts
		final boolean dtcOnly = true;

		InvestmentSecurityEvent event = this.investmentSecurityEventService.getInvestmentSecurityEvent(securityEventId);
		Assertions.assertNotNull(event, "Cannot find InvestmentSecurityEvent with ID: " + securityEventId);

		InvestmentSecurityEventClientElection election = createSecurityEventElection(event, 1, BigDecimal.ONE, BigDecimal.TEN, clientAccountNumber);
		updatePayout(securityEventId, election.getElectionNumber(), dtcOnly);

		// expect validation failure
		Assertions.assertThrows(ValidationException.class, () -> this.investmentSecurityEventClientElectionValidator.validate(election, null));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Initializes the validator.  Validator cannot be instantiated using an @Resource annotation
	 */
	private void initValidator() {
		if (this.investmentSecurityEventClientElectionValidator == null) {
			this.investmentSecurityEventClientElectionValidator = new InvestmentSecurityEventClientElectionValidator();
			this.investmentSecurityEventClientElectionValidator.setInvestmentAccountRelationshipService(this.investmentAccountRelationshipService);
			this.investmentSecurityEventClientElectionValidator.setInvestmentSecurityEventPayoutService(this.investmentSecurityEventPayoutService);
		}
	}


	private InvestmentSecurityEventClientElection createSecurityEventElection(InvestmentSecurityEvent event, int electionNumber, BigDecimal electionQuantity, BigDecimal electionValue, String clientAccountNumber) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setNumber(clientAccountNumber);
		List<InvestmentAccount> clientAccountList = this.investmentAccountService.getInvestmentAccountList(searchForm);
		Assertions.assertFalse(CollectionUtils.isEmpty(clientAccountList));

		InvestmentSecurityEventClientElection election = new InvestmentSecurityEventClientElection();
		election.setSecurityEvent(event);
		election.setElectionNumber((short) electionNumber);
		election.setElectionQuantity(electionQuantity);
		election.setElectionValue(electionValue);
		election.setClientInvestmentAccount(clientAccountList.get(0));

		return election;
	}


	private void updatePayout(int securityEventId, short electionNumber, boolean dtcOnly) {
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(securityEventId);
		payoutSearchForm.setElectionNumber(electionNumber);
		List<InvestmentSecurityEventPayout> payoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm);
		Assertions.assertFalse(CollectionUtils.isEmpty(payoutList), "Cannot find payouts for SecurityEventID: " + securityEventId);
		for (InvestmentSecurityEventPayout payout : CollectionUtils.getIterable(payoutList)) {
			payout.setDtcOnly(dtcOnly);
			this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
		}
	}
}
