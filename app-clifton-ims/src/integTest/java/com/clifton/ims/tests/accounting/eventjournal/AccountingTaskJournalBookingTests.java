package com.clifton.ims.tests.accounting.eventjournal;

import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.posting.AccountingPostingService;
import com.clifton.accounting.taskjournal.AccountingTaskJournal;
import com.clifton.accounting.taskjournal.AccountingTaskJournalService;
import com.clifton.accounting.taskjournal.AccountingTaskJournalType;
import com.clifton.accounting.taskjournal.generator.AccountingTaskJournalGeneratorCommand;
import com.clifton.accounting.taskjournal.generator.AccountingTaskJournalGeneratorService;
import com.clifton.accounting.taskjournal.search.AccountingTaskJournalSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingEventPostResult;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.lending.repo.LendingRepo;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>AccountingTaskJournalTests</code> are integration tests that verify the successful generation and booking for task journals.
 * <p>
 * NOTE:  This test is to make sure that the AccountJournal function properly when lazily loaded.
 *
 * @author mwacker
 */

public class AccountingTaskJournalBookingTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;
	@Resource
	private AccountingBookingService<?> accountingBookingService;
	@Resource
	private AccountingPostingService accountingPostingService;
	@Resource
	private AccountingTaskJournalGeneratorService accountingTaskJournalGeneratorService;
	@Resource
	private AccountingTaskJournalService accountingTaskJournalService;


	/**
	 * This integration test performs the following steps and verifies each one:
	 * <p>
	 * <ol>
	 * <li>Opens bond positions</li>
	 * <li>Opens REPO position</li>
	 * <li>Generates and books Cash Coupon Payment event</li>
	 * <li>Generates the task journal</li>
	 * <li>Book and Post the task journal</li>
	 * <li>Un-posts the task journal</li>
	 * </ol>
	 */
	@Test
	public void testRepoCouponTransferTaskJournal() {
		InvestmentSecurity bond = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("912828JE1");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForRepo(BusinessCompanies.MELLON_BANK_NA, BusinessCompanies.DEUTSCHE_BANK_SECURITIES, "Prohibit REPOs");
		this.complianceUtils.createApprovedContractsOldComplianceRule(accountInfo, bond.getInstrument());
		/*
		 * *********************************************************************************
		 * STEP 1: Execute bond trades
		 * *********************************************************************************
		 */
		testCouponEventDoTrades(bond, accountInfo);

		/*
		 * *********************************************************************************
		 * STEP 2: Create the REPO
		 * *********************************************************************************
		 */

		LendingRepo repo = this.lendingRepoUtils.newOpenLendingRepoBuilder(bond, new BigDecimal("4000000.00"), DateUtils.toDate("09/09/2013"), accountInfo).price(new BigDecimal("107.8125")).haircutPercent(new BigDecimal("99")).indexRatio(new BigDecimal("1.0829700000")).interestAmount(new BigDecimal("10.29")).netCash(new BigDecimal("4632738.63")).marketValue(new BigDecimal("4679533.97")).interestRate(new BigDecimal("0.08")).settlementDate(DateUtils.toDate("09/10/2013")).dayCountConvention("Actual/360").active(true).instrumentHierarchy(bond.getInstrument().getHierarchy()).buildAndSave();

		this.lendingRepoUtils.executeRepo(repo, false);

		/*
		 * *********************************************************************************
		 * STEP 3: Create and book coupon event
		 * *********************************************************************************
		 */
		InvestmentSecurityEvent couponEvent = new InvestmentSecurityEvent();
		couponEvent.setType(this.investmentSecurityEventService.getInvestmentSecurityEventTypeByName(InvestmentSecurityEventType.CASH_COUPON_PAYMENT));
		couponEvent.setBeforeAndAfterEventValue(new BigDecimal("1.375"));
		couponEvent.setSecurity(bond);
		couponEvent.setAccrualStartDate(DateUtils.toDate("7/15/2013"));
		couponEvent.setAccrualEndDate(DateUtils.toDate("01/14/2014"));
		couponEvent.setExDate(DateUtils.toDate("01/15/2014"));
		couponEvent.setPaymentDate(DateUtils.toDate("01/15/2014"));

		this.investmentSecurityEventService.saveInvestmentSecurityEvent(couponEvent);

		AccountingEventPostResult result = this.accountingUtils.generateAndPostEventJournal(couponEvent, accountInfo.getClientAccount());

		/*
		 * *********************************************************************************
		 * STEP 4: Generate task journals
		 * *********************************************************************************
		 */
		AccountingTaskJournalType journalType = this.accountingTaskJournalService.getAccountingTaskJournalTypeByName("REPO Coupon Transfer");
		AccountingTaskJournalGeneratorCommand command = new AccountingTaskJournalGeneratorCommand();
		command.setTaskJournalTypeId(journalType.getId());
		command.setFromDate(DateUtils.toDate("01/01/2014"));
		command.setToDate(DateUtils.toDate("01/30/2014"));
		command.setSynchronous(true);
		this.accountingTaskJournalGeneratorService.generateAccountingTaskJournalList(command);

		AccountingTaskJournalSearchForm searchForm = new AccountingTaskJournalSearchForm();
		searchForm.setAccountingJournalId(result.getAccrualReversalBookedJournal().getId());

		/*
		 * *********************************************************************************
		 * STEP 5: Book and post task journals
		 * *********************************************************************************
		 */
		List<AccountingTaskJournal> taskJournalList = this.accountingTaskJournalService.getAccountingTaskJournalList(searchForm);

		List<Long> journalList = new ArrayList<>();
		for (AccountingTaskJournal taskJournal : CollectionUtils.getIterable(taskJournalList)) {
			journalList.add(this.accountingBookingService.bookAccountingJournal("Task Journal", taskJournal.getId(), true).getId());
		}


		/*
		 * *********************************************************************************
		 * STEP 6: Un-post task journals
		 * *********************************************************************************
		 */
		for (Long journalId : CollectionUtils.getIterable(journalList)) {
			this.accountingPostingService.unpostAccountingJournal(journalId);
		}


		/*
		 * *********************************************************************************
		 * STEP 5: Delete the task journals
		 * *********************************************************************************
		 */
		for (AccountingTaskJournal taskJournal : CollectionUtils.getIterable(taskJournalList)) {
			this.accountingTaskJournalService.deleteAccountingTaskJournal(taskJournal.getId());
		}
	}


	private void testCouponEventDoTrades(InvestmentSecurity bond, AccountInfo accountInfo) {
		Date tradeDate = DateUtils.toDate("08/05/2013");
		BigDecimal quantity = new BigDecimal("5580000");
		Trade firstTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, true, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("110.609375")).withExpectedUnitPrice(new BigDecimal("110.609375")).withNotionalMultiplier(new BigDecimal("1.08067")).withAccrualAmount1(new BigDecimal("4956.84")).withSettlementDate(DateUtils.toDate("08/06/2013")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(firstTrade);

		tradeDate = DateUtils.toDate("01/08/2014");
		quantity = new BigDecimal("1060000");
		Trade secondTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, false, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("108.9375")).withExpectedUnitPrice(new BigDecimal("108.9375")).withNotionalMultiplier(new BigDecimal("1.08247")).withAccrualAmount1(new BigDecimal("7631.27")).withSettlementDate(DateUtils.toDate("01/09/2014")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(secondTrade);

		tradeDate = DateUtils.toDate("03/13/2014");
		quantity = new BigDecimal("590000");
		Trade thirdTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, true, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("109.66015625")).withExpectedUnitPrice(new BigDecimal("109.66015625")).withNotionalMultiplier(new BigDecimal("1.08242")).withAccrualAmount1(new BigDecimal("1406.92")).withSettlementDate(DateUtils.toDate("03/14/2014")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(thirdTrade);

		tradeDate = DateUtils.toDate("04/16/2014");
		quantity = new BigDecimal("3520000");
		Trade forthTrade = this.tradeUtils.newBondTradeBuilder(bond, quantity, true, tradeDate, accountInfo).withOriginalFace(quantity).withAverageUnitPrice(new BigDecimal("108.953125")).withExpectedUnitPrice(new BigDecimal("108.953125")).withNotionalMultiplier(new BigDecimal("1.08689")).withAccrualAmount1(new BigDecimal("13369.35")).withSettlementDate(DateUtils.toDate("04/17/2014")).buildAndSave();

		this.tradeUtils.fullyExecuteTrade(forthTrade);
	}
}
