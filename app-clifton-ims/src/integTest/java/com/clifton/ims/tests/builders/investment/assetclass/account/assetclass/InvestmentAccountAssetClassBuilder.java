package com.clifton.ims.tests.builders.investment.assetclass.account.assetclass;

import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassAssignment;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustment;
import com.clifton.investment.account.targetexposure.InvestmentTargetExposureAdjustmentTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.replication.InvestmentReplication;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author stevenf
 */
public class InvestmentAccountAssetClassBuilder {

	private final InvestmentAccountAssetClassService investmentAccountAssetClassService;

	private InvestmentAccount account;
	private InvestmentAssetClass assetClass;
	private Short order;
	private boolean inactive;
	private boolean privateAssetClass;
	private boolean rollupAssetClass;
	private boolean excludeFromExposure;
	private boolean excludeFromOverlayExposure;
	private InvestmentReplication primaryReplication;
	private InvestmentReplication secondaryReplication;
	private BigDecimal secondaryReplicationAmount;
	private boolean doNotAdjustContractValue;
	private boolean replicationPositionExcludedFromCount;
	private boolean useActualForMatchingReplication;
	private InvestmentSecurity benchmarkSecurity;
	private InvestmentSecurity benchmarkDurationSecurity;
	private BigDecimal assetClassPercentage;
	private BigDecimal cashPercentage;
	private InvestmentTargetExposureAdjustmentTypes targetExposureAdjustmentType;
	private BigDecimal targetExposureAmount;
	private InvestmentManagerAccount targetExposureManagerAccount;
	private List<InvestmentAccountAssetClassAssignment> targetExposureAssignmentList;
	private boolean rebalanceAllocationFixed;
	private BigDecimal rebalanceAllocationMin;
	private BigDecimal rebalanceAllocationMax;
	private BigDecimal rebalanceAllocationAbsoluteMin;
	private BigDecimal rebalanceAllocationAbsoluteMax;
	private InvestmentAccountAssetClass.RebalanceActions rebalanceTriggerAction;
	private BigDecimal rebalanceExposureTarget;
	private InvestmentSecurity rebalanceBenchmarkSecurity;
	private boolean skipRebalanceCashAdjustment;
	private InvestmentAccountAssetClass.ClientDirectedCashOptions clientDirectedCashOption = InvestmentAccountAssetClass.ClientDirectedCashOptions.NONE;
	private BigDecimal clientDirectedCashAmount;
	private Date clientDirectedCashDate;
	private String columnGroupName;
	private List<SystemColumnValue> columnValueList;
	private InvestmentAccountAssetClass.PerformanceOverlayTargetSources performanceOverlayTargetSource;


	private InvestmentAccountAssetClassBuilder(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public static InvestmentAccountAssetClassBuilder investmentAccountAssetClass(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		return new InvestmentAccountAssetClassBuilder(investmentAccountAssetClassService);
	}


	public InvestmentAccountAssetClass build() {
		InvestmentAccountAssetClass investmentAccountAssetClass = new InvestmentAccountAssetClass();
		investmentAccountAssetClass.setAccount(this.account);
		investmentAccountAssetClass.setAssetClass(this.assetClass);
		investmentAccountAssetClass.setOrder(this.order);
		investmentAccountAssetClass.setInactive(this.inactive);
		investmentAccountAssetClass.setPrivateAssetClass(this.privateAssetClass);
		investmentAccountAssetClass.setRollupAssetClass(this.rollupAssetClass);
		investmentAccountAssetClass.setExcludeFromExposure(this.excludeFromExposure);
		investmentAccountAssetClass.setExcludeFromOverlayExposure(this.excludeFromOverlayExposure);
		investmentAccountAssetClass.setPrimaryReplication(this.primaryReplication);
		investmentAccountAssetClass.setSecondaryReplication(this.secondaryReplication);
		investmentAccountAssetClass.setSecondaryReplicationAmount(this.secondaryReplicationAmount);
		investmentAccountAssetClass.setDoNotAdjustContractValue(this.doNotAdjustContractValue);
		investmentAccountAssetClass.setReplicationPositionExcludedFromCount(this.replicationPositionExcludedFromCount);
		investmentAccountAssetClass.setUseActualForMatchingReplication(this.useActualForMatchingReplication);
		investmentAccountAssetClass.setBenchmarkSecurity(this.benchmarkSecurity);
		investmentAccountAssetClass.setBenchmarkDurationSecurity(this.benchmarkDurationSecurity);
		investmentAccountAssetClass.setAssetClassPercentage(this.assetClassPercentage);
		investmentAccountAssetClass.setCashPercentage(this.cashPercentage);
		if (InvestmentTargetExposureAdjustmentTypes.isTargetExposureUsed(this.targetExposureAdjustmentType)) {
			InvestmentTargetExposureAdjustment targetExposureAdjustment = new InvestmentTargetExposureAdjustment();
			targetExposureAdjustment.setTargetExposureAdjustmentType(this.targetExposureAdjustmentType);
			targetExposureAdjustment.setTargetExposureAmount(this.targetExposureAmount);
			targetExposureAdjustment.setTargetExposureManagerAccount(this.targetExposureManagerAccount);
			investmentAccountAssetClass.setTargetExposureAdjustment(targetExposureAdjustment);
		}
		investmentAccountAssetClass.setTargetAdjustmentAssignmentList(this.targetExposureAssignmentList);
		investmentAccountAssetClass.setRebalanceAllocationFixed(this.rebalanceAllocationFixed);
		investmentAccountAssetClass.setRebalanceAllocationMin(this.rebalanceAllocationMin);
		investmentAccountAssetClass.setRebalanceAllocationMax(this.rebalanceAllocationMax);
		investmentAccountAssetClass.setRebalanceAllocationAbsoluteMin(this.rebalanceAllocationAbsoluteMin);
		investmentAccountAssetClass.setRebalanceAllocationAbsoluteMax(this.rebalanceAllocationAbsoluteMax);
		investmentAccountAssetClass.setRebalanceTriggerAction(this.rebalanceTriggerAction);
		investmentAccountAssetClass.setRebalanceExposureTarget(this.rebalanceExposureTarget);
		investmentAccountAssetClass.setRebalanceBenchmarkSecurity(this.rebalanceBenchmarkSecurity);
		investmentAccountAssetClass.setSkipRebalanceCashAdjustment(this.skipRebalanceCashAdjustment);
		investmentAccountAssetClass.setClientDirectedCashOption(this.clientDirectedCashOption);
		investmentAccountAssetClass.setClientDirectedCashAmount(this.clientDirectedCashAmount);
		investmentAccountAssetClass.setClientDirectedCashDate(this.clientDirectedCashDate);
		investmentAccountAssetClass.setColumnGroupName(this.columnGroupName);
		investmentAccountAssetClass.setColumnValueList(this.columnValueList);
		investmentAccountAssetClass.setPerformanceOverlayTargetSource(this.performanceOverlayTargetSource);

		return investmentAccountAssetClass;
	}


	public InvestmentAccountAssetClass buildAndSave() {
		InvestmentAccountAssetClass investmentAccountAssetClass = build();
		this.investmentAccountAssetClassService.saveInvestmentAccountAssetClass(investmentAccountAssetClass);
		//refresh it to ensure proper loading of all properties.
		investmentAccountAssetClass = this.investmentAccountAssetClassService.getInvestmentAccountAssetClass(investmentAccountAssetClass.getId());
		return investmentAccountAssetClass;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassBuilder withAccount(InvestmentAccount account) {
		this.account = account;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withAssetClass(InvestmentAssetClass assetClass) {
		this.assetClass = assetClass;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withOrder(Short order) {
		this.order = order;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withInactive(boolean inactive) {
		this.inactive = inactive;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withPrivateAssetClass(boolean privateAssetClass) {
		this.privateAssetClass = privateAssetClass;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRollupAssetClass(boolean rollupAssetClass) {
		this.rollupAssetClass = rollupAssetClass;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withExcludeFromExposure(boolean excludeFromExposure) {
		this.excludeFromExposure = excludeFromExposure;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withExcludeFromOverlayExposure(boolean excludeFromOverlayExposure) {
		this.excludeFromOverlayExposure = excludeFromOverlayExposure;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withPrimaryReplication(InvestmentReplication primaryReplication) {
		this.primaryReplication = primaryReplication;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withSecondaryReplication(InvestmentReplication secondaryReplication) {
		this.secondaryReplication = secondaryReplication;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withSecondaryReplicationAmount(BigDecimal secondaryReplicationAmount) {
		this.secondaryReplicationAmount = secondaryReplicationAmount;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withDoNotAdjustContractValue(boolean doNotAdjustContractValue) {
		this.doNotAdjustContractValue = doNotAdjustContractValue;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withReplicationPositionExcludedFromCount(boolean replicationPositionExcludedFromCount) {
		this.replicationPositionExcludedFromCount = replicationPositionExcludedFromCount;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withUseActualForMatchingReplication(boolean useActualForMatchingReplication) {
		this.useActualForMatchingReplication = useActualForMatchingReplication;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withBenchmarkDurationSecurity(InvestmentSecurity benchmarkDurationSecurity) {
		this.benchmarkDurationSecurity = benchmarkDurationSecurity;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withAssetClassPercentage(BigDecimal assetClassPercentage) {
		this.assetClassPercentage = assetClassPercentage;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withCashPercentage(BigDecimal cashPercentage) {
		this.cashPercentage = cashPercentage;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withTargetExposureAdjustmentType(InvestmentTargetExposureAdjustmentTypes targetExposureAdjustmentType) {
		this.targetExposureAdjustmentType = targetExposureAdjustmentType;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withTargetExposureAmount(BigDecimal targetExposureAmount) {
		this.targetExposureAmount = targetExposureAmount;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withTargetExposureManagerAccount(InvestmentManagerAccount targetExposureManagerAccount) {
		this.targetExposureManagerAccount = targetExposureManagerAccount;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withTargetExposureAssignmentList(List<InvestmentAccountAssetClassAssignment> targetExposureAssignmentList) {
		this.targetExposureAssignmentList = targetExposureAssignmentList;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceAllocationFixed(boolean rebalanceAllocationFixed) {
		this.rebalanceAllocationFixed = rebalanceAllocationFixed;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceAllocationMin(BigDecimal rebalanceAllocationMin) {
		this.rebalanceAllocationMin = rebalanceAllocationMin;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceAllocationMax(BigDecimal rebalanceAllocationMax) {
		this.rebalanceAllocationMax = rebalanceAllocationMax;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceAllocationAbsoluteMin(BigDecimal rebalanceAllocationAbsoluteMin) {
		this.rebalanceAllocationAbsoluteMin = rebalanceAllocationAbsoluteMin;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceAllocationAbsoluteMax(BigDecimal rebalanceAllocationAbsoluteMax) {
		this.rebalanceAllocationAbsoluteMax = rebalanceAllocationAbsoluteMax;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceTriggerAction(InvestmentAccountAssetClass.RebalanceActions rebalanceTriggerAction) {
		this.rebalanceTriggerAction = rebalanceTriggerAction;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceExposureTarget(BigDecimal rebalanceExposureTarget) {
		this.rebalanceExposureTarget = rebalanceExposureTarget;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withRebalanceBenchmarkSecurity(InvestmentSecurity rebalanceBenchmarkSecurity) {
		this.rebalanceBenchmarkSecurity = rebalanceBenchmarkSecurity;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withSkipRebalanceCashAdjustment(boolean skipRebalanceCashAdjustment) {
		this.skipRebalanceCashAdjustment = skipRebalanceCashAdjustment;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withClientDirectedCashOption(InvestmentAccountAssetClass.ClientDirectedCashOptions clientDirectedCashOption) {
		this.clientDirectedCashOption = clientDirectedCashOption;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withClientDirectedCashAmount(BigDecimal clientDirectedCashAmount) {
		this.clientDirectedCashAmount = clientDirectedCashAmount;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withClientDirectedCashDate(Date clientDirectedCashDate) {
		this.clientDirectedCashDate = clientDirectedCashDate;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withColumnGroupName(String columnGroupName) {
		this.columnGroupName = columnGroupName;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withColumnValueList(List<SystemColumnValue> columnValueList) {
		this.columnValueList = columnValueList;
		return this;
	}


	public InvestmentAccountAssetClassBuilder withPerformanceOverlayTargetSource(InvestmentAccountAssetClass.PerformanceOverlayTargetSources performanceOverlayTargetSource) {
		this.performanceOverlayTargetSource = performanceOverlayTargetSource;
		return this;
	}
}
