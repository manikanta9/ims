package com.clifton.ims.tests.trade.options;


import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionCommand;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountRelationshipPurposes;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestination;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.options.TradeOptionsPositionBalance;
import com.clifton.trade.options.TradeOptionsRequest;
import com.clifton.trade.options.TradeOptionsRequestDetail;
import com.clifton.trade.options.TradeOptionsService;
import com.clifton.workflow.definition.WorkflowState;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>TradeOptionsServiceTests</code> tests the processing of delivery and expiration of option positions. Understanding the
 * test(s) requires an understanding of options and how we process them. See the links below for more information.
 *
 * @author jgommels
 * @see <a href="http://wiki/pages/viewpage.action?pageId=2326656">Options (Puts and Calls)</a>
 * @see <a href="http://wiki/display/IT/Options+Transactions">Options Transactions</a>
 */

public class TradeOptionsServiceTests extends BaseImsIntegrationTest {

	private static final Date OPTION_POSITION_OPEN_DATE = DateUtils.toDate("4/1/2014");
	private static final Date EXPIRATION_DATE = DateUtils.toDate("4/11/2014");
	private static final Date TRADE_DATE = DateUtils.clearTime(new Date());

	@Resource
	private AccountingPositionService accountingPositionService;

	@Resource
	private TradeDestinationService tradeDestinationService;

	@Resource
	private TradeOptionsService tradeOptionsService;

	@Resource
	private TradeGroupService tradeGroupService;

	@Resource
	private RuleViolationUtils ruleViolationUtils;

	private boolean initialized = false;

	private AccountInfo customer1AccountInfo;
	private AccountInfo customer2AccountInfo;
	private AccountInfo customer3AccountInfo;

	//NOTE: These securities are labeled by the underlying security. The actual option symbol can be seen in the configureTests() method.

	////////////////Underlying = SPX//////////////////////
	//Assume actual price is 1950

	//Customer 1 Position = -20, Customer 2 Position = -30
	private InvestmentSecurity spx_call_1900;

	//Customer 1 Position = -15, Customer 2 Position = -25
	private InvestmentSecurity spx_put_1770;

	//Customer 1 Position = -15
	private InvestmentSecurity spx_put_1760;

	///////////////Underlying = SPM4 (big)//////////////////
	//Assume actual price is 1890

	//Customer 1 Position = -22, Customer 2 Position = (none)
	private InvestmentSecurity spm4_call_1885;

	//Customer 1 Position = (none), Customer 2 Position = 20
	private InvestmentSecurity spm4_call_1895;

	//Customer 1 Position = -30, Customer 2 Position = 26
	private InvestmentSecurity spm4_put_1765;

	///////////////Underlying = ESM4 (mini)//////////////////
	//Assume actual price is 1800

	//Customer 3 Position = 35
	private InvestmentSecurity esm4_call_1885;

	//Customer 3 Position = 10
	private InvestmentSecurity esm4_put_1810;

	///////////////Underlying = IBM/////////////////////////

	private InvestmentSecurity ibm_call_195;

	///////////////////////////////////////////////////////////
	private InvestmentSecurity spx;
	private InvestmentSecurity spm4;
	private InvestmentSecurity esm4;
	private InvestmentSecurity ibm;

	private Set<InvestmentSecurity> optionsInTheMoney = new HashSet<>();


	@BeforeEach
	public void configureTests() {
		if (!this.initialized) {
			this.spx_call_1900 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 04/11/14 C1900");
			this.spx_put_1770 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 04/11/14 P1770");
			this.spx_put_1760 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPXW US 04/11/14 P1760");

			this.spm4_call_1885 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("2DJ4C 1890");
			this.spm4_call_1895 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("2DJ4C 1875");
			this.spm4_put_1765 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("2DJ4P 1755");

			this.esm4_call_1885 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("2EJ4C 1875");
			this.esm4_put_1810 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("2EJ4P 1805");

			this.ibm_call_195 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM US 04/19/14 C195");

			this.spx = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPX");
			this.spm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("SPM14");
			this.esm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("ESM14");
			this.ibm = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM");

			this.optionsInTheMoney.add(this.spx_call_1900);
			this.optionsInTheMoney.add(this.spm4_call_1885);
			this.optionsInTheMoney.add(this.esm4_put_1810);
			this.optionsInTheMoney.add(this.ibm_call_195);

			this.customer1AccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);
			this.investmentAccountUtils.createAccountRelationship(this.customer1AccountInfo.getClientAccount(), this.customer1AccountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_FUTURES);

			this.customer2AccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.CITIGROUP, BusinessCompanies.NORTHERN_TRUST, this.options);
			this.investmentAccountUtils.createAccountRelationship(this.customer2AccountInfo.getClientAccount(), this.customer2AccountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_FUTURES);

			this.customer3AccountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.options);
			this.investmentAccountUtils.createAccountRelationship(this.customer3AccountInfo.getClientAccount(), this.customer3AccountInfo.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_FUTURES);
			this.initialized = true;
		}
	}


	/**
	 * This is a thorough integration test that tests that performs the following actions in order, while verifying each step:
	 * <ol>
	 * <li>Opens various option positions (long and short) across three customers</li>
	 * <li>Retrieves the option balances for these customers and verifies them</li>
	 * <li>Processes the delivery of deliverable options positions that are in-the-money</li>
	 * <li>Processes the delivery offset of deliverable option positions that are in-the-money</li>
	 * <li>Processes the expiration of all option positions that are expiring</li>
	 * <li>Retrieves the updated option position balances and verifies the counts of the remaining contracts to be processed</li>
	 * </ol>
	 * <p>
	 * <p>
	 * <p>
	 * If this test is not passing, here are some debugging tips:
	 * <ol>
	 * <li>In IMS, go to Trading > Options Blotter. Filter the expiration date by value of EXPIRATION_DATE in this test. Evaluate the state of the Expiration and Delivery tabs
	 * in comparison to what is failing in this test.</li>
	 * <li>In IMS, go to Trading > Trade History. Add the 'Created On' column and filter on today's date. Filter on additional fields if needed. Also add the 'Trade Group Type' column. Evaluate the
	 * trades and compare with what is failing in this test. For example, look for any "Invalid" trades.
	 * </ol>
	 */
	@Test
	public void testOptionExpirationFullIntegration() {
		//Open positions for all three customers
		List<Trade> trades = openOptionPositions();

		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalancesForDefaultClientAccounts();
		assertOpeningOptionPositionBalanceList(balances, trades);

		//Process delivery of in-the-money options and verify the generated trades
		assertGenerateTradeGroupsForOptionDelivery(balances);

		//Process delivery offset of in-the-money options and verify the generated trades
		generateTradeGroupsForOptionDeliveryOffset(balances);

		//Process the expiration of all expiring options and verify the generated trades
		assertGenerateTradeGroupsForOptionExpiration(balances);

		//Get the new option balances and verify them (the number of pending contracts to be processed should have changed)
		balances = getOptionBalancesForDefaultClientAccounts();
		assertTradedOptionPositionBalanceList(balances, trades, true, true);
	}


	/**
	 * This is a thorough integration test that tests that performs the following actions in order, while verifying each step:
	 * <ol>
	 * <li>Opens various option positions (long and short) across three customers</li>
	 * <li>Retrieves the option balances for these customers and verifies them</li>
	 * <li>Processes the trade to close of all option positions that are expiring</li>
	 * <li>Retrieves the updated option position balances and verifies the counts of the remaining contracts to be processed</li>
	 * </ol>
	 * <p>
	 * Option Trade To Close creates trades to offset open Option positions, usually prior to expiration.
	 * <p>
	 * If this test is not passing, here are some debugging tips:
	 * <ol>
	 * <li>In IMS, go to Trading > Options Blotter. Filter the expiration date by value of EXPIRATION_DATE in this test. Evaluate the state of the Expiration and Delivery tabs
	 * in comparison to what is failing in this test.</li>
	 * <li>In IMS, go to Trading > Trade History. Add the 'Created On' column and filter on today's date. Filter on additional fields if needed. Also add the 'Trade Group Type' column. Evaluate the
	 * trades and compare with what is failing in this test. For example, look for any "Invalid" trades.
	 * </ol>
	 */
	@Test
	public void testOptionTradeToClose() {
		//Open positions for all three customers
		List<Trade> trades = openOptionPositions();

		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalancesForDefaultClientAccounts();
		assertOpeningOptionPositionBalanceList(balances, trades);

		//Process the trade to close of expiring options and verify the generated trades
		assertGenerateTradeGroupsForOptionTradeToClose(balances);

		//Get the new option balances and verify them (the number of pending contracts to be processed should have changed)
		balances = getOptionBalancesForDefaultClientAccounts();
		assertTradedOptionPositionBalanceList(balances, trades, true, false);
	}


	@Test
	public void testOptionAssignment() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo1.getClientAccount(), accountInfo1.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo2.getClientAccount(), accountInfo2.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);

		// create trades
		List<Trade> tradeList = CollectionUtils.createList(
				// Stock trades to create long positions
				this.tradeUtils.newStocksTradeBuilder(this.ibm, new BigDecimal(500), true, DateUtils.addDays(OPTION_POSITION_OPEN_DATE, -1), this.goldmanSachs, accountInfo1)
						.buildAndSave(),
				this.tradeUtils.newStocksTradeBuilder(this.ibm, new BigDecimal(200), true, DateUtils.addDays(OPTION_POSITION_OPEN_DATE, -1), this.goldmanSachs, accountInfo2)
						.buildAndSave(),
				// Options positions
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(2), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo1)
						.buildAndSave(),
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(1), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo2)
						.buildAndSave()
		);
		// execute trades
		tradeList.forEach(this.tradeUtils::fullyExecuteTrade);
		List<Trade> optionTradeList = tradeList.stream()
				.filter(trade -> InvestmentType.OPTIONS.equals(trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName()))
				.collect(Collectors.toList());

		Date expirationDate = DateUtils.toDate("04/19/2014");
		Integer[] clientAccountIds = ArrayUtils.createArray(accountInfo1.getClientAccount().getId(), accountInfo2.getClientAccount().getId());
		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalances(expirationDate, clientAccountIds);
		assertOpeningOptionPositionBalanceList(balances, optionTradeList);

		//Process the trade to close of expiring options and verify the generated trades
		List<TradeGroup> createdTradeGroups = assertGenerateTradeGroupsForOptionAssignment(balances);

		//Get the new option balances and verify them (the number of pending contracts to be processed should have changed)
		balances = getOptionBalances(expirationDate, clientAccountIds);
		assertTradedOptionPositionBalanceList(balances, optionTradeList, true, true);

		// Validate equity positions reflect assignment
		// book equity trades from assignment
		createdTradeGroups.stream()
				.map(TradeGroup::getTradeList)
				.flatMap(CollectionUtils::getStream)
				.filter(trade -> this.ibm.equals(trade.getInvestmentSecurity()))
				.forEach(this.tradeUtils::fullyExecuteTrade);
		// get equity positions for clients and validate
		AccountingPositionCommand command = AccountingPositionCommand.onPositionTransactionDate(new Date());
		command.setClientInvestmentAccountIds(clientAccountIds);
		command.setInvestmentSecurityId(this.ibm.getId());
		List<AccountingPosition> positionList = this.accountingPositionService.getAccountingPositionListUsingCommand(command);
		positionList.forEach(position -> {
			if (position.getClientInvestmentAccount().equals(accountInfo1.getClientAccount())) {
				Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(300), position.getRemainingQuantity()));
			}
			else {
				Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(100), position.getRemainingQuantity()));
			}
		});
	}


	@Test
	public void testOptionForwardRoll() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo1.getClientAccount(), accountInfo1.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo2.getClientAccount(), accountInfo2.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);

		// create trades
		List<Trade> tradeList = CollectionUtils.createList(
				// Options positions
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(20), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo1)
						.buildAndSave(),
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(15), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo2)
						.buildAndSave()
		);
		// execute trades
		tradeList.forEach(this.tradeUtils::fullyExecuteTrade);

		Date expirationDate = DateUtils.toDate("04/19/2014");
		Integer[] clientAccountIds = ArrayUtils.createArray(accountInfo1.getClientAccount().getId(), accountInfo2.getClientAccount().getId());
		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalances(expirationDate, clientAccountIds);
		assertOpeningOptionPositionBalanceList(balances, tradeList);

		//Process the trade to close of expiring options and verify the generated trades
		assertGenerateTradeGroupsForOptionRollForward(balances, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM US 05/17/14 C200"), new BigDecimal("2.0"), null, false);

		//Get the new option balances and verify them (the number of pending contracts to be processed should have changed)
		balances = getOptionBalances(expirationDate, clientAccountIds);
		assertTradedOptionPositionBalanceList(balances, tradeList, true, false);
	}


	@Test
	public void testOptionForwardRollWithNoRollQuantity() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo1.getClientAccount(), accountInfo1.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);
		// create trades
		List<Trade> tradeList = CollectionUtils.createList(
				// Options positions
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(20), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo1)
						.buildAndSave()
		);
		// execute trades
		tradeList.forEach(this.tradeUtils::fullyExecuteTrade);

		Date expirationDate = DateUtils.toDate("04/19/2014");
		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalances(expirationDate, accountInfo1.getClientAccount().getId());
		assertOpeningOptionPositionBalanceList(balances, tradeList);

		try {
			//Process the trade to close of expiring options and verify the generated trades
			assertGenerateTradeGroupsForOptionRollForward(balances, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM US 05/17/14 C200"), BigDecimal.ZERO, null, false);
			Assertions.fail("Expected validation exception for incompatible roll trade quantities");
		}
		catch (Exception e) {
			Throwable cause = ExceptionUtils.getOriginalException(e);
			Assertions.assertTrue(cause.getMessage().contains("Both a trade quantity to close the position and to roll into a new security are required"), "Failed to get expected error from server: " + cause.getMessage());
		}
	}


	@Test
	public void testOptionForwardRollWithNoCloseQuantity() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo1.getClientAccount(), accountInfo1.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);

		// create trades
		List<Trade> tradeList = CollectionUtils.createList(
				// Options positions
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(20), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo1)
						.buildAndSave()
		);
		// execute trades
		tradeList.forEach(this.tradeUtils::fullyExecuteTrade);

		Date expirationDate = DateUtils.toDate("04/19/2014");
		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalances(expirationDate, accountInfo1.getClientAccount().getId());
		assertOpeningOptionPositionBalanceList(balances, tradeList);

		try {
			//Process the trade to close of expiring options and verify the generated trades
			balances = balances.stream()
					.map(balance -> new TradeOptionsPositionBalance(balance.getPositionBalance(), BigDecimal.ZERO, balance.getNumRemainingContractsToDeliver(), balance.getNumRemainingContractsToOffsetDelivery()))
					.collect(Collectors.toList());
			assertGenerateTradeGroupsForOptionRollForward(balances, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM US 05/17/14 C200"), null, BigDecimal.ONE, false);
			Assertions.fail("Expected validation exception for incompatible roll trade quantities");
		}
		catch (Exception e) {
			Throwable cause = ExceptionUtils.getOriginalException(e);
			Assertions.assertTrue(cause.getMessage().contains("Both a trade quantity to close the position and to roll into a new security are required"), "Failed to get expected error from server: " + cause.getMessage());
		}
	}


	@Test
	public void testOptionForwardRollWithUnderlyingTrade() {
		AccountInfo accountInfo1 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo1.getClientAccount(), accountInfo1.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);
		AccountInfo accountInfo2 = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, BusinessCompanies.NORTHERN_TRUST, this.tradeUtils.getTradeType(TradeType.STOCKS));
		this.investmentAccountUtils.createAccountRelationship(accountInfo2.getClientAccount(), accountInfo2.getHoldingAccount(), InvestmentAccountRelationshipPurposes.TRADING_OPTIONS);

		// create trades
		List<Trade> tradeList = CollectionUtils.createList(
				// Stock trades to create long positions
				this.tradeUtils.newStocksTradeBuilder(this.ibm, new BigDecimal(1000), true, DateUtils.addDays(OPTION_POSITION_OPEN_DATE, -1), this.goldmanSachs, accountInfo1)
						.buildAndSave(),
				this.tradeUtils.newStocksTradeBuilder(this.ibm, new BigDecimal(400), true, DateUtils.addDays(OPTION_POSITION_OPEN_DATE, -1), this.goldmanSachs, accountInfo2)
						.buildAndSave(),
				// Options positions
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(2), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo1)
						.buildAndSave(),
				this.tradeUtils.newOptionsTradeBuilder(this.ibm_call_195, new BigDecimal(1), false, OPTION_POSITION_OPEN_DATE, this.goldmanSachs, accountInfo2)
						.buildAndSave()
		);
		// execute trades
		tradeList.forEach(this.tradeUtils::fullyExecuteTrade);
		List<Trade> optionTradeList = tradeList.stream()
				.filter(trade -> InvestmentType.OPTIONS.equals(trade.getInvestmentSecurity().getInstrument().getHierarchy().getInvestmentType().getName()))
				.collect(Collectors.toList());

		Date expirationDate = DateUtils.toDate("04/19/2014");
		Integer[] clientAccountIds = ArrayUtils.createArray(accountInfo1.getClientAccount().getId(), accountInfo2.getClientAccount().getId());
		//Get the option balances and verify them
		List<TradeOptionsPositionBalance> balances = getOptionBalances(expirationDate, clientAccountIds);
		assertOpeningOptionPositionBalanceList(balances, optionTradeList);

		//Process the trade to close of expiring options and verify the generated trades
		assertGenerateTradeGroupsForOptionRollForward(balances, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("IBM US 05/17/14 C200"), new BigDecimal("2.0"), null, true);

		//Get the new option balances and verify them (the number of pending contracts to be processed should have changed)
		balances = getOptionBalances(expirationDate, clientAccountIds);
		assertTradedOptionPositionBalanceList(balances, optionTradeList, true, false);
	}


	@Test
	public void testOptionBalanceListWhenThereIsAPendingTradeWithNoHoldingAccount() {
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.BARCLAYS_BANK_PLC, BusinessCompanies.NORTHERN_TRUST,
				this.tradeUtils.getTradeType(TradeType.FORWARDS));
		accountInfo.setHoldingAccount(null);
		this.tradeUtils.createSecuritySystemUser("imstestuser1", "FX Connect", "test_user_1");
		createForwardTradeGroup(accountInfo, "CNY/USD20150318 AM", "CNY", 1900000, true, "FX Connect Ticket");

		List<TradeOptionsPositionBalance> balanceList = getOptionBalances(EXPIRATION_DATE, accountInfo.getClientAccount().getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(balanceList));
	}


	private List<TradeOptionsPositionBalance> getOptionBalancesForDefaultClientAccounts() {
		return getOptionBalances(EXPIRATION_DATE, this.customer1AccountInfo.getClientAccount().getId(),
				this.customer2AccountInfo.getClientAccount().getId(),
				this.customer3AccountInfo.getClientAccount().getId());
	}


	private List<TradeOptionsPositionBalance> getOptionBalances(Date expirationDate, Integer... clientAccountIds) {
		AccountingBalanceSearchForm searchForm = new AccountingBalanceSearchForm();
		searchForm.setLastDeliveryDate(expirationDate);
		searchForm.setClientInvestmentAccountIds(clientAccountIds);

		return RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.tradeOptionsService.getTradeOptionPositionBalanceList(searchForm), 5);
	}


	private void assertOpeningOptionPositionBalanceList(List<TradeOptionsPositionBalance> balances, List<Trade> trades) {
		assertTradedOptionPositionBalanceList(balances, trades, false, false);
	}


	private void assertTradedOptionPositionBalanceList(List<TradeOptionsPositionBalance> balances, List<Trade> trades, boolean assertExpiration, boolean assertDelivery) {
		List<TradeOptionsPositionBalance> expectedBalances = new ArrayList<>();
		for (Trade trade : trades) {
			InvestmentSecurity option = trade.getInvestmentSecurity();
			BigDecimal tradeQuantity = trade.getQuantityIntended();
			AccountingBalance positionBalance = new AccountingBalance();
			positionBalance.setClientInvestmentAccount(trade.getClientInvestmentAccount());
			positionBalance.setHoldingInvestmentAccount(trade.getHoldingInvestmentAccount());
			positionBalance.setInvestmentSecurity(option);
			positionBalance.setQuantity(trade.isBuy() ? tradeQuantity : tradeQuantity.negate());

			BigDecimal expectedPositionQuantity = trade.isBuy() ? tradeQuantity : tradeQuantity.negate();
			BigDecimal expectedPendingDelivery = option.getInstrument().isDeliverable() ? tradeQuantity : null;
			if (option.getUnderlyingSecurity() != null && !MathUtils.isEqual(option.getPriceMultiplier(), option.getUnderlyingSecurity().getPriceMultiplier())) {
				expectedPendingDelivery = MathUtils.multiply(expectedPendingDelivery, option.getPriceMultiplier());
			}
			BigDecimal expectedPendingDeliveryOffset = expectedPendingDelivery;
			if (assertExpiration) {
				expectedPositionQuantity = BigDecimal.ZERO;

				if (CompareUtils.isEqual(this.ibm_call_195, option) && assertDelivery) {
					expectedPendingDelivery = BigDecimal.ZERO;
				}
				else if (option.getInstrument().isDeliverable()) {
					if (assertDelivery && this.optionsInTheMoney.contains(option) &&
							(CompareUtils.isEqual(this.spm4_call_1885, option) || CompareUtils.isEqual(this.esm4_put_1810, option))) {
						expectedPendingDelivery = BigDecimal.ZERO;
						expectedPendingDeliveryOffset = BigDecimal.ZERO;
					}
				}
			}

			TradeOptionsPositionBalance optionBalance = new TradeOptionsPositionBalance(positionBalance, expectedPositionQuantity, expectedPendingDelivery, expectedPendingDeliveryOffset);
			expectedBalances.add(optionBalance);
		}

		List<Function<TradeOptionsPositionBalance, Object>> sortFunctions = CollectionUtils.createList(
				tradeOptionsPositionBalance -> tradeOptionsPositionBalance.getPositionBalance().getClientInvestmentAccount().getId(),
				tradeOptionsPositionBalance -> tradeOptionsPositionBalance.getPositionBalance().getHoldingInvestmentAccount().getId(),
				tradeOptionsPositionBalance -> tradeOptionsPositionBalance.getPositionBalance().getInvestmentSecurity().getId());

		expectedBalances = BeanUtils.sortWithFunctions(expectedBalances, sortFunctions, CollectionUtils.createList(true, true, true));
		balances = BeanUtils.sortWithFunctions(balances, sortFunctions, CollectionUtils.createList(true, true, true));

		CoreCompareUtils.isNonNullPropertiesEqualForLists(expectedBalances, balances, true, true, new Class<?>[]{AccountingBalance.class}, "newBean");
	}


	private void assertGenerateTradeGroupsForOptionDelivery(List<TradeOptionsPositionBalance> balances) {
		TradeDestination manualDestination = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.MANUAL);

		TradeOptionsRequest request = new TradeOptionsRequest();
		request.setTradeDestination(manualDestination);
		request.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_PHYSICAL_DELIVERY.getTypeName()));
		request.setTraderUser(this.admin);

		List<TradeOptionsRequestDetail> details = new ArrayList<>();
		for (TradeOptionsPositionBalance balance : balances) {
			InvestmentSecurity option = balance.getPositionBalance().getInvestmentSecurity();
			if (this.optionsInTheMoney.contains(option) && option.getInstrument().isDeliverable()) {
				details.add(createDetail(balance, null));
			}
		}

		request.setDetails(details);

		List<TradeGroup> groups = generateTradeGroupListForTradeOptionsRequest(request);
		Assertions.assertEquals(2, groups.size());
		ignoreRuleViolations(groups);
		assertNoDuplicateUnderlyings(groups);

		MultiValueMap<InvestmentSecurity, Trade> expectedTradesForUnderlying = new MultiValueHashMap<>(true);

		//Customer 1 has a short position of a spm4_call_1885. Since it is a short call, this means that we SELL the position quantity of the underlying
		expectedTradesForUnderlying.put(this.spm4,
				generateExpectedFuturesTrade(this.spm4, new BigDecimal(22), false, this.customer1AccountInfo.getHoldingCompany(), manualDestination, this.customer1AccountInfo));

		//Customer 3 has a long position of a esm4_put_1810. Since it is a long put, this means that we SELL the position quantity of the underlying
		expectedTradesForUnderlying.put(this.esm4,
				generateExpectedFuturesTrade(this.esm4, new BigDecimal(10), false, this.customer3AccountInfo.getHoldingCompany(), manualDestination, this.customer3AccountInfo));

		assertTrades(expectedTradesForUnderlying, groups);
	}


	private void generateTradeGroupsForOptionDeliveryOffset(List<TradeOptionsPositionBalance> balances) {
		TradeDestination redi = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.REDI_TICKET);

		TradeOptionsRequest request = new TradeOptionsRequest();
		request.setTradeDestination(redi);
		request.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_DELIVERY_OFFSET.getTypeName()));
		request.setTraderUser(this.admin);

		List<TradeOptionsRequestDetail> details = new ArrayList<>();
		for (TradeOptionsPositionBalance balance : balances) {
			InvestmentSecurity option = balance.getPositionBalance().getInvestmentSecurity();
			if (this.optionsInTheMoney.contains(option) && option.getInstrument().isDeliverable()) {
				details.add(createDetail(balance, this.goldmanSachs));
			}
		}

		request.setDetails(details);

		List<TradeGroup> groups = generateTradeGroupListForTradeOptionsRequest(request);

		Assertions.assertEquals(2, groups.size());
		HashSet<InvestmentSecurity> groupSecuritySet = new HashSet<>(), groupSecondarySecuritySet = new HashSet<>();
		groups.forEach(group -> {
			groupSecuritySet.add(group.getInvestmentSecurity());
			groupSecondarySecuritySet.add(group.getSecondaryInvestmentSecurity());
		});
		Assertions.assertEquals(1, groupSecuritySet.size());
		Assertions.assertEquals(2, groupSecondarySecuritySet.size());

		ignoreRuleViolations(groups);

		MultiValueMap<InvestmentSecurity, Trade> expectedTradesForUnderlying = new MultiValueHashMap<>(true);

		//Customer 1 has a short position of a spm4_call_1885. Since it is a short call, this means that we BUY the position quantity of the underlying to offset the delivery trade
		//Note: SPM4 is a BIG, so we for REDI we trade in the corresponding MINI and multiply the quantity by 5
		expectedTradesForUnderlying.put(this.esm4, generateExpectedFuturesTrade(this.esm4, new BigDecimal(110), true, this.goldmanSachs, redi, this.customer1AccountInfo));

		//Customer 3 has a long position of a esm4_put_1810. Since it is a long put, this means that we BUY the position quantity of the underlying to offset the delivery trade
		expectedTradesForUnderlying.put(this.esm4, generateExpectedFuturesTrade(this.esm4, new BigDecimal(10), true, this.goldmanSachs, redi, this.customer3AccountInfo));

		assertTrades(expectedTradesForUnderlying, groups);
	}


	private void assertGenerateTradeGroupsForOptionExpiration(List<TradeOptionsPositionBalance> balances) {
		TradeDestination manualDestination = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.MANUAL);

		MultiValueMap<InvestmentSecurity, Trade> expectedTradesForUnderlying = new MultiValueHashMap<>(true);

		TradeOptionsRequest request = new TradeOptionsRequest();
		request.setTradeDestination(manualDestination);
		request.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_EXPIRATION.getTypeName()));
		request.setTraderUser(this.admin);

		List<TradeOptionsRequestDetail> details = new ArrayList<>();
		for (TradeOptionsPositionBalance balance : balances) {
			details.add(createDetail(balance, null));
			expectedTradesForUnderlying.put(balance.getPositionBalance().getInvestmentSecurity().getUnderlyingSecurity(), generateExpectedOptionTradeForPosition(balance, manualDestination));
		}

		request.setDetails(details);

		List<TradeGroup> groups = generateTradeGroupListForTradeOptionsRequest(request);
		assertNoDuplicateUnderlyings(groups);
		ignoreRuleViolations(groups);
		assertTrades(expectedTradesForUnderlying, groups);
	}


	private List<TradeGroup> assertGenerateTradeGroupsForOptionTradeToClose(List<TradeOptionsPositionBalance> balances) {
		TradeDestination manualDestination = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.MANUAL);

		MultiValueMap<InvestmentSecurity, Trade> expectedTradesForUnderlying = new MultiValueHashMap<>(true);

		TradeOptionsRequest request = new TradeOptionsRequest();
		request.setTradeDestination(manualDestination);
		request.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_TRADE_TO_CLOSE.getTypeName()));
		request.setTraderUser(this.admin);

		List<TradeOptionsRequestDetail> details = new ArrayList<>();
		for (TradeOptionsPositionBalance balance : balances) {
			details.add(createDetail(balance, null));
			expectedTradesForUnderlying.put(balance.getPositionBalance().getInvestmentSecurity().getUnderlyingSecurity(), generateExpectedOptionTradeForPosition(balance, manualDestination));
		}

		request.setDetails(details);

		List<TradeGroup> groups = generateTradeGroupListForTradeOptionsRequest(request);
		Assertions.assertEquals(5, groups.size());
		// Do not assert No Duplicate Underlying because there will be duplicates since TradeToClose will
		// Create a new Group on the same underlying for buy(long)/sell(short) trades and more than one
		// put or call with the same underlying security (securities with different strike prices).
		Assertions.assertEquals(2L, groups.stream().filter(group -> group.getInvestmentSecurity().equals(this.spx)).count());
		Assertions.assertEquals(2L, groups.stream().filter(group -> group.getInvestmentSecurity().equals(this.spm4)).count());
		ignoreRuleViolations(groups);
		assertTrades(expectedTradesForUnderlying, groups);
		return groups;
	}


	private List<TradeGroup> assertGenerateTradeGroupsForOptionAssignment(List<TradeOptionsPositionBalance> balances) {
		TradeDestination manualDestination = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.MANUAL);

		MultiValueMap<InvestmentSecurity, Trade> expectedTradesForUnderlying = new MultiValueHashMap<>(true);

		TradeOptionsRequest request = new TradeOptionsRequest();
		request.setTradeDestination(manualDestination);
		request.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_ASSIGNMENT.getTypeName()));
		request.setTraderUser(this.admin);

		List<TradeOptionsRequestDetail> details = new ArrayList<>();
		for (TradeOptionsPositionBalance balance : balances) {
			details.add(createDetail(balance, null));
			expectedTradesForUnderlying.put(balance.getPositionBalance().getInvestmentSecurity().getUnderlyingSecurity(), generateExpectedOptionTradeForPosition(balance, manualDestination, true));
		}

		request.setDetails(details);

		List<TradeGroup> groups = generateTradeGroupListForTradeOptionsRequest(request);
		Assertions.assertEquals(1, groups.size());

		Assertions.assertEquals(2L, groups.stream()
				.map(TradeGroup::getTradeList)
				.flatMap(CollectionUtils::getStream)
				.filter(trade -> trade.getInvestmentSecurity().equals(this.ibm_call_195))
				.count());

		Assertions.assertEquals(2L, groups.stream()
				.map(TradeGroup::getTradeList)
				.flatMap(CollectionUtils::getStream)
				.filter(trade -> trade.getInvestmentSecurity().equals(this.ibm))
				.count());

		ignoreRuleViolations(groups);
		return groups;
	}


	private List<TradeGroup> assertGenerateTradeGroupsForOptionRollForward(List<TradeOptionsPositionBalance> balanceList, InvestmentSecurity rollOption, BigDecimal rollRatio, BigDecimal rollQuantitiy, boolean tradeUnderlying) {
		TradeDestination manualDestination = this.tradeDestinationService.getTradeDestinationByName(TradeDestination.MANUAL);

		TradeOptionsRequest request = new TradeOptionsRequest();
		request.setTradeDestination(manualDestination);
		request.setTradeGroupType(this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.OPTION_ROLL_FORWARD.getTypeName()));
		request.setTraderUser(this.admin);

		List<TradeOptionsRequestDetail> details = new ArrayList<>();
		balanceList.forEach(balance -> {
			TradeOptionsRequestDetail detail = createDetail(balance, null);
			detail.setRollSecurity(rollOption);
			detail.setRollRatio(rollRatio);
			detail.setRollQuantity(rollQuantitiy);
			detail.setTradeUnderlying(tradeUnderlying);
			detail.setUnderlyingTradeBuy(false);
			detail.setUnderlyingTradeQuantity(BigDecimal.ONE);
			details.add(detail);
		});

		request.setDetails(details);

		List<TradeGroup> groups = generateTradeGroupListForTradeOptionsRequest(request);
		Assertions.assertEquals(1, groups.size());

		List<Trade> closeTradeList = groups.stream()
				.map(TradeGroup::getTradeList)
				.flatMap(CollectionUtils::getStream)
				.filter(trade -> trade.getInvestmentSecurity().equals(this.ibm_call_195))
				.collect(Collectors.toList());
		Assertions.assertEquals(2, closeTradeList.size());

		List<Trade> rollTradeList = groups.stream()
				.map(TradeGroup::getTradeList)
				.flatMap(CollectionUtils::getStream)
				.filter(trade -> trade.getInvestmentSecurity().equals(rollOption))
				.collect(Collectors.toList());
		Assertions.assertEquals(2, rollTradeList.size());

		closeTradeList.forEach(closeTrade -> {
			BigDecimal expectedRollQuantity = MathUtils.multiply(rollRatio, closeTrade.getQuantityIntended());
			Optional<Trade> rollTrade = rollTradeList.stream()
					.filter(t -> MathUtils.isEqual(t.getQuantityIntended(), expectedRollQuantity))
					.findFirst();
			Assertions.assertNotNull(rollTrade, "Unable to find applicable roll trade for the trade to close the Option position");
			rollTrade.ifPresent(rTrade -> Assertions.assertEquals(closeTrade.isBuy(), !rTrade.isBuy(), "Expected the roll trade to be in the opposite direction as the close trade"));
		});

		if (tradeUnderlying) {
			List<Trade> underlyingTradeList = groups.stream()
					.map(TradeGroup::getTradeList)
					.flatMap(CollectionUtils::getStream)
					.filter(trade -> rollOption.getUnderlyingSecurity().equals(trade.getInvestmentSecurity()))
					.collect(Collectors.toList());
			Assertions.assertEquals(2, underlyingTradeList.size());
			underlyingTradeList.forEach(trade -> {
				Assertions.assertFalse(trade.isBuy(), "Expected the underlying trade to be buy.");
				Assertions.assertEquals(BigDecimal.ONE, trade.getQuantityIntended(), "Expected the underlying trade quantity to be 1");
			});
		}

		ignoreRuleViolations(groups);
		return groups;
	}


	private List<TradeGroup> generateTradeGroupListForTradeOptionsRequest(TradeOptionsRequest request) {
		return RequestOverrideHandler.serviceCallWithOverrides(() -> this.tradeOptionsService.generateTradeGroupListForTradeOptionsRequest(request),
				// Avoid sending the following properties in the request to reduce the request size
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, false, "baseCurrency", "businessClient", "issuingCompany", "type", "description"),
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentSecurity.class, false, "instrument", "bigSecurity", "underlyingSecurity", "description"),
				new RequestOverrideHandler.EntityPropertyOverrides(BusinessCompany.class, false, "parent", "type", "description"),
				new RequestOverrideHandler.EntityPropertyOverrides(WorkflowState.class, false, "workflow", "status", "description"),
				new RequestOverrideHandler.EntityPropertyOverrides(TradeGroupType.class, false, "dynamicCalculatorBean", "description"));
	}


	private Trade generateExpectedFuturesTrade(InvestmentSecurity security, BigDecimal quantity, boolean buy, BusinessCompany executingBroker, TradeDestination tradeDestination,
	                                           AccountInfo customerAccountInfo) {
		return this.tradeUtils.newFuturesTradeBuilder(security, quantity, buy, TRADE_DATE, executingBroker, customerAccountInfo)
				.withTradeDestination(tradeDestination)
				.withAverageUnitPrice(null)
				.withSettlementDate(null)
				.withTraderUser(null)
				.build();
	}


	private Trade generateExpectedOptionTradeForPosition(TradeOptionsPositionBalance balance, TradeDestination destination) {
		return generateExpectedOptionTradeForPosition(balance, destination, false);
	}


	private Trade generateExpectedOptionTradeForPosition(TradeOptionsPositionBalance balance, TradeDestination destination, boolean tradeUnderlying) {
		InvestmentSecurity option = balance.getPositionBalance().getInvestmentSecurity();
		InvestmentSecurity underlying = option.getUnderlyingSecurity();
		Trade expected = new Trade();
		expected.setClientInvestmentAccount(balance.getPositionBalance().getClientInvestmentAccount());
		expected.setHoldingInvestmentAccount(balance.getPositionBalance().getHoldingInvestmentAccount());
		expected.setInvestmentSecurity(option);
		BigDecimal quantity = balance.getPositionBalance().getQuantity().abs();
		if (tradeUnderlying && !MathUtils.isEqual(option.getPriceMultiplier(), underlying.getPriceMultiplier())) {
			quantity = MathUtils.multiply(quantity, option.getPriceMultiplier());
		}
		expected.setQuantityIntended(quantity);
		expected.setBuy(tradeUnderlying ? MathUtils.isPositive(balance.getPositionBalance().getQuantity())
				: MathUtils.isNegative(balance.getPositionBalance().getQuantity()));
		expected.setTradeDate(TRADE_DATE);
		expected.setTradeDestination(destination);
		expected.setTradeType(this.tradeUtils.getTradeType(tradeUnderlying ? TradeType.STOCKS : TradeType.OPTIONS));
		return expected;
	}


	private void assertNoDuplicateUnderlyings(List<TradeGroup> tradeGroups) {
		Set<InvestmentSecurity> underlyings = new HashSet<>();
		for (TradeGroup group : tradeGroups) {
			Assertions.assertTrue(underlyings.add(group.getInvestmentSecurity()));
		}
	}


	private void ignoreRuleViolations(List<TradeGroup> groups) {
		for (TradeGroup group : groups) {
			for (Trade trade : group.getTradeList()) {
				if ("Invalid".equals(trade.getWorkflowState().getName())) {
					this.ruleViolationUtils.ignoreViolation("Trade", trade.getId(), "not the current security for instrument", false);
					this.ruleViolationUtils.ignoreViolation("Trade", trade.getId(), "not a business day for the calendar", false);
					this.tradeUtils.validateTrade(trade);
				}
			}
		}
	}


	private TradeOptionsRequestDetail createDetail(TradeOptionsPositionBalance balance, BusinessCompany executingBrokerOverride) {
		BusinessCompany executingBroker = executingBrokerOverride != null ? executingBrokerOverride : balance.getPositionBalance().getHoldingInvestmentAccount().getIssuingCompany();

		TradeOptionsRequestDetail detail = new TradeOptionsRequestDetail();
		detail.setClientInvestmentAccount(balance.getPositionBalance().getClientInvestmentAccount());
		detail.setHoldingInvestmentAccount(balance.getPositionBalance().getHoldingInvestmentAccount());
		detail.setInvestmentSecurity(balance.getPositionBalance().getInvestmentSecurity());
		detail.setExecutingBroker(executingBroker);
		detail.setTradeQuantity(balance.getTradeQuantityForExpire());

		return detail;
	}


	private void assertTrades(MultiValueMap<InvestmentSecurity, Trade> expectedTradesForUnderlying, List<TradeGroup> actualGroups) {
		long underlyingCount = actualGroups.stream().map(TradeGroup::getInvestmentSecurity).distinct().count();
		Assertions.assertEquals(expectedTradesForUnderlying.keySet().size(), Long.valueOf(underlyingCount).intValue());

		List<Trade> spxTrades = getTradesWithUnderlying(this.spx, actualGroups);
		compareTrades(expectedTradesForUnderlying.get(this.spx), spxTrades);

		List<Trade> spm4Trades = getTradesWithUnderlying(this.spm4, actualGroups);
		compareTrades(expectedTradesForUnderlying.get(this.spm4), spm4Trades);

		List<Trade> esm4Trades = getTradesWithUnderlying(this.esm4, actualGroups);
		compareTrades(expectedTradesForUnderlying.get(this.esm4), esm4Trades);
	}


	private void compareTrades(List<Trade> expected, List<Trade> actual) {
		CoreCompareUtils.isNonNullPropertiesEqualForLists(expected, actual, false, true, "createUserId", "updateUserId", "newBean", "description");
	}


	private List<Trade> getTradesWithUnderlying(InvestmentSecurity underlying, List<TradeGroup> groups) {
		List<Trade> underlyingTrades = new ArrayList<>();
		for (TradeGroup group : groups) {
			if (underlying.equals(group.getInvestmentSecurity())) {
				underlyingTrades.addAll(group.getTradeList());
			}
		}
		return underlyingTrades;
	}


	private void createForwardTradeGroup(AccountInfo accountInfo, String symbol, String payingCurrencySymbol, double quantity, boolean buy, String destinationName) {
		InvestmentSecurity security = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(symbol);
		InvestmentSecurity payingSecurity = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(payingCurrencySymbol);
		TradeDestination destination = this.tradeDestinationService.getTradeDestinationByName(destinationName);
		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_INTERNATIONAL);

		List<Trade> tradeList = new ArrayList<>();
		tradeList.add(this.tradeUtils.newForwardsTradeBuilder(security, null, buy, TRADE_DATE, null, accountInfo)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.withAccountingNotional(BigDecimal.valueOf(quantity))
				.withPayingSecurity(payingSecurity)
				.withExecutingBroker(executingBroker)
				.build());

		tradeList.add(this.tradeUtils.newForwardsTradeBuilder(security, null, buy, TRADE_DATE, null, accountInfo)
				.withTradeDestination(destination)
				.withTraderUser(this.securityUserService.getSecurityUserByPseudonym("imstestuser1"))
				.withAccountingNotional(BigDecimal.valueOf(quantity + 500000))
				.withPayingSecurity(payingSecurity)
				.withExecutingBroker(executingBroker)
				.build());

		this.tradeUtils.newTradeGroup(tradeList);
	}


	private List<Trade> openOptionPositions() {
		BusinessCompany executingBroker = this.goldmanSachs;

		Trade spx_call_1900_customer1 = this.tradeUtils.newOptionsTradeBuilder(this.spx_call_1900, new BigDecimal(20), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer1AccountInfo)
				.buildAndSave();

		Trade spx_call_1900_customer2 = this.tradeUtils.newOptionsTradeBuilder(this.spx_call_1900, new BigDecimal(30), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer2AccountInfo)
				.buildAndSave();

		Trade spx_put_1770_customer1 = this.tradeUtils.newOptionsTradeBuilder(this.spx_put_1770, new BigDecimal(15), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer1AccountInfo)
				.buildAndSave();

		Trade spx_put_1770_customer2 = this.tradeUtils.newOptionsTradeBuilder(this.spx_put_1770, new BigDecimal(25), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer2AccountInfo)
				.buildAndSave();

		Trade spx_put_1760_customer1 = this.tradeUtils.newOptionsTradeBuilder(this.spx_put_1760, new BigDecimal(15), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer1AccountInfo)
				.buildAndSave();

		Trade spm4_call_1885_customer1 = this.tradeUtils.newOptionsTradeBuilder(this.spm4_call_1885, new BigDecimal(22), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer1AccountInfo)
				.buildAndSave();

		Trade spm4_call_1895_customer2 = this.tradeUtils.newOptionsTradeBuilder(this.spm4_call_1895, new BigDecimal(20), true, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer2AccountInfo)
				.buildAndSave();

		Trade spm4_put_1765_customer1 = this.tradeUtils.newOptionsTradeBuilder(this.spm4_put_1765, new BigDecimal(30), false, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer1AccountInfo)
				.buildAndSave();

		Trade spm4_put_1765_customer2 = this.tradeUtils.newOptionsTradeBuilder(this.spm4_put_1765, new BigDecimal(26), true, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer2AccountInfo)
				.buildAndSave();

		Trade esm4_call_1885_customer3 = this.tradeUtils.newOptionsTradeBuilder(this.esm4_call_1885, new BigDecimal(35), true, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer3AccountInfo)
				.buildAndSave();

		Trade esm4_put_1810_customer3 = this.tradeUtils.newOptionsTradeBuilder(this.esm4_put_1810, new BigDecimal(10), true, OPTION_POSITION_OPEN_DATE, executingBroker, this.customer3AccountInfo)
				.buildAndSave();

		this.tradeUtils.fullyExecuteTrade(spx_call_1900_customer1);
		this.tradeUtils.fullyExecuteTrade(spx_call_1900_customer2);
		this.tradeUtils.fullyExecuteTrade(spx_put_1770_customer1);
		this.tradeUtils.fullyExecuteTrade(spx_put_1770_customer2);
		this.tradeUtils.fullyExecuteTrade(spx_put_1760_customer1);
		this.tradeUtils.fullyExecuteTrade(spm4_call_1885_customer1);
		this.tradeUtils.fullyExecuteTrade(spm4_call_1895_customer2);
		this.tradeUtils.fullyExecuteTrade(spm4_put_1765_customer1);
		this.tradeUtils.fullyExecuteTrade(spm4_put_1765_customer2);
		this.tradeUtils.fullyExecuteTrade(esm4_call_1885_customer3);
		this.tradeUtils.fullyExecuteTrade(esm4_put_1810_customer3);

		List<Trade> trades = new ArrayList<>();
		trades.add(spx_call_1900_customer1);
		trades.add(spx_call_1900_customer2);
		trades.add(spx_put_1770_customer1);
		trades.add(spx_put_1770_customer2);
		trades.add(spx_put_1760_customer1);
		trades.add(spm4_call_1885_customer1);
		trades.add(spm4_call_1895_customer2);
		trades.add(spm4_put_1765_customer1);
		trades.add(spm4_put_1765_customer2);
		trades.add(esm4_call_1885_customer3);
		trades.add(esm4_put_1810_customer3);

		return trades;
	}
}
