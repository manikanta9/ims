package com.clifton.ims.tests.performance.composite.account.calculators;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountSubPeriodLDICalculatorTests</code> tests the PerformanceCompositeAccountSubPeriodLDICalculator
 * <p>
 * NOTE: HISTORICALLY IT APPEARS ACCOUNTING ONLY LINKED THE RETURNS IF THERE WAS MORE THAN ONE DAY OF TRADE FLOWS
 *
 * @author manderson
 */
public class PerformanceCompositeAccountSubPeriodLDICalculatorTests extends BasePerformanceCompositeAccountCalculatorTests {


	@Test
	public void test_account_580001() {
		// NOTE: THIS ACCOUNT HOLDS SWAPTIONS WHICH USES ABS QUANTITY INSTEAD OF NOTIONAL
		String accountNumber = "580001";

		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("285750"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.01560874")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("23250"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGainLoss", new BigDecimal("54750"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.02601051")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("73750"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.03640524")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("9000"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_188200() {
		// NOTE: THIS ACCOUNT HOLDS FUTURES AND T-Bills (T-Bills SHOULD BE EXCLUDED)
		String accountNumber = "188200";

		// Futures Trade on 10/5
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-38603220")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.69715141")); // Accounting Value:-0.70280767
		overrideValueMap.put("periodGainLoss", new BigDecimal("-11809354.46"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		// Futures Trade on 11/4
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("52595186")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.60607993")); // Accounting Value:-0.6119337035
		overrideValueMap.put("periodGainLoss", new BigDecimal("-10448201.01"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		// Futures Trade on 12/7
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-37926564")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.28300464")); // Accounting Value:-0.2888207727
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.01560874")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("-4727692.25"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		// No Trades that caused breaks
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("3.68281990")); // Accounting Value: 3.67669195
		overrideValueMap.put("periodGainLoss", new BigDecimal("61695866.47"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		// Futures Trade on 2/16
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-65117000")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodNetReturn", new BigDecimal("1.88604635")); // Accounting Value:1.88007121
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.02601051")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("32188774.71"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		// No Trades that caused breaks
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.11073126")); // Accounting Value: -0.11652693
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.03640524")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("-1834531.25"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_668000() {
		// NOTE: THIS ACCOUNT HOLDS FUTURES
		String accountNumber = "668000";

		// Futures Trade on 11/10 and 11/19
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-0.91647128")); // Accounting Value: -0.92198887
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.92083067")); // Accounting Value: -0.93165746
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("18290192.14")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("-835022.60"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		// Futures Trade on 12/15
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("0.07954703")); // Accounting Value: 0.07422191
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-4436510")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.01560874")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("120258.77"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		// Futures Trade on 1/13 and 1/19
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-29534925")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("6198074.38"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		// Futures Trade on 2/9 and 2/16
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("2.98939729")); // Accounting Value: 2.98431674
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-22618865")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.02601051")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("3619444.39"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		// Futures Trade on 3/15
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-4898719")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.03640524")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("-373755.50"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_668500() {
		// NOTE: THIS ACCOUNT HOLDS FUTURES BUT ALL SHORTS
		String accountNumber = "668500";

		// Futures Trade on 10/5
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-803594"));  // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("562150.16"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("1236750"));  // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("929536.08"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-33963051.55"));  // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("-23214173"));  // Accounting Value: -24,535,097
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.01560874")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("59632.47"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("323187.50"));  // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("-3435313.09"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.02601051")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("-2199680.90"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);
	}


	@Test
	public void test_account_522450() {
		// NOTE: THIS ACCOUNT HOLDS FUTURES AND INTEREST RATE SWAPS AND BONDS
		String accountNumber = "522450";

		// This account was changed recently.  So either they'll need to re-calc history in which case this can be moved to a different calculator test
		// or switch the calculator back.
		setAccountPerformanceCalculationType(accountNumber, "10/31/2015", "LDI Calculator");

		// Issue with "Trade Flow" calc - ON 10/1 USZ5 AND WNZ5 WENT FROM SHORT TO LONG - Accounting looks like trade impact on remaining quantity
		// and not including the full trade ?  WNZ5: -180 -> +45  Accounting is only including the notional for the +45 not the full Buy of 225
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("managementFeeOverride", new BigDecimal("99061.48")); // New Feature to "True Up" Revenue
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-0.65562246")); // Accounting Value: -0.62759747
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.66332608")); // Accounting Value: -0.62759747
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("-0.668030")); // Based Accounting Gross Return Value: -0.640009
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("373424282")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("373424282")); // Accounting Value: 315,511,749
		overrideValueMap.put("periodGainLoss", new BigDecimal("-8430712.00"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "10/31/2015", overrideValueMap);

		// Looks like Accounting is Missing the Bond trade on 11/5 and only included flow from the Future
		setAccountPerformanceCalculationType(accountNumber, "11/30/2015", "LDI Calculator");
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGrossReturn", new BigDecimal("-0.64275419")); // Accounting Value: -0.6341944796
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.64533552")); // Accounting Value: -0.64660512>
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("-0.6551637")); // Based Accounting Gross Return Value: -0.646605
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("5397911")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("4678190")); // Accounting Value: 21,983,543
		overrideValueMap.put("periodGainLoss", new BigDecimal("-8241169.99"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "11/30/2015", overrideValueMap);

		// No Flows
		setAccountPerformanceCalculationType(accountNumber, "12/31/2015", "LDI Calculator");
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.44335716")); // Accounting Value: -0.45318329
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.01560874")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("-5609659.17"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "12/31/2015", overrideValueMap);

		// 2 Days of Trade Flows - one with Bonds
		setAccountPerformanceCalculationType(accountNumber, "01/31/2016", "LDI Calculator");
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodEndMarketValue", new BigDecimal("1317021737")); // Accounting Value: 1,391,721,737
		overrideValueMap.put("periodNetReturn", new BigDecimal("4.75336363")); // Accounting Value: 4.74421639
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-10084608")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("60029922.54"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "01/31/2016", overrideValueMap);

		setAccountPerformanceCalculationType(accountNumber, "02/29/2016", "LDI Calculator");
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGrossReturn", new BigDecimal("2.64396275")); // Accounting Value: 2.50227308
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("2.63114266")); // Based Accounting Gross Return Value: 2.489471
		overrideValueMap.put("periodNetReturn", new BigDecimal("2.64019782")); // Accounting Value: 2.48947070
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.02601051")); // Accounting Value: MISSING?
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("5311662")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("34879676.50"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);

		// Bond and Swap Trades on 3/29
		setAccountPerformanceCalculationType(accountNumber, "03/31/2016", "LDI Calculator");
		overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("-0.22455683")); // Accounting Value: -0.23331511
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.03640524")); // Accounting Value: MISSING?
		// NOTE: CONFIRMED WITH STEVE/LUCAS THAT THIS IS THE CORRECT TOTAL CASH FLOW REVIEWED EACH TRADE INDIVIDUALLY
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("-59404051")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("-5748779")); // Accounting Value: -5,397,917
		overrideValueMap.put("periodGainLoss", new BigDecimal("-2981444.14"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_458810() {
		String accountNumber = "458810";

		// Futures Trade on 2/26 (Roll on 2/23 Not Included)
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("13110875")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.02601051")); // Accounting Value: MISSING?
		overrideValueMap.put("periodGainLoss", new BigDecimal("4650418.75"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "02/29/2016", overrideValueMap);
	}


	@Test
	public void test_account_524000() {
		String accountNumber = "524000";

		// Trade Flow on the First day of the month should NOT cause a break - if it does it inadvertently includes 2/29 gain/loss
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodNetReturn", new BigDecimal("0.200458469")); // Accounting Value: 0.19508346
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.03640524")); // Accounting Value: MISSING?
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("19967438")); // Accounting Value: Missing - only entered weighted value
		overrideValueMap.put("periodGainLoss", new BigDecimal("172777.70"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_668452() {
		String accountNumber = "668452";

		// Break on day before last day of month: 3/1-3, 3/4-30, and 3/31
		Map<String, BigDecimal> overrideValueMap = new HashMap<>();
		overrideValueMap.put("periodGrossReturn", new BigDecimal("0.38641900")); // Accounting Value: 0.50717262
		overrideValueMap.put("periodModelNetReturn", new BigDecimal("0.373881")); // Based Accounting Gross Return Value: 0.494619
		overrideValueMap.put("periodNetReturn", new BigDecimal("0.38382852")); // Accounting Value: 0.49461943
		overrideValueMap.put("periodRiskFreeSecurityReturn", new BigDecimal("0.03640524")); // Accounting Value: MISSING?
		overrideValueMap.put("periodTotalNetCashflow", new BigDecimal("150916405")); // Accounting Value: Missing
		overrideValueMap.put("periodWeightedNetCashflow", new BigDecimal("255854363")); // Accounting Value: Missing
		overrideValueMap.put("periodGainLoss", new BigDecimal("2996506.54"));
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "03/31/2016", overrideValueMap);
	}


	@Test
	public void test_account_578509() {
		String accountNumber = "578509";

		// Had Bond Sells that were originally incorrectly subtracting accrued interest from positive accounting notional
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "05/31/2016", true);
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, "06/30/2016", true);
	}
}
