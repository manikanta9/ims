package com.clifton.ims.tests.core.web.stats;

import com.clifton.core.web.stats.RequestProcessorManagementService;
import com.clifton.core.web.stats.RequestProcessorStatus;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;


/**
 * The integration test class for the {@link RequestProcessorManagementService} service.
 * <p>
 * This test class is used to verify predictable JMX functionality on the target host.
 *
 * @author MikeH
 */
public class RequestProcessorManagementServiceImplTests extends BaseImsIntegrationTest {

	@Resource
	private RequestProcessorManagementService requestProcessorManagementService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetRequestProcessorStatus() {
		// Assert sane responses for JMX-based request processor status properties
		RequestProcessorStatus requestProcessorStatus = getRequestProcessorManagementService().getRequestProcessorStatus();
		Assertions.assertNotNull(requestProcessorStatus);
		Assertions.assertFalse(requestProcessorStatus.getExecutorServiceStatusList().isEmpty());
		Assertions.assertFalse(requestProcessorStatus.getEndpointList().isEmpty());
		Assertions.assertFalse(requestProcessorStatus.getRequestProcessorList().isEmpty());
		Assertions.assertTrue(requestProcessorStatus.getTotalRequestCount() > 0);
		Assertions.assertTrue(requestProcessorStatus.getTotalProcessingTime() > 0);
		Assertions.assertTrue(requestProcessorStatus.getAverageProcessingTime() > 0);
		Assertions.assertNotNull(requestProcessorStatus.getMaxRequestUri());
		Assertions.assertTrue(requestProcessorStatus.getMaxRequestTime() > 0);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RequestProcessorManagementService getRequestProcessorManagementService() {
		return this.requestProcessorManagementService;
	}


	public void setRequestProcessorManagementService(RequestProcessorManagementService requestProcessorManagementService) {
		this.requestProcessorManagementService = requestProcessorManagementService;
	}
}
