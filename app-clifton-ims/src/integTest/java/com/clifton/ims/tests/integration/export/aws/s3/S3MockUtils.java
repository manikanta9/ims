package com.clifton.ims.tests.integration.export.aws.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import org.springframework.stereotype.Component;


@Component
public class S3MockUtils {

	/**
	 * Polls the server every given number of seconds up to a given limit.
	 * Returns true when messages are found otherwise returns false.
	 */
	public boolean bucketHasObjects(AmazonS3 s3Mock, String bucket, String key, int pollLimitCount, int sleepTime) throws InterruptedException {
		int counter = 0;

		while (counter < pollLimitCount) {
			try {
				s3Mock.getObject(bucket, key);
				return true;
			}catch (AmazonS3Exception amazonS3Exception){
				counter += 1;
				Thread.sleep(sleepTime);
			}
		}
		return false;
	}
}
