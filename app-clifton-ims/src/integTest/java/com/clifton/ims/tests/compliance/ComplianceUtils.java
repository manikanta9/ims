package com.clifton.ims.tests.compliance;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.service.BusinessService;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.rule.ComplianceRule;
import com.clifton.compliance.rule.ComplianceRuleSearchForm;
import com.clifton.compliance.rule.ComplianceRuleService;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignment;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentSearchForm;
import com.clifton.compliance.run.execution.ComplianceRunExecutionCommand;
import com.clifton.compliance.run.execution.ComplianceRunExecutionService;
import com.clifton.compliance.run.preview.ComplianceRunPreviewService;
import com.clifton.compliance.run.rule.ComplianceRuleRun;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.ims.tests.investment.InvestmentAccountUtils;
import com.clifton.ims.tests.investment.InvestmentInstrumentUtils;
import com.clifton.ims.tests.rule.RuleViolationUtils;
import com.clifton.ims.tests.system.bean.SystemBeanBuilder;
import com.clifton.ims.tests.trade.TradeUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.group.InvestmentCurrencyTypes;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


@Component
public class ComplianceUtils {

	private static final String COMPLIANCE_WARNING_TYPE_NAME = "Compliance Real Time Rules";

	@Resource
	private ComplianceOldService complianceOldService;
	@Resource
	private ComplianceRuleService complianceRuleService;
	@Resource
	private TradeUtils tradeUtils;
	@Resource
	private InvestmentAccountUtils investmentAccountUtils;
	@Resource
	private RuleViolationUtils ruleViolationUtils;
	@Resource
	private InvestmentInstrumentUtils investmentInstrumentUtils;
	@Resource
	private SystemBeanService systemBeanService;
	@Resource
	private ComplianceRunPreviewService complianceRunPreviewService;
	@Resource
	private ComplianceRunExecutionService complianceRunExecutionService;


	public List<RuleViolation> executeRulesAgainstTrade(String tradeType, String securitySymbol, BigDecimal quantity, boolean buy, Date tradeDate, String... ruleNames) {
		AccountInfo accountInfo = createAccountInfoAndAssignRules(tradeType, ruleNames);
		return executeRulesAgainstTrade(accountInfo, tradeType, securitySymbol, quantity, buy, tradeDate);
	}


	public List<RuleViolation> executeRulesAgainstTrade(String tradeType, String securitySymbol, BigDecimal quantity, boolean buy, Date tradeDate, Date assignmentStartDate, String... ruleNames) {
		AccountInfo accountInfo = createAccountInfoAndAssignRules(tradeType, ruleNames);
		return executeRulesAgainstTrade(accountInfo, tradeType, securitySymbol, quantity, buy, tradeDate);
	}


	public List<RuleViolation> executeRulesAgainstTrade(AccountInfo accountInfo, String tradeType, String securitySymbol, BigDecimal quantity, boolean buy, Date tradeDate) {
		return executeRulesAgainstTrade(accountInfo, tradeType, false, securitySymbol, quantity, buy, tradeDate, null);
	}


	public List<RuleViolation> executeRulesAgainstTrade(AccountInfo accountInfo, String tradeType, String securitySymbol, BigDecimal quantity, boolean buy, Date tradeDate, BusinessCompany executingBroker) {
		return executeRulesAgainstTrade(accountInfo, tradeType, false, securitySymbol, quantity, buy, tradeDate, executingBroker);
	}


	public List<RuleViolation> executeRulesAgainstTrade(AccountInfo accountInfo, String tradeType, boolean rejectTradeIfWarnings, String securitySymbol, BigDecimal quantity, boolean buy, Date tradeDate, BusinessCompany executingBroker) {
		return executeTrade(accountInfo, tradeType, rejectTradeIfWarnings, this.investmentInstrumentUtils.getInvestmentSecurityBySymbol(securitySymbol), quantity, buy, tradeDate, executingBroker);
	}


	public List<RuleViolation> executeTrade(AccountInfo accountInfo, String tradeType, boolean rejectTradeIfWarnings, InvestmentSecurity security, BigDecimal quantity, boolean buy, Date tradeDate, BusinessCompany executingBroker) {
		Trade trade = buildComplianceTrade(accountInfo, tradeType, security, quantity, buy, tradeDate, executingBroker);
		List<RuleViolation> ruleViolationList = this.ruleViolationUtils.getRuleViolationList("Trade", trade.getId());
		ruleViolationList = this.ruleViolationUtils.filterViolationListByTypeName(COMPLIANCE_WARNING_TYPE_NAME, true, ruleViolationList);
		if (rejectTradeIfWarnings && !CollectionUtils.isEmpty(ruleViolationList)) {
			this.tradeUtils.rejectAndCancelTrade(trade);
		}
		return ruleViolationList;
	}


	public Trade buildComplianceTrade(AccountInfo accountInfo, String tradeType, InvestmentSecurity security, BigDecimal quantity, boolean buy, Date date, BusinessCompany executingBroker) {
		return this.tradeUtils.newTradeBuilder(this.tradeUtils.getTradeType(tradeType), security, quantity, BigDecimal.ONE, buy, date, executingBroker, accountInfo).buildAndSave();
	}


	public List<ComplianceRuleAssignment> createInvestmentAccountAndAssignRules(String tradeType, String... ruleNames) {
		return assignRulesToAccountByNames(createComplianceClientAccountForTrading(tradeType).getClientAccount(), ruleNames);
	}


	public AccountInfo createAccountInfoAndAssignRules(String tradeType, String... ruleNames) {
		return createAccountInfoAndAssignRules(tradeType, false, ruleNames);
	}


	public AccountInfo createAccountInfoAndAssignRules(String tradeType, Date startDate, String... ruleNames) {
		return createAccountInfoAndAssignRules(tradeType, false, startDate, ruleNames);
	}


	public AccountInfo createAccountInfoAndAssignRules(String tradeType, boolean includeTradeAllRule, String... ruleNames) {
		return createAccountInfoAndAssignRules(tradeType, includeTradeAllRule, BusinessCompanies.CLIFTON, null, ruleNames);
	}


	public AccountInfo createAccountInfoAndAssignRules(String tradeType, boolean includeTradeAllRule, Date startDate, String... ruleNames) {
		return createAccountInfoAndAssignRules(tradeType, includeTradeAllRule, BusinessCompanies.CLIFTON, null, ruleNames);
	}


	public AccountInfo createAccountInfoAndAssignRules(String tradeType, boolean includeTradeAllRule, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, String... ruleNames) {
		AccountInfo accountInfo = createComplianceClientAccountForTrading(tradeType, includeTradeAllRule, holdingCompany, custodianHoldingCompany);
		assignRulesToAccountByNames(accountInfo.getClientAccount(), ruleNames);
		return accountInfo;
	}


	public AccountInfo createAccountInfoAndAssignRules(String tradeType, boolean includeTradeAllRule, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany, Date startDate, String... ruleNames) {
		AccountInfo accountInfo = createComplianceClientAccountForTrading(tradeType, includeTradeAllRule, holdingCompany, custodianHoldingCompany);
		assignRulesToAccountByNames(accountInfo.getClientAccount(), startDate, ruleNames);
		return accountInfo;
	}


	private AccountInfo createComplianceClientAccountForTrading(String tradeType, boolean includeTradeAllRule, BusinessCompanies holdingCompany, BusinessCompanies custodianHoldingCompany) {
		AccountInfo account = this.investmentAccountUtils.createAccountsForTrading(holdingCompany, custodianHoldingCompany, this.tradeUtils.getTradeType(tradeType), InvestmentAccountUtils.RULE_DEFINITION_SHORT_LONG, InvestmentAccountUtils.RULE_DEFINITION_CONTRACT_APPROVED);
		this.investmentAccountUtils.activateAccount(account.getClientAccount());
		if (includeTradeAllRule) {
			createTradeAllRule(account.getClientAccount());
		}
		return account;
	}


	public void createTradeAllRule(InvestmentAccount clientAccount) {

		//Create underlying bean for allowing all trades for the given account
		SystemBean tradeAllBean = SystemBeanBuilder.newBuilder("Trade All For " + clientAccount.getLabel(), "Long/Short Restriction Evaluator", this.systemBeanService)
				.addProperty("Long Position Allowed", "true")
				.addProperty("Short Position Allowed", "true")
				.addProperty("Use Settlement Date Lookup", "false")
				.build();

		String tradeAllForClientComplianceRuleName = "Trade All For " + clientAccount.getLabel();
		ComplianceRuleSearchForm complianceRuleSearchForm = new ComplianceRuleSearchForm();
		complianceRuleSearchForm.setName(tradeAllForClientComplianceRuleName);
		ComplianceRule existingTradeAllRule = CollectionUtils.getFirstElement(this.complianceRuleService.getComplianceRuleList(complianceRuleSearchForm));
		if (existingTradeAllRule == null) {
			//Create rule to allow trading all on given account
			ComplianceRule tradeAllRule = new ComplianceRule();
			tradeAllRule.setName(tradeAllForClientComplianceRuleName);
			tradeAllRule.setDescription("Trade All For " + clientAccount.getLabel());
			tradeAllRule.setRuleEvaluatorBean(tradeAllBean);
			tradeAllRule.setRuleType(this.complianceRuleService.getComplianceRuleTypeByName("Short/Long Rule"));
			tradeAllRule.setBatch(true);
			tradeAllRule.setRealTime(true);
			tradeAllRule.setCurrencyType(InvestmentCurrencyTypes.BOTH);
			this.complianceRuleService.saveComplianceRule(tradeAllRule);

			//Assign trade all rule to the account
			ComplianceRuleAssignment complianceRuleAssignment = new ComplianceRuleAssignment();
			complianceRuleAssignment.setRule(tradeAllRule);
			complianceRuleAssignment.setClientInvestmentAccount(clientAccount);
			this.complianceRuleService.saveComplianceRuleAssignment(complianceRuleAssignment);
		}
	}


	public AccountInfo createComplianceClientAccountForTrading(String tradeType, boolean includeTradeAllRule) {
		return createComplianceClientAccountForTrading(tradeType, includeTradeAllRule, BusinessCompanies.CLIFTON, null);
	}


	public AccountInfo createComplianceClientAccountForTrading(String tradeType) {
		return createComplianceClientAccountForTrading(tradeType, false, BusinessCompanies.CLIFTON, null);
	}


	public List<ComplianceRuleAssignment> assignRulesToAccountByNames(InvestmentAccount clientAccount, String... ruleNames) {
		ValidationUtils.assertNotNull(ruleNames, "Must define at least one rule to assign to account");
		List<ComplianceRuleAssignment> assignmentList = new ArrayList<>();
		for (String ruleName : CollectionUtils.getIterable(Arrays.asList(ruleNames))) {
			assignmentList.add(assignRuleToAccountByName(clientAccount, ruleName));
		}
		return assignmentList;
	}


	public List<ComplianceRuleAssignment> assignRulesToAccountByNames(InvestmentAccount clientAccount, Date startDate, String... ruleNames) {
		ValidationUtils.assertNotNull(ruleNames, "Must define at least one rule to assign to account");
		List<ComplianceRuleAssignment> assignmentList = new ArrayList<>();
		for (String ruleName : CollectionUtils.getIterable(Arrays.asList(ruleNames))) {
			assignmentList.add(assignRuleToAccountByName(clientAccount, ruleName, startDate));
		}
		return assignmentList;
	}


	public ComplianceRuleAssignment assignRuleToAccountByName(InvestmentAccount investmentAccount, String ruleName) {
		ComplianceRuleAssignment assignment = createNewAssignment(investmentAccount, getComplianceRuleByName(ruleName));
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public ComplianceRuleAssignment assignRuleToAccountByName(InvestmentAccount investmentAccount, String ruleName, Date startDate) {
		ComplianceRuleAssignment assignment = createNewAssignmentOnDate(investmentAccount, getComplianceRuleByName(ruleName), startDate);
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public <T> T executeWithGlobalRuleAssignment(String ruleName, Callable<T> callable) {
		ComplianceRuleAssignment assignment = createNewAssignment(getComplianceRuleByName(ruleName));
		assignment = this.complianceRuleService.saveComplianceRuleAssignment(assignment);
		try {
			return callable.call();
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to execute.", e);
		}
		finally {
			this.removeComplianceRuleAssignment(assignment);
		}
	}


	public <T> T executeWithGlobalRuleAssignment(String ruleName, Date assignmentStartDate, Callable<T> callable) {
		ComplianceRuleAssignment assignment = createNewAssignmentOnDate(getComplianceRuleByName(ruleName), assignmentStartDate);
		assignment = this.complianceRuleService.saveComplianceRuleAssignment(assignment);
		try {
			return callable.call();
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to execute.", e);
		}
		finally {
			this.removeComplianceRuleAssignment(assignment);
		}
	}


	public ComplianceRuleAssignment assignRuleToBusinessServiceByName(BusinessService businessService, String ruleName) {
		ComplianceRuleAssignment assignment = createNewAssignment(businessService, getComplianceRuleByName(ruleName));
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public ComplianceRuleAssignment assignRuleToBusinessServiceByNameOnDate(BusinessService businessService, String ruleName, Date startDate) {
		ComplianceRuleAssignment assignment = createNewAssignmentOnDate(businessService, getComplianceRuleByName(ruleName), startDate);
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public ComplianceRuleAssignment excludeRuleFromAccountByName(InvestmentAccount investmentAccount, String ruleName) {
		ComplianceRuleAssignment assignment = createNewExclusion(investmentAccount, getComplianceRuleByName(ruleName));
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public ComplianceRuleAssignment excludeRuleFromAccountByNameOnDate(InvestmentAccount investmentAccount, String ruleName, Date startDate) {
		ComplianceRuleAssignment assignment = createNewExclusionOnDate(investmentAccount, getComplianceRuleByName(ruleName), startDate);
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public ComplianceRuleAssignment excludeRuleFromBusinessServiceByName(BusinessService businessService, String ruleName) {
		ComplianceRuleAssignment assignment = createNewExclusion(businessService, getComplianceRuleByName(ruleName));
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public ComplianceRuleAssignment excludeRuleFromBusinessServiceByNameOnDate(BusinessService businessService, String ruleName, Date startDate) {
		ComplianceRuleAssignment assignment = createNewExclusionOnDate(businessService, getComplianceRuleByName(ruleName), startDate);
		return this.complianceRuleService.saveComplianceRuleAssignment(assignment);
	}


	public void removeComplianceRuleAssignment(ComplianceRuleAssignment assignment) {
		if (assignment != null) {
			this.complianceRuleService.deleteComplianceRuleAssignment(assignment.getId());
		}
	}


	public void removeComplianceRuleAssignment(InvestmentAccount investmentAccount, String ruleName) {
		ComplianceRuleAssignmentSearchForm searchForm = buildComplianceRuleAssignmentSearchFormByRuleName(ruleName);
		searchForm.setAccountId(investmentAccount.getId());
		ComplianceRuleAssignment assignmentToRemove = CollectionUtils.getOnlyElementStrict(this.complianceRuleService.getComplianceRuleAssignmentList(searchForm));
		removeComplianceRuleAssignment(assignmentToRemove);
	}


	public void removeComplianceRuleAssignment(BusinessService businessService, String ruleName) {
		ComplianceRuleAssignmentSearchForm searchForm = buildComplianceRuleAssignmentSearchFormByRuleName(ruleName);
		searchForm.setBusinessServiceId(businessService.getId());
		ComplianceRuleAssignment assignmentToRemove = CollectionUtils.getOnlyElementStrict(this.complianceRuleService.getComplianceRuleAssignmentList(searchForm));
		removeComplianceRuleAssignment(assignmentToRemove);
	}


	private ComplianceRuleAssignmentSearchForm buildComplianceRuleAssignmentSearchFormByRuleName(String ruleName) {
		ComplianceRuleSearchForm complianceRuleSearchForm = new ComplianceRuleSearchForm();
		complianceRuleSearchForm.setName(ruleName);
		ComplianceRule rule = CollectionUtils.getFirstElementStrict(this.complianceRuleService.getComplianceRuleList(complianceRuleSearchForm));
		ComplianceRuleAssignmentSearchForm complianceRuleAssignmentSearchForm = new ComplianceRuleAssignmentSearchForm();
		complianceRuleAssignmentSearchForm.setRuleId(rule.getId());

		return complianceRuleAssignmentSearchForm;
	}


	public ComplianceRuleAssignment getComplianceRuleAssignmentByRuleNameAndAccountName(String ruleName, String accountName) {
		ComplianceRuleAssignmentSearchForm searchForm = new ComplianceRuleAssignmentSearchForm();
		searchForm.setRuleName(ruleName);
		searchForm.setAccountName(accountName);
		return CollectionUtils.getOnlyElement(this.complianceRuleService.getComplianceRuleAssignmentList(searchForm));
	}


	public ComplianceRuleAssignment getComplianceRuleAssignmentByRuleNameAndBusinessService(String ruleName, short businessServiceId) {
		ComplianceRuleAssignmentSearchForm searchForm = new ComplianceRuleAssignmentSearchForm();
		searchForm.setRuleName(ruleName);
		searchForm.setBusinessServiceId(businessServiceId);
		return CollectionUtils.getOnlyElement(this.complianceRuleService.getComplianceRuleAssignmentList(searchForm));
	}


	public ComplianceRule getComplianceRuleByName(String ruleName) {
		ComplianceRuleSearchForm searchForm = new ComplianceRuleSearchForm();
		SearchRestriction searchRestriction = new SearchRestriction();
		searchRestriction.setField("name");
		searchRestriction.setComparison(ComparisonConditions.EQUALS);
		searchRestriction.setValue(ruleName);
		searchForm.setRestrictionList(CollectionUtils.createList(searchRestriction));
		return CollectionUtils.getOnlyElementStrict(this.complianceRuleService.getComplianceRuleList(searchForm));
	}


	public ComplianceRuleAssignment createNewAssignment(InvestmentAccount investmentAccount, ComplianceRule complianceRule) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setClientInvestmentAccount(investmentAccount);
		return assignment;
	}


	public ComplianceRuleAssignment createNewAssignmentOnDate(InvestmentAccount investmentAccount, ComplianceRule complianceRule, Date startDate) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setClientInvestmentAccount(investmentAccount);
		assignment.setStartDate(startDate);
		return assignment;
	}


	public ComplianceRuleAssignment createNewAssignment(BusinessService businessService, ComplianceRule complianceRule) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setBusinessService(businessService);
		return assignment;
	}


	public ComplianceRuleAssignment createNewAssignmentOnDate(BusinessService businessService, ComplianceRule complianceRule, Date startDate) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setBusinessService(businessService);
		assignment.setStartDate(startDate);
		return assignment;
	}


	private ComplianceRuleAssignment createNewAssignment(ComplianceRule complianceRule) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		return assignment;
	}


	private ComplianceRuleAssignment createNewAssignmentOnDate(ComplianceRule complianceRule, Date startDate) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setStartDate(startDate);
		return assignment;
	}


	public ComplianceRuleAssignment createNewExclusion(InvestmentAccount investmentAccount, ComplianceRule complianceRule) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setClientInvestmentAccount(investmentAccount);
		assignment.setExclusionAssignment(true);
		return assignment;
	}


	public ComplianceRuleAssignment createNewExclusionOnDate(InvestmentAccount investmentAccount, ComplianceRule complianceRule, Date startDate) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setClientInvestmentAccount(investmentAccount);
		assignment.setExclusionAssignment(true);
		assignment.setStartDate(startDate);
		return assignment;
	}


	public ComplianceRuleAssignment createNewExclusion(BusinessService businessService, ComplianceRule complianceRule) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setBusinessService(businessService);
		assignment.setExclusionAssignment(true);
		return assignment;
	}


	public ComplianceRuleAssignment createNewExclusionOnDate(BusinessService businessService, ComplianceRule complianceRule, Date startDate) {
		ComplianceRuleAssignment assignment = new ComplianceRuleAssignment();
		assignment.setRule(complianceRule);
		assignment.setBusinessService(businessService);
		assignment.setExclusionAssignment(true);
		assignment.setStartDate(startDate);
		return assignment;
	}


	public void createApprovedContractsOldComplianceRule(AccountInfo accountInfo, InvestmentInstrument instrument) {
		ComplianceRuleOld approveTradeComplianceRule = new ComplianceRuleOld();
		approveTradeComplianceRule.setRuleType(this.complianceOldService.getComplianceRuleTypeOldByName("Client Approved Contracts"));
		approveTradeComplianceRule.setClientInvestmentAccount(accountInfo.getClientAccount());
		approveTradeComplianceRule.setInvestmentInstrument(instrument);
		this.complianceOldService.saveComplianceRuleOld(approveTradeComplianceRule);
	}


	public void createTradeAllComplianceRule(AccountInfo accountInfo) {
		//Create underlying bean for allowing all trades for the given account
		SystemBean tradeAllBean = SystemBeanBuilder.newBuilder("Trade All For " + accountInfo.getClientAccount().getLabel(), "Long/Short Restriction Evaluator", this.systemBeanService)
				.addProperty("Long Position Allowed", "true")
				.addProperty("Short Position Allowed", "true")
				.addProperty("Use Settlement Date Lookup", "false")
				.build();

		//Create rule to allow trading all on given account
		ComplianceRule tradeAllRule = new ComplianceRule();
		tradeAllRule.setName("Trade All For " + accountInfo.getClientAccount().getLabel());
		tradeAllRule.setDescription("Trade All For " + accountInfo.getClientAccount().getLabel());
		tradeAllRule.setRuleEvaluatorBean(tradeAllBean);
		tradeAllRule.setRuleType(this.complianceRuleService.getComplianceRuleTypeByName("Short/Long Rule"));
		tradeAllRule.setBatch(true);
		tradeAllRule.setRealTime(true);
		tradeAllRule.setCurrencyType(InvestmentCurrencyTypes.BOTH);
		this.complianceRuleService.saveComplianceRule(tradeAllRule);

		//Assign trade all rule to the account
		ComplianceRuleAssignment complianceRuleAssignment = new ComplianceRuleAssignment();
		complianceRuleAssignment.setRule(tradeAllRule);
		complianceRuleAssignment.setClientInvestmentAccount(accountInfo.getClientAccount());
		this.complianceRuleService.saveComplianceRuleAssignment(complianceRuleAssignment);
	}


	public List<ComplianceRuleRun> processRunPreview(InvestmentAccount clientAccount, Date processDate) {
		ComplianceRunExecutionCommand command = new ComplianceRunExecutionCommand();
		command.setCategoryName("Trade Rules");
		command.setClientAccountId(clientAccount.getId());
		command.setProcessDate(processDate);
		return this.complianceRunPreviewService.getComplianceRunPreviewList(command);
	}


	public Status rebuildComplianceRun(Date processDate, InvestmentAccount clientAccount, ComplianceRule complianceRule) {
		ComplianceRunExecutionCommand command = new ComplianceRunExecutionCommand();
		command.setSynchronous(true);
		command.setProcessDate(processDate);
		command.setClientAccountId(clientAccount.getId());
		command.setComplianceRuleId(complianceRule.getId());
		command.setPreviewOnly(true);
		return this.complianceRunExecutionService.rebuildComplianceRun(command);
	}
}
