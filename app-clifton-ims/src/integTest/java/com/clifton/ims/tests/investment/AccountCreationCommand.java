package com.clifton.ims.tests.investment;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractTypes;
import com.clifton.ims.tests.business.BusinessCompanies;


/**
 * The AccountCreationCommand class holds parameters used to create a single InvestmentAccount object.
 *
 * @author vgomelsky
 */
public class AccountCreationCommand {

	private String accountType;
	private BusinessClient client;

	private BusinessCompany issuingCompany; // takes precedence over issuer
	private BusinessCompanies issuer;

	private BusinessContract businessContract; // takes precedence over businessContractType
	private BusinessContractTypes businessContractType;

	private boolean activate;

	private boolean excludeBusinessContract; // used for testing to avoid adding the contract to the account
	private BusinessCompany defaultExchangeRateSourceCompany;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static AccountCreationCommand ofInactive(String accountType, BusinessClient client, BusinessCompanies issuer) {
		AccountCreationCommand command = new AccountCreationCommand();
		command.accountType = accountType;
		command.client = client;
		command.issuer = issuer;
		return command;
	}


	public static AccountCreationCommand ofActive(String accountType, BusinessClient client, BusinessCompanies issuer) {
		AccountCreationCommand command = ofInactive(accountType, client, issuer);
		command.activate = true;
		return command;
	}


	public static AccountCreationCommand ofActive(String accountType, BusinessClient client, BusinessCompanies issuer, BusinessContractTypes businessContractType) {
		AccountCreationCommand command = ofActive(accountType, client, issuer);
		command.businessContractType = businessContractType;
		return command;
	}


	public static AccountCreationCommand ofActive(String accountType, BusinessClient client, BusinessCompanies issuer, BusinessContractTypes businessContractType, boolean excludeBusinessContract) {
		AccountCreationCommand command = ofActive(accountType, client, issuer);
		command.businessContractType = businessContractType;
		command.excludeBusinessContract = excludeBusinessContract;
		return command;
	}


	public static AccountCreationCommand ofActive(String accountType, BusinessClient client, BusinessCompany issuingCompany, BusinessContract businessContract) {
		AccountCreationCommand command = new AccountCreationCommand();
		command.accountType = accountType;
		command.client = client;
		command.issuingCompany = issuingCompany;
		command.businessContract = businessContract;
		command.activate = true;
		return command;
	}


	public static AccountCreationCommand ofActive(String accountType, BusinessClient client, BusinessCompany issuingCompany, BusinessContract businessContract, boolean excludeBusinessContract) {
		AccountCreationCommand command = new AccountCreationCommand();
		command.accountType = accountType;
		command.client = client;
		command.issuingCompany = issuingCompany;
		command.businessContract = businessContract;
		command.activate = true;
		command.excludeBusinessContract = excludeBusinessContract;
		return command;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAccountType() {
		return this.accountType;
	}


	public BusinessClient getClient() {
		return this.client;
	}


	public BusinessCompany getIssuingCompany() {
		return this.issuingCompany;
	}


	public BusinessCompanies getIssuer() {
		return this.issuer;
	}


	public BusinessContract getBusinessContract() {
		return this.businessContract;
	}


	public BusinessContractTypes getBusinessContractType() {
		return this.businessContractType;
	}


	public boolean isActivate() {
		return this.activate;
	}


	public boolean isExcludeBusinessContract() {
		return this.excludeBusinessContract;
	}


	public BusinessCompany getDefaultExchangeRateSourceCompany() {
		return this.defaultExchangeRateSourceCompany;
	}


	public void setDefaultExchangeRateSourceCompany(BusinessCompany defaultExchangeRateSourceCompany) {
		this.defaultExchangeRateSourceCompany = defaultExchangeRateSourceCompany;
	}
}
