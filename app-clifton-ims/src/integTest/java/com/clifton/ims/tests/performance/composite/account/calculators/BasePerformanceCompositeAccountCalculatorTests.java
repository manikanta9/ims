package com.clifton.ims.tests.performance.composite.account.calculators;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.performance.composite.BasePerformanceCompositePerformanceTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceMetricCalculatorTypes;
import com.clifton.performance.composite.performance.metric.PerformanceMetricFieldTypes;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.junit.jupiter.api.Assertions;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author manderson
 */
public abstract class BasePerformanceCompositeAccountCalculatorTests extends BasePerformanceCompositePerformanceTests {

	private static Map<Class<?>, List<PropertyDescriptor>> CLASS_TO_PROPERTY_DESCRIPTOR_MAP = new ConcurrentHashMap<>();

	@Resource
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	@Resource
	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;

	@Resource
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	@Resource
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	protected static final String DE_COMPOSITE_III_CODE = "CGDEFLLCC"; // I.E. USED FOR 800020 ACCOUNT
	protected static final String GLOBAL_DE_COMPOSITE_CODE = "CGGDEFLLCC"; // I.E. USED FOR 800025 ACCOUNT

	private static final BigDecimal THRESHOLD = new BigDecimal("0.005");

	private static final Set<String> ACCOUNT_PERFORMANCE_VALIDATE_PROPERTIES = CollectionUtils.createHashSet("periodEndMarketValue", "periodTotalNetCashflow", "periodWeightedNetCashflow", "periodGainLoss", "periodGrossReturn", "periodNetReturn", "periodModelNetReturn", "periodBenchmarkReturn", "periodSecondaryBenchmarkReturn", "periodThirdBenchmarkReturn", "periodRiskFreeSecurityReturn");
	private static final Set<String> ACCOUNT_DAILY_PERFORMANCE_VALIDATE_PROPERTIES = CollectionUtils.createHashSet("gainLoss", "positionValue", "netCashFlow", "weightedNetCashFlow", "grossReturn", "benchmarkReturn", "secondaryBenchmarkReturn", "thirdBenchmarkReturn");

	private static final Map<String, String> ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP = new HashMap<>();


	static {
		ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP.put("gainLoss", "coalesceGainLossGainLossOverride");
		ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP.put("positionValue", "coalescePositionValuePositionValueOverride");
		ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP.put("netCashFlow", "coalesceNetCashFlowNetCashFlowOverride");
		ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP.put("weightedNetCashFlow", "coalesceWeightedNetCashFlowWeightedNetCashFlowOverride");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Can be overridden by each test to stop using the threshold once we know IMS has been calculating the values
	 * and we can match.  Prior to the calculations being used, values are uploaded and there could be some slight differences between IMS
	 * and the upload if we use a daily calculation vs. monthly that was used manually
	 */
	protected boolean isUseThreshold(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		return true;
	}


	/**
	 * Based on the preview and actual tries to determine if a benchmark field was cleared - if so then we need to ignore some values in comparion
	 */
	protected Set<PerformanceMetricFieldTypes> getIgnoreComparisonFields(PerformanceCompositeInvestmentAccountPerformance previewAccountPerformance, PerformanceCompositeInvestmentAccountPerformance actualAccountPerformance) {
		// If one of the benchmark fields was cleared, but had a benchmark on it before, just skip those values and any dependent values
		Set<PerformanceMetricFieldTypes> ignoreFields = new HashSet<>();
		if (previewAccountPerformance.getBenchmarkSecurity() == null && actualAccountPerformance.getPeriodBenchmarkReturn() != null) {
			ignoreFields.add(PerformanceMetricFieldTypes.PERIOD_BENCHMARK_RETURN);
		}
		if (previewAccountPerformance.getSecondaryBenchmarkSecurity() == null && actualAccountPerformance.getPeriodSecondaryBenchmarkReturn() != null) {
			ignoreFields.add((PerformanceMetricFieldTypes.PERIOD_SECONDARY_BENCHMARK_RETURN));
		}
		if (previewAccountPerformance.getThirdBenchmarkSecurity() == null && actualAccountPerformance.getPeriodThirdBenchmarkReturn() != null) {
			ignoreFields.add((PerformanceMetricFieldTypes.PERIOD_THIRD_BENCHMARK_RETURN));
		}
		if (previewAccountPerformance.getRiskFreeSecurity() == null && actualAccountPerformance.getPeriodRiskFreeSecurityReturn() != null) {
			ignoreFields.add(PerformanceMetricFieldTypes.PERIOD_RISK_FREE_SECURITY_RETURN);

			// Sharpe ratio depends on risk free security but not sure how to link that back easily so just check based on calculator
			for (PerformanceMetricFieldTypes fieldType : PerformanceMetricFieldTypes.values()) {
				if (fieldType.getPerformanceMetricCalculatorTypes() == PerformanceMetricCalculatorTypes.SHARPE_RATIO) {
					ignoreFields.add(fieldType);
				}
			}
		}
		return ignoreFields;
	}


	/**
	 * Checks itself in the ignore set, or it's dependent field (up to 2 levels)
	 */
	protected boolean isIgnoreComparisonField(PerformanceMetricFieldTypes fieldType, Set<PerformanceMetricFieldTypes> ignoreFields) {
		if (fieldType != null) {
			if (ignoreFields.contains(fieldType)) {
				return true;
			}

			if (fieldType.getDependentFieldType() != null) {
				if (ignoreFields.contains(fieldType.getDependentFieldType())) {
					return true;
				}
				if (fieldType.getDependentFieldType().getDependentFieldType() != null && ignoreFields.contains(fieldType.getDependentFieldType().getDependentFieldType())) {
					return true;
				}
			}
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                    Monthly Validation                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void validatePerformanceCompositeInvestmentAccountPerformance(String accountNumber, String periodEndDate) {
		validatePerformanceCompositeInvestmentAccountPerformance(null, accountNumber, periodEndDate);
	}


	protected void validatePerformanceCompositeInvestmentAccountPerformance(String compositeCode, String accountNumber, String periodEndDate) {
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, periodEndDate, false);
	}


	/**
	 * Validates preview = actual values with option to ignore daily overrides in previewed calculation
	 * This allows for the system to fix calculations where previously overrides were necessary
	 */
	protected void validatePerformanceCompositeInvestmentAccountPerformance(String compositeCode, String accountNumber, String periodEndDate, boolean ignoreExistingDailyOverrides) {
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, periodEndDate, new HashMap<>(), ignoreExistingDailyOverrides);
	}


	protected void validatePerformanceCompositeInvestmentAccountPerformance(String compositeCode, String accountNumber, String periodEndDate, Map<String, BigDecimal> overrideExpectedPropertyValues) {
		validatePerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, periodEndDate, overrideExpectedPropertyValues, false);
	}


	protected void setAccountPerformanceCalculationType(String accountNumber, String date, String expectedCalculationType) {
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
		AccountingPeriod period = this.accountingUtils.getAccountingPeriodForDate(DateUtils.toDate(date));
		List<PerformanceCompositeInvestmentAccount> accountAssignmentList = this.performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(account.getId(), period.getId());

		if (CollectionUtils.isEmpty(accountAssignmentList)) {
			throw new ValidationException("Cannot find performance composite assignment for account [" + accountNumber + "] and period [" + period.getLabel() + "].");
		}
		if (accountAssignmentList.size() > 1) {
			throw new ValidationException("Found multiple performance composite assignments for account [" + accountNumber + "] and period [" + period.getLabel() + "]. Need to define a composite or calculator to narrow down the results.");
		}
		PerformanceCompositeInvestmentAccount assignment = accountAssignmentList.get(0);

		if (!StringUtils.isEqual(expectedCalculationType, assignment.getCoalesceAccountPerformanceCalculatorBean().getName())) {
			// If the bean is selected on the composite, then just clear it instead of setting the override to match it (will error out)
			if (assignment.getPerformanceComposite() != null && assignment.getPerformanceComposite().getAccountPerformanceCalculatorBean() != null && StringUtils.isEqual(expectedCalculationType, assignment.getPerformanceComposite().getAccountPerformanceCalculatorBean().getName())) {
				assignment.setAccountPerformanceCalculatorBean(null);
			}
			else {
				SystemBean bean = this.systemBeanService.getSystemBeanByName(expectedCalculationType);
				if (bean == null) {
					throw new ValidationException("Cannot find System Bean with Name " + expectedCalculationType);
				}
				assignment.setAccountPerformanceCalculatorBean(bean);
			}
			RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.performanceCompositeSetupService.savePerformanceCompositeInvestmentAccount(assignment), 1);
		}
	}


	/**
	 * Allows to override specific property expected values
	 * WARNING THE GOAL WILL BE TO REMOVE ALL SUPPORT OF OVERRIDING VALUES AS THE CALCULATORS ARE FIXED TO MATCH THE ACTUAL APPROVED VALUES
	 */
	private void validatePerformanceCompositeInvestmentAccountPerformance(String compositeCode, String accountNumber, String periodEndDate, Map<String, BigDecimal> overrideExpectedPropertyValues, boolean ignoreExistingDailyOverrides) {
		PerformanceCompositeInvestmentAccountPerformance actualAccountPerformance = getPerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, periodEndDate, false);
		PerformanceCompositeInvestmentAccountPerformance previewAccountPerformance = getPreviewResultForPerformanceCompositeInvestmentAccountPerformance(actualAccountPerformance, ignoreExistingDailyOverrides);

		ValidationUtils.assertNotNull(actualAccountPerformance, "Actual PerformanceCompositeInvestmentAccountPerformance is required");
		ValidationUtils.assertNotNull(previewAccountPerformance, "Preview PerformanceCompositeInvestmentAccountPerformance is required");

		Set<PerformanceMetricFieldTypes> ignoreFields = getIgnoreComparisonFields(previewAccountPerformance, actualAccountPerformance);

		String messagePrefix = actualAccountPerformance.getLabel();
		boolean useThreshold = isUseThreshold(actualAccountPerformance);
		List<PropertyDescriptor> propertyList = CLASS_TO_PROPERTY_DESCRIPTOR_MAP.computeIfAbsent(PerformanceCompositeInvestmentAccountPerformance.class, BeanUtils::getPropertyDescriptors);
		for (PropertyDescriptor property : propertyList) {
			if (ACCOUNT_PERFORMANCE_VALIDATE_PROPERTIES.contains(property.getName())) {
				Optional<PerformanceMetricFieldTypes> result = Arrays.stream(PerformanceMetricFieldTypes.values()).filter(fieldType -> StringUtils.isEqual(fieldType.getBeanPropertyName(), property.getName())).findFirst();
				if (isIgnoreComparisonField(result.orElse(null), ignoreFields)) {
					continue;
				}
				if (BigDecimal.class.equals(property.getPropertyType())) {
					BigDecimal actualValue = (BigDecimal) BeanUtils.getPropertyValue(actualAccountPerformance, property.getName());
					boolean override = false;
					if (overrideExpectedPropertyValues.containsKey(property.getName())) {
						override = true;
						actualValue = overrideExpectedPropertyValues.get(property.getName());
					}
					if (actualValue == null) {
						actualValue = BigDecimal.ZERO;
					}
					BigDecimal previewValue = (BigDecimal) BeanUtils.getPropertyValue(previewAccountPerformance, property.getName());
					if (previewValue == null) {
						previewValue = BigDecimal.ZERO;
					}
					int scale = 0;
					if (property.getName().endsWith("Return")) {
						scale = 8;
					}
					// Allow to be more lenient for now since SQL calculated value can be slightly different
					if (property.getName().endsWith("ModelNetReturn")) {
						scale = 6;
					}
					BigDecimal threshold = BigDecimal.ZERO;
					if (useThreshold) {
						threshold = THRESHOLD;
					}
					BigDecimal difference = MathUtils.abs(MathUtils.subtract(actualValue, previewValue));
					if (MathUtils.isGreaterThan(difference, threshold)) {
						// Until this feature is released (8.9.0) Can't fully validate model net return - Accounting on Production has to periodically clear the model fee on Production to get current calculations to work
						if (property.getName().endsWith("ModelNetReturn")) {
							System.out.println("WARNING: " + messagePrefix + ": " + property.getName() + " doesn't match expected");
						}
						else {
							Assertions.assertEquals(MathUtils.round(actualValue, scale), MathUtils.round(previewValue, scale), messagePrefix + ": " + property.getName() + " doesn't match expected" + (override ? " (expected value was overridden, original value was " + CoreMathUtils.formatNumberDecimal((BigDecimal) BeanUtils.getPropertyValue(actualAccountPerformance, property.getName())) + ")." : "") + " and is not within " + CoreMathUtils.formatNumberDecimal(threshold) + " threshold");
						}
					}
				}
				else {
					Assertions.assertEquals(BeanUtils.getPropertyValue(actualAccountPerformance, property.getName()), BeanUtils.getPropertyValue(previewAccountPerformance, property.getName()), messagePrefix + ": " + property.getName() + " doesn't match expected.");
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                    Daily Validation                  /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates preview = actual for all daily property values
	 */
	protected void validatePerformanceCompositeInvestmentAccountDailyPerformance(String accountNumber, String periodEndDate) {
		validatePerformanceCompositeInvestmentAccountDailyPerformance(null, accountNumber, periodEndDate);
	}


	protected void validatePerformanceCompositeInvestmentAccountDailyPerformance(String compositeCode, String accountNumber, String periodEndDate) {
		validatePerformanceCompositeInvestmentAccountDailyPerformance(compositeCode, accountNumber, periodEndDate, false);
	}


	/**
	 * If ignoreExistingDailyOverrides = true will ignore them in preview processing and also validate value = actual coalesce
	 */
	protected void validatePerformanceCompositeInvestmentAccountDailyPerformance(String compositeCode, String accountNumber, String periodEndDate, boolean ignoreExistingDailyOverrides) {
		PerformanceCompositeInvestmentAccountPerformance actualAccountPerformance = getPerformanceCompositeInvestmentAccountPerformance(compositeCode, accountNumber, periodEndDate, false);
		ValidationUtils.assertNotNull(actualAccountPerformance, "Actual PerformanceCompositeInvestmentAccountPerformance is required");

		List<PerformanceCompositeInvestmentAccountDailyPerformance> actualAccountDailyPerformanceList = this.performanceCompositePerformanceService.getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(actualAccountPerformance.getId());
		ValidationUtils.assertNotEmpty(actualAccountDailyPerformanceList, "There are no daily records for " + actualAccountPerformance.getLabel() + " to validate against.");

		List<PerformanceCompositeInvestmentAccountDailyPerformance> previewAccountDailyPerformanceList = getPreviewDailyListResultForPerformanceCompositeInvestmentAccountPerformance(actualAccountPerformance, ignoreExistingDailyOverrides);
		ValidationUtils.assertNotEmpty(previewAccountDailyPerformanceList, "There are no daily records generated during preview for " + actualAccountPerformance.getLabel() + " to validate against.");

		Map<Date, PerformanceCompositeInvestmentAccountDailyPerformance> actualResultDateMap = BeanUtils.getBeanMap(actualAccountDailyPerformanceList, PerformanceCompositeInvestmentAccountDailyPerformance::getMeasureDate);
		Map<Date, PerformanceCompositeInvestmentAccountDailyPerformance> previewResultDateMap = BeanUtils.getBeanMap(previewAccountDailyPerformanceList, PerformanceCompositeInvestmentAccountDailyPerformance::getMeasureDate);

		String messagePrefix = actualAccountPerformance.getLabel();
		Assertions.assertEquals(actualResultDateMap.size(), previewResultDateMap.size(), messagePrefix + ": Expected Daily Record counts don't match expected.");

		List<PropertyDescriptor> propertyList = CLASS_TO_PROPERTY_DESCRIPTOR_MAP.computeIfAbsent(PerformanceCompositeInvestmentAccountDailyPerformance.class, BeanUtils::getPropertyDescriptors);
		for (Map.Entry<Date, PerformanceCompositeInvestmentAccountDailyPerformance> datePerformanceCompositeInvestmentAccountDailyPerformanceEntry : actualResultDateMap.entrySet()) {
			PerformanceCompositeInvestmentAccountDailyPerformance actualDailyPerformance = datePerformanceCompositeInvestmentAccountDailyPerformanceEntry.getValue();
			PerformanceCompositeInvestmentAccountDailyPerformance previewDailyPerformance = previewResultDateMap.get(datePerformanceCompositeInvestmentAccountDailyPerformanceEntry.getKey());
			String dailyMessagePrefix = messagePrefix + " " + DateUtils.fromDateShort(datePerformanceCompositeInvestmentAccountDailyPerformanceEntry.getKey());
			for (PropertyDescriptor property : propertyList) {
				if (ACCOUNT_DAILY_PERFORMANCE_VALIDATE_PROPERTIES.contains(property.getName())) {
					if (BigDecimal.class.equals(property.getPropertyType())) {
						String propertyName = property.getName();
						if (ignoreExistingDailyOverrides && ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP.containsKey(propertyName)) {
							propertyName = ACCOUNT_DAILY_COALESCE_OVERRIDE_PROPERTY_MAP.get(propertyName);
						}
						BigDecimal actualValue = (BigDecimal) BeanUtils.getPropertyValue(actualDailyPerformance, propertyName);
						if (actualValue == null) {
							actualValue = BigDecimal.ZERO;
						}
						BigDecimal previewValue = (BigDecimal) BeanUtils.getPropertyValue(previewDailyPerformance, property.getName());
						if (previewValue == null) {
							previewValue = BigDecimal.ZERO;
						}
						int scale = 2;
						if (property.getName().endsWith("Return")) {
							scale = 4;
						}
						Assertions.assertEquals(MathUtils.round(actualValue, scale), MathUtils.round(previewValue, scale), dailyMessagePrefix + ": " + property.getName() + " doesn't match expected.");
					}
					else {
						Assertions.assertEquals(BeanUtils.getPropertyValue(actualDailyPerformance, property.getName()), BeanUtils.getPropertyValue(previewDailyPerformance, property.getName()), dailyMessagePrefix + ": " + property.getName() + " doesn't match expected.");
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////                     Helper Methods                   /////////////
	////////////////////////////////////////////////////////////////////////////////


	protected PerformanceCompositeInvestmentAccountPerformance getPreviewResultForPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance, boolean ignoreExistingDailyOverrides) {
		rebuildAccountingPositionDailyDataIfMissing(accountPerformance);
		PerformanceCompositeAccountRebuildCommand command = new PerformanceCompositeAccountRebuildCommand();
		command.setPerformanceCompositeInvestmentAccountId(accountPerformance.getPerformanceCompositeInvestmentAccount().getId());
		command.setToAccountingPeriod(accountPerformance.getAccountingPeriod());
		command.setCalculateAccountDailyMonthlyBasePerformance(true);
		command.setPreview(true);
		command.setPreviewIgnoreOverrides(ignoreExistingDailyOverrides);
		command.setSynchronous(true);
		return this.performanceCompositePerformanceRebuildService.previewPerformanceCompositeAccountPerformance(command);
	}


	private List<PerformanceCompositeInvestmentAccountDailyPerformance> getPreviewDailyListResultForPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance, boolean ignoreExistingDailyOverrides) {
		rebuildAccountingPositionDailyDataIfMissing(accountPerformance);
		PerformanceCompositeAccountRebuildCommand command = new PerformanceCompositeAccountRebuildCommand();
		command.setPerformanceCompositeInvestmentAccountId(accountPerformance.getPerformanceCompositeInvestmentAccount().getId());
		command.setToAccountingPeriod(accountPerformance.getAccountingPeriod());
		command.setPreview(true);
		command.setPreviewIgnoreOverrides(ignoreExistingDailyOverrides);
		command.setSynchronous(true);
		return this.performanceCompositePerformanceRebuildService.previewPerformanceCompositeAccountDailyPerformanceRun(command);
	}

	////////////////////////////////////////////////////////////////////////////////


	private void rebuildAccountingPositionDailyDataIfMissing(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		// Do Last Day of Previous Month Separately - Only checks if snapshots exist on start date, so when rebuilding from month to month previous month prevents current month from rebuilding
		this.accountingUtils.rebuildAccountingPositionDailySnapshot(accountPerformance.getClientAccount().getId(), DateUtils.addDays(accountPerformance.getAccountingPeriod().getStartDate(), -1), DateUtils.addDays(accountPerformance.getAccountingPeriod().getStartDate(), -1));
		this.accountingUtils.rebuildAccountingPositionDailySnapshot(accountPerformance.getClientAccount().getId(), accountPerformance.getAccountingPeriod().getStartDate(), accountPerformance.getAccountingPeriod().getEndDate());
	}
}
