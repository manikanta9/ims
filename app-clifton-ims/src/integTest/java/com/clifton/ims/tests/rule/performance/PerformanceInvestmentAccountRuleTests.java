package com.clifton.ims.tests.rule.performance;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.dw.rebuild.DwRebuildCommand;
import com.clifton.dw.rebuild.DwRebuildService;
import com.clifton.ims.tests.rule.RuleTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.benchmark.PerformanceBenchmarkService;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.test.protocol.ImsErrorResponseException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * PerformanceInvestmentAccountRuleTests uses preview feature for rule violations on existing performance summaries
 * to validate expected violations and also passed violations across the rule definitions that we have set up.
 */

public class PerformanceInvestmentAccountRuleTests extends RuleTests {

	private static final String RULE_DEFINITION_HISTORICAL_GAIN_LOSS_ADJUSTMENTS = "Performance Summary: Historical Gain/Loss Adjustments Range";
	private static final String RULE_DEFINITION_OVERLAY_ASSET_CLASS_OVERRIDES = "Performance Overlay Asset Class Overrides";
	private static final String RULE_DEFINITION_MISSING_MAIN_RUN = "Portfolio Run: Missing Main Run On Balance Date";
	private static final String RULE_DEFINITION_INCOMPLETE_RUN = "Portfolio Run: Incomplete";
	private static final String RULE_DEFINITION_CALCULATED_GAIN_LOSS_VS_DATA_WAREHOUSE = "Performance Summary: MTD Calculated Gain/Loss vs. DW Unmatched";
	private static final String RULE_DEFINITION_OVERLAY_TARGET_MISSING = "Performance Overlay Run: Missing Overlay Target On Measure Date";
	private static final String RULE_DEFINITION_ASSET_CLASS_BENCHMARK_MISSING = "Asset Class: Missing Benchmark";
	private static final String RULE_DEFINITION_PARTIAL_PERIOD = "Client Account: Partial Period";
	private static final String RULE_DEFINITION_POSITION_TRANSFER = "Performance Summary: Position Transfer During Period";

	@Resource
	private AccountingPeriodService accountingPeriodService;

	@Resource
	private BusinessServiceService businessServiceService;

	@Resource
	private DwRebuildService dwRebuildService;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private PerformanceInvestmentAccountService performanceInvestmentAccountService;

	@Resource
	private PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService;

	@Resource
	private PerformanceBenchmarkService performanceBenchmarkService;

	/////////////////////////////////////////////////////////////////////////////
	////////////              Superclass Overrides               ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCategoryName() {
		return "Performance Summary Rules";
	}


	@Override
	public Set<String> getExcludedTestMethodSet() {
		return new HashSet<>();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Rule Tests                     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPortfolioRunMissingMainRunOnBalanceDateRule() {
		testPortfolioRunMissingMainRunOnBalanceDateRuleImpl(null);
	}


	private void testPortfolioRunMissingMainRunOnBalanceDateRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists: Ignored - Account Didn't Have Positions On Date
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("305300", DateUtils.toDate("01/31/2015"), false);
		validateViolationExists(performanceInvestmentAccount, "Missing Main Portfolio Run with balance date [01/05/2015]");

		// Violation Exists: Account Missing Holiday Run (Wasn't a valid violation before, but now would be so preview should have it)
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("078300", DateUtils.toDate("01/31/2014"), false);
		validateViolationExists(performanceInvestmentAccount, "Missing Main Portfolio Run with balance date [01/20/2014]");

		// No Violations - All Runs are In Good status
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("305300", DateUtils.toDate("04/30/2015"), false);
		validateViolationPassed(performanceInvestmentAccount, RULE_DEFINITION_MISSING_MAIN_RUN + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix), true);
	}


	@Test
	public void testPortfolioRunIncompleteRule() {
		testPortfolioRunIncompleteRuleImpl(null);
	}


	private void testPortfolioRunIncompleteRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists: Holiday 1/1 Run was never submitted to Trading
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("502040", DateUtils.toDate("01/31/2014"), false);
		validateViolationExistsContains(performanceInvestmentAccount, "Main Portfolio Run with balance date 01/01/2014 is incomplete and has not been submitting to Trading.");

		// No Violations - All Runs are In Good status
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("502040", DateUtils.toDate("03/31/2015"), false);
		validateViolationExists(performanceInvestmentAccount, RULE_DEFINITION_INCOMPLETE_RUN + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix));
	}


	@Test
	public void testPerformanceSummaryBenchmarkMonthlyReturnMissingRule() {
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("424050", DateUtils.toDate("07/05/2013"), false);
		List<PerformanceSummaryBenchmark> summaryBenchmarkList = this.performanceBenchmarkService.getPerformanceSummaryBenchmarkListBySummary(performanceInvestmentAccount.getId());
		for (PerformanceSummaryBenchmark summaryBenchmark : CollectionUtils.getIterable(summaryBenchmarkList)) {
			summaryBenchmark.setMonthToDateReturn(null);
			summaryBenchmark.setMonthToDateReturnOverride(null);
			this.performanceBenchmarkService.savePerformanceSummaryBenchmark(summaryBenchmark);
		}
		validateViolationExistsContains(performanceInvestmentAccount, "Missing MTD Return for Benchmark [Syn. BarCap Gov/Credit]");
	}


	@Test
	public void testPerformanceSummaryHistoricalGainLossAdjustmentsRangeRule() {
		testPerformanceSummaryHistoricalGainLossAdjustmentsRangeRuleImpl(null);
	}


	private void testPerformanceSummaryHistoricalGainLossAdjustmentsRangeRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists - Historical Gain/Loss Adjustment
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("668207", DateUtils.toDate("08/31/2015"), false);
		validateViolationExists(performanceInvestmentAccount, "Gain Loss Adjustments across all summaries since inception to 08/31/2015:<br /><span class='violationMessage'><span class='amountPositive'>1,108,404.00</span> is greater than maximum of <span class='amountPositive'>0.00</span></span>");
		// Violation Not Exists - 0 Gain/Loss Adjustments
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("128000", DateUtils.toDate("03/31/2015"), false);
		validateViolationPassed(performanceInvestmentAccount, RULE_DEFINITION_HISTORICAL_GAIN_LOSS_ADJUSTMENTS + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix), true);
	}


	@Test
	public void testPerformanceOverlayAssetClassOverridesRule() {
		// Violation Exists - Gain/Loss Override
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("455210", DateUtils.toDate("08/31/2014"), false);
		validateViolationExists(performanceInvestmentAccount, "Override(s) entered for Intl Equity - Emerging on Measure Date: 08/01/2014:<br /><span class='violationMessage'>Gain/Loss Adjusted by <span class='amountPositive'>1,982.91</span><br />Note: +1982.91 - to correct July<br />Last Updated By: Steve Bedell</span>");

		// Violation Exists - Overlay Target Override
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("310200", DateUtils.toDate("08/31/2014"), false);
		validateViolationExists(performanceInvestmentAccount, "Override(s) entered for Domestic Equity on Measure Date: 08/01/2014:<br /><span class='violationMessage'>Overlay Target Adjusted by <span class='amountNegative'>-147,828,505.50</span><br />Note: NONE<br />Last Updated By: Allie Neese</span>");

		// Note: NO Existing Cases where both overridden on same day

		// Violation Not Exists - No Overrides Entered
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("128000", DateUtils.toDate("03/31/2015"), false);
		validateViolationPassed(performanceInvestmentAccount, RULE_DEFINITION_OVERLAY_ASSET_CLASS_OVERRIDES, true);
	}


	@Test
	public void testPerformanceSummaryMTDCalculatedGainLossVsDWUnmatchedRule() {
		testPerformanceSummaryMTDCalculatedGainLossVsDWUnmatchedRuleImpl(null);
	}


	private void testPerformanceSummaryMTDCalculatedGainLossVsDWUnmatchedRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists - Summary Puts Treasury G/L into Other so not included
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("078200", DateUtils.toDate("04/30/2015"), false);
		rebuildDWSnapshotsForSummary(performanceInvestmentAccount);
		validateViolationExistsRegex(performanceInvestmentAccount, "Month to Date Calculated Gain/Loss \\[-8,398,312.[0-9]*\\] does not match DW Month To Date Gain/Loss of \\[-8,397,968.[0-9]*\\]. Difference = \\[-343.[0-9]*\\]");

		// Violation Not Exists
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("227000", DateUtils.toDate("03/31/2015"), false);
		rebuildDWSnapshotsForSummary(performanceInvestmentAccount);
		validateViolationPassed(performanceInvestmentAccount, RULE_DEFINITION_CALCULATED_GAIN_LOSS_VS_DATA_WAREHOUSE + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix), true);
	}


	@Test
	public void testPerformanceSummaryValidationWarningRule() {
		String expectedViolationNote = "Asset Class [Foreign Currency] is missing a performance benchmark that is active on [12/27/2012, and there is not a default benchmark security defined for the asset class.";
		//String expectedViolationNote = "Asset Class [Total Equity] is missing a performance benchmark that is active on [10/27/2015, and there is not a default benchmark security defined for the asset class.";
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("050952", DateUtils.toDate("12/27/2012"), false);
		try {
			this.performanceInvestmentAccountProcessService.processPerformanceInvestmentAccount(performanceInvestmentAccount.getId());
		}
		catch (ImsErrorResponseException ims) {
			Assertions.assertEquals(expectedViolationNote, ims.getErrorMessageFromIMS());
		}
	}


	@Test
	public void testPerformanceOverlayRunMissingOverlayTargetOnMeasureDateRule() {
		testPerformanceOverlayRunMissingOverlayTargetOnMeasureDateRuleImpl(null);
	}


	private void testPerformanceOverlayRunMissingOverlayTargetOnMeasureDateRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists - Overlay Target is 0 on 2/2 and No Overrides
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("078228", DateUtils.toDate("02/28/2015"), false);
		validateViolationExists(performanceInvestmentAccount, "Missing Overlay Target value on measure date [02/02/2015], but gain/loss is not zero.  Daily Returns will not calculate.");

		// No Violations - Overlay Target Populated for all Dates
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("161200", DateUtils.toDate("12/31/2014"), false);
		String ruleDefinitionName = RULE_DEFINITION_OVERLAY_TARGET_MISSING + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix);
		validateViolationPassed(performanceInvestmentAccount, ruleDefinitionName, true);

		// No Violations - Overlay Target had some zeros but was overridden manually
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("104001", DateUtils.toDate("08/31/2015"), false);
		validateViolationPassed(performanceInvestmentAccount, ruleDefinitionName, true);
	}


	@Test
	public void testAssetClassMissingBenchmarkRule() {
		// Violation Exists - Should Be Missing a Benchmark - Note if fails, may need to find another candidate - best option is to find an asset class that had
		// this violation, but the asset class is now inactive as more likely to not be re-activated than for analyst to set the benchmark later one
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("372600", DateUtils.toDate("07/31/2015"), false);
		validateViolationExistsContains(performanceInvestmentAccount, "Missing Benchmark for Asset Class [Absolute Return]");

		// Violation Not Exists - Should Not Be Missing
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("071000", DateUtils.toDate("07/31/2015"), false);
		validateViolationPassed(performanceInvestmentAccount, RULE_DEFINITION_ASSET_CLASS_BENCHMARK_MISSING, true);
	}


	@Test
	public void testPerformanceOverlayAssetClassBenchmarkMissingSyntheticBenchmarkOrMonthlyReturnRule() {
		// Violation Exists - Should Be Missing Benchmark synthetic (Old terminated account and custom benchmark)
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("359000", DateUtils.toDate("05/31/2013"), false);
		validateViolationExists(performanceInvestmentAccount, "Fixed Income Performance Benchmark: [.KCFI (Kimberly Clark Blend)] is missing a synthetic benchmark selection.");

		// Violation Not Exists - Should Not Be Missing
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("495000", DateUtils.toDate("06/30/2016"), false);
		validateViolationPassed(performanceInvestmentAccount, "Performance Overlay Asset Class Benchmark Missing Synthetic Benchmark or Monthly Return", true);
	}


	@Test
	public void testClientAccountPartialPeriodRule() {
		testClientAccountPartialPeriodRuleImpl(null);
	}


	private void testClientAccountPartialPeriodRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists - Put Positions on Mid Period
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("662905", DateUtils.toDate("08/31/2015"), false);
		rebuildAccountingPositionDailySnapshotsForSummary(performanceInvestmentAccount);
		validateViolationExists(performanceInvestmentAccount, "Positions On: 08/10/2015; ");

		// Violation Exists - Positions On and Off During Accounting Period
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("660810", DateUtils.toDate("05/31/2015"), false);
		rebuildAccountingPositionDailySnapshotsForSummary(performanceInvestmentAccount);
		validateViolationExists(performanceInvestmentAccount, "Positions On: 05/01/2015; Positions Off: 05/15/2015; ");

		// Violation Exists - Positions Off Entire Period
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("074500", DateUtils.toDate("08/31/2015"), false);
		rebuildAccountingPositionDailySnapshotsForSummary(performanceInvestmentAccount);
		validateViolationExists(performanceInvestmentAccount, "Positions Off All Period");

		// Violation Not Exists - Positions On Entire Period
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("221700", DateUtils.toDate("05/31/2015"), false);
		rebuildAccountingPositionDailySnapshotsForSummary(performanceInvestmentAccount);
		validateViolationPassed(performanceInvestmentAccount, RULE_DEFINITION_PARTIAL_PERIOD + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix), true);
	}


	@Test
	public void testPerformanceSummaryPositionTransferDuringPeriodRule() {
		testPerformanceSummaryPositionTransferDuringPeriodRuleImpl(null);
	}


	private void testPerformanceSummaryPositionTransferDuringPeriodRuleImpl(String ruleDefinitionNameSuffix) {
		// Violation Exists - 2 Position Transfers - 1 From and 1 To Account
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("227500", DateUtils.toDate("02/29/2016"), false);
		validateViolationExistsRegex(performanceInvestmentAccount, "Position Transfer From 227500: .* to 227501: .* on 02/04/2016:<br/><span class='amountPositive'>4,500,000.00</span> of 912828B25 .*<br/><span class='amountPositive'>100,000.00</span> of 912828B25 .*<br/><span class='amountPositive'>1,000,000.00</span> of 912828B25 .*<br/><span class='amountPositive'>1,000,000.00</span> of 912828B25 .*<br/><span class='amountPositive'>1,000,000.00</span> of 912828B25 .*<br/>");
		validateViolationExistsRegex(performanceInvestmentAccount, "Position Transfer From 227501: .* to 227500: .* on 02/04/2016:<br/><span class='amountPositive'>7,077,000.00</span> of 912828SA9 .*<br/>");

		String ruleDefinitionName = RULE_DEFINITION_POSITION_TRANSFER + (ruleDefinitionNameSuffix == null ? "" : ruleDefinitionNameSuffix);
		// No Position Transfers
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("050920", DateUtils.toDate("01/31/2016"), false);
		validateViolationPassed(performanceInvestmentAccount, ruleDefinitionName, true);

		// Has Position Transfers, but within same account
		performanceInvestmentAccount = getPerformanceInvestmentAccountForAccountAndDate("424000", DateUtils.toDate("01/31/2016"), false);
		validateViolationPassed(performanceInvestmentAccount, ruleDefinitionName, true);
	}


	@Test
	public void testClientAccountPartialPeriodPortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(() -> testClientAccountPartialPeriodRuleImpl(" - Portfolio Targets"), "662905", "660810", "074500", "221700");
	}


	@Test
	public void testPerformanceOverlayRunMissingOverlayTargetOnMeasureDatePortfolioTargetsRule() {
		try {
			executeTestWithClientOverriddenBusinessProcessingType(() -> testPerformanceOverlayRunMissingOverlayTargetOnMeasureDateRuleImpl(" - Portfolio Targets"), "078228", "161200", "104001");
		}
		finally {
			updateClientAccountBusinessServiceProcessingType("Structured Options", "104001");
		}
	}


	@Test
	public void testPerformanceSummaryBenchmarkMonthlyReturnMissingPortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(this::testPerformanceSummaryBenchmarkMonthlyReturnMissingRule, "424050");
	}


	@Test
	public void testPerformanceSummaryHistoricalGainLossAdjustmentsRangePortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(() -> testPerformanceSummaryHistoricalGainLossAdjustmentsRangeRuleImpl(" - Portfolio Targets"), "668207", "128000");
	}


	@Test
	public void testPerformanceSummaryMTDCalculatedGainLossVsDWUnmatchedPortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(() -> testPerformanceSummaryMTDCalculatedGainLossVsDWUnmatchedRuleImpl(" - Portfolio Targets"), "078200", "227000");
	}


	@Test
	public void testPerformanceSummaryPositionTransferDuringPeriodPortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(() -> testPerformanceSummaryPositionTransferDuringPeriodRuleImpl(" - Portfolio Targets"), "227500", "050920", "424000");
	}


	@Test
	public void testPortfolioRunIncompletePortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(() -> testPortfolioRunIncompleteRuleImpl(" - Portfolio Targets"), "502040");
	}


	@Test
	public void testPortfolioRunMissingMainRunOnBalanceDatePortfolioTargetsRule() {
		executeTestWithClientOverriddenBusinessProcessingType(() -> testPortfolioRunMissingMainRunOnBalanceDateRuleImpl(" - Portfolio Targets"), "305300", "078300");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////                  Helper Methods                 ////////////////
	/////////////////////////////////////////////////////////////////////////////


	private PerformanceInvestmentAccount getPerformanceInvestmentAccountForAccountAndDate(String accountNumber, Date measureDate, boolean createIfMissing) {
		// Note: Both of these methods throw an exception if they can't find the record
		InvestmentAccount account = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
		AccountingPeriod period = this.accountingPeriodService.getAccountingPeriodForDate(measureDate);

		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setClientAccountId(account.getId());
		searchForm.setAccountingPeriodId(period.getId());
		List<PerformanceInvestmentAccount> summaryList = this.performanceInvestmentAccountService.getPerformanceInvestmentAccountList(searchForm);
		if (CollectionUtils.isEmpty(summaryList)) {
			if (createIfMissing) {
				PerformanceInvestmentAccount performanceInvestmentAccount = new PerformanceInvestmentAccount();
				performanceInvestmentAccount.setAccountingPeriod(period);
				performanceInvestmentAccount.setClientAccount(account);
				this.performanceInvestmentAccountService.savePerformanceInvestmentAccount(performanceInvestmentAccount);
				return performanceInvestmentAccount;
			}
			throw new ValidationException("Cannot find existing performance summary for client account [" + accountNumber + "] and date [" + DateUtils.fromDateShort(measureDate) + "].");
		}
		// Should only be one because of UX
		return CollectionUtils.getOnlyElement(summaryList);
	}


	private void rebuildAccountingPositionDailySnapshotsForSummary(PerformanceInvestmentAccount performanceInvestmentAccount) {
		// Only called explicitly for rules that use this data
		this.accountingUtils.rebuildAccountingPositionDailySnapshot(performanceInvestmentAccount.getClientAccount().getId(), DateUtils.getPreviousWeekday(performanceInvestmentAccount.getAccountingPeriod().getStartDate()), DateUtils.getNextWeekday(performanceInvestmentAccount.getAccountingPeriod().getEndDate()));
	}


	private void rebuildDWSnapshotsForSummary(PerformanceInvestmentAccount performanceInvestmentAccount) {
		// Only used for DW rule that needs this data
		DwRebuildCommand command = new DwRebuildCommand();
		command.setClientAccountId(performanceInvestmentAccount.getClientAccount().getId());
		command.setStartSnapshotDate(performanceInvestmentAccount.getAccountingPeriod().getStartDate());
		command.setEndSnapshotDate(performanceInvestmentAccount.getAccountingPeriod().getEndDate());
		command.setSynchronous(true);
		this.dwRebuildService.rebuildAccountingSnapshots(command);
	}


	private void executeTestWithClientOverriddenBusinessProcessingType(Executable executable, String... clientAccountNumbers) {
		updateClientAccountBusinessServiceProcessingType("Currency Hedge", clientAccountNumbers);
		try {
			executable.execute();
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
		finally {
			updateClientAccountBusinessServiceProcessingType("Overlay", clientAccountNumbers);
		}
	}


	private void updateClientAccountBusinessServiceProcessingType(String processingTypeName, String... clientAccountNumbers) {
		BusinessServiceProcessingType processingType = this.businessServiceService.getBusinessServiceProcessingTypeByName(processingTypeName);
		ArrayUtils.getStream(clientAccountNumbers)
				.forEach(accountNumber -> {
					InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(accountNumber);
					clientAccount.setServiceProcessingType(processingType);
					this.investmentAccountService.saveInvestmentAccount(clientAccount);
				});
	}

	/////////////////////////////////////////////////////////////////////////////
	///////////                Validation Methods                 ///////////////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates Violation with Violation Note exists in the Preview list using "contains" so the text doesn't have to match perfectly
	 */
	private void validateViolationExistsContains(PerformanceInvestmentAccount performanceInvestmentAccount, String violationNoteExpected) {
		validateViolationExistsImpl(performanceInvestmentAccount, violationNoteExpected, true);
	}


	/**
	 * Validates Violation with Violation Note exists in the Preview list
	 */
	private void validateViolationExists(PerformanceInvestmentAccount performanceInvestmentAccount, String violationNoteExpected) {
		validateViolationExistsImpl(performanceInvestmentAccount, violationNoteExpected, false);
	}


	private void validateViolationExistsImpl(PerformanceInvestmentAccount performanceInvestmentAccount, String violationNoteExpected, boolean useContains) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT, performanceInvestmentAccount.getId());
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (useContains && StringUtils.contains(violation.getViolationNote(), violationNoteExpected)) {
				return;
			}
			if (StringUtils.isEqual(violation.getViolationNote(), violationNoteExpected)) {
				return;
			}
		}
		Assertions.fail("Did not find Expected Violation with Message: " + violationNoteExpected + " for Performance Summary " + performanceInvestmentAccount.getLabel());
	}


	/**
	 * Validates Violation with Violation Note exists in the Preview list
	 */
	private void validateViolationExistsRegex(PerformanceInvestmentAccount performanceInvestmentAccount, String violationNoteExpected) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT, performanceInvestmentAccount.getId());
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (violation.getViolationNote().matches(violationNoteExpected)) {
				return;
			}
		}
		Assertions.fail("Did not find Expected Violation with Message Matching: " + violationNoteExpected + " for Performance Summary " + performanceInvestmentAccount.getLabel());
	}


	/**
	 * Validates there are no violations in the preview list for the given rule definition name
	 * Note: exists means would still be in the preview list, with the rule definition name as the violation note, and "Passed" as the ignore note
	 * Otherwise it should be missing from the list because the rule scope is set up to not even run it
	 */
	private void validateViolationPassed(PerformanceInvestmentAccount performanceInvestmentAccount, String definitionName, boolean exists) {
		List<RuleViolation> violationList = this.ruleEvaluatorService.previewRuleViolations(PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT, performanceInvestmentAccount.getId());
		RuleViolation firstRuleViolation = null;
		for (RuleViolation violation : CollectionUtils.getIterable(violationList)) {
			if (definitionName.equals(violation.getRuleAssignment().getRuleDefinition().getName())) {
				firstRuleViolation = violation;
				break;
			}
		}
		if (exists) {
			Assertions.assertNotNull(firstRuleViolation, "Rule Definition: " + definitionName + " for Performance Summary " + performanceInvestmentAccount.getLabel() + " should have been executed, but it wasn't");
			Assertions.assertEquals("Passed", firstRuleViolation.getIgnoreNote(), "Rule Definition: " + definitionName + " for Performance Summary " + performanceInvestmentAccount.getLabel() + " should not have been executed, but found violation with message " + firstRuleViolation.getViolationNote());
		}
		else {
			Assertions.assertNull(firstRuleViolation, "Rule Definition: " + definitionName + " for Performance Summary " + performanceInvestmentAccount.getLabel() + " should not have been executed, but it did");
		}
	}
}
