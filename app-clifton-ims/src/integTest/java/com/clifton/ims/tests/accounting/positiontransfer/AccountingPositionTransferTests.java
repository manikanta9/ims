package com.clifton.ims.tests.accounting.positiontransfer;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.journal.AccountingJournalDetailDefinition;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferDetail;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.accounting.AccountingJournalDetailBuilder;
import com.clifton.ims.tests.accounting.AccountingUtils;
import com.clifton.ims.tests.accounting.GeneralLedgerDescriptions;
import com.clifton.ims.tests.business.BusinessCompanies;
import com.clifton.ims.tests.investment.AccountInfo;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.test.util.ConcurrentExecutionUtils;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;


public class AccountingPositionTransferTests extends BaseImsIntegrationTest {

	@Test
	public void testRoundTurnCommissionWithPositionTransferForFutures() {
		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		this.complianceUtils.createApprovedContractsOldComplianceRule(accountInfo, cch14.getInstrument());

		BigDecimal positionQuantity = new BigDecimal("30");
		//Transfer position into the account
		this.accountingUtils.transferToAccount(accountInfo, cch14, positionQuantity, DateUtils.toDate("12/3/2013"), DateUtils.toDate("12/2/2013"));

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a sell trade
		BigDecimal sellQuantity = new BigDecimal("20");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(cch14, sellQuantity, false, sellDate, executingBroker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> sellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		BigDecimal expectedClearingPrice = new BigDecimal("3.15");
		BigDecimal expectedExecutingPrice = new BigDecimal("1.5");
		BigDecimal expectedCommissionDebit = new BigDecimal("93"); // 4.65 x 20 = 93
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder(sell)
				.security(cch14);

		// Build the expected list of details
		List<AccountingJournalDetailDefinition> expectedListForSell = new ArrayList<>();
		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingPrice)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(MathUtils.multiply(expectedClearingPrice, sellQuantity))
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutingPrice)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(MathUtils.multiply(expectedExecutingPrice, sellQuantity))
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, cch14)
				.build());

		expectedListForSell.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(expectedCommissionDebit.negate())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, cch14)
				.build());

		// Validate the sell trade
		AccountingUtils.assertJournalDetailList(expectedListForSell, sellDetails);
	}


	@Test
	public void testTransferBetweenHoldingAccounts() {
		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);

		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, cch14.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, cch14.getInstrument());
		testTransferBetweenAccounts(fromAccount, toAccount, cch14, null);
	}


	/**
	 * Executes a trade of the provided security for the supplied from account. The position is then transferred to the to account.
	 *
	 * @param fromAccount   the client and holding account to create a position for and transfer from
	 * @param toAccount     the client and holding account to transfer the position to
	 * @param security      the security to open a position for and transfer
	 * @param positionIdSet an optional set that supports concurrent access - used to track which opened positions have been picked up
	 *                      for transfer by a thread to avoid transferring the same position more than once - if null, the first position
	 *                      is selected for transfer, which works fine when single threaded
	 */
	private void testTransferBetweenAccounts(AccountInfo fromAccount, AccountInfo toAccount, InvestmentSecurity security, Set<Long> positionIdSet) {
		// Create a position for 'fromAccount'
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("12/16/2013");
		BigDecimal positionSize = new BigDecimal("40");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(security, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		//Get the position to transfer
		List<AccountingPosition> positionList = this.accountingUtils.getAccountingPositionList(fromAccount, security, openDate);
		AccountingPosition positionToTransfer = positionList.get(0);
		if (positionIdSet != null) {
			for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
				if (positionIdSet.add(position.getId())) {
					positionToTransfer = position;
					break;
				}
			}
		}

		//Transfer the position to the other account
		Date transactionDate = DateUtils.toDate("12/17/2013");
		AccountingPositionTransfer transfer = this.accountingUtils.transferBetweenAccounts(fromAccount, toAccount, positionToTransfer, positionSize, transactionDate, openDate);

		// Build the expected list of details for the transfer
		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.security(security);
		List<AccountingJournalDetailDefinition> expectedTransferDetails = new ArrayList<>();
		BigDecimal expectedClearingPrice = new BigDecimal("3.15");
		BigDecimal expectedExecutingPrice = new BigDecimal("1.5");
		BigDecimal expectedCommissionDebitForTransfer = MathUtils.multiply(MathUtils.add(expectedClearingPrice, expectedExecutingPrice), positionSize);

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(positionSize.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.quantity(positionSize)
				.opening(true)
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingPrice)
				.localDebitCredit(MathUtils.multiply(expectedClearingPrice, positionSize))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutingPrice)
				.localDebitCredit(MathUtils.multiply(expectedExecutingPrice, positionSize))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedTransferDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.localDebitCredit(expectedCommissionDebitForTransfer.negate())
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		List<? extends AccountingJournalDetailDefinition> actualTransferDetails = this.accountingUtils.getPositionTransferDetails(transfer);

		AccountingUtils.assertJournalDetailList(expectedTransferDetails, actualTransferDetails);

		BusinessCompany executingBroker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);

		// Create and execute a sell trade
		BigDecimal sellQuantity = new BigDecimal("1");
		Date sellDate = DateUtils.toDate("12/17/2013");
		Trade sell = this.tradeUtils.newFuturesTradeBuilder(security, sellQuantity, false, sellDate, executingBroker, toAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(sell);

		BigDecimal expectedCommissionDebitForSell = MathUtils.multiply(sellQuantity, new BigDecimal("4.65"));

		//Build the expected list of details for the sell (position close)
		detailBuilder = AccountingJournalDetailBuilder.builder(sell)
				.security(security);
		List<AccountingJournalDetailDefinition> expectedSellDetails = new ArrayList<>();
		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.price(BigDecimal.ONE)
				.quantity(sellQuantity.negate())
				.opening(false)
				.localDebitCredit(BigDecimal.ZERO)
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.PARTIAL_POSITION_CLOSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(expectedClearingPrice)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(MathUtils.multiply(expectedClearingPrice, sellQuantity))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(expectedExecutingPrice)
				.quantity(sellQuantity)
				.opening(true)
				.localDebitCredit(MathUtils.multiply(expectedExecutingPrice, sellQuantity))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		expectedSellDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CASH))
				.security(this.usd)
				.price(null)
				.quantity(null)
				.opening(true)
				.localDebitCredit(expectedCommissionDebitForSell.negate())
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.description(GeneralLedgerDescriptions.CASH_EXPENSE_FROM_CLOSE, security)
				.executingCompany(this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO))
				.build());

		// Get the list of details for the trade after it is executed
		List<? extends AccountingJournalDetailDefinition> actualSellDetails = this.accountingUtils.getFirstTradeFillDetails(sell, true);

		// Validate the sell (position close)
		AccountingUtils.assertJournalDetailList(expectedSellDetails, actualSellDetails);
	}


	@Test
	public void testTransferWithCommissionOverride() {

		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");

		// Create new accounts
		AccountInfo accountInfo = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		this.complianceUtils.createApprovedContractsOldComplianceRule(accountInfo, cch14.getInstrument());

		// Create a position for the account
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("11/5/2013");
		BigDecimal positionSize = new BigDecimal("30");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(cch14, positionSize, true, openDate, broker, accountInfo).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		AccountingTransaction positionOpeningDetail = (AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0);

		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		Date transactionDate = DateUtils.toDate("12/3/2013");

		transfer.setFromClientInvestmentAccount(accountInfo.getClientAccount());
		transfer.setFromHoldingInvestmentAccount(accountInfo.getHoldingAccount());
		transfer.setType(this.accountingUtils.getAccountingPositionTransferType(AccountingUtils.FROM_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(transactionDate);
		transfer.setNote("Transfer to client account " + accountInfo.getClientAccount().getName() + " and holding account " + accountInfo.getHoldingAccount().getName());

		List<AccountingPositionTransferDetail> transferDetails = new ArrayList<>();

		//Create a position transfer detail of 10 CCH14 with a commission override of 7.0
		BigDecimal commissionOverride = new BigDecimal("7.0");
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-10);
		transferDetail.setExistingPosition(positionOpeningDetail);
		transferDetail.setSecurity(cch14);
		transferDetail.setPositionCostBasis(new BigDecimal("10000"));
		transferDetail.setCostPrice(BigDecimal.ONE);
		transferDetail.setTransferPrice(transferDetail.getCostPrice());
		transferDetail.setQuantity(new BigDecimal(10));
		transferDetail.setExchangeRateToBase(BigDecimal.ONE);
		transferDetail.setOriginalPositionOpenDate(DateUtils.toDate("12/2/2013"));
		transferDetail.setCommissionPerUnitOverride(commissionOverride);
		transferDetails.add(transferDetail);

		//Create another position transfer detail of 10 CCH with no commission override
		transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-20);
		transferDetail.setExistingPosition(positionOpeningDetail);
		transferDetail.setSecurity(cch14);
		transferDetail.setPositionCostBasis(new BigDecimal("10000"));
		transferDetail.setCostPrice(BigDecimal.ONE);
		transferDetail.setTransferPrice(transferDetail.getCostPrice());
		transferDetail.setQuantity(new BigDecimal(10));
		transferDetail.setExchangeRateToBase(BigDecimal.ONE);
		transferDetail.setOriginalPositionOpenDate(DateUtils.toDate("11/28/2013"));
		transferDetails.add(transferDetail);

		transfer.setDetailList(transferDetails);
		transfer = this.accountingUtils.saveAccountingPositionTransfer(transfer);
		transfer = this.accountingUtils.bookAndPost(transfer);

		List<? extends AccountingJournalDetailDefinition> actualJournalDetails = this.accountingUtils.getJournal(transfer).getJournalDetailList();

		AccountingUtils.assertJournalDetailList(actualJournalDetails, new String[]{
				"16346016	16346015	CA618970358	HA1151917010	Position	CCH14	1	-10	0	12/03/2013	1	0	-100	11/05/2013	12/03/2013	Position close from transfer to external account",
				"16346017	16346015	CA618970358	HA1151917010	Position	CCH14	1	-10	0	12/03/2013	1	0	-100	11/05/2013	12/03/2013	Position close from transfer to external account",
				"16346018	16346016	CA618970358	HA1151917010	Commission	CCH14	7	10	70	12/03/2013	1	70	0	11/05/2013	12/03/2013	Commission expense for CCH14",
				"16346019	16346016	CA618970358	HA1151917010	Cash	USD			-70	12/03/2013	1	-70	0	11/05/2013	12/03/2013	Cash expense from close of CCH14",
				"16346020	16346017	CA618970358	HA1151917010	Clearing Commission	CCH14	3.15	10	31.5	12/03/2013	1	31.5	0	11/05/2013	12/03/2013	Clearing Commission expense for CCH14",
				"16346021	16346017	CA618970358	HA1151917010	Executing Commission	CCH14	1.5	10	15	12/03/2013	1	15	0	11/05/2013	12/03/2013	Executing Commission expense for CCH14",
				"16346022	16346017	CA618970358	HA1151917010	Cash	USD			-46.5	12/03/2013	1	-46.5	0	11/05/2013	12/03/2013	Cash expense from close of CCH14"
		}, false);
	}


	@Test
	public void testTransferOfForeignFuturePositionBetweenAccounts() {
		InvestmentSecurity zm4 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("Z M4");

		// Create new accounts
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);

		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, zm4.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, zm4.getInstrument());

		// Create a position for the account
		BusinessCompany broker = this.businessUtils.getBusinessCompany(BusinessCompanies.GOLDMAN_SACHS_AND_CO);
		Date openDate = DateUtils.toDate("5/23/2014");
		BigDecimal positionSize = new BigDecimal("60");
		Trade buyTrade = this.tradeUtils.newFuturesTradeBuilder(zm4, positionSize, true, openDate, broker, fromAccount).buildAndSave();
		this.tradeUtils.fullyExecuteTrade(buyTrade);

		//Validate the position open
		this.accountingUtils.assertPositionOpen(buyTrade);

		AccountingTransaction positionOpeningDetail = (AccountingTransaction) this.accountingUtils.getFirstTradeFillDetails(buyTrade, true).get(0);

		AccountingPositionTransfer transfer = new AccountingPositionTransfer();
		Date transactionDate = DateUtils.toDate("5/27/2014");


		transfer.setFromClientInvestmentAccount(fromAccount.getClientAccount());
		transfer.setFromHoldingInvestmentAccount(fromAccount.getHoldingAccount());
		transfer.setToClientInvestmentAccount(toAccount.getClientAccount());
		transfer.setToHoldingInvestmentAccount(toAccount.getHoldingAccount());
		transfer.setType(this.accountingUtils.getAccountingPositionTransferType(AccountingUtils.BETWEEN_ACCOUNTING_POSITION_TRANSFER_TYPE_NAME));
		transfer.setTransactionDate(transactionDate);
		transfer.setSettlementDate(transactionDate);
		transfer.setNote("Transfer from client account " + fromAccount.getClientAccount().getName() + " and holding account " + fromAccount.getHoldingAccount().getName() + "to client account "
				+ toAccount.getClientAccount().getName() + " and holding account " + toAccount.getHoldingAccount().getName());

		List<AccountingPositionTransferDetail> transferDetails = new ArrayList<>();

		BigDecimal transferPrice = new BigDecimal("6829.50");
		//Create another position transfer detail of 10 CCH with no commission override
		AccountingPositionTransferDetail transferDetail = new AccountingPositionTransferDetail();
		transferDetail.setId(-10);
		transferDetail.setExistingPosition(positionOpeningDetail);
		transferDetail.setSecurity(zm4);
		//transferDetail.setPositionCostBasis(new BigDecimal("10000"));
		transferDetail.setCostPrice(new BigDecimal("6553.50"));
		transferDetail.setTransferPrice(transferPrice);
		transferDetail.setQuantity(positionSize);
		transferDetail.setExchangeRateToBase(new BigDecimal("1.65"));
		transferDetail.setOriginalPositionOpenDate(openDate);
		transferDetails.add(transferDetail);

		transfer.setDetailList(transferDetails);

		transfer = this.accountingUtils.saveAccountingPositionTransfer(transfer);
		transfer = this.accountingUtils.bookAndPost(transfer);

		AccountingJournalDetailBuilder detailBuilder = AccountingJournalDetailBuilder.builder()
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.transactionDate(transactionDate);

		List<AccountingJournalDetailDefinition> expectedJournalDetails = new ArrayList<>();

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.security(zm4)
				.price(transferPrice)
				.quantity(positionSize.negate())
				.opening(false)
				.description(GeneralLedgerDescriptions.POSITION_CLOSE_FROM_TRANSFER, toAccount.getClientAccount())
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_POSITION))
				.clientInvestmentAccount(toAccount.getClientAccount())
				.holdingInvestmentAccount(toAccount.getHoldingAccount())
				.quantity(positionSize)
				.opening(true)
				.description(GeneralLedgerDescriptions.POSITION_OPENING_FROM_TRANSFER, fromAccount.getClientAccount())
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.REVENUE_REALIZED))
				.clientInvestmentAccount(fromAccount.getClientAccount())
				.holdingInvestmentAccount(fromAccount.getHoldingAccount())
				.localDebitCredit(new BigDecimal("-4097100"))
				.baseDebitCredit(new BigDecimal("-6760215"))
				.opening(false)
				.description(GeneralLedgerDescriptions.REALIZED_GAIN_LOSS_GAIN, zm4)
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.CLEARING_COMMISSION_EXPENSE))
				.price(new BigDecimal("0.6"))
				.localDebitCredit(new BigDecimal("36"))
				.baseDebitCredit(new BigDecimal("59.4"))
				.opening(true)
				.description(GeneralLedgerDescriptions.CLEARING_COMMISSION_EXPENSE, zm4)
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.EXECUTING_COMMISSION_EXPENSE))
				.price(new BigDecimal("0.48"))
				.localDebitCredit(new BigDecimal("28.8"))
				.baseDebitCredit(new BigDecimal("47.52"))
				.opening(true)
				.description(GeneralLedgerDescriptions.EXECUTING_COMMISSION_EXPENSE, zm4)
				.build());

		expectedJournalDetails.add(detailBuilder.accountingAccount(this.accountingUtils.getAccountingAccount(AccountingAccount.ASSET_CURRENCY))
				.security(this.investmentInstrumentUtils.getCurrencyBySymbol("GBP"))
				.price(null)
				.quantity(null)
				.localDebitCredit(new BigDecimal("4097035.2"))
				.baseDebitCredit(new BigDecimal("6760108.08"))
				.description(GeneralLedgerDescriptions.CURRENCY_PROCEEDS_FROM_CLOSE, zm4)
				.build());

		List<? extends AccountingJournalDetailDefinition> actualJournalDetails = this.accountingUtils.getJournal(transfer).getJournalDetailList();

		AccountingUtils.assertJournalDetailList(expectedJournalDetails, actualJournalDetails);
	}


	/**
	 * Uses concurrent threads, each creating a trade for an account and then transferring the position to another account. Each thread uses the same accounts.
	 */
	@Test
	public void tradeAndTransferBetweenSameClientsConcurrencyTest() {
		AccountInfo fromAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.GOLDMAN_SACHS_AND_CO, this.futures);
		AccountInfo toAccount = this.investmentAccountUtils.createAccountsForTrading(BusinessCompanies.MORGAN_STANLEY_AND_CO_INC, this.futures);

		InvestmentSecurity cch14 = this.investmentInstrumentUtils.getInvestmentSecurityBySymbol("CCH14");
		this.complianceUtils.createApprovedContractsOldComplianceRule(fromAccount, cch14.getInstrument());
		this.complianceUtils.createApprovedContractsOldComplianceRule(toAccount, cch14.getInstrument());

		Set<Long> positionIdSet = CollectionUtils.newConcurrentHashSet();
		testTransferConcurrently(() -> {
			testTransferBetweenAccounts(fromAccount, toAccount, cch14, positionIdSet);
			return null;
		}, "Failed to obtain lock");
	}


	/**
	 * Uses concurrent threads, each creating a trade for an account and then transferring the position to another account. Each thread uses a different set of accounts.
	 */
	@Test
	public void tradeAndTransferBetweenDifferentClientsConcurrencyTest() {
		testTransferConcurrently(() -> {
			testTransferBetweenHoldingAccounts();
			return null;
		});
	}


	private void testTransferConcurrently(Callable<Void> action, String... ignoredErrorMessageFragments) {
		List<ConcurrentExecutionUtils.ConcurrentExecutionResponse<Void>> results = ConcurrentExecutionUtils.executeConcurrently(2, 4, action);
		List<String> errors = new ArrayList<>();
		Set<String> ignoredErrorMessageFragmentSet = CollectionUtils.createHashSet("deadlock");
		ArrayUtils.getStream(ignoredErrorMessageFragments).forEach(ignoredErrorMessageFragmentSet::add);
		results.forEach(result -> {
			if (!result.isSuccessful()) {
				String message = ExceptionUtils.getOriginalMessage(result.getError());
				boolean ignoreError = false;
				for (String ignoredErrorMessageFragment : ignoredErrorMessageFragmentSet) {
					if (message.contains(ignoredErrorMessageFragment)) {
						ignoreError = true;
						break;
					}
				}
				if (!ignoreError) {
					errors.add(message);
				}
			}
		});
		Assertions.assertEquals(0, errors.size(), "Expected 0 failed transfers, but " + errors.size() + " of " + results.size() + " failed. Errors: " + StringUtils.join(errors, "\n"));
	}
}
