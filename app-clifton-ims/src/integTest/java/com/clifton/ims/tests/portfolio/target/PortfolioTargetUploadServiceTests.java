package com.clifton.ims.tests.portfolio.target;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.ims.tests.builders.investment.replication.InvestmentReplicationBuilder;
import com.clifton.ims.tests.investment.replication.InvestmentReplicationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.calculator.PortfolioTargetHoldingsCalculator;
import com.clifton.portfolio.target.upload.PortfolioTargetUploadCommand;
import com.clifton.portfolio.target.upload.PortfolioTargetUploadService;
import com.clifton.system.bean.SystemBeanUtils;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * {@link PortfolioTargetUploadServiceTests} verifies the functionality in {@link com.clifton.portfolio.target.upload.PortfolioTargetUploadService}.
 *
 * @author michaelm
 */
public class PortfolioTargetUploadServiceTests extends BaseImsIntegrationTest {

	// File Locations
	private static final String SIMPLE_PORTFOLIO_TARGET_UPLOAD_FILE_PATH = "src/integTest/java/com/clifton/ims/tests/portfolio/target/upload/TestSimplePortfolioTargetUploadFile.xls";
	// Entity Specific Info
	private static final String CLIENT_ACCOUNT_NUMBER_STRING = "800020";
	private static final Date TARGET_START_DATE = DateUtils.toDate("10/06/2021");
	// System Bean Names
	private static final String BEAN_NAME_ACCOUNTING_POSITION_MARKET_VALUE = "Accounting Position Market Value Calculator";

	@Resource
	private InvestmentGroupService investmentGroupService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private PortfolioTargetUploadService portfolioTargetUploadService;

	@Resource
	private PortfolioTargetService portfolioTargetService;

	@Resource
	private InvestmentReplicationUtils investmentReplicationUtils;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPortfolioTargetUpload_DefensiveEquityAccountWithMatching() {
		PortfolioTargetHoldingsCalculator holdingsCalculator = new PortfolioTargetHoldingsCalculator();
		holdingsCalculator.setPositionValueCalculatorBean(this.systemBeanService.getSystemBeanByName(BEAN_NAME_ACCOUNTING_POSITION_MARKET_VALUE));
		SystemBeanUtils.getOrCreateSystemBeanUsingImplementation(this.systemBeanService, "TPV Holdings Calculator", holdingsCalculator);

		holdingsCalculator.setInvestmentSecurityIds(new Integer[]{this.investmentInstrumentService.getInvestmentSecurityBySymbol("USD", true).getId()});
		SystemBeanUtils.getOrCreateSystemBeanUsingImplementation(this.systemBeanService, "Cash Holdings Calculator", holdingsCalculator);

		holdingsCalculator.setInvestmentSecurityIds(new Integer[]{this.investmentInstrumentService.getInvestmentSecurityBySymbol("IVV", false).getId(), this.investmentInstrumentService.getInvestmentSecurityBySymbol("VOO", false).getId()});
		SystemBeanUtils.getOrCreateSystemBeanUsingImplementation(this.systemBeanService, "Equity Holdings (IVV and VOO) Calculator", holdingsCalculator);

		holdingsCalculator.setInvestmentSecurityIds(null);
		holdingsCalculator.setInvestmentGroupIds(new Short[]{this.investmentGroupService.getInvestmentGroupByName("Bonds").getId()});
		SystemBeanUtils.getOrCreateSystemBeanUsingImplementation(this.systemBeanService, "Fixed Income Holdings Calculator", holdingsCalculator);

		holdingsCalculator.setInvestmentGroupIds(new Short[]{this.investmentGroupService.getInvestmentGroupByName("Options").getId()});
		SystemBeanUtils.getOrCreateSystemBeanUsingImplementation(this.systemBeanService, "Options Market Value Holdings Calculator", holdingsCalculator);

		// DE S&P 500 Call Options
		InvestmentReplicationBuilder.ofReplicationUtilsAndName(this.investmentReplicationUtils, "DE S&P 500 Call Options")
				.withAllocationList(CollectionUtils.createList(this.investmentReplicationUtils.createSecurityUnderlyingReplicationAllocation("SPX", "Call Options")))
				.withInactive(true)
				.checkForExistingBuildAndSave();

		InvestmentAccount clientAccount = this.investmentAccountUtils.getClientAccountByNumber(CLIENT_ACCOUNT_NUMBER_STRING);

		PortfolioTargetUploadCommand bean = createTargetUploadCommand(SIMPLE_PORTFOLIO_TARGET_UPLOAD_FILE_PATH);
		this.portfolioTargetUploadService.uploadPortfolioTargetUploadFile(bean);
		List<PortfolioTarget> portfolioTargetList = this.portfolioTargetService.getPortfolioTargetListByClientAccount(clientAccount.getId(), TARGET_START_DATE, true);
		PortfolioTarget cashEquivalentsTarget = CollectionUtils.getStream(portfolioTargetList).filter(target -> target.getName().equals("Cash Equivalents")).findFirst().orElse(null);
		ValidationUtils.assertTrue(CollectionUtils.getSize(cashEquivalentsTarget.getChildTargetList()) > 2, "Cash Equivalents Target should have at least 3 children.");
		ValidationUtils.assertNotNull(cashEquivalentsTarget.getMatchingTarget(), "Cash Equivalents Target should have a Matching Target");
		ValidationUtils.assertEquals(cashEquivalentsTarget.getMatchingTarget().getName(), "Total Portfolio Value", "Cash Equivalents Target should be matching Total Portfolio Value Target.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PortfolioTargetUploadCommand createTargetUploadCommand(String filePath) {
		PortfolioTargetUploadCommand uploadCommand = new PortfolioTargetUploadCommand();
		uploadCommand.setFile(new MultipartFileImpl(filePath));
		uploadCommand.setOverlappingTargetOption(PortfolioTargetUploadCommand.OverlappingTargetOptions.OVERWRITE);
		return uploadCommand;
	}
}
