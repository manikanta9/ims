package com.clifton.ims.tests.compliance;

import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author stevenf
 */

public class ComplianceCurrentContractRuleTests extends ComplianceRealTimeTradeTests {

	@Resource
	private RuleEvaluatorService ruleEvaluatorService;


	@Test
	public void testCurrentContractsRule() {
		Trade trade = this.tradeUtils.copyTrade(484630, BigDecimal.ONE, new Date(), true);
		try {
			List<RuleViolation> tradeResults = this.ruleEvaluatorService.previewRuleViolations("Trade", trade.getId())
					.stream().filter(ruleViolation -> ruleViolation.getIgnoreNote() == null).collect(Collectors.toList());
			String violationNote = "This trade will open positions and the security MFS[A-Z][0-9]+ is not the current security for instrument MSCI EAFE Mini Futures \\(MFS\\) \\(Equities \\/ Futures \\/ Index\\). The current security is MFS[A-Z][0-9]+";
			validateViolationNotes(tradeResults, true, violationNote);
		}
		finally {
			this.tradeUtils.cancelTrade(trade);
		}
	}
}
