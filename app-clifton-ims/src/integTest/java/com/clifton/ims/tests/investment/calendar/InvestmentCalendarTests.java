package com.clifton.ims.tests.investment.calendar;

import com.clifton.business.service.BusinessService;
import com.clifton.calendar.schedule.CalendarSchedule;
import com.clifton.calendar.schedule.CalendarScheduleService;
import com.clifton.calendar.schedule.ScheduleFrequencies;
import com.clifton.calendar.schedule.search.CalendarScheduleTypeSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.tests.BaseImsIntegrationTest;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.test.protocol.RequestOverrideHandler;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;


/**
 * @author MitchellF
 */
public class InvestmentCalendarTests extends BaseImsIntegrationTest {

	@Resource
	private InvestmentCalendarService investmentCalendarService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	@Resource
	private CalendarScheduleService calendarScheduleService;


	@Test
	public void testCompletedRecurrenceNotAllowed() {
		Date today = new Date();

		CalendarSchedule schedule = new CalendarSchedule();
		schedule.setStartDate(new Date());
		schedule.setStartTime(new Time(today));
		schedule.setEndDate(DateUtils.addDays(today, 7));
		schedule.setFrequency(ScheduleFrequencies.DAILY);
		schedule.setName("Daily101");
		schedule.setDescription("Default");
		schedule.setRecurrence(1);
		CalendarScheduleTypeSearchForm sf = new CalendarScheduleTypeSearchForm();
		sf.setName("Standard Schedules");
		schedule.setCalendarScheduleType(CollectionUtils.getOnlyElement(this.calendarScheduleService.getCalendarScheduleTypeList(sf)));
		this.calendarScheduleService.saveCalendarSchedule(schedule);

		InvestmentEvent baseEvent = new InvestmentEvent();
		baseEvent.setEventType(this.investmentCalendarService.getInvestmentEventTypeByName("Contact Client Reminder"));
		baseEvent.setInvestmentAccount(this.investmentAccountUtils.getClientAccountByNumber("W-013246"));

		// Set event to completed - should be overridden in save
		baseEvent.setCompletedEvent(true);
		baseEvent.setRecurrenceSchedule(schedule);

		InvestmentEvent finalBaseEvent = baseEvent;
		baseEvent = RequestOverrideHandler.serviceCallWithOverrides(() -> this.investmentCalendarService.saveInvestmentEvent(finalBaseEvent, true),
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentEvent.class, false, "coalesceTeam", "eventDate"),
				new RequestOverrideHandler.EntityPropertyOverrides(InvestmentAccount.class, false, "aumCalculatorBean"),
				new RequestOverrideHandler.EntityPropertyOverrides(BusinessService.class, false, "aumCalculatorBean"));

		ValidationUtils.assertFalse(baseEvent.isCompletedEvent(), "Series definition was saved as Completed Event.");


		// run through WF transitions
		Workflow wf = this.workflowDefinitionService.getWorkflowByName("Investment Calendar Workflow (Trading)");
		WorkflowState pending = this.workflowDefinitionService.getWorkflowStateByName(wf.getId(), "Pending Approval");
		this.workflowTransitionService.executeWorkflowTransition("InvestmentEvent", baseEvent.getId(), pending.getId());
		WorkflowState approved = this.workflowDefinitionService.getWorkflowStateByName(wf.getId(), "Approved");
		this.workflowTransitionService.executeWorkflowTransition("InvestmentEvent", baseEvent.getId(), approved.getId());

		// Going to approved state should trigger recurrences to be generated
		// ensure no longer marked as completed
		InvestmentEvent savedEvent = this.investmentCalendarService.getInvestmentEvent(baseEvent.getId());
		ValidationUtils.assertFalse(savedEvent.isCompletedEvent(), "Event was not overridden to non-completed.");

		// verify children are not completed.
		InvestmentEventSearchForm sf2 = new InvestmentEventSearchForm();
		sf2.setSeriesDefinitionId(savedEvent.getId());
		List<InvestmentEvent> children = this.investmentCalendarService.getInvestmentEventList(sf2);
		ValidationUtils.assertNotEmpty(children, "Recurrences were not generated.");
		for (InvestmentEvent child : CollectionUtils.getIterable(children)) {
			ValidationUtils.assertFalse(child.isCompletedEvent(), "Child event was created completed.");
		}
	}
}
