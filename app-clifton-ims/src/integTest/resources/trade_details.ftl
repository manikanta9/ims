[
<#list details as detail>
	[
		"accountingAccount": ["name": "${detail.accountingAccount.name}"],
		"description": "${detail.description}",
		"clientInvestmentAccount": ["number": "${clientAccountNumber_varName}"], "holdingInvestmentAccount": ["number": "${holdingAccountNumber_varName}"],
		"investmentSecurity": ["symbol": "${detail.investmentSecurity.symbol}"], "quantity": ${detail.quantity!"null"}, "price": ${detail.price!"null"},
		"localDebitCredit": ${detail.localDebitCredit}, "baseDebitCredit": ${detail.baseDebitCredit}, "positionCostBasis": ${detail.positionCostBasis}, "exchangeRateToBase": ${detail.exchangeRateToBase},
		"transactionDate": DateUtils.toDate("${detail.transactionDate?string("MM/dd/yyyy")}"), "settlementDate": DateUtils.toDate("${detail.settlementDate?string("MM/dd/yyyy")}"), "originalTransactionDate": DateUtils.toDate("${detail.originalTransactionDate?string("MM/dd/yyyy")}")
	]<#if detail_has_next>,</#if>
</#list>
]