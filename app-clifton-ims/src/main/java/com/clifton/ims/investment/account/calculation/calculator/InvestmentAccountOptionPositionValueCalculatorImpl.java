package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>InvestmentAccountOptionPositionValueCalculatorImpl</code> provides calculations specific to options.
 * These include grouping puts and calls together and positions are limited to Options investment type only.
 * <p>
 *
 * @author manderson
 */
public class InvestmentAccountOptionPositionValueCalculatorImpl extends InvestmentAccountPositionValueCalculatorImpl {

	private InvestmentSetupService investmentSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Gross vs. Net total of Puts vs. Calls
	 */
	private boolean useGrossSummation;

	/**
	 * If true - will include in the final value the larger of the ABS of Net Puts or Net Calls
	 * Otherwise will include the ABS(Net Puts) + ABS(Net Calls)
	 */
	private boolean usePutOrCall;

	/**
	 * Used so put and call securities can be grouped together under meaningful parent details
	 */
	private Short putOptionAssetClassId;
	private Short callOptionAssetClassId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		super.validate();
		ValidationUtils.assertNotNull(getPutOptionAssetClassId(), "Put Option Asset Class selection is required");
		ValidationUtils.assertNotNull(getCallOptionAssetClassId(), "Call Option Asset Class selection is required");
	}


	@Override
	protected List<AccountingPositionDaily> getAccountingPositionDailyLiveList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<AccountingPositionDaily> positionDailyList = super.getAccountingPositionDailyLiveList(snapshot, context);
		// Limit to Options Only
		return BeanUtils.filter(positionDailyList, positionDaily -> InvestmentUtils.isSecurityOfType(positionDaily.getInvestmentSecurity(), InvestmentType.OPTIONS));
	}


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentAccountCalculationSnapshotDetail> detailList = super.calculateInvestmentAccountCalculationSnapshotDetailList(snapshot, context);
		applyInvestmentAccountCalculationSnapshotDetailListGrouping(snapshot, detailList);
		return detailList;
	}


	protected void applyInvestmentAccountCalculationSnapshotDetailListGrouping(InvestmentAccountCalculationSnapshot snapshot, List<InvestmentAccountCalculationSnapshotDetail> detailList) {
		List<InvestmentAccountCalculationSnapshotDetail> putDetailList = new ArrayList<>();
		List<InvestmentAccountCalculationSnapshotDetail> callDetailList = new ArrayList<>();

		for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(detailList)) {
			if (detail.getInvestmentSecurity().isPutOption()) {
				putDetailList.add(detail);
			}
			else {
				callDetailList.add(detail);
			}
		}

		InvestmentAccountCalculationSnapshotDetail putTotalDetail = null;
		InvestmentAccountCalculationSnapshotDetail callTotalDetail = null;
		if (!CollectionUtils.isEmpty(putDetailList)) {
			putTotalDetail = InvestmentAccountCalculationSnapshotDetail.ofAssetClass(snapshot, getInvestmentSetupService().getInvestmentAssetClass(getPutOptionAssetClassId()));
			putTotalDetail.setPreliminaryCalculatedValue(CoreMathUtils.sumProperty(putDetailList, InvestmentAccountCalculationSnapshotDetail::getCalculatedValue));
			putTotalDetail.setCalculatedValue(MathUtils.abs(putTotalDetail.getPreliminaryCalculatedValue()));

			detailList.add(putTotalDetail);
			for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(putDetailList)) {
				detail.setInvestmentAssetClass(putTotalDetail.getInvestmentAssetClass());
				detail.setParent(putTotalDetail);
			}
		}

		if (!CollectionUtils.isEmpty(callDetailList)) {
			callTotalDetail = InvestmentAccountCalculationSnapshotDetail.ofAssetClass(snapshot, getInvestmentSetupService().getInvestmentAssetClass(getCallOptionAssetClassId()));
			callTotalDetail.setPreliminaryCalculatedValue(CoreMathUtils.sumProperty(callDetailList, InvestmentAccountCalculationSnapshotDetail::getCalculatedValue));
			callTotalDetail.setCalculatedValue(MathUtils.abs(callTotalDetail.getPreliminaryCalculatedValue()));

			detailList.add(callTotalDetail);
			for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(callDetailList)) {
				detail.setInvestmentAssetClass(callTotalDetail.getInvestmentAssetClass());
				detail.setParent(callTotalDetail);
			}
		}

		// If we are only taking the larger of the 2, zero the other one out
		if (isUsePutOrCall()) {
			if (putTotalDetail != null && callTotalDetail != null) {
				if (MathUtils.isGreaterThan(putTotalDetail.getCalculatedValue(), callTotalDetail.getCalculatedValue())) {
					callTotalDetail.setCalculatedValue(BigDecimal.ZERO);
					for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(callDetailList)) {
						detail.setCalculatedValue(BigDecimal.ZERO);
					}
				}
				else {
					putTotalDetail.setCalculatedValue(BigDecimal.ZERO);
					for (InvestmentAccountCalculationSnapshotDetail detail : CollectionUtils.getIterable(putDetailList)) {
						detail.setCalculatedValue(BigDecimal.ZERO);
					}
				}
			}
		}
	}


	/**
	 * Options are already grouped into Puts and Calls, so only other options are if the Puts and Calls are the net or gross total of their securities
	 */
	@Override
	@ValueIgnoringGetter
	public PositionSummationTypes getPositionSummationType() {
		return (isUseGrossSummation() ? PositionSummationTypes.GROSS : PositionSummationTypes.NET);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public boolean isUsePutOrCall() {
		return this.usePutOrCall;
	}


	public void setUsePutOrCall(boolean usePutOrCall) {
		this.usePutOrCall = usePutOrCall;
	}


	public Short getPutOptionAssetClassId() {
		return this.putOptionAssetClassId;
	}


	public void setPutOptionAssetClassId(Short putOptionAssetClassId) {
		this.putOptionAssetClassId = putOptionAssetClassId;
	}


	public Short getCallOptionAssetClassId() {
		return this.callOptionAssetClassId;
	}


	public void setCallOptionAssetClassId(Short callOptionAssetClassId) {
		this.callOptionAssetClassId = callOptionAssetClassId;
	}


	public boolean isUseGrossSummation() {
		return this.useGrossSummation;
	}


	public void setUseGrossSummation(boolean useGrossSummation) {
		this.useGrossSummation = useGrossSummation;
	}
}
