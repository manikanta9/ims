package com.clifton.ims.investment.account.wizard;

import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.investment.account.wizard.generator.InvestmentAccountWizardGenerator;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class InvestmentAccountWizardServiceImpl implements InvestmentAccountWizardService {

	private SystemBeanService systemBeanService;

	private SystemConditionService systemConditionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SystemBean> getInvestmentAccountWizardList(SystemBeanSearchForm searchForm) {
		// Track start and page size for the company filter
		int start = searchForm.getStart();
		int pageSize = searchForm.getLimit();

		// Clear values for the actual query
		searchForm.setStart(PagingCommand.DEFAULT_START);
		searchForm.setLimit(PagingCommand.DEFAULT_LIMIT);

		// There will likely just be a handful of conditions, so keep track of them so we don't have to keep re-evaluating them
		Map<Integer, String> conditionEvaluatorFalseMessageMap = new HashMap<>();
		List<SystemBean> beanList = getSystemBeanService().getSystemBeanList(searchForm);
		List<SystemBean> result = CollectionUtils.getFiltered(beanList, systemBean -> isInvestmentAccountWizardUseAllowed(systemBean, conditionEvaluatorFalseMessageMap, false));

		// Reset Limits on the Search Form to what they were
		searchForm.setStart(start);
		searchForm.setLimit(pageSize);

		// Need to return PagingArrayList with proper start/size/page size so filtering in UI works properly
		// If more than one page of data
		int size = CollectionUtils.getSize(result);
		if (size > pageSize) {
			int maxIndex = start + pageSize;
			if (maxIndex > size) {
				maxIndex = size;
			}
			// Filter actual rows returned to the current page
			result = result.subList(start, maxIndex);
		}
		// Return Paging Array List with (if more than one page, then list is already a subset of data, else all the data), start index and the total size of the original list without paging
		return new PagingArrayList<>(result, start, size);
	}


	private boolean isInvestmentAccountWizardUseAllowed(SystemBean wizardGeneratorBean, Map<Integer, String> conditionEvaluatorFalseMessageMap, boolean throwException) {
		if (conditionEvaluatorFalseMessageMap == null) {
			conditionEvaluatorFalseMessageMap = new HashMap<>();
		}
		String falseMessage = null;
		InvestmentAccountWizardGenerator generator = (InvestmentAccountWizardGenerator) getSystemBeanService().getBeanInstance(wizardGeneratorBean);
		Integer systemConditionId = generator.getInvestmentAccountWizardUseConditionId();
		if (systemConditionId != null) {
			falseMessage = conditionEvaluatorFalseMessageMap.computeIfAbsent(systemConditionId, key -> getSystemConditionEvaluationHandler().getConditionFalseMessage(getSystemConditionService().getSystemCondition(key), wizardGeneratorBean));
		}
		if (StringUtils.isEmpty(falseMessage)) {
			return true;
		}
		if (throwException) {
			throw new ValidationException("Cannot use investment account wizard generator [" + wizardGeneratorBean.getLabel() + "] because: " + falseMessage);
		}
		return false;
	}


	@Override
	public InvestmentAccountWizard getInvestmentAccountWizard(int generatorBeanId) {
		ValidationUtils.assertNotNull(generatorBeanId, "Missing generator bean");
		SystemBean generatorBean = getSystemBeanService().getSystemBean(generatorBeanId);
		isInvestmentAccountWizardUseAllowed(generatorBean, null, true);
		InvestmentAccountWizard investmentAccountWizard = new InvestmentAccountWizard();
		investmentAccountWizard.setWizardGeneratorBean(generatorBean);
		InvestmentAccountWizardGenerator generator = (InvestmentAccountWizardGenerator) getSystemBeanService().getBeanInstance(generatorBean);
		return generator.populateInvestmentAccountWizard(investmentAccountWizard);
	}


	@Transactional
	@Override
	public InvestmentAccountWizard saveInvestmentAccountWizard(InvestmentAccountWizard investmentAccountWizard, boolean ignoreValidation) {
		ValidationUtils.assertNotNull(investmentAccountWizard.getWizardGeneratorBean(), "Missing generator bean");
		isInvestmentAccountWizardUseAllowed(investmentAccountWizard.getWizardGeneratorBean(), null, true);
		InvestmentAccountWizardGenerator generator = (InvestmentAccountWizardGenerator) getSystemBeanService().getBeanInstance(investmentAccountWizard.getWizardGeneratorBean());
		investmentAccountWizard = generator.generateInvestmentAccountWizard(investmentAccountWizard, ignoreValidation);
		return investmentAccountWizard;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
