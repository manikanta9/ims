package com.clifton.ims.investment.account.termination.processor;

import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.balance.removal.AccountingBalanceRemovalCommand;
import com.clifton.accounting.gl.balance.removal.AccountingBalanceRemovalService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.search.BillingDefinitionInvestmentAccountSearchForm;
import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.ims.investment.account.termination.InvestmentAccountTerminationCommand;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.relationship.search.InvestmentAccountRelationshipSearchForm;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public class InvestmentAccountTerminationProcessorImpl implements InvestmentAccountTerminationProcessor, ValidationAware {

	private AccountingBalanceRemovalService accountingBalanceRemovalService;
	private AccountingTransactionService accountingTransactionService;

	private BillingDefinitionService billingDefinitionService;

	private BusinessClientService businessClientService;
	private BusinessContractService businessContractService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	private SystemNoteService systemNoteService;
	private SystemSchemaService systemSchemaService;

	private TradeRestrictionService tradeRestrictionService;

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;


	////////////////////////////////////////////////////////////////////////////////

	private String clientRelationshipTerminatedWorkflowStateName; // Terminated

	private String clientAccountTerminatedWorkflowStateName; // Terminated

	private String holdingAccountTerminatedWorkflowStateName; // Closed

	private String billingDefinitionEditableWorkflowStateName; //Draft

	private String billingDefinitionAfterEditWorkflowStateName; //Pending Approval

	private String billingDefinitionEditNoteTypeName; // Definition Changes

	private String businessContractPublishedWorkflowStateName; // Published

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getClientRelationshipTerminatedWorkflowStateName(), "Client Relationship Terminated Workflow State Name is required.");
		ValidationUtils.assertNotEmpty(getClientAccountTerminatedWorkflowStateName(), "Client Account Terminated Workflow State Name is required.");
		ValidationUtils.assertNotEmpty(getHoldingAccountTerminatedWorkflowStateName(), "Holding Account Terminated Workflow State Name is required.");
		ValidationUtils.assertNotEmpty(getBillingDefinitionEditableWorkflowStateName(), "Billing Definition Editable Workflow State Name is required.");
		ValidationUtils.assertNotEmpty(getBillingDefinitionAfterEditWorkflowStateName(), "Billing Definition After Edit Workflow State Name is required.");
		ValidationUtils.assertNotEmpty(getBillingDefinitionEditNoteTypeName(), "Billing Definition Edit Note Type is required.");
	}


	@Override
	public void validateInvestmentAccountTermination(InvestmentAccountTerminationCommand command) {
		// Initial Validation Prior to Starting Transaction
		ValidationUtils.assertNotNull(command.getClientInvestmentAccount(), "Client Account selection is required.");
		ValidationUtils.assertNotNull(command.getTerminationDate(), "Termination Date is required.");
		ValidationUtils.assertFalse(DateUtils.isDateAfter(command.getTerminationDate(), new Date()), "Termination Date cannot be in the future.");
		ValidationUtils.assertFalse(getClientAccountTerminatedWorkflowStateName().equals(command.getClientInvestmentAccount().getWorkflowState().getName()), "Client Account is already terminated.");

		// Confirm no existing transactions after terminate date
		AccountingTransactionSearchForm accountingTransactionSearchForm = new AccountingTransactionSearchForm();
		accountingTransactionSearchForm.setClientInvestmentAccountId(command.getClientInvestmentAccount().getId());
		accountingTransactionSearchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.GREATER_THAN, command.getTerminationDate()));
		accountingTransactionSearchForm.setDeleted(false);
		ValidationUtils.assertTrue(CollectionUtils.isEmpty(getAccountingTransactionService().getAccountingTransactionList(accountingTransactionSearchForm)), "Cannot terminate account on [" + DateUtils.fromDateShort(command.getTerminationDate()) + "] because there are transactions after the selected date.");
	}


	@Override
	public Status processInvestmentAccountTermination(InvestmentAccountTerminationCommand command) {
		Status status = Status.ofMessage("Starting account termination for " + command.getClientInvestmentAccount().getLabel());

		InvestmentAccount clientAccount = command.getClientInvestmentAccount();

		// 1. Optionally Remove Positions and Non Position Assets and Liabilities from the account
		removePositionsAndCashFromAccount(command, status);

		// 2. Terminate All Account Relationships (Returns a list of Holding Accounts Related to the Client Account that we are terminating the relationships for)
		Set<InvestmentAccount> relatedAccountSet = terminateInvestmentAccountRelationshipForClientAccount(clientAccount, command.getTerminationDate(), status);

		// 3. Move the Client Account to Terminated - Preserve the Terminate Date as the Workflow Effective Start
		// There is no transition from Active to Terminated - add one?  Add system defined transition - if so then how to set workflow state effective start date?
		clientAccount = transitionClientAccountToTerminatedState(clientAccount, command.getTerminationDate(), status);


		// 4. Terminate Holding Accounts if there are no more active relationships to any other accounts
		if (command.isTerminateHoldingAccounts()) {
			for (InvestmentAccount relatedAccount : CollectionUtils.getIterable(relatedAccountSet)) {
				if (!getHoldingAccountTerminatedWorkflowStateName().equals(relatedAccount.getWorkflowState().getName())) {
					if (CollectionUtils.isEmpty(getInvestmentAccountRelationshipListActiveForAccount(relatedAccount.getId(), clientAccount.getId()))) {
						relatedAccount.setWorkflowState(getWorkflowStateForEntity(relatedAccount, getHoldingAccountTerminatedWorkflowStateName()));
						relatedAccount.setWorkflowStatus(relatedAccount.getWorkflowState().getStatus());
						relatedAccount.setWorkflowStateEffectiveStartDate(command.getTerminationDate());
						status.addMessage("Transitioned Holding Account " + relatedAccount.getLabel() + " to " + getHoldingAccountTerminatedWorkflowStateName());
						getInvestmentAccountService().saveInvestmentAccount(relatedAccount);
					}
				}
			}
		}

		// 5. End Security Targets (if applies)
		terminateInvestmentAccountSecurityTargetsForClientAccount(command, status);

		// 6. Append Termination Document (if applies)
		saveBusinessContract(command.getBusinessClientTerminationContract(), clientAccount.getBusinessClient().getCompany(), command.getBusinessClientTerminationFile(), status, command);

		// 7. Terminate the Client and / or Client Relationship (If applies and last client account terminated under the client)
		if (command.isTerminateClient()) {
			BusinessClient client = getBusinessClientService().getBusinessClient(clientAccount.getBusinessClient().getId());
			// This should auto switch when the client account is terminated because its the status on the accounts
			if (client.getTerminateDate() == null && getClientAccountTerminatedWorkflowStateName().equals(client.getWorkflowState().getName())) {
				client.setTerminateDate(command.getTerminationDate()); // Should we look up the last date or assume they are terminated in order?
				status.addMessage("Client Terminate date set to " + DateUtils.fromDateShort(command.getTerminationDate()));
				client = getBusinessClientService().saveBusinessClient(client);
			}

			// If client was terminated, check the client relationship
			if (client.getClientRelationship() != null && client.getTerminateDate() != null && command.isTerminateClientRelationship()) {
				BusinessClientRelationship clientRelationship = getBusinessClientService().getBusinessClientRelationship(client.getClientRelationship().getId());
				BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
				searchForm.setClientRelationshipId(clientRelationship.getId());
				List<BusinessClient> clientList = getBusinessClientService().getBusinessClientList(searchForm);
				Date relationshipTerminateDate = null;
				for (BusinessClient businessClient : CollectionUtils.getIterable(clientList)) {
					if (businessClient.getTerminateDate() == null) {
						relationshipTerminateDate = null;
						break;
					}
					if (relationshipTerminateDate == null || DateUtils.isDateAfter(businessClient.getTerminateDate(), relationshipTerminateDate)) {
						relationshipTerminateDate = businessClient.getTerminateDate();
					}
				}
				if (relationshipTerminateDate != null) {
					transitionClientRelationshipToTerminatedState(clientRelationship, relationshipTerminateDate, status);
				}
			}
		}

		// 8. Terminate Billing
		terminateBillingForClientAccount(command, status);

		// 9. Terminate Trade Restrictions
		terminateTradeRestrictionsForClientAccount(command, status);

		status.setMessage("Completed Account Termination Process for " + command.getClientInvestmentAccount().getLabel() + " on " + DateUtils.fromDateShort(command.getTerminationDate()));
		return status;
	}


	private void removePositionsAndCashFromAccount(InvestmentAccountTerminationCommand command, Status status) {
		if (command.isDistributePositions() || command.isDistributeNonPositionAssetsAndLiabilities()) {
			AccountingBalanceRemovalCommand accountingBalanceRemovalCommand = new AccountingBalanceRemovalCommand();
			accountingBalanceRemovalCommand.setClientInvestmentAccountId(command.getClientInvestmentAccount().getId());
			accountingBalanceRemovalCommand.setDistributePositions(command.isDistributePositions());
			accountingBalanceRemovalCommand.setDistributeNonPositionAssetsAndLiabilities(command.isDistributeNonPositionAssetsAndLiabilities());
			accountingBalanceRemovalCommand.setRemovalDate(command.getTerminationDate());
			accountingBalanceRemovalCommand.setRemovalNote("Account Termination");
			accountingBalanceRemovalCommand.setUseFlexiblePriceLookup(true);
			Status balanceRemovalStatus = getAccountingBalanceRemovalService().processAccountingBalanceRemoval(accountingBalanceRemovalCommand);
			for (StatusDetail detail : balanceRemovalStatus.getDetailList()) {
				status.addDetail(detail.getCategory(), detail.getNote());
			}
		}
	}


	private Set<InvestmentAccount> terminateInvestmentAccountRelationshipForClientAccount(InvestmentAccount clientAccount, Date terminateDate, Status status) {
		List<InvestmentAccountRelationship> accountRelationshipList = getInvestmentAccountRelationshipListActiveForAccount(clientAccount.getId(), null);
		Set<InvestmentAccount> relatedAccountSet = new HashSet<>();
		// GET ACTIVE Account Relationships and then end them
		for (InvestmentAccountRelationship relationship : CollectionUtils.getIterable(accountRelationshipList)) {
			if (relationship.getEndDate() == null) {
				relationship.setEndDate(terminateDate);
				// Terminates ALL relationships but will only close holding accounts with no other relationships
				if (clientAccount.equals(relationship.getReferenceOne())) {
					if (!relationship.getReferenceTwo().getType().isOurAccount()) {
						relatedAccountSet.add(relationship.getReferenceTwo());
					}
				}
				else if (!relationship.getReferenceOne().getType().isOurAccount()) {
					relatedAccountSet.add(relationship.getReferenceOne());
				}
				getInvestmentAccountRelationshipService().saveInvestmentAccountRelationship(relationship);
			}
		}
		status.addMessage("Terminated " + CollectionUtils.getSize(accountRelationshipList) + " account relationships");
		return relatedAccountSet;
	}


	private InvestmentAccount transitionClientAccountToTerminatedState(InvestmentAccount clientAccount, Date terminateDate, Status status) {
		clientAccount.setWorkflowState(getWorkflowStateForEntity(clientAccount, getClientAccountTerminatedWorkflowStateName()));
		clientAccount.setWorkflowStatus(clientAccount.getWorkflowState().getStatus());
		clientAccount.setWorkflowStateEffectiveStartDate(terminateDate);
		status.addMessage("Transitioned Client Account to " + clientAccount.getWorkflowState().getName());
		return getInvestmentAccountService().saveInvestmentAccount(clientAccount);
	}


	private void transitionClientRelationshipToTerminatedState(BusinessClientRelationship clientRelationship, Date terminateDate, Status status) {
		clientRelationship.setWorkflowState(getWorkflowStateForEntity(clientRelationship, getClientRelationshipTerminatedWorkflowStateName()));
		clientRelationship.setWorkflowStatus(clientRelationship.getWorkflowState().getStatus());
		clientRelationship.setWorkflowStateEffectiveStartDate(terminateDate);
		clientRelationship.setTerminateDate(terminateDate);
		status.addMessage("Transitioned client relationship to " + clientRelationship.getWorkflowState().getName() + "; Terminate Date set to " + DateUtils.fromDateShort(terminateDate));
		getBusinessClientService().saveBusinessClientRelationship(clientRelationship);
	}


	private void terminateInvestmentAccountSecurityTargetsForClientAccount(InvestmentAccountTerminationCommand command, Status status) {
		if (command.isTerminateSecurityTargets() && command.getClientInvestmentAccount().getServiceProcessingType() != null && ServiceProcessingTypes.SECURITY_TARGET == command.getClientInvestmentAccount().getServiceProcessingType().getProcessingType()) {
			InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
			securityTargetSearchForm.setClientInvestmentAccountId(command.getClientInvestmentAccount().getId());
			securityTargetSearchForm.setNoEndDate(true);
			List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);
			for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(securityTargetList)) {
				securityTarget.setEndDate(command.getTerminationDate());
				getInvestmentAccountSecurityTargetService().saveInvestmentAccountSecurityTarget(securityTarget);
			}
			status.addMessage("Terminated " + CollectionUtils.getSize(securityTargetList) + " security target(s)");
		}
	}


	private void terminateBillingForClientAccount(InvestmentAccountTerminationCommand command, Status status) {
		if (command.isTerminateBilling()) {
			Date billingEndDate = ObjectUtils.coalesce(command.getBillingEndDate(), command.getTerminationDate());
			SystemTable billingDefinitionTable = getSystemSchemaService().getSystemTableByName(BillingDefinition.BILLING_DEFINITION_TABLE_NAME);

			BillingDefinitionInvestmentAccountSearchForm billingAccountSearchForm = new BillingDefinitionInvestmentAccountSearchForm();
			billingAccountSearchForm.setInvestmentAccountId(command.getClientInvestmentAccount().getId());
			billingAccountSearchForm.setNoEndDate(true); // Only check billing accounts that don't already have an end date

			List<BillingDefinitionInvestmentAccount> billingDefinitionInvestmentAccountList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountList(billingAccountSearchForm);
			if (!CollectionUtils.isEmpty(billingDefinitionInvestmentAccountList)) {
				BillingDefinition[] definitionList = BeanUtils.getPropertyValuesUniqueExcludeNull(billingDefinitionInvestmentAccountList, BillingDefinitionInvestmentAccount::getReferenceOne, BillingDefinition.class);
				for (BillingDefinition definition : definitionList) {
					if (definition.getEndDate() == null) { // And only include accounts on billing definitions where the billing definition end date is NOT populated
						boolean transitionDefinition = false;
						if (!getBillingDefinitionEditableWorkflowStateName().equals(definition.getWorkflowState().getName())) {
							transitionDefinition = true;
							getWorkflowTransitionService().executeWorkflowTransition(BillingDefinition.BILLING_DEFINITION_TABLE_NAME, definition.getId(), getWorkflowStateForEntity(definition, getBillingDefinitionEditableWorkflowStateName()).getId());
						}
						// Create a note so we know why the billing definition was rejected
						SystemNote systemNote = new SystemNote();
						systemNote.setNoteType(getSystemNoteService().getSystemNoteTypeByTableAndName(billingDefinitionTable.getId(), getBillingDefinitionEditNoteTypeName()));
						systemNote.setFkFieldId(BeanUtils.getIdentityAsLong(definition));
						systemNote.setText("Updating billing definition to reflect account [" + command.getClientInvestmentAccount().getLabel() + "] termination on [" + DateUtils.fromDateShort(command.getTerminationDate()) + "].  Billing Ended On [" + DateUtils.fromDateShort(billingEndDate) + "].");
						getSystemNoteService().saveSystemNote(systemNote);

						// Pull fully populated definition so account list is populated
						definition = getBillingDefinitionService().getBillingDefinitionPopulated(definition.getId());
						Date maxEndDate = null; // If all accounts are ended, this will be set on the billing definition
						boolean noEndDateExists = false;
						for (BillingDefinitionInvestmentAccount oldBillingAccount : definition.getInvestmentAccountList()) {
							// Need to clone the bean, for some reason the original thinks it's already updated and not actually updating the bean in the database
							// Not sure if it's because it's all done in one big transaction?
							BillingDefinitionInvestmentAccount billingAccount = BeanUtils.cloneBean(oldBillingAccount, false, true);
							if (billingAccount.getReferenceTwo().equals(command.getClientInvestmentAccount()) && billingAccount.getEndDate() == null) {
								status.addMessage("Terminated billing for " + billingAccount.getLabel());
								billingAccount.setEndDate(billingEndDate);
								getBillingDefinitionService().saveBillingDefinitionInvestmentAccount(billingAccount);
							}
							if (billingAccount.getEndDate() != null && (maxEndDate == null || DateUtils.isDateAfter(billingAccount.getEndDate(), maxEndDate))) {
								maxEndDate = billingAccount.getEndDate();
							}
							if (billingAccount.getEndDate() == null) {
								noEndDateExists = true;
							}
						}
						if (!noEndDateExists) {
							status.addMessage("Last Terminated account for billing definition " + definition.getLabel() + ". Billing definition was also terminated.");
							definition.setEndDate(maxEndDate);
							definition = getBillingDefinitionService().saveBillingDefinition(definition);
						}

						// RE-SUBMIT FOR APPROVAL (if was previously approved)
						if (transitionDefinition) {
							status.addMessage("Transition Billing Definition " + definition.getLabel() + " to " + getBillingDefinitionAfterEditWorkflowStateName());
							getWorkflowTransitionService().executeWorkflowTransition(BillingDefinition.BILLING_DEFINITION_TABLE_NAME, definition.getId(), getWorkflowStateForEntity(definition, getBillingDefinitionAfterEditWorkflowStateName()).getId());
						}
						else {
							status.addMessage("Billing Definition " + definition.getLabel() + " left in " + getBillingDefinitionEditableWorkflowStateName() + " workflow state.");
						}
					}
				}
			}
		}
	}


	private void terminateTradeRestrictionsForClientAccount(InvestmentAccountTerminationCommand command, Status status) {
		if (command.isTerminateTradeRestrictions()) {
			TradeRestrictionSearchForm tradeRestrictionSearchForm = new TradeRestrictionSearchForm();
			tradeRestrictionSearchForm.setClientAccountId(command.getClientInvestmentAccount().getId());
			tradeRestrictionSearchForm.setNoEndDate(true);

			List<TradeRestriction> tradeRestrictionList = getTradeRestrictionService().getTradeRestrictionList(tradeRestrictionSearchForm);
			for (TradeRestriction tradeRestriction : CollectionUtils.getIterable(tradeRestrictionList)) {
				tradeRestriction.setEndDate(command.getTerminationDate());
				getTradeRestrictionService().saveTradeRestriction(tradeRestriction);
			}
			status.addMessage("Terminated " + CollectionUtils.getSize(tradeRestrictionList) + " trade restriction(s).");
		}
	}


	private BusinessContract saveBusinessContract(BusinessContract contract, BusinessCompany company, MultipartFile file, Status status, InvestmentAccountTerminationCommand command) {

		if (contract != null && contract.getContractType() != null) {
			contract.setCompany(company);
			contract.setName(company.getName() + " - " + contract.getContractType().getName());

			// try to place the document into a published state if set to do so
			if (command.isAutoPublishDocument()) {
				try {
					contract.setEffectiveDate(command.getTerminationDate());
					contract = getBusinessContractService().saveBusinessContractWithFile(contract, file);
					getWorkflowTransitionService().executeWorkflowTransition(BusinessContract.CONTRACT_TABLE_NAME, contract.getId(), getWorkflowStateForEntity(contract, getBusinessContractPublishedWorkflowStateName()).getId());
				}
				catch (Exception exc) {
					throw new RuntimeException("Cannot automatically publish this document. Please uncheck \"Automatically publish uploaded document\" before proceeding. Error: " + exc.getMessage());
				}
			}
			else {
				contract = getBusinessContractService().saveBusinessContractWithFile(contract, file);
			}

			status.addMessage("Uploaded document " + contract.getLabel());
		}
		return contract;
	}

	////////////////////////////////////////////////////////////////////////////////


	private List<InvestmentAccountRelationship> getInvestmentAccountRelationshipListActiveForAccount(int accountId, Integer excludeRelatedToAccountId) {
		InvestmentAccountRelationshipSearchForm searchForm = new InvestmentAccountRelationshipSearchForm();
		searchForm.setMainOrRelatedAccountId(accountId);
		searchForm.setActive(true);
		List<InvestmentAccountRelationship> relationshipList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipList(searchForm);
		if (excludeRelatedToAccountId != null) {
			relationshipList = BeanUtils.filter(relationshipList, investmentAccountRelationship -> !MathUtils.isEqual(investmentAccountRelationship.getReferenceOne().getId(), excludeRelatedToAccountId) && !MathUtils.isEqual(investmentAccountRelationship.getReferenceTwo().getId(), excludeRelatedToAccountId));
		}
		return relationshipList;
	}


	private WorkflowState getWorkflowStateForEntity(WorkflowAware entity, String stateName) {
		WorkflowState workflowState = getWorkflowDefinitionService().getWorkflowStateByName(entity.getWorkflowState().getWorkflow().getId(), stateName);
		ValidationUtils.assertNotNull(workflowState, "Cannot find workflow state with name [" + stateName + "] for entity [" + BeanUtils.getLabel(entity) + "] using workflow [" + entity.getWorkflowState().getWorkflow().getName() + "].");
		return workflowState;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceRemovalService getAccountingBalanceRemovalService() {
		return this.accountingBalanceRemovalService;
	}


	public void setAccountingBalanceRemovalService(AccountingBalanceRemovalService accountingBalanceRemovalService) {
		this.accountingBalanceRemovalService = accountingBalanceRemovalService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public TradeRestrictionService getTradeRestrictionService() {
		return this.tradeRestrictionService;
	}


	public void setTradeRestrictionService(TradeRestrictionService tradeRestrictionService) {
		this.tradeRestrictionService = tradeRestrictionService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public String getClientRelationshipTerminatedWorkflowStateName() {
		return this.clientRelationshipTerminatedWorkflowStateName;
	}


	public void setClientRelationshipTerminatedWorkflowStateName(String clientRelationshipTerminatedWorkflowStateName) {
		this.clientRelationshipTerminatedWorkflowStateName = clientRelationshipTerminatedWorkflowStateName;
	}


	public String getClientAccountTerminatedWorkflowStateName() {
		return this.clientAccountTerminatedWorkflowStateName;
	}


	public void setClientAccountTerminatedWorkflowStateName(String clientAccountTerminatedWorkflowStateName) {
		this.clientAccountTerminatedWorkflowStateName = clientAccountTerminatedWorkflowStateName;
	}


	public String getHoldingAccountTerminatedWorkflowStateName() {
		return this.holdingAccountTerminatedWorkflowStateName;
	}


	public void setHoldingAccountTerminatedWorkflowStateName(String holdingAccountTerminatedWorkflowStateName) {
		this.holdingAccountTerminatedWorkflowStateName = holdingAccountTerminatedWorkflowStateName;
	}


	public String getBillingDefinitionEditableWorkflowStateName() {
		return this.billingDefinitionEditableWorkflowStateName;
	}


	public void setBillingDefinitionEditableWorkflowStateName(String billingDefinitionEditableWorkflowStateName) {
		this.billingDefinitionEditableWorkflowStateName = billingDefinitionEditableWorkflowStateName;
	}


	public String getBillingDefinitionAfterEditWorkflowStateName() {
		return this.billingDefinitionAfterEditWorkflowStateName;
	}


	public void setBillingDefinitionAfterEditWorkflowStateName(String billingDefinitionAfterEditWorkflowStateName) {
		this.billingDefinitionAfterEditWorkflowStateName = billingDefinitionAfterEditWorkflowStateName;
	}


	public String getBillingDefinitionEditNoteTypeName() {
		return this.billingDefinitionEditNoteTypeName;
	}


	public void setBillingDefinitionEditNoteTypeName(String billingDefinitionEditNoteTypeName) {
		this.billingDefinitionEditNoteTypeName = billingDefinitionEditNoteTypeName;
	}


	public String getBusinessContractPublishedWorkflowStateName() {
		return this.businessContractPublishedWorkflowStateName;
	}


	public void setBusinessContractPublishedWorkflowStateName(String businessContractPublishedWorkflowStateName) {
		this.businessContractPublishedWorkflowStateName = businessContractPublishedWorkflowStateName;
	}
}
