package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.replication.InvestmentReplicationAllocation;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.search.InvestmentReplicationAllocationSearchForm;
import com.clifton.product.ldi.ProductLDIMaturityExtended;
import com.clifton.product.ldi.ProductLDIService;
import com.clifton.product.ldi.search.ProductLDIMaturityExtendedSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
public class InvestmentAccountProductLDIOverlayBalanceValueCalculatorImpl extends InvestmentAccountPositionValueCalculatorImpl {

	private static final String CLIENT_ACCOUNT_LDI_CUSTOM_FIELD_GROUP = "LDI";
	private static final String LDI_NON_REPLICATION_GROUP_FIELD = "Non-Replication Group";
	private InvestmentReplicationService investmentReplicationService;
	private ProductLDIService productLDIService;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		Map<InvestmentSecurity, List<InvestmentAccountCalculationSnapshotDetail>> securityDetailListMap = BeanUtils.getBeansMap(super.calculateInvestmentAccountCalculationSnapshotDetailList(snapshot, context), InvestmentAccountCalculationSnapshotDetail::getInvestmentSecurity);

		ProductLDIMaturityExtendedSearchForm searchForm = new ProductLDIMaturityExtendedSearchForm();
		searchForm.setBalanceDate(context.getSnapshotDate());
		searchForm.setClientInvestmentAccountId(snapshot.getInvestmentAccount().getId());
		List<ProductLDIMaturityExtended> maturityExtendedList = getProductLDIService().getProductLDIMaturityExtendedList(searchForm);

		Integer nonReplicationGroupId = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(snapshot.getInvestmentAccount(), CLIENT_ACCOUNT_LDI_CUSTOM_FIELD_GROUP, LDI_NON_REPLICATION_GROUP_FIELD, false);

		// Don't want to double count any securities, so track security level what we need to include
		Set<InvestmentSecurity> securitySet = new HashSet<>();
		for (ProductLDIMaturityExtended maturityExtended : CollectionUtils.getIterable(maturityExtendedList)) {
			applyMaturityReplicationPositionsToDetailList(snapshot, context, maturityExtended, nonReplicationGroupId, securityDetailListMap, securitySet);
		}

		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();
		for (InvestmentSecurity security : CollectionUtils.getIterable(securitySet)) {
			detailList.addAll(securityDetailListMap.get(security));
		}
		return detailList;
	}


	private void applyMaturityReplicationPositionsToDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context, ProductLDIMaturityExtended maturityExtended, Integer nonReplicationInstrumentGroupId, Map<InvestmentSecurity, List<InvestmentAccountCalculationSnapshotDetail>> securityDetailListMap, Set<InvestmentSecurity> securitySet) {
		if (maturityExtended.getReplication() != null) {
			InvestmentReplicationAllocationSearchForm searchForm = new InvestmentReplicationAllocationSearchForm();
			searchForm.setReplicationId(maturityExtended.getReplication().getId());
			searchForm.setActiveOnDate(snapshot.getSnapshotDate());
			List<InvestmentReplicationAllocation> allocationList = getInvestmentReplicationService().getInvestmentReplicationAllocationList(searchForm);
			for (InvestmentSecurity investmentSecurity : securityDetailListMap.keySet()) {
				if (securitySet.contains(investmentSecurity)) {
					continue;
				}
				if (isInvestmentSecurityApplyToReplicationAllocationList(investmentSecurity, allocationList)) {
					securitySet.add(investmentSecurity);
				}
				else if (nonReplicationInstrumentGroupId != null) {
					if (getInvestmentGroupService().isInvestmentInstrumentInGroup(nonReplicationInstrumentGroupId.shortValue(), investmentSecurity.getInstrument().getId())) {
						// Need to also check maturity dates
						int daysDiff = DateUtils.getDaysDifference(context.getSnapshotDate(), investmentSecurity.getEndDate());
						if (daysDiff >= maturityExtended.getPeriodMinDays() && daysDiff <= maturityExtended.getPeriodMaxDays()) {
							securitySet.add(investmentSecurity);
						}
					}
				}
			}
		}
	}


	private boolean isInvestmentSecurityApplyToReplicationAllocationList(InvestmentSecurity security, List<InvestmentReplicationAllocation> replicationAllocationList) {
		for (InvestmentReplicationAllocation replicationAllocation : CollectionUtils.getIterable(replicationAllocationList)) {
			if (replicationAllocation.getReplicationInstrument() != null) {
				if (CompareUtils.isEqual(security.getInstrument(), replicationAllocation.getReplicationInstrument())) {
					return true;
				}
			}
			if (replicationAllocation.getReplicationSecurity() != null) {
				if (CompareUtils.isEqual(security, replicationAllocation.getReplicationSecurity())) {
					return true;
				}
				else if (replicationAllocation.isSecurityUnderlying() && CompareUtils.isEqual(security.getUnderlyingSecurity(), replicationAllocation.getReplicationSecurity())) {
					return true;
				}
			}
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public PositionValueTypes getPositionValueType() {
		return PositionValueTypes.NOTIONAL;
	}


	@Override
	@ValueIgnoringGetter
	public PositionSummationTypes getPositionSummationType() {
		return PositionSummationTypes.GROSS;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public ProductLDIService getProductLDIService() {
		return this.productLDIService;
	}


	public void setProductLDIService(ProductLDIService productLDIService) {
		this.productLDIService = productLDIService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
