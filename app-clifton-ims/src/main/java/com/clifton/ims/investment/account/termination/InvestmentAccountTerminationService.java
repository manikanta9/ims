package com.clifton.ims.investment.account.termination;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author manderson
 */
public interface InvestmentAccountTerminationService {


	@RequestMapping("investmentAccountTerminationProcessUpload") // must use upload url so that it uses the correct view
	@SecureMethod(dtoClass = InvestmentAccount.class) // WRITE PERMISSIONS ENOUGH FOR THIS?
	public Status processInvestmentAccountTermination(InvestmentAccountTerminationCommand command);
}
