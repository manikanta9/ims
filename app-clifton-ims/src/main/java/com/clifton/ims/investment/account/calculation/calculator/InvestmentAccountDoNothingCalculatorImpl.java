package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;

import java.util.List;


/**
 * The <code>InvestmentAccountDoNothingCalculatorImpl</code> calculator is just a place holder used by External accounts
 * to record a zero value for the calculation.
 *
 * @author manderson
 */
public class InvestmentAccountDoNothingCalculatorImpl extends BaseInvestmentAccountCalculationSnapshotCalculator {


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		return null;
	}
}
