package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.calculation.calculator.InvestmentAccountCalculationSnapshotCalculator;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>RollupInvestmentAccountCalculationSnapshotCalculatorImpl</code> allows grouping individuals calculations into one result
 * for example Notional (Futures) + Market Value (Non-Futures)
 *
 * @author manderson
 */
public class RollupInvestmentAccountCalculationSnapshotCalculatorImpl implements InvestmentAccountCalculationSnapshotCalculator, ValidationAware {


	private SystemBeanService systemBeanService;

	/**
	 * The list of individual calculations to merge together
	 */
	private List<SystemBean> childCalculatorBeanList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(CollectionUtils.getSize(getChildCalculatorBeanList()) > 1, "You must select at least 2 calculators for this roll up calculator.");
		for (SystemBean childBean : getChildCalculatorBeanList()) {
			if (childBean.getType().getClassName().equalsIgnoreCase(this.getClass().getName())) {
				throw new ValidationException(childBean.getLabelShort() + " is invalid as a child because it is also a rollup.  Please add individual child calculators.");
			}
		}
	}


	@Override
	public InvestmentAccountCalculationSnapshot calculateInvestmentAccountCalculationSnapshot(InvestmentAccount investmentAccount, InvestmentAccountCalculationProcessorContext context) {
		validate(); // Confirm setup is correct

		InvestmentAccountCalculationSnapshot snapshot = null;
		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();
		BigDecimal calculatedValue = null;

		// Calculate the Snapshot for each individually
		for (SystemBean bean : CollectionUtils.getIterable(getChildCalculatorBeanList())) {
			InvestmentAccountCalculationSnapshotCalculator childCalculator = (InvestmentAccountCalculationSnapshotCalculator) getSystemBeanService().getBeanInstance(bean);
			InvestmentAccountCalculationSnapshot childSnapshot = childCalculator.calculateInvestmentAccountCalculationSnapshot(investmentAccount, context);
			if (snapshot == null) {
				snapshot = childSnapshot;
			}
			calculatedValue = MathUtils.add(calculatedValue, childSnapshot.getCalculatedValue());
			if (!CollectionUtils.isEmpty(childSnapshot.getDetailList())) {
				detailList.addAll(childSnapshot.getDetailList());
			}
		}

		//noinspection ConstantConditions - because of validate check not possible to be null
		snapshot.setDetailList(detailList);
		snapshot.setCalculatedValue(calculatedValue);

		return snapshot;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public List<SystemBean> getChildCalculatorBeanList() {
		return this.childCalculatorBeanList;
	}


	public void setChildCalculatorBeanList(List<SystemBean> childCalculatorBeanList) {
		this.childCalculatorBeanList = childCalculatorBeanList;
	}
}
