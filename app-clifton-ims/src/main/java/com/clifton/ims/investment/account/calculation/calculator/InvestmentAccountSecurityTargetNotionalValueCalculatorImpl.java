package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>{@link InvestmentAccountSecurityTargetNotionalValueCalculatorImpl}</code> is used to calculate AUM for accounts that use security targets.
 * The result is for each active target on snapshot date is one of 3 values based on how the target is defined:
 * 1. target notional value
 * 2. Manager balance of the target manager
 * 3. Target quantity * Current price i.e. Target of 50,000 shares of AAPL = 50,000 * the Current Price of AAPL
 * <p>
 * All above 3 cases then apply the target multiplier and result is the abs value
 *
 * @author manderson
 */
public class InvestmentAccountSecurityTargetNotionalValueCalculatorImpl extends BaseInvestmentAccountCalculationSnapshotCalculator {


	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(snapshot, context);
		if (CollectionUtils.isEmpty(securityTargetList)) {
			return null;
		}

		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();
		for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(securityTargetList)) {
			InvestmentAssetClass assetClass = getAssetClass(securityTarget.getTargetUnderlyingSecurity(), null);
			InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, securityTarget.getTargetUnderlyingSecurity(), securityTarget.getTargetUnderlyingQuantity());

			TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDate(securityTarget, context.getSnapshotDate());
			detail.setPreliminaryCalculatedValue(tradeSecurityTarget.getTargetUnderlyingNotionalAdjusted());
			if (detail.getPreliminaryCalculatedValue() == null) {
				throw new ValidationException("No value calculated for target " + securityTarget.getLabel() + " on " + DateUtils.fromDate(snapshot.getSnapshotDate()) + ", please verify target setup and price exists");
			}
			detail.setCalculatedValue(MathUtils.abs(tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs()));
			detailList.add(detail);
		}
		return detailList;
	}


	@SuppressWarnings("unchecked")
	protected List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		String key = "SECURITY_TARGET_LIST";
		// Load all active on date into the context, then filter for each specific account as needed
		// Should be more efficient since most cases we process snapshots for a date across all accounts
		List<InvestmentAccountSecurityTarget> securityTargetList = (List<InvestmentAccountSecurityTarget>) context.getValueFromDateContextMap(key, () -> {
			InvestmentAccountSecurityTargetSearchForm securityTargetSearchForm = new InvestmentAccountSecurityTargetSearchForm();
			securityTargetSearchForm.setActiveOnDate(context.getSnapshotDate());
			return getInvestmentAccountSecurityTargetService().getInvestmentAccountSecurityTargetList(securityTargetSearchForm);
		});
		return BeanUtils.filter(securityTargetList, InvestmentAccountSecurityTarget::getClientInvestmentAccount, snapshot.getInvestmentAccount());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public TradeSecurityTargetUtilHandler getTradeSecurityTargetUtilHandler() {
		return this.tradeSecurityTargetUtilHandler;
	}


	public void setTradeSecurityTargetUtilHandler(TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler) {
		this.tradeSecurityTargetUtilHandler = tradeSecurityTargetUtilHandler;
	}
}
