package com.clifton.ims.investment.account.termination;

import com.clifton.business.contract.BusinessContract;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


/**
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentAccountTerminationCommand {

	/**
	 * The account to terminate (will also end all account relationships from/to this client account on the terminate date)
	 */
	private InvestmentAccount clientInvestmentAccount;

	private Date terminationDate;

	/**
	 * If applies, can easily upload a document i.e. termination letter
	 */
	private BusinessContract businessClientTerminationContract;

	private MultipartFile businessClientTerminationFile;

	/**
	 * If set, the service will try to move the uploaded document into the published workflow state.
	 */
	private boolean autoPublishDocument;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, removes all positions from the account on termination date
	 */
	private boolean distributePositions;

	/**
	 * If true, removes all non position assets and liabilities from the account on termination date
	 */
	private boolean distributeNonPositionAssetsAndLiabilities;

	/**
	 * If true, and applies to the account, will end all active security targets on the terminate date.
	 */
	private boolean terminateSecurityTargets;

	/**
	 * Will also close the holding accounts related to the client account (if no associated with another active client account)
	 */
	private boolean terminateHoldingAccounts;

	/**
	 * Will end all active trade restrictions on the account
	 */
	private boolean terminateTradeRestrictions;

	/**
	 * If the last terminated account under the client, will set the terminate date on the client
	 */
	private boolean terminateClient;

	/**
	 * If the last terminated account under the client relationship, will set the terminate date on the client relationship
	 */
	private boolean terminateClientRelationship;

	/**
	 * Set the billing end date on the account in any billing definitions, if the last terminated account on a billing definition will also end the billing definition on the terminate date
	 */
	private boolean terminateBilling;

	/**
	 * Often the ending balance on the termination date is zero, so billing end date is usually back dated one day
	 * This can be changed on screen (ended account on 4/2 - might not bill for that month and want to just end as of 3/31)
	 * If blank will use the termination date
	 */
	private Date billingEndDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public Date getTerminationDate() {
		return this.terminationDate;
	}


	public void setTerminationDate(Date terminationDate) {
		this.terminationDate = terminationDate;
	}


	public BusinessContract getBusinessClientTerminationContract() {
		return this.businessClientTerminationContract;
	}


	public void setBusinessClientTerminationContract(BusinessContract businessClientTerminationContract) {
		this.businessClientTerminationContract = businessClientTerminationContract;
	}


	public MultipartFile getBusinessClientTerminationFile() {
		return this.businessClientTerminationFile;
	}


	public void setBusinessClientTerminationFile(MultipartFile businessClientTerminationFile) {
		this.businessClientTerminationFile = businessClientTerminationFile;
	}


	public boolean isAutoPublishDocument() {
		return this.autoPublishDocument;
	}


	public void setAutoPublishDocument(boolean autoPublishDocument) {
		this.autoPublishDocument = autoPublishDocument;
	}


	public boolean isDistributePositions() {
		return this.distributePositions;
	}


	public void setDistributePositions(boolean distributePositions) {
		this.distributePositions = distributePositions;
	}


	public boolean isDistributeNonPositionAssetsAndLiabilities() {
		return this.distributeNonPositionAssetsAndLiabilities;
	}


	public void setDistributeNonPositionAssetsAndLiabilities(boolean distributeNonPositionAssetsAndLiabilities) {
		this.distributeNonPositionAssetsAndLiabilities = distributeNonPositionAssetsAndLiabilities;
	}


	public boolean isTerminateSecurityTargets() {
		return this.terminateSecurityTargets;
	}


	public void setTerminateSecurityTargets(boolean terminateSecurityTargets) {
		this.terminateSecurityTargets = terminateSecurityTargets;
	}


	public boolean isTerminateHoldingAccounts() {
		return this.terminateHoldingAccounts;
	}


	public void setTerminateHoldingAccounts(boolean terminateHoldingAccounts) {
		this.terminateHoldingAccounts = terminateHoldingAccounts;
	}


	public boolean isTerminateTradeRestrictions() {
		return this.terminateTradeRestrictions;
	}


	public void setTerminateTradeRestrictions(boolean terminateTradeRestrictions) {
		this.terminateTradeRestrictions = terminateTradeRestrictions;
	}


	public boolean isTerminateClient() {
		return this.terminateClient;
	}


	public void setTerminateClient(boolean terminateClient) {
		this.terminateClient = terminateClient;
	}


	public boolean isTerminateClientRelationship() {
		return this.terminateClientRelationship;
	}


	public void setTerminateClientRelationship(boolean terminateClientRelationship) {
		this.terminateClientRelationship = terminateClientRelationship;
	}


	public boolean isTerminateBilling() {
		return this.terminateBilling;
	}


	public void setTerminateBilling(boolean terminateBilling) {
		this.terminateBilling = terminateBilling;
	}


	public Date getBillingEndDate() {
		return this.billingEndDate;
	}


	public void setBillingEndDate(Date billingEndDate) {
		this.billingEndDate = billingEndDate;
	}
}
