package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class InvestmentAccountPortfolioRunLDIValueCalculatorImpl extends BaseInvestmentAccountCalculationSnapshotCalculator {

	private PortfolioRunLDIService portfolioRunLDIService;
	private PortfolioRunService portfolioRunService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();

		List<PortfolioRunLDIExposure> exposureList = getPortfolioRunLDIExposureList(snapshot, context);
		for (PortfolioRunLDIExposure exposure : CollectionUtils.getIterable(exposureList)) {
			InvestmentAssetClass assetClass = getAssetClass(exposure.getSecurity(), null);
			InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, exposure.getSecurity(), exposure.getActualContracts());

			BigDecimal value = CoreMathUtils.sumProperty(exposure.getKeyRateDurationPointList(), this::calculateValueForExposurePoint);
			detail.setPreliminaryCalculatedValue(value);
			detail.setCalculatedValue(value);
			detailList.add(detail);
		}
		return detailList;
	}


	protected List<PortfolioRunLDIExposure> getPortfolioRunLDIExposureList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		// Note: If Snapshot Date is a Weekend - Move to Previous Weekday
		Date balanceDate = getBalanceDateForSnapshotDate(context.getSnapshotDate());
		PortfolioRun run = getPortfolioRunService().getPortfolioRunByMainRun(snapshot.getInvestmentAccount().getId(), balanceDate);
		if (run != null) {
			return getPortfolioRunLDIService().getPortfolioRunLDIExposureListByRun(run.getId(), true);
		}
		return null;
	}


	protected BigDecimal calculateValueForExposurePoint(PortfolioRunLDIExposureKeyRateDurationPoint exposureKeyRateDurationPoint) {
		// (DV01ContractValue * ActualContracts * 100.0) / ((f.FieldOrder / 12.0) / 100.0)
		return MathUtils.divide(MathUtils.multiply(exposureKeyRateDurationPoint.getDv01ActualExposureValue(), MathUtils.BIG_DECIMAL_ONE_HUNDRED), (MathUtils.divide(MathUtils.divide(exposureKeyRateDurationPoint.getKeyRateDurationPoint().getMarketDataField().getFieldOrder(), 12), MathUtils.BIG_DECIMAL_ONE_HUNDRED)));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioRunLDIService getPortfolioRunLDIService() {
		return this.portfolioRunLDIService;
	}


	public void setPortfolioRunLDIService(PortfolioRunLDIService portfolioRunLDIService) {
		this.portfolioRunLDIService = portfolioRunLDIService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
