package com.clifton.ims.investment.account.wizard.generator;

import com.clifton.ims.investment.account.wizard.InvestmentAccountWizard;


/**
 * @author manderson
 */
public interface InvestmentAccountWizardGenerator {


	/**
	 * Returns the conditionId that should be evaluated to determine if a user has access to use this generator
	 */
	public Integer getInvestmentAccountWizardUseConditionId();


	/**
	 * Generates a new empty instance of the investment account wizard
	 */
	public InvestmentAccountWizard populateInvestmentAccountWizard(InvestmentAccountWizard investmentAccountWizard);


	/**
	 * Generates and saves related information from the InvestmentAccountWizard object
	 */
	public InvestmentAccountWizard generateInvestmentAccountWizard(InvestmentAccountWizard investmentAccountWizard, boolean ignoreValidation);
}
