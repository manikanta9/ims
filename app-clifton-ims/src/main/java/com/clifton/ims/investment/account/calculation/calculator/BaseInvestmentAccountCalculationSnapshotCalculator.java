package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.EntityNotFoundValidationException;
import com.clifton.ims.mapping.InvestmentAssetClassFieldMappingHandler;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.calculation.calculator.InvestmentAccountCalculationSnapshotCalculator;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.system.fieldmapping.SystemFieldMapping;
import com.clifton.system.fieldmapping.SystemFieldMappingService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public abstract class BaseInvestmentAccountCalculationSnapshotCalculator implements InvestmentAccountCalculationSnapshotCalculator {

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private SystemFieldMappingService systemFieldMappingService;
	private MarketDataRetriever marketDataRetriever;
	private InvestmentAssetClassFieldMappingHandler investmentAssetClassFieldMappingHandler;


	/**
	 * By default, where possible we'll use the holding account to determine the fx rate
	 */
	private boolean alwaysUseClientAccountFxRate;

	private Short fieldMappingId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final InvestmentAccountCalculationSnapshot calculateInvestmentAccountCalculationSnapshot(InvestmentAccount investmentAccount, InvestmentAccountCalculationProcessorContext context) {
		InvestmentAccountCalculationSnapshot snapshot = context.getExistingAccountSnapshotMap().get(investmentAccount.getId());
		if (snapshot == null) {
			snapshot = new InvestmentAccountCalculationSnapshot(context.getCalculationType(), investmentAccount, context.getSnapshotDate());
		}
		// Since we are re-calculating - reset all information from the account on the snapshot
		snapshot.setCalculatedValueCurrency(investmentAccount.getBaseCurrency());
		snapshot.setFxRateToBase(getFxRateForClientAccount(investmentAccount, snapshot.getCalculatedValueCurrency(), context.getCalculationType().getBaseCurrency(), context));

		// Note: New Details are updated with Existing Detail Ids where possible by the processor
		snapshot.setDetailList(calculateInvestmentAccountCalculationSnapshotDetailList(snapshot, context));
		snapshot.setCalculatedValue(CoreMathUtils.sumProperty(snapshot.getDetailList(), snapshotDetail -> {
			if (snapshotDetail.getParent() == null) {
				return snapshotDetail.getCalculatedValue();
			}
			else {
				return BigDecimal.ZERO;
			}
		}));
		// Database calculated column - but set here because previews don't save data so easier if it's available
		snapshot.setCalculatedValueBase(MathUtils.round(MathUtils.multiply(snapshot.getCalculatedValue(), snapshot.getFxRateToBase()), 2));
		return snapshot;
	}


	protected abstract List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context);

	////////////////////////////////////////////////////////////////////////////////
	///////////////                Helper Methods                    ///////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Useful for calculators that use run data where we need to evaluate on month end, but month end falls on a weekend.
	 * Runs are only supported for weekdays, so we use data from previous weekday.
	 */
	protected Date getBalanceDateForSnapshotDate(Date snapshotDate) {
		if (DateUtils.isWeekday(snapshotDate)) {
			return snapshotDate;
		}
		return DateUtils.getPreviousWeekday(snapshotDate);
	}


	protected BigDecimal getFxRateForClientAccount(InvestmentAccount investmentAccount, InvestmentSecurity fromCcy, InvestmentSecurity toCcy, InvestmentAccountCalculationProcessorContext context) {
		return getFxRateForAccounts(investmentAccount, null, fromCcy, toCcy, context);
	}


	protected BigDecimal getFxRateForAccounts(InvestmentAccount clientAccount, InvestmentAccount holdingAccount, InvestmentSecurity fromCcy, InvestmentSecurity toCcy, InvestmentAccountCalculationProcessorContext context) {
		if (fromCcy.equals(toCcy)) {
			return BigDecimal.ONE;
		}
		if (holdingAccount != null && !isAlwaysUseClientAccountFxRate()) {
			// Try to get FX Rate for the Holding Account
			String dataSourceName = getMarketDataSourceNameForHoldingAccount(holdingAccount, context);
			if (!StringUtils.isEmpty(dataSourceName)) {
				// Need to look up the rate as of the previous weekday if snapshot date is on a weekend
				BigDecimal fxRate = getFxRateForDataSource(dataSourceName, fromCcy, toCcy, context);
				if (fxRate != null) {
					return fxRate;
				}
			}
		}

		// Client Account FX Rate Retrieval
		String dataSourceName = getMarketDataSourceNameForClientAccount(clientAccount, context);
		return getFxRateForDataSource(dataSourceName, fromCcy, toCcy, context);
	}


	private String getMarketDataSourceNameForClientAccount(InvestmentAccount clientAccount, InvestmentAccountCalculationProcessorContext context) {
		String dataSourceKey = "ACCOUNT_DATASOURCE_" + clientAccount.getId();
		return (String) context.getValueFromGlobalContextMap(dataSourceKey, () -> ObjectUtils.coalesce(getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(clientAccount.toClientAccount()), ""));
	}


	private String getMarketDataSourceNameForHoldingAccount(InvestmentAccount holdingAccount, InvestmentAccountCalculationProcessorContext context) {
		String dataSourceKey = "ACCOUNT_DATASOURCE_" + holdingAccount.getId();
		return (String) context.getValueFromGlobalContextMap(dataSourceKey, () -> {
			String dataSourceName = "";
			try {
				dataSourceName = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForHoldingAccount(holdingAccount.toHoldingAccount(), true);
			}
			catch (EntityNotFoundValidationException e) {
				// If it's not found, we don't want to use the global default, then we'll use the client account default leave blank
			}
			return dataSourceName;
		});
	}


	private BigDecimal getFxRateForDataSource(String dataSourceName, InvestmentSecurity fromCcy, InvestmentSecurity toCcy, InvestmentAccountCalculationProcessorContext context) {
		String fxRateKey = dataSourceName + "_" + fromCcy.getId() + "_" + toCcy.getId();
		// Need to look up the rate as of the previous weekday if snapshot date is on a weekend
		return (BigDecimal) context.getValueFromDateContextMap(fxRateKey, () -> getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName, fromCcy.getSymbol(), toCcy.getSymbol(), getBalanceDateForSnapshotDate(context.getSnapshotDate()))));
	}


	protected BigDecimal getSecurityPrice(InvestmentSecurity security, InvestmentAccountCalculationProcessorContext context) {
		String key = "SECURITY_PRICE_" + security.getId();
		return (BigDecimal) context.getValueFromDateContextMap(key, () -> getMarketDataRetriever().getPrice(security, context.getSnapshotDate(), true, null));
	}


	protected InvestmentAssetClass getAssetClass(InvestmentSecurity security, InvestmentAccountAssetClass accountAssetClass) {
		InvestmentAssetClass assetClass = accountAssetClass != null ? accountAssetClass.getAssetClass() : null;

		if (assetClass == null && getFieldMappingId() != null) {
			return getInvestmentAssetClassFieldMappingHandler().getAssetClass(security, getFieldMapping());
		}
		return assetClass;
	}


	protected SystemFieldMapping getFieldMapping() {
		return getFieldMappingId() != null ? getSystemFieldMappingService().getSystemFieldMapping(getFieldMappingId()) : null;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods              ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public SystemFieldMappingService getSystemFieldMappingService() {
		return this.systemFieldMappingService;
	}


	public void setSystemFieldMappingService(SystemFieldMappingService systemFieldMappingService) {
		this.systemFieldMappingService = systemFieldMappingService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public InvestmentAssetClassFieldMappingHandler getInvestmentAssetClassFieldMappingHandler() {
		return this.investmentAssetClassFieldMappingHandler;
	}


	public void setInvestmentAssetClassFieldMappingHandler(InvestmentAssetClassFieldMappingHandler investmentAssetClassFieldMappingHandler) {
		this.investmentAssetClassFieldMappingHandler = investmentAssetClassFieldMappingHandler;
	}


	public boolean isAlwaysUseClientAccountFxRate() {
		return this.alwaysUseClientAccountFxRate;
	}


	public void setAlwaysUseClientAccountFxRate(boolean alwaysUseClientAccountFxRate) {
		this.alwaysUseClientAccountFxRate = alwaysUseClientAccountFxRate;
	}


	public Short getFieldMappingId() {
		return this.fieldMappingId;
	}


	public void setFieldMappingId(Short fieldMappingId) {
		this.fieldMappingId = fieldMappingId;
	}
}
