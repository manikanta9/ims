package com.clifton.ims.investment.account.group.rebuild;

import com.clifton.accounting.investment.account.group.rebuild.BaseInvestmentAccountGroupAccountSearchRebuildAwareExecutor;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;

import java.util.List;


/**
 * @author manderson
 */
public class InvestmentAccountGroupAccountSearchRebuildAwareExecutor extends BaseInvestmentAccountGroupAccountSearchRebuildAwareExecutor {

	private PerformanceCompositeSetupService performanceCompositeSetupService;

	private Short performanceCompositeId;
	private Boolean includeInactivePerformanceCompositeAssignments;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean applyAdditionalInvestmentAccountSearchFormFilters(InvestmentAccountSearchForm accountSearchForm) {
		if (getPerformanceCompositeId() != null) {
			// NOTE: THIS METHOD PROPERLY SUPPORTS ROLLUP COMPOSITES TO INCLUDE ACCOUNTS OF CHILD COMPOSITES
			List<PerformanceCompositeInvestmentAccount> compositeInvestmentAccountList = getPerformanceCompositeSetupService().getPerformanceCompositeAccountListByComposite(getPerformanceCompositeId());
			if (!BooleanUtils.isTrue(getIncludeInactivePerformanceCompositeAssignments())) {
				compositeInvestmentAccountList = BeanUtils.filter(compositeInvestmentAccountList, PerformanceCompositeInvestmentAccount::isActive);
			}
			// If no accounts apply return false so search doesn't continue
			if (CollectionUtils.isEmpty(compositeInvestmentAccountList)) {
				return false;
			}
			accountSearchForm.setIds(BeanUtils.getPropertyValues(compositeInvestmentAccountList, compositeAssignment -> compositeAssignment.getInvestmentAccount().getId(), Integer.class));
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public Short getPerformanceCompositeId() {
		return this.performanceCompositeId;
	}


	public void setPerformanceCompositeId(Short performanceCompositeId) {
		this.performanceCompositeId = performanceCompositeId;
	}


	public Boolean getIncludeInactivePerformanceCompositeAssignments() {
		return this.includeInactivePerformanceCompositeAssignments;
	}


	public void setIncludeInactivePerformanceCompositeAssignments(Boolean includeInactivePerformanceCompositeAssignments) {
		this.includeInactivePerformanceCompositeAssignments = includeInactivePerformanceCompositeAssignments;
	}
}
