package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.accounting.account.AccountingAccountType;
import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.balance.AccountingBalance;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.group.InvestmentGroupService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>InvestmentAccountPositionValueCalculatorImpl</code> is an account calculation snapshot calculator that can be used
 * by accounts that need to calculate there values from a list of positions.
 * <p>
 * Uses AccountingPositionDailyLive retrievals for live position look ups.
 * Additional options to limit the positions that are included
 * Can optionally include cash balances
 *
 * @author manderson
 */
public class InvestmentAccountPositionValueCalculatorImpl extends BaseInvestmentAccountCalculationSnapshotCalculator implements ValidationAware {

	private AccountingBalanceService accountingBalanceService;
	private AccountingAccountIdsCache accountingAccountIdsCache;
	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingValuationService accountingValuationService;

	private InvestmentCalculator investmentCalculator;
	private InvestmentGroupService investmentGroupService;
	private InvestmentManagerAccountService investmentManagerAccountService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * How the value of the position is calculated. i.e. Notional, Market Value, etc.
	 * See below for enum class
	 */
	private PositionValueTypes positionValueType;

	/**
	 * Option to apply delta adjustment to each position
	 * If the security is priced daily, will require Delta value to be entered for each day
	 * Otherwise will do a flexible look up.
	 * Note: This is separated from the positionValueType options because it can (and will) be applied to various calculations
	 */
	private boolean applyDeltaAdjustment;

	/**
	 * Determines how the position total is applied. i.e. Net, Gross, Net By Underlying, etc.
	 */
	private PositionSummationTypes positionSummationType;

	/**
	 * Whether or not to include collateral positions
	 */
	private boolean includeCollateralPositions;

	/**
	 * If set, limit the positions included to those in the selected instrument group
	 */
	private Short includeInvestmentGroupId;

	/**
	 * If set, limit the positions included to those NOT in the selected instrument group
	 */
	private Short excludeInvestmentGroupId;

	/**
	 * Some client accounts will override the duration field name to use when pulling in duration values.
	 * This field is used to calculate position snapshots using the same duration field.
	 * If blank, the default duration field "Duration" will be used.
	 */
	private String durationFieldNameOverride;

	/**
	 * Whether or not to include currency positions
	 */
	private boolean includeCurrency;

	/**
	 * If true, and positions evaluate to zero, then cash retrieval is not performed
	 * and the calculated value for the account and date is zero.
	 */
	private boolean excludeCashIfPositionsZero;

	/**
	 * Whether or not to include cash balance
	 */
	private boolean includeCashBalance;

	/**
	 * Whether or not to include receivables in cash balance
	 */
	private boolean includeReceivables;

	/**
	 * Whether or not to include cash collateral in cash balance
	 */
	private boolean includeCashCollateralBalance;

	/**
	 * If true, will use abs(abs(cash) + position calculated value)
	 * If false, will use abs(cash + position calculated value)
	 */
	private boolean useAbsOfCashBalance;


	private Short[] additionalGlAccountIds;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getPositionValueType(), "Position Value type is required");
	}


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();

		List<AccountingPositionDaily> positionList = getAccountingPositionDailyLiveList(snapshot, context);

		for (AccountingPositionDaily positionDaily : CollectionUtils.getIterable(positionList)) {
			InvestmentAssetClass assetClass = getAssetClass(positionDaily.getInvestmentSecurity(), null);
			InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, positionDaily.getInvestmentSecurity(), positionDaily.getRemainingQuantity());
			detail.setPreliminaryCalculatedValue(getAccountingPositionDailyPreliminaryCalculatedValue(positionDaily, context));
			BigDecimal calculatedValue = detail.getPreliminaryCalculatedValue();
			if (isApplyDeltaAdjustment()) {
				BigDecimal deltaValue = getMarketDataRetriever().getDeltaOnDateOrFlexible(detail.getInvestmentSecurity(), positionDaily.getPositionDate(), true);
				calculatedValue = MathUtils.multiply(calculatedValue, deltaValue);
			}
			detail.setCalculatedValue(calculatedValue);
			detailList.add(detail);
		}
		// Handles Gross, Net, Net by Security, Net By Underlying, etc.
		applyPositionSummationToDetailList(snapshot, detailList);

		if (isIncludeCurrency()) {
			for (AccountingBalanceValue currency : CollectionUtils.getIterable(getCurrencyBalanceList(snapshot, context))) {
				InvestmentAssetClass assetClass = getAssetClass(currency.getInvestmentSecurity(), null);
				InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, currency.getInvestmentSecurity(), null);
				detail.setPreliminaryCalculatedValue(currency.getBaseMarketValue());
				detail.setCalculatedValue(currency.getBaseMarketValue());
				detailList.add(detail);
			}
		}

		if (!MathUtils.isNullOrZero(CoreMathUtils.sumProperty(detailList, InvestmentAccountCalculationSnapshotDetail::getCalculatedValue)) || !isExcludeCashIfPositionsZero()) {
			BigDecimal cashBalance = getCashBalance(snapshot, context);
			// Don't include it in the details if it's 0
			if (!MathUtils.isNullOrZero(cashBalance)) {
				InvestmentAssetClass assetClass = getAssetClass(snapshot.getInvestmentAccount().getBaseCurrency(), null);
				InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, snapshot.getInvestmentAccount().getBaseCurrency(), null);
				detail.setPreliminaryCalculatedValue(cashBalance);
				if (isUseAbsOfCashBalance()) {
					detail.setCalculatedValue(MathUtils.abs(cashBalance));
				}
				else {
					detail.setCalculatedValue(cashBalance);
				}
				detailList.add(detail);
			}
		}

		// Add in the sum of local account balances from selected GL Accounts
		if (!ArrayUtils.isEmpty(getAdditionalGlAccountIds())) {
			BigDecimal additionalBalances = getSumOfAccountingAccountBaseBalances(snapshot, context);
			if (!MathUtils.isNullOrZero(additionalBalances)) {
				InvestmentAssetClass assetClass = getAssetClass(snapshot.getInvestmentAccount().getBaseCurrency(), null);
				InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, snapshot.getInvestmentAccount().getBaseCurrency(), null);
				detail.setPreliminaryCalculatedValue(additionalBalances);
				detail.setCalculatedValue(additionalBalances);
				detailList.add(detail);
			}
		}
		return detailList;
	}


	protected List<AccountingPositionDaily> getAccountingPositionDailyLiveList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		AccountingPositionDailyLiveSearchForm searchForm = new AccountingPositionDailyLiveSearchForm();
		searchForm.setClientAccountId(snapshot.getInvestmentAccount().getId());
		searchForm.setSnapshotDate(context.getSnapshotDate());
		if (!isIncludeCollateralPositions()) {
			searchForm.setCollateralAccountingAccount(false);
		}
		if (getIncludeInvestmentGroupId() != null) {
			searchForm.setInvestmentGroupId(getIncludeInvestmentGroupId());
		}
		List<AccountingPositionDaily> positionDailyList = getAccountingPositionDailyService().getAccountingPositionDailyLiveList(searchForm);
		// Note: Exclude Option is not supported so handle it manually - should be very fast as these retrievals are cached
		if (getExcludeInvestmentGroupId() != null) {
			positionDailyList = BeanUtils.filter(positionDailyList, accountingPositionDaily -> !getInvestmentGroupService().isInvestmentInstrumentInGroup(getExcludeInvestmentGroupId(), accountingPositionDaily.getInvestmentSecurity().getInstrument().getId()));
		}
		return positionDailyList;
	}


	@SuppressWarnings("unchecked")
	protected List<AccountingBalanceValue> getCurrencyBalanceList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		// Faster to get all at once and then filter for the accounts we actually need (since this is used by 300+ accounts) - Still limits by processing account/account group if specified
		return BeanUtils.filter((List<AccountingBalanceValue>) context.getValueFromDateContextMap("CURRENCY_BALANCES", () -> {
			// Same as Analytics -> Client Positions -> Currency Balances
			AccountingBalanceSearchForm currencySearchForm = AccountingBalanceSearchForm.onTransactionDate(context.getSnapshotDate());
			currencySearchForm.setAccountingAccountType(AccountingAccountType.ASSET);
			currencySearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_ACCOUNTS);
			// Limit results if only processing for a sub-set of accounts
			currencySearchForm.setClientInvestmentAccountId(context.getInvestmentAccountId());
			currencySearchForm.setClientInvestmentAccountGroupId(context.getInvestmentAccountGroupId());
			return getAccountingValuationService().getAccountingBalanceValueList(currencySearchForm);
		}), AccountingBalanceValue::getClientInvestmentAccount, snapshot.getInvestmentAccount());
	}


	@SuppressWarnings("unchecked")
	protected BigDecimal getCashBalance(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		if (isIncludeCashBalance() || isIncludeCashCollateralBalance() || isIncludeReceivables()) {
			// Faster to get all at once and then filter for the accounts we actually need (since this is used by 300+ accounts) - Still limits by processing account/account group if specified
			String key = "CASH_BALANCES_" + isIncludeCashBalance() + "_" + isIncludeCashCollateralBalance() + "_" + isIncludeReceivables();
			List<AccountingBalance> balanceList = BeanUtils.filter((List<AccountingBalance>) context.getValueFromDateContextMap(key, () -> {
				// Include All Cash And Non Position Receivables - Optionally Exclude Cash Collateral
				Short[] accountingAccountIds = getAccountingAccountIdsCache().getAccountingAccounts((!isIncludeCashCollateralBalance() ? AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS_EXCLUDE_COLLATERAL : AccountingAccountIdsCacheImpl.AccountingAccountIds.CASH_ACCOUNTS));
				// And Receivables (Non Position - Optionally Exclude Collateral)
				if (isIncludeReceivables()) {
					accountingAccountIds = ArrayUtils.addAll(accountingAccountIds, getAccountingAccountIdsCache().getAccountingAccounts((!isIncludeCashCollateralBalance() ? AccountingAccountIdsCacheImpl.AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION_EXCLUDE_COLLATERAL : AccountingAccountIdsCacheImpl.AccountingAccountIds.RECEIVABLE_ACCOUNTS_EXCLUDE_POSITION)));
				}
				AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(snapshot.getSnapshotDate());
				searchForm.setAccountingAccountIds(accountingAccountIds);
				// Limit results if only processing for a sub-set of accounts
				searchForm.setClientInvestmentAccountId(context.getInvestmentAccountId());
				searchForm.setClientInvestmentAccountGroupId(context.getInvestmentAccountGroupId());
				return getAccountingBalanceService().getAccountingBalanceList(searchForm);
			}), AccountingBalance::getClientInvestmentAccount, snapshot.getInvestmentAccount());
			return CoreMathUtils.sumProperty(balanceList, AccountingBalance::getBaseAmount);
		}
		return null;
	}


	/**
	 * Returns sum of AccountingAccount Local Balances for the GL account IDs passed in via the accountingAccountingList parameter.
	 */
	protected BigDecimal getSumOfAccountingAccountBaseBalances(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		AccountingBalanceSearchForm searchForm = AccountingBalanceSearchForm.onTransactionDate(snapshot.getSnapshotDate());
		searchForm.setAccountingAccountIds(getAdditionalGlAccountIds());
		searchForm.setClientInvestmentAccountId(snapshot.getInvestmentAccount().getId());
		List<AccountingBalance> accountingBalanceList = getAccountingBalanceService().getAccountingBalanceList(searchForm);
		return CoreMathUtils.sumProperty(accountingBalanceList, AccountingBalance::getBaseAmount);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Duration Retrievals                    /////////////
	////////////////////////////////////////////////////////////////////////////////


	protected BigDecimal getBenchmarkDurationValue(AccountingPositionDaily positionDaily, InvestmentAccountCalculationProcessorContext context) {
		String key = "BENCHMARK_DURATION_" + positionDaily.getClientInvestmentAccount().getId();
		return (BigDecimal) context.getValueFromDateContextMap(key, () -> {
			BigDecimal value = null;
			List<InvestmentManagerAccountAssignment> managerAccountAssignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(positionDaily.getClientInvestmentAccount().getId(), true);
			for (InvestmentManagerAccountAssignment assignment : CollectionUtils.getIterable(managerAccountAssignmentList)) {
				if (assignment.getBenchmarkSecurity() != null) {
					BigDecimal benchmarkValue = getSecurityDurationValue(assignment.getBenchmarkSecurity(), context);
					if (benchmarkValue != null) {
						if (value == null || MathUtils.isGreaterThan(benchmarkValue, value)) {
							value = benchmarkValue;
						}
					}
				}
			}
			return ObjectUtils.coalesce(value, BigDecimal.ZERO);
		});
	}


	protected BigDecimal getSecurityDurationValue(InvestmentSecurity security, InvestmentAccountCalculationProcessorContext context) {
		String key = "SECURITY_DURATION_" + security.getId();
		String durationFieldName = getDurationFieldNameOverride();
		if (!StringUtils.isEmpty(durationFieldName)) {
			key = key + durationFieldName;
		}
		return (BigDecimal) context.getValueFromDateContextMap(key, () -> {
			BigDecimal duration = getMarketDataRetriever().getDurationFlexible(security, context.getSnapshotDate(), durationFieldName, false);
			return ObjectUtils.coalesce(duration, BigDecimal.ZERO);
		});
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Position Valuations                    /////////////
	////////////////////////////////////////////////////////////////////////////////

	protected enum PositionValueTypes {

		NOTIONAL,
		MARKET_VALUE,
		/**
		 * Prelim Value is Duration Adjusted Base Notional, Value is ABS(Duration Adjusted Base Notional)
		 * Duration Adjustment = Security Duration Value / Benchmark Duration Value
		 * where the Benchmark Duration Value = MAX Benchmark Duration Value from Managers assigned to the account
		 * TODO is there a better way to work around this?  There is currently only 1 active account that uses this calculation
		 * Could add a separate calculator each each benchmark to use - since there is so few, or a custom field on the account
		 */
		DURATION_ADJUSTED_NOTIONAL,
		QUANTITY_UNADJUSTED, // A.K.A. Unadjusted Face Returns "Remaining Quantity" for all securities that do not have Factor Change Event. For securities with Factor Change Event (CDS and ABS), returns opening quantity (Original Notional or Face)
		QUANTITY_UNADJUSTED_FX_RATE, // QUANTITY UNADJUSTED * FX Rate
		QUANTITY, // A.K.A. Adjusted Face, Used for Swaps Adjusted Notional
		QUANTITY_FX_RATE, // QUANTITY * FX Rate
		STRIKE_PRICE_NOTIONAL, // Strike Price * Quantity * Price Multiplier * FX Rate
		UNDERLYING_PRICE_ADJUSTED_NOTIONAL, // Quantity * Underlying Price Adjusted * Price Multiplier * FX Rate - if no underlying - uses security price
		DIRTY_PRICE_NOTIONAL, // Dirty Price * Quantity * Price Multiplier * FX Rate
		VEGA_NOTIONAL // Used for Variance Swaps - Custom Field Value for "Vega Notional Amount" * FX Rate
	}


	protected BigDecimal getAccountingPositionDailyPreliminaryCalculatedValue(AccountingPositionDaily positionDaily, InvestmentAccountCalculationProcessorContext context) {
		switch (getPositionValueType()) {
			case NOTIONAL:
				return positionDaily.getNotionalValueBase();
			case MARKET_VALUE:
				return positionDaily.getMarketValueBase();
			case DURATION_ADJUSTED_NOTIONAL:
				BigDecimal durationAdjustment = MathUtils.divide(getSecurityDurationValue(positionDaily.getInvestmentSecurity(), context), getBenchmarkDurationValue(positionDaily, context), BigDecimal.ZERO);
				return MathUtils.multiply(positionDaily.getNotionalValueBase(), durationAdjustment);
			case QUANTITY_UNADJUSTED:
				return positionDaily.getUnadjustedQuantity();
			case QUANTITY_UNADJUSTED_FX_RATE:
				return MathUtils.multiply(getFxRateForAccountingPositionDaily(positionDaily, context), positionDaily.getUnadjustedQuantity());
			case QUANTITY:
				return positionDaily.getRemainingQuantity();
			case QUANTITY_FX_RATE:
				return MathUtils.multiply(getFxRateForAccountingPositionDaily(positionDaily, context), positionDaily.getRemainingQuantity());
			case STRIKE_PRICE_NOTIONAL:
				BigDecimal strikePrice = positionDaily.getInvestmentSecurity().getOptionStrikePrice();
				return MathUtils.multiply(getFxRateForAccountingPositionDaily(positionDaily, context), MathUtils.multiply(MathUtils.multiply(positionDaily.getRemainingQuantity(), strikePrice), positionDaily.getInvestmentSecurity().getPriceMultiplier()));
			case UNDERLYING_PRICE_ADJUSTED_NOTIONAL:
				BigDecimal price = positionDaily.getMarketPrice();
				if (positionDaily.getInvestmentSecurity().getUnderlyingSecurity() != null) {
					price = getSecurityPrice(positionDaily.getInvestmentSecurity().getUnderlyingSecurity(), context);
				}
				return MathUtils.multiply(getFxRateForAccountingPositionDaily(positionDaily, context), MathUtils.multiply(MathUtils.multiply(positionDaily.getRemainingQuantity(), price), positionDaily.getInvestmentSecurity().getPriceMultiplier()));
			case DIRTY_PRICE_NOTIONAL:
				price = getInvestmentCalculator().calculateDirtyPrice(positionDaily.getInvestmentSecurity(), positionDaily.getRemainingQuantity(), positionDaily.getMarketPrice(), positionDaily.getPositionDate());
				return MathUtils.multiply(getFxRateForAccountingPositionDaily(positionDaily, context), MathUtils.multiply(MathUtils.multiply(positionDaily.getRemainingQuantity(), price), positionDaily.getInvestmentSecurity().getPriceMultiplier()));
			case VEGA_NOTIONAL:
				return MathUtils.multiply(getFxRateForAccountingPositionDaily(positionDaily, context), (BigDecimal) getInvestmentSecurityUtilHandler().getCustomColumnValue(positionDaily.getInvestmentSecurity(), InvestmentSecurity.CUSTOM_FIELD_VEGA_NOTIONAL_AMOUNT));
			default:
				throw new ValidationException("Calculation Not Defined for Position Value Type " + getPositionValueType());
		}
	}


	/**
	 * Returns the FX rate for the Security CCY Denomination to the Client Account Base Currency
	 */
	protected BigDecimal getFxRateForAccountingPositionDaily(AccountingPositionDaily accountingPositionDaily, InvestmentAccountCalculationProcessorContext context) {
		return getFxRateForAccounts(accountingPositionDaily.getClientInvestmentAccount(), accountingPositionDaily.getAccountingTransaction().getHoldingInvestmentAccount(), accountingPositionDaily.getInvestmentSecurity().getInstrument().getTradingCurrency(), accountingPositionDaily.getClientInvestmentAccount().getBaseCurrency(), context);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Position Valuations                    /////////////
	////////////////////////////////////////////////////////////////////////////////

	protected enum PositionSummationTypes {

		GROSS, // Each Calculated Value is ABS of it's Value
		NET, // Each Calculated Value is as is
		GROSS_NET_BY_SECURITY, // If there is one record for a security calculated value is ABS of it's value.  If there are multiple, a "rollup" record is created with the gross of the net of the sum of the individual security amounts
		GROSS_NET_BY_UNDERLYING // If there is one record for an underlying security, the calculated value is ABS of it's value.  If there are multiple, a "rollup" record is created with the gross of the net of the sum of the individual security amounts
	}


	protected void applyPositionSummationToDetailList(InvestmentAccountCalculationSnapshot snapshot, List<InvestmentAccountCalculationSnapshotDetail> detailList) {
		switch (getPositionSummationType()) {
			case NET:
				// Do Nothing - No Change Necessary
				break;
			case GROSS:
				CollectionUtils.getStream(detailList).forEach(detail -> detail.setCalculatedValue(MathUtils.abs(detail.getCalculatedValue())));
				break;
			case GROSS_NET_BY_SECURITY:
				Map<InvestmentSecurity, List<InvestmentAccountCalculationSnapshotDetail>> securityDetailListMap = BeanUtils.getBeansMap(detailList, InvestmentAccountCalculationSnapshotDetail::getInvestmentSecurity);
				applySecurityGroupingToDetailList(securityDetailListMap, snapshot, detailList);
				break;
			case GROSS_NET_BY_UNDERLYING:
				securityDetailListMap = BeanUtils.getBeansMap(detailList, detail -> detail.getInvestmentSecurity().getUnderlyingSecurity() != null ? detail.getInvestmentSecurity().getUnderlyingSecurity() : detail.getInvestmentSecurity());
				applySecurityGroupingToDetailList(securityDetailListMap, snapshot, detailList);
				break;
			default:
				throw new ValidationException("Calculation Not Defined for Position Summation Type " + getPositionSummationType());
		}
	}


	protected void applySecurityGroupingToDetailList(Map<InvestmentSecurity, List<InvestmentAccountCalculationSnapshotDetail>> securityDetailListMap, InvestmentAccountCalculationSnapshot snapshot, List<InvestmentAccountCalculationSnapshotDetail> detailList) {
		for (Map.Entry<InvestmentSecurity, List<InvestmentAccountCalculationSnapshotDetail>> investmentSecurityListEntry : securityDetailListMap.entrySet()) {
			// If more than one, apply grouping
			if (CollectionUtils.getSize(investmentSecurityListEntry.getValue()) > 1) {
				InvestmentAssetClass assetClass = getAssetClass(investmentSecurityListEntry.getKey(), null);
				InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, investmentSecurityListEntry.getKey(), null);
				for (InvestmentAccountCalculationSnapshotDetail securityDetail : investmentSecurityListEntry.getValue()) {
					// Set Parent on the security detail
					securityDetail.setParent(detail);
					detail.setQuantity(MathUtils.add(detail.getQuantity(), securityDetail.getQuantity()));
					detail.setPreliminaryCalculatedValue(MathUtils.add(detail.getPreliminaryCalculatedValue(), securityDetail.getPreliminaryCalculatedValue()));
					detail.setCalculatedValue(MathUtils.add(detail.getCalculatedValue(), securityDetail.getCalculatedValue()));
				}
				// Gross the total Net of the group
				detail.setCalculatedValue(MathUtils.abs(detail.getCalculatedValue()));
				// Add group record to the list
				detailList.add(detail);
			}
			// Otherwise set final value as the ABS value
			else {
				InvestmentAccountCalculationSnapshotDetail detail = investmentSecurityListEntry.getValue().get(0);
				detail.setCalculatedValue(MathUtils.abs(detail.getCalculatedValue()));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public PositionValueTypes getPositionValueType() {
		return this.positionValueType;
	}


	public void setPositionValueType(PositionValueTypes positionValueType) {
		this.positionValueType = positionValueType;
	}


	public PositionSummationTypes getPositionSummationType() {
		return this.positionSummationType;
	}


	public void setPositionSummationType(PositionSummationTypes positionSummationType) {
		this.positionSummationType = positionSummationType;
	}


	public boolean isApplyDeltaAdjustment() {
		return this.applyDeltaAdjustment;
	}


	public void setApplyDeltaAdjustment(boolean applyDeltaAdjustment) {
		this.applyDeltaAdjustment = applyDeltaAdjustment;
	}


	public boolean isIncludeCollateralPositions() {
		return this.includeCollateralPositions;
	}


	public void setIncludeCollateralPositions(boolean includeCollateralPositions) {
		this.includeCollateralPositions = includeCollateralPositions;
	}


	public boolean isIncludeCurrency() {
		return this.includeCurrency;
	}


	public void setIncludeCurrency(boolean includeCurrency) {
		this.includeCurrency = includeCurrency;
	}


	public boolean isIncludeCashBalance() {
		return this.includeCashBalance;
	}


	public void setIncludeCashBalance(boolean includeCashBalance) {
		this.includeCashBalance = includeCashBalance;
	}


	public boolean isIncludeCashCollateralBalance() {
		return this.includeCashCollateralBalance;
	}


	public void setIncludeCashCollateralBalance(boolean includeCashCollateralBalance) {
		this.includeCashCollateralBalance = includeCashCollateralBalance;
	}


	public boolean isIncludeReceivables() {
		return this.includeReceivables;
	}


	public void setIncludeReceivables(boolean includeReceivables) {
		this.includeReceivables = includeReceivables;
	}


	public boolean isExcludeCashIfPositionsZero() {
		return this.excludeCashIfPositionsZero;
	}


	public void setExcludeCashIfPositionsZero(boolean excludeCashIfPositionsZero) {
		this.excludeCashIfPositionsZero = excludeCashIfPositionsZero;
	}


	public Short getIncludeInvestmentGroupId() {
		return this.includeInvestmentGroupId;
	}


	public void setIncludeInvestmentGroupId(Short includeInvestmentGroupId) {
		this.includeInvestmentGroupId = includeInvestmentGroupId;
	}


	public Short getExcludeInvestmentGroupId() {
		return this.excludeInvestmentGroupId;
	}


	public void setExcludeInvestmentGroupId(Short excludeInvestmentGroupId) {
		this.excludeInvestmentGroupId = excludeInvestmentGroupId;
	}


	public String getDurationFieldNameOverride() {
		return this.durationFieldNameOverride;
	}


	public void setDurationFieldNameOverride(String durationFieldNameOverride) {
		this.durationFieldNameOverride = durationFieldNameOverride;
	}


	public boolean isUseAbsOfCashBalance() {
		return this.useAbsOfCashBalance;
	}


	public void setUseAbsOfCashBalance(boolean useAbsOfCashBalance) {
		this.useAbsOfCashBalance = useAbsOfCashBalance;
	}


	public Short[] getAdditionalGlAccountIds() {
		return this.additionalGlAccountIds;
	}


	public void setAdditionalGlAccountIds(Short[] additionalGlAccountIds) {
		this.additionalGlAccountIds = additionalGlAccountIds;
	}
}
