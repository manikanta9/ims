package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.ManagerCashTypes;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.setup.InvestmentAssetClass;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public class InvestmentAccountManagerBalanceValueCalculatorImpl extends BaseInvestmentAccountCalculationSnapshotCalculator {


	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;
	private InvestmentManagerAccountService investmentManagerAccountService;

	private List<ManagerCashTypes> excludeManagerCashTypes;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentManagerAccountBalance> managerBalanceList = getInvestmentManagerAccountBalanceList(snapshot, context);
		if (CollectionUtils.isEmpty(managerBalanceList)) {
			return null;
		}

		// No Details Possible?
		BigDecimal value = null;
		for (InvestmentManagerAccountBalance balance : managerBalanceList) {
			BigDecimal balanceValue = MathUtils.abs(balance.getAdjustedTotalValue());
			if (!MathUtils.isNullOrZero(balanceValue)) {
				// If the Manager Account is in a different currency than the account, need to convert it first
				balanceValue = MathUtils.multiply(balanceValue, getFxRateForClientAccount(snapshot.getInvestmentAccount(), balance.getManagerAccount().getBaseCurrency(), snapshot.getCalculatedValueCurrency(), context));
			}
			value = MathUtils.add(value, balanceValue);
		}

		InvestmentAssetClass assetClass = getAssetClass(snapshot.getCalculatedValueCurrency(), null);

		InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, snapshot.getCalculatedValueCurrency(), null);
		detail.setPreliminaryCalculatedValue(value);
		detail.setCalculatedValue(value);
		return CollectionUtils.createList(detail);
	}


	protected List<InvestmentManagerAccountBalance> getInvestmentManagerAccountBalanceList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentManagerAccountAssignment> assignmentList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(snapshot.getInvestmentAccount().getId(), true);

		Set<Integer> managerAccountIds = new HashSet<>();
		for (InvestmentManagerAccountAssignment assignment : CollectionUtils.getIterable(assignmentList)) {
			if (!managerAccountIds.contains(assignment.getReferenceOne().getId())) {
				if (getExcludeManagerCashTypes() != null && getExcludeManagerCashTypes().contains(assignment.getCashType())) {
					continue;
				}
				managerAccountIds.add(assignment.getReferenceOne().getId());
			}
		}

		if (CollectionUtils.isEmpty(managerAccountIds)) {
			return null;
		}
		InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
		searchForm.setBalanceDate(getBalanceDateForSnapshotDate(context.getSnapshotDate()));
		searchForm.setManagerAccountIds(managerAccountIds.toArray(new Integer[managerAccountIds.size()]));
		return getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public List<ManagerCashTypes> getExcludeManagerCashTypes() {
		return this.excludeManagerCashTypes;
	}


	public void setExcludeManagerCashTypes(List<ManagerCashTypes> excludeManagerCashTypes) {
		this.excludeManagerCashTypes = excludeManagerCashTypes;
	}
}
