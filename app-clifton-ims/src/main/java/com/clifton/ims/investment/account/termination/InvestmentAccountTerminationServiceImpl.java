package com.clifton.ims.investment.account.termination;

import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.investment.account.termination.processor.InvestmentAccountTerminationProcessor;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author manderson
 */
@Service
public class InvestmentAccountTerminationServiceImpl implements InvestmentAccountTerminationService {

	private static final String INVESTMENT_ACCOUNT_TERMINATION_PROCESSOR_BEAN_NAME = "Investment Account Termination Processor";

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status processInvestmentAccountTermination(InvestmentAccountTerminationCommand command) {
		SystemBean processorBean = getSystemBeanService().getSystemBeanByName(INVESTMENT_ACCOUNT_TERMINATION_PROCESSOR_BEAN_NAME);
		if (processorBean == null) {
			throw new ValidationException("Could Not find a System Bean with name [" + INVESTMENT_ACCOUNT_TERMINATION_PROCESSOR_BEAN_NAME + "] which is needed in order to process account termination.");
		}
		InvestmentAccountTerminationProcessor processor = ((InvestmentAccountTerminationProcessor) getSystemBeanService().getBeanInstance(processorBean));
		processor.validateInvestmentAccountTermination(command);
		return processInvestmentAccountTerminationImpl(processor, command);
	}


	@Transactional
	protected Status processInvestmentAccountTerminationImpl(InvestmentAccountTerminationProcessor processor, InvestmentAccountTerminationCommand command) {
		return processor.processInvestmentAccountTermination(command);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
