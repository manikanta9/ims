package com.clifton.ims.investment.account.wizard;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.ims.investment.account.wizard.generator.InvestmentAccountWizardGenerator;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * @author manderson
 */
public interface InvestmentAccountWizardService {


	/**
	 * Does a system bean search, however also additionally filters the list of results based on the evaluation of conditions from
	 * {@link InvestmentAccountWizardGenerator#getInvestmentAccountWizardUseConditionId()}
	 */
	@SecureMethod(dtoClass = InvestmentAccount.class, permissions = SecurityPermission.PERMISSION_CREATE)
	public List<SystemBean> getInvestmentAccountWizardList(SystemBeanSearchForm searchForm);


	/**
	 * Returns a blank instance of the wizard option for the given bean generator
	 */
	@SecureMethod(dtoClass = InvestmentAccount.class, permissions = SecurityPermission.PERMISSION_CREATE)
	public InvestmentAccountWizard getInvestmentAccountWizard(int generatorBeanId);


	/**
	 * Saves the full account structure and returns the created Client Account to be opened if user clicks apply
	 */
	@RequestMapping("investmentAccountWizardUpload") // must use upload url so that it uses the correct view
	@SecureMethod(dtoClass = InvestmentAccount.class, permissions = SecurityPermission.PERMISSION_CREATE)
	@UserIgnorableValidation
	public InvestmentAccountWizard saveInvestmentAccountWizard(InvestmentAccountWizard investmentAccountWizard, boolean ignoreValidation);
}
