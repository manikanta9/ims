package com.clifton.ims.investment.account.wizard;

import com.clifton.billing.billingbasis.BillingBasisCalculationType;
import com.clifton.billing.billingbasis.BillingBasisValuationType;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoiceType;
import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.contract.BusinessContract;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.system.bean.SystemBean;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class InvestmentAccountWizard {

	private SystemBean wizardGeneratorBean;

	private BusinessClientRelationship businessClientRelationship;

	/**
	 * ONLY applied if new Client Relationship being created
	 * Links the contacts to the client relationship company under contact type Relationship Manager
	 * Usually only one, but if multiple requires one to be selected as a primary
	 */
	private List<BusinessCompanyContact> relationshipManagerContactList;

	private BusinessContract businessClientRelationshipContract1;

	private BusinessContract businessClientRelationshipContract2;

	private BusinessContract businessClientRelationshipContract3;

	private BusinessContract businessClientRelationshipContract4;

	private MultipartFile businessClientRelationshipFile1;

	private MultipartFile businessClientRelationshipFile2;

	private MultipartFile businessClientRelationshipFile3;

	private MultipartFile businessClientRelationshipFile4;

	private BusinessClient businessClient;

	private BusinessContract businessClientContract1;

	private BusinessContract businessClientContract2;

	private BusinessContract businessClientContract3;

	private BusinessContract businessClientContract4;

	private MultipartFile businessClientFile1;

	private MultipartFile businessClientFile2;

	private MultipartFile businessClientFile3;

	private MultipartFile businessClientFile4;

	private InvestmentAccount clientInvestmentAccount;

	private InvestmentAccount holdingInvestmentAccount;

	private List<BusinessCompany> restrictedBrokerCompanyList;

	private Date relationshipPurposeStartDate;

	private List<InvestmentAccountRelationshipPurpose> relationshipPurposeList;

	/**
	 * If true, user has elected to add a new or existing client relationship
	 */
	private boolean clientRelationshipUsed;


	/**
	 * If true, user has elected to add an existing or new holding account
	 */
	private boolean holdingAccountUsed;


	/**
	 * Will only set up billing if this option is checked
	 */
	private boolean billingUsed;

	/**
	 * By default will immediately submit the billing definition to pending approval, unless checked
	 * This can be used when not all of the billing information can be entered on the wizard so additional changes are expected
	 */
	private boolean billingDraft;

	/**
	 * Populated if associating account with an existing billing definition
	 */
	private BillingDefinition billingDefinition;

	/**
	 * Creating a new billing definition
	 * If true, uses the client relationship contract (associated above)
	 * If false, uses the client contract (associated above)
	 * UNLESS a new contract is supplied
	 */
	private boolean useBusinessClientRelationshipContract;

	/**
	 * If Attaching a new document, associates it with the client relationship if true, else the client
	 */
	private boolean billingDefinitionContractUseClientRelationship;

	private BusinessContract billingDefinitionContract;

	/**
	 * Identifies the contract (document) number to be used to link the clientContract / clientRelationshipContract to the billingDefinitionContract (e.g an index of 2 for businessClientContract2).
	 * Only applicable if an existing client contract / client relationship contract is to be used as teh billingDefinitionContract.
	 */
	private Integer contractDocumentIndex;

	private boolean invoicePostedToPortal;

	private MultipartFile billingDefinitionContractFile;

	private BillingInvoiceType billingInvoiceType;

	private BillingFrequencies billingFrequency;

	/**
	 * The template to create the schedule from - if blank can still setup the billing definition without a schedule that can be added later (must explicitly select the option)
	 */
	private BillingSchedule billingScheduleTemplate;

	private boolean doNotAddSchedule;

	/**
	 * Required valuation and calculation type for the account
	 */
	private BillingBasisValuationType billingBasisValuationType;

	private BillingBasisCalculationType billingBasisCalculationType;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBean getWizardGeneratorBean() {
		return this.wizardGeneratorBean;
	}


	public void setWizardGeneratorBean(SystemBean wizardGeneratorBean) {
		this.wizardGeneratorBean = wizardGeneratorBean;
	}


	public BusinessClientRelationship getBusinessClientRelationship() {
		return this.businessClientRelationship;
	}


	public void setBusinessClientRelationship(BusinessClientRelationship businessClientRelationship) {
		this.businessClientRelationship = businessClientRelationship;
	}


	public BusinessClient getBusinessClient() {
		return this.businessClient;
	}


	public void setBusinessClient(BusinessClient businessClient) {
		this.businessClient = businessClient;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public Date getRelationshipPurposeStartDate() {
		return this.relationshipPurposeStartDate;
	}


	public void setRelationshipPurposeStartDate(Date relationshipPurposeStartDate) {
		this.relationshipPurposeStartDate = relationshipPurposeStartDate;
	}


	public List<InvestmentAccountRelationshipPurpose> getRelationshipPurposeList() {
		return this.relationshipPurposeList;
	}


	public void setRelationshipPurposeList(List<InvestmentAccountRelationshipPurpose> relationshipPurposeList) {
		this.relationshipPurposeList = relationshipPurposeList;
	}


	public BusinessContract getBusinessClientRelationshipContract1() {
		return this.businessClientRelationshipContract1;
	}


	public void setBusinessClientRelationshipContract1(BusinessContract businessClientRelationshipContract1) {
		this.businessClientRelationshipContract1 = businessClientRelationshipContract1;
	}


	public BusinessContract getBusinessClientRelationshipContract2() {
		return this.businessClientRelationshipContract2;
	}


	public void setBusinessClientRelationshipContract2(BusinessContract businessClientRelationshipContract2) {
		this.businessClientRelationshipContract2 = businessClientRelationshipContract2;
	}


	public BusinessContract getBusinessClientRelationshipContract3() {
		return this.businessClientRelationshipContract3;
	}


	public void setBusinessClientRelationshipContract3(BusinessContract businessClientRelationshipContract3) {
		this.businessClientRelationshipContract3 = businessClientRelationshipContract3;
	}


	public BusinessContract getBusinessClientRelationshipContract4() {
		return this.businessClientRelationshipContract4;
	}


	public void setBusinessClientRelationshipContract4(BusinessContract businessClientRelationshipContract4) {
		this.businessClientRelationshipContract4 = businessClientRelationshipContract4;
	}


	public MultipartFile getBusinessClientRelationshipFile1() {
		return this.businessClientRelationshipFile1;
	}


	public void setBusinessClientRelationshipFile1(MultipartFile businessClientRelationshipFile1) {
		this.businessClientRelationshipFile1 = businessClientRelationshipFile1;
	}


	public MultipartFile getBusinessClientRelationshipFile2() {
		return this.businessClientRelationshipFile2;
	}


	public void setBusinessClientRelationshipFile2(MultipartFile businessClientRelationshipFile2) {
		this.businessClientRelationshipFile2 = businessClientRelationshipFile2;
	}


	public MultipartFile getBusinessClientRelationshipFile3() {
		return this.businessClientRelationshipFile3;
	}


	public void setBusinessClientRelationshipFile3(MultipartFile businessClientRelationshipFile3) {
		this.businessClientRelationshipFile3 = businessClientRelationshipFile3;
	}


	public MultipartFile getBusinessClientRelationshipFile4() {
		return this.businessClientRelationshipFile4;
	}


	public void setBusinessClientRelationshipFile4(MultipartFile businessClientRelationshipFile4) {
		this.businessClientRelationshipFile4 = businessClientRelationshipFile4;
	}


	public List<BusinessCompany> getRestrictedBrokerCompanyList() {
		return this.restrictedBrokerCompanyList;
	}


	public void setRestrictedBrokerCompanyList(List<BusinessCompany> restrictedBrokerCompanyList) {
		this.restrictedBrokerCompanyList = restrictedBrokerCompanyList;
	}


	public BusinessContract getBusinessClientContract1() {
		return this.businessClientContract1;
	}


	public void setBusinessClientContract1(BusinessContract businessClientContract1) {
		this.businessClientContract1 = businessClientContract1;
	}


	public BusinessContract getBusinessClientContract2() {
		return this.businessClientContract2;
	}


	public void setBusinessClientContract2(BusinessContract businessClientContract2) {
		this.businessClientContract2 = businessClientContract2;
	}


	public BusinessContract getBusinessClientContract3() {
		return this.businessClientContract3;
	}


	public void setBusinessClientContract3(BusinessContract businessClientContract3) {
		this.businessClientContract3 = businessClientContract3;
	}


	public BusinessContract getBusinessClientContract4() {
		return this.businessClientContract4;
	}


	public void setBusinessClientContract4(BusinessContract businessClientContract4) {
		this.businessClientContract4 = businessClientContract4;
	}


	public MultipartFile getBusinessClientFile1() {
		return this.businessClientFile1;
	}


	public void setBusinessClientFile1(MultipartFile businessClientFile1) {
		this.businessClientFile1 = businessClientFile1;
	}


	public MultipartFile getBusinessClientFile2() {
		return this.businessClientFile2;
	}


	public void setBusinessClientFile2(MultipartFile businessClientFile2) {
		this.businessClientFile2 = businessClientFile2;
	}


	public MultipartFile getBusinessClientFile3() {
		return this.businessClientFile3;
	}


	public void setBusinessClientFile3(MultipartFile businessClientFile3) {
		this.businessClientFile3 = businessClientFile3;
	}


	public MultipartFile getBusinessClientFile4() {
		return this.businessClientFile4;
	}


	public void setBusinessClientFile4(MultipartFile businessClientFile4) {
		this.businessClientFile4 = businessClientFile4;
	}


	public boolean isClientRelationshipUsed() {
		return this.clientRelationshipUsed;
	}


	public void setClientRelationshipUsed(boolean clientRelationshipUsed) {
		this.clientRelationshipUsed = clientRelationshipUsed;
	}


	public boolean isHoldingAccountUsed() {
		return this.holdingAccountUsed;
	}


	public void setHoldingAccountUsed(boolean holdingAccountUsed) {
		this.holdingAccountUsed = holdingAccountUsed;
	}


	public boolean isBillingUsed() {
		return this.billingUsed;
	}


	public void setBillingUsed(boolean billingUsed) {
		this.billingUsed = billingUsed;
	}


	public boolean isBillingDraft() {
		return this.billingDraft;
	}


	public void setBillingDraft(boolean billingDraft) {
		this.billingDraft = billingDraft;
	}


	public BillingDefinition getBillingDefinition() {
		return this.billingDefinition;
	}


	public void setBillingDefinition(BillingDefinition billingDefinition) {
		this.billingDefinition = billingDefinition;
	}


	public boolean isUseBusinessClientRelationshipContract() {
		return this.useBusinessClientRelationshipContract;
	}


	public void setUseBusinessClientRelationshipContract(boolean useBusinessClientRelationshipContract) {
		this.useBusinessClientRelationshipContract = useBusinessClientRelationshipContract;
	}


	public boolean isBillingDefinitionContractUseClientRelationship() {
		return this.billingDefinitionContractUseClientRelationship;
	}


	public void setBillingDefinitionContractUseClientRelationship(boolean billingDefinitionContractUseClientRelationship) {
		this.billingDefinitionContractUseClientRelationship = billingDefinitionContractUseClientRelationship;
	}


	public BusinessContract getBillingDefinitionContract() {
		return this.billingDefinitionContract;
	}


	public void setBillingDefinitionContract(BusinessContract billingDefinitionContract) {
		this.billingDefinitionContract = billingDefinitionContract;
	}


	public Integer getContractDocumentIndex() {
		return this.contractDocumentIndex;
	}


	public void setContractDocumentIndex(Integer contractDocumentIndex) {
		this.contractDocumentIndex = contractDocumentIndex;
	}


	public MultipartFile getBillingDefinitionContractFile() {
		return this.billingDefinitionContractFile;
	}


	public void setBillingDefinitionContractFile(MultipartFile billingDefinitionContractFile) {
		this.billingDefinitionContractFile = billingDefinitionContractFile;
	}


	public BillingSchedule getBillingScheduleTemplate() {
		return this.billingScheduleTemplate;
	}


	public void setBillingScheduleTemplate(BillingSchedule billingScheduleTemplate) {
		this.billingScheduleTemplate = billingScheduleTemplate;
	}


	public boolean isDoNotAddSchedule() {
		return this.doNotAddSchedule;
	}


	public void setDoNotAddSchedule(boolean doNotAddSchedule) {
		this.doNotAddSchedule = doNotAddSchedule;
	}


	public BillingInvoiceType getBillingInvoiceType() {
		return this.billingInvoiceType;
	}


	public void setBillingInvoiceType(BillingInvoiceType billingInvoiceType) {
		this.billingInvoiceType = billingInvoiceType;
	}


	public BillingFrequencies getBillingFrequency() {
		return this.billingFrequency;
	}


	public void setBillingFrequency(BillingFrequencies billingFrequency) {
		this.billingFrequency = billingFrequency;
	}


	public BillingBasisValuationType getBillingBasisValuationType() {
		return this.billingBasisValuationType;
	}


	public void setBillingBasisValuationType(BillingBasisValuationType billingBasisValuationType) {
		this.billingBasisValuationType = billingBasisValuationType;
	}


	public BillingBasisCalculationType getBillingBasisCalculationType() {
		return this.billingBasisCalculationType;
	}


	public void setBillingBasisCalculationType(BillingBasisCalculationType billingBasisCalculationType) {
		this.billingBasisCalculationType = billingBasisCalculationType;
	}


	public List<BusinessCompanyContact> getRelationshipManagerContactList() {
		return this.relationshipManagerContactList;
	}


	public void setRelationshipManagerContactList(List<BusinessCompanyContact> relationshipManagerContactList) {
		this.relationshipManagerContactList = relationshipManagerContactList;
	}


	public boolean isInvoicePostedToPortal() {
		return this.invoicePostedToPortal;
	}


	public void setInvoicePostedToPortal(boolean invoicePostedToPortal) {
		this.invoicePostedToPortal = invoicePostedToPortal;
	}
}
