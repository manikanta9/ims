package com.clifton.ims.investment.account.termination.processor;

import com.clifton.core.util.status.Status;
import com.clifton.ims.investment.account.termination.InvestmentAccountTerminationCommand;


/**
 * @author manderson
 */
public interface InvestmentAccountTerminationProcessor {


	/**
	 * Runs pre-checks to confirms the account is valid to terminate
	 */
	public void validateInvestmentAccountTermination(InvestmentAccountTerminationCommand command);


	/**
	 * Processing the account termination and returns status object with details about what was completed
	 */
	public Status processInvestmentAccountTermination(InvestmentAccountTerminationCommand command);
}
