package com.clifton.ims.investment.account.calculation.calculator;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.account.calculation.processor.InvestmentAccountCalculationProcessorContext;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshot;
import com.clifton.investment.account.calculation.snapshot.InvestmentAccountCalculationSnapshotDetail;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.product.overlay.search.ProductOverlayManagerAccountSearchForm;
import com.clifton.product.replication.ProductReplicationService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public class InvestmentAccountPortfolioRunOverlayValueCalculatorImpl extends BaseInvestmentAccountCalculationSnapshotCalculator implements ValidationAware {

	private InvestmentGroupService investmentGroupService;
	private PortfolioRunService portfolioRunService;
	private ProductOverlayService productOverlayService;
	private ProductReplicationService productReplicationService;
	private PortfolioTargetRunService portfolioTargetRunService;

	/**
	 * How the value of the overlay run is calculated. i.e. Synthetic Exposure, Manager Cash, etc.
	 * See below for enum class
	 */
	private OverlayValueTypes overlayValueType;

	/**
	 * If set, limit the positions included to those in the selected instrument group
	 */
	private Short includeInvestmentGroupId;

	private RunProcessingTypes runProcessingType;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getOverlayValueType(), "Overlay Value type is required");
		if (!getOverlayValueType().isSecurityDetails()) {
			ValidationUtils.assertNull(getIncludeInvestmentGroupId(), "Instrument Group selection is not valid for overlay value type: " + getOverlayValueType());
		}
	}


	@Override
	protected List<InvestmentAccountCalculationSnapshotDetail> calculateInvestmentAccountCalculationSnapshotDetailList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		switch (getOverlayValueType()) {
			case SYNTHETIC_EXPOSURE:
				return calculateProductOverlayAssetClassReplicationDetails(snapshot, context);
			case MANAGER_CASH:
				return calculateProductOverlayManagerAccountDetails(snapshot, context);
			default:
				throw new ValidationException("Calculation Not Defined for Overlay Value Type " + getOverlayValueType());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Overlay Valuations                    /////////////
	////////////////////////////////////////////////////////////////////////////////

	protected enum OverlayValueTypes {

		SYNTHETIC_EXPOSURE(true),
		MANAGER_CASH(false);

		/**
		 * If the value uses security level details.  Used to drive if instrument group selection is allowed
		 */
		private final boolean securityDetails;


		OverlayValueTypes(boolean securityDetails) {
			this.securityDetails = securityDetails;
		}


		public boolean isSecurityDetails() {
			return this.securityDetails;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Processing types                       /////////////
	////////////////////////////////////////////////////////////////////////////////

	protected enum RunProcessingTypes {DEFAULT_OVERLAY, PORTFOLIO_TARGETS}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<InvestmentAccountCalculationSnapshotDetail> calculateProductOverlayAssetClassReplicationDetails(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();
		// Add parent rows at the asset class level that is the abs(sum(details)) under that asset class
		Map<Short, InvestmentAccountCalculationSnapshotDetail> assetClassDetailMap = new HashMap<>();

		if (getRunProcessingType() == RunProcessingTypes.PORTFOLIO_TARGETS) {
			appendDetailsForReplicationList(snapshot, getPortfolioTargetRunReplicationList(snapshot, context), detailList, assetClassDetailMap);
		}
		else {
			appendDetailsForReplicationList(snapshot, getProductOverlayAssetClassReplicationList(snapshot, context), detailList, assetClassDetailMap);
		}

		// Then go through the asset class items and set calculated value to be the abs of the calculated value
		for (InvestmentAccountCalculationSnapshotDetail assetClassDetail : CollectionUtils.getIterable(assetClassDetailMap.values())) {
			assetClassDetail.setCalculatedValue(MathUtils.abs(assetClassDetail.getCalculatedValue()));
		}
		return detailList;
	}


	private <T extends BasePortfolioRunReplication> void appendDetailsForReplicationList(InvestmentAccountCalculationSnapshot snapshot, List<T> repList, List<InvestmentAccountCalculationSnapshotDetail> detailList, Map<Short, InvestmentAccountCalculationSnapshotDetail> assetClassDetailMap) {
		for (T replication : CollectionUtils.getIterable(repList)) {
			// NOTE: Excluded from Overlay Total is used for rollup asset classes, matching replications which should already be filtered
			// Cash Exposure isn't previously excluded through SQL because the SQL to join on the coalesce of trading account, main account is cumbersome
			// and since we already look at each rep to create billing basis snapshot it's easier to filter here for the few accounts where this can happen (Macalester, Marist)
			if (replication.isExcludedFromOverlayTotal() || replication.isCashExposure()) {
				continue;
			}

			InvestmentAssetClass assetClass = getAssetClass(replication.getSecurity(), replication.getAccountAssetClass());

			InvestmentAccountCalculationSnapshotDetail detail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, replication.getSecurity(), replication.getActualContracts());
			detail.setPreliminaryCalculatedValue(MathUtils.add(replication.getCurrencyTotalBaseAmount(), replication.getActualExposure()));
			detail.setCalculatedValue(MathUtils.add(MathUtils.abs(replication.getCurrencyTotalBaseAmount()), MathUtils.abs(replication.getActualExposure())));

			if (assetClass != null) {
				InvestmentAccountCalculationSnapshotDetail assetClassDetail = assetClassDetailMap.get(assetClass.getId());
				if (assetClassDetail == null) {
					assetClassDetail = InvestmentAccountCalculationSnapshotDetail.ofAssetClass(snapshot, assetClass);
					assetClassDetailMap.put(assetClass.getId(), assetClassDetail);
					detailList.add(assetClassDetail);
				}
				assetClassDetail.setPreliminaryCalculatedValue(MathUtils.add(assetClassDetail.getPreliminaryCalculatedValue(), detail.getPreliminaryCalculatedValue()));
				assetClassDetail.setCalculatedValue(MathUtils.add(assetClassDetail.getCalculatedValue(), detail.getCalculatedValue()));

				detail.setParent(assetClassDetail);
			}
			detailList.add(detail);
		}
	}


	private List<ProductOverlayAssetClassReplication> getProductOverlayAssetClassReplicationList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		// Note: If Snapshot Date is a Weekend - Move to Previous Weekday
		Date balanceDate = getBalanceDateForSnapshotDate(context.getSnapshotDate());

		// We NEVER Include Matching Replications, so always filter the replication list to exclude them so we don't end up with extra rows
		// in the snapshot table with 0 Values
		List<ProductOverlayAssetClassReplication> replicationList = getProductOverlayService().getProductOverlayAssetClassReplicationListByAccountMainRun(snapshot.getInvestmentAccount().getId(), balanceDate, balanceDate, true);

		// If Investment Group ID Is Populated - Filter on that Here
		if (getIncludeInvestmentGroupId() != null) {
			List<ProductOverlayAssetClassReplication> filteredList = new ArrayList<>();
			for (ProductOverlayAssetClassReplication rep : CollectionUtils.getIterable(replicationList)) {
				if (getInvestmentGroupService().isInvestmentInstrumentInGroup(getIncludeInvestmentGroupId(), rep.getSecurity().getInstrument().getId())) {
					filteredList.add(rep);
				}
			}
			return filteredList;
		}
		return replicationList;
	}


	private List<PortfolioTargetRunReplication> getPortfolioTargetRunReplicationList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		// Note: If Snapshot Date is a Weekend - Move to Previous Weekday
		Date balanceDate = getBalanceDateForSnapshotDate(context.getSnapshotDate());

		// We NEVER Include Matching Replications, so always filter the replication list to exclude them so we don't end up with extra rows
		// in the snapshot table with 0 Values
		List<PortfolioTargetRunReplication> replicationList = getPortfolioTargetRunService().getPortfolioTargetRunReplicationListByAccountMainRun(snapshot.getInvestmentAccount().getId(), balanceDate, balanceDate, true);

		// If Investment Group ID Is Populated - Filter on that Here
		if (getIncludeInvestmentGroupId() != null) {
			List<PortfolioTargetRunReplication> filteredList = new ArrayList<>();
			for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(replicationList)) {
				if (getInvestmentGroupService().isInvestmentInstrumentInGroup(getIncludeInvestmentGroupId(), rep.getSecurity().getInstrument().getId())) {
					filteredList.add(rep);
				}
			}
			return filteredList;
		}
		return replicationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<InvestmentAccountCalculationSnapshotDetail> calculateProductOverlayManagerAccountDetails(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		List<InvestmentAccountCalculationSnapshotDetail> detailList = new ArrayList<>();
		// Add parent rows at the asset class level that is the (sum(details)) under that asset class
		Map<Short, InvestmentAccountCalculationSnapshotDetail> assetClassDetailMap = new HashMap<>();

		List<ProductOverlayManagerAccount> overlayManagerAccountList = getProductOverlayManagerAccountList(snapshot, context);
		for (ProductOverlayManagerAccount overlayManagerAccount : CollectionUtils.getIterable(overlayManagerAccountList)) {

			// We don't have separate fields for managers, only securities, so just group by asset class
			InvestmentAssetClass assetClass = overlayManagerAccount.getAccountAssetClass().getAssetClass();

			InvestmentAccountCalculationSnapshotDetail assetClassDetail = assetClassDetailMap.get(assetClass.getId());
			if (assetClassDetail == null) {
				assetClassDetail = InvestmentAccountCalculationSnapshotDetail.ofAssetClassAndSecurity(snapshot, assetClass, snapshot.getCalculatedValueCurrency(), null);
				assetClassDetailMap.put(assetClass.getId(), assetClassDetail);
				detailList.add(assetClassDetail);
			}
			assetClassDetail.setPreliminaryCalculatedValue(MathUtils.add(assetClassDetail.getPreliminaryCalculatedValue(), overlayManagerAccount.getCashAllocation()));
			assetClassDetail.setCalculatedValue(MathUtils.add(assetClassDetail.getCalculatedValue(), overlayManagerAccount.getCashAllocation()));
		}
		return detailList;
	}


	private List<ProductOverlayManagerAccount> getProductOverlayManagerAccountList(InvestmentAccountCalculationSnapshot snapshot, InvestmentAccountCalculationProcessorContext context) {
		// Note: If Snapshot Date is a Weekend - Move to Previous Weekday
		Date balanceDate = getBalanceDateForSnapshotDate(context.getSnapshotDate());

		PortfolioRun run = getPortfolioRunService().getPortfolioRunByMainRun(snapshot.getInvestmentAccount().getId(), balanceDate);
		if (run == null) {
			return null;
		}

		ProductOverlayManagerAccountSearchForm searchForm = new ProductOverlayManagerAccountSearchForm();
		searchForm.setRollupAssetClass(false);
		searchForm.setRunId(run.getId());
		return getProductOverlayService().getProductOverlayManagerAccountList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public ProductReplicationService getProductReplicationService() {
		return this.productReplicationService;
	}


	public void setProductReplicationService(ProductReplicationService productReplicationService) {
		this.productReplicationService = productReplicationService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public OverlayValueTypes getOverlayValueType() {
		return this.overlayValueType;
	}


	public void setOverlayValueType(OverlayValueTypes overlayValueType) {
		this.overlayValueType = overlayValueType;
	}


	public Short getIncludeInvestmentGroupId() {
		return this.includeInvestmentGroupId;
	}


	public void setIncludeInvestmentGroupId(Short includeInvestmentGroupId) {
		this.includeInvestmentGroupId = includeInvestmentGroupId;
	}


	public RunProcessingTypes getRunProcessingType() {
		return this.runProcessingType;
	}


	public void setRunProcessingType(RunProcessingTypes runProcessingType) {
		this.runProcessingType = runProcessingType;
	}
}
