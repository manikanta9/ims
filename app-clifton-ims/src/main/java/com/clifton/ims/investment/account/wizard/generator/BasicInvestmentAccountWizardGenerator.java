package com.clifton.ims.investment.account.wizard.generator;

import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.setup.BusinessClientSetupService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyPlatform;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.company.contact.BusinessCompanyContactMapping;
import com.clifton.business.company.contact.BusinessCompanyContactService;
import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.business.contract.BusinessContract;
import com.clifton.business.contract.BusinessContractService;
import com.clifton.business.service.BusinessService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.compliance.old.rule.ComplianceOldService;
import com.clifton.compliance.old.rule.ComplianceRuleOld;
import com.clifton.compliance.old.rule.ComplianceRuleTypeOld;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.investment.account.wizard.InvestmentAccountWizard;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.audit.auditor.SystemAuditDaoUtils;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.trade.restriction.TradeRestrictionBroker;
import com.clifton.trade.restriction.TradeRestrictionBrokerService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * @author manderson
 */
public class BasicInvestmentAccountWizardGenerator implements InvestmentAccountWizardGenerator {


	private BillingBasisService billingBasisService;
	private BillingDefinitionService billingDefinitionService;
	private BillingInvoiceService billingInvoiceService;

	private BusinessClientService businessClientService;
	private BusinessClientSetupService businessClientSetupService;

	private BusinessCompanyService businessCompanyService;
	private BusinessCompanyContactService businessCompanyContactService;
	private BusinessContactService businessContactService;

	private BusinessContractService businessContractService;

	private BusinessServiceService businessServiceService;

	private ComplianceOldService complianceOldService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private InvestmentInstrumentService investmentInstrumentService;

	private SecurityUserService securityUserService;

	private SystemNoteService systemNoteService;
	private SystemSchemaService systemSchemaService;

	private TradeRestrictionBrokerService tradeRestrictionBrokerService;

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If set, the use of this wizard is limited to cases where this condition is true.
	 * Used mostly, for example, to limit users to specific wizards - i.e. Team Westport can only use Westport specific wizards
	 */
	private Integer useConditionId;


	/**
	 * UI list of field overrides.  i.e. changing requirement on/off, hiding fields, etc.
	 * Should be a string array for UI to iterate through, i.e. [{name: 'businessClient.name', hidden: true}]
	 */
	private String fieldOverrides;


	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If populated and there is an available transition to get the client relationship to the entered state name then will transition the client relationship to that state
	 * Used to auto activate the client relationship
	 */
	private String transitionBusinessClientRelationshipToState;

	/**
	 * If populated and there is an available transition to get the client account to the entered state name then will transition the client account to that state
	 * Used to immediately submit the client account to pending approval (2 step approval process)
	 */
	private String transitionClientAccountToState;

	/**
	 * If populated and there is an available transition to get the holding account to the entered state name then will transition the holding account to that state
	 * Used to auto activate the holding account
	 */
	private String transitionHoldingAccountToState;


	/**
	 * If auto generating client account number is allowed
	 */
	private boolean autoGeneratedClientAccountNumberAllowed;

	/**
	 * If we auto generate the client account number, can optionally use a prefix
	 */
	private String autoGeneratedClientAccountNumberPrefix;

	/**
	 * We will use the client account id, but pad it to 6 digits
	 */
	private Short autoGeneratedClientAccountDigits = 6;

	/**
	 * Making a property - not likely to change, but if it does can be exposed as an actual property
	 */
	private String clientCategoryName = "Client (Investing Entity)";


	/**
	 * If true, then based on given relationship purposes where investment type is supplied, will create
	 * a ComplianceRuleOld for approved contracts for those investment types.
	 */
	private boolean autoCreateClientApprovedContracts;

	private String clientApprovedContractsRuleName = "Client Approved Contracts";


	/**
	 * DEFAULT VALUES FOR FIELDS ON THE SCREEN
	 */

	private Integer[] defaultRelationshipManagerContactIds;

	private Integer defaultBaseCurrencyId;

	private Short defaultTeamSecurityGroupId;

	private Short defaultBusinessServiceId;

	private Short defaultBusinessServiceProcessingTypeId;

	private Integer defaultClientAccountIssuingCompanyId;

	private Short defaultHoldingAccountTypeId;

	private Integer defaultHoldingAccountIssuingCompanyId;

	private Short[] defaultRelationshipPurposeIds;

	private Integer[] defaultRestrictedBrokerCompanyIds;

	private Integer defaultBusinessCompanyPlatformId;

	private Boolean defaultWrapAccount;

	private Short defaultBillingInvoiceTypeId;

	private BillingFrequencies defaultBillingFrequency;

	private Short defaultBillingBasisValuationTypeId;

	private Short defaultBillingBasisCalculationTypeId;

	private boolean defaultBillingDraft;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String billingDefinitionEditableWorkflowStateName = "Draft";

	private String billingDefinitionAfterEditWorkflowStateName = "Pending Approval";

	private String billingDefinitionEditNoteTypeName = "Definition Changes";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getInvestmentAccountWizardUseConditionId() {
		return getUseConditionId();
	}


	@Override
	public InvestmentAccountWizard populateInvestmentAccountWizard(InvestmentAccountWizard investmentAccountWizard) {
		InvestmentAccount clientAccount = new InvestmentAccount();
		InvestmentAccount holdingAccount = new InvestmentAccount();

		if (!ArrayUtils.isEmpty(getDefaultRelationshipManagerContactIds())) {
			investmentAccountWizard.setRelationshipManagerContactList(new ArrayList<>());
			for (Integer contactId : getDefaultRelationshipManagerContactIds()) {
				BusinessContact contact = getBusinessContactService().getBusinessContact(contactId);
				if (contact != null) {
					BusinessCompanyContact businessCompanyContact = new BusinessCompanyContact();
					businessCompanyContact.setReferenceTwo(contact);
					investmentAccountWizard.getRelationshipManagerContactList().add(businessCompanyContact);
				}
			}
		}

		if (getDefaultBaseCurrencyId() != null) {
			InvestmentSecurity currency = getInvestmentInstrumentService().getInvestmentSecurity(getDefaultBaseCurrencyId());
			clientAccount.setBaseCurrency(currency);
			holdingAccount.setBaseCurrency(currency);
		}
		if (getDefaultTeamSecurityGroupId() != null) {
			SecurityGroup securityGroup = getSecurityUserService().getSecurityGroup(getDefaultTeamSecurityGroupId());
			clientAccount.setTeamSecurityGroup(securityGroup);
		}
		if (getDefaultBusinessServiceId() != null) {
			BusinessService businessService = getBusinessServiceService().getBusinessService(getDefaultBusinessServiceId());
			clientAccount.setBusinessService(businessService);
		}
		if (getDefaultBusinessServiceProcessingTypeId() != null) {
			BusinessServiceProcessingType processingType = getBusinessServiceService().getBusinessServiceProcessingType(getDefaultBusinessServiceProcessingTypeId());
			clientAccount.setServiceProcessingType(processingType);
		}
		if (getDefaultClientAccountIssuingCompanyId() != null) {
			BusinessCompany issuingCompany = getBusinessCompanyService().getBusinessCompany(getDefaultClientAccountIssuingCompanyId());
			clientAccount.setIssuingCompany(issuingCompany);
		}
		if (getDefaultHoldingAccountIssuingCompanyId() != null) {
			BusinessCompany issuingCompany = getBusinessCompanyService().getBusinessCompany(getDefaultHoldingAccountIssuingCompanyId());
			holdingAccount.setIssuingCompany(issuingCompany);

			if (getDefaultBusinessCompanyPlatformId() != null) {
				BusinessCompanyPlatform companyPlatform = getBusinessCompanyService().getBusinessCompanyPlatform(getDefaultBusinessCompanyPlatformId());
				holdingAccount.setCompanyPlatform(companyPlatform);

				if (getDefaultWrapAccount() != null) {
					holdingAccount.setWrapAccount(getDefaultWrapAccount());
				}
			}
		}
		if (getDefaultHoldingAccountTypeId() != null) {
			InvestmentAccountType accountType = getInvestmentAccountService().getInvestmentAccountType(getDefaultHoldingAccountTypeId());
			holdingAccount.setType(accountType);
		}

		if (!ArrayUtils.isEmpty(getDefaultRelationshipPurposeIds())) {
			investmentAccountWizard.setRelationshipPurposeList(new ArrayList<>());
			for (Short relationshipPurposeId : getDefaultRelationshipPurposeIds()) {
				investmentAccountWizard.getRelationshipPurposeList().add(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipPurpose(relationshipPurposeId));
			}
		}

		if (!ArrayUtils.isEmpty(getDefaultRestrictedBrokerCompanyIds())) {
			investmentAccountWizard.setRestrictedBrokerCompanyList(new ArrayList<>());
			for (Integer companyId : getDefaultRestrictedBrokerCompanyIds()) {
				investmentAccountWizard.getRestrictedBrokerCompanyList().add(getBusinessCompanyService().getBusinessCompany(companyId));
			}
		}

		if (getDefaultBillingFrequency() != null) {
			investmentAccountWizard.setBillingFrequency(getDefaultBillingFrequency());
		}
		if (getDefaultBillingInvoiceTypeId() != null) {
			investmentAccountWizard.setBillingInvoiceType(getBillingInvoiceService().getBillingInvoiceType(getDefaultBillingInvoiceTypeId()));
			investmentAccountWizard.setInvoicePostedToPortal(investmentAccountWizard.getBillingInvoiceType().isInvoicePostedToPortalDefault());
		}

		if (getDefaultBillingBasisValuationTypeId() != null) {
			investmentAccountWizard.setBillingBasisValuationType(getBillingBasisService().getBillingBasisValuationType(getDefaultBillingBasisValuationTypeId()));
		}
		if (getDefaultBillingBasisCalculationTypeId() != null) {
			investmentAccountWizard.setBillingBasisCalculationType(getBillingBasisService().getBillingBasisCalculationType(getDefaultBillingBasisCalculationTypeId()));
		}
		investmentAccountWizard.setBillingDraft(isDefaultBillingDraft());

		investmentAccountWizard.setClientInvestmentAccount(clientAccount);
		investmentAccountWizard.setHoldingInvestmentAccount(holdingAccount);
		return investmentAccountWizard;
	}


	@Override
	public InvestmentAccountWizard generateInvestmentAccountWizard(InvestmentAccountWizard investmentAccountWizard, boolean ignoreValidation) {
		// Simple Validation:
		if (!CollectionUtils.isEmpty(investmentAccountWizard.getRelationshipPurposeList())) {
			ValidationUtils.assertNotNull(investmentAccountWizard.getRelationshipPurposeStartDate(), "Relationship Purpose Start Date is required when purposes are supplied.");
		}
		if (!CollectionUtils.isEmpty(investmentAccountWizard.getRestrictedBrokerCompanyList())) {
			ValidationUtils.assertFalse(investmentAccountWizard.getHoldingInvestmentAccount().getType().isExecutionWithIssuerRequired(), "Cannot add restricted brokers because holding account is [" + investmentAccountWizard.getHoldingInvestmentAccount().getType().getName() + "] and execution is allowed with issuer [" + investmentAccountWizard.getHoldingInvestmentAccount().getIssuingCompany().getName() + "] only.");
		}

		if (investmentAccountWizard.isClientRelationshipUsed()) {
			// Client Relationship
			ValidationUtils.assertNotNull(investmentAccountWizard.getBusinessClientRelationship(), "Client Relationship is Required.");
			if (investmentAccountWizard.getBusinessClientRelationship().isNewBean()) {
				// Ensure for Relationship Manager - if multiple selected only 1 is primary - validate before saving the client relationship
				if (CollectionUtils.getSize(investmentAccountWizard.getRelationshipManagerContactList()) > 1) {
					BusinessCompanyContactMapping mapping = getBusinessCompanyContactService().getBusinessCompanyContactMappingFor(getBusinessContactService().getBusinessContactTypeByName(BusinessContactType.RELATIONSHIP_MANAGER_CONTACT_TYPE).getId(), getBusinessCompanyService().getBusinessCompanyTypeByName(BusinessClientRelationship.COMPANY_TYPE_NAME).getId());
					if (mapping == null || mapping.isOnePrimaryRequired()) {
						ValidationUtils.assertEquals(1, CollectionUtils.getSize(BeanUtils.filter(investmentAccountWizard.getRelationshipManagerContactList(), BusinessCompanyContact::isPrimary)), "When multiple relationship managers are entered, one and only one must be selected as the primary.");
					}
				}
				investmentAccountWizard.setBusinessClientRelationship(getBusinessClientService().saveBusinessClientRelationship(investmentAccountWizard.getBusinessClientRelationship()));

				if (!CollectionUtils.isEmpty(investmentAccountWizard.getRelationshipManagerContactList())) {
					for (BusinessCompanyContact businessCompanyContact : investmentAccountWizard.getRelationshipManagerContactList()) {
						businessCompanyContact.setReferenceOne(investmentAccountWizard.getBusinessClientRelationship().getCompany());
						businessCompanyContact.setContactType(getBusinessContactService().getBusinessContactTypeByName(BusinessContactType.RELATIONSHIP_MANAGER_CONTACT_TYPE));
					}
					getBusinessCompanyContactService().saveBusinessCompanyContactList(investmentAccountWizard.getRelationshipManagerContactList(), ignoreValidation);
				}
			}
			else {
				// Ensure no updates from the screen if we are re-using existing
				investmentAccountWizard.setBusinessClientRelationship(getBusinessClientService().getBusinessClientRelationship(investmentAccountWizard.getBusinessClientRelationship().getId()));
			}
			// Transition Client Relationship If Applies
			executeTransitionForEntity(BusinessClientRelationship.BUSINESS_CLIENT_RELATIONSHIP_TABLE_NAME, investmentAccountWizard.getBusinessClientRelationship().getId(), getTransitionBusinessClientRelationshipToState());

			// Client Relationship Docs
			investmentAccountWizard.setBusinessClientRelationshipContract1(saveBusinessContract(investmentAccountWizard.getBusinessClientRelationshipContract1(), investmentAccountWizard.getBusinessClientRelationship().getCompany(), investmentAccountWizard.getBusinessClientRelationshipFile1()));
			investmentAccountWizard.setBusinessClientRelationshipContract2(saveBusinessContract(investmentAccountWizard.getBusinessClientRelationshipContract2(), investmentAccountWizard.getBusinessClientRelationship().getCompany(), investmentAccountWizard.getBusinessClientRelationshipFile2()));
			investmentAccountWizard.setBusinessClientRelationshipContract3(saveBusinessContract(investmentAccountWizard.getBusinessClientRelationshipContract3(), investmentAccountWizard.getBusinessClientRelationship().getCompany(), investmentAccountWizard.getBusinessClientRelationshipFile3()));
			investmentAccountWizard.setBusinessClientRelationshipContract4(saveBusinessContract(investmentAccountWizard.getBusinessClientRelationshipContract4(), investmentAccountWizard.getBusinessClientRelationship().getCompany(), investmentAccountWizard.getBusinessClientRelationshipFile4()));
		}

		// Client
		ValidationUtils.assertNotNull(investmentAccountWizard.getBusinessClient(), "Client is Required.");
		if (investmentAccountWizard.getBusinessClient().isNewBean()) {
			investmentAccountWizard.getBusinessClient().setClientRelationship(investmentAccountWizard.getBusinessClientRelationship());
			investmentAccountWizard.getBusinessClient().setCategory(getBusinessClientSetupService().getBusinessClientCategoryByName(getClientCategoryName()));
			investmentAccountWizard.setBusinessClient(getBusinessClientService().saveBusinessClient(investmentAccountWizard.getBusinessClient()));
		}
		else {
			// Ensure no updates from the screen if we are re-using existing
			investmentAccountWizard.setBusinessClient(getBusinessClientService().getBusinessClient(investmentAccountWizard.getBusinessClient().getId()));
		}

		// Client Docs
		investmentAccountWizard.setBusinessClientContract1(saveBusinessContract(investmentAccountWizard.getBusinessClientContract1(), investmentAccountWizard.getBusinessClient().getCompany(), investmentAccountWizard.getBusinessClientFile1()));
		investmentAccountWizard.setBusinessClientContract2(saveBusinessContract(investmentAccountWizard.getBusinessClientContract2(), investmentAccountWizard.getBusinessClient().getCompany(), investmentAccountWizard.getBusinessClientFile2()));
		investmentAccountWizard.setBusinessClientContract3(saveBusinessContract(investmentAccountWizard.getBusinessClientContract3(), investmentAccountWizard.getBusinessClient().getCompany(), investmentAccountWizard.getBusinessClientFile3()));
		investmentAccountWizard.setBusinessClientContract4(saveBusinessContract(investmentAccountWizard.getBusinessClientContract4(), investmentAccountWizard.getBusinessClient().getCompany(), investmentAccountWizard.getBusinessClientFile4()));

		// Client Account
		ValidationUtils.assertNotNull(investmentAccountWizard.getClientInvestmentAccount(), "Client Account information is required.");
		investmentAccountWizard.getClientInvestmentAccount().setBusinessClient(investmentAccountWizard.getBusinessClient());
		investmentAccountWizard.getClientInvestmentAccount().setType(getInvestmentAccountService().getInvestmentAccountTypeByOurAccount());
		boolean generateNumber = false;
		if (StringUtils.isEmpty(investmentAccountWizard.getClientInvestmentAccount().getNumber())) {
			if (isAutoGeneratedClientAccountNumberAllowed()) {
				generateNumber = true;
				investmentAccountWizard.getClientInvestmentAccount().setNumber(UUID.randomUUID().toString());
			}
			// UI should not allow getting here, but as a fail-safe
			else {
				ValidationUtils.fail("Client Account number is required.");
			}
		}
		investmentAccountWizard.setClientInvestmentAccount(getInvestmentAccountService().saveInvestmentAccount(investmentAccountWizard.getClientInvestmentAccount()));
		if (generateNumber) {
			investmentAccountWizard.getClientInvestmentAccount().setNumber(getClientAccountAutoGeneratedNumber(investmentAccountWizard.getClientInvestmentAccount()));
			DaoUtils.executeWithPostUpdateFlushEnabled(() ->
					SystemAuditDaoUtils.executeWithAuditingDisabled(new String[]{"number"}, () -> {
						investmentAccountWizard.setClientInvestmentAccount(getInvestmentAccountService().saveInvestmentAccount(investmentAccountWizard.getClientInvestmentAccount()));
						return investmentAccountWizard.getClientInvestmentAccount();
					}));
		}

		// Transition Client Account If Applies
		executeTransitionForEntity(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME, investmentAccountWizard.getClientInvestmentAccount().getId(), getTransitionClientAccountToState());

		// Holding Account
		if (investmentAccountWizard.isHoldingAccountUsed()) {
			ValidationUtils.assertNotNull(investmentAccountWizard.getHoldingInvestmentAccount(), "Holding Account information is required.");
			if (investmentAccountWizard.getHoldingInvestmentAccount().isNewBean()) {
				investmentAccountWizard.getHoldingInvestmentAccount().setBusinessClient(investmentAccountWizard.getBusinessClient());
				DaoUtils.executeWithPostUpdateFlushEnabled(() -> investmentAccountWizard.setHoldingInvestmentAccount(getInvestmentAccountService().saveInvestmentAccount(investmentAccountWizard.getHoldingInvestmentAccount())));
			}

			// Transition Holding Account If Applies
			executeTransitionForEntity(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME, investmentAccountWizard.getHoldingInvestmentAccount().getId(), getTransitionHoldingAccountToState());


			// Account Relationships
			List<InvestmentType> investmentTypeList = new ArrayList<>();
			if (!CollectionUtils.isEmpty(investmentAccountWizard.getRelationshipPurposeList())) {
				for (InvestmentAccountRelationshipPurpose relationshipPurpose : investmentAccountWizard.getRelationshipPurposeList()) {
					InvestmentAccountRelationship relationship = new InvestmentAccountRelationship();
					relationship.setReferenceOne(investmentAccountWizard.getClientInvestmentAccount());
					relationship.setReferenceTwo(investmentAccountWizard.getHoldingInvestmentAccount());
					relationship.setStartDate(investmentAccountWizard.getRelationshipPurposeStartDate());
					relationship.setPurpose(relationshipPurpose);
					if (relationship.getPurpose().getInvestmentType() != null) {
						investmentTypeList.add(relationship.getPurpose().getInvestmentType());
					}
					getInvestmentAccountRelationshipService().saveInvestmentAccountRelationship(relationship);
				}
			}


			if (isAutoCreateClientApprovedContracts()) {
				ComplianceRuleTypeOld ruleTypeOld = getComplianceOldService().getComplianceRuleTypeOldByName(getClientApprovedContractsRuleName());
				for (InvestmentType investmentType : CollectionUtils.getIterable(investmentTypeList)) {
					ComplianceRuleOld rule = new ComplianceRuleOld();
					rule.setClientInvestmentAccount(investmentAccountWizard.getClientInvestmentAccount());
					rule.setRuleType(ruleTypeOld);
					rule.setInvestmentType(investmentType);
					getComplianceOldService().saveComplianceRuleOld(rule);
				}
			}
		}

		if (!CollectionUtils.isEmpty(investmentAccountWizard.getRestrictedBrokerCompanyList())) {
			for (BusinessCompany company : investmentAccountWizard.getRestrictedBrokerCompanyList()) {
				TradeRestrictionBroker tradeRestrictionBroker = new TradeRestrictionBroker();
				tradeRestrictionBroker.setClientAccount(investmentAccountWizard.getClientInvestmentAccount());
				tradeRestrictionBroker.setBrokerCompany(company);
				tradeRestrictionBroker.setInclude(true);
				getTradeRestrictionBrokerService().saveTradeRestrictionBroker(tradeRestrictionBroker);
			}
		}

		setupBillingForClientAccount(investmentAccountWizard);
		return investmentAccountWizard;
	}


	private BusinessContract saveBusinessContract(BusinessContract contract, BusinessCompany company, MultipartFile file) {
		if (contract != null && contract.getContractType() != null) {
			contract.setCompany(company);
			contract.setName(company.getName() + " - " + contract.getContractType().getName());
			return getBusinessContractService().saveBusinessContractWithFile(contract, file);
		}
		return null;
	}


	private void executeTransitionForEntity(String tableName, Integer id, String transitionToStateName) {
		if (!StringUtils.isEmpty(transitionToStateName)) {
			List<WorkflowState> stateList = getWorkflowDefinitionService().getWorkflowStateNextAllowedList(tableName, MathUtils.getNumberAsLong(id));
			for (WorkflowState st : CollectionUtils.getIterable(stateList)) {
				if (transitionToStateName.equals(st.getName())) {
					getWorkflowTransitionService().executeWorkflowTransition(tableName, id, st.getId());
					return;
				}
			}
		}
	}


	private String getClientAccountAutoGeneratedNumber(InvestmentAccount clientAccount) {
		StringBuilder num = new StringBuilder("" + clientAccount.getId());
		while (num.length() < getAutoGeneratedClientAccountDigits()) {
			num.insert(0, "0");
		}
		return getAutoGeneratedClientAccountNumberPrefix() + num;
	}


	private void setupBillingForClientAccount(InvestmentAccountWizard investmentAccountWizard) {
		if (investmentAccountWizard.isBillingUsed()) {
			SystemTable billingDefinitionTable = getSystemSchemaService().getSystemTableByName(BillingDefinition.BILLING_DEFINITION_TABLE_NAME);

			// If using an existing billing definition - ensure it's in editable state - if not transition it and add note
			if (investmentAccountWizard.getBillingDefinition() != null) {
				boolean transitionDefinition = false;
				if (!getBillingDefinitionEditableWorkflowStateName().equals(investmentAccountWizard.getBillingDefinition().getWorkflowState().getName())) {
					transitionDefinition = true;
					executeTransitionForEntity(BillingDefinition.BILLING_DEFINITION_TABLE_NAME, investmentAccountWizard.getBillingDefinition().getId(), getBillingDefinitionEditableWorkflowStateName());
				}
				// Create a note so we know why the billing definition is being updated
				SystemNote systemNote = new SystemNote();
				systemNote.setNoteType(getSystemNoteService().getSystemNoteTypeByTableAndName(billingDefinitionTable.getId(), getBillingDefinitionEditNoteTypeName()));
				systemNote.setFkFieldId(BeanUtils.getIdentityAsLong(investmentAccountWizard.getBillingDefinition()));
				systemNote.setText("Updating billing definition to reflect new account [" + investmentAccountWizard.getClientInvestmentAccount().getLabel() + "] on [" + DateUtils.fromDateShort(new Date()) + "].");
				getSystemNoteService().saveSystemNote(systemNote);

				// Pull again so we get the correct definition workflow state name
				BillingDefinition billingDefinition = getBillingDefinitionService().getBillingDefinition(investmentAccountWizard.getBillingDefinition().getId());
				BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount = populateBillingDefinitionInvestmentAccountForBillingDefinition(billingDefinition, investmentAccountWizard);
				getBillingDefinitionService().saveBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccount);

				if (transitionDefinition && !investmentAccountWizard.isBillingDraft()) {
					executeTransitionForEntity(BillingDefinition.BILLING_DEFINITION_TABLE_NAME, billingDefinition.getId(), getBillingDefinitionAfterEditWorkflowStateName());
				}
			}
			else {
				// Adding a new Contract
				BusinessContract billingContract = null;
				if (investmentAccountWizard.getBillingDefinitionContractFile() != null) {
					billingContract = saveBusinessContract(investmentAccountWizard.getBillingDefinitionContract(), investmentAccountWizard.isBillingDefinitionContractUseClientRelationship() ? investmentAccountWizard.getBusinessClientRelationship().getCompany() : investmentAccountWizard.getBusinessClient().getCompany(), investmentAccountWizard.getBillingDefinitionContractFile());
				}
				else {
					ValidationUtils.assertNotNull(investmentAccountWizard.getContractDocumentIndex(), "A client or client relationship contract or a new contract document must be selected for the Billing Definition Contract.");
					switch (investmentAccountWizard.getContractDocumentIndex()) {
						case 1:
							billingContract = investmentAccountWizard.isUseBusinessClientRelationshipContract() ? investmentAccountWizard.getBusinessClientRelationshipContract1() : investmentAccountWizard.getBusinessClientContract1();
							break;
						case 2:
							billingContract = investmentAccountWizard.isUseBusinessClientRelationshipContract() ? investmentAccountWizard.getBusinessClientRelationshipContract2() : investmentAccountWizard.getBusinessClientContract2();
							break;
						case 3:
							billingContract = investmentAccountWizard.isUseBusinessClientRelationshipContract() ? investmentAccountWizard.getBusinessClientRelationshipContract3() : investmentAccountWizard.getBusinessClientContract3();
							break;
						case 4:
							billingContract = investmentAccountWizard.isUseBusinessClientRelationshipContract() ? investmentAccountWizard.getBusinessClientRelationshipContract4() : investmentAccountWizard.getBusinessClientContract4();
							break;
					}
				}
				ValidationUtils.assertNotNull(billingContract, "Unable to determine contract to use to set up billing.  Please re-check your billing contract setup options.");

				BillingDefinition billingDefinition = new BillingDefinition();
				billingDefinition.setBusinessCompany(billingContract != null ? billingContract.getCompany() : null);
				billingDefinition.setBillingContract(billingContract);
				// Should always be USD - just use the base currency of the account
				billingDefinition.setBillingCurrency(investmentAccountWizard.getClientInvestmentAccount().getBaseCurrency());
				billingDefinition.setBillingFrequency(investmentAccountWizard.getBillingFrequency());
				billingDefinition.setInvoiceType(investmentAccountWizard.getBillingInvoiceType());

				billingDefinition.setStartDate(new Date());
				billingDefinition = getBillingDefinitionService().saveBillingDefinition(billingDefinition);

				BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount = populateBillingDefinitionInvestmentAccountForBillingDefinition(billingDefinition, investmentAccountWizard);
				getBillingDefinitionService().saveBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccount);

				if (!investmentAccountWizard.isDoNotAddSchedule() && investmentAccountWizard.getBillingScheduleTemplate() != null) {
					BillingSchedule schedule = getBillingDefinitionService().getBillingScheduleForTemplate(investmentAccountWizard.getBillingScheduleTemplate().getId());
					// Template Description = Schedule Name
					schedule.setName(schedule.getDescription());
					schedule.setDescription(null);
					schedule.setBillingDefinition(billingDefinition);
					getBillingDefinitionService().saveBillingSchedule(schedule);
				}

				if (!investmentAccountWizard.isBillingDraft()) {
					executeTransitionForEntity(BillingDefinition.BILLING_DEFINITION_TABLE_NAME, billingDefinition.getId(), getBillingDefinitionAfterEditWorkflowStateName());
				}
			}
		}
	}


	private BillingDefinitionInvestmentAccount populateBillingDefinitionInvestmentAccountForBillingDefinition(BillingDefinition billingDefinition, InvestmentAccountWizard command) {
		BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount = new BillingDefinitionInvestmentAccount();
		billingDefinitionInvestmentAccount.setReferenceOne(billingDefinition);
		billingDefinitionInvestmentAccount.setReferenceTwo(command.getClientInvestmentAccount());
		billingDefinitionInvestmentAccount.setBillingBasisValuationType(command.getBillingBasisValuationType());
		billingDefinitionInvestmentAccount.setBillingBasisCalculationType(command.getBillingBasisCalculationType());
		return billingDefinitionInvestmentAccount;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public BusinessClientSetupService getBusinessClientSetupService() {
		return this.businessClientSetupService;
	}


	public void setBusinessClientSetupService(BusinessClientSetupService businessClientSetupService) {
		this.businessClientSetupService = businessClientSetupService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public BusinessCompanyContactService getBusinessCompanyContactService() {
		return this.businessCompanyContactService;
	}


	public void setBusinessCompanyContactService(BusinessCompanyContactService businessCompanyContactService) {
		this.businessCompanyContactService = businessCompanyContactService;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public BusinessContractService getBusinessContractService() {
		return this.businessContractService;
	}


	public void setBusinessContractService(BusinessContractService businessContractService) {
		this.businessContractService = businessContractService;
	}


	public BusinessServiceService getBusinessServiceService() {
		return this.businessServiceService;
	}


	public void setBusinessServiceService(BusinessServiceService businessServiceService) {
		this.businessServiceService = businessServiceService;
	}


	public ComplianceOldService getComplianceOldService() {
		return this.complianceOldService;
	}


	public void setComplianceOldService(ComplianceOldService complianceOldService) {
		this.complianceOldService = complianceOldService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public TradeRestrictionBrokerService getTradeRestrictionBrokerService() {
		return this.tradeRestrictionBrokerService;
	}


	public void setTradeRestrictionBrokerService(TradeRestrictionBrokerService tradeRestrictionBrokerService) {
		this.tradeRestrictionBrokerService = tradeRestrictionBrokerService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public Integer getUseConditionId() {
		return this.useConditionId;
	}


	public void setUseConditionId(Integer useConditionId) {
		this.useConditionId = useConditionId;
	}


	public String getFieldOverrides() {
		return this.fieldOverrides;
	}


	public void setFieldOverrides(String fieldOverrides) {
		this.fieldOverrides = fieldOverrides;
	}


	public String getTransitionBusinessClientRelationshipToState() {
		return this.transitionBusinessClientRelationshipToState;
	}


	public void setTransitionBusinessClientRelationshipToState(String transitionBusinessClientRelationshipToState) {
		this.transitionBusinessClientRelationshipToState = transitionBusinessClientRelationshipToState;
	}


	public String getTransitionClientAccountToState() {
		return this.transitionClientAccountToState;
	}


	public void setTransitionClientAccountToState(String transitionClientAccountToState) {
		this.transitionClientAccountToState = transitionClientAccountToState;
	}


	public String getTransitionHoldingAccountToState() {
		return this.transitionHoldingAccountToState;
	}


	public void setTransitionHoldingAccountToState(String transitionHoldingAccountToState) {
		this.transitionHoldingAccountToState = transitionHoldingAccountToState;
	}


	public boolean isAutoGeneratedClientAccountNumberAllowed() {
		return this.autoGeneratedClientAccountNumberAllowed;
	}


	public void setAutoGeneratedClientAccountNumberAllowed(boolean autoGeneratedClientAccountNumberAllowed) {
		this.autoGeneratedClientAccountNumberAllowed = autoGeneratedClientAccountNumberAllowed;
	}


	public String getAutoGeneratedClientAccountNumberPrefix() {
		return this.autoGeneratedClientAccountNumberPrefix;
	}


	public void setAutoGeneratedClientAccountNumberPrefix(String autoGeneratedClientAccountNumberPrefix) {
		this.autoGeneratedClientAccountNumberPrefix = autoGeneratedClientAccountNumberPrefix;
	}


	public Short getAutoGeneratedClientAccountDigits() {
		return this.autoGeneratedClientAccountDigits;
	}


	public void setAutoGeneratedClientAccountDigits(Short autoGeneratedClientAccountDigits) {
		this.autoGeneratedClientAccountDigits = autoGeneratedClientAccountDigits;
	}


	public String getClientCategoryName() {
		return this.clientCategoryName;
	}


	public void setClientCategoryName(String clientCategoryName) {
		this.clientCategoryName = clientCategoryName;
	}


	public Integer getDefaultBaseCurrencyId() {
		return this.defaultBaseCurrencyId;
	}


	public void setDefaultBaseCurrencyId(Integer defaultBaseCurrencyId) {
		this.defaultBaseCurrencyId = defaultBaseCurrencyId;
	}


	public Short getDefaultTeamSecurityGroupId() {
		return this.defaultTeamSecurityGroupId;
	}


	public void setDefaultTeamSecurityGroupId(Short defaultTeamSecurityGroupId) {
		this.defaultTeamSecurityGroupId = defaultTeamSecurityGroupId;
	}


	public Short getDefaultBusinessServiceId() {
		return this.defaultBusinessServiceId;
	}


	public void setDefaultBusinessServiceId(Short defaultBusinessServiceId) {
		this.defaultBusinessServiceId = defaultBusinessServiceId;
	}


	public Short getDefaultBusinessServiceProcessingTypeId() {
		return this.defaultBusinessServiceProcessingTypeId;
	}


	public void setDefaultBusinessServiceProcessingTypeId(Short defaultBusinessServiceProcessingTypeId) {
		this.defaultBusinessServiceProcessingTypeId = defaultBusinessServiceProcessingTypeId;
	}


	public Integer getDefaultClientAccountIssuingCompanyId() {
		return this.defaultClientAccountIssuingCompanyId;
	}


	public void setDefaultClientAccountIssuingCompanyId(Integer defaultClientAccountIssuingCompanyId) {
		this.defaultClientAccountIssuingCompanyId = defaultClientAccountIssuingCompanyId;
	}


	public Short getDefaultHoldingAccountTypeId() {
		return this.defaultHoldingAccountTypeId;
	}


	public void setDefaultHoldingAccountTypeId(Short defaultHoldingAccountTypeId) {
		this.defaultHoldingAccountTypeId = defaultHoldingAccountTypeId;
	}


	public Integer getDefaultHoldingAccountIssuingCompanyId() {
		return this.defaultHoldingAccountIssuingCompanyId;
	}


	public void setDefaultHoldingAccountIssuingCompanyId(Integer defaultHoldingAccountIssuingCompanyId) {
		this.defaultHoldingAccountIssuingCompanyId = defaultHoldingAccountIssuingCompanyId;
	}


	public boolean isAutoCreateClientApprovedContracts() {
		return this.autoCreateClientApprovedContracts;
	}


	public void setAutoCreateClientApprovedContracts(boolean autoCreateClientApprovedContracts) {
		this.autoCreateClientApprovedContracts = autoCreateClientApprovedContracts;
	}


	public String getClientApprovedContractsRuleName() {
		return this.clientApprovedContractsRuleName;
	}


	public void setClientApprovedContractsRuleName(String clientApprovedContractsRuleName) {
		this.clientApprovedContractsRuleName = clientApprovedContractsRuleName;
	}


	public Short[] getDefaultRelationshipPurposeIds() {
		return this.defaultRelationshipPurposeIds;
	}


	public void setDefaultRelationshipPurposeIds(Short[] defaultRelationshipPurposeIds) {
		this.defaultRelationshipPurposeIds = defaultRelationshipPurposeIds;
	}


	public Integer[] getDefaultRestrictedBrokerCompanyIds() {
		return this.defaultRestrictedBrokerCompanyIds;
	}


	public void setDefaultRestrictedBrokerCompanyIds(Integer[] defaultRestrictedBrokerCompanyIds) {
		this.defaultRestrictedBrokerCompanyIds = defaultRestrictedBrokerCompanyIds;
	}


	public Integer getDefaultBusinessCompanyPlatformId() {
		return this.defaultBusinessCompanyPlatformId;
	}


	public void setDefaultBusinessCompanyPlatformId(Integer defaultBusinessCompanyPlatformId) {
		this.defaultBusinessCompanyPlatformId = defaultBusinessCompanyPlatformId;
	}


	public Boolean getDefaultWrapAccount() {
		return this.defaultWrapAccount;
	}


	public void setDefaultWrapAccount(Boolean defaultWrapAccount) {
		this.defaultWrapAccount = defaultWrapAccount;
	}


	public Short getDefaultBillingInvoiceTypeId() {
		return this.defaultBillingInvoiceTypeId;
	}


	public void setDefaultBillingInvoiceTypeId(Short defaultBillingInvoiceTypeId) {
		this.defaultBillingInvoiceTypeId = defaultBillingInvoiceTypeId;
	}


	public BillingFrequencies getDefaultBillingFrequency() {
		return this.defaultBillingFrequency;
	}


	public void setDefaultBillingFrequency(BillingFrequencies defaultBillingFrequency) {
		this.defaultBillingFrequency = defaultBillingFrequency;
	}


	public Short getDefaultBillingBasisValuationTypeId() {
		return this.defaultBillingBasisValuationTypeId;
	}


	public void setDefaultBillingBasisValuationTypeId(Short defaultBillingBasisValuationTypeId) {
		this.defaultBillingBasisValuationTypeId = defaultBillingBasisValuationTypeId;
	}


	public Short getDefaultBillingBasisCalculationTypeId() {
		return this.defaultBillingBasisCalculationTypeId;
	}


	public void setDefaultBillingBasisCalculationTypeId(Short defaultBillingBasisCalculationTypeId) {
		this.defaultBillingBasisCalculationTypeId = defaultBillingBasisCalculationTypeId;
	}


	public String getBillingDefinitionEditableWorkflowStateName() {
		return this.billingDefinitionEditableWorkflowStateName;
	}


	public void setBillingDefinitionEditableWorkflowStateName(String billingDefinitionEditableWorkflowStateName) {
		this.billingDefinitionEditableWorkflowStateName = billingDefinitionEditableWorkflowStateName;
	}


	public String getBillingDefinitionAfterEditWorkflowStateName() {
		return this.billingDefinitionAfterEditWorkflowStateName;
	}


	public void setBillingDefinitionAfterEditWorkflowStateName(String billingDefinitionAfterEditWorkflowStateName) {
		this.billingDefinitionAfterEditWorkflowStateName = billingDefinitionAfterEditWorkflowStateName;
	}


	public String getBillingDefinitionEditNoteTypeName() {
		return this.billingDefinitionEditNoteTypeName;
	}


	public void setBillingDefinitionEditNoteTypeName(String billingDefinitionEditNoteTypeName) {
		this.billingDefinitionEditNoteTypeName = billingDefinitionEditNoteTypeName;
	}


	public boolean isDefaultBillingDraft() {
		return this.defaultBillingDraft;
	}


	public void setDefaultBillingDraft(boolean defaultBillingDraft) {
		this.defaultBillingDraft = defaultBillingDraft;
	}


	public Integer[] getDefaultRelationshipManagerContactIds() {
		return this.defaultRelationshipManagerContactIds;
	}


	public void setDefaultRelationshipManagerContactIds(Integer[] defaultRelationshipManagerContactIds) {
		this.defaultRelationshipManagerContactIds = defaultRelationshipManagerContactIds;
	}
}
