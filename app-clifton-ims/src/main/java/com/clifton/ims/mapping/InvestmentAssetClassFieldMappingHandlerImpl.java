package com.clifton.ims.mapping;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.system.fieldmapping.SystemFieldMapping;
import com.clifton.system.fieldmapping.SystemFieldMappingEntry;
import com.clifton.system.fieldmapping.SystemFieldMappingService;
import com.clifton.system.fieldmapping.search.SystemFieldMappingEntrySearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * Implementation of {@link InvestmentAssetClassFieldMappingHandler}. Looks for asset class mappings for the provided security by {@link com.clifton.investment.setup.group.InvestmentSecurityGroup}. If multiple field mappings match, the one with the first priority order will be selected.
 *
 * @author mitchellf
 */
@Component
public class InvestmentAssetClassFieldMappingHandlerImpl implements InvestmentAssetClassFieldMappingHandler {

	private SystemFieldMappingService systemFieldMappingService;
	private InvestmentSetupService investmentSetupService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAssetClass getAssetClass(InvestmentSecurity security, SystemFieldMapping fieldMapping) {

		// Verify field mapping uses correct entities to avoid incorrect mapping or error from ID type mismatch
		String tableName = fieldMapping.getFieldMappingDefinition().getEntitySystemTable().getName();
		ValidationUtils.assertEquals(tableName, "InvestmentSecurityGroup", "The provided field mapping uses an invalid Field Mapping Definition. Definition entity system table must be InvestmentSecurityGroup, but is [" + tableName + "].");

		SystemFieldMappingEntrySearchForm sf = new SystemFieldMappingEntrySearchForm();
		sf.setSystemFieldMappingId(fieldMapping.getId());
		sf.setOrderBy("priorityOrder:asc");
		List<SystemFieldMappingEntry> entries = getSystemFieldMappingService().getSystemFieldMappingEntryList(sf);
		for (SystemFieldMappingEntry entry : CollectionUtils.getIterable(entries)) {
			if (getInvestmentSecurityGroupService().isInvestmentSecurityInSecurityGroup(security.getId(), (short) entry.getFkFieldId())) {
				return getInvestmentSetupService().getInvestmentAssetClass(new Short(entry.getMappingValue()));
			}
		}

		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemFieldMappingService getSystemFieldMappingService() {
		return this.systemFieldMappingService;
	}


	public void setSystemFieldMappingService(SystemFieldMappingService systemFieldMappingService) {
		this.systemFieldMappingService = systemFieldMappingService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
