package com.clifton.ims.mapping;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.system.fieldmapping.SystemFieldMapping;


/**
 * This is a handler that can be used to to find an {@link InvestmentAssetClass} by a corresponding {@link InvestmentSecurity}. The implementation should use the provided {@link SystemFieldMapping} to find the asset class associated with the provided security.
 *
 * @author mitchellf
 */
public interface InvestmentAssetClassFieldMappingHandler {

	public InvestmentAssetClass getAssetClass(InvestmentSecurity security, SystemFieldMapping fieldMapping);
}
