package com.clifton.ims.reconciliation.event;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.booking.AccountingBookingService;
import com.clifton.accounting.gl.journal.AccountingJournalService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.adjust.AccountingAdjustingJournalService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.simplejournal.AccountingSimpleJournal;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalService;
import com.clifton.accounting.simplejournal.AccountingSimpleJournalType;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.EventNotProcessedException;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.reconciliation.event.jms.ReconciliationEvent;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectJournalData;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class ReconciliationEventJournalListener extends BaseEventListener<ReconciliationEvent> {

	private AccountingBookingService<?> accountingBookingService;

	private AccountingAdjustingJournalService accountingAdjustingJournalService;
	private AccountingJournalService accountingJournalService;
	private AccountingSimpleJournalService accountingSimpleJournalService;
	private AccountingTransactionService accountingTransactionService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private final List<String> RELATIONSHIP_PURPOSE_NAME_LIST = Arrays.asList("Custodian", "Mark to Market", "Trading: Options", "REPO");


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(
				ReconciliationEventObjectCategories.SIMPLE_JOURNAL.name(), ReconciliationEventObjectCategories.ADJUSTING_JOURNAL.name()
		);
	}


	@Override
	public synchronized void onEvent(ReconciliationEvent event) {
		ReconciliationEventObjectData eventObjectData = event.getTarget();
		AssertUtils.assertNotNull(eventObjectData, "No data exists for [" + event.getTarget() + "] for event with name [" + event.getEventName() + "].");
		if (eventObjectData instanceof ReconciliationEventObjectJournalData) {
			if (ReconciliationEventObjectCategories.SIMPLE_JOURNAL.name().equals(event.getEventName())) {
				String eventTypeName = String.valueOf(event.getContextValue("eventTypeName"));
				ReconciliationEventObjectJournalData journalData = (ReconciliationEventObjectJournalData) eventObjectData;
				AccountingSimpleJournalType journalType = getAccountingSimpleJournalService().getAccountingSimpleJournalTypeByName(eventTypeName);
				createSimpleJournal(journalData, journalType);
			}
			else if (ReconciliationEventObjectCategories.ADJUSTING_JOURNAL.name().equals(event.getEventName())) {
				String eventTypeName = String.valueOf(event.getContextValue("eventTypeName"));
				ReconciliationEventObjectJournalData journalData = (ReconciliationEventObjectJournalData) eventObjectData;
				AccountingJournalType journalType = getAccountingJournalService().getAccountingJournalTypeByName(eventTypeName);
				createAdjustingJournal(journalData, journalType);
			}
			else {
				throw new EventNotProcessedException("Unable to handled event!  Unsupported event with name: " + event.getEventName());
			}
		}
		else {
			throw new EventNotProcessedException("Unable to handled event!  Unsupported object of type: " + eventObjectData.getClass());
		}
	}


	private void createSimpleJournal(ReconciliationEventObjectJournalData journalData, AccountingSimpleJournalType journalType) {
		MutableBoolean bookAndPost = new MutableBoolean(true);

		AccountingSimpleJournal simpleJournal = new AccountingSimpleJournal();
		simpleJournal.setJournalType(journalType);
		simpleJournal.setAmount(journalData.getAmount());
		simpleJournal.setDescription(journalType.getLabel());
		simpleJournal.setSettlementDate(journalData.getSettlementDate());
		simpleJournal.setTransactionDate(journalData.getTransactionDate());
		simpleJournal.setOriginalTransactionDate(journalData.getTransactionDate());
		simpleJournal.setExchangeRateToBase(new BigDecimal("1"));
		simpleJournal.setPrivateNote(journalData.getEventNote());

		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccountByNumber(journalData.getHoldingAccountNumber());
		AssertUtils.assertNotNull(holdingAccount, "Unable to locate holding account for number [" + journalData.getHoldingAccountNumber());
		InvestmentAccount clientAccount = getClientAccount(journalData, holdingAccount, bookAndPost);
		ValidationUtils.assertNotNull(clientAccount, "Unable to locate client account for holding account [" + journalData.getHoldingAccountNumber() + "]");

		simpleJournal.setClientInvestmentAccount(clientAccount);
		simpleJournal.setMainHoldingInvestmentAccount(holdingAccount);
		simpleJournal.setOffsettingHoldingInvestmentAccount(holdingAccount);

		simpleJournal.setMainAccountingAccount(journalType.getMainAccountingAccount());
		simpleJournal.setOffsettingAccountingAccount(journalType.getOffsettingAccountingAccount());

		InvestmentSecurity baseCurrency = holdingAccount.getBaseCurrency();
		simpleJournal.setMainInvestmentSecurity(baseCurrency);
		simpleJournal.setOffsettingInvestmentSecurity(baseCurrency);
		simpleJournal = getAccountingSimpleJournalService().saveAccountingSimpleJournal(simpleJournal);
		if (bookAndPost.booleanValue()) {
			getAccountingBookingService().bookAccountingJournal(AccountingJournalType.SIMPLE_JOURNAL, simpleJournal.getId(), true);
		}
		else {
			throw new ValidationException("More than one client account was found for holding account [" + journalData.getHoldingAccountNumber() + "] - Client account [" + (clientAccount != null ? clientAccount.getNumber() : null) + "] was automatically selected for journal creation, but the journal has not been booked and must be manually reviewed and booked in IMS.");
		}
	}


	private InvestmentAccount getClientAccount(ReconciliationEventObjectJournalData journalData, InvestmentAccount holdingAccount, MutableBoolean bookAndPost) {
		InvestmentAccount clientAccount;
		List<InvestmentAccount> accountList = new ArrayList<>();
		for (String relationshipPurpose : CollectionUtils.getIterable(this.RELATIONSHIP_PURPOSE_NAME_LIST)) {
			accountList = CollectionUtils.getStream(getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(holdingAccount.getId(), null, relationshipPurpose, journalData.getTransactionDate()))
					.filter(account -> account.getType().isOurAccount()).collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(accountList)) {
				break;
			}
		}
		if (CollectionUtils.getSize(accountList) == 1) {
			clientAccount = CollectionUtils.getOnlyElement(accountList);
		}
		else {
			//In some cases there are multiple client accounts, so in this case we sort, and choose the 'lowest' based on account number, and the we don't auto bookAndPost
			clientAccount = CollectionUtils.getFirstElement(CollectionUtils.getStream(accountList).sorted(Comparator.comparing(InvestmentAccount::getNumber)).collect(Collectors.toList()));
			bookAndPost.setValue(false);
		}
		return clientAccount;
	}


	private void createAdjustingJournal(ReconciliationEventObjectJournalData journalData, AccountingJournalType journalType) {
		InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccountByNumber(journalData.getHoldingAccountNumber());
		ValidationUtils.assertNotNull(holdingAccount, "Unable to locate holding account with number [" + journalData.getHoldingAccountNumber() + "]");
		InvestmentSecurity investmentSecurity = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(journalData.getSecurityIdentifier(), false);
		ValidationUtils.assertNotNull(investmentSecurity, "Unable to locate security with symbol [" + journalData.getSecurityIdentifier() + "]");

		BigDecimal adjustmentAmount = getAdjustingJournalAmount(journalData, holdingAccount, investmentSecurity);
		AccountingTransaction transaction = getAdjustingJournalTransaction(journalData, journalType, holdingAccount, investmentSecurity);
		if (AccountingJournalType.ADJUSTING_JOURNAL_COMMISSION.equals(journalType.getName())) {
			getAccountingAdjustingJournalService().adjustAccountingJournalCommission(transaction.getParentTransaction().getId(), adjustmentAmount, journalData.getEventNote());
		}
		else if (AccountingJournalType.ADJUSTING_JOURNAL_REALIZED.equals(journalType.getName())) {
			getAccountingAdjustingJournalService().adjustAccountingJournalRealized(transaction.getParentTransaction().getId(), adjustmentAmount, journalData.getEventNote());
		}
		else if (AccountingJournalType.ADJUSTING_JOURNAL_DIVIDEND.equals(journalType.getName())) {
			getAccountingAdjustingJournalService().adjustAccountingJournalDividend(transaction.getParentTransaction().getId(), adjustmentAmount, journalData.getEventNote());
		}
		else {
			throw new ValidationException("Unable to handle event!  Unsupported journal with name: " + journalType.getName());
		}
	}


	private BigDecimal getAdjustingJournalAmount(ReconciliationEventObjectJournalData journalData, InvestmentAccount holdingAccount, InvestmentSecurity investmentSecurity) {
		BigDecimal adjustmentAmount = journalData.getAmount();
		if (!CompareUtils.isEqual(holdingAccount.getBaseCurrency(), investmentSecurity.getInstrument().getTradingCurrency())) {
			BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(holdingAccount.toHoldingAccount(), investmentSecurity.getInstrument().getTradingCurrency().getSymbol(), holdingAccount.getBaseCurrency().getSymbol(), journalData.getTransactionDate()).flexibleLookup());
			adjustmentAmount = adjustmentAmount.multiply(fxRate);
		}
		return adjustmentAmount;
	}


	private AccountingTransaction getAdjustingJournalTransaction(ReconciliationEventObjectJournalData journalData, AccountingJournalType journalType, InvestmentAccount holdingAccount, InvestmentSecurity investmentSecurity) {
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setInvestmentSecurityId(investmentSecurity.getId());
		searchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
		if (AccountingJournalType.ADJUSTING_JOURNAL_COMMISSION.equals(journalType.getName())) {
			searchForm.setAccountingAccountName(AccountingAccount.EXPENSE_COMMISSION);
		}
		else if (AccountingJournalType.ADJUSTING_JOURNAL_REALIZED.equals(journalType.getName())) {
			searchForm.setAccountingAccountName(AccountingAccount.REVENUE_REALIZED);
		}
		else {
			throw new ValidationException("Unable to handle event!  Unsupported journal with name " + journalType.getName());
		}

		searchForm.setJournalTypeName(AccountingJournalType.TRADE_JOURNAL);
		if (DateUtils.isDateBefore(journalData.getEventDate(), journalData.getSettlementDate(), false)) {
			searchForm.setTransactionDate(journalData.getTransactionDate());
		}
		else {
			searchForm.setSettlementDate(journalData.getSettlementDate());
		}
		List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);
		ValidationUtils.assertNotEmpty(transactionList, "No transactions found for [" + investmentSecurity.getSymbol() + " on ["
				+ journalData.getTransactionDate() + "] in account [" + holdingAccount.getNumber() + "].");
		return CoreMathUtils.getBeanWithMaxProperty(transactionList, "localDebitCredit", MathUtils.isGreaterThan(journalData.getAmount(), BigDecimal.ZERO));
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AccountingBookingService<?> getAccountingBookingService() {
		return this.accountingBookingService;
	}


	public void setAccountingBookingService(AccountingBookingService<?> accountingBookingService) {
		this.accountingBookingService = accountingBookingService;
	}


	public AccountingAdjustingJournalService getAccountingAdjustingJournalService() {
		return this.accountingAdjustingJournalService;
	}


	public void setAccountingAdjustingJournalService(AccountingAdjustingJournalService accountingAdjustingJournalService) {
		this.accountingAdjustingJournalService = accountingAdjustingJournalService;
	}


	public AccountingJournalService getAccountingJournalService() {
		return this.accountingJournalService;
	}


	public void setAccountingJournalService(AccountingJournalService accountingJournalService) {
		this.accountingJournalService = accountingJournalService;
	}


	public AccountingSimpleJournalService getAccountingSimpleJournalService() {
		return this.accountingSimpleJournalService;
	}


	public void setAccountingSimpleJournalService(AccountingSimpleJournalService accountingSimpleJournalService) {
		this.accountingSimpleJournalService = accountingSimpleJournalService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
