package com.clifton.ims.reconciliation.jobs.investment;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.reconciliation.definition.ReconciliationDefinitionService;
import com.clifton.reconciliation.investment.ReconciliationInvestmentAccount;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentAccountSearchForm;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public class ReconciliationInvestmentAccountCopyJob implements Task {

	private InvestmentAccountService investmentAccountService;
	private ReconciliationDefinitionService reconciliationDefinitionService;
	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();
		Map<Integer, InvestmentAccount> imsInvestmentAccountMap = getIMSInvestmentAccountMap();
		Map<Integer, ReconciliationInvestmentAccount> reconciliationInvestmentAccountMap = getReconciliationInvestmentAccountMap();
		updateReconciliationAccounts(imsInvestmentAccountMap, reconciliationInvestmentAccountMap, status);
		status.setMessageWithErrors(status.getMessage(), 5);
		return status;
	}


	private Status updateReconciliationAccounts(Map<Integer, InvestmentAccount> sourceInvestmentAccountMap, Map<Integer, ReconciliationInvestmentAccount> reconciliationInvestmentAccountMap, Status status) {
		int skipCount = 0;
		int failCount = 0;
		int deletedCount = 0;
		int failedDeletionCount = 0;
		int successCount = 0;
		for (ReconciliationInvestmentAccount reconciliationInvestmentAccount : CollectionUtils.getIterable(reconciliationInvestmentAccountMap.values())) {
			if (!sourceInvestmentAccountMap.containsKey(reconciliationInvestmentAccount.getAccountIdentifier())) {
				try {
					getReconciliationInvestmentService().deleteReconciliationInvestmentAccount(reconciliationInvestmentAccount.getId());
					deletedCount++;
				}
				catch (Throwable e) {
					failedDeletionCount++;
					//Ignore - account has data references and can't be deleted.
				}
			}
		}
		for (InvestmentAccount sourceInvestmentAccount : CollectionUtils.getIterable(sourceInvestmentAccountMap.values())) {
			ReconciliationInvestmentAccount reconciliationInvestmentAccount = reconciliationInvestmentAccountMap.get(sourceInvestmentAccount.getId());
			if (reconciliationInvestmentAccount == null) {
				reconciliationInvestmentAccount = new ReconciliationInvestmentAccount();
			}
			try {
				//Only update if the source security update data is greater than or equal to the target.
				if (doUpdate(reconciliationInvestmentAccount, sourceInvestmentAccount)) {
					populateAccountValues(reconciliationInvestmentAccount, sourceInvestmentAccount);
					getReconciliationInvestmentService().saveReconciliationInvestmentAccount(reconciliationInvestmentAccount);
					successCount++;
				}
				else {
					skipCount++;
					status.addSkipped(reconciliationInvestmentAccount.getAccountName());
				}
			}
			catch (Throwable e) {
				failCount++;
				String message = "Error Saving Reconciliation Investment Account: " + reconciliationInvestmentAccount.getAccountName() + ": " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				status.addError(message);
			}
		}
		status.setMessage("ReconciliationInvestmentAccount: Successful: " + successCount + ", Deleted: " + deletedCount + " (" + failedDeletionCount + " Deletion(s) Failed), Skipped: " + skipCount + " Failed: " + failCount);
		return status;
	}


	private boolean doUpdate(ReconciliationInvestmentAccount reconciliationInvestmentAccount, InvestmentAccount sourceInvestmentAccount) {
		Date targetUpdateDate = reconciliationInvestmentAccount.getUpdateDate();
		if (targetUpdateDate == null || DateUtils.isDateAfterOrEqual(sourceInvestmentAccount.getUpdateDate(), targetUpdateDate)) {
			return true;
		}
		return false;
	}


	private ReconciliationInvestmentAccount populateAccountValues(ReconciliationInvestmentAccount reconciliationInvestmentAccount, InvestmentAccount sourceInvestmentAccount) {
		reconciliationInvestmentAccount.setAccountIdentifier(sourceInvestmentAccount.getId());
		reconciliationInvestmentAccount.setAccountName(sourceInvestmentAccount.getName());
		reconciliationInvestmentAccount.setAccountNumber(sourceInvestmentAccount.getNumber());
		reconciliationInvestmentAccount.setAccountNumber2(sourceInvestmentAccount.getNumber2());
		reconciliationInvestmentAccount.setIssuingCompany(sourceInvestmentAccount.getIssuingCompany().getName());
		reconciliationInvestmentAccount.setBaseCurrency(sourceInvestmentAccount.getBaseCurrency().getSymbol());
		reconciliationInvestmentAccount.setWorkflowState(sourceInvestmentAccount.getWorkflowState().getName());
		return reconciliationInvestmentAccount;
	}


	private Map<Integer, InvestmentAccount> getIMSInvestmentAccountMap() {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(false);
		List<InvestmentAccount> investmentAccountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		return BeanUtils.getBeanMap(investmentAccountList, InvestmentAccount::getId);
	}


	private Map<Integer, ReconciliationInvestmentAccount> getReconciliationInvestmentAccountMap() {
		List<ReconciliationInvestmentAccount> reconciliationInvestmentAccountList = getReconciliationInvestmentService().getReconciliationInvestmentAccountList(new ReconciliationInvestmentAccountSearchForm());
		return BeanUtils.getBeanMap(reconciliationInvestmentAccountList, ReconciliationInvestmentAccount::getAccountIdentifier);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public ReconciliationDefinitionService getReconciliationDefinitionService() {
		return this.reconciliationDefinitionService;
	}


	public void setReconciliationDefinitionService(ReconciliationDefinitionService reconciliationDefinitionService) {
		this.reconciliationDefinitionService = reconciliationDefinitionService;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
