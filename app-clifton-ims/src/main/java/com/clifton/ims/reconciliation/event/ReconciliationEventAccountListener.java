package com.clifton.ims.reconciliation.event;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.EventNotProcessedException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.reconciliation.event.jms.ReconciliationEvent;
import com.clifton.reconciliation.event.object.ReconciliationEventObjectCategories;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectAccountData;
import com.clifton.reconciliation.event.object.data.ReconciliationEventObjectData;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


@Component
public class ReconciliationEventAccountListener extends BaseEventListener<ReconciliationEvent> {

	private static final String TRADE_RESTRICTION_TYPE_NOT_RECONCILED = "Not Reconciled";

	//TODO - once trade restrictions are enhanced this will be updated to use holding accounts
	//TODO - For now this assumes that the client account is related with a purpose of 'Custodian'
	//TODO - inject this?  Or just wait until trade restrictions are updated and refactor?
	private boolean unlockByClient = true;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private TradeRestrictionService tradeRestrictionService;


	@Override
	public String getEventName() {
		return ReconciliationEventObjectCategories.ACCOUNT.name();
	}


	@Override
	public synchronized void onEvent(ReconciliationEvent event) {
		ReconciliationEventObjectData eventObjectData = event.getTarget();
		AssertUtils.assertNotNull(eventObjectData, "No data exists for [" + event.getTarget() + "] for event with name [" + event.getEventName() + "].");
		if (eventObjectData instanceof ReconciliationEventObjectAccountData) {
			ReconciliationEventObjectAccountData accountData = (ReconciliationEventObjectAccountData) eventObjectData;

			Date activeOnDate = accountData.getEventDate();
			List<InvestmentAccount> clientAccountList = null;
			InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccountByNumber(accountData.getHoldingAccountNumber());
			AssertUtils.assertNotNull(holdingAccount, "Unable to locate holding account with number: " + accountData.getHoldingAccountNumber());
			if (isUnlockByClient() && !(InvestmentAccountType.CLIENT.equals(holdingAccount.getType().getName()))) {
				clientAccountList = getClientAccountList(holdingAccount, activeOnDate);
				AssertUtils.assertNotNull(clientAccountList, "Unable to locate client account(s) for holding account with number: " + accountData.getHoldingAccountNumber());
			}
			if (clientAccountList != null) {
				TradeRestrictionSearchForm searchForm = new TradeRestrictionSearchForm();
				searchForm.setActiveOnDate(activeOnDate);
				searchForm.setClientAccountIds(clientAccountList.stream().map(InvestmentAccount::getId).toArray(Integer[]::new));
				searchForm.setRestrictionTypeName(TRADE_RESTRICTION_TYPE_NOT_RECONCILED);
				for (TradeRestriction tradeRestriction : CollectionUtils.getIterable(getTradeRestrictionService().getTradeRestrictionList(searchForm))) {
					getTradeRestrictionService().deactivateTradeRestriction(tradeRestriction.getId(), accountData.getEventNote());
				}
			}
		}
		else {
			throw new EventNotProcessedException("Unable to handled event!  Unsupported object of type: " + eventObjectData.getClass());
		}
	}


	private List<InvestmentAccount> getClientAccountList(InvestmentAccount investmentAccount, Date activeOnDate) {
		return getInvestmentAccountRelationshipService().getInvestmentAccountRelatedListForPurpose(investmentAccount.getId(), null, InvestmentAccountRelationshipPurpose.TRADING_OPTIONS_PURPOSE_NAME, activeOnDate);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isUnlockByClient() {
		return this.unlockByClient;
	}


	public void setUnlockByClient(boolean unlockByClient) {
		this.unlockByClient = unlockByClient;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public TradeRestrictionService getTradeRestrictionService() {
		return this.tradeRestrictionService;
	}


	public void setTradeRestrictionService(TradeRestrictionService tradeRestrictionService) {
		this.tradeRestrictionService = tradeRestrictionService;
	}
}
