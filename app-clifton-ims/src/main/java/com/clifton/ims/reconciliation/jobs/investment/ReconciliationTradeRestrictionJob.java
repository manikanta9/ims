package com.clifton.ims.reconciliation.jobs.investment;

import com.clifton.accounting.gl.balance.search.AccountingBalanceSearchForm;
import com.clifton.accounting.gl.valuation.AccountingBalanceValue;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.restriction.TradeRestriction;
import com.clifton.trade.restriction.TradeRestrictionService;
import com.clifton.trade.restriction.search.TradeRestrictionSearchForm;
import com.clifton.trade.restriction.type.TradeRestrictionType;
import com.clifton.trade.restriction.type.TradeRestrictionTypeService;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * The <code>ReconciliationTradeRestrictionJob</code> is used to automatically create TradeRestrictions for account and/or positions
 * for the account and/or holdings in the specified account groups.  Restrictions are created for the day the job is run, and do not
 * have an end date applied.  Typically these restrictions are temporary and are removed by external processes or manually where allowed.
 *
 * @author stevenf
 */
public class ReconciliationTradeRestrictionJob implements Task {

	private boolean lockAccountsAndPositions;

	private Short tradeRestrictionTypeId;

	private Short[] accountingAccountIds;
	private Short[] accountingAccountTypeIds;
	private Integer[] investmentAccountGroupIds;

	private String restrictionNote;

	private AccountingValuationService accountingValuationService;
	private InvestmentAccountService investmentAccountService;
	private TradeRestrictionService tradeRestrictionService;
	private TradeRestrictionTypeService tradeRestrictionTypeService;

	private static final String CREATION_COUNT = "creationCount";


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofTitle("Trade Restriction Job");
		TradeRestrictionType tradeRestrictionType = getTradeRestrictionTypeService().getTradeRestrictionType(getTradeRestrictionTypeId());
		ValidationUtils.assertNotNull(tradeRestrictionType, "Trade restriction type may not be null!");

		Set<InvestmentAccount> clientAccountList = new HashSet<>();
		for (Integer investmentAccountGroupId : getInvestmentAccountGroupIds()) {
			InvestmentAccountSearchForm investmentAccountSearchForm = new InvestmentAccountSearchForm();
			investmentAccountSearchForm.setInvestmentAccountGroupId(investmentAccountGroupId);
			investmentAccountSearchForm.setWorkflowStateNameEquals("Active");
			investmentAccountSearchForm.setOurAccount(true);
			clientAccountList.addAll(getInvestmentAccountService().getInvestmentAccountList(investmentAccountSearchForm));
		}
		for (InvestmentAccount clientAccount : CollectionUtils.getIterable(clientAccountList)) {
			if (isLockAccountsAndPositions()) {
				AccountingBalanceSearchForm accountingBalanceSearchForm = new AccountingBalanceSearchForm();
				accountingBalanceSearchForm.setTransactionDate(new Date());
				accountingBalanceSearchForm.setClientInvestmentAccountId(clientAccount.getId());
				accountingBalanceSearchForm.setAccountingAccountIds(getAccountingAccountIds());
				accountingBalanceSearchForm.setAccountingAccountTypeIds(getAccountingAccountTypeIds());
				for (AccountingBalanceValue accountingBalanceValue : CollectionUtils.getIterable(getAccountingValuationService().getAccountingBalanceValueList(accountingBalanceSearchForm))) {
					createTradeRestriction(tradeRestrictionType, clientAccount, accountingBalanceValue.getInvestmentSecurity(), status);
				}
			}
			else {
				createTradeRestriction(tradeRestrictionType, clientAccount, null, status);
			}
		}
		status.setMessage("Success: Created: " + status.getStatusCount(CREATION_COUNT) + " Trade Restrictions; " + status.getErrorCount() + " Error(s)");
		return status;
	}


	private void createTradeRestriction(TradeRestrictionType tradeRestrictionType, InvestmentAccount account, InvestmentSecurity security, Status status) {
		try {
			//We need to search for an existing restriction of this type for this account/security
			TradeRestrictionSearchForm searchForm = new TradeRestrictionSearchForm();
			searchForm.setRestrictionTypeId(tradeRestrictionType.getId());
			searchForm.setClientAccountId(account.getId());
			searchForm.setSecurityId(security != null ? security.getId() : null);
			searchForm.setActiveOnDate(new Date());
			TradeRestriction tradeRestriction = CollectionUtils.getOnlyElement(getTradeRestrictionService().getTradeRestrictionList(searchForm));
			if (tradeRestriction != null) {
				//If found we need to deactivate it so we can create a new one for today
				tradeRestriction.setEndDate(new Date());
				getTradeRestrictionService().deactivateTradeRestriction(tradeRestriction.getId(), "Automatically disabling restriction and creating new restriction for current day.");
			}
			//Create the new restriction
			tradeRestriction = new TradeRestriction();
			tradeRestriction.setRestrictionType(tradeRestrictionType);
			tradeRestriction.setClientInvestmentAccount(account);
			tradeRestriction.setInvestmentSecurity(security);
			//Ensure the new startDate does not match the endDate of the old restriction by adding a second.
			tradeRestriction.setStartDate(DateUtils.addSeconds(new Date(), 1));
			if (tradeRestrictionType.isRestrictionNoteRequired()) {
				tradeRestriction.setNote(tradeRestrictionType.getDefaultRestrictionNote());
			}
			//Batch job restriction note takes precedence over the default.
			if (!StringUtils.isEmpty(getRestrictionNote())) {
				tradeRestriction.setNote(getRestrictionNote());
			}
			getTradeRestrictionService().saveTradeRestriction(tradeRestriction);
			status.incrementStatusCount(CREATION_COUNT);
		}
		catch (Exception e) {
			StringBuilder errorMessage = new StringBuilder("Failed to create trade restriction for account [" + account.getLabel() + "]");
			if (isLockAccountsAndPositions() && security != null) {
				errorMessage.append(" and security [").append(security.getLabel()).append("]");
			}
			errorMessage.append(": ").append(e.getMessage());
			status.addError(errorMessage.toString());
			LogUtils.error(this.getClass(), errorMessage.toString(), e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLockAccountsAndPositions() {
		return this.lockAccountsAndPositions;
	}


	public void setLockAccountsAndPositions(boolean lockAccountsAndPositions) {
		this.lockAccountsAndPositions = lockAccountsAndPositions;
	}


	public Short[] getAccountingAccountIds() {
		return this.accountingAccountIds;
	}


	public void setAccountingAccountIds(Short[] accountingAccountIds) {
		this.accountingAccountIds = accountingAccountIds;
	}


	public Short[] getAccountingAccountTypeIds() {
		return this.accountingAccountTypeIds;
	}


	public void setAccountingAccountTypeIds(Short[] accountingAccountTypeIds) {
		this.accountingAccountTypeIds = accountingAccountTypeIds;
	}


	public Short getTradeRestrictionTypeId() {
		return this.tradeRestrictionTypeId;
	}


	public void setTradeRestrictionTypeId(Short tradeRestrictionTypeId) {
		this.tradeRestrictionTypeId = tradeRestrictionTypeId;
	}


	public Integer[] getInvestmentAccountGroupIds() {
		return this.investmentAccountGroupIds;
	}


	public void setInvestmentAccountGroupIds(Integer[] investmentAccountGroupIds) {
		this.investmentAccountGroupIds = investmentAccountGroupIds;
	}


	public String getRestrictionNote() {
		return this.restrictionNote;
	}


	public void setRestrictionNote(String restrictionNote) {
		this.restrictionNote = restrictionNote;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public TradeRestrictionService getTradeRestrictionService() {
		return this.tradeRestrictionService;
	}


	public void setTradeRestrictionService(TradeRestrictionService tradeRestrictionService) {
		this.tradeRestrictionService = tradeRestrictionService;
	}


	public TradeRestrictionTypeService getTradeRestrictionTypeService() {
		return this.tradeRestrictionTypeService;
	}


	public void setTradeRestrictionTypeService(TradeRestrictionTypeService tradeRestrictionTypeService) {
		this.tradeRestrictionTypeService = tradeRestrictionTypeService;
	}
}
