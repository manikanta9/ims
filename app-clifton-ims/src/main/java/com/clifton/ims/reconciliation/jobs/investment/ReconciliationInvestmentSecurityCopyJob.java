package com.clifton.ims.reconciliation.jobs.investment;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.reconciliation.investment.ReconciliationInvestmentSecurity;
import com.clifton.reconciliation.investment.ReconciliationInvestmentService;
import com.clifton.reconciliation.investment.search.ReconciliationInvestmentSecuritySearchForm;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public class ReconciliationInvestmentSecurityCopyJob implements Task {

	private boolean includeActiveOnly;

	private InvestmentInstrumentService investmentInstrumentService;
	private ReconciliationInvestmentService reconciliationInvestmentService;


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();
		Map<Integer, InvestmentSecurity> imsInvestmentSecurityMap = getIMSInvestmentSecurityMap();
		Map<Integer, ReconciliationInvestmentSecurity> reconciliationInvestmentSecurityMap = getReconciliationInvestmentSecurityMap();
		updateReconciliationSecurities(imsInvestmentSecurityMap, reconciliationInvestmentSecurityMap, status);
		status.setMessageWithErrors(status.getMessage(), 5);
		return status;
	}


	private Status updateReconciliationSecurities(Map<Integer, InvestmentSecurity> sourceInvestmentSecurityMap, Map<Integer, ReconciliationInvestmentSecurity> reconciliationInvestmentSecurityMap, Status status) {
		int skipCount = 0;
		int failCount = 0;
		int deletedCount = 0;
		int failedDeletionCount = 0;
		int successCount = 0;
		for (ReconciliationInvestmentSecurity reconciliationInvestmentSecurity : CollectionUtils.getIterable(reconciliationInvestmentSecurityMap.values())) {
			if (!sourceInvestmentSecurityMap.containsKey(reconciliationInvestmentSecurity.getSecurityIdentifier())) {
				try {
					getReconciliationInvestmentService().deleteReconciliationInvestmentSecurity(reconciliationInvestmentSecurity.getId());
					deletedCount++;
				}
				catch (Throwable e) {
					failedDeletionCount++;
					//Ignore - security has data references and can't be deleted.
				}
			}
		}
		for (InvestmentSecurity sourceInvestmentSecurity : CollectionUtils.getIterable(sourceInvestmentSecurityMap.values())) {
			ReconciliationInvestmentSecurity reconciliationInvestmentSecurity = reconciliationInvestmentSecurityMap.get(sourceInvestmentSecurity.getId());
			if (reconciliationInvestmentSecurity == null) {
				reconciliationInvestmentSecurity = new ReconciliationInvestmentSecurity();
			}
			try {
				//Only update if the source security update data is greater than or equal to the target.
				if (doUpdate(reconciliationInvestmentSecurity, sourceInvestmentSecurity)) {
					populateSecurityValues(reconciliationInvestmentSecurity, sourceInvestmentSecurity);
					getReconciliationInvestmentService().saveReconciliationInvestmentSecurity(reconciliationInvestmentSecurity);
					successCount++;
				}
				else {
					skipCount++;
					status.addSkipped(reconciliationInvestmentSecurity.getSecuritySymbol());
				}
			}
			catch (Throwable e) {
				failCount++;
				String message = "Error Saving Reconciliation Investment Account: " + reconciliationInvestmentSecurity.getSecuritySymbol() + ": " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				status.addError(message);
			}
		}
		status.setMessage("ReconciliationInvestmentAccount: Successful: " + successCount + ", Deleted: " + deletedCount + " (" + failedDeletionCount + " Deletion(s) Failed), Skipped: " + skipCount + " Failed: " + failCount);
		return status;
	}


	private boolean doUpdate(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity, InvestmentSecurity sourceInvestmentSecurity) {
		Date targetUpdateDate = reconciliationInvestmentSecurity.getUpdateDate();
		if (targetUpdateDate == null || DateUtils.isDateAfterOrEqual(sourceInvestmentSecurity.getUpdateDate(), targetUpdateDate)) {
			return true;
		}

		//If the main object is up to date, we still need to check child objects as they could have been updated independent of the parent.
		InvestmentInstrument instrument = sourceInvestmentSecurity.getInstrument();
		if (DateUtils.isDateAfterOrEqual(instrument.getUpdateDate(), targetUpdateDate)) {
			return true;
		}
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		if (DateUtils.isDateAfterOrEqual(hierarchy.getUpdateDate(), targetUpdateDate)) {
			return true;
		}
		InvestmentType investmentType = hierarchy.getInvestmentType();
		if (investmentType != null) {
			if (DateUtils.isDateAfterOrEqual(investmentType.getUpdateDate(), targetUpdateDate)) {
				return true;
			}
		}
		InvestmentTypeSubType investmentTypeSubType = hierarchy.getInvestmentTypeSubType();
		if (investmentTypeSubType != null) {
			if (DateUtils.isDateAfterOrEqual(investmentTypeSubType.getUpdateDate(), targetUpdateDate)) {
				return true;
			}
		}
		InvestmentTypeSubType2 investmentTypeSubType2 = hierarchy.getInvestmentTypeSubType2();
		if (investmentTypeSubType2 != null) {
			if (DateUtils.isDateAfterOrEqual(investmentTypeSubType2.getUpdateDate(), targetUpdateDate)) {
				return true;
			}
		}

		return false;
	}


	private ReconciliationInvestmentSecurity populateSecurityValues(ReconciliationInvestmentSecurity reconciliationInvestmentSecurity, InvestmentSecurity sourceInvestmentSecurity) {
		reconciliationInvestmentSecurity.setSecurityIdentifier(sourceInvestmentSecurity.getId());
		reconciliationInvestmentSecurity.setSecurityName(sourceInvestmentSecurity.getName());
		reconciliationInvestmentSecurity.setSecuritySymbol(sourceInvestmentSecurity.getSymbol());
		reconciliationInvestmentSecurity.setSecurityCusip(sourceInvestmentSecurity.getCusip());
		reconciliationInvestmentSecurity.setSecurityIsin(sourceInvestmentSecurity.getIsin());
		reconciliationInvestmentSecurity.setSecuritySedol(sourceInvestmentSecurity.getSedol());
		reconciliationInvestmentSecurity.setSecurityOccSymbol(sourceInvestmentSecurity.getOccSymbol());
		reconciliationInvestmentSecurity.setEndDate(sourceInvestmentSecurity.getEndDate());
		if (sourceInvestmentSecurity.getInstrument().getTradingCurrency() != null) {
			reconciliationInvestmentSecurity.setCurrencyDenomination(sourceInvestmentSecurity.getInstrument().getTradingCurrency().getSymbol());
		}
		InvestmentType investmentType = sourceInvestmentSecurity.getInstrument().getHierarchy().getInvestmentType();
		if (investmentType != null) {
			reconciliationInvestmentSecurity.setSecurityTypeName(investmentType.getName());
		}
		InvestmentTypeSubType investmentTypeSubType = sourceInvestmentSecurity.getInstrument().getHierarchy().getInvestmentTypeSubType();
		if (investmentTypeSubType != null) {
			reconciliationInvestmentSecurity.setSecurityTypeSubTypeName(investmentTypeSubType.getName());
		}
		InvestmentTypeSubType2 investmentTypeSubType2 = sourceInvestmentSecurity.getInstrument().getHierarchy().getInvestmentTypeSubType2();
		if (investmentTypeSubType2 != null) {
			reconciliationInvestmentSecurity.setSecurityTypeSubType2Name(investmentTypeSubType2.getName());
		}

		return reconciliationInvestmentSecurity;
	}


	private Map<Integer, InvestmentSecurity> getIMSInvestmentSecurityMap() {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		if (isIncludeActiveOnly()) {
			searchForm.setActive(true);
		}
		List<InvestmentSecurity> investmentSecurityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		return BeanUtils.getBeanMap(investmentSecurityList, InvestmentSecurity::getId);
	}


	private Map<Integer, ReconciliationInvestmentSecurity> getReconciliationInvestmentSecurityMap() {
		List<ReconciliationInvestmentSecurity> reconciliationInvestmentSecurityList = getReconciliationInvestmentService().getReconciliationInvestmentSecurityList(new ReconciliationInvestmentSecuritySearchForm());
		return BeanUtils.getBeanMap(reconciliationInvestmentSecurityList, ReconciliationInvestmentSecurity::getSecurityIdentifier);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isIncludeActiveOnly() {
		return this.includeActiveOnly;
	}


	public void setIncludeActiveOnly(boolean includeActiveOnly) {
		this.includeActiveOnly = includeActiveOnly;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public ReconciliationInvestmentService getReconciliationInvestmentService() {
		return this.reconciliationInvestmentService;
	}


	public void setReconciliationInvestmentService(ReconciliationInvestmentService reconciliationInvestmentService) {
		this.reconciliationInvestmentService = reconciliationInvestmentService;
	}
}
