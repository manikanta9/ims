package com.clifton.ims.billing.invoice.rule;

import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.rule.BaseBillingInvoiceMissingSourceRuleEvaluator;
import com.clifton.billing.invoice.rule.BillingInvoiceRuleEvaluatorContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.product.overlay.ProductOverlayService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceAccountMissingMainPortfolioRunRuleEvaluator</code> generates violations for any account that utilizes a billing basis valuation for source table {@link com.clifton.product.overlay.ProductOverlayAssetClassReplication}
 * but doesn't have a main run on any weekday during the billing period.
 *
 * @author manderson
 */
public class BillingInvoiceAccountMissingMainPortfolioRunRuleEvaluator extends BaseBillingInvoiceMissingSourceRuleEvaluator {

	private AccountingPositionHandler accountingPositionHandler;

	private InvestmentAccountService investmentAccountService;

	private ProductOverlayService productOverlayService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Optionally skip the violation if the account didn't have any positions on on the given snapshot date
	 */
	private boolean skipIfNoPositionsOn;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSourceTableName() {
		return "ProductOverlayAssetClassReplication";
	}


	@Override
	public boolean isUseZeroValueAsMissingSource() {
		return false;
	}


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		List<BillingBasisSnapshot> snapshotList = getBillingBasisSnapshotListMissingSource(invoice, context);

		if (!CollectionUtils.isEmpty(snapshotList)) {
			// Get Map of Accounts to Dates Missing
			MultiValueHashMap<Integer, Date> accountMissingDateMap = new MultiValueHashMap<>(false);
			for (BillingBasisSnapshot bbs : snapshotList) {
				Integer accountId = bbs.getBillingDefinitionInvestmentAccount().getReferenceTwo().getId();
				if (accountMissingDateMap.get(accountId) == null || !accountMissingDateMap.get(accountId).contains(bbs.getSnapshotDate())) {
					accountMissingDateMap.put(accountId, bbs.getSnapshotDate());
				}
			}

			// Check for missing runs for each account on the date
			for (Map.Entry<Integer, Collection<Date>> accountMissingEntry : accountMissingDateMap.entrySet()) {
				List<Date> dateMissingList = new ArrayList<>();
				for (Date date : CollectionUtils.getIterable(accountMissingEntry.getValue())) {
					if (!getProductOverlayService().isProductOverlayMainRunExistsForAccount(accountMissingEntry.getKey(), date)) {
						// Last check - Optionally only include in warning if they had positions on
						if (!isSkipIfNoPositionsOn() || isPositionsOnForClientAccountAndDate(accountMissingEntry.getKey(), date, context)) {
							dateMissingList.add(date);
						}
					}
				}
				addRuleViolationToList(violationList, entityConfig, invoice, accountMissingEntry.getKey(), dateMissingList);
			}
		}
		return violationList;
	}


	protected boolean isPositionsOnForClientAccountAndDate(int investmentAccountId, Date date, BillingInvoiceRuleEvaluatorContext context) {
		return context.isPositionsOnForAccountAndDate(investmentAccountId, date, () -> getAccountingPositionHandler().isClientAccountPositionsOnForDate(investmentAccountId, date));
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////            Getter and Setter Methods              ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public boolean isSkipIfNoPositionsOn() {
		return this.skipIfNoPositionsOn;
	}


	public void setSkipIfNoPositionsOn(boolean skipIfNoPositionsOn) {
		this.skipIfNoPositionsOn = skipIfNoPositionsOn;
	}
}
