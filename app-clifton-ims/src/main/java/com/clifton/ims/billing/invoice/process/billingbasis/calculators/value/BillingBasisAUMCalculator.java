package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.accounting.period.search.AccountingPeriodClosingSearchForm;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisAUMCalculator</code> calculates the billing basis using the final records Period AUM values from the Account's {@link AccountingPeriodClosing}
 *
 * @author manderson
 */
public class BillingBasisAUMCalculator extends BaseBillingBasisValueCalculator {

	private AccountingPeriodService accountingPeriodService;

	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This can be configurable, although system bean properties will not be created because
	 * AUM values in IMS are always reported/saved in USD
	 */
	private String aumCurrencyDenomination = "USD";


	/**
	 * Customized validation - since AUM values are saved ONLY on month end we can only use this calculator with
	 * Period Average Projected (since doesn't plug in zeros - used only for projected revenue)
	 * Period End
	 * Period Month End Average
	 */
	private List<Short> allowedBillingBasisCalculationTypeIdList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isValidValuationDate(Date date) {
		return DateUtils.isLastDayOfMonth(date);
	}


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		if (moveForward) {
			if (DateUtils.isLastDayOfMonth(date)) {
				return DateUtils.getLastDayOfMonth(DateUtils.addDays(date, 1));
			}
			return DateUtils.getLastDayOfMonth(date);
		}
		return DateUtils.getLastDayOfPreviousMonth(date);
	}


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);
		validateBillingAccount(billingAccount);

		List<BillingBasisSnapshot> list = new ArrayList<>();
		InvestmentSecurity aumCurrency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(getAumCurrencyDenomination(), true);

		List<AccountingPeriodClosing> accountingPeriodClosingList = getAccountingPeriodClosingList(config, billingAccount, startDate, endDate);
		for (AccountingPeriodClosing closing : CollectionUtils.getIterable(accountingPeriodClosingList)) {
			list.add(createBillingBasisSnapshot(invoice, billingAccount, closing, aumCurrency));
		}
		return list;
	}


	private List<AccountingPeriodClosing> getAccountingPeriodClosingList(BillingInvoiceProcessConfig config, BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		List<AccountingPeriodClosing> list = config.getAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), AccountingPeriodClosing.class, startDate, endDate);
		if (list == null) {
			AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
			searchForm.setInvestmentAccountId(billingAccount.getReferenceTwo().getId());
			// AUM is for Period End - so Use End Date as Date Field to Compare to
			searchForm.addSearchRestriction(new SearchRestriction("accountingPeriodEndDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
			searchForm.addSearchRestriction(new SearchRestriction("accountingPeriodEndDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));

			list = getAccountingPeriodService().getAccountingPeriodClosingList(searchForm);
			if (list == null) {
				list = new ArrayList<>();
			}
			config.setAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), AccountingPeriodClosing.class, startDate, endDate, list);
		}
		return list;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void validateBillingAccount(BillingDefinitionInvestmentAccount billingAccount) {
		ValidationUtils.assertTrue(getAllowedBillingBasisCalculationTypeIdList().contains(billingAccount.getBillingBasisCalculationType().getId()), billingAccount.getBillingBasisValuationType().getName() + " cannot be used with calculation type [" + billingAccount.getBillingBasisCalculationType().getName() + "]");
	}


	private BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, AccountingPeriodClosing closing, InvestmentSecurity aumCurrency) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSourceFkFieldId(closing.getId());
		bbs.setSecurity(closing.getInvestmentAccount().getBaseCurrency());
		bbs.setSnapshotDate(closing.getAccountingPeriod().getEndDate());
		bbs.setSnapshotValue(calculateBillingBasisValue(closing, aumCurrency));
		return bbs;
	}


	/**
	 * For billing use final values - Usually the same but better to show missing value then preliminary value that could be incorrect
	 * If the account's base CCY is not the same as the aumCurrency, then we need to convert it to the account base CCY
	 */
	private BigDecimal calculateBillingBasisValue(AccountingPeriodClosing closing, InvestmentSecurity aumCurrency) {
		BigDecimal value = closing.getPeriodEndAUM();
		if (value == null) {
			value = BigDecimal.ZERO;
		}
		if (MathUtils.isNullOrZero(value) || aumCurrency.equals(closing.getInvestmentAccount().getBaseCurrency())) {
			return value;
		}
		BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forClientAccount(closing.getInvestmentAccount().toClientAccount(), aumCurrency.getSymbol(), closing.getInvestmentAccount().getBaseCurrency().getSymbol(), closing.getAccountingPeriod().getEndDate()));
		return MathUtils.multiply(value, fxRate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public String getAumCurrencyDenomination() {
		return this.aumCurrencyDenomination;
	}


	public void setAumCurrencyDenomination(String aumCurrencyDenomination) {
		this.aumCurrencyDenomination = aumCurrencyDenomination;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public List<Short> getAllowedBillingBasisCalculationTypeIdList() {
		return this.allowedBillingBasisCalculationTypeIdList;
	}


	public void setAllowedBillingBasisCalculationTypeIdList(List<Short> allowedBillingBasisCalculationTypeIdList) {
		this.allowedBillingBasisCalculationTypeIdList = allowedBillingBasisCalculationTypeIdList;
	}
}
