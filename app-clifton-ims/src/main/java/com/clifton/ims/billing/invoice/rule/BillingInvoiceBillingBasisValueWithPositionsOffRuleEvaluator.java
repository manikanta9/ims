package com.clifton.ims.billing.invoice.rule;

import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.billingbasis.BillingBasisService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.billingbasis.search.BillingBasisSnapshotSearchForm;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.rule.BaseBillingInvoiceRuleEvaluator;
import com.clifton.billing.invoice.rule.BillingInvoiceRuleEvaluatorContext;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.system.schema.SystemTable;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceBillingBasisValueWithPositionsOffRuleEvaluator</code> class is used to create rule violations
 * for BillingBasis values (includes only where specific valuation types are specified) have a value, but the account has positions off on that date (determined from Investment Calendar)
 *
 * @author manderson
 */
public class BillingInvoiceBillingBasisValueWithPositionsOffRuleEvaluator extends BaseBillingInvoiceRuleEvaluator {

	private AccountingPositionHandler accountingPositionHandler;

	private BillingBasisService billingBasisService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If blank, will evaluate against all
	 */
	private List<Short> billingBasisValuationTypeList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		// Key is Account ID, Value = True if positions are on for the entire billing basis period, else False
		Map<Integer, Boolean> accountPositionsOnOffMap = new HashMap<>();
		for (BillingBasisSnapshot snapshot : CollectionUtils.getIterable(getBillingBasisSnapshotList(invoice))) {
			int investmentAccountId = snapshot.getBillingDefinitionInvestmentAccount().getReferenceTwo().getId();
			if (!accountPositionsOnOffMap.containsKey(investmentAccountId)) {
				accountPositionsOnOffMap.put(investmentAccountId, getAccountingPositionHandler().isClientAccountPositionsOnForDateRange(investmentAccountId, invoice.getBillingBasisStartDate(), invoice.getBillingBasisEndDate()));
			}
			// Don't bother checking each day if positions are on the whole period
			if (!accountPositionsOnOffMap.get(investmentAccountId)) {
				if (!isPositionsOnForClientAccountAndDate(investmentAccountId, snapshot.getSnapshotDate(), context)) {
					Map<String, Object> templateMap = new HashMap<>();
					templateMap.put("snapshot", snapshot);
					// Try to Use Source from Snapshot
					SystemTable sourceTable = snapshot.getBillingDefinitionInvestmentAccount().getBillingBasisValuationType().getSourceTable();
					if (sourceTable != null && snapshot.getSourceFkFieldId() != null) {
						violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, invoice.getId(), snapshot.getSourceFkFieldId(), sourceTable, templateMap));
					}
					// Otherwise Just Use the Snapshot
					else {
						violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, invoice.getId(), snapshot.getId(), null, templateMap));
					}
				}
			}
		}
		return violationList;
	}


	protected List<BillingBasisSnapshot> getBillingBasisSnapshotList(BillingInvoice invoice) {
		BillingBasisSnapshotSearchForm searchForm = new BillingBasisSnapshotSearchForm();
		searchForm.setBillingInvoiceId(invoice.getId());
		if (!CollectionUtils.isEmpty(getBillingBasisValuationTypeList())) {
			searchForm.setValuationTypeIds(CollectionUtils.toArray(getBillingBasisValuationTypeList(), Short.class));
		}
		searchForm.addSearchRestriction(new SearchRestriction("snapshotValue", ComparisonConditions.NOT_EQUALS, BigDecimal.ZERO));
		return getBillingBasisService().getBillingBasisSnapshotList(searchForm);
	}


	protected boolean isPositionsOnForClientAccountAndDate(int investmentAccountId, Date date, BillingInvoiceRuleEvaluatorContext context) {
		return context.isPositionsOnForAccountAndDate(investmentAccountId, date, () -> getAccountingPositionHandler().isClientAccountPositionsOnForDate(investmentAccountId, date));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public BillingBasisService getBillingBasisService() {
		return this.billingBasisService;
	}


	public void setBillingBasisService(BillingBasisService billingBasisService) {
		this.billingBasisService = billingBasisService;
	}


	public List<Short> getBillingBasisValuationTypeList() {
		return this.billingBasisValuationTypeList;
	}


	public void setBillingBasisValuationTypeList(List<Short> billingBasisValuationTypeList) {
		this.billingBasisValuationTypeList = billingBasisValuationTypeList;
	}
}
