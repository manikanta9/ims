package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.dw.analytics.AccountingPositionSnapshot;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.product.util.ProductUtilService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisNotionalCalculator</code> extends the {@link BaseBillingBasisAccountingPositionSnapshotCalculator} and calculates each {@link BillingBasisSnapshot} with the notional value of the position
 */
public class BillingBasisNotionalCalculator extends BaseBillingBasisAccountingPositionSnapshotCalculator {

	private InvestmentCalculator investmentCalculator;
	private ProductUtilService productUtilService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, will lookup dirty price for the security.  Cannot be used with useStrikePrice or useUnderlyingPrice options
	 * Will also NOT use Notional Price Calculator for the security, and will use default calculation of Quantity * Dirty Price * Price Multiplier
	 */
	private boolean useDirtyPrice;

	/**
	 * If true, will lookup strike price for the security.  If not there, will use
	 * underlying (if selected below) else security price
	 */
	private boolean useStrikePrice;

	/**
	 * If true, will use the underlying security price, else security price
	 */
	private boolean useUnderlyingSecurityPrice;


	/**
	 * In some cases we can hold different lots for long and short the same security.
	 * If true, this will first merge the list of AccountingPositionSnapshot records into 1 record - summing quantity and notional together
	 */
	private boolean useNetPerSecurity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<AccountingPositionSnapshot> getAccountingPositionSnapshotList(BillingInvoiceProcessConfig config, BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		List<AccountingPositionSnapshot> snapshotList = super.getAccountingPositionSnapshotList(config, billingAccount, startDate, endDate);

		if (!CollectionUtils.isEmpty(snapshotList) && isUseNetPerSecurity()) {
			List<AccountingPositionSnapshot> filteredList = new ArrayList<>();
			Map<String, List<AccountingPositionSnapshot>> securityDateSnapshotListMap = BeanUtils.getBeansMap(snapshotList, accountingPositionSnapshot -> accountingPositionSnapshot.getInvestmentSecurityId() + "_" + DateUtils.fromDateShort(accountingPositionSnapshot.getSnapshotDate()));
			// For each grouping - keep only one snapshot record - and set the quantity and notional to the sum of all in the list
			for (Map.Entry<String, List<AccountingPositionSnapshot>> stringListEntry : securityDateSnapshotListMap.entrySet()) {
				List<AccountingPositionSnapshot> securityDateSnapshotList = stringListEntry.getValue();
				if (!CollectionUtils.isEmpty(securityDateSnapshotList)) {
					AccountingPositionSnapshot snapshot = securityDateSnapshotList.get(0);
					if (securityDateSnapshotList.size() > 1) {
						snapshot.setQuantity(CoreMathUtils.sumProperty(securityDateSnapshotList, AccountingPositionSnapshot::getQuantity));
						snapshot.setBaseNotional(CoreMathUtils.sumProperty(securityDateSnapshotList, AccountingPositionSnapshot::getBaseNotional));
					}
					filteredList.add(snapshot);
				}
			}
			snapshotList = filteredList;
		}
		return snapshotList;
	}


	@Override
	public BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, AccountingPositionSnapshot snapshot) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSecurity(snapshot.getInvestmentSecurity());
		bbs.setQuantity(snapshot.getQuantity());
		bbs.setSnapshotDate(snapshot.getSnapshotDate());

		if (isUseDirtyPrice() || isUseStrikePrice() || isUseUnderlyingSecurityPrice()) {
			BigDecimal price = null;

			if (isUseDirtyPrice()) {
				price = getInvestmentCalculator().calculateDirtyPrice(snapshot.getInvestmentSecurity(), snapshot.getQuantity(), snapshot.getSnapshotPrice(), snapshot.getSnapshotDate());
				if (price != null) {
					bbs.setSecurityPrice(price);
					bbs.setSnapshotValue(MathUtils.abs(MathUtils.multiply(MathUtils.multiply(snapshot.getQuantity(), price), snapshot.getInvestmentSecurity().getPriceMultiplier())));
				}
			}
			else {
				if (isUseStrikePrice()) {
					price = snapshot.getInvestmentSecurity().getOptionStrikePrice();
				}
				if (price == null && isUseUnderlyingSecurityPrice() && snapshot.getInvestmentSecurity().getUnderlyingSecurity() != null) {
					price = getMarketDataRetriever().getPrice(snapshot.getInvestmentSecurity().getUnderlyingSecurity(), snapshot.getSnapshotDate(), true, null);
				}

				if (price != null) {
					bbs.setSecurityPrice(price);
					bbs.setSnapshotValue(getInvestmentCalculator().calculateNotional(snapshot.getInvestmentSecurity(), bbs.getSecurityPrice(), bbs.getQuantity(), snapshot.getSnapshotDate()));
				}
			}
		}
		// Default uses security price/notional
		if (bbs.getSecurityPrice() == null) {
			bbs.setSecurityPrice(snapshot.getSnapshotPrice());
			bbs.setSnapshotValue(snapshot.getAbsBaseNotional());
		}

		return bbs;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isUseUnderlyingSecurityPrice() {
		return this.useUnderlyingSecurityPrice;
	}


	public void setUseUnderlyingSecurityPrice(boolean useUnderlyingSecurityPrice) {
		this.useUnderlyingSecurityPrice = useUnderlyingSecurityPrice;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public boolean isUseStrikePrice() {
		return this.useStrikePrice;
	}


	public void setUseStrikePrice(boolean useStrikePrice) {
		this.useStrikePrice = useStrikePrice;
	}


	public boolean isUseDirtyPrice() {
		return this.useDirtyPrice;
	}


	public void setUseDirtyPrice(boolean useDirtyPrice) {
		this.useDirtyPrice = useDirtyPrice;
	}


	public boolean isUseNetPerSecurity() {
		return this.useNetPerSecurity;
	}


	public void setUseNetPerSecurity(boolean useNetPerSecurity) {
		this.useNetPerSecurity = useNetPerSecurity;
	}


	public ProductUtilService getProductUtilService() {
		return this.productUtilService;
	}


	public void setProductUtilService(ProductUtilService productUtilService) {
		this.productUtilService = productUtilService;
	}
}
