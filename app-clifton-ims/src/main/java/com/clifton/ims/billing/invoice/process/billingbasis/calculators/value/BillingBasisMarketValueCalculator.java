package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.accounting.account.cache.AccountingAccountIdsCache;
import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.dw.analytics.AccountingPositionSnapshot;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.field.MarketDataValueHolder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisMarketValueCalculator</code> extends the {@link BaseBillingBasisAccountingPositionSnapshotCalculator} and calculates each {@link BillingBasisSnapshot} with the market value of the position and/or cash balance
 */
public class BillingBasisMarketValueCalculator extends BaseBillingBasisAccountingPositionSnapshotCalculator {

	private AccountingBalanceService accountingBalanceService;
	private AccountingPositionHandler accountingPositionHandler;
	private DataTableRetrievalHandler dataTableRetrievalHandler;
	private InvestmentInstrumentService investmentInstrumentService;

	private AccountingAccountIdsCache accountingAccountIdsCache;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Include Cash Balance in Market Value calculation
	 */
	private boolean includeCash;

	/**
	 * Include Cash Collateral in Cash portion
	 * Can be used without includeCash option so that cash collateral can be included independently
	 */
	private boolean includeCashCollateral;

	/**
	 * Includes Positions Market Value
	 * Note: Can be false and includePositionCollateral (super class property) can be true so that position collateral can be included independently
	 */
	private boolean includePositions;

	/**
	 * When set, will retrieve latest official price and recalculate market value
	 * If official price field is not defined for the security, will use snapshot price
	 */
	private boolean useLatestOfficialPrice;

	/**
	 * Maximum days back official price is valid.  If price date is older than an exception will be thrown
	 */
	private Integer maxDaysBackForOfficialPrice;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		List<BillingBasisSnapshot> result = null;
		if (isIncludePositions() || isIncludePositionCollateral()) {
			result = super.calculateBillingBasisSnapshotListImpl(config, invoice, billingDefinitionInvestmentAccountId, startDate, endDate);

			// CCY is now technically handled like cash, but for billing, we've included it always as a position, so
			// in order to include currency positions, we'll include that lookup here.
			if (result == null) {
				result = new ArrayList<>();
			}
			// Note: If only including Position Collateral this does not apply
			if (isIncludePositions()) {
				result.addAll(calculateCurrencyBillingBasisSnapshotList(invoice, billingDefinitionInvestmentAccountId, startDate, endDate));
			}
		}
		if (isIncludeCash() || isIncludeCashCollateral()) {
			if (result == null) {
				result = new ArrayList<>();
			}
			result.addAll(calculateCashBillingBasisSnapshotList(invoice, billingDefinitionInvestmentAccountId, startDate, endDate));
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                   Position Override Lookup              //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<AccountingPositionSnapshot> getAccountingPositionSnapshotList(BillingInvoiceProcessConfig config, BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		List<AccountingPositionSnapshot> snapshotList = super.getAccountingPositionSnapshotList(config, billingAccount, startDate, endDate);
		//  Just need to filter if including position collateral but not other positions
		// All other cases are handled by the parent method
		if (isIncludePositionCollateral() && !isIncludePositions()) {
			snapshotList = BeanUtils.filter(snapshotList, accountingPositionSnapshot -> accountingPositionSnapshot.getAccountingAccount().isCollateral());
		}
		return snapshotList;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                    Cash Balance Lookup                ///////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<BillingBasisSnapshot> calculateCashBillingBasisSnapshotList(BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);

		List<BillingBasisSnapshot> list = new ArrayList<>();

		Date date = startDate;
		Map<Date, BigDecimal> balanceMap;
		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountWithDateRange(billingAccount.getReferenceTwo().getId(), startDate, endDate);
		if (isIncludeCash()) {
			command.setExcludeCashCollateral(!isIncludeCashCollateral());
			balanceMap = getAccountingBalanceService().getAccountingCashBalanceMapIncludeReceivables(command);
		}
		// If not including cash, then just want cash collateral only so get total, then subtract cash amount without collateral
		else {
			command.setExcludeCashCollateral(false);
			balanceMap = getAccountingBalanceService().getAccountingCashBalanceMapIncludeReceivables(command);
			command.setExcludeCashCollateral(true);
			Map<Date, BigDecimal> cashOnlyBalanceMap = getAccountingBalanceService().getAccountingCashBalanceMapIncludeReceivables(command);
			balanceMap.replaceAll((k, v) -> MathUtils.subtract(v, cashOnlyBalanceMap.get(k)));
		}
		if (balanceMap != null && !balanceMap.isEmpty()) {
			while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
				// CASH VALUATION EXCLUDES COLLATERAL - CASH_COLLATERAL INCLUDES COLLATERAL CASH BALANCES
				BigDecimal result = balanceMap.get(date);
				list.add(createCashBillingBasisSnapshot(invoice, billingAccount, date, result));
				date = DateUtils.addDays(date, 1);
			}
		}
		return list;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////                  Currency Balance Lookup                //////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<BillingBasisSnapshot> calculateCurrencyBillingBasisSnapshotList(BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);

		List<BillingBasisSnapshot> list = new ArrayList<>();

		Map<Integer, Map<Date, BigDecimal>> currencyDateBalanceMap = getAccountingCurrencyBalanceMap(billingAccount.getReferenceTwo().getId(), billingAccount.getInvestmentGroup() == null ? null
				: billingAccount.getInvestmentGroup().getId(), startDate, endDate);

		if (currencyDateBalanceMap != null && !currencyDateBalanceMap.isEmpty()) {
			String dataSource = getMarketDataExchangeRatesApiService().getExchangeRateDataSourceForClientAccount(billingAccount.getReferenceTwo().toClientAccount());
			InvestmentSecurity baseCcy = billingAccount.getReferenceTwo().getBaseCurrency();
			for (Map.Entry<Integer, Map<Date, BigDecimal>> integerMapEntry : currencyDateBalanceMap.entrySet()) {
				Map<Date, BigDecimal> balanceMap = integerMapEntry.getValue();
				InvestmentSecurity cur = getInvestmentInstrumentService().getInvestmentSecurity(integerMapEntry.getKey());
				Date date = startDate;
				while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
					BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSource, cur.getSymbol(), baseCcy.getSymbol(), date).flexibleLookup());
					BigDecimal result = balanceMap.get(date);
					list.add(createCurrencyBillingBasisSnapshot(invoice, billingAccount, cur, date, fxRate, result));
					date = DateUtils.addDays(date, 1);
				}
			}
		}
		return list;
	}


	/**
	 * Similar Logic to Cash lookup, but looks for currency with filter on Investment Group (optional) if specified in case billing isn't on CCY
	 * and returns a map for each currency with a map of date and LOCAL amount.  When adding to billing basis FX rate is specified to get base amount using FX rate on that date
	 * <p>
	 * NOTE: This is custom SQL because we want to perform one SQL query for all dates in the period and not a separate lookup for each date
	 * to make it the most efficient - especially when used for period average
	 */
	private Map<Integer, Map<Date, BigDecimal>> getAccountingCurrencyBalanceMap(Integer clientAccountId, Short investmentGroupId, Date startDate, Date endDate) {

		String sql = "SELECT t.InvestmentSecurityID \"SecurityID\", cd.EndDate \"Date\", SUM(CASE WHEN TransactionDate <= cd.EndDate THEN LocalDebitCredit ELSE 0 END) \"Balance\" " +
				"FROM AccountingTransaction t " //
				+ "INNER JOIN InvestmentAccount ha ON t.HoldingInvestmentAccountID = ha.InvestmentAccountID "
				+ "INNER JOIN InvestmentAccountType at ON ha.InvestmentAccountTypeID = at.InvestmentAccountTypeID "
				+ "INNER JOIN CalendarDay cd ON cd.EndDate >= ? AND cd.EndDate <= ? "
				+ "INNER JOIN InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID "
				+ "WHERE t.AccountingAccountID IN (" + ArrayUtils.toString(getAccountingAccountIdsCache().getAccountingAccounts(AccountingAccountIdsCacheImpl.AccountingAccountIds.CURRENCY_NON_CASH_NON_GAIN_LOSS_ACCOUNTS)) + ")"
				+ " AND at.IsExcludedAccount = 0"
				+ " AND t.IsDeleted = 0"
				+ " AND s.IsCurrency = 1" // Currencies only
				+ " AND TransactionDate <= ?"
				+ " AND ClientInvestmentAccountID = " + clientAccountId
				+ ((investmentGroupId != null) ? " AND s.InvestmentInstrumentID IN (SELECT igi.InvestmentInstrumentID FROM InvestmentGroupItemInstrument igi INNER JOIN InvestmentGroupItem gi ON igi.InvestmentGroupItemID = gi.InvestmentGroupItemID WHERE gi.InvestmentGroupID = " + investmentGroupId + " )" : "")
				+ " GROUP BY cd.EndDate, t.InvestmentSecurityID ";

		SqlParameterValue[] params = new SqlParameterValue[]{SqlParameterValue.ofDate(startDate), SqlParameterValue.ofDate(endDate), SqlParameterValue.ofDate(endDate)};

		DataTable result = getDataTableRetrievalHandler().findDataTable(new SqlSelectCommand(sql, params));
		Map<Integer, Map<Date, BigDecimal>> currencyMap = new HashMap<>();
		if (result != null && result.getTotalRowCount() > 0) {
			for (int i = 0; i < result.getTotalRowCount(); i++) {
				DataRow row = result.getRow(i);
				Integer securityId = (Integer) row.getValue("SecurityID");
				Map<Date, BigDecimal> dateMap = currencyMap.get(securityId);
				if (dateMap == null) {
					dateMap = new HashMap<>();
				}
				dateMap.put((Date) row.getValue("Date"), (BigDecimal) row.getValue("Balance"));
				currencyMap.put(securityId, dateMap);
			}
			return currencyMap;
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, AccountingPositionSnapshot snapshot) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSecurity(snapshot.getInvestmentSecurity());
		bbs.setQuantity(snapshot.getQuantity());
		bbs.setSnapshotDate(snapshot.getSnapshotDate());
		if (isUseLatestOfficialPrice()) {
			// Get the latest official price validating the time frame.  Would throw an exception if the price date is beyond max days back or missing
			try {
				MarketDataValueHolder officialPrice = getMarketDataRetriever().getOfficialPriceFlexible(snapshot.getInvestmentSecurity(), snapshot.getSnapshotDate(), getMaxDaysBackForOfficialPrice());
				if (officialPrice != null) {
					bbs.setSecurityPrice(officialPrice.getMeasureValue());
					bbs.setSnapshotValue(calculateMarketValueForAccountingPositionSnapshot(snapshot, officialPrice));
				}
			}
			catch (ValidationException e) {
				// If running for projected revenue, ignore exception - will just use snapshot price
				if (!isProjectedRevenue()) {
					throw e;
				}
			}
		}
		// If price field isn't set use snapshot price and market value
		if (bbs.getSecurityPrice() == null) {
			bbs.setSecurityPrice(snapshot.getSnapshotPrice());
			bbs.setSnapshotValue(snapshot.getBaseMarketValue());
		}
		return bbs;
	}


	private BillingBasisSnapshot createCashBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, Date snapshotDate, BigDecimal cashBalance) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSnapshotDate(snapshotDate);
		bbs.setSecurity(billingAccount.getReferenceTwo().getBaseCurrency());
		if (isIncludePositions()) {
			// If includes positions, then negative cash balance would reduce overall market value
			bbs.setSnapshotValue(cashBalance);
		}
		else {
			// If cash only, then we bill on the abs value
			bbs.setSnapshotValue(MathUtils.abs(cashBalance));
		}
		return bbs;
	}


	private BillingBasisSnapshot createCurrencyBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, InvestmentSecurity currency, Date snapshotDate,
	                                                                BigDecimal fxRate, BigDecimal localCurrencyBalance) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSnapshotDate(snapshotDate);
		bbs.setSecurity(currency);
		// To keep backwards compatibility with when CCY were treated as positions, Set Local Amount AS Quantity
		bbs.setQuantity(localCurrencyBalance);
		// Note: Billing Basis is saved in the Account's Base CCY, so we use the FX Rate to set the Base Value for the Billing Basis
		// We don't set FX Rate on the Billing Basis Snapshot since that would only be used if the Account base CCY is different than the Billing Definition CCY
		bbs.setSnapshotValue(MathUtils.multiply(localCurrencyBalance, fxRate, 2));
		return bbs;
	}


	private BigDecimal calculateMarketValueForAccountingPositionSnapshot(AccountingPositionSnapshot accountingPositionSnapshot, MarketDataValueHolder priceObject) {
		AccountingTransaction tran = new AccountingTransaction();
		tran.setInvestmentSecurity(accountingPositionSnapshot.getInvestmentSecurity());
		tran.setHoldingInvestmentAccount(accountingPositionSnapshot.getHoldingInvestmentAccount());
		tran.setClientInvestmentAccount(accountingPositionSnapshot.getClientInvestmentAccount());
		tran.setSettlementDate(accountingPositionSnapshot.getSnapshotDate());
		tran.setPrice(priceObject.getMeasureValue());
		AccountingPosition position = AccountingPosition.forOpeningTransaction(tran);
		position.setRemainingQuantity(accountingPositionSnapshot.getQuantity());
		// Need to set Date to the Price Date because Market Value calculation will look up price again and we need the price from the date we found official price on
		// But using current position
		return getAccountingPositionHandler().getAccountingPositionMarketValue(position, priceObject.getMeasureDate());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList) {
		if (isIncludePositions()) {
			return super.getTradeDateList(billingDefinitionInvestmentAccount, startDate, endDate, currentTradeDateList);
		}
		// Cash Only - No Trades, so No Trade Date List
		return currentTradeDateList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isIncludeCash() {
		return this.includeCash;
	}


	public void setIncludeCash(boolean includeCash) {
		this.includeCash = includeCash;
	}


	public boolean isIncludeCashCollateral() {
		return this.includeCashCollateral;
	}


	public void setIncludeCashCollateral(boolean includeCashCollateral) {
		this.includeCashCollateral = includeCashCollateral;
	}


	public boolean isIncludePositions() {
		return this.includePositions;
	}


	public void setIncludePositions(boolean includePositions) {
		this.includePositions = includePositions;
	}


	public boolean isUseLatestOfficialPrice() {
		return this.useLatestOfficialPrice;
	}


	public void setUseLatestOfficialPrice(boolean useLatestOfficialPrice) {
		this.useLatestOfficialPrice = useLatestOfficialPrice;
	}


	public Integer getMaxDaysBackForOfficialPrice() {
		return this.maxDaysBackForOfficialPrice;
	}


	public void setMaxDaysBackForOfficialPrice(Integer maxDaysBackForOfficialPrice) {
		this.maxDaysBackForOfficialPrice = maxDaysBackForOfficialPrice;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public AccountingAccountIdsCache getAccountingAccountIdsCache() {
		return this.accountingAccountIdsCache;
	}


	public void setAccountingAccountIdsCache(AccountingAccountIdsCache accountingAccountIdsCache) {
		this.accountingAccountIdsCache = accountingAccountIdsCache;
	}
}
