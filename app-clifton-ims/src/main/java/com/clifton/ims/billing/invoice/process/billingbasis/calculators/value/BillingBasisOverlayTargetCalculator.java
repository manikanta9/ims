package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.core.util.MathUtils;
import com.clifton.portfolio.run.BasePortfolioRunReplication;

import java.math.BigDecimal;


/**
 * The <code>BillingBasisOverlayTargetCalculator</code> is used for Overlay type strategies (i.e. PIOS) that using daily Overlay {@link com.clifton.portfolio.run.PortfolioRun}
 * <p/>
 * This calculator extends the existing Synthetic Exposure calculator, and overrides the snapshot value to use
 *
 * @author manderson
 */
public class BillingBasisOverlayTargetCalculator extends BillingBasisSyntheticExposureCalculator {

	@Override
	protected <T extends BasePortfolioRunReplication> BigDecimal calculateSnapshotValue(T replication) {
		return MathUtils.abs(replication.getOverlayTargetTotal());
	}
}
