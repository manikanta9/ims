package com.clifton.ims.billing.invoice.process.schedule.waive;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingScheduleAccountBalanceNotExistsComparison</code> class is a {@link Comparison} that evaluates to true
 * if all investment accounts tied to the schedule do not have an accounting account balance on any date in the specified start/end date range specified by date properties
 * <p>
 * Example: Billing Schedules can be defined once to apply a retainer fee for a set of accounts.
 * The system can then use this comparison to then "waive" or skip the retainer fee if
 * the none of the accounts had any positions during the given invoice period
 *
 * @author manderson
 */
public class BillingScheduleAccountBalanceNotExistsWaiveCondition implements Comparison<BillingSchedule> {

	private AccountingAccountService accountingAccountService;
	private AccountingBalanceService accountingBalanceService;

	private BillingDefinitionService billingDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Id of account group that make this comparison evaluate to true.
	 */
	private short accountingAccountId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(BillingSchedule schedule, ComparisonContext context) {
		// Before being passed to the condition evaluation, the schedule start/end dates are changed to the billing basis start/end dates
		// for verification on specific dates that apply to an invoice
		Date startDate = schedule.getWaiveSystemConditionConfig().getInvoice().getBillingBasisStartDate();
		Date endDate = schedule.getWaiveSystemConditionConfig().getInvoice().getBillingBasisEndDate();

		List<BillingDefinitionInvestmentAccount> accountList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(schedule.getWaiveSystemConditionConfig().getInvoice().getBillingDefinition().getId());

		AccountingAccount aa = getAccountingAccountService().getAccountingAccount(getAccountingAccountId());

		List<String> trueAccountList = new ArrayList<>();
		List<String> falseAccountList = new ArrayList<>();

		for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(accountList)) {
			if (billingAccount.getBillingSchedule() == null || billingAccount.getBillingSchedule().equals(schedule)) {
				InvestmentAccount account = billingAccount.getReferenceTwo();
				// Don't evaluate the same account/investment group twice
				String accountLabel = account.getNumber() + (billingAccount.getInvestmentGroup() != null ? " (" + billingAccount.getInvestmentGroup().getName() + ")" : "");
				if (trueAccountList.contains(accountLabel) || falseAccountList.contains(accountLabel)) {
					continue;
				}

				// Validate the account definition is active during date range
				if (DateUtils.isOverlapInDates(schedule.getStartDate(), schedule.getEndDate(), billingAccount.getStartDate(), billingAccount.getEndDate())) {
					Short investmentGroupId = null;
					if (billingAccount.getInvestmentGroup() != null) {
						investmentGroupId = billingAccount.getInvestmentGroup().getId();
					}

					AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountWithDateRange(billingAccount.getReferenceTwo().getId(), startDate, endDate)
							.withAccountingAccountId(getAccountingAccountId())
							.withInvestmentGroupId(investmentGroupId);
					Integer result = getAccountingBalanceService().getAccountingAccountBalanceDaysActive(command);
					if (result == 0) {
						trueAccountList.add(accountLabel);
					}
					else {
						falseAccountList.add(accountLabel);
					}
				}
			}
		}

		if (context != null) {
			String messageSuffix = " [" + aa.getName() + "] balance(s) during the date range [" + DateUtils.fromDateShort(startDate) + " - " + DateUtils.fromDateShort(endDate);
			if (CollectionUtils.isEmpty(falseAccountList)) {
				context.recordTrueMessage("Accounts [" + StringUtils.collectionToCommaDelimitedString(trueAccountList) + "] had no " + messageSuffix);
			}
			else {
				context.recordFalseMessage("Accounts [" + StringUtils.collectionToCommaDelimitedString(falseAccountList) + "] had " + messageSuffix);
			}
		}

		return CollectionUtils.isEmpty(falseAccountList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public short getAccountingAccountId() {
		return this.accountingAccountId;
	}


	public void setAccountingAccountId(short accountingAccountId) {
		this.accountingAccountId = accountingAccountId;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}
}
