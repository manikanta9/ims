package com.clifton.ims.billing.invoice.process.billingbasis.calculators;


import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.billing.invoice.process.billingbasis.calculators.BaseBillingBasisCalculator;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.billingbasis.calculators.value.BillingBasisValueCalculator;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisPeriodAverageProjectedCalculator</code> is NOT a REAL CALCULATOR, but used for calculating projected revenue for an account period (month)
 * <p>
 * Any calculation type that can't support projected revenue calculations on it's own would use this calculator.  It takes the values for just the month we are projecting revenue for and applies it for the full invoice period.
 * i.e. Most invoices are Quarterly.  So if we generate the quarterly invoice using only the billing basis for a specific month, we can then take the invoice total / 3 for that month's projected values.
 *
 * @author manderson
 */
public class BillingBasisPeriodAverageProjectedCalculator extends BaseBillingBasisCalculator {

	private AccountingPositionHandler accountingPositionHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	// Optional - can be used when running a day early and we don't have
	// last day's runs.  This way we can use the previous day's run for that value
	// to help keep projections more consistent.
	private boolean usePreviousValueIfMissing = false;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isProjectedRevenueSupported() {
		return true;
	}


	@Override
	public List<BillingBasisSnapshot> rebuildBillingBasisSnapshotList(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date defaultStart, Date defaultEnd) {
		Date startDate = getBillingBasisStartDateForAccount(billingDefinitionInvestmentAccount, defaultStart);
		Date endDate = getBillingBasisEndDateForAccount(billingDefinitionInvestmentAccount, defaultEnd);

		// Get all values for the date range
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		return calculator.calculateBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount.getId(), startDate, endDate);
	}


	@Override
	public BillingBasis calculateImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate) {
		// Get values
		List<BillingBasisSnapshot> valueList = getBillingBasisSnapshotList(config, invoice, billingDefinitionInvestmentAccount, startDate, endDate);
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		// If the Invoice is billed in a different currency than the client account's base currency will need to convert it.
		// Uses Datasource for the M2M account relationship issuing company, else default.
		String dataSourceName = getBillingBasisSnapshotExchangeRateDataSource(invoice, billingDefinitionInvestmentAccount);

		// Split averages into months and then average the monthly data
		List<BigDecimal> notionalValueList = new ArrayList<>();

		List<BillingBasisSnapshot> filteredValueList = new ArrayList<>();

		// When running projected revenue - if using external billing basis the dates on the billing basis may be from a previous month
		Date billingStart = calculatePeriodAverageStartDate(startDate, valueList);
		Date billingEnd = calculatePeriodAverageEndDate(billingStart, startDate, endDate);


		Date date = billingStart;
		BillingBasisValueCalculator calculator = getBillingBasisValueCalculator(billingDefinitionInvestmentAccount);
		if (!calculator.isValidValuationDate(date)) {
			date = calculator.getNextValuationDate(date, true);
		}

		List<BillingBasisSnapshot> previousDateFilteredList = null;

		while (DateUtils.compare(date, billingEnd, false) <= 0) {
			List<BillingBasisSnapshot> dateFilteredList = filterBillingBasisSnapshotListByDate(valueList, date);
			boolean usingPrevious = false;
			if (isUsePreviousValueIfMissing() && CollectionUtils.isEmpty(dateFilteredList)) {
				if (!CollectionUtils.isEmpty(previousDateFilteredList)) {
					dateFilteredList = previousDateFilteredList;
					usingPrevious = true;
				}
			}

			if (!CollectionUtils.isEmpty(dateFilteredList)) {
				previousDateFilteredList = dateFilteredList;

				// DataSourceName will only be populated if we need to lookup exchange rate
				if (!StringUtils.isEmpty(dataSourceName)) {
					BigDecimal exchangeRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName, billingDefinitionInvestmentAccount.getReferenceTwo().getBaseCurrency().getSymbol(), invoice.getBillingDefinition().getBillingCurrency().getSymbol(), date).flexibleLookup());
					for (BillingBasisSnapshot sn : CollectionUtils.getIterable(dateFilteredList)) {
						// Some cases (right now external) supports setting an explicit FX Rate which has already been set on the snapshot
						// If missing only, then use system determined rate
						if (MathUtils.isNullOrZero(sn.getFxRate())) {
							sn.setFxRate(exchangeRate);
						}
					}
				}

				BigDecimal value = getBillingBasisTotalFromList(dateFilteredList);
				// Don't record dupe details for next day if using previous
				if (!usingPrevious) {
					filteredValueList.addAll(dateFilteredList);
				}
				notionalValueList.add(value);
			}
			// If No Positions On, then include the 0, else ignore it (We ignore random zeros for projection - for example, missing a main run on a specific date to prevent that one 0 from skewing the monthly average)
			else if (!billingDefinitionInvestmentAccount.getBillingBasisValuationType().isExternalBillingBasis() && !getAccountingPositionHandler().isClientAccountPositionsOnForDate(billingDefinitionInvestmentAccount.getReferenceTwo().getId(), date)) {
				notionalValueList.add(BigDecimal.ZERO);
			}

			// Move to next valuation date
			date = calculator.getNextValuationDate(date, true);
		}

		getBillingBasisService().saveBillingBasisSnapshotList(filteredValueList);

		// Ensure Final Billing Basis Total is Positive Value
		BigDecimal average = MathUtils.abs(CoreMathUtils.average(notionalValueList));
		return new BillingBasis(startDate, endDate, average);
	}


	private Date calculatePeriodAverageStartDate(Date startDate, List<BillingBasisSnapshot> snapshotList) {
		if (isProjectedRevenue()) {
			Date minStart = CollectionUtils.getFirstElementStrict(BeanUtils.sortWithFunction(snapshotList, BillingBasisSnapshot::getSnapshotDate, true)).getSnapshotDate();
			if (DateUtils.isDateBefore(minStart, startDate, false)) {
				return DateUtils.getFirstDayOfMonth(minStart);
			}
		}
		return startDate;
	}


	private Date calculatePeriodAverageEndDate(Date startDate, Date originalStartDate, Date endDate) {
		// If the start date was adjusted - then the end date is just the last day of that month
		if (isProjectedRevenue() && !DateUtils.isEqualWithoutTime(startDate, originalStartDate)) {
			return DateUtils.getLastDayOfMonth(startDate);
		}
		return endDate;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public boolean isUsePreviousValueIfMissing() {
		return this.usePreviousValueIfMissing;
	}


	public void setUsePreviousValueIfMissing(boolean usePreviousValueIfMissing) {
		this.usePreviousValueIfMissing = usePreviousValueIfMissing;
	}
}
