package com.clifton.ims.billing.invoice.process;


import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceDetailAccountCalculatorImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BillingInvoiceDetailAccountPositionsOnCalculator</code> extends the
 * BillingInvoiceDetailAccountCalculator, but for proportional account split allocations
 * will filter to include only those accounts that had positions on during the period so we
 * don't allocate fixed fees to accounts that didn't have any AUM/Billing Basis
 *
 * @author manderson
 */
public class BillingInvoiceDetailAccountPositionsOnCalculator extends BillingInvoiceDetailAccountCalculatorImpl {

	private AccountingPositionHandler accountingPositionHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected List<InvestmentAccount> filterProportionalSplitAccountList(BillingInvoice invoice, List<InvestmentAccount> proportionalSplitAccountList) {
		// If the list is empty - or only contains one account - return it
		// but if it contains more than one - filter out those that don't have any positions on during the period
		if (CollectionUtils.getSize(proportionalSplitAccountList) > 1) {
			List<InvestmentAccount> filteredList = new ArrayList<>();
			for (InvestmentAccount account : proportionalSplitAccountList) {

				int daysWithPositions = getAccountingPositionHandler().getClientAccountPositionsOnDays(account.getId(), invoice.getBillingBasisStartDate(),
						invoice.getBillingBasisEndDate(), null);
				if (daysWithPositions > 0) {
					filteredList.add(account);
				}
			}
			// As long as at least one found - use it
			if (!CollectionUtils.isEmpty(filteredList)) {
				return filteredList;
			}
		}
		// Otherwise split across all
		return proportionalSplitAccountList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}
}
