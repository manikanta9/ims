package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;

import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.dw.analytics.AccountingPositionSnapshot;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisQuantityCalculator</code> extends the {@link BaseBillingBasisAccountingPositionSnapshotCalculator} and calculates each {@link BillingBasisSnapshot} with the quantity of the position
 * Where Quantity is the equivalent of saying Face, i.e. Original Quantity / Unadjusted Face
 *
 * @author manderson
 */
public class BillingBasisQuantityCalculator extends BaseBillingBasisAccountingPositionSnapshotCalculator {

	/**
	 * If true - uses original quantity "Unadjusted Face"
	 * If false - uses remaining quantity "Adjusted Face"
	 */
	private boolean useOriginalQuantity;

	/**
	 * Groups positions by underlying and nets quantity by those positions
	 */
	private boolean useNetPerUnderlying;

	////////////////////////////////////////////////////////////////////////////////

	private AccountingTransactionService accountingTransactionService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		List<BillingBasisSnapshot> snapshotList = super.calculateBillingBasisSnapshotListImpl(config, invoice, billingDefinitionInvestmentAccountId, startDate, endDate);

		if (isUseNetPerUnderlying()) {
			// Create a Map by Date and Security (which is already set to the underlying if we are grouping that way)
			Map<String, List<BillingBasisSnapshot>> underlyingDateSnapshotListMap = BeanUtils.getBeansMap(snapshotList, billingBasisSnapshot -> billingBasisSnapshot.getSecurity().getId() + "_" + DateUtils.fromDateShort(billingBasisSnapshot.getSnapshotDate()));
			List<BillingBasisSnapshot> filteredList = new ArrayList<>();
			// For each grouping - keep only one snapshot record - and set the quantity and billing basis to the sum of all in the list
			for (List<BillingBasisSnapshot> underlyingDateSnapshotList : underlyingDateSnapshotListMap.values()) {
				if (!CollectionUtils.isEmpty(underlyingDateSnapshotList)) {
					BillingBasisSnapshot snapshot = underlyingDateSnapshotList.get(0);
					if (underlyingDateSnapshotList.size() > 1) {
						snapshot.setQuantity(CoreMathUtils.sumProperty(underlyingDateSnapshotList, BillingBasisSnapshot::getQuantity));
						snapshot.setSnapshotValue(CoreMathUtils.sumProperty(underlyingDateSnapshotList, BillingBasisSnapshot::getSnapshotValue));
					}
					filteredList.add(snapshot);
				}
			}
			snapshotList = filteredList;
		}

		// Set all final to Abs Value
		for (BillingBasisSnapshot billingBasisSnapshot : snapshotList) {
			billingBasisSnapshot.setSnapshotValue(MathUtils.abs(billingBasisSnapshot.getSnapshotValue()));
		}
		return snapshotList;
	}


	@Override
	public BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, AccountingPositionSnapshot snapshot) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		if (isUseNetPerUnderlying()) {
			bbs.setSecurity(snapshot.getInvestmentSecurity().getUnderlyingSecurity() != null ? snapshot.getInvestmentSecurity().getUnderlyingSecurity() : snapshot.getInvestmentSecurity());
		}
		else {
			bbs.setSecurity(snapshot.getInvestmentSecurity());
		}

		bbs.setQuantity(MathUtils.multiply(snapshot.getQuantity(), snapshot.getSnapshotExchangeRateToBase()));
		bbs.setSecurityPrice(snapshot.getSnapshotPrice());
		bbs.setSnapshotDate(snapshot.getSnapshotDate());

		if (isUseOriginalQuantity()) {
			bbs.setSnapshotValue(MathUtils.multiply(getAccountingTransactionService().getAccountingTransaction(snapshot.getAccountingTransactionId()).getQuantity(), snapshot.getSnapshotExchangeRateToBase()));
		}
		else {
			bbs.setSnapshotValue(MathUtils.multiply(snapshot.getQuantity(), snapshot.getSnapshotExchangeRateToBase()));
		}
		return bbs;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public boolean isUseOriginalQuantity() {
		return this.useOriginalQuantity;
	}


	public void setUseOriginalQuantity(boolean useOriginalQuantity) {
		this.useOriginalQuantity = useOriginalQuantity;
	}


	public boolean isUseNetPerUnderlying() {
		return this.useNetPerUnderlying;
	}


	public void setUseNetPerUnderlying(boolean useNetPerUnderlying) {
		this.useNetPerUnderlying = useNetPerUnderlying;
	}
}
