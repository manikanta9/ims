package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.accounting.account.cache.AccountingAccountIdsCacheImpl;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldTypes;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.product.hedging.ProductHedgingAccountStrategy;
import com.clifton.product.hedging.ProductHedgingAccountStrategyPosition;
import com.clifton.product.hedging.ProductHedgingService;
import com.clifton.product.hedging.search.ProductHedgingAccountStrategySearchForm;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingBasisNotionalExposureHedgeCalculator</code> calculates the {@link com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis) for an account that uses hedging strategies {@link ProductHedgingAccountStrategy}
 * Options Exposure Special Calculation Type From Hedging Account Strategies
 *
 * @author Mary Anderson
 */
public class BillingBasisNotionalExposureHedgeCalculator extends BaseBillingBasisValueCalculator {

	private AccountingTransactionService accountingTransactionService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataRetriever marketDataRetriever;
	private ProductHedgingService productHedgingService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean alwaysUseInitialPrice;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);
		List<BillingBasisSnapshot> list = new ArrayList<>();

		// get account's hedging strategies
		List<ProductHedgingAccountStrategy> strategyList = getProductHedgingAccountStrategyList(config, billingAccount.getReferenceTwo());

		for (ProductHedgingAccountStrategy strategy : CollectionUtils.getIterable(strategyList)) {
			// only process strategies that fall in the billing range (fully or partially)
			if (DateUtils.isOverlapInDates(strategy.getStartDate(), strategy.getEndDate(), startDate, endDate)) {
				List<ProductHedgingAccountStrategyPosition> positionList = getProductHedgingService().getProductHedgingAccountStrategyPositionListByStrategy(strategy.getId());
				// No positions = No billing basis
				if (CollectionUtils.isEmpty(positionList)) {
					return null;
				}

				// only use the first position: don't double count put/spread collar, etc.
				// Note that the first "position" could actually be made up of different lots/allocations, so need to filter the list by one contract and then sum to get all the transactions we'll need
				// Need to find all allocations of the contract as the position may only represent a part of the total contract's quantity
				ProductHedgingAccountStrategyPosition position = CollectionUtils.getFirstElement(positionList);
				if (position != null) {
					List<ProductHedgingAccountStrategyPosition> securityPositionList = BeanUtils.filter(positionList, productHedgingAccountStrategyPosition -> productHedgingAccountStrategyPosition.getAccountingTransaction() == null ? null : productHedgingAccountStrategyPosition.getAccountingTransaction().getInvestmentSecurity(),
							position.getAccountingTransaction().getInvestmentSecurity());

					Map<Date, BigDecimal> priceMap = new HashMap<>();

					// Only done if not using trade date evaluation, otherwise always uses opening price for all calculations
					if (!isAlwaysUseInitialPrice()) {
						// if just checking period end (one date) and last day is not a business day, need to get the last day there was a price
						if (DateUtils.compare(startDate, endDate, false) == 0) {
							priceMap.put(startDate, getMarketDataRetriever().getPriceFlexible(strategy.getUnderlyingSecurity(), startDate, true));
						}
						else {
							// get closing prices for strategy's underlying security for each day of the billing period
							MarketDataPriceFieldMapping priceFieldMapping = getMarketDataFieldMappingRetriever().getMarketDataPriceFieldMappingByInstrument(strategy.getUnderlyingSecurity().getInstrument());

							MarketDataValueSearchForm marketSearchForm = new MarketDataValueSearchForm(true);
							marketSearchForm.setInvestmentSecurityId(strategy.getUnderlyingSecurity().getId());
							marketSearchForm.setMinMeasureDate(DateUtils.addDays(startDate, -1));
							marketSearchForm.setMaxMeasureDate(DateUtils.addDays(endDate, 1));
							marketSearchForm.setDataFieldId(MarketDataPriceFieldTypes.CLOSING.getPriceField(priceFieldMapping, getMarketDataFieldService()).getId());
							marketSearchForm.setDataSourceId(MarketDataPriceFieldTypes.CLOSING.getPriceDataSourceId(priceFieldMapping));
							List<MarketDataValue> mvList = getMarketDataFieldService().getMarketDataValueList(marketSearchForm);

							for (MarketDataValue v : CollectionUtils.getIterable(mvList)) {
								priceMap.put(v.getMeasureDate(), v.getMeasureValue());
							}
						}
					}

					// Tracks the changes in quantity for the strategy over the invoice period.
					Map<Date, BigDecimal> quantityChangeMap = new HashMap<>();
					BigDecimal startingQuantity = BigDecimal.ZERO;

					for (ProductHedgingAccountStrategyPosition pos : CollectionUtils.getIterable(securityPositionList)) {
						// Get a list of child transactions for any changes in quantity
						AccountingTransactionSearchForm transactionSearchForm = new AccountingTransactionSearchForm();
						transactionSearchForm.setParentId(pos.getAccountingTransaction().getId());
						transactionSearchForm.setAccountingAccountIdName(AccountingAccountIdsCacheImpl.AccountingAccountIds.POSITION_ACCOUNTS);
						List<AccountingTransaction> atList = getAccountingTransactionService().getAccountingTransactionList(transactionSearchForm);

						// Add this transaction to the list so it's processed correctly for dates
						// Could have two opening transactions tied to a strategy - one might be part of the initial quantity, where the other is added later
						// Need to know when positions were traded to accurately reflect the dates/quantity changes
						if (atList == null) {
							atList = CollectionUtils.createList(pos.getAccountingTransaction());
						}
						else {
							atList.add(pos.getAccountingTransaction());
						}

						for (AccountingTransaction at : CollectionUtils.getIterable(atList)) {
							// If transaction occurred before invoice period, apply it to the starting quantity
							if (DateUtils.compare(at.getTransactionDate(), startDate, false) <= 0) {
								if (at.isOpening()) {
									startingQuantity = MathUtils.add(startingQuantity, MathUtils.abs(at.getQuantity()));
								}
								else {
									startingQuantity = MathUtils.subtract(startingQuantity, MathUtils.abs(at.getQuantity()));
								}
							}
							else {
								if (!quantityChangeMap.containsKey(at.getTransactionDate())) {
									quantityChangeMap.put(at.getTransactionDate(), BigDecimal.ZERO);
								}
								if (at.isOpening()) {
									quantityChangeMap.put(at.getTransactionDate(), MathUtils.add(quantityChangeMap.get(at.getTransactionDate()), MathUtils.abs(at.getQuantity())));
								}
								else {
									quantityChangeMap.put(at.getTransactionDate(), MathUtils.subtract(quantityChangeMap.get(at.getTransactionDate()), MathUtils.abs(at.getQuantity())));
								}
							}
						}
					}

					// If started before invoice start, set start quantity on the period start
					if (DateUtils.compare(strategy.getStartDate(), startDate, false) < 0) {
						quantityChangeMap.put(startDate, startingQuantity);
					}

					Date date = startDate;
					BigDecimal lastPrice = BigDecimal.ZERO;

					BigDecimal priceMultiplier = position.getAccountingTransaction().getInvestmentSecurity().getPriceMultiplier();
					BigDecimal currentQuantity = BigDecimal.ZERO;
					while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
						BigDecimal price;
						BigDecimal quantity;

						// If Strategy Hasn't Started Yet - Or already ended - Set a Place holder with a 0 Price, so 0 billing basis Value
						// (skip end of strategy date because there should be no position left at the end of the day)
						// Strategy End Date is not required, so if left open-ended consider it to be still active any time after the start date
						if ((DateUtils.compare(date, strategy.getStartDate(), false) < 0) || (strategy.getEndDate() != null && DateUtils.compare(date, strategy.getEndDate(), false) >= 0)) {
							price = BigDecimal.ZERO;
							quantity = BigDecimal.ZERO;
						}
						else {
							// Evaluating strategies, if there is a change in quantity, let's update the quantity
							if (quantityChangeMap.containsKey(date)) {
								quantity = MathUtils.add(currentQuantity, quantityChangeMap.get(date));
								currentQuantity = quantity;
							}
							else {
								quantity = currentQuantity;
							}

							if (isAlwaysUseInitialPrice()) {
								price = strategy.getUnderlyingInitialPrice();
							}
							else {
								price = priceMap.get(date);
								if (price == null) {
									price = lastPrice;
								}
								else {
									lastPrice = price;
								}
							}
						}
						// Exposure Notional = Underlying Price * Strategy Notional Multiplier * Security Price Multiplier * Strategy Quantity
						list.add(createBillingBasisSnapshot(invoice, billingAccount, strategy, date, price, priceMultiplier, quantity));
						date = DateUtils.addDays(date, 1);
					}
				}
			}
		}
		return list;
	}


	private List<ProductHedgingAccountStrategy> getProductHedgingAccountStrategyList(BillingInvoiceProcessConfig config, InvestmentAccount account) {
		List<ProductHedgingAccountStrategy> list = config.getAccountBillingSourceMapForAccount(account.getId(), ProductHedgingAccountStrategy.class, null, null);
		if (list == null) {
			ProductHedgingAccountStrategySearchForm searchForm = new ProductHedgingAccountStrategySearchForm();
			searchForm.setClientInvestmentAccountId(account.getId());
			list = getProductHedgingService().getProductHedgingAccountStrategyList(searchForm);
			if (list == null) {
				list = new ArrayList<>();
			}
			config.setAccountBillingSourceMapForAccount(account.getId(), ProductHedgingAccountStrategy.class, null, null, list);
		}
		return list;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, ProductHedgingAccountStrategy strategy, Date strategyPriceDate,
	                                                        BigDecimal strategyPrice, BigDecimal strategyPriceMultiplier, BigDecimal quantity) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSourceFkFieldId(strategy.getId());
		bbs.setQuantity(quantity);
		bbs.setSnapshotDate(strategyPriceDate);
		bbs.setSecurityPrice(strategyPrice);

		BigDecimal val = MathUtils.multiply(strategyPrice, strategyPriceMultiplier);
		val = MathUtils.multiply(val, strategy.getStrategyNotionalMultiplier());
		val = MathUtils.multiply(val, quantity);
		bbs.setSnapshotValue(val);
		return bbs;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public ProductHedgingService getProductHedgingService() {
		return this.productHedgingService;
	}


	public void setProductHedgingService(ProductHedgingService productHedgingService) {
		this.productHedgingService = productHedgingService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public boolean isAlwaysUseInitialPrice() {
		return this.alwaysUseInitialPrice;
	}


	public void setAlwaysUseInitialPrice(boolean alwaysUseInitialPrice) {
		this.alwaysUseInitialPrice = alwaysUseInitialPrice;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
