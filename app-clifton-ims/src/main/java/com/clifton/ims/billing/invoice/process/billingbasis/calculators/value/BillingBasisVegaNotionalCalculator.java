package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;

import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.dw.analytics.AccountingPositionSnapshot;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>BillingBasisVegaNotionalCalculator</code> extends the {@link BaseBillingBasisAccountingPositionSnapshotCalculator} and calculates each {@link BillingBasisSnapshot} with the vega notional value of the position
 * Billing Basis = Vega Notional custom field value * FX Rate to the Account Base Currency
 * Used for Variance Swaps
 *
 * @author manderson
 */
public class BillingBasisVegaNotionalCalculator extends BaseBillingBasisAccountingPositionSnapshotCalculator {


	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, AccountingPositionSnapshot snapshot) {
		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		bbs.setSecurity(snapshot.getInvestmentSecurity());
		bbs.setQuantity(snapshot.getQuantity());
		bbs.setSecurityPrice(snapshot.getSnapshotPrice());
		bbs.setSnapshotDate(snapshot.getSnapshotDate());

		BigDecimal vegaNotional = (BigDecimal) getInvestmentSecurityUtilHandler().getCustomColumnValue(bbs.getSecurity(), InvestmentSecurity.CUSTOM_FIELD_VEGA_NOTIONAL_AMOUNT);
		bbs.setSnapshotValue(MathUtils.multiply(vegaNotional, snapshot.getSnapshotExchangeRateToBase()));
		return bbs;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}
}
