package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.portfolio.replication.PortfolioReplicationHandler;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>BillingBasisSyntheticExposureCalculator</code> is used for Overlay type strategies (i.e. PIOS) that using daily Overlay {@link com.clifton.portfolio.run.PortfolioRun}
 * Where the {@link com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis} is the Synthetic Exposure value from the runs
 *
 * @author Mary Anderson
 */
public class BillingBasisSyntheticExposureCalculator extends BaseBillingBasisValueCalculator {

	private PortfolioReplicationHandler portfolioReplicationHandler;

	private PortfolioTargetRunService portfolioTargetRunService;
	private ProductOverlayService productOverlayService;

	/**
	 * System bean property for the portfolio run processing types for looking up replications. The default will load replications for overlay processing.
	 */
	private PortfolioRunProcessingTypes runProcessingType = PortfolioRunProcessingTypes.DEFAULT;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isValidValuationDate(Date date) {
		return DateUtils.isWeekday(date);
	}


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		if (moveForward) {
			return DateUtils.getNextWeekday(date);
		}
		return DateUtils.getPreviousWeekday(date);
	}


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);

		// Create a list of billing basis snapshots for applicable portfolio run replications for various processing types, sorted by snapshot date.
		// There can be one main run per day, so there should be no duplicate runs for different processing types.
		List<BillingBasisSnapshot> list = new ArrayList<>();
		List<? extends BasePortfolioRunReplication> replicationList = getPortfolioRunReplicationList(config, billingAccount, startDate, endDate);
		for (BasePortfolioRunReplication rep : CollectionUtils.getIterable(replicationList)) {
			BillingBasisSnapshot bbs = createBillingBasisSnapshot(invoice, billingAccount, rep);
			if (bbs != null) {
				list.add(bbs);
			}
		}

		return list;
	}


	@SuppressWarnings("unchecked")
	private <T extends BasePortfolioRunReplication> List<T> getPortfolioRunReplicationList(BillingInvoiceProcessConfig config, BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		List<T> replicationList;
		if (getRunProcessingType() == PortfolioRunProcessingTypes.PORTFOLIO_TARGET) {
			List<PortfolioTargetRunReplication> list = config.getAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), PortfolioTargetRunReplication.class, startDate, endDate);
			if (list == null) {
				// We NEVER Billing Matching Replications, so always filter the replication list to exclude them so we don't end up with extra rows
				// in the snapshot table with 0 Billing Basis - too much extra unnecessary data
				list = getPortfolioTargetRunService().getPortfolioTargetRunReplicationListByAccountMainRun(billingAccount.getReferenceTwo().getId(), startDate, endDate, true);
				config.setAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), PortfolioTargetRunReplication.class, startDate, endDate, list);
			}
			replicationList = (List<T>) list;
		}
		else {
			List<ProductOverlayAssetClassReplication> list = config.getAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), ProductOverlayAssetClassReplication.class, startDate, endDate);
			if (list == null) {
				// We NEVER Billing Matching Replications, so always filter the replication list to exclude them so we don't end up with extra rows
				// in the snapshot table with 0 Billing Basis - too much extra unnecessary data
				list = getProductOverlayService().getProductOverlayAssetClassReplicationListByAccountMainRun(billingAccount.getReferenceTwo().getId(), startDate, endDate, true);
				config.setAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), ProductOverlayAssetClassReplication.class, startDate, endDate, list);
			}
			replicationList = (List<T>) list;
		}

		return filterPortfolioRunReplicationListForInvestmentGroup(replicationList, billingAccount.getInvestmentGroup());
	}


	/**
	 * Returns a filtered list of replications according to those with a security where the instrument is within the provided instrument group.
	 * If no instrument group is provided, the provided list is returned.
	 */
	private <T extends BasePortfolioRunReplication> List<T> filterPortfolioRunReplicationListForInvestmentGroup(List<T> replicationList, InvestmentGroup investmentGroup) {
		if (investmentGroup == null) {
			return replicationList;
		}

		return CollectionUtils.getStream(replicationList)
				.filter(replication -> getInvestmentGroupService().isInvestmentInstrumentInGroup(investmentGroup.getId(), replication.getSecurity().getInstrument().getId()))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends BasePortfolioRunReplication> BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, T replication) {
		// NOTE: Excluded from Overlay Total is used for rollup asset classes, matching replications which should already be filtered
		// Cash Exposure isn't previously excluded through SQL because the SQL to join on the coalesce of trading account, main account is cumbersome
		// and since we already look at each rep to create billing basis snapshot it's easier to filter here for the few accounts where this can happen (Macalester, Marist)
		if (replication.isExcludedFromOverlayTotal() || replication.isCashExposure()) {
			return null;
		}

		BillingBasisSnapshot bbs = new BillingBasisSnapshot(invoice, billingAccount);
		InvestmentAssetClass assetClass = getAssetClass(replication.getSecurity(), replication.getAccountAssetClass());
		if (assetClass != null) {
			bbs.setAssetClass(assetClass);
		}
		bbs.setSecurity(replication.getSecurity());
		bbs.setQuantity(replication.getActualContracts());
		bbs.setSecurityPrice(replication.getSecurityPrice());
		bbs.setSourceFkFieldId(replication.getId());
		bbs.setSnapshotDate(replication.getBalanceDate());

		BigDecimal value = calculateSnapshotValue(replication);
		if (value == null) {
			value = BigDecimal.ZERO;
		}
		bbs.setSnapshotValue(value);
		return bbs;
	}


	protected <T extends BasePortfolioRunReplication> BigDecimal calculateSnapshotValue(T replication) {
		// If Contract Uses Exposure Multiplier and Replication Type Calculator Includes this
		// Reduce Billing Basis by that multiplier
		// Warnings are added to the invoice when we bill using Synthetic Exposure with an Exposure Multiplier
		// to catch this in case a new security is added using the exposure multiplier
		BigDecimal expMultiplier = replication.getSecurity().getInstrument().getExposureMultiplier();
		if (!MathUtils.isNullOrZero(expMultiplier) && !MathUtils.isEqual(BigDecimal.ONE, expMultiplier)) {
			// Recalc Contract Value
			getPortfolioReplicationHandler().recalculateContractValue(replication, true);
		}
		// Use Actual Exposure, not Overlay Exposure because we don't include Virtual Contracts
		return MathUtils.abs(replication.getActualExposure());
	}

	///////////////////////////////////////////////////////////////////////////

	protected enum PortfolioRunProcessingTypes {
		PORTFOLIO_TARGET,
		/**
		 * The default implementation provides run replications from overlay processing
		 */
		DEFAULT
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public PortfolioReplicationHandler getPortfolioReplicationHandler() {
		return this.portfolioReplicationHandler;
	}


	public void setPortfolioReplicationHandler(PortfolioReplicationHandler portfolioReplicationHandler) {
		this.portfolioReplicationHandler = portfolioReplicationHandler;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public ProductOverlayService getProductOverlayService() {
		return this.productOverlayService;
	}


	public void setProductOverlayService(ProductOverlayService productOverlayService) {
		this.productOverlayService = productOverlayService;
	}


	public PortfolioRunProcessingTypes getRunProcessingType() {
		return this.runProcessingType;
	}


	public void setRunProcessingType(PortfolioRunProcessingTypes runProcessingType) {
		this.runProcessingType = runProcessingType;
	}
}
