package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.process.BillingInvoiceProcessConfig;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.dw.analytics.AccountingPositionSnapshot;
import com.clifton.dw.analytics.DwAnalyticsService;
import com.clifton.dw.analytics.search.AccountingPositionSnapshotSearchForm;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingBasisAccountingPositionSnapshotCalculator</code> calculates {@link com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis} based on
 * the {@link AccountingPositionSnapshot} details for positions. Pulls all applicable positions and extending classes set up the values for the {@link BillingBasisSnapshot}
 * <p>
 * i.e. MarketValue, Notional
 *
 * @author Mary Anderson
 */
public abstract class BaseBillingBasisAccountingPositionSnapshotCalculator extends BaseBillingBasisValueCalculator {

	private DwAnalyticsService dwAnalyticsService;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	/**
	 * Note: This should never be true as we don't bill clients on position collateral,
	 * however there could be a case where we'll need it, so allowing it to be configurable.
	 */
	private boolean includePositionCollateral;


	/**
	 * Option to apply delta adjustment to each position
	 * If the security is priced daily, will require Delta value to be entered for each day
	 * Otherwise will do a flexible look up.
	 */
	private boolean applyDeltaAdjustment;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BillingBasisSnapshot> calculateBillingBasisSnapshotListImpl(BillingInvoiceProcessConfig config, BillingInvoice invoice, int billingDefinitionInvestmentAccountId, Date startDate, Date endDate) {
		BillingDefinitionInvestmentAccount billingAccount = getBillingDefinitionService().getBillingDefinitionInvestmentAccount(billingDefinitionInvestmentAccountId);
		List<BillingBasisSnapshot> list = new ArrayList<>();
		List<AccountingPositionSnapshot> snapshotList = getAccountingPositionSnapshotList(config, billingAccount, startDate, endDate);
		for (AccountingPositionSnapshot aps : CollectionUtils.getIterable(snapshotList)) {
			BillingBasisSnapshot snapshot = createBillingBasisSnapshot(invoice, billingAccount, aps);
			if (isApplyDeltaAdjustment()) {
				BigDecimal deltaValue = getMarketDataRetriever().getDeltaOnDateOrFlexible(snapshot.getSecurity(), snapshot.getSnapshotDate(), true);
				snapshot.setSnapshotValue(MathUtils.multiply(snapshot.getSnapshotValue(), deltaValue));
			}
			list.add(snapshot);
		}

		return list;
	}


	protected List<AccountingPositionSnapshot> getAccountingPositionSnapshotList(BillingInvoiceProcessConfig config, BillingDefinitionInvestmentAccount billingAccount, Date startDate, Date endDate) {
		List<AccountingPositionSnapshot> snapshotList = config.getAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), AccountingPositionSnapshot.class, startDate, endDate);
		if (snapshotList == null) {
			AccountingPositionSnapshotSearchForm searchForm = new AccountingPositionSnapshotSearchForm();
			searchForm.setClientInvestmentAccountId(billingAccount.getReferenceTwo().getId());
			if (startDate != null) {
				searchForm.addSearchRestriction(new SearchRestriction("snapshotDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
			}
			if (endDate != null) {
				searchForm.addSearchRestriction(new SearchRestriction("snapshotDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
			}
			snapshotList = getDwAnalyticsService().getAccountingPositionSnapshotList(searchForm);
			config.setAccountBillingSourceMapForAccount(billingAccount.getReferenceTwo().getId(), AccountingPositionSnapshot.class, startDate, endDate, snapshotList);
		}

		// Filter on Investment Group
		if (billingAccount.getInvestmentGroup() != null) {
			snapshotList = BeanUtils.filter(snapshotList, accountingPositionSnapshot -> getInvestmentGroupService().isInvestmentInstrumentInGroup(billingAccount.getInvestmentGroup().getName(), accountingPositionSnapshot.getInvestmentSecurity().getInstrument().getId()));
		}
		// Filter out Position Collateral
		if (!isIncludePositionCollateral()) {
			snapshotList = BeanUtils.filter(snapshotList, accountingPositionSnapshot -> !accountingPositionSnapshot.getAccountingAccount().isCollateral());
		}
		return snapshotList;
	}


	public abstract BillingBasisSnapshot createBillingBasisSnapshot(BillingInvoice invoice, BillingDefinitionInvestmentAccount billingAccount, AccountingPositionSnapshot snapshot);

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public DwAnalyticsService getDwAnalyticsService() {
		return this.dwAnalyticsService;
	}


	public void setDwAnalyticsService(DwAnalyticsService dwAnalyticsService) {
		this.dwAnalyticsService = dwAnalyticsService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public boolean isIncludePositionCollateral() {
		return this.includePositionCollateral;
	}


	public void setIncludePositionCollateral(boolean includePositionCollateral) {
		this.includePositionCollateral = includePositionCollateral;
	}


	public boolean isApplyDeltaAdjustment() {
		return this.applyDeltaAdjustment;
	}


	public void setApplyDeltaAdjustment(boolean applyDeltaAdjustment) {
		this.applyDeltaAdjustment = applyDeltaAdjustment;
	}
}
