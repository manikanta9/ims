package com.clifton.ims.billing.invoice.process.schedule.amount.proraters;


import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleSharing;
import com.clifton.billing.invoice.process.billingbasis.calculators.BillingBasis;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountDateProraterConfig;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountForBillingDatesProrater;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>BillingAmountForPositionsOnProrater</code>
 * <p>
 * Special processing - if this schedule is tied to a specific account, then we will prorate
 * the fixed fee based on the number of days the account had a Position Balance vs. the number of days
 * in the billing basis.  We use billing basis dates, instead of Invoice period dates because if the invoice
 * is billed in advance, then we have no idea of future holdings and can't properly determine the number of days
 * to prorate the amount for.  Kelly cannot think of an in advance bill where this would come into account
 * but just in case agreed to bill against the billing basis dates which is the period that just ended.
 *
 * @author manderson
 */
public class BillingAmountForPositionsOnProrater extends BillingAmountForBillingDatesProrater {


	private AccountingPositionHandler accountingPositionHandler;

	private BillingDefinitionService billingDefinitionService;

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void populateBillingAmountDateProraterConfig(BillingAmountDateProraterConfig proraterConfig, BillingScheduleProcessConfig scheduleConfig, BillingBasis.BillingBasisDetail billingBasisDetail) {
		Date startDate;
		Date endDate;
		if (scheduleConfig.getSchedule().getScheduleType().isAnnualFee()) {
			startDate = scheduleConfig.getAnnualInvoicePeriodStartDate();
			endDate = scheduleConfig.getAnnualInvoicePeriodEndDate();
		}
		else {
			startDate = scheduleConfig.getBillingBasisStartDate();
			endDate = scheduleConfig.getBillingBasisEndDate();
		}

		BillingSchedule billingSchedule = scheduleConfig.getSchedule();
		List<InvestmentAccount> accountList = new ArrayList<>();
		if (billingSchedule.getInvestmentAccount() != null) {
			accountList.add(billingSchedule.getInvestmentAccount());
		}
		else if (billingSchedule.getInvestmentAccountGroup() != null) {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setOurAccount(true);
			searchForm.setInvestmentAccountGroupId(billingSchedule.getInvestmentAccountGroup().getId());
			accountList.addAll(getInvestmentAccountService().getInvestmentAccountList(searchForm));
		}
		else {
			List<BillingDefinitionInvestmentAccount> billingAccountList = new ArrayList<>();
			// This also includes cases where "IsApplyToAllClientAccounts" since it takes all client accounts on the billing definition
			if (billingSchedule.getBillingDefinition() != null) {
				billingAccountList = getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(billingSchedule.getBillingDefinition().getId());
			}
			else {
				List<BillingScheduleSharing> sharedDefinitionList = getBillingDefinitionService().getBillingScheduleSharingListForSchedule(billingSchedule.getId());
				for (BillingScheduleSharing bss : CollectionUtils.getIterable(sharedDefinitionList)) {
					billingAccountList.addAll(getBillingDefinitionService().getBillingDefinitionInvestmentAccountListForDefinition(bss.getReferenceTwo().getId()));
				}
			}

			for (BillingDefinitionInvestmentAccount billingAccount : CollectionUtils.getIterable(billingAccountList)) {
				if (DateUtils.isOverlapInDates(startDate, endDate, billingAccount.getStartDate(), billingAccount.getEndDate())) {
					if (billingSchedule.isApplyToEachClientAccount() || billingAccount.getBillingSchedule() == null || billingAccount.getBillingSchedule().equals(billingSchedule)) {
						accountList.add(billingAccount.getReferenceTwo());
					}
				}
			}
		}

		List<Integer> checkedAccountIds = new ArrayList<>();
		int maxDays = 0;

		for (InvestmentAccount clientAccount : CollectionUtils.getIterable(accountList)) {
			if (checkedAccountIds.contains(clientAccount.getId())) {
				continue;
			}
			checkedAccountIds.add(clientAccount.getId());
			int accountDays = getAccountingPositionHandler().getClientAccountPositionsOnDays(clientAccount.getId(), startDate, endDate, null);
			if (accountDays > maxDays) {
				maxDays = accountDays;
			}
		}
		proraterConfig.setProrateDays(new BigDecimal(maxDays));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
