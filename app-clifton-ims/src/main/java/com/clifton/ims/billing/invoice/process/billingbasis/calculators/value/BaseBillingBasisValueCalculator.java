package com.clifton.ims.billing.invoice.process.billingbasis.calculators.value;


import com.clifton.billing.definition.BillingDefinitionInvestmentAccount;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.ims.mapping.InvestmentAssetClassFieldMappingHandler;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentAssetClass;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.system.fieldmapping.SystemFieldMapping;
import com.clifton.system.fieldmapping.SystemFieldMappingService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.Date;
import java.util.List;


public abstract class BaseBillingBasisValueCalculator extends com.clifton.billing.invoice.process.billingbasis.calculators.value.BaseBillingBasisValueCalculator {

	private BillingDefinitionService billingDefinitionService;

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentGroupService investmentGroupService;

	private TradeService tradeService;

	private SystemFieldMappingService systemFieldMappingService;

	private InvestmentAssetClassFieldMappingHandler investmentFieldMappingHandler;

	private Short fieldMappingId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Not required, but if set, defines the business day logic
	 * when not set we use weekdays, which currently applies for all
	 * cases except for Private Fund Investors which we need the first business day of the month
	 * as 1/1 is a holiday, so new investors are added on the second.
	 */
	private Short calendarId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getNextValuationDate(Date date, boolean moveForward) {
		if (getCalendarId() == null) {
			return (moveForward ? DateUtils.getNextWeekday(date) : DateUtils.getPreviousWeekday(date));
		}
		else {
			return getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, getCalendarId()), (moveForward ? 1 : -1));
		}
	}


	@Override
	public boolean isValidValuationDate(Date date) {
		if (getCalendarId() == null) {
			return DateUtils.isWeekday(date) || DateUtils.isLastDayOfMonth(date);
		}
		return getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, getCalendarId()));
	}


	/**
	 * Basic Implementation Looks up Actual Trade Dates for the Securities (filtered by Security Group if populated)
	 * Some Calculators - Like Cash - Return Null because it's not traded
	 * External returns all dates values are entered for
	 */
	@Override
	public List<Date> getTradeDateList(BillingDefinitionInvestmentAccount billingDefinitionInvestmentAccount, Date startDate, Date endDate, List<Date> currentTradeDateList) {
		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setClientInvestmentAccountId(billingDefinitionInvestmentAccount.getReferenceTwo().getId());
		if (billingDefinitionInvestmentAccount.getInvestmentGroup() != null) {
			searchForm.setInvestmentGroupId(billingDefinitionInvestmentAccount.getInvestmentGroup().getId());
		}
		searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
		List<Trade> tradeList = getTradeService().getTradeList(searchForm);
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			// We actually use the day before the trade date because
			// because we want the value of the positions before trades were done
			Date tradeDate = DateUtils.addDays(trade.getTradeDate(), -1);
			if (currentTradeDateList.contains(tradeDate)) {
				continue;
			}
			currentTradeDateList.add(tradeDate);
		}

		// Add billing period end date so we value from last trade to last date of billing period
		// Note: End date is the last day of billing period, not last day of invoice period in case the billing definition is
		// to end before the invoice period ends
		if (!currentTradeDateList.contains(endDate)) {
			currentTradeDateList.add(endDate);
		}
		return currentTradeDateList;
	}


	protected InvestmentAssetClass getAssetClass(InvestmentSecurity security, InvestmentAccountAssetClass accountAssetClass) {
		InvestmentAssetClass assetClass = accountAssetClass != null ? accountAssetClass.getAssetClass() : null;

		if (assetClass == null && getFieldMappingId() != null) {
			return getInvestmentFieldMappingHandler().getAssetClass(security, getFieldMapping());
		}
		return assetClass;
	}


	protected SystemFieldMapping getFieldMapping() {
		return getFieldMappingId() != null ? getSystemFieldMappingService().getSystemFieldMapping(getFieldMappingId()) : null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public SystemFieldMappingService getSystemFieldMappingService() {
		return this.systemFieldMappingService;
	}


	public void setSystemFieldMappingService(SystemFieldMappingService systemFieldMappingService) {
		this.systemFieldMappingService = systemFieldMappingService;
	}


	public InvestmentAssetClassFieldMappingHandler getInvestmentFieldMappingHandler() {
		return this.investmentFieldMappingHandler;
	}


	public void setInvestmentFieldMappingHandler(InvestmentAssetClassFieldMappingHandler investmentFieldMappingHandler) {
		this.investmentFieldMappingHandler = investmentFieldMappingHandler;
	}


	public Short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(Short calendarId) {
		this.calendarId = calendarId;
	}


	public Short getFieldMappingId() {
		return this.fieldMappingId;
	}


	public void setFieldMappingId(Short fieldMappingId) {
		this.fieldMappingId = fieldMappingId;
	}
}
