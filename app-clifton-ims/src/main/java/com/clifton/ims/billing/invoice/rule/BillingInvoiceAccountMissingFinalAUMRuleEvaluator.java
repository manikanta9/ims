package com.clifton.ims.billing.invoice.rule;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.billing.billingbasis.BillingBasisSnapshot;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.rule.BaseBillingInvoiceMissingSourceRuleEvaluator;
import com.clifton.billing.invoice.rule.BillingInvoiceRuleEvaluatorContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BillingInvoiceAccountMissingFinalAUMRuleEvaluator</code> validates that there is a source AccountingPeriodClosing record
 * and the value is not null (if null billing inserts a zero value) so this ensures final AUM has been entered for the invoice.
 *
 * @author manderson
 */
public class BillingInvoiceAccountMissingFinalAUMRuleEvaluator extends BaseBillingInvoiceMissingSourceRuleEvaluator {

	private AccountingPeriodService accountingPeriodService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getSourceTableName() {
		return "AccountingPeriodClosing";
	}


	@Override
	public boolean isUseZeroValueAsMissingSource() {
		return true;
	}


	@Override
	public List<RuleViolation> evaluateRuleImpl(BillingInvoice invoice, EntityConfig entityConfig, BillingInvoiceRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		List<BillingBasisSnapshot> snapshotList = getBillingBasisSnapshotListMissingSource(invoice, context);

		if (!CollectionUtils.isEmpty(snapshotList)) {
			// Get Map of Accounts to Dates Missing
			MultiValueHashMap<Integer, Date> accountMissingDateMap = new MultiValueHashMap<>(false);
			for (BillingBasisSnapshot bbs : snapshotList) {
				Integer accountId = bbs.getBillingDefinitionInvestmentAccount().getReferenceTwo().getId();
				if (accountMissingDateMap.get(accountId) == null || !accountMissingDateMap.get(accountId).contains(bbs.getSnapshotDate())) {
					// Missing period closing source fk field id
					if (bbs.getSourceFkFieldId() == null) {
						accountMissingDateMap.put(accountId, bbs.getSnapshotDate());
					}
					else {
						// Blank Final AUM value
						AccountingPeriodClosing periodClosing = getAccountingPeriodService().getAccountingPeriodClosing(bbs.getSourceFkFieldId());
						if (periodClosing == null || periodClosing.getPeriodEndAUM() == null) {
							accountMissingDateMap.put(accountId, bbs.getSnapshotDate());
						}
					}
				}
			}


			for (Map.Entry<Integer, Collection<Date>> accountMissingEntry : accountMissingDateMap.entrySet()) {
				List<Date> dateMissingList = new ArrayList<>();
				for (Date date : CollectionUtils.getIterable(accountMissingEntry.getValue())) {
					dateMissingList.add(date);
				}
				addRuleViolationToList(violationList, entityConfig, invoice, accountMissingEntry.getKey(), dateMissingList);
			}
		}
		return violationList;
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////            Getter and Setter Methods              ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}
}
