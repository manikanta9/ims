package com.clifton.ims.jobs;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;
import java.util.List;


/**
 * The <code>IMSPreLoadCacheJobResult</code> ...
 *
 * @author manderson
 */
public class IMSPreLoadCacheJobResult {

	private final StringBuilder result = new StringBuilder(16);

	private long currentStart = System.currentTimeMillis();

	private Date runDate;

	private Date previousBusinessDay;

	private Date previousMonthEnd;

	private List<InvestmentAccount> activeClientAccountList;

	private Status status;


	////////////////////////////////////////////////////////////////////////////


	public IMSPreLoadCacheJobResult(Status status) {
		this.status = status;
	}


	public void appendResultMessage(String resultMsg, boolean logAndEndDuration) {
		this.result.append(resultMsg);
		if (logAndEndDuration) {
			long end = System.currentTimeMillis();
			this.result.append("\n* Duration: ").append(DateUtils.getTimeDifference((end - this.currentStart), false)).append("\n");
			this.currentStart = end;
		}
		this.result.append("\n");
	}


	public String getResultMessage() {
		return this.result.toString();
	}


	public Date getPreviousBusinessDay() {
		return this.previousBusinessDay;
	}


	public void setPreviousBusinessDay(Date previousBusinessDay) {
		this.previousBusinessDay = previousBusinessDay;
	}


	public Date getPreviousMonthEnd() {
		return this.previousMonthEnd;
	}


	public void setPreviousMonthEnd(Date previousMonthEnd) {
		this.previousMonthEnd = previousMonthEnd;
	}


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public List<InvestmentAccount> getActiveClientAccountList() {
		return this.activeClientAccountList;
	}


	public void setActiveClientAccountList(List<InvestmentAccount> activeClientAccountList) {
		this.activeClientAccountList = activeClientAccountList;
	}


	public Status getStatus() {
		return this.status;
	}
}
