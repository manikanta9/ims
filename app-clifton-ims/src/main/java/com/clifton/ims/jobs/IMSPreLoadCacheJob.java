package com.clifton.ims.jobs;


import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.position.AccountingPositionService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.business.service.search.BusinessServiceProcessingTypeSearchForm;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.compliance.old.exchangelimit.position.CompliancePositionExchangeLimitPositionService;
import com.clifton.compliance.old.exchangelimit.search.CompliancePositionExchangeLimitPositionSearchForm;
import com.clifton.compliance.rule.assignment.ComplianceRuleAssignmentHandler;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.cache.InvestmentAccountGroupCache;
import com.clifton.investment.account.group.search.InvestmentAccountGroupSearchForm;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.InvestmentManagerAccountAssignment;
import com.clifton.investment.manager.InvestmentManagerAccountGroup;
import com.clifton.investment.manager.InvestmentManagerAccountService;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.cache.InvestmentManagerAccountBalanceDateCache;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.investment.manager.search.InvestmentManagerAccountSearchForm;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.product.util.ProductUtilServiceImpl;
import com.clifton.rule.definition.RuleCategory;
import com.clifton.rule.definition.RuleDefinitionService;
import com.clifton.rule.definition.search.RuleCategorySearchForm;
import com.clifton.rule.retriever.RuleRetrieverService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.search.SystemBeanSearchForm;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.search.SystemConditionSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.trade.roll.setup.TradeRollSetupService;
import com.clifton.trade.roll.setup.TradeRollType;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;


/**
 * The <code>IMSPreLoadCacheJob</code> is a batch job that can be run once in the morning
 * and will attempt to perform most common look ups in order to cache that database and
 * prevent additional db look ups for users to process individual accounts/runs, etc.
 *
 * @author manderson
 */
public class IMSPreLoadCacheJob implements Task {

	private DaoLocator daoLocator;

	private ApplicationContextService applicationContextService;

	private CacheHandler<?, ?> cacheHandler;


	private AccountingPositionService accountingPositionService;

	private BusinessServiceService businessServiceService;

	private CalendarBusinessDayService calendarBusinessDayService;
	private ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler;
	private CompliancePositionExchangeLimitPositionService compliancePositionExchangeLimitPositionService;

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountGroupCache investmentAccountGroupCache;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentInstrumentService investmentInstrumentService;

	private InvestmentManagerAccountService investmentManagerAccountService;
	private InvestmentManagerAccountBalanceDateCache investmentManagerAccountBalanceDateCache;
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private RuleDefinitionService ruleDefinitionService;
	private RuleRetrieverService ruleRetrieverService;

	private SystemColumnValueHandler systemColumnValueHandler;

	private SystemBeanService systemBeanService;
	private SystemConditionService systemConditionService;

	private TradeRollSetupService tradeRollSetupService;

	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	///////////////////////////////////////////////////////

	/**
	 * Optionally skip clearing the cache - by default will clear the cache
	 */
	private boolean doNotClearCache;

	/**
	 * Will run through all tables defined and if the primary key is a Short
	 * and the table uses caching will automatically load all rows to pre-cache the data in that row
	 * Add table names here to always skip them
	 */
	private List<String> excludeTables;

	/**
	 * Will run through all caches in the context that implement
	 * {@link NamedEntityCache} and will load all rows and cache
	 * the data.
	 * <p>
	 * Add cache names here to always skip them
	 */
	private List<String> excludeNamedEntityCacheNames;

	/**
	 * Normally, on Production would leave blank and will default to previous business day
	 * But for running locally or on QA and db backup is older, can set the date that would be running as
	 * <p>
	 * Used to determine security lookup for "active positions on date" since it uses snapshot
	 * And Manager Balance pre-loads for previous day, previous month end, etc.
	 */
	private Date portfolioRunDate;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofMessage("Starting Cache Pre Load");
		IMSPreLoadCacheJobResult result = new IMSPreLoadCacheJobResult(status); // sets start time

		Date runDate = getPortfolioRunDate();
		if (runDate == null) {
			runDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forTodayAndDefaultCalendar(), -1);
		}

		Date previousBD = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(runDate), -1);
		Date previousMEBD = DateUtils.getLastDayOfMonth(DateUtils.addMonths(runDate, -1));
		if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(previousMEBD))) {
			previousMEBD = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(previousMEBD), -1);
		}

		result.setRunDate(runDate);
		result.setPreviousBusinessDay(previousBD);
		result.setPreviousMonthEnd(previousMEBD);
		result.appendResultMessage("Initialized Cache Loading", true);

		processStep("Clear Cache", this::clearCache, result);
		processStep("Load All Lookup Tables", this::loadAllLookupTables, result);
		processStep("Load All Self Registering Named Entity Caches", this::loadAllSelfRegisteringNamedEntityCache, result);
		processStep("Load System Info", this::loadSystemInfo, result);
		processStep("Load Workflow Info", this::loadWorkflowInfo, result);
		processStep("Load Account Portfolio Info", this::loadAccountPortfolioInfo, result);
		processStep("Load Investment Security Info", this::loadInvestmentSecurityInfo, result);
		processStep("Load Compliance Info", this::loadComplianceInfo, result);
		processStep("Load System Defined Account Group Info", this::loadSystemDefinedAccountGroupInfo, result);
		processStep("Load Trade Roll Current Securities", this::loadTradeRollCurrentSecurities, result);
		processStep("Load Compliance Position Exchange Limit Positions", this::loadCompliancePositionExchangeLimitPositions, result);

		status.setMessage(result.getResultMessage());
		return status;
	}


	private void processStep(String stepName, Consumer<IMSPreLoadCacheJobResult> cacheStep, IMSPreLoadCacheJobResult result) {
		try {
			cacheStep.accept(result);
		}
		catch (Throwable e) {
			String message = "Error processing step " + stepName + ": " + ExceptionUtils.getDetailedMessage(e);
			result.getStatus().addError(message);
			LogUtils.errorOrInfo(getClass(), message, e);
		}
	}

	//////////////////////////////////////////////////////


	private void clearCache(IMSPreLoadCacheJobResult result) {
		if (!isDoNotClearCache()) {
			String[] caches = getCacheHandler().getCacheNames();
			if (caches != null && caches.length > 0) {
				for (String cacheName : caches) {
					try {
						getCacheHandler().clear(cacheName);
					}
					catch (Throwable e) {
						String msg = "Error clearing cache [" + cacheName + "]: " + ExceptionUtils.getDetailedMessage(e);
						LogUtils.errorOrInfo(getClass(), msg, e);
						result.getStatus().addError(msg);
						result.appendResultMessage(msg, false);
					}
				}
				result.appendResultMessage("Cleared [" + caches.length + "] caches.", true);
			}
		}
	}


	/**
	 * Runs findAll on all Dao's that use a Short for PK
	 * which means the data in the table will be small and used for look ups
	 */
	private void loadAllLookupTables(IMSPreLoadCacheJobResult result) {
		int count = 0;
		Collection<ReadOnlyDAO<?>> daoList = getDaoLocator().locateAll();

		for (ReadOnlyDAO<?> dao : CollectionUtils.getIterable(daoList)) {
			DAOConfiguration<?> config = dao.getConfiguration();
			// Skip DAO Configs with no Tables
			if (config instanceof NoTableDAOConfig<?>) {
				continue;
			}
			if (!config.getTable().isCacheUsed()) {
				continue;
			}
			if (CollectionUtils.contains(getExcludeTables(), config.getTableName())) {
				continue;
			}

			try {
				DataTypes dt = config.getPrimaryKeyColumn().getDataType();
				if (DataTypes.IDENTITY_SHORT == dt) {
					dao.findAll();
					count++;
				}
			}
			catch (Throwable e) {
				String msg = "Error loading table " + config.getTableName() + " for pre-caching: " + ExceptionUtils.getDetailedMessage(e);
				LogUtils.errorOrInfo(getClass(), msg, e);
				result.getStatus().addError(msg);
				result.appendResultMessage(msg, false);
			}
		}
		result.appendResultMessage("All Rows loaded for [" + count + "] table(s).", true);
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private void loadAllSelfRegisteringNamedEntityCache(IMSPreLoadCacheJobResult result) {
		Map<String, NamedEntityCache> cacheMap = getApplicationContextService().getContextBeansOfType(NamedEntityCache.class);

		int count = 0;
		if (cacheMap != null) {
			// for each DAO event cache bean
			for (Map.Entry<String, NamedEntityCache> stringNamedEntityCacheEntry : cacheMap.entrySet()) {
				if (CollectionUtils.contains(getExcludeNamedEntityCacheNames(), stringNamedEntityCacheEntry.getKey())) {
					continue;
				}
				try {
					NamedEntityCache<NamedObject> cacheBean = stringNamedEntityCacheEntry.getValue();
					Class dtoClass = cacheBean.getDtoClass();
					ReadOnlyDAO<NamedObject> dao = getDaoLocator().locate(dtoClass);
					List<NamedObject> list = dao.findAll();

					for (NamedObject bean : CollectionUtils.getIterable(list)) {
						cacheBean.setBean(bean);
					}
					count++;
				}
				catch (Throwable e) {
					String msg = "Error loading cache " + stringNamedEntityCacheEntry.getKey() + ": " + ExceptionUtils.getDetailedMessage(e);
					LogUtils.errorOrInfo(getClass(), msg, e);
					result.getStatus().addError(msg);
					result.appendResultMessage(msg, false);
				}
			}
		}

		result.appendResultMessage("Entities loaded for [" + count + "] BaseDaoNamedEntityCache(s).", true);
	}


	private void loadSystemInfo(IMSPreLoadCacheJobResult result) {
		List<SystemBean> beanList = getSystemBeanService().getSystemBeanList(new SystemBeanSearchForm());
		int singletonCount = 0;
		int count = 0;
		for (SystemBean bean : CollectionUtils.getIterable(beanList)) {
			try {
				// Singleton beans - load the instance since it is cached
				if (bean.getType().isSingleton()) {
					getSystemBeanService().getBeanInstance(bean);
					singletonCount++;
				}
				// Otherwise - load the properties for the bean (which is done for singletons in above call already)
				else {
					getSystemBeanService().getSystemBeanPropertyListByBeanId(bean.getId());
				}
				count++;
			}
			catch (Exception e) {
				String msg = "Error loading SystemBean cache: " + ExceptionUtils.getDetailedMessage(e);
				LogUtils.errorOrInfo(getClass(), msg, e);
				result.getStatus().addError(msg);
				result.appendResultMessage(msg, false);
			}
		}
		result.appendResultMessage("Loaded [" + count + "] system beans and their properties.  [" + singletonCount + "] of which are singletons and object instance was fully loaded.", true);

		List<SystemCondition> conditionList = getSystemConditionService().getSystemConditionList(new SystemConditionSearchForm());
		for (SystemCondition condition : CollectionUtils.getIterable(conditionList)) {
			// cache all condition and entries
			getSystemConditionService().getSystemConditionPopulated(condition.getId());
		}

		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(conditionList) + "] system conditions and their child conditions and entries.", true);
	}


	private void loadWorkflowInfo(IMSPreLoadCacheJobResult result) {
		List<Workflow> workflowList = getWorkflowDefinitionService().getWorkflowList();
		int count = 0;
		for (Workflow workflow : CollectionUtils.getIterable(workflowList)) {
			if (workflow.isActive()) {
				List<WorkflowState> stateList = getWorkflowDefinitionService().getWorkflowStateListByWorkflow(workflow.getId());
				// Load First Transition (Initial Assignment)
				getWorkflowTransitionService().getWorkflowTransitionFirst(workflow);
				for (WorkflowState state : CollectionUtils.getIterable(stateList)) {
					// Cache Workflow State By Name (For Individual Workflow)
					getWorkflowDefinitionService().getWorkflowStateByName(workflow.getId(), state.getName());
					// Cache Workflow State List By Name (Global)
					getWorkflowDefinitionService().getWorkflowStateListByName(state.getName());
					// Cache Transition List for the State
					getWorkflowTransitionService().getWorkflowTransitionListByStartOrEndState(state);
				}
				List<WorkflowTransition> transitionList = getWorkflowTransitionService().getWorkflowTransitionListByWorkflow(workflow.getId());
				for (WorkflowTransition transition : CollectionUtils.getIterable(transitionList)) {
					// Transitions - Cache List of Actions
					getWorkflowTransitionService().getWorkflowTransitionActionListForTransitionId(transition.getId());
				}
				count++;
			}
		}
		result.appendResultMessage("Loaded [" + count + "] active workflows and their states, transitions, and actions.", true);
	}


	private void loadAccountPortfolioInfo(IMSPreLoadCacheJobResult result) {
		// Load/Set List of All Active Client Accounts
		loadClientAccounts(result);

		// Load/Set List of All Active Manager Accounts (also loads Linked Manager Custom Field Values)
		loadManagerAccounts(result);

		// Load Portfolio Specific Info
		loadPortfolioSetup(result);

		//Load RuleConfig objects
		loadRuleConfigs(result);
	}


	private void loadClientAccounts(IMSPreLoadCacheJobResult result) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setWorkflowStatusName(WorkflowStatus.STATUS_ACTIVE);
		result.setActiveClientAccountList(getInvestmentAccountService().getInvestmentAccountList(searchForm));
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(result.getActiveClientAccountList()) + "] active client accounts.", true);

		for (InvestmentAccount account : CollectionUtils.getIterable(result.getActiveClientAccountList())) {
			getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipConfig(account.getId());
		}
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(result.getActiveClientAccountList()) + "] active client accounts relationship config.", true);
	}


	private void loadManagerAccounts(IMSPreLoadCacheJobResult result) {
		// Load All Active Manager Accounts
		InvestmentManagerAccountSearchForm mgrSearchForm = new InvestmentManagerAccountSearchForm();
		mgrSearchForm.setInactive(false);
		List<InvestmentManagerAccount> managerList = getInvestmentManagerAccountService().getInvestmentManagerAccountList(mgrSearchForm);
		List<InvestmentManagerAccount> linkedManagerList = new ArrayList<>();
		for (InvestmentManagerAccount account : CollectionUtils.getIterable(managerList)) {
			// Load Each Manager to get Rollup & M2M Adjustment Lists Cached
			getInvestmentManagerAccountService().getInvestmentManagerAccount(account.getId());

			// Linked Managers - PreLoad custom column values
			if (account.isLinkedManager()) {
				linkedManagerList.add(account);
			}
		}
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(managerList) + "] active manager accounts (including rollup and M2M adjustment info).", true);

		// Pre-load custom column info for Linked Managers
		getSystemColumnValueHandler().loadSystemColumnValueCacheForEntityList(linkedManagerList, ProductUtilServiceImpl.INVESTMENT_MANAGER_ACCOUNT_LINKED_MANAGER_CUSTOM_FIELD_GROUP);
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(linkedManagerList) + "] active linked manager account custom field values.", true);

		// Load Manager Balances for Previous Day and Previous Month End
		loadManagerAccountBalances(result, result.getPreviousBusinessDay());
		// First Business Day of Month - Previous Business Day and Previous Month End Dates are the same
		if (DateUtils.compare(result.getPreviousBusinessDay(), result.getPreviousMonthEnd(), false) != 0) {
			loadManagerAccountBalances(result, result.getPreviousMonthEnd());
		}
	}


	private void loadPortfolioSetup(IMSPreLoadCacheJobResult result) {
		// Service Processing Types - Cache custom fields
		List<BusinessServiceProcessingType> processingTypeList = getBusinessServiceService().getBusinessServiceProcessingTypeList(new BusinessServiceProcessingTypeSearchForm());
		getSystemColumnValueHandler().loadSystemColumnValueCacheForEntityList(processingTypeList, BusinessServiceProcessingType.BUSINESS_SERVICE_PROCESSING_TYPE_COLUMN_GROUP_NAME);

		// Track the accounts that use custom fields for portfolio runs
		List<InvestmentAccount> portfolioAccountList = new ArrayList<>();
		List<InvestmentAccount> piosAccountList = new ArrayList<>();
		for (InvestmentAccount account : CollectionUtils.getIterable(result.getActiveClientAccountList())) {
			if (account.getServiceProcessingType() != null && account.getServiceProcessingType().getProcessingType() != null) {
				ServiceProcessingTypes processingType = account.getServiceProcessingType().getProcessingType();

				if (ServiceProcessingTypes.PORTFOLIO_RUNS == processingType) {
					piosAccountList.add(account);
				}
				// LDI and SECURITY_TARGETS are the only other service processing types that have custom fields for this group
				else if (ServiceProcessingTypes.LDI == processingType || ServiceProcessingTypes.SECURITY_TARGET == processingType) {
					portfolioAccountList.add(account);
				}
			}
		}
		getSystemColumnValueHandler().loadSystemColumnValueCacheForEntityList(portfolioAccountList, InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME);
		result.appendResultMessage(
				"Loaded [" + InvestmentAccount.CLIENT_ACCOUNT_PORTFOLIO_SETUP_COLUMN_GROUP_NAME + "] custom field values for [" + CollectionUtils.getSize(portfolioAccountList)
						+ "] accounts.", true);
		getSystemColumnValueHandler().loadSystemColumnValueCacheForEntityList(piosAccountList, InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME);
		result.appendResultMessage("Loaded [" + InvestmentAccount.CLIENT_ACCOUNT_PIOS_PROCESSING_COLUMN_GROUP_NAME + "] custom field values for [" + CollectionUtils.getSize(piosAccountList)
				+ "] accounts.", true);

		portfolioAccountList.addAll(piosAccountList);
		for (InvestmentAccount account : CollectionUtils.getIterable(portfolioAccountList)) {
			ServiceProcessingTypes processingType = account.getServiceProcessingType().getProcessingType();

			if (processingType.isManagerAssignmentsSupported()) {
				List<InvestmentManagerAccountAssignment> assignList = getInvestmentManagerAccountService().getInvestmentManagerAccountAssignmentListByAccount(account.getId(), true);
				// Load Each to Get all Allocations
				for (InvestmentManagerAccountAssignment assign : CollectionUtils.getIterable(assignList)) {
					getInvestmentManagerAccountService().getInvestmentManagerAccountAssignment(assign.getId());
				}
			}

			if (processingType.isAssetClassesSupported()) {
				// Load Asset Classes
				getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassListByAccount(account.getId());

				// Most cases these will be empty - loading will prevent db retrieval attempt later
				List<InvestmentManagerAccountGroup> managerAccountGroupList = getInvestmentManagerAccountService().getInvestmentManagerAccountGroupListByInvestmentAccount(account.getId(), false);
				for (InvestmentManagerAccountGroup managerAccountGroup : CollectionUtils.getIterable(managerAccountGroupList)) {
					// When they aren't empty - pre-cache the allocations (by pulling the fully populated group bean)
					getInvestmentManagerAccountService().getInvestmentManagerAccountGroup(managerAccountGroup.getId());
				}
			}

			// TODO LDI OR SECURITY TARGET Specifics? Load KRD Points?  Review again after more accounts are setup for additional LDI info we can pre-cache
		}
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(portfolioAccountList) + "] account's manager assignments, allocations, and asset classes (if applies).", true);
	}


	/**
	 * Searches for all rule categories with an additional scope table of 'InvestmentAccount' and
	 * retrieves the rules for each active account so the ruleConfig objects are cached.
	 * This caches both the previous business day and the current day ruleConfig objects.
	 */
	private void loadRuleConfigs(IMSPreLoadCacheJobResult result) {
		RuleCategorySearchForm ruleCategorySearchForm = new RuleCategorySearchForm();
		ruleCategorySearchForm.setAdditionalScopeTableNameEquals("InvestmentAccount");
		List<RuleCategory> ruleCategoryList = getRuleDefinitionService().getRuleCategoryList(ruleCategorySearchForm);
		for (InvestmentAccount account : CollectionUtils.getIterable(result.getActiveClientAccountList())) {
			for (RuleCategory ruleCategory : ruleCategoryList) {
				//Cache previous business day
				getRuleRetrieverService().getRuleConfigListForAdditionalScopeEntity(ruleCategory, account, null, DateUtils.asLocalDate(result.getPreviousBusinessDay()));
				//Cache today
				getRuleRetrieverService().getRuleConfigListForAdditionalScopeEntity(ruleCategory, account, null, null);
			}
		}
		result.appendResultMessage("Loaded Rules Config for [" + CollectionUtils.getSize(result.getActiveClientAccountList()) + "] client accounts.", true);
	}


	private void loadManagerAccountBalances(IMSPreLoadCacheJobResult result, Date date) {
		InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
		searchForm.setBalanceDate(date);
		List<InvestmentManagerAccountBalance> balanceList = getInvestmentManagerAccountBalanceService().getInvestmentManagerAccountBalanceList(searchForm);
		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(balanceList)) {
			getInvestmentManagerAccountBalanceDateCache().storeInvestmentManagerAccountBalanceId(balance);
		}
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(balanceList) + "] manager balances for [" + DateUtils.fromDateShort(date) + "].", true);
	}


	private void loadInvestmentSecurityInfo(IMSPreLoadCacheJobResult result) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setActive(true);
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(securityList) + "] active securities.", true);

		Set<Integer> securityIdList = getPositionsOnSecurityIdList(result);
		List<InvestmentSecurity> usedSecurityList = new ArrayList<>();
		for (InvestmentSecurity sec : CollectionUtils.getIterable(securityList)) {
			// Note: lastPositionMap will never be null
			if (!sec.getInstrument().getHierarchy().isTradingDisallowed() && securityIdList.contains(sec.getId())) {
				usedSecurityList.add(sec);
			}
		}
		getSystemColumnValueHandler().loadSystemColumnValueCacheForEntityList(usedSecurityList, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		result.appendResultMessage("Loaded custom field values for [" + CollectionUtils.getSize(securityIdList) + "] active securities with positions.", true);
	}


	private void loadComplianceInfo(IMSPreLoadCacheJobResult result) {
		getComplianceRuleAssignmentHandler().buildComplianceCaches();
		result.appendResultMessage("Loaded compliance rule caches", true);
	}


	private void loadSystemDefinedAccountGroupInfo(IMSPreLoadCacheJobResult result) {
		InvestmentAccountGroupSearchForm searchForm = new InvestmentAccountGroupSearchForm();
		searchForm.setSystemDefined(true);
		List<InvestmentAccountGroup> accountGroupList = getInvestmentAccountGroupService().getInvestmentAccountGroupList(searchForm);

		for (InvestmentAccountGroup accountGroup : CollectionUtils.getIterable(accountGroupList)) {
			getInvestmentAccountGroupCache().buildGroupCache(accountGroup.getName());
		}

		result.appendResultMessage("Loaded [" + CollectionUtils.getSize(accountGroupList) + "] system defined account groups", true);
	}


	private void loadTradeRollCurrentSecurities(IMSPreLoadCacheJobResult result) {
		List<TradeRollType> tradeRollTypeList = getTradeRollSetupService().getTradeRollTypeList();
		for (TradeRollType rollType : CollectionUtils.getIterable(tradeRollTypeList)) {
			getTradeRollSetupService().getTradeRollTypeCurrentSecurityList(rollType.getId(), result.getRunDate());
		}
		result.appendResultMessage("Loaded Current Securities for [" + CollectionUtils.getSize(tradeRollTypeList) + "] roll types on " + DateUtils.fromDateShort(result.getRunDate()) + ".", true);
	}


	private void loadCompliancePositionExchangeLimitPositions(IMSPreLoadCacheJobResult result) {
		// Pre-Load Opening Positions for the past 30 days of positions for each instrument we have an exchange limit defined
		Date startDate = DateUtils.addDays(result.getRunDate(), -31);
		startDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 1);
		while (DateUtils.compare(startDate, result.getRunDate(), false) <= 0) {
			CompliancePositionExchangeLimitPositionSearchForm searchForm = new CompliancePositionExchangeLimitPositionSearchForm();
			searchForm.setActiveOnDate(startDate);
			// Note: Excludes pending trades, the goal is to load the position opening cache
			getCompliancePositionExchangeLimitPositionService().getCompliancePositionExchangeLimitPositionList(searchForm, true);
			startDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 1);
		}
		result.appendResultMessage("Loaded Positions vs. Limits for past 30 days.", true);
	}


	private Set<Integer> getPositionsOnSecurityIdList(IMSPreLoadCacheJobResult result) {
		// Instead of checking each security - load positions for each active client account - less DB retrievals
		Set<Integer> securityIdList = new HashSet<>();
		ArrayList<PositionLookupStats> lookupStats = new ArrayList<>();
		for (InvestmentAccount clientAccount : CollectionUtils.getIterable(result.getActiveClientAccountList())) {
			// NOTE: this code block sometimes takes very long time: record additional details to help troubleshoot the issue
			long startTimeMillis = System.currentTimeMillis();

			// use populated version to also cache AccountingTransactionHolder for all opening transactions
			List<AccountingPosition> positionList = getAccountingPositionService().getAccountingPositionListForClientAccount(clientAccount.getId(), result.getRunDate());

			lookupStats.add(new PositionLookupStats(clientAccount, CollectionUtils.getSize(positionList), System.currentTimeMillis() - startTimeMillis));

			for (AccountingPosition position : CollectionUtils.getIterable(positionList)) {
				securityIdList.add(position.getInvestmentSecurity().getId());
			}
		}

		result.appendResultMessage("Found current positions for [" + CollectionUtils.getSize(securityIdList) + "] securities.", true);
		// include stats for 10 slowest position look ups
		lookupStats.stream().filter(o -> o.durationMillis >= 500).sorted((o1, o2) -> Long.compare(o2.durationMillis, o1.durationMillis)).limit(10)
				.forEach(stats -> result.appendResultMessage("  - CA_ID: " + stats.clientAccount.getId() + ": " + stats.positionCount + " positions: " + stats.durationMillis + " ms", false));

		return securityIdList;
	}


	private static class PositionLookupStats {

		private final InvestmentAccount clientAccount;
		private final int positionCount;
		private final long durationMillis;


		PositionLookupStats(InvestmentAccount clientAccount, int positionCount, long durationMillis) {
			this.clientAccount = clientAccount;
			this.positionCount = positionCount;
			this.durationMillis = durationMillis;
		}
	}

	//////////////////////////////////////////////////////
	//////////     Getter & Setter Methods      //////////
	//////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public boolean isDoNotClearCache() {
		return this.doNotClearCache;
	}


	public void setDoNotClearCache(boolean doNotClearCache) {
		this.doNotClearCache = doNotClearCache;
	}


	public List<String> getExcludeTables() {
		return this.excludeTables;
	}


	public void setExcludeTables(List<String> excludeTables) {
		this.excludeTables = excludeTables;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentManagerAccountService getInvestmentManagerAccountService() {
		return this.investmentManagerAccountService;
	}


	public void setInvestmentManagerAccountService(InvestmentManagerAccountService investmentManagerAccountService) {
		this.investmentManagerAccountService = investmentManagerAccountService;
	}


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public RuleDefinitionService getRuleDefinitionService() {
		return this.ruleDefinitionService;
	}


	public void setRuleDefinitionService(RuleDefinitionService ruleDefinitionService) {
		this.ruleDefinitionService = ruleDefinitionService;
	}


	public RuleRetrieverService getRuleRetrieverService() {
		return this.ruleRetrieverService;
	}


	public void setRuleRetrieverService(RuleRetrieverService ruleRetrieverService) {
		this.ruleRetrieverService = ruleRetrieverService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public Date getPortfolioRunDate() {
		return this.portfolioRunDate;
	}


	public void setPortfolioRunDate(Date portfolioRunDate) {
		this.portfolioRunDate = portfolioRunDate;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentManagerAccountBalanceService getInvestmentManagerAccountBalanceService() {
		return this.investmentManagerAccountBalanceService;
	}


	public void setInvestmentManagerAccountBalanceService(InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService) {
		this.investmentManagerAccountBalanceService = investmentManagerAccountBalanceService;
	}


	public List<String> getExcludeNamedEntityCacheNames() {
		return this.excludeNamedEntityCacheNames;
	}


	public void setExcludeNamedEntityCacheNames(List<String> excludeNamedEntityCacheNames) {
		this.excludeNamedEntityCacheNames = excludeNamedEntityCacheNames;
	}


	public AccountingPositionService getAccountingPositionService() {
		return this.accountingPositionService;
	}


	public void setAccountingPositionService(AccountingPositionService accountingPositionService) {
		this.accountingPositionService = accountingPositionService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public InvestmentManagerAccountBalanceDateCache getInvestmentManagerAccountBalanceDateCache() {
		return this.investmentManagerAccountBalanceDateCache;
	}


	public void setInvestmentManagerAccountBalanceDateCache(InvestmentManagerAccountBalanceDateCache investmentManagerAccountBalanceDateCache) {
		this.investmentManagerAccountBalanceDateCache = investmentManagerAccountBalanceDateCache;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public ComplianceRuleAssignmentHandler getComplianceRuleAssignmentHandler() {
		return this.complianceRuleAssignmentHandler;
	}


	public void setComplianceRuleAssignmentHandler(ComplianceRuleAssignmentHandler complianceRuleAssignmentHandler) {
		this.complianceRuleAssignmentHandler = complianceRuleAssignmentHandler;
	}


	public InvestmentAccountGroupCache getInvestmentAccountGroupCache() {
		return this.investmentAccountGroupCache;
	}


	public void setInvestmentAccountGroupCache(InvestmentAccountGroupCache investmentAccountGroupCache) {
		this.investmentAccountGroupCache = investmentAccountGroupCache;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public TradeRollSetupService getTradeRollSetupService() {
		return this.tradeRollSetupService;
	}


	public void setTradeRollSetupService(TradeRollSetupService tradeRollSetupService) {
		this.tradeRollSetupService = tradeRollSetupService;
	}


	public CompliancePositionExchangeLimitPositionService getCompliancePositionExchangeLimitPositionService() {
		return this.compliancePositionExchangeLimitPositionService;
	}


	public void setCompliancePositionExchangeLimitPositionService(CompliancePositionExchangeLimitPositionService compliancePositionExchangeLimitPositionService) {
		this.compliancePositionExchangeLimitPositionService = compliancePositionExchangeLimitPositionService;
	}


	public BusinessServiceService getBusinessServiceService() {
		return this.businessServiceService;
	}


	public void setBusinessServiceService(BusinessServiceService businessServiceService) {
		this.businessServiceService = businessServiceService;
	}
}
