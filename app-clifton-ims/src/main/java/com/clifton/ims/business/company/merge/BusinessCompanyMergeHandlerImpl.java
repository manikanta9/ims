package com.clifton.ims.business.company.merge;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.company.search.BusinessCompanyTypeSearchForm;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.usedby.SystemUsedByEntity;
import com.clifton.system.usedby.SystemUsedByService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @see BusinessCompanyMergeHandler
 */
@Component
public class BusinessCompanyMergeHandlerImpl implements BusinessCompanyMergeHandler {

	/**
	 * Business Company type names that are not allowed as the source or target of a Business Company merge.
	 * Also used to filter the Business Company Type combo box on the user interface and the business companies
	 * shown in the usage details.
	 */
	private static final List<String> blackListedCompanyTypeNames = Arrays.asList(
			"Broker",
			"Client",
			"Client Group",
			"Client Relationship",
			"Commingled Vehicle",
			"Virtual Client");

	private SystemUsedByService systemUsedByService;
	private BusinessCompanyService businessCompanyService;
	private BusinessCompanyMergeActionHandler businessCompanyMergeActionHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public void mergeBusinessCompanies(BusinessCompany sourceBusinessCompany, BusinessCompany targetBusinessCompany, boolean deleteBusinessCompanyAfterMerge) {
		ValidationUtils.assertNotNull(sourceBusinessCompany, "Must supply a source business company.");
		ValidationUtils.assertNotNull(targetBusinessCompany, "Must supply a target business company.");
		ValidationUtils.assertFalse(Objects.equals(sourceBusinessCompany, targetBusinessCompany), "Cannot be the same Business Companies.");
		ValidationUtils.assertFalse(blackListedCompanyTypeNames.contains(sourceBusinessCompany.getType().getName()), String.format("The source business company type %s is not allowed", sourceBusinessCompany.getType().getName()));
		ValidationUtils.assertFalse(blackListedCompanyTypeNames.contains(targetBusinessCompany.getType().getName()), String.format("The target business company type %s is not allowed", targetBusinessCompany.getType().getName()));

		try {
			List<SystemUsedByEntity> sourceSystemUsedByEntities = getSystemUsedByService().getSystemUsedByEntityList("BusinessCompany", sourceBusinessCompany.getId());
			getBusinessCompanyMergeActionHandler().processSystemUsedByEntities(sourceSystemUsedByEntities, sourceBusinessCompany, targetBusinessCompany, deleteBusinessCompanyAfterMerge);
		}
		catch (Exception e) {
			//TODO: Should not be log and throwing
			String message = String.format("Failed to merge %s -> %s.", sourceBusinessCompany.getName(), targetBusinessCompany.getName());
			LogUtils.error(getClass(), message, e);
			throw new RuntimeException(message, e);
		}
	}


	@Override
	@Transactional(readOnly = true)
	public void mergeAllBusinessCompanies(Integer bloombergCompanyId, Short businessCompanyTypeId, BusinessCompany targetBusinessCompany, boolean deleteBusinessCompanyAfterMerge) {
		ValidationUtils.assertNotNull(bloombergCompanyId, "Must supply a Bloomberg ID.");
		ValidationUtils.assertNotNull(businessCompanyTypeId, "Must supply a business company type.");
		ValidationUtils.assertNotNull(targetBusinessCompany, "Must supply a target business company.");

		BusinessCompany processingBusinessCompany = null;
		try {
			CompanySearchForm companySearchForm = new CompanySearchForm();
			companySearchForm.setBloombergCompanyIdentifier(bloombergCompanyId);
			companySearchForm.setCompanyTypeId(businessCompanyTypeId);

			List<BusinessCompany> businessCompanyList = getBusinessCompanyService().getBusinessCompanyList(companySearchForm);
			for (BusinessCompany sourceBusinessCompany : businessCompanyList) {
				if (!Objects.equals(sourceBusinessCompany, targetBusinessCompany)) {
					processingBusinessCompany = sourceBusinessCompany;
					mergeBusinessCompanies(sourceBusinessCompany, targetBusinessCompany, deleteBusinessCompanyAfterMerge);
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Failed to merge ALL -> %s.", targetBusinessCompany == null ? "" : targetBusinessCompany.getName()) + String.format("Current source Business Company %s", processingBusinessCompany == null ? "" : processingBusinessCompany.getId()), e);
		}
	}


	@Override
	@Transactional(readOnly = true)
	public Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> getSystemUsedByEntityListMap(Integer bloombergCompanyId, Short businessCompanyTypeId) {
		ValidationUtils.assertNotNull(bloombergCompanyId, "Must supply a Bloomberg ID.");
		ValidationUtils.assertNotNull(businessCompanyTypeId, "Must supply a business company type.");

		Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> results = new HashMap<>();

		CompanySearchForm companySearchForm = new CompanySearchForm();
		companySearchForm.setBloombergCompanyIdentifier(bloombergCompanyId);
		companySearchForm.setCompanyTypeId(businessCompanyTypeId);

		List<BusinessCompany> businessCompanyList = getBusinessCompanyService().getBusinessCompanyList(companySearchForm);
		for (BusinessCompany businessCompany : businessCompanyList) {
			List<SystemUsedByEntity> usedByEntities = getSystemUsedByService().getSystemUsedByEntityList("BusinessCompany", businessCompany.getId());
			Map<SystemColumn, List<SystemUsedByEntity>> usedByMap = new HashMap<>();
			for (SystemUsedByEntity systemUsedByEntity : usedByEntities) {
				SystemColumn key = systemUsedByEntity.getColumn();
				if (!usedByMap.containsKey(key)) {
					usedByMap.put(key, new ArrayList<>());
				}
				List<SystemUsedByEntity> usedList = usedByMap.get(key);
				usedList.add(systemUsedByEntity);
			}
			results.put(businessCompany, usedByMap);
		}
		return sortByCustomerEntryCount(results);
	}


	@Override
	public List<BusinessCompanyType> getWhiteListedBusinessCompanyTypeList() {
		List<BusinessCompanyType> results = new ArrayList<>();

		BusinessCompanyTypeSearchForm businessCompanyTypeSearchForm = new BusinessCompanyTypeSearchForm();
		businessCompanyTypeSearchForm.setSystemDefined(false);
		List<BusinessCompanyType> types = getBusinessCompanyService().getBusinessCompanyTypeList(businessCompanyTypeSearchForm);

		for (BusinessCompanyType type : types) {
			if (!blackListedCompanyTypeNames.contains(type.getName())) {
				results.add(type);
			}
		}

		return results;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sort the provided map by the most {@link SystemUsedByEntity} first.
	 */
	private Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> sortByCustomerEntryCount(Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> entries) {
		List<Map.Entry<BusinessCompany, Integer>> customerEntries = entries
				.entrySet()
				.stream()
				.map(e -> new AbstractMap.SimpleImmutableEntry<>(e.getKey(), e.getValue().values().stream().mapToInt(List::size).sum()))
				.sorted((a, b) -> Integer.compare(b.getValue(), a.getValue()))
				.collect(Collectors.toList());
		Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> sorted = new LinkedHashMap<>();
		for (Map.Entry<BusinessCompany, Integer> entry : customerEntries) {
			sorted.put(entry.getKey(), entries.get(entry.getKey()));
		}
		return sorted;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemUsedByService getSystemUsedByService() {
		return this.systemUsedByService;
	}


	public void setSystemUsedByService(SystemUsedByService systemUsedByService) {
		this.systemUsedByService = systemUsedByService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public BusinessCompanyMergeActionHandler getBusinessCompanyMergeActionHandler() {
		return this.businessCompanyMergeActionHandler;
	}


	public void setBusinessCompanyMergeActionHandler(BusinessCompanyMergeActionHandler businessCompanyMergeActionHandler) {
		this.businessCompanyMergeActionHandler = businessCompanyMergeActionHandler;
	}
}
