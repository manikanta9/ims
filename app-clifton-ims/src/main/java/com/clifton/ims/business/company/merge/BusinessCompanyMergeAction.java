package com.clifton.ims.business.company.merge;

import java.util.Objects;


/**
 * <code>BusinessCompanyMergeAction</code> placeholder for information needed to merge Business Companies including service bean and service method
 * names.
 */
public class BusinessCompanyMergeAction {

	public enum ActionTypes {
		SERVICE_BEAN,
		SYSTEM_COLUMN_VALUE,
		USED_BY_AWARE,
		SOFT_LINKS
	}

	private ActionTypes type;

	private String serviceBeanName;
	private String methodName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyMergeAction(String serviceBeanName, String methodName, ActionTypes type) {
		this.serviceBeanName = serviceBeanName;
		this.methodName = methodName;
		this.type = type;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BusinessCompanyMergeAction that = (BusinessCompanyMergeAction) o;
		return this.type == that.type &&
				Objects.equals(this.serviceBeanName, that.serviceBeanName) &&
				Objects.equals(this.methodName, that.methodName);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.type, this.serviceBeanName, this.methodName);
	}


	@Override
	public String toString() {
		return "BusinessCompanyMergeAction{" +
				"type=" + type +
				", serviceBeanName='" + serviceBeanName + '\'' +
				", methodName='" + methodName + '\'' +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ActionTypes getType() {
		return this.type;
	}


	public void setType(ActionTypes type) {
		this.type = type;
	}


	public String getServiceBeanName() {
		return this.serviceBeanName;
	}


	public void setServiceBeanName(String serviceBeanName) {
		this.serviceBeanName = serviceBeanName;
	}


	public String getMethodName() {
		return this.methodName;
	}


	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
}
