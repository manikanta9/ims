package com.clifton.ims.business.company.merge;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.usedby.SystemUsedByEntity;

import java.util.List;
import java.util.Map;


/**
 * <code>BusinessCompanyMergeHandler</code>  provides controller-type business methods to the business company usage JSP including functionality to merge
 * business companies and generating a map of Business Company system used by entities based on Bloomberg Identifier and business company type.
 */
public interface BusinessCompanyMergeHandler {

	/**
	 * Merges sourceBusinessCompany into targetBusinessCompany.
	 * All foreign key references to sourceBusinessCompany are updated to targetBusinessCompany.
	 */
	public void mergeBusinessCompanies(BusinessCompany sourceBusinessCompany, BusinessCompany targetBusinessCompany, boolean deleteBusinessCompanyAfterMerge);


	/**
	 * Merges all source business companies identified by the foreign key values into targetBusinessCompany.  Each company is merged in its own transaction, this is not
	 * all or nothing.
	 */
	void mergeAllBusinessCompanies(Integer bloombergCompanyId, Short businessCompanyTypeId, BusinessCompany targetBusinessCompany, boolean deleteBusinessCompanyAfterMerge);


	/**
	 * Get All {@link SystemUsedByEntity} entities based on the Bloomberg Identifier keyed by business company and system column.
	 */
	public Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> getSystemUsedByEntityListMap(Integer bloombergCompanyId, Short businessCompanyTypeId);


	/**
	 * Get the list of supported business company types.
	 */
	public List<BusinessCompanyType> getWhiteListedBusinessCompanyTypeList();
}
