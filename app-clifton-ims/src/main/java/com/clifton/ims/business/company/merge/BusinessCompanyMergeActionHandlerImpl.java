package com.clifton.ims.business.company.merge;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.system.usedby.SystemUsedByEntity;
import com.clifton.system.usedby.softlink.SoftLinkField;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * @see BusinessCompanyMergeActionHandler
 */
@Component
public class BusinessCompanyMergeActionHandlerImpl<T extends IdentityObject> implements BusinessCompanyMergeActionHandler, ApplicationContextAware {

	/**
	 * Table names that cannot be merged, attempts to merge business companies using these table types will
	 * result in an error message and an abort of the merge process.
	 */
	public static final List<String> blackListedTableNames = Arrays.asList(
			"Trade",
			"TradeOrder",
			"TradeDestinationMapping",
			"TradeQuote",
			"AccountingCommissionDefinition",
			"BusinessClient",
			"BusinessContract",
			"BusinessContractParty",
			"BusinessClientRelationship",
			"CollateralHaircutDefinition",
			"InvestmentInstructionDelivery",
			"InvestmentInstruction",
			"InvestmentInstructionContact",
			"LendingRepoDefinition",
			"MarketDataSource",
			"MarketDataSourceCompanyMapping",
			"ReconcilePositionDefinition",
			"ReconcilePositionExternalPriceOverride",
			"ReconcileTradeIntradayExternal"
	);

	private ApplicationContext applicationContext;
	private DaoLocator daoLocator;
	private SystemColumnService systemColumnService;
	private SystemColumnValueService systemColumnValueService;
	private BusinessCompanyService businessCompanyService;
	private SystemBeanService systemBeanService;

	private Map<String, Map<BusinessCompanyMergeAction.ActionTypes, BusinessCompanyMergeAction>> actionsByTableMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processSystemUsedByEntities(List<SystemUsedByEntity> sourceSystemUsedByEntities, BusinessCompany sourceBusinessCompany, BusinessCompany targetBusinessCompany, boolean deleteBusinessCompanyAfterMerge) {
		for (SystemUsedByEntity systemUsedByEntity : sourceSystemUsedByEntities) {
			if (blackListedTableNames.contains(systemUsedByEntity.getColumn().getTable().getName())) {
				throw new RuntimeException(String.format("Merges to [%s] are not supported.", systemUsedByEntity.getColumn().getTable()));
			}
			else {
				BusinessCompanyMergeAction action = getBusinessCompanyMergeAction(systemUsedByEntity);
				if (action == null) {
					throw new RuntimeException("Merge Action cannot be found for " + systemUsedByEntity.getLabel());
				}
				else {
					executeBusinessCompanyMergeAction(systemUsedByEntity, sourceBusinessCompany, targetBusinessCompany);
				}
			}
		}
		if (deleteBusinessCompanyAfterMerge) {
			getBusinessCompanyService().deleteBusinessCompany(sourceBusinessCompany.getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * for the provided system used by entity return the merge action, will throw exception if more than one merge action is
	 * possible for the merge action could not be determined
	 */
	private BusinessCompanyMergeAction getBusinessCompanyMergeAction(SystemUsedByEntity systemUsedByEntity) {
		AssertUtils.assertNotNull(systemUsedByEntity, "System used by entity is required.");
		if (this.actionsByTableMap == null) {
			buildActionByTypeMap();
		}
		String tableName = systemUsedByEntity.getColumn().getTable().getName();
		if (!this.actionsByTableMap.containsKey(tableName) || !this.actionsByTableMap.get(tableName).containsKey(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN)) {
			BusinessCompanyMergeAction action = getStandardServiceBeanAction(systemUsedByEntity);
			if (action != null) {
				this.actionsByTableMap.putIfAbsent(tableName, generateEmptyActionsMap());
				this.actionsByTableMap.get(tableName).put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN, action);
			}
		}
		Map<BusinessCompanyMergeAction.ActionTypes, BusinessCompanyMergeAction> actionMap = this.actionsByTableMap.get(tableName);
		if (systemUsedByEntity.getColumn().isCustomColumn()) {
			return actionMap.get(BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE);
		}
		List<BusinessCompanyMergeAction> actionList = actionMap.values().stream().filter(a -> a.getType() != BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE).collect(Collectors.toList());
		AssertUtils.assertFalse(actionList.size() > 1, "Ambiguous merge action.");
		return actionList.isEmpty() ? null : actionList.get(0);
	}


	/**
	 * Perform the merge from source to target business for the provide system used by entity.
	 */
	private void executeBusinessCompanyMergeAction(SystemUsedByEntity systemUsedByEntity, BusinessCompany sourceBusinessCompany, BusinessCompany targetBusinessCompany) {
		AssertUtils.assertNotNull(systemUsedByEntity, "Used by entity is required.");
		AssertUtils.assertNotNull(sourceBusinessCompany, "Target Business Company is required.");
		AssertUtils.assertNotNull(systemUsedByEntity.getColumn(), "System Column is required.");
		AssertUtils.assertTrue(SystemColumnStandard.class.isAssignableFrom(systemUsedByEntity.getColumn().getClass()), "Must be a Standard Column type.");

		BusinessCompanyMergeAction action = getBusinessCompanyMergeAction(systemUsedByEntity);
		if (action == null) {
			throw new RuntimeException("Could not find action for system used by entity " + systemUsedByEntity.getLabel());
		}
		switch (action.getType()) {
			case SERVICE_BEAN:
				executeServiceMethod(systemUsedByEntity, action, targetBusinessCompany);
				break;
			case SOFT_LINKS:
				executeSoftLinksMethod(systemUsedByEntity, action, targetBusinessCompany);
				break;
			case SYSTEM_COLUMN_VALUE:
				executeSystemColumnValueMethod(systemUsedByEntity, action, targetBusinessCompany);
				break;
			case USED_BY_AWARE:
				executeUsedByAwareMethod(systemUsedByEntity, action, sourceBusinessCompany, targetBusinessCompany);
				break;
			default:
				throw new RuntimeException("Action handler not found " + action.getType());
		}
	}


	/**
	 * Do the service method call to update the foreign key reference.
	 */
	private void executeServiceMethod(SystemUsedByEntity systemUsedByEntity, BusinessCompanyMergeAction action, BusinessCompany targetBusinessCompany) {
		AssertUtils.assertTrue(action.getType() == BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN, "Only handles Service method type merges.");
		AssertUtils.assertTrue((systemUsedByEntity.getTotalRecords() != null && systemUsedByEntity.getUsedById() == null) ||
				(systemUsedByEntity.getTotalRecords() == null && systemUsedByEntity.getUsedById() != null), "System used by entity ID or bulk listing but not both.");

		Integer sourceEntityKey = systemUsedByEntity.getUsedById();
		if (sourceEntityKey != null) {
			SystemColumnStandard systemColumnStandard = (SystemColumnStandard) systemUsedByEntity.getColumn();
			ReadOnlyDAO<T> dao = getDaoLocator().locate(systemUsedByEntity.getColumn().getTable().getName());
			AssertUtils.assertNotNull(dao, "Could not find dao for " + systemUsedByEntity.getColumn().getName());
			IdentityObject sourceEntity = dao.findByPrimaryKey(sourceEntityKey);
			AssertUtils.assertNotNull(sourceEntity, "Could not locate source entity " + sourceEntityKey);
			if ("InvestmentManagerAccount".equals(systemUsedByEntity.getColumn().getTable().getName())) {
				sourceEntity = (IdentityObject) executeAction("investmentManagerAccountService", "getInvestmentManagerAccount", sourceEntityKey);
			}
			BeanUtils.setPropertyValue(sourceEntity, systemColumnStandard.getBeanPropertyName(), targetBusinessCompany);
			executeAction(action.getServiceBeanName(), action.getMethodName(), sourceEntity);
		}
		else {
			executeServiceMethodBulk(systemUsedByEntity, action, targetBusinessCompany);
		}
	}


	/**
	 * Do the service method call to update the soft link key reference.
	 */
	private void executeSoftLinksMethod(SystemUsedByEntity systemUsedByEntity, BusinessCompanyMergeAction action, BusinessCompany targetBusinessCompany) {
		AssertUtils.assertTrue(action.getType() == BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS, "Only handles Soft Link method type merges.");
		AssertUtils.assertTrue((systemUsedByEntity.getTotalRecords() != null && systemUsedByEntity.getUsedById() == null) ||
				(systemUsedByEntity.getTotalRecords() == null && systemUsedByEntity.getUsedById() != null), "System used by entity ID or bulk listing but not both.");

		Integer sourceEntityKey = systemUsedByEntity.getUsedById();
		if (sourceEntityKey != null) {
			ReadOnlyDAO<T> dao = getDaoLocator().locate(systemUsedByEntity.getColumn().getTable().getName());
			AssertUtils.assertNotNull(dao, "Could not find dao for " + systemUsedByEntity.getColumn().getName());
			IdentityObject sourceEntity = dao.findByPrimaryKey(sourceEntityKey);
			AssertUtils.assertNotNull(sourceEntity, "Could not locate source entity " + sourceEntityKey);

			String propertyName = getSoftLinkPropertyName(systemUsedByEntity, sourceEntity);
			BeanUtils.setPropertyValue(sourceEntity, propertyName, targetBusinessCompany.getId());
			executeAction(action.getServiceBeanName(), action.getMethodName(), sourceEntity);
		}
		else {
			// cannot determine property name without instance, would soft links exceed 500?
			throw new RuntimeException("Cannot execute bulk soft links.");
		}
	}


	/**
	 * Do the service method call to update the system column value key reference.
	 */
	private void executeSystemColumnValueMethod(SystemUsedByEntity systemUsedByEntity, BusinessCompanyMergeAction action, BusinessCompany targetBusinessCompany) {
		AssertUtils.assertTrue(action.getType() == BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE, "Only handles System Column Value method type merges.");
		AssertUtils.assertTrue((systemUsedByEntity.getTotalRecords() != null && systemUsedByEntity.getUsedById() == null) ||
				(systemUsedByEntity.getTotalRecords() == null && systemUsedByEntity.getUsedById() != null), "System used by entity ID or bulk listing but not both.");

		Integer sourceEntityKey = systemUsedByEntity.getUsedById();

		if (sourceEntityKey != null) {
			SystemColumnCustom systemColumnCustom = (SystemColumnCustom) systemUsedByEntity.getColumn();
			SystemColumnCustomConfig config = new SystemColumnCustomConfig();
			config.setColumnGroupName(systemColumnCustom.getColumnGroup().getName());
			config.setColumnList(Collections.singletonList(systemColumnCustom));
			config.setEntityId(sourceEntityKey);

			Integer businessCompanyId = targetBusinessCompany.getId();
			config = getSystemColumnValueService().getSystemColumnCustomConfig(config);
			for (SystemColumnValue value : config.getColumnValueList()) {
				value.setFkFieldId(businessCompanyId);
			}
			getSystemColumnValueService().saveSystemColumnValueList(config);
		}
		else {
			// cannot lookup configuration without instance, would custom column values exceed 500?
			throw new RuntimeException("Cannot execute bulk system column values.");
		}
	}


	/**
	 * Do the service method call to update the used by aware key reference.
	 */
	private void executeUsedByAwareMethod(SystemUsedByEntity systemUsedByEntity, BusinessCompanyMergeAction action, BusinessCompany sourceBusinessCompany, BusinessCompany targetBusinessCompany) {
		AssertUtils.assertTrue(action.getType() == BusinessCompanyMergeAction.ActionTypes.USED_BY_AWARE, "Only handles Used By Aware method type merges.");
		AssertUtils.assertTrue((systemUsedByEntity.getTotalRecords() != null && systemUsedByEntity.getUsedById() == null) ||
				(systemUsedByEntity.getTotalRecords() == null && systemUsedByEntity.getUsedById() != null), "System used by entity ID or bulk listing but not both.");
		AssertUtils.assertTrue("SystemBean".equals(systemUsedByEntity.getColumn().getTable().getName()), "System used by aware only supported for System Beans not " + systemUsedByEntity.getColumn().getTable().getName());

		String sourceKey = sourceBusinessCompany.getId().toString();
		String targetKey = targetBusinessCompany.getId().toString();

		SystemColumn businessCompanySystemColumn = getBusinessCompanySystemColumn();
		SystemBean systemBean = getSystemBeanService().getSystemBean(systemUsedByEntity.getUsedById());

		boolean updatedProperty = false;
		for (SystemBeanProperty systemBeanProperty : systemBean.getPropertyList()) {
			if (Objects.equals(businessCompanySystemColumn.getTable(), systemBeanProperty.getType().getValueTable())) {
				if (StringUtils.isEqual(systemBeanProperty.getValue(), sourceKey)) {
					systemBeanProperty.setValue(targetKey);
					updatedProperty = true;
				}
			}
		}
		if (updatedProperty) {
			getSystemBeanService().saveSystemBean(systemBean);
		}
	}


	/**
	 * Perform bulk update, the system used by service didn't return a record for each entity to update, only a record count.
	 */
	private void executeServiceMethodBulk(SystemUsedByEntity systemUsedByEntity, BusinessCompanyMergeAction action, BusinessCompany targetBusinessCompany) {
		SystemColumnStandard systemColumnStandard = (SystemColumnStandard) systemUsedByEntity.getColumn();

		Serializable entityIdNumber = targetBusinessCompany.getId();
		ReadOnlyDAO<T> beanDao = getDaoLocator().locate(systemColumnStandard.getTable().getName());
		Column daoColumn = CollectionUtils.getFirstElement(BeanUtils.filter(beanDao.getConfiguration().getColumnList(), Column::getName, systemColumnStandard.getName()));
		ValidationUtils.assertNotNull(daoColumn, "Cannot find DAO configuration for column: " + systemColumnStandard);
		entityIdNumber = beanDao.convertToPrimaryKeyDataType(entityIdNumber);

		ReadOnlyDAO<T> dao = getDaoLocator().locate(systemColumnStandard.getTable().getName());
		List<T> list = dao.findByField(systemColumnStandard.getBeanPropertyName() + ".id", entityIdNumber);

		for (T identityObject : list) {
			BeanUtils.setPropertyValue(identityObject, systemColumnStandard.getBeanPropertyName(), targetBusinessCompany);
			executeAction(action.getServiceBeanName(), action.getMethodName(), identityObject);
		}
	}


	/**
	 * Do reflection-based method invocation with entity.
	 */
	private Object executeAction(String beanName, String methodName, Object parameter) {
		try {
			AssertUtils.assertTrue(getApplicationContext().containsBean(beanName), "Missing bean [" + beanName + "] in the context.");
			Object service = getApplicationContext().getBean(beanName);
			Method method = MethodUtils.getMethod(service.getClass(), methodName, false);
			AssertUtils.assertNotNull(method, "Bean of Class [" + service.getClass() + "] is missing method [" + methodName + "] or method is not accessible.");
			Class<?>[] parameterTypes = method.getParameterTypes();
			AssertUtils.assertEquals(1, parameterTypes.length, "The number of method arguments should be one, but is [" + parameterTypes.length + "].");
			return method.invoke(service, parameter);
		}
		catch (Exception e) {
			throw new RuntimeException("Error invoking method [" + beanName + "." + methodName + "] " + " with argument [" + parameter + "]", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Lazily build map of actions for performing the merge operation.
	 */
	private synchronized void buildActionByTypeMap() {

		// hard links
		Map<String, Map<BusinessCompanyMergeAction.ActionTypes, BusinessCompanyMergeAction>> results = new ConcurrentHashMap<>(getCustomServiceActionMap());

		// custom values
		results.putIfAbsent("BusinessCompany", generateEmptyActionsMap());
		results.get("BusinessCompany").put(BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE,
				new BusinessCompanyMergeAction("systemColumnValueService", "saveSystemColumnValueList", BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE));
		results.putIfAbsent("BusinessClientRelationship", generateEmptyActionsMap());
		results.get("BusinessClientRelationship").put(BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE,
				new BusinessCompanyMergeAction("systemColumnValueService", "saveSystemColumnValueList", BusinessCompanyMergeAction.ActionTypes.SYSTEM_COLUMN_VALUE));

		// used by aware
		results.putIfAbsent("SystemBean", generateEmptyActionsMap());
		results.get("SystemBean").put(BusinessCompanyMergeAction.ActionTypes.USED_BY_AWARE,
				new BusinessCompanyMergeAction("systemBeanService", "saveSystemBean", BusinessCompanyMergeAction.ActionTypes.USED_BY_AWARE));

		// soft links
		results.putIfAbsent("SystemNote", generateEmptyActionsMap());
		results.get("SystemNote").put(BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS,
				new BusinessCompanyMergeAction("systemNoteService", "saveSystemNote", BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS));
		results.putIfAbsent("RuleViolation", generateEmptyActionsMap());
		results.get("RuleViolation").put(BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS,
				new BusinessCompanyMergeAction("ruleViolationService", "saveRuleViolation", BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS));
		results.putIfAbsent("SystemHierarchy", generateEmptyActionsMap());
		results.get("SystemHierarchy").put(BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS,
				new BusinessCompanyMergeAction("systemHierarchyDefinitionService", "saveSystemHierarchy", BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS));
		results.putIfAbsent("IntegrationManagerAssetClassGroup", generateEmptyActionsMap());
		results.get("IntegrationManagerAssetClassGroup").put(BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS,
				new BusinessCompanyMergeAction("integrationManagerService", "saveIntegrationManagerAssetClassGroup", BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS));
		results.putIfAbsent("IntegrationSource", generateEmptyActionsMap());
		results.get("IntegrationSource").put(BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS,
				new BusinessCompanyMergeAction("integrationSourceService", "saveIntegrationSource", BusinessCompanyMergeAction.ActionTypes.SOFT_LINKS));

		this.actionsByTableMap = results;
	}


	/**
	 * Get service beans for handling hard foreign keys in which the standard naming convention does not work.
	 */
	private Map<String, Map<BusinessCompanyMergeAction.ActionTypes, BusinessCompanyMergeAction>> getCustomServiceActionMap() {
		Map<String, Map<BusinessCompanyMergeAction.ActionTypes, BusinessCompanyMergeAction>> results = new HashMap<>();

		results.put("InvestmentManagerAccount", generateEmptyActionsMap());
		results.get("InvestmentManagerAccount").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("investmentManagerAccountService", "saveInvestmentManagerAccount", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("InvestmentSecurity", generateEmptyActionsMap());
		results.get("InvestmentSecurity").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("investmentSecurityService", "saveInvestmentSecurity", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("AccountingCommissionDefinition", generateEmptyActionsMap());
		results.get("AccountingCommissionDefinition").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("accountingCommissionService", "saveAccountingCommissionDefinition", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("BusinessCompanyRelationship", generateEmptyActionsMap());
		results.get("BusinessCompanyRelationship").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("businessCompanyService", "saveBusinessCompanyRelationship", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("BusinessContractParty", generateEmptyActionsMap());
		results.get("BusinessContractParty").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("businessContractService", "saveBusinessContractParty", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("InvestmentInstructionContact", generateEmptyActionsMap());
		results.get("InvestmentInstructionContact").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("investmentInstructionSetupService", "saveInvestmentInstructionContact", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("ComplianceCreditRatingOutlook", generateEmptyActionsMap());
		results.get("ComplianceCreditRatingOutlook").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("complianceCreditRatingService", "saveComplianceCreditRatingOutlook", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("MarketDataSourceCompanyMapping", generateEmptyActionsMap());
		results.get("MarketDataSourceCompanyMapping").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("marketDataSourceCompanyService", "saveMarketDataSourceCompanyMapping", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("BusinessClientRelationship", generateEmptyActionsMap());
		results.get("BusinessClientRelationship").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("businessClientService", "saveBusinessClientRelationship", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("InvestmentGroupMatrix", generateEmptyActionsMap());
		results.get("InvestmentGroupMatrix").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("investmentGroupService", "saveInvestmentGroupMatrix", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("InvestmentSpecificityEntry", generateEmptyActionsMap());
		results.get("InvestmentSpecificityEntry").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("investmentSpecificityService", "saveInvestmentSpecificityEntry", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("InvestmentManagerCustodianAccount", generateEmptyActionsMap());
		results.get("InvestmentManagerCustodianAccount").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("investmentManagerCustodianService", "saveInvestmentManagerCustodianAccount", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("ReconcilePositionExternalPriceOverride", generateEmptyActionsMap());
		results.get("ReconcilePositionExternalPriceOverride").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("reconcilePositionExternalDailyService", "saveReconcilePositionExternalPriceOverride", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));
		results.put("TradeDestinationMapping", generateEmptyActionsMap());
		results.get("TradeDestinationMapping").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("TradeDestinationService", "saveTradeDestinationMapping", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));

		results.put("AccountingTransaction", generateEmptyActionsMap());
		results.get("AccountingTransaction").put(BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN,
				new BusinessCompanyMergeAction("accountingJournalService", "saveAccountingJournal", BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN));

		return results;
	}


	/**
	 * Attempt to find the service bean in the context using the standard bean naming convention of table name + 'Service', verify the save method exists and
	 * also uses the standard naming convention.
	 */
	private BusinessCompanyMergeAction getStandardServiceBeanAction(SystemUsedByEntity systemUsedByEntity) {
		DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) getApplicationContext().getAutowireCapableBeanFactory();
		String tableName = systemUsedByEntity.getColumn().getTable().getName();
		String beanName = Character.toLowerCase(tableName.charAt(0)) + tableName.substring(1) + "Service";
		if (beanFactory.containsBean(beanName)) {
			BeanDefinition definition = beanFactory.getBeanDefinition(beanName);
			String className = definition.getBeanClassName();
			if (!definition.isAbstract() && !StringUtils.isEmpty(className)) {
				Class<?> beanClass = CoreClassUtils.getClass(className);
				if (AnnotationUtils.isAnnotationPresent(beanClass, Service.class) || AnnotationUtils.isAnnotationPresent(beanClass, Controller.class)) {
					String methodName = "save" + tableName;
					List<Method> methods = MethodUtils.getAllDeclaredMethods(beanClass)
							.stream()
							.filter(m -> m.getName().equals(methodName))
							.collect(Collectors.toList());
					if (!CollectionUtils.isEmpty(methods) && methods.size() == 1) {
						return new BusinessCompanyMergeAction(beanName, methodName, BusinessCompanyMergeAction.ActionTypes.SERVICE_BEAN);
					}
				}
			}
		}
		return null;
	}


	/**
	 * System column for the Business Company Table.
	 */
	private SystemColumn getBusinessCompanySystemColumn() {
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setTableName("BusinessCompany");
		searchForm.setBeanPropertyNameEquals("id");
		searchForm.setStandardOnly(true);
		List<SystemColumnStandard> primaryKeyColumns = getSystemColumnService().getSystemColumnList(searchForm);
		AssertUtils.assertNotNull(primaryKeyColumns, "Could not find Business Company System Column.");
		AssertUtils.assertTrue(primaryKeyColumns.size() == 1, "Expected only one Business Company Primary Key System Column");
		return primaryKeyColumns.get(0);
	}


	/**
	 * Determine the entity property name from the DB column name given in the system used by entity label.
	 */
	private String getSoftLinkPropertyName(SystemUsedByEntity systemUsedByEntity, IdentityObject sourceEntity) {
		String[] fieldParts = systemUsedByEntity.getLabel().split("\\.");
		AssertUtils.assertTrue(fieldParts.length == 2, "Format of the Soft Link Label should be tableName.FieldName but was " + systemUsedByEntity.getLabel());
		AssertUtils.assertTrue(StringUtils.isEqual(systemUsedByEntity.getColumn().getTable().getName(), fieldParts[0]),
				"Format of the Soft Link Label should be tableName.FieldName but was " + systemUsedByEntity.getLabel());

		ReadOnlyDAO<T> dao = getDaoLocator().locate(systemUsedByEntity.getColumn().getTable().getName());
		String results = fieldParts[1];
		Field[] fields = ClassUtils.getClassFields(dao.getConfiguration().getBeanClass(), true, true);
		for (Field field : fields) {
			if (AnnotationUtils.isAnnotationPresent(field, SoftLinkField.class)) {
				if (StringUtils.isEqual(dao.getConfiguration().getDBFieldName(field.getName()), results)) {
					results = field.getName();
					break;
				}
			}
		}
		AssertUtils.assertTrue(BeanUtils.isPropertyPresent(sourceEntity.getClass(), results), "Soft Link Entity expects property name " + fieldParts[1]);

		return results;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<BusinessCompanyMergeAction.ActionTypes, BusinessCompanyMergeAction> generateEmptyActionsMap() {
		return new EnumMap<>(BusinessCompanyMergeAction.ActionTypes.class);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
