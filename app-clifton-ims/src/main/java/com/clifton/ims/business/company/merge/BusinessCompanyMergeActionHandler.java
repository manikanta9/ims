package com.clifton.ims.business.company.merge;

import com.clifton.business.company.BusinessCompany;
import com.clifton.system.usedby.SystemUsedByEntity;

import java.util.List;


/**
 * <code>BusinessCompanyMergeActionHandler</code> determines the {@link BusinessCompanyMergeAction} based on the provided {@link SystemUsedByEntity} and provides
 * a mechanism for executing the merge action.
 */
public interface BusinessCompanyMergeActionHandler {

	/**
	 * Merge all source business company into the target for all used by entities.  Any black-listed tables affected by the merge will result in an exception and rollback.
	 */
	public void processSystemUsedByEntities(List<SystemUsedByEntity> sourceSystemUsedByEntities, BusinessCompany sourceBusinessCompany, BusinessCompany targetBusinessCompany, boolean deleteBusinessCompanyAfterMerge);
}
