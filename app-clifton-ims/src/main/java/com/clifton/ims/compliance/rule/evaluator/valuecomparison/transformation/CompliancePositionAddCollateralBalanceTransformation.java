package com.clifton.ims.compliance.rule.evaluator.valuecomparison.transformation;

import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.position.AccountingPosition;
import com.clifton.accounting.gl.valuation.AccountingValuationService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.compliance.rule.evaluator.ComplianceRuleEvaluationConfig;
import com.clifton.compliance.rule.evaluator.position.CompliancePositionValueHolder;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.BaseComplianceRuleAccountingPositionValueCalculator;
import com.clifton.compliance.rule.evaluator.valuecomparison.calculator.BaseComplianceRuleValueCalculator;
import com.clifton.compliance.rule.evaluator.valuecomparison.transformation.CompliancePositionTransformation;
import com.clifton.compliance.run.rule.detail.ComplianceRuleRunSubDetail;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.context.Context;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The {@link CompliancePositionTransformation} bean type for applying net collateral balances to {@link CompliancePositionValueHolder} objects.
 * <p>
 * This bean type is used to find and apply all net collateral balances which are related to each {@link CompliancePositionValueHolder} in the current state of the {@link
 * BaseComplianceRuleAccountingPositionValueCalculator calculator} evaluation.
 * <p>
 * During its transformation step, beans of this type will determine all net collateral balances related to the client account under evaluation. It will then add all associated net
 * collateral balances to each {@link CompliancePositionValueHolder}. Value holders are said to be associated with a collateral balance when they contain at least one position
 * which has as its holding account the account against which the collateral has been posted.
 * <p>
 * If collateral balances exist for holding accounts which have no associated positions in the current list then they can be added to the result via the {@link
 * #includeCollateralForUnmatchedAccounts} toggle.
 *
 * @author MikeH
 */
public class CompliancePositionAddCollateralBalanceTransformation implements CompliancePositionTransformation {

	/**
	 * If {@code true}, include collateral from unmatched accounts as "dummy" {@link CompliancePositionValueHolder position value holders}. These collateral balances will be
	 * grouped and summed by ultimate issuer.
	 */
	private boolean includeCollateralForUnmatchedAccounts;
	/**
	 * The {@link CollateralType} ID to use when querying for collateral for the account.
	 */
	private short collateralTypeId;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private AccountingAccountService accountingAccountService;
	private AccountingValuationService accountingValuationService;
	private CollateralService collateralService;
	private CollateralBalanceService collateralBalanceService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<CompliancePositionValueHolder<AccountingPosition>> apply(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Date processDate, Context context, List<ComplianceRuleRunSubDetail> runSubDetailList) {
		// Get net collateral values
		Map<InvestmentAccount, BigDecimal> netCollateralByHoldingAccount = getNetCollateralByHoldingAccount(getClientAccount(context), processDate);

		// Get value holders with net collateral applied
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderCollateralList = getValueHolderWithCollateralList(positionValueList, netCollateralByHoldingAccount);
		List<CompliancePositionValueHolder<AccountingPosition>> valueHolderCollateralUnmatchedList = getValueHolderWithCollateralListForUnmatchedAccounts(positionValueList, netCollateralByHoldingAccount);

		return CollectionUtils.combineCollections(valueHolderCollateralList, valueHolderCollateralUnmatchedList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the map of holding accounts to net collateral values for the given client account.
	 */
	private Map<InvestmentAccount, BigDecimal> getNetCollateralByHoldingAccount(InvestmentAccount clientAccount, Date balanceDate) {
		// Get and group net collateral by holding account
		return CollectionUtils.getStream(getCollateralBalanceListForClient(clientAccount, balanceDate))
				.collect(Collectors.toMap(CollateralBalance::getHoldingInvestmentAccount, balance -> getNetCollateralForClientAccount(balance, clientAccount, balanceDate)));
	}


	/**
	 * Gets the list of {@link CompliancePositionValueHolder} entities using the provided {@code positionValueList} with collateral balances applied.
	 * <p>
	 * This method will apply a net collateral balance to each position value holder for each holding account which is associated with at least one position in that position value
	 * holder.
	 */
	private List<CompliancePositionValueHolder<AccountingPosition>> getValueHolderWithCollateralList(List<CompliancePositionValueHolder<AccountingPosition>> positionValueList, Map<InvestmentAccount, BigDecimal> netCollateralByHoldingAccount) {
		// Aggregate collateral to existing position value holders
		return CollectionUtils.getConverted(positionValueList, valueHolder -> {
			// Index all holding accounts for the value holder
			Collection<InvestmentAccount> positionAccountSet = CollectionUtils.getStream(valueHolder.getPositionList())
					.map(AccountingPosition::getHoldingInvestmentAccount)
					.collect(Collectors.toSet());

			// Aggregate all collateral values which apply to the value holder
			BigDecimal aggregatedValueWithCollateral = CollectionUtils.getStream(netCollateralByHoldingAccount.entrySet())
					.filter(accountToCollateralEntry -> positionAccountSet.contains(accountToCollateralEntry.getKey()))
					.map(Map.Entry::getValue)
					.reduce(valueHolder.getValue(), MathUtils::add);

			return new CompliancePositionValueHolder<>(aggregatedValueWithCollateral, valueHolder.getPositionList());
		});
	}


	/**
	 * Gets the list of {@link CompliancePositionValueHolder} entities for holding accounts in {@code netCollateralByHoldingAccount} that are not matched in the provided {@code
	 * existingPositionValueList}.
	 * <p>
	 * This method will generate a unique {@link CompliancePositionValueHolder} for each {@link BusinessCompany#getRootParent() ultimate issuer}. This will be a "dummy" value
	 * holder (i.e., it will contain no positions) and its value will be the aggregate of the net collateral for all included holding accounts under that issuer.
	 */
	private List<CompliancePositionValueHolder<AccountingPosition>> getValueHolderWithCollateralListForUnmatchedAccounts(List<CompliancePositionValueHolder<AccountingPosition>> existingPositionValueList, Map<InvestmentAccount, BigDecimal> netCollateralByHoldingAccount) {
		// Guard-clause: Do not attempt to process unmatched accounts if disabled
		if (!isIncludeCollateralForUnmatchedAccounts()) {
			return Collections.emptyList();
		}

		// Get all holding accounts for existing position value holders
		Set<InvestmentAccount> positionAccountSet = CollectionUtils.getStream(existingPositionValueList)
				.flatMap(valueList -> valueList.getPositionList().stream())
				.map(AccountingPosition::getHoldingInvestmentAccount)
				.collect(Collectors.toSet());

		// Aggregate collateral by ultimate issuer for unmatched accounts
		Map<BusinessCompany, BigDecimal> remainingIssuerToNetCollateral = CollectionUtils.getStream(netCollateralByHoldingAccount.keySet())
				// Filter to unmatched accounts
				.filter(account -> !positionAccountSet.contains(account))
				// Group by ultimate issuer and aggregate
				.collect(Collectors.groupingBy(account -> account.getIssuingCompany().getRootParent(),
						Collectors.reducing(BigDecimal.ZERO, netCollateralByHoldingAccount::get, MathUtils::add)));

		// Convert collateral values to position holders
		return CollectionUtils.getConverted(remainingIssuerToNetCollateral.values(), netCollateral -> new CompliancePositionValueHolder<>(netCollateral, Collections.emptyList()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Extracts the client account out of the given context.
	 */
	private InvestmentAccount getClientAccount(Context context) {
		ComplianceRuleEvaluationConfig ruleEvaluationConfig = (ComplianceRuleEvaluationConfig) context.getBean(BaseComplianceRuleValueCalculator.CONTEXT_RULE_CONFIG);
		InvestmentAccount account = ruleEvaluationConfig.retrieveClientAccount();
		Objects.requireNonNull(account);
		return account;
	}


	/**
	 * Retrieves the list of {@link CollateralBalance} values for the client on the given date.
	 */
	private List<CollateralBalance> getCollateralBalanceListForClient(InvestmentAccount clientAccount, Date balanceDate) {
		// Get relevant related accounts
		List<InvestmentAccountRelationship> relatedInvestmentAccountList = getInvestmentAccountRelationshipService().getRelatedInvestmentAccountList(clientAccount.getId(), true);
		Integer[] relatedAccountIds = CollectionUtils.getStream(relatedInvestmentAccountList)
				.map(ManyToManyEntity::getReferenceTwo)
				.filter(account -> account.getType().isOtc())
				.map(BaseSimpleEntity::getId)
				.toArray(Integer[]::new);

		// Short-circuit if no related accounts are found
		if (ArrayUtils.isEmpty(relatedAccountIds)) {
			return Collections.emptyList();
		}

		// Get collateral balances for associated holding accounts
		CollateralBalanceSearchForm collateralBalanceSearchForm = new CollateralBalanceSearchForm();
		collateralBalanceSearchForm.setBusinessClientId(clientAccount.getBusinessClient().getId());
		collateralBalanceSearchForm.setBalanceDate(balanceDate);
		collateralBalanceSearchForm.setParentIsNull(true);
		collateralBalanceSearchForm.setCollateralTypeId(getCollateralTypeId());
		collateralBalanceSearchForm.setHoldingInvestmentAccountIds(relatedAccountIds);
		return getCollateralBalanceService().getCollateralBalanceList(collateralBalanceSearchForm);
	}


	/**
	 * Gets the net collateral value from the given {@link CollateralBalance}.
	 * <p>
	 * This value is converted to the base currency for the provided client account for the given conversion date.
	 */
	private BigDecimal getNetCollateralForClientAccount(CollateralBalance balanceValue, InvestmentAccount clientAccount, Date currencyConversionDate) {
		MathUtils.subtract(balanceValue.getEndingPostedCollateralValue(), balanceValue.getEndingPostedCounterpartyCollateralValue());
		BigDecimal collateralBalance = MathUtils.subtract(balanceValue.getEndingPostedCollateralValue(), balanceValue.getEndingPostedCounterpartyCollateralValue());

		// Get exchange rate
		InvestmentSecurity localCurrency = balanceValue.getHoldingInvestmentAccount().getBaseCurrency();
		InvestmentSecurity baseCurrency = clientAccount.getBaseCurrency();
		BigDecimal fxRate = BigDecimal.ONE;
		if (!CompareUtils.isEqual(localCurrency, baseCurrency)) {
			fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forClientAccount(clientAccount.toClientAccount(), localCurrency.getSymbol(), baseCurrency.getSymbol(), currencyConversionDate).flexibleLookup());
		}

		// Apply exchange rate
		return MathUtils.multiply(collateralBalance, fxRate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isIncludeCollateralForUnmatchedAccounts() {
		return this.includeCollateralForUnmatchedAccounts;
	}


	public void setIncludeCollateralForUnmatchedAccounts(boolean includeCollateralForUnmatchedAccounts) {
		this.includeCollateralForUnmatchedAccounts = includeCollateralForUnmatchedAccounts;
	}


	public short getCollateralTypeId() {
		return this.collateralTypeId;
	}


	public void setCollateralTypeId(short collateralTypeId) {
		this.collateralTypeId = collateralTypeId;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingValuationService getAccountingValuationService() {
		return this.accountingValuationService;
	}


	public void setAccountingValuationService(AccountingValuationService accountingValuationService) {
		this.accountingValuationService = accountingValuationService;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
