package com.clifton.ims.rule.reconcile.position;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.portfolio.run.BasePortfolioRunExposureSummary;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.rule.PortfolioRunRuleEvaluatorContext;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionDefinition;
import com.clifton.reconcile.position.ReconcilePositionService;
import com.clifton.reconcile.position.search.ReconcilePositionSearchForm;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * The<code>PortfolioRunUnreconciledSecurityPositionQuantityRuleEvaluator</code> class is a code-based RuleEvaluator.
 * It generates violations during a PortfolioRun for accounts with unreconciled security position quantities.
 *
 * @author sfahey
 */
public class PortfolioRunUnreconciledSecurityPositionQuantityRuleEvaluator extends BaseRuleEvaluator<PortfolioRun, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication>> {

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private ReconcilePositionService reconcilePositionService;


	@Override
	public List<RuleViolation> evaluateRule(PortfolioRun run, RuleConfig ruleConfig, PortfolioRunRuleEvaluatorContext<BasePortfolioRunExposureSummary, BasePortfolioRunReplication> context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			Map<Short, String> reconcilePositionDefinitionPurposes = new HashMap<>();
			//Get all reconcilePositionDefinitions for use in filtering later
			for (ReconcilePositionDefinition reconcilePositionDefinition : getReconcilePositionService().getReconcilePositionDefinitionList()) {
				reconcilePositionDefinitionPurposes.put(reconcilePositionDefinition.getAccountRelationshipPurpose().getId(), reconcilePositionDefinition.getAccountRelationshipPurpose().getName());
			}
			for (InvestmentAccount clientAccount : context.getClientAccountList(run)) {
				for (ReconcilePosition reconcilePosition : CollectionUtils.getIterable(getReconcilePositions(clientAccount, context.getBalanceDate(run), reconcilePositionDefinitionPurposes))) {
					Map<String, Object> templateValues = new HashMap<>();
					templateValues.put("ourQuantity", reconcilePosition.getOurQuantity());
					templateValues.put("externalQuantity", reconcilePosition.getExternalQuantity());
					ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, run.getId(), reconcilePosition.getId(), null, templateValues));
				}
			}
		}
		return ruleViolationList;
	}


	private List<ReconcilePosition> getReconcilePositions(InvestmentAccount clientAccount, Date balanceDate, Map<Short, String> reconcilePositionDefinitionPurposes) {
		//get holdingAccountIds for holdingAccounts with a relationship purpose that begins with 'Trading' (e.g. Trading Futures)
		List<Integer> holdingAccountIds = new ArrayList<>();
		for (Map.Entry<Short, String> shortStringEntry : reconcilePositionDefinitionPurposes.entrySet()) {
			String purpose = shortStringEntry.getValue();
			if (purpose != null && purpose.startsWith("Trading")) {
				for (InvestmentAccountRelationship investmentAccountRelationship :
						CollectionUtils.getIterable(getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurpose(clientAccount.getId(), purpose, balanceDate, true))) {
					holdingAccountIds.add(investmentAccountRelationship.getReferenceTwo().getId());
				}
			}
		}
		//return unreconciled positions for applicable holding accounts.
		ReconcilePositionSearchForm reconcilePositionSearchForm = new ReconcilePositionSearchForm();
		reconcilePositionSearchForm.setClientAccountId(clientAccount.getId());
		reconcilePositionSearchForm.setHoldingAccountIds(ArrayUtils.toIntegerArray(holdingAccountIds.toArray()));
		reconcilePositionSearchForm.setPositionDate(balanceDate);
		reconcilePositionSearchForm.setQuantityReconciled(false);
		return getReconcilePositionService().getReconcilePositionList(reconcilePositionSearchForm);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public ReconcilePositionService getReconcilePositionService() {
		return this.reconcilePositionService;
	}


	public void setReconcilePositionService(ReconcilePositionService reconcilePositionService) {
		this.reconcilePositionService = reconcilePositionService;
	}
}
