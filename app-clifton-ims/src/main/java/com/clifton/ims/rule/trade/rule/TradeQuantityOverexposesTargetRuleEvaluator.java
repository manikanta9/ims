package com.clifton.ims.rule.trade.rule;

import com.clifton.business.service.BusinessServiceService;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.BasePortfolioRunReplication;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.portfolio.target.PortfolioTargetService;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.trade.Trade;
import com.clifton.trade.rule.TradeRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * This rule evaluator determines when a trade pushes any replications for the associated client account over their target by more than the specified threshold.
 * Currently this rule only supports trades for client accounts that use Portfolio Runs, i.e. Portfolio Target, Overlay, LDI
 * <p>
 * Application should be be filtered to client accounts with a certain Business Service Processing Type and trades for a specific Investment Type
 * Can determine if exposure exceeds threshold using currency, the security's given units (contracts, shares, etc.), or as a percentage of target exposure.
 * <p>
 * Implementation logic differs based on client account service processing type.
 *
 * @author mitchellf
 */
public class TradeQuantityOverexposesTargetRuleEvaluator<D extends PortfolioRunTradeDetail> extends BaseRuleEvaluator<Trade, TradeRuleEvaluatorContext> implements ValidationAware {

	private PortfolioTargetService portfolioTargetService;
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private InvestmentInstrumentService investmentInstrumentService;
	private PortfolioRunService portfolioRunService;
	private PortfolioTargetRunService portfolioTargetRunService;
	private PortfolioRunTradeService<PortfolioRunTrade<D>, D> portfolioRunTradeService;
	private BusinessServiceService businessServiceService;
	private InvestmentSetupService investmentSetupService;
	private InvestmentReplicationService investmentReplicationService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private BigDecimal targetOverexposureThreshold;
	private Short serviceProcessingTypeId;
	private Short investmentTypeId;
	private ExposureMethods targetOverexposureMethod;


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getTargetOverexposureThreshold(), "Target overexposure threshold must be defined");
		ValidationUtils.assertNotNull(getServiceProcessingTypeId(), "Service processing type must be defined");
		ValidationUtils.assertNotNull(getTargetOverexposureMethod(), "Target overexposure method must be defined");
		ValidationUtils.assertNotNull(getInvestmentTypeId(), "Investment Type must be defined");
		if (getBusinessServiceService().getBusinessServiceProcessingType(getServiceProcessingTypeId()).getProcessingType() == ServiceProcessingTypes.LDI) {
			ValidationUtils.assertFalse(getTargetOverexposureMethod() == ExposureMethods.CURRENCY, "For LDI processing type, CURRENCY exposure method is not supported. You must select UNITS or PERCENTAGE.");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	public enum ExposureMethods {
		UNITS,
		CURRENCY,
		PERCENTAGE
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(Trade trade, RuleConfig ruleConfig, TradeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		// if investmentTypeName is not populated, rule will be applied to all securities
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), getInvestmentSetupService().getInvestmentType(getInvestmentTypeId()).getName())
				&& trade.getClientInvestmentAccount().getServiceProcessingType().getId().equals(getServiceProcessingTypeId())) {
			PortfolioRunTrade<D> runTrade = getPortfolioRunTrade(trade);
			if (runTrade != null) {
				ServiceProcessingTypes processingType = getBusinessServiceService().getBusinessServiceProcessingType(getServiceProcessingTypeId()).getProcessingType();
				List<D> detailList = runTrade.getDetailList();
				List<D> filteredDetailList = detailList.stream().filter(detail -> {
					if (detail.getSecurity().equals(trade.getInvestmentSecurity())) {
						return true;
					}
					else {
						return getInvestmentReplicationService().isInvestmentSecurityInReplication(detail.getReplication(), trade.getInvestmentSecurity());
					}
				}).collect(Collectors.toList());

				if (!CollectionUtils.isEmpty(filteredDetailList)) {
					detailList = filteredDetailList;
				}
				switch (processingType) {
					case PORTFOLIO_TARGET:
					case PORTFOLIO_RUNS:
						detailList.forEach(detail -> processDetailForPortfolioRuns(detail, ruleConfig, ruleViolationList, trade));
						break;
					case LDI:
						detailList.forEach(detail -> processDetailForLDI(detail, ruleConfig, ruleViolationList, trade));
						break;
					default:
						break;
				}
			}
		}
		return ruleViolationList;
	}


	@SuppressWarnings("unchecked")
	private PortfolioRunTrade<D> getPortfolioRunTrade(Trade trade) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRunByMainRun(trade.getClientInvestmentAccount().getId(), trade.getTradeDate());
		if (run != null) {
			return getPortfolioRunTradeService().getPortfolioRunTrade(run.getId());
		}
		return null;
	}


	private void processDetailForPortfolioRuns(PortfolioRunTradeDetail detail, RuleConfig ruleConfig, List<RuleViolation> ruleViolationList, Trade trade) {
		// If overlay or portfolio targets, we know detail is instance of type
		BasePortfolioRunReplication rep = (BasePortfolioRunReplication) detail;
		BigDecimal diff = null;
		String type = "";
		switch (getTargetOverexposureMethod()) {
			case UNITS:
				diff = MathUtils.subtract(MathUtils.add(rep.getActualContractsAdjusted(), rep.getPendingContractsAdjusted()), rep.getTargetContractsAdjusted());
				type = " contracts";
				break;
			case CURRENCY:
				diff = MathUtils.subtract(MathUtils.add(rep.getActualExposureAdjusted(), rep.getPendingExposureAdjusted()), rep.getTargetExposureAdjusted());
				type = " exposure";
				break;
			case PERCENTAGE:
				diff = MathUtils.divide(MathUtils.subtract(MathUtils.add(rep.getActualContractsAdjusted(), rep.getPendingContractsAdjusted()), rep.getTargetContractsAdjusted()), rep.getTargetContractsAdjusted());
				break;
		}
		if (MathUtils.isGreaterThan(diff, getTargetOverexposureThreshold())) {
			ruleViolationList.add(generateViolationForPortfolioRuns(ruleConfig, type, rep.getLabel(), trade));
		}
	}


	private void processDetailForLDI(PortfolioRunTradeDetail detail, RuleConfig ruleConfig, List<RuleViolation> ruleViolationList, Trade trade) {
		PortfolioRunLDIExposure exposure = (PortfolioRunLDIExposure) detail;
		BigDecimal diff = null;
		String type = "";
		if (getTargetOverexposureMethod() == ExposureMethods.UNITS) {
			diff = MathUtils.subtract(MathUtils.add(exposure.getCurrentContracts(), exposure.getPendingContracts()), exposure.getTargetContracts());
			type = " contracts";
		}
		else {
			// Currency is not supported for LDI, so if not units, we know it's percentage.
			diff = MathUtils.divide(MathUtils.subtract(MathUtils.add(exposure.getCurrentContracts(), exposure.getPendingContracts()), exposure.getTargetContracts()), exposure.getTargetContracts());
		}
		if (MathUtils.isGreaterThan(diff, getTargetOverexposureThreshold())) {
			ruleViolationList.add(generateViolationForPortfolioRuns(ruleConfig, type, exposure.getLabel(), trade));
		}
	}


	private RuleViolation generateViolationForPortfolioRuns(RuleConfig ruleConfig, String type, String label, Trade trade) {
		EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(trade));
		Map<String, Object> templateValues = new HashMap<>();
		StringBuilder violationNote = new StringBuilder();
		violationNote.append("Trade causes replication ")
				.append(label)
				.append(" to deviate from target")
				.append(type)
				.append(" by ")
				.append(getTargetOverexposureThreshold());
		if (getTargetOverexposureMethod() == ExposureMethods.PERCENTAGE) {
			violationNote.append("%");
		}
		templateValues.put("violationNote", violationNote.toString());
		return getRuleViolationService().createRuleViolation(entityConfig, BeanUtils.getIdentityAsLong(trade), templateValues);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortfolioTargetService getPortfolioTargetService() {
		return this.portfolioTargetService;
	}


	public void setPortfolioTargetService(PortfolioTargetService portfolioTargetService) {
		this.portfolioTargetService = portfolioTargetService;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}


	public PortfolioRunTradeService<PortfolioRunTrade<D>, D> getPortfolioRunTradeService() {
		return this.portfolioRunTradeService;
	}


	public void setPortfolioRunTradeService(PortfolioRunTradeService<PortfolioRunTrade<D>, D> portfolioRunTradeService) {
		this.portfolioRunTradeService = portfolioRunTradeService;
	}


	public BusinessServiceService getBusinessServiceService() {
		return this.businessServiceService;
	}


	public void setBusinessServiceService(BusinessServiceService businessServiceService) {
		this.businessServiceService = businessServiceService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public BigDecimal getTargetOverexposureThreshold() {
		return this.targetOverexposureThreshold;
	}


	public void setTargetOverexposureThreshold(BigDecimal targetOverexposureThreshold) {
		this.targetOverexposureThreshold = targetOverexposureThreshold;
	}


	public Short getServiceProcessingTypeId() {
		return this.serviceProcessingTypeId;
	}


	public void setServiceProcessingTypeId(Short serviceProcessingTypeId) {
		this.serviceProcessingTypeId = serviceProcessingTypeId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public ExposureMethods getTargetOverexposureMethod() {
		return this.targetOverexposureMethod;
	}


	public void setTargetOverexposureMethod(ExposureMethods targetOverexposureMethod) {
		this.targetOverexposureMethod = targetOverexposureMethod;
	}
}
