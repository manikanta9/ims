package com.clifton.ims.integration.business.company;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyParentRelationshipType;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.address.BusinessCompanyAddressConverter;
import com.clifton.business.company.address.BusinessCompanyAddressConverterLocator;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.integration.incoming.business.IntegrationBusinessCompany;
import com.clifton.integration.incoming.business.IntegrationBusinessCompanyService;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.stereotype.Service;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * @see IntegrationBusinessCompanyMappingService
 */
@Service
public class IntegrationBusinessCompanyMappingServiceImpl<T extends IdentityObject> implements IntegrationBusinessCompanyMappingService {

	private static final Pattern entireAlternativeNameRegex = Pattern.compile("^;2;(?<count>[1-9][0-9]?);1;(?<names>(1;.*;)*)$");
	private static final Pattern individualAlternativeNameRegex = Pattern.compile("1;(?<name>.*?);");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PropertySetter populatePropertyAlways = (PropertyMapping p, Object sourceBean, Object targetBean, @SuppressWarnings("unused") Status status) -> {
		Object value = BeanUtils.getPropertyValue(sourceBean, p.sourcePropertyName);
		if (value instanceof String && StringUtils.isEmpty((String) value)) {
			value = null;
		}
		BeanUtils.setPropertyValue(targetBean, p.targetPropertyName, value);
	};

	private PropertySetter populatePropertyNonEmptyString = (PropertyMapping p, Object sourceBean, Object targetBean, @SuppressWarnings("unused") Status status) -> {
		AssertUtils.assertTrue(BeanUtils.getPropertyType(sourceBean, p.sourcePropertyName).equals(String.class), "Integration Company property type must be a string.");
		AssertUtils.assertTrue(BeanUtils.getPropertyType(targetBean, p.targetPropertyName).equals(String.class), "Business Company property type must be a string.");
		Object value = BeanUtils.getPropertyValue(sourceBean, p.sourcePropertyName);
		if (value instanceof String && !StringUtils.isEmpty((String) value)) {
			BeanUtils.setPropertyValue(targetBean, p.targetPropertyName, value);
		}
	};

	private PropertySetter populateParentBusinessCompany = (@SuppressWarnings("unused") PropertyMapping p, Object sourceBean, Object targetBean, Status status) -> {
		AssertUtils.assertTrue(sourceBean instanceof IntegrationBusinessCompany, "Source must be an Integration Business Company");
		AssertUtils.assertTrue(targetBean instanceof BusinessCompany, "Target must be a Business Company.");
		IntegrationBusinessCompany integrationBusinessCompany = (IntegrationBusinessCompany) sourceBean;
		BusinessCompany businessCompany = (BusinessCompany) targetBean;

		Integer parentBloombergCompanyId = integrationBusinessCompany.getParentBloombergCompanyIdentifier();
		if (parentBloombergCompanyId != null && parentBloombergCompanyId > 0) {
			// [BloombergCompanyIdentifier] is NOT unique in the Business Company table, we have many duplicates.
			List<BusinessCompany> parentBusinessCompanyList = getBusinessCompanyService().getBusinessCompanyByBloombergCompanyIdentifier(parentBloombergCompanyId);
			if (parentBusinessCompanyList != null && parentBusinessCompanyList.size() == 1) {
				businessCompany.setParent(parentBusinessCompanyList.get(0));
			}
			else if (parentBusinessCompanyList != null && parentBusinessCompanyList.size() > 1) {
				List<Integer> duplicates = parentBusinessCompanyList.stream().map(BusinessCompany::getId).collect(Collectors.toList());
				status.addError(String.format("Many Parent Business Companies %s have the same Bloomberg Identifier %s", duplicates, parentBloombergCompanyId));
			}
			else {
				IntegrationBusinessCompany parentBusinessCompany = getIntegrationBusinessCompanyService().getIntegrationBusinessCompanyByBloombergCompanyId(parentBloombergCompanyId);
				String longCompanyName = parentBusinessCompany == null ? "Unknown" : parentBusinessCompany.getLongCompanyName();
				status.addError(String.format("Could not find Parent Business Company with Bloomberg ID %s and Long Name %s", parentBloombergCompanyId, longCompanyName));
			}
		}
		else {
			businessCompany.setParent(null);
		}
	};

	private PropertySetter populateObligorBusinessCompany = (@SuppressWarnings("unused") PropertyMapping p, Object sourceBean, Object targetBean, Status status) -> {
		AssertUtils.assertTrue(sourceBean instanceof IntegrationBusinessCompany, "Source must be an Integration Business Company");
		AssertUtils.assertTrue(targetBean instanceof BusinessCompany, "Target must be a Business Company.");
		IntegrationBusinessCompany integrationBusinessCompany = (IntegrationBusinessCompany) sourceBean;
		BusinessCompany businessCompany = (BusinessCompany) targetBean;

		Integer obligorBloombergCompanyIdentifier = integrationBusinessCompany.getObligorBloombergCompanyIdentifier();
		if (obligorBloombergCompanyIdentifier != null && obligorBloombergCompanyIdentifier > 0) {
			// [BloombergCompanyIdentifier] is NOT unique in the Business Company table, we have many duplicates.
			List<BusinessCompany> obligorBusinessCompanyList = getBusinessCompanyService().getBusinessCompanyByBloombergCompanyIdentifier(obligorBloombergCompanyIdentifier);
			if (obligorBusinessCompanyList != null && obligorBusinessCompanyList.size() == 1) {
				businessCompany.setObligorBusinessCompany(obligorBusinessCompanyList.get(0));
			}
			else if (obligorBusinessCompanyList != null && obligorBusinessCompanyList.size() > 1) {
				List<Integer> duplicates = obligorBusinessCompanyList.stream().map(BusinessCompany::getId).collect(Collectors.toList());
				status.addError(String.format("Many Obligor Business Companies %s have the same Bloomberg Identifier %s", duplicates, obligorBloombergCompanyIdentifier));
			}
			else {
				IntegrationBusinessCompany obligorBusinessCompany = getIntegrationBusinessCompanyService().getIntegrationBusinessCompanyByBloombergCompanyId(obligorBloombergCompanyIdentifier);
				String longCompanyName = obligorBusinessCompany == null ? "Unknown" : obligorBusinessCompany.getLongCompanyName();
				status.addError(String.format("Could not find Obligor Business Company with Bloomberg ID %s and Long Name %s", obligorBloombergCompanyIdentifier, longCompanyName));
			}
		}
		else {
			businessCompany.setObligorBusinessCompany(null);
		}
	};

	private PropertySetter populateAlternativeCompanyName = (@SuppressWarnings("unused") PropertyMapping p, Object sourceBean, Object targetBean, @SuppressWarnings("unused") Status status) -> {
		AssertUtils.assertTrue(sourceBean instanceof IntegrationBusinessCompany, "Source must be an Integration Business Company");
		AssertUtils.assertTrue(targetBean instanceof BusinessCompany, "Target must be a Business Company.");
		IntegrationBusinessCompany integrationBusinessCompany = (IntegrationBusinessCompany) sourceBean;
		BusinessCompany businessCompany = (BusinessCompany) targetBean;

		String alternativeCompanyName = integrationBusinessCompany.getAlternateCompanyName();
		if (!StringUtils.isEmpty(alternativeCompanyName)) {
			Matcher matcher = entireAlternativeNameRegex.matcher(alternativeCompanyName);
			if (matcher.matches()) {
				String namesPortion = matcher.group("names");
				matcher = individualAlternativeNameRegex.matcher(namesPortion);
				if (matcher.matches()) {
					matcher.reset();
					List<String> names = new ArrayList<>();
					while (matcher.find()) {
						names.add(matcher.group("name"));
					}
					names = names
							.stream()
							.peek(StringUtils::trim)
							.filter(a -> !StringUtils.isEmpty(a))
							.collect(Collectors.toList());
					if (!names.isEmpty()) {
						businessCompany.setAlternateCompanyName(names.get(0));
					}
				}
			}
			else {
				businessCompany.setAlternateCompanyName(alternativeCompanyName);
			}
		}
		else {
			businessCompany.setAlternateCompanyName(null);
		}
	};

	private PropertySetter populateParentRelationshipType = (@SuppressWarnings("unused") PropertyMapping p, Object sourceBean, Object targetBean, Status status) -> {
		AssertUtils.assertTrue(sourceBean instanceof IntegrationBusinessCompany, "Source must be an Integration Business Company");
		AssertUtils.assertTrue(targetBean instanceof BusinessCompany, "Target must be a Business Company.");
		IntegrationBusinessCompany integrationBusinessCompany = (IntegrationBusinessCompany) sourceBean;
		BusinessCompany businessCompany = (BusinessCompany) targetBean;

		String companyToParentRelationship = integrationBusinessCompany.getCompanyToParentRelationship();
		if (!StringUtils.isEmpty(companyToParentRelationship)) {
			BusinessCompanyParentRelationshipType businessCompanyParentRelationshipType = getBusinessCompanyService()
					.getBusinessCompanyParentRelationshipTypeByName(companyToParentRelationship);
			if (businessCompanyParentRelationshipType != null) {
				businessCompany.setParentRelationshipType(businessCompanyParentRelationshipType);
			}
			else {
				status.addError(String.format("Could not find Parent Relationship Type for %s", companyToParentRelationship));
			}
		}
		else {
			businessCompany.setParentRelationshipType(null);
		}
	};


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// list of mapped column names for source (Integration Business Company) to target (Integration Business Company) with the action to perform the update action.
	private List<PropertyMapping> propertyMappingList = Arrays.asList(
			new PropertyMapping("companyLegalName", "companyLegalName", this.populatePropertyAlways),
			new PropertyMapping("ultimateParent", "ultimateParent", this.populatePropertyAlways),
			new PropertyMapping("privateCompany", "privateCompany", this.populatePropertyAlways),
			new PropertyMapping("acquiredByParent", "acquiredByParent", this.populatePropertyAlways),
			new PropertyMapping("countryOfIncorporation", "countryOfIncorporation", this.populatePropertyAlways),
			new PropertyMapping("countryOfRisk", "countryOfRisk", this.populatePropertyAlways),
			new PropertyMapping("stateOfIncorporation", "stateOfIncorporation", this.populatePropertyAlways),
			new PropertyMapping("companyCorporateTicker", "companyCorporateTicker", this.populatePropertyAlways),
			new PropertyMapping("legalEntityIdentifier", "legalEntityIdentifier", this.populatePropertyAlways),
			new PropertyMapping("longCompanyName", "name", this.populatePropertyAlways),
			new PropertyMapping("ultimateParentTickerExchange", "ultimateParentEquityTicker", this.populatePropertyAlways),
			new PropertyMapping("legalEntityStatus", "leiEntityStatus", this.populatePropertyAlways),
			new PropertyMapping("legalEntityAssignedDate", "leiEntityStartDate", this.populatePropertyAlways),
			new PropertyMapping("legalEntityDisabledDate", "leiEntityEndDate", this.populatePropertyAlways),
			new PropertyMapping("companyFaxNumber", "faxNumber", this.populatePropertyNonEmptyString),
			new PropertyMapping("companyTelephoneNumber", "phoneNumber", this.populatePropertyNonEmptyString),
			new PropertyMapping("companyWebAddress", "webAddress", this.populatePropertyNonEmptyString),
			new PropertyMapping("parentBloombergCompanyIdentifier", "parent", this.populateParentBusinessCompany),
			new PropertyMapping("obligorBloombergCompanyIdentifier", "obligorBusinessCompany", this.populateObligorBusinessCompany),
			new PropertyMapping("alternateCompanyName", "alternateCompanyName", this.populateAlternativeCompanyName),
			new PropertyMapping("companyToParentRelationship", "parentRelationshipType", this.populateParentRelationshipType)
	);
	// list of columns used to update the domicile address, separated from the above list for performance reasons.
	private List<PropertyMapping> addressPropertyMappingList = Arrays.asList(
			new PropertyMapping("addressLine1", "addressLine1", this.populatePropertyAlways),
			new PropertyMapping("addressLine2", "addressLine2", this.populatePropertyAlways),
			new PropertyMapping("addressLine3", "addressLine3", this.populatePropertyAlways),
			new PropertyMapping("city", "city", this.populatePropertyAlways),
			new PropertyMapping("state", "state", this.populatePropertyAlways),
			new PropertyMapping("postalCode", "postalCode", this.populatePropertyAlways),
			new PropertyMapping("country", "country", this.populatePropertyAlways)
	);
	// list of columns used to update the registration address, separated from the above list for performance reasons.
	private List<PropertyMapping> registrationAddressPropertyMappingList = Arrays.asList(
			new PropertyMapping("addressLine1", "registrationAddressLine1", this.populatePropertyAlways),
			new PropertyMapping("addressLine2", "registrationAddressLine2", this.populatePropertyAlways),
			new PropertyMapping("addressLine3", "registrationAddressLine3", this.populatePropertyAlways),
			new PropertyMapping("city", "registrationCity", this.populatePropertyAlways),
			new PropertyMapping("state", "registrationState", this.populatePropertyAlways),
			new PropertyMapping("postalCode", "registrationPostalCode", this.populatePropertyAlways),
			new PropertyMapping("country", "registrationCountry", this.populatePropertyAlways)
	);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private IntegrationBusinessCompanyService integrationBusinessCompanyService;
	private BusinessCompanyService businessCompanyService;
	private BusinessCompanyAddressConverterLocator businessCompanyAddressConverterLocator;

	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = BusinessCompany.class, permissions = SecurityPermission.PERMISSION_READ)
	@Override
	public IntegrationBusinessCompanyMappingResults getBusinessCompanyFromIntegrationBusinessCompany(int businessCompanyId) {
		return getBusinessCompanyFromIntegrationBusinessCompanyWithExclusions(businessCompanyId, Collections.emptyList());
	}


	@SecureMethod(dtoClass = BusinessCompany.class, permissions = SecurityPermission.PERMISSION_READ)
	@Override
	public IntegrationBusinessCompanyMappingResults getBusinessCompanyFromIntegrationBusinessCompanyWithExclusions(int businessCompanyId, List<Column> excludedColumnList) {
		final Status status = Status.ofMessage("Building a Business Company Entity from Integration Data.");
		boolean changed = false;

		// IMPORTANT:  Exceptions are swallowed and recorded in the status.
		final BusinessCompany businessCompany = getBusinessCompanyService().getBusinessCompany(businessCompanyId);
		if (businessCompany == null) {
			status.addError("Could not find Business Company with ID " + businessCompanyId);
		}
		else {
			Integer bloombergCompanyIdentifier = businessCompany.getBloombergCompanyIdentifier();
			if (bloombergCompanyIdentifier == null) {
				status.addError("Business Company does not have a valid Bloomberg Company Identifier.");
			}
			else {
				final IntegrationBusinessCompany integrationBusinessCompany = getIntegrationBusinessCompanyService().getIntegrationBusinessCompanyByBloombergCompanyId(bloombergCompanyIdentifier);
				if (integrationBusinessCompany != null) {
					Map<String, Object> originalValues = getBusinessCompanyMappedValues(businessCompany);
					// incoming columns must be in the list of mapped Business Company Columns.
					List<Column> mappedBusinessCompanyColumns = getBusinessCompanyMappedColumns();
					if (!mappedBusinessCompanyColumns.containsAll(excludedColumnList)) {
						List<Column> tmp = new ArrayList<>(excludedColumnList);
						tmp.removeAll(mappedBusinessCompanyColumns);
						status.addError("The excluded list includes columns that are not mapped " + tmp.stream().map(Column::getName).collect(Collectors.toList()));
					}
					// convert the incoming list of columns to property names.
					final List<String> excludedPropertyNames = CollectionUtils.asNonNullList(excludedColumnList).stream().map(Column::getBeanPropertyName).collect(Collectors.toList());
					// what properties have getters on the IntegrationBusinessCompany
					final List<String> readableSourcePropertyNames = Arrays.stream(PropertyUtils.getPropertyDescriptors(integrationBusinessCompany))
							.filter(p -> p.getReadMethod() != null)
							.map(PropertyDescriptor::getName)
							.collect(Collectors.toList());
					// what properties have setters on the BusinessCompany
					final List<String> writableTargetPropertyNames = Arrays.stream(PropertyUtils.getPropertyDescriptors(businessCompany))
							.filter(p -> p.getWriteMethod() != null)
							.map(PropertyDescriptor::getName)
							.collect(Collectors.toList());
					// filter out properties based on the incoming excluded list, make sure properties have getters and setters and populate the values.
					this.propertyMappingList.stream()
							.filter(p -> !excludedPropertyNames.contains(p.targetPropertyName))
							.filter(p -> {
								if (!readableSourcePropertyNames.contains(p.sourcePropertyName)) {
									status.addError("Mapping skipped for " + p.sourcePropertyName + " no getter property found.");
									return false;
								}
								return true;
							})
							.filter(p -> {
								if (!writableTargetPropertyNames.contains(p.targetPropertyName)) {
									status.addError("Mapping skipped for " + p.targetPropertyName + " no setter property found.");
									return false;
								}
								return true;
							})
							.forEach(p -> {
								try {
									p.populatePropertyValue(integrationBusinessCompany, businessCompany, status);
								}
								catch (Exception ex) {
									status.addError(ExceptionUtils.getDetailedMessage(ex));
								}
							});
					AddressObject addressObject = getAddressObject(integrationBusinessCompany.getCompanyAddress());
					if (addressObject != null) {
						final List<String> readableAddressPropertyNames = Arrays.stream(PropertyUtils.getPropertyDescriptors(addressObject))
								.filter(p -> p.getReadMethod() != null)
								.map(PropertyDescriptor::getName)
								.collect(Collectors.toList());
						// filter out domicile address properties based on the incoming excluded list, make sure properties have getters and setters and populate the values.
						this.addressPropertyMappingList.stream()
								.filter(p -> !excludedPropertyNames.contains(p.sourcePropertyName))
								.filter(p -> {
									if (!readableAddressPropertyNames.contains(p.sourcePropertyName)) {
										status.addError("Mapping skipped for " + p.sourcePropertyName + " no getter property found.");
										return false;
									}
									return true;
								})
								.filter(p -> {
									if (!writableTargetPropertyNames.contains(p.targetPropertyName)) {
										status.addError("Mapping skipped for " + p.targetPropertyName + " no setter property found.");
										return false;
									}
									return true;
								})
								.forEach(p -> {
									try {
										p.populatePropertyValue(addressObject, businessCompany, status);
									}
									catch (Exception ex) {
										status.addError(ExceptionUtils.getDetailedMessage(ex));
									}
								});
					}
					AddressObject registrationAddressObject = getAddressObject(integrationBusinessCompany.getLegalEntityRegistrationAddress());
					if (registrationAddressObject != null) {
						final List<String> readableAddressPropertyNames = Arrays.stream(PropertyUtils.getPropertyDescriptors(registrationAddressObject))
								.filter(p -> p.getReadMethod() != null)
								.map(PropertyDescriptor::getName)
								.collect(Collectors.toList());
						// filter out registration address properties based on the incoming excluded list, make sure properties have getters and setters and populate the values.
						this.registrationAddressPropertyMappingList.stream()
								.filter(p -> !excludedPropertyNames.contains(p.sourcePropertyName))
								.peek(p -> AssertUtils.assertTrue(readableAddressPropertyNames.contains(p.sourcePropertyName), "Business Company does not have a getter property " + p.sourcePropertyName))
								.peek(p -> AssertUtils.assertTrue(writableTargetPropertyNames.contains(p.targetPropertyName), "Business Company does not have a setter property " + p.targetPropertyName))
								.forEach(p -> {
									try {
										p.populatePropertyValue(registrationAddressObject, businessCompany, status);
									}
									catch (Exception ex) {
										status.addError(ExceptionUtils.getDetailedMessage(ex));
									}
								});
					}
					// if address object did not populate, set the domicile country from the separate country of domicile column value.
					if (!excludedPropertyNames.contains("country") && (StringUtils.isEmpty(businessCompany.getCountry()) || StringUtils.length(businessCompany.getCountry()) > 3)) {
						businessCompany.setCountry(integrationBusinessCompany.getCountryOfDomicile());
					}
					// if address object did not populate, set the domicile state from the separate state of domicile column value.
					if (!excludedPropertyNames.contains("state") && StringUtils.isEmpty(businessCompany.getState())) {
						businessCompany.setState(integrationBusinessCompany.getStateOfDomicile());
					}
					Map<String, Object> updatedValues = getBusinessCompanyMappedValues(businessCompany);
					changed = areDifferent(originalValues, updatedValues);
				}
				else {
					status.addError("Could not find Integration Business Company with Bloomberg Identifier " + bloombergCompanyIdentifier);
				}
			}
		}

		return new IntegrationBusinessCompanyMappingResults(businessCompany, status.getErrorList(), changed);
	}


	@DoNotAddRequestMapping
	@SuppressWarnings(value = "unchecked")
	@Override
	public List<Column> getBusinessCompanyMappedColumns() {
		List<PropertyMapping> propertyMappings = new ArrayList<>(this.propertyMappingList);
		propertyMappings.addAll(this.addressPropertyMappingList);
		propertyMappings.addAll(this.registrationAddressPropertyMappingList);

		DAOConfiguration<T> configuration = getDaoLocator().locate((Class<T>) BusinessCompany.class).getConfiguration();
		List<Column> allColumns = configuration.getColumnList();
		return propertyMappings.stream()
				.map(PropertyMapping::getTargetPropertyName)
				.map(property -> CollectionUtils.getOnlyElementStrict(BeanUtils.filter(allColumns, Column::getBeanPropertyName, property)))
				.collect(Collectors.toList());
	}


	@DoNotAddRequestMapping
	@SuppressWarnings(value = "unchecked")
	@Override
	public List<Column> getIntegrationBusinessCompanyMappedColumns() {
		List<PropertyMapping> propertyMappings = new ArrayList<>(this.propertyMappingList);

		DAOConfiguration<T> configuration = getDaoLocator().locate((Class<T>) IntegrationBusinessCompany.class).getConfiguration();
		List<Column> allColumns = configuration.getColumnList();
		return propertyMappings.stream()
				.map(PropertyMapping::getSourcePropertyName)
				.map(property -> CollectionUtils.getOnlyElementStrict(BeanUtils.filter(allColumns, Column::getBeanPropertyName, property)))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<String, Object> getBusinessCompanyMappedValues(BusinessCompany businessCompany) {
		List<String> mappedProperties = getBusinessCompanyMappedColumns().stream()
				.map(Column::getBeanPropertyName)
				.collect(Collectors.toList());
		Map<String, Object> allValues = BeanUtils.describe(businessCompany);
		Map<String, Object> results = new HashMap<>();
		for (Map.Entry<String, Object> entry : allValues.entrySet()) {
			if (mappedProperties.contains(entry.getKey())) {
				results.put(entry.getKey(), entry.getValue());
			}
		}
		return results;
	}


	private boolean areDifferent(Map<String, Object> first, Map<String, Object> second) {
		if (first.size() != second.size()) {
			return true;
		}
		if (!second.keySet().containsAll(first.keySet()) && !first.keySet().containsAll(second.keySet())) {
			return true;
		}
		for (Map.Entry<String, Object> entry : first.entrySet()) {
			if (!Objects.equals(entry.getValue(), second.get(entry.getKey()))) {
				return true;
			}
		}
		return false;
	}


	private AddressObject getAddressObject(String addressString) {
		if (!StringUtils.isEmpty(addressString)) {
			BusinessCompanyAddressConverter converter = getBusinessCompanyAddressConverterLocator().locate(addressString);
			if (converter != null) {
				return converter.convertAddress(addressString);
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class PropertyMapping {

		private String sourcePropertyName;
		private String targetPropertyName;
		private PropertySetter propertySetter;


		public PropertyMapping(String sourcePropertyName, String targetPropertyName, PropertySetter propertySetter) {
			this.sourcePropertyName = sourcePropertyName;
			this.targetPropertyName = targetPropertyName;
			this.propertySetter = propertySetter;
		}


		public void populatePropertyValue(Object sourceBean, Object targetBean, Status status) {
			this.propertySetter.setPropertyValue(this, sourceBean, targetBean, status);
		}


		public String getSourcePropertyName() {
			return this.sourcePropertyName;
		}


		public String getTargetPropertyName() {
			return this.targetPropertyName;
		}


		public PropertySetter getPropertySetter() {
			return this.propertySetter;
		}
	}

	@FunctionalInterface
	public interface PropertySetter {

		public void setPropertyValue(PropertyMapping propertyMapping, Object sourceBean, Object targetBean, Status status);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationBusinessCompanyService getIntegrationBusinessCompanyService() {
		return this.integrationBusinessCompanyService;
	}


	public void setIntegrationBusinessCompanyService(IntegrationBusinessCompanyService integrationBusinessCompanyService) {
		this.integrationBusinessCompanyService = integrationBusinessCompanyService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public BusinessCompanyAddressConverterLocator getBusinessCompanyAddressConverterLocator() {
		return this.businessCompanyAddressConverterLocator;
	}


	public void setBusinessCompanyAddressConverterLocator(BusinessCompanyAddressConverterLocator businessCompanyAddressConverterLocator) {
		this.businessCompanyAddressConverterLocator = businessCompanyAddressConverterLocator;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
