package com.clifton.ims.integration.event;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.Event;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.manager.IntegrationManagerPositionHistorySummary;
import com.clifton.integration.incoming.manager.IntegrationManagerService;
import com.clifton.product.manager.ManagerPositionHistorySummary;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>ProductManagerEventListener</code> loads a list of manager summary values.
 *
 * @author mwacker
 */
@Component
public class ProductManagerEventListener extends BaseEventListener<Event<Object, List<ManagerPositionHistorySummary>>> {

	private String eventName = "Integration Manager Position History Summary";
	private IntegrationManagerService integrationManagerService;


	@Override
	public void onEvent(Event<Object, List<ManagerPositionHistorySummary>> event) {
		ValidationUtils.assertNotNull(event, "Event cannot be null");

		Integer custodianId = (Integer) event.getContextValue("custodianId");
		Date balanceDate = (Date) event.getContextValue("balanceDate");

		List<ManagerPositionHistorySummary> result = new ArrayList<>();
		List<IntegrationManagerPositionHistorySummary> balances = getIntegrationManagerService().getIntegrationManagerPositionHistorySummaryList(custodianId, balanceDate);
		for (IntegrationManagerPositionHistorySummary summary : CollectionUtils.getIterable(balances)) {
			ManagerPositionHistorySummary resultSummary = new ManagerPositionHistorySummary();
			BeanUtils.copyProperties(summary, resultSummary);
			result.add(resultSummary);
		}
		event.setResult(result);
	}


	@Override
	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	public IntegrationManagerService getIntegrationManagerService() {
		return this.integrationManagerService;
	}


	public void setIntegrationManagerService(IntegrationManagerService integrationManagerService) {
		this.integrationManagerService = integrationManagerService;
	}
}
