package com.clifton.ims.integration.batch.ftp.export;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.core.util.ftp.FtpFileResult;
import com.clifton.core.util.ftp.FtpUtils;
import com.clifton.core.util.status.Status;
import com.clifton.ims.integration.batch.ftp.AbstractFtpJobBean;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>FtpExportJob</code> implements and FTP export batch job.  The job requires a system bean the implements
 * the FtpExportBean interface that is used generate the files being sent over FTP.
 *
 * @author mwacker
 */
public class FtpExportJob extends AbstractFtpJobBean {

	/**
	 * The target folder on the server where the files go.
	 */
	private String targetServerFolder;


	@Override
	public Status run(Map<String, Object> context) {
		Object obj = getSystemBeanService().getBeanInstance(getSystemBean());
		if (obj instanceof FtpExportBean) {
			FtpExportBean exportJob = (FtpExportBean) obj;
			FtpConfig ftpConfig = new FtpConfig(getUrl(), getPort(), getFtpProtocol(), getUsername(), getPasswordSecret() != null ? decryptSecuritySecret(getPasswordSecret()).getSecretString() : null);
			ftpConfig.setSshPrivateKey(getSftpPrivateKeySecret() != null ? decryptSecuritySecret(getSftpPrivateKeySecret()).getSecretString() : null);
			ftpConfig.setSshPublicKey(getSftpPublicKey());

			Date reportingDate = getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());

			Status status = Status.ofMessage("Empty file list in FTP export to: " + getUrl());
			List<FileWrapper> fileList = exportJob.getExportFileList(reportingDate, status);
			if (!CollectionUtils.isEmpty(fileList)) {
				List<FtpFileResult> fileResult = FtpUtils.put(ftpConfig, getTargetServerFolder(), fileList);

				for (FtpFileResult result : CollectionUtils.getIterable(fileResult)) {
					exportJob.handleFileResult(result, status);
				}

				List<FtpFileResult> filesWithoutErrors = BeanUtils.filter(fileResult, FtpFileResult::isSuccess);

				status.setMessage("[" + CollectionUtils.getSize(filesWithoutErrors) + "] files were sent via FTP to [" + getUrl() + "]");
			}
			return status;
		}
		else {
			Class<?> clazz = obj != null ? obj.getClass() : null;
			throw new RuntimeException("Cannot run an ftp export job for bean of type [" + clazz + "].");
		}
	}


	public String getTargetServerFolder() {
		return this.targetServerFolder;
	}


	public void setTargetServerFolder(String targetServerFolder) {
		this.targetServerFolder = targetServerFolder;
	}
}
