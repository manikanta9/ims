package com.clifton.ims.integration.export.definition.dynamic;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionContentHistory;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.dynamic.ExportDefinitionDynamicConfigurationGenerator;
import com.clifton.export.definition.search.ExportDefinitionContentHistorySearchForm;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunStatus;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeFillSearchForm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Gets a distinct list of accounts with trades of a specified type on the given trade date.
 * <p>
 * Constructs a map containing pertinent values (Client Account ID, trade date, trade type)
 *
 * @author theodorez
 */
public class ExportDefinitionInvestmentAccountTradeDynamicConfigurationGenerator implements ExportDefinitionDynamicConfigurationGenerator, ValidationAware {

	/**
	 * The key for the Client Account ID
	 */
	private String clientInvestmentAccountVariableName;

	/**
	 * The account group that trades will be queried for
	 */
	private Integer clientInvestmentAccountGroupId;

	/**
	 * The key for the Trade Type ID
	 */
	private String tradeTypeVariableName;

	/**
	 * The type of trades that must be present for the given group
	 */
	private Short tradeTypeId;

	/**
	 * The number of days back that the query should search
	 */
	private Integer daysBack = 0;

	/**
	 * The key for the trade date
	 */
	private String tradeDateVariableName;

	/**
	 * If set, will NOT filter the Trade Fill list by those that have already been sent
	 * <p>
	 * NOTE: If filtering is desired then the Table Name and PK Column must match the constants below.
	 */
	private boolean notFilterSentTradeFills;

	private static final String CONTENT_HISTORY_TABLE_NAME = "TradeFill";

	private static final String CONTENT_HISTORY_PK_COLUMN_NAME = "TradeFillID";

	////////////////////////////////////////////////////////////////////////////

	private ExportDefinitionService exportDefinitionService;
	private ExportRunService exportRunService;
	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Map<String, Object>> getConfigurationList(ExportDefinition exportDefinition) {
		List<Map<String, Object>> configurationList = new ArrayList<>();

		//Calculate the trade date to query
		Date tradeDate = DateUtils.addDays(new Date(), -1 * getDaysBack());

		List<TradeFill> fillList = getTradeFillList(exportDefinition, tradeDate);
		LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), () -> "Query produced " + fillList.size() + " results"));
		ValidationUtils.assertNotEmpty(fillList, "No trade fills were returned for the configuration");


		Set<Integer> clientAccountIds = CollectionUtils.getStream(fillList).map(fill -> fill.getTrade().getClientInvestmentAccount().getId()).collect(Collectors.toSet());
		LogUtils.debug(LogCommand.ofMessageSupplier(getClass(), () -> "Client Account IDs with Trades: " + StringUtils.collectionToCommaDelimitedString(clientAccountIds)));
		ValidationUtils.assertNotEmpty(clientAccountIds, "No Client Accounts were returned for the configuration");

		String tradeDateString = DateUtils.fromDate(tradeDate, DateUtils.DATE_FORMAT_INPUT);
		for (Integer accountId : clientAccountIds) {
			Map<String, Object> value = new HashMap<>();
			value.put(getClientInvestmentAccountVariableName(), accountId.toString());
			value.put(getTradeDateVariableName(), tradeDateString);
			value.put(getTradeTypeVariableName(), getTradeTypeId());
			configurationList.add(value);
		}

		return configurationList;
	}


	private List<TradeFill> getTradeFillList(ExportDefinition exportDefinition, Date tradeDate) {
		TradeFillSearchForm fillSearchForm = new TradeFillSearchForm();
		fillSearchForm.setClientInvestmentAccountGroupId(getClientInvestmentAccountGroupId());
		fillSearchForm.setTradeDate(tradeDate);
		fillSearchForm.setBookingDateIsNull(false);
		fillSearchForm.setTradeTypeId(getTradeTypeId());

		if (!isNotFilterSentTradeFills()) {
			Integer[] exclusionIds = getTradeFillsToExclude(exportDefinition, tradeDate);
			if (!ArrayUtils.isEmpty(exclusionIds)) {
				fillSearchForm.setExcludeIdList(exclusionIds);
			}
		}

		LogUtils.debug(LogCommand.ofMessage(getClass(), "Querying trades for: ClientInvestmentAccountGroupID: ", getClientInvestmentAccountGroupId(), " Trade Date: ", tradeDate, " Trade Type ID: ", getTradeTypeId()));

		return getTradeService().getTradeFillList(fillSearchForm);
	}


	/**
	 * Gets an array of IDs that have already been sent for the given date
	 */
	private Integer[] getTradeFillsToExclude(ExportDefinition exportDefinition, Date tradeDate) {
		exportDefinition = getExportDefinitionService().getExportDefinitionPopulated(exportDefinition.getId());

		List<ExportDefinitionContent> contents = exportDefinition.getContentList();

		List<ExportDefinitionContentHistory> historiesForDefinition = new ArrayList<>();

		Object[] runStatusValues = ExportRunStatus.ExportRunStatusNames.getExportRunStatusNameListFailed().stream().map(status -> getExportRunService().getExportRunStatusByName(status).getId()).toArray();
		Short[] runHistoryStatusIds = Arrays.copyOf(runStatusValues, runStatusValues.length, Short[].class);
		LogUtils.debug(LogCommand.ofMessage(getClass(), "Excluding RunStatusID's: ", ArrayUtils.toString(runHistoryStatusIds)));

		for (ExportDefinitionContent content : contents) {
			ValidationUtils.assertTrue(content.isContentHistorySupported(), "Export Content History is not currently supported for content");
			ValidationUtils.assertEquals(CONTENT_HISTORY_TABLE_NAME, content.getHistorySystemTable().getName(), "Export Contents for this definition MUST use a History System Table that is " + CONTENT_HISTORY_TABLE_NAME);
			ValidationUtils.assertEquals(CONTENT_HISTORY_PK_COLUMN_NAME, content.getHistoryPKColumnName(), "Export Contents for this definition MUST use a History PK Column Name that is " + CONTENT_HISTORY_PK_COLUMN_NAME);

			ExportDefinitionContentHistorySearchForm historySearchForm = new ExportDefinitionContentHistorySearchForm();
			historySearchForm.setExportDefinitionId(exportDefinition.getId());
			historySearchForm.setDefinitionContentId(content.getId());
			historySearchForm.setHistorySystemTableName(CONTENT_HISTORY_TABLE_NAME);
			historySearchForm.setEndDateAfter(DateUtils.clearTime(tradeDate));
			historySearchForm.setExcludeRunHistoryStatusIds(runHistoryStatusIds);
			historiesForDefinition.addAll(getExportDefinitionService().getExportDefinitionContentHistoryList(historySearchForm));
		}

		Set<Integer> tradeFillIds = new HashSet<>();

		for (ExportDefinitionContentHistory contentHistory : historiesForDefinition) {
			// Not sure if this needs better error handling, assumption is that all data should be Integer for this particular subset of the contentHistory table
			tradeFillIds.add(Integer.parseInt( contentHistory.getHistoryPKFieldId()));
		}

		return CollectionUtils.toArray(tradeFillIds, Integer.class);
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotEmpty(getClientInvestmentAccountVariableName(), "A variable to represent the Client Investment Account is required");
	}

	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public String getClientInvestmentAccountVariableName() {
		return this.clientInvestmentAccountVariableName;
	}


	public void setClientInvestmentAccountVariableName(String clientInvestmentAccountVariableName) {
		this.clientInvestmentAccountVariableName = clientInvestmentAccountVariableName;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public String getTradeTypeVariableName() {
		return this.tradeTypeVariableName;
	}


	public void setTradeTypeVariableName(String tradeTypeVariableName) {
		this.tradeTypeVariableName = tradeTypeVariableName;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Integer getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(Integer daysBack) {
		this.daysBack = daysBack;
	}


	public String getTradeDateVariableName() {
		return this.tradeDateVariableName;
	}


	public void setTradeDateVariableName(String tradeDateVariableName) {
		this.tradeDateVariableName = tradeDateVariableName;
	}


	public boolean isNotFilterSentTradeFills() {
		return this.notFilterSentTradeFills;
	}


	public void setNotFilterSentTradeFills(boolean notFilterSentTradeFills) {
		this.notFilterSentTradeFills = notFilterSentTradeFills;
	}
}
