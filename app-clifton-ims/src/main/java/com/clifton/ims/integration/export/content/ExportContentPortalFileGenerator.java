package com.clifton.ims.integration.export.content;


import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.Date;


/**
 * The <code>ExportContentPortalFileGenerator</code> defines a portal file content generator.
 *
 * @author mwacker
 */
public interface ExportContentPortalFileGenerator<T> extends ExportContentGenerator<Object> {

	/**
	 * Get the portal file.
	 */
	public T getPortalFile(ExportDefinitionContent content);


	/**
	 * Get the calculated export date for the content.
	 */
	public Date getExportDate();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isContentReady(ExportDefinitionContent content);


	public String getComparisonTrueMessage(ExportDefinitionContent content);


	public String getComparisonFalseMessage(ExportDefinitionContent content);
}
