package com.clifton.ims.integration.export.content.field;


import com.clifton.bloomberg.client.BloombergMarketDataProvider;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.exchange.InvestmentExchangeSearchForm;
import com.clifton.investment.exchange.InvestmentExchangeService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Component("tradeExportFieldStoreFactory")
public class TradeExportFieldStoreFactoryImpl implements TradeExportFieldStoreFactory {

	public static final String BUSINESS_IDENTIFIER_CODE = "BusinessIdentifierCode";
	public static final String COMPANY_CUSTOM_FIELDS = "Company Custom Fields";

	private BusinessCompanyService businessCompanyService;
	private InvestmentExchangeService investmentExchangeService;
	private InvestmentAccountService investmentAccountService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;

	private CurrencyTradeExportFieldGenerator currencyTradeExportFieldGenerator;

	private BondTradeExportFieldGenerator bondTradeExportFieldGenerator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportFieldStore buildFieldStore(List<Trade> trades) {
		ExportFieldStore fieldStore = new ExportFieldStore();

		///////////////////////////////////////////
		//   Business Identifier Codes (BICs)
		//////////////////////////////////////////

		//Get the brokers that have a BIC set
		CompanySearchForm companySearchForm = new CompanySearchForm();
		companySearchForm.setBusinessIdentifierCodeNotNull(true);
		List<BusinessCompany> brokers = getBusinessCompanyService().getBusinessCompanyList(companySearchForm);

		//Populate the BIC map
		for (BusinessCompany broker : CollectionUtils.getIterable(brokers)) {
			fieldStore.addBIC(broker, broker.getBusinessIdentifierCode());
		}

		///////////////////////////////////////////
		//     Market Identifier Codes (MICs)
		//////////////////////////////////////////

		//Get the exchanges that have a Market Identifier Code (MIC) set
		InvestmentExchangeSearchForm exchangeSearchForm = new InvestmentExchangeSearchForm();
		exchangeSearchForm.setMarketIdentifierCodeNotNull(true);
		List<InvestmentExchange> exchanges = getInvestmentExchangeService().getInvestmentExchangeList(exchangeSearchForm);

		for (InvestmentExchange exchange : CollectionUtils.getIterable(exchanges)) {
			fieldStore.addMIC(exchange, exchange.getMarketIdentifierCode());
		}

		////////////////////////////////////////////////////////////////////////
		//     Custodian Account Numbers, Yellow keys and Trade Properties
		///////////////////////////////////////////////////////////////////////

		Set<InvestmentSecurity> securities = new HashSet<>();

		for (Trade trade : CollectionUtils.getIterable(trades)) {
			securities.add(trade.getInvestmentSecurity());
			InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
			accountSearchForm.setAccountType("Custodian");
			accountSearchForm.setMainPurpose(InvestmentAccountRelationshipPurpose.MARK_TO_MARKET_ACCOUNT_PURPOSE_NAME);
			accountSearchForm.setMainAccountId(trade.getHoldingInvestmentAccount().getId());
			accountSearchForm.setMainPurposeActiveOnDate(trade.getTradeDate());
			List<InvestmentAccount> custodianAccounts = getInvestmentAccountService().getInvestmentAccountList(accountSearchForm);
			ValidationUtils.assertFalse(custodianAccounts.size() > 1, "Found more than 1 custodian account for holding account: " + trade.getHoldingInvestmentAccount().getNumber());
			InvestmentAccount custodianAccount = CollectionUtils.getFirstElement(custodianAccounts);


			if (custodianAccount != null) {
				fieldStore.addCustodianAccountNumber(trade, custodianAccount.getNumber());
			}

			Map<String, Object> propertyMap = null;
			if (trade.getTradeType().getName().equals(TradeType.CURRENCY)) {
				propertyMap = getCurrencyTradeExportFieldGenerator().generateFields(trade);
			}
			else if (trade.getTradeType().getName().equals(TradeType.BONDS)) {
				propertyMap = getBondTradeExportFieldGenerator().generateFields(trade);
			}

			if (propertyMap != null) {
				fieldStore.addTradeProperties(trade, propertyMap);
			}
		}

		//Get the yellow keys for each security
		MarketDataSource bloombergDataSource = getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);
		for (InvestmentSecurity security : CollectionUtils.getIterable(securities)) {
			String yellowKey = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), bloombergDataSource)
					.getMarketSector()
					.getName();

			if (yellowKey != null) {
				fieldStore.addYellowKey(security, yellowKey);
			}
		}

		return fieldStore;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////             Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentExchangeService getInvestmentExchangeService() {
		return this.investmentExchangeService;
	}


	public void setInvestmentExchangeService(InvestmentExchangeService investmentExchangeService) {
		this.investmentExchangeService = investmentExchangeService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public CurrencyTradeExportFieldGenerator getCurrencyTradeExportFieldGenerator() {
		return this.currencyTradeExportFieldGenerator;
	}


	public void setCurrencyTradeExportFieldGenerator(CurrencyTradeExportFieldGenerator currencyTradeExportFieldGenerator) {
		this.currencyTradeExportFieldGenerator = currencyTradeExportFieldGenerator;
	}


	public BondTradeExportFieldGenerator getBondTradeExportFieldGenerator() {
		return this.bondTradeExportFieldGenerator;
	}


	public void setBondTradeExportFieldGenerator(BondTradeExportFieldGenerator bondTradeExportFieldGenerator) {
		this.bondTradeExportFieldGenerator = bondTradeExportFieldGenerator;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
