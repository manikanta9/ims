package com.clifton.ims.integration.event.corporate.action;

import com.clifton.integration.incoming.corporate.action.IntegrationCorporateAction;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateActionPayoutSecurityTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;

import java.util.Date;


/**
 * <code>CorporateActionEventUtilHandler</code> uses information in the {@link IntegrationCorporateAction} to update or create
 * {@link InvestmentSecurityEvent} or {@link InvestmentSecurityEventPayout}.
 *
 * @author TerryS
 */
public interface CorporateActionEventUtilHandler {

	/**
	 * Determine if the type is supported, if its not empty and found in the database true else false.
	 */
	public boolean isInvestmentSecurityEventTypeSupported(String eventTypeName);


	/**
	 * Creates a new Investment Security Event and populates it from the provided corporate action.
	 */
	public InvestmentSecurityEvent buildInvestmentSecurityEvent(IntegrationCorporateAction integrationCorporateAction);


	/**
	 * Updates security event from the corporate action and returns true if the investment security event is created/new or any updates were performed on the event.
	 */
	public boolean updateInvestmentSecurityEvent(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEvent investmentSecurityEvent);


	/**
	 * Creates and investment security payout event and populates it from the corporate action.
	 */
	public InvestmentSecurityEventPayout buildInvestmentSecurityEventPayout(IntegrationCorporateAction integrationCorporateAction);


	/**
	 * Updates the investment security payout event from the corporate action and returns true if the investment security event payout is created/new or any updates were performed on the payout.
	 */
	public boolean updateInvestmentSecurityEventPayout(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEventPayout investmentSecurityEventPayout);


	/**
	 * Gets the InvestmentSecurity using the provided payout security event, type and security active date.
	 */
	public InvestmentSecurity getInvestmentSecurityByCorporateActionPayoutSecurityType(IntegrationCorporateActionPayoutSecurityTypes payoutSecurityType, String payoutSecurityIdentifier, Date eventDate);
}
