package com.clifton.ims.integration.export.retriever;


import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;

import java.util.Date;


public class ExportSourceRetrieverPerformanceSummary implements ExportSourceRetriever {

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;


	@Override
	public Integer getSourceId(Integer clientInvestmentAccountId, @SuppressWarnings("unused") String sourceSystemTableName, Date exportDate) {
		PerformanceInvestmentAccountSearchForm runSearchForm = new PerformanceInvestmentAccountSearchForm();
		runSearchForm.setMeasureDate(exportDate);
		runSearchForm.setClientAccountId(clientInvestmentAccountId);

		PerformanceInvestmentAccount run = CollectionUtils.getOnlyElement(getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(runSearchForm));
		return run != null ? run.getId() : null;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}
}
