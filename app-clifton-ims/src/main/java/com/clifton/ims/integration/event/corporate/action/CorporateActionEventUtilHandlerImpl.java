package com.clifton.ims.integration.event.corporate.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateAction;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateActionPayoutSecurityTypes;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventStatus;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutType;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * <code>CorporateActionEventUtilHandler</code> uses information in the {@link IntegrationCorporateAction} to update or create
 * {@link InvestmentSecurityEvent} or {@link InvestmentSecurityEventPayout}.
 * <p>
 * The {@link IntegrationCorporateAction} should come in with the correct field values for each {@link InvestmentSecurityEventType}
 * The {@link InvestmentSecurityEventType} has many settings specific to each type which correlate to different validations.
 * See {@link com.clifton.investment.instrument.event.InvestmentSecurityEventObserver} for validations specific to each event type.
 *
 * @author TerryS
 */
@Component
public class CorporateActionEventUtilHandlerImpl implements CorporateActionEventUtilHandler {

	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;


	@Override
	public boolean isInvestmentSecurityEventTypeSupported(String eventTypeName) {
		try {
			if (!StringUtils.isEmpty(eventTypeName)) {
				InvestmentSecurityEventType type = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(eventTypeName);
				// be safe, someone may change the strict cache lookup.
				return type != null;
			}
		}
		catch (Exception e) {
			// Swallow the validation exception if its not found.
		}
		return false;
	}


	@Override
	public boolean updateInvestmentSecurityEvent(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEvent investmentSecurityEvent) {
		InvestmentSecurityEvent beforeInvestmentSecurityEvent = investmentSecurityEvent.isNewBean() ? null : BeanUtils.cloneBean(investmentSecurityEvent, false, false);

		ObjectUtils.doIfPresent(integrationCorporateAction.getCorporateActionIdentifier(), investmentSecurityEvent::setCorporateActionIdentifier);
		ObjectUtils.doIfPresent(integrationCorporateAction.getDeclareDate(), investmentSecurityEvent::setDeclareDate);
		ObjectUtils.doIfPresent(integrationCorporateAction.getExDate(), investmentSecurityEvent::setExDate);
		ObjectUtils.doIfPresent(integrationCorporateAction.getRecordDate(), investmentSecurityEvent::setRecordDate);
		ObjectUtils.doIfPresent(integrationCorporateAction.getEventDate(), investmentSecurityEvent::setEventDate);
		ObjectUtils.doIfPresent(integrationCorporateAction.getAdditionalEventDate(), investmentSecurityEvent::setAdditionalDate);
		ObjectUtils.doIfPresent(integrationCorporateAction.getAdditionalEventValue(), investmentSecurityEvent::setAdditionalEventValue);
		ObjectUtils.doIfPresent(integrationCorporateAction.getEventDescription(), investmentSecurityEvent::setEventDescription);
		if (investmentSecurityEvent.isNewBean()) {
			// the following need to be populated on new events to suppress validations even if they don't apply to the event type (see event payout).
			ObjectUtils.doIfPresent(integrationCorporateAction.getBeforeEventValue(), investmentSecurityEvent::setBeforeEventValue);
			ObjectUtils.doIfPresent(integrationCorporateAction.getAfterEventValue(), investmentSecurityEvent::setAfterEventValue);
		}

		investmentSecurityEvent.setVoluntary(integrationCorporateAction.isVoluntary());
		investmentSecurityEvent.setTaxable(integrationCorporateAction.isTaxable());

		// resolve references
		ObjectUtils.doIfPresent(integrationCorporateAction.getEventTypeName(), name -> {
			InvestmentSecurityEventType type = getInvestmentSecurityEventService().getInvestmentSecurityEventTypeByName(name);
			ValidationUtils.assertNotNull(type, () -> String.format("Investment Security Event Type cannot be found with name [%s].", name));
			investmentSecurityEvent.setType(type);
		});
		ObjectUtils.doIfPresent(integrationCorporateAction.getEventStatusName(), name -> {
			InvestmentSecurityEventStatus securityEventStatus = getInvestmentSecurityEventService().getInvestmentSecurityEventStatusByName(name);
			ValidationUtils.assertNotNull(securityEventStatus, () -> String.format("Investment Security Event Status cannot be found with name [%s].", name));
			investmentSecurityEvent.setStatus(securityEventStatus);
		});
		if (beforeInvestmentSecurityEvent == null || beforeInvestmentSecurityEvent.getSecurity() == null) {
			ObjectUtils.doIfPresent(integrationCorporateAction.getInvestmentSecurityIdentifier(), identifier -> {
				InvestmentSecurity investmentSecurity = getInvestmentInstrumentService().getInvestmentSecurity(identifier);
				ValidationUtils.assertNotNull(investmentSecurity, () -> String.format("Investment Security cannot be found for ID [%s].", identifier));
				investmentSecurityEvent.setSecurity(investmentSecurity);
			});
		}
		updateTypeSpecificValues(integrationCorporateAction, investmentSecurityEvent);

		if (beforeInvestmentSecurityEvent == null) {
			return true;
		}
		// determine if the payout object changed.
		List<String> updatedPropertyList = CoreCompareUtils.getNoEqualPropertiesWithSetters(beforeInvestmentSecurityEvent, investmentSecurityEvent, false, "id");
		return !CollectionUtils.isEmpty(updatedPropertyList);
	}


	@Override
	public InvestmentSecurityEventPayout buildInvestmentSecurityEventPayout(IntegrationCorporateAction integrationCorporateAction) {
		InvestmentSecurityEventPayout investmentSecurityEventPayout = new InvestmentSecurityEventPayout();
		updateInvestmentSecurityEventPayout(integrationCorporateAction, investmentSecurityEventPayout);
		return investmentSecurityEventPayout;
	}


	@Override
	public boolean updateInvestmentSecurityEventPayout(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEventPayout investmentSecurityEventPayout) {
		InvestmentSecurityEventPayout beforeInvestmentSecurityEventPayout = investmentSecurityEventPayout.isNewBean() ? null : BeanUtils.cloneBean(investmentSecurityEventPayout, false, false);

		ObjectUtils.doIfPresent(integrationCorporateAction.getElectionNumber(), investmentSecurityEventPayout::setElectionNumber);
		ObjectUtils.doIfPresent(integrationCorporateAction.getPayoutNumber(), investmentSecurityEventPayout::setPayoutNumber);
		ObjectUtils.doIfPresent(integrationCorporateAction.getBeforeEventValue(), investmentSecurityEventPayout::setBeforeEventValue);
		ObjectUtils.doIfPresent(integrationCorporateAction.getAfterEventValue(), investmentSecurityEventPayout::setAfterEventValue);
		ObjectUtils.doIfPresent(integrationCorporateAction.getAdditionalPayoutValue(), investmentSecurityEventPayout::setAdditionalPayoutValue);
		ObjectUtils.doIfPresent(integrationCorporateAction.getAdditionalPayoutValue2(), investmentSecurityEventPayout::setAdditionalPayoutValue2);
		ObjectUtils.doIfPresent(integrationCorporateAction.getProrationRate(), investmentSecurityEventPayout::setProrationRate);
		ObjectUtils.doIfPresent(integrationCorporateAction.getAdditionalPayoutDate(), investmentSecurityEventPayout::setAdditionalPayoutDate);
		// Only set fractional shares method if the name can be resolved to a type.
		ObjectUtils.doIfPresent(integrationCorporateAction.getFractionalSharesMethodName(), fractionalSharesMethodName -> investmentSecurityEventPayout.setFractionalSharesMethod(FractionalShares.forNameStrict(fractionalSharesMethodName)));
		ObjectUtils.doIfPresent(integrationCorporateAction.getPayoutDescription(), investmentSecurityEventPayout::setDescription);
		investmentSecurityEventPayout.setDtcOnly(integrationCorporateAction.isDtcOnly());
		investmentSecurityEventPayout.setDeleted(integrationCorporateAction.isDeleted());
		investmentSecurityEventPayout.setDefaultElection(integrationCorporateAction.isDefaultElection());

		// resolve security reference
		ObjectUtils.doIfPresent(getCorporateActionPayoutSecurity(integrationCorporateAction), investmentSecurityEventPayout::setPayoutSecurity);
		ObjectUtils.doIfPresent(integrationCorporateAction.getPayoutTypeIdentifier(), identifier -> {
			InvestmentSecurityEventPayoutType investmentSecurityEventPayoutType = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutType(identifier);
			ValidationUtils.assertNotNull(investmentSecurityEventPayoutType, () -> String.format("The payout type cannot be found for ID [%s].", identifier));
			investmentSecurityEventPayout.setPayoutType(investmentSecurityEventPayoutType);
		});

		if (beforeInvestmentSecurityEventPayout == null) {
			return true;
		}
		// determine if the payout object changed.
		List<String> updatedPropertyList = CoreCompareUtils.getNoEqualPropertiesWithSetters(beforeInvestmentSecurityEventPayout, investmentSecurityEventPayout, false, "id");
		return !CollectionUtils.isEmpty(updatedPropertyList);
	}


	@Override
	public InvestmentSecurityEvent buildInvestmentSecurityEvent(IntegrationCorporateAction integrationCorporateAction) {
		InvestmentSecurityEvent investmentSecurityEvent = new InvestmentSecurityEvent();
		updateInvestmentSecurityEvent(integrationCorporateAction, investmentSecurityEvent);
		updateTypeSpecificValues(integrationCorporateAction, investmentSecurityEvent);

		return investmentSecurityEvent;
	}


	@Override
	public InvestmentSecurity getInvestmentSecurityByCorporateActionPayoutSecurityType(IntegrationCorporateActionPayoutSecurityTypes payoutSecurityType, String payoutSecurityIdentifier, Date eventDate) {
		ValidationUtils.assertNotNull(payoutSecurityType, "The Payout Security Type is required.");
		ValidationUtils.assertNotEmpty(payoutSecurityIdentifier, "The Payout Security Identifier is required.");

		// event date is NOT required, use run date (today) when not specified
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setActiveOnDate(eventDate == null ? new Date() : eventDate);
		switch (payoutSecurityType) {
			case VALOREN_CODE:
				ValidationUtils.fail(String.format("Payment Security Type %s is not supported yet.", payoutSecurityType.getName()));
				break;
			case FIGI_CODE:
				ValidationUtils.fail(() -> String.format("Payment Security Type %s is not supported yet.", payoutSecurityType.getName()));
				break;
			case ISIN_CODE:
				securitySearchForm.setIsinEquals(payoutSecurityIdentifier);
				break;
			case SEDOL_CODE:
				securitySearchForm.setSedolEquals(payoutSecurityIdentifier);
				break;
			case TICKER_SYMBOL:
				return getInvestmentInstrumentService().getInvestmentSecurityBySymbolOnly(payoutSecurityIdentifier);
			case CINS_CUSIP_CODE:
				securitySearchForm.setCusipEquals(payoutSecurityIdentifier);
				break;
			case ISO_4217_CURRENCY_CODE:
				return getInvestmentInstrumentService().getInvestmentSecurityBySymbol(payoutSecurityIdentifier, true);
			default:
				ValidationUtils.fail(() -> String.format("Payout Security Identifier %s and type %s cannot be resolved to a security", payoutSecurityType.getName(), payoutSecurityIdentifier));
		}
		List<InvestmentSecurity> matches = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
		return CollectionUtils.getOnlyElement(matches);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void updateTypeSpecificValues(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEvent investmentSecurityEvent) {
		InvestmentSecurityEventType eventType = investmentSecurityEvent.getType();
		if (InvestmentSecurityEventType.MERGER.equals(eventType.getName()) || InvestmentSecurityEventType.EXCHANGE.equals(eventType.getName())) {
			ObjectUtils.doIfPresent(integrationCorporateAction.getEventDate(), investmentSecurityEvent::setExDate);
			ObjectUtils.doIfPresent(integrationCorporateAction.getEventDate(), investmentSecurityEvent::setRecordDate);
		}
		else if (eventType.isSinglePayoutOnly()) {
			ObjectUtils.doIfPresent(getCorporateActionPayoutSecurity(integrationCorporateAction), investmentSecurityEvent::setAdditionalSecurity);
			ObjectUtils.doIfPresent(integrationCorporateAction.getAdditionalPayoutDate(), investmentSecurityEvent::setAdditionalDate);
		}
	}


	private InvestmentSecurity getCorporateActionPayoutSecurity(IntegrationCorporateAction integrationCorporateAction) {
		if (!StringUtils.isEmpty(integrationCorporateAction.getPayoutSecurityIdentifier())) {
			return Optional.ofNullable(integrationCorporateAction.getPayoutSecurityTypeIdentifier())
					.map(identifier -> {
						IntegrationCorporateActionPayoutSecurityTypes payoutSecurityType = IntegrationCorporateActionPayoutSecurityTypes.resolveByCode(identifier);
						ValidationUtils.assertNotNull(payoutSecurityType, () -> String.format("The payout security type cannot be resolved [%s].", identifier));
						return payoutSecurityType;
					}).map(payoutSecurityType -> {
						InvestmentSecurity investmentSecurity = getInvestmentSecurityByCorporateActionPayoutSecurityType(payoutSecurityType,
								integrationCorporateAction.getPayoutSecurityIdentifier(), integrationCorporateAction.getEventDate());
						ValidationUtils.assertNotNull(investmentSecurity, () -> String.format("The payout security cannot be found for type [%s] and identifier [%s].",
								payoutSecurityType.getName(), integrationCorporateAction.getPayoutSecurityIdentifier()));
						return investmentSecurity;
					}).orElse(null);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}
}
