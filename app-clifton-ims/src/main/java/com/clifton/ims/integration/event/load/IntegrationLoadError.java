package com.clifton.ims.integration.event.load;


import com.clifton.core.beans.BaseEntity;


public class IntegrationLoadError extends BaseEntity<Integer> {

	private String uuid;
	private String definitionName;
	private String fieldValue;
	private String error;


	@Override
	public String toString() {
		return "Error in [" + getDefinitionName() + "] could not load [" + getFieldValue() + "]. " + getError();
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public String getFieldValue() {
		return this.fieldValue;
	}


	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}
}
