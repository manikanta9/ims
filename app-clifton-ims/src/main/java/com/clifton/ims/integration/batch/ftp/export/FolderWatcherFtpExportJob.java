package com.clifton.ims.integration.batch.ftp.export;


import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.ftp.FtpFileResult;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>FolderWatchFtpExportJob</code> returns a list of files from a directory to be sent via FTP.  If
 * the archiveFolder exists, the file will be moved there and the result will be a list of archived files.  Otherwise,
 * the result is the list of file in the directory that will be deleted after they are sent via FTP.
 *
 * @author mwacker
 */
public class FolderWatcherFtpExportJob implements FtpExportBean {

	private String archiveFolderPath;
	private String errorFolderPath;
	private String exportFolderPath;

	public static final String FILE_NAME_READY_EXTENSION = "ready";


	@Override
	public List<FileWrapper> getExportFileList(@SuppressWarnings("unused") Date reportingDate, Status status) {
		FileContainer exportDir = FileContainerFactory.getFileContainer(getExportFolderPath());
		FileContainer archiveDir = FileContainerFactory.getFileContainer(getArchiveFolderPath());

		ValidationUtils.assertTrue(exportDir.exists(), "[" + getExportFolderPath() + "] is not a valid export folder path.");
		ValidationUtils.assertTrue(archiveDir.exists(), "[" + getArchiveFolderPath() + "] is not a valid archive directory.");

		List<FileContainer> readyFileList = new ArrayList<>();
		List<FileContainer> fileList = FileUtils.listFiles(exportDir);
		// check each file and add the .ready extension to be sure we have full access to it
		for (FileContainer file : fileList) {
			FileContainer renamedFile = getFileReady(file);
			if (renamedFile != null) {
				readyFileList.add(renamedFile);
			}
		}

		List<FileWrapper> result = new ArrayList<>();
		for (FileContainer file : CollectionUtils.getIterable(readyFileList)) {
			try {
				if (file.isDirectory()) {
					continue;
				}
				FileWrapper wrapper;
				// if there is an archive directory, move the file
				if (archiveDir.exists() && archiveDir.isDirectory()) {
					String outgoingFileName = file.getName().replace("." + FILE_NAME_READY_EXTENSION, "");
					String archiveFileName = outgoingFileName;
					String ext = FileUtils.getFileExtension(archiveFileName);

					archiveFileName = FileUtils.getFileNameWithoutExtension(archiveFileName) + "__" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE) + "." + ext;
					FileContainer archiveFile = FileContainerFactory.getFileContainer(FileUtils.combinePath(getArchiveFolderPath(), archiveFileName));
					FileUtils.moveFileCreatePath(file, archiveFile);
					wrapper = new FileWrapper(FilePath.forPath(archiveFile.getPath()), outgoingFileName, false);
				}
				else {
					wrapper = new FileWrapper(FilePath.forPath(file.getPath()), file.getName().replace("." + FILE_NAME_READY_EXTENSION, ""), false);
				}
				result.add(wrapper);
			}
			catch (Throwable e) {
				status.addError("Failed archive file [" + file.getAbsolutePath() + "].\nCAUSED BY " + ExceptionUtils.getDetailedMessage(e));
			}
		}

		return result;
	}


	@Override
	public void handleFileResult(FtpFileResult fileResult, Status status) {
		if (!fileResult.isSuccess()) {
			// if an error folder exists, then move the file to the error folder
			if (FileUtils.fileExists(fileResult.getFile().getFile()) && !StringUtils.isEmpty(getErrorFolderPath())) {
				String path = getErrorFolderPath();
				String newFileName = FileUtils.combinePath(path, FileUtils.getFileName(fileResult.getFile().getFile().getPath()));
				try {
					FileUtils.moveFileCreatePath(new File(fileResult.getFile().getFile().getPath()), newFileName);
				}
				catch (IOException e) {
					LogUtils.error(getClass(), "Failed to move file [" + FileUtils.getFileName(fileResult.getFile().getFile().getPath()) + "] to error folder.", e);
				}
			}
			status.addError(fileResult.getError().toString());
		}
		// delete files that are not archived or in the error folder
		if (StringUtils.isEmpty(getArchiveFolderPath()) && FileUtils.fileExists(fileResult.getFile().getFile())) {
			FileUtils.delete(fileResult.getFile().getFile());
		}
	}


	/**
	 * Check if the file is available by renaming it.
	 */
	private FileContainer getFileReady(FileContainer file) {
		try {
			if (!file.getName().endsWith("." + FILE_NAME_READY_EXTENSION) && !file.isDirectory()) {
				FileContainer renamedFile = FileContainerFactory.getFileContainer(file.getAbsolutePath() + "." + FILE_NAME_READY_EXTENSION);
				if (file.renameToFile(renamedFile)) {
					return renamedFile;
				}
			}
		}
		catch (Throwable e) {
			// do nothing
			LogUtils.warn(getClass(), "Failed to rename [" + file.getName() + "].", e);
		}
		return null;
	}


	public String getArchiveFolderPath() {
		return this.archiveFolderPath;
	}


	public void setArchiveFolderPath(String archiveFolderPath) {
		this.archiveFolderPath = archiveFolderPath;
	}


	public String getExportFolderPath() {
		return this.exportFolderPath;
	}


	public void setExportFolderPath(String exportFolderPath) {
		this.exportFolderPath = exportFolderPath;
	}


	public String getErrorFolderPath() {
		return this.errorFolderPath;
	}


	public void setErrorFolderPath(String errorFolderPath) {
		this.errorFolderPath = errorFolderPath;
	}
}
