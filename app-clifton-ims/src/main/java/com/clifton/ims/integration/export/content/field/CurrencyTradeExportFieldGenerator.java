package com.clifton.ims.integration.export.content.field;


import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>CurrencyTradeExportFieldGenerator</code> generates extra data/fields for currency trade exports.
 *
 * @author jgommels
 */
@Component
public class CurrencyTradeExportFieldGenerator {

	private InvestmentCurrencyService investmentCurrencyService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, Object> generateFields(Trade trade) {
		Map<String, Object> fieldMap = new HashMap<>();

		BigDecimal amountTraded = trade.getAccountingNotional();

		String currencyTraded = trade.getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol();
		String baseCurrency = InvestmentUtils.getClientAccountBaseCurrency(trade).getSymbol();

		String fromCurrency;
		String toCurrency;
		BigDecimal baseAmount;
		if (getInvestmentCurrencyService().isInvestmentCurrencyMultiplyConvention(baseCurrency, currencyTraded)) {
			fromCurrency = baseCurrency;
			toCurrency = currencyTraded;

			baseAmount = MathUtils.divide(amountTraded, trade.getExchangeRateToBase(), 2);
		}
		else {
			fromCurrency = currencyTraded;
			toCurrency = baseCurrency;

			baseAmount = MathUtils.multiply(amountTraded, trade.getExchangeRateToBase(), 2);
		}

		if (trade.isBuy()) {
			fieldMap.put("amountDelivered", baseAmount);
			fieldMap.put("amountReceived", amountTraded);
			fieldMap.put("currencyDelivered", baseCurrency);
			fieldMap.put("currencyReceived", currencyTraded);
		}
		else {
			fieldMap.put("amountDelivered", amountTraded);
			fieldMap.put("amountReceived", baseAmount);
			fieldMap.put("currencyDelivered", currencyTraded);
			fieldMap.put("currencyReceived", baseCurrency);
		}

		fieldMap.put("fromCurrency", fromCurrency);
		fieldMap.put("toCurrency", toCurrency);

		return fieldMap;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}
}
