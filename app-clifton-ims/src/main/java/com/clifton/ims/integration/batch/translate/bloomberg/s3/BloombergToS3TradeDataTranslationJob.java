package com.clifton.ims.integration.batch.translate.bloomberg.s3;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.run.runner.ExportRunnerServiceImpl;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentType;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * A translator class that converts a daily Bloomberg trade data file to a new trade file to be sent to
 * S3 Financial Services.  This class reads the incoming file, performs the data translation, then exports
 * the new file containing the translated data per the specifications provided by S3 Financial Services.
 * Note: will process all files in the incoming file directory and place all processed input files into an archive directory.
 * The processed files will be kept for the number of days specified in the DaysToArchive property.  Older files will be deleted from the
 * archive.
 *
 * @author davidi
 */
public class BloombergToS3TradeDataTranslationJob implements Task {

	private static final String ORDER_RECORD_IDENTIFIER = "ORDER";
	private static final String ROUTE_RECORD_IDENTIFIER = "ROUTE";
	private static final String FILL_RECORD_IDENTIFIER = "FILL";

	private static final String BLOOMBERG_FILE_DELIMITER = ",";
	private static final String ARCHIVE_DIRECTORY_NAME = "archive";
	private static final String OUTGOING_DIRECTORY_NAME = "outgoing";

	private static final String COMMON_CACHE_KEY = "common";
	private static final String INPUT_FILE_NAME_CACHE_KEY = "inputFileName";
	private static final String LINE_NUMBER_CACHE_KEY = "lineNumber";

	private Date processingDateOverride;
	private String incomingFileDirectory;
	private String incomingFileNameRoot;
	private String incomingFileNameExtension;
	private String exportFileNameRoot;
	private String exportFileNameExtension;
	private Integer exportDefinitionDestinationId;
	private Integer daysToArchive;

	InvestmentInstrumentService investmentInstrumentService;
	ExportDefinitionService exportDefinitionService;
	ExportRunnerServiceImpl exportRunnerService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status jobStatus = Status.ofTitle("Bloomberg To S3 Trade Data TranslationJob Run Status");

		FileContainer inputDirectoryFileContainer = FileContainerFactory.getFileContainer(getIncomingFileDirectory());
		final String outputFileDirectoryPath = inputDirectoryFileContainer.getPath() + File.separator + OUTGOING_DIRECTORY_NAME;
		final FileContainer outputDirectoryContainer = FileContainerFactory.getFileContainer(outputFileDirectoryPath);
		final String archiveFileDirectoryPath = inputDirectoryFileContainer.getPath() + File.separator + ARCHIVE_DIRECTORY_NAME;
		final FileContainer archiveDirectoryContainer = FileContainerFactory.getFileContainer(archiveFileDirectoryPath);

		if (!createArchiveOutputDirectories(archiveDirectoryContainer, outputDirectoryContainer, jobStatus)) {
			return jobStatus;
		}

		// get filtered list of files to process
		List<String> filteredFileList = filterFiles(inputDirectoryFileContainer, jobStatus);
		if (filteredFileList == null || jobStatus.getErrorCount() > 0) {
			return jobStatus;
		}

		// iterate file list in incoming directory
		for (String inputFileName : CollectionUtils.getIterable(filteredFileList)) {
			final Date processingDate = IntegrationFileUtils.getEffectiveDateFromFileName(inputFileName);
			if (processingDate == null) {
				jobStatus.addError("Processing error:  cannot extract effective date from filename: " + inputFileName);
				continue;
			}

			final String inputFileSuffix = "_" + DateUtils.fromDate(processingDate, DateUtils.DATE_FORMAT_ISO_SIMPLE) + (StringUtils.isEmpty(getIncomingFileNameExtension()) ? "" : "." + getIncomingFileNameExtension());
			final String outputFileSuffix = "." + DateUtils.fromDate(processingDate, DateUtils.DATE_FORMAT_ISO_SIMPLE) + (StringUtils.isEmpty(getExportFileNameExtension()) ? "" : "." + getExportFileNameExtension());
			final String outputFileName = getExportFileNameRoot() + outputFileSuffix;
			final String inputFilePath = getIncomingFileDirectory() + File.separator + getIncomingFileNameRoot() + inputFileSuffix;

			final String outputFilePath = outputFileDirectoryPath + File.separator + outputFileName;
			final FileContainer inputFileContainer = FileContainerFactory.getFileContainer(inputFilePath);
			final FileContainer outputFileContainer = FileContainerFactory.getFileContainer(outputFilePath);

			if (!createTranslatedOutputFile(processingDate, inputFileContainer, outputFileContainer, jobStatus)) {
				// remove invalid output file from outgoing directory
				try {
					if (outputDirectoryContainer.exists()) {
						outputFileContainer.deleteFile();
					}
				}
				catch (IOException exc) {
					jobStatus.addError("Deletion of file: " + outputFileContainer.getName() + " failed due to: " + exc.getMessage());
				}
				continue;
			}

			if (!exportFile(outputFileContainer, jobStatus)) {
				continue;
			}

			moveInputFileToArchive(inputFileContainer, archiveDirectoryContainer, jobStatus);
		}

		removeExpiredFilesFromArchive(archiveDirectoryContainer, jobStatus);


		if (jobStatus.getErrorCount() == 0) {
			jobStatus.addMessage("File(s) sent to Export Runner Service for transmission.");
		}

		return jobStatus;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean createArchiveOutputDirectories(FileContainer archiveDirectoryContainer, FileContainer outputDirectoryContainer, Status jobStatus) {
		try {
			FileUtils.createDirectory(outputDirectoryContainer.getPath());
			FileUtils.createDirectory(archiveDirectoryContainer.getPath());
			return true;
		}
		catch (Exception exc) {
			jobStatus.addError("Failed to create directories required for processing: " + exc.getMessage());
			return false;
		}
	}


	/**
	 * Filters file names in the input directory to contain only file names, omitting directory names.  If a ProcessingDateOverride, further
	 * filtering is applied to only include a file with the processing date within its filename.
	 *
	 * @param inputDirectoryFileContainer FileContainer representing the directory of files to be processed.
	 * @param jobStatus                   reference to a Status object that gets updated if an error occurs
	 * @return a filtered list of files if successful; a value of null if filtering process failed.
	 */
	private List<String> filterFiles(FileContainer inputDirectoryFileContainer, Status jobStatus) {
		try {
			List<String> fullFilePathList = CollectionUtils.createList(inputDirectoryFileContainer.list());
			List<String> filteredFileList = CollectionUtils.getStream(fullFilePathList)
					.filter(entry -> entry.startsWith(getIncomingFileNameRoot()) && entry.endsWith(getIncomingFileNameExtension()))
					.filter(entry -> getProcessingDateOverride() == null || IntegrationFileUtils.getEffectiveDateFromFileName(entry).equals(getProcessingDateOverride()))
					.collect(Collectors.toList());
			return filteredFileList;
		}
		catch (IOException exc) {
			jobStatus.addError("Failed to get list of files to process: " + exc.getMessage());
			return null;
		}
	}


	/**
	 * Translates the Bloomberg tradedata input file to a translated S3 trade data file.
	 * returns true if successful, false if exception occurs.  On exception, adds an error the the Status object.
	 */
	private boolean createTranslatedOutputFile(Date processingDate, FileContainer inputFileContainer, FileContainer outputFileContainer, Status jobStatus) {
		try (BufferedReader inputFileReader = new BufferedReader(new InputStreamReader(inputFileContainer.getInputStream()));
		     BufferedWriter outputFileWriter = new BufferedWriter(new OutputStreamWriter(outputFileContainer.getOutputStream()))) {

			// Temporary cache to allow storage of data that is to be shared amongst record types
			Map<String, Map<String, String>> cachedData = new HashMap<>();

			// Save current file name, add entry to hold line numbers
			Map<String, String> commonCacheEntry = new HashMap<>();
			commonCacheEntry.put(INPUT_FILE_NAME_CACHE_KEY, inputFileContainer.getName());
			commonCacheEntry.put(LINE_NUMBER_CACHE_KEY, "1");
			cachedData.put(COMMON_CACHE_KEY, commonCacheEntry);

			final String headerRecord = createS3OutputHeaderRecord(processingDate);

			outputFileWriter.write(headerRecord);
			outputFileWriter.newLine();

			String inputRecord;
			int lineCounter = 0;
			while ((inputRecord = inputFileReader.readLine()) != null) {
				lineCounter++;
				// ignore first 3 lines, they are headers, not data
				if (lineCounter <= 3) {
					continue;
				}
				if (StringUtils.isEmpty(inputRecord)) {
					continue;
				}

				commonCacheEntry.put(LINE_NUMBER_CACHE_KEY, Integer.toString(lineCounter));
				String outputRecord = translateInputRecord(lineCounter - 1, inputRecord, cachedData);
				AssertUtils.assertNotNull(outputRecord, "Cannot translate input record in file: " + inputFileContainer.getName() + " at line " + lineCounter + " to a corresponding output record.");

				outputFileWriter.write(outputRecord);
				outputFileWriter.newLine();
			}
			return true;
		}
		catch (Exception exc) {
			jobStatus.addError("Processing Error: " + exc.getMessage());
			return false;
		}
	}


	/**
	 * Export the translated output file to Integration Services for further processing.
	 * Returns true if successful or false if not successful.  If not successful, updates the Status reference with an error message.
	 */
	private boolean exportFile(FileContainer outputFileContainer, Status jobStatus) {
		try {
			FileWrapper outputFileWrapper = new FileWrapper(FilePath.forPath(outputFileContainer.getPath()), outputFileContainer.getName(), true);
			getExportRunnerService().sendSingleCustomExportFile(outputFileWrapper, getExportDefinitionDestinationId());
			return true;
		}
		catch (Exception exc) {
			jobStatus.addError("Export Processing error for file: " + outputFileContainer.getName() + "Error: " + exc.getMessage());
			return false;
		}
	}


	/**
	 * Moves an input file to the archiveDirectoryContainer.  Returns true if successful, false on failure.  On failure, also
	 * updates the Status reference with an error message.
	 */
	private boolean moveInputFileToArchive(FileContainer inputFileContainer, FileContainer archiveDirectoryContainer, Status jobStatus) {
		try {
			FileUtils.moveFileCreatePath(inputFileContainer, archiveDirectoryContainer);
			return true;
		}
		catch (Exception exc) {
			jobStatus.addError("Processing error: cannot move file: " + inputFileContainer.getName() + " to archive directory: " + exc.getMessage());
			return false;
		}
	}


	/**
	 * Deletes files from the archive directory that are older than the specified days specified in the DaysToArchive property.
	 * Returns true if successful, false on failure.  On failure, also updates the jobStatus with an error message.
	 */
	private boolean removeExpiredFilesFromArchive(FileContainer archiveDirectoryContainer, Status jobStatus) {
		try {
			if (getDaysToArchive() != null && getDaysToArchive() > 0) {
				String archiveDirPath = archiveDirectoryContainer.getPath();
				Date currentDate = new Date();

				// Iterate across all files and delete the ones older than the value specified by the DaysToArchive property.
				for (String filename : CollectionUtils.createList(archiveDirectoryContainer.list())) {
					FileContainer archivedFileContainer = FileContainerFactory.getFileContainer(archiveDirPath + File.separator + filename);
					Date fileCreationDate = IntegrationFileUtils.getEffectiveDateFromFileName(filename);

					try {
						if (DateUtils.getDaysDifference(currentDate, fileCreationDate) > getDaysToArchive()) {
							archivedFileContainer.deleteFile();
						}
					}
					catch (IOException exc) {
						jobStatus.addError("Processing error: cannot delete file: " + filename + " from archive directory: " + exc.getMessage());
					}
				}
			}
			return true;
		}
		catch (IOException exc) {
			jobStatus.addError("I/O Exception encountered while trying to delete older files in archive directory: " + exc.getMessage());
			return false;
		}
	}


	private String createS3OutputHeaderRecord(Date processingDate) {
		final String headerTemplate = "H|V2.0.5|%s|%sT080000000|%sT163000000|PARM|PARAPORT_MPID2|WEPO-Trading@paraport.com|Daily File";
		String formattedProcessingDate = DateUtils.fromDate(processingDate, "yyyyMMdd");
		String formattedProcessingDateTime = DateUtils.fromDate(processingDate, "yyyyMMdd-HHmmssSSS");
		formattedProcessingDateTime = formattedProcessingDateTime.replace("-", "T");
		return String.format(headerTemplate, formattedProcessingDateTime, formattedProcessingDate, formattedProcessingDate);
	}


	private String translateInputRecord(int recordIndex, String inputRecord, Map<String, Map<String, String>> cachedData) {

		if (inputRecord == null || inputRecord.isEmpty()) {
			return null;
		}

		String[] fields = inputRecord.split(BLOOMBERG_FILE_DELIMITER);
		if (fields.length == 0) {
			return null;
		}
		switch (fields[0]) {
			case ORDER_RECORD_IDENTIFIER:
				return translateOrderRecord(inputRecord, cachedData);

			case ROUTE_RECORD_IDENTIFIER:
				return translateRouteRecord(inputRecord, cachedData);

			case FILL_RECORD_IDENTIFIER:
				return translateFillRecord(recordIndex, inputRecord, cachedData);
			default:
				return null;
		}
	}


	private String translateOrderRecord(String orderRecord, Map<String, Map<String, String>> cachedData) {
		//example:  O|NO|15075436|15075436|||||PARM|LARRY BERMAN||O|ACN   190621C00170000||S|20190305T095038000|MKT|253|||DAY||0||0||0|0|0|0|0|0|0|0
		final String template = "O|NO|%s|%s|||||PARM|%s||%s|%s||%s|%s|%s|%s|%s||%s||0||0||0|0|0|0|0|0|0|0";
		final String limitOrderSymbol = "LMT";
		final int expectedRecordFieldCount = 17;


		if (orderRecord != null) {
			String[] fields = orderRecord.split(",");
			AssertUtils.assertEquals(expectedRecordFieldCount, fields.length,
					getFormattedFileNameWithLine(cachedData) + "found input ORDER record with unexpected field count of " + fields.length + ". Expected " + expectedRecordFieldCount + " fields.");

			String orderId = fields[1].trim();
			String symbol = fields[3].trim();
			String side = fields[4].trim();
			String amount = fields[5].trim();
			String limitPrice = fields[7].trim();
			String tif = fields[11].trim();
			String orderType = fields[13].trim();
			String traderName = fields[14].trim();
			String createDate = fields[15].trim();
			String createTime = fields[16].trim();

			InvestmentSecurity investmentSecurity = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(symbol, false);
			AssertUtils.assertNotNull(investmentSecurity, getFormattedFileNameWithLine(cachedData) + "Cannot find InvestmentSecurity for symbol: \"" + symbol + "\".");

			// Additional conversions
			// orderAction note:  source file will have SS for sell, and BS for buy, but the translated field uses only the first character.
			String orderAction = side.substring(0, 1);
			String securityType = translateSecurityType(investmentSecurity, cachedData);

			if ("O".equals(securityType)) {
				symbol = investmentSecurity.getOccSymbol();
				AssertUtils.assertNotEmpty(symbol, getFormattedFileNameWithLine(cachedData) + "No OCC Symbol defined for security with symbol: \"" + investmentSecurity.getSymbol() + "\".");
			}

			String orderDateTime = translateDateTime(createDate, createTime);

			if (!limitOrderSymbol.equals(orderType)) {
				limitPrice = "";
			}

			HashMap<String, String> cachedValues = new HashMap<>();
			cachedValues.put("tif", tif);
			cachedValues.put("securityType", securityType);
			cachedValues.put("limitPrice", limitPrice);
			cachedValues.put("orderType", orderType);
			cachedValues.put("orderAction", orderAction);
			cachedValues.put("symbol", symbol);
			cachedData.put(orderId, cachedValues);

			String translatedRecord = String.format(template, orderId, orderId, traderName, securityType, symbol, orderAction, orderDateTime, orderType, amount, limitPrice, tif);
			return translatedRecord;
		}
		return null;
	}


	private String translateRouteRecord(String routeRecord, Map<String, Map<String, String>> cachedData) {
		//example:  O|RO|15075436|15075436-R|15075436|||||MSET||O|ACN   190621C00170000||S|20190305T095316000|MKT|253|||DAY||0||0||0|0|0|0|0|0|0|0
		final String template = "O|RO|%s|%s-R|%s|||||%s||%s|%s||%s|%s|%s|%s|%s||%s||0||0||0|0|0|0|0|0|0|0";
		final int expectedFieldCount = 13;

		if (routeRecord != null) {
			String[] fields = routeRecord.split(",");

			// Usually the field count is 13 for a ROUTE entry, but it may be 12 if the Route Filled Amount (field 11) = 0 (where the Route Avg Price field may be empty)  No fields beyond field index 9 are currently used, so the omission does not affect processing.
			AssertUtils.assertTrue(fields.length == expectedFieldCount || (fields.length == expectedFieldCount - 1 && "0".equals(fields[11])),
					getFormattedFileNameWithLine(cachedData) + "Found input ROUTE record with unexpected field count of " + fields.length + ". Expected at least" + (expectedFieldCount - 1) + " fields.");

			String routedAmount = fields[1].trim();
			String routeDate = fields[4].trim();
			String routeTime = fields[5].trim();
			String broker = fields[8].trim();
			String orderId = fields[9].trim();

			String routeDateTime = translateDateTime(routeDate, routeTime);
			AssertUtils.assertNotEmpty(orderId, getFormattedFileNameWithLine(cachedData) + "Order ID is missing in the ROUTE record");
			AssertUtils.assertNotNull(cachedData.get(orderId), getFormattedFileNameWithLine(cachedData) + "Cannot find cached data for Order ID: " + orderId);

			String tif = cachedData.get(orderId).get("tif");
			String securityType = cachedData.get(orderId).get("securityType");
			String limitPrice = cachedData.get(orderId).get("limitPrice");
			String orderType = cachedData.get(orderId).get("orderType");
			String orderAction = cachedData.get(orderId).get("orderAction");
			String symbol = cachedData.get(orderId).get("symbol");

			String translatedRecord = String.format(template, orderId, orderId, orderId, broker, securityType, symbol, orderAction, routeDateTime, orderType, routedAmount, limitPrice, tif);
			return translatedRecord;
		}
		return null;
	}


	private String translateFillRecord(int recordIndex, String fillRecord, Map<String, Map<String, String>> cachedData) {
		//example:  T|15075436-R|15075436-5|||O||20190305T095323000|7|3.5700|MSET||A
		final String template = "T|%s-R|%s-%s|||%s||%s|%s|%.4f|%s||A";
		final int expectedRecordFieldCount = 11;

		if (fillRecord != null) {
			String[] fields = fillRecord.split(",");
			AssertUtils.assertEquals(expectedRecordFieldCount, fields.length,
					getFormattedFileNameWithLine(cachedData) + "Found input FILL record with unexpected field count of " + fields.length + ". Expected " + expectedRecordFieldCount + " fields.");

			String orderId = fields[1].trim();
			String fillDate = fields[6].trim();
			String fillTime = fields[7].trim();
			String broker = fields[8].trim();
			String fillAmount = fields[9].trim();
			String fillPriceString = fields[10].trim();

			BigDecimal fillPrice = new BigDecimal(fillPriceString);

			AssertUtils.assertNotEmpty(orderId, getFormattedFileNameWithLine(cachedData) + "Order ID is missing in the FILL record");
			AssertUtils.assertNotNull(cachedData.get(orderId), getFormattedFileNameWithLine(cachedData) + "Cannot find cached data for Order ID: " + orderId);
			String securityType = cachedData.get(orderId).get("securityType");
			String actionDateTime = translateDateTime(fillDate, fillTime);

			String translatedRecord = String.format(template, orderId, orderId, recordIndex, securityType, actionDateTime, fillAmount, fillPrice, broker);
			return translatedRecord;
		}
		return null;
	}


	private String translateDateTime(String dateString, String timeString) {
		Date dateComponent = DateUtils.toDate(dateString, "MM/dd/yy", "MM/dd/yyyy");
		Time timeComponent = Time.parse(timeString);
		Date dateTime = DateUtils.addMilliseconds(dateComponent, timeComponent.getMilliseconds());

		String formattedProcessingDateTime = DateUtils.fromDate(dateTime, "yyyyMMdd-HHmmssSSS");
		formattedProcessingDateTime = formattedProcessingDateTime.replace("-", "T");
		return formattedProcessingDateTime;
	}


	/**
	 * Translates an InvestmentType for an InvestmentSecurity to a symbol that can be used as a corresponding investment type identifier in
	 * the S3 output record.
	 */
	private String translateSecurityType(InvestmentSecurity investmentSecurity, Map<String, Map<String, String>> cachedData) {
		InvestmentType investmentType = investmentSecurity.getInstrument().getHierarchy().getInvestmentType();

		switch (investmentType.getName()) {
			case InvestmentType.OPTIONS:
				return "O";
			case InvestmentType.STOCKS:
			case InvestmentType.FUNDS:
				return "E";
			case InvestmentType.FUTURES:
				return "F";
			case InvestmentType.BONDS:
				return "B";
		}

		throw new RuntimeException(getFormattedFileNameWithLine(cachedData) + "No translation for investment type: \"" + investmentType.getName() + "\" symbol used by S3 record");
	}


	/**
	 * Returns a formatted string containing filename and line number, based on the current values in the cache.
	 */
	private String getFormattedFileNameWithLine(Map<String, Map<String, String>> cachedData) {
		return "In file: " + cachedData.get(COMMON_CACHE_KEY).get(INPUT_FILE_NAME_CACHE_KEY) + " line: " + cachedData.get(COMMON_CACHE_KEY).get(LINE_NUMBER_CACHE_KEY) + " ";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getProcessingDateOverride() {
		return this.processingDateOverride;
	}


	public void setProcessingDateOverride(Date processingDateOverride) {
		this.processingDateOverride = processingDateOverride;
	}


	public String getIncomingFileDirectory() {
		return this.incomingFileDirectory;
	}


	public void setIncomingFileDirectory(String incomingFileDirectory) {
		this.incomingFileDirectory = incomingFileDirectory;
	}


	public String getIncomingFileNameRoot() {
		return this.incomingFileNameRoot;
	}


	public void setIncomingFileNameRoot(String incomingFileNameRoot) {
		this.incomingFileNameRoot = incomingFileNameRoot;
	}


	public String getIncomingFileNameExtension() {
		return this.incomingFileNameExtension;
	}


	public void setIncomingFileNameExtension(String incomingFileNameExtension) {
		this.incomingFileNameExtension = incomingFileNameExtension;
	}


	public String getExportFileNameRoot() {
		return this.exportFileNameRoot;
	}


	public void setExportFileNameRoot(String exportFileNameRoot) {
		this.exportFileNameRoot = exportFileNameRoot;
	}


	public String getExportFileNameExtension() {
		return this.exportFileNameExtension;
	}


	public void setExportFileNameExtension(String exportFileNameExtension) {
		this.exportFileNameExtension = exportFileNameExtension;
	}


	public Integer getExportDefinitionDestinationId() {
		return this.exportDefinitionDestinationId;
	}


	public void setExportDefinitionDestinationId(Integer exportDefinitionDestinationId) {
		this.exportDefinitionDestinationId = exportDefinitionDestinationId;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public ExportRunnerServiceImpl getExportRunnerService() {
		return this.exportRunnerService;
	}


	public void setExportRunnerService(ExportRunnerServiceImpl exportRunnerService) {
		this.exportRunnerService = exportRunnerService;
	}


	public Integer getDaysToArchive() {
		return this.daysToArchive;
	}


	public void setDaysToArchive(Integer daysToArchive) {
		this.daysToArchive = daysToArchive;
	}
}




