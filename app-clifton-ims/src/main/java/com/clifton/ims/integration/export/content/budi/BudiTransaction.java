package com.clifton.ims.integration.export.content.budi;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.budi.transaction.BudiExecutionTypes;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;


/**
 * @author TerryS
 */
public class BudiTransaction {

	private final Trade trade;
	private final BudiExecutionTypes type;


	public BudiTransaction(Trade trade, BudiExecutionTypes type) {
		this.trade = trade;
		this.type = type;
	}


	public Trade getTrade() {
		return this.trade;
	}


	public String getExecutionType() {
		return this.type.toString();
	}


	public String getTradeReportIdentifier() {
		return getTrade().getIdentity().toString();
	}


	public String getTransactionTime() {
		return DateUtils.fromDate(getTrade().getTradeDate(), "yyyyMMdd");
	}


	public String getBlpTicketType() {
		// DT = Direct Trade (can also be used for booking TT tickets by AIM customers)
		return "DT";
	}


	public String getTransactionAction() {
		return getTrade().isBuy() ? "BUY" : "SELL";
	}


	public String getPrice() {
		String results = getTrade().getAverageUnitPrice() == null ? "" : getTrade().getAverageUnitPrice().toPlainString();
		if (TradeType.CURRENCY.equals(getTrade().getTradeType().getName())) {
			results = getTrade().getExchangeRateToBase().toPlainString();
		}
		return results;
	}


	public String getPriceType() {
		// 1 = Percentage (e.g. percent of par). Often called "dollar price" for fixed income.
		return "1";
	}


	public String getQuantity() {
		if (TradeType.CURRENCY.equals(getTrade().getTradeType().getName())) {
			return getTrade().getAccountingNotional() == null ? "" : getTrade().getAccountingNotional().toPlainString();
		}
		return getTrade().getQuantityIntended() == null ? "" : getTrade().getQuantityIntended().toPlainString();
	}


	public String getQuantityType() {
		// 0 = Units (shares, par, currency) – this is default
		return "0";
	}


	public String getSettleDate() {
		return DateUtils.fromDate(getTrade().getSettlementDate(), "yyyyMMdd");
	}


	public String getAccruedInterestAmount() {
		// Can be set only when the security’s Bloomberg pricing record has a 99 Yield Flag.
		return getTrade().getAccrualAmount() == null ? "0" : getTrade().getAccrualAmount().toPlainString();
	}


	public String getCounterParty() {
		return "PARA";
	}


	public String getBroker() {
		return "";
	}


	public String getStipulationBlock() {
		// TRUE_POINTS=Y;TRANSACTION_CODE=O
		return "Y";
	}


	public String getTransactionCostType1() {
		// This is a numeric field that corresponds with a code in {CFTK<GO>}.
		return "2";
	}


	public String getTransactionCostAmount1() {
		return MathUtils.isNullOrZero(getTrade().getCommissionAmount()) ? "" : getTrade().getCommissionAmount().abs().toPlainString();
	}


	public String getTransactionCostFlag1() {
		return "-";
	}


	public String getTransactionCostCurrency1() {
		return getTrade().getPayingSecurity().getSymbol();
	}


	public String getTransactionCostType2() {
		// This is a numeric field that corresponds with a code in {CFTK<GO>}.
		return "3";
	}


	public String getTransactionCostAmount2() {
		return MathUtils.isNullOrZero(getTrade().getFeeAmount()) ? "" : getTrade().getFeeAmount().abs().toPlainString();
	}


	public String getTransactionCostFlag2() {
		return "-";
	}


	public String getTransactionCostCurrency2() {
		return getTrade().getPayingSecurity().getSymbol();
	}
}
