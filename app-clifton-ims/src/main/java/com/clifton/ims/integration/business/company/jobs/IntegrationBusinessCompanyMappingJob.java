package com.clifton.ims.integration.business.company.jobs;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.ims.integration.business.company.IntegrationBusinessCompanyMappingResults;
import com.clifton.ims.integration.business.company.IntegrationBusinessCompanyMappingService;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * <code>IntegrationBusinessCompanyMappingJob</code> A Batch job to update IMS Business Companies from Integration
 * Business Companies based on matching Bloomberg identifiers.  The job has options for comma-separated lists of
 * Business Company Types and Business Company Columns to update.
 */
public class IntegrationBusinessCompanyMappingJob<T extends IdentityObject> implements Task, ValidationAware {

	/**
	 * Comma-separated list of Business Company Types (defaults to all if empty)
	 */
	private List<String> includedBusinessCompanyTypes;

	/**
	 * Comma-separated list of Business Company Columns (defaults to all if none selected; exclude custom)
	 */
	private List<String> excludedColumns;

	////////////////////////////////////////////////////////////////////////////

	private BusinessCompanyService businessCompanyService;
	private IntegrationBusinessCompanyMappingService integrationBusinessCompanyMappingService;
	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		// IMPORTANT:  Exceptions are swallowed and recorded in the status.
		final Status status = Status.ofEmptyMessage();
		try {
			validate();
			CompanySearchForm companySearchForm = new CompanySearchForm();
			companySearchForm.setBloombergCompanyIdentifierNotNull(true);
			// handle restricting the type list
			if (!CollectionUtils.isEmpty(getIncludedBusinessCompanyTypes())) {
				List<String> businessCompanyList = getIncludedBusinessCompanyTypes();
				String[] includedCompanyTypeArray = CollectionUtils.toArray(businessCompanyList, String.class);
				companySearchForm.setCompanyTypeNames(includedCompanyTypeArray);
			}
			// get all business companies of the included types.
			List<BusinessCompany> businessCompanyList = getBusinessCompanyService().getBusinessCompanyList(companySearchForm);
			// perform the integration data update and gather update statistics
			Map<Boolean, Long> counts = businessCompanyList
					.stream()
					.map(BusinessCompany::getId)
					.map(b -> updateBusinessCompany(b, status))
					.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
			status.setMessage(String.format("Total Records %s, Updated Records %s, Skipped Records %s, Errors %s",
					businessCompanyList.size(),
					counts.containsKey(Boolean.TRUE) ? counts.get(Boolean.TRUE) : 0,
					counts.containsKey(Boolean.FALSE) ? counts.get(Boolean.FALSE) : 0,
					status.getErrorCount()));
		}
		catch (ValidationException v) {
			status.setMessage("FAILED setup validation.");
			status.addError(v.getMessage());
		}
		return status;
	}


	/**
	 * Implicitly called when the job is saved, explicitly called each time the job is ran.
	 */
	@Override
	public void validate() throws ValidationException {
		// all type names exist
		for (String typeName : CollectionUtils.asNonNullList(getIncludedBusinessCompanyTypes())) {
			BusinessCompanyType businessCompanyType = getBusinessCompanyService().getBusinessCompanyTypeByName(typeName);
			if (businessCompanyType == null) {
				throw new ValidationException("Business company type is not valid '" + typeName + "'");
			}
		}
		// all column names exist
		@SuppressWarnings(value = "unchecked")
		Map<String, Column> allColumnsMap = getDaoLocator().locate((Class<T>) BusinessCompany.class).getConfiguration().getColumnList().stream()
				.collect(Collectors.toMap(Column::getName, Function.identity()));
		for (String columnName : CollectionUtils.asNonNullList(getExcludedColumns())) {
			if (!allColumnsMap.containsKey(columnName)) {
				throw new ValidationException("Business company column is not valid '" + columnName + "'");
			}
		}
		// all excluded columns are actually updated.
		// may want to exclude from runtime validation
		List<Column> mappedColumnList = getIntegrationBusinessCompanyMappingService().getBusinessCompanyMappedColumns();
		List<Column> columnList = CollectionUtils.asNonNullList(getExcludedColumns()).stream().map(allColumnsMap::get).collect(Collectors.toList());
		columnList.removeAll(mappedColumnList);
		ValidationUtils.assertEmpty(columnList, () -> ("Excluded Business Company Columns do not get updated by integration data "
				+ columnList.stream().map(Column::getName).collect(Collectors.toList())));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean updateBusinessCompany(int businessCompanyId, final Status status) {
		// convert string column to actual column
		@SuppressWarnings(value = "unchecked")
		Map<String, Column> allColumnsMap = getDaoLocator().locate((Class<T>) BusinessCompany.class).getConfiguration().getColumnList().stream()
				.collect(Collectors.toMap(Column::getName, Function.identity()));
		List<Column> excludedColumnList = CollectionUtils.asNonNullList(getExcludedColumns()).stream().map(allColumnsMap::get).collect(Collectors.toList());
		// do the integration service call
		IntegrationBusinessCompanyMappingResults integrationBusinessCompanyMappingResults = getIntegrationBusinessCompanyMappingService()
				.getBusinessCompanyFromIntegrationBusinessCompanyWithExclusions(businessCompanyId, excludedColumnList);
		// update status from integration service call
		String prefix = "Company ID [" + businessCompanyId + "] ";
		List<String> validations = integrationBusinessCompanyMappingResults.getValidationList();
		CollectionUtils.asNonNullList(validations).stream()
				.map(v -> prefix + v)
				.forEach(status::addError);
		// update business company if its changed
		BusinessCompany businessCompany = integrationBusinessCompanyMappingResults.getBusinessCompany();
		if (integrationBusinessCompanyMappingResults.isChanged()) {
			try {
				getBusinessCompanyService().saveBusinessCompany(businessCompany);
				return true;
			}
			catch (Exception ex) {
				LogUtils.warn(IntegrationBusinessCompanyMappingJob.class, prefix, ex);
				status.addError(prefix + ExceptionUtils.getDetailedMessage(ex));
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<String> getIncludedBusinessCompanyTypes() {
		return this.includedBusinessCompanyTypes;
	}


	public void setIncludedBusinessCompanyTypes(List<String> includedBusinessCompanyTypes) {
		this.includedBusinessCompanyTypes = includedBusinessCompanyTypes;
	}


	public List<String> getExcludedColumns() {
		return this.excludedColumns;
	}


	public void setExcludedColumns(List<String> excludedColumns) {
		this.excludedColumns = excludedColumns;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public IntegrationBusinessCompanyMappingService getIntegrationBusinessCompanyMappingService() {
		return this.integrationBusinessCompanyMappingService;
	}


	public void setIntegrationBusinessCompanyMappingService(IntegrationBusinessCompanyMappingService integrationBusinessCompanyMappingService) {
		this.integrationBusinessCompanyMappingService = integrationBusinessCompanyMappingService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
