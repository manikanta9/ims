package com.clifton.ims.integration.business.company;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.StatusDetail;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <code>IntegrationBusinessCompanyMappingResults</code> a DTO class with Business company and errors encountered while mapping an Integration Business Company
 * to an IMS Business Company.
 */
public class IntegrationBusinessCompanyMappingResults {

	private BusinessCompany businessCompany;
	private List<String> validationList;
	private boolean changed;


	public IntegrationBusinessCompanyMappingResults(BusinessCompany businessCompany, List<StatusDetail> error, boolean changed) {
		this.businessCompany = businessCompany;
		this.validationList = CollectionUtils.asNonNullList(error).stream().map(StatusDetail::toString).collect(Collectors.toList());
		this.changed = changed;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public List<String> getValidationList() {
		return this.validationList;
	}


	public void setValidationList(List<String> validationList) {
		this.validationList = validationList;
	}


	public boolean isChanged() {
		return this.changed;
	}


	public void setChanged(boolean changed) {
		this.changed = changed;
	}
}
