package com.clifton.ims.integration.event.m2m;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.reconcile.IntegrationReconcileAccountM2MDailyExpense;
import com.clifton.integration.incoming.reconcile.IntegrationReconcileService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class ReconcileM2MExpenseIntegrationEventListener extends AbstractReconcileM2MIntegrationEventListener {

	private IntegrationReconcileService integrationReconcileService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return IntegrationImportRun.INTEGRATION_EVENT_BROKER_M2M_EXPENSE;
	}


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for Collateral events processing.");

		IntegrationImportDefinition importDefinition = run.getIntegrationImportDefinition();
		AssertUtils.assertNotNull(importDefinition.getFileDefinition().getSource(), "An import source is required for M2M events processing.");

		BusinessCompany issuingCompany = getBusinessCompanyService().getBusinessCompany(run.getIntegrationImportDefinition().getFileDefinition().getSource().getSourceCompanyId());

		List<Throwable> errors = new ArrayList<>();

		// stores the indicator that mark is in the local currency
		Map<Integer, Boolean> markTypeCache = new HashMap<>();

		/*
		  TODO This logic assumes that [holding account, date, markCurrency] is unique. This means that if a file has
		  multiple rows with the same expense type (or multiple of them map to the same expense type on our end) with
		  the same currency (or if we use reporting currency instead of local currency), then each processed amount
		  will overwrite the previous amount. We may want to consider changing this to support multiple expense rows
		  with the same currency.
		 */
		if (IntegrationImportDefinitionType.BROKER_IMPORT_M2M_EXPENSES.equals(importDefinition.getType().getName())) {
			// The list of expenses that have been imported into integration, for which need to be translated into {@link AccountingM2MDailyExpense}s.
			List<IntegrationReconcileAccountM2MDailyExpense> expenses = getIntegrationReconcileService().getIntegrationReconcileAccountM2MDailyExpenseListByRun(run.getId());

			// Maps a key of [holding account, date, markCurrency] to a matching AccountingM2MDaily record
			Map<String, AccountingM2MDaily> map = new HashMap<>();

			for (IntegrationReconcileAccountM2MDailyExpense integrationExpense : CollectionUtils.getIterable(expenses)) {
				try {
					InvestmentAccount holdingAccount = lookupHoldingAccount(integrationExpense.getAccountNumber(), issuingCompany);
					Date positionDate = integrationExpense.getPositionDate();

					// Indicates that this account will have multiple M2M entries, one for each currency
					boolean m2mInLocalCurrency = isM2MinLocalCurrency(holdingAccount, markTypeCache);

					// get the expense currency
					InvestmentSecurity expenseCurrency;
					if (!StringUtils.isEmpty(integrationExpense.getCurrencySymbol())) {
						expenseCurrency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(integrationExpense.getCurrencySymbol(), true);
					}
					else {
						expenseCurrency = holdingAccount.getBaseCurrency();
					}

					// create the key and get the m2m object from the cache
					String key = BeanUtils.createKeyFromBeans(holdingAccount, positionDate, m2mInLocalCurrency ? expenseCurrency : "");
					AccountingM2MDaily m2mDaily = map.get(key);

					//If there wasn't a matching entry, then we need to look it up from the database (or create a new one if it doesn't exist yet) and put it in the map.
					if (m2mDaily == null) {
						// get the fully populated m2m entry
						m2mDaily = findAccountingM2MDaily(positionDate, holdingAccount, m2mInLocalCurrency ? expenseCurrency : null);
						if (m2mDaily == null) {
							m2mDaily = buildNewM2MDaily(holdingAccount, positionDate, expenseCurrency, m2mInLocalCurrency);
							m2mDaily.setExpenseList(new ArrayList<>());
						}

						map.put(key, m2mDaily);
					}


					// get the expense type
					AccountingM2MDailyExpenseType expenseType = getAccountingM2MService().getAccountingM2MDailyExpenseTypeByExternalNameAndAmount(integrationExpense.getExpenseType().name(), integrationExpense.getExpenseAmount());

					// Get an existing AccountingM2MDailyExpense to update or create a new one
					AccountingM2MDailyExpense accountingExpense = m2mDaily.isNewBean() ? null : getAccountingM2MService().getAccountingM2MDailyExpenseForSpecificMark(m2mDaily, expenseType, expenseCurrency, null);
					if (accountingExpense == null) {
						accountingExpense = new AccountingM2MDailyExpense();
						accountingExpense.setType(expenseType);
						accountingExpense.setExpenseCurrency(expenseCurrency);
						accountingExpense.setM2mDaily(m2mDaily);
						m2mDaily.getExpenseList().add(accountingExpense);
					}
					accountingExpense.setExpectedExpenseAmount(integrationExpense.getExpenseAmount());

					// set the expense amount if it is not populated or it is the same as the internal expense amount
					if (accountingExpense.getExpenseAmount() == null || MathUtils.isEqual(accountingExpense.getExpenseAmount(), accountingExpense.getOurExpenseAmount())) {
						accountingExpense.setExpenseAmount(integrationExpense.getExpenseAmount());
					}

					//Set the FX rate to the conversion rate from the expense currency to the mark currency.
					if (holdingAccount.getBaseCurrency().equals(expenseCurrency)) {
						accountingExpense.setExpenseFxRate(BigDecimal.ONE);
					}
					else {
						accountingExpense.setExpenseFxRate(getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(holdingAccount.toHoldingAccount(), expenseCurrency.getSymbol(), holdingAccount.getBaseCurrency().getSymbol(), positionDate).flexibleLookup()));
					}
				}
				catch (Throwable e) {
					errors.add(e);
				}
			}

			//Now that all expenses have been processed and associated with AccountingM2MDaily records (new or old), we can now save everything.
			for (AccountingM2MDaily m2mDaily : CollectionUtils.getIterable(map.values())) {
				getAccountingM2MService().saveAccountingM2MDailyWithExpenses(m2mDaily, true);
			}
		}

		if (!errors.isEmpty()) {
			throw new RuntimeException(errors.get(errors.size() - 1));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationReconcileService getIntegrationReconcileService() {
		return this.integrationReconcileService;
	}


	public void setIntegrationReconcileService(IntegrationReconcileService integrationReconcileService) {
		this.integrationReconcileService = integrationReconcileService;
	}
}
