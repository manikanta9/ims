package com.clifton.ims.integration.export.content.budi;

import com.clifton.core.util.ObjectUtils;
import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.security.BudiFxSecurity;
import com.clifton.export.budi.security.BudiIrSecurity;
import com.clifton.export.budi.security.BudiPlaceholderSecurity;
import com.clifton.export.budi.security.BudiSecurity;
import com.clifton.export.budi.transaction.BudiSecurityPopulator;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.trade.Trade;
import org.springframework.stereotype.Component;


/**
 * @author TerryS
 */
@Component
public class BudiSecurityPopulatorImpl implements BudiSecurityPopulator<Trade> {

	private SystemColumnValueService systemColumnValueService;


	@Override
	public BudiSecurity populate(BudiInstrumentTypes type, Trade trade) {
		InvestmentSecurity security = trade.getInvestmentSecurity();
		switch (type.getFormat()) {
			case FX_AND_FX_OPTIONS: {
				BudiFxSecurity budiFxSecurity = new BudiFxSecurity(type);

				budiFxSecurity.setAction("AddUpdate");
				budiFxSecurity.setInstType(type.toString().toLowerCase());

				budiFxSecurity.setDeliveryDate(security.getFirstDeliveryDate());
				budiFxSecurity.setDescription(security.getDescription());
				budiFxSecurity.setDelivery(security.getInstrument().getHierarchy().getInstrumentDeliverableType().name());
				budiFxSecurity.setCurrency(security.getInstrument().getTradingCurrency().getSymbol());
				budiFxSecurity.setUnderlyingSecurity(getSecuritySymbol(security.getUnderlyingSecurity()));

				budiFxSecurity.setIdentifier(trade.getId() + "_" + security.getId());
				budiFxSecurity.setPayOffSide(trade.isBuy() ? "Buy" : "Sell");
				budiFxSecurity.setTradeDate(trade.getTradeDate());
				budiFxSecurity.setSettlementCurrency(trade.getSettlementCurrency().getSymbol());
				budiFxSecurity.setCounterparty(trade.getExecutingBrokerCompany().getName());
				budiFxSecurity.setRate(trade.getAverageUnitPrice());
				budiFxSecurity.setNotional(trade.getAccountingNotional());

				return budiFxSecurity;
			}
			case INTEREST_RATE_DERIVATIVES: {
				BudiIrSecurity budiIrSecurity = new BudiIrSecurity(type);

				budiIrSecurity.setAction("AddUpdate");
				budiIrSecurity.setInstType(type.toString().toLowerCase());
				budiIrSecurity.setxRefContext("User");

				budiIrSecurity.setIndex(security.getInstrument().getHierarchy().getNameExpanded().contains("Index") ? "Index" : "");
				budiIrSecurity.setDescription(security.getDescription());
				budiIrSecurity.setAsset(getSecuritySymbol(security.getUnderlyingSecurity()));
				budiIrSecurity.setCurrency(security.getInstrument().getTradingCurrency().getSymbol());
				budiIrSecurity.setDayCountBasis(getDayCount(security));

				budiIrSecurity.setIdentifier(trade.getId() + "_" + security.getId());
				budiIrSecurity.setCounterparty(trade.getHoldingInvestmentAccount().getIssuingCompany().getName());
				budiIrSecurity.setPayoffSide(trade.isBuy() ? "Buy" : "Sell");
				budiIrSecurity.setNotional(trade.getAccountingNotional());

				return budiIrSecurity;
			}
			case PLACEHOLDER:
			default: {
				BudiPlaceholderSecurity budiPlaceholderSecurity = new BudiPlaceholderSecurity(type);

				budiPlaceholderSecurity.setInstType(type.toString().toLowerCase());
				// The instrument type and associated market value convention. See 2.6 Reference Data Tables.
				budiPlaceholderSecurity.setRiskSecurityType(type.getLabel());
				budiPlaceholderSecurity.setDescription(security.getDescription());
				budiPlaceholderSecurity.setTicker(getSecuritySymbol(security));
				budiPlaceholderSecurity.setCurrency(security.getInstrument().getTradingCurrency().getSymbol());
				budiPlaceholderSecurity.setMaturityDate(security.getEndDate());
				budiPlaceholderSecurity.setIdentifier(trade.getId() + "_" + security.getId());
				budiPlaceholderSecurity.setRiskSecurityType("UNKNOWN");

				return budiPlaceholderSecurity;
			}
		}
	}


	public String getSecuritySymbol(InvestmentSecurity security) {
		if (security == null) {
			return null;
		}
		return ObjectUtils.coalesce(
				security.getFigi(),
				security.getCusip(),
				security.getIsin(),
				security.getSedol(),
				security.getOccSymbol(),
				security.getSymbol()
		);
	}


	public String getDayCount(InvestmentSecurity security) {
		Object value = getSystemColumnValueService().getSystemColumnCustomValue(security.getId(), InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_DAY_COUNT_CONVENTION);
		return value == null ? null : value.toString();
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
