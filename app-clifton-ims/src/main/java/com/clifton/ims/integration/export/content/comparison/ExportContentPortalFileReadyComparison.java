package com.clifton.ims.integration.export.content.comparison;


import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.ims.integration.export.content.ExportContentPortalFileGenerator;
import com.clifton.system.bean.SystemBeanService;


public class ExportContentPortalFileReadyComparison implements Comparison<ExportDefinitionContent> {

	private SystemBeanService systemBeanService;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(ExportDefinitionContent content, ComparisonContext context) {
		//TODO - while refactoring for class signatures - came across this - not sure how best to deal with it.
		ExportContentGenerator<?> systemBean = (ExportContentGenerator<?>) getSystemBeanService().getBeanInstance(content.getContentSystemBean());
		ValidationUtils.assertTrue(systemBean instanceof ExportContentPortalFileGenerator, ExportContentPortalFileReadyComparison.class.getSimpleName() +
				" only supports " + ExportContentPortalFileGenerator.class.getSimpleName() + " objects.");

		ExportContentPortalFileGenerator<?> contentGenerator = (ExportContentPortalFileGenerator<?>) systemBean;

		boolean contentReady = contentGenerator.isContentReady(content);
		if (context != null) {
			if (contentReady) {
				String message = contentGenerator.getComparisonTrueMessage(content);
				context.recordTrueMessage(message);
			}
			else {
				String message = contentGenerator.getComparisonFalseMessage(content);
				context.recordFalseMessage(message);
			}
		}

		return contentReady;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
