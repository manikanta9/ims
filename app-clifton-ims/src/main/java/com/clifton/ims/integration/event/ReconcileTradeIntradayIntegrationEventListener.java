package com.clifton.ims.integration.event;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.search.IntegrationImportDefinitionSearchForm;
import com.clifton.integration.incoming.trade.intraday.IntegrationTradeIntraday;
import com.clifton.integration.incoming.trade.intraday.IntegrationTradeIntradayService;
import com.clifton.integration.incoming.trade.intraday.IntegrationTradeIntradayTypes;
import com.clifton.integration.incoming.trade.intraday.search.IntegrationTradeIntradaySearchForm;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourcePriceMultiplier;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMapping;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyMappingSearchForm;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurposeService;
import com.clifton.reconcile.ReconcileHandler;
import com.clifton.reconcile.trade.intraday.ReconcileTradeIntradayReconciliationService;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalService;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalStatuses;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalSearchForm;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class ReconcileTradeIntradayIntegrationEventListener extends BaseIntegrationEventListener {

	public IntegrationImportService integrationImportService;

	private ReconcileTradeIntradayExternalService reconcileTradeIntradayExternalService;
	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;
	private ReconcileTradeIntradayReconciliationService reconcileTradeIntradayReconciliationService;
	private MarketDataSourcePurposeService marketDataSourcePurposeService;
	private MarketDataSourceCompanyService marketDataSourceCompanyService;
	private IntegrationTradeIntradayService integrationTradeIntradayService;
	private BusinessCompanyService businessCompanyService;

	private ReconcileHandler reconcileHandler;

	private String eventName = IntegrationImportRun.INTEGRATION_EVENT_BROKER_INTRADAY_TRADES;


	@Override
	public void onEvent(IntegrationImportEvent event) {
		ValidationUtils.assertNotNull(event, "Event Listener received null event");
		Boolean reconcileUnconfirmed = false;
		IntegrationImportRun run = null;
		Date date;
		if (event.getContextValue("isReload") != null) {
			date = (Date) event.getContextValue("date");
			ValidationUtils.assertNotNull(date, "Reload date is missing from request");

			reconcileUnconfirmed = (Boolean) event.getContextValue("reconcileUnconfirmed");
			ValidationUtils.assertNotNull(reconcileUnconfirmed, "\"Reconcile Unconfirmed\" flag is missing from request");
		}
		else {
			run = event.getTarget();
			AssertUtils.assertNotNull(run, "No run exists for [" + event.getTarget() + "] for event with name [" + event.getEventName() + "].");

			date = DateUtils.clearTime(run.getEffectiveDate());
		}

		List<RuntimeException> errorList = new ArrayList<>();
		MultiValueMap<Date, Integer> accountsToReconcile = loadExternalTrades(run, date, errorList);
		if (accountsToReconcile != null) {
			reconcileAccounts(accountsToReconcile, reconcileUnconfirmed, errorList);
		}

		if (!errorList.isEmpty()) {
			throw errorList.get(0);
		}
	}


	/**
	 * Load the external data and return a map of accounts and dates that need to be auto reconciled.
	 *
	 * @param run
	 * @param tradeDate
	 */
	private MultiValueMap<Date, Integer> loadExternalTrades(IntegrationImportRun run, Date tradeDate, List<RuntimeException> errorList) {
		try {
			MultiValueMap<Date, Integer> result = new MultiValueHashMap<>(false);
			List<IntegrationTradeIntraday> externalTradeList = getExternalTradeList(run, tradeDate);

			// We could make these caches global if we are having performance issues.
			ExternalTradeLoadDataHolder rebuildData = new ExternalTradeLoadDataHolder(run, tradeDate);

			for (IntegrationTradeIntraday externalTrade : CollectionUtils.getIterable(externalTradeList)) {
				ReconcileTradeIntradayExternal reconcileTrade = loadExternalTrade(externalTrade, rebuildData);
				if (reconcileTrade != null) {
					result.put(reconcileTrade.getTradeDate(), reconcileTrade.getHoldingInvestmentAccount().getId());
				}
			}

			// remove any trades the are no longer on the trade file
			for (RunDataHolder holder : CollectionUtils.getIterable(rebuildData.getRunDataHolderCache().values())) {
				for (ReconcileTradeIntradayExternal external : CollectionUtils.getIterable(holder.getExistingReconcileTradeIntradayExternalList())) {
					deleteExistingReconcileTradeIntradayExternal(external);
				}
			}

			if (rebuildData.getErrors() != null) {
				errorList.add(new RuntimeException(rebuildData.getErrors().toString(), rebuildData.getFirstCause()));
			}
			return result;
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to load external trades for run [" + run + "].", e);
			errorList.add(new RuntimeException("Failed to load external trades for run [" + run + "].", e));
		}
		return null;
	}


	private void reconcileAccounts(MultiValueMap<Date, Integer> accountsToReconcile, boolean reconcileUnconfirmed, List<RuntimeException> errorList) {
		if (accountsToReconcile != null) {
			StringBuilder errors = null;
			Throwable firstCause = null;
			for (Map.Entry<Date, Collection<Integer>> tradeDateEntry : accountsToReconcile.entrySet()) {
				for (Integer accountId : tradeDateEntry.getValue()) {
					try {
						// run the match process
						getReconcileTradeIntradayReconciliationService().autoReconcileTradeIntradayByHoldingAccount(tradeDateEntry.getKey(), reconcileUnconfirmed, accountId);
					}
					catch (Throwable e) {
						LogUtils.error(getClass(), "Failed to auto reconcile trades to accounts [" + accountsToReconcile + "].", e);

						if (e.getMessage() != null) {
							if (errors == null) {
								firstCause = e;
								errors = new StringBuilder(1024);
							}
							else {
								errors.append("\n\n");
							}
							errors.append("Failed to auto reconcile account with id [").append(accountId).append("].");
							errors.append("\nCAUSED BY ");
							errors.append(e.getMessage());
						}
					}
				}
			}
			if (errors != null) {
				errorList.add(new RuntimeException(errors.toString(), firstCause));
			}
		}
	}


	private ReconcileTradeIntradayExternal loadExternalTrade(IntegrationTradeIntraday externalTrade, ExternalTradeLoadDataHolder rebuildData) {
		try {
			// get the run data holder
			getRunCacheDataHolder(rebuildData);
			return doLoadExternalTrade(externalTrade, rebuildData);
		}
		catch (Throwable e) {
			addError(e, rebuildData, externalTrade);
		}
		return null;
	}


	private ReconcileTradeIntradayExternal doLoadExternalTrade(IntegrationTradeIntraday externalTrade, ExternalTradeLoadDataHolder rebuildData) {
		// get the run data holder
		RunDataHolder runData = getRunCacheDataHolder(rebuildData);

		// get the existing reconcile trade
		ReconcileTradeIntradayExternal reconcileTrade = null;
		List<ReconcileTradeIntradayExternal> reconcileTradeList = getReconcileTradeList(externalTrade.getUniqueTradeId(), externalTrade.getTradeDate(), runData.getSourceCompany().getId(),
				rebuildData);
		// if there is more than one trade, delete them all and reload -- THERE SHOULD ONLY BE 1
		if (reconcileTradeList.size() > 1) {
			for (ReconcileTradeIntradayExternal trade : CollectionUtils.getIterable(reconcileTradeList)) {
				deleteExistingReconcileTradeIntradayExternal(trade);
			}
		}
		else {
			reconcileTrade = CollectionUtils.getOnlyElement(reconcileTradeList);
		}
		// delete the trade and its matches if it's a deleted trade
		if (IntegrationTradeIntradayTypes.DELETED == externalTrade.getType()) {
			if (reconcileTrade != null) {
				deleteExistingReconcileTradeIntradayExternal(reconcileTrade);
				runData.getExistingReconcileTradeIntradayExternalList().remove(reconcileTrade);
				reconcileTrade = null;
			}
		}
		else {
			reconcileTrade = createOrUpdateReconcileTrade(reconcileTrade, externalTrade, rebuildData);
		}
		return reconcileTrade;
	}


	private ReconcileTradeIntradayExternal createOrUpdateReconcileTrade(ReconcileTradeIntradayExternal reconcileTrade, IntegrationTradeIntraday externalTrade, ExternalTradeLoadDataHolder rebuildData) {
		RunDataHolder runData = getRunCacheDataHolder(rebuildData);

		// resolve the holding account and executing broker
		InvestmentAccount holdingAccount = getHoldingInvestmentAccount(externalTrade.getAccountNumber(), rebuildData);
		BusinessCompany executingBroker = getExecutingBroker(externalTrade, runData.getMarketDataSource(), runData.getMarketDataSourcePurpose().getId(), rebuildData);

		// only load data for accounts that clear at the source company or that don't have a active load of there own
		if (holdingAccount.getIssuingCompany().equals(runData.getSourceCompany()) || isBrokerIntegrationLoadDisable(holdingAccount.getIssuingCompany(), rebuildData)) {

			InvestmentSecurity security = getInvestmentSecurity(externalTrade.getInvestmentSecuritySymbol(), runData.getSecurityMarketDataSource(), false, rebuildData);
			ValidationUtils.assertNotNull(security, "Cannot find a security for symbol [" + externalTrade.getInvestmentSecuritySymbol() + "] in account [" + holdingAccount + "].");
			InvestmentSecurity payingSecurity = getInvestmentSecurity(externalTrade.getPayingSecuritySymbol(), runData.getSecurityMarketDataSource(), true, rebuildData);

			// adjust the price
			BigDecimal price = adjustIncomingMarketDataSourcePrice(externalTrade.getPrice(), security.getInstrument(), runData.getSourceCompany(),
					runData.getMarketDataSourcePurpose(), rebuildData);
			if (reconcileTrade == null) {
				reconcileTrade = new ReconcileTradeIntradayExternal();
			}
			reconcileTrade.setHoldingInvestmentAccount(holdingAccount);
			reconcileTrade.setHoldingInvestmentAccountNumber(externalTrade.getAccountNumber());
			reconcileTrade.setPayingSecurity(payingSecurity);
			reconcileTrade.setInvestmentSecurity(security);
			reconcileTrade.setExecutingBrokerCompany(executingBroker);
			reconcileTrade.setBuy(externalTrade.isBuy());
			reconcileTrade.setTradeDate(externalTrade.getTradeDate());
			reconcileTrade.setQuantity(externalTrade.getQuantity());
			reconcileTrade.setOriginalPrice(externalTrade.getPrice());
			reconcileTrade.setPrice(price);
			reconcileTrade.setStatus(ReconcileTradeIntradayExternalStatuses.valueOf(externalTrade.getStatus().name()));
			reconcileTrade.setExternalUniqueTradeId(externalTrade.getUniqueTradeId());
			reconcileTrade.setSourceCompany(runData.getSourceCompany());

			getReconcileTradeIntradayExternalService().saveReconcileTradeIntradayExternal(reconcileTrade);

			// remove the saved reconcile trade from the existing list
			runData.getExistingReconcileTradeIntradayExternalList().remove(reconcileTrade);
		}
		return reconcileTrade;
	}


	public BigDecimal adjustIncomingMarketDataSourcePrice(BigDecimal price, InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose, ExternalTradeLoadDataHolder rebuildData) {
		String key = instrument.getId() + ":" + company.getId() + ":" + purpose.getId();
		BigDecimal multiplier = null;
		if (rebuildData.getSecurityMultiplierCache().containsKey(key)) {
			multiplier = rebuildData.getSecurityMultiplierCache().get(key);
		}
		else {
			MarketDataSourcePriceMultiplier priceMultiplier = getMarketDataSourceService().getMarketDataSourcePriceMultiplierByCompany(instrument, company, purpose);
			if (priceMultiplier != null) {
				multiplier = priceMultiplier.getAdditionalPriceMultiplier();
			}
			rebuildData.getSecurityMultiplierCache().put(key, multiplier);
		}
		if (multiplier != null) {
			return price.multiply(multiplier);
		}
		return price;
	}


	private void deleteExistingReconcileTradeIntradayExternal(ReconcileTradeIntradayExternal reconcileTrade) {
		if (!CollectionUtils.isEmpty(reconcileTrade.getMatchList())) {
			getReconcileTradeIntradayMatchService().deleteReconcileTradeIntradayMatchList(reconcileTrade.getMatchList());
		}
		getReconcileTradeIntradayExternalService().deleteReconcileTradeIntradayExternal(reconcileTrade);
	}


	private List<IntegrationTradeIntraday> getExternalTradeList(IntegrationImportRun run, Date tradeDate) {
		IntegrationTradeIntradaySearchForm searchForm = new IntegrationTradeIntradaySearchForm();
		if (run != null) {
			searchForm.setRunId(run.getId());
			searchForm.setSourceCompanyId(run.getFile().getFileDefinition().getSource().getSourceCompanyId());
		}
		else if (tradeDate != null) {
			searchForm.setTradeDate(tradeDate);
		}
		searchForm.setExcludeOverriddenTrades(true);
		return getIntegrationTradeIntradayService().getIntegrationTradeIntradayList(searchForm);
	}


	private boolean isBrokerIntegrationLoadDisable(BusinessCompany clearingBroker, ExternalTradeLoadDataHolder cache) {
		if (cache.getBrokerLoadDisabledCache().containsKey(clearingBroker.getId())) {
			return cache.getBrokerLoadDisabledCache().get(clearingBroker.getId());
		}
		IntegrationImportDefinitionSearchForm searchForm = new IntegrationImportDefinitionSearchForm();
		searchForm.setSourceCompanyId(clearingBroker.getId());
		searchForm.setTypeName(IntegrationImportDefinitionType.BROKER_IMPORT_TRADE_INTRADAY);
		List<IntegrationImportDefinition> definitionList = getIntegrationImportService().getIntegrationImportDefinitionList(searchForm);
		boolean result = true;
		for (IntegrationImportDefinition definition : CollectionUtils.getIterable(definitionList)) {
			if (!definition.isDisabled()) {
				result = false;
				break;
			}
		}
		cache.getBrokerLoadDisabledCache().put(clearingBroker.getId(), result);
		return result;
	}


	private InvestmentAccount getHoldingInvestmentAccount(String accountNumber, ExternalTradeLoadDataHolder cache) {
		InvestmentAccount result;
		if (cache.getHoldingAccountCache().containsKey(accountNumber)) {
			return cache.getHoldingAccountCache().get(accountNumber);
		}
		result = getReconcileHandler().lookupInvestmentHoldingAccount(accountNumber, null);
		if (result != null) {
			cache.getHoldingAccountCache().put(accountNumber, result);
		}
		return result;
	}


	private InvestmentSecurity getInvestmentSecurity(String securitySymbol, MarketDataSource dataSource, boolean currency, ExternalTradeLoadDataHolder cache) {
		if (StringUtils.isEmpty(securitySymbol)) {
			return null;
		}
		InvestmentSecurity result;
		String key = securitySymbol + ":" + dataSource.getId() + ":" + currency;
		if (cache.getSecurityCache().containsKey(key)) {
			result = cache.getSecurityCache().get(key);
		}
		else {
			try {
				result = getReconcileHandler().lookupInvestmentSecurity(securitySymbol, dataSource.getId(), currency);
			}
			catch (Throwable e) {
				throw new RuntimeException("Failed to find security for [" + securitySymbol + "].", e);
			}
			cache.getSecurityCache().put(key, result);
		}
		return result;
	}


	private BusinessCompany getExecutingBroker(IntegrationTradeIntraday externalTrade, MarketDataSource dataSource, Short purposeId, ExternalTradeLoadDataHolder cache) {
		String key = externalTrade.getExecutingBrokerCompanyName() + ":" + dataSource.getId() + ":" + purposeId;
		if (cache.getExecutingBrokerCache().containsKey(key)) {
			return cache.getExecutingBrokerCache().get(key);
		}

		MarketDataSourceCompanyMappingSearchForm searchForm = new MarketDataSourceCompanyMappingSearchForm();
		searchForm.setMarketDataSourceId(dataSource.getId());
		searchForm.setMarketDataSourcePurposeId(purposeId);
		if (externalTrade.getExecutingBrokerCompanyName() != null) {
			searchForm.setExternalCompanyName(externalTrade.getExecutingBrokerCompanyName());
		}
		else {
			searchForm.setExternalCompanyNameIsNull(true);
		}
		try {
			MarketDataSourceCompanyMapping mapping = CollectionUtils.getOnlyElement(getMarketDataSourceCompanyService().getMarketDataSourceCompanyMappingList(searchForm));
			ValidationUtils.assertNotNull(mapping, "Failed to find company for [" + externalTrade.getExecutingBrokerCompanyName() + "] in datasource [" + dataSource + "].");
			cache.getExecutingBrokerCache().put(key, mapping.getBusinessCompany());
			return mapping.getBusinessCompany();
		}
		catch (Throwable e) {
			addError(new RuntimeException("Failed to find company for [" + externalTrade.getExecutingBrokerCompanyName() + "] in datasource [" + dataSource + "].", e), cache, externalTrade);
		}
		return null;
	}


	/**
	 * Creates a cache by source company id and trade date of all ReconcileTradeIntradayExternal objects for that company.  The cached value is a
	 * MultiValueMap were the key is the unique trade id and the value is a list of ReconcileTradeIntradayExternals.
	 */
	private List<ReconcileTradeIntradayExternal> getReconcileTradeList(String uniqueId, Date tradeDate, Integer sourceCompanyId, ExternalTradeLoadDataHolder cache) {
		List<ReconcileTradeIntradayExternal> result;
		String key = sourceCompanyId + ":" + tradeDate;
		if (cache.getReconcileTradeCache().containsKey(key)) {
			result = cache.getReconcileTradeCache().get(key).get(uniqueId);
		}
		else {
			ReconcileTradeIntradayExternalSearchForm searchForm = new ReconcileTradeIntradayExternalSearchForm();
			searchForm.setTradeDate(tradeDate);
			searchForm.setSourceCompanyId(sourceCompanyId);
			// gets a list of fully populated reconcile trade objects, which mean the match list is populated
			List<ReconcileTradeIntradayExternal> reconcileTradeList = getReconcileTradeIntradayExternalService().getReconcileTradeExternalList(searchForm);
			MultiValueMap<String, ReconcileTradeIntradayExternal> tradeMap = BeanUtils.getMultiValueMapFromProperty(reconcileTradeList, "externalUniqueTradeId");
			cache.getReconcileTradeCache().put(key, tradeMap);
			result = tradeMap.get(uniqueId);
		}
		return result == null ? new ArrayList<>() : result;
	}


	private void addError(Throwable e, ExternalTradeLoadDataHolder rebuildData, IntegrationTradeIntraday externalTrade) {
		RunDataHolder runData = getRunCacheDataHolder(rebuildData);
		// full log for first error and only causes for consecutive errors
		if (e.getMessage() != null && !rebuildData.getAddedErrors().contains(e.getMessage())) {
			if (rebuildData.getErrors() == null) {
				rebuildData.setFirstCause(e);
				rebuildData.setErrors(new StringBuilder(1024));
			}
			else {
				rebuildData.getErrors().append("\n\n");
			}
			rebuildData.getErrors().append("Failed to import trades for [").append(runData != null && runData.getSourceCompany() != null ? runData.getSourceCompany().getName() : externalTrade.getRun().getIntegrationImportDefinition().getName()).append("].");
			rebuildData.getErrors().append("\nCAUSED BY ");
			rebuildData.getErrors().append(e.getMessage());
			if (e.getMessage() != null) {
				rebuildData.getAddedErrors().add(e.getMessage());
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Build run data or return it from the cache.
	 */
	private RunDataHolder getRunCacheDataHolder(ExternalTradeLoadDataHolder cache) {
		if (cache.getRunDataHolderCache().containsKey(cache.getImportRun().getId())) {
			return cache.getRunDataHolderCache().get(cache.getImportRun().getId());
		}
		ValidationUtils.assertNotNull(cache.getTradeDate(), "Trade date cannot be null.");

		MarketDataSourcePurpose dataSourcePurpose = getPurpose(cache.getImportRun());
		MarketDataSource securityMarketDataSource = getMarketDataSourceService().getMarketDataSourceByName(cache.getImportRun().getIntegrationImportDefinition().getDataSourceName());
		BusinessCompany sourceCompany = getBusinessCompanyService().getBusinessCompany(cache.getImportRun().getFile().getFileDefinition().getSource().getSourceCompanyId());
		BusinessCompany dataSourceCompany = sourceCompany;
		if (sourceCompany != null) {
			dataSourceCompany = sourceCompany.getParent() != null ? sourceCompany.getParent() : sourceCompany;
		}
		MarketDataSource marketDataSource = getMarketDataSourceForCompany(dataSourceCompany);
		// get the list of existing trades
		List<ReconcileTradeIntradayExternal> reconcileTradeIntradayExternalList = getReconcileTradeIntradayExternalList(cache.getTradeDate(), sourceCompany != null ? sourceCompany.getId() : null);

		RunDataHolder holder = new RunDataHolder(dataSourcePurpose, securityMarketDataSource, sourceCompany, marketDataSource, reconcileTradeIntradayExternalList);
		cache.getRunDataHolderCache().put(cache.getImportRun().getId(), holder);
		return holder;
	}


	private List<ReconcileTradeIntradayExternal> getReconcileTradeIntradayExternalList(Date tradeDate, Integer sourceCompanyId) {
		ReconcileTradeIntradayExternalSearchForm searchForm = new ReconcileTradeIntradayExternalSearchForm();
		searchForm.setTradeDate(tradeDate);
		if (sourceCompanyId != null) {
			searchForm.setSourceCompanyId(sourceCompanyId);
		}
		// gets a list of fully populated reconcile trade objects, which mean the match list is populated
		return getReconcileTradeIntradayExternalService().getReconcileTradeExternalList(searchForm);
	}


	/**
	 * Get the data source purpose for the trade load.
	 */
	private MarketDataSourcePurpose getPurpose(IntegrationImportRun run) {
		if (run != null && run.getFile().getFileDefinition().getPriceMarketDataSourcePurposeId() != null) {
			return getMarketDataSourcePurposeService().getMarketDataSourcePurpose(run.getFile().getFileDefinition().getPriceMarketDataSourcePurposeId());
		}
		return getMarketDataSourcePurposeService().getMarketDataSourcePurposeByName(MarketDataSourcePurpose.MARKET_DATA_SOURCE_PURPOSE_NAME_INTRADAY);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String integrationEventName) {
		this.eventName = integrationEventName;
	}


	public ReconcileTradeIntradayExternalService getReconcileTradeIntradayExternalService() {
		return this.reconcileTradeIntradayExternalService;
	}


	public void setReconcileTradeIntradayExternalService(ReconcileTradeIntradayExternalService reconcileTradeIntradayExternalService) {
		this.reconcileTradeIntradayExternalService = reconcileTradeIntradayExternalService;
	}


	public ReconcileTradeIntradayReconciliationService getReconcileTradeIntradayReconciliationService() {
		return this.reconcileTradeIntradayReconciliationService;
	}


	public void setReconcileTradeIntradayReconciliationService(ReconcileTradeIntradayReconciliationService reconcileTradeIntradayReconciliationService) {
		this.reconcileTradeIntradayReconciliationService = reconcileTradeIntradayReconciliationService;
	}


	public MarketDataSourcePurposeService getMarketDataSourcePurposeService() {
		return this.marketDataSourcePurposeService;
	}


	public void setMarketDataSourcePurposeService(MarketDataSourcePurposeService marketDataSourcePurposeService) {
		this.marketDataSourcePurposeService = marketDataSourcePurposeService;
	}


	public IntegrationTradeIntradayService getIntegrationTradeIntradayService() {
		return this.integrationTradeIntradayService;
	}


	public void setIntegrationTradeIntradayService(IntegrationTradeIntradayService integrationTradeIntradayService) {
		this.integrationTradeIntradayService = integrationTradeIntradayService;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public MarketDataSourceCompanyService getMarketDataSourceCompanyService() {
		return this.marketDataSourceCompanyService;
	}


	public void setMarketDataSourceCompanyService(MarketDataSourceCompanyService marketDataSourceCompanyService) {
		this.marketDataSourceCompanyService = marketDataSourceCompanyService;
	}


	public ReconcileHandler getReconcileHandler() {
		return this.reconcileHandler;
	}


	public void setReconcileHandler(ReconcileHandler reconcileHandler) {
		this.reconcileHandler = reconcileHandler;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	private class ExternalTradeLoadDataHolder {

		private final IntegrationImportRun importRun;
		private final Date tradeDate;
		private final List<String> addedErrors = new ArrayList<>();
		private final Map<String, InvestmentSecurity> securityCache = new HashMap<>();
		private final Map<String, InvestmentAccount> holdingAccountCache = new HashMap<>();
		private final Map<String, BusinessCompany> executingBrokerCache = new HashMap<>();
		private final Map<Integer, Boolean> brokerLoadDisabledCache = new HashMap<>();
		private final Map<Integer, RunDataHolder> runDataHolderCache = new HashMap<>();
		private final Map<String, BigDecimal> securityMultiplierCache = new HashMap<>();
		private final Map<String, MultiValueMap<String, ReconcileTradeIntradayExternal>> reconcileTradeCache = new HashMap<>();
		private StringBuilder errors = null;
		private Throwable firstCause = null;


		public ExternalTradeLoadDataHolder(IntegrationImportRun importRun, Date tradeDate) {
			this.importRun = importRun;
			this.tradeDate = tradeDate;
		}


		public Map<String, InvestmentSecurity> getSecurityCache() {
			return this.securityCache;
		}


		public Map<String, InvestmentAccount> getHoldingAccountCache() {
			return this.holdingAccountCache;
		}


		public Map<String, BusinessCompany> getExecutingBrokerCache() {
			return this.executingBrokerCache;
		}


		public Map<Integer, Boolean> getBrokerLoadDisabledCache() {
			return this.brokerLoadDisabledCache;
		}


		public Map<Integer, RunDataHolder> getRunDataHolderCache() {
			return this.runDataHolderCache;
		}


		public Map<String, MultiValueMap<String, ReconcileTradeIntradayExternal>> getReconcileTradeCache() {
			return this.reconcileTradeCache;
		}


		public IntegrationImportRun getImportRun() {
			return this.importRun;
		}


		public Date getTradeDate() {
			return this.tradeDate;
		}


		public StringBuilder getErrors() {
			return this.errors;
		}


		public void setErrors(StringBuilder errors) {
			this.errors = errors;
		}


		public Throwable getFirstCause() {
			return this.firstCause;
		}


		public void setFirstCause(Throwable firstCause) {
			this.firstCause = firstCause;
		}


		public List<String> getAddedErrors() {
			return this.addedErrors;
		}


		public Map<String, BigDecimal> getSecurityMultiplierCache() {
			return this.securityMultiplierCache;
		}
	}

	/**
	 * The <code>RunDataHolder</code> holds the market data and source company objects for a run.
	 *
	 * @author mwacker
	 */
	private class RunDataHolder {

		/**
		 * The data source purposed used for broker, price multiplier and security look-ups.
		 */
		private final MarketDataSourcePurpose marketDataSourcePurpose;
		/**
		 * The data source used for security look-ups.
		 */
		private final MarketDataSource securityMarketDataSource;
		/**
		 * The company from which the trade file arrived.
		 */
		private final BusinessCompany sourceCompany;
		/**
		 * The data source used to look up the executing broker.
		 */
		private final MarketDataSource marketDataSource;

		private final List<ReconcileTradeIntradayExternal> existingReconcileTradeIntradayExternalList;


		public RunDataHolder(MarketDataSourcePurpose marketDataSourcePurpose, MarketDataSource securityMarketDataSource, BusinessCompany sourceCompany, MarketDataSource marketDataSource,
		                     List<ReconcileTradeIntradayExternal> existingReconcileTradeIntradayExternalList) {
			this.marketDataSourcePurpose = marketDataSourcePurpose;
			this.securityMarketDataSource = securityMarketDataSource;
			this.sourceCompany = sourceCompany;
			this.marketDataSource = marketDataSource;
			this.existingReconcileTradeIntradayExternalList = existingReconcileTradeIntradayExternalList;
		}


		public MarketDataSource getSecurityMarketDataSource() {
			return this.securityMarketDataSource;
		}


		public BusinessCompany getSourceCompany() {
			return this.sourceCompany;
		}


		public MarketDataSource getMarketDataSource() {
			return this.marketDataSource;
		}


		public MarketDataSourcePurpose getMarketDataSourcePurpose() {
			return this.marketDataSourcePurpose;
		}


		public List<ReconcileTradeIntradayExternal> getExistingReconcileTradeIntradayExternalList() {
			return this.existingReconcileTradeIntradayExternalList;
		}
	}
}
