package com.clifton.ims.integration.export.content;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.ims.integration.export.content.field.ExportFieldStore;
import com.clifton.ims.integration.export.content.field.ExportTemplateProperties;
import com.clifton.ims.integration.export.content.field.TradeExportFieldStoreFactory;
import com.clifton.ims.integration.export.trade.TradeExportFileFormatTypes;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroup;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.reconcile.trade.intraday.ReconcileTradeIntradayService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.search.TradeSearchForm;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ExportContentTradeFileGenerator implements ExportContentGenerator<List<Trade>> {

	private TradeExportFileFormatTypes fileTemplateFormatType;
	private Short investmentGroupId;
	private Short investmentSecurityGroupId;
	private Integer clientInvestmentAccountGroupId;
	private Integer holdingInvestmentAccountGroupId;
	private Integer custodianCompanyId;
	private Integer brokerCompanyId;
	private Boolean reconciledOnly;
	private String environment;
	private String senderIdentifier;
	/**
	 * Number of days to go back to run the FTP export.
	 */
	private Integer daysFromToday = 1;
	/**
	 * The date generation strategy.
	 */
	private DateGenerationOptions dateGenerationOption;

	////////////////////////////////////////////////////////////////////////////

	private InvestmentAccountGroupService investmentAccountGroupService;
	private BusinessCompanyService businessCompanyService;
	private InvestmentGroupService investmentGroupService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;
	private ReconcileTradeIntradayService reconcileTradeIntradayService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	private TradeService tradeService;
	private TradeDestinationService tradeDestinationService;
	private InvestmentInstrumentService investmentInstrumentService;

	private TradeExportFieldStoreFactory tradeExportFieldStoreFactory;

	private TemplateConverter templateConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Trade> generateContentData(ExportDefinitionContent content, Date previewDate, Map<String, Object> configurationMap) {
		return getTradeList(true, previewDate, configurationMap);
	}


	@Override
	public List<FileWrapper> writeContent(List<Trade> tradeList, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory, Date previewDate) {
		if (!CollectionUtils.isEmpty(tradeList)) {
			MultiValueMap<String, Trade> tradesByType = BeanUtils.getMultiValueMapFromProperty(tradeList, "tradeType.name");

			TradeExportFileFormatTypes templateFormatType = getFileTemplateFormatType();

			if (!templateFormatType.isAllowMultipleTradeTypes() && tradesByType.keyCount() > 1) {
				throw new ValidationException("Content contains multiple trade types. This is not currently supported. Each 'content' must have exactly one type of trade.\n"
						+ "Trade Types included were : " + Arrays.toString(tradesByType.keySet().toArray()));
			}

			ExportFieldStore fieldStore = getTradeExportFieldStoreFactory().buildFieldStore(tradeList);

			// 2 Prepare the template input
			Map<String, Object> input = new HashMap<>();
			input.put(ExportTemplateProperties.FIELD_STORE, fieldStore);
			input.put(ExportTemplateProperties.UNIQUE_EXPORT_ID_PROP, generateUniqueExportFileID(content, runHistory));
			input.put(ExportTemplateProperties.DESTINATION_ENVIRONMENT, getEnvironment());
			input.put(ExportTemplateProperties.EXPORT_DATE, new Date());
			input.put(ExportTemplateProperties.GENERATION_DATE, getGenerationDate(previewDate));
			input.put(ExportTemplateProperties.TRADE_LIST, tradeList);
			input.put(ExportTemplateProperties.TRADE_FILL_LIST_SIZE, getTradeFillListSize(tradeList));
			input.put(ExportTemplateProperties.EXPORT_GENERATOR, this);
			if (getBrokerCompanyId() != null) {
				input.put(ExportTemplateProperties.BROKER_COMPANIES, getBrokerCompanyAndSiblings(getBusinessCompanyService().getBusinessCompany(getBrokerCompanyId())));
			}
			if (!templateFormatType.isAllowMultipleTradeTypes()) {
				input.put(ExportTemplateProperties.TRADE_TYPE, CollectionUtils.getOnlyElement(tradesByType.keySet()));
			}

			try {
				String output = getTemplateConverter().convertFromTemplateFile(templateFormatType.getTemplatePath(), templateFormatType.getClass(), input);
				Path file = Files.createTempFile(FileUtils.getFileNameWithoutExtension(fileName), "." + FileUtils.getFileExtension(fileName));
				Files.write(file, output.getBytes());
				return CollectionUtils.createList(new FileWrapper(file.toFile(), fileName, true));
			}
			catch (IOException e) {
				throw new RuntimeException("Failed to generate export file.", e);
			}
		}

		return null;
	}


	@Override
	public Map<String, Object> getTemplateBeanMap(ExportDefinitionContent content, Date previewDate) {
		Map<String, Object> map = new HashMap<>();
		if (!StringUtils.isEmpty(getSenderIdentifier())) {
			map.put("SENDER_IDENTIFIER", getSenderIdentifier());
			map.put("GENERATION_DATE", getGenerationDate(previewDate));
		}

		return map;
	}


	private Integer getTradeFillListSize(List<Trade> tradeList) {
		int count = 0;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (trade != null && trade.getTradeFillList() != null) {
				count = count + trade.getTradeFillList().size();
			}
		}
		return count;
	}


	private Date getGenerationDate(Date previewDate) {
		return previewDate != null ? previewDate :
				getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
	}


	public Integer getFutureMonth(InvestmentSecurity security) {
		return InvestmentUtils.getFutureMonth(security);
	}


	public Integer getFutureYear(InvestmentSecurity security) {
		return InvestmentUtils.getFutureYear(security);
	}


	public boolean isBrokerCompanyOrSibling(int companyId, Integer[] brokerCompanyIds) {
		return Arrays.stream(brokerCompanyIds).anyMatch(x -> x == companyId);
	}


	public boolean isBrokerCompanyOrSibling(int companyId, Set<Integer> brokerCompanyIds) {
		return brokerCompanyIds.contains(companyId);
	}


	private HashSet<Integer> getBrokerCompanyAndSiblings(BusinessCompany brokerCompany) {
		if (brokerCompany != null) {
			HashSet<Integer> companySet;
			if (brokerCompany.getParent() != null) {
				CompanySearchForm searchForm = new CompanySearchForm();
				searchForm.setCompanyType("Broker");
				searchForm.setParentId(brokerCompany.getParent().getId());
				List<BusinessCompany> companies = getBusinessCompanyService().getBusinessCompanyList(searchForm);
				companySet = new HashSet<>(companies.size());
				for (BusinessCompany company : companies) {
					companySet.add(company.getId());
				}
			}
			else {
				companySet = new HashSet<>(1);
				companySet.add(brokerCompany.getId());
			}
			return companySet;
		}
		return null;
	}


	@Override
	public String getContentLabel() {
		StringBuilder result = new StringBuilder(100);
		result.append(getDateGenerationOption().getLabel());
		result.append(' ');

		InvestmentGroup investmentGroup = getInvestmentGroup();
		if (investmentGroup != null) {
			result.append(investmentGroup.getLabel());
			result.append(' ');
		}
		InvestmentSecurityGroup investmentSecurityGroup = getInvestmentSecurityGroup();
		if (investmentSecurityGroup != null) {
			result.append(investmentSecurityGroup.getLabel());
			result.append(' ');
		}

		InvestmentAccountGroup clientInvestmentAccountGroup = getClientInvestmentAccountGroup();
		InvestmentAccountGroup holdingInvestmentAccountGroup = getHoldingInvestmentAccountGroup();
		BusinessCompany custodianCompany = getCustodianCompany();
		BusinessCompany brokerCompany = getBrokerCompany();

		result.append(" export for ");
		if (clientInvestmentAccountGroup != null) {
			result.append(clientInvestmentAccountGroup.getLabel());
		}
		else if (holdingInvestmentAccountGroup != null) {
			result.append(holdingInvestmentAccountGroup.getLabel());
		}
		else if (custodianCompany != null) {
			result.append(custodianCompany.getLabel());
		}
		else {
			result.append(brokerCompany.getLabel());
		}

		return result.toString();
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public List<Trade> getTradeList(boolean includeIsReconciledOnlyFilter) {
		return getTradeList(includeIsReconciledOnlyFilter, null, null);
	}


	private List<Trade> getTradeList(boolean includeIsReconciledOnlyFilter, Date previewDate, Map<String, Object> configurationMap) {
		Date tradeDate = getGenerationDate(previewDate);

		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setInvestmentGroupId(getInvestmentGroupId());
		if (getClientInvestmentAccountGroupId() != null) {
			searchForm.setClientInvestmentAccountGroupId(getClientInvestmentAccountGroupId());
		}
		if (getHoldingInvestmentAccountGroupId() != null) {
			searchForm.setHoldingInvestmentAccountGroupId(getHoldingInvestmentAccountGroupId());
		}

		searchForm.setInvestmentSecurityGroupId(getInvestmentSecurityGroupId());
		searchForm.setTradeDate(tradeDate);
		searchForm.setCustodianCompanyId(getCustodianCompanyId());
		if (getBrokerCompanyId() != null) {
			searchForm.setBrokerOrIssuingCompanyIds(ArrayUtils.toIntegerArray(getBrokerCompanyAndSiblings(getBusinessCompanyService().getBusinessCompany(getBrokerCompanyId())).toArray()));
		}

		if (includeIsReconciledOnlyFilter && getReconciledOnly()) {
			SearchRestriction reconciledRestriction = new SearchRestriction("reconciled", ComparisonConditions.EQUALS, "true");
			searchForm.addSearchRestriction(reconciledRestriction);
		}
		else {
			//If the reconciled flag is not set, then make sure the trade is at least booked
			searchForm.setWorkflowStateName("Booked");
		}

		if (configurationMap != null) {
			for (Map.Entry<String, Object> entry : configurationMap.entrySet()) {
				BeanUtils.setPropertyValue(searchForm, entry.getKey(), entry.getValue(), true);
			}
		}

		List<Trade> tradeList = getTradeService().getTradeList(searchForm);

		//Populate the trade fill list for each trade
		//TODO This could be changed later to a single query with an 'IN' clause for better performance
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			if (trade.getTradeFillList() == null) {
				trade.setTradeFillList(getTradeService().getTradeFillListByTrade(trade.getId()));
			}
		}

		return tradeList;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public BigDecimal retrieveStrikePrice(InvestmentSecurity security) {
		return security != null ? security.getOptionStrikePrice() : null;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public String retrievePutCall(InvestmentSecurity security) {
		return security != null ? security.getOptionType().toString() : null;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public InvestmentAccountGroup getHoldingInvestmentAccountGroup() {
		if (getHoldingInvestmentAccountGroupId() != null) {
			return getInvestmentAccountGroupService().getInvestmentAccountGroup(getHoldingInvestmentAccountGroupId());
		}
		return null;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public InvestmentAccountGroup getClientInvestmentAccountGroup() {
		if (getClientInvestmentAccountGroupId() != null) {
			return getInvestmentAccountGroupService().getInvestmentAccountGroup(getClientInvestmentAccountGroupId());
		}
		return null;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public BusinessCompany getCustodianCompany() {
		if (getCustodianCompanyId() != null) {
			return getBusinessCompanyService().getBusinessCompany(getCustodianCompanyId());
		}
		return null;
	}


	public BusinessCompany getBrokerCompany() {
		if (getBrokerCompanyId() != null) {
			return getBusinessCompanyService().getBusinessCompany(getBrokerCompanyId());
		}
		return null;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public InvestmentGroup getInvestmentGroup() {
		if (getInvestmentGroupId() != null) {
			return getInvestmentGroupService().getInvestmentGroup(getInvestmentGroupId());
		}
		return null;
	}


	/**
	 * Needs to be public - used by the freemarker templates
	 */
	public InvestmentSecurityGroup getInvestmentSecurityGroup() {
		if (getInvestmentSecurityGroupId() != null) {
			return getInvestmentSecurityGroupService().getInvestmentSecurityGroup(getInvestmentSecurityGroupId());
		}
		return null;
	}


	public boolean isTradeAllocatedViaFix(Trade trade) {
		TradeDestinationMappingSearchCommand command = new TradeDestinationMappingSearchCommand(trade);
		TradeDestinationMapping mapping = getTradeDestinationService().getTradeDestinationMappingByCommand(command);
		return mapping.getMappingConfiguration().isFixPostTradeAllocation();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a unique file ID to be used within the file if necessary.
	 * <p>
	 * Note: JP Morgan requires that this identifier be no more than 14 characters long.
	 *
	 * @param content
	 * @param runHistory
	 */
	private String generateUniqueExportFileID(ExportDefinitionContent content, ExportRunHistory runHistory) {
		StringBuilder idBuilder = new StringBuilder();

		//If the runHistory is not null (meaning it is a real run), generate an ID from the runHistory ID and the content ID
		if (runHistory != null) {
			idBuilder.append('R');
			idBuilder.append(runHistory.getId());
			idBuilder.append('C');
			idBuilder.append(content.getId());
		}
		//Otherwise, generate an ID from the timestamp.
		else {
			idBuilder.append(DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS));
		}

		return idBuilder.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeExportFileFormatTypes getFileTemplateFormatType() {
		return this.fileTemplateFormatType;
	}


	public void setFileTemplateFormatType(TradeExportFileFormatTypes fileTemplateFormatType) {
		this.fileTemplateFormatType = fileTemplateFormatType;
	}


	public TradeExportFieldStoreFactory getTradeExportFieldStoreFactory() {
		return this.tradeExportFieldStoreFactory;
	}


	public void setTradeExportFieldStoreFactory(TradeExportFieldStoreFactory tradeExportFieldStoreFactory) {
		this.tradeExportFieldStoreFactory = tradeExportFieldStoreFactory;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Boolean getReconciledOnly() {
		return this.reconciledOnly;
	}


	public void setReconciledOnly(Boolean reconciledOnly) {
		this.reconciledOnly = reconciledOnly;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public ReconcileTradeIntradayService getReconcileTradeIntradayService() {
		return this.reconcileTradeIntradayService;
	}


	public void setReconcileTradeIntradayService(ReconcileTradeIntradayService reconcileTradeIntradayService) {
		this.reconcileTradeIntradayService = reconcileTradeIntradayService;
	}


	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public Integer getCustodianCompanyId() {
		return this.custodianCompanyId;
	}


	public void setCustodianCompanyId(Integer custodianCompanyId) {
		this.custodianCompanyId = custodianCompanyId;
	}


	public Short getInvestmentSecurityGroupId() {
		return this.investmentSecurityGroupId;
	}


	public void setInvestmentSecurityGroupId(Short investmentSecurityGroupId) {
		this.investmentSecurityGroupId = investmentSecurityGroupId;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public Integer getBrokerCompanyId() {
		return this.brokerCompanyId;
	}


	public void setBrokerCompanyId(Integer brokerCompanyId) {
		this.brokerCompanyId = brokerCompanyId;
	}


	public Integer getHoldingInvestmentAccountGroupId() {
		return this.holdingInvestmentAccountGroupId;
	}


	public void setHoldingInvestmentAccountGroupId(Integer holdingInvestmentAccountGroupId) {
		this.holdingInvestmentAccountGroupId = holdingInvestmentAccountGroupId;
	}


	public String getSenderIdentifier() {
		return this.senderIdentifier;
	}


	public void setSenderIdentifier(String senderIdentifier) {
		this.senderIdentifier = senderIdentifier;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}
}
