package com.clifton.ims.integration.batch.ftp.input;


import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.core.util.ftp.FtpResult;
import com.clifton.core.util.ftp.FtpUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.ims.integration.batch.ftp.AbstractFtpJobBean;
import com.clifton.integration.file.IntegrationFileUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class FtpImportJob extends AbstractFtpJobBean {

	/**
	 * Indicates the that job should reformat the file name by moving the effective date to the front of the file.
	 */
	private Boolean reformatFileNameEffectiveDate;


	@Override
	public Status run(Map<String, Object> context) {
		FtpFileFilter filter = getFtpFileFilter();
		Date reportingDate = getReportingDate();

		FtpConfig ftpConfig = new FtpConfig(getUrl(), getPort(), getFtpProtocol(), getUsername(), getPasswordSecret() != null ? decryptSecuritySecret(getPasswordSecret()).getSecretString() : null);
		Status status = Status.ofMessage("Starting FTP Import from: " + getUrl());
		ftpConfig.setSshPrivateKey(getSftpPrivateKeySecret() != null ? decryptSecuritySecret(getSftpPrivateKeySecret()).getSecretString() : null);
		List<FtpResult> ftpFileResults = processFtpFileFilter(filter, ftpConfig, reportingDate);
		for (FtpResult ftpResult : ftpFileResults) {
			if (ftpResult.isSuccess()) {
				status.addDetail(StatusDetail.CATEGORY_SUCCESS, ftpResult.getFileName());
			}
			else {
				StringBuilder builder = new StringBuilder("Failed to download ftp [").append(FileUtils.combinePath(filter.getFtpServerFolder(), ftpResult.getFileName())).append("].");
				builder.append("\nCAUSED BY ");
				builder.append(ExceptionUtils.getDetailedMessage(ftpResult.getError()));
				status.addError(builder.toString());
			}
			if (ftpResult.hasWarning()) {
				status.addMessage(ftpResult.getWarning());
			}
		}

		status.setMessage("Success: " + status.getDetailListForCategory(StatusDetail.CATEGORY_SUCCESS).size() + " Errors: " + status.getErrorCount());
		status.setActionPerformed(!status.getDetailListForCategory(StatusDetail.CATEGORY_SUCCESS).isEmpty());

		return status;
	}


	protected List<FtpResult> processFtpFileFilter(FtpFileFilter filter, FtpConfig ftpConfig, Date reportingDate) {
		String dateString;
		if (!StringUtils.isEmpty(filter.getDateFormat())) {
			dateString = DateUtils.fromDate(reportingDate, filter.getDateFormat());
		}
		else {
			dateString = DateUtils.fromDate(reportingDate, DateUtils.FIX_DATE_FORMAT_INPUT);
		}

		String fileFilterRegex = filter.getFilterRegex().replace("${DATE}", dateString);

		Converter<String, String> fileNameConverter = null;
		if (getReformatFileNameEffectiveDate()) {
			fileNameConverter = from -> {
				Date effectiveDate = IntegrationFileUtils.getEffectiveDateFromFileName(from);
				if (effectiveDate != null) {
					String fileName = IntegrationFileUtils.getOriginalFileName(from);
					return DateUtils.fromDate(effectiveDate, DateUtils.FIX_DATE_FORMAT_INPUT) + "." + fileName;
				}
				return from;
			};
		}
		// cannot have both reformat effective date and file mappings on the same definition.
		if (fileNameConverter == null && !CollectionUtils.isEmpty(getTranslatedDestinationFileNameMap())) {
			final Map<String, String> fileNameMappings = getTranslatedDestinationFileNameMap().entrySet().stream()
					.filter(e -> !StringUtils.isEmpty(e.getValue()))
					.collect(Collectors.toMap(e -> e.getKey().toUpperCase(), e -> doFreemarkerReportingDateReplacement(e.getValue())));
			fileNameConverter = from -> fileNameMappings.getOrDefault(from.toUpperCase(), from);
		}

		return FtpUtils.get(ftpConfig, filter.getFtpServerFolder(), fileFilterRegex, filter.getTargetPath(), filter.isWriteExtension(), fileNameConverter);
	}


	protected FtpFileFilter getFtpFileFilter() {
		FtpFileFilter result = new FtpFileFilter();
		result.setFtpServerFolder(doFreemarkerReportingDateReplacement(getFtpServerFolder()));
		result.setFilterRegex(getFilterRegex());
		result.setDateFormat(getDateFormat());
		result.setTargetPath(getTargetPath());
		ObjectUtils.doIfPresent(getWriteExtension(), result::setWriteExtension);
		return result;
	}


	protected String doFreemarkerReportingDateReplacement(String template) {
		if (!StringUtils.isEmpty(template)) {
			TemplateConfig config = new TemplateConfig(template);
			config.addBeanToContext("DATE", DateUtils.clearTime(getReportingDate()));
			return getTemplateConverter().convert(config);
		}
		return template;
	}


	protected Date getReportingDate() {
		return getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
	}


	public Boolean getReformatFileNameEffectiveDate() {
		return this.reformatFileNameEffectiveDate;
	}


	public void setReformatFileNameEffectiveDate(Boolean reformatFileNameEffectiveDate) {
		this.reformatFileNameEffectiveDate = reformatFileNameEffectiveDate;
	}
}
