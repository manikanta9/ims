package com.clifton.ims.integration.batch.file;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.integration.file.IntegrationFileUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>IntegrationFileCopyJob</code> is used to copy integration files from one environment to another (e.g. from
 * production to QA or DEMO) in order to process them in the target environment; it checks both the integration input
 * directory and the specified processed directory to ensure that the files have not already been copied/processed.
 * It differs from the {@link FileTransferJob} in that it does not crawl directories recursively, and does not overwrite
 * existing files, but instead only copied files that do not yet exist.
 *
 * @author stevenf
 */

public class IntegrationFileCopyJob implements Task {

	private String fileFilterRegex; //^(.(?!.*\.csv))*$
	private String sourceProcessedPath; //"\\\\MSP-700-06\\Integration\\processed\\broker\\daily"
	private String targetInputPath; //"\\\\MSP-2700-01\\Integration\\input"
	private String[] targetProcessedPathList; //"\\\\MSP-2700-01\\Integration\\processed\\broker\\daily::\\\\\MSP-2700-01\\Integration\\processed\\unknown\\UNKNOWN"


	@Override
	public Status run(Map<String, Object> context) {
		int totalFileCount = 0;
		int totalFilesCopiedCount = 0;
		Status status = Status.ofMessage("Copying files!");
		try {
			List<String> sourceFileList = Arrays.stream(FileContainerFactory.getFileContainer(getSourceProcessedPath()).list()).filter(this::filterFile).collect(Collectors.toList());
			totalFileCount = sourceFileList.size();
			List<String> targetFileList = Arrays.stream(FileContainerFactory.getFileContainer(getTargetInputPath()).list()).filter(this::filterFile).collect(Collectors.toList());
			for (String targetProcessedPath : getTargetProcessedPathList()) {
				targetFileList.addAll(Arrays.stream(FileContainerFactory.getFileContainer(targetProcessedPath).list()).filter(this::filterFile).collect(Collectors.toList()));
			}
			Set<String> sourceFilenameSet = new HashSet<>(sourceFileList);
			Set<String> targetFilenameSet = new HashSet<>(targetFileList);
			for (String sourceFilename : CollectionUtils.getIterable(sourceFilenameSet)) {
				if (!targetFilenameSet.contains(sourceFilename)) {
					String sourceAbsolutePath = FileUtils.combinePath(getSourceProcessedPath(), sourceFilename);
					String targetAbsolutePath = FileUtils.combinePaths(getTargetInputPath(), sourceFilename);
					String targetWriteAbsolutePath = FileUtils.combinePaths(getTargetInputPath(), sourceFilename + "." + IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION);
					//Now that we know the file hasn't been copied yet we can create the FileContainers and copy.
					//The SMB library will hold the object in memory, so we only create right before we copy to avoid memory issues.
					FileContainer sourceFileContainer = FileContainerFactory.getFileContainer(sourceAbsolutePath);
					//Create the 'write' file location and copy the file there (with .write extension)
					FileContainer targetWriteFileContainer = FileContainerFactory.getFileContainer(targetWriteAbsolutePath);
					FileContainer targetFileContainer = FileContainerFactory.getFileContainer(targetAbsolutePath);
					//Now that we've created the fileContainers, check if it is a directory, and skip
					//We can't do this earlier because we want to avoid creating the FileContainers until we have to
					if (!sourceFileContainer.isDirectory()) {
						FileUtils.copyFileCreatePath(sourceFileContainer, targetWriteFileContainer);
						//Now that the file has been written rename it to drop the .write extension and allow integration to process it.
						boolean successful = targetWriteFileContainer.renameToFile(targetFileContainer);
						if (!successful) {
							status.addError(String.format("Could not rename [%s] to [%s].", targetWriteFileContainer.getAbsolutePath(), targetFileContainer.getAbsolutePath()));
						}
						status.addMessage("Copied [" + sourceAbsolutePath + "] to [" + targetAbsolutePath + "]");
						totalFilesCopiedCount++;
					}
					else {
						//If it was a directory then subtract from the total file count.
						totalFileCount--;
					}
				}
			}
		}
		catch (Exception e) {
			status.addError("Failed to copy files from [" + getSourceProcessedPath() + "] to [" + getTargetInputPath() + "]");
			LogUtils.error(getClass(), "Failed to copy files from [" + getSourceProcessedPath() + "] to [" + getTargetInputPath() + "]", e);
		}
		if (status.getErrorCount() == 0) {
			status.setMessage("Complete!  Processed [" + totalFileCount + "] file(s); Copied [" + totalFilesCopiedCount + "]");
		}
		return status;
	}


	public boolean filterFile(FileContainer fileContainer) {
		return filterFile(fileContainer.getName());
	}


	public boolean filterFile(String fileName) {
		return fileName.matches(getFileFilterRegex());
	}

	////////////////////////////////////////////////////////////////////////////
	////////                 Getter & Setter Methods                   /////////
	////////////////////////////////////////////////////////////////////////////


	public String getFileFilterRegex() {
		return this.fileFilterRegex;
	}


	public void setFileFilterRegex(String fileFilterRegex) {
		this.fileFilterRegex = fileFilterRegex;
	}


	public String getSourceProcessedPath() {
		return this.sourceProcessedPath;
	}


	public void setSourceProcessedPath(String sourceProcessedPath) {
		this.sourceProcessedPath = sourceProcessedPath;
	}


	public String getTargetInputPath() {
		return this.targetInputPath;
	}


	public void setTargetInputPath(String targetInputPath) {
		this.targetInputPath = targetInputPath;
	}


	public String[] getTargetProcessedPathList() {
		return this.targetProcessedPathList;
	}


	public void setTargetProcessedPathList(String[] targetProcessedPathList) {
		this.targetProcessedPathList = targetProcessedPathList;
	}
}
