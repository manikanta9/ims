package com.clifton.ims.integration.event.collateral;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.collateral.CollateralService;
import com.clifton.collateral.CollateralType;
import com.clifton.collateral.balance.CollateralBalance;
import com.clifton.collateral.balance.CollateralBalanceFactory;
import com.clifton.collateral.balance.CollateralBalanceService;
import com.clifton.collateral.balance.search.CollateralBalanceSearchForm;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.reconcile.collateral.IntegrationReconcileCollateralDailyImport;
import com.clifton.integration.incoming.reconcile.collateral.IntegrationReconcileCollateralService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.reconcile.ReconcileHandler;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@Component
public class CollateralBalanceEventListener extends BaseEventListener<IntegrationImportEvent> {

	private static final String[] RELATIONSHIP_PURPOSE_NAME_LIST = new String[]{"Mark to Market"};
	private static final String SYSTEM_COLUMN = "ExternalCollateralAccountNumber";

	private BusinessCompanyService businessCompanyService;
	private CollateralService collateralService;
	private InvestmentInstrumentService investmentInstrumentService;
	private IntegrationReconcileCollateralService integrationReconcileCollateralService;
	private ReconcileHandler reconcileHandler;
	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;
	private CollateralBalanceService collateralBalanceService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentAccountService investmentAccountService;
	private SystemColumnValueService systemColumnValueService;
	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();

		List<IntegrationReconcileCollateralDailyImport> collateralDailyList;
		try {
			collateralDailyList = getIntegrationReconcileCollateralService().getIntegrationReconcileCollateralDailyImportListByRun(run.getId());
		}
		catch (Throwable e) {
			throw new RuntimeException("Error retrieving daily collateral for run: " + run.getId(), e);
		}
		AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for Collateral events processing.");

		Integer sourceCompanyId = run.getIntegrationImportDefinition().getFileDefinition().getSource().getSourceCompanyId();
		BusinessCompany issuingCompany = (sourceCompanyId != null) ? getBusinessCompanyService().getBusinessCompany(sourceCompanyId) : null;

		Map<Integer, CollateralType> holdingAccountCollateralTypeMap = new HashMap<>();

		List<Throwable> errors = new ArrayList<>();
		for (IntegrationReconcileCollateralDailyImport collateralDaily : CollectionUtils.getIterable(collateralDailyList)) {
			try {
				// get the account group the holding account belongs to and the primary account for the group
				InvestmentAccountGroupAccount accountGroupAccount = lookupPrimaryInvestmentHoldingAccountGroup(collateralDaily.getAccountNumber(), issuingCompany != null ? issuingCompany.getId() : null);

				InvestmentAccountGroup accountGroup = accountGroupAccount != null ? accountGroupAccount.getReferenceOne() : null;
				InvestmentAccount primaryHoldingAccount = accountGroupAccount != null ? accountGroupAccount.getReferenceTwo() : null;

				InvestmentAccount holdingAccount;
				if (IntegrationImportRun.INTEGRATION_EVENT_BROKER_DAILY_CASH.equals(event.getEventName())) {
					holdingAccount = getMappedInvestmentAccount(collateralDaily.getAccountNumber());
				}
				else {
					holdingAccount = ObjectUtils.coalesce(
							getReconcileHandler().lookupInvestmentHoldingAccount(collateralDaily.getAccountNumber(), issuingCompany != null ? issuingCompany.getId() : null, false),
							primaryHoldingAccount
					);
				}

				if (holdingAccount == null) {
					throw new RuntimeException("Cannot find an investment account for [" + collateralDaily.getAccountNumber() + "] issued by ["
							+ (issuingCompany != null ? issuingCompany.getLabel() : "UNKNOWN") + "]");
				}

				CollateralType collateralType = holdingAccountCollateralTypeMap.computeIfAbsent(holdingAccount.getId(), k -> getCollateralService().getCollateralTypeByAccount(holdingAccount));
				ValidationUtils.assertNotNull(collateralType, "No Collateral Type could be found for account [" + holdingAccount.getLabel() + "]. Collateral Type is required for Collateral event processing.");

				InvestmentSecurity collateralCurrency = null;
				if ((collateralDaily.getCurrencySymbolLocal() != null) && !collateralDaily.getCurrencySymbolLocal().isEmpty()) {
					collateralCurrency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(collateralDaily.getCurrencySymbolLocal(), true);
				}

				BigDecimal externalCollateralRequirement = MathUtils.abs(collateralCurrency != null ? collateralDaily.getCollateralRequirementLocal() : collateralDaily.getCollateralRequirementBase());
				BigDecimal externalCollateralAmount = collateralCurrency != null ? collateralDaily.getCollateralAmountLocal() : collateralDaily.getCollateralAmountBase();
				BigDecimal externalCollateralExcess = collateralCurrency != null ? collateralDaily.getExpectedTransferAmountLocal() : collateralDaily.getExpectedTransferAmountBase();

				if (externalCollateralRequirement == null && externalCollateralAmount == null) {
					continue;
				}

				if (MathUtils.isNullOrZero(externalCollateralRequirement) && MathUtils.isNullOrZero(externalCollateralAmount)
						&& (getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyCount(run.getEffectiveDate(), holdingAccount.getId()) == 0)) {
					continue;
				}

				CollateralBalance collateralInternalDaily = findAccountingCollateralDaily(collateralDaily.getPositionDate(), holdingAccount, collateralCurrency,
						holdingAccount.equals(primaryHoldingAccount) ? accountGroup : null);
				if (collateralInternalDaily == null) {
					collateralInternalDaily = CollateralBalanceFactory.createCollateralBalance(collateralType);

					// May want to use AccountingM2MService.getAccountingM2MInvestmentAccount()
					InvestmentAccount clientAccount = getReconcileHandler().lookupClientInvestmentAccount(holdingAccount.getId(), issuingCompany, holdingAccount.getNumber(),
							RELATIONSHIP_PURPOSE_NAME_LIST);

					collateralInternalDaily.setHoldingInvestmentAccount(holdingAccount);
					if (accountGroup != null && holdingAccount.equals(primaryHoldingAccount)) {
						collateralInternalDaily.setParentCollateralBalance(true);
					}
					collateralInternalDaily.setClientInvestmentAccount(clientAccount);
					collateralInternalDaily.setBalanceDate(run.getEffectiveDate());
					collateralInternalDaily.setPositionsMarketValue(BigDecimal.ZERO);
					collateralInternalDaily.setPostedCollateralValue(BigDecimal.ZERO);
					collateralInternalDaily.setPostedCounterpartyCollateralValue(BigDecimal.ZERO);
					collateralInternalDaily.setPostedCollateralHaircutValue(BigDecimal.ZERO);
					collateralInternalDaily.setPostedCounterpartyCollateralHaircutValue(BigDecimal.ZERO);
					collateralInternalDaily.setTotalCashValue(BigDecimal.ZERO);
					collateralInternalDaily.setAgreedMovementAmount(BigDecimal.ZERO);
				}

				collateralInternalDaily.setExternalCollateralRequirement(externalCollateralRequirement);
				collateralInternalDaily.setExternalCollateralAmount(externalCollateralAmount);
				if (collateralCurrency != null) {
					collateralInternalDaily.setCollateralCurrency(collateralCurrency);
				}
				if (IntegrationImportRun.INTEGRATION_EVENT_BROKER_DAILY_CASH.equals(event.getEventName())) {
					collateralInternalDaily.setExternalExcessCollateral(externalCollateralExcess);
				}

				getCollateralBalanceService().saveCollateralBalance(collateralInternalDaily);
				applyParentChildCollateralBalance(collateralInternalDaily, accountGroupAccount);
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), "Failed to load Collateral for [" + collateralDaily.getAccountNumber() + "]: " + e.getMessage(), e);
				errors.add(e);
			}
		}
		if (!errors.isEmpty()) {
			throw new RuntimeException(errors.get(errors.size() - 1));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected CollateralBalance applyParentChildCollateralBalance(CollateralBalance balance, InvestmentAccountGroupAccount primaryAccountAssignment) {
		CollateralBalance groupBalance = balance;
		if (balance != null && primaryAccountAssignment != null) {
			if (!balance.isParentCollateralBalance()) {
				CollateralBalanceSearchForm sf = new CollateralBalanceSearchForm();
				sf.setBalanceDate(balance.getBalanceDate());
				sf.setHoldingInvestmentAccountId(primaryAccountAssignment.getReferenceTwo().getId());
				sf.setParentCollateralBalance(true);
				if (balance.getCollateralCurrency() != null) {
					sf.setCollateralCurrencyId(balance.getCollateralCurrency().getId());
				}
				groupBalance = CollectionUtils.getOnlyElement(getCollateralBalanceService().getCollateralBalanceList(sf));
			}

			if (groupBalance != null) {
				CollateralBalanceSearchForm sf2 = new CollateralBalanceSearchForm();
				sf2.setBalanceDate(groupBalance.getBalanceDate());
				sf2.setAccountGroupId(primaryAccountAssignment.getReferenceOne().getId());
				sf2.setParentCollateralBalance(false);
				List<CollateralBalance> balanceList = getCollateralBalanceService().getCollateralBalanceList(sf2);

				for (CollateralBalance childBalance : CollectionUtils.getIterable(balanceList)) {
					if (!groupBalance.equals(childBalance.getParent()) && !groupBalance.equals(childBalance)) {
						childBalance.setParent(groupBalance);
						getCollateralBalanceService().saveCollateralBalance(childBalance);
					}
				}
			}
		}
		return groupBalance;
	}


	protected InvestmentAccountGroupAccount lookupPrimaryInvestmentHoldingAccountGroup(String accountNumber, Integer issuingCompanyId) {
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setGroupAlias(accountNumber);
		searchForm.setGroupTypeNameEquals(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		searchForm.setPrimary(true);
		searchForm.setIssuingCompanyId(issuingCompanyId);
		searchForm.setLimit(1);
		List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);
		ValidationUtils.assertFalse(CollectionUtils.getSize(groupAccountList) > 1, "Multiple holding account groups were found for [" + accountNumber + "].");
		return CollectionUtils.isEmpty(groupAccountList) ? null : groupAccountList.get(0);
	}


	protected CollateralBalance findAccountingCollateralDaily(Date balanceDate, InvestmentAccount holdingAccount, InvestmentSecurity collateralCurrency, InvestmentAccountGroup accountGroup) {
		CollateralBalanceSearchForm searchForm = new CollateralBalanceSearchForm();
		searchForm.setHoldingInvestmentAccountId(holdingAccount.getId());
		searchForm.setBalanceDate(balanceDate);
		if (collateralCurrency != null) {
			searchForm.setCollateralCurrencyId(collateralCurrency.getId());
		}
		if (accountGroup != null) {
			searchForm.setAccountGroupId(accountGroup.getId());
			searchForm.setParentCollateralBalance(true);
		}
		else {
			searchForm.setParentCollateralBalance(false);
		}
		return CollectionUtils.getOnlyElement(getCollateralBalanceService().getCollateralBalanceList(searchForm));
	}


	protected InvestmentAccount getMappedInvestmentAccount(String accountNumber) {
		if (!StringUtils.isEmpty(accountNumber)) {
			AssertUtils.assertNotNull(getSystemColumnService().getSystemColumnGroupByName(InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME), "Cannot find group " + InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME);
			List<String> customColumnList = CollectionUtils.getStream(getSystemColumnService().getSystemColumnCustomListForGroup(InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME))
					.map(SystemColumnCustom::getName)
					.filter(SYSTEM_COLUMN::equals)
					.collect(Collectors.toList());
			AssertUtils.assertEquals(customColumnList.size(), 1, "Did not find column " + SYSTEM_COLUMN + " in column group " + InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME);

			SystemColumnValueSearchForm searchForm = new SystemColumnValueSearchForm();
			searchForm.setColumnGroupName(InvestmentAccount.HOLDING_ACCOUNT_COLUMN_GROUP_NAME);
			searchForm.setValue(accountNumber.replaceAll("-", ""));
			searchForm.setColumnName(SYSTEM_COLUMN);
			List<SystemColumnValue> systemColumnValueList = CollectionUtils.asNonNullList(getSystemColumnValueService().getSystemColumnValueList(searchForm));

			if (systemColumnValueList.size() > 1) {
				List<Integer> mappedInvestmentAccountIds = systemColumnValueList.stream()
						.map(SystemColumnValue::getFkFieldId)
						.filter(Objects::nonNull)
						.collect(Collectors.toList());
				List<String> mappedAccountsList = CollectionUtils.getStream(getInvestmentAccountService().getInvestmentAccountListByIds(mappedInvestmentAccountIds))
						.map(InvestmentAccount::getName)
						.collect(Collectors.toList());
				LogUtils.error(this.getClass(), String.format("Too many investment accounts [%s] are mapped to the account number [%s]", mappedAccountsList, accountNumber));
			}
			else if (systemColumnValueList.isEmpty()) {
				LogUtils.error(this.getClass(), String.format("Could not find mapped account using [%s] and account number [%s].", SYSTEM_COLUMN, accountNumber));
			}
			else {
				SystemColumnValue mappedValue = CollectionUtils.getOnlyElement(systemColumnValueList);
				InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(mappedValue.getFkFieldId());
				return (holdingAccount != null && !holdingAccount.getType().isOurAccount()) ? holdingAccount : null;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(IntegrationImportRun.INTEGRATION_EVENT_BROKER_M2M, IntegrationImportRun.INTEGRATION_EVENT_BROKER_DAILY_CASH);
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public ReconcilePositionExternalDailyService getReconcilePositionExternalDailyService() {
		return this.reconcilePositionExternalDailyService;
	}


	public void setReconcilePositionExternalDailyService(ReconcilePositionExternalDailyService reconcilePositionExternalDailyService) {
		this.reconcilePositionExternalDailyService = reconcilePositionExternalDailyService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public ReconcileHandler getReconcileHandler() {
		return this.reconcileHandler;
	}


	public void setReconcileHandler(ReconcileHandler reconcileHandler) {
		this.reconcileHandler = reconcileHandler;
	}


	public CollateralService getCollateralService() {
		return this.collateralService;
	}


	public void setCollateralService(CollateralService collateralService) {
		this.collateralService = collateralService;
	}


	public IntegrationReconcileCollateralService getIntegrationReconcileCollateralService() {
		return this.integrationReconcileCollateralService;
	}


	public void setIntegrationReconcileCollateralService(IntegrationReconcileCollateralService integrationReconcileCollateralService) {
		this.integrationReconcileCollateralService = integrationReconcileCollateralService;
	}


	public CollateralBalanceService getCollateralBalanceService() {
		return this.collateralBalanceService;
	}


	public void setCollateralBalanceService(CollateralBalanceService collateralBalanceService) {
		this.collateralBalanceService = collateralBalanceService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
