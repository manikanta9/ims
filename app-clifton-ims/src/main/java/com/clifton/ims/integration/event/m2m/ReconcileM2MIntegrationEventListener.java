package com.clifton.ims.integration.event.m2m;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionType;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.reconcile.m2m.IntegrationReconcileM2MDailyImport;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.InvestmentAccountGroupType;
import com.clifton.investment.account.group.search.InvestmentAccountGroupSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>ReconcileM2MIntegrationEventListener</code> listens for integration events that indicate there is M2M data ready
 * to be loaded from the integration system to IMS.
 */
@Component
public class ReconcileM2MIntegrationEventListener extends AbstractReconcileM2MIntegrationEventListener {

	private InvestmentAccountGroupService investmentAccountGroupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return IntegrationImportRun.INTEGRATION_EVENT_BROKER_M2M;
	}


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for Collateral events processing.");

		IntegrationImportDefinition importDefinition = run.getIntegrationImportDefinition();
		AssertUtils.assertNotNull(importDefinition.getFileDefinition().getSource(), "An import source is required for M2M events processing.");

		BusinessCompany issuingCompany = getBusinessCompanyService().getBusinessCompany(run.getIntegrationImportDefinition().getFileDefinition().getSource().getSourceCompanyId());

		// this is a small list right now, but may need to be revisited in the future
		Set<String> collateralAccountGroupAliasList = getCollateralAccountGroupAliasList();
		List<Throwable> errors = new ArrayList<>();

		// stores the indicator that mark is in the local currency
		Map<Integer, Boolean> markTypeCache = new HashMap<>();

		if (IntegrationImportDefinitionType.BROKER_IMPORT_ACCOUNT_BALANCES.equals(importDefinition.getType().getName())) {
			List<IntegrationReconcileM2MDailyImport> m2mDailyList = getIntegrationReconcileM2MDailyService().getIntegrationReconcileM2MDailyImportListByRun(run.getId());

			for (IntegrationReconcileM2MDailyImport m2mDaily : CollectionUtils.getIterable(m2mDailyList)) {
				try {
					// skip all collateral group accounts that will throw an error when looked up because they do not exist
					if (collateralAccountGroupAliasList.contains(m2mDaily.getAccountNumber())) {
						continue;
					}
					InvestmentAccount holdingAccount = lookupHoldingAccount(m2mDaily.getAccountNumber(), issuingCompany);

					boolean m2mInLocalCurrency = isM2MinLocalCurrency(holdingAccount, markTypeCache);
					Date positionDate = m2mDaily.getPositionDate();

					InvestmentSecurity markCurrency = null;
					if (m2mInLocalCurrency && (m2mDaily.getCurrencySymbolLocal() != null) && !m2mDaily.getCurrencySymbolLocal().isEmpty()) {
						markCurrency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(m2mDaily.getCurrencySymbolLocal(), true);
					}

					BigDecimal expectedTransferAmount = markCurrency != null ? m2mDaily.getExpectedTransferAmountLocal() : m2mDaily.getExpectedTransferAmountBase();
					if ((expectedTransferAmount.compareTo(BigDecimal.ZERO) == 0) && (getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyCount(run.getEffectiveDate(), holdingAccount.getId()) == 0)) {
						continue;
					}

					AccountingM2MDaily m2mInternalDaily = findAccountingM2MDaily(positionDate, holdingAccount, markCurrency);
					if (m2mInternalDaily == null) {
						m2mInternalDaily = buildNewM2MDaily(holdingAccount, positionDate, markCurrency, m2mInLocalCurrency);
					}
					//Skip if booked and/or reconciled
					else if (m2mInternalDaily.isBooked() || m2mInternalDaily.isReconciled()) {
						continue;
					}

					BigDecimal expectedExpenseAmountAdjustment = BigDecimal.ZERO;
					// only look up expenses for existing entries
					if (!m2mInternalDaily.isNewBean()) {
						expectedExpenseAmountAdjustment = getAccountingM2MService().getExpectedExpenseAmountAdjustment(m2mInternalDaily);
					}

					m2mInternalDaily.setReconciled(false);
					m2mInternalDaily.setExpectedTransferAmount(MathUtils.subtract(expectedTransferAmount, expectedExpenseAmountAdjustment));
					getAccountingM2MService().saveAccountingM2MDaily(m2mInternalDaily);
				}
				catch (Throwable e) {
					errors.add(e);
				}
			}
		}

		if (!errors.isEmpty()) {
			throw new RuntimeException(errors.get(errors.size() - 1));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Set<String> getCollateralAccountGroupAliasList() {
		InvestmentAccountGroupSearchForm searchForm = new InvestmentAccountGroupSearchForm();
		searchForm.setTypeNameEquals(InvestmentAccountGroupType.INVESTMENT_ACCOUNT_GROUP_TYPE_RELATED_COLLATERAL_NAME);
		return getInvestmentAccountGroupService().getInvestmentAccountGroupList(searchForm).stream().map(InvestmentAccountGroup::getGroupAlias).collect(Collectors.toSet());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}
}
