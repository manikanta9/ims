package com.clifton.ims.integration.export.retriever;


import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;

import java.util.Date;


/**
 * The <code>ExportClientPortalFileSourceRetriever</code> looks up the source id for BusinessClient table.
 *
 * @author mwacker
 */
public class ExportSourceRetrieverBusinessClient implements ExportSourceRetriever {

	private InvestmentAccountService investmentAccountService;


	@Override
	public Integer getSourceId(Integer clientInvestmentAccountId, @SuppressWarnings("unused") String sourceSystemTableName, @SuppressWarnings("unused") Date exportDate) {
		InvestmentAccount ia = getInvestmentAccountService().getInvestmentAccount(clientInvestmentAccountId);
		return ia != null ? ia.getBusinessClient().getId() : null;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}
}
