package com.clifton.ims.integration.event.corporate.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.integration.event.BaseIntegrationEventListener;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateAction;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateActionService;
import com.clifton.integration.incoming.corporate.action.search.IntegrationCorporateActionSearchForm;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BooleanSupplier;


/**
 * <code>CorporateActionEventListener</code> loads data from the IntegrationCorporateAction table
 * into InvestmentSecurityEvent or InvestmentSecurityEventPayout in IMS.
 * <p>
 * Processes each corporate action event in a single run within its own transaction and catches and logs all exceptions.
 * Errors encountered during processing can be resolved and the file reprocessed, duplicate corporate actions will be identified by
 * the {@link IntegrationCorporateAction#corporateActionIdentifier} and will result in updates to existing events not duplicates.
 *
 * @author TerryS
 */
@Component
public class CorporateActionEventListener extends BaseIntegrationEventListener {

	// Must match IntegrationImportDefinitionType#EventName
	public final static String EVENT_NAME = "Corporate Action Import";

	private CorporateActionEventUtilHandler corporateActionEventUtilHandler;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;

	private IntegrationCorporateActionService integrationCorporateActionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for Corporate Action events processing.");

		// get from integration
		IntegrationCorporateActionSearchForm integrationCorporateActionSearchForm = new IntegrationCorporateActionSearchForm();
		integrationCorporateActionSearchForm.setImportRunId(run.getId());
		List<IntegrationCorporateAction> integrationCorporateActions = getIntegrationCorporateActionService().getIntegrationCorporateActionList(integrationCorporateActionSearchForm);

		ErrorMessageFormatter errorFormatter = new ErrorMessageFormatter(integrationCorporateActions.size());
		for (IntegrationCorporateAction integrationCorporateAction : integrationCorporateActions) {
			Long corporateActionIdentifier = integrationCorporateAction.getCorporateActionIdentifier();
			ValidationUtils.assertNotNull(corporateActionIdentifier, "The Corporate Action Identifier is required " + integrationCorporateAction.getIdentity());

			// determine if IMS supports this event type
			if (!getCorporateActionEventUtilHandler().isInvestmentSecurityEventTypeSupported(integrationCorporateAction.getEventTypeName())) {
				LogUtils.warn(this.getClass(), String.format("Skipping corporate action [%s] due to missing event type with name [%s]",
						corporateActionIdentifier, integrationCorporateAction.getEventTypeName()));
				continue;
			}

			InvestmentSecurityEvent investmentSecurityEvent = getInvestmentSecurityEventByCorporateActionIdentifier(corporateActionIdentifier);
			if (integrationCorporateAction.getPayoutNumber() != null) {
				if (investmentSecurityEvent == null) {
					processCorporateActionEvent(corporateActionIdentifier, () -> createInvestmentSecurityEventAndPayout(integrationCorporateAction), errorFormatter);
				}
				else {
					InvestmentSecurityEventPayout existingPayout = getInvestmentSecurityEventPayoutByEventAndElectionAndPayoutNumber(investmentSecurityEvent, integrationCorporateAction.getElectionNumber(), integrationCorporateAction.getPayoutNumber());
					if (existingPayout == null) {
						processCorporateActionEvent(corporateActionIdentifier, () -> createInvestmentSecurityEventPayout(integrationCorporateAction, investmentSecurityEvent), errorFormatter);
					}
					else {
						processCorporateActionEvent(corporateActionIdentifier, () -> updateInvestmentSecurityEventPayout(integrationCorporateAction, investmentSecurityEvent, existingPayout), errorFormatter);
					}
				}
			}
			else {
				if (investmentSecurityEvent == null) {
					processCorporateActionEvent(corporateActionIdentifier, () -> createInvestmentSecurityEvent(integrationCorporateAction), errorFormatter);
				}
				else {
					processCorporateActionEvent(corporateActionIdentifier, () -> updateInvestmentSecurityEvent(integrationCorporateAction, investmentSecurityEvent), errorFormatter);
				}
			}
		}
		// provide the user a list of errors
		if (errorFormatter.isError()) {
			String formattedErrorMessagesSb = errorFormatter.getErrorMessage();
			// The import run has a maximum message length, output the entire message to the log.
			LogUtils.error(CorporateActionEventListener.class, formattedErrorMessagesSb);
			throw new RuntimeException(String.format("Failed to import [%s] of [%s] corporate actions because of these errors:%n%n %s.%n",
					errorFormatter.getErrorCount(), errorFormatter.getRunTotal(), formattedErrorMessagesSb));
		}
	}


	@Override
	public String getEventName() {
		return EVENT_NAME;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Allow exceptions to be thrown out of this method, individual event exceptions must trigger rollbacks.
	 * Returns true if an update was to a corporate event or payout.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected boolean doProcessCorporateActionEvent(BooleanSupplier processor) {
		return processor.getAsBoolean();
	}


	/**
	 * Enable processing a single corporate action event in its own transaction.  Catch and log all exceptions to allow processing subsequent
	 * corporate action events; also keep the transaction time smaller.
	 */
	private void processCorporateActionEvent(Long corporateActionIdentifier, BooleanSupplier processor, ErrorMessageFormatter errorFormatter) {
		try {
			doProcessCorporateActionEvent(processor);
		}
		catch (Exception e) {
			errorFormatter.addException(e, corporateActionIdentifier);
		}
	}


	private InvestmentSecurityEvent getInvestmentSecurityEventByCorporateActionIdentifier(Long corporateActionIdentifier) {
		AssertUtils.assertNotNull(corporateActionIdentifier, "Corporate Action Identifier is required.");

		InvestmentSecurityEventSearchForm investmentSecurityEventSearchForm = new InvestmentSecurityEventSearchForm();
		investmentSecurityEventSearchForm.setCorporateActionIdentifier(corporateActionIdentifier);

		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(investmentSecurityEventSearchForm);
		ValidationUtils.assertFalse(CollectionUtils.getSize(eventList) > 1, () -> String.format("There should be only one InvestmentSecurityEvent with corporate identifier [%s].", corporateActionIdentifier));
		return CollectionUtils.getOnlyElement(eventList);
	}


	private InvestmentSecurityEventPayout getInvestmentSecurityEventPayoutByEventAndElectionAndPayoutNumber(InvestmentSecurityEvent investmentSecurityEvent, Short electionNumber, Short payoutNumber) {
		AssertUtils.assertNotNull(electionNumber, "Corporate Action Election Number is required.");
		AssertUtils.assertNotNull(payoutNumber, "Corporate Action Payout Number is required.");
		AssertUtils.assertNotNull(investmentSecurityEvent, "Investment Security Event is required.");

		InvestmentSecurityEventPayoutSearchForm investmentSecurityEventPayoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		investmentSecurityEventPayoutSearchForm.setSecurityEventId(investmentSecurityEvent.getId());
		investmentSecurityEventPayoutSearchForm.setElectionNumber(electionNumber);
		investmentSecurityEventPayoutSearchForm.setPayoutNumber(payoutNumber);
		return CollectionUtils.getOnlyElement(getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(investmentSecurityEventPayoutSearchForm));
	}


	private boolean createInvestmentSecurityEvent(IntegrationCorporateAction integrationCorporateAction) {
		AssertUtils.assertNotNull(integrationCorporateAction, "Corporate Action Identifier is required.");

		// create event
		InvestmentSecurityEvent investmentSecurityEvent = getCorporateActionEventUtilHandler().buildInvestmentSecurityEvent(integrationCorporateAction);
		getInvestmentSecurityEventService().saveInvestmentSecurityEvent(investmentSecurityEvent);

		return true;
	}


	private boolean updateInvestmentSecurityEvent(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEvent investmentSecurityEvent) {
		AssertUtils.assertNotNull(integrationCorporateAction, "Corporate Action Identifier is required.");
		AssertUtils.assertNotNull(investmentSecurityEvent, "The investment security event is required.");

		// update event if new or changed
		if (getCorporateActionEventUtilHandler().updateInvestmentSecurityEvent(integrationCorporateAction, investmentSecurityEvent)) {
			getInvestmentSecurityEventService().saveInvestmentSecurityEvent(investmentSecurityEvent);
			return true;
		}

		return false;
	}


	private boolean createInvestmentSecurityEventAndPayout(IntegrationCorporateAction integrationCorporateAction) {
		AssertUtils.assertNotNull(integrationCorporateAction, "Corporate Action Identifier is required.");

		// create event
		InvestmentSecurityEvent investmentSecurityEvent = getCorporateActionEventUtilHandler().buildInvestmentSecurityEvent(integrationCorporateAction);

		// create payout
		InvestmentSecurityEventPayout investmentSecurityEventPayout = getCorporateActionEventUtilHandler().buildInvestmentSecurityEventPayout(integrationCorporateAction);
		investmentSecurityEventPayout.setSecurityEvent(investmentSecurityEvent);

		// update select fields on event from payout
		updateInvestmentSecurityEventFromPayout(investmentSecurityEventPayout, investmentSecurityEvent);

		getInvestmentSecurityEventService().saveInvestmentSecurityEvent(investmentSecurityEvent);
		getInvestmentSecurityEventPayoutService().saveInvestmentSecurityEventPayout(investmentSecurityEventPayout);

		return true;
	}


	private boolean createInvestmentSecurityEventPayout(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEvent investmentSecurityEvent) {
		AssertUtils.assertNotNull(integrationCorporateAction, "Corporate Action Identifier is required.");
		AssertUtils.assertNotNull(investmentSecurityEvent, "The investment security event is required.");

		// apply any event updates not on payout (e.g. ex date, record date, event date, declare date, status, etc.)
		boolean eventHasUpdates = getCorporateActionEventUtilHandler().updateInvestmentSecurityEvent(integrationCorporateAction, investmentSecurityEvent);
		// create payout
		InvestmentSecurityEventPayout investmentSecurityEventPayout = getCorporateActionEventUtilHandler().buildInvestmentSecurityEventPayout(integrationCorporateAction);
		// conditionally update event, if event is new or payout is first default payout
		if (updateInvestmentSecurityEventFromPayout(investmentSecurityEventPayout, investmentSecurityEvent) || eventHasUpdates) {
			investmentSecurityEvent = getInvestmentSecurityEventService().saveInvestmentSecurityEvent(investmentSecurityEvent);
		}
		// set event on payout and save
		investmentSecurityEventPayout.setSecurityEvent(investmentSecurityEvent);
		getInvestmentSecurityEventPayoutService().saveInvestmentSecurityEventPayout(investmentSecurityEventPayout);

		return true;
	}


	private boolean updateInvestmentSecurityEventPayout(IntegrationCorporateAction integrationCorporateAction, InvestmentSecurityEvent investmentSecurityEvent, InvestmentSecurityEventPayout existingInvestmentSecurityEventPayout) {
		// apply any event updates not on payout (e.g. ex date, record date, event date, declare date, status, etc.)
		boolean eventHasUpdates = getCorporateActionEventUtilHandler().updateInvestmentSecurityEvent(integrationCorporateAction, investmentSecurityEvent);
		if (getCorporateActionEventUtilHandler().updateInvestmentSecurityEventPayout(integrationCorporateAction, existingInvestmentSecurityEventPayout) || eventHasUpdates) {
			// conditionally update event, if event is new or payout is first default payout
			if (updateInvestmentSecurityEventFromPayout(existingInvestmentSecurityEventPayout, investmentSecurityEvent) || eventHasUpdates) {
				getInvestmentSecurityEventService().saveInvestmentSecurityEvent(investmentSecurityEvent);
			}
			getInvestmentSecurityEventPayoutService().saveInvestmentSecurityEventPayout(existingInvestmentSecurityEventPayout);
			return true;
		}
		return false;
	}


	/**
	 * Update event fields from payout until the corporate actions are completely migrated to payouts. Returns true if the event was updated by the payout information.
	 */
	private boolean updateInvestmentSecurityEventFromPayout(InvestmentSecurityEventPayout investmentSecurityEventPayout, InvestmentSecurityEvent investmentSecurityEvent) {
		if (!investmentSecurityEventPayout.isDeleted()) {
			boolean updateEventFields = investmentSecurityEvent.isNewBean();
			if (!updateEventFields) {
				if (investmentSecurityEventPayout.isDefaultElection()) {
					// update event details for default, non-deleted payout with lowest payout number
					InvestmentSecurityEventPayoutSearchForm payoutSearchForm = getPayoutSearchFrom(investmentSecurityEvent);
					payoutSearchForm.setDefaultElection(Boolean.TRUE);
					updateEventFields = shouldUpdateEventFields(investmentSecurityEventPayout, getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(payoutSearchForm));
				}
				else if (investmentSecurityEvent.isVoluntary()) {
					// update voluntary event details for non-deleted payout with lowest payout number unless a default payout exists for this event
					InvestmentSecurityEventPayoutSearchForm payoutSearchForm = getPayoutSearchFrom(investmentSecurityEvent);
					List<InvestmentSecurityEventPayout> payoutList = getInvestmentSecurityEventPayoutService().getInvestmentSecurityEventPayoutList(payoutSearchForm);
					Optional<InvestmentSecurityEventPayout> defaultPayout = CollectionUtils.getStream(payoutList).filter(InvestmentSecurityEventPayout::isDefaultElection).findFirst();
					if (!defaultPayout.isPresent()) {
						updateEventFields = shouldUpdateEventFields(investmentSecurityEventPayout, payoutList);
					}
				}
			}
			if (updateEventFields) {
				return updateEventFields(investmentSecurityEvent, investmentSecurityEventPayout);
			}
		}
		return false;
	}


	private InvestmentSecurityEventPayoutSearchForm getPayoutSearchFrom(InvestmentSecurityEvent investmentSecurityEvent) {
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setSecurityEventId(investmentSecurityEvent.getId());
		payoutSearchForm.setDeleted(Boolean.FALSE);
		payoutSearchForm.setOrderBy("payoutNumber:asc");
		payoutSearchForm.setLimit(1);
		return payoutSearchForm;
	}


	private boolean shouldUpdateEventFields(InvestmentSecurityEventPayout investmentSecurityEventPayout, List<InvestmentSecurityEventPayout> payoutList) {
		InvestmentSecurityEventPayout firstPayout = CollectionUtils.getFirstElement(payoutList);
		return firstPayout == null || firstPayout.getPayoutNumber() >= investmentSecurityEventPayout.getPayoutNumber();
	}


	private boolean updateEventFields(InvestmentSecurityEvent investmentSecurityEvent, InvestmentSecurityEventPayout investmentSecurityEventPayout) {
		InvestmentSecurityEvent beforeInvestmentSecurityEvent = BeanUtils.cloneBean(investmentSecurityEvent, false, false);
		if (investmentSecurityEvent.getType().isAdditionalSecurityAllowed()) {
			ObjectUtils.doIfPresent(investmentSecurityEventPayout.getPayoutSecurity(), investmentSecurityEvent::setAdditionalSecurity);
		}
		ObjectUtils.doIfPresent(investmentSecurityEventPayout.getBeforeEventValue(), investmentSecurityEvent::setBeforeEventValue);
		ObjectUtils.doIfPresent(investmentSecurityEventPayout.getAfterEventValue(), investmentSecurityEvent::setAfterEventValue);

		// determine if the event object changed.
		List<String> updatedPropertyList = CoreCompareUtils.getNoEqualPropertiesWithSetters(beforeInvestmentSecurityEvent, investmentSecurityEvent, false, "id");
		return investmentSecurityEvent.isNewBean() || !CollectionUtils.isEmpty(updatedPropertyList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CorporateActionEventUtilHandler getCorporateActionEventUtilHandler() {
		return this.corporateActionEventUtilHandler;
	}


	public void setCorporateActionEventUtilHandler(CorporateActionEventUtilHandler corporateActionEventUtilHandler) {
		this.corporateActionEventUtilHandler = corporateActionEventUtilHandler;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public IntegrationCorporateActionService getIntegrationCorporateActionService() {
		return this.integrationCorporateActionService;
	}


	public void setIntegrationCorporateActionService(IntegrationCorporateActionService integrationCorporateActionService) {
		this.integrationCorporateActionService = integrationCorporateActionService;
	}


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Logs all exception error message with its corresponding {@link IntegrationCorporateAction#corporateActionIdentifier}
	 * Suppress the stack trace for validation exceptions.
	 * Present the same validation error once along with a list of identifiers.
	 * Put all validations messages first and unknown exceptions with their stack traces last.
	 */
	private static class ErrorMessageFormatter {

		private final int runTotal;
		private final Map<String, List<Long>> exceptions = new HashMap<>();
		private final Map<Long, String> stackTraces = new HashMap<>();


		ErrorMessageFormatter(int runTotal) {
			this.runTotal = runTotal;
		}


		int getRunTotal() {
			return this.runTotal;
		}


		void addException(Exception exception, Long corporateActionNumber) {
			if (exception instanceof ValidationException) {
				this.exceptions.putIfAbsent(exception.getMessage(), new ArrayList<>());
				this.exceptions.get(exception.getMessage()).add(corporateActionNumber);
			}
			else if (findValidationExceptionRecursive(exception) != null) {
				Exception validationException = (Exception) findValidationExceptionRecursive(exception);
				String message = validationException.getMessage();
				this.exceptions.putIfAbsent(message, new ArrayList<>());
				this.exceptions.get(message).add(corporateActionNumber);
			}
			else {
				String message = exception.getMessage();
				if (StringUtils.isEmpty(message)) {
					message = exception.getClass().getSimpleName();
				}
				this.exceptions.putIfAbsent(message, new ArrayList<>());
				this.exceptions.get(message).add(corporateActionNumber);

				this.stackTraces.put(corporateActionNumber, ExceptionUtils.getFullStackTrace(exception));
			}
		}


		Throwable findValidationExceptionRecursive(Throwable throwable) {
			if (throwable instanceof ValidationException) {
				return throwable;
			}
			if (throwable.getCause() == null) {
				return null;
			}
			return findValidationExceptionRecursive(throwable.getCause());
		}


		boolean isError() {
			return !this.exceptions.isEmpty() || !this.stackTraces.isEmpty();
		}


		int getErrorCount() {
			return this.stackTraces.size() + (int) this.exceptions.values().stream().mapToLong(List::size).sum();
		}


		String getErrorMessage() {
			StringBuilder errorMessagesSb = new StringBuilder();
			for (Map.Entry<String, List<Long>> entry : this.exceptions.entrySet()) {
				errorMessagesSb.append(entry.getKey());
				errorMessagesSb.append("\n");
				errorMessagesSb.append("\tCAID(s):");
				errorMessagesSb.append(entry.getValue());
				errorMessagesSb.append("\n\n");
			}
			for (Map.Entry<Long, String> entry : this.stackTraces.entrySet()) {
				errorMessagesSb.append("\n");
				errorMessagesSb.append("Begin Stack [");
				errorMessagesSb.append(entry.getKey());
				errorMessagesSb.append("]\n");
				errorMessagesSb.append(entry.getValue());
				errorMessagesSb.append("End Stack [");
				errorMessagesSb.append(entry.getKey());
				errorMessagesSb.append("]\n");
			}
			char[] hr = new char[80];
			Arrays.fill(hr, '_');
			errorMessagesSb.append(hr);
			errorMessagesSb.append("\n");

			return errorMessagesSb.toString();
		}
	}
}
