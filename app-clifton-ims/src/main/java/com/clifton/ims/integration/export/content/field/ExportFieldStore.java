package com.clifton.ims.integration.export.content.field;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.trade.Trade;

import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ExportFieldStore</code> is a wrapper for outgoing fields for an export. It is intended
 * to be used as a means for pre-processing fields that are not easily accessible during template processing.
 *
 * @author jgommels
 */
public class ExportFieldStore {

	private final Map<BusinessCompany, String> brokerBICMap = new HashMap<>();
	private final Map<InvestmentExchange, String> exchangeMICMap = new HashMap<>();
	private final Map<InvestmentSecurity, String> securityYellowKeyMap = new HashMap<>();
	private final Map<Trade, String> custodianAccountNumberMap = new HashMap<>();

	/**
	 * Maps a Trade to a property Map, which maps a property name to a value.
	 */
	private final Map<Trade, Map<String, Object>> tradePropertyMap = new HashMap<>();


	//These methods throw an exception if a match is not found since this lookup will occur during template processing.
	public String getBIC(BusinessCompany broker) {
		if (!this.brokerBICMap.containsKey(broker)) {
			throw new ValidationException("No Business Identifier Code (BIC) could be found for the broker [" + broker.getName() + "]");
		}

		return this.brokerBICMap.get(broker);
	}


	public String getMIC(InvestmentExchange exchange) {
		if (!this.exchangeMICMap.containsKey(exchange)) {
			throw new ValidationException("No Market Identifier Code (MIC) could be found for the exchange [" + exchange.getName() + "]");
		}

		return this.exchangeMICMap.get(exchange);
	}


	public String getYellowKey(InvestmentSecurity security) {
		if (!this.securityYellowKeyMap.containsKey(security)) {
			throw new ValidationException("No yellow key could be found for the security [" + security.getSymbol() + "]");
		}

		return this.securityYellowKeyMap.get(security);
	}


	public String getCustodianAccountNumber(Trade trade) {
		if (!this.custodianAccountNumberMap.containsKey(trade)) {
			throw new ValidationException("No Custodian Account Number could be found for the trade [" + trade.getId() + "]");
		}

		return this.custodianAccountNumberMap.get(trade);
	}


	public Object getPropertyForTrade(Trade trade, String property) {
		return this.tradePropertyMap.get(trade).get(property);
	}


	public void addBIC(BusinessCompany broker, String bic) {
		this.brokerBICMap.put(broker, bic);
	}


	public void addMIC(InvestmentExchange exchange, String mic) {
		this.exchangeMICMap.put(exchange, mic);
	}


	public void addYellowKey(InvestmentSecurity security, String yellowKey) {
		this.securityYellowKeyMap.put(security, yellowKey);
	}


	public void addCustodianAccountNumber(Trade trade, String accountNumber) {
		this.custodianAccountNumberMap.put(trade, accountNumber);
	}


	public void addTradeProperties(Trade trade, Map<String, Object> propertyMap) {
		this.tradePropertyMap.put(trade, propertyMap);
	}
}
