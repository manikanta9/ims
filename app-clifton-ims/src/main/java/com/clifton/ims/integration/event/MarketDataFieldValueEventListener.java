package com.clifton.ims.integration.event;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.marketdata.field.IntegrationMarketDataFieldService;
import com.clifton.integration.incoming.marketdata.field.IntegrationMarketDataFieldValue;
import com.clifton.integration.incoming.marketdata.field.search.IntegrationMarketDataFieldValueSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataFieldSearchForm;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>MarketDataExchangeRatesEventListener</code> loads data from the IntegrationMarketExchangeRate table
 * into the MarketDataExchangeRate table in IMS.  It only loads rates for foreign currencies to USD.  It does not load
 * exchange rates between foreign currencies, nor does it load inverse rates from USD to foreign.  Also, it filters out currencies
 * that are not defined in our system.
 *
 * @author rbrooks
 */
@Component
public class MarketDataFieldValueEventListener extends BaseIntegrationEventListener {

	private static final String EVENT_NAME = IntegrationImportRun.INTEGRATION_EVENT_MARKET_DATA_FIELD_VALUE_IMPORT;

	private IntegrationMarketDataFieldService integrationMarketDataFieldService;
	private InvestmentInstrumentService investmentInstrumentService;
	private BusinessCompanyService businessCompanyService;
	private MarketDataFieldService marketDataFieldService;


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for Collateral events processing.");

		// Get new exchange rates from Integration
		IntegrationMarketDataFieldValueSearchForm searchForm = new IntegrationMarketDataFieldValueSearchForm();
		searchForm.setRunId(run.getId());
		List<IntegrationMarketDataFieldValue> newMarketDataFieldValueList = getIntegrationMarketDataFieldService().getIntegrationMarketDataFieldValueList(searchForm);

		// Lookup MarketDataSource from SourceCompany
		Integer sourceCompanyId = run.getIntegrationImportDefinition().getFileDefinition().getSource().getSourceCompanyId();
		AssertUtils.assertNotNull(sourceCompanyId, "Integration source [" + run.getIntegrationImportDefinition().getFileDefinition().getSource().getName() + "] does not have a source company id defined.");
		BusinessCompany sourceCompany = getBusinessCompanyService().getBusinessCompany(sourceCompanyId);

		MarketDataSource marketDataSource = getMarketDataSourceForCompany(sourceCompany);

		AssertUtils.assertNotNull(marketDataSource, "No MarketDataSource exists for [" + sourceCompany.getLabel() + "].");

		List<Throwable> errors = new ArrayList<>();
		for (IntegrationMarketDataFieldValue newFieldValue : CollectionUtils.getIterable(newMarketDataFieldValueList)) {
			try {
				// get the market data field
				MarketDataField field = getMarketDataField(newFieldValue.getMarketDataFieldName());

				// get the investment security
				InvestmentSecurity security = null;
				if (!StringUtils.isEmpty(newFieldValue.getInvestmentSecuritySymbol())) {
					security = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(newFieldValue.getInvestmentSecuritySymbol(), null);
				}
				if (security == null && !StringUtils.isEmpty(newFieldValue.getInvestmentSecurityCusip())) {
					SecuritySearchForm securitySearchForm = new SecuritySearchForm();
					securitySearchForm.setCusipEquals(newFieldValue.getInvestmentSecurityCusip());
					List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
					if (CollectionUtils.getSize(securityList) == 1) {
						security = CollectionUtils.getFirstElement(securityList);
					}
				}
				ValidationUtils.assertNotNull(security, "No security found for symbol [" + newFieldValue.getInvestmentSecuritySymbol() + "] or CUSIP [" + newFieldValue.getInvestmentSecurityCusip() + "].");

				MarketDataValue marketDataValue = new MarketDataValue();
				marketDataValue.setDataSource(marketDataSource);
				marketDataValue.setDataField(field);
				marketDataValue.setInvestmentSecurity(security);

				marketDataValue.setMeasureDate(newFieldValue.getMeasureDate());
				marketDataValue.setMeasureTime(newFieldValue.getMeasureTime());
				marketDataValue.setMeasureValue(new BigDecimal(newFieldValue.getMeasureValue()));

				getMarketDataFieldService().saveMarketDataValueWithOptions(marketDataValue, true);
			}
			catch (Throwable e) {
				errors.add(e);
			}
		}
		if (!errors.isEmpty()) {
			throw new RuntimeException(errors.get(errors.size() - 1));
		}
	}


	@Override
	public String getEventName() {
		return EVENT_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataField getMarketDataField(String fieldName) {
		MarketDataField field = getMarketDataFieldService().getMarketDataFieldByName(fieldName);
		if (field == null) {
			MarketDataFieldSearchForm sf = new MarketDataFieldSearchForm();
			sf.setExternalFieldName(fieldName);
			List<MarketDataField> fl = getMarketDataFieldService().getMarketDataFieldList(sf);
			ValidationUtils.assertTrue(fl.size() <= 1, "Cannot run job because more than one MarketDataField with external name [" + fieldName + "] was found.");
			if (fl.size() == 1) {
				field = fl.get(0);
			}
		}
		ValidationUtils.assertNotNull(field, "Could not find market data field with name [" + fieldName + "]");
		return field;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public IntegrationMarketDataFieldService getIntegrationMarketDataFieldService() {
		return this.integrationMarketDataFieldService;
	}


	public void setIntegrationMarketDataFieldService(IntegrationMarketDataFieldService integrationMarketDataFieldService) {
		this.integrationMarketDataFieldService = integrationMarketDataFieldService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
