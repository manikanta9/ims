package com.clifton.ims.integration.export.content;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileExtendedSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileSetupService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A system bean that will generate an export file from an uploaded Portal File.
 *
 * @author KellyJ
 */
public class ExportContentPortalFileGeneratorImpl implements ExportContentPortalFileGenerator<List<? extends PortalFile>>, ValidationAware {

	/**
	 * The client account that the portal file was posted for.
	 */
	private Integer assignedPortalEntityId;

	/**
	 * The expanded file category that the file is a member of.
	 */
	private Short fileCategoryIdExpanded;

	/**
	 * Number of days to go back to run the FTP export.
	 */
	private Integer daysFromToday = 1;

	/**
	 * The date generation strategy.
	 */
	private DateGenerationOptions exportDateOption;

	private boolean useDownloadedFileName;

	////////////////////////////////////////////////////////////////////////////

	private Date exportDate;

	private PortalFileService portalFileService;
	private PortalEntityService portalEntityService;
	private PortalFileSetupService portalFileSetupService;
	private ExportRunService exportRunService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;


	////////////////////////////////////////////////////////////////////////////
	@Override
	public boolean isContentReady(ExportDefinitionContent content) {
		boolean result = false;
		List<? extends PortalFile> fileList = getPortalFile(content);
		if (!CollectionUtils.isEmpty(fileList)) {
			result = fileList.stream().allMatch(file -> file != null && file.isApproved());
		}
		return result;
	}


	/**
	 * Example messages -
	 * Single file:  "(Portal file [ATestFile2_2017-10-16_07_27_56.117.txt] is ready for [Client Account: 040001: 1998 Frank Batten, Jr. Trust-ETFs] on [10/16/2017])"
	 * Multiple files:  "(3 portal files (txt) are ready for [Client Account: 040001: 1998 Frank Batten, Jr. Trust-ETFs] - [Documentation / Legal Documentation / Investment Guidelines] on [10/13/2017])"
	 *
	 * @param content
	 */
	@Override
	public String getComparisonTrueMessage(ExportDefinitionContent content) {
		List<? extends PortalFile> fileList = getPortalFile(content);
		ValidationUtils.assertNotEmpty(fileList, "No portal files found.");
		PortalFile file = fileList.get(0);
		if (fileList.size() == 1) {
			return String.format("(Portal file [%s] is ready for [%s] on [%s])", file.getFileName(), file.getPostPortalEntity().getEntityLabelWithSourceSection(), DateUtils.fromDateShort(file.getReportDate()));
		}
		else {
			return String.format("(%d portal files (%s) are ready for [%s] - [%s] on [%s])", fileList.size(), file.getDocumentFileType(), file.getPostPortalEntity().getEntityLabelWithSourceSection(), file.getFileCategory().getLabelExpanded(), DateUtils.fromDateShort(file.getReportDate()));
		}
	}


	/**
	 * Example message - "(No portal file for [Documentation / Legal Documentation / Investment Guidelines] is ready for [Client Account: 040001: 1998 Frank Batten, Jr. Trust-ETFs] on [10/13/2017])"
	 *
	 * @param content
	 */
	@Override
	public String getComparisonFalseMessage(ExportDefinitionContent content) {
		return String.format("(No portal file for [%s] is ready for [%s] on [%s])", getPortalFileCategory().getLabelExpanded(), getPortalEntity().getEntityLabelWithSourceSection(), DateUtils.fromDateShort(getExportDate()));
	}


	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FileWrapper> writeContent(Object data, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory, Date previewDate) {
		if (previewDate != null) {
			setExportDate(getCalendarDateGenerationHandler().generateDate(getExportDateOption(), previewDate, getDaysFromToday()));
		}

		List<? extends PortalFile> portalFileList = getPortalFile(content);
		ValidationUtils.assertNotEmpty(portalFileList, "No Portal File found.");

		boolean isMultipleFiles = portalFileList.size() > 1;
		List<FileWrapper> resultList = new ArrayList<>();

		for (PortalFile portalFile : portalFileList) {
			FileWrapper file = getPortalFileService().downloadPortalFile(portalFile.getId(), null);
			ValidationUtils.assertNotNull(file, "Unable to download Portal File with ID: " + portalFile.getId());
			if (!isMultipleFiles && !isUseDownloadedFileName()) {
				file.setFileName(fileName);
			}
			resultList.add(file);
		}
		return resultList;
	}


	@Override
	public List<? extends PortalFile> getPortalFile(ExportDefinitionContent content) {
		PortalFileExtendedSearchForm searchForm = new PortalFileExtendedSearchForm();
		searchForm.setApproved(Boolean.TRUE);
		searchForm.setFileNameAndPathPopulated(Boolean.TRUE);
		searchForm.setAssignedPortalEntityId(getAssignedPortalEntityId());
		searchForm.setFileCategoryIdExpanded(getFileCategoryIdExpanded());
		searchForm.setReportDate(getExportDate());

		// check that the approval date > last sent date to ensure only one file is picked up.
		Date lastSentDate = getLastSentDate(content);
		if (lastSentDate != null) {
			searchForm.addSearchRestriction(new SearchRestriction("approvalDate", ComparisonConditions.GREATER_THAN, lastSentDate));
		}

		return getPortalFileService().getPortalFileExtendedList(searchForm);
	}


	protected Date getLastSentDate(ExportDefinitionContent content) {
		ExportRunHistory exportRunHistory = getExportRunService().getExportRunHistoryLatestSuccessful(content.getDefinition().getId());
		return exportRunHistory != null ? exportRunHistory.getEndDate() : null;
	}


	/**
	 * Used for generating the SystemBean name
	 * format:  [DateOption] [[PortalFileCategoryLabelExpanded]] for [PortalEntityLabelWithSourceSection]
	 * example:  'Previous Business Day [Documentation / Legal Documentation / Client Authorization Form] for Client Account: 040200: 3M Canada Company Master Trust'
	 */
	@Override
	public String getContentLabel() {
		StringBuilder result = new StringBuilder(100);
		result.append(getExportDateOption().getLabel());
		PortalEntity portalEntity = getPortalEntity();
		PortalFileCategory portalFileCategory = this.getPortalFileCategory();
		if (portalFileCategory != null && !StringUtils.isEmpty(portalFileCategory.getLabelExpanded())) {
			result.append(" [");
			result.append(this.getPortalFileCategory().getLabelExpanded());
			result.append("]");
		}
		result.append(" for ");
		result.append(portalEntity.getEntityLabelWithSourceSection());
		return result.toString();
	}


	@Override
	public Date getExportDate() {
		if (this.exportDate == null) {
			this.exportDate = getCalendarDateGenerationHandler().generateDate(getExportDateOption(), getDaysFromToday());
		}
		return this.exportDate;
	}


	public PortalEntity getPortalEntity() {
		return getPortalEntityService().getPortalEntity(getAssignedPortalEntityId());
	}


	public PortalFileCategory getPortalFileCategory() {
		return getFileCategoryIdExpanded() != null ? getPortalFileSetupService().getPortalFileCategory(getFileCategoryIdExpanded()) : null;
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getAssignedPortalEntityId(), "Portal entity is required.");
		ValidationUtils.assertNotNull(getFileCategoryIdExpanded(), "File Category is required.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getAssignedPortalEntityId() {
		return this.assignedPortalEntityId;
	}


	public void setAssignedPortalEntityId(Integer assignedPortalEntityId) {
		this.assignedPortalEntityId = assignedPortalEntityId;
	}


	public Short getFileCategoryIdExpanded() {
		return this.fileCategoryIdExpanded;
	}


	public void setFileCategoryIdExpanded(Short fileCategoryIdExpanded) {
		this.fileCategoryIdExpanded = fileCategoryIdExpanded;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getExportDateOption() {
		return this.exportDateOption;
	}


	public void setExportDateOption(DateGenerationOptions exportDateOption) {
		this.exportDateOption = exportDateOption;
	}


	public void setExportDate(Date exportDate) {
		this.exportDate = exportDate;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}


	public boolean isUseDownloadedFileName() {
		return this.useDownloadedFileName;
	}


	public void setUseDownloadedFileName(boolean useDownloadedFileName) {
		this.useDownloadedFileName = useDownloadedFileName;
	}
}
