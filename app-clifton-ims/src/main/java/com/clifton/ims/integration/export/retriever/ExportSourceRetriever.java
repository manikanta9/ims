package com.clifton.ims.integration.export.retriever;


import java.util.Date;


/**
 * The <code>ExportClientPortalFileSourceRetriever</code> defines the methods used to look up the
 * ID of the source entity for file export.
 *
 * @author mwacker
 */
public interface ExportSourceRetriever {

	public Integer getSourceId(Integer clientInvestmentAccountId, String sourceSystemTableName, Date exportDate);
}
