package com.clifton.ims.integration.export.destination;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.company.contact.BusinessCompanyContactSearchForm;
import com.clifton.business.company.contact.BusinessCompanyContactService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.destination.ExportDestinationEmailGenerator;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Retrieves all contacts for a given Client Investment Account that have a given type
 *
 * @author theodorez
 */
public class ExportDestinationInvestmentAccountEmailGenerator extends ExportDestinationEmailGenerator {

	private InvestmentAccountService investmentAccountService;
	private BusinessCompanyContactService businessCompanyContactService;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * The key to be used to retrieve the Client Account
	 */
	private String clientInvestmentAccountIdVariableName;

	/**
	 * An optional Client Account (if no value is retrieved from the configuration map this will be used)
	 */
	private Integer clientInvestmentAccountId;

	/**
	 * The types of contacts that the email should be sent To
	 */
	private List<Short> toContactTypeIds;
	/**
	 * The types of contacts that should be CC'd on the email
	 */
	private List<Short> ccContactTypeIds;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generateDestination(ExportDefinitionDestination destination, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		Integer clientAccountId = getClientInvestmentAccountId();
		if (!StringUtils.isEmpty(getClientInvestmentAccountIdVariableName())) {
			Object clientIdConfiguration = configurationMap.get(getClientInvestmentAccountIdVariableName());
			ValidationUtils.assertNotNull(clientIdConfiguration, "No Client Investment Account ID was found using variable name: " + getClientInvestmentAccountIdVariableName());
			clientAccountId = Integer.parseInt((String) clientIdConfiguration);
		}

		ValidationUtils.assertNotNull(clientAccountId, "A Client Investment Account ID was not passed in via Dynamic Configuration OR configured directly on the destination");

		Map<ExportMapKeys, String> resultMap = super.generateDestination(destination, exportDefinition, files, configurationMap);

		InvestmentAccount clientInvestmentAccount = getInvestmentAccountService().getInvestmentAccount(clientAccountId);
		BusinessClient businessClient = clientInvestmentAccount.getBusinessClient();

		if (!CollectionUtils.isEmpty(getToContactTypeIds())) {
			List<BusinessCompanyContact> toCompanyContacts = getCompanyContactsForCompanyAndContactType(businessClient.getCompany(), getToContactTypeIds());
			addCompanyContactsToEmail(toCompanyContacts, ExportMapKeys.EMAIL_TO, resultMap);
		}

		if (!CollectionUtils.isEmpty(getCcContactTypeIds())) {
			List<BusinessCompanyContact> ccCompanyContacts = getCompanyContactsForCompanyAndContactType(businessClient.getCompany(), getCcContactTypeIds());
			addCompanyContactsToEmail(ccCompanyContacts, ExportMapKeys.EMAIL_CC, resultMap);
		}

		return resultMap;
	}


	private void addCompanyContactsToEmail(List<BusinessCompanyContact> companyContacts, ExportMapKeys key, Map<ExportMapKeys, String> resultMap) {
		List<Integer> contactIds = CollectionUtils.getStream(companyContacts).map(companyContact -> companyContact.getReferenceTwo().getId()).collect(Collectors.toList());
		addEmailAddresses(contactIds, key, resultMap);
	}


	private List<BusinessCompanyContact> getCompanyContactsForCompanyAndContactType(BusinessCompany company, List<Short> contactTypeIds) {
		BusinessCompanyContactSearchForm companyContactSearchForm = new BusinessCompanyContactSearchForm();
		companyContactSearchForm.setCompanyId(company.getId());
		companyContactSearchForm.setIncludeClientOrClientRelationship(true);
		companyContactSearchForm.setActive(true);
		companyContactSearchForm.setContactTypeIds(CollectionUtils.toArray(contactTypeIds, Short.class));
		return getBusinessCompanyContactService().getBusinessCompanyContactList(companyContactSearchForm);
	}


	@Override
	public String getDestinationLabel() {
		StringBuilder result = new StringBuilder(100);
		int toCount = CollectionUtils.getSize(getToContactTypeIds());
		if (toCount > 0) {
			result.append(getBusinessContactService().getBusinessContactType(getToContactTypeIds().get(0)).getLabel());
			if (toCount > 1) {
				result.append(", ...: ");
			}
			else {
				result.append(": ");
			}
		}
		result.append(getSubject());
		return result.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyContactService getBusinessCompanyContactService() {
		return this.businessCompanyContactService;
	}


	public void setBusinessCompanyContactService(BusinessCompanyContactService businessCompanyContactService) {
		this.businessCompanyContactService = businessCompanyContactService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	////////////////////////////////////////////////////////////////////////////


	public String getClientInvestmentAccountIdVariableName() {
		return this.clientInvestmentAccountIdVariableName;
	}


	public void setClientInvestmentAccountIdVariableName(String clientInvestmentAccountIdVariableName) {
		this.clientInvestmentAccountIdVariableName = clientInvestmentAccountIdVariableName;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public List<Short> getToContactTypeIds() {
		return this.toContactTypeIds;
	}


	public void setToContactTypeIds(List<Short> toContactTypeIds) {
		this.toContactTypeIds = toContactTypeIds;
	}


	public List<Short> getCcContactTypeIds() {
		return this.ccContactTypeIds;
	}


	public void setCcContactTypeIds(List<Short> ccContactTypeIds) {
		this.ccContactTypeIds = ccContactTypeIds;
	}
}
