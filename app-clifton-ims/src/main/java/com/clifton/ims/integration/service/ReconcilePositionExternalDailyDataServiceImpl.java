package com.clifton.ims.integration.service;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.reconcile.IntegrationReconcilePositionHistory;
import com.clifton.integration.incoming.reconcile.IntegrationReconcileService;
import com.clifton.integration.incoming.reconcile.search.IntegrationReconcilePositionHistorySearchForm;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourcePriceMultiplier;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.marketdata.datasource.MarketDataSourceSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.search.MarketDataSourceSecuritySearchForm;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDaily;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailySearchForm;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Formatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author TerryS
 */
@Service
public class ReconcilePositionExternalDailyDataServiceImpl implements ReconcilePositionExternalDailyDataService {

	public static final int DEFAULT_PRICE_DB_COLUMN_SCALE = 15;

	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;
	private IntegrationReconcileService integrationReconcileService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentAccountService investmentAccountService;
	private BusinessCompanyService businessCompanyService;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	private static final Consumer<String> doNothing = s -> {};

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void loadReconcilePositionExternalDailyData(IntegrationImportRun run, MarketDataSourcePurpose purpose) {
		ReconcilePositionLoaderCommand command = new ReconcilePositionLoaderCommand(run, purpose, this::getPriceMultiplier);
		List<ReconcilePositionExternalDaily> existingReconcilePositionList = getReconcilePositionExternalDaily(command.getRun());
		getReconcilePositionExternalDailyService().deleteReconcilePositionExternalDaily(existingReconcilePositionList);
		List<ReconcilePositionExternalDaily> reconcilePositionExternalDailyList = createReconcilePositionExternalDailyList(command);
		getReconcilePositionExternalDailyService().saveReconcilePositionExternalDaily(reconcilePositionExternalDailyList);
		if (command.hasErrors()) {
			String errorMessage = String.format("Errors during position load [%s]", command.getErrorMessages());
			LogUtils.error(this.getClass(), errorMessage);
			throw new RuntimeException(errorMessage);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<ReconcilePositionExternalDaily> createReconcilePositionExternalDailyList(ReconcilePositionLoaderCommand command) {
		List<ReconcilePositionExternalDaily> results = new ArrayList<>();

		IntegrationReconcilePositionHistorySearchForm searchForm = new IntegrationReconcilePositionHistorySearchForm();
		searchForm.setRunId(command.getRun().getId());
		searchForm.setPositionDate(command.getRun().getEffectiveDate());

		List<IntegrationReconcilePositionHistory> historyList = getIntegrationReconcileService().getIntegrationReconcilePositionHistoryList(searchForm);
		for (IntegrationReconcilePositionHistory history : CollectionUtils.getIterable(historyList)) {
			InvestmentAccount investmentAccount = getInvestmentAccount(history.getAccountNumber(), s -> command.addError(history.getAccountNumber(), s));
			InvestmentSecurity investmentSecurity = getInvestmentSecurity(command.getRun(), history.getSecuritySymbol(), s -> command.addError(history.getAccountNumber(), s));
			if (investmentSecurity != null) {
				BusinessCompany sourceCompany = getSourceCompany(history, s -> command.addError(history.getAccountNumber(), s));
				if (sourceCompany != null) {
					if (investmentAccount != null) {
						if (sourceCompany.equals(investmentAccount.getIssuingCompany()) || isParentCompany(investmentAccount.getIssuingCompany(), sourceCompany)) {
							ReconcilePositionExternalDaily reconcilePositionExternalDaily = buildReconcilePositionExternalDaily(command, history);
							results.add(reconcilePositionExternalDaily);
						}
						else {
							command.addError(history.getAccountNumber(), String.format("Source Company [%s] is not the same as the accounting issuing company [%s].",
									sourceCompany.getLabel(), investmentAccount.getIssuingCompany().getLabel()));
							command.addError(history.getAccountNumber(), String.format("Source Company [%s] is not a child of [%s].", sourceCompany.getLabel(),
									investmentAccount.getIssuingCompany().getLabel()));
						}
					}
				}
			}
		}
		return results;
	}


	private ReconcilePositionExternalDaily buildReconcilePositionExternalDaily(ReconcilePositionLoaderCommand command, IntegrationReconcilePositionHistory history) {
		ReconcilePositionExternalDaily reconcilePositionExternalDaily = new ReconcilePositionExternalDaily();

		InvestmentAccount holdingAccount = getInvestmentAccount(history.getAccountNumber(), s -> command.addError(history.getAccountNumber(), s));
		BusinessCompany issuingCompany = Optional.ofNullable(holdingAccount).map(InvestmentAccount::getIssuingCompany).orElse(null);
		InvestmentAccount clientAccount = getClientAccountByNumber(history.getClientAccountNumber(), issuingCompany, doNothing);
		InvestmentSecurity investmentSecurity = getInvestmentSecurity(command.getRun(), history.getSecuritySymbol(), s -> command.addError(history.getAccountNumber(), s));
		InvestmentSecurity investmentCurrency = getInvestmentSecurity(command.getRun(), history.getPositionCurrencySymbol(), true, s -> command.addError(history.getAccountNumber(), s));
		InvestmentSecurity baseCurrency = getInvestmentSecurity(command.getRun(), history.getAccountCurrencySymbol(), true, doNothing);
		BusinessCompany sourceCompany = getSourceCompany(history, s -> command.addError(history.getAccountNumber(), s));
		Key key = new Key(command.getPurpose(), investmentSecurity, sourceCompany);
		BigDecimal multiplier = command.getMultiplier(key);

		reconcilePositionExternalDaily.setHoldingAccount(holdingAccount);
		reconcilePositionExternalDaily.setClientAccount(clientAccount);
		reconcilePositionExternalDaily.setInvestmentSecurity(investmentSecurity);
		reconcilePositionExternalDaily.setInvestmentCurrency(investmentCurrency);
		reconcilePositionExternalDaily.setBaseCurrency(baseCurrency);
		reconcilePositionExternalDaily.setPositionDate(history.getPositionDate());
		reconcilePositionExternalDaily.setTradeDate(history.getTradeDate());
		reconcilePositionExternalDaily.setSettleDate(history.getSettleDate());
		reconcilePositionExternalDaily.setFxRate(history.getFxRate());
		reconcilePositionExternalDaily.setQuantity(history.isShortPosition() ? MathUtils.negate(history.getQuantity()) : history.getQuantity());
		reconcilePositionExternalDaily.setShortPosition(history.isShortPosition());
		reconcilePositionExternalDaily.setRemainingCostBasisLocal(history.getCostBasisLocal());
		reconcilePositionExternalDaily.setRemainingCostBasisBase(history.getCostBasisBase());
		reconcilePositionExternalDaily.setMarketValueLocal(history.getMarketValueLocal());
		reconcilePositionExternalDaily.setMarketValueBase(history.getMarketValueBase());
		reconcilePositionExternalDaily.setOpenTradeEquityLocal(history.getOpenTradeEquityLocal());
		reconcilePositionExternalDaily.setOpenTradeEquityBase(history.getOpenTradeEquityBase());
		reconcilePositionExternalDaily.setTradePrice(MathUtils.multiply(history.getTradePrice(), multiplier, DEFAULT_PRICE_DB_COLUMN_SCALE));
		reconcilePositionExternalDaily.setMarketPrice(MathUtils.multiply(history.getMarketPrice(), multiplier, DEFAULT_PRICE_DB_COLUMN_SCALE));

		return reconcilePositionExternalDaily;
	}


	private InvestmentAccount getClientAccountByNumber(String accountNumber, BusinessCompany issuingCompany, Consumer<String> messages) {
		if (StringUtils.isEmpty(accountNumber)) {
			messages.accept("The client account number is required.");
		}
		else {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setNumber(accountNumber);
			ObjectUtils.doIfPresent(issuingCompany, c -> searchForm.setIssuingCompanyId(c.getId()));
			List<InvestmentAccount> results = getInvestmentAccountService().getInvestmentAccountList(searchForm);
			if (results.isEmpty()) {
				messages.accept(String.format("Could not find client account [%s] for issuer [%s].", accountNumber, Optional.ofNullable(issuingCompany).map(BusinessCompany::getId).orElse(-1)));
			}
			return getInvestmentAccountService().getInvestmentAccountByNumber(accountNumber);
		}
		return null;
	}


	private MarketDataSource getDataSource(IntegrationImportRun run) {
		return Optional.of(run)
				.map(IntegrationImportRun::getIntegrationImportDefinition)
				.map(IntegrationImportDefinition::getDataSourceName)
				.map(n -> getMarketDataSourceService().getMarketDataSourceByName(n))
				.orElse(null);
	}


	private MarketDataSource getPriceMarketDataSource(BusinessCompany sourceCompany) {
		MarketDataSource marketDataSource = null;
		if (sourceCompany != null) {
			if (sourceCompany.getParent() != null) {
				marketDataSource = getMarketDataSourceService().getMarketDataSourceByCompany(sourceCompany.getParent());
			}
			if (marketDataSource == null) {
				marketDataSource = getMarketDataSourceService().getMarketDataSourceByCompany(sourceCompany);
			}
		}
		return marketDataSource;
	}


	private BigDecimal getPriceMultiplier(Key key) {
		if (key.isValid()) {
			MarketDataSource priceDataSource = getPriceMarketDataSource(key.getSourceCompany());
			if (priceDataSource != null) {
				MarketDataSourcePriceMultiplier priceMultiplier = getMarketDataSourceService().getMarketDataSourcePriceMultiplierByDataSource(key.getInvestmentSecurity().getInstrument(), priceDataSource.getName(), key.getPurpose());
				if (priceMultiplier != null) {
					BigDecimal multiplier = priceMultiplier.getAdditionalPriceMultiplier();
					return multiplier != null ? multiplier : BigDecimal.ONE;
				}
			}
		}
		return BigDecimal.ONE;
	}


	private boolean isParentCompany(BusinessCompany issuingCompany, BusinessCompany sourceCompany) {
		if (issuingCompany != null && sourceCompany != null && sourceCompany.getParent() != null) {
			if (Objects.equals(sourceCompany.getParent(), issuingCompany)) {
				return true;
			}
			else {
				List<BusinessCompany> childrenList = getBusinessCompanyService().getBusinessCompanyChildrenListForParent(sourceCompany.getParent().getId());
				return childrenList.contains(issuingCompany);
			}
		}
		return false;
	}


	private BusinessCompany getSourceCompany(IntegrationReconcilePositionHistory history, Consumer<String> messages) {
		BusinessCompany results = null;
		Integer sourceCompanyId = Optional.of(history)
				.map(IntegrationReconcilePositionHistory::getRun)
				.map(IntegrationImportRun::getIntegrationImportDefinition)
				.map(IntegrationImportDefinition::getFileDefinition)
				.map(IntegrationFileDefinition::getSource)
				.map(IntegrationSource::getSourceCompanyId)
				.orElse(null);
		if (sourceCompanyId == null) {
			messages.accept(String.format("Source company is required on history [%s] definition source.", history.getId()));
		}
		else {
			results = getBusinessCompanyService().getBusinessCompany(sourceCompanyId);
			if (results == null) {
				messages.accept("Could not load source company " + sourceCompanyId);
			}
		}
		return results;
	}


	private InvestmentAccount getInvestmentAccount(String accountNumber, Consumer<String> messages) {
		InvestmentAccount investmentAccount = getInvestmentAccountService().getInvestmentAccountByNumber(accountNumber);
		if (investmentAccount == null) {
			investmentAccount = getInvestmentAccountService().getInvestmentAccountByNumber(StringUtils.removeAll(accountNumber, "-"));
		}
		if (investmentAccount == null) {
			messages.accept(String.format("Could not find account [%s]", accountNumber));
		}
		return investmentAccount;
	}


	private MarketDataSourceSecurity getMarketDataSourceSecurity(IntegrationImportRun run, String symbol) {
		MarketDataSource dataSource = getDataSource(run);

		if (dataSource != null && !StringUtils.isEmpty(symbol)) {
			MarketDataSourceSecuritySearchForm searchForm = new MarketDataSourceSecuritySearchForm();
			searchForm.setDataSourceSymbol(symbol);
			searchForm.setDataSourceId(dataSource.getId());

			List<MarketDataSourceSecurity> securityList = getMarketDataSourceService().getMarketDataSourceSecurityList(searchForm);
			return CollectionUtils.getSize(securityList) == 1 ? securityList.get(0) : null;
		}
		return null;
	}


	private MarketDataSourceSector getMarketDataSourceSector(String symbol, MarketDataSource marketDataSource) {
		if (!StringUtils.isEmpty(symbol) && marketDataSource != null && symbol.indexOf(' ') >= 0) {
			try {
				String sectorName = symbol.substring(symbol.indexOf(' ') + 1);
				return getMarketDataSourceService().getMarketDataSourceSectorByNameAndDataSourceName(sectorName, marketDataSource.getName());
			}
			catch (Exception e) {
				// suppress
			}
		}
		return null;
	}


	private InvestmentSecurity getInvestmentSecurity(IntegrationImportRun run, String symbol, Consumer<String> messages) {
		return getInvestmentSecurity(run, symbol, false, messages);
	}


	private InvestmentSecurity getInvestmentSecurity(IntegrationImportRun run, String symbol, boolean isCurrency, Consumer<String> messages) {
		InvestmentSecurity investmentSecurity = null;

		if (!StringUtils.isEmpty(symbol)) {
			MarketDataSourceSecurity marketDataSourceSecurity = getMarketDataSourceSecurity(run, symbol);
			if (marketDataSourceSecurity != null) {
				investmentSecurity = marketDataSourceSecurity.getSecurity();
			}
			else {
				MarketDataSourceSector marketDataSourceSector = getMarketDataSourceSector(symbol, getDataSource(run));
				if (marketDataSourceSector != null) {
					symbol = symbol.substring(0, symbol.indexOf(' '));
				}
				try {
					investmentSecurity = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(symbol, isCurrency);
				}
				catch (Exception e) {
					messages.accept(ExceptionUtils.getDetailedMessage(e));
				}
				if (investmentSecurity == null && marketDataSourceSector != null) {
					List<InvestmentSecurity> securityListBySymbol = getInvestmentInstrumentService().getInvestmentSecurityListBySymbol(symbol);
					List<InvestmentSecurity> matches = new ArrayList<>();
					for (InvestmentSecurity security : CollectionUtils.getIterable(securityListBySymbol)) {
						if (!security.isCurrency()) {
							MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), marketDataSourceSector.getDataSource());
							if (marketDataSourceSector.equals(mapping.getMarketSector())) {
								matches.add(security);
							}
						}
					}
					if (CollectionUtils.isSingleElement(matches)) {
						investmentSecurity = matches.get(0);
					}
				}
			}
			if (investmentSecurity == null) {
				messages.accept(String.format("Investment Security could not be found with symbol [%s]", symbol));
			}
		}
		else {
			messages.accept("Investment security symbol is empty.");
		}

		return investmentSecurity;
	}


	private List<ReconcilePositionExternalDaily> getReconcilePositionExternalDaily(IntegrationImportRun run) {
		ReconcilePositionExternalDailySearchForm searchForm = new ReconcilePositionExternalDailySearchForm();
		searchForm.setPositionDate(run.getEffectiveDate());
		List<Integer> issuingCompanyList = getPositionHistoryIssuingCompanyList(run);
		if (!issuingCompanyList.isEmpty()) {
			searchForm.setHoldingCompanyIdList(issuingCompanyList.toArray(new Integer[0]));
			return getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyList(searchForm);
		}
		return Collections.emptyList();
	}


	private List<Integer> getPositionHistoryIssuingCompanyList(IntegrationImportRun run) {
		if (run != null && !run.isNewBean()) {
			IntegrationReconcilePositionHistorySearchForm searchForm = new IntegrationReconcilePositionHistorySearchForm();
			searchForm.setRunId(run.getId());
			return getIntegrationReconcileService().getIntegrationReconcilePositionHistoryList(searchForm).stream()
					.map(IntegrationReconcilePositionHistory::getAccountNumber)
					.filter(Objects::nonNull)
					.flatMap(n -> n.contains("-") ? Stream.of(n, n.replaceAll("-", "")) : Stream.of(n))
					.distinct()
					.map(n -> getInvestmentAccountService().getInvestmentAccountByNumber(n))
					.filter(Objects::nonNull)
					.map(InvestmentAccount::getIssuingCompany)
					.map(BusinessCompany::getId)
					.distinct()
					.collect(Collectors.toList());
		}
		return Collections.emptyList();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private static class ReconcilePositionLoaderCommand {

		private final Map<Key, BigDecimal> cache = new HashMap<>();

		private final IntegrationImportRun run;
		private final MarketDataSourcePurpose purpose;
		private final Function<Key, BigDecimal> supplier;


		private final Map<String, Set<String>> accountErrors = new HashMap<>();


		public ReconcilePositionLoaderCommand(IntegrationImportRun run, MarketDataSourcePurpose purpose, Function<Key, BigDecimal> supplier) {
			this.run = run;
			this.purpose = purpose;
			this.supplier = supplier;
		}


		public void addError(String accountId, String format, Object... args) {
			String message = new Formatter().format(format, args).toString();
			Set<String> accounts = this.accountErrors.computeIfAbsent(message, k -> new HashSet<>());
			accounts.add(accountId);
		}


		public boolean hasErrors() {
			return !this.accountErrors.isEmpty();
		}


		public String getErrorMessages() {
			return this.accountErrors.entrySet().stream()
					.map(e -> String.format("%s accounts %s", e.getKey(), e.getValue()))
					.collect(Collectors.joining("\n"));
		}


		public BigDecimal getMultiplier(Key key) {
			if (!this.cache.containsKey(key)) {
				BigDecimal value = key.isValid() ? this.supplier.apply(key) : BigDecimal.ONE;
				this.cache.put(key, value);
			}
			return this.cache.get(key);
		}


		public IntegrationImportRun getRun() {
			return this.run;
		}


		public MarketDataSourcePurpose getPurpose() {
			return this.purpose;
		}
	}

	private static class Key {

		private final MarketDataSourcePurpose purpose;
		private final InvestmentSecurity investmentSecurity;
		private final BusinessCompany sourceCompany;

		private final boolean valid;


		public Key(MarketDataSourcePurpose purpose, InvestmentSecurity investmentSecurity, BusinessCompany sourceCompany) {
			this.purpose = purpose;
			this.investmentSecurity = investmentSecurity;
			this.sourceCompany = sourceCompany;
			this.valid = this.purpose != null && this.investmentSecurity != null && this.sourceCompany != null && this.investmentSecurity.getInstrument() != null;
		}


		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			Key key = (Key) o;
			return Objects.equals(this.purpose, key.purpose) &&
					Objects.equals(this.investmentSecurity, key.investmentSecurity) &&
					Objects.equals(this.sourceCompany, key.sourceCompany);
		}


		@Override
		public int hashCode() {
			return Objects.hash(this.purpose, this.investmentSecurity, this.sourceCompany);
		}


		public MarketDataSourcePurpose getPurpose() {
			return this.purpose;
		}


		public InvestmentSecurity getInvestmentSecurity() {
			return this.investmentSecurity;
		}


		public BusinessCompany getSourceCompany() {
			return this.sourceCompany;
		}


		public boolean isValid() {
			return this.valid;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconcilePositionExternalDailyService getReconcilePositionExternalDailyService() {
		return this.reconcilePositionExternalDailyService;
	}


	public void setReconcilePositionExternalDailyService(ReconcilePositionExternalDailyService reconcilePositionExternalDailyService) {
		this.reconcilePositionExternalDailyService = reconcilePositionExternalDailyService;
	}


	public IntegrationReconcileService getIntegrationReconcileService() {
		return this.integrationReconcileService;
	}


	public void setIntegrationReconcileService(IntegrationReconcileService integrationReconcileService) {
		this.integrationReconcileService = integrationReconcileService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
