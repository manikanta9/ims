package com.clifton.ims.integration.export.content.budi;

import com.clifton.export.budi.transaction.BudiTransactionFilter;
import com.clifton.investment.account.group.InvestmentAccountGroup;

import java.util.Date;


/**
 * @author TerryS
 */
public class BudiTransactionFilterImpl implements BudiTransactionFilter {

	private final InvestmentAccountGroup clientAccountGroup;
	private final Date postingDate;


	public BudiTransactionFilterImpl(InvestmentAccountGroup clientAccountGroup, Date postingDate) {
		this.clientAccountGroup = clientAccountGroup;
		this.postingDate = postingDate;
	}


	public InvestmentAccountGroup getClientAccountGroup() {
		return this.clientAccountGroup;
	}


	public Date getPostingDate() {
		return this.postingDate;
	}
}
