package com.clifton.ims.integration.export.content.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.ims.integration.export.content.ExportContentTradeFileGenerator;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.util.List;


public class ExportContentTradesReconciledComparison implements Comparison<ExportDefinitionContent> {

	private SystemBeanService systemBeanService;
	private TradeService tradeService;


	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(ExportDefinitionContent content, ComparisonContext context) {
		//TODO - while refactoring for class signatures - came across this - not sure how best to deal with it.
		ExportContentGenerator<?> systemBean = (ExportContentGenerator<?>) getSystemBeanService().getBeanInstance(content.getContentSystemBean());
		ValidationUtils.assertTrue(systemBean instanceof ExportContentTradeFileGenerator, ExportContentTradesReconciledComparison.class.getSimpleName()
				+ " only supports ExportContentTradeFileGenerator objects.");

		ExportContentTradeFileGenerator contentGenerator = (ExportContentTradeFileGenerator) systemBean;

		List<Trade> tradeList = contentGenerator.getTradeList(false);

		if (CollectionUtils.isEmpty(tradeList)) {
			context.recordTrueMessage("Trade export content [" + contentGenerator.getContentLabel() + "] is ready. (There are no trades).");
			return true;
		}

		TradeSearchForm searchForm = new TradeSearchForm();
		searchForm.setIds(BeanUtils.getBeanIdentityArray(tradeList, Integer.class));

		SearchRestriction reconciledRestriction = new SearchRestriction("reconciled", ComparisonConditions.EQUALS, "true");
		searchForm.addSearchRestriction(reconciledRestriction);

		List<Trade> reconciledTradeList = getTradeService().getTradeList(searchForm);

		boolean ready = CollectionUtils.getSize(tradeList) == CollectionUtils.getSize(reconciledTradeList);

		if (ready) {
			context.recordTrueMessage("Trade export content [" + contentGenerator.getContentLabel() + "] is ready. Trades are reconciled.");
		}
		else {
			context.recordFalseMessage("Trade export content [" + contentGenerator.getContentLabel() + "] is NOT ready. Not all trades are reconciled.");
		}

		return ready;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
