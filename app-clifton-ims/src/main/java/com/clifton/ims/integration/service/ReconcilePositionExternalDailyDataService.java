package com.clifton.ims.integration.service;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;


/**
 * @author TerryS
 */
public interface ReconcilePositionExternalDailyDataService {

	@DoNotAddRequestMapping
	public void loadReconcilePositionExternalDailyData(IntegrationImportRun run, MarketDataSourcePurpose purpose);
}
