package com.clifton.ims.integration.export.content;


import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


public class ExportContentPortfolioRunReportGenerator implements ExportContentGenerator<Object>, ValidationAware {

	private Integer clientInvestmentAccountId;
	private String dateGenerationString;
	private FileFormats format;
	private boolean excludePrivate = true;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private PortfolioRunService portfolioRunService;
	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;
	private ReportExportService reportExportService;
	private InvestmentAccountService investmentAccountService;

	private CalendarDateGenerationHandler calendarDateGenerationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FileWrapper> writeContent(@SuppressWarnings("unused") Object data, String fileName, @SuppressWarnings("unused") ExportDefinitionContent content,
	                                      @SuppressWarnings("unused") ExportRunHistory runHistory, Date previewDate) {
		InvestmentAccount clientInvestmentAccount = getClientInvestmentAccount();
		Date runDate = previewDate != null ? previewDate : getCalendarDateGenerationHandler().generateDate(getDateGenerationString());
		PortfolioRun run = getPortfolioRunService().getPortfolioRunByMainRun(getClientInvestmentAccountId(), runDate);
		ValidationUtils.assertNotNull(run,
				"Could not generate Portfolio Run Report for account [" + clientInvestmentAccount.getLabel() + "] on [" + DateUtils.fromDate(runDate, DateUtils.DATE_FORMAT_INPUT)
						+ "] because no run exists.");
		Integer reportId = (Integer) getPortfolioAccountDataRetriever().getPortfolioReportCustomFieldValue(run.getClientInvestmentAccount(), run.getServiceProcessingType());

		ReportExportConfiguration exportConfig = new ReportExportConfiguration();
		exportConfig.setExportFormat(getFormat());
		exportConfig.setReportId(reportId);
		exportConfig.setParameterMap(getPortfolioRunReportParameters(run, isExcludePrivate()));

		FileWrapper wrapper = getReportExportService().downloadReportFile(exportConfig);
		wrapper.setFileName(fileName);
		return CollectionUtils.createList(wrapper);
	}


	@Override
	public String getContentLabel() {
		StringBuilder resultSb = new StringBuilder(100);
		InvestmentAccount clientInvestmentAccount = getClientInvestmentAccount();
		if (clientInvestmentAccount != null) {
			resultSb.append("Portfolio Run Report for [");
			resultSb.append(clientInvestmentAccount.getLabel()).append("]");
		}

		if (getFormat() != null) {
			resultSb.append(" (").append(getFormat()).append(")");
		}

		return resultSb.toString();
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getClientInvestmentAccountId(), "Client investment account is required.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private InvestmentAccount getClientInvestmentAccount() {
		return getInvestmentAccountService().getInvestmentAccount(getClientInvestmentAccountId());
	}


	private LinkedHashMap<String, Object> getPortfolioRunReportParameters(PortfolioRun run, boolean excludePrivateComponents) {
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("RunID", run.getId());
		parameters.put("ClientAccountID", run.getClientInvestmentAccount().getId());
		parameters.put("BalanceDate", DateUtils.fromDateShort(run.getBalanceDate()));
		if (excludePrivateComponents) {
			parameters.put("ExcludePrivate", true);
		}
		return parameters;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}


	public FileFormats getFormat() {
		return this.format;
	}


	public void setFormat(FileFormats format) {
		this.format = format;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public boolean isExcludePrivate() {
		return this.excludePrivate;
	}


	public void setExcludePrivate(boolean excludePrivate) {
		this.excludePrivate = excludePrivate;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public String getDateGenerationString() {
		return this.dateGenerationString;
	}


	public void setDateGenerationString(String dateGenerationString) {
		this.dateGenerationString = dateGenerationString;
	}
}
