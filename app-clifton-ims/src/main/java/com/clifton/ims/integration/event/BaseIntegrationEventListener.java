package com.clifton.ims.integration.event;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.validation.EntityNotFoundValidationException;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import org.springframework.stereotype.Component;


/**
 * @author theodorez
 */
@Component
public abstract class BaseIntegrationEventListener extends BaseEventListener<IntegrationImportEvent> {

	private MarketDataSourceService marketDataSourceService;


	public MarketDataSource getMarketDataSourceForCompany(BusinessCompany company) {
		MarketDataSource marketDataSource = doMarketDataSourceForCompany(company);
		if (marketDataSource == null) {
			throw new EntityNotFoundValidationException("Unable to find Market Data Source with Name [" + company.getName() + "]", MarketDataSource.class.getName());
		}
		return marketDataSource;
	}


	/**
	 * Logic to traverse upwards through the company structure until a Market data source is found
	 */
	private MarketDataSource doMarketDataSourceForCompany(BusinessCompany company) {
		MarketDataSource marketDataSource = null;
		try {
			marketDataSource = getMarketDataSourceService().getMarketDataSourceByName(company.getName());
		}
		catch (EntityNotFoundValidationException e) {
			// ignore entity not found
		}
		if (marketDataSource == null && company.getParent() != null) {
			marketDataSource = doMarketDataSourceForCompany(company.getParent());
		}
		return marketDataSource;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
