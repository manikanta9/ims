package com.clifton.ims.integration.event;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.incoming.manager.IntegrationManagerBank;
import com.clifton.integration.incoming.manager.IntegrationManagerService;
import com.clifton.integration.incoming.manager.IntegrationManagerTransaction;
import com.clifton.integration.incoming.manager.search.IntegrationManagerTransactionSearchForm;
import com.clifton.investment.manager.custodian.InvestmentManagerCustodianAccount;
import com.clifton.investment.manager.custodian.InvestmentManagerCustodianService;
import com.clifton.investment.manager.transaction.InvestmentManagerCustodianTransaction;
import com.clifton.investment.manager.transaction.InvestmentManagerCustodianTransactionService;
import com.clifton.investment.manager.transaction.search.InvestmentManagerCustodianTransactionSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ManagerCustodianTransactionEventListener</code> loads data from the IntegrationManagerTransaction table
 * into the InvestmentManagerCustodianTransactionTable in IMS.
 *
 * @author theodorez
 */
@Component
public class ManagerCustodianTransactionEventListener extends BaseEventListener<IntegrationImportEvent> {

	private final static String EVENT_NAME = IntegrationImportRun.INTEGRATION_EVENT_MANAGER_TRANSACTION_IMPORT;

	private IntegrationImportService integrationImportService;
	private IntegrationManagerService integrationManagerService;

	private InvestmentManagerCustodianTransactionService investmentManagerCustodianTransactionService;
	private InvestmentManagerCustodianService investmentManagerCustodianService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run, "There is no Integration Import Run for event: " + event.getEventName());

		Integer sourceCompanyId = run.getIntegrationImportDefinition().getFileDefinition().getSource().getSourceCompanyId();
		AssertUtils.assertNotNull(sourceCompanyId, "Integration source [" + run.getIntegrationImportDefinition().getFileDefinition().getSource().getName() + "] does not have a source company id defined.");

		cleanupPriorRunData(run);

		Map<String, InvestmentManagerCustodianAccount> custodianAccountMap = new HashMap<>();

		//Get the new manager transactions from integration
		IntegrationManagerTransactionSearchForm searchForm = new IntegrationManagerTransactionSearchForm();
		searchForm.setRunId(run.getId());
		List<IntegrationManagerTransaction> managerTransactions = getIntegrationManagerService().getIntegrationManagerTransactionList(searchForm);

		Exception firstError = null;
		StringBuilder errorBuilder = null;
		List<String> custodianAccountErrorList = new ArrayList<>();
		for (IntegrationManagerTransaction managerTransaction : CollectionUtils.getIterable(managerTransactions)) {
			try {
				ValidationUtils.assertTrue(DateUtils.compare(run.getEffectiveDate(), managerTransaction.getTransactionDate(), false) == 0, "The Manager Transaction has a date: " + managerTransaction.getTransactionDate() + " That does not match the effective date of the run: " + run.getEffectiveDate());

				InvestmentManagerCustodianAccount custodianAccount = getCustodianAccount(managerTransaction.getManagerBank(), sourceCompanyId, custodianAccountMap, custodianAccountErrorList);
				if (custodianAccount != null) {
					InvestmentManagerCustodianTransaction investmentTransaction = new InvestmentManagerCustodianTransaction();
					investmentTransaction.setCustodianAccount(custodianAccount);
					investmentTransaction.setTransactionAmount(managerTransaction.getTransactionAmount());
					investmentTransaction.setTransactionDate(managerTransaction.getTransactionDate());
					investmentTransaction.setDescription(managerTransaction.getTransactionDescription());
					investmentTransaction.setSourceDataIdentifier(run.getId());
					getInvestmentManagerCustodianTransactionService().saveInvestmentManagerCustodianTransaction(investmentTransaction);
				}
			}
			catch (Exception e) {
				if (errorBuilder == null) {
					firstError = e;
					errorBuilder = new StringBuilder(1024);
				}
				else {
					errorBuilder.append("\n\n");
				}
				errorBuilder.append("Failed to import manager transaction id: ")
						.append(managerTransaction.getId())
						.append(" with  transaction date: ")
						.append(managerTransaction.getTransactionDate())
						.append(" and sourceCompanyId: ")
						.append(sourceCompanyId);
				errorBuilder.append("\nCAUSED BY:");
				errorBuilder.append(StringUtils.formatStringUpToNCharsWithDots(e.getMessage(), 4000));
			}
		}
		if (!CollectionUtils.isEmpty(custodianAccountErrorList)) {
			if (errorBuilder == null) {
				errorBuilder = new StringBuilder(1024);
			}
			errorBuilder.append("Cannot find Custodian Accounts for the following Account numbers: ");
			errorBuilder.append("\n[");
			errorBuilder.append(StringUtils.collectionToCommaDelimitedString(CollectionUtils.removeDuplicates(custodianAccountErrorList)));
			errorBuilder.append("]\n\n");
		}
		if (errorBuilder != null) {
			throw new RuntimeException(StringUtils.formatStringUpToNCharsWithDots(errorBuilder.toString(), 4000), firstError);
		}
	}


	@Override
	public String getEventName() {
		return EVENT_NAME;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void cleanupPriorRunData(IntegrationImportRun run) {
		//Get all the runs for today for this definition
		IntegrationImportRunSearchForm runSearchForm = new IntegrationImportRunSearchForm();
		runSearchForm.setIntegrationImportDefinitionId(run.getIntegrationImportDefinition().getId());
		runSearchForm.setEffectiveDate(DateUtils.clearTime(run.getEffectiveDate()));
		List<IntegrationImportRun> runsForToday = getIntegrationImportService().getIntegrationImportRunList(runSearchForm);
		//Delete all data associated with the runs
		if (!CollectionUtils.isEmpty(runsForToday)) {
			InvestmentManagerCustodianTransactionSearchForm existingTransactionSearchForm = new InvestmentManagerCustodianTransactionSearchForm();
			existingTransactionSearchForm.setSourceDataIdentifierList(BeanUtils.getPropertyValues(runsForToday, IntegrationImportRun::getId, Integer.class));
			List<InvestmentManagerCustodianTransaction> transactionsToBeDeleted = getInvestmentManagerCustodianTransactionService().getInvestmentManagerCustodianTransactionList(existingTransactionSearchForm);
			if (!CollectionUtils.isEmpty(transactionsToBeDeleted)) {
				getInvestmentManagerCustodianTransactionService().deleteInvestmentManagerCustodianTransactionList(transactionsToBeDeleted);
			}
		}
	}


	private InvestmentManagerCustodianAccount getCustodianAccount(IntegrationManagerBank managerBank, Integer sourceCompanyId, Map<String, InvestmentManagerCustodianAccount> custodianAccountMap, List<String> custodianAccountErrorList) {
		String key = managerBank.getBankCode() + "_" + sourceCompanyId;
		InvestmentManagerCustodianAccount custodianAccount = custodianAccountMap.get(key) != null ? custodianAccountMap.get(key) : getInvestmentManagerCustodianService().getInvestmentManagerCustodianAccountByNumberAndCompany(managerBank.getBankCode(), sourceCompanyId);
		if (custodianAccount == null) {
			custodianAccountErrorList.add(managerBank.getBankCode());
		}
		else {
			custodianAccountMap.put(key, custodianAccount);
		}
		return custodianAccount;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public IntegrationManagerService getIntegrationManagerService() {
		return this.integrationManagerService;
	}


	public void setIntegrationManagerService(IntegrationManagerService integrationManagerService) {
		this.integrationManagerService = integrationManagerService;
	}


	public InvestmentManagerCustodianTransactionService getInvestmentManagerCustodianTransactionService() {
		return this.investmentManagerCustodianTransactionService;
	}


	public void setInvestmentManagerCustodianTransactionService(InvestmentManagerCustodianTransactionService investmentManagerCustodianTransactionService) {
		this.investmentManagerCustodianTransactionService = investmentManagerCustodianTransactionService;
	}


	public InvestmentManagerCustodianService getInvestmentManagerCustodianService() {
		return this.investmentManagerCustodianService;
	}


	public void setInvestmentManagerCustodianService(InvestmentManagerCustodianService investmentManagerCustodianService) {
		this.investmentManagerCustodianService = investmentManagerCustodianService;
	}
}
