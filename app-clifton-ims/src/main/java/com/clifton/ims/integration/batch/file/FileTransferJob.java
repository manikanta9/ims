package com.clifton.ims.integration.batch.file;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.integration.file.IntegrationFileUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Will move files from one network location to another.
 * <p>
 * TODO: Combined some of this functionality with the incoming FTP job.
 *
 * @author mwacker
 */
public class FileTransferJob implements Task, ValidationAware {

	/**
	 * The path to the network location where we will be pulling files from.
	 */
	private String fromPath;
	/**
	 * The network path where the files are going to.
	 */
	private String toPath;
	/**
	 * A regex filter used to select the files to be moved.  This can be a Freemarker template
	 * with the DATE variable.  For example, (${DATE?string("MMddyy")}) would create a regex list
	 * (031616) on 03/16/2016 and match a file named 'FuturesWeekly_031616.xlsx'.
	 */
	private String filterRegex;
	/**
	 * A server-side glob or wildcard pattern, can significantly improve performance.
	 * This can take advantage of the Freemarker template pattern ${DATE}?string("MMddyy").
	 */
	private String wildcardPattern;
	/**
	 * Delete the files from the source directory after they have been moved.
	 */
	private boolean deleteSourceFiles;
	/**
	 * Number of days to go back to calculate the DATE variable for the filterRegex template.
	 */
	private Integer daysFromToday = 1;
	/**
	 * The date generation strategy used to calculate the DATE variable for the filterRegex template.
	 */
	private DateGenerationOptions dateGenerationOption;
	/**
	 * The file is being copied to an integration monitored folder, copy the file with the .write extension and remove it upon copy complete.
	 */
	private boolean integrationFolderDestination;
	/**
	 * The JSON string representation of the map of source to destination file extension mappings.
	 */
	private String destinationExtensionMap;
	/**
	 * The map built from the JSON representation in destinationExtensionMap
	 */
	private Map<String, String> translatedDestinationExtensionMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private TemplateConverter templateConverter;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		String wildcard = StringUtils.isEmpty(getWildcardPattern()) ? "*" : doFreemarkerReplacements(getWildcardPattern());
		Pattern fileNamePattern = getFileNamePattern();

		String finalFromPath = doFreemarkerReplacements(getFromPath());
		String finalToPath = doFreemarkerReplacements(getToPath());

		FileContainer fromDirectoryContainer = FileContainerFactory.getFileContainer(finalFromPath);
		this.translatedDestinationExtensionMap = buildTranslatedDestinationExtensionMap();

		Status status = Status.ofEmptyMessage();
		final FileCopyResult fileCopyResult = new FileCopyResult(status);
		try {
			fromDirectoryContainer.list(wildcard, fn -> true).forEach(fn -> {
				Path filePath = Paths.get(fn);
				if (!filePath.toFile().isDirectory()) {
					if (fileNamePattern.matcher(fn).matches()) {
						FileContainer fromFileContainer = FileContainerFactory.getFileContainer(FileUtils.combinePath(finalFromPath, fn));
						String fileName = doFileExtensionReplacement(fn);
						if (isIntegrationFolderDestination()) {
							fileName = fileName + "." + IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION;
						}
						String newFileStringPath = FileUtils.combinePath(finalToPath, fileName);
						FileContainer toFileContainer = FileContainerFactory.getFileContainer(newFileStringPath);
						// will throw an exception if the destination file exists
						moveOrCopyFile(fromFileContainer, toFileContainer, fileCopyResult);
						if (isIntegrationFolderDestination()) {
							FileContainer renamedFileContainer = FileContainerFactory.getFileContainer(FileUtils.getFileNameWithoutExtension(newFileStringPath));
							boolean successful = toFileContainer.renameToFile(renamedFileContainer);
							if (!successful) {
								status.addError(String.format("Could not rename [%s] to [%s].", renamedFileContainer.getAbsolutePath(), toFileContainer.getAbsolutePath()));
								// only delete the writing file if the original exists
								if (!isDeleteSourceFiles()) {
									try {
										// delete writing file if rename operation fails
										renamedFileContainer.deleteFile();
									}
									catch (IOException e) {
										status.addError(String.format("Could not delete [%s] after failed rename.", renamedFileContainer.getAbsolutePath()));
									}
								}
							}
						}
					}
					else {
						fileCopyResult.setSkippedFileCount(fileCopyResult.getSkippedFileCount() + 1);
					}
				}
			});
		}
		catch (Throwable e) {
			status.addError(ExceptionUtils.getDetailedMessage(e));
		}

		status.setMessage("File Transfer Count: " + fileCopyResult.getFileCount() + "\nFile Skipped Count: " + fileCopyResult.getSkippedFileCount() + "\nError Count: " + fileCopyResult.getErrorCount());
		if (fileCopyResult.getFileCount() == 0) {
			status.setActionPerformed(false);
		}
		return status;
	}


	@Override
	public void validate() throws ValidationException {
		String finalFromPath = doFreemarkerReplacements(getFromPath());
		String finalToPath = doFreemarkerReplacements(getToPath());

		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(finalFromPath).isDirectory(), "The from path [" + finalFromPath + "] does not exist.");
		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(finalToPath).isDirectory(), "The to path [" + finalToPath + "] does not exist.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void moveOrCopyFile(FileContainer filePath, FileContainer newFilePath, FileCopyResult fileCopyResult) {
		try {
			if (isDeleteSourceFiles()) {
				FileUtils.moveFile(filePath, newFilePath);
			}
			else {
				FileUtils.copyFile(filePath, newFilePath);
			}
			fileCopyResult.setFileCount(fileCopyResult.getFileCount() + 1);
		}
		catch (Exception e) {
			fileCopyResult.getStatus().addError(ExceptionUtils.getDetailedMessage(e));
			fileCopyResult.setErrorCount(fileCopyResult.getErrorCount() + 1);
		}
	}


	private String doFileExtensionReplacement(String fileName) {
		if (!CollectionUtils.isEmpty(getTranslatedDestinationExtensionMap())) {
			// allow adding an extension to a file without one
			String extension = Optional.ofNullable(FileUtils.getFileExtension(fileName)).orElse("");
			if (getTranslatedDestinationExtensionMap().containsKey(extension.toUpperCase())) {
				return FileUtils.getFileNameWithoutExtension(fileName) + "." + getTranslatedDestinationExtensionMap().get(extension.toUpperCase());
			}
			else if (getTranslatedDestinationExtensionMap().containsKey("+")) {
				return fileName + "." + getTranslatedDestinationExtensionMap().get("+");
			}
		}
		return fileName;
	}


	private Date getReportingDate() {
		if (getDateGenerationOption() != null) {
			return getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
		}
		return getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.PREVIOUS_BUSINESS_DAY);
	}


	private Pattern getFileNamePattern() {
		String fileRegex = doFreemarkerReplacements(getFilterRegex());
		if (!StringUtils.isEmpty(fileRegex)) {
			return Pattern.compile(fileRegex);
		}
		return Pattern.compile(".*");
	}


	private String doFreemarkerReplacements(String freemarker) {
		if (!StringUtils.isEmpty(freemarker)) {
			TemplateConfig config = new TemplateConfig(freemarker);
			config.addBeanToContext("DATE", DateUtils.clearTime(getReportingDate()));
			return getTemplateConverter().convert(config);
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	private Map<String, String> buildTranslatedDestinationExtensionMap() {
		if (!StringUtils.isEmpty(getDestinationExtensionMap())) {
			Map<String, Object> objectMap = (new JsonStringToMapConverter()).convert(getDestinationExtensionMap());
			if (objectMap.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME)) {
				List<Object> arr = (List<Object>) objectMap.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
				if (!CollectionUtils.isEmpty(arr)) {
					Map<String, String> strMap = (Map<String, String>) arr.get(0);
					return strMap.entrySet().stream()
							.collect(Collectors.toMap(e -> e.getKey().toUpperCase(), Map.Entry::getValue));
				}
			}
		}
		return Collections.emptyMap();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFromPath() {
		return this.fromPath;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setFromPath(String fromPath) {
		this.fromPath = fromPath;
	}


	public String getToPath() {
		return this.toPath;
	}


	public void setToPath(String toPath) {
		this.toPath = toPath;
	}


	public String getFilterRegex() {
		return this.filterRegex;
	}


	public void setFilterRegex(String filterRegex) {
		this.filterRegex = filterRegex;
	}


	public String getWildcardPattern() {
		return this.wildcardPattern;
	}


	public void setWildcardPattern(String wildcardPattern) {
		this.wildcardPattern = wildcardPattern;
	}


	public boolean isDeleteSourceFiles() {
		return this.deleteSourceFiles;
	}


	public void setDeleteSourceFiles(boolean deleteSourceFiles) {
		this.deleteSourceFiles = deleteSourceFiles;
	}


	public com.clifton.core.converter.template.TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(com.clifton.core.converter.template.TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public boolean isIntegrationFolderDestination() {
		return this.integrationFolderDestination;
	}


	public void setIntegrationFolderDestination(boolean integrationFolderDestination) {
		this.integrationFolderDestination = integrationFolderDestination;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public String getDestinationExtensionMap() {
		return this.destinationExtensionMap;
	}


	public void setDestinationExtensionMap(String destinationExtensionMap) {
		this.destinationExtensionMap = destinationExtensionMap;
	}


	public Map<String, String> getTranslatedDestinationExtensionMap() {
		return this.translatedDestinationExtensionMap;
	}


	/**
	 * Used to hold the results of the folder copy.  Needed because the
	 * lambda loop can only access final objects.
	 */
	private static class FileCopyResult {

		private final Status status;
		private int fileCount = 0;
		private int errorCount = 0;
		private int skippedFileCount = 0;


		public FileCopyResult(Status status) {
			this.status = status;
		}


		public int getFileCount() {
			return this.fileCount;
		}


		public void setFileCount(int fileCount) {
			this.fileCount = fileCount;
		}


		public int getErrorCount() {
			return this.errorCount;
		}


		public void setErrorCount(int errorCount) {
			this.errorCount = errorCount;
		}


		public int getSkippedFileCount() {
			return this.skippedFileCount;
		}


		public void setSkippedFileCount(int skippedFileCount) {
			this.skippedFileCount = skippedFileCount;
		}


		public Status getStatus() {
			return this.status;
		}
	}
}
