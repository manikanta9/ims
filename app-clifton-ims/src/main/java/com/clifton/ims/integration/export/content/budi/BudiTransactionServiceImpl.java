package com.clifton.ims.integration.export.content.budi;

import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.budi.transaction.BudiExecutionTypes;
import com.clifton.export.budi.transaction.BudiTransactionService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.workflow.history.WorkflowHistory;
import com.clifton.workflow.history.WorkflowHistorySearchForm;
import com.clifton.workflow.history.WorkflowHistoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * @author TerryS
 */
@Service
public class BudiTransactionServiceImpl implements BudiTransactionService<BudiTransaction, BudiTransactionFilterImpl> {

	private TradeService tradeService;
	private AccountingTransactionService accountingTransactionService;
	private WorkflowHistoryService workflowHistoryService;
	private InvestmentAccountGroupService investmentAccountGroupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BudiTransaction> getTransactionList(BudiTransactionFilterImpl filter) {
		ValidationUtils.assertNotNull(filter.getClientAccountGroup(), "Client Investment Account Group Name is Required.");
		ValidationUtils.assertNotNull(filter.getPostingDate(), "Trade Date is Required.");

		return CollectionUtils.combineCollections(
				getBookedTrades(filter.getClientAccountGroup(), filter.getPostingDate()),
				getCancelledTrades(filter.getClientAccountGroup(), filter.getPostingDate())
		);
	}


	private List<BudiTransaction> getBookedTrades(InvestmentAccountGroup clientAccountGroup, Date postingDate) {
		List<BudiTransaction> results = new ArrayList<>();

		// get trade accounting transactions posted on provided date belonging to the account group.
		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setPostingDate(postingDate);
		searchForm.setClientInvestmentAccountGroupId(clientAccountGroup.getId());
		searchForm.setJournalTypeName(AccountingJournalType.TRADE_JOURNAL);
		searchForm.setTableName(TradeFill.TABLE_NAME);
		List<AccountingTransaction> transactions = getAccountingTransactionService().getAccountingTransactionList(searchForm);

		for (AccountingTransaction accountingTransaction : transactions) {
			Integer fkFieldId = accountingTransaction.getFkFieldId();
			if (fkFieldId != null) {
				TradeFill tradeFill = getTradeService().getTradeFill(fkFieldId);
				if (tradeFill != null) {
					Trade trade = tradeFill.getTrade();
					// double check booked state.
					if (TradeService.TRADE_BOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
						// only add the trade once.
						if (results.stream().noneMatch(bt -> Objects.equals(bt.getTrade(), trade))) {
							// previously booked, its a cancel otherwise new.
							BudiExecutionTypes executionType = isPreviouslyBooked(trade, postingDate) ? BudiExecutionTypes.CORRECT : BudiExecutionTypes.NEW;
							results.add(new BudiTransaction(trade, executionType));
						}
					}
				}
			}
		}
		return results;
	}


	private List<BudiTransaction> getCancelledTrades(InvestmentAccountGroup clientAccountGroup, Date postingDate) {
		List<BudiTransaction> results = new ArrayList<>();

		// look for cancelled workflow history on posting date.
		WorkflowHistorySearchForm searchForm = new WorkflowHistorySearchForm();
		searchForm.setEndStateStartDate(postingDate);
		searchForm.setTableName(Trade.TABLE_NAME);
		searchForm.setEndWorkflowStateNameEquals(TradeService.TRADES_CANCELLED_STATE_NAME);
		List<WorkflowHistory> histories = getWorkflowHistoryService().getWorkflowHistoryList(searchForm);

		List<InvestmentAccount> clientAccounts = getInvestmentAccountGroupService().getInvestmentAccountListByGroup(clientAccountGroup.getName());
		for (WorkflowHistory history : histories) {
			Trade trade = getTradeService().getTrade(history.getFkFieldId().intValue());
			// must belong to account group.
			if (trade != null && clientAccounts.contains(trade.getClientInvestmentAccount())) {
				// double check trade is in cancelled state.
				if (TradeService.TRADES_CANCELLED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
					// was the trade previously posted?
					if (isPreviouslyBooked(trade, postingDate)) {
						results.add(new BudiTransaction(trade, BudiExecutionTypes.CANCEL));
					}
				}
			}
		}

		return results;
	}


	private boolean isPreviouslyBooked(Trade trade, Date postingDate) {
		return getWorkflowHistoryService().getWorkflowHistoryListByWorkflowAwareEntity(trade, false).stream()
				.anyMatch(h ->
						// Booked on a prior day.
						DateUtils.compare(h.getEndStateStartDate(), postingDate, false) < 0
								&& TradeService.TRADE_BOOKED_STATE_NAME.equals(h.getEndWorkflowState().getName())
				);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public WorkflowHistoryService getWorkflowHistoryService() {
		return this.workflowHistoryService;
	}


	public void setWorkflowHistoryService(WorkflowHistoryService workflowHistoryService) {
		this.workflowHistoryService = workflowHistoryService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}
}
