package com.clifton.ims.integration.export.context;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.context.ExportContextGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;
import com.clifton.report.posting.ReportPostingFileConfiguration;

import java.util.EnumMap;
import java.util.Map;


/**
 * @author theodorez
 */
public class ExportContextClientPortalGenerator extends ReportPostingFileConfiguration implements ExportContextGenerator, ValidationAware {

	private Integer postEntityTypeId;
	private Integer postSectionId;
	private Integer postEntityId;
	private Integer rollupSectionId;
	private Integer rollupEntityId;

	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportContentContextMapKeys, Object> getContext(ExportDefinitionContent content, Object generatedData) {
		ReportPostingFileConfiguration fileConfiguration = new ReportPostingFileConfiguration();
		if (!StringUtils.isEmpty(getDisplayName())) {
			fileConfiguration.setDisplayName(getExportTemplateGenerationHandler().processTemplate(getDisplayName(), new ExportTemplateCommand<>(this, content, content.getDefinition(), null)));
		}
		fileConfiguration.setPortalFileCategoryId(getPortalFileCategoryId());
		fileConfiguration.setPortalFileFrequency(getPortalFileFrequency());
		if (getPostEntityId() != null) {
			fileConfiguration.setPostPortalEntityId(getPostEntityId());
		}
		else {
			fileConfiguration.setPostPortalEntityId(getRollupEntityId());
		}

		Map<ExportContentContextMapKeys, Object> map = new EnumMap<>(ExportContentContextMapKeys.class);
		map.put(ExportContentContextMapKeys.REPORT_POSTING_FILE_CONFIG, fileConfiguration);
		return map;
	}


	@Override
	public String getContextLabel() {
		return "Client Portal";
	}


	@Override
	public void validate() {
		ValidationUtils.assertMutuallyExclusive("Either Post Entity or Rollup Entity may be specified and at least one must be specified.", getPostEntityId(), getRollupEntityId());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}


	public Integer getPostEntityTypeId() {
		return this.postEntityTypeId;
	}


	public void setPostEntityTypeId(Integer postEntityTypeId) {
		this.postEntityTypeId = postEntityTypeId;
	}


	public Integer getPostSectionId() {
		return this.postSectionId;
	}


	public void setPostSectionId(Integer postSectionId) {
		this.postSectionId = postSectionId;
	}


	public Integer getPostEntityId() {
		return this.postEntityId;
	}


	public void setPostEntityId(Integer postEntityId) {
		this.postEntityId = postEntityId;
	}


	public Integer getRollupSectionId() {
		return this.rollupSectionId;
	}


	public void setRollupSectionId(Integer rollupSectionId) {
		this.rollupSectionId = rollupSectionId;
	}


	public Integer getRollupEntityId() {
		return this.rollupEntityId;
	}


	public void setRollupEntityId(Integer rollupEntityId) {
		this.rollupEntityId = rollupEntityId;
	}
}
