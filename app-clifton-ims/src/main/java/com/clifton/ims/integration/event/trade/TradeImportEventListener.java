package com.clifton.ims.integration.event.trade;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.integration.event.BaseIntegrationEventListener;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.trade.IntegrationTrade;
import com.clifton.integration.incoming.trade.IntegrationTradeService;
import com.clifton.integration.incoming.trade.search.IntegrationTradeSearchForm;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.company.MarketDataSourceCompanyService;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurposeService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.TradeType;
import com.clifton.trade.destination.TradeDestinationMapping;
import com.clifton.trade.destination.TradeDestinationService;
import com.clifton.trade.destination.search.TradeDestinationMappingSearchCommand;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.search.TradeGroupSearchForm;
import com.clifton.trade.order.workflow.TradeOrderWorkflowService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.trade.util.TradeFillUtilHandler;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;


@Component
public class TradeImportEventListener extends BaseIntegrationEventListener {

	private static final String RECONCILE_INTRADAY_TRADE_MATCH_TYPE_NAME = "External Import";
	private static final String EVENT_NAME = IntegrationImportRun.INTEGRATION_EVENT_INTEGRATION_TRADE_IMPORT;

	private BusinessCompanyService businessCompanyService;
	private ContextHandler contextHandler;
	private IntegrationTradeService integrationTradeService;
	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataSourceCompanyService marketDataSourceCompanyService;
	private MarketDataSourcePurposeService marketDataSourcePurposeService;
	private TradeService tradeService;
	private TradeGroupService tradeGroupService;
	private TradeDestinationService tradeDestinationService;
	private TradeFillUtilHandler tradeFillUtilHandler;
	private SecurityUserService securityUserService;
	private TradeOrderWorkflowService tradeOrderWorkflowService;
	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		ValidationUtils.assertNotNull(run, "No run exists for [" + event.getTarget() + "] for event with name [" + event.getEventName() + "].");
		ValidationUtils.assertNotNull(run.getFile().getFileDefinition().getSource().getSourceCompanyId(), "Cannot import trades for a file with a source company defined.  See integration source [" + run.getFile().getFileDefinition().getSource().getName() + "].");

		IntegrationTradeLoadContext context = new IntegrationTradeLoadContext(run, getTraderUser());
		populateDataSourceValues(context);
		populateContextWithInternalTrades(context);
		populateContextWithExternalTrades(context);

		buildAndThrowError(context);

		// Filter out existing Integration Trade Import Trades so duplicates are not entered.
		if (!context.getImportTradeList().isEmpty()) {

			List<Trade> newTradeList = saveTradeList(context.getImportTradeList(), context);
			if (!CollectionUtils.isEmpty(newTradeList)) {
				transitionTradesToExecuted(newTradeList, context);
			}
		}

		// throw an error with the failures transitioning the trades
		buildAndThrowError(context, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void buildAndThrowError(IntegrationTradeLoadContext context) {
		buildAndThrowError(context, false);
	}


	private void buildAndThrowError(IntegrationTradeLoadContext context, boolean includeWarnings) {
		String errorMessage = buildErrorMessage(context, includeWarnings);
		if (!StringUtils.isEmpty(errorMessage)) {
			throw new RuntimeException("Failed to import trade because of these errors:\n\n" + errorMessage);
		}
	}


	private String buildErrorMessage(IntegrationTradeLoadContext context, boolean includeWarnings) {
		if (!CollectionUtils.isEmpty(context.getErrorMap())) {
			StringBuilder sb = new StringBuilder();
			context.getErrorMap().forEach((key, value) -> {
				sb.append(key).append(" Errors:\n");
				value.forEach(error -> sb.append(error).append("\n"));
				sb.append("\n\n");
			});
			if (includeWarnings) {
				context.getWarningMap().forEach((key, value) -> {
					sb.append(key).append(" Warnings:\n");
					value.forEach(error -> sb.append(error).append("\n"));
					sb.append("\n\n");
				});
			}
			return sb.toString();
		}
		return null;
	}


	private SecurityUser getTraderUser() {
		SecurityUser result = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		if (result == null) {
			result = getSecurityUserService().getSecurityUserByName(SecurityUser.SYSTEM_USER);
		}
		return result;
	}


	private void populateDataSourceValues(IntegrationTradeLoadContext context) {
		BusinessCompany sourceCompany = getBusinessCompanyService().getBusinessCompany(context.getRun().getFile().getFileDefinition().getSource().getSourceCompanyId());
		ValidationUtils.assertNotEmpty(sourceCompany.getAlias(), "Cannot find data source for company [" + sourceCompany.getLabel() + "].");
		context.setSourceBusinessCompany(sourceCompany);
		context.setDataSource(getMarketDataSourceService().getMarketDataSourceByCompany(sourceCompany));
		ValidationUtils.assertNotNull(context.getDataSource(), "Cannot find data source for company [" + sourceCompany.getLabel() + "].");
		context.setDataSourcePurpose(getMarketDataSourcePurposeService().getMarketDataSourcePurposeByName(MarketDataSourcePurpose.MARKET_DATA_SOURCE_PURPOSE_NAME_TRADE_IMPORT));
		ValidationUtils.assertNotNull(context.getDataSourcePurpose(), "Cannot find data source purpose with name [" + MarketDataSourcePurpose.MARKET_DATA_SOURCE_PURPOSE_NAME_TRADE_IMPORT + "].");
	}


	private void populateContextWithExternalTrades(IntegrationTradeLoadContext context) {
		IntegrationTradeSearchForm searchForm = new IntegrationTradeSearchForm();
		searchForm.setRunId(context.getRun().getId());
		List<IntegrationTrade> externalTradeList = getIntegrationTradeService().getIntegrationTradeList(searchForm);

		AtomicInteger skippedTradeCount = new AtomicInteger();
		externalTradeList.forEach(integrationTrade -> {
			Trade trade = createTradeFromIntegrationTrade(integrationTrade, context, skippedTradeCount);
			if (trade != null) {
				IntegrationTradeTrade integrationTradeTrade = new IntegrationTradeTrade(integrationTrade, trade);
				context.getImportTradeList().add(integrationTradeTrade);
			}
		});

		if (skippedTradeCount.get() > 0) {
			context.addWarning("Trade", "[" + skippedTradeCount.get() + "] external trades were matched to an internal trade but skipped because the internal trade was not in Draft state.\n");
		}
	}


	private Trade createTradeFromIntegrationTrade(IntegrationTrade integrationTrade, IntegrationTradeLoadContext context, AtomicInteger skippedTradeCount) {
		InvestmentSecurity payingCurrency = resolvePayingCurrency(integrationTrade, context);
		InvestmentSecurity security = resolveInvestmentSecurity(integrationTrade, payingCurrency, context);
		if (security == null) {
			// no security an error was added to the context so just return nothing
			return null;
		}
		if (payingCurrency == null) {
			payingCurrency = security.getInstrument().getTradingCurrency();
		}


		String uniqueTradeIdentifier = getIntegrationTradeUniqueIdentifier(integrationTrade, context);
		Trade trade = context.getExistingTradeMap().computeIfAbsent(uniqueTradeIdentifier, key -> new Trade());
		trade.setExternalTradeIdentifier(uniqueTradeIdentifier);
		if (!trade.isNewBean() && !TradeService.TRADES_DRAFT_STATUS_NAME.equals(trade.getWorkflowStatus().getName())) {
			skippedTradeCount.incrementAndGet();
			return null;
		}
		trade.setTradeType(getTradeType(security, context));
		if (trade.getTradeType() == null) {
			return null;
		}
		trade.setBuy(integrationTrade.isBuy());
		trade.setInvestmentSecurity(security);
		trade.setHoldingInvestmentAccount(resolveHoldingInvestmentAccount(integrationTrade, context));
		trade.setClientInvestmentAccount(resolveClientInvestmentAccount(integrationTrade, trade, context));

		trade.setTradeDate(integrationTrade.getTradeDate());
		trade.setSettlementDate(integrationTrade.getSettlementDate());
		trade.setCommissionAmount(integrationTrade.getCommissionAmount());
		trade.setFeeAmount(integrationTrade.getFeeAmount());

		trade.setPayingSecurity(payingCurrency);


		if (integrationTrade.isCurrencyTrade()) {
			trade.setExecutingBrokerCompany(trade.getHoldingInvestmentAccount().getIssuingCompany());
			trade.setAccountingNotional(integrationTrade.getQuantity());

			trade.setExchangeRateToBase(getMarketDataSourceService().adjustIncomingMarketDataSourcePrice(integrationTrade.getExchangeRateToBase(), trade.getPayingSecurity().getInstrument(), context.getSourceBusinessCompany(), context.getDataSourcePurpose()));
		}
		else {
			trade.setExecutingBrokerCompany(resolveExecutingBroker(integrationTrade, context));
			trade.setQuantityIntended(integrationTrade.getQuantity());
			trade.setExchangeRateToBase(integrationTrade.getExchangeRateToBase());

			BigDecimal price = getMarketDataSourceService().adjustIncomingMarketDataSourcePrice(integrationTrade.getPrice(), trade.getPayingSecurity().getInstrument(), context.getSourceBusinessCompany(), context.getDataSourcePurpose());
			trade.setExpectedUnitPrice(price);
			trade.setAverageUnitPrice(price);
		}

		if (!trade.getPayingSecurity().equals(trade.getInvestmentSecurity().getInstrument().getTradingCurrency()) && MathUtils.isNullOrZero(trade.getExchangeRateToBase())) {
			context.addError("Trade", "No exchange rate is set for trade of [" + trade.getInvestmentSecurity().getSymbol() + "] with paying security of [" + trade.getPayingSecurity().getSymbol() + "].");
		}

		trade.setTraderUser(context.getTraderUser());

		TradeDestinationMapping mapping = getTradeDestinationMapping(trade, context);
		if (mapping != null) {
			trade.setTradeDestination(mapping.getTradeDestination());
		}

		return trade;
	}


	private String getIntegrationTradeUniqueIdentifier(IntegrationTrade integrationTrade, IntegrationTradeLoadContext context) {
		return context.getSourceBusinessCompany().getAlias() + "::" + integrationTrade.getUniqueIdentifier();
	}


	private TradeType getTradeType(InvestmentSecurity security, IntegrationTradeLoadContext context) {
		try {
			return getTradeService().getTradeTypeForSecurity(security.getId());
		}
		catch (Exception ex) {
			context.addError("Trade Type", "Cannot find trade type for investment security [" + security + "].\n\n" + ExceptionUtils.getDetailedMessage(ex));
		}
		return null;
	}


	private InvestmentSecurity resolvePayingCurrency(IntegrationTrade integrationTrade, IntegrationTradeLoadContext context) {
		InvestmentSecurity result = getSecurityByForLookUpType(integrationTrade, null, context, SecurityLookupTypes.PAYING_CURRENCY);
		if (result == null) {
			context.addError("Investment Security", "No currency could be found for [" + integrationTrade.getPayingSecuritySymbol() + "].");
		}
		return result;
	}


	private InvestmentSecurity resolveInvestmentSecurity(IntegrationTrade integrationTrade, InvestmentSecurity ccy, IntegrationTradeLoadContext context) {
		InvestmentSecurity security;
		if (integrationTrade.isCurrencyTrade()) {
			security = getSecurityByForLookUpType(integrationTrade, null, context, SecurityLookupTypes.CURRENCY);
		}
		else {
			// Multiple security values may be set on the integration trade, some may not resolve a security so we make multiple attempts
			security = getSecurityByForLookUpType(integrationTrade, ccy, context, SecurityLookupTypes.CUSIP);
			if (security == null) {
				security = getSecurityByForLookUpType(integrationTrade, ccy, context, SecurityLookupTypes.SYMBOL);
			}
			if (security == null) {
				security = getSecurityByForLookUpType(integrationTrade, ccy, context, SecurityLookupTypes.SEDOL);
			}
			if (security == null) {
				security = getSecurityByForLookUpType(integrationTrade, ccy, context, SecurityLookupTypes.ISIN);
			}
		}
		if (security == null) {
			context.addError("Investment Security", "No security could be found for CUSIP = [" + integrationTrade.getCusip()
					+ "], Symbol = [" + integrationTrade.getSecuritySymbol()
					+ "], SEDOL = [" + integrationTrade.getSedol()
					+ "], ISIN = [" + integrationTrade.getIsin() + "] and currency [" + integrationTrade.getPayingSecuritySymbol() + "].");
		}
		return security;
	}


	private InvestmentSecurity getSecurityByForLookUpType(IntegrationTrade integrationTrade, InvestmentSecurity ccy, IntegrationTradeLoadContext context, SecurityLookupTypes lookupType) {
		if (lookupType == null || !lookupType.isApplicable(integrationTrade, ccy)) {
			return null;
		}

		String cacheKey = lookupType.getCacheKey(integrationTrade);
		return context.getSecurityMap().computeIfAbsent(cacheKey, key -> {
			InvestmentSecurity result = lookupType.retrieveSecurity(integrationTrade, ccy, this.getInvestmentInstrumentService());
			return new ObjectWrapper<>(result);
		}).getObject();
	}


	private InvestmentAccount resolveHoldingInvestmentAccount(IntegrationTrade integrationTrade, IntegrationTradeLoadContext context) {
		String holdingAccountNumber = integrationTrade.getHoldingAccountNumber();
		return context.getHoldingAccountMap().computeIfAbsent(holdingAccountNumber, accountNumber -> {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			searchForm.setNumberEquals(accountNumber);
			searchForm.setOurAccount(false);
			List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

			InvestmentAccount result = null;
			if (CollectionUtils.getSize(accountList) == 1) {
				result = accountList.get(0);
			}
			if (result == null) {
				context.addError("Holding Account",
						"Could not find an account for [" + integrationTrade.getHoldingAccountNumber() + "] that is active on date [" + DateUtils.fromDate(integrationTrade.getTradeDate()) + "].");
			}
			return new ObjectWrapper<>(result);
		}).getObject();
	}


	private InvestmentAccount resolveClientInvestmentAccount(IntegrationTrade integrationTrade, Trade trade, IntegrationTradeLoadContext context) {
		String clientAccountKey = StringUtils.isEmpty(integrationTrade.getClientAccountNumber()) ? trade.getTradeType().getHoldingAccountPurpose().getName() + "_" + integrationTrade.getHoldingAccountNumber() : integrationTrade.getClientAccountNumber();
		return context.getClientAccountMap().computeIfAbsent(clientAccountKey, key -> {
			InvestmentAccount result = null;
			if (!StringUtils.isEmpty(integrationTrade.getClientAccountNumber())) {
				result = getInvestmentAccountService().getInvestmentAccountByNumber(integrationTrade.getClientAccountNumber());
				if (result == null) {
					context.addError("Client Account", "Could not find a client account for [" + integrationTrade.getClientAccountNumber() + "] that is active on date [" + DateUtils.fromDate(integrationTrade.getTradeDate()) + "].");
				}
			}
			else if (trade.getHoldingInvestmentAccount() != null) {
				result = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(trade.getHoldingInvestmentAccount().getId(), trade.getTradeType().getHoldingAccountPurpose().getName(), integrationTrade.getTradeDate());
				if (result == null) {
					context.addError("Client Account", "Could not find a client account related to holding account [" + trade.getHoldingInvestmentAccount()
							+ "] for purpose [" + trade.getTradeType().getHoldingAccountPurpose().getName() + "] that is active for trading on date [" + DateUtils.fromDate(trade.getTradeDate()) + "].");
				}
			}
			return new ObjectWrapper<>(result);
		}).getObject();
	}


	private BusinessCompany resolveExecutingBroker(IntegrationTrade integrationTrade, IntegrationTradeLoadContext context) {
		return context.getExecutingBrokerMap().computeIfAbsent(integrationTrade.getExecutingBrokerCode(),
				brokerCode -> new ObjectWrapper<>(getMarketDataSourceCompanyService().getMappedCompanyByCodeStrict(context.getDataSource(), context.getDataSourcePurpose(), integrationTrade.getExecutingBrokerCode())))
				.getObject();
	}


	private TradeDestinationMapping getTradeDestinationMapping(Trade trade, IntegrationTradeLoadContext context) {
		String mappingKey = trade.getTradeType().getId() + "_" + trade.getExecutingBrokerCompany().getId() + "_" + DateUtils.fromDate(trade.getTradeDate());
		return context.getTradeDestinationMappingMap().computeIfAbsent(mappingKey, key -> {
			List<TradeDestinationMapping> mappingList = getTradeDestinationService().getTradeDestinationMappingListByCommand(
					new TradeDestinationMappingSearchCommand(trade.getInvestmentSecurity().getId(), trade.getExecutingBrokerCompany().getId(), trade.getTradeDate()));

			TradeDestinationMapping destinationMapping = CollectionUtils.getOnlyElement(BeanUtils.filter(mappingList, mapping -> mapping.getTradeDestination().getType().isManual()));
			if (destinationMapping == null) {
				context.addError("Trade Destination Mapping", "No trade destination mapping exists for trade type [" + trade.getTradeType().getName() + "] and executing broker ["
						+ trade.getExecutingBrokerCompany().getName() + "] that is active on date [" + DateUtils.fromDate(trade.getTradeDate(), DateUtils.DATE_FORMAT_INPUT) + "].");
			}
			return new ObjectWrapper<>(destinationMapping);
		}).getObject();
	}


	/**
	 * Returns a collection of existing, non-canceled trade fills that are associated with Integration Trade Import trade groups for the run's effective date and context's trader.
	 * The returned map is keyed by a unique natural key from the fill with a value of the fill.
	 */
	protected void populateContextWithInternalTrades(IntegrationTradeLoadContext context) {
		TradeGroupSearchForm groupSearchForm = new TradeGroupSearchForm();
		groupSearchForm.setTradeGroupTypeName(TradeGroupType.GroupTypes.INTEGRATION_TRADE_IMPORT.getTypeName());
		groupSearchForm.setTradeDate(context.getRun().getEffectiveDate());
		groupSearchForm.setTraderUserId(context.getTraderUser().getId());
		List<TradeGroup> existingTradeGroupList = getTradeGroupService().getTradeGroupList(groupSearchForm);
		if (!CollectionUtils.isEmpty(existingTradeGroupList)) {

			Integer[] groupIds = existingTradeGroupList.stream()
					.map(TradeGroup::getId)
					.toArray(Integer[]::new);

			TradeSearchForm tradeSearchForm = new TradeSearchForm();
			tradeSearchForm.setTradeGroupIds(groupIds);
			tradeSearchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
			List<Trade> existingTradeFillList = getTradeService().getTradeList(tradeSearchForm);

			context.setExistingTradeMap(CollectionUtils.isEmpty(existingTradeFillList) ? new HashMap<>()
					: existingTradeFillList.stream().collect(Collectors.toMap(Trade::getExternalTradeIdentifier, Function.identity())));
		}
	}


	private TradeGroupType getIntegrationImportTradeGroupType() {
		return getTradeGroupService().getTradeGroupTypeByName(TradeGroupType.GroupTypes.INTEGRATION_TRADE_IMPORT.getTypeName());
	}


	protected TradeGroup createTradeGroup(SecurityUser traderUser, Date tradeDate) {
		TradeGroup group = new TradeGroup();
		group.setTradeGroupType(getIntegrationImportTradeGroupType());
		group.setTraderUser(traderUser);
		group.setTradeList(new ArrayList<>());
		group.setTradeDate(tradeDate);
		return getTradeGroupService().saveTradeGroup(group);
	}


	/**
	 * Save the trades and group in a separate transaction so they are independent from the workflow transition
	 */
	protected List<Trade> saveTradeList(List<IntegrationTradeTrade> tradeList, IntegrationTradeLoadContext context) {
		if (CollectionUtils.isEmpty(tradeList)) {
			return Collections.emptyList();
		}

		TradeGroup group = createTradeGroup(context.getTraderUser(), context.getRun().getEffectiveDate());
		List<Trade> result = new ArrayList<>();
		tradeList.forEach(integrationTradeTrade -> {
			Trade trade = integrationTradeTrade.getTrade();
			try {
				saveTrade(integrationTradeTrade, group, result);
			}
			catch (Exception e) {
				context.addError("Trade", "Failed to save trade [" + trade.getLabel() + ": " + trade.getLabelShort() + "].\n\n" + ExceptionUtils.getDetailedMessage(e));
			}
		});
		return result;
	}


	@Transactional(timeout = 30, propagation = Propagation.REQUIRES_NEW)
	protected void saveTrade(IntegrationTradeTrade integrationTradeTrade, TradeGroup group, List<Trade> result) {
		Trade trade = integrationTradeTrade.getTrade();
		IntegrationTrade integrationTrade = integrationTradeTrade.getIntegrationTrade();

		boolean newTrade = trade.isNewBean();
		if (trade.getTradeGroup() == null) {
			trade.setTradeGroup(group);
		}
		trade = getTradeService().saveTrade(trade);
		result.add(trade);

		TradeFill fill = removeAndCreateTradeFill(trade, newTrade);
		if (fill != null) {
			if (integrationTrade.getOpeningDate() != null && integrationTrade.getOpeningPrice() != null) {
				fill.setOpeningPrice(integrationTrade.getOpeningPrice());
				fill.setOpeningDate(integrationTrade.getOpeningDate());
			}
			getTradeService().saveTradeFill(fill);
		}

		if (newTrade) {
			createTradeReconciliationEntries(fill);
		}
	}


	private TradeFill removeAndCreateTradeFill(Trade trade, boolean newTrade) {
		if (!newTrade) {
			List<TradeFill> fillList = getTradeService().getTradeFillListByTrade(trade.getId());
			getTradeService().deleteTradeFillList(fillList);
		}
		return getTradeFillUtilHandler().createTradeFill(trade, false);
	}


	/**
	 * Transition the trades to Executed using a new transaction so that any failures here will not rollback all trades.
	 */
	protected void transitionTradesToExecuted(List<Trade> tradeList, IntegrationTradeLoadContext context) {
		int failedCount = 0;
		Exception firstException = null;
		for (Trade trade : CollectionUtils.getIterable(tradeList)) {
			try {
				getTradeOrderWorkflowService().setTradeWorkflowState(trade, TradeService.TRADE_EXECUTED_STATE_NAME);
			}
			catch (Exception ex) {
				failedCount++;
				if (firstException == null) {
					firstException = ex;
				}
			}
		}
		if (failedCount > 0) {
			context.addError("Trade", "Failed to transition [" + failedCount + "] trades to executed.\n\n" + ExceptionUtils.getDetailedMessage(firstException));
		}
	}


	protected void createTradeReconciliationEntries(TradeFill tradeFill) {
		ReconcileTradeMatchType matchType = getReconcileTradeIntradayMatchService().getReconcileTradeMatchTypeByName(RECONCILE_INTRADAY_TRADE_MATCH_TYPE_NAME);

		ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
		match.setTradeFill(tradeFill);
		match.setMatchType(matchType);
		getReconcileTradeIntradayMatchService().saveReconcileTradeIntradayMatch(match);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private enum SecurityLookupTypes {
		CUSIP("CUSIP", IntegrationTrade::getCusip, SecuritySearchForm::setCusipEquals, false, false, false),
		SYMBOL("SYMBOL", IntegrationTrade::getSecuritySymbol, null, true, true, false),
		SEDOL("SEDOL", IntegrationTrade::getSedol, SecuritySearchForm::setSedolEquals, false, false, false),
		ISIN("ISIN", IntegrationTrade::getIsin, SecuritySearchForm::setIsinEquals, false, false, false),
		PAYING_CURRENCY("SYMBOL", IntegrationTrade::getPayingSecuritySymbol, null, false, true, true),
		CURRENCY("SYMBOL", IntegrationTrade::getSecuritySymbol, null, false, true, true);

		private final String cachePrefix;
		private final boolean useCurrency;
		private final boolean currency;
		private final Function<IntegrationTrade, String> getter;
		private final BiConsumer<SecuritySearchForm, String> setter;
		private final boolean getBySymbol;


		SecurityLookupTypes(String cachePrefix, Function<IntegrationTrade, String> getter, BiConsumer<SecuritySearchForm, String> setter, boolean useCurrency, boolean getBySymbol, boolean currency) {
			this.cachePrefix = cachePrefix;
			this.getter = getter;
			this.setter = setter;
			this.useCurrency = useCurrency;
			this.getBySymbol = getBySymbol;
			this.currency = currency;
		}


		public boolean isApplicable(IntegrationTrade integrationTrade, InvestmentSecurity ccy) {
			if (this.useCurrency && (ccy == null || !ccy.isCurrency())) {
				return false;
			}

			String value = this.getter.apply(integrationTrade);
			if (StringUtils.isEmpty(value)) {
				return false;
			}

			return true;
		}


		public String getCacheKey(IntegrationTrade integrationTrade) {
			String value = this.getter.apply(integrationTrade);
			return this.cachePrefix + "_" + value;
		}


		public InvestmentSecurity retrieveSecurity(IntegrationTrade integrationTrade, InvestmentSecurity ccy, InvestmentInstrumentService service) {
			if (this.useCurrency && (ccy == null || !ccy.isCurrency())) {
				return null;
			}

			String value = this.getter.apply(integrationTrade);
			if (StringUtils.isEmpty(value)) {
				return null;
			}

			if (this.getBySymbol) {
				InvestmentSecurity security = service.getInvestmentSecurityBySymbol(value, this.currency);
				// ensure that security is active on trade and that (if ccy is provided) that currency type matches
				if (security != null && security.isActiveOn(integrationTrade.getTradeDate()) && (!this.useCurrency || security.getInstrument().getTradingCurrency().equals(ccy))) {
					return security;
				}
				return null;
			}

			SecuritySearchForm securitySearchForm = new SecuritySearchForm();
			securitySearchForm.setActiveOnDate(integrationTrade.getTradeDate());
			if (this.useCurrency) {
				securitySearchForm.setTradingCurrencyId(ccy.getId());
			}
			this.setter.accept(securitySearchForm, value);
			List<InvestmentSecurity> securityList = service.getInvestmentSecurityList(securitySearchForm);
			if (securityList.size() > 1) {
				return null;
			}
			return CollectionUtils.getOnlyElement(securityList);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Class containing the {@link IntegrationTrade} with its corresponding {@link Trade}
	 */
	private static final class IntegrationTradeTrade {

		private final IntegrationTrade integrationTrade;
		private final Trade trade;


		private IntegrationTradeTrade(IntegrationTrade integrationTrade, Trade trade) {
			this.integrationTrade = integrationTrade;
			this.trade = trade;
		}


		public IntegrationTrade getIntegrationTrade() {
			return this.integrationTrade;
		}


		public Trade getTrade() {
			return this.trade;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class IntegrationTradeLoadContext {

		private final IntegrationImportRun run;
		private final SecurityUser traderUser;
		private MarketDataSource dataSource;
		private MarketDataSourcePurpose dataSourcePurpose;
		private BusinessCompany sourceBusinessCompany;

		private final Map<String, ObjectWrapper<InvestmentSecurity>> securityMap = new HashMap<>();
		private final Map<String, ObjectWrapper<InvestmentAccount>> holdingAccountMap = new HashMap<>();
		private final Map<String, ObjectWrapper<InvestmentAccount>> clientAccountMap = new HashMap<>();
		private final Map<String, ObjectWrapper<BusinessCompany>> executingBrokerMap = new HashMap<>();
		private final Map<String, ObjectWrapper<TradeDestinationMapping>> tradeDestinationMappingMap = new HashMap<>();
		private final List<IntegrationTradeTrade> importTradeList = new ArrayList<>();

		private Map<String, Trade> existingTradeMap = new HashMap<>();

		/**
		 * Map of errors for result message
		 */
		private final Map<String, List<String>> errorMap = new HashMap<>();

		private final Map<String, List<String>> warningMap = new HashMap<>();

		///////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		IntegrationTradeLoadContext(IntegrationImportRun run, SecurityUser traderUser) {
			this.run = run;
			this.traderUser = traderUser;
		}


		public void addError(String group, String error) {
			getErrorMap().computeIfAbsent(group, key -> new ArrayList<>()).add(error);
		}


		public void addWarning(String group, String error) {
			getWarningMap().computeIfAbsent(group, key -> new ArrayList<>()).add(error);
		}

		///////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public IntegrationImportRun getRun() {
			return this.run;
		}


		public SecurityUser getTraderUser() {
			return this.traderUser;
		}


		public MarketDataSource getDataSource() {
			return this.dataSource;
		}


		public void setDataSource(MarketDataSource dataSource) {
			this.dataSource = dataSource;
		}


		public MarketDataSourcePurpose getDataSourcePurpose() {
			return this.dataSourcePurpose;
		}


		public void setDataSourcePurpose(MarketDataSourcePurpose dataSourcePurpose) {
			this.dataSourcePurpose = dataSourcePurpose;
		}


		public Map<String, ObjectWrapper<InvestmentSecurity>> getSecurityMap() {
			return this.securityMap;
		}


		public Map<String, ObjectWrapper<InvestmentAccount>> getHoldingAccountMap() {
			return this.holdingAccountMap;
		}


		public Map<String, ObjectWrapper<InvestmentAccount>> getClientAccountMap() {
			return this.clientAccountMap;
		}


		public Map<String, ObjectWrapper<BusinessCompany>> getExecutingBrokerMap() {
			return this.executingBrokerMap;
		}


		public Map<String, ObjectWrapper<TradeDestinationMapping>> getTradeDestinationMappingMap() {
			return this.tradeDestinationMappingMap;
		}


		public List<IntegrationTradeTrade> getImportTradeList() {
			return this.importTradeList;
		}


		public Map<String, List<String>> getErrorMap() {
			return this.errorMap;
		}


		public Map<String, List<String>> getWarningMap() {
			return this.warningMap;
		}


		public Map<String, Trade> getExistingTradeMap() {
			return this.existingTradeMap;
		}


		public void setExistingTradeMap(Map<String, Trade> existingTradeMap) {
			this.existingTradeMap = existingTradeMap;
		}


		public BusinessCompany getSourceBusinessCompany() {
			return this.sourceBusinessCompany;
		}


		public void setSourceBusinessCompany(BusinessCompany sourceBusinessCompany) {
			this.sourceBusinessCompany = sourceBusinessCompany;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return EVENT_NAME;
	}


	public IntegrationTradeService getIntegrationTradeService() {
		return this.integrationTradeService;
	}


	public void setIntegrationTradeService(IntegrationTradeService integrationTradeService) {
		this.integrationTradeService = integrationTradeService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public TradeGroupService getTradeGroupService() {
		return this.tradeGroupService;
	}


	public void setTradeGroupService(TradeGroupService tradeGroupService) {
		this.tradeGroupService = tradeGroupService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public TradeDestinationService getTradeDestinationService() {
		return this.tradeDestinationService;
	}


	public void setTradeDestinationService(TradeDestinationService tradeDestinationService) {
		this.tradeDestinationService = tradeDestinationService;
	}


	public MarketDataSourceCompanyService getMarketDataSourceCompanyService() {
		return this.marketDataSourceCompanyService;
	}


	public void setMarketDataSourceCompanyService(MarketDataSourceCompanyService marketDataSourceCompanyService) {
		this.marketDataSourceCompanyService = marketDataSourceCompanyService;
	}


	public MarketDataSourcePurposeService getMarketDataSourcePurposeService() {
		return this.marketDataSourcePurposeService;
	}


	public void setMarketDataSourcePurposeService(MarketDataSourcePurposeService marketDataSourcePurposeService) {
		this.marketDataSourcePurposeService = marketDataSourcePurposeService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public TradeFillUtilHandler getTradeFillUtilHandler() {
		return this.tradeFillUtilHandler;
	}


	public void setTradeFillUtilHandler(TradeFillUtilHandler tradeFillUtilHandler) {
		this.tradeFillUtilHandler = tradeFillUtilHandler;
	}


	public TradeOrderWorkflowService getTradeOrderWorkflowService() {
		return this.tradeOrderWorkflowService;
	}


	public void setTradeOrderWorkflowService(TradeOrderWorkflowService tradeOrderWorkflowService) {
		this.tradeOrderWorkflowService = tradeOrderWorkflowService;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}

}
