package com.clifton.ims.integration.export.content.field;


/**
 * The <code>ExportTemplateProperties</code> class defines static constants for properties that can be accessed within FreeMarker templates for
 * trade exports.
 *
 * @author jgommels
 */
public class ExportTemplateProperties {

	//Warning: If you change any of the values here, the FreeMarker templates that reference them will need to be updated as well
	public static final String FIELD_STORE = "field_store";
	public static final String DESTINATION_ENVIRONMENT = "destination_environment";
	public static final String EXPORT_DATE = "export_date";
	public static final String GENERATION_DATE = "generation_date";
	public static final String TRADE_TYPE = "trade_type";
	public static final String TRADE_LIST = "trade_list";
	public static final String TRADE_FILL_LIST_SIZE = "trade_fill_size";
	public static final String UNIQUE_EXPORT_ID_PROP = "unique_export_id";
	public static final String EXPORT_GENERATOR = "export_generator";
	public static final String BROKER_COMPANIES = "broker_companies";
}
