package com.clifton.ims.integration.export.trade;


public enum TradeExportFileFormatTypes {
	JP_MORGAN("/META-INF/export/templates/trades/jpmorgan/jpmorgan_outgoing_main.ftl", false),
	STANDARD_FUTURES("/META-INF/export/templates/trades/standard_futures.ftl", true);


	private final String templatePath;
	private final boolean allowMultipleTradeTypes;


	TradeExportFileFormatTypes(String templatePath, boolean allowMultipleTradeTypes) {
		this.templatePath = templatePath;
		this.allowMultipleTradeTypes = allowMultipleTradeTypes;
	}


	/**
	 * @return {@link #templatePath}
	 */

	public String getTemplatePath() {
		return this.templatePath;
	}


	public boolean isAllowMultipleTradeTypes() {
		return this.allowMultipleTradeTypes;
	}
}
