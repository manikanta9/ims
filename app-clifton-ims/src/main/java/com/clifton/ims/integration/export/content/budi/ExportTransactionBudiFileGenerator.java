package com.clifton.ims.integration.export.content.budi;

import com.clifton.bloomberg.client.BloombergMarketDataProvider;
import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.file.BudiFileFormats;
import com.clifton.export.budi.file.BudiSecurityFile;
import com.clifton.export.budi.file.BudiTransactionFile;
import com.clifton.export.budi.security.BudiSecurity;
import com.clifton.export.budi.template.BudiTemplateFactory;
import com.clifton.export.budi.transaction.BudiTransactionService;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.investment.account.util.InvestmentAccountUtilHandler;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityEntryProperty;
import com.clifton.investment.specificity.InvestmentSpecificityService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldMappingSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeType;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Transaction export with associated Bloomberg User Defined Instruments (BUDI) files.
 *
 * @author TerryS
 */
public class ExportTransactionBudiFileGenerator implements ExportContentGenerator<List<BudiTransaction>> {

	private final Map<String, Function<BudiTransaction, String>> outputMap = new LinkedHashMap<>();

	private static final Function<BudiTransaction, String> EmptyStringValue = t -> "";


	public ExportTransactionBudiFileGenerator() {
		this.outputMap.put("EXECUTION_TYPE", BudiTransaction::getExecutionType);
		this.outputMap.put("TRADE_REPORT_ID", BudiTransaction::getTradeReportIdentifier);
		this.outputMap.put("TRANSACT_TIME", BudiTransaction::getTransactionTime);
		this.outputMap.put("BLP_TICKET_TYPE", BudiTransaction::getBlpTicketType);
		this.outputMap.put("SECURITY_SYMBOL", this::getSecuritySymbol);
		this.outputMap.put("SECURITY_TYPE", this::getSecurityType);
		this.outputMap.put("SECURITY_YELLOW_KEY", this::getYellowKey);
		this.outputMap.put("TRANSACTION_ACTION", BudiTransaction::getTransactionAction);
		this.outputMap.put("PRICE", BudiTransaction::getPrice);
		this.outputMap.put("PRICE_TYPE", BudiTransaction::getPriceType);
		this.outputMap.put("QUANTITY", BudiTransaction::getQuantity);
		this.outputMap.put("QUANTITY_TYPE", BudiTransaction::getQuantityType);
		this.outputMap.put("SETTLE_DATE", BudiTransaction::getSettleDate);

		this.outputMap.put("FX_MARKET_TYPE", EmptyStringValue);
		this.outputMap.put("FX_FIXING_DATE", EmptyStringValue);
		this.outputMap.put("FX_SPOT_RATE", EmptyStringValue);
		this.outputMap.put("FX_FWD_POINTS", EmptyStringValue);
		this.outputMap.put("SETTLEMENT_CCY", EmptyStringValue);
		this.outputMap.put("SETTLEMENT_CCY_FX_RATE", EmptyStringValue);

		this.outputMap.put("ACCRUED_INTEREST_AMT", BudiTransaction::getAccruedInterestAmount);
		this.outputMap.put("COUNTERPARTY", BudiTransaction::getCounterParty);
		this.outputMap.put("EXECUTING_FIRM", EmptyStringValue);
		this.outputMap.put("BROKER", BudiTransaction::getBroker);
		this.outputMap.put("CLIENT_ID", EmptyStringValue);
		this.outputMap.put("CLEARING_FIRM", EmptyStringValue);
		this.outputMap.put("SETTLEMENT_LOCATION", EmptyStringValue);
		this.outputMap.put("EXECUTIN_TRADER", EmptyStringValue);
		this.outputMap.put("TRADER", this::getTrader);

		this.outputMap.put("SALESBOOK", EmptyStringValue);
		this.outputMap.put("REPORT_TO_TRACE", EmptyStringValue);
		this.outputMap.put("DELIVER_TO_TRADER", EmptyStringValue);
		this.outputMap.put("PRIME_BROKER", EmptyStringValue);
		this.outputMap.put("PARTIES_BLOCK", EmptyStringValue);
		this.outputMap.put("ALLOCATION_ACCOUNT", EmptyStringValue);
		this.outputMap.put("ALLOCATION_QTY", EmptyStringValue);
		this.outputMap.put("ALLOCATION_BLOCK", EmptyStringValue);

		this.outputMap.put("SHORTNOTE1", EmptyStringValue);
		this.outputMap.put("SHORTNOTE2", EmptyStringValue);
		this.outputMap.put("SHORTNOTE3", EmptyStringValue);
		this.outputMap.put("SHORTNOTE4", EmptyStringValue);
		this.outputMap.put("SHORTNOTE5", EmptyStringValue);
		this.outputMap.put("SHORTNOTE6", EmptyStringValue);
		this.outputMap.put("SHORTNOTE7", EmptyStringValue);
		this.outputMap.put("SHORTNOTE8", EmptyStringValue);
		this.outputMap.put("LONGNOTE1", EmptyStringValue);
		this.outputMap.put("LONGNOTE2", EmptyStringValue);
		this.outputMap.put("LONGNOTE3", EmptyStringValue);
		this.outputMap.put("LONGNOTE4", EmptyStringValue);
		this.outputMap.put("LONGNOTE5", EmptyStringValue);
		this.outputMap.put("LONGNOTE6", EmptyStringValue);
		this.outputMap.put("LONGNOTE7", EmptyStringValue);
		this.outputMap.put("LONGNOTE8", EmptyStringValue);
		this.outputMap.put("NOTES_BLOCK", EmptyStringValue);

		this.outputMap.put("STIPULATION_BLOCK", BudiTransaction::getStipulationBlock);
		this.outputMap.put("TRANSACTION_COST_TYPE1", BudiTransaction::getTransactionCostType1);
		this.outputMap.put("TRANSACTION_COST_AMT1", BudiTransaction::getTransactionCostAmount1);
		this.outputMap.put("TRANSACTION_COST_RATE1", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_FLAG1", BudiTransaction::getTransactionCostFlag1);
		this.outputMap.put("TRANSACTION_COST_CURR1", BudiTransaction::getTransactionCostCurrency1);

		this.outputMap.put("TRANSACTION_COST_TYPE2", BudiTransaction::getTransactionCostType2);
		this.outputMap.put("TRANSACTION_COST_AMT2", BudiTransaction::getTransactionCostAmount2);
		this.outputMap.put("TRANSACTION_COST_RATE2", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_FLAG2", BudiTransaction::getTransactionCostFlag2);
		this.outputMap.put("TRANSACTION_COST_CURR2", BudiTransaction::getTransactionCostCurrency2);

		this.outputMap.put("TRANSACTION_COST_TYPE3", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_AMT3", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_RATE3", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_FLAG3", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_CURR3", EmptyStringValue);

		this.outputMap.put("TRANSACTION_COST_TYPE4", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_AMT4", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_RATE4", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_FLAG4", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_CURR4", EmptyStringValue);

		this.outputMap.put("TRANSACTION_COST_TYPE5", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_AMT5", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_RATE5", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_FLAG5", EmptyStringValue);
		this.outputMap.put("TRANSACTION_COST_CURR5", EmptyStringValue);

		this.outputMap.put("TRANSACTION_COST_BLOCK", EmptyStringValue);
		this.outputMap.put("UUID", EmptyStringValue);
		this.outputMap.put("AIM_Strategy", EmptyStringValue);
	}


	private Integer clientInvestmentAccountGroupId;
	private Integer daysFromToday;
	private DateGenerationOptions dateGenerationOption;

	private BudiTransactionService<BudiTransaction, BudiTransactionFilterImpl> budiTransactionService;
	private InvestmentAccountGroupService investmentAccountGroupService;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataFieldMappingService marketDataFieldMappingService;
	private InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private InvestmentAccountUtilHandler investmentAccountUtilHandler;
	private InvestmentSpecificityService investmentSpecificityService;
	private BudiSecurityPopulatorImpl budiSecurityPopulator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<BudiTransaction> generateContentData(ExportDefinitionContent content, Date previewDate, Map<String, Object> configurationMap) {
		BudiTransactionFilterImpl filter = new BudiTransactionFilterImpl(getClientInvestmentAccountGroup(), getTradeDate(previewDate));
		return getBudiTransactionService().getTransactionList(filter);
	}


	@Override
	public List<FileWrapper> writeContent(List<BudiTransaction> tradeList, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory, Date previewDate) {
		List<FileWrapper> results = new ArrayList<>();

		if (CollectionUtils.isEmpty(tradeList)) {
			return results;
		}
		try {
			Path file = Files.createTempFile(FileUtils.getFileNameWithoutExtension(fileName), "." + FileUtils.getFileExtension(fileName));
			results.add(new FileWrapper(file.toFile(), fileName, true));

			BudiTransactionFile transactionFile = new BudiTransactionFile(file.toFile());
			transactionFile.write(getRowValuesIterator(tradeList), this.outputMap.keySet().toArray(new String[0]));
			List<FileWrapper> securityFiles = writeSecurityFiles(tradeList, fileName, previewDate);
			BudiTemplateFactory.flush();
			results.addAll(securityFiles);
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to generate export file.", e);
		}

		return results;
	}


	@Override
	public String getContentLabel() {
		StringBuilder result = new StringBuilder(100);
		result.append(getDateGenerationOption().getLabel());
		result.append(" ");
		result.append("BUDI export for ");
		InvestmentAccountGroup clientInvestmentAccountGroup = getClientInvestmentAccountGroup();
		if (clientInvestmentAccountGroup != null) {
			result.append(clientInvestmentAccountGroup.getLabel());
		}
		result.append(" ");

		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Iterable<Object[]> getRowValuesIterator(List<BudiTransaction> tradeList) {
		return () -> new Iterator<Object[]>() {
			private final Iterator<BudiTransaction> iterator = tradeList.iterator();


			@Override
			public boolean hasNext() {
				return this.iterator.hasNext();
			}


			@Override
			public Object[] next() {
				BudiTransaction trade = this.iterator.next();
				if (trade != null) {
					return getTradeValues(trade);
				}
				return new Object[0];
			}
		};
	}


	private Object[] getTradeValues(BudiTransaction trade) {
		List<String> values = new ArrayList<>();
		for (Function<BudiTransaction, String> supplier : this.outputMap.values()) {
			values.add(supplier.apply(trade));
		}
		return values.toArray();
	}


	private List<FileWrapper> writeSecurityFiles(List<BudiTransaction> tradeList, String fileName, Date previewDate) {
		List<FileWrapper> results = new ArrayList<>();

		try {
			Map<BudiFileFormats, List<Trade>> tradeMap = categorizeTradeList(tradeList);
			//noinspection KeySetIterationMayUseEntrySet
			for (BudiFileFormats format : tradeMap.keySet()) {
				FileWrapper fileWrapper = createSecurityFile(format, previewDate, FileUtils.getFileExtension(fileName));
				results.add(fileWrapper);
				BudiSecurityFile securityFile = new BudiSecurityFile(format);
				List<Trade> trades = tradeMap.get(format);
				if (!trades.isEmpty()) {
					try (FileWriter writer = new FileWriter(fileWrapper.getFile().toFile())) {
						List<BudiSecurity> converted = convertSecurities(trades);
						securityFile.format(converted, writer);
					}
				}
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to generate export file.", e);
		}
		return results;
	}


	private Map<BudiFileFormats, List<Trade>> categorizeTradeList(List<BudiTransaction> tradeList) {
		Map<BudiFileFormats, List<Trade>> results = new EnumMap<>(BudiFileFormats.class);

		for (BudiTransaction budiTransaction : tradeList) {
			Trade trade = budiTransaction.getTrade();
			InvestmentSecurity security = trade.getInvestmentSecurity();
			if (isBudiFormatted(security)) {
				BudiInstrumentTypes instrumentType = getInstrumentType(security);
				List<Trade> trades = results.computeIfAbsent(instrumentType.getFormat(), k -> new ArrayList<>());
				trades.add(trade);
			}
		}
		return results;
	}


	private List<BudiSecurity> convertSecurities(List<Trade> tradeList) {
		List<BudiSecurity> results = new ArrayList<>();

		for (Trade trade : tradeList) {
			BudiInstrumentTypes instrumentType = getInstrumentType(trade.getInvestmentSecurity());
			results.add(getBudiSecurityPopulator().populate(instrumentType, trade));
		}

		return results;
	}


	private BudiInstrumentTypes getInstrumentType(InvestmentSecurity security) {
		List<InvestmentSpecificityEntry> specificityEntries = getInvestmentSpecificityService()
				.getInvestmentSpecificityEntryListForSecurity(InvestmentSpecificityService.SPECIFICITY_BLOOMBERG_TYPE_MAPPING_DEFINITION, security.getId());
		if (CollectionUtils.getSize(specificityEntries) == 1) {
			InvestmentSpecificityEntry entry = specificityEntries.get(0);
			List<InvestmentSpecificityEntryProperty> properties = entry.getPropertyList();
			if (CollectionUtils.getSize(properties) == 1) {
				InvestmentSpecificityEntryProperty property = properties.get(0);
				String value = property.getValue();
				return BudiInstrumentTypes.valueOf(value);
			}
		}
		return BudiInstrumentTypes.UNKNOWN;
	}


	private FileWrapper createSecurityFile(BudiFileFormats format, Date previewDate, String extension) throws IOException {
		String securityFileName = FileUtils.getFileNameWithoutExtension(format.getFileName("PPA", previewDate));
		Path securityFilePath = Paths.get(FileUtils.SYSTEM_TEMPORARY_DIRECTORY, securityFileName + "." + extension);
		File securityFile = securityFilePath.toFile();
		securityFile.createNewFile();
		return new FileWrapper(securityFile, securityFile.getName(), true);
	}


	private boolean isBudiFormatted(InvestmentSecurity security) {
		BudiInstrumentTypes instrumentType = getInstrumentType(security);
		return (security.getInstrument().getHierarchy().isOtc() && instrumentType != BudiInstrumentTypes.NON_BUDI);
	}


	private boolean isCurrencyTrade(BudiTransaction transaction) {
		Trade trade = transaction.getTrade();
		InvestmentSecurity security = trade.getInvestmentSecurity();
		return security.isCurrency() && TradeType.CURRENCY.equals(trade.getTradeType().getName());
	}


	private boolean isCurrencyForwardTrade(BudiTransaction transaction) {
		Trade trade = transaction.getTrade();
		if (TradeType.FORWARDS.equals(trade.getTradeType().getName())) {
			InvestmentSecurity security = trade.getInvestmentSecurity();
			return security.getUnderlyingSecurity() != null && security.getUnderlyingSecurity().isCurrency();
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSecuritySymbol(BudiTransaction transaction) {
		String results;

		Trade trade = transaction.getTrade();
		InvestmentSecurity security = trade.getInvestmentSecurity();
		if (isBudiFormatted(security)) {
			results = trade.getId() + "_" + security.getId();
		}
		else if (isCurrencyTrade(transaction)) {
			if (trade.isBuy()) {
				results = security.getSymbol() + "/" + trade.getPayingSecurity().getSymbol() + " Curncy";
			}
			else {
				results = trade.getPayingSecurity().getSymbol() + "/" + security.getSymbol() + " Curncy";
			}
		}
		else if (isCurrencyForwardTrade(transaction)) {
			return security.getUnderlyingSecurity().getSymbol() + "/" + security.getInstrument().getTradingCurrency().getSymbol() + " "
					+ DateUtils.fromDate(security.getLastDeliveryDate(), "MM/dd/yyyy") + " Curncy";
		}
		else {
			results = ObjectUtils.coalesce(
					security.getFigi(),
					security.getCusip(),
					security.getIsin(),
					security.getSedol(),
					security.getOccSymbol(),
					security.getSymbol()
			);
		}
		return results;
	}


	public String getSecurityType(BudiTransaction transaction) {
		String results;
		Trade trade = transaction.getTrade();
		InvestmentSecurity security = trade.getInvestmentSecurity();
		if (isBudiFormatted(security)) {
			return "";
		}
		String symbol = getSecuritySymbol(transaction);
		if (Objects.equals(symbol, security.getFigi())) {
			results = "112";
		}
		else if (Objects.equals(symbol, security.getCusip())) {
			results = "1";
		}
		else if (Objects.equals(symbol, security.getIsin())) {
			results = "4";
		}
		else if (Objects.equals(symbol, security.getSedol())) {
			results = "103";
		}
		else if (Objects.equals(symbol, security.getOccSymbol())) {
			results = "108";
		}
		else if (Objects.equals(symbol, security.getSymbol())) {
			results = "108";
		}
		else if (isCurrencyTrade(transaction)) {
			results = "104";
		}
		else if (isCurrencyForwardTrade(transaction)) {
			return "104";
		}
		else {
			results = "0";
		}
		return results;
	}


	public String getTrader(BudiTransaction transaction) {
		Trade trade = transaction.getTrade();
		if (trade.getHoldingInvestmentAccount() != null) {
			InvestmentAccount investmentAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurposeStrict(trade.getClientInvestmentAccount().getId(),
					trade.getHoldingInvestmentAccount().getId(), trade.getInvestmentSecurity().getId(), InvestmentAccountRelationshipPurpose.CUSTODIAN_ACCOUNT_PURPOSE_NAME, trade.getTradeDate());
			if (investmentAccount != null) {
				Object value = getInvestmentAccountUtilHandler().getHoldingInvestmentAccountCustomColumnValue(investmentAccount, "AdditionalReportingAccountNumber");
				return value == null ? investmentAccount.getNumber() : value.toString();
			}
		}
		return "3220OA22A";
	}


	private String getYellowKey(BudiTransaction transaction) {
		if (isCurrencyTrade(transaction)) {
			return "Curncy";
		}
		else if (isCurrencyForwardTrade(transaction)) {
			return "Curncy";
		}
		return getYellowKey(transaction.getTrade().getInvestmentSecurity());
	}


	private String getYellowKey(InvestmentSecurity security) {
		MarketDataSource marketDataSource = getMarketDataSourceService().getMarketDataSourceByName(BloombergMarketDataProvider.DATASOURCE_NAME);
		ValidationUtils.assertNotNull(marketDataSource, "Market data source expected " + BloombergMarketDataProvider.DATASOURCE_NAME);

		MarketDataFieldMappingSearchForm searchForm = new MarketDataFieldMappingSearchForm();
		searchForm.setDataSourceId(marketDataSource.getId());
		List<MarketDataFieldMapping> marketDataFieldMappingList = getMarketDataFieldMappingService().getMarketDataFieldMappingList(searchForm);

		InvestmentInstrument instrument = security.getInstrument();
		InvestmentInstrumentHierarchy hierarchy = instrument.getHierarchy();
		InvestmentType type = hierarchy.getInvestmentType();
		InvestmentTypeSubType subType = hierarchy.getInvestmentTypeSubType();
		InvestmentTypeSubType2 subType2 = hierarchy.getInvestmentTypeSubType2();

		List<String> sectorPriorityList = Stream.of(
				// instrument
				marketDataFieldMappingList.stream()
						.filter(m -> m.getInstrument() != null)
						.filter(m -> Objects.equals(m.getInstrument(), instrument))
						.map(MarketDataFieldMapping::getMarketSector)
						.collect(Collectors.toList()),
				// hierarchy
				marketDataFieldMappingList.stream()
						.filter(m -> m.getInstrumentHierarchy() != null)
						.filter(m -> Objects.equals(m.getInstrumentHierarchy(), hierarchy))
						.map(MarketDataFieldMapping::getMarketSector)
						.collect(Collectors.toList()),
				// subtype, subtype2, type
				marketDataFieldMappingList.stream()
						.filter(m -> m.getInvestmentType() != null)
						.filter(m -> m.getInvestmentTypeSubType() != null)
						.filter(m -> m.getInvestmentTypeSubType2() != null)
						.filter(m -> Objects.equals(m.getInvestmentTypeSubType2(), subType2))
						.filter(m -> Objects.equals(m.getInvestmentTypeSubType(), subType))
						.filter(m -> Objects.equals(m.getInvestmentType(), type))
						.map(MarketDataFieldMapping::getMarketSector)
						.collect(Collectors.toList()),
				// subtype2, type, subtype == null
				marketDataFieldMappingList.stream()
						.filter(m -> m.getInvestmentType() != null)
						.filter(m -> m.getInvestmentTypeSubType() == null)
						.filter(m -> m.getInvestmentTypeSubType2() != null)
						.filter(m -> Objects.equals(m.getInvestmentTypeSubType2(), subType2))
						.filter(m -> Objects.equals(m.getInvestmentType(), type))
						.map(MarketDataFieldMapping::getMarketSector)
						.collect(Collectors.toList()),
				// type, subtype, subtype2 = null
				marketDataFieldMappingList.stream()
						.filter(m -> m.getInvestmentType() != null)
						.filter(m -> m.getInvestmentTypeSubType() != null)
						.filter(m -> m.getInvestmentTypeSubType2() == null)
						.filter(m -> Objects.equals(m.getInvestmentTypeSubType(), subType))
						.filter(m -> Objects.equals(m.getInvestmentType(), type))
						.map(MarketDataFieldMapping::getMarketSector)
						.collect(Collectors.toList()),
				// type, subtype = subtype2 = null
				marketDataFieldMappingList.stream()
						.filter(m -> m.getInvestmentType() != null)
						.filter(m -> m.getInvestmentTypeSubType() == null)
						.filter(m -> m.getInvestmentTypeSubType2() == null)
						.filter(m -> Objects.equals(m.getInvestmentType(), type))
						.map(MarketDataFieldMapping::getMarketSector)
						.collect(Collectors.toList())
		)
				// ambiguous not allowed
				.filter(l -> l.size() == 1)
				.map(l -> l.get(0))
				.map(NamedEntity::getName)
				.collect(Collectors.toList());

		return sectorPriorityList.isEmpty() ? "Other" : sectorPriorityList.get(0);
	}


	private InvestmentAccountGroup getClientInvestmentAccountGroup() {
		if (getClientInvestmentAccountGroupId() != null) {
			return getInvestmentAccountGroupService().getInvestmentAccountGroup(getClientInvestmentAccountGroupId());
		}
		return null;
	}


	private Date getTradeDate(Date previewDate) {
		return previewDate != null ? previewDate :
				getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public BudiTransactionService<BudiTransaction, BudiTransactionFilterImpl> getBudiTransactionService() {
		return this.budiTransactionService;
	}


	public void setBudiTransactionService(BudiTransactionService<BudiTransaction, BudiTransactionFilterImpl> budiTransactionService) {
		this.budiTransactionService = budiTransactionService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public InvestmentAccountUtilHandler getInvestmentAccountUtilHandler() {
		return this.investmentAccountUtilHandler;
	}


	public void setInvestmentAccountUtilHandler(InvestmentAccountUtilHandler investmentAccountUtilHandler) {
		this.investmentAccountUtilHandler = investmentAccountUtilHandler;
	}


	public InvestmentSpecificityService getInvestmentSpecificityService() {
		return this.investmentSpecificityService;
	}


	public void setInvestmentSpecificityService(InvestmentSpecificityService investmentSpecificityService) {
		this.investmentSpecificityService = investmentSpecificityService;
	}


	public BudiSecurityPopulatorImpl getBudiSecurityPopulator() {
		return this.budiSecurityPopulator;
	}


	public void setBudiSecurityPopulator(BudiSecurityPopulatorImpl budiSecurityPopulator) {
		this.budiSecurityPopulator = budiSecurityPopulator;
	}
}
