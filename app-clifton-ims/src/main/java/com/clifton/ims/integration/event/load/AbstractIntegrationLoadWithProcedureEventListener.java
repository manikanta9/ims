package com.clifton.ims.integration.event.load;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import org.hibernate.Criteria;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class AbstractIntegrationLoadWithProcedureEventListener extends BaseEventListener<IntegrationImportEvent> {

	private AdvancedReadOnlyDAO<IntegrationLoadError, Criteria> integrationLoadErrorDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run, "No run exists for [" + run.getId() + "] for event with name [" + event.getEventName() + "].");

		loadExternalData(run.getId());
	}


	/**
	 * Return the name of the query to load.
	 */
	public abstract String getQueryName();


	/**
	 * Loads integration data into IMS for the specified queryName.  Accepts runId as default query parameter, and optional
	 * argsOverride when runId is not needed
	 *
	 * @param runId        The default query parameter for integration loads
	 * @param argsOverride Optional argument to override the default runId query parameter.
	 *                     If not empty, argsOverride will be sent as the query parameter to the load procedure instead of default runId
	 */
	@DoNotAddRequestMapping
	public void loadExternalData(Integer runId, Object... argsOverride) {
		// pass run id only if it's not null: some stored procedures are not run specific and don't use it
		List<Object> fullArgs = new ArrayList<>();
		if (runId != null) {
			fullArgs.add(runId);
		}
		if (argsOverride != null) {
			Collections.addAll(fullArgs, argsOverride);
		}

		List<IntegrationLoadError> errors = doLoadExternalData(getQueryName(), fullArgs.toArray());

		if (errors != null && !errors.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			for (IntegrationLoadError error : CollectionUtils.getIterable(errors)) {
				sb.append(error).append("\n");
			}
			throw new RuntimeException(sb.toString());
		}
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW, timeout = 180)
	protected List<IntegrationLoadError> doLoadExternalData(String query, Object... args) {
		return new ArrayList<>(getIntegrationLoadErrorDAO().findByNamedQuery(query, args));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<IntegrationLoadError, Criteria> getIntegrationLoadErrorDAO() {
		return this.integrationLoadErrorDAO;
	}


	public void setIntegrationLoadErrorDAO(AdvancedReadOnlyDAO<IntegrationLoadError, Criteria> integrationLoadErrorDAO) {
		this.integrationLoadErrorDAO = integrationLoadErrorDAO;
	}
}
