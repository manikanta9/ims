package com.clifton.ims.integration.export.content.field;


import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BondTradeExportFieldGenerator</code> generates extra property information for bond trades for exports.
 *
 * @author jgommels
 */
@Component("bondTradeExportFieldGenerator")
public class BondTradeExportFieldGenerator {

	public Map<String, Object> generateFields(Trade trade) {
		Map<String, Object> fieldMap = new HashMap<>();
		BigDecimal interestAmount = trade.getAccrualAmount() == null ? BigDecimal.ZERO : trade.getAccrualAmount();
		BigDecimal principalAmount = trade.getAccountingNotional();
		BigDecimal settlementAmount = MathUtils.add(principalAmount, interestAmount);

		fieldMap.put("interestAmount", interestAmount.doubleValue());
		fieldMap.put("principalAmount", principalAmount.doubleValue());
		fieldMap.put("settlementAmount", settlementAmount.doubleValue());

		return fieldMap;
	}
}
