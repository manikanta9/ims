package com.clifton.ims.integration.batch.ftp.export;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.ftp.FtpFileResult;
import com.clifton.core.util.status.Status;

import java.util.Date;
import java.util.List;


/**
 * The <code>FtpExportBean</code> defines a bean that creates files to be sent via FTP.  Used by FtpExportJob.
 *
 * @author mwacker
 */
public interface FtpExportBean {

	/**
	 * Return a list of file wrappers for the files being sent via FTP.
	 */
	public List<FileWrapper> getExportFileList(Date reportingDate, Status status);


	/**
	 * Do something with the failed files.
	 */
	public void handleFileResult(FtpFileResult fileResult, Status status);
}
