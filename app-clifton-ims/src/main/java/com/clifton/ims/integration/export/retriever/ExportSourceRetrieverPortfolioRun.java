package com.clifton.ims.integration.export.retriever;


import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;

import java.util.Date;


public class ExportSourceRetrieverPortfolioRun implements ExportSourceRetriever {

	private PortfolioRunService portfolioRunService;


	@Override
	public Integer getSourceId(Integer clientInvestmentAccountId, @SuppressWarnings("unused") String sourceSystemTableName, Date exportDate) {
		PortfolioRun run = getPortfolioRunService().getPortfolioRunByMainRun(clientInvestmentAccountId, exportDate);
		return run != null ? run.getId() : null;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
