package com.clifton.ims.integration.event.m2m;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.reconcile.m2m.IntegrationReconcileM2MDailyService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.reconcile.ReconcileHandler;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;

import java.util.Date;
import java.util.Map;


/**
 * Base methods for loading M2M reconciliation data.
 *
 * @author mwacker
 */
public abstract class AbstractReconcileM2MIntegrationEventListener extends BaseEventListener<IntegrationImportEvent> {

	private AccountingM2MService accountingM2MService;
	private BusinessCompanyService businessCompanyService;
	private IntegrationReconcileM2MDailyService integrationReconcileM2MDailyService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private ReconcileHandler reconcileHandler;
	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected InvestmentAccount lookupHoldingAccount(String accountNumber, BusinessCompany issuingCompany) {
		InvestmentAccount holdingAccount = getReconcileHandler().lookupInvestmentHoldingAccount(accountNumber, issuingCompany != null ? issuingCompany.getId() : null);
		if (holdingAccount == null) {
			throw new RuntimeException("Cannot find an investment account for [" + accountNumber + "] issued by [" + (issuingCompany != null ? issuingCompany.getLabel() : "UNKNOWN") + "]");
		}

		return holdingAccount;
	}


	protected AccountingM2MDaily buildNewM2MDaily(InvestmentAccount holdingAccount, Date positionDate, InvestmentSecurity markCurrency, boolean m2mInLocalCurrency) {
		AccountingM2MDaily m2mInternalDaily = new AccountingM2MDaily();

		InvestmentAccount custodianAccount = null;
		try {
			custodianAccount = getAccountingM2MService().getAccountingM2MInvestmentAccount(holdingAccount, positionDate, false);
		}
		catch (ValidationException e) {
			// It's possible that there is no custodian account for hold accounts, so suppressing ValidationException here
		}
		InvestmentAccount clientAccount = getAccountingM2MService().getAccountingM2MInvestmentAccount(holdingAccount, positionDate, true);

		m2mInternalDaily.setClientInvestmentAccount(clientAccount);
		m2mInternalDaily.setCustodianInvestmentAccount(custodianAccount);
		m2mInternalDaily.setHoldingInvestmentAccount(holdingAccount);
		if (m2mInLocalCurrency) {
			m2mInternalDaily.setMarkCurrency(markCurrency);
		}
		m2mInternalDaily.setMarkDate(positionDate);

		return m2mInternalDaily;
	}


	protected boolean isM2MinLocalCurrency(InvestmentAccount account, Map<Integer, Boolean> markTypeCache) {
		// account that uses CAD and USD marks: 157-85098
		return markTypeCache.computeIfAbsent(account.getId(), k -> getAccountingM2MService().isM2MinLocalCurrency(account));
	}


	protected AccountingM2MDaily findAccountingM2MDaily(Date markDate, InvestmentAccount holdingAccount, InvestmentSecurity markCurrency) {
		return getAccountingM2MService().getAccountingM2MDailyByHoldingAccount(holdingAccount, markDate, markCurrency);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public ReconcilePositionExternalDailyService getReconcilePositionExternalDailyService() {
		return this.reconcilePositionExternalDailyService;
	}


	public void setReconcilePositionExternalDailyService(ReconcilePositionExternalDailyService reconcilePositionExternalDailyService) {
		this.reconcilePositionExternalDailyService = reconcilePositionExternalDailyService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public ReconcileHandler getReconcileHandler() {
		return this.reconcileHandler;
	}


	public void setReconcileHandler(ReconcileHandler reconcileHandler) {
		this.reconcileHandler = reconcileHandler;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public IntegrationReconcileM2MDailyService getIntegrationReconcileM2MDailyService() {
		return this.integrationReconcileM2MDailyService;
	}


	public void setIntegrationReconcileM2MDailyService(IntegrationReconcileM2MDailyService integrationReconcileM2MDailyService) {
		this.integrationReconcileM2MDailyService = integrationReconcileM2MDailyService;
	}
}
