package com.clifton.ims.integration.event;


import com.clifton.core.util.AssertUtils;
import com.clifton.ims.integration.event.load.AbstractIntegrationLoadWithProcedureEventListener;
import com.clifton.ims.integration.service.ReconcilePositionExternalDailyDataService;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurposeService;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;
import org.springframework.stereotype.Component;


@Component
public class ReconcilePositionAndPNLEventListener extends AbstractIntegrationLoadWithProcedureEventListener {

	private MarketDataSourcePurposeService marketDataSourcePurposeService;
	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;
	private ReconcilePositionExternalDailyDataService reconcilePositionExternalDailyDataService;
	private String eventName = IntegrationImportRun.INTEGRATION_EVENT_BROKER_DAILY_POSITIONS;
	private String queryName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public synchronized String getQueryName() {
		return this.queryName;
	}


	@Override
	public synchronized void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run, "No run exists for [" + event.getTarget() + "] for event with name [" + event.getEventName() + "].");

		Short purposeId = run.getFile().getFileDefinition().getPriceMarketDataSourcePurposeId();

		// get the name of the procedure to run
		if ("Broker Import Positions".equals(run.getIntegrationImportDefinition().getType().getName())) {
			this.queryName = "reconcile-loadReconcilePositionExternalDailyData";
			if (purposeId == null) {
				MarketDataSourcePurpose purpose = getMarketDataSourcePurposeService().getMarketDataSourcePurposeByName(MarketDataSourcePurpose.MARKET_DATA_SOURCE_PURPOSE_NAME_POSITIONS);
				purposeId = purpose != null ? purpose.getId() : purposeId;
			}
			MarketDataSourcePurpose marketDataSourcePurpose = (purposeId != null) ? getMarketDataSourcePurposeService().getMarketDataSourcePurpose(purposeId) : null;
			// INTEGRATION-276
			getReconcilePositionExternalDailyDataService().loadReconcilePositionExternalDailyData(run, marketDataSourcePurpose);
			//loadExternalData(run.getId(), purposeId);
		}
		else if ("Broker Import Daily PNL".equals(run.getIntegrationImportDefinition().getType().getName())) {
			this.queryName = "reconcile-loadReconcilePositionExternalDailyPNLData";
			if (purposeId == null) {
				MarketDataSourcePurpose purpose = getMarketDataSourcePurposeService().getMarketDataSourcePurposeByName(MarketDataSourcePurpose.MARKET_DATA_SOURCE_PURPOSE_NAME_ACTIVITY);
				purposeId = purpose != null ? purpose.getId() : purposeId;
			}
			loadExternalData(run.getId(), purposeId);
		}
		else {
			throw new RuntimeException("No loading procedure is defined for type [" + run.getIntegrationImportDefinition().getType().getName() + "] in event with name [" + event.getEventName() + "]");
		}
		getReconcilePositionExternalDailyService().loadReconcilePriceOverrides(run.getEffectiveDate());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String integrationEventName) {
		this.eventName = integrationEventName;
	}


	public ReconcilePositionExternalDailyService getReconcilePositionExternalDailyService() {
		return this.reconcilePositionExternalDailyService;
	}


	public void setReconcilePositionExternalDailyService(ReconcilePositionExternalDailyService reconcilePositionExternalDailyService) {
		this.reconcilePositionExternalDailyService = reconcilePositionExternalDailyService;
	}


	public ReconcilePositionExternalDailyDataService getReconcilePositionExternalDailyDataService() {
		return this.reconcilePositionExternalDailyDataService;
	}


	public void setReconcilePositionExternalDailyDataService(ReconcilePositionExternalDailyDataService reconcilePositionExternalDailyDataService) {
		this.reconcilePositionExternalDailyDataService = reconcilePositionExternalDailyDataService;
	}


	public MarketDataSourcePurposeService getMarketDataSourcePurposeService() {
		return this.marketDataSourcePurposeService;
	}


	public void setMarketDataSourcePurposeService(MarketDataSourcePurposeService marketDataSourcePurposeService) {
		this.marketDataSourcePurposeService = marketDataSourcePurposeService;
	}
}
