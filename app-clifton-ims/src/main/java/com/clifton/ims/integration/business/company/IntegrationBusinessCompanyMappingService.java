package com.clifton.ims.integration.business.company;

import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.migrate.schema.Column;

import java.util.List;


/**
 * <code>IntegrationBusinessCompanyMappingService</code> Load the existing {@link BusinessCompany} using the provided Business Company ID, lookup the
 * {@link com.clifton.integration.incoming.business.IntegrationBusinessCompany} using the business Comapany's Bloomberg Business Company ID,
 * map columns to the {@link BusinessCompany} and return the populated BusinessCompany.
 * <p>
 * <emp>Exceptions are swallowed and added to the returned Status Object, the caller must decide if the update should be all or nothing.</emp>
 */
public interface IntegrationBusinessCompanyMappingService {


	/**
	 * Update the provided business company from the integration data based on matching Bloomberg identifiers, all mapped columns are updated.
	 */
	public IntegrationBusinessCompanyMappingResults getBusinessCompanyFromIntegrationBusinessCompany(int businessCompanyId);


	/**
	 * Update the provided business company from integration data based on matching Bloomberg identifiers, limit the mapped columns to those provided.
	 */
	public IntegrationBusinessCompanyMappingResults getBusinessCompanyFromIntegrationBusinessCompanyWithExclusions(int businessCompanyId, final List<Column> excludedColumnList);


	/**
	 * For all mapped Business Company column names, lookup and return the meta data Column.  Used by the batch job to perform validations to ensure the user provided list of
	 * of columns are actually updated.
	 */
	public List<Column> getBusinessCompanyMappedColumns();


	/**
	 * For all mapped Integration Business Company column names, lookup and return the meta data Column.  Used by the batch job to perform validations to ensure the user
	 * provided list of of columns are actually updated.
	 */
	public List<Column> getIntegrationBusinessCompanyMappedColumns();
}
