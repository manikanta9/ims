package com.clifton.ims.integration.batch.ftp.export;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToCsvFileConverter;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.ftp.FtpFileResult;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.search.InvestmentAccountGroupSearchForm;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>ElectraFtpExportJob</code> creates zip file of all Electra Export queries.
 *
 * @author mwacker
 */
public class ElectraFtpExportJob implements FtpExportBean {

	/**
	 * Electra account group name.  Used look up accountGroupId when accountGroupId is null.
	 */
	private static final String ELECTRA_ACCOUNT_GROUP_NAME = "Electra Accounts";
	/**
	 * Electra account group id.  Should be populate by the system bean property, but if it not the account group name will be used to look it up.
	 */
	private Integer accountGroupId;
	/**
	 * Comma delimited list of Electra Export query names.  Each query will be it's own file, and all files will be zipped into 1 file.
	 */
	private String queryNameList = "Electra Account File,Electra Cash File,Electra Client File,Electra Holdings File,Electra Securities File,Electra Transactions File";
	/**
	 * Date format for Electra file names.
	 */
	private static final String DATE_FORMAT = "yyyyMMdd";

	private SystemQueryExecutionService systemQueryExecutionService;
	private SystemQueryService systemQueryService;
	private InvestmentAccountGroupService investmentAccountGroupService;


	@Override
	public List<FileWrapper> getExportFileList(Date reportDate, @SuppressWarnings("unused") Status status) {
		List<FileWrapper> result = new ArrayList<>();
		InvestmentAccountGroup accountGroup = getInvestmentAccountGroup();
		String[] queries = getQueryNameList().split(",");
		for (String queryName : queries) {
			if (!StringUtils.isEmpty(queryName)) {
				File file = getFile(queryName, accountGroup, reportDate);
				result.add(new FileWrapper(file, getFileName(queryName, reportDate), true));
			}
		}
		if (!result.isEmpty()) {
			String zippedFileName = FileUtils.getFilePath(result.get(0).getFile().getPath()) + DateUtils.fromDate(reportDate, DATE_FORMAT) + "TCG.ZIP";

			FileContainer zippedFile = ZipUtils.zipFiles(result, zippedFileName);
			return CollectionUtils.createList(new FileWrapper(FilePath.forPath(zippedFile.getPath()), zippedFile.getName(), true));
		}
		return null;
	}


	@Override
	public void handleFileResult(FtpFileResult fileResult, Status status) {
		if (!fileResult.isSuccess()) {
			if (FileUtils.fileExists(fileResult.getFile().getFile())) {
				FileUtils.delete(fileResult.getFile().getFile());
			}
			status.addError(fileResult.getError().toString());
		}
	}


	private File getFile(String queryName, InvestmentAccountGroup accountGroup, Date reportDate) {

		SystemQuery query = getSystemQueryService().getSystemQueryByName(queryName);
		ValidationUtils.assertNotNull(query, "No system query found with name [" + queryName + "].");

		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		List<SystemQueryParameterValue> parameterValueList = new ArrayList<>();
		for (SystemQueryParameter parameter : CollectionUtils.getIterable(parameterList)) {
			SystemQueryParameterValue parameterValue = null;
			switch (parameter.getName()) {
				case "Investment Account Group":
					parameterValue = new SystemQueryParameterValue();
					parameterValue.setParameter(parameter);
					parameterValue.setValue(accountGroup.getId().toString());
					parameterValue.setText(accountGroup.getName());
					break;
				case "Reporting Date":
				case "Report Date":
				case "ReportingDate":
				case "ReportDate":
					parameterValue = new SystemQueryParameterValue();
					parameterValue.setParameter(parameter);
					parameterValue.setValue(DateUtils.fromDate(reportDate, DateUtils.DATE_FORMAT_INPUT));
					parameterValue.setText(DateUtils.fromDate(reportDate, DateUtils.DATE_FORMAT_INPUT));
					break;
			}
			if (parameterValue != null) {
				parameterValueList.add(parameterValue);
			}
		}
		query.setParameterValueList(parameterValueList);

		DataTable result = getSystemQueryExecutionService().getSystemQueryResult(query);
		DataTableFileConfig config = new DataTableFileConfig(result, DataTableFileConfig.DATA_TABLE_HEADER_EXPORT_STYLE, null);
		return (new DataTableToCsvFileConverter()).convert(config);
	}


	/**
	 * Get the custom extension for the Electra query files.
	 *
	 * @param queryName
	 * @param reportingDate
	 */
	private String getFileName(String queryName, Date reportingDate) {
		String baseFileName = DateUtils.fromDate(reportingDate, DATE_FORMAT) + "_TCG_01";
		String extension = "";
		switch (queryName) {
			case "Electra Account File":
				extension = ".act";
				break;
			case "Electra Cash File":
				extension = ".csh";
				break;
			case "Electra Client File":
				extension = ".cli";
				break;
			case "Electra Holdings File":
				extension = ".hld";
				break;
			case "Electra Securities File":
				extension = ".sec";
				break;
			case "Electra Transactions File":
				extension = ".trn";
				break;
		}
		return baseFileName + extension;
	}


	private InvestmentAccountGroup getInvestmentAccountGroup() {
		if (getAccountGroupId() != null) {
			return getInvestmentAccountGroupService().getInvestmentAccountGroup(getAccountGroupId());
		}
		InvestmentAccountGroupSearchForm sf = new InvestmentAccountGroupSearchForm();
		sf.setName(ELECTRA_ACCOUNT_GROUP_NAME);
		return CollectionUtils.getOnlyElement(getInvestmentAccountGroupService().getInvestmentAccountGroupList(sf));
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public String getQueryNameList() {
		return this.queryNameList;
	}


	public void setQueryNameList(String queryNameList) {
		this.queryNameList = queryNameList;
	}


	public Integer getAccountGroupId() {
		return this.accountGroupId;
	}


	public void setAccountGroupId(Integer accountGroupId) {
		this.accountGroupId = accountGroupId;
	}
}
