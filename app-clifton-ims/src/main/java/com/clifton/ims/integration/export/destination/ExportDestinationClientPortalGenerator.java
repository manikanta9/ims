package com.clifton.ims.integration.export.destination;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.destination.ExportDestinationGenerator;
import com.clifton.export.file.ExportFile;
import com.clifton.export.file.ExportFileUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author theodorez
 */
public class ExportDestinationClientPortalGenerator implements ExportDestinationGenerator {

	private String reportDateOption;
	private String publishDateOption;

	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private ReportPostingController<?> reportPostingController;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processInternalDestination(List<ExportFile> files, Map<String, Object> configurationMap) {
		Date reportDate = getCalendarDateGenerationHandler().generateDate(getReportDateOption());
		Date publishDate = getCalendarDateGenerationHandler().generateDate(getPublishDateOption());

		ValidationUtils.assertNotEmpty(files, "Cannot process Client Portal Destination if Export File list is empty.");
		for (ExportFile file : files) {
			Map<ExportContentContextMapKeys, Object> context = file.getFileContext();
			if (context != null) {
				Object configObj = context.get(ExportContentContextMapKeys.REPORT_POSTING_FILE_CONFIG);
				if (configObj instanceof ReportPostingFileConfiguration) {
					ReportPostingFileConfiguration reportConfig = (ReportPostingFileConfiguration) configObj;
					reportConfig.setReportDate(reportDate);
					reportConfig.setPublishDate(publishDate);
					reportConfig.setFileWrapper(new FileWrapper(new File(file.getPath()), ExportFileUtils.removeUniqueIdentifier(FileUtils.getFileName(file.getPath())), false)); //temp file is set elsewhere
					getReportPostingController().postClientFile(reportConfig, null);
				}
			}
		}
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.PORTAL;
	}


	@Override
	public String getDestinationLabel() {
		return "Report Date: " + getReportDateOption() + " Publish Date: " + getPublishDateOption();
	}


	@Override
	public Map<ExportMapKeys, String> generateDestination(ExportDefinitionDestination destination, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getReportDateOption() {
		return this.reportDateOption;
	}


	public void setReportDateOption(String reportDateOption) {
		this.reportDateOption = reportDateOption;
	}


	public String getPublishDateOption() {
		return this.publishDateOption;
	}


	public void setPublishDateOption(String publishDateOption) {
		this.publishDateOption = publishDateOption;
	}


	public ReportPostingController<?> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<?> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}
}
