package com.clifton.ims.integration.export.content;


import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.report.definition.BaseReportBeanPropertyProvider;
import com.clifton.report.definition.template.ReportTemplateParameterValue;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


public class ExportContentReportGenerator extends BaseReportBeanPropertyProvider implements ExportContentGenerator<Object> {

	/**
	 * The file format for the export.
	 */
	private FileFormats format;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private ReportExportService reportExportService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FileWrapper> writeContent(Object data, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory, Date previewDate) {

		List<ReportTemplateParameterValue> valueList = getPropertyList(null);
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		for (ReportTemplateParameterValue parameterValue : CollectionUtils.getIterable(valueList)) {
			String value = parameterValue.getValue();
			// TODO: move the date look up logic to a central location.
			if (DataTypeNames.DATE == DataTypeNames.valueOf(parameterValue.getParameter().getDataType().getName())) {
				value = previewDate != null ? DateUtils.fromDate(previewDate, DateUtils.DATE_FORMAT_INPUT) :
						DateUtils.fromDate(getCalendarDateGenerationHandler().generateDate(parameterValue.getValue()), DateUtils.DATE_FORMAT_INPUT);
			}
			parameters.put(parameterValue.getParameter().getTemplateParameterName(), value);
		}

		ReportExportConfiguration exportConfig = new ReportExportConfiguration();
		exportConfig.setExportFormat(getFormat());
		exportConfig.setReportId(getReportId());
		exportConfig.setParameterMap(parameters);
		exportConfig.setRegenerateCache(true);
		FileWrapper wrapper = getReportExportService().downloadReportFile(exportConfig);
		wrapper.setFileName(fileName);
		return CollectionUtils.createList(wrapper);
	}


	@Override
	public String getContentLabel() {
		StringBuilder resultSb = new StringBuilder(100);

		if (getReport() != null) {
			resultSb.append(getReport().getLabel());
			resultSb.append(' ');
		}

		if (getFormat() != null) {
			resultSb.append('(').append(getFormat()).append(')');
		}

		return resultSb.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FileFormats getFormat() {
		return this.format;
	}


	public void setFormat(FileFormats format) {
		this.format = format;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}
}
