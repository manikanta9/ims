package com.clifton.ims.integration.event;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.marketdata.rates.IntegrationMarketDataExchangeRate;
import com.clifton.integration.incoming.marketdata.rates.IntegrationMarketDataExchangeRateService;
import com.clifton.integration.incoming.marketdata.rates.search.IntegrationMarketDataExchangeRateSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataRatesService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataExchangeRatesEventListener</code> loads data from the IntegrationMarketExchangeRate table
 * into the MarketDataExchangeRate table in IMS.  It only loads rates for foreign currencies to USD.  I does not load
 * exchange rates between foreign currencies, nor does it load inverse rates from USD to foreign.  Also, it filters out currencies
 * that are not defined in our system.
 *
 * @author rbrooks
 */
@Component
public class MarketDataExchangeRateEventListener extends BaseIntegrationEventListener {

	private final static String EVENT_NAME = IntegrationImportRun.INTEGRATION_EVENT_MARKET_DATA_DAILY_EXCHANGE_RATES;

	private IntegrationMarketDataExchangeRateService integrationMarketDataExchangeRateService;
	private MarketDataRatesService marketDataRatesService;
	private InvestmentInstrumentService investmentInstrumentService;
	private BusinessCompanyService businessCompanyService;
	private DataTableRetrievalHandler dataTableRetrievalHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(IntegrationImportEvent event) {
		IntegrationImportRun run = event.getTarget();
		AssertUtils.assertNotNull(run.getIntegrationImportDefinition().getFileDefinition().getSource(), "An import source is required for Collateral events processing.");

		// Get new exchange rates from Integration
		IntegrationMarketDataExchangeRateSearchForm integrationExchangeRateSearchForm = new IntegrationMarketDataExchangeRateSearchForm();
		integrationExchangeRateSearchForm.setRunId(run.getId());
		List<IntegrationMarketDataExchangeRate> newExchangeRates = getIntegrationMarketDataExchangeRateService().getIntegrationMarketDataExchangeRateList(integrationExchangeRateSearchForm);

		// Lookup MarketDataSource from SourceCompany
		int sourceCompanyId = run.getIntegrationImportDefinition().getFileDefinition().getSource().getSourceCompanyId();
		BusinessCompany sourceCompany = getBusinessCompanyService().getBusinessCompany(sourceCompanyId);
		MarketDataSource marketDataSource = getMarketDataSourceForCompany(sourceCompany);

		// Query for base-currencies used by active clients in our system
		DataTable validBaseCurrencies = getDataTableRetrievalHandler().findDataTable(
				"SELECT s.Symbol FROM(SELECT cc.FromCurrencyInvestmentSecurityID \"CurrencyInvestmentSecurityID\" FROM InvestmentCurrencyConvention cc UNION SELECT cc.ToCurrencyInvestmentSecurityID FROM InvestmentCurrencyConvention cc) x INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = x.CurrencyInvestmentSecurityID");

		// Make a list of valid base currencies
		ArrayList<String> validBaseCurrenciesList = new ArrayList<>();

		// For each base currency returned by the query, add to validBaseCurrenciesList
		for (int i = 0; i < validBaseCurrencies.getTotalRowCount(); i++) {

			String symbol = (String) validBaseCurrencies.getRow(i).getValue("Symbol");
			if (symbol != null) {
				validBaseCurrenciesList.add(symbol);
			}
		}

		// For each new exchange rate, see if it already exists in IMS.  If it exists, update.  If not, insert.
		for (IntegrationMarketDataExchangeRate newExchangeRate : CollectionUtils.getIterable(newExchangeRates)) {

			// If the exchange rate's "to-currency" is not a valid base currency for one of our clients, continue
			if (!validBaseCurrenciesList.contains(newExchangeRate.getToCurrency())) {
				continue;
			}

			// If fromCurrency and toCurrency are the same, continue 
			if (newExchangeRate.getFromCurrency().equals(newExchangeRate.getToCurrency())) {
				continue;
			}

			// Get currency from IMS that matches the new exchange rate's "from-currency"
			InvestmentSecurity fromCurrency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(newExchangeRate.getFromCurrency(), true);

			// Get currency from IMS that matches the new exchange rate's "to-currency"
			InvestmentSecurity toCurrency = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(newExchangeRate.getToCurrency(), true);

			// If from-currency is undefined in the system, skip the update
			if (fromCurrency == null) {
				informUndefinedCurrency(newExchangeRate.getFromCurrency());
				continue;
			}

			// Prepare from-currency, to-currency, and rateDate to look up matching IMS exchange rate
			int fromCurrencyId = fromCurrency.getId();
			int toCurrencyId = toCurrency.getId();
			Date rateDate = newExchangeRate.getRateDate();

			// Look up matching IMS exchange rate.  If there is one, we will update, if not we will create new record
			MarketDataExchangeRate imsExchangeRate = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDate(marketDataSource.getName(), fromCurrencyId, toCurrencyId, rateDate);

			// If there is not a matching exchange rate, save the new one
			if (imsExchangeRate == null) {

				// Create new MarketDataExchangeRate
				MarketDataExchangeRate newRate = new MarketDataExchangeRate();
				newRate.setDataSource(marketDataSource);
				newRate.setFromCurrency(fromCurrency);
				newRate.setToCurrency(toCurrency);
				newRate.setRateDate(rateDate);
				newRate.setExchangeRate(newExchangeRate.getRate());

				// Save new exchange rate
				getMarketDataRatesService().saveMarketDataExchangeRate(newRate);
			}
			else {

				// If there is already a matching exchange rate, set the actual rate and update the record
				imsExchangeRate.setExchangeRate(newExchangeRate.getRate());
				getMarketDataRatesService().saveMarketDataExchangeRate(imsExchangeRate);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void informUndefinedCurrency(String undefinedCurrency) {
		LogUtils.info(getClass(), "Currency \"" + undefinedCurrency + "\" is undefined in the system.  The exchange rate will not be loaded.");
	}


	@Override
	public String getEventName() {
		return EVENT_NAME;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationMarketDataExchangeRateService getIntegrationMarketDataExchangeRateService() {
		return this.integrationMarketDataExchangeRateService;
	}


	public void setIntegrationMarketDataExchangeRateService(IntegrationMarketDataExchangeRateService integrationMarketDataExchangeRateService) {
		this.integrationMarketDataExchangeRateService = integrationMarketDataExchangeRateService;
	}


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}
}
