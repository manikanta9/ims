package com.clifton.ims.integration.export.content.field;


import com.clifton.trade.Trade;

import java.util.List;


/**
 * The <code>TradeExportFieldStoreFactory</code> handles the generation of export file content for trades.
 *
 * @author jgommels
 */
public interface TradeExportFieldStoreFactory {

	/**
	 * Populates extra data/fields pertaining to trades that are not easily accessible from the
	 * Trade objects themselves.
	 *
	 * @param trades
	 */
	public ExportFieldStore buildFieldStore(List<Trade> trades);
}
