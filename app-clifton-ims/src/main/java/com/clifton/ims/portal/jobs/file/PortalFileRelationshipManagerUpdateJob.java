package com.clifton.ims.portal.jobs.file;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.BusinessCompanyType;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.contact.BusinessContact;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.file.setup.PortalFileCategory;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalFileRelationshipManagerUpdateJob</code> handles creating new {@link com.clifton.portal.entity.PortalEntity} and {@link com.clifton.portal.file.PortalFile} objects where a new
 * relationship manager is found.  And updating all file assignments to relationship managers for Client Relationships and Sister Clients
 * <p>
 * This should run before {@link com.clifton.ims.portal.jobs.entity.PortalEntityContactCopyJob} which relies on the contacts existing in the Portal Entity table
 * to know which contacts need to be updated
 *
 * @author manderson
 */
public class PortalFileRelationshipManagerUpdateJob extends BasePortalFileUpdateJob<BusinessContact> {

	private BusinessClientService businessClientService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private String fileGroupName = PortalFileCategory.CATEGORY_NAME_RELATIONSHIP_MANAGER;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getSourcePortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_CONTACT;
	}


	@Override
	protected Map<Integer, List<PortalEntity>> getSourceToPostPortalEntityListMap(Map<String, Object> context) {
		Map<Integer, List<PortalEntity>> contactPortalEntityMap = new HashMap<>();
		Collection<BusinessCompanyContact> relationshipManagerList = CollectionUtils.combineCollectionOfCollections(getBusinessCompanyRelationshipManagerMap(context).values());
		for (BusinessCompanyContact businessCompanyContact : CollectionUtils.getIterable(relationshipManagerList)) {
			BusinessCompanyType companyType = businessCompanyContact.getReferenceOne().getType();
			// If company type = Client Relationship - Add it
			if (StringUtils.isEqual(BusinessClientRelationship.COMPANY_TYPE_NAME, companyType.getName())) {
				BusinessClientRelationship clientRelationship = getBusinessClientService().getBusinessClientRelationshipByCompany(businessCompanyContact.getReferenceOne().getId());
				if (clientRelationship != null) {
					addPostPortalEntityToMap(PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP, clientRelationship.getId(), businessCompanyContact.getReferenceTwo(), contactPortalEntityMap, context);
				}
			}
			// Else it's a "Client"
			else {
				BusinessClient client = getBusinessClientService().getBusinessClientByCompany(businessCompanyContact.getReferenceOne().getId());
				if (client != null) {
					if (BusinessClientCategoryTypes.SISTER_CLIENT == client.getCategory().getCategoryType() && client.getParent() == null) {
						addPostPortalEntityToMap(PORTAL_SOURCE_SECTION_SISTER_CLIENT, client.getId(), businessCompanyContact.getReferenceTwo(), contactPortalEntityMap, context);
					}
					else if (BusinessClientCategoryTypes.VIRTUAL_CLIENT == client.getCategory().getCategoryType()) {
						if (client.getCategory().getChildClientCategory() != null && BusinessClientCategoryTypes.SISTER_CLIENT == client.getCategory().getChildClientCategory().getCategoryType()) {
							addPostPortalEntityToMap(PORTAL_SOURCE_SECTION_SISTER_CLIENT_GROUP, client.getId(), businessCompanyContact.getReferenceTwo(), contactPortalEntityMap, context);
						}
					}
				}
			}
		}
		// Add In Account Overrides
		Map<Integer, BusinessCompanyContact> accountRelationshipManagerMap = getInvestmentAccountRelationshipManagerOverrideMap(context);
		for (Map.Entry<Integer, BusinessCompanyContact> integerBusinessCompanyContactEntry : accountRelationshipManagerMap.entrySet()) {
			addPostPortalEntityToMap(PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT, integerBusinessCompanyContactEntry.getKey(), integerBusinessCompanyContactEntry.getValue().getReferenceTwo(), contactPortalEntityMap, context);
		}
		return contactPortalEntityMap;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	@Override
	public String getFileGroupName() {
		return this.fileGroupName;
	}


	public void setFileGroupName(String fileGroupName) {
		this.fileGroupName = fileGroupName;
	}
}
