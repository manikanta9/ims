package com.clifton.ims.portal.jobs.entity;

import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.ims.portal.jobs.BasePortalJob;
import com.clifton.portal.security.user.PortalSecurityUserService;

import java.util.Map;


/**
 * The <code>PortalEntityCleanUpJob</code> will attempt to go through all portal entities with a source of IMS that were marked for deletion and actually delete them
 * This is run after all of the update jobs which should disconnect all parent/child links for deleted entities.
 * <p>
 * If the error is used elsewhere will get an error for that entity.  As those cases come up, may want to add support to auto map an item.  For example - a Client is moved to be a Sister Client
 * The jobs will mark the "Client" for deletion and add the "Sister Client"  - IF there are users assigned - do we want to automatically reassign them?  Probably not, and this should be extremely rare
 * and I think would be better to review each case independently until a common theme comes up that makes sense to automatically move.
 *
 * @author manderson
 */
public class PortalEntityCleanUpJob extends BasePortalJob {

	private PortalSecurityUserService portalSecurityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		status = getPortalEntityService().deletePortalEntitiesMarkedForDeletion(PORTAL_SOURCE_SYSTEM_IMS, status);

		boolean disabledAssignments = false;
		try {
			getPortalSecurityUserService().processPortalSecurityUserAssignmentDisabledList();
			disabledAssignments = true;
		}
		catch (Exception e) {
			status.addError("Error processing user assignments for terminated portal entities: " + ExceptionUtils.getOriginalMessage(e));
		}
		status.setMessageWithErrors(status.getMessage() + " " + (disabledAssignments ? "Also successfully processed all user assignments for terminated portal entities." : "Failed to process all user assignments for terminated portal entities.  See errors for more details."), 20);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}
}
