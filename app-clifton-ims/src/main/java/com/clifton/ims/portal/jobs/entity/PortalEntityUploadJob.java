package com.clifton.ims.portal.jobs.entity;

import com.clifton.batch.job.BatchJobStatuses;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.core.util.ftp.FtpProtocols;
import com.clifton.core.util.ftp.FtpResult;
import com.clifton.core.util.ftp.FtpUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.upload.PortalEntityRollupUploadCommand;
import com.clifton.portal.entity.upload.PortalEntityUploadCommand;
import com.clifton.portal.entity.upload.PortalEntityUploadService;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * This batch job is designed to download one or more files from an SFTP service for processing.  Files are filtered by their filename
 * prefix and date stamp.  The processing then uploads the data contained in the file to the appropriate tables in IMS.
 *
 * @author davidi
 */
public class PortalEntityUploadJob implements Task {


	private BusinessCompanyService businessCompanyService;

	private PortalEntityUploadService portalEntityUploadService;

	private PortalEntitySetupService portalEntitySetupService;

	private SecuritySecretService securitySecretService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Only files with the given filename prefix will be processed
	 */
	private String fileNamePrefix;

	/**
	 * Indicates how many days to go back to look for a file to process.  0 days indicates today, 1 day indicates yesterday, etc.
	 */
	private int daysBackToProcessFile;

	/**
	 * A formatting string used to parse the filename suffix date.  If not specified, the default value is "YYYYmmdd".
	 */
	String dateFormat;

	/**
	 * The number of days prior to the current date for which files with the fileNamePrefix should be deleted.  Any files with filenames
	 * containing date suffixes older than the currentDate - daysBackToDeleteHistricalFiles will be deleted.
	 */
	private int daysBackToDeleteHistoricalFiles;

	private String sftpLocation;

	private String sftpUsername;

	private SecuritySecret sftpPasswordSecret;

	private String sftpDirectory;

	/**
	 * Source of the data, i.e. Salesforce
	 */
	private String portalEntitySourceSystemName;


	/**
	 * All Salesforce Entities are Institutional
	 */
	private String portalEntityViewTypeName = PortalEntityViewType.ENTITY_VIEW_TYPE_INSTITUTIONAL;


	/**
	 * If true, the file is a PortalEntityRollup upload file, else a PortalEntity upload file
	 */
	private boolean portalEntityRollup;

	/**
	 * Used for rollup uploads only
	 * If true, then the list in the upload file for each rollup is considered to be all encompassing - so any child not in the list will be removed as a child of the rollup.
	 * Any rollups NOT included in the file for the given source section will NOT be touched.
	 * If false, then any child not in the list won't be updated
	 */
	private boolean deleteChildEntitiesMissingFromFile;


	/**
	 * Does not apply to rollup uploads
	 * Optional contact override for the portal contact team
	 */
	private Integer overridePortalContactTeamBusinessCompanyId;


	/**
	 * Does not apply to rollup uploads
	 * Optional contact override for the portal service team
	 */
	private Integer overridePortalSupportTeamBusinessCompanyId;

	private String statusFileSuffix = "-Status";

	private String statusFileExtension = ".txt";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status batchJobStatus = Status.ofMessage("Starting portal upload");

		File tempDirectory = null;
		StringBuilder uploadResults = new StringBuilder();

		try {
			String tempDirectoryName = FileUtils.combinePath(FileUtils.SYSTEM_TEMPORARY_DIRECTORY, "tempUploadFiles" + "_" + new Date().getTime());
			FileUtils.createDirectory(tempDirectoryName);
			tempDirectory = new File(tempDirectoryName);

			Date currentDate = DateUtils.clearTime(new Date());
			Date fileProcessDate = DateUtils.addDays(currentDate, -1 * getDaysBackToProcessFile());
			String dateFileSuffix = DateUtils.fromDate(fileProcessDate, getDateFormat());
			String filenamePattern = getFileNamePrefix() + dateFileSuffix;

			FtpConfig ftpConfig = new FtpConfig(getSftpLocation(), FtpProtocols.SFTP, getSftpUsername(), getDecryptedPassword());
			List<FtpResult> ftpResults = FtpUtils.get(ftpConfig, getSftpDirectory(), filenamePattern, tempDirectoryName, false, null);

			boolean fileToProcessFound = false;
			for (FtpResult fileResult : CollectionUtils.getIterable(ftpResults)) {
				String filename = fileResult.getFileName();
				// don't process previously added status files (may occur if job is has been run before on the same day)
				if (filename.startsWith(filenamePattern + getStatusFileSuffix())) {
					continue;
				}

				if (!fileResult.isSuccess()) {
					batchJobStatus.addError("Error processing file " + filename + ".  " + ExceptionUtils.getDetailedMessage(fileResult.getError()));
					continue;
				}

				fileToProcessFound = true;

				File fileToUpload = new File(tempDirectoryName, filename);

				Status uploadStatus;
				if (isPortalEntityRollup()) {
					uploadStatus = processPortalEntityRollupFile(filename, fileToUpload);
				}
				else {
					uploadStatus = processPortalEntityFile(filename, fileToUpload);
				}

				createAndSendStatusFile(ftpConfig, tempDirectory.toString(), filenamePattern, uploadStatus, batchJobStatus);
				// Include in the summary the number of inserts / updates / skips, etc. so it's clear that an action was performed.
				// Error details are explicitly copied over as batch job status details
				if (uploadStatus.getErrorCount() > 0) {
					batchJobStatus.addErrors(CollectionUtils.getStream(uploadStatus.getErrorList()).map(StatusDetail::getNote).collect(Collectors.toList()));
				}

				// Also Track Successes for General Message
				for (StatusDetail successDetail : CollectionUtils.getIterable(uploadStatus.getDetailListForCategory(StatusDetail.CATEGORY_SUCCESS))) {
					uploadResults.append(successDetail).append(StringUtils.NEW_LINE);
				}
			}

			if (!fileToProcessFound) {
				batchJobStatus.addError("No input files found for processing.");
			}

			deleteHistoricalFiles(ftpConfig, currentDate, batchJobStatus);
		}
		catch (Exception exc) {
			batchJobStatus.addError("Processing error: " + exc.getMessage());
		}
		finally {
			batchJobStatus.setMessage("Processing completed. " + uploadResults.toString());
			batchJobStatus.addMessage(batchJobStatus.getErrorCount() > 0 ? BatchJobStatuses.COMPLETED_WITH_ERRORS.toString() : BatchJobStatuses.COMPLETED.toString());
			removeFilesAndDirectory(tempDirectory);
		}

		return batchJobStatus;
	}


	private Status processPortalEntityFile(String fileName, File fileToUpload) {
		PortalEntityUploadCommand command = new PortalEntityUploadCommand();

		command.setFile(FileUtils.convertFileToMultipartFile(fileName, fileToUpload));
		command.setDeleteEntitiesMissingFromFile(false);
		command.setSourceSystem(this.getPortalEntitySetupService().getPortalEntitySourceSystemByName(getPortalEntitySourceSystemName()));
		if (!StringUtils.isEmpty(getPortalEntityViewTypeName())) {
			command.setEntityViewType(getPortalEntitySetupService().getPortalEntityViewTypeByName(getPortalEntityViewTypeName()));
		}

		if (getOverridePortalContactTeamBusinessCompanyId() != null) {
			BusinessCompany businessCompany = getBusinessCompanyService().getBusinessCompany(getOverridePortalContactTeamBusinessCompanyId());
			if (businessCompany != null) {
				command.setPortalContactTeamEmail(businessCompany.getEmailAddress());
			}
		}

		if (getOverridePortalSupportTeamBusinessCompanyId() != null) {
			BusinessCompany businessCompany = getBusinessCompanyService().getBusinessCompany(getOverridePortalSupportTeamBusinessCompanyId());
			if (businessCompany != null) {
				command.setPortalSupportTeamEmail(businessCompany.getEmailAddress());
			}
		}
		return getPortalEntityUploadService().uploadPortalEntityUploadFile(command);
	}


	private Status processPortalEntityRollupFile(String fileName, File fileToUpload) {
		PortalEntityRollupUploadCommand command = new PortalEntityRollupUploadCommand();

		command.setFile(FileUtils.convertFileToMultipartFile(fileName, fileToUpload));
		command.setDeleteChildEntitiesMissingFromFile(isDeleteChildEntitiesMissingFromFile());
		command.setSourceSystem(this.getPortalEntitySetupService().getPortalEntitySourceSystemByName(getPortalEntitySourceSystemName()));

		return getPortalEntityUploadService().uploadPortalEntityRollupUploadFile(command);
	}


	/**
	 * Deletes the directory, and all contained files and subdirectories.  Used to remove the temporary directory and
	 * any files therein.
	 */
	private void removeFilesAndDirectory(File directory) {
		if (directory != null && directory.isDirectory()) {
			File[] fileArray = directory.listFiles();
			List<File> containedFileList = fileArray != null ? Arrays.asList(fileArray) : new ArrayList<>();

			for (File file : CollectionUtils.getIterable(containedFileList)) {
				if (file.isDirectory()) {
					removeFilesAndDirectory(file);
				}
				else {
					FileUtils.delete(file);
				}
			}
			FileUtils.delete(directory);
		}
	}


	/**
	 * Removes all files form the FTP server that match the file name prefix that are older than a calculated historical date.
	 * The calculated boundary date is calculated by subtracting the 'daysBackToDeleteHistoricalFile' value from the
	 * current date.
	 */
	private void deleteHistoricalFiles(FtpConfig ftpConfig, Date currentDate, Status batchJobStatus) {
		Date deleteHistoricalFileDateBoundary = DateUtils.addDays(currentDate, -1 * getDaysBackToDeleteHistoricalFiles());
		List<String> filenameList = FtpUtils.list(ftpConfig, getSftpDirectory(), getFileNamePrefix());

		for (String filename : CollectionUtils.getIterable(filenameList)) {
			try {
				if (DateUtils.isDateBefore(getDateFromFilename(filename), deleteHistoricalFileDateBoundary, false)) {
					FtpUtils.delete(ftpConfig, getSftpDirectory(), filename, null);
				}
			}
			catch (RuntimeException exc) {
				batchJobStatus.addError(exc.getMessage());
			}
		}
	}


	/**
	 * Creates a status file that contains the Status message and detail messages resulting from the upload operation.
	 * The file is then sent to the SFTP service containing the source files.
	 */
	private void createAndSendStatusFile(FtpConfig config, String directory, String fileNamePattern, Status status, Status batchJobStatus) {
		if (status == null) {
			batchJobStatus.addError("Status file upload failed: the Status parameter cannot be null");
			return;
		}


		final String filename = fileNamePattern + getStatusFileSuffix() + getStatusFileExtension();
		String filePath = directory + File.separator + filename;

		try {
			// Write file content
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(filePath), StandardCharsets.UTF_8);
			BufferedWriter bufferedWriter = new BufferedWriter(writer);

			bufferedWriter.write(status.getMessage());
			bufferedWriter.newLine();
			for (StatusDetail detail : status.getDetailList()) {
				String detailEntry = detail.getCategory() + ": " + detail.getNote();
				bufferedWriter.write(detailEntry);
				bufferedWriter.newLine();
			}

			bufferedWriter.flush();
			bufferedWriter.close();

			// Send file up to source
			FileWrapper fileWrapper = new FileWrapper(new File(filePath), filename, true);
			FtpUtils.put(config, getSftpDirectory(), fileWrapper);
		}
		catch (IOException exc) {
			batchJobStatus.addError("Status file upload failed: " + exc.getMessage());
		}
	}


	/**
	 * Gets the formatted date out of the filename and converts it to an actual date.
	 */
	private Date getDateFromFilename(String fileName) {
		ValidationUtils.assertNotNull(fileName, "The filename parameter cannot be null");
		String subStringFileName = fileName.replace(getFileNamePrefix(), "");
		subStringFileName = subStringFileName.replace(getStatusFileSuffix(), "");
		String[] components = subStringFileName.split("[.]", 1);
		String dateString = components[0];

		return DateUtils.toDate(dateString, getDateFormat());
	}


	protected String getDecryptedPassword() {
		return getSecuritySecretService().decryptSecuritySecret(getSecuritySecretService().getSecuritySecretPopulated(getSftpPasswordSecret())).getSecretString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isPortalEntityRollup() {
		return this.portalEntityRollup;
	}


	public void setPortalEntityRollup(boolean portalEntityRollup) {
		this.portalEntityRollup = portalEntityRollup;
	}


	public String getFileNamePrefix() {
		return this.fileNamePrefix;
	}


	public void setFileNamePrefix(String fileNamePrefix) {
		this.fileNamePrefix = fileNamePrefix;
	}


	public int getDaysBackToProcessFile() {
		return this.daysBackToProcessFile;
	}


	public void setDaysBackToProcessFile(int daysBackToProcessFile) {
		this.daysBackToProcessFile = daysBackToProcessFile;
	}


	public String getDateFormat() {
		return this.dateFormat;
	}


	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}


	public int getDaysBackToDeleteHistoricalFiles() {
		return this.daysBackToDeleteHistoricalFiles;
	}


	public void setDaysBackToDeleteHistoricalFiles(int daysBackToDeleteHistoricalFiles) {
		this.daysBackToDeleteHistoricalFiles = daysBackToDeleteHistoricalFiles;
	}


	public String getSftpLocation() {
		return this.sftpLocation;
	}


	public void setSftpLocation(String sftpLocation) {
		this.sftpLocation = sftpLocation;
	}


	public String getSftpUsername() {
		return this.sftpUsername;
	}


	public void setSftpUsername(String sftpUsername) {
		this.sftpUsername = sftpUsername;
	}


	public SecuritySecret getSftpPasswordSecret() {
		return this.sftpPasswordSecret;
	}


	public void setSftpPasswordSecret(SecuritySecret sftpPasswordSecret) {
		this.sftpPasswordSecret = sftpPasswordSecret;
	}


	public String getSftpDirectory() {
		return this.sftpDirectory != null ? this.sftpDirectory : "/";
	}


	public void setSftpDirectory(String sftpDirectory) {
		this.sftpDirectory = sftpDirectory;
	}


	public String getPortalEntitySourceSystemName() {
		return this.portalEntitySourceSystemName;
	}


	public void setPortalEntitySourceSystemName(String portalEntitySourceSystemName) {
		this.portalEntitySourceSystemName = portalEntitySourceSystemName;
	}


	public boolean isDeleteChildEntitiesMissingFromFile() {
		return this.deleteChildEntitiesMissingFromFile;
	}


	public void setDeleteChildEntitiesMissingFromFile(boolean deleteChildEntitiesMissingFromFile) {
		this.deleteChildEntitiesMissingFromFile = deleteChildEntitiesMissingFromFile;
	}


	public Integer getOverridePortalContactTeamBusinessCompanyId() {
		return this.overridePortalContactTeamBusinessCompanyId;
	}


	public void setOverridePortalContactTeamBusinessCompanyId(Integer overridePortalContactTeamBusinessCompanyId) {
		this.overridePortalContactTeamBusinessCompanyId = overridePortalContactTeamBusinessCompanyId;
	}


	public Integer getOverridePortalSupportTeamBusinessCompanyId() {
		return this.overridePortalSupportTeamBusinessCompanyId;
	}


	public void setOverridePortalSupportTeamBusinessCompanyId(Integer overridePortalSupportTeamBusinessCompanyId) {
		this.overridePortalSupportTeamBusinessCompanyId = overridePortalSupportTeamBusinessCompanyId;
	}


	public String getStatusFileSuffix() {
		return this.statusFileSuffix;
	}


	public void setStatusFileSuffix(String statusFileSuffix) {
		this.statusFileSuffix = statusFileSuffix;
	}


	public String getStatusFileExtension() {
		return this.statusFileExtension;
	}


	public void setStatusFileExtension(String statusFileExtension) {
		this.statusFileExtension = statusFileExtension;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public PortalEntityUploadService getPortalEntityUploadService() {
		return this.portalEntityUploadService;
	}


	public void setPortalEntityUploadService(PortalEntityUploadService portalEntityUploadService) {
		this.portalEntityUploadService = portalEntityUploadService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public String getPortalEntityViewTypeName() {
		return this.portalEntityViewTypeName;
	}


	public void setPortalEntityViewTypeName(String portalEntityViewTypeName) {
		this.portalEntityViewTypeName = portalEntityViewTypeName;
	}
}
