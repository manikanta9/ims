package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.BusinessClientCompanyRelationshipExtended;
import com.clifton.business.client.BusinessClientService;
import com.clifton.business.client.search.BusinessClientCompanyRelationshipExtendedSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyRelationship;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.ims.portal.jobs.BasePortalJob;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityFieldValue;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntityViewType;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BasePortalEntityCopyJob</code> is an abstract class that is extended by jobs that populate the Portal with
 * data from IMS for a specific source section.
 * <p>
 * Deletes are not really deleted from the Portal, but the entity is marked as IsDeleted = true
 * Then, later, a clean up job can handle deleting unused "deleted" entities.  We can't just delete because entities can be hierarchical
 * or files could be posted to them.
 *
 * @author manderson
 */
public abstract class BasePortalEntityCopyJob<T extends IdentityObject> extends BasePortalJob {

	private static final String CONTEXT_KEY_ORGANIZATION_GROUP_PARENT_CHILD_MAP = "CONTEXT_KEY_ORGANIZATION_GROUP_PARENT_CHILD_MAP";
	private static final String CONTEXT_KEY_COMPANY_EMAIL_ADDRESS_MAP = "CONTEXT_KEY_COMPANY_EMAIL_ADDRESS_MAP";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private BusinessClientService businessClientService;
	private BusinessCompanyService businessCompanyService;

	/**
	 * Default view type for now is Institutional.  Will set this where blank on any securable entity
	 */
	private String defaultPortalEntityViewTypeName = PortalEntityViewType.ENTITY_VIEW_TYPE_INSTITUTIONAL;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		// Portal Entity Objects mapped to IMS Source FK Field ID
		Map<Integer, PortalEntity> portalEntityMap = getPortalEntitySourceMap(getPortalEntitySourceSectionName(), context);
		// IMS Objects mapped to IMS ID
		Map<Integer, T> sourceEntityMap = BeanUtils.getBeanMap(getSourceEntityList(portalEntityMap, context), BeanUtils::getIdentityAsInteger);
		// Updates to PortalEntity Map from IMS (Inserts, Updates, and Marked for Deletion)
		updatePortalEntityMap(sourceEntityMap, portalEntityMap, context);


		List<PortalEntity> saveList = savePortalEntities(portalEntityMap, status, context);

		// If the Portal Entity is a Rollup of Other Entities, populate the Map and Save
		Map<PortalEntity, List<PortalEntityRollup>> rollupListMap = populatePortalEntityRollupListMap(saveList, context);
		savePortalEntityRollups(rollupListMap, status);

		status.setMessageWithErrors(status.getMessage(), 5);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected abstract String getPortalEntitySourceSectionName();


	protected abstract List<T> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context);


	protected abstract boolean isParentPortalEntityRequired();


	protected abstract PortalEntity getParentPortalEntityForSource(T sourceEntity, PortalEntity portalEntity, Map<String, Object> context);


	protected abstract boolean isSourceEntityAvailableForInsert(T sourceEntity);


	protected abstract void populatePortalEntityPropertiesFromSource(T sourceEntity, PortalEntity portalEntity, Map<String, Object> context);


	/**
	 * Needs to be overridden when rollups need to be populated
	 */
	protected Map<PortalEntity, List<PortalEntityRollup>> populatePortalEntityRollupListMap(List<PortalEntity> portalEntityList, Map<String, Object> context) {
		return null;
	}


	/**
	 * Needs to be overridden when field values are supported
	 */
	protected String getFieldValueForSourceEntity(T sourceEntity, String fieldTypeName, Map<String, Object> context) {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                     Helper Methods                   /////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void updatePortalEntityMap(Map<Integer, T> sourceEntityMap, Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		List<PortalEntityFieldType> fieldTypeList = getPortalEntityFieldTypeListForSourceSection(getPortalEntitySourceSectionName(), context);

		for (Map.Entry<Integer, T> integerTEntry : sourceEntityMap.entrySet()) {
			T sourceEntity = integerTEntry.getValue();
			PortalEntity portalEntity = portalEntityMap.get(integerTEntry.getKey());
			PortalEntity parentPortalEntity = getParentPortalEntityForSource(sourceEntity, portalEntity, context);

			if (portalEntity == null) {
				if (isSourceEntityAvailableForInsert(sourceEntity) && (!isParentPortalEntityRequired() || parentPortalEntity != null)) {
					portalEntity = new PortalEntity();
					portalEntity.setEntitySourceSection(getPortalEntitySourceSectionByName(getPortalEntitySourceSectionName(), context));
					portalEntity.setSourceFkFieldId(integerTEntry.getKey());
				}
				else {
					continue;
				}
			}
			portalEntity.setDeleted(false); // Can't think of how it could be deleted and then not

			// What if parent is null on an update - i.e. it was moved ???
			// Mark as deleted? How would this happen
			portalEntity.setParentPortalEntity(parentPortalEntity);
			portalEntity.setEntityViewType(getPortalEntityViewTypeForSource(sourceEntity, portalEntity, context));

			populatePortalEntityPropertiesFromSource(sourceEntity, portalEntity, context);
			if (!CollectionUtils.isEmpty(fieldTypeList) && !portalEntity.isDeleted()) {
				if (!portalEntity.isNewBean()) {
					portalEntity.setFieldValueList(getPortalEntityService().getPortalEntityFieldValueListByEntity(portalEntity.getId()));
				}
				populatePortalEntityFieldValueList(sourceEntity, portalEntity, fieldTypeList, context);
			}
			else {
				portalEntity.setFieldValueList(null);
			}
			portalEntityMap.put(integerTEntry.getKey(), portalEntity);
		}

		for (Map.Entry<Integer, PortalEntity> integerPortalEntityEntry : portalEntityMap.entrySet()) {
			if (!sourceEntityMap.containsKey(integerPortalEntityEntry.getKey())) {
				PortalEntity portalEntity = integerPortalEntityEntry.getValue();
				portalEntity.setDeleted(true);
			}
		}
	}


	/**
	 * View Type applies to securable entities only.  In all cases it copies the view type from it's parent (if none defaults to the default view type - currently institutional)
	 * Note - this method is ONLY overridden on the ClientRelationshipCopyJob to properly set retail where needed - which in IMS is where that value is driven from
	 */
	protected PortalEntityViewType getPortalEntityViewTypeForSource(T sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		if (portalEntity != null && portalEntity.getEntitySourceSection().getEntityType().isSecurableEntity()) {
			if (portalEntity.getParentPortalEntity() != null) {
				return portalEntity.getParentPortalEntity().getEntityViewType();
			}
		}
		return null;
	}


	protected void populatePortalEntityFieldValueList(T sourceEntity, PortalEntity portalEntity, List<PortalEntityFieldType> fieldTypeList, Map<String, Object> context) {
		Map<Short, PortalEntityFieldValue> existingValueMap = BeanUtils.getBeanMap(portalEntity.getFieldValueList(), portalEntityFieldValue -> portalEntityFieldValue.getPortalEntityFieldType().getId());
		List<PortalEntityFieldValue> newValueList = new ArrayList<>();

		for (PortalEntityFieldType fieldType : CollectionUtils.getIterable(fieldTypeList)) {
			PortalEntityFieldValue value = existingValueMap.get(fieldType.getId());
			String valueString = getFieldValueForSourceEntity(sourceEntity, fieldType.getName(), context);
			if (!StringUtils.isEmpty(valueString)) {
				if (value == null) {
					value = new PortalEntityFieldValue();
					value.setPortalEntityFieldType(fieldType);
					value.setPortalEntity(portalEntity);
				}
				value.setValue(valueString);
				newValueList.add(value);
			}
		}
		portalEntity.setFieldValueList(newValueList);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                  Portal Save Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Saves each PortalEntity from the Portal Entity map - tries each individually and updates status
	 * Returns the list of saved PortalEntities
	 */
	protected List<PortalEntity> savePortalEntities(Map<Integer, PortalEntity> portalEntityMap, Status status, Map<String, Object> context) {
		PortalEntityViewType defaultViewType = (StringUtils.isEmpty(getDefaultPortalEntityViewTypeName()) ? null : getPortalEntitySetupService().getPortalEntityViewTypeByName(getDefaultPortalEntityViewTypeName()));

		List<PortalEntity> savedList = new ArrayList<>();

		int skipCount = 0;
		int failCount = 0;
		int successCount = 0;
		Date now = new Date();
		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityMap.values())) {
			try {
				// FOR NOW IF NOT SET, SET ALL SECURABLE ENTITIES TO INSTITUTIONAL VIEW TYPE
				if (defaultViewType != null && portalEntity.getEntityViewType() == null && portalEntity.getEntitySourceSection().getEntityType().isSecurableEntity()) {
					portalEntity.setEntityViewType(defaultViewType);
				}
				portalEntity = getPortalEntityService().savePortalEntity(portalEntity);
				if (DateUtils.isDateAfterOrEqual(portalEntity.getUpdateDate(), now)) {
					successCount++;
				}
				else {
					skipCount++;
					status.addSkipped(portalEntity.getEntityLabelWithSourceSection());
				}
				savedList.add(portalEntity);
			}
			catch (Throwable e) {
				failCount++;
				String message = "Error saving Portal Entity: " + portalEntity.getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				status.addError(message);
			}
		}
		status.setMessage(getPortalEntitySourceSectionName() + ": Successful: " + successCount + ", Skipped: " + skipCount + " Failed: " + failCount);
		clearPortalEntitySourceMap(getPortalEntitySourceSectionName(), context);
		return savedList;
	}


	protected void savePortalEntityRollups(Map<PortalEntity, List<PortalEntityRollup>> rollupListMap, Status status) {
		int rollupSuccessCount = 0;
		int rollupFailCount = 0;
		if (!CollectionUtils.isEmpty(rollupListMap)) {
			for (Map.Entry<PortalEntity, List<PortalEntityRollup>> portalEntityListEntry : rollupListMap.entrySet()) {
				try {
					getPortalEntityService().savePortalEntityRollupListForParent(portalEntityListEntry.getKey(), CollectionUtils.asNonNullList(portalEntityListEntry.getValue()));
					rollupSuccessCount++;
				}
				catch (Exception e) {
					rollupFailCount++;
					String message = "Error saving rollup entries for Portal Entity: " + (portalEntityListEntry.getKey()).getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), message, e);
					status.addError(message);
				}
			}
		}

		if (rollupFailCount != 0 || rollupSuccessCount != 0) {
			status.appendToMessage("Rollups successfully updated for " + rollupSuccessCount + " entities. Failed: " + rollupFailCount);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Organization Group Retrievals              ///////////
	////////////////////////////////////////////////////////////////////////////////


	protected List<BusinessCompany> getOrganizationGroupCompanyList(Map<String, Object> context) {
		return CollectionUtils.getDistinct(getIdentityObjectToOrganizationGroupMap(context).values(), BusinessCompany::getId);
	}


	@SuppressWarnings("unchecked")
	protected Map<IdentityObject, BusinessCompany> getIdentityObjectToOrganizationGroupMap(Map<String, Object> context) {
		// Note: We need to use Identity Object, because the Organization Group could be related at the Sister Client level (i.e. no Client Relationship)
		return (Map<IdentityObject, BusinessCompany>) CollectionUtils.getValue(context, CONTEXT_KEY_ORGANIZATION_GROUP_PARENT_CHILD_MAP, () -> {
			BusinessClientCompanyRelationshipExtendedSearchForm searchForm = new BusinessClientCompanyRelationshipExtendedSearchForm();
			searchForm.setActive(true);
			searchForm.setNameEquals(BusinessCompanyRelationship.RELATIONSHIP_NAME_PORTAL_RELATIONSHIP);
			List<BusinessClientCompanyRelationshipExtended> relationshipExtendedList = getBusinessClientService().getBusinessClientCompanyRelationshipExtendedList(searchForm);
			Map<IdentityObject, BusinessCompany> map = new HashMap<>();
			for (BusinessClientCompanyRelationshipExtended companyRelationshipExtended : CollectionUtils.getIterable(relationshipExtendedList)) {
				// We assume one per relationship - so if we have one skip it.  There could be some at the client level but for a specific branch
				// And we always use the top level of the related company - we don't care about branches in the portal
				if (companyRelationshipExtended.getClientRelationship() != null) {
					if (!map.containsKey(companyRelationshipExtended.getClientRelationship())) {
						map.put(companyRelationshipExtended.getClientRelationship(), companyRelationshipExtended.getRelatedCompany().getRootParent());
					}
				}
				else if (companyRelationshipExtended.getClient() != null) {
					if (!map.containsKey(companyRelationshipExtended.getClient())) {
						map.put(companyRelationshipExtended.getClient(), companyRelationshipExtended.getRelatedCompany().getRootParent());
					}
				}
			}
			return map;
		}, HashMap::new);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////      Relationship Manager Portal Team Email Helper Methods     ////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to find the company (if applicable) associated with the Portal Support Team Override, and Portal Contact Team Override defined on the custom fields
	 * for a Relationship Manager (BusinessCompanyContact) table.  Finds the companyId, and then retrieves the email from that company.
	 */
	@SuppressWarnings("unchecked")
	protected String getPortalTeamOverrideEmailAddressForBusinessCompanyContact(BusinessCompanyContact companyContact, String customFieldName, Map<String, Object> context) {
		if (companyContact != null) {
			Integer companyId = (Integer) getSystemColumnValueService().getSystemColumnCustomValue(companyContact.getId(), BusinessCompanyContact.COMPANY_CONTACT_CUSTOM_FIELDS_GROUP_NAME, customFieldName);
			if (companyId != null) {
				// Use the Context as there won't be many overrides, but when they are set, there are only 2 (currently) that are used for all cases
				Map<Integer, String> companyEmailMap = (Map<Integer, String>) CollectionUtils.getValue(context, CONTEXT_KEY_COMPANY_EMAIL_ADDRESS_MAP, HashMap::new);
				return CollectionUtils.getValue(companyEmailMap, companyId, () -> {
					BusinessCompany businessCompany = getBusinessCompanyService().getBusinessCompany(companyId);
					if (businessCompany != null) {
						return businessCompany.getEmailAddress();
					}
					return null;
				}, null);
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessClientService getBusinessClientService() {
		return this.businessClientService;
	}


	public void setBusinessClientService(BusinessClientService businessClientService) {
		this.businessClientService = businessClientService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public String getDefaultPortalEntityViewTypeName() {
		return this.defaultPortalEntityViewTypeName;
	}


	public void setDefaultPortalEntityViewTypeName(String defaultPortalEntityViewTypeName) {
		this.defaultPortalEntityViewTypeName = defaultPortalEntityViewTypeName;
	}
}
