package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.company.BusinessCompany;
import com.clifton.portal.entity.PortalEntity;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityOrganizationGroupCopyJob</code> is used to maintain Organization Group Companies in the Portal
 * Most commonly used for OCIO relationships, but specifically looks for Portal Relationship as the company relationship name
 *
 * @author manderson
 */
public class PortalEntityOrganizationGroupCopyJob extends BasePortalEntityCopyJob<BusinessCompany> {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessCompany sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		return null;
	}


	@Override
	protected List<BusinessCompany> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		return getOrganizationGroupCompanyList(context);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessCompany sourceEntity) {
		return true;
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessCompany sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getName());
		portalEntity.setDescription(sourceEntity.getDescription());
		portalEntity.setStartDate(null);
		// Note: End Date is Set in {@link PortalEntityObserver} based on the last end date of all of it's children (if any are blank, this is blank)
	}
}
