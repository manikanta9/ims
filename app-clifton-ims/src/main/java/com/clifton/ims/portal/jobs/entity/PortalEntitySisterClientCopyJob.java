package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.BusinessCompany;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.core.util.MathUtils;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntitySisterClientCopyJob</code> is used to maintain Sister Clients in the Portal
 * <p>
 * Note: Applies to BusinessClient table where ClientCategoryType = "SISTER_CLIENT" only
 *
 * @author manderson
 */
public class PortalEntitySisterClientCopyJob extends PortalEntityClientCopyJob {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_SISTER_CLIENT;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		// If Sister Client Belongs to a Sister Client Group - Use that
		if (sourceEntity.getParent() != null) {
			if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
				// Verify the parent is the same, if so return it - otherwise look it up
				if (PORTAL_SOURCE_SECTION_SISTER_CLIENT_GROUP.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), sourceEntity.getParent().getId())) {
					return portalEntity.getParentPortalEntity();
				}
			}
			return getPortalEntityBySource(PORTAL_SOURCE_SECTION_SISTER_CLIENT_GROUP, sourceEntity.getParent().getId(), context);
		}
		// Otherwise if there is an organization group - use that.
		BusinessCompany outsourcedCIO = getIdentityObjectToOrganizationGroupMap(context).get(sourceEntity);
		if (outsourcedCIO != null) {
			if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
				// Verify the parent is the same, if so return it - otherwise look it up
				if (PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), outsourcedCIO.getId())) {
					return portalEntity.getParentPortalEntity();
				}
			}
			return getPortalEntityBySource(PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP, outsourcedCIO.getId(), context);
		}
		// Otherwise no parent
		return null;
	}


	@Override
	protected List<BusinessClient> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
		searchForm.setCategoryType(BusinessClientCategoryTypes.SISTER_CLIENT);
		return getBusinessClientService().getBusinessClientList(searchForm);
	}
}
