package com.clifton.ims.portal.file;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import org.springframework.stereotype.Component;


/**
 * The <code>PortalFileAwarePostedEventListener</code> listens for post or delete events from in order to update
 * the PortalFileAware bean properties
 *
 * @author manderson
 */
@Component
public class PortalFileAwarePostedEventListener<T extends IdentityObject> extends BaseEventListener<PortalFileAwarePostedEvent<T>> {

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return PortalFileAwarePostedEvent.EVENT_NAME;
	}


	@Override
	public void onEvent(PortalFileAwarePostedEvent<T> event) {
		ReportPostingFileConfiguration config = event.getTarget();

		UpdatableDAO<T> dao = null;
		T saveObject = null;
		try {
			if (event.getSourceEntity() != null) {
				if (event.getSourceEntity() instanceof PortalFileAware) {
					saveObject = event.getSourceEntity();
					dao = (UpdatableDAO<T>) getDaoLocator().locate(saveObject);
				}
			}
			else if (!StringUtils.isEmpty(config.getSourceEntityTableName()) && config.getSourceEntityFkFieldId() != null) {
				dao = (UpdatableDAO<T>) getDaoLocator().<T>locate(config.getSourceEntityTableName());
				if (PortalFileAware.class.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
					saveObject = dao.findByPrimaryKey(config.getSourceEntityFkFieldId());
				}
			}
			executeEvent(event, saveObject, dao);
		}
		catch (Exception e) {
			// Log the error, but don't throw it.  Batch job can reset this and don't want issue updating this field to cause the post not to happen
			LogUtils.errorOrInfo(getClass(), "Error updating Posted to Portal field for " + config.getSourceEntityTableName() + " id " + config.getSourceEntityFkFieldId(), e);
		}
	}


	private void executeEvent(PortalFileAwarePostedEvent<T> event, T bean, UpdatableDAO<T> dao) {
		PortalFileAware awareBean = (PortalFileAware) bean;
		if (dao != null && bean != null && awareBean.isPostedToPortal() != event.isPosted()) {
			awareBean.setPostedToPortal(event.isPosted());
			event.setResult(DaoUtils.executeWithAllObserversDisabled(() -> dao.save(bean)));
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
