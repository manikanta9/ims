package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityRollup;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.core.util.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityClientGroupCopyJob</code> is used to maintain client groups in the portal
 * Also assigns child clients in the rollup table for the group
 * <p>
 * Note: Applies to BusinessClient table where ClientCategoryType = "VIRTUAL_CLIENT" and IsChildrenAllowed = true and excludes where the child category type - Sister Client
 * <p>
 * Applies to Client Groups and and Commingled Vehicle Groups
 * Sister clients are handled different (See {@link PortalEntitySisterClientGroupCopyJob}
 *
 * @author manderson
 */
public class PortalEntityClientGroupCopyJob extends BasePortalEntityCopyJob<BusinessClient> {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_CLIENT_GROUP;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		if (sourceEntity.getClientRelationship() == null) {
			return null;
		}
		if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
			// Verify the parent is the same, if so return it - otherwise look it up
			if (PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), sourceEntity.getClientRelationship().getId())) {
				return portalEntity.getParentPortalEntity();
			}
		}
		return getPortalEntityBySource(PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP, sourceEntity.getClientRelationship().getId(), context);
	}


	@Override
	protected List<BusinessClient> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
		searchForm.setCategoryType(BusinessClientCategoryTypes.VIRTUAL_CLIENT);
		searchForm.setCategoryChildrenAllowed(true);
		searchForm.setExcludeCategoryChildCategoryType(BusinessClientCategoryTypes.SISTER_CLIENT);
		return getBusinessClientService().getBusinessClientList(searchForm);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessClient sourceEntity) {
		return sourceEntity.getTerminateDate() == null;
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getLegalLabel());
		portalEntity.setDescription(sourceEntity.getLegalLabel());
		portalEntity.setStartDate(sourceEntity.getInceptionDate());
		portalEntity.setEndDate(sourceEntity.getTerminateDate());
	}


	@Override
	protected String getFieldValueForSourceEntity(BusinessClient sourceEntity, String fieldTypeName, Map<String, Object> context) {
		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) || StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_CONTACT_TEAM_EMAIL, fieldTypeName)) {
			List<BusinessCompanyContact> companyContactList = getRelationshipManagerListForClient(sourceEntity, context);
			// If only one - set the overrides - otherwise we'll set them on the account (which is where if there is more than 1 they would be used)
			if (CollectionUtils.getSize(companyContactList) == 1) {
				return getPortalTeamOverrideEmailAddressForBusinessCompanyContact(companyContactList.get(0), StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) ? BusinessCompanyContact.CUSTOM_FIELD_PORTAL_SUPPORT_TEAM_OVERRIDE : BusinessCompanyContact.CUSTOM_FIELD_PORTAL_CONTACT_TEAM_OVERRIDE, context);
			}
		}
		return null;
	}


	@Override
	protected Map<PortalEntity, List<PortalEntityRollup>> populatePortalEntityRollupListMap(List<PortalEntity> portalEntityList, Map<String, Object> context) {
		Map<PortalEntity, List<PortalEntityRollup>> portalEntityRollupListMap = new HashMap<>();
		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			// If Source was deleted - then clear the rollup list
			if (portalEntity.isDeleted()) {
				portalEntityRollupListMap.put(portalEntity, null);
			}
			else {
				BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
				searchForm.setParentId(portalEntity.getSourceFkFieldId());
				List<BusinessClient> childClients = getBusinessClientService().getBusinessClientList(searchForm);

				Map<Integer, PortalEntityRollup> rollupMap = BeanUtils.getBeanMap(getPortalEntityService().getPortalEntityRollupListByParent(portalEntity.getId()), portalEntityRollup -> portalEntityRollup.getReferenceTwo().getSourceFkFieldId());
				List<PortalEntityRollup> newRollupList = new ArrayList<>();

				for (BusinessClient childClient : CollectionUtils.getIterable(childClients)) {
					PortalEntityRollup rollup = rollupMap.get(childClient.getId());
					if (rollup == null) {
						String sourceSection = PORTAL_SOURCE_SECTION_CLIENT;
						if (childClient.getCategory().getCategoryType() == BusinessClientCategoryTypes.SISTER_CLIENT) {
							sourceSection = PORTAL_SOURCE_SECTION_SISTER_CLIENT;
						}
						PortalEntity childEntity = getPortalEntityBySource(sourceSection, childClient.getId(), context);
						if (childEntity != null) {
							rollup = new PortalEntityRollup();
							rollup.setReferenceOne(portalEntity);
							rollup.setReferenceTwo(childEntity);
						}
					}
					if (rollup != null) {
						// Don't use Start Date for clients in the group.  There are times (Client Authorization form)
						// where the group level document isn't updated but still applies to the new client.  If it doesn't apply
						// users can manually exclude the file from that specific client
						rollup.setStartDate(null);
						// Use End Date because the Client has been terminated
						rollup.setEndDate(rollup.getReferenceTwo().getEndDate());
						newRollupList.add(rollup);
					}
				}
				portalEntityRollupListMap.put(portalEntity, newRollupList);
			}
		}
		return portalEntityRollupListMap;
	}
}
