package com.clifton.ims.portal.jobs.security;

import com.clifton.core.beans.BeanUtils;
import com.clifton.ims.portal.jobs.BasePortalJob;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserService;
import com.clifton.portal.security.user.search.PortalSecurityUserSearchForm;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;

import java.util.Map;


/**
 * The <code>BasePortalSecurityUserJob</code> is a base job extended by Portal Security jobs to handle common retrievals of security related objects
 *
 * @author manderson
 */
public abstract class BasePortalSecurityUserJob extends BasePortalJob {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private PortalSecurityUserService portalSecurityUserService;

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////////
	////////////                      Helper Methods                    ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a map of the existing PortalSecurityUser objects whose source system is IMS and the key is the SourceFKFieldID
	 */
	protected Map<Short, PortalSecurityUser> getPortalSecurityUserMap(boolean populateResourcePermissionMap, Map<String, Object> context) {
		PortalSecurityUserSearchForm searchForm = new PortalSecurityUserSearchForm();
		searchForm.setPortalEntitySourceSystemId(getPortalEntitySourceSystem(context).getId());
		return BeanUtils.getBeanMap(getPortalSecurityUserService().getPortalSecurityUserList(searchForm, populateResourcePermissionMap), PortalSecurityUser::getSourceFKFieldId);
	}


	/**
	 * Returns a map of the IMS SecurityUser objects where key is the id
	 */
	protected Map<Short, SecurityUser> getSecurityUserMap() {
		return BeanUtils.getBeanMap(getSecurityUserService().getSecurityUserList(), SecurityUser::getId);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalSecurityUserService getPortalSecurityUserService() {
		return this.portalSecurityUserService;
	}


	public void setPortalSecurityUserService(PortalSecurityUserService portalSecurityUserService) {
		this.portalSecurityUserService = portalSecurityUserService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
