package com.clifton.ims.portal.file;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.portal.file.upload.PortalFileUploadService;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PortalFileReportPostingController</code> handles posting/unposting files from the portal
 */
@Component("reportPostingController")
public class PortalFileReportPostingController<T extends IdentityObject> implements ReportPostingController<T> {

	private static final String IMS_PORTAL_ENTITY_SOURCE_SYSTEM_NAME = "IMS";

	private EventHandler eventHandler;

	private PortalEntityService portalEntityService;
	private PortalEntitySetupService portalEntitySetupService;
	private PortalFileService portalFileService;
	private PortalFileSetupService portalFileSetupService;
	private PortalFileUploadService portalFileUploadService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@SecureMethod(securityResource = "ReportPosting", permissions = SecurityPermission.PERMISSION_WRITE)
	public T postClientFile(ReportPostingFileConfiguration config, T sourceEntity) {
		try {
			PortalFile portalFile = postClientFileImpl(config);
			if (portalFile != null && portalFile.isApproved()) {
				return updatePortalFileAwarePostedProperty(config, sourceEntity, true);
			}
		}
		finally {
			if ((config.getFileWrapper() != null) && config.getFileWrapper().isTempFile() && FileUtils.fileExists(config.getFileWrapper().getFile())) {
				FileUtils.delete(config.getFileWrapper().getFile());
			}
		}
		return sourceEntity;
	}


	protected PortalFile postClientFileImpl(ReportPostingFileConfiguration config) {
		PortalFile portalFile = new PortalFile();
		if (config.getPortalFileCategoryId() != null) {
			portalFile.setFileCategory(getPortalFileSetupService().getPortalFileCategory(config.getPortalFileCategoryId()));
		}
		else {
			ValidationUtils.assertNotNull(config.getPortalFileCategoryName(), "Portal File Category Name is Required");
			portalFile.setFileCategory(getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, config.getPortalFileCategoryName()));
		}
		portalFile.setReportDate(config.getReportDate());
		portalFile.setPublishDate(config.getPublishDate());
		if (!StringUtils.isEmpty(config.getPortalFileFrequency())) {
			portalFile.setFileFrequency(PortalFileFrequencies.valueOf(config.getPortalFileFrequency()));
		}
		// When posting files from a source system, if the category doesn't require additional approval and it's not a custom file, then we automatically
		// mark it as approved.  Otherwise dual approval process must take place
		if (!portalFile.getFileCategory().isApprovalRequired() && !config.isCustomFile()) {
			portalFile.setApproved(true);
		}

		if (config.getPostPortalEntityId() != null) {
			portalFile.setPostPortalEntity(getPortalEntityService().getPortalEntity(config.getPostPortalEntityId()));
		}
		else if (!StringUtils.isEmpty(config.getPostEntitySourceTableName()) && config.getPostEntitySourceFkFieldId() != null) {
			PortalEntity postEntity = getPortalEntityService().getPortalEntityBySourceTable(IMS_PORTAL_ENTITY_SOURCE_SYSTEM_NAME, config.getPostEntitySourceTableName(), config.getPostEntitySourceFkFieldId(), false);
			if (postEntity == null) {
				throw new ValidationException("Cannot find entity in Portal system for table name [" + config.getPostEntitySourceTableName() + "] and FK Field ID " + config.getPostEntitySourceFkFieldId());
			}
			portalFile.setPostPortalEntity(postEntity);
		}
		else {
			throw new ValidationException("Unable to determine what entity file is posted to.");
		}
		if (!StringUtils.isEmpty(config.getSourceEntitySectionName()) && config.getSourceEntityFkFieldId() != null) {
			PortalEntitySourceSection sourceSection = getPortalEntitySetupService().getPortalEntitySourceSectionByName(IMS_PORTAL_ENTITY_SOURCE_SYSTEM_NAME, config.getSourceEntitySectionName());
			if (sourceSection == null) {
				throw new ValidationException("Source Section [" + config.getSourceEntitySectionName() + "] is not set up in the Portal for " + IMS_PORTAL_ENTITY_SOURCE_SYSTEM_NAME);
			}
			// Used to Update PortalFileAware properties
			config.setSourceEntityTableName(sourceSection.getSourceSystemTableName());
			portalFile.setEntitySourceSection(sourceSection);
			portalFile.setSourceFkFieldId(config.getSourceEntityFkFieldId());
		}
		if (!StringUtils.isEmpty(config.getDisplayName())) {
			portalFile.setDisplayName(config.getDisplayName());
		}
		portalFile.setFile(FileUtils.convertFileToMultipartFile(config.getFileWrapper().getFileName(), FileContainerFactory.getFileContainer(config.getFileWrapper().getFile())));
		return getPortalFileUploadService().uploadPortalFile(portalFile, config.getFileDuplicateAction() == null ? FileDuplicateActions.ERROR : config.getFileDuplicateAction());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public T unpostClientFile(ReportPostingFileConfiguration config, T sourceEntity, boolean delete) {
		if (!StringUtils.isEmpty(config.getSourceEntityTableName()) && config.getSourceEntityFkFieldId() != null) {
			List<PortalFile> fileList = getPortalFileService().getPortalFileListForSourceEntity(IMS_PORTAL_ENTITY_SOURCE_SYSTEM_NAME, config.getSourceEntityTableName(), config.getSourceEntityFkFieldId());
			if (delete) {
				for (PortalFile portalFile : CollectionUtils.getIterable(fileList)) {
					getPortalFileService().deletePortalFile(portalFile.getId());
				}
			}
			else {
				fileList = BeanUtils.filter(fileList, PortalFile::isApproved);
				if (!CollectionUtils.isEmpty(fileList)) {
					getPortalFileService().approvePortalFileList(BeanUtils.getBeanIdentityArray(fileList, Integer.class), false);
				}
			}
		}
		return updatePortalFileAwarePostedProperty(config, sourceEntity, false);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////                PortalFileAware Updates               /////////////
	////////////////////////////////////////////////////////////////////////////////


	protected T updatePortalFileAwarePostedProperty(ReportPostingFileConfiguration config, T sourceEntity, boolean posted) {
		PortalFileAwarePostedEvent<T> event = new PortalFileAwarePostedEvent<>(config, sourceEntity, posted);
		getEventHandler().raiseEvent(event);
		return event.getResult();
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}


	public PortalFileUploadService getPortalFileUploadService() {
		return this.portalFileUploadService;
	}


	public void setPortalFileUploadService(PortalFileUploadService portalFileUploadService) {
		this.portalFileUploadService = portalFileUploadService;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}
}
