package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.search.BusinessContactSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntityFieldType;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityContactCopyJob</code> handles UPDATES/DELETES only to contacts that are in the portal
 * This assumes that the initial post of a contact will set up the portal entity first, then this job keeps the properties maintained
 * <p>
 * See: {@link com.clifton.ims.portal.jobs.file.PortalFileRelationshipManagerUpdateJob} for where the contact entities are initial created
 *
 * @author manderson
 */
public class PortalEntityContactCopyJob extends BasePortalEntityCopyJob<BusinessContact> {


	private BusinessContactService businessContactService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_CONTACT;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessContact sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		return null;
	}


	@Override
	protected List<BusinessContact> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		if (CollectionUtils.isEmpty(portalEntityMap)) {
			return null;
		}

		BusinessContactSearchForm searchForm = new BusinessContactSearchForm();
		searchForm.setIds(portalEntityMap.keySet().toArray(new Integer[portalEntityMap.size()]));
		return getBusinessContactService().getBusinessContactList(searchForm);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessContact sourceEntity) {
		return false; // No Inserts
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessContact sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		// Set Name as Last Name, First Name so we can easily sort
		portalEntity.setName(sourceEntity.getLastName() + (StringUtils.isEmpty(sourceEntity.getFirstName()) ? "" : (", " + sourceEntity.getFirstName())));
		portalEntity.setLabel(sourceEntity.getLabelShort());
		portalEntity.setDescription(null);
		portalEntity.setStartDate(sourceEntity.getStartDate());
		portalEntity.setEndDate(sourceEntity.getEndDate());
	}


	@Override
	protected String getFieldValueForSourceEntity(BusinessContact sourceEntity, String fieldTypeName, Map<String, Object> context) {
		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_BIOGRAPHY, fieldTypeName)) {
			return getBusinessContactCustomFieldValueAsString(sourceEntity.getId(), BusinessContact.CUSTOM_COLUMN_BIOGRAPHY);
		}
		else if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_EMAIL, fieldTypeName)) {
			return sourceEntity.getEmailAddress();
		}
		else if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_PHONE_NUMBER, fieldTypeName)) {
			return sourceEntity.getPhoneNumber();
		}
		else if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_TITLE, fieldTypeName)) {
			return sourceEntity.getTitle();
		}
		else if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_PROFESSIONAL_DESIGNATIONS, fieldTypeName)) {
			String valueString = getBusinessContactCustomFieldValueAsString(sourceEntity.getId(), BusinessContact.CUSTOM_COLUMN_PROFESSIONAL_DESIGNATION);
			if (!StringUtils.isEmpty(valueString)) {
				String appendToString = getBusinessContactCustomFieldValueAsString(sourceEntity.getId(), BusinessContact.CUSTOM_COLUMN_PROFESSIONAL_DESIGNATION_2);
				if (!StringUtils.isEmpty(appendToString)) {
					valueString += ", " + appendToString;

					appendToString = getBusinessContactCustomFieldValueAsString(sourceEntity.getId(), BusinessContact.CUSTOM_COLUMN_PROFESSIONAL_DESIGNATION_3);
					if (!StringUtils.isEmpty(appendToString)) {
						valueString += ", " + appendToString;
					}
				}
			}
			return valueString;
		}
		return null;
	}


	private String getBusinessContactCustomFieldValueAsString(Integer contactId, String columnName) {
		return (String) getSystemColumnValueService().getSystemColumnCustomValue(contactId, BusinessContact.BUSINESS_CONTACT_EMPLOYEE_DETAILS_CUSTOM_COLUMN_GROUP_NAME, columnName);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}
}
