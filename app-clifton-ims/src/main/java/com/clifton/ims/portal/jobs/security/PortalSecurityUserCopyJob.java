package com.clifton.ims.portal.jobs.security;

import com.clifton.business.contact.BusinessContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.PortalSecurityUserRole;
import com.clifton.portal.security.user.search.PortalSecurityUserRoleSearchForm;
import com.clifton.security.user.SecurityUser;

import java.util.Map;


/**
 * The <code>PortalSecurityUserCopyJob</code> copies IMS users to Portal Users.
 * Will create where missing, disable where removed or disabled in IMS
 * <p>
 *
 * @author manderson
 */
public class PortalSecurityUserCopyJob extends BasePortalSecurityUserJob {


	private BusinessContactService businessContactService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * When new users are created from IMS users, the role they are given.
	 * This can be changed within Portal Admin app, but would be rare and only to give users additional administrative functionality
	 */
	private String defaultPortalSecurityUserRoleName = PortalSecurityUserRole.ROLE_PPA_INTERNAL_USER;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		PortalSecurityUserRole defaultRole = getDefaultPortalSecurityUserRole();

		Map<Short, PortalSecurityUser> portalSecurityUserMap = getPortalSecurityUserMap(false, context);
		Map<Short, SecurityUser> securityUserMap = getSecurityUserMap();

		int successCount = 0;
		int disabledCount = 0;
		int failCount = 0;
		int skipCount = 0;

		for (Map.Entry<Short, SecurityUser> shortSecurityUserEntry : securityUserMap.entrySet()) {
			SecurityUser securityUser = shortSecurityUserEntry.getValue();
			BusinessContact contact = ((securityUser == null || securityUser.getContactIdentifier() == null) ? null : getBusinessContactService().getBusinessContact(securityUser.getContactIdentifier()));
			PortalSecurityUser portalSecurityUser = portalSecurityUserMap.get(shortSecurityUserEntry.getKey());

			try {
				// User has been disabled in IMS or no Contact Mapped yet
				// Initial script will load existing disabled users for history, but for maintenance we won't insert disabled users
				if (contact == null || securityUser.isDisabled() || StringUtils.isEmpty(contact.getEmailAddress())) {
					// Disable in Portal if Exists
					if (portalSecurityUser != null) {
						if (!portalSecurityUser.isDisabled()) {
							portalSecurityUser.setDisabled(true);

							getPortalSecurityUserService().savePortalSecurityUserFromSourceSystem(portalSecurityUser);
							disabledCount++;
						}
						else {
							skipCount++;
						}
					}
				}
				else {
					if (portalSecurityUser == null) {
						portalSecurityUser = new PortalSecurityUser();
						portalSecurityUser.setPortalEntitySourceSystem(getPortalEntitySourceSystem(context));
						portalSecurityUser.setSourceFKFieldId(securityUser.getId());
						portalSecurityUser.setUserRole(defaultRole);
					}
					updatePortalSecurityUser(portalSecurityUser, securityUser, contact);
					getPortalSecurityUserService().savePortalSecurityUserFromSourceSystem(portalSecurityUser);
					successCount++;
				}
			}
			catch (Exception e) {
				failCount++;
				String message = "Error saving Portal Security User: " + BeanUtils.getLabel(portalSecurityUser) + ": " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				status.addError(message);
			}
		}

		// Any In Portal that Don't exist in IMS anymore - just disable them
		for (Map.Entry<Short, PortalSecurityUser> shortPortalSecurityUserEntry : portalSecurityUserMap.entrySet()) {
			if (!securityUserMap.containsKey(shortPortalSecurityUserEntry.getKey())) {
				PortalSecurityUser portalSecurityUser = shortPortalSecurityUserEntry.getValue();
				if (!portalSecurityUser.isDisabled()) {
					portalSecurityUser.setDisabled(true);
					try {
						getPortalSecurityUserService().savePortalSecurityUserFromSourceSystem(portalSecurityUser);
						disabledCount++;
					}
					catch (Exception e) {
						failCount++;
						String message = "Error saving Portal Security User: " + portalSecurityUser.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e);
						LogUtils.errorOrInfo(getClass(), message, e);
						status.addError(message);
					}
				}
			}
		}
		status.setMessageWithErrors("Updated Portal Security Users: " + successCount + " Successfully Inserted/Updated, " + disabledCount + " Disabled, " + skipCount + " Skipped, " + failCount + " failed.", 5);
		return status;
	}


	protected void updatePortalSecurityUser(PortalSecurityUser portalSecurityUser, SecurityUser securityUser, BusinessContact contact) {
		portalSecurityUser.setUsername(securityUser.getUserName());
		portalSecurityUser.setEmailAddress(contact.getEmailAddress());
		portalSecurityUser.setFirstName(contact.getFirstName());
		portalSecurityUser.setLastName(contact.getLastName());
		portalSecurityUser.setTitle(contact.getTitle());
		portalSecurityUser.setCompanyName(contact.getCompany() != null ? contact.getCompany().getName() : null);
		portalSecurityUser.setDisabled(securityUser.isDisabled());
	}


	protected PortalSecurityUserRole getDefaultPortalSecurityUserRole() {
		ValidationUtils.assertNotNull(getDefaultPortalSecurityUserRoleName(), "Default Portal Security User Role Name Is Required.");
		// Note: Not cached by name because this is currently the only retrieval by name and it's only done once for this batch job
		PortalSecurityUserRoleSearchForm securityUserRoleSearchForm = new PortalSecurityUserRoleSearchForm();
		securityUserRoleSearchForm.setNameEquals(getDefaultPortalSecurityUserRoleName());
		return CollectionUtils.getOnlyElementStrict(getPortalSecurityUserService().getPortalSecurityUserRoleList(securityUserRoleSearchForm));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public String getDefaultPortalSecurityUserRoleName() {
		return this.defaultPortalSecurityUserRoleName;
	}


	public void setDefaultPortalSecurityUserRoleName(String defaultPortalSecurityUserRoleName) {
		this.defaultPortalSecurityUserRoleName = defaultPortalSecurityUserRoleName;
	}
}
