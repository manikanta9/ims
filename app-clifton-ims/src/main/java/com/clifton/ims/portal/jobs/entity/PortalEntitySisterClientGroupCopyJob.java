package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.core.util.MathUtils;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntitySisterClientGroupCopyJob</code> is used to maintain sister client groups in the portal
 * <p>
 * <p>
 * Note: Applies to BusinessClient table where ClientCategoryType = "VIRTUAL_CLIENT" and IsChildrenAllowed = true AND Category Child Category Type = SISTER_CLIENT
 * <p>
 * Since sister clients do not belong to a Client Relationship, for the portal - the sister client groups act as a client relationship in the portal
 *
 * @author manderson
 */
public class PortalEntitySisterClientGroupCopyJob extends BasePortalEntityCopyJob<BusinessClient> {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_SISTER_CLIENT_GROUP;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		BusinessCompany outsourcedCIO = getIdentityObjectToOrganizationGroupMap(context).get(sourceEntity);
		if (outsourcedCIO != null) {
			if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
				// Verify the parent is the same, if so return it - otherwise look it up
				if (PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), outsourcedCIO.getId())) {
					return portalEntity.getParentPortalEntity();
				}
			}
			return getPortalEntityBySource(PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP, outsourcedCIO.getId(), context);
		}
		// Otherwise no parent
		return null;
	}


	@Override
	protected List<BusinessClient> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
		searchForm.setCategoryType(BusinessClientCategoryTypes.VIRTUAL_CLIENT);
		searchForm.setCategoryChildrenAllowed(true);
		searchForm.setCategoryChildCategoryType(BusinessClientCategoryTypes.SISTER_CLIENT);
		return getBusinessClientService().getBusinessClientList(searchForm);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessClient sourceEntity) {
		return sourceEntity.getTerminateDate() == null;
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getLegalLabel());
		portalEntity.setDescription(sourceEntity.getLegalLabel());
		portalEntity.setStartDate(sourceEntity.getInceptionDate());
		// Note: End Date is Set in {@link PortalEntityObserver} based on the last end date of all of it's children (if any are blank, this is blank)
	}


	@Override
	protected String getFieldValueForSourceEntity(BusinessClient sourceEntity, String fieldTypeName, Map<String, Object> context) {
		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) || StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_CONTACT_TEAM_EMAIL, fieldTypeName)) {
			List<BusinessCompanyContact> companyContactList = getRelationshipManagerListForClient(sourceEntity, context);
			// If more than one - pull the value from the Primary Only - otherwise we'll set them on the account (which is where if there is more than 1 they would be used)
			if (CollectionUtils.getSize(companyContactList) > 1) {
				companyContactList = BeanUtils.filter(companyContactList, BusinessCompanyContact::isPrimary);
			}
			if (CollectionUtils.getSize(companyContactList) == 1) {
				return getPortalTeamOverrideEmailAddressForBusinessCompanyContact(companyContactList.get(0), StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) ? BusinessCompanyContact.CUSTOM_FIELD_PORTAL_SUPPORT_TEAM_OVERRIDE : BusinessCompanyContact.CUSTOM_FIELD_PORTAL_CONTACT_TEAM_OVERRIDE, context);
			}
		}
		return null;
	}
}
