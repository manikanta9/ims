package com.clifton.ims.portal.jobs.file;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.portal.jobs.BasePortalJob;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portal.file.search.PortalFileSearchForm;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileSetupService;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


/**
 * The <code>BasePortalFileUpdateJob</code> can be extended by jobs where the system manually manages the files and file assignments
 * This is currently used for Relationship Managers and Investment Teams.
 * <p>
 * We have one "file" per contact and one "file" per team and those files are assigned to the relevant entities (Relationships, Sister Clients)
 * This job will also insert the missing source entity (i.e. contact or team) that is needed to post.  That insert is very basic and expects the
 * relevant entity copy job will properly set name, label, description, and field values (if applies).
 *
 * @author manderson
 */
public abstract class BasePortalFileUpdateJob<T extends IdentityObject> extends BasePortalJob {

	private PortalFileService portalFileService;
	private PortalFileSetupService portalFileSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		// Create a Map of Source Entity to the Portal Entities they should be posted to.
		Map<Integer, List<PortalEntity>> sourceToPostPortalEntityMap = getSourceToPostPortalEntityListMap(context);

		// Add missing source entities to the portal, this will do inserts only, no updates which should be handled by relevant Portal Entity Copy job.
		addMissingSourceEntitiesToPortalEntityTable(sourceToPostPortalEntityMap, status, context);

		// Each Source Entity is associated with a particular Portal File
		// If there isn't an existing PortalFile, this will create the new object, but the save will happen below.
		Map<Integer, PortalFile> sourceEntityPortalFileMap = getSourceEntityPortalFileMap(sourceToPostPortalEntityMap, context);

		// For Each Source/Portal File, update the assignments for each portal entity it should be posted to.
		int successCount = 0;
		int failCount = 0;
		for (Map.Entry<Integer, PortalFile> integerPortalFileEntry : sourceEntityPortalFileMap.entrySet()) {
			PortalFile portalFile = integerPortalFileEntry.getValue();
			try {
				getPortalFileService().updatePortalFileManualAssignments(portalFile, getPortalEntitySourceSystem(context), new HashSet<>(), CollectionUtils.asNonNullList(sourceToPostPortalEntityMap.get(integerPortalFileEntry.getKey())));
				successCount++;
			}
			catch (Exception e) {
				failCount++;
				String message = "Error saving Portal File: " + portalFile.getDisplayName() + " and it's assignments: " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				status.addError(message);
			}
		}

		status.appendToMessage("Successfully saved " + successCount + " Portal Files and their assignments. Failed: " + failCount);
		status.setMessageWithErrors(status.getMessage(), 5);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract String getFileGroupName();


	protected abstract String getSourcePortalEntitySourceSectionName();


	protected abstract Map<Integer, List<PortalEntity>> getSourceToPostPortalEntityListMap(Map<String, Object> context);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void addPostPortalEntityToMap(String postEntitySourceSectionName, Integer postEntitySourceId, T sourceEntity, Map<Integer, List<PortalEntity>> sourceToPostPortalEntityMap, Map<String, Object> context) {
		if (sourceEntity != null) {
			PortalEntity postPortalEntity = getPortalEntityBySource(postEntitySourceSectionName, postEntitySourceId, context);
			if (postPortalEntity != null) {
				Integer sourceEntityId = BeanUtils.getIdentityAsInteger(sourceEntity);
				List<PortalEntity> entityList = sourceToPostPortalEntityMap.get(sourceEntityId);
				if (entityList == null) {
					entityList = new ArrayList<>();
				}
				// Check that it's not there already. For Teams, there can be multiple active accounts under a relationship for a team.  We only post it once
				if (!entityList.contains(postPortalEntity)) {
					entityList.add(postPortalEntity);
				}
				sourceToPostPortalEntityMap.put(sourceEntityId, entityList);
			}
		}
	}


	protected void addMissingSourceEntitiesToPortalEntityTable(Map<Integer, List<PortalEntity>> sourceToPostPortalEntityMap, Status status, Map<String, Object> context) {
		List<PortalEntity> insertList = new ArrayList<>();

		for (Integer sourceId : sourceToPostPortalEntityMap.keySet()) {
			PortalEntity sourceEntity = getPortalEntityBySource(getSourcePortalEntitySourceSectionName(), sourceId, context);
			// Note: The relevant batch jobs to update the entities will fix them and their properties
			// i.e. The PortalEntityContactJob will properly update and fill in the contact data
			// The PortalEntityTeamCopyJob will properly set the team name and email address
			if (sourceEntity == null) {
				sourceEntity = new PortalEntity();
				sourceEntity.setEntitySourceSection(getPortalEntitySourceSectionByName(getSourcePortalEntitySourceSectionName(), context));
				sourceEntity.setSourceFkFieldId(sourceId);
				sourceEntity.setName(getSourcePortalEntitySourceSectionName() + ": " + sourceId);
				insertList.add(sourceEntity);
			}
		}

		if (!CollectionUtils.isEmpty(insertList)) {
			int successCount = 0;
			int failCount = 0;
			for (PortalEntity portalEntity : insertList) {
				try {
					getPortalEntityService().savePortalEntity(portalEntity);
					successCount++;
				}
				catch (Exception e) {
					failCount++;
					String message = "Error inserting Portal Entity: " + portalEntity.getEntityLabelWithSourceSection() + ": " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), message, e);
					status.addError(message);
				}
			}
			status.appendToMessage(getSourcePortalEntitySourceSectionName() + " portal entities inserted: " + successCount + " successful, " + failCount + " failed");
			clearPortalEntitySourceMap(getSourcePortalEntitySourceSectionName(), context);
		}
	}


	protected Map<Integer, PortalFile> getSourceEntityPortalFileMap(Map<Integer, List<PortalEntity>> sourceToPostPortalEntityMap, Map<String, Object> context) {
		PortalFileCategory fileGroup = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, getFileGroupName());
		if (fileGroup == null) {
			throw new ValidationException("File Group [" + getFileGroupName() + "] is missing from the Portal");
		}

		PortalEntitySourceSection sourceSection = getPortalEntitySourceSectionByName(getSourcePortalEntitySourceSectionName(), context);

		PortalFileSearchForm searchForm = new PortalFileSearchForm();
		searchForm.setEntitySourceSectionId(sourceSection.getId());
		searchForm.setFileGroupId(fileGroup.getId());

		List<PortalFile> fileList = getPortalFileService().getPortalFileList(searchForm);

		Map<Integer, PortalFile> sourceEntityPortalFileMap = BeanUtils.getBeanMap(fileList, PortalFile::getSourceFkFieldId);


		for (Integer sourceId : sourceToPostPortalEntityMap.keySet()) {
			// No Portal File - Add One
			if (!sourceEntityPortalFileMap.containsKey(sourceId)) {
				PortalFile portalFile = new PortalFile();
				portalFile.setFileCategory(fileGroup);
				portalFile.setEntitySourceSection(sourceSection);
				portalFile.setSourceFkFieldId(sourceId);
				portalFile.setApproved(true);
				portalFile.setPublishDate(new Date());
				portalFile.setReportDate(new Date());
				sourceEntityPortalFileMap.put(sourceId, portalFile);
			}
		}
		return sourceEntityPortalFileMap;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}
}
