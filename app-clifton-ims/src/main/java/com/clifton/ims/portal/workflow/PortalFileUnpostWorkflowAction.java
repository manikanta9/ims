package com.clifton.ims.portal.workflow;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.workflow.WorkflowAware;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>PortalFileUnpostWorkflowAction</code> is a generic unposting action that allows unposting of
 * portal files from any entity that implements WorkflowAware
 *
 * @author manderson
 */
public class PortalFileUnpostWorkflowAction<T extends WorkflowAware> implements WorkflowTransitionActionHandler<T> {


	private DaoLocator daoLocator;

	private ReportPostingController<T> reportPostingController;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		ReadOnlyDAO<T> dao = getDaoLocator().locate(bean);
		String tableName = dao.getConfiguration().getTableName();
		Integer beanId = BeanUtils.getIdentityAsInteger(bean);

		ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
		postingConfig.setSourceEntityTableName(tableName);
		postingConfig.setSourceEntityFkFieldId(beanId);

		return getReportPostingController().unpostClientFile(postingConfig, bean, false);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ReportPostingController<T> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<T> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}
}
