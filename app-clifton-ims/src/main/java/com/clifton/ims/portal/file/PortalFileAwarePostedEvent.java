package com.clifton.ims.portal.file;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.event.Event;
import com.clifton.core.util.event.EventObject;
import com.clifton.report.posting.ReportPostingFileConfiguration;


/**
 * The <code>PortalFileAwarePostedEvent</code> represents an {@link Event} for posting/unposting PortalFiles
 * If the target (ReportPostingFileConfiguration) object contains a source object, will use otherwise will attempt to retrieve it using sourceEntityTableName and sourceEntityFkFieldId
 * properties.  The result is the result of the saved PortalFileAware object
 *
 * @author manderson
 */
public class PortalFileAwarePostedEvent<T extends IdentityObject> extends EventObject<ReportPostingFileConfiguration, T> {

	public static final String EVENT_NAME = "PORTAL_FILE_POSTED_EVENT";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If the file was posted/or all files for the source entity were unposted
	 * Note: posted means there is at least one (usually only one) file posted and APPROVED
	 * Otherwise there are no files or all files are currently unapproved
	 */
	private final boolean posted;

	/**
	 * The source entity for the portal file.  i.e. PortfolioRun, AccountingPeriodClosing
	 * If the object is passed in, and implements PortalFileAware, this object is updated and save and the save result is the event result
	 * If the object is NOT passed in, will use dao locator  to find the source entity (if config has source table and source fk field id populate) save that object and save result is the event result
	 * If the sourceEntity is not PortalFileAware - nothing is done
	 */
	private final T sourceEntity;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PortalFileAwarePostedEvent(ReportPostingFileConfiguration config, T sourceEntity, boolean posted) {
		super(EVENT_NAME);
		setTarget(config);
		this.sourceEntity = sourceEntity;
		this.posted = posted;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isPosted() {
		return this.posted;
	}


	public T getSourceEntity() {
		return this.sourceEntity;
	}
}
