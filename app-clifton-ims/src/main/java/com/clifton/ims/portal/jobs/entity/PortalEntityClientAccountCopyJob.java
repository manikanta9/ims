package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.setup.BusinessClientCategory;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.core.util.MathUtils;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityClientAccountCopyJob</code> is used to maintain Client Accounts in the Portal
 * <p>
 *
 * @author manderson
 */
public class PortalEntityClientAccountCopyJob extends BasePortalEntityCopyJob<InvestmentAccount> {

	private InvestmentAccountService investmentAccountService;


	/**
	 * If set and client account is terminated and is a fund investor and end date is not set
	 * will default the portal end date to 4/30 of the following year.  If blank, then will leave end date
	 * blank and will let portal default end date to x days after termination date
	 */
	private String fundInvestorEndMonthDayOfFollowingYear = "0430";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return true;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(InvestmentAccount sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		String sourceSectionName = PORTAL_SOURCE_SECTION_CLIENT;
		BusinessClientCategory clientCategory = sourceEntity.getBusinessClient().getCategory();
		if (clientCategory.getCategoryType() == BusinessClientCategoryTypes.SISTER_CLIENT) {
			sourceSectionName = PORTAL_SOURCE_SECTION_SISTER_CLIENT;
		}
		else if (clientCategory.getCategoryType() == BusinessClientCategoryTypes.VIRTUAL_CLIENT && clientCategory.isChildrenAllowed() && clientCategory.getChildClientCategory() != null && clientCategory.getChildClientCategory().getCategoryType() == BusinessClientCategoryTypes.CLIENT) {
			sourceSectionName = PORTAL_SOURCE_SECTION_CLIENT_GROUP;
		}
		if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
			// Verify the parent is the same, if so return it - otherwise look it up
			if (sourceSectionName.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), sourceEntity.getBusinessClient().getId())) {
				return portalEntity.getParentPortalEntity();
			}
		}
		return getPortalEntityBySource(sourceSectionName, sourceEntity.getBusinessClient().getId(), context);
	}


	@Override
	protected List<InvestmentAccount> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		return getInvestmentAccountService().getInvestmentAccountList(searchForm);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(InvestmentAccount sourceEntity) {
		return !sourceEntity.getWorkflowState().getName().contains("Terminated") && !sourceEntity.getWorkflowState().getName().contains("Paused");
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(InvestmentAccount sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getNumber());
		portalEntity.setLabel(sourceEntity.getNumber() + ": " + sourceEntity.getName());
		portalEntity.setDescription(StringUtils.coalesce(false, sourceEntity.getShortName(), sourceEntity.getName()));
		portalEntity.setStartDate(sourceEntity.getInceptionDate());
		if ("Terminated".equals(sourceEntity.getWorkflowState().getName())) {
			portalEntity.setTerminationDate(sourceEntity.getWorkflowStateEffectiveStartDate());
			// If Entity End Date is NULL - see if it's Fund Investor - If so we need set set end date to 4/30 of following year
			if (portalEntity.getEndDate() == null && !StringUtils.isEmpty(getFundInvestorEndMonthDayOfFollowingYear())) {
				if (CollectionUtils.contains(getFundInvestorClientAccountIds(null, context), sourceEntity.getId())) {
					portalEntity.setEndDate(DateUtils.toDate(getFundInvestorEndMonthDayOfFollowingYear() + (DateUtils.getYear(portalEntity.getTerminationDate()) + 1), "MMddyyyy"));
				}
			}
		}
		else {
			portalEntity.setTerminationDate(null);
			portalEntity.setEndDate(null);
		}
	}


	@Override
	protected String getFieldValueForSourceEntity(InvestmentAccount sourceEntity, String fieldTypeName, Map<String, Object> context) {
		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SERVICE_NAME, fieldTypeName)) {
			return sourceEntity.getCoalesceBusinessServiceGroupName();
		}

		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) || StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_CONTACT_TEAM_EMAIL, fieldTypeName)) {
			BusinessCompanyContact companyContact = getRelationshipManagerOverrideForClientAccount(sourceEntity, context);
			if (companyContact != null) {
				return getPortalTeamOverrideEmailAddressForBusinessCompanyContact(companyContact, StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) ? BusinessCompanyContact.CUSTOM_FIELD_PORTAL_SUPPORT_TEAM_OVERRIDE : BusinessCompanyContact.CUSTOM_FIELD_PORTAL_CONTACT_TEAM_OVERRIDE, context);
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public String getFundInvestorEndMonthDayOfFollowingYear() {
		return this.fundInvestorEndMonthDayOfFollowingYear;
	}


	public void setFundInvestorEndMonthDayOfFollowingYear(String fundInvestorEndMonthDayOfFollowingYear) {
		this.fundInvestorEndMonthDayOfFollowingYear = fundInvestorEndMonthDayOfFollowingYear;
	}
}
