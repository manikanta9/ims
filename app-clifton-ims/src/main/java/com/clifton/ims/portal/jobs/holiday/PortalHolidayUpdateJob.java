package com.clifton.ims.portal.jobs.holiday;

import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.ims.portal.jobs.BasePortalJob;
import com.clifton.portal.holiday.PortalHoliday;
import com.clifton.portal.holiday.PortalHolidaySearchForm;
import com.clifton.portal.holiday.PortalHolidayService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalHolidayUpdateJob</code> updates the PortalHoliday table with holidays from a specified calendar in IMS and date range
 *
 * @author manderson
 */
public class PortalHolidayUpdateJob extends BasePortalJob {


	private CalendarHolidayService calendarHolidayService;

	private PortalHolidayService portalHolidayService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The calendar to use to determine the holidays
	 */
	private short calendarId;

	/**
	 * How many days in the past to start the sync
	 * Will ONLY update holidays in the portal on or after this date.
	 */
	private int startDaysBackFromToday;

	/**
	 * How many days in the future to end the sync.
	 * Will ONLY update holidays in the portal on or before this date.
	 */
	private int endDaysForwardFromToday;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		Date today = DateUtils.clearTime(new Date());
		Date startDate = DateUtils.addDays(today, -getStartDaysBackFromToday());
		Date endDate = DateUtils.addDays(today, getEndDaysForwardFromToday());

		Date[] holidays = getHolidayDates(startDate, endDate);
		List<PortalHoliday> portalHolidayList = getPortalHolidayList(startDate, endDate);

		status.setActionPerformed(false); // More likely not to have any saves

		if (holidays == null || holidays.length == 0) {
			if (!CollectionUtils.isEmpty(portalHolidayList)) {
				getPortalHolidayService().deletePortalHolidayList(portalHolidayList);
				status.setActionPerformed(true);
				status.setMessage("Removed the following holidays: " + StringUtils.collectionToCommaDelimitedString(portalHolidayList, portalHoliday -> DateUtils.fromDateShort(portalHoliday.getHolidayDate())));
			}
		}
		else {
			Map<Date, PortalHoliday> portalHolidayMap = BeanUtils.getBeanMap(portalHolidayList, PortalHoliday::getHolidayDate);

			List<PortalHoliday> insertList = new ArrayList<>();

			for (Date holidayDate : holidays) {
				if (!portalHolidayMap.containsKey(holidayDate)) {
					PortalHoliday portalHoliday = new PortalHoliday();
					portalHoliday.setHolidayDate(holidayDate);
					insertList.add(portalHoliday);
				}
				else {
					portalHolidayMap.remove(holidayDate);
				}
			}


			if (!CollectionUtils.isEmpty(insertList)) {
				getPortalHolidayService().savePortalHolidayList(insertList);
				status.setActionPerformed(true);
				status.appendToMessage("Added the following holidays: " + StringUtils.collectionToCommaDelimitedString(insertList, portalHoliday -> DateUtils.fromDateShort(portalHoliday.getHolidayDate())));
			}

			if (!CollectionUtils.isEmpty(portalHolidayMap)) {
				List<PortalHoliday> deleteList = new ArrayList<>(portalHolidayMap.values());
				getPortalHolidayService().deletePortalHolidayList(deleteList);
				status.setActionPerformed(true);
				status.appendToMessage("Removed the following holidays: " + StringUtils.collectionToCommaDelimitedString(portalHolidayList, portalHoliday -> DateUtils.fromDateShort(portalHoliday.getHolidayDate())));
			}
		}
		return status;
	}


	private Date[] getHolidayDates(Date startDate, Date endDate) {
		CalendarHolidayDaySearchForm searchForm = new CalendarHolidayDaySearchForm();
		searchForm.setCalendarId(getCalendarId());
		// Calendar Days have start date = beginning of date and end date = beginning of next day so we search only against start date field
		searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		List<CalendarHolidayDay> calendarHolidayDayList = getCalendarHolidayService().getCalendarHolidayDayList(searchForm);
		// WARNING: MUST USE DateUtils.clearTime so the object is a Date.  CalendarDay Start Date supports time and otherwise returns as Timestamp which doesn't work when looking up dates in the map
		return BeanUtils.getPropertyValues(calendarHolidayDayList, calendarHolidayDay -> DateUtils.clearTime(calendarHolidayDay.getDay().getStartDate()), Date.class);
	}


	private List<PortalHoliday> getPortalHolidayList(Date startDate, Date endDate) {
		PortalHolidaySearchForm searchForm = new PortalHolidaySearchForm();
		searchForm.setStartDate(startDate);
		searchForm.setEndDate(endDate);
		return getPortalHolidayService().getPortalHolidayList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public PortalHolidayService getPortalHolidayService() {
		return this.portalHolidayService;
	}


	public void setPortalHolidayService(PortalHolidayService portalHolidayService) {
		this.portalHolidayService = portalHolidayService;
	}


	public short getCalendarId() {
		return this.calendarId;
	}


	public void setCalendarId(short calendarId) {
		this.calendarId = calendarId;
	}


	public int getStartDaysBackFromToday() {
		return this.startDaysBackFromToday;
	}


	public void setStartDaysBackFromToday(int startDaysBackFromToday) {
		this.startDaysBackFromToday = startDaysBackFromToday;
	}


	public int getEndDaysForwardFromToday() {
		return this.endDaysForwardFromToday;
	}


	public void setEndDaysForwardFromToday(int endDaysForwardFromToday) {
		this.endDaysForwardFromToday = endDaysForwardFromToday;
	}
}
