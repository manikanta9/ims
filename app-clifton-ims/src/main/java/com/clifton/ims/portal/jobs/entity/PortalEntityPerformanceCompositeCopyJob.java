package com.clifton.ims.portal.jobs.entity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityRollup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityPerformanceCompositeCopyJob</code> is used to update performance composites in the Portal
 * Also assigns any client account that was assigned to the composite to the rollup table.
 * If the composite is a rollup, checks child composite account assignments.
 * <p>
 * Rollup table dates are the account assignment start end dates
 *
 * @author manderson
 */
public class PortalEntityPerformanceCompositeCopyJob extends BasePortalEntityCopyJob<PerformanceComposite> {


	private PerformanceCompositeSetupService performanceCompositeSetupService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_PERFORMANCE_COMPOSITE;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(PerformanceComposite sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		return null;
	}


	@Override
	protected List<PerformanceComposite> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		return getPerformanceCompositeSetupService().getPerformanceCompositeList(new PerformanceCompositeSearchForm());
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(PerformanceComposite sourceEntity) {
		return sourceEntity.isActive();
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(PerformanceComposite sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(StringUtils.coalesce(true, sourceEntity.getCompositeShortName(), sourceEntity.getName()));
		portalEntity.setLabel(sourceEntity.getLabel());
		portalEntity.setDescription(sourceEntity.getDescription());
		portalEntity.setStartDate(sourceEntity.getStartDate());
		portalEntity.setEndDate(sourceEntity.getEndDate());
	}


	@Override
	protected Map<PortalEntity, List<PortalEntityRollup>> populatePortalEntityRollupListMap(List<PortalEntity> portalEntityList, Map<String, Object> context) {
		Map<PortalEntity, List<PortalEntityRollup>> portalEntityRollupListMap = new HashMap<>();
		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			// If Source was deleted - then clear the rollup list
			if (portalEntity.isDeleted()) {
				portalEntityRollupListMap.put(portalEntity, null);
			}
			else {
				PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
				// Search Form acts differently if the composite is a roll up or not
				PerformanceComposite composite = getPerformanceCompositeSetupService().getPerformanceComposite(portalEntity.getSourceFkFieldId().shortValue());
				if (composite.isRollupComposite()) {
					searchForm.setRollupPerformanceCompositeId(composite.getId());
				}
				else {
					searchForm.setPerformanceCompositeId(composite.getId());
				}
				List<PerformanceCompositeInvestmentAccount> accountAssignmentList = getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(searchForm);

				// These are not 1:1, so we consider existing rollups to be equal if the dates are also equal.  These should rarely change, but if they do it will just do a delete/insert instead of an update
				// We need the child to be the account, not the assignment.
				Map<Integer, List<PortalEntityRollup>> rollupMap = BeanUtils.getBeansMap(getPortalEntityService().getPortalEntityRollupListByParent(portalEntity.getId()), portalEntityRollup -> portalEntityRollup.getReferenceTwo().getSourceFkFieldId());
				List<PortalEntityRollup> newRollupList = new ArrayList<>();

				for (PerformanceCompositeInvestmentAccount accountAssignment : CollectionUtils.getIterable(accountAssignmentList)) {
					List<PortalEntityRollup> rollupList = rollupMap.get(accountAssignment.getInvestmentAccount().getId());
					PortalEntityRollup rollup = null;

					for (PortalEntityRollup child : CollectionUtils.getIterable(rollupList)) {
						if (DateUtils.isEqualWithoutTime(child.getStartDate(), accountAssignment.getStartDate()) && DateUtils.isEqualWithoutTime(child.getEndDate(), accountAssignment.getEndDate())) {
							rollup = child;
							break;
						}
					}
					if (rollup == null) {
						PortalEntity childEntity = getPortalEntityBySource(PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT, accountAssignment.getInvestmentAccount().getId(), context);
						if (childEntity != null) {
							rollup = new PortalEntityRollup();
							rollup.setReferenceOne(portalEntity);
							rollup.setReferenceTwo(childEntity);
						}
					}
					if (rollup != null) {
						// Start and End Dates come from the assignment - set start to the first day of the month, and end to the last day of the month
						// so partial periods still get the composite report???
						rollup.setStartDate(accountAssignment.getStartDate() == null ? null : DateUtils.getFirstDayOfMonth(accountAssignment.getStartDate()));
						rollup.setEndDate(accountAssignment.getEndDate() == null ? null : DateUtils.getLastDayOfMonth(accountAssignment.getEndDate()));
						newRollupList.add(rollup);
					}
				}
				portalEntityRollupListMap.put(portalEntity, newRollupList);
			}
		}
		return portalEntityRollupListMap;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}
}
