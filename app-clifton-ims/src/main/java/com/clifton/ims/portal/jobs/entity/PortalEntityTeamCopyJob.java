package com.clifton.ims.portal.jobs.entity;

import com.clifton.core.util.StringUtils;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUserService;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityTeamCopyJob</code> handles maintaining teams in the Portal.
 * Teams are IMS Security Groups that start with "Team" and those that have "Do Not Use" in the name are excluded.
 * <p>
 * See: {@link com.clifton.ims.portal.jobs.file.PortalFileInvestmentTeamUpdateJob} which could also insert a missing team it needs to post
 * and then this job would fix the name/description/email properties.
 *
 * @author manderson
 */
public class PortalEntityTeamCopyJob extends BasePortalEntityCopyJob<SecurityGroup> {


	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_INVESTMENT_TEAM;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(SecurityGroup sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		return null;
	}


	@Override
	protected List<SecurityGroup> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		return getSecurityUserService().getSecurityGroupTeamList();
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(SecurityGroup sourceEntity) {
		// Teams that we stopped using, but are still assigned to old terminated accounts - don't add in
		return !(sourceEntity.getName().contains("Do Not Use"));
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(SecurityGroup sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getLabel());
		portalEntity.setDescription(sourceEntity.getDescription());
		portalEntity.setStartDate(null);
		portalEntity.setEndDate(null);
	}


	@Override
	protected String getFieldValueForSourceEntity(SecurityGroup sourceEntity, String fieldTypeName, Map<String, Object> context) {
		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_NAME_EMAIL, fieldTypeName)) {
			return sourceEntity.getEmailAddress();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
