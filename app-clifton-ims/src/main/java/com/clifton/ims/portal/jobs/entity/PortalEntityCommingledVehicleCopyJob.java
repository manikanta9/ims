package com.clifton.ims.portal.jobs.entity;

import com.clifton.accounting.gl.position.AccountingPositionHandlerImpl;
import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.calendar.InvestmentCalendarService;
import com.clifton.investment.calendar.InvestmentEvent;
import com.clifton.investment.calendar.search.InvestmentEventSearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityRollup;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityCommingledVehicleCopyJob</code> is used to commingled vehicle clients in the Portal
 * Also assigns any client account that had a transaction for a security where the security.investmentAccount.businessClient = this Fund to the rollup table.
 * Rollup table dates are applies the account start and end dates.
 * <p>
 * Note: Applies to BusinessClient table where ClientCategoryType = "FUND" only
 *
 * @author manderson
 */
public class PortalEntityCommingledVehicleCopyJob extends BasePortalEntityCopyJob<BusinessClient> {


	private InvestmentAccountService investmentAccountService;

	private InvestmentCalendarService investmentCalendarService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_COMMINGLED_VEHICLE;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		return null;
	}


	@Override
	protected List<BusinessClient> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
		searchForm.setCategoryType(BusinessClientCategoryTypes.FUND);
		return getBusinessClientService().getBusinessClientList(searchForm);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessClient sourceEntity) {
		return sourceEntity.getTerminateDate() == null;
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getLegalLabel());
		portalEntity.setDescription(sourceEntity.getLegalLabel());
		portalEntity.setStartDate(sourceEntity.getInceptionDate());
		portalEntity.setEndDate(sourceEntity.getTerminateDate());
	}


	@Override
	protected Map<PortalEntity, List<PortalEntityRollup>> populatePortalEntityRollupListMap(List<PortalEntity> portalEntityList, Map<String, Object> context) {
		Map<PortalEntity, List<PortalEntityRollup>> portalEntityRollupListMap = new HashMap<>();
		for (PortalEntity portalEntity : CollectionUtils.getIterable(portalEntityList)) {
			// If Source was deleted - then clear the rollup list
			if (portalEntity.isDeleted()) {
				portalEntityRollupListMap.put(portalEntity, null);
			}
			else {
				List<Integer> clientAccountIds = getFundInvestorClientAccountIds(portalEntity.getSourceFkFieldId(), context);

				Map<Integer, PortalEntityRollup> rollupMap = BeanUtils.getBeanMap(getPortalEntityService().getPortalEntityRollupListByParent(portalEntity.getId()), portalEntityRollup -> portalEntityRollup.getReferenceTwo().getSourceFkFieldId());
				List<PortalEntityRollup> newRollupList = new ArrayList<>();

				for (Integer clientAccountId : CollectionUtils.getIterable(clientAccountIds)) {
					PortalEntityRollup rollup = rollupMap.get(clientAccountId);
					if (rollup == null) {
						PortalEntity childEntity = getPortalEntityBySource(PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT, clientAccountId, context);
						if (childEntity != null) {
							rollup = new PortalEntityRollup();
							rollup.setReferenceOne(portalEntity);
							rollup.setReferenceTwo(childEntity);
						}
					}
					if (rollup != null) {
						// Use the Entity Start and Termination Date for Funds
						rollup.setStartDate(rollup.getReferenceTwo().getStartDate());
						InvestmentAccount clientAccount = getInvestmentAccountService().getInvestmentAccount(clientAccountId);
						// Since accounts aren't actually terminated for a period of time (so accounting can close everything out)
						// Once the status is no longer active back date the end date to the last positions off date
						// They wouldn't be included here unless they had positions on at some point which means they were active and no longer are
						if (clientAccount != null && !clientAccount.isActive()) {
							rollup.setEndDate(ObjectUtils.coalesce(getLastPositionsOnDateForClientAccount(clientAccount)));
						}
						// If end date is blank then check account termination date (should not be necessary, but fail safe)
						if (rollup.getEndDate() == null) {
							rollup.setEndDate(rollup.getReferenceTwo().getTerminationDate());
						}
						newRollupList.add(rollup);
					}
				}
				portalEntityRollupListMap.put(portalEntity, newRollupList);
			}
		}
		return portalEntityRollupListMap;
	}


	private Date getLastPositionsOnDateForClientAccount(InvestmentAccount clientAccount) {
		InvestmentEventSearchForm searchForm = new InvestmentEventSearchForm();
		searchForm.setInvestmentAccountId(clientAccount.getId());
		searchForm.setEventTypeName(AccountingPositionHandlerImpl.INVESTMENT_CALENDAR_EVENT_TYPE_POSITIONS_OFF);
		searchForm.setLimit(1);
		searchForm.setOrderBy("eventDate:desc");

		InvestmentEvent lastPositionsOffEvent = CollectionUtils.getFirstElement(getInvestmentCalendarService().getInvestmentEventList(searchForm));
		if (lastPositionsOffEvent != null) {
			return lastPositionsOffEvent.getEventDate();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentCalendarService getInvestmentCalendarService() {
		return this.investmentCalendarService;
	}


	public void setInvestmentCalendarService(InvestmentCalendarService investmentCalendarService) {
		this.investmentCalendarService = investmentCalendarService;
	}
}
