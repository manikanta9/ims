package com.clifton.ims.portal.jobs.file;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.file.PortalFile;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.security.user.SecurityGroup;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortalFileInvestmentTeamUpdateJob</code> handles creating new {@link PortalFile} objects where a new
 * team is found (Rare).  And updating all file assignments to teams for Client Accounts for any active account.
 * <p>
 *
 * @author manderson
 */
public class PortalFileInvestmentTeamUpdateJob extends BasePortalFileUpdateJob<SecurityGroup> {

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private String fileGroupName = PortalFileCategory.CATEGORY_NAME_MEET_YOUR_TEAM;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getSourcePortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_INVESTMENT_TEAM;
	}


	@Override
	protected Map<Integer, List<PortalEntity>> getSourceToPostPortalEntityListMap(Map<String, Object> context) {
		// Create a Map of Teams to the Portal Entities (Accounts) they should be posted to.
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);

		Map<Integer, List<PortalEntity>> teamPortalEntityMap = new HashMap<>();

		// This includes all accounts in the portal that are still active (we can't exclude terminated from IMS because in the portal they are still pending termination for a period of time)
		Set<Integer> accountIdsWithApprovedFilesPosted = getAccountIdsWithApprovedFilesPosted(context);

		for (InvestmentAccount account : CollectionUtils.getIterable(accountList)) {
			if (account.getTeamSecurityGroup() != null) {
				// If Account is Active - Include it - If it's not active, then include if it has approved files posted
				if (StringUtils.isEqual("Active", account.getWorkflowState().getName()) || accountIdsWithApprovedFilesPosted.contains(account.getId())) {
					addPostPortalEntityToMap(PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT, account.getId(), account.getTeamSecurityGroup(), teamPortalEntityMap, context);
				}
			}
		}
		return teamPortalEntityMap;
	}


	/**
	 * Excludes Fully Terminated accounts (accounts pending termination (terminated in IMS) are still included
	 */
	protected Set<Integer> getAccountIdsWithApprovedFilesPosted(Map<String, Object> context) {
		PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
		searchForm.setEntitySourceSectionId(getPortalEntitySourceSectionByName(PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT, context).getId());
		searchForm.setSecurableEntityHasApprovedFilesPosted(true);
		searchForm.setActive(true);
		List<PortalEntity> portalEntityList = getPortalEntityService().getPortalEntityList(searchForm);
		Set<Integer> accountIdSet = new HashSet<>();
		for (PortalEntity entity : CollectionUtils.getIterable(portalEntityList)) {
			accountIdSet.add(entity.getSourceFkFieldId());
		}
		return accountIdSet;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	@Override
	public String getFileGroupName() {
		return this.fileGroupName;
	}


	public void setFileGroupName(String fileGroupName) {
		this.fileGroupName = fileGroupName;
	}
}
