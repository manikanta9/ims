package com.clifton.ims.portal.jobs.security;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.portal.security.user.PortalSecurityUser;
import com.clifton.portal.security.user.group.PortalSecurityGroup;
import com.clifton.portal.security.user.group.PortalSecurityUserGroupService;
import com.clifton.portal.security.user.group.search.PortalSecurityGroupSearchForm;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.search.SecurityGroupSearchForm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>PortalSecurityGroupCopyJob</code> job copies all groups in IMS tagged with a specific hierarchy tag
 * as well as all of the users that are members of that group
 *
 * @author manderson
 */
public class PortalSecurityGroupCopyJob extends BasePortalSecurityUserJob implements ValidationAware {

	private PortalSecurityUserGroupService portalSecurityUserGroupService;


	////////////////////////////////////////////////////////////////////////////////

	/**
	 * The tag on the group that identifies it should be copied to the portal
	 */
	private Short securityGroupTagId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getSecurityGroupTagId(), "Security Group Tag is required.");
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		// Track All Portal Users for setting membership
		// Key is the IMS Security User ID
		Map<Short, PortalSecurityUser> portalSecurityUserMap = getPortalSecurityUserMap(false, context);

		Map<Short, SecurityGroup> securityGroupMap = getSecurityGroupMap();
		Map<Integer, PortalSecurityGroup> portalSecurityGroupMap = getPortalSecurityGroupMap(context);


		int successCount = 0;
		int deleteCount = 0;
		int failCount = 0;

		for (Map.Entry<Short, SecurityGroup> shortSecurityGroupEntry : securityGroupMap.entrySet()) {

			SecurityGroup securityGroup = shortSecurityGroupEntry.getValue();
			PortalSecurityGroup portalSecurityGroup = portalSecurityGroupMap.get(shortSecurityGroupEntry.getKey().intValue());

			if (portalSecurityGroup == null) {
				portalSecurityGroup = new PortalSecurityGroup();
				portalSecurityGroup.setSourceSystem(getPortalEntitySourceSystem(context));
				portalSecurityGroup.setSourceFkFieldId(securityGroup.getId().intValue());
			}
			portalSecurityGroup.setName(securityGroup.getName());
			portalSecurityGroup.setDescription(securityGroup.getDescription());

			List<SecurityUser> userList = getSecurityUserService().getSecurityUserListByGroup(securityGroup.getId());
			List<PortalSecurityUser> portalSecurityUserList = new ArrayList<>();
			for (SecurityUser user : CollectionUtils.getIterable(userList)) {
				if (portalSecurityUserMap.containsKey(user.getId())) {
					portalSecurityUserList.add(portalSecurityUserMap.get(user.getId()));
				}
			}

			try {
				getPortalSecurityUserGroupService().savePortalSecurityGroupFromSourceSystem(portalSecurityGroup, portalSecurityUserList);
				successCount++;
			}

			catch (Exception e) {
				failCount++;
				String message = "Error saving Portal Security Group: " + portalSecurityGroup.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e);
				LogUtils.errorOrInfo(getClass(), message, e);
				status.addError(message);
			}
		}

		// Any In Portal that Don't exist in IMS anymore - delete them
		for (Map.Entry<Integer, PortalSecurityGroup> portalSecurityGroupEntry : portalSecurityGroupMap.entrySet()) {
			if (!securityGroupMap.containsKey(portalSecurityGroupEntry.getKey().shortValue())) {
				PortalSecurityGroup portalSecurityGroup = portalSecurityGroupEntry.getValue();
				try {
					getPortalSecurityUserGroupService().deletePortalSecurityGroupFromSourceSystem(portalSecurityGroup.getId());
					deleteCount++;
				}
				catch (Exception e) {
					failCount++;
					String message = "Error deleting Portal Security Group: " + portalSecurityGroup.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e);
					LogUtils.errorOrInfo(getClass(), message, e);
					status.addError(message);
				}
			}
		}

		status.setMessageWithErrors("Updated Portal Security Groups and their memberships: " + successCount + " Successfully Inserted/Updated, " + deleteCount + " Deleted, " + failCount + " failed.", 5);
		return status;
	}


	private Map<Short, SecurityGroup> getSecurityGroupMap() {
		SecurityGroupSearchForm searchForm = new SecurityGroupSearchForm();
		searchForm.setSecurityGroupHierarchyId(getSecurityGroupTagId());
		return BeanUtils.getBeanMap(getSecurityUserService().getSecurityGroupList(searchForm), SecurityGroup::getId);
	}


	private Map<Integer, PortalSecurityGroup> getPortalSecurityGroupMap(Map<String, Object> context) {
		PortalSecurityGroupSearchForm searchForm = new PortalSecurityGroupSearchForm();
		searchForm.setSourceSystemId(getPortalEntitySourceSystem(context).getId());
		return BeanUtils.getBeanMap(getPortalSecurityUserGroupService().getPortalSecurityGroupList(searchForm), PortalSecurityGroup::getSourceFkFieldId);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////             Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getSecurityGroupTagId() {
		return this.securityGroupTagId;
	}


	public void setSecurityGroupTagId(Short securityGroupTagId) {
		this.securityGroupTagId = securityGroupTagId;
	}


	public PortalSecurityUserGroupService getPortalSecurityUserGroupService() {
		return this.portalSecurityUserGroupService;
	}


	public void setPortalSecurityUserGroupService(PortalSecurityUserGroupService portalSecurityUserGroupService) {
		this.portalSecurityUserGroupService = portalSecurityUserGroupService;
	}
}
