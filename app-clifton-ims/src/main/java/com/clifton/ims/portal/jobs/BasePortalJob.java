package com.clifton.ims.portal.jobs;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.company.contact.BusinessCompanyContactSearchForm;
import com.clifton.business.company.contact.BusinessCompanyContactService;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.PortalEntityService;
import com.clifton.portal.entity.search.PortalEntitySearchForm;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntitySetupService;
import com.clifton.portal.entity.setup.PortalEntitySourceSection;
import com.clifton.portal.entity.setup.PortalEntitySourceSystem;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.portal.entity.setup.search.PortalEntitySourceSectionSearchForm;
import com.clifton.portal.entity.setup.search.PortalEntityViewTypeSearchForm;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnDatedValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BasePortalJob</code> can be extended by all Portal related jobs to make use of the context for storing common look ups
 *
 * @author manderson
 */
public abstract class BasePortalJob implements Task {

	protected static final String PORTAL_SOURCE_SYSTEM_IMS = "IMS";

	public static final String PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP = "Organization Group";
	public static final String PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP = "Client Relationship (Organization)";
	public static final String PORTAL_SOURCE_SECTION_CLIENT = "Client (Investing Entity)";
	public static final String PORTAL_SOURCE_SECTION_CLIENT_GROUP = "Client Group";
	public static final String PORTAL_SOURCE_SECTION_COMMINGLED_VEHICLE = "Commingled Vehicle";
	public static final String PORTAL_SOURCE_SECTION_SISTER_CLIENT_GROUP = "Sister Client Group";
	public static final String PORTAL_SOURCE_SECTION_SISTER_CLIENT = "Sister Client";
	public static final String PORTAL_SOURCE_SECTION_CLIENT_ACCOUNT = "Client Account";
	public static final String PORTAL_SOURCE_SECTION_CONTACT = "Contact";
	public static final String PORTAL_SOURCE_SECTION_BILLING_INVOICE = "Management Fee Invoice";
	public static final String PORTAL_SOURCE_SECTION_INVESTMENT_TEAM = "Investment Team";
	public static final String PORTAL_SOURCE_SECTION_PERFORMANCE_COMPOSITE = "GIPS Composite";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	// Context Keys for Re-Use Through Each Job Step
	private static final String CONTEXT_KEY_PORTAL_ENTITY_SOURCE_SYSTEM_IMS = "PORTAL_ENTITY_SOURCE_SYSTEM_IMS";
	private static final String CONTEXT_KEY_PORTAL_ENTITY_SOURCE_SECTION_MAP = "PORTAL_ENTITY_SOURCE_SECTION_MAP";
	private static final String CONTEXT_KEY_PREFIX_PORTAL_ENTITY_SOURCE_MAP = "PORTAL_ENTITY_SOURCE_MAP_";
	private static final String CONTEXT_KEY_PORTAL_ENTITY_FIELD_TYPE_LIST_MAP = "PORTAL_ENTITY_FIELD_TYPE_LIST_MAP";

	private static final String CONTEXT_KEY_PORTAL_ENTITY_VIEW_TYPE_MAP = "PORTAL_ENTITY_VIEW_TYPE_MAP";

	private static final String CONTEXT_KEY_COMPANY_RELATIONSHIP_MANAGER_MAP = "COMPANY_RELATIONSHIP_MANAGER_MAP";
	private static final String CONTEXT_KEY_ACCOUNT_RELATIONSHIP_MANAGER_MAP = "ACCOUNT_RELATIONSHIP_MANAGER_MAP";

	private static final String CONTEXT_KEY_FUND_CLIENT_INVESTMENT_ACCOUNT_INVESTOR_MAP = "CONTEXT_KEY_FUND_CLIENT_INVESTMENT_ACCOUNT_INVESTOR_MAP";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private BusinessCompanyContactService businessCompanyContactService;

	private DataTableRetrievalHandler dataTableRetrievalHandler;

	private InvestmentInstrumentService investmentInstrumentService;

	private PortalEntitySetupService portalEntitySetupService;
	private PortalEntityService portalEntityService;

	private SystemColumnValueService systemColumnValueService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns IMS Source System from the Portal.  Will check the cache first if it was already retrieved otherwise will look it up in the portal
	 */
	protected PortalEntitySourceSystem getPortalEntitySourceSystem(Map<String, Object> context) {
		return (PortalEntitySourceSystem) CollectionUtils.getValue(context, CONTEXT_KEY_PORTAL_ENTITY_SOURCE_SYSTEM_IMS, () -> getPortalEntitySetupService().getPortalEntitySourceSystemByName(PORTAL_SOURCE_SYSTEM_IMS), null);
	}


	/**
	 * Returns the PortalEntityFieldType list that applies to the given source section with the given name from the Portal where source system = IMS
	 * Will check the cache first if it was already retrieved, otherwise will look it up in the portal
	 */
	@SuppressWarnings("unchecked")
	protected List<PortalEntityFieldType> getPortalEntityFieldTypeListForSourceSection(String sourceSectionName, Map<String, Object> context) {
		PortalEntitySourceSection sourceSection = getPortalEntitySourceSectionByName(sourceSectionName, context);
		Map<Short, List<PortalEntityFieldType>> entityTypeFieldTypeListMap = (Map<Short, List<PortalEntityFieldType>>) CollectionUtils.getValue(context, CONTEXT_KEY_PORTAL_ENTITY_FIELD_TYPE_LIST_MAP, () -> BeanUtils.getBeansMap(getPortalEntitySetupService().getPortalEntityFieldTypeList(), portalEntityFieldType -> portalEntityFieldType.getPortalEntityType().getId()), HashMap::new);
		return entityTypeFieldTypeListMap.get(sourceSection.getEntityType().getId());
	}


	/**
	 * Returns the source section with the given name from the Portal where source system = IMS
	 * Will check the cache first if it was already retrieved, otherwise will look it up in the portal
	 */
	@SuppressWarnings("unchecked")
	protected PortalEntitySourceSection getPortalEntitySourceSectionByName(String name, Map<String, Object> context) {
		Map<String, PortalEntitySourceSection> sourceSectionMap = (Map<String, PortalEntitySourceSection>) CollectionUtils.getValue(context, CONTEXT_KEY_PORTAL_ENTITY_SOURCE_SECTION_MAP, () -> {
			PortalEntitySourceSectionSearchForm sectionSearchForm = new PortalEntitySourceSectionSearchForm();
			sectionSearchForm.setSourceSystemId(getPortalEntitySourceSystem(context).getId());
			return BeanUtils.getBeanMap(getPortalEntitySetupService().getPortalEntitySourceSectionList(sectionSearchForm), PortalEntitySourceSection::getName);
		}, HashMap::new);
		return sourceSectionMap.get(name);
	}


	/**
	 * Returns the portal entity view type with the given name from the Portal
	 * Will check the cache first if it was already retrieved, otherwise will look it up in the portal
	 */
	@SuppressWarnings("unchecked")
	protected PortalEntityViewType getPortalEntityViewTypeByName(String name, Map<String, Object> context) {
		Map<String, PortalEntityViewType> entityViewTypeMap = (Map<String, PortalEntityViewType>) CollectionUtils.getValue(context, CONTEXT_KEY_PORTAL_ENTITY_VIEW_TYPE_MAP, () -> BeanUtils.getBeanMap(getPortalEntitySetupService().getPortalEntityViewTypeList(new PortalEntityViewTypeSearchForm(), false), PortalEntityViewType::getName), HashMap::new);
		return entityViewTypeMap.get(name);
	}


	/**
	 * Returns the PortalEntity object for the given source section and source FkFieldId. Will look it up in the cache first.
	 * When retrieving from the cache, we get a list of all portal entities for the give source section.
	 */
	protected PortalEntity getPortalEntityBySource(String sourceSectionName, Integer sourceFkFieldId, Map<String, Object> context) {
		return getPortalEntitySourceMap(sourceSectionName, context).get(sourceFkFieldId);
	}


	/**
	 * Returns a map of PortalEntity SourceFKFieldID to PortalEntity objects for a given source section.
	 * Will set the map in the cache if it doesn't exist.
	 */
	@SuppressWarnings("unchecked")
	protected Map<Integer, PortalEntity> getPortalEntitySourceMap(String sourceSectionName, Map<String, Object> context) {
		if (StringUtils.isEmpty(sourceSectionName)) {
			throw new ValidationException("Source section name is required");
		}
		String key = CONTEXT_KEY_PREFIX_PORTAL_ENTITY_SOURCE_MAP + sourceSectionName;
		return (Map<Integer, PortalEntity>) CollectionUtils.getValue(context, key, () -> {
			PortalEntitySearchForm searchForm = new PortalEntitySearchForm();
			searchForm.setIncludeDeleted(true);
			searchForm.setEntitySourceSectionId(getPortalEntitySourceSectionByName(sourceSectionName, context).getId());
			return BeanUtils.getBeanMap(getPortalEntityService().getPortalEntityList(searchForm), PortalEntity::getSourceFkFieldId);
		}, HashMap::new);
	}


	/**
	 * Clears the PortalEntity map in the context for the given source section
	 * This is used when we save the updated list of entities, we clear the cache so the next
	 * job can get the updated list
	 */
	protected void clearPortalEntitySourceMap(String sourceSectionName, Map<String, Object> context) {
		String key = CONTEXT_KEY_PREFIX_PORTAL_ENTITY_SOURCE_MAP + sourceSectionName;
		context.remove(key);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////               Relationship Manager Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * At the Account Level (Overrides) there can and only will be at most one
	 * To even have one is rare and currently only used for cases where we share with Seattle
	 */
	protected BusinessCompanyContact getRelationshipManagerOverrideForClientAccount(InvestmentAccount clientAccount, Map<String, Object> context) {
		return getInvestmentAccountRelationshipManagerOverrideMap(context).get(clientAccount.getId());
	}


	protected List<BusinessCompanyContact> getRelationshipManagerListForClient(BusinessClient client, Map<String, Object> context) {
		List<BusinessCompanyContact> companyContactList = getBusinessCompanyRelationshipManagerMap(context).get(client.getCompany().getId());
		if (CollectionUtils.isEmpty(companyContactList) && client.getParent() != null) {
			companyContactList = getBusinessCompanyRelationshipManagerMap(context).get(client.getParent().getCompany().getId());
		}
		if (CollectionUtils.isEmpty(companyContactList) && client.getClientRelationship() != null) {
			return getRelationshipManagerListForClientRelationship(client.getClientRelationship(), context);
		}
		return companyContactList;
	}


	protected List<BusinessCompanyContact> getRelationshipManagerListForClientRelationship(BusinessClientRelationship clientRelationship, Map<String, Object> context) {
		return getBusinessCompanyRelationshipManagerMap(context).get(clientRelationship.getCompany().getId());
	}


	@SuppressWarnings("unchecked")
	protected Map<Integer, List<BusinessCompanyContact>> getBusinessCompanyRelationshipManagerMap(Map<String, Object> context) {
		return (Map<Integer, List<BusinessCompanyContact>>) CollectionUtils.getValue(context, CONTEXT_KEY_COMPANY_RELATIONSHIP_MANAGER_MAP, () -> {
			BusinessCompanyContactSearchForm searchForm = new BusinessCompanyContactSearchForm();
			searchForm.setActive(true);
			searchForm.setContactTypeName(BusinessContactType.RELATIONSHIP_MANAGER_CONTACT_TYPE);
			return BeanUtils.getBeansMap(getBusinessCompanyContactService().getBusinessCompanyContactList(searchForm), businessCompanyContact -> businessCompanyContact.getReferenceOne().getId());
		}, HashMap::new);
	}


	@SuppressWarnings("unchecked")
	protected Map<Integer, BusinessCompanyContact> getInvestmentAccountRelationshipManagerOverrideMap(Map<String, Object> context) {
		return (Map<Integer, BusinessCompanyContact>) CollectionUtils.getValue(context, CONTEXT_KEY_ACCOUNT_RELATIONSHIP_MANAGER_MAP, () -> {
			// Add In Account Overrides
			SystemColumnDatedValueSearchForm datedValueSearchForm = new SystemColumnDatedValueSearchForm();
			datedValueSearchForm.setActive(true);
			datedValueSearchForm.setColumnGroupName(InvestmentAccount.CLIENT_ACCOUNT_INFO_COLUMN_GROUP_NAME);
			datedValueSearchForm.setColumnName(InvestmentAccount.CLIENT_ACCOUNT_RELATIONSHIP_MANAGER_OVERRIDE);
			List<SystemColumnDatedValue> datedValueList = getSystemColumnValueService().getSystemColumnDatedValueList(datedValueSearchForm);
			Map<Integer, BusinessCompanyContact> accountMap = new HashMap<>();
			for (SystemColumnDatedValue datedValue : CollectionUtils.getIterable(datedValueList)) {
				BusinessCompanyContact companyContact = getBusinessCompanyContactService().getBusinessCompanyContact(new Integer(datedValue.getValue()));
				if (companyContact != null) {
					accountMap.put(datedValue.getFkFieldId(), companyContact);
				}
			}
			return accountMap;
		}, HashMap::new);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                   Fund Investor Methods                   ///////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of Client Account Ids that are Fund Investors (if fundClientId is null returns all investors, otherwise just investors of specified fund)
	 * Note: Includes all accounts that were ever an investor (held a security) regardless if they have been terminated or not or are currently invested in the fund
	 * Does one database retrieval for all - then can be easily filtered
	 */
	@SuppressWarnings("unchecked")
	protected List<Integer> getFundInvestorClientAccountIds(Integer fundClientId, Map<String, Object> context) {
		Map<Integer, List<Integer>> fundInvestmentClientAccountIdMap = (Map<Integer, List<Integer>>) CollectionUtils.getValue(context, CONTEXT_KEY_FUND_CLIENT_INVESTMENT_ACCOUNT_INVESTOR_MAP, () -> {
			Map<Integer, List<Integer>> fundInvestorMap = new HashMap<>();
			SecuritySearchForm securitySearchForm = new SecuritySearchForm();
			securitySearchForm.setBusinessClientCategoryType(BusinessClientCategoryTypes.FUND);

			List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
			Map<Integer, InvestmentSecurity> securityMap = BeanUtils.getBeanMap(securityList, BaseSimpleEntity::getId);

			// Is there a better way than executing SQL?
			String securityIds = BeanUtils.getPropertyValues(securityList, "id", ",");
			DataTable result = getDataTableRetrievalHandler().findDataTable("SELECT ClientInvestmentAccountID, InvestmentSecurityID FROM AccountingTransaction WHERE InvestmentSecurityID IN (" + securityIds + ") AND IsDeleted = 0 GROUP BY ClientInvestmentAccountID, InvestmentSecurityID");

			if (result != null && result.getTotalRowCount() > 0) {
				for (int i = 0; i < result.getTotalRowCount(); i++) {
					DataRow row = result.getRow(i);
					Integer clientAccountId = (Integer) row.getValue("ClientInvestmentAccountID");
					Integer securityId = (Integer) row.getValue("InvestmentSecurityID");
					InvestmentSecurity security = securityMap.get(securityId);
					Integer securityClientId = security.getInstrument().getInvestmentAccount().getBusinessClient().getId();
					if (!fundInvestorMap.containsKey(securityClientId)) {
						fundInvestorMap.put(securityClientId, new ArrayList<>());
					}
					if (!fundInvestorMap.get(securityClientId).contains(clientAccountId)) {
						fundInvestorMap.get(securityClientId).add(clientAccountId);
					}
				}
			}
			return fundInvestorMap;
		}, HashMap::new);

		if (fundClientId != null) {
			return fundInvestmentClientAccountIdMap.get(fundClientId);
		}
		return CollectionUtils.combineCollectionOfCollections(fundInvestmentClientAccountIdMap.values());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BusinessCompanyContactService getBusinessCompanyContactService() {
		return this.businessCompanyContactService;
	}


	public void setBusinessCompanyContactService(BusinessCompanyContactService businessCompanyContactService) {
		this.businessCompanyContactService = businessCompanyContactService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public PortalEntitySetupService getPortalEntitySetupService() {
		return this.portalEntitySetupService;
	}


	public void setPortalEntitySetupService(PortalEntitySetupService portalEntitySetupService) {
		this.portalEntitySetupService = portalEntitySetupService;
	}


	public PortalEntityService getPortalEntityService() {
		return this.portalEntityService;
	}


	public void setPortalEntityService(PortalEntityService portalEntityService) {
		this.portalEntityService = portalEntityService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
