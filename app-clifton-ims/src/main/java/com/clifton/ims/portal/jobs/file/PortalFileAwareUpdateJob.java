package com.clifton.ims.portal.jobs.file;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.dataaccess.sql.SqlUpdateCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.ims.portal.jobs.BasePortalJob;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.portal.file.PortalFileService;
import com.clifton.portfolio.run.PortfolioRun;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Set;


/**
 * The <code>PortalFileAwareUpdateJob</code> re-synchronizes the posted to portal property of all PortalFileAware objects in IMS. (i.e. Portfolio Run, Billing
 * Invoice). There should not be many changes as posting/unposting from IMS directly should keep it up to date. However, manually deleted the portal file would
 * cause systems to get out of sync. Can also optionally limit to a specific min report date to limit the number of retrievals/updates.
 *
 * @author manderson
 */
public class PortalFileAwareUpdateJob extends BasePortalJob {

	private CacheHandler<Serializable, ?> cacheHandler;

	private DaoLocator daoLocator;

	private SqlHandler sqlHandler;

	private PortalFileService portalFileService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Use a positive value.  Ability to limit synchronizing to only items with a report date of last x days
	 * i.e. Daily job would only do files for past year, but on a weekly basis we might synchronize all
	 * Each Portal File Aware object must be explicitly configured to apply the report date filter
	 */
	private Integer reportDateDaysBack;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofEmptyMessage();

		Collection<ReadOnlyDAO<? extends PortalFileAware>> daoList = this.daoLocator.locateAll(PortalFileAware.class);

		for (ReadOnlyDAO<? extends PortalFileAware> dao : CollectionUtils.getIterable(daoList)) {
			// Skip DAO Configs with no Tables
			if (!(dao.getConfiguration() instanceof NoTableDAOConfig<?>)) {
				try {
					status.appendToMessage(dao.getConfiguration().getTableName() + ": " + updatePostedToPortalForDao(dao) + " changes");
				}
				catch (Exception e) {
					status.addError(dao.getConfiguration().getTableName() + " Failed: " + ExceptionUtils.getOriginalMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error updating PortalFileAware Posted Property for table " + dao.getConfiguration().getTableName(), e);
				}
			}
		}
		return status;
	}


	protected int updatePostedToPortalForDao(ReadOnlyDAO<? extends PortalFileAware> dao) {
		Date minReportDate = (getReportDateDaysBack() == null ? null : DateUtils.addDays(new Date(), -getReportDateDaysBack()));
		Set<Integer> sourceIdListWithApprovedFilesPosted = getPortalFileService().getPortalFileSourceFkFieldIdList(PORTAL_SOURCE_SYSTEM_IMS, dao.getConfiguration().getTableName(), minReportDate, true);

		DataTable dataTable = getPortalFileAwareDataTable(dao, minReportDate);
		String cacheName = dao.getConfiguration().getCacheName();
		String updateSql = "UPDATE " + dao.getConfiguration().getTableName() + " SET IsPostedToPortal = ? WHERE " + dao.getConfiguration().getPrimaryKeyColumn().getName() + " = ? ";
		int updateCount = 0;

		if (dataTable != null && dataTable.getTotalRowCount() > 0) {
			for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
				DataRow row = dataTable.getRow(i);
				Integer id = (Integer) row.getValue(0);
				boolean fileAwarePosted = (boolean) row.getValue(1);

				boolean posted = sourceIdListWithApprovedFilesPosted.contains(id);

				// If new value doesn't match, set it and update it
				if (posted != fileAwarePosted) {
					// Use direct SQL to update to increase performance than using DAOs for retrievals and updates 10+ times faster
					// Because of direct SQL, also need to remove this specific item from the cache
					// We don't clear the whole cache because there should be very few updates from maintenance perspective
					// as all posting/unposting from IMS will go through {@link PortalFileReportPostingController} which updates this field as necessary
					getSqlHandler().executeUpdate(new SqlUpdateCommand(updateSql)
							.addBooleanParameterValue(posted)
							.addIntegerParameterValue(id)
					);
					if (!StringUtils.isEmpty(cacheName)) {
						getCacheHandler().remove(cacheName, id);
					}
					updateCount++;
				}
			}
		}
		return updateCount;
	}


	private DataTable getPortalFileAwareDataTable(ReadOnlyDAO<? extends PortalFileAware> dao, Date minReportDate) {
		SqlParameterValue[] sqlParameterValues = (minReportDate == null ? null : new SqlParameterValue[]{SqlParameterValue.ofDate(minReportDate)});
		StringBuilder sql = new StringBuilder(50);
		sql.append(" SELECT ");
		sql.append(dao.getConfiguration().getPrimaryKeyColumn().getName());
		sql.append(", IsPostedToPortal FROM ");
		sql.append(dao.getConfiguration().getTableName());

		if (minReportDate != null) {
			appendReportDateToSql(dao.getConfiguration().getTableName(), sql);
		}

		return getDataTableRetrievalHandler().findDataTable(new SqlSelectCommand(sql.toString(), sqlParameterValues));
	}


	private void appendReportDateToSql(String tableName, StringBuilder sql) {
		switch (tableName) {
			case PortfolioRun.TABLE_PORTFOLIO_RUN:
				sql.append(" WHERE BalanceDate >= ? ");
				break;
			case BillingInvoice.BILLING_INVOICE_TABLE_NAME:
				sql.append(" WHERE InvoiceDate >= ?");
				break;
			case AccountingPeriodClosing.ACCOUNTING_PERIOD_CLOSING_TABLE_NAME:
			case PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT:
			case PerformanceCompositePerformance.PERFORMANCE_COMPOSITE_PERFORMANCE_TABLE_NAME:
			case PerformanceCompositeInvestmentAccountPerformance.PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_PERFORMANCE_TABLE_NAME:
				sql.append(" x INNER JOIN AccountingPeriod p ON x.AccountingPeriodID = p.AccountingPeriodID WHERE p.EndDate >= ? ");
				break;
			default:
				throw new ValidationException("Missing report date filter support for table " + tableName);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public PortalFileService getPortalFileService() {
		return this.portalFileService;
	}


	public void setPortalFileService(PortalFileService portalFileService) {
		this.portalFileService = portalFileService;
	}


	public Integer getReportDateDaysBack() {
		return this.reportDateDaysBack;
	}


	public void setReportDateDaysBack(Integer reportDateDaysBack) {
		this.reportDateDaysBack = reportDateDaysBack;
	}


	public CacheHandler<Serializable, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Serializable, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
