package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.BusinessClient;
import com.clifton.business.client.search.BusinessClientSearchForm;
import com.clifton.business.client.setup.BusinessClientCategoryTypes;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.core.util.MathUtils;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityClientCopyJob</code> is used to maintain clients in the portal
 * <p>
 * Note: Applies to BusinessClient table where ClientCategoryType = "CLIENT" only
 *
 * @author manderson
 */
public class PortalEntityClientCopyJob extends BasePortalEntityCopyJob<BusinessClient> {


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_CLIENT;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return true;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		if (sourceEntity.getClientRelationship() == null) {
			return null;
		}
		if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
			// Verify the parent is the same, if so return it - otherwise look it up
			if (PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), sourceEntity.getClientRelationship().getId())) {
				return portalEntity.getParentPortalEntity();
			}
		}
		return getPortalEntityBySource(PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP, sourceEntity.getClientRelationship().getId(), context);
	}


	@Override
	protected List<BusinessClient> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		BusinessClientSearchForm searchForm = new BusinessClientSearchForm();
		searchForm.setCategoryType(BusinessClientCategoryTypes.CLIENT);
		return getBusinessClientService().getBusinessClientList(searchForm);
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessClient sourceEntity) {
		return sourceEntity.getTerminateDate() == null;
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessClient sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getLegalLabel());
		portalEntity.setDescription(sourceEntity.getLegalLabel());
		portalEntity.setStartDate(sourceEntity.getInceptionDate());
		// Note: End Date is Set in {@link PortalEntityObserver} based on the last end date of all of it's children (if any are blank, this is blank)
	}
}
