package com.clifton.ims.portal.jobs.entity;

import com.clifton.business.client.BusinessClientRelationship;
import com.clifton.business.client.search.BusinessClientRelationshipSearchForm;
import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.portal.entity.PortalEntity;
import com.clifton.portal.entity.setup.PortalEntityFieldType;
import com.clifton.portal.entity.setup.PortalEntityViewType;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.List;
import java.util.Map;


/**
 * The <code>PortalEntityClientRelationshipCopyJob</code> is used to maintain Client Relationships in the Portal
 *
 * @author manderson
 */
public class PortalEntityClientRelationshipCopyJob extends BasePortalEntityCopyJob<BusinessClientRelationship> {

	private InvestmentAccountGroupService investmentAccountGroupService;

	private InvestmentAccountService investmentAccountService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If populated and ALL of the client accounts under this relationship are in the account group
	 * the client relationship WILL NOT be inserted into the portal.
	 * Will be used for Westport migration to temporarily exclude Westport relationships from the portal
	 */
	private Integer excludeClientAccountGroupId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getPortalEntitySourceSectionName() {
		return PORTAL_SOURCE_SECTION_CLIENT_RELATIONSHIP;
	}


	@Override
	protected boolean isParentPortalEntityRequired() {
		return false;
	}


	@Override
	protected PortalEntity getParentPortalEntityForSource(BusinessClientRelationship sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		BusinessCompany outsourcedCIO = getIdentityObjectToOrganizationGroupMap(context).get(sourceEntity);
		if (outsourcedCIO != null) {
			if (portalEntity != null && portalEntity.getParentPortalEntity() != null) {
				// Verify the parent is the same, if so return it - otherwise look it up
				if (PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP.equals(portalEntity.getParentPortalEntity().getEntitySourceSection().getName()) && MathUtils.isEqual(portalEntity.getParentPortalEntity().getSourceFkFieldId(), outsourcedCIO.getId())) {
					return portalEntity.getParentPortalEntity();
				}
			}
			return getPortalEntityBySource(PORTAL_SOURCE_SECTION_ORGANIZATION_GROUP, outsourcedCIO.getId(), context);
		}
		// Otherwise no parent
		return null;
	}


	@Override
	protected List<BusinessClientRelationship> getSourceEntityList(Map<Integer, PortalEntity> portalEntityMap, Map<String, Object> context) {
		return getBusinessClientService().getBusinessClientRelationshipList(new BusinessClientRelationshipSearchForm());
	}


	@Override
	protected boolean isSourceEntityAvailableForInsert(BusinessClientRelationship sourceEntity) {
		// Inactive Status = Workflow State of Inactive or Terminated
		// Or Pending Status - Still a Prospect and shouldn't show up in portal until client to be
		// Otherwise they are Client-To-Be (Open) or Active
		boolean insert = !(WorkflowStatus.STATUS_INACTIVE.equals(sourceEntity.getWorkflowStatus().getName())) && !(WorkflowStatus.STATUS_PENDING.equals(sourceEntity.getWorkflowStatus().getName()));
		if (insert && getExcludeClientAccountGroupId() != null) {
			// If available for insert and exclude account group is set - check if all existing client accounts under this relationship are in the exclude group
			// If so - don't insert it
			insert = !isAllClientAccountsInExcludeGroup(sourceEntity);
		}
		return insert;
	}


	private boolean isAllClientAccountsInExcludeGroup(BusinessClientRelationship clientRelationship) {
		InvestmentAccountSearchForm relationshipAccountsSearchForm = new InvestmentAccountSearchForm();
		relationshipAccountsSearchForm.setClientRelationshipId(clientRelationship.getId());
		relationshipAccountsSearchForm.setOurAccount(true);

		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(relationshipAccountsSearchForm);
		// No Accounts, then nothing to filter
		if (CollectionUtils.isEmpty(accountList)) {
			return false;
		}

		InvestmentAccountGroup group = getInvestmentAccountGroupService().getInvestmentAccountGroup(getExcludeClientAccountGroupId());
		for (InvestmentAccount account : accountList) {
			if (!getInvestmentAccountGroupService().isInvestmentAccountInGroup(account.getId(), group.getName())) {
				return false;
			}
		}
		return true;
	}


	@Override
	protected void populatePortalEntityPropertiesFromSource(BusinessClientRelationship sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		portalEntity.setName(sourceEntity.getName());
		portalEntity.setLabel(sourceEntity.getLegalLabel());
		portalEntity.setDescription(sourceEntity.getLegalLabel());
		portalEntity.setStartDate(sourceEntity.getInceptionDate());
		// Note: End Date is Set in {@link PortalEntityObserver} based on the last end date of all of it's children (if any are blank, this is blank)
	}


	@Override
	protected String getFieldValueForSourceEntity(BusinessClientRelationship sourceEntity, String fieldTypeName, Map<String, Object> context) {
		if (StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) || StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_CONTACT_TEAM_EMAIL, fieldTypeName)) {
			List<BusinessCompanyContact> companyContactList = getRelationshipManagerListForClientRelationship(sourceEntity, context);
			// If only one - set the overrides - otherwise we'll set them on the account (which is where if there is more than 1 they would be used)
			if (CollectionUtils.getSize(companyContactList) == 1) {
				return getPortalTeamOverrideEmailAddressForBusinessCompanyContact(companyContactList.get(0), StringUtils.isEqual(PortalEntityFieldType.FIELD_TYPE_SUPPORT_TEAM_EMAIL, fieldTypeName) ? BusinessCompanyContact.CUSTOM_FIELD_PORTAL_SUPPORT_TEAM_OVERRIDE : BusinessCompanyContact.CUSTOM_FIELD_PORTAL_CONTACT_TEAM_OVERRIDE, context);
			}
		}
		return null;
	}


	/**
	 * For Client Relationships - returns the corresponding Portal Entity View Type based on the Relationship Category Name
	 */
	@Override
	protected PortalEntityViewType getPortalEntityViewTypeForSource(BusinessClientRelationship sourceEntity, PortalEntity portalEntity, Map<String, Object> context) {
		if (sourceEntity != null) {
			return getPortalEntityViewTypeByName(sourceEntity.getRelationshipCategory().getName(), context);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public Integer getExcludeClientAccountGroupId() {
		return this.excludeClientAccountGroupId;
	}


	public void setExcludeClientAccountGroupId(Integer excludeClientAccountGroupId) {
		this.excludeClientAccountGroupId = excludeClientAccountGroupId;
	}
}
