package com.clifton.ims.performance.composite.performance.process.calculators.fee;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.accounting.period.search.AccountingPeriodClosingSearchForm;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingDefinitionService;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.search.BillingDefinitionSearchForm;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceDetailAccount;
import com.clifton.billing.invoice.BillingInvoiceService;
import com.clifton.billing.invoice.search.BillingInvoiceDetailAccountSearchForm;
import com.clifton.billing.invoice.search.BillingInvoiceSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.process.calculators.fee.PerformanceCompositeAccountManagementFeeOverrideCalculator;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext.RULE_MANAGEMENT_FEE_OVERRIDE_VIOLATION;


/**
 * The <code>PerformanceBillingAmountManagementFeeOverrideCalculator</code> class is an implementation of {@link PerformanceCompositeAccountManagementFeeOverrideCalculator}
 * that calculates the override value as the current projected + actual revenue from previous quarter - that quarter's projected total
 * <p>
 * Example: Jan, Feb and March all have $1,500 revenue estimates; 1Q bill is approved in April for $4,700; For April performance, we need IMS to pull the monthly revenue estimate
 * and add in the 1Q true up difference of $200 in this example.
 * <p>
 * NOTE: THIS ONLY INCLUDES SUPPORT FOR MONTHLY AND QUARTERLY INVOICES - SEMI-ANNUAL IS NOT USED AND THE FEW ANNUAL ONES WE HAVE ARE MANUAL (EXTERNALLY CALCULATED)
 * WOULD NEED ADDITIONAL LOGIC TO SUPPORT INVOICES THAT EXTEND BEYOND A QUARTER
 *
 * @author manderson
 */
public class PerformanceBillingAmountManagementFeeOverrideCalculator implements PerformanceCompositeAccountManagementFeeOverrideCalculator {

	private AccountingPeriodService accountingPeriodService;
	private BillingDefinitionService billingDefinitionService;
	private BillingInvoiceService billingInvoiceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Which invoice states are valid to pull actual revenue for
	 * At this point, once the invoice is initially approved it's OK to include
	 * THIS MUST BE A LIST OF STRING NAMES and NOT IDs because Invoices have common workflow states, but based on being in groups or not, they use different Workflows
	 */
	private List<String> includeWorkflowStateNameList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculateManagementFeeOverride(PerformanceCompositeAccountRuleEvaluatorContext config, BigDecimal managementFee) {
		config.getPerformanceCompositeInvestmentAccountPerformance().setManagementFeeOverride(null);

		// Get Actual Revenue for the Account for the Previous Quarter
		Date endDate = DateUtils.getLastDayOfPreviousQuarter(config.getAccountingPeriod().getStartDate());
		Date startDate = DateUtils.getFirstDayOfQuarter(endDate);
		BigDecimal billingAmount = getBillingAmount(config, startDate, endDate);

		// Billing Amount would return NULL if there are missing or invalid invoices because we don't want to enter bad override values if the data isn't available yet
		if (billingAmount != null) {
			// Get Projected Revenue for the Account for the Previous Quarter
			BigDecimal projectedAmount = getProjectedRevenue(config.getInvestmentAccount(), startDate, endDate);

			BigDecimal difference = MathUtils.subtract(billingAmount, projectedAmount);

			// If there is a difference to apply...
			if (!MathUtils.isNullOrZero(difference)) {
				// If first month of the quarter - then apply the difference without going negative
				if (1 == DateUtils.getMonthOfQuarter(config.getAccountingPeriod().getEndDate())) {
					BigDecimal overrideAmount = MathUtils.add(managementFee, difference);
					if (MathUtils.isLessThan(overrideAmount, BigDecimal.ZERO)) {
						overrideAmount = BigDecimal.ZERO;
					}
					config.getPerformanceCompositeInvestmentAccountPerformance().setManagementFeeOverride(overrideAmount);
				}
				// Otherwise if second or third month of the quarter we only need to apply what would have been negative on the previous month(s)
				else if (MathUtils.isLessThan(difference, BigDecimal.ZERO)) {
					// If previous override value was ZERO - then there could be a negative balance to carry over and we'll need to calculate what that should be
					PerformanceCompositeInvestmentAccountPerformance previousPerformance = config.getPreviousPerformanceCompositeInvestmentAccountPerformance();
					// Checking for less than for historical reasons - technically because of above should never be negative
					if (previousPerformance.getManagementFeeOverride() != null && MathUtils.isLessThanOrEqual(previousPerformance.getManagementFeeOverride(), BigDecimal.ZERO)) {
						// Get Projected Revenue for this Quarter Before this Period
						BigDecimal currentProjectedAmount = getProjectedRevenue(config.getInvestmentAccount(), DateUtils.getFirstDayOfQuarter(config.getAccountingPeriod().getStartDate()), DateUtils.getLastDayOfPreviousMonth(config.getAccountingPeriod().getEndDate()));
						BigDecimal overrideAmount = MathUtils.add(managementFee, MathUtils.add(currentProjectedAmount, difference));
						if (MathUtils.isLessThan(overrideAmount, BigDecimal.ZERO)) {
							overrideAmount = BigDecimal.ZERO;
						}
						config.getPerformanceCompositeInvestmentAccountPerformance().setManagementFeeOverride(overrideAmount);
					}
				}
			}
		}
	}


	/**
	 * Calculates and returns the actual billing amount for the date range.
	 * <p>
	 * Note: Will also create rule violations for cases where we are missing invoices or invoices are not in a ready state
	 * When not ready - will return null so we don't under value actual revenue
	 * This does involve additional data retrievals, however those are necessary in order to properly validate the required invoices have been generated/processed/approved
	 */
	private BigDecimal getBillingAmount(PerformanceCompositeAccountRuleEvaluatorContext config, Date startDate, Date endDate) {
		// Validate Invoices Are Available That are Needed to Calculate Amounts
		List<BillingDefinition> billingDefinitionList = getBillingDefinitionList(config, startDate, endDate);
		Set<Integer> validInvoiceIds = new HashSet<>();
		if (!CollectionUtils.isEmpty(billingDefinitionList)) {
			Map<Integer, List<BillingInvoice>> billingDefinitionInvoiceListMap = getBillingDefinitionInvoiceListMap(billingDefinitionList, startDate, endDate);

			List<String> errorList = new ArrayList<>();
			for (BillingDefinition billingDefinition : billingDefinitionList) {
				List<BillingInvoice> definitionInvoiceList = billingDefinitionInvoiceListMap.get(billingDefinition.getId());
				if (CollectionUtils.isEmpty(definitionInvoiceList)) {
					errorList.add("Missing Invoice(s) for Billing Definition [" + billingDefinition.getLabel() + "] during " + DateUtils.fromDateRange(startDate, endDate, true, true));
				}
				else if (BillingFrequencies.MONTHLY == billingDefinition.getBillingFrequency()) {
					Date invoiceStartDate = (DateUtils.isDateAfter(billingDefinition.getStartDate(), startDate) ? billingDefinition.getStartDate() : startDate);
					Date invoiceEndDate = (billingDefinition.getEndDate() != null && DateUtils.isDateAfter(endDate, billingDefinition.getEndDate())) ? DateUtils.getLastDayOfMonth(billingDefinition.getEndDate()) : endDate;
					Date monthEndDate = DateUtils.getLastDayOfMonth(invoiceStartDate);
					while (DateUtils.isDateAfter(invoiceEndDate, monthEndDate)) {
						BillingInvoice validInvoice = validateBillingDefinitionInvoice(billingDefinition, definitionInvoiceList, monthEndDate, errorList);
						if (validInvoice != null) {
							validInvoiceIds.add(validInvoice.getId());
						}
						monthEndDate = DateUtils.getLastDayOfMonth(DateUtils.addDays(monthEndDate, 1));
					}
				}
				// Quarterly
				else {
					BillingInvoice validInvoice = validateBillingDefinitionInvoice(billingDefinition, definitionInvoiceList, endDate, errorList);
					if (validInvoice != null) {
						validInvoiceIds.add(validInvoice.getId());
					}
				}
			}

			if (!CollectionUtils.isEmpty(errorList)) {
				config.addSystemDefinedRuleViolationNote(RULE_MANAGEMENT_FEE_OVERRIDE_VIOLATION, "Unable to Calculate Management Fee Override because: " + StringUtils.collectionToDelimitedString(errorList, "<br/>"));
				return null;
			}

			BillingInvoiceDetailAccountSearchForm searchForm = new BillingInvoiceDetailAccountSearchForm();
			searchForm.setInvestmentAccountId(config.getInvestmentAccount().getId());
			searchForm.setInvoiceIds(validInvoiceIds.toArray(new Integer[validInvoiceIds.size()]));
			return CoreMathUtils.sumProperty(getBillingInvoiceService().getBillingInvoiceDetailAccountList(searchForm), BillingInvoiceDetailAccount::getBillingAmount);
		}
		return null;
	}


	private List<BillingDefinition> getBillingDefinitionList(PerformanceCompositeAccountRuleEvaluatorContext config, Date startDate, Date endDate) {
		BillingDefinitionSearchForm searchForm = new BillingDefinitionSearchForm();
		searchForm.setInvestmentAccountId(config.getInvestmentAccount().getId());
		searchForm.setBillingFrequencies(new BillingFrequencies[]{BillingFrequencies.MONTHLY, BillingFrequencies.QUARTERLY});
		List<BillingDefinition> billingDefinitionList = getBillingDefinitionService().getBillingDefinitionList(searchForm);
		billingDefinitionList = BeanUtils.filter(billingDefinitionList, billingDefinition -> DateUtils.isOverlapInDates(startDate, endDate, billingDefinition.getStartDate(), billingDefinition.getEndDate()));
		if (CollectionUtils.isEmpty(billingDefinitionList)) {
			config.addSystemDefinedRuleViolationNote(RULE_MANAGEMENT_FEE_OVERRIDE_VIOLATION, "There are no active billing definitions during " + DateUtils.fromDateRange(startDate, endDate, true, true) + " associated with account [" + config.getInvestmentAccount().getLabel() + "] that apply to calculating management fee override.");
		}
		return billingDefinitionList;
	}


	private Map<Integer, List<BillingInvoice>> getBillingDefinitionInvoiceListMap(List<BillingDefinition> billingDefinitionList, Date startDate, Date endDate) {
		BillingInvoiceSearchForm invoiceSearchForm = new BillingInvoiceSearchForm();
		invoiceSearchForm.setBillingDefinitionIds(BeanUtils.getBeanIdentityArray(billingDefinitionList, Integer.class));
		invoiceSearchForm.addSearchRestriction(new SearchRestriction("invoiceDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		invoiceSearchForm.addSearchRestriction(new SearchRestriction("invoiceDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		invoiceSearchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CANCELED);

		List<BillingInvoice> invoiceList = getBillingInvoiceService().getBillingInvoiceList(invoiceSearchForm);
		return BeanUtils.getBeansMap(invoiceList, billingInvoice -> billingInvoice.getBillingDefinition().getId());
	}


	private BillingInvoice validateBillingDefinitionInvoice(BillingDefinition billingDefinition, List<BillingInvoice> definitionInvoiceList, Date invoiceDate, List<String> errorList) {
		BillingInvoice invoice = CollectionUtils.getOnlyElement(BeanUtils.filter(definitionInvoiceList, BillingInvoice::getInvoiceDate, invoiceDate));
		if (invoice == null) {
			errorList.add("Missing Invoice for Billing Definition [" + billingDefinition.getLabel() + "] on [" + DateUtils.fromDateShort(invoiceDate) + "]");
		}
		else if (!CollectionUtils.isEmpty(getIncludeWorkflowStateNameList())) {
			if (!getIncludeWorkflowStateNameList().contains(invoice.getWorkflowState().getName())) {
				errorList.add("Invoice # " + invoice.getId() + " is currently in workflow state [" + invoice.getWorkflowState().getName() + "].");
			}
		}
		return invoice;
	}

	////////////////////////////////////////////////////////////////////////////////


	private BigDecimal getProjectedRevenue(InvestmentAccount investmentAccount, Date startDate, Date endDate) {
		AccountingPeriodClosingSearchForm searchForm = new AccountingPeriodClosingSearchForm();
		searchForm.setInvestmentAccountId(investmentAccount.getId());
		searchForm.addSearchRestriction(new SearchRestriction("accountingPeriodEndDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("accountingPeriodEndDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));

		List<AccountingPeriodClosing> periodClosingList = getAccountingPeriodService().getAccountingPeriodClosingList(searchForm);
		return CoreMathUtils.sumProperty(periodClosingList, AccountingPeriodClosing::getPeriodProjectedRevenue);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public BillingDefinitionService getBillingDefinitionService() {
		return this.billingDefinitionService;
	}


	public void setBillingDefinitionService(BillingDefinitionService billingDefinitionService) {
		this.billingDefinitionService = billingDefinitionService;
	}


	public BillingInvoiceService getBillingInvoiceService() {
		return this.billingInvoiceService;
	}


	public void setBillingInvoiceService(BillingInvoiceService billingInvoiceService) {
		this.billingInvoiceService = billingInvoiceService;
	}


	public List<String> getIncludeWorkflowStateNameList() {
		return this.includeWorkflowStateNameList;
	}


	public void setIncludeWorkflowStateNameList(List<String> includeWorkflowStateNameList) {
		this.includeWorkflowStateNameList = includeWorkflowStateNameList;
	}
}
