<#-- NOTE: This has not been fully tested yet -->
DCD,19,Type,BlockID,AllocationID,Account,InstrumentType,AmountDelivered,AmountReceived,Counterparty,CurrencyDelivered,CurrencyReceived,FromCurrency,FwdRate,NonDeliveryForward,SettlementDate,SpotRate,ToCurrency,TradeDate,CounterPartyIDType,TradeTime
<#list trade_list as trade>
<#assign recordID = recordID + 1>
<#assign fromCurrency = trade.investmentSecurity.instrument.underlyingInstrument.tradingCurrency>
<#assign toCurrency = trade.investmentSecurity.instrument.tradingCurrency>
<#---------------------------------------------->
<#if trade.buy>
<#assign amountDelivered = trade.originalFace>
<#assign amountReceived = trade.accountingNotional>
<#assign currencyDelivered = fromCurrency>
<#assign currencyReceived = toCurrency>
<#else>
<#assign amountDelivered = trade.accountingNotional>
<#assign amountReceived = trade.originalFace>
<#assign currencyDelivered = toCurrency>
<#assign currencyReceived = fromCurrency>
</#if>
<#---------------------------------------------->
<#assign currencySymbol = "${trade.investmentSecurity.instrument.tradingCurrency.symbol}">
DBL,${recordID?c},NEWM,${trade.id?c},,,FXFORWARD,${amountDelivered},${amountReceived},${field_store.getBIC(trade.executingBrokerCompany)},${currencyDelivered},${currencyReceived},${fromCurrency},${trade.averageUnitPrice?string(priceFormat)},false,${trade.settlementDate?string(dateFormat)},${trade.averageUnitPrice},${toCurrency},${trade.tradeDate?string(dateFormat)},${currencySymbol},BIC,${trade.createDate?string(timeFormat)}
<#list trade.tradeFillList as fill>
<#assign recordID = recordID + 1>
DAL,${recordID?c},,${trade.id?c},${fill.id?c},${field_store.getCustodianAccountNumber(trade)},FXFORWARD,${amountDelivered},${amountReceived},,${currencyDelivered},${currencyReceived},${fromCurrency},
</#list>
</#list>
TCD,1,DataRecords
TTT,1,${recordID?c}
