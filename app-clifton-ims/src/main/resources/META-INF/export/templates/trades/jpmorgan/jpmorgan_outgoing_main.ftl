<#assign dateFormat = "yyyy-MM-dd">
<#assign timeFormat = "HH:mm:ss">
<#assign priceFormat = "########0.00######"> <#-- JPM requires limit of 8 digits after decimal.  Additionally, the price should not include comma separators. -->
<#assign recordID = 0>
FDD,
HCD,9,WSSFDLVersion,WSSMLVersion,SenderCode,ReceiverCode,SentDate,SentTime,MessageID,Environment,Direction
HHH,9,1,1.4.1,CLIFTONX,CHASGB20AXXX,${export_date?string(dateFormat)},${export_date?string(timeFormat)},${unique_export_id},${destination_environment},INBOUND
<#if trade_type == "Bonds">
<#include "jpmorgan_outgoing_bonds.ftl">
</#if>
<#if trade_type == "Currency">
<#include "jpmorgan_outgoing_currency.ftl">
</#if>
<#if trade_type == "Forwards">
<#include "jpmorgan_outgoing_forwards.ftl">
</#if>
<#if trade_type == "Futures">
<#include "jpmorgan_outgoing_futures.ftl">
</#if>
TCD,1,DataRecords
TTT,1,${recordID?c}
