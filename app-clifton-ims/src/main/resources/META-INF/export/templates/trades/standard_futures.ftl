<#assign dateFormat = "yyyyMMdd">
<#assign recordID = 0>
<#if export_generator.senderIdentifier??>${export_generator.senderIdentifier},${trade_fill_size},${generation_date?string(dateFormat)}</#if>
"TYPE","CLIENTREF","CONTRACT","MONTH","YEAR","PUTCALL","STRIKE","SIDE","QUANTITY","ACCOUNT","ORDERTYPE","PRICE","EXECBROKER","CLEARINGBROKER","RELATEDID","GROUPID","SPREADREF","MEMO","ELECTRONICEXECUTION","SYMBOLOGYTYPE","IGNOREDFIELD","PROMPTDATE","CLEARINGDATE","TFM"
<#list trade_list as trade>
	<#assign executingCompanyID = trade.executingBrokerCompany.getId()>
	<#assign clearingCompanyID = trade.holdingInvestmentAccount.issuingCompany.getId()>
	<#if executingCompanyID == clearingCompanyID>
		<#assign transaction_type = "A">
	<#elseif export_generator.isBrokerCompanyOrSibling(executingCompanyID, broker_companies)>
		<#assign transaction_type = "GO">
	<#elseif export_generator.isBrokerCompanyOrSibling(clearingCompanyID, broker_companies)>
		<#assign transaction_type = "GI">
	<#else>
		<#assign transaction_type = "F">
	</#if>
	<#assign putCall = export_generator.retrievePutCall(trade.getInvestmentSecurity())!>
	<#assign strikePrice = export_generator.retrieveStrikePrice(trade.investmentSecurity)!>
	<#if !export_generator.isTradeAllocatedViaFix(trade) || transaction_type == "GI">
		<#assign trade_file_management = "Y">
	<#else>
		<#assign trade_file_management = "N">
	</#if>
	<#list trade.tradeFillList as fill>
		<#assign recordID = recordID + 1>
"${transaction_type}","${fill.id}","${trade.investmentSecurity.symbol} ${field_store.getYellowKey(trade.investmentSecurity)}","${export_generator.getFutureMonth(trade.investmentSecurity)}","${export_generator.getFutureYear(trade.investmentSecurity)}","<#if putCall?has_content>${putCall[0..0]}</#if>","${strikePrice}","<#if trade.buy>B<#else>S</#if>","${fill.quantity?c}","${trade.holdingInvestmentAccount.number?replace("-","")}","<#-- APS -->","${fill.notionalUnitPrice}","${trade.executingBrokerCompany.abbreviation}","${trade.holdingInvestmentAccount.issuingCompany.abbreviation}","<#--GS System ref number (for allocations only)-->","<#--Group code for average pricing-->","<#--roll id (roll trades only)-->","<#-- memo -->","<#--Commission Flag -->","BID","<#--ignore col-->","<#--prompt date-->","${trade.settlementDate?string(dateFormat)}","${trade_file_management}"
	</#list>
</#list>