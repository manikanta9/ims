DCD,23,Type,BlockID,AllocationID,Account,InstrumentType,Action,CleanPrice,Counterparty,InstrumentID,InstrumentIDType,InterestAmount,PrincipalAmount,Quantity,SettlementAmount,SettlementDate,TradeCurrency,TradeDate,PriceCurrency,CounterPartyIDType,TradeTime,InterestCurrency,SettlementCurrency,PrincipalAmountCurrency
<#list trade_list as trade>
<#assign recordID = recordID + 1>
<#-- All currencies should be in the local currency -->
<#assign currencySymbol = "${trade.investmentSecurity.instrument.tradingCurrency.symbol}">
DBL,${recordID?c},NEWM,${trade.id?c},,,BOND,<#if trade.buy>BUY<#else>SELL</#if>,${trade.averageUnitPrice?string(priceFormat)},${(field_store.getBIC(trade.executingBrokerCompany))!"UNKNOWN"},${trade.investmentSecurity.symbol},CUSIP,${field_store.getPropertyForTrade(trade, "interestAmount")?string(priceFormat)},${field_store.getPropertyForTrade(trade, "principalAmount")?string(priceFormat)},${trade.originalFace?c},${field_store.getPropertyForTrade(trade, "settlementAmount")?string(priceFormat)},${trade.settlementDate?string(dateFormat)},${currencySymbol},${trade.tradeDate?string(dateFormat)},${currencySymbol},BIC,${trade.createDate?string(timeFormat)},${currencySymbol},${currencySymbol},${currencySymbol}
<#list trade.tradeFillList as fill>
<#assign recordID = recordID + 1>
DAL,${recordID?c},,${trade.id?c},${fill.id?c},${field_store.getCustodianAccountNumber(trade)},,,,,,,${field_store.getPropertyForTrade(trade, "interestAmount")?string(priceFormat)},${field_store.getPropertyForTrade(trade, "principalAmount")?string(priceFormat)},${trade.originalFace?c},${field_store.getPropertyForTrade(trade, "settlementAmount")?string(priceFormat)},,,,,,,${currencySymbol},${currencySymbol},${currencySymbol}
</#list>
</#list>
