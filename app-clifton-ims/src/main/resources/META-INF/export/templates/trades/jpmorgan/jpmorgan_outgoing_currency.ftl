DCD,19,Type,BlockID,AllocationID,Account,InstrumentType,AmountDelivered,AmountReceived,Counterparty,CurrencyDelivered,CurrencyReceived,FromCurrency,FwdRate,NonDeliveryForward,SettlementDate,SpotRate,ToCurrency,TradeDate,CounterPartyIDType,TradeTime
<#list trade_list as trade>
<#assign recordID = recordID + 1>
<#assign amountDelivered>${field_store.getPropertyForTrade(trade, "amountDelivered")}</#assign>
<#assign amountReceived>${field_store.getPropertyForTrade(trade, "amountReceived")}</#assign>
<#assign currencyDelivered>${field_store.getPropertyForTrade(trade, "currencyDelivered")}</#assign>
<#assign currencyReceived>${field_store.getPropertyForTrade(trade, "currencyReceived")}</#assign>
<#assign fromCurrency>${field_store.getPropertyForTrade(trade, "fromCurrency")}</#assign>
<#assign toCurrency>${field_store.getPropertyForTrade(trade, "toCurrency")}</#assign>
DBL,${recordID?c},NEWM,${trade.id?c},,,FXSPOT,${field_store.getPropertyForTrade(trade, "amountDelivered")?string(priceFormat)},${field_store.getPropertyForTrade(trade, "amountReceived")?string(priceFormat)},${field_store.getBIC(trade.executingBrokerCompany)},${currencyDelivered},${currencyReceived},${fromCurrency},${trade.exchangeRateToBase?c},false,${trade.settlementDate?string(dateFormat)},${trade.exchangeRateToBase?c},${toCurrency},${trade.tradeDate?string(dateFormat)},BIC,${trade.createDate?string(timeFormat)}
<#list trade.tradeFillList as fill>
<#assign recordID = recordID + 1>
DAL,${recordID?c},,${trade.id?c},${fill.id?c},${field_store.getCustodianAccountNumber(trade)},FXSPOT,${field_store.getPropertyForTrade(trade, "amountDelivered")?string(priceFormat)},${field_store.getPropertyForTrade(trade, "amountReceived")?string(priceFormat)},,${currencyDelivered},${currencyReceived},${fromCurrency},,,,,,,,
</#list>
</#list>
