DCD,17,Type,BlockID,AllocationID,Account,InstrumentType,Action,ExecutingBroker,ExpiryDate,InstrumentID,InstrumentIDType,MarketCode,Price,Quantity,TradeDate,PriceCurrency,TradeTime,ExecutingBrokerIDType
<#list trade_list as trade>
	<#assign recordID = recordID + 1>
	<#assign currencySymbol = "${trade.investmentSecurity.instrument.tradingCurrency.symbol}">
DBL,${recordID?c},NEWM,${trade.id?c},,,FUTURE,<#if trade.buy>BUY<#else>SELL</#if>,${field_store.getBIC(trade.executingBrokerCompany)},${trade.investmentSecurity.endDate?string(dateFormat)},${trade.investmentSecurity.symbol} ${field_store.getYellowKey(trade.investmentSecurity)},BBG,${(field_store.getMIC(trade.investmentSecurity.instrument.exchange))!"UNKNOWN"},${trade.averageUnitPrice?string(priceFormat)},${trade.quantityIntended?c},${trade.tradeDate?string(dateFormat)},${currencySymbol},${trade.createDate?string(timeFormat)},BIC
<#list trade.tradeFillList as fill>
<#assign recordID = recordID + 1>DAL,${recordID?c},,${trade.id?c},${fill.id?c},${field_store.getCustodianAccountNumber(trade)},,,,,,,,${fill.notionalUnitPrice?string(priceFormat)},${fill.quantity?c},,${currencySymbol},,
</#list>
</#list>