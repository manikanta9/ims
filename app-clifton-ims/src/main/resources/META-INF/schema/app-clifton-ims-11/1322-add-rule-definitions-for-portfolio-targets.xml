<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd">

	<data table="RuleDefinition">
		<value column="DefinitionName">Cash Exposure % Of Total Portfolio Value - Portfolio Targets</value>
		<value column="DefinitionDescription">Identify when effective cash exposure is above or below the desired range</value>
		<value column="DefinitionOrder">260</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Cash Exposure % of Total Portfolio Value')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="ViolationNoteTemplate"><![CDATA[Cash exposure of ${formatNumberPercent(rangeValue)} of Total Portfolio Value, is <#if lessThanRange>less than minimum ${formatNumberPercent(minAmount)}<#else>greater than maximum ${formatNumberPercent(maxAmount)}</#if>]]></value>
		<value column="MaxSnoozeDays">0</value>
		<value column="MaxAmountLabel">Maximum %</value>
		<value column="MinAmountLabel">Minimum %</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Cash Target: Balance Range - Portfolio Targets</value>
		<value column="DefinitionDescription">Identify when effective cash target balance is above or below the desired range</value>
		<value column="DefinitionOrder">270</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Cash Target Balance Range Comparison')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="ViolationNoteTemplate"><![CDATA[Cash Target Balance ${formatNumberMoney(rangeValue)} is <#if lessThanRange>less than minimum of ${formatNumberMoney(minAmount)}<#else>greater than maximum of ${formatNumberMoney(maxAmount)}</#if>]]></value>
		<value column="MaxSnoozeDays">0</value>
		<value column="MaxAmountLabel">Maximum Value</value>
		<value column="MinAmountLabel">Minimum Value</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Cash Target: Change Since Previous Day - Portfolio Targets</value>
		<value column="DefinitionDescription">Identify when the Cash Target changes significantly versus the previous day or appears for the first time</value>
		<value column="DefinitionOrder">270</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Previous Run Cash Target Value Comparison')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="ViolationNoteTemplate"><![CDATA[Cash Target Value changed ${formatNumberPercent(percentChange)}<br /><span style='padding-left:20px;'>Cash Target Value ${formatNumberMoney(runTargetValue)} vs. Previous Run Cash Target Value ${formatNumberMoney(previousRunTargetValue)}</span>]]></value>
		<value column="MaxSnoozeDays">0</value>
		<value column="MaxAmountLabel">Maximum Value</value>
		<value column="MinAmountLabel">Minimum Value</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Explicit Position Allocation: Unused - Portfolio Targets</value>
		<value column="DefinitionDescription">Generates violations when the Explicit position allocation set up for security/asset class active on run balance date, but security is not in the asset class on that run.</value>
		<value column="DefinitionOrder">600</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Unused Explicit Position Allocation')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Low')</value>
		<value column="CauseTableID" select="true">(SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentAccountAssetClassPositionAllocation')</value>
		<value column="ViolationNoteTemplate"><![CDATA[${causeEntity.label} allocation is not present:<br /><span class='violationMessage'>Security is not present on the run in that asset class<#if causeEntity.replication??> and replication</#if>.</span>]]></value>
		<value column="MaxSnoozeDays">0</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Explicit Position Allocation: Change Since Previous Day - Portfolio Targets</value>
		<value column="DefinitionDescription">Generates violations when the explicit allocations are different than they were on the previous run (and not from/to 0 quantity to prevent unnecessary clutter for rolling contracts.)</value>
		<value column="DefinitionOrder">700</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Explicit Position Allocation Changed Since Previous Day')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Low')</value>
		<value column="CauseTableID" select="true">(SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentAccountAssetClassPositionAllocation')</value>
		<value column="ViolationNoteTemplate"><![CDATA[${causeEntity.label}:<br /><span class='violationMessage'>Previous day allocation ${formatNumberMoney(previousTotalAllocated)} vs. current allocation ${formatNumberMoney(totalAllocated)}</span>]]></value>
		<value column="MaxSnoozeDays">0</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Explicit Position Allocation: Unable To FulFill - Portfolio Targets</value>
		<value column="DefinitionDescription">Generates violations when the explicit allocations doesn't match what the run was able to process for splitting.</value>
		<value column="DefinitionOrder">60</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Explicit Position Allocation Unable To Fulfill')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Low')</value>
		<value column="CauseTableID" select="true">(SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentAccountAssetClassPositionAllocation')</value>
		<value column="ViolationNoteTemplate"><![CDATA[${causeEntity.label} allocation was unable to be fulfilled:<br /><span class='violationMessage'>Actual Allocation - ${formatNumberMoney(totalActualContracts)}<br />Virtual Allocation - ${formatNumberMoney(totalVirtualContracts)}</span>]]></value>
		<value column="MaxSnoozeDays">0</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Contracts Unmatched - Portfolio Targets</value>
		<value column="DefinitionDescription">Total and Security Individual positions in Accounting doesn't match up with contracts allocated to asset class replications. Validates that all of the client positions are accounted for under the replication's Actual Contracts.</value>
		<value column="DefinitionOrder">5</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Portfolio Run Contracts Unmatched')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="CauseTableID" select="true">(SELECT SystemTableID FROM SystemTable WHERE TableName = 'PortfolioTargetAssetClassReplication')</value>
		<value column="ViolationNoteTemplate"><![CDATA[$Total Contracts: ${formatNumberMoney(totalContracts)}, PIOS Contracts: ${formatNumberMoney(piosContracts)}<br /><span class='violationMessage'><#list unmatchedContracts?keys as unmatchedContract>Security ${unmatchedContracts[unmatchedContract]["symbol"]}: Actual = ${formatNumberMoney(unmatchedContracts[unmatchedContract]["contractQuantity"])}, PIOS = <#if unmatchedContracts[unmatchedContract]["piosContractQuantity"]??>${formatNumberMoney(unmatchedContracts[unmatchedContract]["piosContractQuantity"])}<#else> Missing</#if><br /></#list></span>]]></value>
		<value column="MaxSnoozeDays">0</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Contracts Unmatched: Exclude Bonds And Currency - Portfolio Targets</value>
		<value column="DefinitionDescription">Total and Security Individual positions in Accounting doesn't match up with contracts allocated to asset class replications. Validates that all of the client positions are accounted for under the replication's Actual Contracts. This definition skips checking Bonds and CCY balances.</value>
		<value column="DefinitionOrder">0</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Portfolio Run Contracts Unmatched - Exclude Bonds and Currency')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="ViolationNoteTemplate"><![CDATA[Total Contracts: ${formatNumberMoney(totalContracts)}, PIOS Contracts: ${formatNumberMoney(piosContracts)}<br /><span class='violationMessage'><#list unmatchedContracts?keys as unmatchedContract>Security ${unmatchedContracts[unmatchedContract]["symbol"]}: Actual = ${formatNumberMoney(unmatchedContracts[unmatchedContract]["contractQuantity"])}, PIOS = <#if unmatchedContracts[unmatchedContract]["piosContractQuantity"]??>${formatNumberMoney(unmatchedContracts[unmatchedContract]["piosContractQuantity"])}<#else> Missing</#if><br /></#list></span>]]></value>
		<value column="MaxSnoozeDays">0</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Contracts Unmatched: Exclude Collateral and Currency - Portfolio Targets</value>
		<value column="DefinitionDescription">Total and Security Individual positions in Accounting doesn't match up with contracts allocated to asset class replications. Validates that all of the client positions are accounted for under the replication's Actual Contracts. This definition skips checking Collateral positions and Currency.</value>
		<value column="DefinitionOrder">0</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Portfolio Run Contracts Unmatched - Exclude Collateral and Currency')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">false</value>
		<value column="IgnoreConditionID" select="true">(SELECT SystemConditionID FROM SystemCondition WHERE SystemConditionName = 'Contracts Unmatched Ignorable For Client Accounts Not Members of Account Groups')</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="ViolationNoteTemplate"><![CDATA[Total Contracts: ${formatNumberMoney(totalContracts)}, PIOS Contracts: ${formatNumberMoney(piosContracts)}<br /><span class='violationMessage'><#list unmatchedContracts?keys as unmatchedContract>Security ${unmatchedContracts[unmatchedContract]["symbol"]}: Actual = ${formatNumberMoney(unmatchedContracts[unmatchedContract]["contractQuantity"])}, PIOS = <#if unmatchedContracts[unmatchedContract]["piosContractQuantity"]??>${formatNumberMoney(unmatchedContracts[unmatchedContract]["piosContractQuantity"])}<#else> Missing</#if><br /></#list></span>]]></value>
		<value column="MaxSnoozeDays">0</value>
	</data>


	<data table="RuleDefinition">
		<value column="DefinitionName">Synthetically Adjusted Positions Exposure Range - Portfolio Targets</value>
		<value column="DefinitionDescription">Identify when the overlay exposure in the portfolio is outside specific percentage range</value>
		<value column="DefinitionOrder">300</value>
		<value column="RuleScopeID" select="true">(SELECT RuleScopeID FROM RuleScope s INNER JOIN RuleCategory c ON s.RuleCategoryID = c.RuleCategoryID WHERE c.CategoryName = 'Portfolio Run Rules' AND ScopeName = 'Portfolio Target Client Accounts')</value>
		<value column="EvaluatorBeanID" select="true">(SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Violation: Portfolio Run Syn Adj Positions Exceeds Limit')</value>
		<value column="RuleCategoryID" select="true">(SELECT RuleCategoryID FROM RuleCategory WHERE CategoryName = 'Portfolio Run Rules')</value>
		<value column="IsIgnorable">true</value>
		<value column="IsAutomaticIgnoreOnCreate">false</value>
		<value column="IsIgnoreNoteRequired">true</value>
		<value column="SystemPriorityID" select="true">(SELECT SystemPriorityID FROM SystemPriority WHERE PriorityName = 'Medium')</value>
		<value column="ViolationNoteTemplate"><![CDATA[Total Exposure for Synthetically Adjusted Positions ${formatNumberMoney(totalSynAdjustmentValue)} is ${formatNumberPercent(rangeValue)} of the total portfolio value ${linkedEntity.portfolioTotalValue}<br /><span class='violationMessage'>${formatNumberPercent(rangeValue)} is <#if lessThanRange>less than minimum of ${formatNumberPercent(minAmount)}<#else>greater than maximum of ${formatNumberPercent(maxAmount)}</#if></span>]]></value>
		<value column="MaxSnoozeDays">0</value>
		<value column="CauseTableID" select="true">(SELECT SystemTableID FROM SystemTable WHERE TableName = 'PortfolioTargetAssetClassReplication')</value>
		<value column="IsNameSystemDefined">true</value>
		<value column="MaxAmountLabel">Maximum (%)</value>
		<value column="MinAmountLabel">Minimum (%)</value>
	</data>


	<sql runWith="PRE_DDL">
		<statement>
			UPDATE SystemBeanType SET ClassName = 'com.clifton.portfolio.run.rule.PortfolioRunAssetClassPositionAllocationRuleEvaluator' WHERE ClassName = 'com.clifton.product.overlay.rule.ProductOverlayAssetClassPositionAllocationRuleEvaluator';
			UPDATE SystemBeanType SET ClassName = 'com.clifton.portfolio.run.rule.PortfolioRunUnmatchedContractsRuleEvaluator' WHERE ClassName = 'com.clifton.product.overlay.rule.ProductOverlayUnmatchedContractsRuleEvaluator';
			UPDATE SystemBeanType SET ClassName = 'com.clifton.portfolio.run.rule.PortfolioRunSyntheticAdjustmentRangeRuleEvaluator' WHERE ClassName = 'com.clifton.product.overlay.rule.ProductOverlaySyntheticAdjustmentRangeRuleEvaluator';
		</statement>
	</sql>

</migrations>
