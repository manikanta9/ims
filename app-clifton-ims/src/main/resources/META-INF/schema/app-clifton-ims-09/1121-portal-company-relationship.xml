<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd"
			conditionalSql="(SELECT 1 WHERE NOT EXISTS (SELECT * FROM SystemListItem i INNER JOIN SystemList l ON i.SystemListID = l.SystemListID WHERE l.SystemListName = 'Company Relationship Types' AND i.SystemListItemValue = 'Portal Relationship'))">


	<data table="SystemListItem">
		<value column="SystemListID" select="true">(SELECT SystemListID FROM SystemList WHERE SystemListName = 'Company Relationship Types')</value>
		<value column="SystemListItemValue">Portal Relationship</value>
		<value column="SystemListItemText">Portal Relationship</value>
		<value column="SystemListItemTooltip">Relationship used to create a parent entity on the portal.  The most common use case is an OCIO relationship, where the OCIO has control over its child relationships.  Users can be assigned to the parent, and would automatically inherit the child relationships, clients, and accounts.  These should be added with caution as they can affect users file access to their children.  The parent can apply to Client Relationships and Sister Client Groups or Sister Clients (no current parent applied).</value>
		<value column="IsActive">true</value>
	</data>

	<!-- Copy OCIO Company Relationships to Portal Relationships -->
	<sql>
		<statement>
			DECLARE @OcioTable TABLE (ClientRelationshipID INT, RelationshipName NVARCHAR(500), RelationshipCompanyID INT, RelatedCompanyID INT, RelatedCompanyName NVARCHAR(500))

			INSERT INTO @OcioTable (ClientRelationshipID, RelationshipName, RelationshipCompanyID, RelatedCompanyID, RelatedCompanyName)
			SELECT clr.BusinessClientRelationshipID
					, clr.RelationshipName
					, clr.BusinessCompanyID "RelationshipCompanyID"
					, CASE WHEN mct.CompanyTypeName = 'Client Relationship' THEN rc.BusinessCompanyID ELSE mc.BusinessCompanyID END "RelatedCompanyID"
					, CASE WHEN mct.CompanyTypeName = 'Client Relationship' THEN rc.CompanyName ELSE mc.CompanyName END "RelatedCompanyName"
			FROM BusinessCompanyRelationship r
				INNER JOIN BusinessCompany mc ON r.FirstBusinessCompanyID = mc.BusinessCompanyID
				INNER JOIN BusinessCompanyType mct ON mc.BusinessCompanyTypeID = mct.BusinessCompanyTypeID
				INNER JOIN BusinessCompany rc ON r.SecondBusinessCompanyID = rc.BusinessCompanyID
				INNER JOIN BusinessCompanyType rct ON rc.BusinessCompanyTypeID = rct.BusinessCompanyTypeID
				INNER JOIN BusinessClientRelationship clr ON clr.BusinessCompanyID = CASE WHEN mct.CompanyTypeName = 'Client Relationship' THEN mc.BusinessCompanyID WHEN rct.CompanyTypeName = 'Client Relationship' THEN rc.BusinessCompanyID ELSE NULL END
				INNER JOIN WorkflowState ws ON clr.WorkflowStateID = ws.WorkflowStateID
			WHERE CompanyRelationshipName = 'Outsourced CIO'
			AND ws.WorkflowStateName = 'Active'
			AND (r.StartDate IS NULL OR r.StartDate &lt;= GETDATE())
			AND (r.EndDate IS NULL OR r.EndDate &gt;= GETDATE())

			INSERT INTO @OcioTable (ClientRelationshipID, RelationshipName, RelationshipCompanyID, RelatedCompanyID, RelatedCompanyName)
			SELECT clr.BusinessClientRelationshipID
					, clr.RelationshipName
					, clr.BusinessCompanyID "RelationshipCompanyID"
					, CASE WHEN mct.CompanyTypeName = 'Client' THEN rc.BusinessCompanyID ELSE mc.BusinessCompanyID END "RelatedCompanyID"
					, CASE WHEN mct.CompanyTypeName = 'Client' THEN rc.CompanyName ELSE mc.CompanyName END "RelatedCompanyName"
			FROM BusinessCompanyRelationship r
				INNER JOIN BusinessCompany mc ON r.FirstBusinessCompanyID = mc.BusinessCompanyID
				INNER JOIN BusinessCompanyType mct ON mc.BusinessCompanyTypeID = mct.BusinessCompanyTypeID
				INNER JOIN BusinessCompany rc ON r.SecondBusinessCompanyID = rc.BusinessCompanyID
				INNER JOIN BusinessCompanyType rct ON rc.BusinessCompanyTypeID = rct.BusinessCompanyTypeID
				INNER JOIN BusinessClient cl ON cl.BusinessCompanyID = CASE WHEN mct.CompanyTypeName = 'Client' THEN mc.BusinessCompanyID WHEN rct.CompanyTypeName = 'Client' THEN rc.BusinessCompanyID ELSE NULL END
				INNER JOIN BusinessClientRelationship clr ON cl.BusinessClientRelationshipID = clr.BusinessClientRelationshipID
				INNER JOIN WorkflowState ws ON clr.WorkflowStateID = ws.WorkflowStateID
			WHERE CompanyRelationshipName = 'Outsourced CIO'
			AND ws.WorkflowStateName = 'Active'
			AND (r.StartDate IS NULL OR r.StartDate &lt;= GETDATE())
			AND (r.EndDate IS NULL OR r.EndDate &gt;= GETDATE())

			-- IF THE COMPANY DEFINED IS A PARTICULAR BRANCH - SET IT TO THE ROOT PARENT
			-- ASSUMES NO MORE THAN 3 LEVELS DEEP
			UPDATE t
				SET RelatedCompanyID = COALESCE(pc.ParentBusinessCompanyID, c.ParentBusinessCompanyID, c.BusinessCompanyID)
			FROM @OcioTable t
			INNER JOIN BusinessCompany c ON t.RelatedCompanyID = c.BusinessCompanyID
			LEFT JOIN BusinessCompany pc ON c.ParentBusinessCompanyID = pc.BusinessCompanyID



			INSERT INTO BusinessCompanyRelationship (FirstBusinessCompanyID, SecondBusinessCompanyID, CompanyRelationshipName, StartDate, EndDate, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT t.RelationshipCompanyID
					, MIN(t.RelatedCompanyID)
					, 'Portal Relationship'
					, NULL, NULL, 0, GETDATE(), 0, GETDATE()
			FROM @OcioTable t
			GROUP BY t.RelationshipCompanyID
		</statement>
	</sql>


	<!-- rename batch job -->
	<sql>
		<statement>
			UPDATE SystemBeanType
				SET SystemBeanTypeName = 'Portal Entity: Organization Group Copy Job'
					, ClassName = 'com.clifton.ims.portal.jobs.entity.PortalEntityOrganizationGroupCopyJob'
					, SystemBeanTypeDescription = 'Copies Organization Group company changes to Portal Entities. The companies that are copied are based off of company relationship types of "Portal Relationship"'
			WHERE SystemBeanTypeName = 'Portal Entity: Outsourced CIO Copy Job'

			UPDATE SystemBean
				SET SystemBeanName = 'Portal Entity: Organization Group Copy Job'
					, SystemBeanDescription = 'Copies Organization Group company changes to Portal Entities. The companies that are copied are based off of company relationship types of "Portal Relationship"'
			WHERE SystemBeanName = 'Portal Entity: Outsourced CIO Copy Job'

			UPDATE BatchJob
				SET JobName = 'Copy Organization Group company changes'
				, JobDescription = 'Copies Organization Group companies to Portal Entities. These are created based on Portal Relationship type company relationships.  Where the Main company is the Client Relationship, Sister Client, or Sister Client Group, the related company is the parent.  Mostly used for OCIO relationship.'
			WHERE JobName = 'Copy Outsourced CIO company changes'
		</statement>
		<statement>
			-- REVERSE ORDER SO CLIENT GROUPS ARE CREATED BEFORE THE ACCOUNTS
			UPDATE BatchJob SET BatchJobOrder = 50 WHERE JobName = 'Copy Client Group Changes'
			UPDATE BatchJob SET BatchJobOrder = 60 WHERE JobName = 'Copy Client Account Changes'
		</statement>
	</sql>

</migrations>

