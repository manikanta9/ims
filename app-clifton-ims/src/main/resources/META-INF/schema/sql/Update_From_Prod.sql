/**
EXEC master.dbo.sp_addlinkedserver @server = N'TCGIMSSQL', @srvproduct=N'SQL Server'
USE [master]
GO
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname = N'TCGIMSSQL', @locallogin = NULL , @useself = N'False', @rmtuser = N'cliftonUser', @rmtpassword = N'cl1ft0nPr0d'
GO
**/

-- Last Trade Price
INSERT INTO MarketDataValue (MarketDataFieldID, MarketDataSourceID, InvestmentSecurityID, MeasureDate, MeasureTime, MeasureValue, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT v.MarketDataFieldID, v.MarketDataSourceID, v.InvestmentSecurityID, v.MeasureDate, v.MeasureTime, v.MeasureValue, v.CreateUserID, v.CreateDate, v.UpdateUserID, v.UpdateDate
FROM TCGIMSSQL.CliftonIMS.dbo.MarketDataValue v
INNER JOIN TCGIMSSQL.CliftonIMS.dbo.MarketDataField f ON v.MarketDataFieldID = f.MarketDataFieldID and f.DataFieldName = 'Last Trade Price'
INNER JOIN InvestmentSecurity s ON v.InvestmentSecurityID = s.InvestmentSecurityID
WHERE v.MeasureDate > '12/21/2010'

