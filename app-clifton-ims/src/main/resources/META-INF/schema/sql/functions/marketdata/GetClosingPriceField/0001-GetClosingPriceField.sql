USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [MarketData].[GetClosingPriceField]    Script Date: 3/31/2016 9:31:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





/**
  * Returns the Closing Price Market Data Field mapped to the Security.
  * Checks Instrument first, if none, then checks Hierarchy, if still none, then returns Last Trade Price
  */
CREATE FUNCTION [MarketData].[GetClosingPriceField]	(

	@InvestmentSecurityID INT

) RETURNS INT AS


BEGIN

DECLARE
	  @Result INT

	SELECT @Result = COALESCE(im.ClosingPriceMarketDataFieldID, hm.ClosingPriceMarketDataFieldID)
		FROM InvestmentSecurity s
			INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID
			LEFT JOIN MarketDataFieldMapping im ON i.InvestmentInstrumentID = im.InvestmentInstrumentID
			LEFT JOIN MarketDataFieldMapping hm ON i.InvestmentInstrumentHierarchyID = hm.InvestmentInstrumentHierarchyID
			WHERE s.InvestmentSecurityID = @InvestmentSecurityID --AND im.IsInstrumentExcludedFromDataSource = 0 AND hm.IsInstrumentExcludedFromDataSource = 0
	IF (@Result IS NULL)
	BEGIN
		SELECT @Result = d.MarketDataFieldID FROM MarketDataField d WHERE d.DataFieldName = 'Last Trade Price'
	END

RETURN

	@Result

END






GO


