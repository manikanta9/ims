USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [Accounting].[GetClientAccountCalculatedAUM]    Script Date: 12/22/2015 2:06:13 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER FUNCTION [Accounting].[GetClientAccountCalculatedAUM](@Date DATE, @ClientAccountID INT)
	RETURNS @Result TABLE(InvestmentAccountID INT, BaseCurrencyID INT, AUMType NVARCHAR(100), MasterAssetClassName NVARCHAR(100), CalculatedAUM DECIMAL(19,2), CalculatedAUMUSD DECIMAL(19,2), FXRateToUSD DECIMAL(28,16), AUMNotes NVARCHAR(500), IsIncludeShort BIT)
BEGIN
	 /**
	  * Returns table of Active Client Account (where the Client falls under "CLIENT" Category only - i.e. no Funds, Client Groups, Virtual Clients
	  * with the calculated AUM based on the account's override or service aum calculation type
	  * Optional Client Account ID can be passed to limit results to a specific account - used to find daily values for an account across a date range
	  *
	  * CURRENTLY USED BY QUERY EXPORTS ONLY:
	  ***   Client Account AUM Based on Business Service (Requested By Accounting/John)
	  ***   Client Account Asset Class AUM Based on Business Service (Requested By Marketing/Ashley/Alyssa) Where asset class returned is the master asset class
	  ***   EV: Client Account Calculated AUM with Daily Change (Eaton Vance)
	 **/

DECLARE @LastBusinessDay DATE
SET @LastBusinessDay = Calendar.GetBusinessDayFrom(DATEADD(day,1,@Date),NULL, -1)


IF (DATEPART(dw, @Date) IN (1, 7))
BEGIN
	IF (CliftonDW.calendar.GetPreviousMonthEnd(DATEADD(DAY, 1, @Date)) != @Date)
	BEGIN
		SET @Date = Calendar.GetPreviousWeekday(@Date)
	END
END

DECLARE @ActiveStateID INT
SELECT @ActiveStateID = WorkflowStateID FROM WorkflowState s INNER JOIN Workflow w ON s.WorkflowID = w.WorkflowID WHERE s.WorkflowStateName = 'Active' AND w.WorkflowName = 'Investment Account Status - Clifton Accounts'

DECLARE @Options NVARCHAR(50) = 'Options Notional'
DECLARE @OptionsPutOrCall NVARCHAR(50) = 'Options Notional (Put or Call)'
DECLARE @MarketValue NVARCHAR(50) = 'Market Value'
DECLARE @DurationAdjNotional NVARCHAR(50) = 'Duration Adjusted Notional'
DECLARE @Notional NVARCHAR(50) = 'Notional'
DECLARE @PositionMarketValue NVARCHAR(50) = 'Position Market Value'
DECLARE @SynExp NVARCHAR(50) = 'Synthetic Exposure'
DECLARE @SynExpFutures NVARCHAR(50) = 'Synthetic Exposure (Futures Only)'
DECLARE @SynExpFuturesSwapAdjNot NVARCHAR(50) = 'Syn. Exposure (Futures Only) + Swap Adj. Notional'
DECLARE @SwapAdjNotional NVARCHAR(50) = 'Swap Adj. Notional'
DECLARE @ManagerCash NVARCHAR(50) = 'Manager Cash' -- Selected for specific accounts - not by service type
DECLARE @ManagerBalance NVARCHAR(50) = 'Manager Balance' -- Selected for specific accounts - not by service type, uses actual manager balances, not expected to have runs attached
DECLARE @LDI NVARCHAR(50) = 'LDI Overlay Balance'
DECLARE @OverlayTarget NVARCHAR(50) = 'Overlay Target'
DECLARE @NotionalFuturesElseMarketValue NVARCHAR(50) = 'Notional (Futures) + Market Value (Non-Futures)'

DECLARE @USD INT, @DataSourceID INT
SELECT @USD = InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'USD' AND IsCurrency = 1
SELECT @DataSourceID = MarketDataSourceID FROM MarketDataSource WHERE DataSourceName = 'Goldman Sachs'

DECLARE @AccountTable TABLE (InvestmentAccountID INT, PortfolioRunID INT NULL, AUMType NVARCHAR(100), AUMNotes NVARCHAR(500), BaseCurrencyID INT NULL, FXRate DECIMAL(28,16) NULL, DefaultAssetClassName NVARCHAR(100))
INSERT INTO @AccountTable (InvestmentAccountID, AUMType, BaseCurrencyID, DefaultAssetClassName, FXRate, PortfolioRunID)
	SELECT a.InvestmentAccountID
		, COALESCE(a.AUMCalculationTypeOverride, s.AUMCalculationType)
		, a.BaseCurrencyID
		, CASE WHEN s.ServiceName IN ('Equal Sector Fund LLC','Commodities Fund LP (50/50)','Equal Sector with TIPS SA','Parametric - Structured Commodities Strategy MF')
				THEN 'Commodities'
			WHEN s.ServiceName = 'DE (ESP) Enhanced Large Cap' OR st.ServiceTypeName IN ('Structured Options', 'Hedging')
				THEN 'Options'
			WHEN s.ServiceName IN ('Enhanced Core Fixed Income', 'Cash Management', 'Passive Index Mgmt of Treasury Securities (TIPS)', 'Passive Index Mgmt of Treasury Securities', 'Enhanced Intermediate Govt Credit Fixed Income')
				THEN 'Fixed Income'
			WHEN s.ServiceName IN ('Enhanced U.S. Large Cap','Enhanced U.S. Small Cap')
				THEN 'U.S. Equity'
			WHEN s.ServiceName IN ('Parametric - Structured EM Currency Strategy ','Global Balanced Risk - PBR Mutual Fund','Global Balanced Risk Contraction Portfolio','Global Balanced Risk Fund Ltd ', 'Legacy Assets')
				THEN 'Other'
			ELSE 'Unmapped' END
		, CASE WHEN a.BaseCurrencyID = @USD THEN 1 ELSE
				(SELECT TOP 1 e.ExchangeRate
				FROM MarketDataExchangeRate e
				WHERE e.MarketDataSourceID = @DataSourceID
				AND e.RateDate <= @Date
				AND e.RateDate >= @LastBusinessDay
				AND e.FromCurrencySecurityID = a.BaseCurrencyID
				AND e.ToCurrencySecurityID = @USD
				ORDER BY e.RateDate DESC) END
		, COALESCE((SELECT r.PortfolioRunID FROM PortfolioRun r
					INNER JOIN RuleViolationStatus ws ON r.RuleViolationStatusID = ws.RuleViolationStatusID
					WHERE a.InvestmentAccountID = r.ClientInvestmentAccountID
					AND r.IsMainRun = 1
					AND ws.IsReadyForUse = 1
					AND r.BalanceDate = @LastBusinessDay
					AND COALESCE(a.AUMCalculationTypeOverride, s.AUMCalculationType) IN (@SynExp, @ManagerCash, @OverlayTarget, @SynExpFutures, @SynExpFuturesSwapAdjNot)
					AND EXISTS (SELECT TOP 1 * FROM ProductOverlayAssetClass ac WHERE ac.ProductOverlayRunID = r.PortfolioRunID)),NULL)
	FROM InvestmentAccount a
		INNER JOIN InvestmentAccountType t ON a.InvestmentAccountTypeID = t.InvestmentAccountTypeID
		INNER JOIN BusinessClient cl ON a.BusinessClientID = cl.BusinessClientID
		INNER JOIN BusinessClientCategory cc ON cl.BusinessClientCategoryID = cc.BusinessClientCategoryID
		INNER JOIN BusinessService s ON a.BusinessServiceID = s.BusinessServiceID
		LEFT JOIN BusinessServiceType st ON s.BusinessServiceTypeID = st.BusinessServiceTypeID
	WHERE t.IsOurAccount = 1
		AND cc.CategoryType IN ('CLIENT', 'SISTER_CLIENT')
		AND EXISTS (SELECT wh.WorkflowHistoryID FROM WorkflowHistory wh
			WHERE wh.EndWorkflowStateID = @ActiveStateID
			AND ((wh.EffectiveEndStateEndDate IS NULL AND @Date >= CAST(EffectiveEndStatestartDate AS DATE)) OR @Date BETWEEN CAST(wh.EffectiveEndStateStartDate AS DATE) AND CAST(wh.EffectiveEndStateEndDate AS DATE))
			AND wh.FKFieldID = a.InvestmentAccountID)
		AND (@ClientAccountID IS NULL OR @ClientAccountID = a.InvestmentAccountID)

UPDATE a
SET PortfolioRunID = r.PortfolioRunID
, AUMNotes = '* SubAccount from PIOS Main Run [' + pa.AccountNumber + ']'
FROM @AccountTable a
INNER JOIN ProductOverlayAssetClass ac ON ac.PositionInvestmentAccountID = a.InvestmentAccountID
INNER JOIN PortfolioRun r ON ac.ProductOverlayRunID = r.PortfolioRunID
INNER JOIN InvestmentAccount pa ON r.ClientInvestmentAccountID = pa.InvestmentAccountID
INNER JOIN RuleViolationStatus ws ON r.RuleViolationStatusID = ws.RuleViolationStatusID
WHERE a.PortfolioRunID IS NULL
AND r.IsMainRun = 1
AND ws.IsReadyForUse = 1
AND r.BalanceDate = @LastBusinessDay
AND a.AUMType IN (@SynExp, @ManagerCash, @OverlayTarget, @SynExpFutures, @SynExpFuturesSwapAdjNot)


UPDATE a
SET AUMNotes = CASE WHEN EXISTS (SELECT * FROM PortfolioRun r WHERE r.ClientInvestmentAccountID = a.InvestmentAccountID AND r.BalanceDate = @LastBusinessDay)
	THEN '* Missing PIOS Main Run on ' + CONVERT(VARCHAR, @LastBusinessDay, 1)
	ELSE '* No PIOS Run on ' + CONVERT(VARCHAR, @LastBusinessDay, 1) END
FROM @AccountTable a
WHERE a.PortfolioRunID IS NULL
AND a.AUMType IN (@SynExp, @ManagerCash, @OverlayTarget, @SynExpFutures, @SynExpFuturesSwapAdjNot)

DECLARE @AccountAUMTable TABLE(InvestmentAccountID INT, AUMNotes NVARCHAR(500), InvestmentAssetClassID INT NULL, CalculatedAUM DECIMAL(19,2), IsIncludeShort BIT)

INSERT INTO @AccountAUMTable (InvestmentAccountID, InvestmentAssetClassID, CalculatedAUM)
SELECT a.InvestmentAccountID
	, iac.InvestmentAssetClassID
	, SUM(COALESCE(m.CashAllocation,0))
FROM @AccountTable a
INNER JOIN PortfolioRun r ON a.PortfolioRunID = r.PortfolioRunID
INNER JOIN ProductOverlayManagerAccount m ON r.PortfolioRunID = m.ProductOverlayRunID
INNER JOIN ProductOverlayAssetClass ac ON m.ProductOverlayRunID = ac.ProductOverlayRunID AND m.InvestmentAccountAssetClassID = ac.InvestmentAccountAssetClassID AND COALESCE(ac.PositionInvestmentAccountID, r.ClientInvestmentAccountID) = a.InvestmentAccountID
INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
WHERE m.IsRollupAssetClass = 0
AND a.AUMType = @ManagerCash
GROUP BY a.InvestmentAccountID, iac.InvestmentAssetClassID


INSERT INTO @AccountAUMTable (InvestmentAccountID, InvestmentAssetClassID, CalculatedAUM, AUMNotes, IsIncludeShort)
SELECT x.InvestmentAccountID
	, x.InvestmentAssetClassID
	, x.AUM
	, x.AUMNotes
	, x.IsShort
FROM (
	SELECT a.InvestmentAccountID
		, iac.InvestmentAssetClassID
		, CASE WHEN a.AUMType = @OverlayTarget THEN SUM(ABS(coalesce(rep.TargetExposureAdjusted,0)))
			ELSE (SUM(ABS(COALESCE(rep.CurrencyOtherBaseAmount,0) + COALESCE((rep.CurrencyExchangeRate * (COALESCE(rep.CurrencyActualLocalAmount,0) + COALESCE(rep.CurrencyUnrealizedLocalAmount,0))),0))))
			+ (SUM(ABS(rep.ActualContracts * rep.ContractValue))) END "AUM"
		, CASE WHEN a.AUMType = @OverlayTarget THEN NULL WHEN MAX(coalesce(rep.VirtualContracts,0)) != 0 THEN '* Has Virtual Contracts' ELSE NULL END "AUMNotes"
		, CASE WHEN a.AUMType = @OverlayTarget THEN 0 WHEN MIN(rep.ActualContracts) < 0 THEN 1 ELSE 0 END "IsShort"
	FROM @AccountTable a
	INNER JOIN PortfolioRun r ON a.PortfolioRunID = r.PortfolioRunID
	INNER JOIN ProductOverlayAssetClass ac ON r.PortfolioRunID = ac.ProductOverlayRunID
	INNER JOIN ProductOverlayAssetClassReplication rep ON ac.ProductOverlayAssetClassID = rep.ProductOverlayAssetClassID
	INNER JOIN InvestmentSecurity s ON rep.InvestmentSecurityID = s.InvestmentSecurityID
	INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID
	INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID
	INNER JOIN InvestmentType t ON h.InvestmentTypeID = t.InvestmentTypeID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	WHERE a.AUMType IN (@OverlayTarget, @SynExp, @SynExpFutures, @SynExpFuturesSwapAdjNot)
		AND ac.IsRollupAssetClass = 0
		AND COALESCE(ac.PositionInvestmentAccountID, r.ClientInvestmentAccountID) = a.InvestmentAccountID
		AND (ac.PrimaryInvestmentReplicationID = rep.InvestmentReplicationID OR (ac.SecondaryInvestmentReplicationID IS NOT NULL AND ac.SecondaryInvestmentReplicationID = rep.InvestmentReplicationID))
		AND rep.InvestmentSecurityID != a.BaseCurrencyID
		AND (t.InvestmentTypeName = 'Futures' OR a.AUMType NOT IN (@SynExpFutures, @SynExpFuturesSwapAdjNot))
	GROUP BY a.InvestmentAccountID, iac.InvestmentAssetClassID, a.AUMType
) x
WHERE x.AUM != 0
OPTION (RECOMPILE)

/**
 * Note: Recompile Option was added for 2014 Compatibility because table variables when created assume 1 value
 * when a lot of values are in the table the path SQL Server takes is much different and was causing this to run
 * in a minute vs. a second
 **/



INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM)
SELECT a.InvestmentAccountID
	, (
	SELECT ABS(SUM(b.AdjustedCashValue + b.AdjustedSecuritiesValue)) "ManagerBalance"
	FROM InvestmentManagerAccountBalance b
	INNER JOIN InvestmentManagerAccount m ON b.InvestmentManagerAccountID = m.InvestmentManagerAccountID
	WHERE b.BalanceDate = @LastBusinessDay
	AND m.IsInactive = 0
	AND m.InvestmentManagerAccountID IN (SELECT mas.InvestmentManagerAccountID FROM InvestmentManagerAccountAssignment mas WHERE mas.InvestmentAccountID = a.InvestmentAccountID AND mas.IsInactive = 0))
FROM @AccountTable a
WHERE a.AUMType = @ManagerBalance

DECLARE @SecurityTableID INT
SELECT @SecurityTableID = SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurity'

INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM, IsIncludeShort)
	SELECT
		ps.InvestmentAccountID
		, CASE WHEN ps.AUMType = @OptionsPutOrCall THEN MAX(ps.OptionNotional) ELSE SUM(ps.OptionNotional) END "OptionNotional"
		, MAX(ps.IsShort) "IsShort"
	FROM (
		SELECT a.InvestmentAccountID
		, a.AUMType
		, cv.ColumnValue "PutOrCall"
		, ABS(SUM(p.Quantity * sd.PriceMultiplier * mv.MeasureValue)) "OptionNotional"
		, MAX(CASE WHEN p.Quantity < 0 THEN 1 ELSE 0 END) "IsShort"
		FROM @AccountTable a
			INNER JOIN CliftonDW.dbo.AccountingPositionSnapshot p ON a.InvestmentAccountID = p.ClientInvestmentAccountID
			INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
			INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension sd ON p.InvestmentSecurityID = sd.InvestmentSecurityID
			INNER JOIN InvestmentSecurity us ON sd.UnderlyingSecurityID = us.InvestmentSecurityID
			INNER JOIN MarketDataValue mv ON mv.InvestmentSecurityID = COALESCE(us.BigInvestmentSecurityID, us.InvestmentSecurityID) AND mv.MeasureDate = @LastBusinessDay
			INNER JOIN MarketDataField f ON mv.MarketDataFieldID = f.MarketDataFieldID AND f.DataFieldName IN ('Settlement Price', 'Last Trade Price')
			INNER JOIN SystemColumn c ON c.SystemTableID = @SecurityTableID AND c.ColumnName = 'Put or Call'
			INNER JOIN SystemColumnValue cv ON c.SystemColumnID = cv.SystemColumnID AND cv.FKFieldID = sd.InvestmentSecurityID
		WHERE a.AUMType IN (@Options, @OptionsPutOrCall)
			AND aa.IsCollateral = 0
			AND p.SnapshotDate = @LastBusinessDay
			AND sd.InvestmentTypeName = 'Options'
			GROUP BY a.InvestmentAccountID, cv.ColumnValue, a.AUMType
	) ps
	GROUP BY ps.InvestmentAccountID, ps.AUMType


INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM, IsIncludeShort)
SELECT a.InvestmentAccountID
	, SUM(ABS(p.BaseNotional))
	, MAX(CASE WHEN p.Quantity < 0 THEN 1 ELSE 0 END)
FROM @AccountTable a
INNER JOIN CliftonDW.dbo.AccountingPositionSnapshot p ON a.InvestmentAccountID = p.ClientInvestmentAccountID
INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension sd ON p.InvestmentSecurityID = sd.InvestmentSecurityID
WHERE p.SnapshotDate = @Date
AND aa.IsCollateral = 0
AND (a.AUMType = @Notional
	OR (a.AUMType = @NotionalFuturesElseMarketValue AND sd.InvestmentTypeName = 'Futures'))
GROUP BY a.InvestmentAccountID


INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM, IsIncludeShort)
SELECT a.InvestmentAccountID
	, SUM(ABS(CASE WHEN COALESCE(bmv.BenchmarkDuration,0) = 0 THEN 0 ELSE p.BaseNotional * (mv.Duration/ bmv.BenchmarkDuration) END))
	, MAX(CASE WHEN p.Quantity < 0 THEN 1 ELSE 0 END)
FROM @AccountTable a
INNER JOIN CliftonDW.dbo.AccountingPositionSnapshot p ON a.InvestmentAccountID = p.ClientInvestmentAccountID
INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension sd ON p.InvestmentSecurityID = sd.InvestmentSecurityID
LEFT JOIN CliftonDW.dbo.MarketDataValueSnapshot mv ON sd.InvestmentSecurityID = mv.InvestmentSecurityID AND mv.SnapshotDate = @LastBusinessDay
OUTER APPLY (
SELECT MAX(MarketData.GetMarketDataValue('Duration',ma.BenchmarkInvestmentSecurityID,@LastBusinessDay,1)) "BenchmarkDuration"
FROM InvestmentManagerAccount mgr
	INNER JOIN InvestmentManagerAccountAssignment ma ON mgr.InvestmentManagerAccountID = ma.InvestmentManagerAccountID
WHERE ma.InvestmentAccountID = a.InvestmentAccountID AND ma.IsInactive = 0 AND ma.BenchmarkInvestmentSecurityID IS NOT NULL) bmv
WHERE p.SnapshotDate = @Date
AND aa.IsCollateral = 0
AND a.AUMType = @DurationAdjNotional
GROUP BY a.InvestmentAccountID

INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM, IsIncludeShort)
SELECT a.InvestmentAccountID
	, SUM(ABS(p.Quantity))
	, MAX(CASE WHEN p.Quantity < 0 THEN 1 ELSE 0 END)
FROM @AccountTable a
INNER JOIN CliftonDW.dbo.AccountingPositionSnapshot p ON a.InvestmentAccountID = p.ClientInvestmentAccountID
INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension sd ON p.InvestmentSecurityID = sd.InvestmentSecurityID
WHERE p.SnapshotDate = @Date
AND aa.IsCollateral = 0
AND a.AUMType IN (@SynExpFuturesSwapAdjNot, @SwapAdjNotional)
AND (sd.InvestmentTypeName = 'Swaps' OR sd.InstrumentHierarchyName LIKE '%Swaption%')
GROUP BY a.InvestmentAccountID


INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM, IsIncludeShort)
SELECT a.InvestmentAccountID
	, SUM(p.BaseMarketValue)
	, MAX(CASE WHEN p.Quantity < 0 THEN 1 ELSE 0 END)
FROM @AccountTable a
INNER JOIN CliftonDW.dbo.AccountingPositionSnapshot p ON a.InvestmentAccountID = p.ClientInvestmentAccountID
INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension sd ON p.InvestmentSecurityID = sd.InvestmentSecurityID
WHERE p.SnapshotDate = @Date
AND (a.AUMType IN (@MarketValue, @PositionMarketValue)
OR (a.AUMType = @NotionalFuturesElseMarketValue AND sd.InvestmentTypeName != 'Futures' AND aa.IsCollateral = 0))
GROUP BY a.InvestmentAccountID
HAVING SUM(p.BaseMarketValue) != 0

-- Cash AND Currency Balances
UPDATE a
	SET CalculatedAUM = CalculatedAUM + COALESCE((
		SELECT SUM(p.LocalDebitCreditBalance * f.FxRate) "Base Market Value"
		FROM CliftonDW.dbo.AccountingBalanceSnapshot p
			INNER JOIN CliftonDW.dbo.AccountingAccountDimension ad ON p.AccountingAccountID = ad.AccountingAccountID
			INNER JOIN CliftonDW.dbo.InvestmentAccountDimension ca ON p.ClientInvestmentAccountID = ca.InvestmentAccountID
			INNER JOIN CliftonDW.dbo.InvestmentAccountDimension ha ON p.HoldingInvestmentAccountID = ha.InvestmentAccountID
			INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension s ON p.InvestmentSecurityID = s.InvestmentSecurityID
			CROSS APPLY (
				SELECT CASE WHEN s.TradingCurrencyID = ca.BaseCurrencyID THEN 1
					ELSE CliftonIMS.MarketData.GetExchangeRate(s.TradingCurrencyId, ca.BaseCurrencyID, ha.InvestmentAccountID, @Date) END "FxRate") f
		WHERE p.SnapshotDate = @Date
			AND ((ad.IsCurrency = 1  AND ad.IsGainLoss = 0) OR ad.IsReceivable = 1)
			AND	p.ClientInvestmentAccountID = a.InvestmentAccountID), 0)
FROM @AccountAUMTable a
INNER JOIN @AccountTable aa ON a.InvestmentAccountID = aa.InvestmentAccountID
WHERE aa.AUMType = @MarketValue
OPTION (RECOMPILE)

INSERT INTO @AccountAUMTable (InvestmentAccountID, CalculatedAUM, IsIncludeShort)
SELECT a.InvestmentAccountID
	, (
	SELECT ABS((SELECT SUM(m.OverlayBalance)
	FROM (
		SELECT
			(SELECT COALESCE(SUM(p.BaseNotional), 0) -- replication securities
				FROM CliftonDW.dbo.AccountingPositionSnapshot p
					INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
					INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension s ON p.InvestmentSecurityID = s.InvestmentSecurityID
					INNER JOIN InvestmentReplicationAllocation ra ON (s.InvestmentInstrumentID = ra.ReplicationInstrumentID OR s.InvestmentSecurityID = ra.ReplicationSecurityID)
				WHERE p.ClientInvestmentAccountID = a.InvestmentAccountID AND aa.IsCollateral = 0 AND p.SnapshotDate = @LastBusinessDay AND ra.InvestmentReplicationID = r.InvestmentReplicationID
			) + ( -- non-replication securities
				SELECT COALESCE(SUM(p.BaseNotional), 0)
					FROM CliftonDW.dbo.AccountingPositionSnapshot p
						INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID
						INNER JOIN CliftonDW.dbo.InvestmentSecurityDimension s ON p.InvestmentSecurityID = s.InvestmentSecurityID
						INNER JOIN CliftonDW.dbo.InvestmentGroupInstrumentDimension g ON g.InvestmentInstrumentID = s.InvestmentInstrumentID
					WHERE p.ClientInvestmentAccountID = a.InvestmentAccountID AND aa.IsCollateral = 0 AND p.SnapshotDate = @LastBusinessDay
						AND g.InvestmentGroupID = SystemUtil.GetCustomColumnValue('InvestmentAccount','Non-Replication Group', a.InvestmentAccountID)
						AND DATEDIFF(DAY, @LastBusinessDay, s.EndDate) >= r.PeriodMinDays AND DATEDIFF(DAY, @LastBusinessDay, s.EndDate) <= r.PeriodMaxDays
			)"OverlayBalance"
		FROM (
			SELECT m.MaturityLabel, m.PeriodMinDays, m.PeriodMaxDays, m.InvestmentReplicationID, b.BalanceDate, b.AssetsBalance, b.AssetsDV01, b.LiabilitiesBalance, b.LiabilitiesDV01
				,m.AssetsProxyBenchmarkSecurityID, m.LiabilitiesProxyBenchmarkSecurityID, m.AssetsProxyBenchmarkInterestRateIndexID, m.LiabilitiesProxyBenchmarkInterestRateIndexID
			FROM CliftonIMS.dbo.ProductLDIMaturity m
				LEFT JOIN CliftonIMS.dbo.ProductLDIBalance b ON m.ProductLDIMaturityID = b.ProductLDIMaturityID
			WHERE m.ClientInvestmentAccountID = a.InvestmentAccountID
				AND b.BalanceDate = (SELECT MAX(b2.BalanceDate) FROM CliftonIMS.dbo.ProductLDIBalance b2 WHERE b2.ProductLDIMaturityID = b.ProductLDIMaturityID AND b2.BalanceDate <= @LastBusinessDay)
			UNION -- include maturities with no balances
			SELECT m.MaturityLabel, m.PeriodMinDays, m.PeriodMaxDays, m.InvestmentReplicationID, NULL, 0, 0, 0, 0
				,m.AssetsProxyBenchmarkSecurityID, m.LiabilitiesProxyBenchmarkSecurityID, m.AssetsProxyBenchmarkInterestRateIndexID, m.LiabilitiesProxyBenchmarkInterestRateIndexID
			FROM CliftonIMS.dbo.ProductLDIMaturity m
			WHERE m.ClientInvestmentAccountID = a.InvestmentAccountID
		) r
		GROUP BY MaturityLabel, PeriodMinDays, PeriodMaxDays, InvestmentReplicationID
		) m
	))

)
, (SELECT MAX(CASE WHEN p.Quantity < 0 THEN 1 ELSE 0 END) FROM CliftonDW.dbo.AccountingPositionSnapshot p INNER JOIN CliftonDW.dbo.AccountingAccountDimension aa ON p.AccountingAccountID = aa.AccountingAccountID WHERE p.ClientInvestmentAccountID = a.InvestmentAccountID AND aa.IsCollateral = 0 AND p.SnapshotDate = @LastBusinessDay)
FROM @AccountTable a
WHERE a.AUMType = @LDI


	INSERT INTO @Result
		SELECT a.InvestmentAccountID
			, a.BaseCurrencyID
			, a.AUMType
			, COALESCE(aum.MasterAssetClassName, a.DefaultAssetClassName)
			, COALESCE(aum.CalculatedAUM,0) "CalculatedAUM"
			, COALESCE(aum.CalculatedAUM,0) * a.FXRate "CalculatedAUM-USD"
			, a.FXRate "FX Rate To USD"
			, COALESCE(a.AUMNotes,'') + COALESCE(aum.AUMNotes,'') "AUMNotes"
			, COALESCE(aum.IsIncludeShort,0) "IsIncludeShort"
		FROM @AccountTable a
		LEFT JOIN (
			SELECT aum.InvestmentAccountID
				, mac.AssetClassName "MasterAssetClassName"
				, SUM(COALESCE(aum.CalculatedAUM,0)) "CalculatedAUM"
				, MAX(aum.AUMNotes) "AUMNotes"
				, MAX(COALESCE(aum.IsIncludeShort,0)) "IsIncludeShort"
			FROM @AccountAUMTable aum
				LEFT JOIN InvestmentAssetClass ac ON aum.InvestmentAssetClassID = ac.InvestmentAssetClassID
				LEFT JOIN InvestmentAssetClass mac ON (CASE WHEN ac.IsMasterAssetClass = 1 THEN ac.InvestmentAssetClassID ELSE ac.MasterInvestmentAssetClassID END) = mac.InvestmentAssetClassID
			GROUP BY aum.InvestmentAccountID, mac.AssetClassName
		) aum ON a.InvestmentAccountID = aum.InvestmentAccountID


RETURN
END




GO


