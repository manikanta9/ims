SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/**
  * Returns the given client account (if IncludeParent is true)
  * And any sub-accounts (if IncludeSubAccounts is true)
  * If Active Date is supplied will limit to those sub-account relationships active on the given date.
  */
CREATE FUNCTION [Investment].[GetClientInvestmentAccountList](@ParentClientInvestmentAccountID INT, @IncludeParent BIT, @IncludeSubAccounts BIT, @ActiveDate DATETIME NULL)
	RETURNS @Result TABLE(InvestmentAccountID INT)
AS
BEGIN

	IF @IncludeParent = 1
	BEGIN
		INSERT INTO @Result
			SELECT @ParentClientInvestmentAccountID
	END

	IF @IncludeSubAccounts = 1
	BEGIN
	INSERT INTO @Result
		SELECT DISTINCT RelatedAccountID
		FROM InvestmentAccountRelationship r
			INNER JOIN InvestmentAccountRelationshipPurpose p ON r.InvestmentAccountRelationshipPurposeID = p.InvestmentAccountRelationshipPurposeID
		WHERE r.MainAccountID = @ParentClientInvestmentAccountID
			AND p.RelationshipPurposeName = 'Clifton Sub-Account'
			AND (@ActiveDate IS NULL OR ((r.EndDate IS NULL AND @ActiveDate >= r.StartDate) OR @ActiveDate BETWEEN r.StartDate AND r.EndDate))
	END

	RETURN
END
GO



