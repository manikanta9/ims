USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [Calendar].[IsBusinessDay]    Script Date: 12/6/2019 1:13:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/**
 * Returns true if it's a business day for the date and calendar (default calendar if null)
 * A business day is considered a week day and not a full day holiday (trading or settlement)
**/
	ALTER FUNCTION [Calendar].[IsBusinessDay] (

	@Date DATETIME,
	@CalendarID INT


	) RETURNS BIT AS
	BEGIN
		DECLARE
			@BusinessDay BIT

		SELECT @BusinessDay =
			   CASE
				   WHEN DATEPART(DW, @Date) IN (1, 7) THEN 0
				   ELSE 1
			   END

		IF (@BusinessDay = 1)
			BEGIN

				IF (@CalendarID IS NULL)
					BEGIN
						SELECT @CalendarID = CalendarID FROM Calendar WHERE IsDefaultSystemCalendar = 1
					END

				DECLARE
					@CalendarDayID INT
				SELECT @CalendarDayID = CalendarDayID FROM CalendarDay WHERE StartDate = CAST(@Date AS DATE)

				SELECT @BusinessDay =
					   CASE
						   WHEN EXISTS(SELECT * FROM CalendarHolidayDay WHERE CalendarID = @CalendarID AND CalendarDayID = @CalendarDayID AND IsFullDayHoliday = 1) THEN 0
						   ELSE 1
					   END
			END


		RETURN
			@BusinessDay

	END


GO


