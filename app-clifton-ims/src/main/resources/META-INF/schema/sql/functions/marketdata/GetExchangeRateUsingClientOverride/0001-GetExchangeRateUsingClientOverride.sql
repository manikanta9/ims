USE [CliftonIMS]
GO
/****** Object:  UserDefinedFunction [MarketData].[GetExchangeRateUsingClientOverride]    Script Date: 10/6/2017 12:20:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [MarketData].[GetExchangeRateUsingClientOverride]	(
	 @FromCurrencyID INT,
	 @ToCurrencyID INT,
	 @HoldingInvestmentAccountID INT, -- used to determine the data source
	 @RateDate DATETIME,
	 @ClientInvestmentAccountID INT -- used to override holding account the data source
)
RETURNS DECIMAL(28,16) AS

BEGIN
	IF (@FromCurrencyID = @ToCurrencyID)
	BEGIN
		RETURN 1
	END

	DECLARE @DataSourceID INT;

	IF @ClientInvestmentAccountID IS NOT NULL
	BEGIN
		SET @DataSourceID = (
			SELECT MarketDataSourceID FROM MarketDataSource ds
			WHERE ds.BusinessCompanyID = (
				SELECT COALESCE(p.BusinessCompanyID, c.BusinessCompanyID)
					FROM BusinessCompany c
						INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.DefaultExchangeRateSourceCompanyID
						LEFT JOIN BusinessCompany p ON c.ParentBusinessCompanyID = p.BusinessCompanyID
					WHERE a.InvestmentAccountID = @ClientInvestmentAccountID
			)
		)
	END

	IF @DataSourceID IS NULL
	BEGIN
		SET @DataSourceID = (
			SELECT MarketDataSourceID FROM MarketDataSource ds
			WHERE ds.BusinessCompanyID = (
				SELECT COALESCE(p.BusinessCompanyID, c.BusinessCompanyID)
					FROM BusinessCompany c
						INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
						LEFT JOIN BusinessCompany p ON c.ParentBusinessCompanyID = p.BusinessCompanyID
					WHERE a.InvestmentAccountID = @HoldingInvestmentAccountID
			)
		)
	END

	RETURN MarketData.GetExchangeRateForDataSource(@FromCurrencyID, @ToCurrencyID, @DataSourceID, @RateDate, 1, 1)

END
