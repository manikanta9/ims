USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [MarketData].[GetExchangeRate]    Script Date: 3/9/2016 4:04:51 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE FUNCTION [MarketData].[GetExchangeRate]	(
	 @FromCurrencyID INT,
	 @ToCurrencyID INT,
	 @HoldingInvestmentAccountID INT, -- used to determine the data source
	 @RateDate DATETIME
)
RETURNS DECIMAL(28,16) AS

BEGIN
	IF (@FromCurrencyID = @ToCurrencyID)
	BEGIN
		RETURN 1
	END

	DECLARE @DefaultDataSourceID INT = (
		SELECT ds.MarketDataSourceID FROM MarketDataSource ds
			INNER JOIN BusinessCompany c ON ds.BusinessCompanyID = c.BusinessCompanyID
		WHERE CompanyName = 'Goldman Sachs'
	)

	DECLARE @DataSourceID INT = (
		SELECT MarketDataSourceID FROM MarketDataSource ds
		WHERE ds.BusinessCompanyID = (
			SELECT COALESCE(p.BusinessCompanyID, c.BusinessCompanyID)
				FROM BusinessCompany c
					INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
					LEFT JOIN BusinessCompany p ON c.ParentBusinessCompanyID = p.BusinessCompanyID
				WHERE a.InvestmentAccountID = @HoldingInvestmentAccountID
		)
	)

	IF (@DataSourceID IS NULL)
	BEGIN
		SET @DataSourceID = @DefaultDataSourceID
	END


	DECLARE @Result DECIMAL(28,16), @ResultDate DATE = NULL
	DECLARE @EarliestRateDate DATE = DATEADD(DAY, -7, @RateDate) -- limit lookups to last 7 days

	SELECT TOP 1 @Result = ExchangeRate, @ResultDate = RateDate
		FROM MarketDataExchangeRate
		WHERE FromCurrencySecurityID = @FromCurrencyID AND ToCurrencySecurityID = @ToCurrencyID
			AND MarketDataSourceID = @DataSourceID AND RateDate BETWEEN @EarliestRateDate AND @RateDate
		ORDER BY RateDate DESC

	IF ((@ResultDate IS NULL OR @ResultDate < CAST(@RateDate AS DATE)) AND @DefaultDataSourceID <> @DataSourceID)
	BEGIN
		-- no rate found for the specified Data Source on the date: check if Default Data Source has the rate
		DECLARE @Result2 DECIMAL(28,16), @ResultDate2 DATE

		SELECT TOP 1 @Result2 = ExchangeRate, @ResultDate2 = RateDate
			FROM MarketDataExchangeRate
			WHERE FromCurrencySecurityID = @FromCurrencyID AND ToCurrencySecurityID = @ToCurrencyID
				AND MarketDataSourceID = @DefaultDataSourceID AND RateDate BETWEEN @EarliestRateDate AND @RateDate
			ORDER BY RateDate DESC

		IF (@Result2 IS NOT NULL AND (@ResultDate IS NULL OR @ResultDate2 > @ResultDate))
		BEGIN
			RETURN @Result2
		END
	END

	RETURN @Result

END



GO


