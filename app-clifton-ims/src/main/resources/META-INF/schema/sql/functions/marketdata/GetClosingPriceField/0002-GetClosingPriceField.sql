USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [MarketData].[GetClosingPriceField]    Script Date: 3/31/2016 9:31:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/**
  * Returns the Closing Price Market Data Field mapped to the Security.
  * Checks Specificity for Instrument first, Hierarchy, Type/SubType/SubType2, Type/SubType2, Type/SubType, Type, finally Last Trade Price
  */
ALTER FUNCTION [MarketData].[GetClosingPriceField]    (

	@InvestmentSecurityID INT

)
RETURNS INT AS


BEGIN

	DECLARE
	@Result INT

	SELECT @Result = COALESCE(im.ClosingPriceMarketDataFieldID, hm.ClosingPriceMarketDataFieldID, st12m.ClosingPriceMarketDataFieldID, st2m.ClosingPriceMarketDataFieldID,
							  st1m.ClosingPriceMarketDataFieldID, tm.ClosingPriceMarketDataFieldID)
	FROM InvestmentSecurity s
		INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID
		INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID
		LEFT JOIN MarketDataPriceFieldMapping im ON i.InvestmentInstrumentID = im.InvestmentInstrumentID
		LEFT JOIN MarketDataPriceFieldMapping hm ON i.InvestmentInstrumentHierarchyID = hm.InvestmentInstrumentHierarchyID
		LEFT JOIN MarketDataPriceFieldMapping st12m ON h.InvestmentTypeSubTypeID = st12m.InvestmentTypeSubTypeID AND h.InvestmentTypeSubType2ID = st12m.InvestmentTypeSubType2ID
		LEFT JOIN MarketDataPriceFieldMapping st2m ON h.InvestmentTypeSubType2ID = st2m.InvestmentTypeSubType2ID AND st2m.InvestmentTypeSubTypeID IS NULL
		LEFT JOIN MarketDataPriceFieldMapping st1m ON h.InvestmentTypeSubTypeID = st1m.InvestmentTypeSubTypeID AND st1m.InvestmentTypeSubType2ID IS NULL
		LEFT JOIN MarketDataPriceFieldMapping tm ON h.InvestmentTypeID = tm.InvestmentTypeID AND tm.InvestmentTypeSubTypeID IS NULL AND tm.InvestmentTypeSubType2ID IS NULL
	WHERE s.InvestmentSecurityID = @InvestmentSecurityID

	IF (@Result IS NULL)
		BEGIN
			SELECT @Result = d.MarketDataFieldID
			FROM MarketDataField d
			WHERE d.DataFieldName = 'Last Trade Price'
		END

	RETURN

	@Result

END


GO


