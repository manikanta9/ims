ALTER FUNCTION [dbo].[InvestmentSecurityLookup]
(
	@Symbol nvarchar(50),
	@DataSourceID INT,
	@IsCurrency BIT = 0
)
RETURNS INT
AS
BEGIN
	
	DECLARE @InvestmentSecurityID INT
	DECLARE @SectorName nvarchar(50)
	DECLARE @SectorID INT

	SELECT
		@InvestmentSecurityID = InvestmentSecurityID
	FROM MarketDataSourceSecurity mdss
	WHERE DataSourceSymbol = @Symbol AND mdss.MarketDataSourceID = @DataSourceID

	IF(@InvestmentSecurityID IS NOT NULL) RETURN @InvestmentSecurityID

	SET @SectorName = SUBSTRING(@Symbol, LEN(@Symbol) - CHARINDEX(' ', REVERSE(@Symbol)) + 2, LEN(@Symbol))
	IF @SectorName IS NOT NULL
	BEGIN
		SET @SectorID = (SELECT MarketDataSourceSectorID FROM MarketDataSourceSector WHERE SectorName = @SectorName AND MarketDataSourceID = @DataSourceID)

		IF @SectorID IS NOT NULL
		BEGIN
			--SET @SectorName = ''
			SET @Symbol = SUBSTRING(@Symbol,1, LEN(@Symbol) - CHARINDEX(' ', REVERSE(@Symbol)))
		END
	END

	IF (SELECT COUNT(*) FROM InvestmentSecurity WHERE Symbol = @Symbol AND IsCurrency = @IsCurrency) <= 1
	BEGIN
		SELECT @InvestmentSecurityID = InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = @Symbol AND IsCurrency = @IsCurrency
	END
	ELSE
	BEGIN
		SET @InvestmentSecurityID = (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s
				INNER JOIN InvestmentInstrument i ON i.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentInstrumentHierarchy h ON h.InvestmentInstrumentHierarchyID = i.InvestmentInstrumentHierarchyID

				LEFT JOIN MarketDataFieldMapping im ON i.InvestmentInstrumentID = im.InvestmentInstrumentID
				LEFT JOIN MarketDataFieldMapping hm ON h.InvestmentInstrumentHierarchyID = hm.InvestmentInstrumentHierarchyID
				LEFT JOIN MarketDataFieldMapping st12m ON h.InvestmentTypeID = st12m.InvestmentTypeID AND h.InvestmentTypeSubTypeID = st12m.InvestmentTypeSubTypeID AND h.InvestmentTypeSubType2ID = st12m.InvestmentTypeSubType2ID
				LEFT JOIN MarketDataFieldMapping st2m ON h.InvestmentTypeID = st2m.InvestmentTypeID AND h.InvestmentTypeSubType2ID = st2m.InvestmentTypeSubType2ID AND st2m.InvestmentTypeSubTypeID IS NULL
				LEFT JOIN MarketDataFieldMapping st1m ON h.InvestmentTypeID = st1m.InvestmentTypeID AND h.InvestmentTypeSubTypeID = st1m.InvestmentTypeSubTypeID AND st1m.InvestmentTypeSubType2ID IS NULL
				LEFT JOIN MarketDataFieldMapping tm ON h.InvestmentTypeID = tm.InvestmentTypeID AND tm.InvestmentTypeSubTypeID IS NULL AND tm.InvestmentTypeSubType2ID IS NULL
				LEFT JOIN MarketDataSourceSector ms ON ms.MarketDataSourceSectorID =  COALESCE(im.MarketDataSourceSectorID, hm.MarketDataSourceSectorID, st12m.MarketDataSourceSectorID, st2m.MarketDataSourceSectorID, st1m.MarketDataSourceSectorID, tm.MarketDataSourceSectorID)

			WHERE Symbol = @Symbol AND s.IsCurrency = @IsCurrency
				AND (@SectorID IS NULL OR ms.MarketDataSourceSectorID = @SectorID)
		)
	END
	RETURN @InvestmentSecurityID

END
