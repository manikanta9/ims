USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [MarketData].[GetExchangeRateForCompany]    Script Date: 3/9/2016 4:07:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER FUNCTION [MarketData].[GetExchangeRateForCompany]	(
	 @FromCurrencyID INT,
	 @ToCurrencyID INT,
	 @BusinessCompanyID INT, -- used to determine the data source (also checks parent company)
	 @RateDate DATETIME
)
RETURNS DECIMAL(28,16) AS

BEGIN
	IF (@FromCurrencyID = @ToCurrencyID)
	BEGIN
		RETURN 1
	END

	DECLARE @DataSourceID INT = COALESCE(
		(SELECT MarketDataSourceID FROM MarketDataSource WHERE BusinessCompanyID = @BusinessCompanyID),
		(SELECT MarketDataSourceID FROM MarketDataSource ds
			INNER JOIN BusinessCompany c ON ds.BusinessCompanyID = c.ParentBusinessCompanyID
			WHERE c.BusinessCompanyID = @BusinessCompanyID
		)
	)

	RETURN MarketData.GetExchangeRateForDataSource(@FromCurrencyID, @ToCurrencyID, @DataSourceID, @RateDate, 1, 1)

END



GO


