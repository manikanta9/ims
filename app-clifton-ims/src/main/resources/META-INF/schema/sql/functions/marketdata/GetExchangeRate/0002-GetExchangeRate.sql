USE [CliftonIMS]
GO

/****** Object:  UserDefinedFunction [MarketData].[GetExchangeRate]    Script Date: 3/9/2016 4:07:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





ALTER FUNCTION [MarketData].[GetExchangeRate]	(
	 @FromCurrencyID INT,
	 @ToCurrencyID INT,
	 @HoldingInvestmentAccountID INT, -- used to determine the data source
	 @RateDate DATETIME
)
RETURNS DECIMAL(28,16) AS

BEGIN
	IF (@FromCurrencyID = @ToCurrencyID)
	BEGIN
		RETURN 1
	END

	DECLARE @DataSourceID INT = (
		SELECT MarketDataSourceID FROM MarketDataSource ds
		WHERE ds.BusinessCompanyID = (
			SELECT COALESCE(p.BusinessCompanyID, c.BusinessCompanyID)
				FROM BusinessCompany c
					INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
					LEFT JOIN BusinessCompany p ON c.ParentBusinessCompanyID = p.BusinessCompanyID
				WHERE a.InvestmentAccountID = @HoldingInvestmentAccountID
		)
	)

	RETURN MarketData.GetExchangeRateForDataSource(@FromCurrencyID, @ToCurrencyID, @DataSourceID, @RateDate, 1, 1)

END



GO


