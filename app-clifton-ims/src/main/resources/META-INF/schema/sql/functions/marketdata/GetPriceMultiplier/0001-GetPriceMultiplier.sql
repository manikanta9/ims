CREATE FUNCTION [MarketData].[GetPriceMultiplier]
(
	@MarketDataSourceID INT,
	@InvestmentSecurityID INT,
	@MarketDataSourcePurposeID SMALLINT
)
RETURNS DECIMAL(19, 10)
AS
BEGIN
	
	DECLARE @Result Decimal(19, 10)
	
	SELECT TOP 1 @Result = AdditionalPriceMultiplier 
	FROM MarketDataSourcePriceMultiplier  pm
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = pm.InvestmentInstrumentID
		INNER JOIN InvestmentSecurity s ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		LEFT JOIN MarketDataSourcePurpose p ON p.MarketDataSourcePurposeID = pm.MarketDataSourcePurposeID
	WHERE MarketDataSourceID = @MarketDataSourceID AND InvestmentSecurityID = @InvestmentSecurityID
		AND (p.MarketDataSourcePurposeID = @MarketDataSourcePurposeID OR pm.MarketDataSourcePurposeID IS NULL)
	ORDER BY pm.MarketDataSourcePurposeID DESC
	
	RETURN ISNULL(@Result, 1.0)
END
