
ALTER FUNCTION [Accounting].[FindCommissionRate]
(		 @InvestmentTypeID  int
         ,@InvestmentInstrumentID  int
         ,@InvestmentInstrumentHierarchyID  int
         ,@BrokerCompanyID  int
         ,@InvestmentAccountID  int
         ,@AcccountingAccountID  int
         ,@CommissionDate datetime
         ,@ApplyOnOpen bit
)
RETURNS TABLE
AS
RETURN

select top 1 * from
(Select * from AccountingCommissionDefinition          where HoldingAccountID = @InvestmentAccountID and HoldingCompanyID is null and InvestmentInstrumentID = @InvestmentInstrumentID and InvestmentInstrumentHierarchyID = @InvestmentInstrumentHierarchyID and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingAccountID = @InvestmentAccountID and HoldingCompanyID is null and InvestmentInstrumentID is null                   and InvestmentInstrumentHierarchyID = @InvestmentInstrumentHierarchyID and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingAccountID = @InvestmentAccountID and HoldingCompanyID is null and InvestmentInstrumentID is null                   and InvestmentInstrumentHierarchyID is null                            and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingCompanyID = @BrokerCompanyID     and HoldingAccountID is null and InvestmentInstrumentID = @InvestmentInstrumentID and InvestmentInstrumentHierarchyID = @InvestmentInstrumentHierarchyID and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingCompanyID = @BrokerCompanyID     and HoldingAccountID is null and InvestmentInstrumentID is null                   and InvestmentInstrumentHierarchyID = @InvestmentInstrumentHierarchyID and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingCompanyID = @BrokerCompanyID     and HoldingAccountID is null and InvestmentInstrumentID is null                   and InvestmentInstrumentHierarchyID is null                            and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingCompanyID is null                and HoldingAccountID is null and InvestmentInstrumentID = @InvestmentInstrumentID and InvestmentInstrumentHierarchyID = @InvestmentInstrumentHierarchyID and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingCompanyID is null                and HoldingAccountID is null and InvestmentInstrumentID is null                   and InvestmentInstrumentHierarchyID = @InvestmentInstrumentHierarchyID and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
union all Select * from AccountingCommissionDefinition where HoldingCompanyID is null                and HoldingAccountID is null and InvestmentInstrumentID is null                   and InvestmentInstrumentHierarchyID is null                            and InvestmentTypeID = @InvestmentTypeID and IsApplyOnOpen=@ApplyOnOpen and AccountingAccountID=@AcccountingAccountID and (StartDate is null or StartDate <= @CommissionDate) and (EndDate is null or EndDate >= @CommissionDate)
) sub
