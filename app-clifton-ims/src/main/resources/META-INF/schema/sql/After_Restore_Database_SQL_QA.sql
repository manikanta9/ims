-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group - note that the group is assumed to exist and will not be created
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'
UNION
-- PW: password2
SELECT 'imstestuser2', 'Test User 2', NULL
UNION
-- PW: password3
SELECT 'imstestuser3', 'Test User 3', NULL
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', 'Analysts|'
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', 'Documentation Team|'
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|Compliance Violation Support|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trade Confirmation|Analysts|Portfolio Managers|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', 'Client Admins|Investment Account Admins|Reporting Admins|Operations|'

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)


INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================

-- DISABLE ALL BATCH JOBS
UPDATE BatchJob
SET IsEnabled = 0;

-- DISABLE EXPORTS
UPDATE ExportDefinition
SET StartDate = DATEADD(DAY, -10, CONVERT(DATE, GETDATE())),
	EndDate   = DATEADD(DAY, -10, CONVERT(DATE, GETDATE()))

-- UPDATE DEFAULT ACCOUNTS FOR FIX
UPDATE tdm
SET tdm.FixPostTradeAllocationTopAccountNumber = CASE
													 WHEN t.TradeTypeName IN ('Stocks', 'Funds', 'Notes')
														 THEN '00999900'
													 ELSE 'C0320613'
												 END
FROM TradeDestinationMapping tdm
	 INNER JOIN TradeType t ON t.TradeTypeID = tdm.TradeTypeID
	 INNER JOIN TradeDestination td ON td.TradeDestinationID = tdm.TradeDestinationID
	 INNER JOIN TradeDestinationType tdt ON tdt.TradeDestinationTypeID = td.TradeDestinationTypeID
WHERE tdt.IsFixTradeOnly = 1
  AND td.TradeDestinationName IN ('REDI Ticket', 'GS Desk')


-- IN ORDER TO MAKE RESTORES MORE EFFICIENT
-- ACTUAL SP HAS BEEN ADDED TO PRODUCTION
-- CREATE PROC [dbo].[spMarketDataUpdateFromProduction]
-- HOWEVER THERE IS A CHECK ON THE SERVER NAME THAT ENSURE IT'S NEVER
-- ACTUALLY EXECUTED ON PRODUCTION
--	IF (@@SERVERNAME = 'TCGIMSSQL')
--	BEGIN
--		RAISERROR('CANNOT EXECUTE spMarketDataUpdateFromProduction ON PRODUCTION', 16, 1)
--		RETURN
--	END
-- THE BATCH JOB AND SYSTEM BEAN THAT CALL THAT SP ARE ADDED AFTER EACH RESTORE


DECLARE
	@BeanTypeID INT,
	@BeanID INT,
	@Name NVARCHAR(50) = '** QA Copy Market Data from Production',
	@Description NVARCHAR(100) = '** QA copies Market Data (Exchange Rates, Prices, Events, etc) from Production for past 2 days'

DECLARE @BatchJobCategoryID INT
SELECT @BatchJobCategoryID = BatchJobCategoryID
FROM BatchJobCategory
WHERE CategoryName = 'Other'

IF NOT EXISTS(SELECT *
			  FROM BatchJob
			  WHERE JobName = @Name)
	BEGIN
		SELECT @BeanTypeID = SystemBeanTypeID
		FROM SystemBeanType
		WHERE SystemBeanTypeName = 'Stored Procedure Executor'

		INSERT INTO SystemBean (SystemBeanTypeID, SystemBeanName, SystemBeanDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanTypeID, @Name, @Description, 0, GETDATE(), 0, GETDATE())


		SELECT @BeanID = SystemBeanID
		FROM SystemBean
		WHERE SystemBeanName = @Name

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Stored Procedure Name'), 'spMarketDataUpdateFromProduction',
				'spMarketDataUpdateFromProduction', 0, GETDATE(), 0, GETDATE())

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Data Source Name'), 'dataSource', 'dataSource', 0, GETDATE(), 0, GETDATE())

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Transaction Timeout (Seconds)'), '90', '90', 0, GETDATE(), 0, GETDATE())


		INSERT INTO BatchJob (BatchJobCategoryID, SystemBeanID, CalendarScheduleID, JobName, JobDescription, IsEnabled, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BatchJobCategoryID, @BeanID, (SELECT CalendarScheduleID
											   FROM CalendarSchedule
											   WHERE ScheduleName = 'Every Day at 8:00 AM'), @Name, @Description, 1, 0, GETDATE(), 0, GETDATE())
	END


SET @Name = '** QA Copy New Users from Production'
SET @Description = '** QA copies new users and their group assignments from Production (only if the user does not exist in QA)'

IF NOT EXISTS(SELECT *
			  FROM BatchJob
			  WHERE JobName = @Name)
	BEGIN
		SELECT @BeanTypeID = SystemBeanTypeID
		FROM SystemBeanType
		WHERE SystemBeanTypeName = 'Stored Procedure Executor'

		INSERT INTO SystemBean (SystemBeanTypeID, SystemBeanName, SystemBeanDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanTypeID, @Name, @Description, 0, GETDATE(), 0, GETDATE())


		SELECT @BeanID = SystemBeanID
		FROM SystemBean
		WHERE SystemBeanName = @Name

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Stored Procedure Name'), 'spNewUserCopyFromProduction',
				'spNewUserCopyFromProduction', 0, GETDATE(), 0, GETDATE())

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Data Source Name'), 'dataSource', 'dataSource', 0, GETDATE(),
				0, GETDATE())

		INSERT INTO BatchJob (BatchJobCategoryID, SystemBeanID, CalendarScheduleID, JobName, JobDescription, IsEnabled, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BatchJobCategoryID, @BeanID, (SELECT CalendarScheduleID
											   FROM CalendarSchedule
											   WHERE ScheduleName = 'Every Day at 8:00 AM'), @Name, @Description, 1, 0, GETDATE(), 0, GETDATE())
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'db_executor')
	BEGIN
		CREATE ROLE db_executor
		GRANT EXECUTE TO db_executor
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'PARAPORT\MPLS SQL QA Readers')
	BEGIN
		CREATE USER [PARAPORT\MPLS SQL QA Readers] FOR LOGIN [PARAPORT\MPLS SQL QA Readers];
	END
EXEC sp_addrolemember 'db_datareader', 'PARAPORT\MPLS SQL QA Readers';
EXEC sp_addrolemember 'db_executor', 'PARAPORT\MPLS SQL QA Readers';

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'svrLink')
	BEGIN
		CREATE USER [svrLink]
	END
EXEC sp_addrolemember 'db_owner', 'svrLink'


--Disable all Export destinations
UPDATE ExportDefinitionDestination
SET IsDisabled = 1

--Remove schedules from all Exports
UPDATE ExportDefinition
SET RecurrenceScheduleID = NULL


DECLARE
	@ExportRunHistoryId INT

IF EXISTS(SELECT *
		  FROM sys.databases
		  WHERE name = 'CliftonIntegration')
	BEGIN
		SET @ExportRunHistoryId = (SELECT MAX(SourceSystemIdentifier)
								   FROM (SELECT MAX(CONVERT(INT, SourceSystemIdentifier)) AS SourceSystemIdentifier
										 FROM CliftonIntegration.dbo.IntegrationExport
										 UNION ALL
										 SELECT MAX(ExportRunHistoryID)
										 FROM ExportRunHistory) x)
	END
ELSE
	BEGIN
		SET @ExportRunHistoryId = (SELECT MAX(ExportRunHistoryID) FROM ExportRunHistory)
	END
DBCC CHECKIDENT (ExportRunHistory, RESEED, @ExportRunHistoryId)


DECLARE
	@QueryID INT
SELECT @QueryID = SystemQueryID
FROM SystemQuery
WHERE QueryName = 'Billing Sales Commissions'

DELETE r
FROM SystemQueryRunHistory r
WHERE r.SystemQueryID = @QueryID
DELETE p
FROM SystemQueryParameter p
WHERE p.SystemQueryID = @QueryID
DELETE q
FROM SystemQuery q
WHERE SystemQueryID = @QueryID

-- reset the FixIdentifier id to avoid duplicates
IF EXISTS(SELECT name
		  FROM master.dbo.sysdatabases
		  WHERE ('[' + name + ']' = 'CliftonFIX' OR name = 'CliftonFIX'))
	BEGIN
		DECLARE
			@MaxOrderId INT
		SET @MaxOrderId = (
							  SELECT MAX(ExternalIdentifier)
							  FROM (
									   SELECT MAX(ExternalIdentifier) AS ExternalIdentifier
									   FROM CliftonIMS.dbo.TradeOrder
									   UNION ALL
									   SELECT MAX(ExternalIdentifier)
									   FROM CliftonIMS.dbo.TradeOrderAllocation
									   UNION ALL
									   SELECT MAX(FixIdentifierID)
									   FROM CliftonFIX.dbo.FixIdentifier
								   ) x
						  ) + 1
		DBCC CHECKIDENT ([CliftonFIX.dbo.FixIdentifier], RESEED, @MaxOrderId)
	END

--Set all Investment Instructions to use a test group
IF NOT EXISTS(SELECT iicg.InvestmentInstructionContactGroupID
			  FROM InvestmentInstructionContactGroup iicg
			  WHERE iicg.ContactGroupName = 'Test Group')
	BEGIN
		INSERT INTO InvestmentInstructionContactGroup
		(ContactGroupName, ContactGroupLabel, ContactGroupDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES ('Test Group', 'Test Group', 'Group for testing', 0, GETDATE(), 0, GETDATE());
	END

UPDATE InvestmentInstructionDefinition
SET InvestmentInstructionContactGroupID = (SELECT iicg.InvestmentInstructionContactGroupID
										   FROM InvestmentInstructionContactGroup iicg
										   WHERE iicg.ContactGroupName = 'Test Group');


DECLARE
	@SystemBeanTypeID INT = (SELECT SystemBeanTypeID
							 FROM SystemBeanType
							 WHERE SystemBeanTypeName = 'Instruction Email');
DECLARE
	@SystemBeanPropertyTypeID INT = (SELECT SystemBeanPropertyTypeID
									 FROM SystemBeanPropertyType
									 WHERE SystemBeanPropertyTypeName = 'Recipient Contact'
									   AND SystemBeanTypeID = @SystemBeanTypeID);
DECLARE
	@CommunicationPreference AS NVARCHAR(10) = 'EMAIL';
DECLARE
	@ContactGroupId INT = (SELECT InvestmentInstructionContactGroupID
						   FROM InvestmentInstructionContactGroup
						   WHERE ContactGroupName = 'Test Group');
DECLARE
	@RecipientContactLastName AS NVARCHAR(10) = 'TCGOps';
IF NOT EXISTS(SELECT *
			  FROM BusinessContact
			  WHERE ContactLastName = @RecipientContactLastName)
	BEGIN
		INSERT INTO BusinessContact (BusinessContactCategoryID, ContactLastName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (2, 'TCGOps', 0, GETDATE(), 0, GETDATE());
	END
DECLARE
	@RecipientContactId INT= (SELECT bc.BusinessContactID
							  FROM BusinessContact bc
							  WHERE bc.ContactLastName = @RecipientContactLastName);

INSERT INTO SystemBean(SystemBeanTypeID, SystemBeanName, SystemBeanDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT DISTINCT @SystemBeanTypeID,
				'Test Instruction Contact:' + @RecipientContactLastName + ':' + @CommunicationPreference + ':' + CONVERT(NVARCHAR, iic.RecipientCompanyId),
				'Test Instruction Contact:' + @RecipientContactLastName + ':' + @CommunicationPreference + ':' + CONVERT(NVARCHAR, iic.RecipientCompanyId),
				0,
				GETDATE(),
				0,
				GETDATE()
FROM InvestmentInstructionContact iic;

INSERT INTO SystemBeanProperty(SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT (SELECT SystemBeanID
		FROM SystemBean
		WHERE SystemBeanDescription =
			  'Test Instruction Contact:' + @RecipientContactLastName + ':' + @CommunicationPreference + ':' + CONVERT(NVARCHAR, iic.RecipientCompanyID)
	   ),
	   @SystemBeanPropertyTypeID,
	   @RecipientContactId,
	   @RecipientContactLastName + COALESCE(' (' + bcp.CompanyName + ')', ''),
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
FROM InvestmentInstructionContact iic
	 INNER JOIN BusinessCompany bcp
				ON iic.RecipientCompanyID = bcp.BusinessCompanyID
GROUP BY iic.RecipientCompanyID, bcp.CompanyName


INSERT INTO InvestmentInstructionContact(InvestmentInstructionContactGroupID, RecipientCompanyID, StartDate, DestinationSystemBeanID, CreateUserID, CreateDate, UpdateUserID,
										 UpdateDate)
SELECT DISTINCT @ContactGroupId,
				iic.RecipientCompanyID,
				GETDATE(),
				(SELECT SystemBeanID
				 FROM SystemBean
				 WHERE SystemBeanDescription =
					   'Test Instruction Contact:' + @RecipientContactLastName + ':' + @CommunicationPreference + ':' + CONVERT(NVARCHAR, iic.RecipientCompanyID)),
				0,
				GETDATE(),
				0,
				GETDATE()
FROM InvestmentInstructionContact iic;

UPDATE sb
SET sb.SystemBeanName = 'BEAN' + CONVERT(NVARCHAR, sb.SystemBeanID) + SUBSTRING(sb.SystemBeanName, 25, CHARINDEX(':', sb.SystemBeanName, 38) - 25)
FROM SystemBean sb
WHERE sb.SystemBeanDescription LIKE 'Test Instruction Contact%';

UPDATE sb
SET sb.SystemBeanDescription = SUBSTRING(sb.SystemBeanName, 10, CHARINDEX(':', sb.SystemBeanName, 10) - 10)
FROM SystemBean sb
WHERE sb.SystemBeanDescription LIKE 'Test Instruction Contact%';

-- Set the BIC codes to test BIC's
UPDATE v
SET ColumnValue = STUFF(ColumnValue, 8, 1, '0'),
	ColumnText  = CASE WHEN ColumnText IS NOT NULL THEN STUFF(ColumnText, 8, 1, '0') ELSE NULL END
FROM SystemColumnValue v
	 INNER JOIN SystemColumn sc ON sc.SystemColumnID = v.SystemColumnID
WHERE sc.ColumnName = 'BusinessIdentifierCode'

UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';


UPDATE SystemDataSource
SET DatabaseName   = '@integrationDataSource.databaseName@',
	ConnectionUrl  = '@integrationDataSource.url@',
	ApplicationUrl = '@external.service.endpoint.integrationApplication@'
WHERE DataSourceName = 'integrationDataSource';


UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.integrationSeattleApplication@'
WHERE DataSourceName = 'integrationSeattleDataSource';


UPDATE SystemDataSource
SET DatabaseName  = '@dwDataSource.databaseName@',
	ConnectionUrl = '@dwDataSource.url@'
WHERE DataSourceName = 'dwDataSource';


UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.clientPortalApplication@'
WHERE DataSourceName = 'portalDataSource';


UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.fixApplication@'
WHERE DataSourceName = 'fixDataSource';


UPDATE SystemDataSource
SET DatabaseName  = '@swiftDataSource.databaseName@',
	ConnectionUrl = '@swiftDataSource.url@'
WHERE DataSourceName = 'swiftDataSource';


UPDATE SystemDataSource
SET DatabaseName  = '@archiveDataSource.databaseName@',
	ConnectionUrl = '@archiveDataSource.url@'
WHERE DataSourceName = 'archiveDataSource';

UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.reconciliationApplication@'
WHERE DataSourceName = 'reconciliationDataSource';

UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.deltaApplication@'
WHERE DataSourceName = 'deltaDataSource';
