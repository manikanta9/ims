-- Create test account groups

DECLARE
	@TestGroupName NVARCHAR(50),
	@GroupTypeID INT,
	@BalanceDate DATE = '08/31/2020'
SET @TestGroupName = 'Overlay Account Tests'
SELECT @GroupTypeID = InvestmentAccountGroupTypeID
FROM InvestmentAccountGroupType
WHERE AccountGroupTypeName = 'Other'

INSERT INTO InvestmentAccountGroup (AccountGroupName, InvestmentAccountGroupTypeID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
VALUES (@TestGroupName, @GroupTypeID, 0, GETDATE(), 0, GETDATE())

DECLARE
	@GroupID INT
SELECT @GroupID = InvestmentAccountGroupID
FROM InvestmentAccountGroup
WHERE AccountGroupName = @TestGroupName
INSERT INTO InvestmentAccountGroupAccount (InvestmentAccountGroupID, InvestmentAccountID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT @GroupID
	 , r.ClientInvestmentAccountID
	 , 0
	 , GETDATE()
	 , 0
	 , GETDATE()
FROM PortfolioRun r
	 INNER JOIN InvestmentAccount a ON r.ClientInvestmentAccountID = a.InvestmentAccountID
	 INNER JOIN BusinessServiceProcessingType pt ON a.BusinessServiceProcessingTypeID = pt.BusinessServiceProcessingTypeID
	 INNER JOIN WorkflowStatus ws ON r.WorkflowStatusID = ws.WorkflowStatusID
WHERE r.BalanceDate = @BalanceDate
  AND ws.WorkflowStatusName NOT IN ('Open', 'Invalid')
  AND pt.ServiceProcessingType = 'PORTFOLIO_RUNS'
  -- Ignoring because of Setup Changes and duplicate replications
  AND a.AccountNumber NOT IN (
    '110500', '123600', '051450', '578280', '668199', '800025', '227000', '227550', '385451',
    '572000', '329030', '250002', '051101', '051102', '051103',
	'380000', -- Missing assets in custodian download for manager (M07510)
    '329060', -- Ignored due to change to market data TMW50
    '663131', '668450', '668457', '668449', '668475', '668476' -- Ignored due to market data changes affecting replication exposure
    , '358003' -- Ignored because MathUtil.GetPercentValue was updated to have max of +/-9999.9999
	, '051200', '051455' -- Skipping due to monthly schedule occurrence updates with invalid schedule setup
  )
GROUP BY r.ClientInvestmentAccountID


SET @TestGroupName = 'LDI Account Tests'
INSERT INTO InvestmentAccountGroup (AccountGroupName, InvestmentAccountGroupTypeID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
VALUES (@TestGroupName, @GroupTypeID, 0, GETDATE(), 0, GETDATE())

SELECT @GroupID = InvestmentAccountGroupID
FROM InvestmentAccountGroup
WHERE AccountGroupName = @TestGroupName
INSERT INTO InvestmentAccountGroupAccount (InvestmentAccountGroupID, InvestmentAccountID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT @GroupID
	 , r.ClientInvestmentAccountID
	 , 0
	 , GETDATE()
	 , 0
	 , GETDATE()
FROM PortfolioRun r
	 INNER JOIN InvestmentAccount a ON r.ClientInvestmentAccountID = a.InvestmentAccountID
	 INNER JOIN BusinessServiceProcessingType pt ON a.BusinessServiceProcessingTypeID = pt.BusinessServiceProcessingTypeID
	 INNER JOIN WorkflowStatus ws ON r.WorkflowStatusID = ws.WorkflowStatusID
WHERE r.BalanceDate = @BalanceDate
  AND ws.WorkflowStatusName NOT IN ('Open', 'Invalid')
  AND pt.ServiceProcessingType = 'LDI'
  AND a.AccountNumber NOT IN (
	'668000', '661260' -- ignoring because runs were executed prior to KRD marketdata updates on LULCTRUU benchmark security'
  )
GROUP BY r.ClientInvestmentAccountID

SET @TestGroupName = 'Ignore Rule Violations'

INSERT INTO InvestmentAccountGroup (AccountGroupName, InvestmentAccountGroupTypeID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
VALUES (@TestGroupName, @GroupTypeID, 0, GETDATE(), 0, GETDATE())

SELECT @GroupID = InvestmentAccountGroupID
FROM InvestmentAccountGroup
WHERE AccountGroupName = @TestGroupName
INSERT INTO InvestmentAccountGroupAccount (InvestmentAccountGroupID, InvestmentAccountID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT @GroupID
	 , InvestmentAccountID
	 , 0
	 , GETDATE()
	 , 0
	 , GETDATE()
FROM InvestmentAccount a
WHERE a.AccountNumber IN ('445162', '456070', '445201')

-- Rollback transactions for run processing
UPDATE AccountingTransaction
SET IsDeleted = 1
WHERE AccountingJournalID IN (
    5206452 -- 227500 Remove currency transactions
    )
UPDATE AccountingJournal
SET AccountingJournalStatusID = 3
WHERE AccountingJournalID IN (
    5206452 -- 227500 Remove currency transactions
    )

-- Revert 051101, 051102, 051103 allocation percentages
UPDATE maa SET maa.AllocationPercent = 1.209737923901106
FROM InvestmentManagerAccountAllocation maa
WHERE maa.InvestmentManagerAccountAllocationID = 21741

UPDATE maa SET maa.AllocationPercent = 23.0008413959
FROM InvestmentManagerAccountAllocation maa
WHERE maa.InvestmentManagerAccountAllocationID = 21742

UPDATE maa SET maa.AllocationPercent = 14.34430130306326
FROM InvestmentManagerAccountAllocation maa
WHERE maa.InvestmentManagerAccountAllocationID = 21743

-- Correct Client Directed Asset Class Values
UPDATE aac SET aac.ClientDirectedCashAmount = 0, aac.ClientDirectedCashOption = 'NONE',
			   aac.SecondaryInvestmentReplicationID = NULL,
			   aac.SecondaryInvestmentReplicationAmount = NULL
FROM InvestmentAccountAssetClass aac
	 INNER JOIN InvestmentAccount a ON a.InvestmentAccountID = aac.InvestmentAccountID
	 INNER JOIN InvestmentAssetClass ac ON ac.InvestmentAssetClassID = aac.InvestmentAssetClassID
WHERE a.AccountNumber = '079110' AND ac.AssetClassName = 'US Large Cap Equity'

UPDATE aac SET aac.ClientDirectedCashAmount = -76250814
FROM InvestmentAccountAssetClass aac
	 INNER JOIN InvestmentAccount a ON a.InvestmentAccountID = aac.InvestmentAccountID
	 INNER JOIN InvestmentAssetClass ac ON ac.InvestmentAssetClassID = aac.InvestmentAssetClassID
WHERE a.AccountNumber = '250002' AND ac.AssetClassName = 'Domestic Equity Large Cap'

UPDATE aac SET aac.ClientDirectedCashAmount = -11272497.00
FROM InvestmentAccountAssetClass aac
	 INNER JOIN InvestmentAccount a ON a.InvestmentAccountID = aac.InvestmentAccountID
	 INNER JOIN InvestmentAssetClass ac ON ac.InvestmentAssetClassID = aac.InvestmentAssetClassID
WHERE a.AccountNumber = '250002' AND ac.AssetClassName = 'International Equity - EAFE'

UPDATE aac SET aac.ClientDirectedCashAmount = -1152904.00
FROM InvestmentAccountAssetClass aac
	 INNER JOIN InvestmentAccount a ON a.InvestmentAccountID = aac.InvestmentAccountID
	 INNER JOIN InvestmentAssetClass ac ON ac.InvestmentAssetClassID = aac.InvestmentAssetClassID
WHERE a.AccountNumber = '250002' AND ac.AssetClassName = 'International Equity - EM'

-- Revert Proxy Value Updates to Match Runs
UPDATE m
SET ProxyValueDate = x.BeforeDateValue
  , ProxyValue     = replace(x.BeforeValue, ',', '')
FROM InvestmentManagerAccount m
	 INNER JOIN (
	SELECT ma.InvestmentManagerAccountID
		 , r.ClientInvestmentAccountID
		 , ma.ProxyValueDate
		 , ma.ProxyValue
		 , sum(m.ProxyValueAllocation) "Run Total"
		 , (SELECT
			TOP 1
			e.OldValue
			FROM SystemAuditEvent e
				 INNER JOIN SystemColumn c ON e.SystemColumnID = c.SystemColumnID
				 INNER JOIN SystemTable t ON c.SystemTableID = t.SystemTableID
			WHERE t.TableName = 'InvestmentManagerAccount'
			  AND c.ColumnName = 'ProxyValueDate'
			  AND e.MainPKFieldID = ma.InvestmentManagerAccountID
			ORDER BY AuditDate DESC)   "BeforeDateValue"
		 , (SELECT
			TOP 1
			e.OldValue
			FROM SystemAuditEvent e
				 INNER JOIN SystemColumn c ON e.SystemColumnID = c.SystemColumnID
				 INNER JOIN SystemTable t ON c.SystemTableID = t.SystemTableID
			WHERE t.TableName = 'InvestmentManagerAccount'
			  AND c.ColumnName = 'ProxyValue'
			  AND e.MainPKFieldID = ma.InvestmentManagerAccountID
			ORDER BY AuditDate DESC)   "BeforeValue"
	FROM PortfolioRun r
		 INNER JOIN ProductOverlayManagerAccount m ON r.PortfolioRunID = m.ProductOverlayRunID
		 INNER JOIN InvestmentManagerAccountAssignment mas ON m.InvestmentManagerAccountAssignmentID = mas.InvestmentManagerAccountAssignmentID
		 INNER JOIN InvestmentManagerAccount ma ON mas.InvestmentManagerAccountID = ma.InvestmentManagerAccountID
	WHERE r.PortfolioRunID = (SELECT
							  TOP 1
							  PortfolioRunID
							  FROM PortfolioRun r2
							  WHERE r2.BalanceDate = @BalanceDate
								AND r2.ClientInvestmentAccountID = r.ClientInvestmentAccountID
								AND r2.CreateDate < '09/02/2020'
							  ORDER BY r2.UpdateDate DESC)
	  AND ma.UpdateDate > @BalanceDate
	  AND m.IsRollupAssetClass = 0
	GROUP BY ma.InvestmentManagerAccountID, ma.ManagerAccountNumber, ma.ProxyValueDate, ma.ProxyValue, r.ClientInvestmentAccountID
	HAVING ma.ProxyValue != SUM(m.ProxyValueAllocation)
) x ON m.InvestmentManagerAccountID = x.InvestmentManagerAccountID
	 INNER JOIN InvestmentAccount ca ON x.ClientInvestmentAccountID = ca.InvestmentAccountID
WHERE ca.AccountNumber IN ('386000', '392000')
