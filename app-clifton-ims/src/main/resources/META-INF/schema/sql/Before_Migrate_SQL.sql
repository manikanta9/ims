-- WARNING: This file gets executed on PROD. It is run immediately before DB migration is executed (dbMigrateRestore & dbMigrate). The only current use case is to rename migration files.
--------------------------------------------------------------------------------
------              Migrations table updates                            --------
--------------------------------------------------------------------------------

-- "clifton-archive" migrations have been updated to "clifton-archive-server" in CORE-460
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES t
		  WHERE t.TABLE_SCHEMA = 'env'
			AND t.TABLE_NAME = 'MigrationModuleVersion')
	BEGIN
		IF EXISTS(SELECT *
				  FROM env.MigrationModuleVersion mmv
				  WHERE mmv.MigrationPath LIKE 'clifton-archive-__/%')
			AND NOT EXISTS(SELECT *
						   FROM env.MigrationModuleVersion mmv
						   WHERE mmv.MigrationPath LIKE 'clifton-archive-server-%/%')
			BEGIN
				UPDATE mmv
				SET mmv.MigrationModuleName = 'clifton-archive-server',
					mmv.MigrationPath       = REPLACE(mmv.MigrationPath, 'clifton-archive-', 'clifton-archive-server-')
				FROM env.MigrationModuleVersion mmv
				WHERE mmv.MigrationPath LIKE 'clifton-archive-__/%'

				SELECT *
				FROM env.MigrationModuleVersion
			END
	END

-- Some database instances may still be using the old migration schema
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES t
		  WHERE t.TABLE_SCHEMA = 'dbo'
			AND t.TABLE_NAME = 'Migration')
	BEGIN
		IF NOT EXISTS(SELECT *
					  FROM Migration m
					  WHERE m.MigrationPath LIKE 'clifton-archive-server-%/%')
			BEGIN
				UPDATE m
				SET m.MigrationModuleName = 'clifton-archive-server',
					m.MigrationPath       = REPLACE(m.MigrationPath, 'clifton-archive-', 'clifton-archive-server-')
				FROM Migration m
				WHERE m.MigrationPath LIKE 'clifton-archive-__/%'

				SELECT *
				FROM Migration
			END
	END
