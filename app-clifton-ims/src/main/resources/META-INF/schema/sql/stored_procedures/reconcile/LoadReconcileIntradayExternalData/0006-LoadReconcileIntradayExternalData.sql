
ALTER PROC [Reconcile].[LoadReconcileIntradayExternalData]
	@TradeDate DATE
AS
BEGIN
	DECLARE @DataSourceID INT, @PriceDataSourceID INT, @IntegrationImportRunID INT

	SELECT TOP 1 @IntegrationImportRunID = irph.IntegrationImportRunID
	FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
	WHERE irph.PositionDate = @TradeDate

	SELECT @DataSourceID = MarketDataSourceID
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN MarketDataSource mds ON mds.DataSourceName = iid.DataSourceName	
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID


	SELECT @PriceDataSourceID = coalesce(mdsp.MarketDataSourceID, mds.MarketDataSourceID)
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = iid.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN MarketDataSource mds ON mds.DataSourceName = bc.CompanyName
		LEFT JOIN BusinessCompany bcp ON bc.ParentBusinessCompanyID = bcp.BusinessCompanyID
		LEFT JOIN MarketDataSource mdsp ON mdsp.DataSourceName = bcp.CompanyName
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
	
	DECLARE @Data TABLE(
		[HoldingInvestmentAccountID] [int],
		[ClientInvestmentAccountID] [int],
		[HoldingInvestmentAccountNumber] [nvarchar](50),
		[SourceCompanyID] [int],
		[PayingSecurityID] [int],
		[InvestmentSecurityID] [int],
		[IsBuy] [bit],
		[TradeDate] [datetime],
		[Quantity] [decimal](19, 10),
		[Price] [decimal](28, 15),
		[TradeExternalStatus] [nvarchar](50),
		[ExternalUniqueTradeId] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CS_AS,
		[SourceName] [nvarchar](50),
		[Type] [nvarchar](50))

	INSERT INTO @Data
	SELECT
		HoldingInvestmentAccountID = Reconcile.GetHoldingAccount(t.AccountNumber),
		ClientInvestmentAccountID = 
		(
			SELECT TOP 1 ia2.InvestmentAccountID
			FROM InvestmentAccount ia2
				INNER JOIN InvestmentAccountRelationship iar ON iar.MainAccountID = ia2.InvestmentAccountID
				INNER JOIN InvestmentAccountRelationshipPurpose iarp ON iarp.InvestmentAccountRelationshipPurposeID = iar.InvestmentAccountRelationshipPurposeID
				INNER JOIN InvestmentAccountType iat ON iat.InvestmentAccountTypeID = ia2.InvestmentAccountTypeID
				INNER JOIN InvestmentAccount iaRelated ON iaRelated.InvestmentAccountID = iar.RelatedAccountID 
				INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iaRelated.IssuingCompanyID
				INNER JOIN WorkflowState ws ON ws.WorkflowStateID = ia2.WorkflowStateID
			WHERE iarp.RelationshipPurposeName IN ('Trading: Futures')
				AND WorkflowStateName IN ('Active','Open')
				AND iat.IsOurAccount = 1 AND iar.RelatedAccountID = Reconcile.GetHoldingAccount(t.AccountNumber)
		),
		t.AccountNumber AS HoldingInvestmentAccountNumber,
		SourceCompanyID = bc.BusinessCompanyID,
		PayingSecurityID = dbo.InvestmentSecurityLookup(t.PayingSecuritySymbol, @DataSourceID),
		InvestmentSecurityID = dbo.InvestmentSecurityLookup(t.InvestmentSecuritySymbol, @DataSourceID),
		t.IsBuy,
		t.TradeDate,
		t.Quantity,
		t.Price * coalesce(Reconcile.GetMarketDataSourcePriceMultiplier(coalesce(mdsp.MarketDataSourceID, mds.MarketDataSourceID), dbo.InvestmentSecurityLookup(t.InvestmentSecuritySymbol, @DataSourceID)),1),
		CASE WHEN t.Status = 'MATCHED' THEN 'CONFIRMED' ELSE 'UNCONFIRMED' END  as TradeExternalStatus,
		t.UniqueTradeId AS ExternalUniqueTradeId,
		bc.CompanyName AS SourceName,
		t.Type as Type
	FROM CliftonIntegration.dbo.IntegrationTradeIntraday t
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = t.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = iid.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		
		INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN MarketDataSource mds ON mds.DataSourceName = bc.CompanyName
		LEFT JOIN BusinessCompany bcp ON bc.ParentBusinessCompanyID = bcp.BusinessCompanyID
		LEFT JOIN MarketDataSource mdsp ON mdsp.DataSourceName = bcp.CompanyName
	WHERE TradeDate = @TradeDate
	
	

	DELETE FROM ReconcileTradeIntradayMatch
	WHERE ReconcileTradeIntradayExternalID IN
	(
		SELECT ReconcileTradeIntradayExternalID
		FROM ReconcileTradeIntradayExternal r
			LEFT JOIN @Data d ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId AND r.TradeDate = d.TradeDate
		WHERE r.TradeDate = @TradeDate AND (d.ExternalUniqueTradeId IS NULL OR d.Type = 'DELETED')
	)

	DELETE FROM r
	FROM ReconcileTradeIntradayExternal r
		LEFT JOIN @Data d ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId  AND r.TradeDate = d.TradeDate
	WHERE r.TradeDate = @TradeDate AND (d.ExternalUniqueTradeId IS NULL OR d.Type = 'DELETED')


	INSERT INTO ReconcileTradeIntradayExternal
	(
		HoldingInvestmentAccountID,
		ClientInvestmentAccountID,
		HoldingInvestmentAccountNumber,
		PayingSecurityID,
		InvestmentSecurityID,
		IsBuy,
		TradeDate,
		Quantity,
		Price,
		TradeExternalStatus,
		ExternalUniqueTradeId,
		SourceName,
		CreateUserID,
		CreateDate,
		UpdateUserID,
		UpdateDate
	)
	SELECT
		d.HoldingInvestmentAccountID,
		d.ClientInvestmentAccountID,
		d.HoldingInvestmentAccountNumber,
		d.PayingSecurityID,
		d.InvestmentSecurityID,
		d.IsBuy,
		d.TradeDate,
		d.Quantity,
		d.Price,
		d.TradeExternalStatus,
		d.ExternalUniqueTradeId,
		d.SourceName,
		0,
		GETDATE(),
		0,
		GETDATE()
	FROM @Data d
		LEFT JOIN ReconcileTradeIntradayExternal r ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId AND r.TradeDate = d.TradeDate
	WHERE r.ExternalUniqueTradeId IS NULL AND d.Type <> 'DELETED'
		
		-- Only load if SourceCompany is clearing broker for the trade, or if SourceCompany is executing broker and the import for the holdingCompany is disabled
		AND (d.SourceCompanyID = (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
			OR (d.SourceCompanyID <> (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
				AND NOT EXISTS(
					SELECT i.IntegrationImportDefinitionID 
					FROM CliftonIntegration.dbo.IntegrationImportDefinition i 
						INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON i.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinitionType t ON i.IntegrationImportDefinitionTypeID = t.IntegrationImportDefinitionTypeID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
						INNER JOIN BusinessCompany c ON s.SourceCompanyId = c.BusinessCompanyID
						INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
					WHERE d.HoldingInvestmentAccountID = a.InvestmentAccountID AND t.ImportDefinitionTypeName = 'Broker Import Trade Intraday' AND i.IsDisabled = 0))
		)


	UPDATE r SET
		r.HoldingInvestmentAccountID = d.HoldingInvestmentAccountID,
		r.ClientInvestmentAccountID = d.ClientInvestmentAccountID,
		r.HoldingInvestmentAccountNumber = d.HoldingInvestmentAccountNumber,
		r.PayingSecurityID = d.PayingSecurityID,
		r.InvestmentSecurityID = d.InvestmentSecurityID,
		r.IsBuy = d.IsBuy,
		r.TradeDate = d.TradeDate,
		r.Quantity = d.Quantity,
		r.Price = d.Price,
		r.TradeExternalStatus = d.TradeExternalStatus,
		r.ExternalUniqueTradeId = d.ExternalUniqueTradeId,
		r.SourceName = d.SourceName,
		UpdateUserID = 0,
		UpdateDate = GETDATE()
	FROM @Data d
		INNER JOIN ReconcileTradeIntradayExternal r ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId AND r.TradeDate = d.TradeDate
	WHERE  d.Type <> 'DELETED'

		-- Only load if SourceCompany is clearing broker for the trade, or if SourceCompany is executing broker and the import for the holdingCompany is disabled
		AND (d.SourceCompanyID = (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
			OR (d.SourceCompanyID <> (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
				AND NOT EXISTS(
					SELECT i.IntegrationImportDefinitionID 
					FROM CliftonIntegration.dbo.IntegrationImportDefinition i 
						INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON i.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinitionType t ON i.IntegrationImportDefinitionTypeID = t.IntegrationImportDefinitionTypeID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
						INNER JOIN BusinessCompany c ON s.SourceCompanyId = c.BusinessCompanyID
						INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
					WHERE d.HoldingInvestmentAccountID = a.InvestmentAccountID AND t.ImportDefinitionTypeName = 'Broker Import Trade Intraday' AND i.IsDisabled = 0))
		)
		
	-- Error handling to indicate when we cannot join to BusinessCompany, and hence cannot tell if the import is disabled for that company
	SELECT uuid = NEWID(), 
		ImportDefinitionName = (
			SELECT iid.ImportDefinitionName
			FROM CliftonIntegration.dbo.IntegrationImportRun iir
				INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		),
		'SourceCompanyID for IntegrationImportSource "' + s.ImportSourceName + '"' as FieldValue, 
		'No SourceCompany defined for IntegrationImportSource "' + s.ImportSourceName + 
			'." External Trades may not have loaded for this Run.' as Error
	FROM CliftonIntegration.dbo.IntegrationImportDefinition i 
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON i.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinitionType t ON i.IntegrationImportDefinitionTypeID = t.IntegrationImportDefinitionTypeID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
		LEFT JOIN BusinessCompany c ON s.SourceCompanyId = c.BusinessCompanyID
	WHERE 
		t.ImportDefinitionTypeName = 'Broker Import Trade Intraday' AND c.BusinessCompanyID IS NULL
END
