--exec reconcile.LoadReconcilePositionExternalDailyPNLData '7/29/11'
ALTER PROC reconcile.LoadReconcilePositionExternalDailyPNLData
	@PositionDate DATETIME
AS
BEGIN
	DECLARE @DataSourceID INT, @IntegrationImportRunID INT

	SELECT TOP 1 @IntegrationImportRunID = irph.IntegrationImportRunID
	FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
	WHERE irph.PositionDate = @PositionDate

	SELECT @DataSourceID = MarketDataSourceID
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN MarketDataSource mds ON mds.DataSourceName = iid.DataSourceName	
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
	
	DELETE FROM ReconcilePositionExternalDailyPNL WHERE PositionDate = @PositionDate		
		
	INSERT INTO ReconcilePositionExternalDailyPNL
	(
		[HoldingAccountID]
		,[ClientAccountID]
		,[InvestmentSecurityID]
		,[InvestmentCurrencyID]
		,[BaseCurrencyID]
		,[PositionDate]
		,[FxRate]
		,[Quantity]
		,[PriorQuantity]
		,[TodayClosedQuantity]
		,[IsShortPosition]
		,[OpenTradeEquityLocal]
		,[OpenTradeEquityBase]
		,[PriorOpenTradeEquityLocal]
		,[PriorOpenTradeEquityBase]
		,[TodayCommissionLocal]
		,[TodayCommissionBase]
		,[TodayRealizedGainLossLocal]
		,[TodayRealizedGainLossBase]
		,[CreateUserID]
		,[CreateDate]
		,[UpdateUserID]
		,[UpdateDate]
	)
	SELECT
		HoldingAccountID =ia.InvestmentAccountID,
		ClientAccountID = iaClient.InvestmentAccountID,
		InvestmentSecurityID = 
			coalesce(dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID), 'cannot find security - ' + irph.SecuritySymbol),
		InvestmentCurrencyID = posCurr.InvestmentSecurityID,
		BaseCurrencyID = actCurr.InvestmentSecurityID,
		@PositionDate,
		irph.FxRate,
		ABS(irph.Quantity),
		ABS(irph.PriorQuantity),
		ABS(irph.TodayClosedQuantity),
		irph.IsShortPosition,
		irph.OpenTradeEquityLocal,
		irph.OpenTradeEquityBase,
		irph.PriorOpenTradeEquityLocal,
		irph.PriorOpenTradeEquityBase,
		ABS(irph.TodayCommissionLocal),
		ABS(irph.TodayCommissionBase),
		irph.TodayRealizedGainLossLocal,
		irph.TodayRealizedGainLossBase,
		0,
		GETDATE(),
		0,
		GETDATE()
	FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
			AND (ia.IssuingCompanyID = iid.SourceCompanyId OR iid.SourceCompanyId IS NULL)
		LEFT JOIN InvestmentAccount iaClient ON iaClient.AccountNumber = irph.ClientAccountNumber
		LEFT JOIN InvestmentSecurity posCurr ON posCurr.Symbol = irph.PositionCurrencySymbol
		LEFT JOIN InvestmentSecurity actCurr ON actCurr.Symbol = irph.AccountCurrencySymbol
	WHERE EffectiveDate = @PositionDate
	
END
		
