--USED ONLY AS A SUB-PROCEDURE OF [Reconcile].[LoadReconcilePositionDefinitionData]

CREATE PROC [Reconcile].[PopulateReconcilePositionDailyData]

	@PositionDate DATETIME,
	@ReconcilePositionDefinitionID INT,
	@InvestmentSecurityID INT = NULL,
	@HoldingInvestmentAccountID INT = NULL

AS
BEGIN
	--DECLARE @ReconcilePositionDefinitionID INT, @InvestmentGroupID INT, @HoldingAccountID INT, @PositionDate DATETIME
	DECLARE @InvestmentGroupID INT
	
	SELECT 
		@InvestmentGroupID = InvestmentGroupID
	FROM ReconcilePositionDefinition  rpd
	WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

	INSERT INTO #ReconcilePosition
	(
		HoldingAccountID,
		InvestmentSecurityID,
		ReconcilePositionAccountID,
		ReconcilePositionID,
		AccountingAccountID,
		OurPrice,
		OurFxRate,
		OurQuantity,
		OurPriorQuantity,
		OurTodayClosedQuantity,
		OurMarketValueLocal,
		OurMarketValueBase,
		OurOpenTradeEquityLocal,
		OurOpenTradeEquityBase,
		OurPriorOpenTradeEquityLocal,
		OurPriorOpenTradeEquityBase,
		OurTodayCommissionLocal,
		OurTodayRealizedGainLossLocal,
		IsMatched
	)
	SELECT
		HoldingInvestmentAccountID,
		s.InvestmentSecurityID,
		MAX(rp.ReconcilePositionAccountID),
		rp.ReconcilePositionID,
		MAX(t.AccountingAccountID),
		AVG(MarketPrice),
		AVG(MarketFxRate),
		SUM(RemainingQuantity),
		SUM(PriorQuantity),
		SUM(ABS(apd.TodayClosedQuantity)),
		SUM(NotionalValueLocal),
		SUM(NotionalValueBase),
		SUM(OpenTradeEquityLocal),
		SUM(OpenTradeEquityBase),
		SUM(PriorOpenTradeEquityLocal),
		SUM(PriorOpenTradeEquityBase),
		SUM(TodayCommissionLocal),
		SUM(TodayRealizedGainLossLocal),
		0
	FROM AccountingPositionDaily apd
		INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = apd.AccountingTransactionID
		INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = t.InvestmentSecurityID
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
		LEFT JOIN ReconcilePositionAccount rpa ON rpa.HoldingAccountID = t.HoldingInvestmentAccountID 
				AND rpa.PositionDate = apd.PositionDate 
				AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
		LEFT JOIN ReconcilePosition rp ON rp.InvestmentSecurityID = s.InvestmentSecurityID AND
			 rp.ReconcilePositionAccountID = rpa.ReconcilePositionAccountID
	WHERE apd.PositionDate = @PositionDate
		--AND apd.RemainingQuantity <> 0
		--AND (rpa.ReconcilePositionAccountID IS NOT NULL OR (rp.ReconcilePositionID IS NULL AND rpa.ReconcilePositionAccountID IS NULL))
		AND (s.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		AND (s.InvestmentSecurityID IN (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s 
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
				INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
			WHERE ig.InvestmentGroupID = @InvestmentGroupID) )
		AND (@HoldingInvestmentAccountID IS NULL OR HoldingInvestmentAccountID = @HoldingInvestmentAccountID)
	GROUP BY HoldingInvestmentAccountID, s.InvestmentSecurityID, rp.ReconcilePositionID
	 
	
	UPDATE r SET
		ExternalFxRate = ext.ExternalFxRate,
		ExternalPrice = ext.ExternalPrice,
		ExternalQuantity = ext.ExternalQuantity,
		ExternalMarketValueLocal = ext.ExternalMarketValueLocal,
		ExternalMarketValueBase = ext.ExternalMarketValueBase,
		ExternalOpenTradeEquityLocal = ext.ExternalOpenTradeEquityLocal,
		ExternalOpenTradeEquityBase = ext.ExternalOpenTradeEquityBase,
		IsMatched = 1
	FROM #ReconcilePosition r
		INNER JOIN
		(
			SELECT
				--rp.ReconcilePositionID, --MAX(rpCurrent.ReconcilePositionID) as ReconcilePositionID,
				apd.HoldingAccountID,
				apd.InvestmentSecurityID,
				ExternalFxRate = AVG(FxRate),
				ExternalQuantity = SUM(Quantity),
				ExternalPrice = AVG(MarketPrice),
				ExternalMarketValueLocal = SUM(MarketValueLocal),
				ExternalMarketValueBase = SUM(MarketValueBase),
				ExternalOpenTradeEquityLocal = SUM(OpenTradeEquityLocal),
				ExternalOpenTradeEquityBase = SUM(OpenTradeEquityBase)
			FROM ReconcilePositionExternalDaily apd
				INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = apd.InvestmentSecurityID
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN #ReconcilePosition rp ON rp.InvestmentSecurityID = s.InvestmentSecurityID AND apd.HoldingAccountID = rp.HoldingAccountID			
			WHERE apd.PositionDate = @PositionDate
				--AND rp.ReconcilePositionID IS NOT NULL
				AND (apd.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
				AND (s.InvestmentSecurityID IN (
					SELECT InvestmentSecurityID
					FROM InvestmentSecurity s 
						INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
						INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
						INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
						INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
					WHERE ig.InvestmentGroupID = @InvestmentGroupID) OR @InvestmentGroupID IS NULL)
			GROUP BY apd.HoldingAccountID, apd.InvestmentSecurityID--, rp.ReconcilePositionID
			
		) ext ON r.HoldingAccountID = ext.HoldingAccountID AND r.InvestmentSecurityID = ext.InvestmentSecurityID
    
   
   
    INSERT INTO #ReconcilePosition 
	(
		HoldingAccountID,
		ClientAccountID,
		ReconcilePositionAccountID,
		ReconcilePositionID,
		InvestmentSecurityID,
		ExternalFxRate,
		ExternalQuantity,
		ExternalPrice,
		ExternalMarketValueLocal,
		ExternalMarketValueBase,
		ExternalOpenTradeEquityLocal,
		ExternalOpenTradeEquityBase,
		IsMatched
	)
	SELECT
		apd.HoldingAccountID,
		MAX(apd.ClientAccountID),
		MAX(rpa.ReconcilePositionAccountID),
		MAX(rpCurrent.ReconcilePositionID),
		apd.InvestmentSecurityID,
		ExternalFxRate = AVG(FxRate),
		ExternalQuantity = SUM(Quantity),
		ExternalPrice = AVG(MarketPrice),
		ExternalMarketValueLocal = SUM(MarketValueLocal),
		ExternalMarketValueBase = SUM(MarketValueBase),
		ExternalOpenTradeEquityLocal = SUM(OpenTradeEquityLocal),
		ExternalOpenTradeEquityBase = SUM(OpenTradeEquityBase),
		0
	FROM ReconcilePositionExternalDaily apd
		INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = apd.InvestmentSecurityID
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
		LEFT JOIN #ReconcilePosition rp ON rp.InvestmentSecurityID = s.InvestmentSecurityID AND apd.HoldingAccountID = rp.HoldingAccountID
		LEFT JOIN ReconcilePositionAccount rpa ON rpa.HoldingAccountID = apd.HoldingAccountID 
					AND rpa.PositionDate = apd.PositionDate 
					AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID 
		LEFT JOIN ReconcilePosition rpCurrent ON rpCurrent.InvestmentSecurityID = s.InvestmentSecurityID 
					AND rpCurrent.ReconcilePositionAccountID = rpa.ReconcilePositionAccountID
	WHERE apd.PositionDate = @PositionDate
		AND rp.InvestmentSecurityID IS NULL
		AND ((rpa.ReconcilePositionAccountID IS NULL OR rp.ReconcilePositionID IS NULL) OR
			(rpa.ReconcilePositionAccountID IS NOT NULL OR rp.ReconcilePositionID IS NOT NULL))
		AND (apd.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		AND (s.InvestmentSecurityID IN (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s 
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
				INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
			WHERE ig.InvestmentGroupID = @InvestmentGroupID) OR @InvestmentGroupID IS NULL)
		AND (@HoldingInvestmentAccountID IS NULL OR apd.HoldingAccountID = @HoldingInvestmentAccountID)
	GROUP BY apd.HoldingAccountID, apd.InvestmentSecurityID--, rpCurrent.ReconcilePositionID
	
	
	
	UPDATE rp SET
		ClientAccountID = pnl.ClientAccountID,
		ExternalPriorQuantity = pnl.PriorQuantity,
		ExternalTodayClosedQuantity = pnl.TodayClosedQuantity,
		ExternalPriorOpenTradeEquityLocal = pnl.PriorOpenTradeEquityLocal,
		ExternalPriorOpenTradeEquityBase = pnl.PriorOpenTradeEquityBase,
		ExternalTodayCommissionLocal = pnl.TodayCommissionLocal,
		ExternalTodayRealizedGainLossLocal = pnl.TodayRealizedGainLossLocal,
		ExternalFxRate = COALESCE(rp.ExternalFxRate, pnl.FxRate),
		ExternalPrice = COALESCE(rp.ExternalPrice, pnl.MarketPrice)
	FROM #ReconcilePosition rp
		INNER JOIN ReconcilePositionExternalDailyPNL pnl ON pnl.HoldingAccountID = rp.HoldingAccountID
			AND pnl.InvestmentSecurityID = rp.InvestmentSecurityID AND pnl.PositionDate = @PositionDate
	
	RETURN
END
