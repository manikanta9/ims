--exec reconcile.LoadReconcilePositionExternalDailyPNLData '5/9/12'
ALTER PROC reconcile.LoadReconcilePositionExternalDailyPNLData
	@IntegrationImportRunID INT
AS
BEGIN
	DECLARE @DataSourceID INT, @SourceCompanyID INT, @PositionDate DATE

	SELECT @SourceCompanyID = iis.SourceCompanyId, @PositionDate = iir.EffectiveDate FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID

	SELECT @DataSourceID = MarketDataSourceID
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN MarketDataSource mds ON mds.DataSourceName = iid.DataSourceName
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
	
	--DELETE FROM ReconcilePositionExternalDaily WHERE PositionDate = @PositionDate	
	DELETE FROM r FROM ReconcilePositionExternalDailyPNL r
		INNER JOIN InvestmentAccount ha ON ha.InvestmentAccountID = r.HoldingAccountID
	WHERE ha.IssuingCompanyID = @SourceCompanyID AND PositionDate = @PositionDate
		
	INSERT INTO ReconcilePositionExternalDailyPNL
	(
		[HoldingAccountID]
		,[ClientAccountID]
		,[InvestmentSecurityID]
		,[InvestmentCurrencyID]
		,[BaseCurrencyID]
		,[PositionDate]
		,[FxRate]
		,[Quantity]
		,[PriorQuantity]
		,[TodayClosedQuantity]
		,[IsShortPosition]
		,[OpenTradeEquityLocal]
		,[OpenTradeEquityBase]
		,[PriorOpenTradeEquityLocal]
		,[PriorOpenTradeEquityBase]
		,[TodayCommissionLocal]
		,[TodayRealizedGainLossLocal]
	)
	SELECT
		HoldingAccountID =ia.InvestmentAccountID,
		ClientAccountID = iaClient.InvestmentAccountID,
		InvestmentSecurityID = dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID),
		InvestmentCurrencyID = posCurr.InvestmentSecurityID,
		BaseCurrencyID = actCurr.InvestmentSecurityID,
		@PositionDate,
		irph.FxRate,
		ABS(irph.Quantity),
		ABS(irph.PriorQuantity),
		ABS(irph.TodayClosedQuantity),
		irph.IsShortPosition,
		irph.OpenTradeEquityLocal,
		irph.OpenTradeEquityBase,
		irph.PriorOpenTradeEquityLocal,
		irph.PriorOpenTradeEquityBase,
		ABS(irph.TodayCommissionLocal),
		irph.TodayRealizedGainLossLocal
	FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		INNER JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
			AND (ia.IssuingCompanyID = iis.SourceCompanyId OR iis.SourceCompanyId IS NULL)
		LEFT JOIN InvestmentAccount iaClient ON iaClient.AccountNumber = irph.ClientAccountNumber
		LEFT JOIN InvestmentSecurity posCurr ON posCurr.Symbol = irph.PositionCurrencySymbol
		LEFT JOIN InvestmentSecurity actCurr ON actCurr.Symbol = irph.AccountCurrencySymbol
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		AND dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID) IS NOT NULL
	
	
	SELECT uuid = NEWID(), iid.ImportDefinitionName, irph.SecuritySymbol as FieldValue, 'cannot find security - ' + irph.SecuritySymbol as Error
	FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND dbo.InvestmentSecurityLookup(irph.SecuritySymbol, 1) IS NULL
	UNION ALL
	SELECT uuid = NEWID(), iid.ImportDefinitionName, irph.AccountNumber as FieldValue, 'cannot find account - ' + irph.AccountNumber as Error
	FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		LEFT JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
			AND (ia.IssuingCompanyID = iis.SourceCompanyId OR iis.SourceCompanyId IS NULL)
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND ia.InvestmentAccountID IS NULL
END
		
