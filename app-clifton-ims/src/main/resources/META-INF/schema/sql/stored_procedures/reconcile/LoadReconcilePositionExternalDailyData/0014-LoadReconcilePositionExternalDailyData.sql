--exec reconcile.LoadReconcilePositionExternalDailyData '5/9/12'
ALTER PROC reconcile.LoadReconcilePositionExternalDailyData
	@IntegrationImportRunID INT,
	@PriceMarketDataSourcePurposeID SMALLINT
AS
BEGIN
	DECLARE @DataSourceID INT, @PriceDataSourceID INT, @SourceCompanyId INT, @PositionDate DATETIME
	
	SELECT
		@SourceCompanyID = iis.SourceCompanyId,
		@PositionDate = iir.EffectiveDate
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID

	SELECT @DataSourceID = MarketDataSourceID
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN MarketDataSource mds ON mds.DataSourceName = iid.DataSourceName	
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
	
	SELECT @PriceDataSourceID = COALESCE(mdsp.MarketDataSourceID, mds.MarketDataSourceID)
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN MarketDataSource mds ON mds.DataSourceName = bc.CompanyName
		LEFT JOIN BusinessCompany bcp ON bc.ParentBusinessCompanyID = bcp.BusinessCompanyID
		LEFT JOIN MarketDataSource mdsp ON mdsp.DataSourceName = bcp.CompanyName
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
	
	
	--DELETE FROM ReconcilePositionExternalDaily WHERE PositionDate = @PositionDate	
	DELETE FROM r FROM ReconcilePositionExternalDaily r
		INNER JOIN InvestmentAccount ha ON ha.InvestmentAccountID = r.HoldingAccountID
	WHERE ha.IssuingCompanyID = @SourceCompanyID AND PositionDate = @PositionDate

	INSERT INTO ReconcilePositionExternalDaily
	(
		[HoldingAccountID]
		,[ClientAccountID]
		,[InvestmentSecurityID]
		,[InvestmentCurrencyID]
		,[BaseCurrencyID]
		,[PositionDate]
		,[TradeDate]
		,[SettleDate]
		,[FxRate]
		,[Quantity]
		,[IsShortPosition]
		,[TradePrice]
		,[MarketPrice]
		,[RemainingCostBasisLocal]
		,[RemainingCostBasisBase]
		,[MarketValueLocal]
		,[MarketValueBase]
		,[OpenTradeEquityLocal]
		,[OpenTradeEquityBase]
	)
	SELECT
		HoldingAccountID = ia.InvestmentAccountID,
		ClientAccountID = iaClient.InvestmentAccountID,
		InvestmentSecurityID = dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0),
		InvestmentCurrencyID = dbo.InvestmentSecurityLookup(irph.PositionCurrencySymbol, @DataSourceID, 1),
		BaseCurrencyID = dbo.InvestmentSecurityLookup(irph.AccountCurrencySymbol, @DataSourceID, 1),
		irph.PositionDate,
		irph.TradeDate,
		irph.SettleDate,
		irph.FxRate,
		CASE WHEN irph.IsShortPosition = 1 THEN -1 * irph.Quantity ELSE irph.Quantity END,
		irph.IsShortPosition,
		irph.TradePrice * MarketData.GetPriceMultiplier(COALESCE(mdsp.MarketDataSourceID, mds.MarketDataSourceID), dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0),@PriceMarketDataSourcePurposeID),
		irph.MarketPrice * MarketData.GetPriceMultiplier(COALESCE(mdsp.MarketDataSourceID, mds.MarketDataSourceID), dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0),@PriceMarketDataSourcePurposeID),
		irph.CostBasisLocal,
		irph.CostBasisBase,
		irph.MarketValueLocal,
		irph.MarketValueBase,
		irph.OpenTradeEquityLocal,
		irph.OpenTradeEquityBase
	FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		INNER JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
			AND (ia.IssuingCompanyID = iis.SourceCompanyId OR iis.SourceCompanyId IS NULL)
		LEFT JOIN InvestmentAccount iaClient ON iaClient.AccountNumber = irph.ClientAccountNumber
		
		INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN MarketDataSource mds ON mds.DataSourceName = bc.CompanyName
		LEFT JOIN BusinessCompany bcp ON bc.ParentBusinessCompanyID = bcp.BusinessCompanyID
		LEFT JOIN MarketDataSource mdsp ON mdsp.DataSourceName = bcp.CompanyName
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		AND dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0) IS NOT NULL
	
	
	SELECT uuid = NEWID(), iid.ImportDefinitionName, irph.SecuritySymbol as FieldValue, 'cannot find security - ' + irph.SecuritySymbol as Error
	FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND dbo.InvestmentSecurityLookup(irph.SecuritySymbol, 1, 0) IS NULL
	UNION ALL
	SELECT uuid = NEWID(), ImportDefinitionName, AccountNumber as FieldValue, 'cannot find account - ' + AccountNumber as Error
	FROM
	(
		SELECT iid.ImportDefinitionName, irph.AccountNumber
		FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
			INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
			INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
			INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
			LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
			LEFT JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
				AND (ia.IssuingCompanyID = iis.SourceCompanyId OR iis.SourceCompanyId IS NULL)
		WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND ia.InvestmentAccountID IS NULL
		GROUP BY iid.ImportDefinitionName, irph.AccountNumber
	) x
END
		
