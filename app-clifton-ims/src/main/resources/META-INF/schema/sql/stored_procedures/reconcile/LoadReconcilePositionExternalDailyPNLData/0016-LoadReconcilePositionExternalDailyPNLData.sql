
--exec reconcile.LoadReconcilePositionExternalDailyPNLData '5/9/12'
ALTER PROC [Reconcile].[LoadReconcilePositionExternalDailyPNLData]
	@IntegrationImportRunID INT,
	@PriceMarketDataSourcePurposeID SMALLINT
AS
BEGIN
	DECLARE @DataSourceID INT, @SourceCompanyID INT, @PositionDate DATE

	SELECT 
		@SourceCompanyID = iis.SourceCompanyId,
		@PositionDate = iir.EffectiveDate
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationSource iis ON iis.IntegrationSourceID = fd.IntegrationSourceID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID

	SELECT @DataSourceID = MarketDataSourceID
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN MarketDataSource mds ON mds.DataSourceName = iid.DataSourceName
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID

	--DELETE FROM ReconcilePositionExternalDaily WHERE PositionDate = @PositionDate	
	DELETE FROM r FROM ReconcilePositionExternalDailyPNL r
		INNER JOIN InvestmentAccount ha ON ha.InvestmentAccountID = r.HoldingAccountID
	WHERE PositionDate = @PositionDate
		AND ha.IssuingCompanyID IN (
			SELECT ia.IssuingCompanyID
			FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
				INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
				INNER JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
			WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
			GROUP BY ia.IssuingCompanyID
		)
		
	INSERT INTO ReconcilePositionExternalDailyPNL
	(
		[HoldingAccountID]
		,[ClientAccountID]
		,[InvestmentSecurityID]
		,[InvestmentCurrencyID]
		,[BaseCurrencyID]
		,[PositionDate]
		,[FxRate]
		,[MarketPrice]
		,[Quantity]
		,[PriorQuantity]
		,[TodayClosedQuantity]
		,[IsShortPosition]
		,[OpenTradeEquityLocal]
		,[OpenTradeEquityBase]
		,[PriorOpenTradeEquityLocal]
		,[PriorOpenTradeEquityBase]
		,[TodayCommissionLocal]
		,[TodayRealizedGainLossLocal]
	)
	SELECT
		HoldingAccountID =ia.InvestmentAccountID,
		ClientAccountID = iaClient.InvestmentAccountID,
		InvestmentSecurityID = dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0),
		InvestmentCurrencyID = dbo.InvestmentSecurityLookup(irph.PositionCurrencySymbol, @DataSourceID, 1),
		BaseCurrencyID = dbo.InvestmentSecurityLookup(irph.AccountCurrencySymbol, @DataSourceID, 1),
		
		@PositionDate,
		irph.FxRate,
		irph.MarketPrice * MarketData.GetPriceMultiplier(COALESCE(mdsp.MarketDataSourceID, mds.MarketDataSourceID), dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0), @PriceMarketDataSourcePurposeID),
		ABS(irph.Quantity),
		ABS(irph.PriorQuantity),
		ABS(irph.TodayClosedQuantity),
		irph.IsShortPosition,
		irph.OpenTradeEquityLocal,
		irph.OpenTradeEquityBase,
		irph.PriorOpenTradeEquityLocal,
		irph.PriorOpenTradeEquityBase,
		ABS(irph.TodayCommissionLocal),
		irph.TodayRealizedGainLossLocal
	FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		LEFT JOIN CliftonIntegration.dbo.IntegrationSource iis ON iis.IntegrationSourceID = fd.IntegrationSourceID
		INNER JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
		LEFT JOIN InvestmentAccount iaClient ON iaClient.AccountNumber = irph.ClientAccountNumber
		
		INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN MarketDataSource mds ON mds.DataSourceName = bc.CompanyName
		LEFT JOIN BusinessCompany bcp ON bc.ParentBusinessCompanyID = bcp.BusinessCompanyID
		LEFT JOIN MarketDataSource mdsp ON mdsp.DataSourceName = bcp.CompanyName
		
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		AND dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID, 0) IS NOT NULL
		AND (iis.SourceCompanyId IS NULL
			OR ia.IssuingCompanyID = iis.SourceCompanyId
			OR ia.IssuingCompanyID IN
			(
				SELECT bc.ParentBusinessCompanyID
				FROM BusinessCompany bc
				WHERE bc.BusinessCompanyID = iis.SourceCompanyId
				UNION ALL
				SELECT bc.BusinessCompanyID
				FROM BusinessCompany bc
				WHERE bc.ParentBusinessCompanyID =
				(
					SELECT bc.ParentBusinessCompanyID
					FROM BusinessCompany bc
					WHERE bc.BusinessCompanyID = iis.SourceCompanyId
				)
			)
		)
	
	SELECT uuid = NEWID(), iid.ImportDefinitionName, irph.SecuritySymbol as FieldValue, 'cannot find security - ' + ISNULL(irph.SecuritySymbol, 'NULL') + ' for account ' + ISNULL(irph.AccountNumber, 'NULL') as Error
	FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND dbo.InvestmentSecurityLookup(irph.SecuritySymbol, 1, 0) IS NULL
	UNION ALL
	SELECT uuid = NEWID(), ImportDefinitionName, AccountNumber as FieldValue, 'cannot find account - ' + ISNULL(AccountNumber, 'NULL') as Error
	FROM
	(
		SELECT iid.ImportDefinitionName, irph.AccountNumber
		FROM CliftonIntegration.dbo.IntegrationReconcileDailyPNLHistory irph
			INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
			INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
			INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
			LEFT JOIN CliftonIntegration.dbo.IntegrationSource iis ON iis.IntegrationSourceID = fd.IntegrationSourceID
			LEFT JOIN InvestmentAccount ia ON (ia.AccountNumber = irph.AccountNumber OR ia.AccountNumber = REPLACE(irph.AccountNumber,'-',''))
		WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND ia.InvestmentAccountID IS NULL
			AND (iis.SourceCompanyId IS NULL
				OR ia.IssuingCompanyID = iis.SourceCompanyId
				OR ia.IssuingCompanyID IN
				(
					SELECT bc.ParentBusinessCompanyID
					FROM BusinessCompany bc
					WHERE bc.BusinessCompanyID = iis.SourceCompanyId
					UNION ALL
					SELECT bc.BusinessCompanyID
					FROM BusinessCompany bc
					WHERE bc.ParentBusinessCompanyID =
					(
						SELECT bc.ParentBusinessCompanyID
						FROM BusinessCompany bc
						WHERE bc.BusinessCompanyID = iis.SourceCompanyId
					)
				)
			)
		GROUP BY iid.ImportDefinitionName, irph.AccountNumber
	) x
END
		
