-- exec [Reconcile].[LoadReconcileIntradayExternalData] '10/1/12', NULL

ALTER PROC [Reconcile].[LoadReconcileIntradayExternalData]
	@TradeDate DATE,
	@IntegrationImportRunID INT
AS
BEGIN
	DECLARE @SourceCompanyId INT;
	
	SET @SourceCompanyId = (SELECT iis.SourceCompanyId
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationFile f ON f.IntegrationFileID = iir.IntegrationFileID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID)
	
	
	DECLARE @Data TABLE(
		[HoldingInvestmentAccountID] [int],
		[ClientInvestmentAccountID] [int],
		[HoldingInvestmentAccountNumber] [nvarchar](50),
		[SourceCompanyID] [int],
		[PayingSecurityID] [int],
		[InvestmentSecurityID] [int],
		[ExecutingCompanyID] [int],
		[IsBuy] [bit],
		[TradeDate] [datetime],
		[Quantity] [decimal](19, 10),
		[Price] [decimal](28, 15),
		[TradeExternalStatus] [nvarchar](50),
		[ExternalUniqueTradeId] [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CS_AS,
		[Type] [nvarchar](50))

	INSERT INTO @Data
	SELECT
		HoldingInvestmentAccountID = Reconcile.GetHoldingAccount(t.AccountNumber),
		ClientInvestmentAccountID = 
		(
			SELECT TOP 1 ia2.InvestmentAccountID
			FROM InvestmentAccount ia2
				INNER JOIN InvestmentAccountRelationship iar ON iar.MainAccountID = ia2.InvestmentAccountID
				INNER JOIN InvestmentAccountRelationshipPurpose iarp ON iarp.InvestmentAccountRelationshipPurposeID = iar.InvestmentAccountRelationshipPurposeID
				INNER JOIN InvestmentAccountType iat ON iat.InvestmentAccountTypeID = ia2.InvestmentAccountTypeID
				INNER JOIN InvestmentAccount iaRelated ON iaRelated.InvestmentAccountID = iar.RelatedAccountID 
				INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iaRelated.IssuingCompanyID
				INNER JOIN WorkflowState ws ON ws.WorkflowStateID = ia2.WorkflowStateID
			WHERE iarp.RelationshipPurposeName IN ('Trading: Futures')
				AND WorkflowStateName IN ('Active','Open')
				AND iat.IsOurAccount = 1 AND iar.RelatedAccountID = Reconcile.GetHoldingAccount(t.AccountNumber)
		),
		t.AccountNumber AS HoldingInvestmentAccountNumber,
		SourceCompanyID = bc.BusinessCompanyID,
		PayingSecurityID = dbo.InvestmentSecurityLookup(t.PayingSecuritySymbol, security_mds.MarketDataSourceID),
		InvestmentSecurityID = dbo.InvestmentSecurityLookup(t.InvestmentSecuritySymbol, security_mds.MarketDataSourceID),
		mdscp.BusinessCompanyID,
		t.IsBuy,
		t.TradeDate,
		t.Quantity,
		t.Price * coalesce(Reconcile.GetMarketDataSourcePriceMultiplier(coalesce(mdsp.MarketDataSourceID, mds.MarketDataSourceID), dbo.InvestmentSecurityLookup(t.InvestmentSecuritySymbol, security_mds.MarketDataSourceID)),1),
		t.Status,
		t.UniqueTradeId AS ExternalUniqueTradeId,
		t.Type as Type
	FROM CliftonIntegration.dbo.IntegrationTradeIntraday t
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = t.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition fd ON fd.IntegrationFileDefinitionID = iid.IntegrationFileDefinitionID
		INNER JOIN MarketDataSource security_mds ON security_mds.DataSourceName = iid.DataSourceName	
		
		LEFT JOIN CliftonIntegration.dbo.IntegrationImportSource iis ON iis.IntegrationImportSourceID = fd.IntegrationImportSourceID
		
		INNER JOIN BusinessCompany bc ON bc.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN MarketDataSource mds ON mds.BusinessCompanyID = iis.SourceCompanyId
		LEFT JOIN BusinessCompany bcp ON bc.ParentBusinessCompanyID = bcp.BusinessCompanyID
		LEFT JOIN MarketDataSource mdsp ON mdsp.DataSourceName = bcp.CompanyName
		LEFT JOIN MarketDataSourceCompanyMapping mdscp ON mdscp.MarketDataSourceID = COALESCE(mdsp.MarketDataSourceID, mds.MarketDataSourceID)
			AND ISNULL(mdscp.ExternalCompanyName, 1) = ISNULL(t.ExecutingBrokerCompanyName, 1)
	WHERE TradeDate = @TradeDate
		-- get all runs in the trade table for the definition
		AND (@SourceCompanyId IS NULL OR iis.SourceCompanyId = @SourceCompanyId)
	
	
	DELETE FROM m
	FROM ReconcileTradeIntradayMatch m
	INNER JOIN
	(
		SELECT TradeFillID, ReconcileTradeIntradayExternalID
		FROM ReconcileTradeIntradayMatch
		WHERE ReconcileTradeIntradayExternalID IN
		(
			SELECT ReconcileTradeIntradayExternalID
			FROM ReconcileTradeIntradayExternal r
				LEFT JOIN @Data d ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId AND r.TradeDate = d.TradeDate
			WHERE r.TradeDate = @TradeDate AND (d.ExternalUniqueTradeId IS NULL OR d.Type = 'DELETED')
				AND (@SourceCompanyId IS NULL OR r.SourceCompanyID = @SourceCompanyId)
		)
	) x ON m.TradeFillID = x.TradeFillID OR m.ReconcileTradeIntradayExternalID = x.ReconcileTradeIntradayExternalID
	
	
	DELETE FROM r
	FROM ReconcileTradeIntradayExternal r
		LEFT JOIN @Data d ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId  AND r.TradeDate = d.TradeDate
	WHERE r.TradeDate = @TradeDate AND (d.ExternalUniqueTradeId IS NULL OR d.Type = 'DELETED')
		AND (@SourceCompanyId IS NULL OR r.SourceCompanyID = @SourceCompanyId)


	INSERT INTO ReconcileTradeIntradayExternal
	(
		HoldingInvestmentAccountID,
		ClientInvestmentAccountID,
		HoldingInvestmentAccountNumber,
		PayingSecurityID,
		InvestmentSecurityID,
		ExecutingCompanyID,
		IsBuy,
		TradeDate,
		Quantity,
		Price,
		TradeExternalStatus,
		ExternalUniqueTradeId,
		SourceCompanyID
	)
	SELECT
		d.HoldingInvestmentAccountID,
		d.ClientInvestmentAccountID,
		d.HoldingInvestmentAccountNumber,
		d.PayingSecurityID,
		d.InvestmentSecurityID,
		d.ExecutingCompanyID,
		d.IsBuy,
		d.TradeDate,
		d.Quantity,
		d.Price,
		d.TradeExternalStatus,
		d.ExternalUniqueTradeId,
		d.SourceCompanyID
	FROM @Data d
		LEFT JOIN ReconcileTradeIntradayExternal r ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId AND r.TradeDate = d.TradeDate
			AND (@SourceCompanyId IS NULL OR r.SourceCompanyID = @SourceCompanyId)
	WHERE r.ExternalUniqueTradeId IS NULL AND d.Type <> 'DELETED'
		
		-- Only load if SourceCompany is clearing broker for the trade, or if SourceCompany is not clearing broker and the clearing company's intraday import is disabled
		AND (d.SourceCompanyID = (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
			OR (d.SourceCompanyID <> (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
				AND NOT EXISTS(
					SELECT i.IntegrationImportDefinitionID 
					FROM CliftonIntegration.dbo.IntegrationImportDefinition i 
						INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON i.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinitionType t ON i.IntegrationImportDefinitionTypeID = t.IntegrationImportDefinitionTypeID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
						INNER JOIN BusinessCompany c ON s.SourceCompanyId = c.BusinessCompanyID
						INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
					WHERE d.HoldingInvestmentAccountID = a.InvestmentAccountID AND t.ImportDefinitionTypeName = 'Broker Import Trade Intraday' AND i.IsDisabled = 0))
		)


	UPDATE r SET
		r.HoldingInvestmentAccountID = d.HoldingInvestmentAccountID,
		r.ClientInvestmentAccountID = d.ClientInvestmentAccountID,
		r.HoldingInvestmentAccountNumber = d.HoldingInvestmentAccountNumber,
		r.PayingSecurityID = d.PayingSecurityID,
		r.InvestmentSecurityID = d.InvestmentSecurityID,
		r.ExecutingCompanyID = d.ExecutingCompanyID,
		r.IsBuy = d.IsBuy,
		r.TradeDate = d.TradeDate,
		r.Quantity = d.Quantity,
		r.Price = d.Price,
		r.TradeExternalStatus = d.TradeExternalStatus,
		r.ExternalUniqueTradeId = d.ExternalUniqueTradeId,
		r.SourceCompanyID = d.SourceCompanyID
	FROM @Data d
		INNER JOIN ReconcileTradeIntradayExternal r ON r.ExternalUniqueTradeId = d.ExternalUniqueTradeId AND r.TradeDate = d.TradeDate
			AND (@SourceCompanyId IS NULL OR r.SourceCompanyID = @SourceCompanyId)
	WHERE  d.Type <> 'DELETED'
		
		-- Only load if SourceCompany is clearing broker for the trade, or if SourceCompany is not clearing broker and the clearing company's intraday import is disabled
		AND (d.SourceCompanyID = (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
			OR (d.SourceCompanyID <> (SELECT b.BusinessCompanyID FROM BusinessCompany b INNER JOIN InvestmentAccount a ON b.BusinessCompanyID = a.IssuingCompanyID WHERE a.InvestmentAccountID = d.HoldingInvestmentAccountID)
				AND NOT EXISTS(
					SELECT i.IntegrationImportDefinitionID 
					FROM CliftonIntegration.dbo.IntegrationImportDefinition i 
						INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON i.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinitionType t ON i.IntegrationImportDefinitionTypeID = t.IntegrationImportDefinitionTypeID
						INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
						INNER JOIN BusinessCompany c ON s.SourceCompanyId = c.BusinessCompanyID
						INNER JOIN InvestmentAccount a ON c.BusinessCompanyID = a.IssuingCompanyID
					WHERE d.HoldingInvestmentAccountID = a.InvestmentAccountID AND t.ImportDefinitionTypeName = 'Broker Import Trade Intraday' AND i.IsDisabled = 0))
		)
		
	
	
	SELECT uuid = NEWID(), iid.ImportDefinitionName, i.InvestmentSecuritySymbol as FieldValue, 'cannot find security - ' + ISNULL(i.InvestmentSecuritySymbol,'') as Error
	FROM CliftonIntegration.dbo.IntegrationTradeIntraday i
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = i.IntegrationImportRunID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON iid.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
	WHERE dbo.InvestmentSecurityLookup(i.InvestmentSecuritySymbol, 1) IS NULL AND TradeDate = @TradeDate
		AND (@SourceCompanyId IS NULL OR s.SourceCompanyID = @SourceCompanyId)
	UNION ALL
	-- Error handling to indicate when we cannot join to BusinessCompany, and hence cannot tell if the import is disabled for that company
	SELECT uuid = NEWID(), 
		i.ImportDefinitionName,
		'SourceCompanyID for IntegrationImportSource "' + s.ImportSourceName + '"' as FieldValue, 
		'No SourceCompany defined for IntegrationImportSource "' + s.ImportSourceName + 
			'." External Trades may not have loaded for this Run.' as Error
	FROM CliftonIntegration.dbo.IntegrationImportDefinition i 
		INNER JOIN CliftonIntegration.dbo.IntegrationFileDefinition f ON i.IntegrationFileDefinitionID = f.IntegrationFileDefinitionID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinitionType t ON i.IntegrationImportDefinitionTypeID = t.IntegrationImportDefinitionTypeID
		INNER JOIN CliftonIntegration.dbo.IntegrationImportSource s ON f.IntegrationImportSourceID = s.IntegrationImportSourceID
		LEFT JOIN BusinessCompany c ON s.SourceCompanyId = c.BusinessCompanyID
	WHERE 
		t.ImportDefinitionTypeName = 'Broker Import Trade Intraday' AND c.BusinessCompanyID IS NULL
		AND (@SourceCompanyId IS NULL OR s.SourceCompanyID = @SourceCompanyId)
END
