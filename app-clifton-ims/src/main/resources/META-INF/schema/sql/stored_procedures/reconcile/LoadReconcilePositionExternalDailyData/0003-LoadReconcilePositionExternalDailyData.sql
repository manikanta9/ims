--exec reconcile.LoadReconcilePositionExternalDailyData '6/10/11'
ALTER PROC reconcile.LoadReconcilePositionExternalDailyData
	@PositionDate DATETIME
AS
BEGIN
	DECLARE @DataSourceID INT, @IntegrationImportRunID INT

	SELECT TOP 1 @IntegrationImportRunID = irph.IntegrationImportRunID
	FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
	WHERE irph.PositionDate = @PositionDate

	SELECT @DataSourceID = MarketDataSourceID
	FROM CliftonIntegration.dbo.IntegrationImportRun iir
		INNER JOIN CliftonIntegration.dbo.IntegrationImportDefinition iid ON iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN MarketDataSource mds ON mds.DataSourceName = iid.DataSourceName	
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
	
	DELETE FROM ReconcilePositionExternalDaily WHERE PositionDate = @PositionDate		
		
	INSERT INTO ReconcilePositionExternalDaily
	(
		[HoldingAccountID]
		,[InvestmentSecurityID]
		,[InvestmentCurrencyID]
		,[BaseCurrencyID]
		,[PositionDate]
		,[TradeDate]
		,[SettleDate]
		,[FxRate]
		,[Quantity]
		,[IsShortPosition]
		,[TradePrice]
		,[MarketPrice]
		,[RemainingCostBasisLocal]
		,[RemainingCostBasisBase]
		,[MarketValueLocal]
		,[MarketValueBase]
		,[OptionMarketValueLocal]
		,[OptionMarketValueBase]
		,[OpenTradeEquityLocal]
		,[OpenTradeEquityBase]
		,[CreateUserID]
		,[CreateDate]
		,[UpdateUserID]
		,[UpdateDate]
	)
	SELECT
		HoldingAccountID =ia.InvestmentAccountID,
		InvestmentSecurityID = 
			coalesce(dbo.InvestmentSecurityLookup(irph.SecuritySymbol, @DataSourceID), 'cannot find security - ' + irph.SecuritySymbol),
		InvestmentCurrencyID = posCurr.InvestmentSecurityID,
		BaseCurrencyID = actCurr.InvestmentSecurityID,
		irph.PositionDate,
		irph.TradeDate,
		irph.SettleDate,
		irph.FxRate,
		CASE WHEN irph.IsShortPosition = 1 THEN -1 * irph.Quantity ELSE irph.Quantity END,
		irph.IsShortPosition,
		irph.TradePrice,
		irph.MarketPrice,
		irph.CostBasisLocal,
		irph.CostBasisBase,
		irph.MarketValueLocal,
		irph.MarketValueBase,
		irph.OptionMarketValueLocal,
		irph.OptionMarketValueBase,
		irph.OpenTradeEquityLocal,
		irph.OpenTradeEquityBase,
		0,
		GETDATE(),
		0,
		GETDATE()
	FROM CliftonIntegration.dbo.IntegrationReconcilePositionHistory irph
		INNER JOIN CliftonIntegration.dbo.IntegrationImportRun iir ON iir.IntegrationImportRunID = irph.IntegrationImportRunID
		INNER JOIN InvestmentAccount ia ON ia.AccountNumber = irph.AccountNumber
		LEFT JOIN InvestmentSecurity posCurr ON posCurr.Symbol = irph.PositionCurrencySymbol
		LEFT JOIN InvestmentSecurity actCurr ON actCurr.Symbol = irph.AccountCurrencySymbol
	WHERE PositionDate = @PositionDate
	
END
		
