-- CREATE cliftonUser (used by IMS web app) if one is not defined and make it db_owner
IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'cliftonUser')
	BEGIN
		CREATE USER [cliftonUser] FOR LOGIN [cliftonUser];
	END
EXEC sp_addrolemember 'db_owner', 'cliftonUser';

-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group - note that the group is assumed to exist and will not be created
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'
UNION
-- PW: password2
SELECT 'imstestuser2', 'Test User 2', NULL
UNION
-- PW: password3
SELECT 'imstestuser3', 'Test User 3', NULL
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', 'Analysts|'
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', 'Documentation Team|'
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|Compliance Violation Support|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trade Confirmation|Analysts|Portfolio Managers|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', 'Client Admins|Investment Account Admins|Reporting Admins|Operations|'

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)


INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================


-- DISABLE ALL BATCH JOBS
UPDATE BatchJob
SET IsEnabled = 0;


-- DISABLE ALL NOTIFICATIONS
UPDATE NotificationDefinition
SET IsActive = 0

-- DISABLE ALL WORKFLOW TASK DEFINITIONS
UPDATE WorkflowTaskDefinition
SET IsActive = 0

-- DISABLE ALL EXPORTS
UPDATE ExportDefinition
SET StartDate = DATEADD(DAY, -10, CONVERT(DATE, GETDATE())),
	EndDate   = DATEADD(DAY, -10, CONVERT(DATE, GETDATE()))

-- For simplicity during integration tests - Futures Accounts require an NFA doc to be on the client and that that doc is published
-- This would require custom field values, etc. So, for local and dev we just take off the restriction that the doc needs to be published
DELETE p
FROM SystemBeanProperty p
	 INNER JOIN SystemBeanPropertyType pt ON p.SystemBeanPropertyTypeID = pt.SystemBeanPropertyTypeID
	 INNER JOIN SystemBean b ON p.SystemBeanID = b.SystemBeanID
WHERE b.SystemBeanName = 'Account''s Client Has an Active NFA Registration Document'
  AND pt.SystemBeanPropertyTypeName = 'Workflow Status Name'

-- UPDATE DEFAULT ACCOUNTS FOR FIX
UPDATE tdm
SET tdm.FixPostTradeAllocationTopAccountNumber = CASE WHEN t.TradeTypeName IN ('Stocks', 'Funds', 'Notes') THEN '00999900' ELSE 'C0320613' END
FROM TradeDestinationMapping tdm
	 INNER JOIN TradeType t ON t.TradeTypeID = tdm.TradeTypeID
	 INNER JOIN TradeDestination td ON td.TradeDestinationID = tdm.TradeDestinationID
	 INNER JOIN TradeDestinationType tdt ON tdt.TradeDestinationTypeID = td.TradeDestinationTypeID
	 INNER JOIN BusinessCompany eb ON eb.BusinessCompanyID = tdm.ExecutingBrokerCompanyID
WHERE tdt.IsFixTradeOnly = 1
  AND td.TradeDestinationName IN ('REDI Ticket', 'GS Desk')
  AND eb.CompanyName = 'Goldman Sachs & Co.'

UPDATE tdm
SET tdm.FixPostTradeAllocationTopAccountNumber = 'APS121'
FROM TradeDestinationMapping tdm
	 INNER JOIN TradeType t ON t.TradeTypeID = tdm.TradeTypeID
	 INNER JOIN TradeDestination td ON td.TradeDestinationID = tdm.TradeDestinationID
	 INNER JOIN TradeDestinationType tdt ON tdt.TradeDestinationTypeID = td.TradeDestinationTypeID
	 INNER JOIN BusinessCompany eb ON eb.BusinessCompanyID = tdm.ExecutingBrokerCompanyID
WHERE tdt.IsFixTradeOnly = 1
  AND td.TradeDestinationName IN ('REDI Ticket', 'GS Desk')
  AND eb.CompanyName = 'Citigroup Global Markets Inc.'

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'db_executor')
	BEGIN
		CREATE ROLE db_executor
		GRANT EXECUTE TO db_executor
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'PARAPORT\MPLS SQL QA Readers')
	BEGIN
		CREATE USER [PARAPORT\MPLS SQL QA Readers] FOR LOGIN [PARAPORT\MPLS SQL QA Readers];
	END
EXEC sp_addrolemember 'db_datareader', 'PARAPORT\MPLS SQL QA Readers';
EXEC sp_addrolemember 'db_executor', 'PARAPORT\MPLS SQL QA Readers';


--Disable all Export destinations (Need to check because of PIOS test db)
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.COLUMNS
		  WHERE TABLE_NAME = 'ExportDefinitionDestination'
			AND COLUMN_NAME = 'IsDisabled')
	BEGIN
		EXEC ('UPDATE ExportDefinitionDestination SET IsDisabled = 1')
	END

--Remove schedules from all Exports
UPDATE ExportDefinition
SET RecurrenceScheduleID = NULL


IF (SELECT COUNT(*)
	FROM master.sys.databases
	WHERE name = 'CliftonIntegration') > 0
	BEGIN
		DECLARE @ExportRunHistoryId INT
		SET @ExportRunHistoryId = (
			SELECT MAX(SourceSystemIdentifier)
			FROM (
					 SELECT MAX(CONVERT(INT, SourceSystemIdentifier)) AS SourceSystemIdentifier
					 FROM CliftonIntegration.dbo.IntegrationExport
					 UNION ALL
					 SELECT MAX(ExportRunHistoryID)
					 FROM ExportRunHistory
				 ) x
		)
		DBCC CHECKIDENT (ExportRunHistory, RESEED, @ExportRunHistoryId)
	END

IF EXISTS(SELECT *
		  FROM SystemQuery
		  WHERE QueryName = 'Billing Sales Commissions')
	BEGIN
		DECLARE @QueryID INT
		SELECT @QueryID = SystemQueryID FROM SystemQuery WHERE QueryName = 'Billing Sales Commissions'

		DELETE r FROM SystemQueryRunHistory r WHERE r.SystemQueryID = @QueryID
		DELETE p FROM SystemQueryParameter p WHERE p.SystemQueryID = @QueryID
		DELETE q FROM SystemQuery q WHERE SystemQueryID = @QueryID
	END

-- reset the message id on the fix message table
IF EXISTS(SELECT name
		  FROM master.dbo.sysdatabases
		  WHERE ('[' + name + ']' = 'CliftonFIX' OR name = 'CliftonFIX'))
	BEGIN
		DECLARE @MaxOrderId INT
		SET @MaxOrderId = (
							  SELECT MAX(ExternalIdentifier)
							  FROM (
									   SELECT MAX(ExternalIdentifier) AS ExternalIdentifier
									   FROM CliftonIMS.dbo.TradeOrder
									   UNION ALL
									   SELECT MAX(ExternalIdentifier)
									   FROM CliftonIMS.dbo.TradeOrderAllocation
									   UNION ALL
									   SELECT MAX(FixIdentifierID)
									   FROM CliftonFIX.dbo.FixIdentifier
								   ) x
						  ) + 1
		DBCC CHECKIDENT ([CliftonFIX.dbo.FixIdentifier], RESEED, @MaxOrderId)
	END

--REMOVE ALL FROM CONTACTS
UPDATE InvestmentInstructionCategory
SET FromContactID = NULL;


--Set all Investment Instructions to use a test group
IF NOT EXISTS(SELECT iicg.InvestmentInstructionContactGroupID
			  FROM InvestmentInstructionContactGroup iicg
			  WHERE iicg.ContactGroupName = 'Test Group')
	BEGIN
		INSERT INTO InvestmentInstructionContactGroup
		(ContactGroupName, ContactGroupLabel, ContactGroupDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES ('Test Group', 'Test Group', 'Group for testing', 0, GETDATE(), 0, GETDATE())
	END


UPDATE InvestmentInstructionDefinition
SET InvestmentInstructionContactGroupID = (SELECT iicg.InvestmentInstructionContactGroupID
										   FROM InvestmentInstructionContactGroup iicg
										   WHERE iicg.ContactGroupName = 'Test Group')

-- Set the BIC codes to test BIC's
UPDATE v
SET ColumnValue = STUFF(ColumnValue, 8, 1, '0'),
	ColumnText  = CASE WHEN ColumnText IS NOT NULL THEN STUFF(ColumnText, 8, 1, '0') ELSE NULL END
FROM SystemColumnValue v
	 INNER JOIN SystemColumn sc ON sc.SystemColumnID = v.SystemColumnID
WHERE sc.ColumnName = 'BusinessIdentifierCode'

UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';


UPDATE SystemDataSource
SET DatabaseName   = '@integrationDataSource.databaseName@',
	ConnectionUrl  = '@integrationDataSource.url@',
	ApplicationUrl = '@external.service.endpoint.integrationApplication@'
WHERE DataSourceName = 'integrationDataSource';


UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.integrationSeattleApplication@'
WHERE DataSourceName = 'integrationSeattleDataSource';


UPDATE SystemDataSource
SET DatabaseName  = '@dwDataSource.databaseName@',
	ConnectionUrl = '@dwDataSource.url@'
WHERE DataSourceName = 'dwDataSource';


UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.clientPortalApplication@'
WHERE DataSourceName = 'portalDataSource';


UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.fixApplication@'
WHERE DataSourceName = 'fixDataSource';


UPDATE SystemDataSource
SET DatabaseName  = '@swiftDataSource.databaseName@',
	ConnectionUrl = '@swiftDataSource.url@'
WHERE DataSourceName = 'swiftDataSource';


UPDATE SystemDataSource
SET DatabaseName  = '@archiveDataSource.databaseName@',
	ConnectionUrl = '@archiveDataSource.url@'
WHERE DataSourceName = 'archiveDataSource';

UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.reconciliationApplication@'
WHERE DataSourceName = 'reconciliationDataSource';

UPDATE SystemDataSource
SET ApplicationUrl = '@external.service.endpoint.deltaApplication@'
WHERE DataSourceName = 'deltaDataSource';
