<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd">
		<sql>
			<statement>
				<![CDATA[
				UPDATE RuleDefinition
					SET CauseTableID = (Select SystemTableID FROM SystemTable WHERE TableName = 'InvestmentInstrument')
					WHERE DefinitionName = 'Compliance Client Approved Contracts Rule Evaluator';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Missing Benchmark for Asset Class [${causeEntity.assetClass.name}]'
					WHERE DefinitionName = 'Asset Class: Missing Benchmark';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Trade date is in the future.'
					WHERE DefinitionName = 'Compliance Trade Date In Future Rule Evaluator';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'This account has a full period of positions on and requires GIPs discretion but is not assigned to any composite.'
					WHERE DefinitionName = 'Client Account: GIPS Discretion Missing Composite Assignment';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Missing MTD Return for Benchmark [${causeEntity.referenceTwo.name}]'
					WHERE DefinitionName = 'Performance Summary Benchmark: Monthly Return Missing';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Currency balance found for ''${causeEntity.symbol}'' without corresponding future on account ''${linkedEntity.clientInvestmentAccount.label}''.'
					WHERE DefinitionName = 'Currency Balance without Currency Future Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Client Account [${causeEntity.clientInvestmentAccount.label}] has an unbooked M2M [${causeEntity.label}].'
					WHERE DefinitionName = 'Unbooked Cleared M2M Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Missing Overlay Target value on measure date [${causeEntity.measureDate?string["MM/dd/yyyy"]}], but gain/loss is not zero.  Daily Returns will not calculate.'
					WHERE DefinitionName = 'Performance Overlay Run: Missing Overlay Target On Measure Date';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Missing Main Portfolio Run with balance date [${causeEntity.overlayRunDate?string["MM/dd/yyyy"]}]'
					WHERE DefinitionName = 'Portfolio Run: Missing Main Run On Balance Date';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Billing Definition [${causeEntity.label}] is not active during Invoice Period [${linkedEntity.invoicePeriodStartDate?string["MM/dd/yyyy"]} - ${linkedEntity.invoicePeriodEndDate?string["MM/dd/yyyy"]}].'
					WHERE DefinitionName = 'Billing Invoice - Definition Not Active During Invoice Period';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Manager Account [${causeEntity.managerAccount.label}] Balance on ${linkedEntity.balanceDate?string["MM/dd/yyyy"]} has not been adjusted.'
					WHERE DefinitionName = 'Manager Account Balance: Processing Incomplete';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Main Portfolio Run with balance date [${causeEntity.balanceDate?string["MM/dd/yyyy"]}] is incomplete and has not been submitting to Trading.'
					WHERE DefinitionName = 'Portfolio Run: Incomplete';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Variation Margin Percentage Dropped Below ${formatNumberMoney(minAmount, true)}:<br /><span class=''violationMessage''>Broker Variation margin ${formatNumberMoney(rangeValue, true)} is less than required minimum ${formatNumberMoney(minAmount, true)}</span>'
					WHERE DefinitionName = 'Variation Margin Range Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Parametric Account Value Difference is ${formatNumberMoney(rangeValue, true)}:<br /><span class=''violationMessage''>Parametric Change ${formatNumberMoney(managerChange)} vs. Daily Gain/Loss ${formatNumberMoney(gainLoss)}</span>'
					WHERE DefinitionName = 'Daily Gain/Loss vs. Clifton Account Value Change';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.managerAccountAssignment.referenceOne.accountName!causeEntity.managerAccountAssignment.referenceOne.managerCompany.name!causeEntity.managerAccountAssignment.referenceOne.accountNumber} - ${causeEntity.accountAssetClass.assetClass.name} Changed ${formatNumberMoney(managerChange, true)}:<br /><span class=''violationMessage''>Benchmark:${causeEntity.accountAssetClass.benchmarkSecurity.symbol} changed ${formatNumberMoney(rangeValue, true)}, difference is greater than maximum allowed ${formatNumberMoney(maxAmount, true)}</span>'
					WHERE DefinitionName = 'Manager Daily Change vs. Benchmark Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Benchmark Security in ${causeEntity.label} Class had significant duration change: ${causeEntity.accountAssetClass.benchmarkDurationSecurity.symbol!causeEntity.accountAssetClass.benchmarkSecurity.symbol}<br /><span class=''violationMessage''>Previous duration value of ${formatNumberMoney(previousValue)} is now ${formatNumberMoney(currentValue)}, a change of ${formatNumberMoney(rangeValue, true)} is <#if lessThanRange>less than minimum ${formatNumberMoney(minAmount, true)}<#else>greater than maximum ${formatNumberMoney(maxAmount, true)}</#if></span>'
					WHERE DefinitionName = 'Asset Class Benchmark Duration Value: Large Change Since Previous Day';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Security in ${causeEntity.overlayAssetClass.label} Asset Class and ${causeEntity.replication.name} Replication had significant duration change: ${causeEntity.security.symbol}<br /><span class=''violationMessage''>Previous duration value of ${formatNumberMoney(previousValue)} is now ${formatNumberMoney(currentValue)}, a change of ${formatNumberMoney(rangeValue, true)} is <#if lessThanRange>less than minimum ${formatNumberMoney(minAmount, true)}<#else>greater than maximum ${formatNumberMoney(maxAmount, true)}</#if> Please verify the duration value entered.</span>'
					WHERE DefinitionName = 'Security Duration Value: Large Change Since Previous Day';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.label} Asset Class Overlay Exposure deviation from target: ${formatNumberMoney(rangeValue, true)}'
					WHERE DefinitionName = 'Asset Class Replication Deviation % from Target Range Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.label} Asset Class Overlay Exposure deviation from target: ${formatNumberMoney(rangeValue)}'
					WHERE DefinitionName = 'Asset Class Replication Deviation Amount from Target Range Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Cash exposure of ${formatNumberMoney(rangeValue, true)} of Total Portfolio Value, is <#if lessThanRange>less than minimum ${formatNumberMoney(minAmount, true)}<#else>greater than maximum ${formatNumberMoney(maxAmount, true)}</#if>'
					WHERE DefinitionName = 'Cash Exposure % of Total Portfolio Value';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Broker Account ${causeEntity.labelShort} Margin Value ${formatNumberMoney(rangeValue)}'
					WHERE DefinitionName = 'Margin Analysis: 0 "Our Account" Value';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.symbol} Currency Balance as a % of ${causeEntity.symbol} Denominated Positions:<br /><span class=''violationMessage''>${formatNumberMoney(rangeValue, true)} is <#if lessThanRange>less than minimum ${formatNumberMoney(minAmount, true)}<#else>greater than maximum ${formatNumberMoney(maxAmount, true)}</#if></span>'
					WHERE DefinitionName = 'Currency Balance Single Base Exposure % of Replication Range Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.symbol} Currency Balance of ${causeEntity.symbol} Denominated Positions:<br /><span class=''violationMessage''>${formatNumberMoney(rangeValue)} is <#if lessThanRange>less than minimum ${formatNumberMoney(minAmount)}<#else>greater than maximum ${formatNumberMoney(maxAmount)}</#if></span>'
					WHERE DefinitionName = 'Currency Balance Single Base Exposure Range Violation';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.label}:<br /><span class=''violationMessage''>Previous day allocation ${formatNumberMoney(previousTotalAllocated)} vs. current allocation ${formatNumberMoney(totalAllocated)}</span>'
					WHERE DefinitionName = 'Explicit Position Allocation Change Since Previous Day';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.managerAccount.accountName!causeEntity.managerAccount.managerCompany.name!causeEntity.managerAccount.accountNumber} [${adjustmentNote}]:<br /><span class=''violationMessage''>Securities ${formatNumberMoney(securitiesAdjustment)}, Cash ${formatNumberMoney(cashAdjustment)} by ${userName}</span>'
					WHERE DefinitionName = 'Manager Account Balance: Adjusted From Event';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = '${causeEntity.managerAccountBalance.managerAccount.accountName!causeEntity.managerAccountBalance.managerAccount.managerCompany.name!causeEntity.managerAccountBalance.managerAccount.accountNumber} [${adjustmentNote}]:<br /><span class=''violationMessage''>Securities ${formatNumberMoney(securitiesAdjustment)}, Cash ${formatNumberMoney(cashAdjustment)} by ${userName}</span>'
					WHERE DefinitionName = 'Manager Account Balance: Manually Adjusted';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Fixed Income is off target: ${causeEntity.accountAssetClass.rebalanceTriggerAction.name()}<br /><span class=''violationMessage''>Off target ${formatNumberMoney(rangeValue)} which is <#if lessThanRange>below ${formatNumberMoney(minAmount)}<#else>above ${formatNumberMoney(maxAmount)}</#if></span>'
					WHERE DefinitionName = 'Rebalancing: Asset Class Rebalance Recommended';

				UPDATE RuleDefinition
					SET ViolationNoteTemplate = 'Total Rebalance adjusted cash outside Account specified range:<br /><span class=''violationMessage''>${formatNumberMoney(rangeValue)} is <#if lessThanRange>less than ${formatNumberMoney(minAmount)}<#else>greater than ${formatNumberMoney(maxAmount)}</#if></span>'
					WHERE DefinitionName = 'Rebalancing: Mini-Rebalance Recommended';
				]]>
			</statement>
		</sql>
</migrations>
