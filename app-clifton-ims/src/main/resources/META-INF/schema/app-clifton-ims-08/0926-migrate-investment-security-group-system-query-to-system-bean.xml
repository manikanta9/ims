<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd"
            conditionalSql="SELECT 1 WHERE NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'InvestmentSecurityGroup' AND COLUMN_NAME = 'RebuildSystemBeanID')">

	<sql runWith="DDL">
		<statement>
			<!-- Add new column (RebuildSystemBeanID) to InvestmentSecurityGroup -->
			ALTER TABLE InvestmentSecurityGroup
			ADD RebuildSystemBeanID INT NULL;
			ALTER TABLE InvestmentSecurityGroup ADD CONSTRAINT [FK_InvestmentSecurityGroup_SystemBean_RebuildSystemBeanID] FOREIGN KEY (RebuildSystemBeanID)
			REFERENCES SystemBean(SystemBeanID);
			CREATE INDEX [ix_InvestmentSecurityGroup_RebuildSystemBeanID] ON InvestmentSecurityGroup (RebuildSystemBeanID);
		</statement>
	</sql>

	<sql runWith="META_DATA">
		<statement>
			<!-- Add new column (RebuildSystemBeanID) to SystemColumn -->
			INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurityGroup'), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'RebuildSystemBeanID', 'Rebuild System Bean ID', 'Rebuild System Bean ID', 'rebuildSystemBean', 1, 0, getdate(), 0, getdate();

			INSERT INTO SystemRelationship(SystemColumnID, ParentSystemColumnID, RelationshipName, RelationshipLabel, RelationshipDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'RebuildSystemBeanID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurityGroup')), (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'SystemBeanID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'SystemBean')), 'FK_InvestmentSecurityGroup_SystemBean_RebuildSystemBeanID', NULL, NULL, 0, getdate(), 0, getdate();
		</statement>
	</sql>

	<!-- Add new SystemBeanGroup for rebuilding InvestmentSecurityGroup security mappings -->
	<data table="SystemBeanGroup">
		<value column="SystemBeanGroupName">Investment Security Group Rebuild Executor</value>
		<value column="SystemBeanGroupDescription">Executes an algorithm to rebuild InvestmentSecurities mapped to an InvestmentSecurityGroup.</value>
		<value column="InterfaceClassName">com.clifton.system.bean.rebuild.EntityGroupRebuildAwareExecutor</value>
	</data>

	<!-- Add new SystemBeanType for rebuilding InvestmentSecurityGroup security mappings by referencing a SystemQuery -->
	<data table="SystemBeanType">
		<value column="SystemBeanGroupID" select="true">(SELECT SystemBeanGroupID FROM SystemBeanGroup WHERE SystemBeanGroupName = 'Investment Security Group Rebuild Executor')</value>
		<value column="SystemBeanTypeName">System Query Investment Security Group Rebuild Executor</value>
		<value column="SystemBeanTypeDescription">Executes a system query to rebuild InvestmentSecurities mapped to an InvestmentSecurityGroup.</value>
		<value column="ClassName">com.clifton.system.query.bean.rebuild.SystemQueryEntityGroupRebuildAwareExecutor</value>
	</data>

	<!-- Add new SystemBeanPropertyType for rebuilding InvestmentSecurityGroup security mappings by referencing a SystemQuery -->
	<data table="SystemBeanPropertyType">
		<value column="SystemBeanTypeID" select="true">(SELECT SystemBeanTypeID FROM SystemBeanType WHERE SystemBeanTypeName = 'System Query Investment Security Group Rebuild Executor')</value>
		<value column="SystemBeanPropertyTypeName">Rebuild System Query</value>
		<value column="SystemBeanPropertyTypeDescription">System query to use for rebuilding the group.</value>
		<value column="SystemPropertyName">systemQueryId</value>
		<value column="IsRequired">true</value>
		<value column="SystemDataTypeID" select="true">(SELECT SystemDataTypeID from SystemDataType where DataTypeName = 'INTEGER')</value>
		<value column="ValueListUrl">systemQueryListFind.json?tableName=InvestmentSecurityGroupSecurity</value>
		<value column="UserInterfaceConfig">{displayField: 'name'}</value>
		<value column="ValueSystemTableID" select="true">(SELECT SystemTableID from SystemTable where TableName = 'SystemQuery')</value>
	</data>

	<data table="SystemBeanPropertyType">
		<value column="SystemBeanTypeID" select="true">(SELECT SystemBeanTypeID FROM SystemBeanType WHERE SystemBeanTypeName = 'System Query Investment Security Group Rebuild Executor')</value>
		<value column="SystemDataTypeID" select="true">(SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING')</value>
		<value column="SystemBeanPropertyTypeName">System Query Parameter Values</value>
		<value column="SystemBeanPropertyTypeDescription">The json string that represent the query parameter values for the select system query.</value>
		<value column="SystemPropertyName">jsonParameterValue</value>
		<value column="IsRequired">false</value>
		<value column="PropertyOrder">30</value>
		<value column="UserInterfaceConfig">{hidden: true}</value>
		<value column="TriggerPropertyTypeID">(SELECT SystemBeanPropertyTypeID FROM SystemBeanPropertyType WHERE SystemBeanTypeID = (SELECT SystemBeanTypeID FROM SystemBeanType WHERE SystemBeanTypeName = 'System Query Investment Security Group Rebuild Executor' AND SystemBeanGroupID = (SELECT SystemBeanGroupID FROM SystemBeanGroup WHERE SystemBeanGroupName = 'Investment Security Group Rebuild Executor')) AND SystemBeanPropertyTypeName = 'Rebuild System Query')</value>
	</data>

	<sql>
		<!-- Add new SystemBean and associated SystemBeanProperty for each InvestmentSecurityGroup that currently uses a SystemQuery for rebuilding.
			 Then, update the InvestmentSecurityGroup to reference the newly created SystemBean in the new RebuildSystemBean column. -->
		<statement>
			DECLARE @SystemBeanTypeID INT = (SELECT SystemBeanTypeID from SystemBeanType WHERE SystemBeanTypeName = 'System Query Investment Security Group Rebuild Executor');
			DECLARE @SystemBeanPropertyTypeID INT = (SELECT SystemBeanPropertyTypeID from SystemBeanPropertyType WHERE SystemBeanPropertyTypeName = 'Rebuild System Query' AND SystemBeanTypeID = @SystemBeanTypeID);

			<!-- Insert bean for each query referenced by a investment security group row -->
			INSERT INTO SystemBean(SystemBeanTypeID, SystemBeanName, SystemBeanDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT @SystemBeanTypeID, QueryName, QueryDescription, 0, getdate(), 0, getdate()
			FROM InvestmentSecurityGroup g
			INNER JOIN SystemQuery q on q.SystemQueryID = g.RebuildSystemQueryID
			WHERE g.RebuildSystemQueryID IS NOT NULL;

			<!-- Insert bean property for each bean previously created for each investment security group row -->
			INSERT INTO SystemBeanProperty(SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = QueryName), @SystemBeanPropertyTypeID, SystemQueryID, QueryName, 0, getdate(), 0, getdate()
			FROM InvestmentSecurityGroup g
			INNER JOIN SystemQuery q on q.SystemQueryID = g.RebuildSystemQueryID
			WHERE g.RebuildSystemQueryID IS NOT NULL

			<!-- Update each investment security group row to reference the bean instead of the query -->
			UPDATE g SET
			RebuildSystemBeanID = (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = (SELECT QueryName FROM SystemQuery WHERE SystemQueryID = g.RebuildSystemQueryID))
			FROM InvestmentSecurityGroup g
			WHERE RebuildSystemQueryID IS NOT NULL
		</statement>

		<!-- Drop column (RebuildSystemQueryID) from InvestmentSecurityGroup -->
		<statement>
			DELETE FROM SystemRelationship
			WHERE SystemColumnID = (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'RebuildSystemQueryID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurityGroup'));
			DELETE FROM SystemColumn
			WHERE SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurityGroup') AND ColumnName = 'RebuildSystemQueryID';
			DROP INDEX ix_InvestmentSecurityGroup_RebuildSystemQueryID ON InvestmentSecurityGroup;
			ALTER TABLE InvestmentSecurityGroup DROP CONSTRAINT FK_InvestmentSecurityGroup_SystemQuery_RebuildSystemQueryID;
			ALTER TABLE InvestmentSecurityGroup
			DROP COLUMN RebuildSystemQueryID;
		</statement>
	</sql>
</migrations>
