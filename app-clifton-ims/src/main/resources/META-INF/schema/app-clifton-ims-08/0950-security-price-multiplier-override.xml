<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd"
			conditionalSql="(SELECT 1 WHERE NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'InvestmentSecurity' AND COLUMN_NAME = 'PriceMultiplierOverride'))">

	<sql runWith="DDL">
		<statement>
			ALTER TABLE InvestmentInstrumentHierarchy ADD IsSecurityPriceMultiplierOverrideAllowed BIT NOT NULL CONSTRAINT DF_InvestmentInstrumentHierarchy_IsSecurityPriceMultiplierOverrideAllowed DEFAULT(0);
		</statement>
		<statement>
			ALTER TABLE InvestmentSecurity ADD PriceMultiplierOverride DECIMAL(19,10) NULL;
		</statement>
	</sql>


	<sql runWith="META_DATA">
		<statement>
			INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentInstrumentHierarchy'), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'BOOLEAN'), 'IsSecurityPriceMultiplierOverrideAllowed', 'Is Security Price Multiplier Override Allowed', 'Is Security Price Multiplier Override Allowed', 'securityPriceMultiplierOverrideAllowed', 1, 0, getdate(), 0, getdate();
		</statement>
		<statement>
			INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
			SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurity'), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'DECIMAL'), 'PriceMultiplierOverride', 'Price Multiplier Override', 'Price Multiplier Override', 'priceMultiplierOverride', 1, 0, getdate(), 0, getdate();
		</statement>
	</sql>

	<sql>
		<statement>
			UPDATE h
				SET IsSecurityPriceMultiplierOverrideAllowed = 1
			FROM InvestmentInstrumentHierarchy h
				INNER JOIN InvestmentInstrumentHierarchy ph ON h.ParentInvestmentInstrumentHierarchyID = ph.InvestmentInstrumentHierarchyID
				INNER JOIN InvestmentInstrumentHierarchy gph ON ph.ParentInvestmentInstrumentHierarchyID = gph.InvestmentInstrumentHierarchyID
			WHERE gph.ParentInvestmentInstrumentHierarchyID IS NULL
				AND gph.InstrumentHierarchyName = 'Commodities'
				AND ph.InstrumentHierarchyName = 'Futures'
				AND h.InstrumentHierarchyName = 'Physicals'
		</statement>
	</sql>

	<sql>
		<statement>
ALTER FUNCTION [PIOS].[GetSecurityPriceAdjusted]	(

	 @SecurityID INT
	 , @PriceDate DATETIME
	 , @IsMultiplierAdjusted BIT = 0

) RETURNS Decimal(28,12) AS

/**
	Returns the Security Price for a given Security on give date.  If IsMultiplierAdjusted = 1 then will also multiply the price by the instruments price multiplier.
	Checks first for the Settlement Price on that date, if it doesn't exist, then checks for the Last Trade Price on that date

	Also, if the Instrument's Exchange Calendar is set, and there is no price, will get the price from the previous business day.  If no calendar is set for the exchange
	will use the default system calendar

	NOTE: Uses Normalized Pricing, so lookups BIG price if necessary (SINCE CANNOT RAISE AN ERROR IN A USER DEFINED FUNCTION
	WILL JUST RETURN NULL IF BIG SECURITY IS MISSING WHERE THERE IS A BIG INSTRUMENT)
**/
BEGIN

	DECLARE @SecurityPrice Decimal(28,12)

	-- Check if should get BIG price - i.e. Big Instrument is set.  If Big Instrument is set, requires Big Security to be set on the security
	DECLARE @BigInstrumentID INT, @BigSecurityID INT, @SecurityLabel NVARCHAR(50)

	SELECT @BigInstrumentID = i.BigInstrumentID
		, @BigSecurityID = s.BigInvestmentSecurityID
		, @SecurityLabel = s.Symbol + ': ' + s.InvestmentSecurityName
	FROM InvestmentSecurity s
	INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID
	WHERE s.InvestmentSecurityID = @SecurityID

	IF (@BigInstrumentID IS NOT NULL AND @BigSecurityID IS NULL)
	BEGIN
		RETURN NULL
		-- RAISERROR(N'Unable to get normalized price for security [%s] because big security is not set.', 16, 1, @SecurityLabel)
	END

	IF (@BigSecurityID IS NOT NULL)
	BEGIN
		-- Get the Big Price - Unadjusted - Then use the big price with the little multiplier (if applies)
		SET @SecurityPrice = PIOS.GetSecurityPriceAdjusted(@BigSecurityID, @PriceDate, 0)
	END
	ELSE
	BEGIN
		-- GET THE PRICE

		-- Clear time
		SET @PriceDate = CAST(FLOOR(CAST(@PriceDate AS FLOAT)) AS DATETIME)

		-- if PriceDate is not a business date for the specified security, find previous business date
		DECLARE @CalendarID INT
		SELECT @CalendarID = e.CalendarID
			FROM InvestmentInstrument i
				INNER JOIN InvestmentSecurity s ON i.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentExchange e ON i.InvestmentExchangeID = e.InvestmentExchangeID
			WHERE InvestmentSecurityID = @SecurityID

		IF (Calendar.isBusinessDay(@PriceDate, @CalendarID) = 0)
		BEGIN
			SET @PriceDate = Calendar.getBusinessDayFrom(@PriceDate, @CalendarID, -1)
		END

		-- Settlement Price is more important than Last Trade Price if both exist
		SELECT @SecurityPrice = v.MeasureValue
		FROM MarketDataValue v
			INNER JOIN MarketDataField f ON v.MarketDataFieldID = f.MarketDataFieldID
		WHERE f.DataFieldName = 'Settlement Price' AND v.MeasureDate = @PriceDate AND v.InvestmentSecurityID = @SecurityID
		ORDER BY v.MeasureTime DESC

		IF (@SecurityPrice IS NULL)
		BEGIN
			SELECT @SecurityPrice = v.MeasureValue
			FROM MarketDataValue v
				INNER JOIN MarketDataField f ON v.MarketDataFieldID = f.MarketDataFieldID
			WHERE f.DataFieldName = 'Last Trade Price' AND v.MeasureDate = @PriceDate AND v.InvestmentSecurityID = @SecurityID
			ORDER BY v.MeasureTime DESC
		END
	END

	IF (@SecurityPrice IS NOT NULL AND @IsMultiplierAdjusted = 1)
	BEGIN
		SET @SecurityPrice = @SecurityPrice * (
				SELECT COALESCE(s.PriceMultiplierOverride, i.PriceMultiplier, 1)
				FROM InvestmentInstrument i
					INNER JOIN InvestmentSecurity s ON s.InvestmentInstrumentID = i.InvestmentInstrumentID
				WHERE s.InvestmentSecurityID = @SecurityID
			)
	END

RETURN

	@SecurityPrice

END

		</statement>
	</sql>

</migrations>






