<%@ page import="com.clifton.core.util.AssertUtils" %>
<%@ page import="org.springframework.beans.factory.config.ConfigurableListableBeanFactory" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%!
	// version is reset every time the server is restarted and should avoid browser caching
	private static final long version = System.currentTimeMillis();

%><%
	// ask the browser not to cache this page
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);


	final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletConfig().getServletContext());
	final ConfigurableListableBeanFactory beanFactory = (ConfigurableListableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
	final String extjsRootUrl = beanFactory.resolveEmbeddedValue("${application.extjs.rootUrl}");

%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Clifton IMS - Investment Management System</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

	<link rel="SHORTCUT ICON" href="core/images/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/ux/gridfilters/css/GridFilters.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/ux/gridfilters/css/RangeMenu.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/resources/css/ext-all.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/ux/css/ux-all.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="core/css/application.css?version=<%=version%>" media="screen"/>

	<%-- Library styles --%>
	<%-- Ideally, we use the webjar version, but with bootstrap 3, there is no easy way to override the CSS we need --%>
	<%--<link rel="stylesheet" href="webjars/bootstrap/3.3.7/dist/css/bootstrap.min.css?version=<%=version%>" id="bt-theme">--%>
	<link rel="stylesheet" href="css/bootstrap.css?version=<%=version%>" id="ims-theme">
	<link rel="stylesheet" href="webjars/jQuery-QueryBuilder/2.4.5/dist/css/query-builder.default.css?version=<%=version%>" id="qb-theme">
	<%-- TODO: add this back with only the necessary css classes when rec goes live --%>
	<%--<link rel="stylesheet" href="css/reconciliation-css-overrides.css?version=<%=version%>" id="ims-theme">--%>

	<%-- Used for JSON migration functionality --%>
	<link rel="stylesheet" type="text/css" href="webjars/jsoneditor/5.5.7/dist/jsoneditor.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="core/css/jsoneditor-ims-theme.css?version=<%=version%>"/>

	<%-- ExtJS core sources --%>
	<script type="text/javascript" src="<%=extjsRootUrl%>/adapter/ext/ext-base-debug.js?version=<%=version%>"></script>
	<script type="text/javascript" src="<%=extjsRootUrl%>/ext-all-debug.js?version=<%=version%>"></script>
	<script type="text/javascript" src="<%=extjsRootUrl%>/ux/ux-all-debug.js?version=<%=version%>"></script>

	<%-- ExtJS extension sources --%>
	<script type="text/javascript" src="<%=extjsRootUrl%>/ux/FieldReplicator.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/extjs-printer/1.0.0/Printer-all.js?version=<%=version%>"></script>

	<%-- Libraries --%>
	<script type="text/javascript" src="webjars/stomp__stompjs/4.0.3/lib/stomp.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/sockjs-client/1.1.4/dist/sockjs.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/jquery/3.4.1/dist/jquery.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/bootstrap/3.3.7/dist/js/bootstrap.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/interactjs/1.2.9/dist/interact.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/jQuery-QueryBuilder/2.4.5/dist/js/query-builder.standalone.js?version=<%=version%>"></script>

	<%-- Used for JSON migration functionality --%>
	<script type="text/javascript" src="webjars/jsoneditor/5.5.7/dist/jsoneditor.min.js?version=<%=version%>"></script>

	<%-- Used for differences in audit trail values --%>
	<script type="text/javascript" src="webjars/google-diff-match-patch/20121119-1/diff_match_patch.js?version=<%=version%>"></script>

	<%-- Used for Charting --%>
	<script type="text/javascript" src="webjars/moment/2.24.0/min/moment.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/chart.js/2.7.3/dist/Chart.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/chartjs-plugin-datalabels/0.7.0/dist/chartjs-plugin-datalabels.min.js?version=<%=version%>"></script>

	<script type="text/javascript">
		Ext.ns('TCG');
		TCG.ExtRootUrl = '<%=extjsRootUrl%>';
		TCG.PageConfiguration = {
			applicationName: '<%=AssertUtils.assertNotNull(beanFactory.resolveEmbeddedValue("${application.name}"), "Property application.name is null").toLowerCase()%>',
			externalApplication: {
				names: '<%=AssertUtils.assertNotNull(beanFactory.resolveEmbeddedValue("${application.external.names}"), "Property application.external.names is null").toLowerCase()%>'
						.split(',')
						.filter(name => !!name), // Remove empty values
				rootUri: '<%=beanFactory.resolveEmbeddedValue("${application.external.root.uri}")%>'
			},
			instance: {
				text: '<%=beanFactory.resolveEmbeddedValue("${application.instance.text}")%>',
				color: '<%=beanFactory.resolveEmbeddedValue("${application.instance.text.color}")%>',
				size: '<%=beanFactory.resolveEmbeddedValue("${application.instance.text.size}")%>',
				version: '<%=beanFactory.resolveEmbeddedValue("${application.instance.version}")%>',
				theme: '<%=beanFactory.resolveEmbeddedValue("${application.instance.theme}")%>'
			},
			graylog: {
				apiScheme: '<%=beanFactory.resolveEmbeddedValue("${graylog.api.scheme}")%>',
				apiHost: '<%=beanFactory.resolveEmbeddedValue("${graylog.api.host}")%>'
			},
			websocket: {
				enabled: '<%=beanFactory.resolveEmbeddedValue("${application.websocket.enabled}")%>' === 'true',
				// Use relative path for non-root contexts; path must be normalized (remove redundant slashes) to avoid Spring Security constraints on missing URI path elements
				endpoint: './<%=beanFactory.resolveEmbeddedValue("${application.websocket.servlet.path}")%>/<%=beanFactory.resolveEmbeddedValue("${application.websocket.stomp.path}")%>'.replace(/\/+(?=\/)|\/+$/g, '')
			},
			clientPortal: {
				adminUrl: '<%=beanFactory.resolveEmbeddedValue("${portal.admin.client.ui.url}")%>'
			}
		};
	</script>
	<script type="text/javascript" src="core/js/tcg.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-app.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-cache.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-data.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-file.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-form.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-grid.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-gridfilters.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-gridsummary.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-toolbar.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-tree.js?version=<%=version%>"></script>

	<script type="text/javascript" src="core/core-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/core-messaging-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="websocket/websocket-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="websocket/websocket-alert.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-search-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-statistic-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-note-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-navigation-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-priority-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-query-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-field-mappings-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="security/security-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="rule/rule-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="portal/portal-api-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-event-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-client-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-contact-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-contract-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="billing/billing-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="calendar/calendar-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="calendar/calendar-schedule-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="collateral/collateral-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="batch/batch-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="document/document-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="export/export-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="fax/fax-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="workflow/workflow-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="notification/notification-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="investment/investment-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="marketdata/marketdata-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="accounting/accounting-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="performance/performance-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="performance/performance-portfolio-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="performance/performance-target-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="trade/trade-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="fix/fix-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="trade/trade-order-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="trade/trade-order-fix-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="otc/otc-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="reconcile/reconcile-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="reconciliation/reconciliation-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="compliance/compliance-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="portfolio/portfolio-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="portfolio/portfolio-target-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="product/product-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="report/report-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="replication/replication-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="lending/lending-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="integration/integration-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="integration/outgoing/integration-outgoing-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="swift/swift-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="instruction/instruction-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="archive/archive-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="bloomberg/bloomberg-shared.js?version=<%=version%>"></script>
	<%--
		use ims-bootstrap.js file to configure cross project dependencies: add an extra tab from non-dependent project, etc.
	--%>
	<script type="text/javascript" src="ims/ims-bootstrap.js?version=<%=version%>"></script>
	<script type="text/javascript" src="app-ims/app-ims-bootstrap.js?version=<%=version%>"></script>

</head>

<body>
<div id="officePluginHolder"></div>
</body>
</html>
