<%@ page import="com.atlassian.crowd.integration.springsecurity.CrowdSSOAuthenticationToken" %>
<%@ page import="com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails" %>
<%@ page import="com.clifton.batch.job.BatchJob" %>
<%@ page import="com.clifton.batch.runner.BatchRunnerService" %>
<%@ page import="com.clifton.bloomberg.client.BloombergDataService" %>
<%@ page import="com.clifton.bloomberg.client.BloombergShortResponse" %>
<%@ page import="com.clifton.bloomberg.client.BloombergShortResponseCommand" %>
<%@ page import="com.clifton.business.contract.BusinessContractService" %>
<%@ page import="com.clifton.business.contract.BusinessContractTemplate" %>
<%@ page import="com.clifton.business.contract.search.BusinessContractTemplateSearchForm" %>
<%@ page import="com.clifton.core.beans.BaseSimpleEntity" %>
<%@ page import="com.clifton.core.beans.BeanUtils" %>
<%@ page import="com.clifton.core.context.Context" %>
<%@ page import="com.clifton.core.context.ContextHandler" %>
<%@ page import="com.clifton.core.dataaccess.file.FileFormats" %>
<%@ page import="com.clifton.core.dataaccess.file.FileUtils" %>
<%@ page import="com.clifton.core.dataaccess.file.FileWrapper" %>
<%@ page import="com.clifton.core.health.HealthCheckHandler" %>
<%@ page import="com.clifton.core.health.HealthCheckParameters" %>
<%@ page import="com.clifton.core.messaging.MessageStatus" %>
<%@ page import="com.clifton.core.messaging.jms.synchronous.SynchronousMessageSender" %>
<%@ page import="com.clifton.core.messaging.synchronous.AbstractSynchronousRequestMessage" %>
<%@ page import="com.clifton.core.messaging.synchronous.SynchronousStatusRequestMessage" %>
<%@ page import="com.clifton.core.messaging.synchronous.SynchronousStatusResponseMessage" %>
<%@ page import="com.clifton.core.util.CollectionUtils" %>
<%@ page import="com.clifton.core.util.StringUtils" %>
<%@ page import="com.clifton.core.util.date.DateUtils" %>
<%@ page import="com.clifton.core.util.date.TimeTrackedOperation" %>
<%@ page import="com.clifton.core.util.date.TimeUtils" %>
<%@ page import="com.clifton.core.util.encryption.EncryptionUtils" %>
<%@ page import="com.clifton.core.util.encryption.EncryptionUtilsAES" %>
<%@ page import="com.clifton.core.util.validation.ValidationException" %>
<%@ page import="com.clifton.core.util.validation.ValidationUtils" %>
<%@ page import="com.clifton.document.DocumentManagementService" %>
<%@ page import="com.clifton.document.DocumentRecord" %>
<%@ page import="com.clifton.dw.analytics.DwAnalyticsService" %>
<%@ page import="com.clifton.dw.analytics.search.AccountingPositionSnapshotSearchForm" %>
<%@ page import="com.clifton.fax.FaxMessageService" %>
<%@ page import="com.clifton.fix.api.client.FixMessageApi" %>
<%@ page import="com.clifton.fix.api.client.model.FixMessageEntryHeartbeatStatus" %>
<%@ page import="com.clifton.fix.api.client.model.FixMessageEntryHeartbeatStatusCommand" %>
<%@ page import="com.clifton.fix.session.FixSessionService" %>
<%@ page import="com.clifton.integration.incoming.server.IntegrationServerStatusProcessorService" %>
<%@ page import="com.clifton.integration.source.IntegrationSourceService" %>
<%@ page import="com.clifton.integration.source.search.IntegrationSourceSearchForm" %>
<%@ page import="com.clifton.investment.account.InvestmentAccountService" %>
<%@ page import="com.clifton.investment.account.search.InvestmentAccountSearchForm" %>
<%@ page import="com.clifton.notification.definition.NotificationDefinitionBatch" %>
<%@ page import="com.clifton.notification.runner.NotificationBatchRunnerService" %>
<%@ page import="com.clifton.portal.security.resource.PortalSecurityResource" %>
<%@ page import="com.clifton.portal.security.resource.PortalSecurityResourceSearchForm" %>
<%@ page import="com.clifton.portal.security.resource.PortalSecurityResourceService" %>
<%@ page import="com.clifton.portfolio.run.PortfolioRun" %>
<%@ page import="com.clifton.portfolio.run.PortfolioRunService" %>
<%@ page import="com.clifton.portfolio.run.report.PortfolioRunReportService" %>
<%@ page import="com.clifton.portfolio.run.search.PortfolioRunSearchForm" %>
<%@ page import="com.clifton.reconciliation.definition.ReconciliationDefinition" %>
<%@ page import="com.clifton.reconciliation.definition.ReconciliationDefinitionService" %>
<%@ page import="com.clifton.reconciliation.definition.search.ReconciliationDefinitionSearchForm" %>
<%@ page import="com.clifton.security.authentication.SecurityDisabledAuthenticationProvider" %>
<%@ page import="com.clifton.security.encryption.transport.client.SecurityRSATransportEncryptionClientHandler" %>
<%@ page import="com.clifton.security.secret.SecuritySecretService" %>
<%@ page import="com.clifton.security.user.SecurityUser" %>
<%@ page import="com.clifton.security.user.SecurityUserService" %>
<%@ page import="com.clifton.security.user.search.SecurityUserSearchForm" %>
<%@ page import="com.clifton.swift.message.SwiftMessageService" %>
<%@ page import="com.clifton.swift.message.search.SwiftMessageSearchForm" %>
<%@ page import="com.clifton.swift.system.SwiftSourceSystemService" %>
<%@ page import="com.clifton.swift.system.search.SwiftSourceSystemSearchForm" %>
<%@ page import="com.clifton.system.schema.SystemDataSource" %>
<%@ page import="com.clifton.system.schema.SystemSchemaService" %>
<%@ page import="com.clifton.system.search.SystemSearchSearchForm" %>
<%@ page import="com.clifton.system.search.SystemSearchService" %>
<%@ page import="com.clifton.system.search.provider.search.SystemSearchResult" %>
<%@ page import="org.springframework.security.core.GrantedAuthority" %>
<%@ page import="org.springframework.security.core.context.SecurityContext" %>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="java.io.Writer" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Set" %>
<body style="FONT-FAMILY: arial,serif; FONT-SIZE: 12px;">
<%!
	Map<String, SystemDataSource> dataSourceMap = null;
	final Set<String> messageExclusions = CollectionUtils.createHashSet(MessageStatus.ENVIRONMENT, MessageStatus.DATABASE_NAME, MessageStatus.DATA_SOURCE_URL);


	//Performs one-time page setup (Information gathering and data structure processing)
	void setupPage(WebApplicationContext applicationContext) {
		SystemSchemaService systemSchemaService = applicationContext.getBean("systemSchemaService", SystemSchemaService.class);
		List<SystemDataSource> dataSourceList = systemSchemaService.getSystemDataSourceList();
		this.dataSourceMap = BeanUtils.getBeanMapUnique(dataSourceList, SystemDataSource::getDatabaseName);
	}


	//Appends the given message status to the builder in a standard format IF it is not in the exclusion list
	void appendMessageStatus(StringBuilder builder, MessageStatus messageStatus) {
		if (!this.messageExclusions.contains(messageStatus.getResponseName())) {
			builder.append(messageStatus.getResponseName()).append(" : ").append(messageStatus.getResponseStatus()).append("<br />");
		}
	}


	String verifyDataSourceStatus(String serviceName, SynchronousStatusResponseMessage response, HealthCheckHandler healthCheckHandler) {
		StringBuilder statusMessages = new StringBuilder();
		boolean isError = false;
		if (response.getError() != null) {
			isError = true;
			statusMessages.append(serviceName).append(" Data Source Status check returned an error: ").append(response.getError().getMessage());
			statusMessages.append("<br />");
		}

		Map<String, MessageStatus> messageStatusMap = BeanUtils.getBeanMapUnique(response.getMessageStatusList(), MessageStatus::getResponseName);

		//Validate the Environment property matches IMS
		MessageStatus environmentStatus = messageStatusMap.remove(MessageStatus.ENVIRONMENT);
		ValidationUtils.assertNotNull(environmentStatus, "Environment property not returned from " + serviceName);
		String environmentLevel = environmentStatus.getResponseStatus();
		if (!StringUtils.isEqual(healthCheckHandler.getEnvironment(), environmentLevel)) {
			isError = true;
			statusMessages.append(serviceName).append(" Environment Level: ").append(environmentLevel);
			statusMessages.append(" Does not match the IMS Environment Level: ").append(healthCheckHandler.getEnvironment());
			statusMessages.append("<br />");
		}

		MessageStatus databaseStatus = messageStatusMap.remove(MessageStatus.DATABASE_NAME);
		ValidationUtils.assertNotNull(databaseStatus, "Database_Name property not returned from " + serviceName);
		String databaseName = databaseStatus.getResponseStatus();

		//Get the datasource for the given database name
		SystemDataSource dataSource = this.dataSourceMap.get(databaseName);

		//If the datasource is not null, then it must exist in the SystemDataSource table
		// Otherwise it is not valid
		if (dataSource == null) {
			isError = true;
			statusMessages.append(serviceName).append(" Database Name: ").append(databaseName);
			statusMessages.append(" Is not assigned to a SystemDataSource.");
			statusMessages.append("<br />");
		}
		else {
			//Validate that the URL returned matches what is on the datasource in the DB
			MessageStatus urlStatus = messageStatusMap.remove(MessageStatus.DATA_SOURCE_URL);
			ValidationUtils.assertNotNull(urlStatus, "Data_Source_URL property not returned from " + serviceName);
			String datasourceUrl = urlStatus.getResponseStatus();

			if (!StringUtils.isEqual(dataSource.getConnectionUrl(), datasourceUrl)) {
				isError = true;
				statusMessages.append(serviceName).append(" JDBC Connection URL: ").append(datasourceUrl);
				statusMessages.append(" Does not match the Connection URL registered with IMS: ").append(dataSource.getConnectionUrl());
				statusMessages.append("<br />");
			}
		}

		for (MessageStatus messageStatus : messageStatusMap.values()) {
			if (messageStatus.isError()) {
				isError = true;
			}
			statusMessages.append(messageStatus.getResponseName()).append(" : ").append(messageStatus.getResponseStatus()).append("<br />");
		}

		ValidationUtils.assertFalse(isError, statusMessages.toString());
		return statusMessages.toString();
	}

%><%
	final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletConfig().getServletContext());
	final HealthCheckHandler healthCheckHandler = applicationContext.getBean("healthCheckHandler", HealthCheckHandler.class);
	final ContextHandler contextHandler = applicationContext.getBean("contextHandler", ContextHandler.class);
	final Writer writer = out;

	// Impersonate user for page
	SecurityUser pageUser = new SecurityUser();
	pageUser.setId((short) 999);
	pageUser.setUserName(SecurityUser.SYSTEM_USER);
	contextHandler.setBean(Context.USER_BEAN_NAME, pageUser);

	// Write header information
	setupPage(applicationContext);
	final HealthCheckParameters healthCheckParameters = healthCheckHandler.getHealthCheckPageParameters(request);
	healthCheckHandler.printSystemInformation(writer);
	healthCheckHandler.printHealthCheckParameterInformation(writer, healthCheckParameters);

	// Execute tests
	healthCheckHandler.printSectionHeader(writer, "TESTS");
	TimeTrackedOperation<Void> healthStatusOperation = TimeUtils.getElapsedTime(() -> {
		/*
		 * Security Authentication Check
		 */
		healthCheckHandler.runTest("Authentication Provider", 500, null, writer, healthCheckParameters, () -> {
			String[] beanNames = applicationContext.getBeanNamesForType(SecurityDisabledAuthenticationProvider.class);
			ValidationUtils.assertTrue(beanNames.length == 0, "Security Disabled Authentication Provider is being used");
		});

		/*
		 * CliftonIMS database access: systemuser should never be deleted
		 * Database maintenance tasks window: 11:00 PM - 12:00 AM
		 */
		healthCheckHandler.runTest("CliftonIMS DB", 500, Collections.singletonList("11:00 PM - 12:00 AM"), writer, healthCheckParameters, () -> {
			SecurityUserService securityUserService = applicationContext.getBean("securityUserService", SecurityUserService.class);
			SecurityUserSearchForm securityUserSearchForm = new SecurityUserSearchForm();
			securityUserSearchForm.setLimit(1);
			securityUserSearchForm.setUserNameEquals("systemuser");
			SecurityUser user = CollectionUtils.getFirstElementStrict(securityUserService.getSecurityUserList(securityUserSearchForm));
			ValidationUtils.assertNotNull(user, "Cannot find security user: " + SecurityUser.SYSTEM_USER);
		});

		/*
		 * CliftonDW database access: month end snapshots should never be deleted
		 * Database maintenance tasks window: 11:00 PM - 12:00 AM
		 */
		healthCheckHandler.runTest("CliftonDW DB", 500, Collections.singletonList("11:00 PM - 12:00 AM"), writer, healthCheckParameters, () -> {
			DwAnalyticsService dwAnalyticsService = applicationContext.getBean("dwAnalyticsService", DwAnalyticsService.class);
			AccountingPositionSnapshotSearchForm searchForm = new AccountingPositionSnapshotSearchForm();
			searchForm.setSnapshotDate(DateUtils.toDate("08/31/2015"));
			searchForm.setLimit(1);
			ValidationUtils.assertNotEmpty(dwAnalyticsService.getAccountingPositionSnapshotList(searchForm), "Cannot find DW Position Snapshots on 08/31/2015");
		});

		/*
		 * CliftonIntegration DB: there is always an export
		 * Database maintenance tasks window: 11:00 PM - 12:00 AM
		 */
		healthCheckHandler.runTest("CliftonIntegration DB", 500, Collections.singletonList("11:00 PM - 12:00 AM"), writer, healthCheckParameters, () -> {
			IntegrationSourceService integrationSourceService = applicationContext.getBean("integrationSourceService", IntegrationSourceService.class);
			IntegrationSourceSearchForm integrationSourceSearchForm = new IntegrationSourceSearchForm();
			integrationSourceSearchForm.setLimit(1);
			ValidationUtils.assertNotEmpty(integrationSourceService.getIntegrationSourceList(integrationSourceSearchForm), "Cannot find Integration Sources");
		});


		/*
		 * SWIFT DB
		 * Database maintenance tasks window: 11:00 PM - 12:00 AM
		 */
		healthCheckHandler.runTest("CliftonSWIFT DB", 500, Collections.singletonList("11:00 PM - 12:00 AM"), writer, healthCheckParameters, () -> {
			SwiftSourceSystemService swiftSourceSystemService = applicationContext.getBean("swiftSourceSystemService", SwiftSourceSystemService.class);
			SwiftSourceSystemSearchForm swiftSourceSystemSearchForm = new SwiftSourceSystemSearchForm();
			swiftSourceSystemSearchForm.setLimit(1);
			ValidationUtils.assertNotEmpty(swiftSourceSystemService.getSwiftSourceSystemList(swiftSourceSystemSearchForm), "Cannot find Swift source systems");
		});

		/*
		 * Document Management
		 */
		healthCheckHandler.runTest("Document Management ", 1000, null, writer, healthCheckParameters, () -> {
			try {
				SecurityContext securityContext = SecurityContextHolder.getContext();
				GrantedAuthority[] authorities = new GrantedAuthority[0];
				CrowdUserDetails principal = new CrowdUserDetails(new com.atlassian.crowd.integration.soap.SOAPPrincipal("imsclientadminuser"), authorities);
				CrowdSSOAuthenticationToken token = new CrowdSSOAuthenticationToken(principal, null, authorities);
				securityContext.setAuthentication(token);
				DocumentManagementService documentManagementService = applicationContext.getBean("documentManagementService", DocumentManagementService.class);
				DocumentRecord docRecord = documentManagementService.getDocumentRecord("BusinessContract", 604);
				ValidationUtils.assertNotNull(docRecord, "Cannot get document record for Table: BusinessContract and fkFieldId: 604");
			}
			finally {
				SecurityContext securityContext = SecurityContextHolder.getContext();
				securityContext.setAuthentication(null);
			}
		});

		/*
		 * BLOOMBERG - Heartbeat
		 */
		healthCheckHandler.runTest("Bloomberg Service", 500, null, writer, healthCheckParameters, () -> {
			SynchronousMessageSender<?> synchronousMessageSender = applicationContext.getBean("bloombergClientMessageSender", SynchronousMessageSender.class);
			ValidationUtils.assertTrue(synchronousMessageSender.sendHeartbeat(), "Bloomberg heartbeat failure");
		});

		/*
		 * BLOOMBERG - Price lookup
		 */
		healthCheckHandler.runTest("Bloomberg Data Lookup", 3000, null, writer, healthCheckParameters, () -> {
			BloombergDataService bloombergDataService = applicationContext.getBean("bloombergDataService", BloombergDataService.class);
			for (int i = 0; i < 4; i++) {
				BloombergShortResponseCommand command = new BloombergShortResponseCommand();
				command.setSymbol("IBM");
				command.setField("PX_LAST");
				List<BloombergShortResponse> responseList = bloombergDataService.getBloombergShortResponseList(command);
				ValidationUtils.assertTrue(responseList.size() == 1, "Bloomberg failed to return a price for PX_LAST of security IBM.");
			}
		});

		/*
		 * FIX OAuth Check - Makes simple call to fix using OAuth
		 */
		healthCheckHandler.runTest("FIX OAuth Status", 3000, null, writer, healthCheckParameters, () -> {
			FixSessionService fixSessionService = applicationContext.getBean("fixSessionService", FixSessionService.class);
			Boolean isAlive = fixSessionService.isFixSessionServiceAlive();
			if (isAlive == null || !isAlive) {
				throw new RuntimeException("Error occurred making call to the Fix with OAuth. Review the logs for further details");
			}
		});


		/*
		 * FIX heartbeat check
		 */
		healthCheckHandler.runTest("FIX Heartbeat", 800, null, writer, healthCheckParameters, () -> {
			FixMessageApi fixMessageApi = applicationContext.getBean("fixMessageApi", FixMessageApi.class);

			final int maximumAllowedHeartbeatIntervalMinutes = 5;
			List<FixMessageEntryHeartbeatStatus> lastHeartbeatList;
			FixMessageEntryHeartbeatStatusCommand fixMessageEntryHeartbeatStatusCommand = new FixMessageEntryHeartbeatStatusCommand();
			fixMessageEntryHeartbeatStatusCommand.setActiveSessionsOnly(true);
			fixMessageEntryHeartbeatStatusCommand.setReturnWarningsOnly(false);
			fixMessageEntryHeartbeatStatusCommand.setMaximumMinutesSinceLastHeartbeatWarn(maximumAllowedHeartbeatIntervalMinutes);

			try {
				lastHeartbeatList = fixMessageApi.getFixMessageLastHeartbeatBySession(fixMessageEntryHeartbeatStatusCommand);
			}
			catch (Throwable e) {
				if (e.getMessage().contains("null")) {
					throw new RuntimeException("Failed to get a list of FIX heartbeats.  Most likely cause is that there is no FIX server running.");
				}
				throw new RuntimeException("Failed to get a list of FIX heartbeats.", e);
			}

			StringBuilder heartbeatMsgBuilder = new StringBuilder();
			StringBuilder errorMessageBuilder = new StringBuilder();
			for (FixMessageEntryHeartbeatStatus heartbeatStatus : CollectionUtils.getIterable(lastHeartbeatList)) {
				if (heartbeatStatus.getFailed() != null && heartbeatStatus.getFailed()) {
					errorMessageBuilder.append(heartbeatStatus.getStatusMessage()).append("<br/>");
				}
				else {
					heartbeatMsgBuilder.append("<br/>").append("&nbsp;&nbsp;").append(heartbeatStatus.getStatusMessage());
				}
			}
			String errorMessage = errorMessageBuilder.toString();
			ValidationUtils.assertTrue(StringUtils.isEmpty(errorMessage), errorMessage);

			return heartbeatMsgBuilder.toString();
		});

		/*
		 * SWIFT Services - JMS, Swift Server, Database check.
		 */
		healthCheckHandler.runTest("SWIFT Service", 500, null, writer, healthCheckParameters, () -> {
			try {
				SwiftMessageService swiftClientMessageService = applicationContext.getBean("swiftClientMessageService", SwiftMessageService.class);
				SwiftMessageSearchForm swiftMessageSearchForm = new SwiftMessageSearchForm();
				swiftMessageSearchForm.setLimit(1);
				ValidationUtils.assertNotEmpty(swiftClientMessageService.getSwiftMessageList(swiftMessageSearchForm), "Unable to retrieve SWIFT messages.");
			}
			catch (Throwable e) {
				if (e.getMessage().contains("null")) {
					throw new RuntimeException("Failed to get a list of SWIFT messages.  Most likely cause is that there is no SWIFT server running.");
				}
				throw new RuntimeException("Failed to get a list of SWIFT messages.", e);
			}
		});

		/*
		 * SWIFT Status Check
		 */
		healthCheckHandler.runTest("SWIFT Status Check", 500, null, writer, healthCheckParameters, () -> {
			@SuppressWarnings("unchecked")
			SynchronousMessageSender<AbstractSynchronousRequestMessage> synchronousMessageSender = (SynchronousMessageSender<AbstractSynchronousRequestMessage>) applicationContext.getBean("swiftClientSynchronousMessageSender", SynchronousMessageSender.class);
			return verifyDataSourceStatus("SWIFT", (SynchronousStatusResponseMessage) synchronousMessageSender.call(new SynchronousStatusRequestMessage()), healthCheckHandler);
		});

		/*
		 * INTEGRATION Datasource Check
		 * TODO - fix
		 */
		healthCheckHandler.runTest("Integration Datasource Check", 500, null, writer, healthCheckParameters, () -> {
			IntegrationServerStatusProcessorService integrationStatusProcessor = applicationContext.getBean("integrationServerStatusProcessorService", IntegrationServerStatusProcessorService.class);
			MessageStatus status = integrationStatusProcessor.getIntegrationStatus();
			StringBuilder statusMessages = new StringBuilder();
			if (status.isError()) {
				throw new ValidationException("Status check returned an error: " + status.getException().getMessage());
			}
			statusMessages.append("<br />");
			appendMessageStatus(statusMessages, status);
			return statusMessages.toString();
			//return verifyDataSourceStatus("Integration", response);
		});

		/*
		 * INTEGRATION Service - Status
		 */
		healthCheckHandler.runTest("Integration FTP Status", 1500, null, writer, healthCheckParameters, () -> {
			IntegrationServerStatusProcessorService integrationStatusProcessor = applicationContext.getBean("integrationServerStatusProcessorService", IntegrationServerStatusProcessorService.class);
			MessageStatus status = integrationStatusProcessor.getIntegrationFtpStatus();
			StringBuilder statusMessages = new StringBuilder();
			if (status.isError()) {
				throw new ValidationException("Status check returned an error: " + status.getException().getMessage());
			}
			statusMessages.append("<br />");
			appendMessageStatus(statusMessages, status);
			return statusMessages.toString();
		});

		/*
		 * INTEGRATION Service - Transport Encryption
		 * TODO - fix
		 */
		healthCheckHandler.runTest("Integration Service Transport Encryption", 1500, null, writer, healthCheckParameters, () -> {
			StringBuilder statusMessages = new StringBuilder();
			IntegrationServerStatusProcessorService integrationStatusProcessor = applicationContext.getBean("integrationServerStatusProcessorService", IntegrationServerStatusProcessorService.class);
			SecurityRSATransportEncryptionClientHandler encryptionClientHandler = applicationContext.getBean("securityRSATransportEncryptionClientHandler", SecurityRSATransportEncryptionClientHandler.class);
			String testValue = EncryptionUtils.byteArrayToHexString(EncryptionUtilsAES.generateInitializationVector());
			List<MessageStatus> statusList = integrationStatusProcessor.getIntegrationTransportEncryptionStatus(encryptionClientHandler.encryptValueForTransport(testValue));
			for (MessageStatus status : statusList) {
				if (status.isError()) {
					throw new ValidationException("Status check returned an error: " + status.getException().getMessage());
				}
				statusMessages.append("<br />");
				appendMessageStatus(statusMessages, status);
			}
			return statusMessages.toString();
		});

		/*
		 * INTEGRATION Transformation
		 */
		healthCheckHandler.runTest("Integration Service Transformation", 15000, Arrays.asList("6:40 PM - 7:20 PM", "7:40 PM - 8:20 PM", "8:40 PM - 9:20 PM", "9:40 PM - 10:20 PM", "10:40 PM - 11:50 PM", "12:10 AM - 5:50 AM", "6:10 AM - 7:00 AM", "7:20 AM - 11:50 AM", "12:10 PM - 6:20 PM"), writer, healthCheckParameters, () -> {
			IntegrationServerStatusProcessorService integrationStatusProcessor = applicationContext.getBean("integrationServerStatusProcessorService", IntegrationServerStatusProcessorService.class);
			MessageStatus status = integrationStatusProcessor.getIntegrationTransformationStatus();
			StringBuilder statusMessages = new StringBuilder();
			if (status.isError()) {
				throw new ValidationException("Status check returned an error: " + status.getException().getMessage());
			}
			statusMessages.append("<br />");
			appendMessageStatus(statusMessages, status);
			return statusMessages.toString();
		});

		/*
		 * REPORTING
		 */
		healthCheckHandler.runTest("Reporting", 8000, null, writer, healthCheckParameters, () -> {
			InvestmentAccountService investmentAccountService = applicationContext.getBean("investmentAccountService", InvestmentAccountService.class);
			InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
			accountSearchForm.setNumber("183050");
			accountSearchForm.setOurAccount(true);

			PortfolioRunService portfolioRunService = applicationContext.getBean("portfolioRunService", PortfolioRunService.class);
			PortfolioRunSearchForm portfolioRunSearchForm = new PortfolioRunSearchForm();
			portfolioRunSearchForm.setLimit(1);
			portfolioRunSearchForm.setClientInvestmentAccountId(CollectionUtils.getFirstElementStrict(investmentAccountService.getInvestmentAccountList(accountSearchForm)).getId());
			PortfolioRun portfolioRun = CollectionUtils.getFirstElementStrict(portfolioRunService.getPortfolioRunList(portfolioRunSearchForm));

			PortfolioRunReportService reportService = applicationContext.getBean("portfolioRunReportService", PortfolioRunReportService.class);
			ValidationUtils.assertNotNull(reportService.downloadPortfolioRunReport(portfolioRun.getId(), true, FileFormats.PDF, true), "Cannot download report for run: " + portfolioRun.getLabel());
		});

		/*
		 * PDF conversion
		 */
		healthCheckHandler.runTest("PDF Document Conversion", 2000, null, writer, healthCheckParameters, () -> {
			FileWrapper file = null;
			try {
				DocumentManagementService documentManagementService = applicationContext.getBean("documentManagementService", DocumentManagementService.class);
				BusinessContractService businessContractService = applicationContext.getBean("businessContractService", BusinessContractService.class);

				BusinessContractTemplateSearchForm templateSearchForm = new BusinessContractTemplateSearchForm();
				templateSearchForm.setContractTypeName("Guidelines");
				templateSearchForm.setInactive(false);
				templateSearchForm.setOrderBy("id:ASC");
				BusinessContractTemplate template = CollectionUtils.getFirstElementStrict(businessContractService.getBusinessContractTemplateList(templateSearchForm));

				Integer fkFieldId = template.getId();
				String tableName = "BusinessContractTemplate";
				DocumentRecord record = documentManagementService.getDocumentRecord(tableName, fkFieldId);
				ValidationUtils.assertNotNull(record, "Document Record not found for Table Name: " + tableName + " ID: " + fkFieldId);
				file = documentManagementService.downloadDocumentRecordConversion(record.getDocumentId(), "pdf");
				ValidationUtils.assertNotNull(file, "File not downloaded for PDF Conversion");
			}
			finally {
				if (file != null && FileUtils.fileExists(file.getFile())) {
					FileUtils.delete(file.getFile());
				}
			}
		});

		/*
		 * Elastic Search
		 */
		healthCheckHandler.runTest("Elastic Search", 500, Collections.singletonList("11:30 PM - 1:00 AM"), writer, healthCheckParameters, () -> {
			SystemSearchService systemSearchService = applicationContext.getBean("systemSearchService", SystemSearchService.class);
			SystemSearchSearchForm searchSearchForm = new SystemSearchSearchForm();
			searchSearchForm.setSearchPattern("aapl");
			searchSearchForm.setLimit(1);
			SystemSearchResult result = CollectionUtils.getFirstElementStrict(systemSearchService.getSystemSearchList(searchSearchForm));
			ValidationUtils.assertNotNull(result, "No results found for AAPL");
			ValidationUtils.assertTrue(StringUtils.isEqual(result.getLabel(), "AAPL (Apple Inc)"), "Search result is not: AAPL (Apple Inc). Found: " + result.getLabel());
		});

		/*
		 * Fax Server
		 */
		healthCheckHandler.runTest("Fax Server", 3000, null, writer, healthCheckParameters, () -> {
			FaxMessageService faxMessageService = applicationContext.getBean("faxMessageService", FaxMessageService.class);
			ValidationUtils.assertTrue(faxMessageService.isFaxServerRunning(), "Unable to verify that Fax Server is running");
		});

		/*
		 * Invalid Notifications Check: Identifies notifications that have a RUNNING status but are actually not running
		 */
		healthCheckHandler.runTest("Notification Status", 3000, null, writer, healthCheckParameters, () -> {
			final String messageTemplate = "&nbsp;&nbsp;&nbsp;&nbsp;Notification Definition Name: %s, ID: %d";
			NotificationBatchRunnerService batchRunnerService = applicationContext.getBean("notificationBatchRunnerService", NotificationBatchRunnerService.class);
			List<NotificationDefinitionBatch> invalidNotificationDefinitionBatchList = batchRunnerService.getNotificationDefinitionBatchListInvalid();
			if (CollectionUtils.isEmpty(invalidNotificationDefinitionBatchList)) {
				return;
			}
			invalidNotificationDefinitionBatchList.sort(Comparator.comparing(BaseSimpleEntity::getId));
			StringBuilder stringBuilder = new StringBuilder("Notifications with status RUNNING that are not currently running:");
			for (NotificationDefinitionBatch invalidNotification : CollectionUtils.getIterable(invalidNotificationDefinitionBatchList)) {
				stringBuilder.append("<br />").append(String.format(messageTemplate, invalidNotification.getName(), invalidNotification.getId()));
			}
			throw new RuntimeException(stringBuilder.toString());
		});

		/*
		 * Invalid Batch Job Check: Identifies batch jobs that have a RUNNING status but are actually not running
		 */
		healthCheckHandler.runTest("Batch Job Status", 3000, null, writer, healthCheckParameters, () -> {
			final String messageTemplate = "&nbsp;&nbsp;&nbsp;&nbsp;Batch Job Name: %s, ID: %d";
			BatchRunnerService batchRunnerService = applicationContext.getBean("batchRunnerService", BatchRunnerService.class);
			List<BatchJob> invalidBatchJobList = batchRunnerService.getBatchJobListInvalid();
			if (CollectionUtils.isEmpty(invalidBatchJobList)) {
				return;
			}
			invalidBatchJobList.sort(Comparator.comparing(BaseSimpleEntity::getId));
			StringBuilder stringBuilder = new StringBuilder("Batch Jobs with status 'RUNNING' that are not currently running:");
			for (BatchJob invalidBatchJob : invalidBatchJobList) {
				stringBuilder.append("<br />").append(String.format(messageTemplate, invalidBatchJob.getName(), invalidBatchJob.getId()));
			}
			throw new RuntimeException(stringBuilder.toString());
		});

		/*
		 * Database Secret Encryption/Decryption: Checks that a secret can be encrypted and decrypted to/from the database.
		 */
		healthCheckHandler.runTest("Security Secret Status", 3000, null, writer, healthCheckParameters, () -> {
			SecuritySecretService securitySecretService = applicationContext.getBean("securitySecretService", SecuritySecretService.class);
			if (!securitySecretService.doHealthCheck()) {
				throw new RuntimeException("Error occurred conducting Security Secret health check. Review the logs for further details");
			}
		});

		/*
		 * Portal OAuth Check - Makes simple call to portal using OAuth
		 */
		healthCheckHandler.runTest("Portal OAuth Status", 3000, null, writer, healthCheckParameters, () -> {
			PortalSecurityResourceService portalSecurityResourceService = applicationContext.getBean("portalSecurityResourceService", PortalSecurityResourceService.class);
			PortalSecurityResourceSearchForm searchForm = new PortalSecurityResourceSearchForm();
			searchForm.setLimit(1);
			PortalSecurityResource portalSecurityResource = CollectionUtils.getFirstElement(portalSecurityResourceService.getPortalSecurityResourceList(searchForm));
			if (portalSecurityResource == null) {
				throw new RuntimeException("Error occurred making call to the Portal with OAuth. Review the logs for further details");
			}
		});

		/*
		 * Reconciliation OAuth Check - Makes simple call to Reconciliation using OAuth
		 */
		healthCheckHandler.runTest("Reconciliation OAuth Status", 3000, null, writer, healthCheckParameters, () -> {
			ReconciliationDefinitionService reconciliationDefinitionService = applicationContext.getBean("reconciliationDefinitionService", ReconciliationDefinitionService.class);
			ReconciliationDefinitionSearchForm searchForm = new ReconciliationDefinitionSearchForm();
			searchForm.setLimit(1);
			ReconciliationDefinition definition = CollectionUtils.getFirstElement(reconciliationDefinitionService.getReconciliationDefinitionList(searchForm));
			if (definition == null) {
				throw new RuntimeException("Error occurred making call to the Reconciliation with OAuth. Review the logs for further details");
			}
		});

		//TODO: Email ???
		//TODO: Crowd Authentication for test user
	});
	writer.write(String.format("<br/><b>PROCESSING TIME: %d milliseconds</b>", healthStatusOperation.getElapsedMilliseconds()));
	writer.flush();
%>
</body>
