// disable accidental Browser back button or refresh for non-local instances
TCG.enableConfirmBeforeLeavingWindow();


/*
 * Apply constants
 */
TCG.ApplicationName = TCG.PageConfiguration.applicationName;
TCG.ExternalApplicationNames = TCG.PageConfiguration.externalApplication.names;
TCG.ExternalApplicationRootUri = TCG.PageConfiguration.externalApplication.rootUri;

TCG.websocket.enabled = TCG.PageConfiguration.websocket.enabled;
TCG.websocket.endpoint = TCG.PageConfiguration.websocket.endpoint;

/*
 * Initialize application
 */
TCG.app.app = new TCG.app.Application({
	getTopBar: function() {
		return {
			html: '<div class="x-panel-header">' +
				'<table cellspacing="0" cellpadding="0" width="100%" class="app-header">' +
				'<tr>' +
				'<td width="23" qtip="Minneapolis Investment Center - Investment Management System (IMS)"><img src="core/images/icons/parametric.png" height="16" border="0" /></td>' +
				'<td style="FONT-WEIGHT: bold; COLOR: #646464; FONT-FAMILY: Arial;">Investment Management System (IMS)</td>' +
				'<td align="right" id="welcomeMessage">&nbsp;</td>' +
				'</tr>' +
				'</table>' +
				'</div>'
		};
	},
	getBody: function() {
		let suffix = '';
		const instanceText = TCG.PageConfiguration.instance.text;
		if (TCG.isNotBlank(instanceText)) {
			suffix = `<br /><br /><br /><div style="WIDTH: 100%; TEXT-ALIGN: center; COLOR: ${TCG.PageConfiguration.instance.color}; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: ${TCG.PageConfiguration.instance.size};">${instanceText}</div>`;
		}
		return {
			html: TCG.trimWhitespace(`
				<div style="WIDTH: 100%; TEXT-ALIGN: center; PADDING-TOP: 50px">
					<div><img src="core/images/logo/Parametric-120px-height.png" border="0" /></div>
					<div style="PADDING-TOP: 20px; COLOR: #646464; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 20pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">Investment Management System</div>
					</div>
					<div style="COLOR: #34abdb; FONT-WEIGHT: bold; FONT-FAMILY: Arial; FONT-SIZE: 16pt">
						<div style="WIDTH: 632px; TEXT-ALIGN: right; MARGIN: auto">${TCG.PageConfiguration.instance.version}</div>
					</div>
					${suffix}
				</div>
			`)
		};
	},
	getMenuBar: function() {
		const mb = new Ext.Toolbar({height: 40});
		mb.render(Ext.getBody());

		mb.add({
				id: 'myNotifications',
				iconCls: 'flag-red',
				text: '(0)',
				tooltip: 'View My Notifications<br />(count includes notifications of Medium and higher priority)',
				hidden: true,
				handler: function() {
					TCG.createComponent('Clifton.notification.NotificationListWindow');
				}
			}, '-', {
				text: 'Portfolio',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Investment Calendar',
						iconCls: 'calendar',
						handler: function() {
							TCG.createComponent('Clifton.investment.calendar.EventListWindow', {id: 'investmentEventListWindow-Trading', defaultTagName: 'Trading'});
						}
					}, {
						text: 'Investment Manager Balances',
						iconCls: 'manager',
						handler: function() {
							TCG.createComponent('Clifton.product.manager.ManagerBalanceProcessWindow');
						}
					}, {
						text: 'Portfolio Runs',
						iconCls: 'run',
						handler: function() {
							TCG.createComponent('Clifton.product.overlay.RunSetupWindow');
						}
					}, '-', {
						text: 'Performance Summaries',
						iconCls: 'chart-bar',
						handler: function() {
							TCG.createComponent('Clifton.product.performance.PerformanceSetupWindow');
						}
					}, '-', {
						text: 'Settings',
						iconCls: 'config',
						menu: {
							items: [{
								text: 'Investment Managers Setup',
								handler: function() {
									TCG.createComponent('Clifton.investment.manager.ManagerSetupWindow');
								},
								iconCls: 'manager'
							}, {
								text: 'Investment Replications',
								iconCls: 'replicate',
								handler: function() {
									TCG.createComponent('Clifton.investment.replication.ReplicationListWindow');
								}
							}, {
								text: 'Position Allocation (Contract Splitting)',
								iconCls: 'chart-pie',
								handler: function() {
									TCG.createComponent('Clifton.investment.account.assetclass.position.PositionAllocationListWindow');
								}
							}, {
								text: 'Security Target Setup',
								iconCls: 'account',
								handler: function() {
									TCG.createComponent('Clifton.investment.account.securitytarget.SecurityTargetListWindow');
								}
							}, {
								text: 'LDI Setup',
								iconCls: 'account',
								handler: function() {
									TCG.createComponent('Clifton.portfolio.ldi.LDISetupWindow');
								}
							}]
						}
					}]
				})
			}, '-', {
				text: 'Trading',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Trade One Client',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Trade Bonds',
								iconCls: 'bonds',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Bonds'});
								}
							}, {
								text: 'Trade Currency',
								iconCls: 'currency',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Currency'});
								}
							}, {
								text: 'Trade Currency Forwards',
								iconCls: 'currency-forward',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Forwards'});
								}
							}, {
								text: 'Trade Funds',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Funds'});
								}
							}, {
								text: 'Trade Futures',
								iconCls: 'futures',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Futures'});
								}
							}, {
								text: 'Trade Notes',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Notes'});
								}
							}, {
								text: 'Trade Options',
								iconCls: 'options',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Options'});
								}
							}, {
								text: 'Trade Stocks',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Stocks'});
								}
							}, {
								text: 'Trade Swaps',
								iconCls: 'swap',
								menu: {
									items: [{
										text: 'Credit Default Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps: CDS'});
										}
									}, {
										text: 'Interest Rate Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps: IRS'});
										}
									}, {
										text: 'Total Return Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps: TRS'});
										}
									}, {
										text: 'Other Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaps'});
										}
									}]
								}
							}, {
								text: 'Trade Swaps - Cleared',
								iconCls: 'swap',
								menu: {
									items: [{
										text: 'Credit Default Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: CDS'});
										}
									}, {
										text: 'Interest Rate Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: IRS'});
										}
									}, {
										text: 'Inflation Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: INFS'});
										}
									}, {
										text: 'Zero Coupon Swaps',
										iconCls: 'swap',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Cleared: ZCS'});
										}
									}]
								}
							}, {
								text: 'Trade Swaptions',
								iconCls: 'options',
								menu: {
									items: [{
										text: 'Credit Default Swaptions',
										iconCls: 'options',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaptions: CDS'});
										}
									}, {
										text: 'Interest Rate Swaptions',
										iconCls: 'options',
										handler: function() {
											TCG.createComponent('Clifton.trade.TradeWindow', {tradeType: 'Swaptions: IRS'});
										}
									}]
								}
							}, {
								text: 'Trade EFP',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.EFPTradeWindow');
								}
							}]
						})
					}, {
						text: 'Trade Multiple Clients',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Trade Bonds',
								iconCls: 'bonds',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.BondTradeEntryWindow', {tradeType: 'Bonds'});
								}
							}, {
								text: 'Trade Currency',
								iconCls: 'currency',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.CurrencyTradeEntryWindow', {tradeType: 'Currency'});
								}
							}, {
								text: 'Trade Funds',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Funds'});
								}
							}, {
								text: 'Trade Futures',
								iconCls: 'futures',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Futures'});
								}
							}, {
								text: 'Trade Notes',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Notes'});
								}
							}, {
								text: 'Trade Options',
								iconCls: 'options',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Options'});
								}
							}, {
								text: 'Trade Stocks',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.trade.entry.DefaultTradeEntryWindow', {tradeType: 'Stocks'});
								}
							}]
						})
					}, {
						text: 'Trade Import',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Generic Trade Import',
								iconCls: 'import',
								handler: function() {
									TCG.createComponent('Clifton.trade.upload.TradeUploadWindow');
								}
							}, {
								text: 'Currency Trade Import',
								iconCls: 'import',
								handler: function() {
									TCG.createComponent('Clifton.trade.upload.CurrencyTradeUploadWindow', {simpleTradeTypeName: 'Currency'});
								}
							}, {
								text: 'Forwards Trade Import',
								iconCls: 'import',
								handler: function() {
									TCG.createComponent('Clifton.trade.upload.CurrencyTradeUploadWindow', {simpleTradeTypeName: 'Forwards'});
								}
							}, {
								text: 'Options Trade Import',
								iconCls: 'import',
								handler: function() {
									TCG.createComponent('Clifton.trade.upload.OptionTradeUploadWindow');
								}
							}, '-', {
								text: 'Trade Fill Import',
								iconCls: 'import',
								handler: function() {
									TCG.createComponent('Clifton.trade.upload.TradeFillUploadWindow');
								}
							}]
						})
					}, '-', {
						text: 'Options Blotter',
						iconCls: 'options',
						handler: function() {
							TCG.createComponent('Clifton.trade.option.OptionsBlotterWindow');
						}
					}, {
						text: 'Volatility Risk Premium Blotter',
						iconCls: 'options',
						handler: function() {
							TCG.createComponent('Clifton.trade.option.VRPBlotterWindow');
						}
					}, {
						text: 'Security Rolls',
						iconCls: 'arrow-right-green',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Futures Rolls',
								iconCls: 'futures',
								handler: function() {
									TCG.createComponent('Clifton.trade.roll.RollEntryWindow');
								}
							}, {
								text: 'Currency Forwards Rolls',
								iconCls: 'currency-forward',
								handler: function() {
									TCG.createComponent('Clifton.trade.roll.CurrencyForwardRollTradeGroupWindow');
								}
							}, {
								text: 'Bonds Rolls',
								iconCls: 'bonds',
								handler: function() {
									TCG.createComponent('Clifton.trade.roll.BondRollTradeGroupWindow');
								}
							}]
						})
					}, '-', {
						text: 'Trading Blotter',
						iconCls: 'blotter',
						handler: function() {
							TCG.createComponent('Clifton.trade.TradeBlotterWindow');
						}
					}, {
						text: 'Trade History',
						iconCls: 'shopping-cart',
						handler: function() {
							TCG.createComponent('Clifton.trade.TradeListWindow');
						}
					}, {
						text: 'Order History',
						iconCls: 'shopping-cart-blue',
						handler: function() {
							TCG.createComponent('Clifton.trade.order.fix.TradeOrderFixListWindow');
						}
					}, {
						text: 'REPO Blotter',
						iconCls: 'repo',
						handler: function() {
							TCG.createComponent('Clifton.lending.repo.RepoBlotterWindow');
						}
					}, {
						text: 'Trade Quotes',
						iconCls: 'call',
						handler: function() {
							TCG.createComponent('Clifton.trade.quote.TradeQuoteListWindow');
						}
					}, '-', {
						text: 'Settings',
						iconCls: 'config',
						menu: {
							items: [
								{
									text: 'Trade Restrictions',
									iconCls: 'rule',
									handler: function() {
										TCG.createComponent('Clifton.trade.restriction.TradeRestrictionSetupWindow');
									}
								}, {
									text: 'Trade Destination Settings',
									iconCls: 'shopping-cart',
									handler: function() {
										TCG.createComponent('Clifton.trade.destination.TradeDestinationMappingSetupWindow');
									}
								}, {
									text: 'REPO Settings',
									iconCls: 'repo',
									handler: function() {
										TCG.createComponent('Clifton.lending.repo.RepoSettingListWindow');
									}
								}, '-',
								{
									text: 'FIX Messages',
									iconCls: 'shopping-cart-blue',
									handler: function() {
										TCG.createComponent('Clifton.fix.FixMessageListWindow');
									}
								},
								{
									text: 'FIX Setup',
									iconCls: 'shopping-cart-blue',
									handler: function() {
										TCG.createComponent('Clifton.fix.FixSetupWindow');
									}
								}
							]
						}
					}
					]
				})
			},
			'-', {
				text: 'Operations',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Trade Reconciliation',
						iconCls: 'reconcile',
						handler: function() {
							TCG.createComponent('Clifton.reconcile.trade.ReconcileTradeListWindow');
						}
					}, {
						text: 'REPO Confirmations',
						iconCls: 'repo',
						handler: function() {
							TCG.createComponent('Clifton.lending.repo.RepoConfirmationBlotterWindow');
						}
					}, {
						text: 'Position/Activity Reconciliation',
						iconCls: 'reconcile',
						handler: function() {
							TCG.createComponent('Clifton.reconcile.position.ReconcilePositionSetupWindow');
						}
					}, {
						text: 'Event Journal Reconciliation',
						iconCls: 'event',
						handler: function() {
							TCG.createComponent('Clifton.accounting.eventjournal.EventJournalReconciliationWindow', {defaultActiveTabName: 'Pending Journal Details'});
						}
					}, {
						text: 'Reconciliation Management',
						iconCls: 'reconcile',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Activity Reconciliation',
								iconCls: 'reconcile',
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationSetupWindow', {
										id: 'activityReconciliationWindow',
										title: 'Activity Reconciliation Window',
										typeName: 'Activity Reconciliation',
										iconCls: 'reconcile'
									});
								}
							}, {
								text: 'Position Reconciliation',
								iconCls: 'chart-pie',
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationSetupWindow', {
										id: 'positionReconciliationWindow',
										title: 'Position Reconciliation Window',
										typeName: 'Position Reconciliation',
										iconCls: 'chart-pie'
									});
								}
							}, {
								text: 'Trade Reconciliation',
								iconCls: 'shopping-cart',
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationSetupWindow', {
										id: 'tradeReconciliationWindow',
										title: 'Trade Reconciliation Window',
										typeName: 'Trade Reconciliation',
										iconCls: 'shopping-cart'
									});
								}
							}, {
								text: 'Cash Reconciliation',
								iconCls: 'money',
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationSetupWindow', {
										id: 'cashReconciliationWindow',
										title: 'Cash Reconciliation Window',
										typeName: 'Cash Reconciliation',
										iconCls: 'money'
									});
								}
							}, {
								text: 'Transaction Reconciliation',
								iconCls: 'book-red',
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationSetupWindow', {
										id: 'transactionReconciliationWindow',
										title: 'Transaction Reconciliation Window',
										typeName: 'Transaction Reconciliation',
										iconCls: 'book-red'
									});
								}
							}, {
								text: 'Market Value Reconciliation',
								iconCls: 'numbers',
								handler: function() {
									TCG.createComponent('Clifton.reconciliation.setup.ReconciliationSetupWindow', {
										id: 'marketValueReconciliationWindow',
										title: 'Market Value Reconciliation Window',
										typeName: 'Market Value Reconciliation',
										iconCls: 'numbers'
									});
								}
							}, '-', {
								text: 'Settings',
								iconCls: 'config',
								menu: new Ext.menu.Menu({
									items: [{
										text: 'Reconciliation Definition Setup',
										iconCls: 'grid',
										handler: function() {
											TCG.createComponent('Clifton.reconciliation.setup.ReconciliationDefinitionManagementWindow');
										}
									}, {
										text: 'Reconciliation Events',
										iconCls: 'event',
										handler: function() {
											TCG.createComponent('Clifton.reconciliation.event.ReconciliationEventListWindow');
										}
									}, {
										text: 'Reconciliation Rules',
										iconCls: 'rule',
										handler: function() {
											TCG.createComponent('Clifton.reconciliation.rule.ReconciliationRuleListWindow');
										}
									}, {
										text: 'Reconciliation Processing',
										iconCls: 'run',
										handler: function() {
											TCG.createComponent('Clifton.reconciliation.setup.ReconciliationProcessingWindow');
										}
									}, {
										text: 'Reconciliation Approval Management',
										iconCls: 'verify',
										handler: function() {
											TCG.createComponent('Clifton.reconciliation.setup.ReconciliationApprovalManagementWindow');
										}
									}]
								})
							}]
						})
					}, '-', {
						text: 'Mark to Market',
						iconCls: 'exchange',
						menu: {
							items: [{
								text: 'Futures Mark to Market',
								iconCls: 'futures',
								handler: function() {
									TCG.createComponent('Clifton.accounting.m2m.M2MSetupWindow');
								}
							}, {
								text: 'Cleared Swaps Mark to Market',
								iconCls: 'swap',
								handler: function() {
									TCG.createComponent('Clifton.accounting.m2m.M2MClearedSwapsSetupWindow');
								}
							}]
						}
					}, {
						text: 'Collateral Management',
						iconCls: 'bank',
						menu: {
							items: [{
								text: 'Futures Collateral',
								iconCls: 'bank',
								handler: function() {
									TCG.createComponent('Clifton.collateral.FuturesCollateralManagementWindow');
								}
							}, {
								text: 'Options Escrow Collateral',
								iconCls: 'bank',
								handler: function() {
									TCG.createComponent('Clifton.collateral.OptionsEscrowCollateralManagementWindow');
								}
							}, {
								text: 'Options Margin Collateral',
								iconCls: 'bank',
								handler: function() {
									TCG.createComponent('Clifton.collateral.OptionsMarginCollateralManagementWindow');
								}
							}, {
								text: 'Cleared OTC Collateral',
								iconCls: 'bank',
								handler: function() {
									TCG.createComponent('Clifton.collateral.ClearedOTCCollateralManagementWindow');
								}
							}, {
								text: 'OTC Collateral',
								iconCls: 'bank',
								handler: function() {
									TCG.createComponent('Clifton.collateral.OTCCollateralManagementWindow');
								}
							}, {
								text: 'REPO Collateral',
								iconCls: 'bank',
								handler: function() {
									TCG.createComponent('Clifton.collateral.REPOCollateralManagementWindow');
								}
							}, '-', {
								text: 'Generate Cash Collateral Transactions',
								iconCls: 'run',
								handler: function() {
									TCG.createComponent('Clifton.collateral.CashCollateralTransactionsCreationWindow');
								}
							}, {
								text: 'Collateral Management Setup',
								iconCls: 'config',
								handler: function() {
									TCG.createComponent('Clifton.collateral.setup.CollateralSetupWindow');
								}
							}]
						}
					}, {
						text: 'Commissions and Fees',
						iconCls: 'fee',
						handler: function() {
							TCG.createComponent('Clifton.accounting.commission.CommissionSetupWindow');
						}
					}, '-', {
						text: 'Position Transfers',
						iconCls: 'transfer',
						handler: function() {
							TCG.createComponent('Clifton.accounting.positiontransfer.PositionTransferListWindow');
						}
					}, '-', {
						text: 'Volatility Risk Premium Operations',
						iconCls: 'options',
						handler: function() {
							TCG.createComponent('Clifton.trade.option.VRPOperationsWindow');
						}
					}, '-', {
						text: 'Trade & Cash Instructions',
						iconCls: 'fax',
						handler: function() {
							TCG.createComponent('Clifton.investment.instruction.InstructionListWindow');
						}
					}]
				})
			}, '-', {
				text: 'Accounting',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'General Ledger',
						iconCls: 'book-red',
						handler: function() {
							TCG.createComponent('Clifton.accounting.gl.TransactionAnalysisWindow');
						}
					},
						{
							text: 'Journals',
							iconCls: 'book-open-blue',
							handler: function() {
								TCG.createComponent('Clifton.accounting.journal.JournalListWindow');
							}
						}, {
							text: 'Simple Journals',
							iconCls: 'cash',
							handler: function() {
								TCG.createComponent('Clifton.accounting.simplejournal.SimpleJournalListWindow');
							}
						}, {
							text: 'Event Journals',
							iconCls: 'event',
							handler: function() {
								TCG.createComponent('Clifton.accounting.eventjournal.EventJournalListWindow');
							}
						}, {
							text: 'Task Journals',
							iconCls: 'task',
							handler: function() {
								TCG.createComponent('Clifton.accounting.taskjournal.TaskJournalListWindow');
							}
						}, '-', {
							text: 'Investment Calendar',
							iconCls: 'calendar',
							handler: function() {
								TCG.createComponent('Clifton.investment.calendar.EventListWindow', {id: 'investmentEventListWindow-Accounting', defaultTagName: 'Accounting'});
							}
						}, '-', {
							text: 'Billing Definition Blotter',
							iconCls: 'billing',
							handler: function() {
								TCG.createComponent('Clifton.billing.definition.DefinitionBlotterWindow', {defaultActiveTabName: 'All Billing Definitions'});
							}
						}, {
							text: 'Invoice Blotter',
							iconCls: 'billing',
							handler: function() {
								TCG.createComponent('Clifton.billing.invoice.InvoiceBlotterWindow');
							}
						}, {
							text: 'Payment Blotter',
							iconCls: 'billing',
							handler: function() {
								TCG.createComponent('Clifton.billing.payment.PaymentBlotterWindow');
							}
						}, {
							text: 'Billing Setup',
							iconCls: 'billing',
							handler: function() {
								TCG.createComponent('Clifton.billing.BillingSetupWindow');
							}
						}, '-', {
							text: 'Sales Commission Assignment Blotter',
							iconCls: 'sales-commission',
							handler: function() {
								TCG.createComponent('Clifton.billing.commission.schedule.AssignmentBlotterWindow');
							}
						}, {
							text: 'Sales Commission Schedule Blotter',
							iconCls: 'sales-commission',
							handler: function() {
								TCG.createComponent('Clifton.billing.commission.schedule.ScheduleBlotterWindow');
							}
						}, '-', {
							text: 'Performance',
							iconCls: 'hierarchy',
							menu: {
								items: [{
									text: 'Composite Setup',
									iconCls: 'hierarchy',
									handler: function() {
										TCG.createComponent('Clifton.performance.composite.setup.CompositeSetupWindow');
									}
								}, {
									text: 'Composite Performance',
									iconCls: 'chart-bar',
									handler: function() {
										TCG.createComponent('Clifton.performance.composite.performance.CompositePerformanceWorkflowWindow');
									}
								}, {
									text: 'Account Performance',
									iconCls: 'chart-bar',
									handler: function() {
										TCG.createComponent('Clifton.performance.composite.account.CompositeAccountPerformanceWorkflowWindow');
									}
								}]
							}
						}, '-', {
							text: 'Settings',
							iconCls: 'config',
							menu: {
								items: [{
									text: 'Accounting Periods',
									iconCls: 'calendar',
									handler: function() {
										TCG.createComponent('Clifton.accounting.period.PeriodListWindow');
									}
								}, {
									text: 'Chart of Accounts',
									iconCls: 'accounts',
									handler: function() {
										TCG.createComponent('Clifton.accounting.account.AccountListWindow');
									}
								}]
							}
						}]
				})
			}, '-', {
				text: 'Compliance',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Trading Rules',
						iconCls: 'verify',
						handler: function() {
							TCG.createComponent('Clifton.compliance.ComplianceSetupOldWindow');
						}
					}, {
						text: 'Trade Restrictions',
						iconCls: 'rule',
						handler: function() {
							TCG.createComponent('Clifton.trade.restriction.TradeRestrictionSetupWindow');
						}
					}, '-', {
						text: 'Compliance Rules',
						iconCls: 'verify',
						handler: function() {
							TCG.createComponent('Clifton.compliance.ComplianceSetupWindow');
						}
					}, {
						text: 'Credit Ratings',
						iconCls: 'thumb-down',
						handler: function() {
							TCG.createComponent('Clifton.compliance.creditrating.CreditRatingSetupWindow');
						}
					}, {
						text: 'Exchange Position Limits',
						iconCls: 'warning-red',
						handler: function() {
							TCG.createComponent('Clifton.compliance.exchangelimit.ExchangeLimitSetupWindow');
						}
					}, {
						text: 'Rule Violations',
						iconCls: 'rule',
						handler: function() {
							TCG.createComponent('Clifton.rule.setup.RuleSetupWindow');
						}
					}, '-', {
						text: 'Compliance Notifications',
						iconCls: 'flag-red',
						handler: function() {
							TCG.createComponent('Clifton.compliance.ComplianceNotificationListWindow');
						}
					}, {
						text: 'Compliance Query Exports',
						iconCls: 'run',
						handler: function() {
							TCG.createComponent('Clifton.system.query.QueryExportListWindow', {
								defaultData: {tagName: 'Compliance'}
							});
						}
					}, {
						text: 'Compliance Dashboard',
						iconCls: 'chart-bar',
						handler: function() {
							TCG.createComponent('Clifton.compliance.ComplianceDashboardWindow');
						}
					}]
				})
			}, '-',
			{
				text: 'Client',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Client Relationships (Organizations)',
						iconCls: 'people',
						handler: function() {
							TCG.createComponent('Clifton.business.client.ClientSetupWindow', {defaultActiveTabName: 'Client Relationships'});
						}
					}, {
						text: 'Clients (Investing Entities)',
						iconCls: 'client',
						handler: function() {
							TCG.createComponent('Clifton.business.client.ClientSetupWindow');
						}
					}, {
						text: 'Investment Accounts',
						iconCls: 'account',
						handler: function() {
							TCG.createComponent('Clifton.investment.account.AccountSetupWindow');
						}
					}, {
						text: 'Account Creation Wizard',
						iconCls: 'wand-plus',
						handler: function() {
							TCG.createComponent('TCG.app.OKCancelWindow', {
								title: 'Select Account Wizard Generator',
								iconCls: 'wand-plus',
								height: 150,
								width: 600,
								openerCt: opener,
								items: [{
									xtype: 'formpanel',
									instructions: 'Select an Investment Account Wizard Generator to use.',
									labelWidth: 140,
									items: [
										{
											fieldLabel: 'Generator', name: 'systemBean.name', hiddenName: 'systemBean.id', xtype: 'combo', detailPageClass: 'Clifton.system.bean.BeanWindow', url: 'investmentAccountWizardListFind.json?groupName=Investment Account Wizard Generator',
											getDefaultData: function() {
												return {type: {group: {name: 'Investment Account Wizard Generator'}}};
											}
										}
									]
								}],
								saveWindow: function(closeOnSuccess, forceSubmit) {
									const panel = this.getMainFormPanel();
									const generatorBeanId = panel.getFormValue('systemBean.id');
									TCG.createComponent('Clifton.ims.investment.account.wizard.InvestmentAccountWizardWindow', {generatorBeanId: generatorBeanId});
									this.closeWindow();
								}
							});
						}
					}, {
						text: 'Client Account Termination',
						iconCls: 'cancel',
						handler: function() {
							TCG.createComponent('Clifton.ims.investment.account.termination.InvestmentAccountTerminationWindow');
						}
					}, '-', {
						text: 'Companies & Contacts',
						handler: function() {
							TCG.createComponent('Clifton.business.BusinessSetupWindow');
						},
						iconCls: 'group'
					}, {
						text: 'Contact Roles',
						handler: function() {
							TCG.createComponent('Clifton.business.contact.assignment.ContactAssignmentSetupWindow');
						},
						iconCls: 'group'
					}, '-', {
						text: 'Documents',
						iconCls: 'contract',
						handler: function() {
							TCG.createComponent('Clifton.business.contract.ContractSetupWindow');
						}
					}, {
						text: 'Services',
						iconCls: 'services',
						handler: function() {
							TCG.createComponent('Clifton.business.service.ServiceSetupWindow');
						}
					}, '-', {
						text: 'Parametric Offices',
						iconCls: 'parametric',
						menu: {
							items: [
								{
									text: 'Parametric Australia',
									handler: function() {
										mb.openOurCompanyWindow('PPA-AUS');
									},
									iconCls: 'kangaroo'
								}, {
									text: 'Parametric Minneapolis',
									handler: function() {
										mb.openOurCompanyWindow('PPA-MN');
									},
									iconCls: 'house'
								}, {
									text: 'Parametric Seattle',
									handler: function() {
										mb.openOurCompanyWindow('PPA-SEA');
									},
									iconCls: 'parametric'
								}, {
									text: 'Parametric Westport',
									handler: function() {
										mb.openOurCompanyWindow('PPA-WPT');
									},
									iconCls: 'options'
								}
							]
						}
					}, '-', {
						text: 'Client Portal Management',
						iconCls: 'www',
						menu: {
							items: [
								{
									text: 'Client Portal Management',
									iconCls: 'www',
									handler: function() {
										TCG.createComponent('Clifton.portal.PortalManagementWindow');
									}
								},
								{
									text: 'Post Investment Report',
									tooltip: 'Generate selected system generated investment report from IMS and post it to the portal.',
									iconCls: 'report',
									scope: this,
									handler: function() {
										const clz = 'Clifton.report.posting.ReportPostingWindow';
										const cmpId = TCG.getComponentId(clz, id);
										TCG.createComponent(clz, {
											id: cmpId,
											postingType: 'InvestmentReport',
											openerCt: this
										});
									}
								},
								{
									text: 'Upload One File',
									iconCls: 'upload',
									tooltip: 'Upload single file and post it to the portal.',
									handler: function() {
										TCG.createComponent('Clifton.portal.file.FileUploadWindow');
									}
								},
								{
									text: 'Upload Multiple Files',
									tooltip: 'Uses drag and drop functionality to bulk import multiple files to the portal.',
									iconCls: 'upload',
									handler: function() {
										TCG.createComponent('Clifton.portal.file.FileUploadPendingListWindow');
									}
								}
							]
						}
					}]
				})
			}, '-', {
				text: 'Analytics',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Reports',
						iconCls: 'report',
						handler: function() {
							TCG.createComponent('Clifton.report.ReportSetupWindow');
						}
					},
						{
							text: 'Query Exports',
							iconCls: 'excel',
							handler: function() {
								TCG.createComponent('Clifton.system.query.QueryExportListWindow');
							}
						},
						{
							text: 'Export Multiple Reports',
							iconCls: 'pdf',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Investment Reports',
									tooltip: 'Export multiple investment reports.',
									iconCls: 'pdf',
									scope: this,
									handler: function() {
										const clz = 'Clifton.report.exports.ReportExportWindow';
										const cmpId = TCG.getComponentId(clz + ':InvestmentReport', id);
										TCG.createComponent(clz, {
											id: cmpId,
											exportType: 'InvestmentReport',
											openerCt: this
										});
									}
								}, {
									text: 'Portfolio Reports',
									tooltip: 'Export multiple Portfolio reports.',
									iconCls: 'pdf',
									scope: this,
									handler: function() {
										const clz = 'Clifton.report.exports.ReportExportWindow';
										const cmpId = TCG.getComponentId(clz + ':PIOS', id);
										TCG.createComponent(clz, {
											id: cmpId,
											exportType: 'PIOS',
											openerCt: this
										});
									}
								}, {
									text: 'Daily Gain Loss Reports',
									tooltip: 'Export multiple Daily Gain Loss reports.',
									iconCls: 'excel',
									scope: this,
									handler: function() {
										const clz = 'Clifton.report.exports.ReportExportWindow';
										const cmpId = TCG.getComponentId(clz + ':DailyGainLossBySecurity', id);
										TCG.createComponent(clz, {
											id: cmpId,
											exportType: 'DailyGainLossBySecurity',
											openerCt: this
										});
									}
								}]
							})
						}, '-',
						{
							text: 'Client Positions',
							iconCls: 'chart-pie',
							handler: function() {
								TCG.createComponent('Clifton.accounting.gl.PositionAnalysisWindow');
							}
						}, {
							text: 'Data Warehouse',
							iconCls: 'database',
							handler: function() {
								TCG.createComponent('Clifton.dw.analytics.DwAnalyticsWindow');
							}
						}, {
							text: 'Calculation Snapshots',
							tooltip: 'Investment Account calculations and snapshots. Example: AUM',
							iconCls: 'calculator',
							handler: function() {
								TCG.createComponent('Clifton.investment.account.calculation.CalculationSetupWindow');
							}
						}, '-', {
							text: 'Investments',
							iconCls: 'stock-chart',
							handler: function() {
								TCG.createComponent('Clifton.investment.setup.InvestmentListWindow');
							}
						}, {
							text: 'Market Data',
							iconCls: 'numbers',
							handler: function() {
								TCG.createComponent('Clifton.marketdata.MarketDataWindow');
							}
						}, {
							text: 'Bloomberg Lookup',
							iconCls: 'bloomberg',
							handler: function() {
								TCG.createComponent('Clifton.bloomberg.QueryBloombergWindow');
							}
						}, '-',
						{
							text: 'Settings',
							iconCls: 'config',
							menu: {
								items: [{
									text: 'Investments Setup',
									handler: function() {
										TCG.createComponent('Clifton.investment.setup.InvestmentSetupWindow');
									},
									iconCls: 'stock-chart'
								}, {
									text: 'Market Data Setup',
									iconCls: 'numbers',
									handler: function() {
										TCG.createComponent('Clifton.marketdata.MarketDataSetupWindow');
									}
								}, {
									text: 'Specificity Setup',
									iconCls: 'grouping',
									handler: function() {
										TCG.createComponent('Clifton.investment.specificity.SpecificitySetupWindow');
									}
								}]
							}
						}]
				})
			}, '-', {
				text: 'Tools',
				menu: new Ext.menu.Menu({
					items: [{
						text: 'Business Events',
						iconCls: 'schedule',
						handler: function() {
							TCG.createComponent('Clifton.business.event.EventSetupWindow');
						}
					}, '-',
						{
							text: 'System Uploads',
							iconCls: 'import',
							handler: function() {
								TCG.createComponent('Clifton.system.upload.UploadWindow');
							}
						}, {
							text: 'Export Management',
							iconCls: 'export',
							handler: function() {
								TCG.createComponent('Clifton.export.ExportSetupWindow');
							}
						}, {
							text: 'Fax Management',
							iconCls: 'fax',
							handler: function() {
								TCG.createComponent('Clifton.fax.FaxSetupWindow');
							}
						}, {
							text: 'Integration System',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Incoming Integration',
									iconCls: 'import',
									handler: function() {
										TCG.createComponent('Clifton.integration.definition.IntegrationSetupWindow');
									}
								}, {
									text: 'Outgoing Integration',
									iconCls: 'export',
									handler: function() {
										TCG.createComponent('Clifton.integration.outgoing.IntegrationExportSetupWindow');
									}
								}]
							})
						}, '-', {
							text: 'Administration',
							menu: new Ext.menu.Menu({
								items: [
									{
										text: 'Batch Jobs',
										iconCls: 'clock',
										handler: function() {
											TCG.createComponent('Clifton.batch.job.JobSetupWindow');
										}
									},
									{
										text: 'Document Management',
										iconCls: 'book',
										handler: function() {
											TCG.createComponent('Clifton.document.setup.DocumentSetupWindow');
										}
									},
									{
										text: 'SWIFT',
										iconCls: 'swift',
										menu: new Ext.menu.Menu({
											items: [{
												text: 'SWIFT Administration',
												iconCls: 'swift',
												handler: function() {
													TCG.createComponent('Clifton.swift.message.SwiftMessageAdministrationWindow');
												}
											}, {
												text: 'SWIFT Message Validation',
												iconCls: 'verify',
												handler: function() {
													TCG.createComponent('Clifton.swift.validation.SwiftValidationWindow');
												}
											}]
										})
									},
									{
										text: 'Search Administration',
										iconCls: 'view',
										handler: function() {
											TCG.createComponent('Clifton.system.search.SearchSetupWindow');
										}
									},
									{
										text: 'Security',
										iconCls: 'lock',
										handler: function() {
											TCG.createComponent('Clifton.security.SecuritySetupWindow');
										}
									},
									{
										text: 'Schema',
										iconCls: 'grid',
										handler: function() {
											TCG.createComponent('Clifton.system.schema.SchemaSetupWindow');
										}
									},
									{
										text: 'Data Replication',
										iconCls: 'replicate',
										handler: function() {
											TCG.createComponent('Clifton.replication.setup.ReplicationTaskSetupWindow');
										}
									},
									{
										text: 'System Beans',
										iconCls: 'objects',
										handler: function() {
											TCG.createComponent('Clifton.system.bean.BeanSetupWindow');
										}
									},
									{
										text: 'System Conditions',
										iconCls: 'question',
										handler: function() {
											TCG.createComponent('Clifton.system.condition.ConditionSetupWindow');
										}
									}, '-',
									{
										text: 'Audit Trail',
										iconCls: 'pencil',
										handler: function() {
											TCG.createComponent('Clifton.system.audit.AuditSetupWindow');
										}
									},
									{
										text: 'Entity Archive',
										iconCls: 'archive',
										handler: function() {
											TCG.createComponent('Clifton.archive.ArchiveListWindow');
										}
									},
									{
										text: 'Centralized Logging',
										iconCls: 'explorer',
										handler: function() {
											const config = {
												graylogUrl: `${TCG.PageConfiguration.graylog.apiScheme}://${TCG.PageConfiguration.graylog.apiHost}`
											};
											TCG.createComponent('Clifton.core.logging.LogMessageListWindow', config);
										}
									},
									{
										text: 'System Ops Views',
										iconCls: 'doctor',
										handler: function() {
											TCG.createComponent('Clifton.core.stats.StatsListWindow');
										}
									},
									{
										text: 'System Access Stats',
										iconCls: 'timer',
										handler: function() {
											TCG.createComponent('Clifton.system.statistic.StatisticListWindow');
										}
									}, '-',
									{
										text: 'Business Company Usage',
										iconCls: 'group',
										handler: function() {
											new TCG.app.URLWindow({
												id: 'businessCompanyUsageWindow',
												title: 'Business Company Usage',
												width: 900,
												height: 600,
												iconCls: 'group',
												url: 'business-company-usage.jsp'
											});
										}
									}
								]
							})
						}, {
							text: 'Setup',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Calendars',
									handler: function() {
										TCG.createComponent('Clifton.calendar.CalendarSetupWindow');
									},
									iconCls: 'calendar'
								}, {
									text: 'Calendar Schedules',
									handler: function() {
										TCG.createComponent('Clifton.calendar.schedule.ScheduleSetupWindow');
									},
									iconCls: 'schedule'
								}, {
									text: 'Hierarchies',
									handler: function() {
										TCG.createComponent('Clifton.system.hierarchy.HierarchySetupWindow');
									},
									iconCls: 'hierarchy'
								}, {
									text: 'Lists and Queries',
									iconCls: 'list',
									handler: function() {
										TCG.createComponent('Clifton.system.list.ListSetupWindow');
									}
								}, {
									text: 'Notes',
									iconCls: 'pencil',
									handler: function() {
										TCG.createComponent('Clifton.system.note.NoteSetupWindow');
									}
								}, {
									text: 'Notifications',
									iconCls: 'flag-red',
									handler: function() {
										TCG.createComponent('Clifton.notification.definition.DefinitionSetupWindow');
									}
								}, {
									text: 'Priorities',
									iconCls: 'priority',
									handler: function() {
										TCG.createComponent('Clifton.system.priority.PrioritySetupWindow');
									}
								}, {
									text: 'Rules',
									iconCls: 'rule',
									handler: function() {
										TCG.createComponent('Clifton.rule.setup.RuleSetupWindow');
									}
								}, {
									text: 'System Field Mappings',
									iconCls: 'tools',
									handler: function() {
										TCG.createComponent('Clifton.system.fieldmapping.SystemFieldMappingSetupWindow');
									}
								}, {
									text: 'Workflow',
									iconCls: 'workflow',
									handler: function() {
										TCG.createComponent('Clifton.workflow.WorkflowSetupWindow');
									}
								}, {
									text: 'Workflow Tasks',
									iconCls: 'task',
									handler: function() {
										TCG.createComponent('Clifton.workflow.task.WorkflowTaskSetupWindow');
									}
								}]
							})
						}, {
							text: 'String Differences',
							iconCls: 'diff',
							handler: function() {
								new TCG.app.OKCancelWindow({
									title: 'String Differences Window',
									iconCls: 'diff',
									id: 'stringDiffWindow',
									enableShowInfo: false,
									closeWindowTrail: true,
									okButtonText: 'Compare',
									okButtonTooltip: 'Compare String 1 to String 2 and highlight differences.',
									width: 750,
									height: 450,

									items: [{
										xtype: 'formpanel',
										instructions: 'Use this window to compare 2 strings and highlight differences. Enter or copy/paste value for each string and click "Compare" button.',
										labelWidth: 70,
										items: [
											{fieldLabel: 'String 1', name: 'string1', xtype: 'textarea', height: 150},
											{fieldLabel: 'String 2', name: 'string2', xtype: 'textarea', height: 150}
										]
									}],
									saveWindow: function() {
										const fp = this.getMainFormPanel();
										TCG.showDifferences(fp.getFormValue('string1'), fp.getFormValue('string2'));
									}
								});
							}
						}, {
							text: 'User Tools',
							iconCls: 'tools',
							menu: new Ext.menu.Menu({
								items: [{
									text: 'Change Password',
									iconCls: 'key',
									handler: function() {
										TCG.showInfo('System authentication is integrated with company\'s Active Directory.<br/><br/>To change password, press &lt;Ctrl&gt; + &lt;Alt&gt; + &lt;Delete&gt; while logged in with your corporate Windows account. Click the [Change Password...] button and follow instructions for password change.', 'Password Change Instructions');
									}
								}, {
									text: 'My Notifications',
									iconCls: 'flag-red',
									handler: function() {
										TCG.createComponent('Clifton.notification.NotificationListWindow');
									}
								}, {
									text: 'User Preferences',
									disabled: true,
									iconCls: 'config',
									handler: menuSelect
								}]
							})
						}
					]
				})
			},
			'-',
			new TCG.app.WindowMenu(this),
			'-',
			{
				text: 'Help',
				menu: {
					items: [
						TCG.app.ContactSupportMenu, '-', {
							text: 'IMS Documentation',
							iconCls: 'help',
							handler: function() {
								TCG.openWIKI('IT/IMS+-+Investment+Management+System', 'IMS Documentation');
							}
						}
						, '-', {
							text: 'System Health Check',
							iconCls: 'verify',
							handler: function() {
								new TCG.app.URLWindow({
									id: 'healthCheckWindow',
									title: 'System Health Check',
									width: 1000,
									height: 700,
									iconCls: 'verify',
									url: 'health-status.jsp?enableTimeIntervals=false'
								});
							}
						}
					]
				}
			},
			'-',
			{
				text: 'Logout',
				id: 'logoutMenu',
				handler: () => TCG.data.AuthHandler.logout()
			},


			'->', {
				text: '<u>S</u>earch:',
				tooltip: 'Search for the entity in selected category to open its details'
			}, {
				xtype: 'combo',
				mode: 'local',
				width: 140,
				listWidth: 150,
				valueField: 'name',
				displayIconClsField: 'windowIcon',
				value: 'All',
				store: {
					xtype: 'arraystore',
					fields: ['name', 'description', 'url', 'windowClass', 'windowIcon'],
					data: [
						['All', 'Search for everything: clients, contacts, investment accounts, securities, etc. Use more specific searches to get more specific/accurate results.', 'systemSearchListFind.json', 'CUSTOM', 'find'],
						['Client Relationships', 'Search for Client relationships', 'businessClientRelationshipListFind.json', 'Clifton.business.client.relationship.ClientRelationshipWindow', 'people'],
						['Clients', 'Search for client details', 'businessClientListFind.json', 'Clifton.business.client.ClientWindow', 'client'],
						['Contracts', 'Search for contract details', 'businessContractListFind.json', 'Clifton.business.contract.ContractWindow', 'contract'],
						['Companies', 'Search for company details', 'businessCompanyListFind.json', 'Clifton.business.company.CompanyWindow', 'business'],
						['Billing Definitions', 'Search for billing definitions', 'billingDefinitionListFind.json?excludeWorkflowStateName=Archived', 'Clifton.billing.definition.DefinitionWindow', 'billing'],
						['Client Accounts', 'Search for a client investment account', 'investmentAccountListFind.json?ourAccount=true', 'Clifton.investment.account.AccountWindow', 'account'],
						['Holding Accounts', 'Search for a holding investment account', 'investmentAccountListFind.json?ourAccount=false', 'Clifton.investment.account.AccountWindow', 'account'],
						['Investment Instruments', 'Search for an investment instrument', 'investmentInstrumentListFind.json', 'Clifton.investment.instrument.InstrumentWindow', 'stock-chart'],
						['Investment Securities', 'Search for an investment security', 'investmentSecurityListFind.json', 'Clifton.investment.instrument.SecurityWindow', 'stock-chart'],
						['Query Exports', 'Search for an query exports', 'systemQueryListFind.json?category2Name=System Query Tags&category2HierarchyName=Query Export&', 'Clifton.system.query.QueryExportWindow', 'excel']
					]
				},
				listeners: {
					select: function(combo, record, index) {
						const o = Ext.getCmp('globalSearchField');
						o.emptyText = 'Search for ' + record.data.name;
						o.detailPageClass = record.data.windowClass;
						o.clearValue();
						o.store.removeAll();
						o.lastQuery = null;

						if (record.data.windowClass === 'CUSTOM') {
							o.minChars = 1;
						}
						else {
							o.minChars = 0;
						}
						o.store.proxy.setUrl(record.data.url, true);
					}
				}
			}, {
				xtype: 'combo',
				id: 'globalSearchField',
				errorFieldLabel: 'Search',
				url: 'CUSTOM', // needs to be set after login
				requestedProps: 'systemTable.name|systemTable.detailScreenClass|pkFieldId|systemSearchArea.label|systemSearchArea.systemTable.screenIconCls|label|tooltip|description|screenClass|userInterfaceConfig',
				detailPageClass: 'Clifton.business.client.ClientWindow',
				doNotTrimStrings: false,
				doNotAddContextMenu: true,
				forceSelection: false,
				displayField: 'label',
				hideTrigger: true,
				minChars: 1,
				emptyText: ' Search for All',
				tpl: '<tpl for="."><div class="x-combo-list-item" ext:qtip="{tooltip:htmlEncode}{description:htmlEncode}"><tpl if="icon"><span class="inline-icon {icon}">&nbsp;</span> </tpl>{label}<tpl if="searchAreaLabel"> <span style="COLOR: #330027; FONT-STYLE: italic">in {searchAreaLabel}</span></tpl></div></tpl>',
				fields: ['id', {name: 'searchAreaLabel', mapping: 'systemSearchArea.label'}, 'label', 'description', 'tooltip', {name: 'icon', mapping: 'systemSearchArea.systemTable.screenIconCls'}],
				width: 200,
				listWidth: 600,
				maxHeight: 450,
				pageSize: 20,
				listeners: {
					select: function(combo, record, index) {
						let className = combo.detailPageClass;
						let id = combo.getValue();

						if (record.json.systemTable && record.json.systemTable.detailScreenClass) {
							className = record.json.systemTable.detailScreenClass;
							id = record.json.pkFieldId;
						}
						else if (record.json.screenClass) {
							className = record.json.screenClass;
							id = record.json.pkFieldId;
						}

						if (className === 'Clifton.system.query.QueryWindow') {
							className = 'Clifton.system.query.QueryExportWindow'; // execute vs open query exports
						}

						const winParams = {
							id: TCG.getComponentId(className, id),
							params: {id: id},
							openerCt: combo
						};

						TCG.createComponent(className, Ext.apply({}, Ext.decode(record.json.userInterfaceConfig), winParams));
						combo.reload(null, true);
					}
				}
			}
		);

		mb.openOurCompanyWindow = function(alias) {
			TCG.data.getDataPromiseUsingCaching('businessCompanyListFind.json?companyType=Our Companies&alias=' + alias, this, 'business.company.our.' + alias)
				.then(function(cmList) {
					if (cmList.length === 1) {
						const cm = cmList[0];
						const clz = 'Clifton.business.company.CompanyWindow';
						const cmpId = TCG.getComponentId(clz, cm.id);
						TCG.createComponent(clz, {
							id: cmpId,
							defaultDataIsReal: true,
							defaultData: cm
						});
					}
					else {
						if (cmList.length === 0) {
							TCG.showError('Cannot find Company of type "Our Companies" with alias ' + alias);
						}
						else {
							TCG.showError('Found multiple companies of type "Our Companies" with alias ' + alias);
						}
					}
				});
		};
		return mb;
	}
});


TCG.app.app.addListener('afterload', function() {
	// easy way to distinguish environments
	const theme = TCG.PageConfiguration.instance.theme;
	if (TCG.isNotBlank(theme)) {
		Ext.util.CSS.swapStyleSheet('theme', theme);
	}

	if (TCG.isTrue(TCG.getQueryParams()['maximize'])) {
		TCG.app.Window.prototype.maximized = true;
	}

	// When clicking the welcome message, open the Workflow task window
	Ext.fly('welcomeMessage').addListener('click', function() {
		TCG.createComponent('Clifton.workflow.task.WorkflowTaskSetupWindow');
	});

	TCG.data.AuthHandler.initLogin('securityUserCurrent.json', user => {
		TCG.setCurrentUserIsAdmin(TCG.getResponseText('securityUserCurrentInGroup.json?groupName=Administrators'));
		TCG.setCurrentUserIsDeveloper(TCG.getResponseText('securityUserCurrentInGroup.json?groupName=Software Developers'));
		// load controls specified by createComponents URL parameter.  URL can be follow by an ID e.i. createComponents=Clifton.workflow.WorkflowWindow:workflowWindowID
		const componentStr = Ext.urlDecode(window.location.search.substring(1)).createComponents;
		if (componentStr) {
			const components = componentStr.split(',');
			for (const component of components) {
				if (component.indexOf(':') > 0) {
					const items = component.split(':');
					TCG.createComponent(items[0], {id: items[1]});
				}
				else {
					TCG.createComponent(component);
				}
			}
		}

		// Set the Portal Admin Client UI URL
		Clifton.portal.AdminClientUIUrl = TCG.PageConfiguration.clientPortal.adminUrl;

		const loadWindowClass = TCG.getQueryParams()['loadWindowClass'];
		if (loadWindowClass) {
			const loadWindowParams = TCG.getQueryParams()['loadWindowParams'];
			TCG.createComponent(loadWindowClass, {params: Ext.decode(loadWindowParams)});
		}

		const cmb = Ext.getCmp('globalSearchField');
		cmb.url = 'systemSearchListFind.json';
		cmb.store.proxy.setUrl(cmb.url, true);
		cmb.focus(false, 250);

		// Pre-Load/Cache all users (used for Created By/Updated By columns) - only gets id and label
		const loader = new TCG.data.JsonLoader(Ext.apply({
			onLoad: (records, conf) => {
				if (records && records.length > 0) {
					for (const record of records) {
						TCG.CacheUtils.put('DATA_CACHE', record, `security.user.${record.id}_Application=Internal`);
					}
				}
			}
		}));
		loader.load('securityUserList.json?requestedPropertiesRoot=data&requestedProperties=id|label');
	});

	// set focus on search box on <Alt> + S
	const map = new Ext.KeyMap(document, [{
		key: 's',
		alt: true,
		stopEvent: true,
		fn: function() {
			Ext.getCmp('globalSearchField').focus(false, 250); // delay to select after browser
		}
	}, { // disable confirm on F5 to allow explicit browser reload
		key: Ext.EventObject.F5,
		fn: function() {
			TCG.enableConfirmBeforeLeavingWindow(true);
			window.setTimeout(TCG.enableConfirmBeforeLeavingWindow, 1000);
		}
	}]);
	map.stopEvent = false;
});


function menuSelect(a) {
	TCG.showError('Menu item "' + a.text + '" is currently under construction', 'Under Construction');
}
