Clifton.ims.investment.account.termination.InvestmentAccountTerminationWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Client Account Termination',
	iconCls: 'cancel',
	width: 1100,
	height: 500,
	doNotWarnOnCloseModified: true,
	okButtonText: 'Process Termination',
	okButtonWidth: 150,
	okButtonTooltip: 'Process Account Termination',

	items: [{
		xtype: 'formpanel',
		fileUpload: true,
		getSaveURL: function() {
			return 'investmentAccountTerminationProcessUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},
		confirmBeforeSaveMsgTitle: 'Terminate Client Account',
		getConfirmBeforeSaveMsg: function() {
			return 'Are you sure you want to continue and terminate the selected account?';
		},
		items: [
			{fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true&excludeWorkflowStateName=Terminated', allowBlank: false, detailPageClass: 'Clifton.investment.account.AccountWindow'},
			{
				fieldLabel: 'Terminate Date', name: 'terminationDate', xtype: 'datefield', allowBlank: false, maxValue: new Date(),
				listeners: {
					change: function(field, newValue, oldValue) {
						const form = TCG.getParentFormPanel(field).getForm();
						const billingEndDateField = form.findField('billingEndDate');

						if (TCG.isBlank(newValue)) {
							billingEndDateField.setValue('');
						}
						else {
							billingEndDateField.setValue(TCG.getPreviousWeekday(TCG.parseDate(newValue)).format('m/d/Y'));
						}
					},
					select: function(field, date) {
						const form = TCG.getParentFormPanel(field).getForm();
						const billingEndDateField = form.findField('billingEndDate');
						billingEndDateField.setValue(TCG.getPreviousWeekday(TCG.parseDate(date)).format('m/d/Y'));
					}
				}
			},
			{xtype: 'sectionheaderfield', header: 'Attach a document to the client (Optional)'},
			{fieldLabel: 'Document Type', name: 'businessClientTerminationContract.contractType.name', hiddenName: 'businessClientTerminationContract.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json', qtip: 'Example - attach a termination letter'},
			{fieldLabel: 'File', name: 'businessClientTerminationFile', xtype: 'fileuploadfield', allowBlank: false, requiredFields: ['businessClientTerminationContract.contractType.name']},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Automatically publish uploaded document', name: 'autoPublishDocument', checked: true},
			{xtype: 'sectionheaderfield', header: 'Holdings'},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Distribute all positions out of selected account on Terminate Date', name: 'distributePositions', checked: true},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Distribute non Position Assets and Liabilities out of selected account on Terminate Date', name: 'distributeNonPositionAssetsAndLiabilities', checked: true},
			{xtype: 'sectionheaderfield', header: 'Account Structure'},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Terminate Holding Accounts (If no other active relationships)', name: 'terminateHoldingAccounts', checked: true},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Terminate Client (If no other active client accounts)', name: 'terminateClient', checked: true},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'Terminate Client Relationship (If no other active clients)', name: 'terminateClientRelationship', checked: true},
			{xtype: 'sectionheaderfield', header: 'Portfolio / Trading'},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'End All Security Targets (If Applies)', name: 'terminateSecurityTargets', checked: true, qtip: 'If the account supports security targets - any without an end date will be set to the terminate date.'},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'End All Trade Restrictions', name: 'terminateTradeRestrictions', checked: true},
			{xtype: 'sectionheaderfield', header: 'Billing'},
			{xtype: 'checkbox', fieldLabel: '', boxLabel: 'End Billing', name: 'terminateBilling', checked: false, qtip: 'End all account billing definitions on billing end date.  If it is the last terminated account for the billing definition, will also end the billing definition.  Note: if the billing definition is not in Draft state will reject it for changes and then re-submit for approval.  A note will also be added to the billing definition with the account that is being terminated.'},
			{fieldLabel: 'Billing End Date', name: 'billingEndDate', xtype: 'datefield', maxValue: new Date(), qtip: 'If blank will use the termination date.', requiredFields: ['terminateBilling'], doNotClearIfRequiredChanges: true}
		]

	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Processing...',
			success: function(form, action) {
				const result = action.result.data;
				TCG.createComponent('Clifton.core.StatusWindow', {
					title: 'Account Termination Status',
					defaultData: {status: result}
				});
				win.closeWindow();
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	}
});
