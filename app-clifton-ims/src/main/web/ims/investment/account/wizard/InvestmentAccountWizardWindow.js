// not the actual window but a window selector based on generator bean id (if selected)
Clifton.ims.investment.account.wizard.InvestmentAccountWizardWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'investmentAccountWizard.json',
	className: 'Clifton.ims.investment.account.wizard.BasicInvestmentAccountWizardWindow',

	openEntityWindow: function(config) {
		// entity should never exist
		if (TCG.isBlank(config.generatorBeanId)) {
			TCG.showError('Missing required generator selection');
		}
		const entity = TCG.data.getData('investmentAccountWizard.json?generatorBeanId=' + config.generatorBeanId, this);
		this.doOpenEntityWindow(config, entity, this.className);
	}
});
