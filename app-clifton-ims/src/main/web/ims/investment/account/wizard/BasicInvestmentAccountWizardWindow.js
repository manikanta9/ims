TCG.use('Clifton.system.common.AddressFormFragment');

Clifton.ims.investment.account.wizard.BasicInvestmentAccountWizardWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Account Structure Setup Wizard',
	iconCls: 'wand-plus',
	width: 1200,
	height: 700,
	items: [{
		xtype: 'formpanel',
		labelWidth: 150,
		labelFieldName: 'wizardGeneratorBean.label',
		fileUpload: true,
		getSaveURL: function() {
			return 'investmentAccountWizardUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		},

		initComponent: function() {
			TCG.form.FormPanel.prototype.initComponent.call(this);
			this.updateDisplay();
		},
		updateDisplay: function() {
			const fp = this;
			const wizardGeneratorBean = TCG.getValue('wizardGeneratorBean', this.getWindow().defaultData);
			if (wizardGeneratorBean && wizardGeneratorBean.propertyList) {
				wizardGeneratorBean.propertyList.forEach(function(property) {
					if (TCG.isEquals('fieldOverrides', property.type.systemPropertyName)) {
						const fieldOverrides = property.value;
						if (TCG.isNotBlank(fieldOverrides)) {
							const fieldOverridesObj = Ext.decode(fieldOverrides);
							fieldOverridesObj.forEach(function(fldOverride) {
								fp.updateField(fldOverride);
							});
						}
					}
				});
			}
		},
		updateField: function(fldOverride) {
			const field = this.getForm().findField(fldOverride.name);
			if (field) {
				Ext.apply(field, fldOverride);
			}
		},
		showHideFormFragment: function(formFragmentName, show) {
			const formPanel = this;
			const formFragment = TCG.getChildByName(formPanel, formFragmentName);
			if (!formFragment) {
				TCG.showError('Cannot find Form Fragment with name: ' + formFragmentName);
			}
			formFragment.setVisible(show);
			const items = formFragment.items;
			if (items) {
				formPanel.enableDisableRequiredFields(formPanel, show, items);
			}
		},
		enableDisableRequiredFields: function(fp, show, items) {
			if (!items) {
				return;
			}
			items.each(function(fld) {
				// Don't auto enable if it uses required fields
				if (!show || TCG.isBlank(fld.requiredFields)) {
					fld.setDisabled(!show);
				}
				if (fld.items) {
					fp.enableDisableRequiredFields(fp, show, fld.items);
				}
			});
		},
		clearClientRelationshipDocumentSelections: function() {
			const formPanel = this;
			const fieldNames = ['businessClientRelationshipContract1.contractType.name', 'businessClientRelationshipContract2.contractType.name', 'businessClientRelationshipContract3.contractType.name', 'businessClientRelationshipContract4.contractType.name'];
			fieldNames.forEach(fieldName => {
				formPanel.getForm().findField(fieldName).clearAndReset();
			});
		},
		/**
		 * Clears all previously  stored contract selections and repopulates the contractDocumentIndex data with document indexes
		 * for uploaded documents either in the client relationship  or client section (depending on which section was configured for use via
		 * the boolean field  billingDefinitionContractUseClientRelationship.
		 */
		populateBillingContractSelector: function() {
			const formPanel = this;
			const useClientRelationship = formPanel.getForm().findField('useBusinessClientRelationshipContract').getValue();

			if (TCG.isNotBlank(useClientRelationship)) {
				const contractDocumentIndexCombo = formPanel.getForm().findField('contractDocumentIndex');
				contractDocumentIndexCombo.clearAndReset();

				const documentFieldCount = 4;
				const fieldNamePrefix = TCG.isTrue(useClientRelationship) ? 'businessClientRelationship' : 'businessClient';
				const contractData = [];

				// Synthesize the field names for the document type and document file and lookup field values and add thier values the combo's data store if both values exist.
				for (let documentIndex = 1; documentIndex <= documentFieldCount; ++documentIndex) {
					const documentTypeFieldName = fieldNamePrefix + 'Contract' + documentIndex + '.' + 'contractType.name';
					const fileFieldName = fieldNamePrefix + 'File' + documentIndex;

					const documentTypeField = formPanel.getForm().findField(documentTypeFieldName);
					const fileField = formPanel.getForm().findField(fileFieldName);

					if (TCG.isNotBlank(documentTypeField.getValue()) && TCG.isNotBlank(fileField.getValue())) {
						const documentIdentifier = documentTypeField.lastSelectionText + ' (Document ' + documentIndex + ')';
						contractData.push([documentIndex, documentIdentifier]);
					}
				}
				contractDocumentIndexCombo.store.loadData(contractData);
			}
		},
		listeners: {
			// After Creating - open the newly created Client Account window
			aftercreate: function(panel, closeOnSuccess) {
				const config = {
					defaultDataIsReal: true,
					params: {id: TCG.getValue('clientInvestmentAccount.id', panel.getForm().formValues)},
					openerCt: panel.ownerCt.openerCt
				};
				TCG.createComponent('Clifton.investment.account.AccountWindow', config);
			}
		},
		items: [
			{name: 'wizardGeneratorBean.id', hidden: true},
			{
				xtype: 'fieldset',
				title: '1. Client Relationship',
				collapsed: false,
				items: [
					{xtype: 'hidden', name: 'clientRelationshipUsed'},
					{
						xtype: 'radiogroup', columns: 3, fieldLabel: '', name: 'clientRelationship', allowBlank: false, submitValue: false,
						items: [
							{boxLabel: 'Use Existing Client Relationship', xtype: 'radio', name: 'clientRelationship', inputValue: 'existing'},
							{boxLabel: 'Add New Client Relationship', xtype: 'radio', name: 'clientRelationship', inputValue: 'new'},
							{boxLabel: 'Do Not Add A Client Relationship', xtype: 'radio', name: 'clientRelationship', inputValue: 'none'}
						],
						listeners: {
							change: (radioGroup, checkedItem) => {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								// Show correct fields
								switch (checkedItem && checkedItem.inputValue) {
									case 'existing':
										formPanel.showHideFormFragment('existingClientRelationship', true);
										formPanel.showHideFormFragment('newClientRelationship', false);
										formPanel.showHideFormFragment('clientRelationshipDocuments', true);
										formPanel.getForm().findField('businessClientRelationship.label').clearAndReset();
										formPanel.getForm().findField('clientRelationshipUsed').setValue(true);
										break;
									case 'new':
										// Must clear existing selection
										formPanel.getForm().findField('businessClientRelationship.label').clearAndReset();
										formPanel.showHideFormFragment('existingClientRelationship', false);
										formPanel.showHideFormFragment('newClientRelationship', true);
										formPanel.showHideFormFragment('clientRelationshipDocuments', true);
										formPanel.getForm().findField('client').setValue('new');
										formPanel.getForm().findField('clientRelationshipUsed').setValue(true);
										break;
									case 'none':
										formPanel.getForm().findField('businessClientRelationship.label').clearAndReset();
										formPanel.showHideFormFragment('existingClientRelationship', false);
										formPanel.showHideFormFragment('newClientRelationship', false);
										formPanel.showHideFormFragment('clientRelationshipDocuments', false);
										formPanel.getForm().findField('clientRelationshipUsed').setValue(false);
										formPanel.clearClientRelationshipDocumentSelections();
										break;
									default:
									// Do nothing
								}
							}
						}
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'existingClientRelationship',
						hidden: true,
						items: [
							{xtype: 'hidden', name: 'businessClientRelationship.company.id', submitValue: false},
							{
								fieldLabel: 'Client Relationship', name: 'businessClientRelationship.label', hiddenName: 'businessClientRelationship.id', xtype: 'combo', url: 'businessClientRelationshipListFind.json?terminated=false', detailPageClass: 'Clifton.business.client.relationship.ClientRelationshipWindow',
								requestedProps: 'company.id', allowBlank: false,
								listeners: {
									// reset company id, billing definition
									select: function(combo, record, index) {
										const formPanel = combo.getParentForm();
										formPanel.setFormValue('businessClientRelationship.company.id', record.json.company.id, true);
										formPanel.getForm().findField('billingDefinition.label').clearAndReset();
									}
								}
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'newClientRelationship',
						hidden: true,
						items: [
							{fieldLabel: 'Short Name', name: 'businessClientRelationship.name', allowBlank: false},
							{fieldLabel: 'Legal Name', name: 'businessClientRelationship.legalName'},
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [
									{
										rows: [
											{xtype: 'hidden', name: 'businessClientRelationship.relationshipCategory.relationshipTypeSystemList.name', submitValue: false},
											{xtype: 'hidden', name: 'businessClientRelationship.relationshipCategory.relationshipSubTypeSystemList.name', submitValue: false},

											{
												fieldLabel: 'Category', name: 'businessClientRelationship.relationshipCategory.name', hiddenName: 'businessClientRelationship.relationshipCategory.id', xtype: 'combo', url: 'businessClientRelationshipCategoryListFind.json', allowBlank: false,
												requestedProps: 'relationshipTypeSystemList.name|relationshipSubTypeSystemList.name',
												listeners: {
													select: function(combo, record, index) {
														const fp = combo.getParentForm();
														fp.setFormValue('businessClientRelationship.relationshipCategory.relationshipTypeSystemList.name', TCG.getValue('relationshipTypeSystemList.name', record.json));
														fp.setFormValue('businessClientRelationship.relationshipCategory.relationshipSubTypeSystemList.name', TCG.getValue('relationshipSubTypeSystemList.name', record.json));
													}
												}
											},
											{
												fieldLabel: 'Firm Category', name: 'businessClientRelationship.relationshipType.text', hiddenName: 'businessClientRelationship.relationshipType.id', xtype: 'system-list-combo', valueField: 'id', allowBlank: false, requiredFields: ['businessClientRelationship.relationshipCategory.name', 'businessClientRelationship.relationshipCategory.relationshipTypeSystemList.name'],
												beforequery: function(queryEvent) {
													const fp = queryEvent.combo.getParentForm();
													queryEvent.combo.listName = fp.getFormValue('businessClientRelationship.relationshipCategory.relationshipTypeSystemList.name', true);
													queryEvent.combo.store.setBaseParam('listName', queryEvent.combo.listName);
												}
											},
											{xtype: 'sectionheaderfield', header: 'Contact Info'},
											{fieldLabel: 'Phone', name: 'businessClientRelationship.company.phoneNumber'},
											{fieldLabel: 'Fax', name: 'businessClientRelationship.company.faxNumber'},
											{fieldLabel: 'E-Fax', name: 'businessClientRelationship.company.efaxNumber'},
											{fieldLabel: 'Email', name: 'businessClientRelationship.company.emailAddress', vtype: 'email'}
										],
										config: {columnWidth: 0.35}
									},
									{
										rows: [
											{xtype: 'displayfield', html: '&nbsp;'},
											{
												fieldLabel: 'Firm Sub-Category', name: 'businessClientRelationship.relationshipSubType.text', hiddenName: 'businessClientRelationship.relationshipSubType.id', xtype: 'system-list-combo', valueField: 'id', requiredFields: ['businessClientRelationship.relationshipCategory.name', 'businessClientRelationship.relationshipCategory.relationshipSubTypeSystemList.name'],
												beforequery: function(queryEvent) {
													const fp = queryEvent.combo.getParentForm();
													queryEvent.combo.listName = fp.getFormValue('businessClientRelationship.relationshipCategory.relationshipSubTypeSystemList.name', true);
													queryEvent.combo.store.setBaseParam('listName', queryEvent.combo.listName);
												}
											},
											{xtype: 'sectionheaderfield', header: 'Address'},
											{
												xtype: 'address-formfragment',
												useFieldSet: false,
												frame: false,
												addressFieldsPrefix: 'businessClientRelationship.company.'
											}
										],
										config: {columnWidth: 0.65}
									}
								]
							},
							{xtype: 'sectionheaderfield', header: 'Relationship Manager(s)'},
							{
								xtype: 'formgrid-scroll',
								storeRoot: 'relationshipManagerContactList',
								dtoClass: 'com.clifton.business.company.contact.BusinessCompanyContact',
								height: 100,
								viewConfig: {forceFit: true, emptyText: 'No Relationship Manager(s) Selected', deferEmptyText: false},
								columnsConfig: [
									{
										header: 'Contact', dataIndex: 'referenceTwo.label', idDataIndex: 'referenceTwo.id', editor: {xtype: 'combo', url: 'businessContactListFind.json?ourCompany=true', displayField: 'label'},
										tooltip: 'If multiple contacts are entered, one should be designated as a primary'
									},
									{header: 'Primary', dataIndex: 'primary', type: 'boolean', editor: {xtype: 'checkbox'}, width: 35}
								]
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'clientRelationshipDocuments',
						hidden: true,
						items: [
							{xtype: 'sectionheaderfield', header: 'Attach new documents to the client relationship'},
							{
								xtype: 'fieldset',
								title: 'Client Relationship Document',
								name: 'crDocumentFieldSet1',
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientRelationshipContract1.contractType.name', hiddenName: 'businessClientRelationshipContract1.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientRelationshipFile1');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientRelationshipFile1', xtype: 'fileuploadfield', requiredFields: ['businessClientRelationshipContract1.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												const documentFieldSet = TCG.getChildByName(formPanel, 'crDocumentFieldSet2');
												documentFieldSet.setVisible(true);
												formPanel.populateBillingContractSelector();
											}
										}
									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Additional Client Relationship Document',
								name: 'crDocumentFieldSet2',
								hidden: true,
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientRelationshipContract2.contractType.name', hiddenName: 'businessClientRelationshipContract2.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientRelationshipFile2');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientRelationshipFile2', xtype: 'fileuploadfield', requiredFields: ['businessClientRelationshipContract2.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												const documentFieldSet = TCG.getChildByName(formPanel, 'crDocumentFieldSet3');
												documentFieldSet.setVisible(true);
												formPanel.populateBillingContractSelector();
											}
										}

									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Additional Client Relationship Document',
								name: 'crDocumentFieldSet3',
								hidden: true,
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientRelationshipContract3.contractType.name', hiddenName: 'businessClientRelationshipContract3.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientRelationshipFile3');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientRelationshipFile3', xtype: 'fileuploadfield', requiredFields: ['businessClientRelationshipContract3.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												const documentFieldSet = TCG.getChildByName(formPanel, 'crDocumentFieldSet4');
												documentFieldSet.setVisible(true);
												formPanel.populateBillingContractSelector();
											}
										}

									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Additional Client Relationship Document',
								name: 'crDocumentFieldSet4',
								hidden: true,
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientRelationshipContract4.contractType.name', hiddenName: 'businessClientRelationshipContract4.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientRelationshipFile4');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientRelationshipFile4', xtype: 'fileuploadfield', requiredFields: ['businessClientRelationshipContract4.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												formPanel.populateBillingContractSelector();
											}
										}
									}
								]
							}
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				title: '2. Client',
				collapsed: false,
				items: [
					{
						xtype: 'radiogroup', columns: 2, fieldLabel: '', name: 'client', allowBlank: false, submitValue: false,
						items: [
							{boxLabel: 'Use Existing Client', xtype: 'radio', name: 'client', inputValue: 'existing', qtip: 'This option can only be used if you are using an existing client relationship.'},
							{boxLabel: 'Add New Client', xtype: 'radio', name: 'client', inputValue: 'new'}
						],
						listeners: {
							change: (radioGroup, checkedItem) => {
								const formPanel = TCG.getParentFormPanel(radioGroup);

								// Show correct fields
								switch (checkedItem && checkedItem.inputValue) {
									case 'existing':
										formPanel.showHideFormFragment('existingClient', true);
										formPanel.showHideFormFragment('newClient', false);
										const businessClientRelationshipFieldValue = formPanel.getFormValue('businessClientRelationship.label');
										// Force-enable this under the condition that it is disabled and a businessClientRelationship value has been previously selected.  In such the control it won't auto-enable itself when switching to 'new' and then back to 'existing'
										const businessClientField = formPanel.getForm().findField('businessClient.label');
										if (TCG.isTrue(businessClientField.disabled) && TCG.isNotBlank(businessClientRelationshipFieldValue)) {
											businessClientField.setDisabled(false);
											businessClientField.setEditable(true);
										}
										break;
									case 'new':
										// Must clear existing selection
										formPanel.getForm().findField('businessClient.label').clearAndReset();
										formPanel.showHideFormFragment('newClient', true);
										formPanel.showHideFormFragment('existingClient', false);
										break;
									default:
									// Do nothing
								}
							}
						}
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'existingClient',
						hidden: true,
						items: [
							{xtype: 'hidden', name: 'businessClient.company.id', submitValue: false},
							{
								fieldLabel: 'Client', name: 'businessClient.label', hiddenName: 'businessClient.id', xtype: 'combo', url: 'businessClientListFind.json?terminated=false', detailPageClass: 'Clifton.business.client.ClientWindow',
								requiredFields: 'businessClientRelationship.id', allowBlank: false,
								requestedProps: ['company.id'],
								listeners: {
									// reset company id, billing definition combo
									select: function(combo, record, index) {
										const formPanel = combo.getParentForm();
										formPanel.setFormValue('businessClient.company.id', record.json.company.id, true);
										formPanel.getForm().findField('billingDefinition.label').clearAndReset();
									}
								},
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									const cr = f.findField('businessClientRelationship.label').getValue();
									combo.store.setBaseParam('clientRelationshipId', cr);
								}
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'newClient',
						hidden: true,
						tbar: ['->',
							{
								text: 'Copy Values from Client Relationship',
								xtype: 'button',
								iconCls: 'copy',

								menu: {
									items: [
										{
											text: 'Copy All Fields',
											tooltip: 'Copies name fields, contact info and main address',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.copyFromClientRelationshipToClient(true, true, true);
											}
										},
										{
											text: 'Copy Names Only',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.copyFromClientRelationshipToClient(true, false, false);
											}
										},
										{
											text: 'Copy Contact Info Only',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.copyFromClientRelationshipToClient(false, true, false);
											}
										},
										{
											text: 'Copy Address Only',
											handler: function() {
												const f = TCG.getParentFormPanel(this);
												f.copyFromClientRelationshipToClient(false, false, true);
											}
										}
									]
								}
							}

						],
						items: [
							{fieldLabel: 'Short Name', name: 'businessClient.name', allowBlank: false},
							{fieldLabel: 'Legal Name', name: 'businessClient.legalName', qtip: 'Legal entity name for this client (if different than name)'},
							{
								xtype: 'columnpanel',
								defaults: {xtype: 'textfield'},
								columns: [
									{
										rows: [
											{fieldLabel: 'Client Type', name: 'businessClient.clientType.text', hiddenName: 'businessClient.clientType.id', xtype: 'system-list-combo', listName: 'Business Client Types', allowBlank: false, valueField: 'id'},
											{fieldLabel: 'Origin', name: 'businessClient.origin', xtype: 'system-list-combo', listName: 'Client Origin Options'},
											{
												fieldLabel: 'Domicile', name: 'businessClient.originDomicile', xtype: 'combo', displayField: 'text', tooltipField: 'tooltip', valueField: 'text', url: 'systemListItemListFind.json',
												requiredFields: ['businessClient.origin'],
												beforequery: function(queryEvent) {
													const cmb = queryEvent.combo;
													const f = TCG.getParentFormPanel(cmb).getForm();
													const origin = f.findField('businessClient.origin').getValue();
													if (origin === 'United States') {
														cmb.store.baseParams = {
															listName: 'States'
														};
													}
													else if (origin === 'International Entity') {
														cmb.store.baseParams = {
															listName: 'Countries'
														};
													}
													else if (origin === 'Canada') {
														cmb.store.baseParams = {
															listName: 'Canadian Provinces'
														};
													}
													else {
														TCG.showError('Domicile selection only applies where Origin = United States, Canada, or International Equity');
														return false;
													}
												}
											},
											{xtype: 'sectionheaderfield', header: 'Contact Info'},
											{fieldLabel: 'Phone', name: 'businessClient.company.phoneNumber'},
											{fieldLabel: 'Fax', name: 'businessClient.company.faxNumber'},
											{fieldLabel: 'E-Fax', name: 'businessClient.company.efaxNumber'},
											{fieldLabel: 'Email', name: 'businessClient.company.emailAddress', vtype: 'email'}
										],
										config: {columnWidth: 0.35}
									},
									{
										rows: [
											{boxLabel: 'This Client is taxable', xtype: 'checkbox', name: 'businessClient.taxable'},
											{fieldLabel: 'Federal Tax #', name: 'businessClient.federalTaxNumber'},
											{fieldLabel: 'State Tax #', name: 'businessClient.stateTaxNumber'},
											{xtype: 'sectionheaderfield', header: 'Address'},
											{
												xtype: 'address-formfragment',
												useFieldSet: false,
												frame: false,
												addressFieldsPrefix: 'businessClient.company.'
											}
										],
										config: {columnWidth: 0.65}
									}
								]
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'clientDocuments',
						hidden: false,
						items: [
							{xtype: 'sectionheaderfield', header: 'Attach new client documents to this client'},
							{
								xtype: 'fieldset',
								title: 'Client Document',
								name: 'clDocumentFieldSet1',
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientContract1.contractType.name', hiddenName: 'businessClientContract1.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientFile1');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientFile1', xtype: 'fileuploadfield', requiredFields: ['businessClientContract1.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												const documentFieldSet = TCG.getChildByName(formPanel, 'clDocumentFieldSet2');
												documentFieldSet.setVisible(true);
												formPanel.populateBillingContractSelector();
											}
										}
									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Additional Client Document',
								name: 'clDocumentFieldSet2',
								hidden: true,
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientContract2.contractType.name', hiddenName: 'businessClientContract2.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientFile2');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientFile2', xtype: 'fileuploadfield', requiredFields: ['businessClientContract2.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												const documentFieldSet = TCG.getChildByName(formPanel, 'clDocumentFieldSet3');
												documentFieldSet.setVisible(true);
												formPanel.populateBillingContractSelector();
											}
										}

									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Additional Client Document',
								name: 'clDocumentFieldSet3',
								hidden: true,
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientContract3.contractType.name', hiddenName: 'businessClientContract3.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientFile3');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientFile3', xtype: 'fileuploadfield', requiredFields: ['businessClientContract3.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												const documentFieldSet = TCG.getChildByName(formPanel, 'clDocumentFieldSet4');
												documentFieldSet.setVisible(true);
												formPanel.populateBillingContractSelector();
											}
										}
									}
								]
							},
							{
								xtype: 'fieldset',
								title: 'Additional Client Document',
								name: 'clDocumentFieldSet4',
								hidden: true,
								collapsed: false,
								items: [
									{
										fieldLabel: 'Document Type', name: 'businessClientContract4.contractType.name', hiddenName: 'businessClientContract4.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json',
										listeners: {
											select: function(combo, record, index) {
												const formPanel = TCG.getParentFormPanel(combo);
												const fileUploadField = formPanel.getForm().findField('businessClientFile4');
												fileUploadField.reset();
											}
										}
									},
									{
										fieldLabel: 'File', name: 'businessClientFile4', xtype: 'fileuploadfield', requiredFields: ['businessClientContract4.contractType.name'], allowBlank: false,
										listeners: {
											fileselected: function() {
												const formPanel = TCG.getParentFormPanel(this);
												formPanel.populateBillingContractSelector();
											}
										}
									}
								]
							}
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				title: '3. Client Account',
				collapsed: false,
				tbar: ['->',
					{
						text: 'Copy Account Name(s) from Client',
						xtype: 'button',
						iconCls: 'copy',
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							f.copyFromClientToClientAccount(true);
						}
					}
				],
				items: [
					{
						xtype: 'radiogroup', columns: 3, fieldLabel: '', name: 'clientAccountRadioButtonGroup', allowBlank: false, submitValue: false,
						items: [
							{boxLabel: 'Use Existing Client Account', xtype: 'radio', name: 'clientAccountRadioButtonGroup', inputValue: 'existing'},
							{boxLabel: 'Add New Client Account', xtype: 'radio', name: 'clientAccountRadioButtonGroup', inputValue: 'new'}
						],
						listeners: {
							change: (radioGroup, checkedItem) => {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								// Show correct fields
								switch (checkedItem && checkedItem.inputValue) {
									case 'existing':
										formPanel.showHideFormFragment('existingClientAccount', true);
										formPanel.showHideFormFragment('newClientAccount', false);
										const businessClientFieldValue = formPanel.getFormValue('businessClient.label');
										// force-enable this control under the conditions where it is disabled and a businessClient value has been previously selected. The control won't auto-enable itself when switching to 'new' and then back to 'existing'
										const clientAccountField = formPanel.getForm().findField('clientInvestmentAccount.label');
										if (TCG.isTrue(clientAccountField.disabled) && TCG.isNotBlank(businessClientFieldValue)) {
											clientAccountField.setDisabled(false);
											clientAccountField.setEditable(true);
										}
										break;
									case 'new':
										// Must clear existing selection
										formPanel.getForm().findField('clientInvestmentAccount.label').clearAndReset();
										formPanel.showHideFormFragment('existingClientAccount', false);
										formPanel.showHideFormFragment('newClientAccount', true);
										formPanel.showHideFormFragment('holdingAccountConfigurationGrids', true);
										break;
									default:
									// Do nothing
								}
							}
						}
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'existingClientAccount',
						hidden: true,
						items: [
							{
								fieldLabel: 'Client Account', name: 'clientInvestmentAccount.label', hiddenName: 'clientInvestmentAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json',
								displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true, requiredFields: ['businessClient.label'],
								requestedProps: 'id|label', allowBlank: false,
								listeners: {
									beforequery: function(queryEvent) {
										const formPanel = this.getParentForm();
										const businessClientId = formPanel.getFormValue('businessClient.id');
										queryEvent.combo.store.baseParams = {
											ourAccount: true,
											excludeWorkflowStatusName: 'Closed',
											clientId: businessClientId
										};
									}
								}
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'newClientAccount',
						hidden: true,
						items: [
							{fieldLabel: 'Account Name', name: 'clientInvestmentAccount.name', allowBlank: false},
							{
								xtype: 'columnpanel',
								columns: [{
									rows: [
										{fieldLabel: 'Account Number', name: 'clientInvestmentAccount.number', xtype: 'textfield', allowBlank: false},
										{fieldLabel: 'Base Currency', name: 'clientInvestmentAccount.baseCurrency.label', hiddenName: 'clientInvestmentAccount.baseCurrency.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', allowBlank: false},
										{
											fieldLabel: 'FX Source', name: 'clientInvestmentAccount.defaultExchangeRateSourceCompany.name', hiddenName: 'clientInvestmentAccount.defaultExchangeRateSourceCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
											qtip: 'The default company (uses parent name if applies) to use for retrieving FX rates. Applies to both Client and Holding Accounts and can be used to override default behaviour.<br/>Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Holding Account Issuer, Default: Goldman Sachs)<br/>When holding account is not available (manager balances, billing, etc.), client account FX Source is used: COALESCE(Client Account FX Source, Default: Goldman Sachs).'
										},
										{
											fieldLabel: 'Service', name: 'clientInvestmentAccount.businessService.name', hiddenName: 'clientInvestmentAccount.businessService.id', xtype: 'combo', displayField: 'name', queryParam: 'nameExpanded', tooltipField: 'nameExpanded', url: 'businessServiceListFind.json?serviceLevelType=SERVICE', detailPageClass: 'Clifton.business.service.ServiceWindow',
											requestedProps: 'aumCalculatorBean.labelShort',
											listeners: {
												beforequery: function(queryEvent) {
													const s = queryEvent.combo.store;
													s.setBaseParam('activeOnDate', new Date().format('m/d/Y'));
												},
												select: function(combo, record, index) {
													const fp = this.getParentForm();
													const serviceProcessingTypeCombo = TCG.getChildByName(fp, 'clientInvestmentAccount.serviceProcessingType.name');
													serviceProcessingTypeCombo.store.removeAll();
													serviceProcessingTypeCombo.lastQuery = null;
													serviceProcessingTypeCombo.doQuery();

													// Set AUM Calculator
													if (record.json.aumCalculatorBean) {
														fp.setFormValue('clientInvestmentAccount.businessService.aumCalculatorBean.labelShort', record.json.aumCalculatorBean.labelShort, true);
													}
												}
											}
										},
										{fieldLabel: 'AUM Calculator', name: 'clientInvestmentAccount.businessService.aumCalculatorBean.labelShort', xtype: 'displayfield', qtip: 'AUM calculator as defined by the service.  Can be overridden for this account.'},
										{fieldLabel: 'Pooled Vehicle Type', name: 'clientInvestmentAccount.clientAccountType', xtype: 'system-list-combo', listName: 'Client Account Types', qtip: 'Client Account Type - Used to define types specifically for our client accounts'},
										{fieldLabel: 'EV Portfolio Code', name: 'clientInvestmentAccount.externalPortfolioCode.text', hiddenName: 'clientInvestmentAccount.externalPortfolioCode.id', xtype: 'system-list-combo', listName: 'Client Account: Portfolio Codes', valueField: 'id', qtip: 'EV Portfolio/Product Codes - used to classify our client accounts for Eaton Vance'},
										{fieldLabel: 'Channel', name: 'clientInvestmentAccount.clientAccountChannel.text', hiddenName: 'clientInvestmentAccount.clientAccountChannel.id', xtype: 'system-list-combo', listName: 'Client Account: Channels', valueField: 'id', qtip: 'Designated unit to be credited with the "sale" of the product used by the client as indicated on the Client Account.', allowBlank: false}

									]
								}, {
									rows: [
										{fieldLabel: 'Short Name', name: 'clientInvestmentAccount.shortName', xtype: 'textfield', qtip: 'Optional short name for accounts with long legal names. When populated, will be used within UI for account labels.'},
										{fieldLabel: 'Issued By', name: 'clientInvestmentAccount.issuingCompany.name', hiddenName: 'clientInvestmentAccount.issuingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=true', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true, allowBlank: false},
										{fieldLabel: 'Servicing Team', name: 'clientInvestmentAccount.teamSecurityGroup.name', hiddenName: 'clientInvestmentAccount.teamSecurityGroup.id', xtype: 'combo', url: 'securityGroupTeamList.json', loadAll: true, detailPageClass: 'Clifton.security.user.GroupWindow'},
										{
											fieldLabel: 'Processing Type', name: 'clientInvestmentAccount.serviceProcessingType.name', hiddenName: 'clientInvestmentAccount.serviceProcessingType.id', xtype: 'combo', url: 'businessServiceProcessingTypeListFind.json', requiredFields: ['clientInvestmentAccount.businessService.name'], detailPageClass: 'Clifton.business.service.ProcessingTypeWindow',
											qtip: 'Defines the processing type that should be used by this Client Account for its portfolio management. Different processing types may use additional data (tables) specific to them and will drive corresponding UI (usually by adding tabs to Client Account).',
											listeners: {
												afterrender: function(combo) {
													this.store.on('load', function(store, records) {
														if (records.length === 1) {
															combo.setValue(records[0].id);
														}
													});
												},

												beforequery: function(queryEvent) {
													const fp = this.getParentForm();
													const businessServiceId = TCG.getChildByName(fp, 'clientInvestmentAccount.businessService.name').getValue();
													if (TCG.isBlank(businessServiceId)) {
														return;
													}
													const loader = new TCG.data.JsonLoader({
														waitTarget: fp,
														params: {
															businessServiceId: businessServiceId
														},
														onLoad: function(recArray, conf) {
															if (TCG.isNotBlank(recArray)) {
																const allowedServiceProcessingTypeIdList = [];
																for (const rec of recArray) {
																	if (TCG.isNotBlank(rec.businessServiceProcessingType.id)) {
																		allowedServiceProcessingTypeIdList.push(rec.businessServiceProcessingType.id);
																	}
																}

																if (allowedServiceProcessingTypeIdList.length === 0) {
																	TCG.showError('Cannot find any business service processing types for the selected business service.');
																	return;
																}
																queryEvent.combo.store.baseParams = {ids: allowedServiceProcessingTypeIdList};
															}
														}
													});
													loader.loadSynchronous('businessServiceBusinessServiceProcessingTypeListFind.json');
												}
											}
										},
										{fieldLabel: 'AUM Calculator Override', beanName: 'clientInvestmentAccount.aumCalculatorOverrideBean', xtype: 'system-bean-combo', groupName: 'Investment Account Calculation Snapshot Calculator', qtip: 'If the account should use a different AUM Calculator than what the service uses by default, that selection can be made here.'},
										{fieldLabel: 'GIPS (Account Discretion)', name: 'clientInvestmentAccount.discretionType', xtype: 'system-list-combo', listName: 'Client Account Discretion Options (GIPS)'},
										{fieldLabel: 'EV Product Code', name: 'clientInvestmentAccount.externalProductCode.text', hiddenName: 'clientInvestmentAccount.externalProductCode.id', xtype: 'system-list-combo', listName: 'Client Account: Product Codes', valueField: 'id'}
									]
								}]
							}
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				title: '4. Holding Account',
				collapsed: false,
				tbar: ['->',
					{
						text: 'Copy Account Name(s) from Client Account',
						xtype: 'button',
						iconCls: 'copy',
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							f.copyFromClientAccountToHoldingAccount(true);
						}
					}
				],
				items: [
					{xtype: 'hidden', name: 'holdingAccountUsed'},
					{
						xtype: 'radiogroup', columns: 3, fieldLabel: '', name: 'holdingAccountRadioButtonGroup', allowBlank: false, submitValue: false,
						items: [
							{boxLabel: 'Use Existing Holding Account', xtype: 'radio', name: 'holdingAccountRadioButtonGroup', inputValue: 'existing'},
							{boxLabel: 'Add New Holding Account', xtype: 'radio', name: 'holdingAccountRadioButtonGroup', inputValue: 'new'},
							{boxLabel: 'Do Not Add A Holding Account', xtype: 'radio', name: 'holdingAccountRadioButtonGroup', inputValue: 'none'}
						],
						listeners: {
							change: (radioGroup, checkedItem) => {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								// Show correct fields
								switch (checkedItem && checkedItem.inputValue) {
									case 'existing':
										formPanel.showHideFormFragment('existingHoldingAccount', true);
										formPanel.showHideFormFragment('newHoldingAccount', false);
										formPanel.showHideFormFragment('holdingAccountConfigurationGrids', true);
										const businessClientFieldValue = formPanel.getFormValue('businessClient.label');
										// force-enable this control under the conditions where it is disabled and a businessClient value has been previously selected. The control won't auto-enable itself when switching to 'new' and then back to 'existing'
										const holdingAccountField = formPanel.getForm().findField('holdingInvestmentAccount.label');
										if (TCG.isTrue(holdingAccountField.disabled) && TCG.isNotBlank(businessClientFieldValue)) {
											holdingAccountField.setDisabled(false);
											holdingAccountField.setEditable(true);
										}
										formPanel.getForm().findField('holdingAccountUsed').setValue(true);
										break;
									case 'new':
										// Must clear existing selection
										formPanel.getForm().findField('holdingInvestmentAccount.label').clearAndReset();
										formPanel.showHideFormFragment('existingHoldingAccount', false);
										formPanel.showHideFormFragment('newHoldingAccount', true);
										formPanel.showHideFormFragment('holdingAccountConfigurationGrids', true);
										formPanel.getForm().findField('holdingAccountUsed').setValue(true);
										break;
									case 'none':
										formPanel.getForm().findField('holdingInvestmentAccount.label').clearAndReset();
										formPanel.showHideFormFragment('existingHoldingAccount', false);
										formPanel.showHideFormFragment('newHoldingAccount', false);
										formPanel.showHideFormFragment('holdingAccountConfigurationGrids', false);
										formPanel.getForm().findField('holdingAccountUsed').setValue(false);
										break;
									default:
									// Do nothing
								}
							}
						}
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'existingHoldingAccount',
						hidden: true,
						items: [
							{
								fieldLabel: 'Holding Account', name: 'holdingInvestmentAccount.label', hiddenName: 'holdingInvestmentAccount.id', xtype: 'combo', url: 'investmentAccountListFind.json',
								displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true, requiredFields: ['businessClient.label'],
								requestedProps: 'id|label', allowBlank: false,
								listeners: {
									beforequery: function(queryEvent) {
										const formPanel = this.getParentForm();
										const businessClientId = formPanel.getFormValue('businessClient.id');
										queryEvent.combo.store.baseParams = {
											ourAccount: false,
											excludeWorkflowStatusName: 'Closed',
											clientId: businessClientId
										};
									}
								}
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'newHoldingAccount',
						hidden: true,
						items: [
							{
								xtype: 'columnpanel',
								columns: [{
									rows: [
										{
											fieldLabel: 'Account Type', name: 'holdingInvestmentAccount.type.label', hiddenName: 'holdingInvestmentAccount.type.id', displayField: 'label', xtype: 'combo', url: 'investmentAccountTypeListFind.json?ourAccount=false', limitRequestedProperties: false, detailPageClass: 'Clifton.investment.account.AccountTypeWindow', disableAddNewItem: true, allowBlank: false,
											listeners: {
												select: function(combo, record, index) {
													const fp = combo.getParentForm();
													fp.toggleHoldingAccountNumbers(record.json);
												}
											}
										},
										{fieldLabel: 'Account Number', name: 'holdingInvestmentAccount.number', xtype: 'textfield', allowBlank: false},
										{fieldLabel: 'Account Number2', name: 'holdingInvestmentAccount.number2', xtype: 'textfield', hidden: true},
										{fieldLabel: 'Base Currency', name: 'holdingInvestmentAccount.baseCurrency.label', hiddenName: 'holdingInvestmentAccount.baseCurrency.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', disableAddNewItem: true, allowBlank: false},
										{
											fieldLabel: 'Platform', xtype: 'combo', name: 'holdingInvestmentAccount.companyPlatform.name', hiddenName: 'holdingInvestmentAccount.companyPlatform.id', url: 'businessCompanyPlatformListFind.json?active=true', detailPageClass: 'Clifton.business.company.CompanyPlatformWindow',
											qtip: 'A company platform is a Broker-Dealer or RIA program providing access to a variety of investment alternatives including third party investment managers\' strategies. Each platform is unique to a particular company, and the only selections allowed are those associated with the issuing company of this account.',
											requiredFields: ['holdingInvestmentAccount.issuingCompany.id'],
											beforequery: function(queryEvent) {
												const fp = queryEvent.combo.getParentForm();
												const businessCompanyId = fp.getFormValue('holdingInvestmentAccount.issuingCompany.id', true);
												queryEvent.combo.store.setBaseParam('businessCompanyId', businessCompanyId);
											},
											getDefaultData: function(f) { // defaults client/counterparty/isda
												const fp = TCG.getParentFormPanel(this);
												const issuingCompany = fp.getFormValue('holdingInvestmentAccount.issuingCompany');
												return {'businessCompany': issuingCompany};
											},
											listeners: {
												change: function(field, newValue, oldValue) {
													// Because WRAP account is a required radio box field, when clearing the platform we must set wrap account = false
													if (TCG.isBlank(newValue)) {
														const fp = TCG.getParentFormPanel(field);
														fp.getForm().findField('holdingInvestmentAccount.wrapAccount').items.items[1].setValue(true);
													}
												}
											}
										},
										{
											xtype: 'radiogroup', columns: 2, fieldLabel: 'Wrap Account', name: 'holdingInvestmentAccount.wrapAccount', requiredFields: ['holdingInvestmentAccount.companyPlatform.name'], allowBlank: false, doNotSubmit: false,
											qtip: 'Required selection if a platform is selected. Differentiates accounts where the client pays a periodic account fee rather than trade commissions.  Is tied to ‘Platform’ in IMS so that we may differentiate between clients who are vs. are not utilizing the platform WRAP feature (retail clients generally do (Institutional clients generally do not) participate in the WRAP feature/account structure on a platform.',
											items: [
												{boxLabel: 'Yes', xtype: 'radio', name: 'holdingInvestmentAccount.wrapAccount', inputValue: true},
												{boxLabel: 'No', xtype: 'radio', name: 'holdingInvestmentAccount.wrapAccount', inputValue: false}
											]
										}
									]

								}, {
									rows: [
										{
											fieldLabel: 'Issued By', name: 'holdingInvestmentAccount.issuingCompany.name', hiddenName: 'holdingInvestmentAccount.issuingCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?issuer=true&ourCompany=false', detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true, allowBlank: false,
											listeners: {
												change: function(field, newValue, oldValue) {
													const fp = TCG.getParentFormPanel(field);
													if (TCG.isTrue(fp.getFormValue('holdingInvestmentAccount.type.defaultExchangeRateSourceCompanySameAsIssuer'))) {
														fp.setFormValue('holdingInvestmentAccount.defaultExchangeRateSourceCompany.id', {value: newValue, text: field.getRawValue()});
													}
												}
											}
										},
										{fieldLabel: 'Account Name', name: 'holdingInvestmentAccount.name', xtype: 'textfield', allowBlank: false},
										{fieldLabel: 'Short Name', name: 'holdingInvestmentAccount.shortName', xtype: 'textfield', qtip: 'Optional short name for accounts with long legal names. When populated, will be used within UI for account labels.'},
										{
											fieldLabel: 'FX Source', name: 'holdingInvestmentAccount.defaultExchangeRateSourceCompany.name', hiddenName: 'holdingInvestmentAccount.defaultExchangeRateSourceCompany.id', xtype: 'combo', url: 'marketDataCompanyForExchangeRatesList.json', loadAll: true, detailPageClass: 'Clifton.business.company.CompanyWindow', disableAddNewItem: true,
											qtip: 'The default company (uses parent name if applies) to use for retrieving FX rates. Applies to both Client and Holding Accounts and can be used to override default behaviour.<br/>Whenever possible, holding account FX Source is used: COALESCE(Holding Account FX Source, Holding Account Issuer, Default: Goldman Sachs)<br/>When holding account is not available (manager balances, billing, etc.), client account FX Source is used: COALESCE(Client Account FX Source, Default: Goldman Sachs).'
										},
										{fieldLabel: 'Initial Margin Multiplier', name: 'holdingInvestmentAccount.initialMarginMultiplier', xtype: 'floatfield', value: '1', qtip: 'This field applies to holding accounts and is usually equal to 1 (no additional requirements: use standard). However, a broker may deem a certain account to be more risky and impose additional requirement: 1.5, 2.5, 3.25, etc.'}
									]
								}]
							},
							{boxLabel: 'This account is used for speculation rather than hedging (may require higher margin)', xtype: 'checkbox', name: 'holdingInvestmentAccount.usedForSpeculation'},
							{boxLabel: 'Trading for this account is Directed by the Client', xtype: 'checkbox', name: 'holdingInvestmentAccount.clientDirected'}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'holdingAccountConfigurationGrids',
						hidden: true,
						items: [
							{xtype: 'sectionheaderfield', header: 'Relationship Purpose(s) between client and holding account'},
							{fieldLabel: 'Start Date', xtype: 'datefield', name: 'relationshipPurposeStartDate'},
							{
								xtype: 'formgrid-scroll',
								storeRoot: 'relationshipPurposeList',
								dtoClass: 'com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose',
								height: 150,
								viewConfig: {forceFit: true, emptyText: 'No Account Relationship Purpose(s) Selected', deferEmptyText: false},
								columnsConfig: [
									{header: 'Purpose', dataIndex: 'name', idDataIndex: 'id', editor: {xtype: 'combo', url: 'investmentAccountRelationshipPurposeListFind.json'}}
								]
							},
							{xtype: 'sectionheaderfield', header: 'Restricted Broker(s) - Optional'},
							{
								xtype: 'formgrid-scroll',
								storeRoot: 'restrictedBrokerCompanyList',
								dtoClass: 'com.clifton.business.company.BusinessCompany',
								height: 150,
								viewConfig: {forceFit: true, emptyText: 'No Broker(s) Selected - can trade with any broker unless holding account type requires trading only with its issuer (i.e. Directed Broker)', deferEmptyText: false},
								columnsConfig: [
									{
										header: 'Broker Company', dataIndex: 'name', idDataIndex: 'id', editor: {xtype: 'combo', url: 'businessCompanyListFind.json?tradingAllowed=true'},
										tooltip: 'If trading is restricted to a specific broker or set of brokers they can be entered here.  Note: Should NOT be used if the holding account is a type that requires execution with issuer, i.e. Directed Broker account type.'
									}
								]
							}
						]
					}
				]
			},
			{
				xtype: 'fieldset',
				title: '5. Billing',
				collapsed: false,
				instructions: 'Use the following to do the initial billing set up for this account (optional).  In some cases you may not be able to fully set up the billing definition, however you can optionally leave the billing definition in Draft state and return to it after the account is finished being set up.',
				items: [
					{xtype: 'hidden', name: 'billingUsed'},
					{
						xtype: 'radiogroup', columns: 3, fieldLabel: '', name: 'billingDefinitionOption', allowBlank: false, submitValue: false,
						items: [
							{boxLabel: 'Add Account to Existing Billing Definition', xtype: 'radio', name: 'billingDefinitionOption', inputValue: 'existing'},
							{boxLabel: 'Create new Billing Definition', xtype: 'radio', name: 'billingDefinitionOption', inputValue: 'new'},
							{boxLabel: 'Do Not setup Billing', xtype: 'radio', name: 'billingDefinitionOption', inputValue: 'none'}
						],
						listeners: {
							change: (radioGroup, checkedItem) => {
								const formPanel = TCG.getParentFormPanel(radioGroup);
								formPanel.showHideFormFragment('newBillingContract', false); // Disables required fields - will re-enable if selected
								// Show correct fields
								switch (checkedItem && checkedItem.inputValue) {
									case 'existing':
										formPanel.getForm().findField('billingUsed').setValue(true);
										formPanel.showHideFormFragment('newBilling', false);
										formPanel.showHideFormFragment('newBillingSchedule', false);
										formPanel.showHideFormFragment('billing', true);
										formPanel.showHideFormFragment('existingBilling', true);
										formPanel.showHideFormFragment('existingBillingContract', false);
										formPanel.getForm().findField('billingDefinitionContractOption').reset();
										break;
									case 'new':
										// Must clear existing selection
										formPanel.getForm().findField('billingUsed').setValue(true);
										formPanel.getForm().findField('billingDefinition.label').clearAndReset();
										formPanel.showHideFormFragment('newBilling', true);
										formPanel.showHideFormFragment('newBillingSchedule', true);
										formPanel.showHideFormFragment('billing', true);
										formPanel.showHideFormFragment('existingBilling', false);
										break;
									default:
										// Must clear existing selection
										formPanel.getForm().findField('billingUsed').setValue(false);
										formPanel.getForm().findField('billingDefinition.label').clearAndReset();
										formPanel.showHideFormFragment('newBilling', false);
										formPanel.showHideFormFragment('newBillingSchedule', false);
										formPanel.showHideFormFragment('billing', false);
										formPanel.showHideFormFragment('existingBilling', false);
										formPanel.showHideFormFragment('existingBillingContract', false);
										formPanel.getForm().findField('billingDefinitionContractOption').reset();
								}
							}
						}
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'existingBilling',
						hidden: true,
						items: [
							{
								fieldLabel: 'Billing Definition', xtype: 'combo', name: 'billingDefinition.label', hiddenName: 'billingDefinition.id', url: 'billingDefinitionListFind.json?active=true', displayField: 'label', allowBlank: false, detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
								beforequery: function(queryEvent) {
									const fp = queryEvent.combo.getParentForm();
									const clientCompanyId = fp.getFormValue('businessClient.company.id', true);
									const clientRelationshipCompanyId = fp.getFormValue('businessClientRelationship.company.id', true);
									if (!TCG.isTrue(fp.getFormValue('allowAnyClientBillingDefinition', true))) {
										queryEvent.combo.store.setBaseParam('includeClientOrClientRelationship', true);
										if (TCG.isNotBlank(clientCompanyId)) {
											queryEvent.combo.store.setBaseParam('includeClientOrClientRelationship', true);
											queryEvent.combo.store.setBaseParam('companyIdOrRelatedCompany', clientCompanyId);
										}
										else if (TCG.isNotBlank(clientRelationshipCompanyId)) {
											queryEvent.combo.store.setBaseParam('companyIdOrRelatedCompany', clientRelationshipCompanyId);
										}
										else {
											TCG.showError('Cannot use existing billing definition for a new client relationship unless the option to select any is enabled.');
											return false;
										}
									}
									else {
										queryEvent.combo.store.setBaseParam('includeClientOrClientRelationship', false);
										queryEvent.combo.store.setBaseParam('companyIdOrRelatedCompany', null);
									}
								}
							},
							{
								fieldLabel: '', xtype: 'checkbox', boxLabel: 'Allow Selecting Billing Definition for Any Client / Client Relationship (Rare)', name: 'allowAnyClientBillingDefinition', submitValue: false,
								listeners: {
									check: function(f) {
										// On changes reset billing definition drop down
										const p = TCG.getParentFormPanel(f);
										const bc = p.getForm().findField('billingDefinition.label');
										bc.clearValue();
										bc.store.removeAll();
										bc.lastQuery = null;
									}
								}
							}
						]
					},

					{
						xtype: 'formfragment',
						frame: false,
						name: 'newBilling',
						hidden: true,
						items: [
							{xtype: 'sectionheaderfield', header: 'Billing Contract'},
							{xtype: 'hidden', name: 'useBusinessClientRelationshipContract'},
							{
								xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'billingDefinitionContractOption', allowBlank: false, submitValue: false,
								items: [
									{boxLabel: 'Use Client Relationship Contract (Attached above)', xtype: 'radio', name: 'billingDefinitionContractOption', inputValue: 'useBusinessClientRelationshipContract'},
									{boxLabel: 'Use Client Contract (Attached above)', xtype: 'radio', name: 'billingDefinitionContractOption', inputValue: 'useBusinessClientContract'},
									{boxLabel: 'Attach New Contract', xtype: 'radio', name: 'billingDefinitionContractOption', inputValue: 'new'}
								],
								listeners: {
									change: (radioGroup, checkedItem) => {
										const formPanel = TCG.getParentFormPanel(radioGroup);
										// Show correct fields
										switch (checkedItem && checkedItem.inputValue) {
											case 'new':
												formPanel.showHideFormFragment('newBillingContract', true);
												formPanel.showHideFormFragment('existingBillingContract', false);
												formPanel.getForm().findField('useBusinessClientRelationshipContract').setValue(false);
												formPanel.getForm().findField('billingDefinitionContract.contractType.name').clearValue();

												break;
											case 'useBusinessClientRelationshipContract':
												formPanel.showHideFormFragment('newBillingContract', false);
												formPanel.showHideFormFragment('existingBillingContract', true);
												formPanel.getForm().findField('useBusinessClientRelationshipContract').setValue(true);
												formPanel.getForm().findField('billingDefinitionContract.contractType.name').clearValue();
												formPanel.populateBillingContractSelector();
												break;
											case 'useBusinessClientContract':
												formPanel.showHideFormFragment('newBillingContract', false);
												formPanel.showHideFormFragment('existingBillingContract', true);
												formPanel.getForm().findField('useBusinessClientRelationshipContract').setValue(false);
												formPanel.getForm().findField('billingDefinitionContract.contractType.name').clearValue();
												formPanel.populateBillingContractSelector();
												break;
											default:
											// Do Nothing
										}
									}
								}
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'existingBillingContract',
						hidden: true,
						items: [
							{
								fieldLabel: 'Existing Contract', xtype: 'combo', hiddenName: 'contractDocumentIndex', displayField: 'name', valueField: 'id', mode: 'local', allowBlank: false,
								store: new Ext.data.ArrayStore({
									fields: ['id', 'name'],
									data: []
								})
							}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'newBillingContract',
						hidden: true,
						items: [
							{
								xtype: 'radiogroup', columns: 2, fieldLabel: '', name: 'billingDefinitionContractUseClientRelationship', allowBlank: false,
								items: [
									{boxLabel: 'Associate Contract with Client Relationship', xtype: 'radio', name: 'billingDefinitionContractUseClientRelationship', inputValue: true},
									{boxLabel: 'Associate Contract with Client', xtype: 'radio', name: 'billingDefinitionContractUseClientRelationship', inputValue: false}
								]
							},
							{fieldLabel: 'Document Type', name: 'billingDefinitionContract.contractType.name', hiddenName: 'billingDefinitionContract.contractType.id', xtype: 'combo', url: 'businessContractTypeListFind.json', allowBlank: false},
							{fieldLabel: 'File', name: 'billingDefinitionContractFile', xtype: 'fileuploadfield', allowBlank: false, requiredFields: ['billingDefinitionContract.contractType.name']}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'newBillingSchedule',
						hidden: true,
						items: [
							{
								fieldLabel: 'Invoice Type', name: 'billingInvoiceType.name', hiddenName: 'billingInvoiceType.id', xtype: 'combo', url: 'billingInvoiceTypeListFind.json?billingDefinitionRequired=true&revenue=true', detailPageClass: 'Clifton.billing.invoice.TypeWindow',
								requestedProps: 'invoicePostedToPortalDefault', allowBlank: false,
								listeners: {
									// set post to portal if a new billing definition and invoice type is selected to do so
									select: function(combo, record, index) {
										const formPanel = combo.getParentForm();
										formPanel.getForm().findField('invoicePostedToPortal').setValue(TCG.isTrue(record.json.invoicePostedToPortalDefault));
									}
								}
							},
							{xtype: 'checkbox', name: 'invoicePostedToPortal', fieldLabel: '', boxLabel: 'Invoices for this billing definition are posted to the portal.'},
							{
								fieldLabel: 'Billing Frequency', name: 'billingFrequency', hiddenName: 'billingFrequency', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo', allowBlank: false,
								store: new Ext.data.ArrayStore({
									fields: ['name', 'value', 'description'],
									data: Clifton.billing.BillingFrequencies
								})
							},
							{xtype: 'sectionheaderfield', header: 'Schedule'},
							{fieldLabel: 'Schedule Template', xtype: 'combo', name: 'billingScheduleTemplate.name', hiddenName: 'billingScheduleTemplate.id', url: 'billingScheduleTemplateListFind.json', mutuallyExclusiveFields: ['doNotAddSchedule']},
							{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Do Not Create Schedule From Existing Template (Manually Add Schedule to Billing Definition after Wizard completes)', name: 'doNotAddSchedule', mutuallyExclusiveFields: ['billingScheduleTemplate.name']}
						]
					},
					{
						xtype: 'formfragment',
						frame: false,
						name: 'billing',
						hidden: true,
						items: [
							{xtype: 'sectionheaderfield', header: 'Account Valuation'},
							{fieldLabel: 'Valuation Type', xtype: 'combo', name: 'billingBasisValuationType.name', hiddenName: 'billingBasisValuationType.id', url: 'billingBasisValuationTypeListFind.json', loadAll: true, allowBlank: false},
							{fieldLabel: 'Calculation Type', xtype: 'combo', name: 'billingBasisCalculationType.name', hiddenName: 'billingBasisCalculationType.id', url: 'billingBasisCalculationTypeListFind.json?inactive=false', loadAll: true, allowBlank: false},
							{fieldLabel: '', xtype: 'checkbox', boxLabel: 'Leave Billing Definition in Draft State', name: 'billingDraft', qtip: 'Leave unchecked to have the billing definition submitted for approval.  If there are additional items that you need to add after the initial "skeleton" of the definition is set up, check here to leave it in Draft state.'}
						]
					}
				]
			}
		],

		copyFromClientRelationshipToClient: function(names, contactInfo, address) {
			const f = this;
			if (TCG.isNotBlank(f.getFormValue('businessClientRelationship.id'))) {
				TCG.data.getDataPromise('businessClientRelationship.json?id=' + f.getFormValue('businessClientRelationship.id'), this).then(function(clr) {
					f.copyFromClientRelationshipToClientImpl(clr, names, contactInfo, address);
				});
			}
			else {
				f.copyFromClientRelationshipToClientImpl(undefined, names, contactInfo, address);
			}
		},
		copyFromClientRelationshipToClientImpl: function(clientRelationship, names, contactInfo, address) {
			const f = this;
			if (TCG.isTrue(names)) {
				f.setFormValue('businessClient.name', clientRelationship ? clientRelationship.name : f.getFormValue('businessClientRelationship.name'));
				f.setFormValue('businessClient.legalName', clientRelationship ? clientRelationship.legalName : f.getFormValue('businessClientRelationship.legalName'));
			}
			if (TCG.isTrue(contactInfo)) {
				f.setFormValue('businessClient.company.phoneNumber', clientRelationship ? clientRelationship.company.phoneNumber : f.getFormValue('businessClientRelationship.company.phoneNumber'));
				f.setFormValue('businessClient.company.faxNumber', clientRelationship ? clientRelationship.company.faxNumber : f.getFormValue('businessClientRelationship.company.faxNumber'));
				f.setFormValue('businessClient.company.efaxNumber', clientRelationship ? clientRelationship.company.efaxNumber : f.getFormValue('businessClientRelationship.company.efaxNumber'));
				f.setFormValue('businessClient.company.emailAddress', clientRelationship ? clientRelationship.company.emailAddress : f.getFormValue('businessClientRelationship.company.emailAddress'));
			}
			if (TCG.isTrue(address)) {
				f.setFormValue('businessClient.company.addressLine1', clientRelationship ? clientRelationship.company.addressLine1 : f.getFormValue('businessClientRelationship.company.addressLine1'));
				f.setFormValue('businessClient.company.addressLine2', clientRelationship ? clientRelationship.company.addressLine2 : f.getFormValue('businessClientRelationship.company.addressLine2'));
				f.setFormValue('businessClient.company.addressLine3', clientRelationship ? clientRelationship.company.addressLine3 : f.getFormValue('businessClientRelationship.company.addressLine3'));
				f.setFormValue('businessClient.company.city', clientRelationship ? clientRelationship.company.city : f.getFormValue('businessClientRelationship.company.city'));
				f.setFormValue('businessClient.company.state', clientRelationship ? clientRelationship.company.state : f.getFormValue('businessClientRelationship.company.state'));
				f.setFormValue('businessClient.company.postalCode', clientRelationship ? clientRelationship.company.postalCode : f.getFormValue('businessClientRelationship.company.postalCode'));
				f.setFormValue('businessClient.company.country', clientRelationship ? clientRelationship.company.country : f.getFormValue('businessClientRelationship.company.country'));
			}
		},

		copyFromClientToClientAccount: function(names) {
			const f = this;
			if (TCG.isNotBlank(f.getFormValue('businessClient.id'))) {
				TCG.data.getDataPromise('businessClient.json?id=' + f.getFormValue('businessClient.id'), this).then(function(cl) {
					f.copyFromClientToClientAccountImpl(cl, names);
				});
			}
			else {
				f.copyFromClientToClientAccountImpl(undefined, names);
			}
		},

		copyFromClientToClientAccountImpl: function(client, names) {
			const f = this;
			if (TCG.isTrue(names)) {
				f.setFormValue('clientInvestmentAccount.shortName', client ? client.name : f.getFormValue('businessClient.name'));
				f.setFormValue('clientInvestmentAccount.name', client ? client.name : f.getFormValue('businessClient.legalName'));
			}
		},

		copyFromClientAccountToHoldingAccount: function(names) {
			const f = this;
			if (TCG.isTrue(names)) {
				f.setFormValue('holdingInvestmentAccount.shortName', f.getFormValue('clientInvestmentAccount.shortName'));
				f.setFormValue('holdingInvestmentAccount.name', f.getFormValue('clientInvestmentAccount.name'));
			}
		},

		toggleHoldingAccountNumbers: function(holdingAccountType) {
			let label = holdingAccountType.accountNumberLabel;
			this.setFieldLabel('holdingInvestmentAccount.number', (label || 'Account Number') + ':');

			label = holdingAccountType.accountNumber2Label;
			const o = this.getForm().findField('holdingInvestmentAccount.number2');
			if (TCG.isNull(label)) {
				o.hide();
			}
			else {
				o.setFieldLabel(label + ':');
				o.show();
			}
		}
	}]
});
