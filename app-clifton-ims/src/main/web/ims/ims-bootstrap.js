// use this file to configure cross project dependencies: add an extra tab from non-dependent project, etc.

// register name spaces for projects that do not have their own '*-shared.js' file
Ext.ns('Clifton.ims.migration');
Ext.ns('Clifton.dw.analytics');
Ext.ns('Clifton.ims.investment');
Ext.ns('Clifton.ims.investment.account');
Ext.ns('Clifton.ims.investment.account.wizard');
Ext.ns('Clifton.ims.investment.account.termination');


// Business Service window additional tabs from other projects
Clifton.business.service.ServiceWindowAdditionalTabs.push(
	{
		title: 'Tasks',
		items: [{
			xtype: 'workflow-task-grid',
			tableName: 'BusinessService'
		}]
	}
);

Clifton.investment.instrument.InstrumentSecurityTradingGrid = Ext.extend(Clifton.investment.instrument.InstrumentSecurityGrid, {
	plugins: [
		{ptype: 'investment-contextmenu-plugin', recordSecurityIdPath: 'id', recordInstrumentIdPath: 'instrument.id', filterForPositions: false, openInstrument: false, openClientAccount: false, openHoldingAccount: false},
		{ptype: 'trade-contextmenu-plugin', tradeToClose: false, tradeSecurityWithAccounts: false, recordSecurityIdPath: 'id'}
	]
});
Ext.reg('investment-instrumentSecurityTradingGrid', Clifton.investment.instrument.InstrumentSecurityTradingGrid);

Clifton.accounting.LiveHoldingsTradingGrid = Ext.extend(Clifton.accounting.LiveHoldingsGrid, {
	plugins: [
		{ptype: 'gridsummary'},
		{ptype: 'accounting-position-balance-contextmenu-plugin'},
		{ptype: 'investment-contextmenu-plugin'},
		{ptype: 'trade-contextmenu-plugin', filterTradable: true}
	]
});
Ext.reg('accounting-liveHoldingsTradingGrid', Clifton.accounting.LiveHoldingsTradingGrid);

Clifton.accounting.SecurityLiveHoldingsTradingGrid = Ext.extend(Clifton.accounting.SecurityLiveHoldingsGrid, {
	plugins: [
		{ptype: 'gridsummary'},
		{ptype: 'accounting-position-balance-contextmenu-plugin'},
		{ptype: 'investment-contextmenu-plugin'},
		{ptype: 'trade-contextmenu-plugin', filterTradable: true}
	]
});
Ext.reg('accounting-securityLiveHoldingsTradingGrid', Clifton.accounting.SecurityLiveHoldingsTradingGrid);

Clifton.investment.instrument.InstrumentStandardTabOverrides.push(
	{
		title: 'Securities',
		items: [{
			xtype: 'investment-instrumentSecurityTradingGrid'
		}]
	},
	{
		title: 'Open Positions',
		items: [{
			xtype: 'accounting-securityLiveHoldingsTradingGrid',
			instructions: 'Live position balances for selected instrument on the specified date.',
			groupField: 'investmentSecurity.symbol',
			hiddenColumns: ['holdingInvestmentAccount.label', 'price', 'exchangeRateToBase', 'localCostBasis', 'baseNotional', 'baseMarketValue', 'holdingInvestmentAccount.issuingCompany.label', 'accountingAccount.name'],
			getCustomFilters: function() {
				return {positionAccountingAccount: true, investmentInstrumentId: this.getWindow().getMainFormId()};
			}
		}]
	}
);

/**
 * Update all security window additional tabs for holdings to include trading plugin version.
 */
Object.keys(Clifton.investment.instrument.SecurityAdditionalTabsByType).forEach(key => {
	const tabs = Clifton.investment.instrument.SecurityAdditionalTabsByType[key];
	if (Array.isArray(tabs)) {
		tabs.forEach(tab => {
			if (tab.title === 'Open Positions' || tab.title === 'Holdings') {
				if (tab.items[0].xtype === 'accounting-securityLiveHoldingsGrid') {
					tab.items[0].xtype = 'accounting-securityLiveHoldingsTradingGrid';
				}
			}
		});
	}
});

/**
 * Insert and append Swap window tabs here to resolve dependency issues.
 */
Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'].splice(0, 0,
	{
		title: 'Commissions and Fees',
		items: [{xtype: 'swap-commissions-grid'}]
	},
	{
		title: 'Symbol Overrides',
		items: [{xtype: 'marketdata-datasource-security-override-grid'}]
	}
);
Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'],
	{
		title: 'Notes',
		items: [{
			xtype: 'investment-security-system-note-grid'
		}]
	},
	{
		title: 'Instrument Groups',
		layout: 'border',
		items: [
			{
				xtype: 'investment-instrumentGroupGrid_ByInstrument',
				region: 'north',
				height: 250,
				securityWindow: true
			},
			{
				xtype: 'investment-instrumentGroupMissingGrid_ByInstrument',
				region: 'center',
				securityWindow: true
			}
		]
	}
);
/**
 * Some swap subtypes use an overridden grid for open positions, add support for them here
 */
Clifton.accounting.SwapLiveHoldingsTradingGrid = Ext.extend(Clifton.accounting.SecurityLiveHoldingsTradingGrid, {
	initComponent: function() {
		TCG.callParent(this, 'initComponent', arguments);
		const prototype = Object.getPrototypeOf(this);
		const viewNames = prototype.viewNames;
		if (!viewNames.find(view => view?.text === 'Accrual Unsettled Payments')) {
			prototype.viewNames = viewNames.concat({
				text: 'Accrual Unsettled Payments',
				tooltip: 'Includes unsettled leg payment columns.  This view depends on the view option \'Include Unsettled Payments\' and the instrument hierarchy option \'Include pending leg payments with accrual amount\'.',
				url: 'accountingPositionValueListFind'
			});
		}
	},
	swapAccrualColumns: [
		{header: 'Base Equity Accrual', dataIndex: 'baseEquityAccrual', type: 'currency', negativeInRed: true, width: 65, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},
		{header: 'Local Equity Accrual', dataIndex: 'localEquityAccrual', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},

		{header: 'Base Equity Receivable Accrual', dataIndex: 'baseEquityReceivableAccrual', type: 'currency', negativeInRed: true, width: 65, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},
		{header: 'Local Equity Receivable Accrual', dataIndex: 'localEquityReceivableAccrual', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},

		{header: 'Base Receivable Accrual', dataIndex: 'baseReceivableAccrual', type: 'currency', negativeInRed: true, width: 65, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},
		{header: 'Base Receivable Accrual 1', dataIndex: 'baseReceivableAccrual1', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},
		{header: 'Base Receivable Accrual 2', dataIndex: 'baseReceivableAccrual2', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},

		{header: 'Local Receivable Accrual', dataIndex: 'localReceivableAccrual', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},
		{header: 'Local Receivable Accrual 1', dataIndex: 'localReceivableAccrual1', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']},
		{header: 'Local Receivable Accrual 2', dataIndex: 'localReceivableAccrual2', type: 'currency', negativeInRed: true, width: 65, hidden: true, nonPersistentField: true, viewNames: ['Accrual Unsettled Payments']}
	],
	configureAdditionalViews: function(menu, gridPanel) {
		TCG.callParent(this, 'configureAdditionalViews', arguments);
		const accrualDetail = menu.find('text', 'Accrual Unsettled Payments');
		if (accrualDetail && accrualDetail.length === 1) {
			const unsettled = menu.find('text', 'Include Unsettled Payments');
			if (unsettled && unsettled.length === 1) {
				accrualDetail[0].on('checkchange', (item, checked) => {
					gridPanel.unsettledPaymentRequest = checked;
					unsettled[0].setChecked(checked);
				}, this);
			}
		}
	},
	beforeSwitchToView: function(viewName) {
		if (viewName === 'Accrual Unsettled Payments') {
			this.constructor.prototype.resetStoreRoot.apply(this, ['data']);
		}
		else {
			TCG.callParent(this, 'beforeSwitchToView', [viewName]);
		}
	},
	getColumnOverrides: function() {
		const columns = TCG.callParent(this, 'getColumnOverrides', arguments);
		return columns.concat(this.swapAccrualColumns);
	}
});
Ext.reg('accounting-swapLiveHoldingsTradingGrid', Clifton.accounting.SwapLiveHoldingsTradingGrid);
['Inflation Swaps', 'Interest Rate Swaps', 'Zero Coupon Swaps', 'Total Return Swaps'].forEach(subType => {
	const swapTabs = Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'];
	if (Array.isArray(swapTabs)) {
		if (!Array.isArray(Clifton.investment.instrument.SecurityAdditionalTabsByType[subType])) {
			Clifton.investment.instrument.SecurityAdditionalTabsByType[subType] = [...swapTabs];
		}
		const positionsTabIndex = Clifton.investment.instrument.SecurityAdditionalTabsByType[subType].findIndex(element => element.title === 'Open Positions');
		if (positionsTabIndex >= 0) {
			// Replace Open Positions tab with one that includes accrual payments view
			Clifton.investment.instrument.SecurityAdditionalTabsByType[subType].splice(positionsTabIndex, 1, {title: 'Open Positions', items: [{xtype: 'accounting-swapLiveHoldingsTradingGrid'}]});
		}
	}
});

// Client Account window additional tabs from other projects
Clifton.investment.account.ClientAccountWindowAdditionalTabs.push(
	{
		title: 'Holdings',
		items: [{
			xtype: 'accounting-liveHoldingsTradingGrid',
			hiddenColumns: ['clientInvestmentAccount.label'],
			skipAccountFiltersAll: true,
			includeInvestmentGroupFilter: true,
			showRemovalButton: true,
			getRemovalDefaultData: function() {
				return {
					clientInvestmentAccountId: this.getWindow().getMainFormId(),
					clientInvestmentAccountLabel: TCG.getValue('label', this.getWindow().getMainForm().formValues)
				};
			},

			getCustomFilters: function() {
				return {clientInvestmentAccountId: this.getWindow().getMainFormId()};
			},

			getCashActivityDefaultData: function() {
				return {clientInvestmentAccount: {id: this.getWindow().getMainFormId()}};
			}
		}]
	},


	{
		title: 'Transactions',
		items: [{
			xtype: 'accounting-transactionGrid',
			instructions: 'The following GL transaction have been posted to the General Ledger for selected Client Account.',
			includeClientAccountFilter: false,
			columnOverrides: [
				{dataIndex: 'parentTransaction.id', hidden: true},
				{dataIndex: 'clientInvestmentAccount.label', hidden: true}
			],
			getCustomFilters: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('transactionDate', {'after': new Date().add(Date.YEAR, -1)});
				}
				return {
					clientInvestmentAccountId: this.getWindow().getMainFormId()
				};
			}
		}]
	},


	{
		title: 'Periods',
		items: [{
			name: 'accountingPeriodClosingListFind',
			xtype: 'gridpanel',
			instructions: 'The following accounting periods have been closed or re-open for this client account.',
			importTableName: 'AccountingPeriodClosing',
			columns: [
				{header: 'Start Date', width: 55, dataIndex: 'accountingPeriod.startDate', filter: {searchFieldName: 'accountingPeriodStartDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'},
				{header: 'End Date', width: 55, dataIndex: 'accountingPeriod.endDate', filter: {searchFieldName: 'accountingPeriodEndDate'}},
				{header: 'Notes', width: 250, dataIndex: 'combinedNote', sortable: false},
				{header: 'Accounting Note', width: 250, dataIndex: 'note', hidden: true},
				{header: 'AUM/Revenue Note', width: 250, dataIndex: 'externalNote', hidden: true},
				{header: 'Preliminary AUM', width: 70, dataIndex: 'preliminaryPeriodEndAUM', type: 'currency', useNull: true, tooltip: 'Preliminary Estimate of Assets Under Management at the end of the period'},
				{header: 'AUM', width: 70, dataIndex: 'periodEndAUM', type: 'currency', useNull: true, tooltip: 'Assets Under Management at the end of the period'},
				{header: 'Projected Revenue (Gross)', width: 75, dataIndex: 'periodProjectedRevenue', type: 'currency', useNull: true, tooltip: 'Projected Revenue for this account for the accounting period (excluding annual minimum)', hidden: true},
				{header: 'Projected Revenue (Annual Min)', width: 75, dataIndex: 'periodProjectedRevenueAnnualMin', type: 'currency', useNull: true, tooltip: 'Annual Minimum portion of the projected revenue for the account and period.', hidden: true},

				{
					header: 'Projected Revenue Total (Gross)', width: 80, dataIndex: 'periodProjectedRevenueTotal', type: 'currency', useNull: true, tooltip: 'Total Projected Revenue for the Account and Accounting Period (Including Annual Minimum portion)',
					renderer: function(v, metaData, r) {
						const annualMin = r.data['periodProjectedRevenueAnnualMin'];
						if (TCG.isNotNull(annualMin) && annualMin !== 0) {
							return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenue'], '0,000.00', 'Annual Min Applied', false);
						}
						return TCG.renderAmount(v, false, '0,000.00');
					}
				},
				{header: 'Projected Broker Fee', width: 75, dataIndex: 'periodProjectedRevenueShareExternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is paid to external party (Broker).', hidden: true},
				{
					header: 'Projected Revenue (Net)', width: 70, dataIndex: 'periodProjectedRevenueNetTotal', type: 'currency', useNull: true, tooltip: 'Gross Projected Revenue less Broker Fees for this account for the accounting period',
					renderer: function(v, metaData, r) {
						const brokerFee = r.data['periodProjectedRevenueShareExternal'];
						if (TCG.isNotNull(brokerFee) && brokerFee !== 0) {
							return TCG.renderAdjustedAmount(v, true, r.data['periodProjectedRevenueTotal'], '0,000.00', 'Broker Fee', false);
						}
						return TCG.renderAmount(v, false, '0,000.00');
					}
				},
				{header: 'Projected Revenue Share', width: 70, dataIndex: 'periodProjectedRevenueShareInternal', type: 'currency', useNull: true, tooltip: 'Portion of the projected revenue for the account and period that is shared with an internal party (i.e. EV).'},
				{header: 'Closed', width: 35, dataIndex: 'closed', type: 'boolean'},
				{header: '<div class="www" style="WIDTH:16px;">&nbsp;</div>', width: 15, tooltip: 'If checked, there is at least one APPROVED file associated with this period on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean'}
			],
			editor: {
				detailPageClass: 'Clifton.accounting.period.PeriodClosingWindow',
				addEditButtons: function(toolBar, gridPanel) {
					toolBar.add({
						text: 'Close Next Period',
						tooltip: 'Close next open accounting period',
						iconCls: 'lock',
						scope: this,
						handler: function() {
							Ext.Msg.confirm('Close Accounting Period', 'Would you like to Close the next Accounting Period for selected Client Account?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Closing...',
										params: {
											investmentAccountId: gridPanel.getWindow().getMainFormId()
										},
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('accountingPeriodNextClose.json');
								}
							});
						}
					});
					toolBar.add('-');
					toolBar.add({
						text: 'Create Next Period',
						tooltip: 'Create next open accounting period: If none exist, will create period for previous month.  Otherwise will find the last one for the account and open the next one.',
						iconCls: 'add',
						scope: this,
						handler: function() {
							Ext.Msg.confirm('Create Accounting Period', 'Would you like to Create the next Accounting Period for selected Client Account?', function(a) {
								if (a === 'yes') {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										waitMsg: 'Creating...',
										params: {
											investmentAccountId: gridPanel.getWindow().getMainFormId()
										},
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('accountingPeriodNextCreate.json');
								}
							});
						}
					});
					toolBar.add('-');
					this.addToolbarDeleteButton(toolBar, gridPanel);
					toolBar.add({
						text: 'Accounting Statement',
						tooltip: 'Download PDF of Accounting Statement report for selected accounting period',
						iconCls: 'pdf',
						scope: this,
						handler: function() {
							const sm = this.grid.getSelectionModel();
							if (sm.getCount() === 0) {
								TCG.showError('Please select a Period for the Accounting Statement.', 'No Period Selected');
							}
							else {
								TCG.downloadFile('accountingPeriodClosingReportDownload.json?periodClosingId=' + sm.getSelected().id + '&format=PDF', null, this);
							}
						}
					});
					toolBar.add('-');
				}
			},
			getLoadParams: function() {
				return {investmentAccountId: this.getWindow().getMainFormId()};
			}
		}]
	},


	{
		title: 'Billing',
		layout: 'border',

		items: [
			{
				title: 'Billing Definition Accounts',
				region: 'north',
				height: 260,
				xtype: 'gridpanel',
				name: 'billingDefinitionInvestmentAccountListFind',

				columns: [
					{header: 'Def ID', width: 35, dataIndex: 'referenceOne.id', type: 'int', filter: {searchFieldName: 'billingDefinitionId'}, hidden: true},
					{header: 'Invoice Type', width: 85, dataIndex: 'referenceOne.invoiceType.name', filter: {type: 'combo', searchFieldName: 'invoiceTypeId', url: 'billingInvoiceTypeListFind.json'}},
					{
						header: 'Contract', width: 150, dataIndex: 'referenceOne.billingContract.name', filter: {searchFieldName: 'contractName'},
						renderer: function(v, metaData, r) {
							const note = r.data['referenceOne.note'];
							if (TCG.isNotBlank(note)) {
								const qtip = r.data['referenceOne.note'];
								metaData.css = 'amountAdjusted';
								metaData.attr = TCG.renderQtip(qtip);
							}
							return v;
						}
					},
					{header: 'Definition Note', width: 150, hidden: true, dataIndex: 'referenceOne.note', filter: false, sortable: false},
					{
						header: 'Valuation Type', width: 80, dataIndex: 'billingBasisValuationType.name', filter: {searchFieldName: 'billingBasisValuationTypeName', url: 'billingBasisValuationTypeListFind.json', loadAll: true},
						renderer: function(v, metaData, r) {
							const desc = r.data['billingBasisValuationTypeDescription'];
							if (TCG.isNotBlank(desc)) {
								const qtip = desc;
								metaData.css = 'amountAdjusted';
								metaData.attr = TCG.renderQtip(qtip);
							}
							return v;
						}
					},
					{header: 'Valuation Type Description', width: 100, hidden: true, dataIndex: 'billingBasisValuationTypeDescription', filter: {searchFieldName: 'billingBasisValuationTypeDescription'}},
					{header: 'Calculation Type', width: 75, dataIndex: 'billingBasisCalculationType.name', filter: {searchFieldName: 'billingBasisCalculationTypeName', url: 'billingBasisCalculationTypeListFind.json?inactive=false', loadAll: true}},
					{header: 'Instrument Group', width: 75, dataIndex: 'investmentGroup.name', filter: {type: 'combo', searchFieldName: 'investmentGroupId', url: 'investmentGroupListFind.json'}},
					{header: 'Manager Account', width: 75, dataIndex: 'investmentManagerAccount.label', filter: {type: 'combo', searchFieldName: 'investmentManagerAccountId', url: 'investmentManagerAccountListFind.json', displayField: 'label'}, hidden: true},
					{header: 'Schedule', width: 75, dataIndex: 'billingSchedule.name', filter: {searchFieldName: 'billingScheduleName'}, tooltip: 'If selected, the billing definition investment account applies ONLY to that specific schedule - otherwise to all schedules.', hidden: true},
					{header: 'Start Date', width: 35, dataIndex: 'startDate'},
					{header: 'End Date', width: 35, dataIndex: 'endDate'},
					{header: 'Active Account', width: 35, dataIndex: 'active', type: 'boolean', sortable: false},
					{header: 'Active Definition', width: 35, dataIndex: 'referenceOne.active', type: 'boolean', filter: {searchFieldName: 'activeDefinition'}, sortable: false}
				],
				editor: {
					detailPageClass: 'Clifton.billing.definition.DefinitionWindow',
					drillDownOnly: true,
					getDetailPageId: function(gridPanel, row) {
						return row.json.referenceOne.id;
					},
					// Used to Open the Allocation or Security/Instrument Window Directly
					openWindowFromContextMenu: function(grid, rowIndex, screen) {
						const gridPanel = this.getGridPanel();
						const row = grid.store.data.items[rowIndex];

						let clazz;
						let id;

						if (screen === 'BILLING_DEFINITION') {
							id = row.json.referenceOne.id;
							clazz = 'Clifton.billing.definition.DefinitionWindow';
						}
						else {
							id = row.json.id;
							clazz = 'Clifton.billing.definition.DefinitionAccountWindow';
						}
						if (clazz) {
							this.openDetailPage(clazz, gridPanel, id, row);
						}
					}
				},
				getLoadParams: function(firstLoad) {
					return {investmentAccountId: this.getWindow().getMainFormId()};
				},

				listeners: {
					afterrender: function(gridPanel) {
						const el = gridPanel.getEl();
						el.on('contextmenu', function(e, target) {
							const g = gridPanel.grid;
							g.contextRowIndex = g.view.findRowIndex(target);
							e.preventDefault();
							if (!g.drillDownMenu) {
								g.drillDownMenu = new Ext.menu.Menu({
									items: [
										{
											text: 'Open Billing Definition', iconCls: 'billing',
											handler: function() {
												gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'BILLING_DEFINITION');
											}
										},
										{
											text: 'Open Billing Definition Account', iconCls: 'account',
											handler: function() {
												gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'BILLING_DEFINITION_ACCOUNT');
											}
										}
									]
								});
							}
							TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
						}, gridPanel);
					}
				}
			},

			{
				title: 'Invoices / Payments',
				region: 'center',
				xtype: 'billing-payment-account-allocation-grid',
				instructions: 'The following displays each account billed on this invoice and their current payment status.',

				columnOverrides: [
					{dataIndex: 'billingInvoice.invoiceDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
					{dataIndex: 'investmentAccount.label', hidden: true}
				],

				getLoadParams: function(firstLoad) {
					if (firstLoad) {
						this.setFilterValue('billingInvoice.invoiceDate', {'after': new Date().add(Date.YEAR, -3)});
					}
					return {investmentAccountId: this.getWindow().getMainFormId()};
				},
				getTopToolbarFilters: function(toolbar) {
					return [];
				}

			}
		]
	},


	{
		title: 'Account Rules',
		items: [{
			xtype: 'rule-assignment-grid-forAdditionalScope',
			scopeTableName: 'InvestmentAccount',
			defaultCategoryName: 'Trade Rules',
			defaultViewName: 'Global and Overrides'
		}]
	},


	{
		title: 'Compliance',
		items: [{
			xtype: 'compliance-rule-form'
		}]
	},


	{
		title: 'Trade Restrictions',
		items: [{
			xtype: 'trade-restriction-grid',
			getDefaultClientAccount: function() {
				return {
					id: this.getWindow().getMainFormId(),
					label: TCG.getValue('label', this.getWindow().getMainForm().formValues)
				};
			}
		}]
	},


	{
		title: 'Broker Restrictions',
		items: [{
			xtype: 'trade-restriction-broker-grid',
			columnOverrides: [
				{dataIndex: 'clientAccount.label', hidden: true}
			],
			getLoadParams: function(firstLoad) {
				return {clientAccountId: this.getWindow().getMainFormId()};
			},
			editor: {
				detailPageClass: 'Clifton.trade.restriction.TradeRestrictionBrokerWindow',
				getDefaultData: function(gridPanel) {
					return {
						clientAccount: {id: gridPanel.getWindow().getMainFormId(), label: TCG.getValue('label', gridPanel.getWindow().getMainForm().formValues)}
					};
				}
			}
		}]
	},


	{
		title: 'Contacts',
		items: [{
			xtype: 'business-contact-assignments-grid',
			entityTableNameFilter: 'InvestmentAccount',
			columnOverrides: [
				{dataIndex: 'assignmentEntityLabel', hidden: true}
			]
		}]
	},


	{
		title: 'Notes',
		items: [{
			xtype: 'document-system-note-grid',
			tableName: 'InvestmentAccount',
			showOrderInfo: true
		}]
	},


	{
		title: 'Events',
		items: [{
			xtype: 'investment-calendar-event-grid-by-account'
		}]
	},


	{
		title: 'Tasks',
		items: [{
			xtype: 'workflow-task-grid',
			tableName: 'InvestmentAccount',
			addTaskCategoryFilter: true
		}]
	},


	{
		title: 'Data Fields',
		items: [{
			xtype: 'system-column-dated-value-grid',
			tableName: 'InvestmentAccount'
		}]
	},


	{
		title: 'Objectives',
		items: [{
			name: 'investmentAccountServiceObjectiveListFind',
			xtype: 'gridpanel',
			instructions: 'Service Objectives for this investment account. Used in account performance summaries.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Objective Name', width: 150, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'objectiveName'}},
				{
					header: 'Description', width: 300, dataIndex: 'objectiveDescription', filter: {searchFieldName: 'coalesceObjectiveDescription'},
					renderer: function(v, metaData, r) {
						if (TCG.isTrue(r.json.objectiveDescriptionOverridden)) {
							metaData.css = 'amountAdjusted';
						}
						return v;
					}
				},
				{header: 'Overridden', width: 50, dataIndex: 'objectiveDescriptionOverridden', type: 'boolean'}
			],
			getLoadParams: function() {
				return {
					investmentAccountId: this.getWindow().getMainFormId()
				};
			},
			editor: {
				detailPageClass: 'Clifton.investment.account.AccountServiceObjectiveWindow',
				deleteURL: 'investmentAccountServiceObjectiveDelete.json',
				getDefaultData: function(gridPanel) {
					return {
						referenceOne: gridPanel.getWindow().getMainForm().formValues
					};
				}
			}
		}]
	},


	{
		title: 'Account Groups',
		items: [{
			xtype: 'investment-account-group-grid',
			instructions: 'This Investment Account belongs to the following Investment Account Group(s).',
			configureAdditionalLoadParams: function(params) {
				params.investmentAccountId = this.getWindow().getMainFormId();
			},
			editor: {
				detailPageClass: 'Clifton.investment.account.AccountGroupWindow',
				drillDownOnly: true
			}
		}]
	},


	{
		title: 'Account Mappings',
		items: [{
			xtype: 'investment-account-mapping-grid',
			getLoadParams: function(firstLoad) {
				const params = {};

				const searchPatternControl = TCG.getChildByName(this.getTopToolbar(), 'searchPatternControl');
				if (TCG.isNotBlank(searchPatternControl.getValue())) {
					params[this.topToolbarSearchParameter] = searchPatternControl.getValue();
				}

				// get accountID from parent
				const win = this.getWindow();
				const accountId = win.getMainFormId();
				if (TCG.isNotBlank(accountId)) {
					params['investmentAccountId'] = accountId;
				}

				return params;
			}
		}]
	}
);

Clifton.investment.account.HoldingAccountWindowAdditionalTabs.push(
	{
		title: 'Holdings',
		items: [{
			xtype: 'accounting-liveHoldingsTradingGrid',
			instructions: 'Asset and Liability account balances (live from the General Ledger + valuation using corresponding market data) on the specified date for this Holding Account. Note, base amounts are converted Holding Account\'s Base Currency.',
			hiddenColumns: ['holdingInvestmentAccount.label'],
			skipAccountFiltersAll: true,
			includeInvestmentGroupFilter: true,
			showRemovalButton: true,
			getRemovalDefaultData: function() {
				return {
					holdingInvestmentAccountId: this.getWindow().getMainFormId(),
					holdingInvestmentAccountLabel: TCG.getValue('label', this.getWindow().getMainForm().formValues)
				};
			},

			getCustomFilters: function() {
				return {
					holdingInvestmentAccountId: this.getWindow().getMainFormId(),
					useHoldingAccountBaseCurrency: true
				};
			},

			getCashActivityDefaultData: function() {
				return {holdingInvestmentAccount: {id: this.getWindow().getMainFormId()}};
			}
		}]
	},


	{
		title: 'Transactions',
		items: [{
			xtype: 'accounting-transactionGrid',
			instructions: 'The following GL transaction have been posted to the General Ledger for selected Holding Account.',
			includeClientAccountFilter: false,
			columnOverrides: [
				{dataIndex: 'parentTransaction.id', hidden: true},
				{dataIndex: 'holdingInvestmentAccount.label', hidden: true}
			],
			getCustomFilters: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('transactionDate', {'after': new Date().add(Date.YEAR, -1)});
				}
				return {
					holdingInvestmentAccountId: this.getWindow().getMainFormId()
				};
			}
		}]
	}
);

// Security Event window additional tabs from other projects
Clifton.investment.instrument.event.AdditionalTabs[Clifton.investment.instrument.event.AdditionalTabs.length] = {
	title: 'Integration History',
	items: [{
		name: 'integrationCorporateActionListFind',
		xtype: 'gridpanel',
		instructions: 'The following corporate actions were imported and processed for the given corporate action identifier.',
		additionalPropertiesToRequest: 'importRun',
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true, doNotFormat: true},
			{header: 'Event Date', width: 30, dataIndex: 'eventDate', type: 'date'},
			{header: 'Election #', width: 25, dataIndex: 'electionNumber', type: 'int', useNull: true},
			{header: 'Payout #', width: 25, dataIndex: 'payoutNumber', type: 'int', useNull: true},
			{header: 'Event Type', width: 50, dataIndex: 'eventTypeName'},
			{header: 'Event Status', renderer: Clifton.investment.instrument.event.renderEventStatus, width: 50, dataIndex: 'eventStatusName'},
			{header: 'Declare Date', width: 30, dataIndex: 'declareDate', type: 'date', hidden: true},
			{header: 'Expiration Date', width: 30, dataIndex: 'exDate', type: 'date', hidden: true},
			{header: 'Record Date', width: 30, dataIndex: 'recordDate', type: 'date', hidden: true},
			{header: 'Additional Date', width: 30, dataIndex: 'additionalEventDate', type: 'date', hidden: true},
			{header: 'Additional Event Value', width: 50, dataIndex: 'additionalEventValue', type: 'float', useNull: true, hidden: true},
			{header: 'Event Description', width: 60, dataIndex: 'eventDescription'},
			{header: 'Payout Security', width: 30, dataIndex: 'payoutSecurityIdentifier'},
			{header: 'Payout Security Type Identifier', width: 25, dataIndex: 'payoutSecurityTypeIdentifier', type: 'int', useNull: true, hidden: true},
			{header: 'Before Value', width: 25, dataIndex: 'beforeEventValue', type: 'float', useNull: true},
			{header: 'After  Value', width: 25, dataIndex: 'afterEventValue', type: 'float', useNull: true},
			{header: 'Additional Payout Value', width: 50, dataIndex: 'additionalPayoutValue', type: 'float', useNull: true, hidden: true},
			{header: 'Payout Description', width: 60, dataIndex: 'payoutDescription'},
			{header: 'Is Voluntary', width: 25, dataIndex: 'voluntary', type: 'boolean', hidden: true},
			{header: 'Is Taxable', width: 25, dataIndex: 'taxable', type: 'boolean', hidden: true},
			{header: 'Is Deleted', width: 25, dataIndex: 'deleted', type: 'boolean', hidden: true},
			{header: 'Is Default Election', width: 25, dataIndex: 'defaultElection', type: 'boolean', hidden: true},
			{header: 'Payout Type Identifier', width: 25, dataIndex: 'payoutTypeIdentifier', useNull: true, type: 'int', hidden: true}
		],
		getLoadParams: function() {
			return {
				'corporateActionIdentifier': TCG.coalesce(this.getWindow().getMainFormPanel().getFormValue('corporateActionIdentifier'), 0)
			};
		},
		editor: {
			detailPageClass: 'Clifton.integration.definition.ImportRunWindow',
			getDetailPageId: function(gridPanel, row) {
				return row.json.importRun.id;
			},
			drillDownOnly: true
		}
	}]
};


// Add System Navigation Screen List tab to Search Window
Clifton.system.search.SearchSetupWindowAdditionalTabs.push(
	{
		title: 'Navigation Screens',
		items: [{
			xtype: 'system-navigation-screen-grid'
		}]
	}
);


// Used for Clients, Client Relationships to show the list of files that were posted that are associated with the given client or relationship, etc.
Clifton.business.PostedPortalFileGrid = Ext.extend(Clifton.portal.file.BasePortalFileExtendedGrid, {

	sourceTableName: 'OVERRIDE_ME', // i.e. BusinessClient, BusinessClientRelationship
	getSourceFkFieldId: function() {
		return this.getWindow().getMainFormId();
	},

	groupField: 'fileCategoryWithAssignmentLabel',
	groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Files" : "File"]})',

	getCategoryName: function() {
		return null;
	},

	getApprovedFilesOnly: function() {
		return false;
	},

	getTopToolbarFilters: function(toolbar) {
		return [
			{fieldLabel: 'File Category', xtype: 'toolbar-combo', url: 'portalFileCategoryListFind.json', displayField: 'nameExpanded', linkedFilter: 'fileCategory.name'},
			{fieldLabel: 'Report Date from', xtype: 'toolbar-datefield', name: 'reportDateStart', allowBlank: true, qtip: 'If no report date filters are present, displays files that are active today.'},
			{fieldLabel: 'to', labelSeparator: '', xtype: 'toolbar-datefield', name: 'reportDateEnd', allowBlank: true}
		];
	},

	getAdditionalLoadParams: function(firstLoad, params) {
		const gp = this;
		const returnParams = params || {};
		return TCG.data.getDataPromiseUsingCaching('portalEntityBySourceTable.json?sourceSystemName=IMS&sourceTableName=' + this.sourceTableName + '&sourceFkFieldId=' + this.getSourceFkFieldId() + '&populateFieldValues=false', this, 'portal.entity.' + this.sourceTableName + '-' + this.getSourceFkFieldId())
			.then(function(portalEntity) {
				if (portalEntity) {
					returnParams.assignedPortalEntityId = portalEntity.id;
					return returnParams;
				}
				TCG.showError('Missing Portal Entity for selected ' + gp.sourceTableName + '.');
				return false;
			});
	},

	viewConfig: {
		startCollapsed: true
	},


	columns: [
		{header: 'UUID', dataIndex: 'uuid', width: 30, hidden: true, skipExport: true},
		{header: 'PortalFileID', width: 12, dataIndex: 'portalFileId', hidden: true},
		{header: 'Category Sub-Set', width: 100, dataIndex: 'fileCategory.parent.name', hidden: true},
		{header: 'File Group', width: 100, dataIndex: 'fileCategory.name', hidden: true, filter: {searchFieldName: 'fileCategoryIdExpanded', type: 'combo', url: 'portalFileCategoryListFind.json', displayField: 'nameExpanded'}},
		{
			header: '', menuHeader: 'Format', width: 30, fixed: true, dataIndex: 'documentFileType', filter: false, align: 'center', tooltip: 'File Format',
			renderer: function(ft, args, r) {
				if (!TCG.isBlank(ft)) {
					return TCG.renderActionColumn(TCG.getDocumentFileTypeIconClass(ft), '', 'View document (' + ft + ')', 'ViewFile');
				}
			}
		},
		{
			header: '', menuHeader: 'View Client Portal', width: 30, fixed: true, sortable: false, filter: false, tooltip: 'View Client Portal as User with Access to this file or an Entity this File is posted under',
			renderer: function(v, metaData, r) {
				if (TCG.isTrue(r.data.approved)) {
					return TCG.renderActionColumn('www', '', 'View This File in Client Portal', 'ViewClientUI');
				}
				metaData.attr = TCG.renderQtip('This file is NOT approved and therefore is not visible in the Client Portal');
				return '&nbsp;';
			}
		},
		{
			header: 'Group Label', width: 100, dataIndex: 'fileCategoryWithAssignmentLabel', hidden: true, skipExport: true,
			groupRenderer: function(v, unused, r, rowIndex, colIndex, ds) {
				let value = '';
				if (!TCG.isBlank(r.data['assignedPortalEntity.label'])) {
					value = r.data['assignedPortalEntity.label'] + ': ';
				}
				return '<span style="font-weight: normal;" >' + value + '</span>' + r.data['fileCategory.name'];
			}
		},
		{header: 'Display Name', width: 100, dataIndex: 'displayName'},
		{header: 'Display Text', width: 100, dataIndex: 'displayText', hidden: true},
		{header: 'Assigned Entity', width: 100, hidden: true, dataIndex: 'assignedPortalEntity.label'},
		{header: 'Post Entity', width: 100, hidden: true, dataIndex: 'postPortalEntity.label'},
		{header: 'Publish Date', width: 45, dataIndex: 'publishDate', filter: false},
		{header: 'Fk Field Id', width: 45, dataIndex: 'sourceFkFieldId', hidden: true},
		{header: 'Table Name', width: 45, dataIndex: 'entitySourceSection.sourceSystemTableName', hidden: true},
		{header: 'Report Date', width: 45, dataIndex: 'reportDate'},
		{header: 'Approved', width: 35, dataIndex: 'approved', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
		{
			header: 'Approved By', width: 30, dataIndex: 'approvedByUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'approvedByUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		},
		{header: 'Approved On', width: 45, dataIndex: 'approvalDate', hidden: true},
		{header: 'File Name', width: 100, dataIndex: 'fileName', hidden: true, filter: {searchFieldName: 'fileNameAndPath'}},
		{header: 'File Name and Path', width: 300, dataIndex: 'fileNameAndPath', hidden: true},
		{
			header: 'File Created By', width: 40, dataIndex: 'coalesceFileCreateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'coalesceFileCreateUserId', url: 'portalSecurityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.portal.security.renderPortalSecurityUser,
			exportColumnValueConverter: 'portalSecurityUserColumnReversableConverter'
		}
	]

});
Ext.reg('business-posted-portal-file-grid', Clifton.business.PostedPortalFileGrid);

Ext.override(Clifton.accounting.gl.PositionBalanceGrid, {
	plugins: [
		{ptype: 'gridsummary'},
		{ptype: 'accounting-position-balance-contextmenu-plugin'},
		{ptype: 'investment-contextmenu-plugin'},
		{ptype: 'trade-contextmenu-plugin'}
	]
});
