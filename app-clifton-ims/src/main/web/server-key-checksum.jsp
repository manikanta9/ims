<%@ page import="com.clifton.core.util.StringUtils" %>
<%@ page import="com.clifton.core.util.encryption.EncryptionUtils" %>
<%@ page import="com.clifton.core.util.encryption.EncryptionUtilsAES" %>
<%@ page import="com.clifton.security.secret.SecuritySecretServiceImpl" %>
<%@ page import="java.io.File" %>
<%
	final org.springframework.web.context.WebApplicationContext applicationContext = org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());

	String keyFileLocation = request.getParameter("keyFileLocation");
	if (StringUtils.isEmpty(keyFileLocation)) {
		final SecuritySecretServiceImpl securitySecretService = (SecuritySecretServiceImpl) applicationContext.getBean("securitySecretService");
		keyFileLocation = securitySecretService.getServerKeyLocation();
	}

	out.println(EncryptionUtils.byteArrayToHexString(EncryptionUtilsAES.hashBytes(EncryptionUtils.readKeyFileToBytes(new File(keyFileLocation)))));
%>
