<%@ page import="com.clifton.business.company.BusinessCompany" %>
<%@ page import="com.clifton.business.company.BusinessCompanyService" %>
<%@ page import="com.clifton.business.company.BusinessCompanyType" %>
<%@ page import="com.clifton.core.logging.LogUtils" %>
<%@ page import="com.clifton.core.util.ExceptionUtils" %>
<%@ page import="static org.apache.logging.log4j.web.WebLoggerContextUtils.getServletContext" %>
<%@ page import="com.clifton.core.util.StringUtils" %>
<%@ page import="com.clifton.core.util.date.DateUtils" %>
<%@ page import="com.clifton.ims.business.company.merge.BusinessCompanyMergeHandler" %>
<%@ page import="com.clifton.security.impersonation.SecurityImpersonationHandler" %>
<%@ page import="com.clifton.security.system.SecuritySystemService" %>
<%@ page import="com.clifton.security.system.SecuritySystemUser" %>
<%@ page import="com.clifton.security.user.SecurityUser" %>
<%@ page import="com.clifton.system.schema.column.SystemColumn" %>
<%@ page import="com.clifton.system.usedby.SystemUsedByEntity" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ListIterator" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.Objects" %>

<%-- Controller --%>
<%!
	WebApplicationContext applicationContext = null;


	/**
	 * get the map of business company usage data keyed by type.
	 */
	Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> getSystemUsedByEntityListMap(String bloombergCompanyIdentifier, String businessCompanyTypeIdentifier) {
		Integer bloombergCompanyID = parseInteger(bloombergCompanyIdentifier);
		Short businessCompanyTypeId = parseShort(businessCompanyTypeIdentifier);
		if (bloombergCompanyID != null && businessCompanyTypeId != null) {
			BusinessCompanyMergeHandler businessCompanyMergeHandler = (BusinessCompanyMergeHandler) this.applicationContext.getBean("businessCompanyMergeHandler");
			return businessCompanyMergeHandler.getSystemUsedByEntityListMap(bloombergCompanyID, businessCompanyTypeId);
		}
		return Collections.emptyMap();
	}


	/**
	 * List of all business company types for use by the combo box.
	 */
	List<BusinessCompanyType> getBusinessCompanyTypeLabelList() {
		BusinessCompanyMergeHandler businessCompanyMergeHandler = (BusinessCompanyMergeHandler) this.applicationContext.getBean("businessCompanyMergeHandler");
		return businessCompanyMergeHandler.getWhiteListedBusinessCompanyTypeList();
	}


	/**
	 * Merge company data.
	 */
	void mergeCompanyData(String sourceIdentifier, String targetIdentifier, boolean deleteBusinessCompanyAfterMerge, PageContext pageContext) {
		try {
			BusinessCompanyService businessCompanyService = (BusinessCompanyService) this.applicationContext.getBean("businessCompanyService");
			BusinessCompanyMergeHandler businessCompanyMergeHandler = (BusinessCompanyMergeHandler) this.applicationContext.getBean("businessCompanyMergeHandler");
			Integer sourceId = parseInteger(sourceIdentifier);
			Integer targetId = parseInteger(targetIdentifier);
			if (sourceId != null && targetId != null) {
				BusinessCompany sourceBusinessCompany = businessCompanyService.getBusinessCompany(sourceId);
				BusinessCompany targetBusinessCompany = businessCompanyService.getBusinessCompany(targetId);
				businessCompanyMergeHandler.mergeBusinessCompanies(sourceBusinessCompany, targetBusinessCompany, deleteBusinessCompanyAfterMerge);
				pageContext.setAttribute("status-message", String.format("Successfully Merged: %s - %s To: %s - %s"
						, sourceBusinessCompany.getId(), sourceBusinessCompany.getName(), targetBusinessCompany.getId(), targetBusinessCompany.getName()));
			}
		}
		catch (Exception e) {
			pageContext.setAttribute("error-message", ExceptionUtils.getDetailedMessage(e), PageContext.PAGE_SCOPE);
		}
	}


	/**
	 * Merge all company data.
	 */
	void mergeAllCompanyData(String bloombergCompanyIdentifier, String businessCompanyTypeIdentifier, String targetIdentifier, boolean deleteBusinessCompanyAfterMerge, PageContext pageContext) {
		try {
			BusinessCompanyService businessCompanyService = (BusinessCompanyService) this.applicationContext.getBean("businessCompanyService");
			BusinessCompanyMergeHandler businessCompanyMergeHandler = (BusinessCompanyMergeHandler) this.applicationContext.getBean("businessCompanyMergeHandler");
			Integer targetId = parseInteger(targetIdentifier);
			if (targetId != null) {
				BusinessCompany targetBusinessCompany = businessCompanyService.getBusinessCompany(targetId);
				Integer bloombergCompanyID = parseInteger(bloombergCompanyIdentifier);
				Short businessCompanyTypeId = parseShort(businessCompanyTypeIdentifier);
				if (targetBusinessCompany != null && bloombergCompanyID != null && businessCompanyTypeId != null) {
					businessCompanyMergeHandler.mergeAllBusinessCompanies(bloombergCompanyID, businessCompanyTypeId, targetBusinessCompany, deleteBusinessCompanyAfterMerge);
					pageContext.setAttribute("status-message", String.format("Successfully Merged: [ALL] To: %s - %s", targetBusinessCompany.getId(), targetBusinessCompany.getName()));
				}
			}
		}
		catch (Exception e) {
			pageContext.setAttribute("error-message", ExceptionUtils.getDetailedMessage(e), PageContext.PAGE_SCOPE);
			LogUtils.error(getClass(), "Error occurred while merging company data.", e);
		}
	}


	/**
	 * get the company entry count
	 */
	int getEntryCount(Map<SystemColumn, List<SystemUsedByEntity>> typeSystemUsedByMap) {
		int count = 0;
		for (List<SystemUsedByEntity> entries : typeSystemUsedByMap.values()) {
			count += entries.size();
		}
		return count;
	}


	/**
	 * just convert to integer without exceptions.
	 */
	Integer parseInteger(String value) {
		try {
			return Integer.valueOf(value);
		}
		catch (NumberFormatException e) {
			// suppress.
		}
		return null;
	}


	/**
	 * just convert to short without exceptions
	 */
	Short parseShort(String value) {
		try {
			return Short.valueOf(value);
		}
		catch (NumberFormatException e) {
			// suppress.
		}
		return null;
	}


	/**
	 * get the display label for a system column.
	 */
	public String getLabel(SystemColumn systemColumn) {
		String lbl = "";
		if (systemColumn.getTable() != null) {
			lbl = systemColumn.getTable().getLabel() + ".";
		}
		lbl += systemColumn.getLabel();
		return lbl;
	}


	/**
	 * get the display label for a business company.
	 */
	public String getLabel(BusinessCompany businessCompany) {
		String lbl = "";
		if (businessCompany != null) {
			lbl = businessCompany.getId() + " - " + businessCompany.getName();
		}
		return lbl;
	}


	/**
	 * Get the used by name for the ID.
	 */
	public String getUsedByName(Short id) {
		if (id != null) {
			SecuritySystemService securitySystemService = (SecuritySystemService) this.applicationContext.getBean("securitySystemService");
			SecuritySystemUser securitySystemUser = securitySystemService.getSecuritySystemUser(id);
			if (securitySystemUser != null) {
				return securitySystemUser.getUserName();
			}
		}
		return "";
	}


	/**
	 *  Convert to String if possible, otherwise, return empty string.
	 */
	public static String toString(Object obj) {
		if (obj == null) {
			return "";
		}

		return obj.toString();
	}


	/**
	 *  Total count of system used by labels.
	 */
	public Map<String, Integer> getUsageCounts(Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> companySystemUsedByEntityMap) {
		Map<String, Integer> usageSummary = new HashMap<String, Integer>();
		for (Map.Entry<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> businessCompanyMapEntry : companySystemUsedByEntityMap.entrySet()) {
			Map<SystemColumn, List<SystemUsedByEntity>> typeSystemUsedByMap = businessCompanyMapEntry.getValue();
			for (List<SystemUsedByEntity> systemUsedByEntityList : typeSystemUsedByMap.values()) {
				for (SystemUsedByEntity systemUsedByEntity : systemUsedByEntityList) {
					String systemUsedByLabel = systemUsedByEntity.getLabel();
					if (!usageSummary.containsKey(systemUsedByLabel)) {
						usageSummary.put(systemUsedByLabel, 0);
					}
					Integer count = usageSummary.get(systemUsedByLabel) + 1;
					usageSummary.put(systemUsedByLabel, count);
				}
			}
		}
		return usageSummary;
	}
%>
<%-- Initialize Context for controller --%>
<%
	this.applicationContext = org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
%>
<html>
<head>
	<style>
		.grid {
			display: grid;
			grid-template-columns: [a] auto [b] auto [c] auto [d] auto [e] auto [f] auto [g] auto [h] auto [i] auto;
			grid-template-rows: auto;
			grid-column-gap: 0;
			grid-auto-flow: row;
			justify-content: stretch;
			align-content: stretch;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
		}

		.title {
			font-weight: bold;
			font-size: 11px;
		}

		.cell-font {
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
		}

		.cell-header {
			background-color: #BFBFBF;
			font-style: italic;
			text-align: center;
			border: 1px solid gray;
		}

		.cell-text {
			text-align: center;
			border: 1px solid gray;
		}

		.cell-detail {
			text-align: center;
		}

		.cell-numeric {
			text-align: right;
			border: 1px solid gray;
		}

		.button {
			background-color: #BFBFBF;
			color: white;
			font-family: Arial, Helvetica, sans-serif;
			font-size: 10px;
		}

		.action-button {
			display: flex;
			align-items: center;
			padding-left: 3px;
		}

		.border-top {
			border-top: 1px solid gray;
		}

		.border-sides {
			border-right: 1px solid gray;
			border-left: 1px solid gray;
		}

		.status-text {
			font-size: 12px;
			font-weight: bold;
			color: blue;
		}

		.error-text {
			font-size: 12px;
			font-weight: bold;
			color: red;
		}

		.entire-row {
			background-color: #808080;
		}
	</style>
	<title>Business Company Usage</title>
	<script>
		function topFunction() {
			document.body.scrollTop = 0;
			document.documentElement.scrollTop = 0;
		}
	</script>
</head>
<body>
<%
	@SuppressWarnings("unchecked")
	Map<String, String[]> parameters = request.getParameterMap();

	final String bloombergCompanyIdentifier = parameters.containsKey("bloombergCompanyIdentifier") ? request.getParameter("bloombergCompanyIdentifier") : "";
	final String businessCompanyTypeIdentifier = parameters.containsKey("businessCompanyTypeIdentifier") ? request.getParameter("businessCompanyTypeIdentifier") : "";
	final String sourceBusinessCompanyIdentifier = parameters.containsKey("sourceBusinessCompanyIdentifier") ? request.getParameter("sourceBusinessCompanyIdentifier") : "";
	final String targetBusinessCompanyIdentifier = parameters.containsKey("targetBusinessCompanyIdentifier") ? request.getParameter("targetBusinessCompanyIdentifier") : "";
	final boolean deleteBusinessCompanyAfterMerge = StringUtils.isEqual(request.getParameter("delete company"), "true");
	final PageContext context = pageContext;

	final String buttonAction = parameters.containsKey("action") ? request.getParameter("action") : "";
	Map<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> companySystemUsedByEntityMap = Collections.emptyMap();
	if ("LOAD".equals(buttonAction)) {
		if (StringUtils.isEmpty(bloombergCompanyIdentifier) || StringUtils.isEmpty(businessCompanyTypeIdentifier)) {
			pageContext.setAttribute("status-message", "Load requires 'Bloomberg Company Identifier' and 'Investment Manager' values.");
		}
	}
	else if ("MERGE".equals(buttonAction)) {
		if (!StringUtils.isEmpty(sourceBusinessCompanyIdentifier) && !StringUtils.isEmpty(targetBusinessCompanyIdentifier)) {
			SecurityImpersonationHandler securityImpersonationHandler = (SecurityImpersonationHandler) this.applicationContext.getBean("securityImpersonationHandler");
			securityImpersonationHandler.runAsSecurityUserName(SecurityUser.SYSTEM_USER, new Runnable() {
				@Override
				public void run() {
					mergeCompanyData(sourceBusinessCompanyIdentifier, targetBusinessCompanyIdentifier, deleteBusinessCompanyAfterMerge, context);
				}
			});
		}
		else {
			pageContext.setAttribute("status-message", "Merge requires 'Starting Business Company' and 'Target Business Company' values.");
		}
	}
	else if ("MERGE ALL".equals(buttonAction)) {
		if (!StringUtils.isEmpty(targetBusinessCompanyIdentifier)) {
			SecurityImpersonationHandler securityImpersonationHandler = (SecurityImpersonationHandler) this.applicationContext.getBean("securityImpersonationHandler");
			securityImpersonationHandler.runAsSecurityUserName(SecurityUser.SYSTEM_USER, new Runnable() {
				@Override
				public void run() {
					mergeAllCompanyData(bloombergCompanyIdentifier, businessCompanyTypeIdentifier, targetBusinessCompanyIdentifier, deleteBusinessCompanyAfterMerge, context);
				}
			});
		}
		else {
			pageContext.setAttribute("status-message", "Merge requires 'Starting Business Company' and 'Target Business Company' values.");
		}
	}

	if (!StringUtils.isEmpty(bloombergCompanyIdentifier) && !StringUtils.isEmpty(businessCompanyTypeIdentifier)) {
		companySystemUsedByEntityMap = getSystemUsedByEntityListMap(bloombergCompanyIdentifier, businessCompanyTypeIdentifier);
	}
%>
<%-- bloomberge identifier and company type selection --%>
<br>
<form id="identifierMergeForm" class="grid" action="business-company-usage.jsp" method="post" onsubmit="action.disabled=true; return true;">
	<div style="grid-column: a / span 2;" class="title border-top border-sides">Business Company Usage Lookup:</div>
	<div style="grid-column: d / i;"></div>

	<div style="grid-column: a;" class="cell-header">Bloomberg Company Identifier</div>
	<div style="grid-column: span 1;" class="cell-header">Company Type</div>
	<div style="grid-column: c;" class="action-button">
		<input name="action" type="submit" value="LOAD" class="button">
	</div>
	<div style="grid-column:  d / i; grid-row: span 2;"></div>

	<input style="grid-column: a;" class="cell-text cell-font" type="text" name="bloombergCompanyIdentifier" value="<%=bloombergCompanyIdentifier%>"
		   title="Bloomberg ID">
	<select style="grid-column: span 1;" class="cell-text cell-font" name="businessCompanyTypeIdentifier" title="Company Type">
		<%
			for (BusinessCompanyType businessCompanyType : getBusinessCompanyTypeLabelList()) {
				String selected = Objects.equals(businessCompanyType.getId().toString(), businessCompanyTypeIdentifier) ? "selected" : "";
		%>
		<option value="<%=businessCompanyType.getId()%>" <%=selected%>><%=businessCompanyType.getLabel()%>
		</option>
		<% } %>
	</select>
	<div style="grid-column:  d / i;"></div>

	<%-- source and targe business company selection --%>
	<div style="grid-column:  a / i;"><br></div>

	<div style="grid-column: a / span 2;" class="title border-top border-sides">Business Company Merge:</div>
	<div style="grid-column: c;" class="action-button">
		<input name="action" type="submit" form="identifierMergeForm" value="MERGE" class="button">
	</div>
	<div style="grid-column: d / i;"></div>

	<div style="grid-column: a;" class="cell-header">Starting Business Company</div>
	<div style="grid-column: span 1;" class="cell-header">Target Business Company</div>
	<div style="grid-column: c;" class="action-button">
		<input name="action" type="submit" value="MERGE ALL" class="button">
	</div>
	<div style="grid-column:  d / i;"></div>

	<select style="grid-column: a;" class="cell-text cell-font" name="sourceBusinessCompanyIdentifier" title="Source Business Company">
		<%
			@SuppressWarnings("unchecked")
			ListIterator<BusinessCompany> iterator = new ArrayList(companySystemUsedByEntityMap.keySet()).listIterator(companySystemUsedByEntityMap.size());
			while (iterator.hasPrevious()) {
				BusinessCompany businessCompany = iterator.previous();
				String selected = Objects.equals(businessCompany.getId().toString(), sourceBusinessCompanyIdentifier) ? "selected" : "";
		%>
		<option value="<%=businessCompany.getId()%>" <%=selected%>><%=getLabel(businessCompany)%>
		</option>
		<% } %>
	</select>
	<select style="grid-column: b / c;" class="cell-text cell-font" name="targetBusinessCompanyIdentifier" title="Target Business Company">
		<%
			for (BusinessCompany businessCompany : companySystemUsedByEntityMap.keySet()) {
				String selected = Objects.equals(businessCompany.getId().toString(), targetBusinessCompanyIdentifier) ? "selected" : "";
		%>
		<option value="<%=businessCompany.getId()%>" <%=selected%>><%=getLabel(businessCompany)%>
		</option>
		<% } %>
	</select>
	<div style="grid-column: d / i;"></div>

	<div style="grid-column: c / i;">
		<input type="checkbox" id="deleteCompany" name="delete company" value="true">
		<label for="deleteCompany">Delete Business Company After Merge</label>
	</div>
</form>
<%-- status row --%>
<%
	String message;
	String clazz;
	if (StringUtils.isEmpty(StringUtils.toNullableString(pageContext.getAttribute("error-message")))) {
		String statusMessage = toString(pageContext.getAttribute("status-message"));
		message = StringUtils.isEmpty(statusMessage) ? "" : "STATUS:  " + statusMessage;
		clazz = "status-text";
	}
	else {
		message = "ERROR:  " + toString(pageContext.getAttribute("error-message"));
		clazz = "error-text";
	}
%>
<br>
<div class="grid">
	<div style="grid-column: a / i;" class="<%=clazz%>"><%=message%>
	</div>
	<br>
</div>
<%-- usage summary by system used by type --%>
<div class="grid">
	<div style="grid-column: a /i;">
		<hr>
	</div>
	<div style="grid-column: a / span 2;" class="title border-top border-sides">Business Company Link Usage Summary:</div>
	<div style="grid-column: d / i;"></div>

	<div style="grid-column: a;" class="cell-header">Link Type</div>
	<div class="cell-header">Link Usage</div>
	<div style="grid-column:  c / i"></div>
	<%
		Map<String, Integer> usageSummary = getUsageCounts(companySystemUsedByEntityMap);
		for (Map.Entry<String, Integer> stringIntegerEntry : usageSummary.entrySet()) {
			Integer count = stringIntegerEntry.getValue();
	%>
	<div style="grid-column: a;" class="cell-text"><%=stringIntegerEntry.getKey()%>
	</div>
	<div style="grid-column: b;" class="cell-numeric"><%=count%>
	</div>
	<div style="grid-column: c / i;"></div>
	<%
		}
	%>
</div>
<br>
<%-- Used by detail table --%>
<div class="grid">
	<div style="grid-column: a / i;">
		<hr>
	</div>

	<div style="grid-column: a / span 4;" class="title">Business Company Total Usage Summary:</div>
	<div style="grid-column: f / i;"></div>

	<%
		// go through business company organized system used by entities.
		for (Map.Entry<BusinessCompany, Map<SystemColumn, List<SystemUsedByEntity>>> businessCompanyMapEntry : companySystemUsedByEntityMap.entrySet()) {
			Map<SystemColumn, List<SystemUsedByEntity>> typeSystemUsedByMap = businessCompanyMapEntry.getValue();

	%>
	<div style="grid-column: a;" class="cell-header">Company Name</div>
	<div style="grid-column: b;" class="cell-header">Total Usage</div>
	<div style="grid-column: c;" class="cell-header">Business Company ID</div>
	<div style="grid-column: d / i;"></div>

	<div style="grid-column: a;" class="cell-text"><%=(businessCompanyMapEntry.getKey()).getName()%>
	</div>
	<div style="grid-column: b;" class="cell-numeric"><%=getEntryCount(typeSystemUsedByMap)%>
	</div>
	<div style="grid-column: c;" class="cell-numeric"><%=(businessCompanyMapEntry.getKey()).getId()%>
	</div>
	<div style="grid-column: d;" class="cell-header">Link Type</div>
	<div style="grid-column: e;" class="cell-header">Link Usage</div>
	<div style="grid-column: f / i;"></div>
	<%-- usage by company details --%>
	<%
		for (Map.Entry<SystemColumn, List<SystemUsedByEntity>> systemColumnListEntry : typeSystemUsedByMap.entrySet()) {
			List<SystemUsedByEntity> entries = systemColumnListEntry.getValue();
	%>
	<div style="grid-column: a / c;"></div>
	<div style="grid-column: d;" class="cell-text"><%=getLabel(systemColumnListEntry.getKey())%>
	</div>
	<div style="grid-column: e;" class="cell-numeric"><%=entries.size()%>
	</div>
	<div style="grid-column: f;" class="cell-header">Foreign Key Column</div>
	<div style="grid-column: g;" class="cell-header">Object ID</div>
	<div style="grid-column: h;" class="cell-header">Created By</div>
	<div style="grid-column: i;" class="cell-header">Created On</div>
	<%
		for (SystemUsedByEntity systemUsedByEntity : entries) {
			String usedById = systemUsedByEntity.getUsedById() == null ? "" : systemUsedByEntity.getUsedById().toString();
			String createdDate = systemUsedByEntity.getCreateDate() == null ? "" : DateUtils.fromDate(systemUsedByEntity.getCreateDate());
	%>
	<div style="grid-column: d / span 2;" class="cell-detail border-sides"><%=systemUsedByEntity.getUsedByLabel()%>
	</div>
	<div style="grid-column: f;" class="cell-detail border-sides"><%=systemUsedByEntity.getColumn().getLabel()%>
	</div>
	<div style="grid-column: g;" class="cell-detail border-sides"><%=usedById%>
	</div>
	<div style="grid-column: h;" class="cell-detail border-sides"><%=getUsedByName(systemUsedByEntity.getCreateUserId())%>
	</div>
	<div style="grid-column: i;" class="cell-detail border-sides"><%=createdDate%>
	</div>
	<%
		}
	%>
	<%
		}
	%>

	<div style="grid-column: a / span 9;" class="entire-row">&nbsp;</div>
	<%
		}
	%>

	<div style="grid-column: a / i;">
		<hr>
	</div>

	<div style="grid-column: a">
		<button class="button" onclick="topFunction()">BACK TO THE TOP</button>
	</div>
</div>
<br>
</body>
</html>
