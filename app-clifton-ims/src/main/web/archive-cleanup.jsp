<%@ page import="com.clifton.core.converter.string.StringToIntegerConverter" %>
<%@ page import="com.clifton.core.dataaccess.file.FileUtils" %>
<%@ page import="com.clifton.core.util.StringUtils" %>
<%@ page import="com.clifton.core.util.date.DateUtils" %>
<%@ page import="com.clifton.export.definition.ExportDefinition" %>
<%@ page import="com.clifton.export.definition.ExportDefinitionService" %>
<%@ page import="com.clifton.export.definition.search.ExportDefinitionSearchForm" %>
<%@ page import="com.clifton.export.run.ExportRunHistory" %>
<%@ page import="com.clifton.export.run.ExportRunService" %>
<%@ page import="com.clifton.export.run.search.ExportRunHistorySearchForm" %>
<%@ page import="com.clifton.integration.file.IntegrationFile" %>
<%@ page import="com.clifton.integration.file.IntegrationFileService" %>
<%@ page import="com.clifton.integration.outgoing.export.IntegrationExport" %>
<%@ page import="com.clifton.integration.outgoing.export.IntegrationExportIntegrationFile" %>
<%@ page import="com.clifton.integration.outgoing.export.IntegrationExportService" %>
<%@ page import="com.clifton.integration.outgoing.export.search.IntegrationExportIntegrationFileSearchForm" %>
<%@ page import="com.clifton.integration.outgoing.export.search.IntegrationExportSearchForm" %>
<%@ page import="com.clifton.investment.instruction.InvestmentInstruction" %>
<%@ page import="com.clifton.investment.instruction.run.InvestmentInstructionRun" %>
<%@ page import="com.clifton.investment.instruction.run.InvestmentInstructionRunService" %>
<%@ page import="com.clifton.security.impersonation.SecurityImpersonationHandler" %>
<%@ page import="com.clifton.security.user.SecurityUser" %>
<%@ page import="java.io.File" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.List" %>
<%!
	void moveFile(String originalLocation, String rootArchiveDirectory, IntegrationFile file, boolean encrypted) throws IOException {
		File originalFile = new File(originalLocation);
		FileUtils.moveFileCreatePath(originalFile, FileUtils.combinePath(rootArchiveDirectory, !encrypted ? file.getFileNameWithPath() : file.getEncryptedFileNameWithPath()));
		File originalDirectory = originalFile.getParentFile();
		if (!FileUtils.directoryContainsFiles(originalDirectory)) {
			FileUtils.delete(originalDirectory);
		}
	}


	void saveIntegrationFile(SecurityImpersonationHandler securityImpersonationHandler, final IntegrationFileService integrationFileService, final IntegrationFile file) {
		securityImpersonationHandler.runAsSecurityUserName(SecurityUser.SYSTEM_USER, new Runnable() {
			@Override
			public void run() {
				integrationFileService.saveIntegrationFile(file);
			}
		});
	}


	void addIntegrationFileError(StringBuilder integrationFileErrors, IntegrationFile file, String originalLocation, boolean encrypted) {
		integrationFileErrors.append("Integration ");
		if (encrypted) {
			integrationFileErrors.append("Encrypted ");
		}
		integrationFileErrors.append("File: ,");
		integrationFileErrors.append(file.getId());
		integrationFileErrors.append(", does not exist at location: ,");
		integrationFileErrors.append(originalLocation);
		integrationFileErrors.append("<br />");
	}


	void addIntegrationExportIntegrationFileError(StringBuilder integrationExportIntegrationFileErrors, IntegrationExportIntegrationFile integrationExportIntegrationFile, IntegrationExport integrationExport) {
		integrationExportIntegrationFileErrors.append("IntegrationExportIntegrationFile: ,");
		integrationExportIntegrationFileErrors.append(integrationExportIntegrationFile.getId());
		integrationExportIntegrationFileErrors.append(" for Integration Export: ,");
		integrationExportIntegrationFileErrors.append(integrationExport.getId());
		integrationExportIntegrationFileErrors.append(", has a null integration file <br />");
	}


	void printLine(String source, String definitionName, String definitionId, String runHistoryId, String integrationExportId, String integrationFileId, String originalLocation, String newlocation, javax.servlet.jsp.JspWriter out) throws IOException {
		StringBuilder line = new StringBuilder();
		addQuotedValue(line, source, true);
		addQuotedValue(line, definitionName, true);
		addQuotedValue(line, definitionId, true);
		addQuotedValue(line, runHistoryId, true);
		addQuotedValue(line, integrationExportId, true);
		addQuotedValue(line, integrationFileId, true);
		addQuotedValue(line, originalLocation, true);
		addQuotedValue(line, newlocation, false);
		line.append("<br />");
		out.println(line.toString());
	}


	void addQuotedValue(StringBuilder builder, String value, boolean addComma) {
		builder.append("\"");
		builder.append(value);
		builder.append("\"");
		if (addComma) {
			builder.append(",");
		}
	}
%>
<%
	final org.springframework.web.context.WebApplicationContext applicationContext = org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());

	final ExportDefinitionService exportDefinitionService = (ExportDefinitionService) applicationContext.getBean("exportDefinitionService");
	final ExportRunService exportRunService = (ExportRunService) applicationContext.getBean("exportRunService");
	final IntegrationExportService integrationExportService = (IntegrationExportService) applicationContext.getBean("integrationExportService");
	final IntegrationFileService integrationFileService = (IntegrationFileService) applicationContext.getBean("integrationFileService");
	final SecurityImpersonationHandler securityImpersonationHandler = (SecurityImpersonationHandler) applicationContext.getBean("securityImpersonationHandler");
	final InvestmentInstructionRunService investmentInstructionRunService = (InvestmentInstructionRunService) applicationContext.getBean("investmentInstructionRunService");


	long time = System.currentTimeMillis();
	out.println("Starting migration <br />");

	try {
		int fileCounter = 0;

		StringBuilder integrationExportIntegrationFileErrors = new StringBuilder();
		integrationExportIntegrationFileErrors.append("IntegrationExportIntegrationFile Errors <br />");
		StringBuilder integrationFileErrors = new StringBuilder();
		integrationFileErrors.append("IntegrationFile Errors <br />");

		printLine("Export Source", "Export Definition Name", "Export Definition Id", "Export Run History Id", "Integration Export Id", "Integration File Id", "From Location", "To Location", out);

		/**
		 * LOOP THROUGH INSTRUCTION EXPORTS
		 */
		IntegrationExportSearchForm instructionSearchForm = new IntegrationExportSearchForm();
		instructionSearchForm.setIntegrationSourceName("IMS_INSTRUCTION");
		//Get instructionExports for instructions
		List<IntegrationExport> instructionExports = integrationExportService.getIntegrationExportList(instructionSearchForm);

		for (IntegrationExport integrationExport : instructionExports) {
			String sourceSystemIdentifier = integrationExport.getSourceSystemIdentifier();
			if (!StringUtils.isEmpty(sourceSystemIdentifier)) {

				IntegrationExportIntegrationFileSearchForm integrationExportIntegrationFileSearchForm = new IntegrationExportIntegrationFileSearchForm();
				integrationExportIntegrationFileSearchForm.setExportId(integrationExport.getId());
				List<IntegrationExportIntegrationFile> integrationExportIntegrationFiles = integrationExportService.getIntegrationExportIntegrationFileList(integrationExportIntegrationFileSearchForm);

				//The current files
				for (IntegrationExportIntegrationFile integrationExportIntegrationFile : integrationExportIntegrationFiles) {
					final IntegrationFile file = integrationExportIntegrationFile.getReferenceTwo();
					if (file != null) {
						//Get the instruction run for a given instruction export
						InvestmentInstructionRun instructionRun = investmentInstructionRunService.getInvestmentInstructionRun(new StringToIntegerConverter().convert(sourceSystemIdentifier));
						//Get the instruction for a given instruction run
						InvestmentInstruction instruction = instructionRun.getInvestmentInstruction();

						String rootArchiveDirectory = FileUtils.normalizeFilePathSeparators(integrationFileService.getRootArchiveDirectoryMap().get(integrationExport.getIntegrationSource().getType().getName()));
						String originalLocation = FileUtils.combinePaths(rootArchiveDirectory, file.getFileNameWithPath());

						if (FileUtils.fileExists(originalLocation)) {
							Date fileDate = file.getReceivedDate();
							String correctPath = FileUtils.combinePaths("Operations",
									"Instructions",
									DateUtils.fromDate(fileDate, "yyyy"),
									DateUtils.fromDate(fileDate, "M-MMMM"),
									DateUtils.fromDate(fileDate, "dd"),
									FileUtils.replaceInvalidCharacters(instruction.getRecipientCompany().getName(), "_"),
									FileUtils.getFileName(file.getFileNameWithPath()));
							if (!StringUtils.isEqual(originalLocation, FileUtils.combinePath(rootArchiveDirectory, correctPath))) {

								file.setFileNameWithPath(correctPath);
								String newLocation = FileUtils.combinePath(rootArchiveDirectory, file.getFileNameWithPath());
								printLine("IMS_INSTRUCTION", "", "", "", integrationExport.getId().toString(), file.getId().toString(), originalLocation, newLocation, out);

								moveFile(originalLocation, rootArchiveDirectory, file, false);
								saveIntegrationFile(securityImpersonationHandler, integrationFileService, file);

								fileCounter += 1;
							}
						}
						else {
							addIntegrationFileError(integrationFileErrors, file, originalLocation, false);
							continue;
						}

					}
					else {
						addIntegrationExportIntegrationFileError(integrationExportIntegrationFileErrors, integrationExportIntegrationFile, integrationExport);
					}
				}
			}
		}


		StringBuilder definitionErrors = new StringBuilder();
		definitionErrors.append("Export Definition Errors <br />");


		/**
		 * LOOP THROUGH EXPORT DEFINITIONS
		 */
		List<ExportDefinition> definitions = exportDefinitionService.getExportDefinitionList(new ExportDefinitionSearchForm());
		for (ExportDefinition definition : definitions) {
			if (definition.getType() != null) {
				ExportRunHistorySearchForm runHistorySearchForm = new ExportRunHistorySearchForm();
				runHistorySearchForm.setDefinitionId(definition.getId());
				//runHistorySearchForm.setStatusId((short) 6);
				List<ExportRunHistory> runHistories = exportRunService.getExportRunHistoryList(runHistorySearchForm);

				for (ExportRunHistory runHistory : runHistories) {
					IntegrationExportSearchForm integrationExportSearchForm = new IntegrationExportSearchForm();
					integrationExportSearchForm.setSourceSystemIdentifier(runHistory.getId().toString());
					integrationExportSearchForm.setIntegrationSourceName("IMS");
					List<IntegrationExport> integrationExports = integrationExportService.getIntegrationExportList(integrationExportSearchForm);

					for (IntegrationExport integrationExport : integrationExports) {
						if (!StringUtils.isEmpty(integrationExport.getSourceSystemDefinitionName())) {
							//There should only ever be one export for a given export run
							IntegrationExportIntegrationFileSearchForm integrationExportIntegrationFileSearchForm = new IntegrationExportIntegrationFileSearchForm();
							integrationExportIntegrationFileSearchForm.setExportId(integrationExport.getId());
							List<IntegrationExportIntegrationFile> integrationExportIntegrationFiles = integrationExportService.getIntegrationExportIntegrationFileList(integrationExportIntegrationFileSearchForm);

							//The current files
							for (IntegrationExportIntegrationFile integrationExportIntegrationFile : integrationExportIntegrationFiles) {
								final IntegrationFile file = integrationExportIntegrationFile.getReferenceTwo();
								if (file != null) {

									String rootArchiveDirectory = FileUtils.normalizeFilePathSeparators(integrationFileService.getRootArchiveDirectoryMap().get(integrationExport.getIntegrationSource().getType().getName()));
									String originalLocation = FileUtils.combinePaths(rootArchiveDirectory, file.getFileNameWithPath());

									if (FileUtils.fileExists(originalLocation)) {
										if (!StringUtils.isEqual(FileUtils.combinePaths(rootArchiveDirectory, definition.getType().getName(), FileUtils.replaceInvalidCharacters(definition.getName(), "_"), FileUtils.getFileName(file.getFileNameWithPath())), originalLocation)) {
											file.setFileNameWithPath(FileUtils.combinePaths(definition.getType().getName(), file.getFileNameWithPath()));
											String newLocation = FileUtils.combinePath(rootArchiveDirectory, file.getFileNameWithPath());

											printLine("IMS", definition.getName(), definition.getId().toString(), runHistory.getId().toString(), integrationExport.getId().toString(), file.getId().toString(), originalLocation, newLocation, out);

											moveFile(originalLocation, rootArchiveDirectory, file, false);

											fileCounter += 1;
										}
									}
									else {
										addIntegrationFileError(integrationFileErrors, file, originalLocation, false);
										continue;
									}

									if (!StringUtils.isEmpty(file.getEncryptedFileNameWithPath())) {
										String originalEncryptedLocation = FileUtils.combinePaths(rootArchiveDirectory, file.getEncryptedFileNameWithPath());
										if (FileUtils.fileExists(originalEncryptedLocation)) {
											if (!StringUtils.isEqual(FileUtils.combinePaths(rootArchiveDirectory, definition.getType().getName(), FileUtils.replaceInvalidCharacters(definition.getName(), "_"), FileUtils.getFileName(file.getEncryptedFileNameWithPath())), originalEncryptedLocation)) {
												file.setEncryptedFileNameWithPath(FileUtils.combinePaths(definition.getType().getName(), file.getEncryptedFileNameWithPath()));
												String newEncryptedLocation = FileUtils.combinePaths(definition.getType().getName(), file.getEncryptedFileNameWithPath());
												printLine("IMS", definition.getName(), definition.getId().toString(), runHistory.getId().toString(), integrationExport.getId().toString(), file.getId().toString(), originalEncryptedLocation, newEncryptedLocation, out);

												moveFile(originalEncryptedLocation, rootArchiveDirectory, file, true);

												fileCounter += 1;
											}
										}
										else {
											addIntegrationFileError(integrationFileErrors, file, originalEncryptedLocation, true);
											continue;
										}
									}
									saveIntegrationFile(securityImpersonationHandler, integrationFileService, file);
								}
								else {
									addIntegrationExportIntegrationFileError(integrationExportIntegrationFileErrors, integrationExportIntegrationFile, integrationExport);
								}
							}
						}
					}
				}
			}
			else {
				definitionErrors.append("Export Definition[").append(definition.getId()).append("] : ").append(definition.getName()).append(" does not have a type + <br />");
			}
		}
		out.println(definitionErrors.toString());
		out.println(integrationExportIntegrationFileErrors.toString());
		out.println(integrationFileErrors.toString());
		out.println("Files moved " + fileCounter);
	}
	catch (Throwable e) {
		out.println("Error occurred: " + e.toString() + "<br />");
	}


	time = System.currentTimeMillis() - time;
	out.println("<br />PROCESSING TIME: " + time + " milliseconds");

%>
