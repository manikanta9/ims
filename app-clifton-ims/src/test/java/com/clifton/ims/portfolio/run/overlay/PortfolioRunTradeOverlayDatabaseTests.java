package com.clifton.ims.portfolio.run.overlay;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.BaseIMSDatabaseTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocation;
import com.clifton.investment.account.assetclass.position.InvestmentAccountAssetClassPositionAllocationService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.trade.PortfolioRunTrade;
import com.clifton.portfolio.run.trade.PortfolioRunTradeDetail;
import com.clifton.portfolio.run.trade.PortfolioRunTradeService;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocation;
import com.clifton.trade.assetclass.TradeAssetClassPositionAllocationSearchForm;
import com.clifton.trade.assetclass.TradeAssetClassService;
import com.clifton.trade.assetclass.process.TradeAssetClassPositionAllocationProcessService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.search.TradeSearchForm;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>PortfolioRunTradeOverlayDatabaseTests</code> test backend code for submitting trades
 * through trade creation.  This is specifically testing:
 * 1. Ability to enter long and shorts of same security and resulting Trade created is the net amount (or no trade if net = 0)
 * 2. Contract splitting result for Pending Trades/"Current" Booked Trades on Trade Creation Screen after trades are submitted/booked
 * <p>
 * When updating PIOS test db, this test will likely also need to be updated to reflect recent runs.  Should try to pick accounts that didn't have
 * any trades entered already to prevent issues comparing these changes to existing
 *
 * @author manderson
 */
public class PortfolioRunTradeOverlayDatabaseTests<T extends PortfolioRunTrade<D>, D extends PortfolioRunTradeDetail> extends BaseIMSDatabaseTests {

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private InvestmentAccountService investmentAccountService;

	@Resource
	private InvestmentAccountAssetClassPositionAllocationService investmentAccountAssetClassPositionAllocationService;


	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private PortfolioRunTradeService<T, D> portfolioRunTradeService;

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private TradeAssetClassPositionAllocationProcessService tradeAssetClassPositionAllocationProcessService;

	@Resource
	private TradeAssetClassService tradeAssetClassService;

	@Resource
	private TradeGroupService tradeGroupService;

	@Resource
	private TradeService tradeService;

	@Resource
	private WorkflowDefinitionService workflowDefinitionService;

	@Resource
	private WorkflowTransitionService workflowTransitionService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	private static final Date BALANCE_DATE = DateUtils.toDate("08/31/2020");
	private static final Date TRADE_DATE = DateUtils.toDate("09/01/2020");

	private static final String ANALYST_APPROVED = "Analyst Approved";

	private WorkflowState workflowStateAnalystApproved = null;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@BeforeEach
	@Override
	public void setUser() {
		super.setUser();

		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "imstestuser1");

		// Change all Trade Workflow Transitions to be Non Automatic to prevent validating trades - no necessary
		// for this test
		Workflow wf = this.workflowDefinitionService.getWorkflowByName("Trade Workflow");
		List<WorkflowTransition> transitionList = this.workflowTransitionService.getWorkflowTransitionListByWorkflow(wf.getId());
		for (WorkflowTransition wt : transitionList) {
			if (wt.isAutomatic()) {
				wt.setAutomatic(false);
				wt.setAutomaticDelayed(false);
				this.workflowTransitionService.saveWorkflowTransition(wt);
			}
		}
	}


	@Test
	public void testTradeContractSplittingWithExplicitAllocations_TradeInNotExplicitAssetClass() {
		// ESU9	2 split
		// Split is 54/1 explicit for the 1ST (47)

		// 050950	ABC-NABET Retirement Trust Fund - Target Allocation
		// Get the Tradable Run
		T run = getTradableRunForAccount("050950");

		// Get allocation string before trade
		String allocStringBefore = getExplicitPositionAllocationAsString(run.getClientInvestmentAccount().getId(), BALANCE_DATE);
		Assertions.assertEquals("[ESU0_2075_null_123382_08/31/2020_null_-7][MFSU0_2232_null_123392_07/23/2020_null_5][MESU0_2232_null_123394_07/23/2020_null_4][USZ0_7823_null_133181_08/26/2020_null_153][MESU0_7867_null_123394_07/10/2020_null_3]", allocStringBefore);

		tradeSecurity(run, "ESU0", new int[]{0, 2});
		List<Trade> tradeList = tradeRun(run, false);
		Assertions.assertEquals(1, tradeList.size());

		validateTrade(tradeList.get(0), true, 2);

		// Validate TradeAssetClassPositionAllocation entries
		String tradeAssetClassAllocationResult = getTradeAssetClassPositionAllocationListAsString(run);
		String expectedTradeAllocResult = "[ESU0_2232_46_050950: BUY 2 of ESU0 on 09/01/2020_2]";
		Assertions.assertEquals(expectedTradeAllocResult, tradeAssetClassAllocationResult);

		// Pull the run again - and Validate Splits for Pending Trades load correctly
		run = getTradableRunForAccount("050950");
		validateSecuritySplit(run, "pendingContracts", "ESU0", new int[]{0, 2});

		// Process the Account Allocations
		processTradeAssetClassPositionAllocations(run.getClientInvestmentAccount().getId(), TRADE_DATE);

		// Traded in asset class that wasn't explicit - should remain the same
		Assertions.assertEquals(allocStringBefore, getExplicitPositionAllocationAsString(run.getClientInvestmentAccount().getId(), TRADE_DATE), "Expect same explicit position allocations after trade.");
	}


	@Test
	public void testTradeContractSplittingWithExplicitAllocations_TradeInExplicitAssetClass() {
		// FVU9
		// Split is 6/0/3 explicit for the 1ST AND 2ND (6/0)

		// 221700	1199 SEIU Greater NY Pension Fund PIOS
		// Get the Tradable Run
		T run = getTradableRunForAccount("221700");
		// Get allocation string before trade
		String allocStringBefore = getExplicitPositionAllocationAsString(run.getClientInvestmentAccount().getId(), BALANCE_DATE);
		Assertions.assertEquals("[TUZ0_4_null_133173_08/26/2020_null_8][FVZ0_4_null_133175_08/26/2020_null_15][TYZ0_4_null_133177_08/26/2020_null_9][USZ0_4_null_133181_08/26/2020_null_12][TUZ0_2855_null_133173_08/26/2020_null_10][FVZ0_2855_null_133175_08/26/2020_null_8][TYZ0_2855_null_133177_08/26/2020_null_2][USZ0_2855_null_133181_08/26/2020_null_1]",
				allocStringBefore);

		tradeSecurity(run, "FVZ0", new int[]{1, 2, 0});
		List<Trade> tradeList = tradeRun(run, false);
		Assertions.assertEquals(1, tradeList.size());

		validateTrade(tradeList.get(0), true, 3);

		// Validate TradeAssetClassPositionAllocation entries
		String tradeAssetClassAllocationResult = getTradeAssetClassPositionAllocationListAsString(run);
		String expectedTradeAllocResult = "[FVZ0_4_407_221700: BUY 3 of FVZ0 on 09/01/2020_1][FVZ0_2855_410_221700: BUY 3 of FVZ0 on 09/01/2020_2]";
		Assertions.assertEquals(expectedTradeAllocResult, tradeAssetClassAllocationResult);

		// Pull the run again - and Validate Splits for Pending Trades load correctly
		run = getTradableRunForAccount("221700");
		validateSecuritySplit(run, "pendingContracts", "FVZ0", new int[]{1, 2, 0});

		// Process the Account Allocations
		processTradeAssetClassPositionAllocations(run.getClientInvestmentAccount().getId(), TRADE_DATE);

		// Traded in asset class that was explicit
		// Updated Split 17/-4(exp)
		Assertions.assertEquals("[TUZ0_4_null_133173_08/26/2020_null_8][FVZ0_4_null_133175_09/01/2020_null_16][TYZ0_4_null_133177_08/26/2020_null_9][USZ0_4_null_133181_08/26/2020_null_12][TUZ0_2855_null_133173_08/26/2020_null_10][FVZ0_2855_null_133175_09/01/2020_null_10][TYZ0_2855_null_133177_08/26/2020_null_2][USZ0_2855_null_133181_08/26/2020_null_1]", getExplicitPositionAllocationAsString(run.getClientInvestmentAccount().getId(), TRADE_DATE));
	}


	@Test
	public void testTradeContractSplittingWithExplicitAllocations_TradeInExplicitAssetClass_ZeroNetTrade() {
		// ESU9	3
		// Split is 45/105/21/2 explicit for the last 3

		// 576700	Sonoma County
		// Get the Tradable Run
		T run = getTradableRunForAccount("576700");
		// Get allocation string before trade
		String allocStringBefore = getExplicitPositionAllocationAsString(run.getClientInvestmentAccount().getId(), BALANCE_DATE);
		Assertions.assertEquals(
				"[ESU0_2668_null_123382_08/28/2020_null_17][FAU0_2668_null_123384_07/30/2020_null_4][RTYU0_2669_null_123386_08/07/2020_null_11][RTYU0_2670_null_123386_08/17/2020_null_317][CDU0_2671_null_123441_07/02/2020_null_38][ESU0_2672_null_123382_08/07/2020_null_103][RTYU0_2672_null_123386_08/07/2020_null_35][MFSU0_2672_null_123392_08/28/2020_null_101][MESU0_2672_null_123394_08/07/2020_null_82][PTU0_2672_null_123421_07/02/2020_null_6][USZ0_2673_null_133181_08/26/2020_null_53][ESU0_2959_null_123382_08/07/2020_null_3][MFSU0_2959_null_123392_08/07/2020_null_2][PTU0_2959_null_123421_07/02/2020_null_0][CDU0_2959_null_123441_07/02/2020_null_0][TUZ0_2960_null_133173_08/26/2020_null_1][FVZ0_2960_null_133175_08/26/2020_null_2][TYZ0_2960_null_133177_08/26/2020_null_1]",
				allocStringBefore);

		tradeSecurity(run, "ESU0", new int[]{0, 2, -1, -1});
		List<Trade> tradeList = tradeRun(run, true);
		Assertions.assertEquals(0, tradeList.size());

		// Validate TradeAssetClassPositionAllocation entries
		String tradeAssetClassAllocationResult = getTradeAssetClassPositionAllocationListAsString(run);
		String expectedTradeAllocResult = "[ESU0_2668_5_576700: Virtual Trade of ESU0 on 09/01/2020_2][ESU0_2672_152_576700: Virtual Trade of ESU0 on 09/01/2020_-1][ESU0_2959_152_576700: Virtual Trade of ESU0 on 09/01/2020_-1][FAU0_2668_5_576700: SELL 3 of FAU0 on 09/01/2020_-3]";
		Assertions.assertEquals(expectedTradeAllocResult, tradeAssetClassAllocationResult);

		// Pull the run again - and Validate Splits for Pending Trades load correctly
		run = getTradableRunForAccount("576700");
		validateSecuritySplit(run, "pendingVirtualContracts", "ESU0", new int[]{0, 2, -1, -1});

		// Process the Account Allocations
		processTradeAssetClassPositionAllocations(run.getClientInvestmentAccount().getId(), TRADE_DATE);

		// Traded in asset classes that were explicit
		Assertions.assertEquals(
				"[ESU0_2668_null_123382_09/01/2020_null_19][FAU0_2668_null_123384_09/01/2020_null_1][RTYU0_2669_null_123386_08/07/2020_null_11][RTYU0_2670_null_123386_08/17/2020_null_317][CDU0_2671_null_123441_07/02/2020_null_38][ESU0_2672_null_123382_09/01/2020_null_102][RTYU0_2672_null_123386_08/07/2020_null_35][MFSU0_2672_null_123392_08/28/2020_null_101][MESU0_2672_null_123394_08/07/2020_null_82][PTU0_2672_null_123421_07/02/2020_null_6][USZ0_2673_null_133181_08/26/2020_null_53][ESU0_2959_null_123382_09/01/2020_null_2][MFSU0_2959_null_123392_08/07/2020_null_2][PTU0_2959_null_123421_07/02/2020_null_0][CDU0_2959_null_123441_07/02/2020_null_0][TUZ0_2960_null_133173_08/26/2020_null_1][FVZ0_2960_null_133175_08/26/2020_null_2][TYZ0_2960_null_133177_08/26/2020_null_1]",
				getExplicitPositionAllocationAsString(run.getClientInvestmentAccount().getId(), TRADE_DATE));
	}


	///////////////////////////////////////////////////////////////


	private T getTradableRunForAccount(String accountNumber) {
		PortfolioRun run = this.portfolioRunService.getPortfolioRunByMainRun(getClientAccountByNumber(accountNumber).getId(), BALANCE_DATE);
		// If run is not in tradable state - move it
		if (!ANALYST_APPROVED.equals(run.getWorkflowState().getName())) {
			if (this.workflowStateAnalystApproved == null) {
				Workflow workflow = this.workflowDefinitionService.getWorkflowByName("Portfolio Run Status");
				this.workflowStateAnalystApproved = this.workflowDefinitionService.getWorkflowStateByName(workflow.getId(), ANALYST_APPROVED);
			}
			this.workflowTransitionService.executeWorkflowTransition(PortfolioRun.TABLE_PORTFOLIO_RUN, run.getId(), this.workflowStateAnalystApproved.getId());
		}
		return this.portfolioRunTradeService.getPortfolioRunTrade(run.getId());
	}


	private void validateSecuritySplit(T run, String propertyName, String securitySymbol, int[] contractQuantities) {
		int quantityCount = 0;

		for (D rep : CollectionUtils.getIterable(run.getDetailList())) {
			if (securitySymbol.equals(rep.getSecurity().getSymbol())) {
				if (contractQuantities.length > quantityCount) {
					int quantity = contractQuantities[quantityCount];
					BigDecimal actualQuantity = (BigDecimal) BeanUtils.getPropertyValue(rep, propertyName);
					String msg = "Expected [" + securitySymbol + "] # " + (quantityCount + 1) + " asset class to have [" + quantity + "] " + propertyName + " contracts allocated but was ["
							+ CoreMathUtils.formatNumberInteger(actualQuantity) + "].";
					if (quantity == 0) {
						Assertions.assertTrue(MathUtils.isNullOrZero(actualQuantity), msg);
					}
					else {
						Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(quantity), actualQuantity), msg);
					}
				}
				quantityCount = quantityCount + 1;
				if (quantityCount == contractQuantities.length) {
					break;
				}
			}
		}
	}


	private void tradeSecurity(T run, String securitySymbol, int[] contractQuantities) {
		int quantityCount = 0;

		for (D rep : CollectionUtils.getIterable(run.getDetailList())) {
			if (securitySymbol.equals(rep.getSecurity().getSymbol())) {
				if (contractQuantities.length > quantityCount) {
					int quantity = contractQuantities[quantityCount];
					if (quantity < 0) {
						rep.setSellContracts(BigDecimal.valueOf(quantity * -1.0));
					}
					else {
						rep.setBuyContracts(BigDecimal.valueOf(quantity));
					}
				}
				quantityCount = quantityCount + 1;
				if (quantityCount == contractQuantities.length) {
					break;
				}
			}
		}
	}


	private List<Trade> tradeRun(T run, boolean expectNoTrades) {
		run.setTradeDate(TRADE_DATE);
		// Not sure why this is happening in the test, but the "current contracts" which
		// aren't saved seem to be kept and then later when retrieving again it's pulling them in again
		// adding to the quantity instead of starting over.
		// For now - just submit details with buy/sell contracts similar to what the UI does
		List<D> originalDetailList = run.getDetailList();
		run.setDetailList(originalDetailList.stream()
				.filter(d -> d.getSellContracts() != null || d.getBuyContracts() != null)
				.collect(Collectors.toList()));

		TradeGroup group = this.portfolioRunTradeService.processPortfolioRunTradeCreation(run);

		// reset details on run
		run.setDetailList(originalDetailList);

		if (expectNoTrades) {
			Assertions.assertNull(group, "Expected no trades to be created (0 Net Trade)");
			return Collections.emptyList();
		}
		else {
			Assertions.assertNotNull(group, "Expected trades to be created.");
		}

		TradeSearchForm tradeSearchForm = new TradeSearchForm();
		tradeSearchForm.setTradeGroupSourceFkFieldId(run.getId());
		tradeSearchForm.setTradeGroupTypeId(this.tradeGroupService.getTradeGroupTypeByName("Portfolio Run").getId());
		tradeSearchForm.setTradeGroupId(group.getId());
		return this.tradeService.getTradeList(tradeSearchForm);
	}


	private void processTradeAssetClassPositionAllocations(Integer clientAccountId, Date date) {
		// Process the Account Allocations
		this.tradeAssetClassPositionAllocationProcessService.processTradeAssetClassPositionAllocationList(clientAccountId, date);

		// Tests do not appear to be actually saving anything in the database except for inserts.
		// Data returned is the updated data, but when searching against the database it searches against original values, not updated
		//this.investmentAccountAssetClassPositionAllocationDAO.flushSession();

	}


	private String getTradeAssetClassPositionAllocationListAsString(T run) {
		TradeAssetClassPositionAllocationSearchForm searchForm = new TradeAssetClassPositionAllocationSearchForm();
		searchForm.setAccountId(run.getClientInvestmentAccount().getId());
		searchForm.setProcessed(false);
		List<TradeAssetClassPositionAllocation> list = this.tradeAssetClassService.getTradeAssetClassPositionAllocationList(searchForm);

		StringBuilder sb = new StringBuilder(16);
		list = BeanUtils.sortWithFunctions(list, CollectionUtils.createList(
				tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getSecurity().getId(),
				tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getAccountAssetClass().getId(),
				tradeAssetClassPositionAllocation -> tradeAssetClassPositionAllocation.getReplication().getId()),
				CollectionUtils.createList(true, true, true));

		for (TradeAssetClassPositionAllocation alloc : list) {
			sb.append('[').append(alloc.getSecurity().getSymbol())
					.append('_').append(alloc.getAccountAssetClass().getId())
					.append('_').append(alloc.getReplication() != null ? alloc.getReplication().getId() : null)
					.append('_').append(alloc.getTradeLabel())
					.append('_').append(CoreMathUtils.formatNumberInteger(alloc.getAllocationQuantity()))
					.append(']');
		}
		return sb.toString();
	}


	private InvestmentAccount getClientAccountByNumber(String number) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setNumberEquals(number);
		return this.investmentAccountService.getInvestmentAccountList(searchForm).get(0);
	}


	private String getExplicitPositionAllocationAsString(Integer investmentAccountId, Date activeOnDate) {
		List<InvestmentAccountAssetClassPositionAllocation> allocList = this.investmentAccountAssetClassPositionAllocationService.getInvestmentAccountAssetClassPositionAllocationList(
				investmentAccountId, activeOnDate);
		if (CollectionUtils.isEmpty(allocList)) {
			return "[NONE]";
		}

		allocList = BeanUtils.sortWithFunctions(allocList, CollectionUtils.createList(
				accountAssetClassPositionAllocation -> accountAssetClassPositionAllocation.getAccountAssetClass().getId(),
				accountAssetClassPositionAllocation -> accountAssetClassPositionAllocation.getReplication() == null ? null : accountAssetClassPositionAllocation.getReplication().getId(),
				accountAssetClassPositionAllocation -> accountAssetClassPositionAllocation.getSecurity().getId()),
				CollectionUtils.createList(true, true, true));

		StringBuilder sb = new StringBuilder(16);
		for (InvestmentAccountAssetClassPositionAllocation alloc : allocList) {
			sb.append('[').append(alloc.getSecurity().getSymbol())
					.append('_').append(alloc.getAccountAssetClass().getId())
					.append('_').append(alloc.getReplication() != null ? alloc.getReplication().getId() : null)
					.append('_').append(alloc.getSecurity().getId())
					.append('_').append(DateUtils.fromDateShort(alloc.getStartDate()))
					.append('_').append(DateUtils.fromDateShort(alloc.getEndDate()))
					.append('_').append(CoreMathUtils.formatNumberInteger(alloc.getAllocationQuantity()))
					.append(']');
		}
		return sb.toString();
	}


	private void validateTrade(Trade trade, boolean buy, int quantity) {
		Assertions.assertEquals(buy, trade.isBuy(), () -> "Expected [" + trade.getInvestmentSecurity().getSymbol() + "] trade to be a " + (buy ? "buy but was a sell." : "sell but was a buy"));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(quantity), trade.getQuantityIntended()), () -> "Expected [" + trade.getInvestmentSecurity().getSymbol() + "] trade to be for " + quantity + " contracts but was for " + CoreMathUtils.formatNumberInteger(trade.getQuantityIntended()) + ".");
	}
}
