package com.clifton.ims.portfolio.manager;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.ims.BaseIMSDatabaseTests;
import com.clifton.investment.manager.InvestmentManagerAccount;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalance;
import com.clifton.investment.manager.balance.InvestmentManagerAccountBalanceService;
import com.clifton.investment.manager.balance.search.InvestmentManagerAccountBalanceSearchForm;
import com.clifton.product.manager.ProductManagerBalanceProcessingConfig;
import com.clifton.product.manager.ProductManagerBalanceProcessingTypes;
import com.clifton.product.manager.ProductManagerServiceImpl;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * Tests re-loading and processing LinkedManager type balances to ensure linked values remain correct
 * <p>
 * NOTE: When using a new backup you must update SKIP_MANAGER_ACCOUNTS and BALANCE_DATE to match latest backup
 * <p>
 * To figure out which Linked Managers to skip - locally on Balance History tab for the balance date - filter on all Linked Manager balances Select All and
 * click Reload (Note: ManagerBalanceProcessWindow has a UI selection limit of 50 - change that temporarily for this testing) Expose Create Date column to see
 * which balances changed and verify the change is good (because of change in underlying data) If the change is valid, skip that manager
 *
 * @author manderson
 */
public class PortfolioLinkedManagerBalanceLoadAndProcessDatabaseTests extends BaseIMSDatabaseTests {

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private InvestmentManagerAccountBalanceService investmentManagerAccountBalanceService;

	@Resource
	private ProductManagerServiceImpl productManagerService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * Underlying data has changed so result is slightly different - skip in tests
	 * These are generally market value (Cash or Total Market Value type) where there are transitions booked the next morning for previous day
	 * but the manager balance was already processed.
	 */
	private static final Set<String> SKIP_MANAGER_ACCOUNTS = CollectionUtils.createHashSet(
			"M06199",
			"M08317",
			"M08320",
			"M11159",
			"M11160",
			"M14979",
			"M15250",
			"M15254",
			"M15255",
			"M15256",
			"M15257",
			"M15258",
			"M15259",
			"M16619",
			"M17878",
			"M17923",
			"M18115",
			"M18163",
			"M18195",
			"M19496",
			"M20370",
			"M21117",
			"M23279",
			"M23426",
			"M23662",
			"M23663",
			"M23665",
			"M23666",
			"M23668",
			"M23669",
			"M24048",
			"M24049",
			"M24158",
			"M25140",
			"M25151",
			"M25343",
			"M25503",
			"M25713",
			"M25847",
			"M27392"
	);


	/**
	 * Whenever Database is restored, should update this to the last business day
	 * to ensure we are trying to reload balances that are still being loaded
	 * NOTE: Currently uses Last Business Day - 2 business days so that any changes
	 * do not affect PIOS tests
	 * Make sure not previous month end either
	 */
	private static final Date BALANCE_DATE = DateUtils.toDate("08/27/2020");

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testLatestManagerBalances() {
		List<String> successful = new ArrayList<>();
		List<String> failed = new ArrayList<>();

		InvestmentManagerAccountBalanceSearchForm searchForm = new InvestmentManagerAccountBalanceSearchForm();
		searchForm.setBalanceDate(BALANCE_DATE);
		searchForm.setManagerType(InvestmentManagerAccount.ManagerTypes.LINKED);
		searchForm.setViolationStatusNames(new String[]{RuleViolationStatus.RuleViolationStatusNames.PROCESSED_IGNORED.name(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS.name()});
		List<InvestmentManagerAccountBalance> balanceList = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceList(searchForm);

		ProductManagerBalanceProcessingConfig managerBalanceProcessingConfig = new ProductManagerBalanceProcessingConfig(BALANCE_DATE, this.calendarBusinessDayService);

		for (InvestmentManagerAccountBalance balance : CollectionUtils.getIterable(balanceList)) {
			InvestmentManagerAccount mgr = balance.getManagerAccount();
			if (SKIP_MANAGER_ACCOUNTS.contains(mgr.getAccountNumber())) {
				continue;
			}
			// These managers will be de-activated after linked types are de-activated.  For now, just skip any inactive linked types since they are no longer supported
			if (mgr.getLinkedManagerType().isInactive()) {
				continue;
			}
			BigDecimal originalCash = balance.getCashValue();
			BigDecimal adjustedCash = balance.getAdjustedCashValue();
			BigDecimal originalSecurities = balance.getSecuritiesValue();
			BigDecimal adjustedSecurities = balance.getAdjustedSecuritiesValue();
			try {
				// Reload Keeps any manual adjustments, so should be the same
				this.productManagerService.processProductManagerAccountBalance(mgr, ProductManagerBalanceProcessingTypes.RELOAD_PROCESS, managerBalanceProcessingConfig);

				InvestmentManagerAccountBalance newBalance = this.investmentManagerAccountBalanceService.getInvestmentManagerAccountBalanceByManagerAndDate(mgr.getId(), balance.getBalanceDate(),
						false);
				validateValue(originalCash, newBalance.getCashValue(), "Original Cash");
				validateValue(originalSecurities, newBalance.getSecuritiesValue(), "Original Securities");
				validateValue(adjustedCash, newBalance.getAdjustedCashValue(), "Adjusted Cash");
				validateValue(adjustedSecurities, newBalance.getAdjustedSecuritiesValue(), "Adjusted Securities");

				successful.add(newBalance.getManagerAccount().getAccountNumber());
			}
			catch (Exception e) {
				failed.add(mgr.getAccountNumber() + "(" + mgr.getLinkedManagerType().getLabel() + "): " + e.getMessage());
				e.printStackTrace();
			}
			catch (AssertionError e2) {
				failed.add(mgr.getAccountNumber() + "(" + mgr.getLinkedManagerType().getLabel() + "): " + e2.getMessage());
			}
		}
		LogUtils.warn(getClass(), "Successful Managers (" + successful.size() + "): " + StringUtils.collectionToCommaDelimitedString(successful));
		if (!failed.isEmpty()) {
			throw new RuntimeException("Linked Manager Account Loading/Processing Tests failed for " + failed.size() + " manager(s):\n" + StringUtils.collectionToDelimitedString(failed, "\n"));
		}
	}


	////////////////////////////////////////////////////////////////////////
	// Helper Methods
	////////////////////////////////////////////////////////////////////////


	private void validateValue(BigDecimal expected, BigDecimal actual, String fieldName) {
		BigDecimal percentThreshold = BigDecimal.ONE;
		// Allow more flexible threshold (for cases where we know a value may be slightly different (MV of Bonds used to include an extra day of accrual))
		BigDecimal percentChange = MathUtils.getPercentChange(expected, actual, true);
		if (MathUtils.isInRange(percentChange, MathUtils.negate(percentThreshold), percentThreshold)) {
			return;
		}
		String msg = "Error comparing value [" + CoreMathUtils.formatNumberMoney(expected) + "] to [" + CoreMathUtils.formatNumberMoney(actual) + "] for field [" + fieldName + "]"
				+ " Percent Change " + CoreMathUtils.formatNumberDecimal(percentChange) + " is outside of expected threshold of " + CoreMathUtils.formatNumberDecimal(percentThreshold);
		Assertions.fail(msg);
	}
}
