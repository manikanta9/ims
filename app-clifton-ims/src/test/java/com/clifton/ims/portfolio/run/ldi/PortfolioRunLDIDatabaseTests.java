package com.clifton.ims.portfolio.run.ldi;

import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.portfolio.run.BasePortfolioRunDatabaseTests;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposure;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIExposureKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccount;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIManagerAccountKeyRateDurationPoint;
import com.clifton.portfolio.run.ldi.PortfolioRunLDIService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;


/**
 * @author manderson
 */
public class PortfolioRunLDIDatabaseTests extends BasePortfolioRunDatabaseTests {

	@Resource
	private PortfolioRunLDIService portfolioRunLDIService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static final Set<String> EXCLUDE_ALLOCATION_HISTORY_ID_CLIENT_ACCOUNTS = CollectionUtils.createHashSet();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTestAccountGroupName() {
		return "LDI Account Tests";
	}


	@Override
	public boolean isRebalanceCheck() {
		return false;
	}


	@Override
	public void validateRunDetails(PortfolioRun run, PortfolioRun newRun) {
		validatePortfolioRunLDIManagerAccountList(run, newRun);
		validatePortfolioRunLDIExposureList(run, newRun);
		validatePortfolioRunLDIPointList(run, newRun);
		validatePortfolioRunMargin(run, newRun);
	}


	/**
	 * Validates each manager account and all of their KRD point details
	 */
	private void validatePortfolioRunLDIManagerAccountList(PortfolioRun run, PortfolioRun newRun) {
		List<PortfolioRunLDIManagerAccount> managerAccountList = this.portfolioRunLDIService.getPortfolioRunLDIManagerAccountListByRun(run.getId(), true);
		List<PortfolioRunLDIManagerAccount> newManagerAccountList = this.portfolioRunLDIService.getPortfolioRunLDIManagerAccountListByRun(newRun.getId(), true);
		validateListSizesEqual("Manager Account", managerAccountList, newManagerAccountList);

		for (PortfolioRunLDIManagerAccount managerAccount : CollectionUtils.getIterable(managerAccountList)) {
			for (PortfolioRunLDIManagerAccount newManagerAccount : CollectionUtils.getIterable(newManagerAccountList)) {
				if (managerAccount.getManagerAccountAssignment().equals(newManagerAccount.getManagerAccountAssignment())) {
					validateBeanPropertiesEqual(managerAccount, newManagerAccount, managerAccount.getLabel(), null, null);
					validateListSizesEqual("Manager [" + managerAccount.getLabel() + "] KRD Points ", managerAccount.getKeyRateDurationPointList(), newManagerAccount.getKeyRateDurationPointList());
					for (PortfolioRunLDIManagerAccountKeyRateDurationPoint managerPoint : CollectionUtils.getIterable(managerAccount.getKeyRateDurationPointList())) {
						for (PortfolioRunLDIManagerAccountKeyRateDurationPoint newManagerPoint : CollectionUtils.getIterable(newManagerAccount.getKeyRateDurationPointList())) {
							if (managerPoint.getKeyRateDurationPoint().equals(newManagerPoint.getKeyRateDurationPoint())) {
								validateBeanPropertiesEqual(managerPoint, newManagerPoint, managerPoint.getLabel(), null, null);
								break;
							}
						}
					}
					break;
				}
			}
		}
	}


	private void validatePortfolioRunLDIExposureList(PortfolioRun run, PortfolioRun newRun) {
		List<PortfolioRunLDIExposure> list = this.portfolioRunLDIService.getPortfolioRunLDIExposureListByRun(run.getId(), true);
		List<PortfolioRunLDIExposure> newList = this.portfolioRunLDIService.getPortfolioRunLDIExposureListByRun(newRun.getId(), true);
		validateListSizesEqual("Exposure", list, newList);

		for (PortfolioRunLDIExposure bean : CollectionUtils.getIterable(list)) {
			for (PortfolioRunLDIExposure newBean : CollectionUtils.getIterable(newList)) {
				if (bean.getTradingClientAccount().equals(newBean.getTradingClientAccount()) && bean.getSecurity().equals(newBean.getSecurity())) {
					validateBeanPropertiesEqual(bean, newBean, bean.getLabel(), null, getExcludeExposurePropertiesForAccount(run.getClientInvestmentAccount().getNumber()));
					validateListSizesEqual("Exposure [" + bean.getTradingClientAccount().getLabel() + " - " + bean.getLabel() + "] KRD Points ", bean.getKeyRateDurationPointList(), bean.getKeyRateDurationPointList());
					for (PortfolioRunLDIExposureKeyRateDurationPoint point : CollectionUtils.getIterable(bean.getKeyRateDurationPointList())) {
						for (PortfolioRunLDIExposureKeyRateDurationPoint newPoint : CollectionUtils.getIterable(newBean.getKeyRateDurationPointList())) {
							if (point.getKeyRateDurationPoint().equals(newPoint.getKeyRateDurationPoint())) {
								validateBeanPropertiesEqual(point, newPoint, point.getLabel(), null, null);
								break;
							}
						}
					}
					break;
				}
			}
		}
	}


	private String[] getExcludeExposurePropertiesForAccount(String accountNumber) {
		if (EXCLUDE_ALLOCATION_HISTORY_ID_CLIENT_ACCOUNTS.contains(accountNumber)) {
			return new String[]{"replicationAllocationHistory"};
		}
		return null;
	}


	private void validatePortfolioRunLDIPointList(PortfolioRun run, PortfolioRun newRun) {
		List<PortfolioRunLDIKeyRateDurationPoint> list = this.portfolioRunLDIService.getPortfolioRunLDIKeyRateDurationPointListByRun(run.getId(), false);
		List<PortfolioRunLDIKeyRateDurationPoint> newList = this.portfolioRunLDIService.getPortfolioRunLDIKeyRateDurationPointListByRun(newRun.getId(), false);
		validateListSizesEqual("KRD Point", list, newList);

		for (PortfolioRunLDIKeyRateDurationPoint bean : CollectionUtils.getIterable(list)) {
			for (PortfolioRunLDIKeyRateDurationPoint newBean : CollectionUtils.getIterable(newList)) {
				if (bean.getKeyRateDurationPoint().equals(newBean.getKeyRateDurationPoint())) {
					validateBeanPropertiesEqual(bean, newBean, bean.getLabel(), null, null);
					break;
				}
			}
		}
	}
}
