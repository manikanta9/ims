package com.clifton.ims.portfolio.run.overlay;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.portfolio.run.BasePortfolioRunDatabaseTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassSecurityAllowance;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.product.overlay.ProductOverlayAssetClass;
import com.clifton.product.overlay.ProductOverlayAssetClassReplication;
import com.clifton.product.overlay.ProductOverlayManagerAccount;
import com.clifton.product.overlay.ProductOverlayManagerAccountRebalance;
import com.clifton.product.overlay.ProductOverlayService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
public class PortfolioRunOverlayDatabaseTests extends BasePortfolioRunDatabaseTests {

	@Resource
	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	@Resource
	private InvestmentAccountRebalanceService investmentAccountRebalanceService;

	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private ProductOverlayService productOverlayService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	// Accounts we'll exclude from the security allowance/contract splitting test.  The actual results on the run may not match what could be possible.
	// For example if target and actual are zero one of the replications may be removed from the run
	private static final Set<String> EXCLUDE_ACCOUNTS_ASSET_CLASS_SECURITY_ALLOWANCE_TEST = CollectionUtils.createHashSet(
			"424000",
			"604600",
			"664110",
			"385399",
			"297502",
			"297552",
			"391321",
			"900400",
			"800070",
			"900150",
			"668471",
			"057020",
			"550001",
			"385451",
			"664115",
			"400100"
	);


	private static final Set<String> EXCLUDE_ALLOCATION_HISTORY_ID_CLIENT_ACCOUNTS = CollectionUtils.createHashSet(
			// "664151",
			// "158806",
			// "393001"
	);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Additional test that takes the same existing runs and confirms for each run replication
	 * the security passes as valid for the Asset Class Replication.  Backwards test to confirm the validation works
	 * and correctly finds cases where contract splitting can apply.
	 */
	@Test
	public void testInvestmentAccountAssetClassSecurityAllowance() {
		InvestmentAccount[] accounts = getInvestmentAccountList();

		List<String> successful = new ArrayList<>();
		List<String> failed = new ArrayList<>();
		for (InvestmentAccount account : accounts) {
			PortfolioRun run = getLatestRunForAccount(account);
			try {
				// Validate Results
				if (!EXCLUDE_ACCOUNTS_ASSET_CLASS_SECURITY_ALLOWANCE_TEST.contains(run.getClientInvestmentAccount().getNumber())) {
					validateInvestmentAccountAssetClassSecurityAllowance(run, this.productOverlayService.getProductOverlayAssetClassReplicationListByRun(run.getId()));
					successful.add(run.getClientInvestmentAccount().getNumber());
				}
			}
			catch (Throwable e) {
				LogUtils.error(LogCommand.ofThrowable(getClass(), e));
				failed.add(StringUtils.NEW_LINE + run.getClientInvestmentAccount().getNumber() + ": " + e.getMessage());
			}
		}

		LogUtils.warn(getClass(), "Successful Accounts (" + successful.size() + "): " + StringUtils.collectionToCommaDelimitedString(successful));
		if (!failed.isEmpty()) {
			LogUtils.warn(LogCommand.ofMessageSupplier(getClass(), () -> "Failed Accounts: " + StringUtils.collectionToCommaDelimitedString(failed)));
			throw new RuntimeException("Investment Account Asset Class Allowance test failed for " + failed.size() + " accounts: " + failed.toString());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTestAccountGroupName() {
		return "Overlay Account Tests";
	}


	@Override
	public boolean isRebalanceCheck() {
		return true;
	}


	@Override
	public void validateRunDetails(PortfolioRun run, PortfolioRun newRun) {
		validateRunManagerProcessing(run, newRun);
		validateRunManagerRebalanceProcessing(run, newRun);
		validateRunAssetClassProcessing(run, newRun);
		validateRunReplicationProcessing(run, newRun);
		validateRunCurrencyOther(run, newRun);
		validatePortfolioRunMargin(run, newRun);
	}


	private void validateRunManagerProcessing(PortfolioRun run, PortfolioRun newRun) {
		List<ProductOverlayManagerAccount> runList = this.productOverlayService.getProductOverlayManagerAccountListByRun(run.getId());
		List<ProductOverlayManagerAccount> newRunList = this.productOverlayService.getProductOverlayManagerAccountListByRun(newRun.getId());

		validateListSizesEqual("Manager Account", runList, newRunList);

		for (ProductOverlayManagerAccount bean : CollectionUtils.getIterable(runList)) {
			for (ProductOverlayManagerAccount newBean : CollectionUtils.getIterable(newRunList)) {
				if (bean.getAccountAssetClass().equals(newBean.getAccountAssetClass())) {
					if (bean.getManagerAccountAssignment().equals(newBean.getManagerAccountAssignment())) {
						validateBeanPropertiesEqual(bean, newBean, bean.getLabel(), new String[]{"assetClassLabelExpanded"}, null);
						break;
					}
				}
			}
		}
	}


	private void validateRunManagerRebalanceProcessing(PortfolioRun run, PortfolioRun newRun) {
		List<ProductOverlayManagerAccountRebalance> runList = this.productOverlayService.getProductOverlayManagerAccountRebalanceListByRun(run.getId());
		List<ProductOverlayManagerAccountRebalance> newRunList = this.productOverlayService.getProductOverlayManagerAccountRebalanceListByRun(newRun.getId());

		validateListSizesEqual("Manager Account Rebalance ", runList, newRunList);

		for (ProductOverlayManagerAccountRebalance ma : CollectionUtils.getIterable(runList)) {
			for (ProductOverlayManagerAccountRebalance newMa : CollectionUtils.getIterable(newRunList)) {
				if (ma.getManagerAccountGroup().equals(newMa.getManagerAccountGroup())) {
					if (ma.getManagerAccount().equals(newMa.getManagerAccount())) {
						validateBeanPropertiesEqual(ma, newMa, ma.getManagerAccountGroup().getName() + " - " + ma.getManagerAccount().getLabel(), null, null);
						break;
					}
				}
			}
		}
	}


	private void validateRunAssetClassProcessing(PortfolioRun run, PortfolioRun newRun) {
		List<ProductOverlayAssetClass> runList = this.productOverlayService.getProductOverlayAssetClassListByRun(run.getId());
		List<ProductOverlayAssetClass> newRunList = this.productOverlayService.getProductOverlayAssetClassListByRun(newRun.getId());

		String[] excludes = null;

		// If minimize imbalances will validate cash total, not individual buckets
		// because of attribution changes in PERFORMANC-278 and not sure at this point if we can auto fix history
		InvestmentAccountRebalance accountRebalance = this.investmentAccountRebalanceService.getInvestmentAccountRebalance(run.getClientInvestmentAccount().getId());
		boolean minimizeImbalances = (accountRebalance != null && accountRebalance.getMinimizeImbalancesCalculatorBean() != null);

		if (minimizeImbalances) {
			excludes = new String[]{"managerCash", "fundCash", "transitionCash", "rebalanceCash", "rebalanceCashAdjusted", "rebalanceAttributionCash"};
		}

		validateListSizesEqual("Asset Class ", runList, newRunList);
		for (ProductOverlayAssetClass b : CollectionUtils.getIterable(runList)) {
			for (ProductOverlayAssetClass b2 : CollectionUtils.getIterable(newRunList)) {
				if (b.getAccountAssetClass().equals(b2.getAccountAssetClass())) {
					validateBeanPropertiesEqual(b, b2, b.getLabel(), new String[]{"labelExpanded"}, excludes);
					if (minimizeImbalances) {
						String msgPrefix = "ProductOverlayAssetClass - " + b.getLabel() + ": Property [Cash Total] (Note excluded specific cash buckets because uses Minimize Imbalances)";
						validateBeanPropertyEqual(b, b2, msgPrefix, "cashTotal", DataTypes.MONEY);
					}
					break;
				}
			}
		}
	}


	private void validateRunReplicationProcessing(PortfolioRun run, PortfolioRun newRun) {
		List<ProductOverlayAssetClassReplication> runList = this.productOverlayService.getProductOverlayAssetClassReplicationListByRun(run.getId());
		List<ProductOverlayAssetClassReplication> newRunList = this.productOverlayService.getProductOverlayAssetClassReplicationListByRun(newRun.getId());

		validateListSizesEqual("Asset Class Replication", runList, newRunList);


		for (ProductOverlayAssetClassReplication b : CollectionUtils.getIterable(runList)) {
			ProductOverlayAssetClassReplication b2 = findReplicationInNewList(b, newRunList); // Throws an exception if not found
			String[] ignoreReplicationFieldsForAccount = getIgnoreReplicationFieldsForAccount(run.getClientInvestmentAccount().getNumber(), b2);
			validateBeanPropertiesEqual(b, b2, b.getLabel(), null, ignoreReplicationFieldsForAccount);
			break;
		}
	}


	private ProductOverlayAssetClassReplication findReplicationInNewList(ProductOverlayAssetClassReplication existingReplication, List<ProductOverlayAssetClassReplication> newReplicationList) {
		List<ProductOverlayAssetClassReplication> filteredList = CollectionUtils.getStream(newReplicationList)
				.filter(replication -> replication.getAccountAssetClass().equals(existingReplication.getAccountAssetClass())
						&& replication.getReplication().equals(existingReplication.getReplication())
						&& replication.getSecurity().equals(existingReplication.getSecurity())
				).collect(Collectors.toList());
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(filteredList), "Did not find " + existingReplication.getLabel() + " on new run.");
		// 336020 example on 8/23
		if (CollectionUtils.getSize(filteredList) > 1) {
			// Try By Order Field
			filteredList = BeanUtils.filter(filteredList, newReplication -> MathUtils.isEqual(newReplication.getOrder(), existingReplication.getOrder()));
		}
		// If still more than one use target exposure adjusted 800025 on 8/23
		if (CollectionUtils.getSize(filteredList) > 1) {
			filteredList = BeanUtils.filter(filteredList, newReplication -> MathUtils.isEqual(newReplication.getTargetExposureAdjusted(), existingReplication.getTargetExposureAdjusted()));
		}
		// If still more than one or NONE throw exception...
		ValidationUtils.assertTrue(CollectionUtils.getSize(filteredList) == 1, "Did not find " + existingReplication.getLabel() + " on new run. Found potential matches, but didn't additionally filter on order and target adjusted to get one unique result.");
		return filteredList.get(0);
	}


	private String[] getIgnoreReplicationFieldsForAccount(String accountNumber, ProductOverlayAssetClassReplication newReplication) {
		if (EXCLUDE_ALLOCATION_HISTORY_ID_CLIENT_ACCOUNTS.contains(accountNumber) || newReplication.getReplication().isDynamic()) {
			return new String[]{"replicationAllocationHistory"};
		}
		return null;
	}


	private void validateInvestmentAccountAssetClassSecurityAllowance(PortfolioRun portfolioRun, List<ProductOverlayAssetClassReplication> replicationList) {
		Map<InvestmentSecurity, InvestmentAccountAssetClassSecurityAllowance> securityAllowanceMap = new HashMap<>();
		Map<InvestmentSecurity, List<ProductOverlayAssetClassReplication>> securityReplicationListMap = BeanUtils.getBeansMap(replicationList, ProductOverlayAssetClassReplication::getSecurity);
		for (Map.Entry<InvestmentSecurity, List<ProductOverlayAssetClassReplication>> investmentSecurityListEntry : securityReplicationListMap.entrySet()) {
			InvestmentAccountAssetClassSecurityAllowance securityAllowance = securityAllowanceMap.get(investmentSecurityListEntry.getKey());
			if (securityAllowance == null) {
				// THIS IS AN ISSUE FOR ACCOUNTS WITH DYNAMIC REPLICATIONS THE NEW ALLOCATION IS BEING INSERTED, BUT THE UPDATE TO END THE EXISTING ISN'T MAKING IT TO THE DATABASE
				// BY USING EXECUTE WITH POST FLUSH ENABLED THAT UPDATE IS FORCIBLY PUSHED TO THE DATABASE
				securityAllowance = DaoUtils.executeWithPostUpdateFlushEnabled(() -> this.investmentAccountAssetClassService.getInvestmentAccountAssetClassSecurityAllowance(portfolioRun.getClientInvestmentAccount().getId(), (investmentSecurityListEntry.getKey()).getId(), portfolioRun.getBalanceDate()));
				securityAllowanceMap.put(investmentSecurityListEntry.getKey(), securityAllowance);
			}
			Map<Boolean, List<ProductOverlayAssetClassReplication>> inclusionReplicationMap = BeanUtils.getBeansMap(investmentSecurityListEntry.getValue(), productOverlayAssetClassReplication -> !productOverlayAssetClassReplication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount());
			List<ProductOverlayAssetClassReplication> includeReplicationList = inclusionReplicationMap.get(Boolean.TRUE);
			List<ProductOverlayAssetClassReplication> excludeReplicationList = inclusionReplicationMap.get(Boolean.FALSE);

			for (ProductOverlayAssetClassReplication runReplication : CollectionUtils.getIterable(investmentSecurityListEntry.getValue())) {
				String messagePrefix = "Run: " + portfolioRun.getLabel() + ", Security: " + (investmentSecurityListEntry.getKey()).getSymbol() + ", Asset Class: " + runReplication.getOverlayAssetClass().getLabel() + " , Replication: " + runReplication.getReplication().getName();
				if (!securityAllowance.isValidSelection(runReplication.getOverlayAssetClass().getAccountAssetClass(), runReplication.getReplication())) {
					throw new ValidationException(messagePrefix + " is not considered to be a valid security allowance but it should be.");
				}
				boolean expectedSplit = CollectionUtils.getSize((runReplication.getOverlayAssetClass().getAccountAssetClass().isReplicationPositionExcludedFromCount()) ? excludeReplicationList : includeReplicationList) >= 2;
				if (expectedSplit != securityAllowance.isSecuritySplit(runReplication.getOverlayAssetClass().getAccountAssetClass())) {
					Assertions.fail(messagePrefix + " expected " + (expectedSplit ? "to be available" : "not to be available") + " for contract splitting but it " + (expectedSplit ? "wasn't." : "was."));
				}
			}
		}
	}


	private void validateRunCurrencyOther(PortfolioRun run, PortfolioRun newRun) {
		List<PortfolioCurrencyOther> runList = this.portfolioRunService.getPortfolioCurrencyOtherListByRun(run.getId());
		List<PortfolioCurrencyOther> newRunList = this.portfolioRunService.getPortfolioCurrencyOtherListByRun(newRun.getId());

		validateListSizesEqual("Currency Other", runList, newRunList);

		for (PortfolioCurrencyOther b : CollectionUtils.getIterable(runList)) {
			for (PortfolioCurrencyOther b2 : CollectionUtils.getIterable(newRunList)) {
				if (b.getCurrencySecurity().equals(b2.getCurrencySecurity())) {
					validateBeanPropertiesEqual(b, b2, b.getCurrencySecurity().getSymbol(), null, null);
					break;
				}
			}
		}
	}
}
