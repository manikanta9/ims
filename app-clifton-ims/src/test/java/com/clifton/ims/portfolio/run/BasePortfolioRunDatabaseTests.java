package com.clifton.ims.portfolio.run;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.ims.BaseIMSDatabaseTests;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalance;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceHistory;
import com.clifton.investment.account.assetclass.rebalance.InvestmentAccountRebalanceService;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import com.clifton.portfolio.run.margin.PortfolioRunMargin;
import com.clifton.portfolio.run.margin.PortfolioRunMarginService;
import com.clifton.portfolio.run.process.PortfolioRunProcessService;
import com.clifton.portfolio.run.search.PortfolioRunSearchForm;
import com.clifton.rule.definition.RuleDefinition;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.rule.violation.search.RuleViolationSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * @author manderson
 */
public abstract class BasePortfolioRunDatabaseTests extends BaseIMSDatabaseTests {

	protected final static PropertyDataTypeConverter PROPERTY_DATA_TYPE_CONVERTER = new PropertyDataTypeConverter();

	@Resource
	private DaoLocator daoLocator;

	@Resource
	private PortfolioRunProcessService portfolioRunProcessService;

	@Resource
	private InvestmentAccountGroupService investmentAccountGroupService;

	@Resource
	private InvestmentAccountRebalanceService investmentAccountRebalanceService;

	@Resource
	private PortfolioRunService portfolioRunService;

	@Resource
	private PortfolioRunMarginService portfolioRunMarginService;

	@Resource
	private RuleViolationService ruleViolationService;


	///////////////////////////////////////////////////////////////////////////


	/**
	 * Note: Set up of test account groups is in additional SQL that is run during restore db process
	 */
	public abstract String getTestAccountGroupName();


	/**
	 * Set to true for Overlay only which if the last run had an automatic rebalance history record performed last, it will undo it
	 * so the test will also automatically trigger that same rebalance and validate
	 */
	public abstract boolean isRebalanceCheck();


	/**
	 * Override with validation of supporting tables details to the portfolio run.  Each run object can use different supporting tables
	 * The actual {@link PortfolioRun} object is validated in this base class after all the details are validated
	 */
	public abstract void validateRunDetails(PortfolioRun run, PortfolioRun newRun);


	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testLatestRuns() {
		beforeTestLatestRuns();
		InvestmentAccount[] accounts = getInvestmentAccountList();

		List<String> successful = new ArrayList<>();
		List<String> failed = new ArrayList<>();
		for (InvestmentAccount account : accounts) {
			PortfolioRun run = getLatestRunForAccount(account);
			PortfolioRun newRun;
			// Copy the Run & Process It
			try {
				newRun = copyAndProcessRun(run);
				// Validate Results are the Same
				validateRun(run, newRun);
				successful.add(run.getClientInvestmentAccount().getNumber());
			}
			catch (Exception e) {
				failed.add(StringUtils.NEW_LINE + run.getClientInvestmentAccount().getNumber() + ": " + e.getMessage());
				e.printStackTrace();
			}
			catch (AssertionError e2) {
				failed.add(StringUtils.NEW_LINE + run.getClientInvestmentAccount().getNumber() + ": " + e2.getMessage());
			}
		}
		LogUtils.warn(getClass(), "Successful Accounts (" + successful.size() + "): " + StringUtils.collectionToCommaDelimitedString(successful));
		if (!failed.isEmpty()) {
			LogUtils.warn(LogCommand.ofMessageSupplier(getClass(), () -> "Failed Accounts: " + StringUtils.collectionToCommaDelimitedString(failed)));
			throw new RuntimeException(getClass().getName() + " failed for " + failed.size() + " accounts: " + failed.toString());
		}
	}


	protected void beforeTestLatestRuns() {
		// DO NOTHING
		// CURRENTLY OVERRIDDEN FOR OVERLAY TESTS TO RE-ADJUST SOME OF THE REPLICATION ALLOCATION OPTIONS DUE TO RE-FACTORING
	}

	//////////////////////////////////////////////////////////////////
	////////      Look Up Helper Methods
	//////////////////////////////////////////////////////////////////


	protected InvestmentAccount[] getInvestmentAccountList() {
		InvestmentAccountGroup group = this.investmentAccountGroupService.getInvestmentAccountGroupByName(getTestAccountGroupName());
		if (group == null) {
			throw new RuntimeException("Missing Test Account Group Named: " + getTestAccountGroupName());
		}
		InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
		searchForm.setGroupId(group.getId());
		List<InvestmentAccountGroupAccount> groupAccountList = this.investmentAccountGroupService.getInvestmentAccountGroupAccountList(searchForm);
		if (CollectionUtils.isEmpty(groupAccountList)) {
			throw new RuntimeException("No accounts set up for Test Account Group: " + getTestAccountGroupName());
		}
		return BeanUtils.getPropertyValues(groupAccountList, "referenceTwo", InvestmentAccount.class);
	}


	protected PortfolioRun getLatestRunForAccount(InvestmentAccount account) {
		PortfolioRunSearchForm runSearchForm = new PortfolioRunSearchForm();
		runSearchForm.setClientInvestmentAccountId(account.getId());
		runSearchForm.setExcludeWorkflowStatusName(WorkflowStatus.STATUS_CLOSED); // Want the latest active one - not created by the test "SystemUser" (id = 1)
		runSearchForm.addSearchRestriction(new SearchRestriction("createUserId", ComparisonConditions.NOT_EQUALS, 1));

		runSearchForm.setOrderBy("balanceDate:desc#updateDate:desc");
		PortfolioRun run = CollectionUtils.getFirstElement(this.portfolioRunService.getPortfolioRunList(runSearchForm));
		ValidationUtils.assertNotNull(account, "Cannot find a run for account # [" + account.getNumber() + "].");
		return run;
	}


	//////////////////////////////////////////////////////////////////
	////////      Processing Helper Methods
	//////////////////////////////////////////////////////////////////


	private PortfolioRun copyAndProcessRun(PortfolioRun run) {
		PortfolioRun newRun = new PortfolioRun();
		newRun.setClientInvestmentAccount(run.getClientInvestmentAccount());
		newRun.setBalanceDate(run.getBalanceDate());
		newRun.setMarketOnCloseAdjustmentsIncluded(run.isMarketOnCloseAdjustmentsIncluded());

		// NOTE: ONLY APPLIES TO OVERLAY
		if (isRebalanceCheck()) {
			// If last rebalance was on the same date and was automatic - undo it so the system will recalculate it
			InvestmentAccountRebalance acctRebal = this.investmentAccountRebalanceService.getInvestmentAccountRebalance(run.getClientInvestmentAccount().getId());
			if (acctRebal != null && acctRebal.isAutomatic()) {
				InvestmentAccountRebalanceHistory history = this.investmentAccountRebalanceService.getInvestmentAccountRebalanceHistoryRecent(run.getClientInvestmentAccount().getId(),
						run.getBalanceDate());
				if (history != null && DateUtils.compare(run.getBalanceDate(), history.getRebalanceDate(), false) == 0 && "Automatic Rebalancing".equals(history.getDescription())) {
					this.investmentAccountRebalanceService.deleteInvestmentAccountRebalanceHistory(history.getId());
				}
			}
		}

		this.portfolioRunService.savePortfolioRun(newRun);

		try {
			this.portfolioRunProcessService.processPortfolioRun(newRun.getId(), false, null);
		}
		catch (ValidationException e) {
			// Do Nothing - Validation Exceptions are created as warnings, so as long as warnings match, everything is fine
		}
		return this.portfolioRunService.getPortfolioRun(newRun.getId());
	}


	//////////////////////////////////////////////////////////////////
	////////      Validation Helper Methods
	//////////////////////////////////////////////////////////////////


	private void validateRun(PortfolioRun run, PortfolioRun newRun) {
		validateRunDetails(run, newRun);

		// Validate Main Run Properties
		validateRunProperties(run, newRun);

		// Validate Rule Violations
		validateRuleViolations(run, newRun);
	}


	private void validateRunProperties(PortfolioRun run, PortfolioRun newRun) {
		// Calculated Getters - Based on initial processing - we know:
		Assertions.assertFalse(newRun.isClosed(), "Run should not be flagged as closed.");
		Assertions.assertTrue(newRun.isProcessingAllowed(), "Run should allow re-processing.");
		Assertions.assertFalse(newRun.isSubmittedToTrading(), "Run should not be submitted to trading");
		Assertions.assertFalse(newRun.isTradingAllowed(), "Run should not allow trading");

		// If they both failed - both should be same for incomplete
		// And should have failed during the same step
		Assertions.assertEquals(run.isIncomplete(), newRun.isIncomplete());
		if (run.isIncomplete()) {
			Assertions.assertEquals(run.getWorkflowState().getName(), newRun.getWorkflowState().getName());
		}

		validateBeanPropertiesEqual(run, newRun, run.getLabel(), null, new String[]{"mainRun", "workflowState", "workflowStatus", "violationStatus", "notes", "documentFileCount", "postedToPortal"});
	}


	private void validateRuleViolations(PortfolioRun run, PortfolioRun newRun) {
		if (this.investmentAccountGroupService.isInvestmentAccountInGroup(run.getClientInvestmentAccount().getId(), "Ignore Rule Violations")) {
			return;
		}
		List<RuleViolation> existingViolations = getPortfolioRunRuleViolationList(run.getId());
		List<RuleViolation> newViolations = getPortfolioRunRuleViolationList(newRun.getId());

		if (CollectionUtils.isEmpty(existingViolations) && CollectionUtils.isEmpty(newViolations)) {
			return;
		}

		Map<RuleDefinition, List<RuleViolation>> existingViolationMap = BeanUtils.getBeansMap(existingViolations, rv -> rv.getRuleAssignment().getRuleDefinition());
		Map<RuleDefinition, List<RuleViolation>> newViolationMap = BeanUtils.getBeansMap(newViolations, rv -> rv.getRuleAssignment().getRuleDefinition());

		int errorCount = 0;
		StringBuilder errors = new StringBuilder(16);
		for (Map.Entry<RuleDefinition, List<RuleViolation>> ruleDefinitionListEntry : existingViolationMap.entrySet()) {
			if (isIgnoreRuleDefinition(run, ruleDefinitionListEntry.getKey(), true)) {
				continue;
			}

			List<RuleViolation> existingList = ruleDefinitionListEntry.getValue();
			List<RuleViolation> newList = newViolationMap.get(ruleDefinitionListEntry.getKey());

			if (CollectionUtils.getSize(existingList) != CollectionUtils.getSize(newList)) {
				errorCount++;
				errors.append("Expected [").append(CollectionUtils.getSize(existingList)).append("] violations for definition [").append((ruleDefinitionListEntry.getKey()).getName()).append("] but new run has [").append(CollectionUtils.getSize(newList)).append("]: ");
				for (RuleViolation violation : CollectionUtils.getIterable(existingList)) {
					errors.append("Violation On Existing Run: ").append(violation.getViolationNote()).append(StringUtils.NEW_LINE);
				}
				for (RuleViolation violation : CollectionUtils.getIterable(newList)) {
					errors.append("Violation On New Run: ").append(violation.getViolationNote()).append(StringUtils.NEW_LINE);
				}
			}
		}
		for (Map.Entry<RuleDefinition, List<RuleViolation>> ruleDefinitionListEntry : newViolationMap.entrySet()) {
			if (isIgnoreRuleDefinition(run, ruleDefinitionListEntry.getKey(), false)) {
				continue;
			}

			List<RuleViolation> existingList = existingViolationMap.get(ruleDefinitionListEntry.getKey());
			// Only Check New Where Existing is Empty
			if (CollectionUtils.isEmpty(existingList)) {
				List<RuleViolation> newList = ruleDefinitionListEntry.getValue();
				if (CollectionUtils.getSize(existingList) != CollectionUtils.getSize(newList)) {
					errorCount++;
					errors.append("Expected [").append(CollectionUtils.getSize(existingList)).append("] violations for definition [").append((ruleDefinitionListEntry.getKey()).getName()).append("] but new run has [").append(CollectionUtils.getSize(newList)).append("]: ").append(StringUtils.NEW_LINE);
					for (RuleViolation violation : CollectionUtils.getIterable(existingList)) {
						errors.append(StringUtils.TAB).append("Violation On Existing Run: ").append(violation.getViolationNote()).append(StringUtils.NEW_LINE);
					}
					for (RuleViolation violation : CollectionUtils.getIterable(newList)) {
						errors.append(StringUtils.TAB).append("Violation On New Run: ").append(violation.getViolationNote()).append(StringUtils.NEW_LINE);
					}
				}
			}
		}
		Assertions.assertEquals(0, errorCount, errors.toString());
	}


	private boolean isIgnoreRuleDefinition(PortfolioRun run, RuleDefinition definition, boolean existingRun) {
		// New Rule Definitions Added
		if (DateUtils.compare(definition.getCreateDate(), DateUtils.addDays(run.getBalanceDate(), 1), false) > 0) {
			return true;
		}
		if (StringUtils.isEqualIgnoreCase("Asset Class: Changed Since Previous Run", definition.getName())) {
			return true;
		}
		if (StringUtils.isEqualIgnoreCase("Cash Dividend Payment - Ex Date", definition.getName())) {
			// Uses Today's Date so Results will never be the same
			return true;
		}
		// The accounts might not have been reconciled when the run was originally processed, but were by the time the back up was done
		// So we only ignore if the violation was on the existing run, not if it suddenly started generated when it wasn't there before
		if (existingRun && StringUtils.isEqualIgnoreCase("Unreconciled Position Quantity", definition.getName())) {
			return true;
		}
		if (StringUtils.isEqualIgnoreCase("Account With Unapproved Events", definition.getName())) {
			return true;
		}
		return false;
	}


	private List<RuleViolation> getPortfolioRunRuleViolationList(int runId) {
		RuleViolationSearchForm searchForm = new RuleViolationSearchForm();
		searchForm.setLinkedTableName(PortfolioRun.TABLE_PORTFOLIO_RUN);
		searchForm.setLinkedFkFieldId(MathUtils.getNumberAsLong(runId));
		searchForm.setManual(false);
		return this.ruleViolationService.getRuleViolationList(searchForm);
	}


	protected void validateListSizesEqual(String prefix, List<?> originalList, List<?> newList) {
		Assertions.assertEquals(CollectionUtils.getSize(originalList), CollectionUtils.getSize(newList), prefix + " List Size");
	}


	/**
	 * Validate all field values are the same - with the selected properties as excluded Uses DAO Configuration Column List to pull properties to validate (not
	 * necessary to validate calculated getters as they will always match if underlying data is the same) Will Automatically exclude columns that are FK to
	 * other run tables, db calculated columns, id column and all system managed columns Can optionally include some calculated getter properties that aren't
	 * real columns on the table - useful for validating parent, where the run's would have different parent ids, but the label expanded should be equal
	 * <p>
	 * There is some leeway with BigDecimal comparisons - difference of 5 (or 0.01 if value data type is a percent) or percent change less than 0.01%
	 */
	protected void validateBeanPropertiesEqual(IdentityObject bean, IdentityObject bean2, String beanLabel, String[] additionalProperties, String[] additionalExcludeProperties) {
		ReadOnlyDAO<IdentityObject> dao = this.daoLocator.locate(bean);
		List<Column> columnList = dao.getConfiguration().getColumnList();

		Set<String> additionalExcludePropertySet = ArrayUtils.isEmpty(additionalExcludeProperties) ? Collections.emptySet() : CollectionUtils.createHashSet(additionalExcludeProperties);

		for (Column column : CollectionUtils.getIterable(columnList)) {
			if (isValidateColumn(column, additionalExcludePropertySet)) {
				String propertyName = column.getBeanPropertyName();
				String msgPrefix = dao.getConfiguration().getTableName() + " - " + beanLabel + ": Property [" + propertyName + "] ";
				validateBeanPropertyEqual(bean, bean2, msgPrefix, propertyName, column.getDataType());
			}
		}

		if (additionalProperties != null && additionalProperties.length > 0) {
			for (String propertyName : additionalProperties) {
				String msgPrefix = dao.getConfiguration().getTableName() + " - " + beanLabel + ": Property [" + propertyName + "] ";
				PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(bean.getClass(), propertyName);
				validateBeanPropertyEqual(bean, bean2, msgPrefix, propertyName, PROPERTY_DATA_TYPE_CONVERTER.convert(descriptor));
			}
		}
	}


	protected void validateBeanPropertyEqual(IdentityObject bean, IdentityObject bean2, String messagePrefix, String propertyName, DataTypes dataType) {
		Object value = BeanUtils.getPropertyValue(bean, propertyName);
		Object newValue = BeanUtils.getPropertyValue(bean2, propertyName);

		String msg = messagePrefix + "Value [" + value + "] doesn't match new value [" + newValue + "].";

		if (DataTypeNames.DECIMAL == dataType.getTypeName()) {
			BigDecimal v = (BigDecimal) value;
			BigDecimal nV = (BigDecimal) newValue;
			// If Either Value Is NULL Consider to be 0 for comparison checks
			if (v == null) {
				v = BigDecimal.ZERO;
			}
			if (nV == null) {
				nV = BigDecimal.ZERO;
			}

			// For BigDecimal checks - If field name contains weight or percent then difference should be < 0.01 difference
			// Otherwise - for amounts than within 0.01 % change of a difference
			BigDecimal difference = MathUtils.abs(MathUtils.subtract(v, nV));
			BigDecimal threshold = BigDecimal.valueOf(5);
			if (DataTypes.PERCENT == dataType || DataTypes.PERCENT_PRECISE == dataType) {
				threshold = BigDecimal.valueOf(0.02);
			}
			if (MathUtils.isLessThanOrEqual(difference, threshold)) {
				return;
			}
			// Otherwise if percent change between the two values is less then 0.01 percent - OK
			BigDecimal percentChange = MathUtils.getPercentChange(v, nV, true);
			if (MathUtils.isLessThanOrEqual(MathUtils.abs(percentChange), BigDecimal.valueOf(0.01))) {
				return;
			}
			// Change error message for numbers so it's displayed in a more friendly format
			msg = messagePrefix + "Value [" + CoreMathUtils.formatNumberMoney(v) + "] doesn't match new value [" + CoreMathUtils.formatNumberMoney(nV) + "]. Change is not within threshold of [" + CoreMathUtils.formatNumberDecimal(threshold) + "], and has a % change that exceeds 0.01 % ["
					+ CoreMathUtils.formatNumberMoney(percentChange) + "]";
			Assertions.fail(msg);
		}
		else if (DataTypeNames.DATE == dataType.getTypeName()) {
			Assertions.assertTrue(CompareUtils.isEqual(DateUtils.fromDateShort((Date) value), DateUtils.fromDateShort((Date) newValue)), msg);
		}
		else {
			Assertions.assertTrue(CompareUtils.isEqual(value, newValue), msg);
		}
	}


	protected void validatePortfolioRunMargin(PortfolioRun run, PortfolioRun newRun) {
		List<PortfolioRunMargin> list = this.portfolioRunMarginService.getPortfolioRunMarginListByRun(run.getId());
		List<PortfolioRunMargin> newList = this.portfolioRunMarginService.getPortfolioRunMarginListByRun(newRun.getId());

		validateListSizesEqual("Margin", list, newList);
		for (PortfolioRunMargin bean : CollectionUtils.getIterable(list)) {
			for (PortfolioRunMargin newBean : CollectionUtils.getIterable(newList)) {
				if (bean.getBrokerInvestmentAccount().equals(newBean.getBrokerInvestmentAccount())) {
					validateBeanPropertiesEqual(bean, newBean, bean.getLabel(), null, null);
				}
			}
		}
	}


	private boolean isValidateColumn(Column column, Set<String> additionalExcludeProperties) {
		String propertyName = column.getBeanPropertyName();
		if (additionalExcludeProperties != null && additionalExcludeProperties.contains(propertyName)) {
			return false;
		}
		if (StringUtils.isEqual("id", propertyName) || BeanUtils.isSystemManagedField(propertyName)) {
			return false;
		}
		if (column.isCalculatedColumn()) {
			return false;
		}
		if (!StringUtils.isEmpty(column.getFkTable())) {
			if (column.getFkTable().startsWith("PortfolioRun")) {
				return false;
			}
			if (column.getFkTable().startsWith("ProductOverlay")) {
				return false;
			}
		}
		return true;
	}
}
