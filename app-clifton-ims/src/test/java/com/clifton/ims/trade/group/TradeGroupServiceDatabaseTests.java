package com.clifton.ims.trade.group;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.ims.BaseIMSDatabaseTests;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.trade.group.TradeGroup;
import com.clifton.trade.group.TradeGroupService;
import com.clifton.trade.group.TradeGroupServiceImpl;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.group.search.TradeGroupSearchForm;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * <code>TradeGroupServiceDatabaseTests</code> is a unit test that uses a local IMS database.
 * <p>
 * There are nine tests that save TradeGroups/HierarchicalAware entities in different ways to verify
 * effective saving practice through Hibernate. The tests should pass with and without the use of the
 * IMS {@link com.clifton.core.dataaccess.dao.hibernate.DirtyCheckingStrategy}.
 *
 * @author NickK
 */
public class TradeGroupServiceDatabaseTests extends BaseIMSDatabaseTests {

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private TradeGroupService tradeGroupService;


	@Test
	public void testSaveTradeGroupParentAndChildListParentFirst() {
		assertSaveTradeGroupParentAndChildren(false, true, false);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentLast() {
		assertSaveTradeGroupParentAndChildren(false, false, false);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentFirstResettingParentOnSave() {
		assertSaveTradeGroupParentAndChildren(false, true, true);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentLastResettingParentOnSave() {
		assertSaveTradeGroupParentAndChildren(false, false, true);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentFirstTransactional() {
		assertSaveTradeGroupParentAndChildren(true, true, false);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentLastTransactional() {
		assertSaveTradeGroupParentAndChildren(true, false, false);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentFirstTransactionalResettingParentOnSave() {
		assertSaveTradeGroupParentAndChildren(true, true, true);
	}


	@Test
	public void testSaveTradeGroupParentAndChildListParentLastTransactionalResettingParentOnSave() {
		assertSaveTradeGroupParentAndChildren(true, false, true);
	}


	/**
	 * Saves a list of TradeGroup with groups referencing other TradeGroup in the list to be saved.
	 */
	@Test
	public void testSaveTradeGroupHierarchicalList() {
		TradeGroupType importTradeGroupType = this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.TRADE_IMPORT.getTypeName());
		SecurityUser currentUser = getTraderUser();
		List<TradeGroup> tradeGroupList = createHierarchicalTradeGroupList(importTradeGroupType, currentUser);
		saveTradeGroupList(tradeGroupList);
		for (TradeGroup saved : tradeGroupList) {
			TradeGroup found = this.tradeGroupService.getTradeGroup(saved.getId());
			Assertions.assertEquals(saved.getParent(), found.getParent());
			deleteTradeGroups(found);
		}
	}


	/**
	 * Save a parent TradeGroup and List of TradeGroups that reference the parent as parent.
	 * <p>
	 * Flags:
	 * <br>- transaction - execute the save of parent and children within same transaction (unit of work)
	 * <br>- saveParentFirst - save the parent before saving the children
	 * <br>- resetParentOnSave - save via the DAO returns the updated object, set the provided argument to
	 * the updated state post save. Not doing this can sometimes cause issues regarding stale data.
	 */
	private void assertSaveTradeGroupParentAndChildren(boolean transactional, boolean saveParentFirst, boolean resetParentOnSave) {
		TradeGroupType importTradeGroupType = this.tradeGroupService.getTradeGroupTypeByName(TradeGroupType.GroupTypes.TRADE_IMPORT.getTypeName());
		SecurityUser currentUser = getTraderUser();
		TradeGroup parent = newTradeGroup(importTradeGroupType, currentUser);
		List<TradeGroup> tradeGroupList = createChildTradeGroupList(parent);
		List<TradeGroup> foundList = null;
		try {
			parent = transactional ? saveTradeGroupParentAndChildrenTransactional(parent, tradeGroupList, saveParentFirst, resetParentOnSave) : saveTradeGroupParentAndChildren(parent, tradeGroupList, saveParentFirst, resetParentOnSave);
			foundList = getTradeGroupListByParent(parent);
			Assertions.assertTrue(foundList.size() == tradeGroupList.size(), "Not all children were found.");
		}
		catch (IllegalStateException e) {
			/*
			 * Due to Hibernate flushing, we were inconsistently getting the exception when transactional and !saveParentFirst.
			 * Thus, we cannot use the ExpectedException Rule and instead catch the exception and evaluate it.
			 */
			Assertions.assertFalse(saveParentFirst, "Received InvalidDataAccessApiUsageException when saveParentFirst is true.");
			Assertions.assertEquals("org.hibernate.TransientObjectException: object references an unsaved transient instance - save the transient instance before flushing: com.clifton.trade.group.TradeGroup", e.getMessage());
		}
		finally {
			TradeGroup[] toDelete;
			if (!CollectionUtils.isEmpty(foundList)) {
				toDelete = foundList.toArray(new TradeGroup[foundList.size()]);
			}
			else {
				toDelete = tradeGroupList.toArray(new TradeGroup[tradeGroupList.size()]);
			}
			try {
				deleteTradeGroups(ArrayUtils.add(toDelete, parent));
			}
			catch (Exception e) {
				/*
				 * We are cleaning up to avoid  erroneous TradeGroup.
				 * Ignore an exception to ensure test result is not masked by this.
				 */
			}
		}
	}


	private List<TradeGroup> createChildTradeGroupList(TradeGroup parent) {
		List<TradeGroup> tradeGroupList = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			TradeGroup group = newTradeGroup(parent.getTradeGroupType(), parent.getTraderUser());
			group.setParent(parent);
			tradeGroupList.add(group);
		}
		return tradeGroupList;
	}


	private List<TradeGroup> createHierarchicalTradeGroupList(TradeGroupType type, SecurityUser traderUser) {
		List<TradeGroup> tradeGroupList = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			TradeGroup group = newTradeGroup(type, traderUser);
			tradeGroupList.add(group);
		}
		tradeGroupList.get(0).setParent(tradeGroupList.get(3));
		tradeGroupList.get(1).setParent(tradeGroupList.get(3));
		tradeGroupList.get(2).setParent(tradeGroupList.get(4));
		return tradeGroupList;
	}


	private TradeGroup newTradeGroup(TradeGroupType type, SecurityUser traderUser) {
		TradeGroup tradeGroup = new TradeGroup();
		tradeGroup.setTradeGroupType(type);
		tradeGroup.setTraderUser(traderUser);
		return tradeGroup;
	}


	private TradeGroup saveTradeGroupParentAndChildren(TradeGroup parent, List<TradeGroup> tradeGroupList, boolean saveParentFirst, boolean resetParentOnSave) {
		TradeGroup tg;
		if (saveParentFirst) {
			if (resetParentOnSave) {
				parent = saveTradeGroup(parent);
				tg = parent;
			}
			else {
				tg = saveTradeGroup(parent);
			}
			saveTradeGroupList(tradeGroupList);
			return tg;
		}
		else {
			saveTradeGroupList(tradeGroupList);
			if (resetParentOnSave) {
				parent = saveTradeGroup(parent);
				tg = parent;
			}
			else {
				tg = saveTradeGroup(parent);
			}
		}
		return tg;
	}


	@Transactional
	protected TradeGroup saveTradeGroupParentAndChildrenTransactional(TradeGroup parent, List<TradeGroup> tradeGroupList, boolean saveParentFirst, boolean resetParentOnSave) {
		return saveTradeGroupParentAndChildren(parent, tradeGroupList, saveParentFirst, resetParentOnSave);
	}


	private void saveTradeGroupList(List<TradeGroup> tradeGroupList) {
		AdvancedUpdatableDAO<TradeGroup, Criteria> dao = ((TradeGroupServiceImpl) this.tradeGroupService).getTradeGroupDAO();
		dao.saveList(tradeGroupList);
	}


	private TradeGroup saveTradeGroup(TradeGroup tradeGroup) {
		AdvancedUpdatableDAO<TradeGroup, Criteria> dao = ((TradeGroupServiceImpl) this.tradeGroupService).getTradeGroupDAO();
		return dao.save(tradeGroup);
	}


	private List<TradeGroup> getTradeGroupListByParent(TradeGroup parent) {
		TradeGroupSearchForm searchForm = new TradeGroupSearchForm();
		searchForm.setParentId(parent.getId());
		return this.tradeGroupService.getTradeGroupList(searchForm);
	}


	@Transactional
	protected void deleteTradeGroups(TradeGroup... tradeGroups) {
		AdvancedUpdatableDAO<TradeGroup, Criteria> dao = ((TradeGroupServiceImpl) this.tradeGroupService).getTradeGroupDAO();
		for (TradeGroup tradeGroup : tradeGroups) {
			try {
				dao.delete(tradeGroup);
			}
			catch (Exception e) {
				// ignore - try to delete each item for cleanup
			}
		}
	}


	private SecurityUser getTraderUser() {
		return this.securityUserService.getSecurityUserByName(SecurityUser.SYSTEM_USER);
	}
}
