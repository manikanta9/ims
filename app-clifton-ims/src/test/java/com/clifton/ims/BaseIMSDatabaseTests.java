package com.clifton.ims;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author manderson
 */
@ContextConfiguration(locations = "BaseIMSDatabaseTests-context.xml")
@ExtendWith(SpringExtension.class)
@Tag("database")
public abstract class BaseIMSDatabaseTests {

	@Autowired
	private ContextHandler contextHandler;


	@BeforeEach
	public void setUser() {
		this.contextHandler.setBean(Context.USER_BEAN_NAME, newUser());
	}


	protected SecurityUser getUser() {
		return ((SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME));
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	private SecurityUser newUser() {
		SecurityUser testUser = new SecurityUser();
		testUser.setId((short) 1);
		testUser.setUserName("TestUser");
		return testUser;
	}
}


