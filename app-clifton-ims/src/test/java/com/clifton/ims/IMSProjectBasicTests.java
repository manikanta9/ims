package com.clifton.ims;

import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.system.schema.column.SystemColumnServiceImpl;
import com.clifton.system.validation.DataAccessExceptionConverter;
import com.clifton.workflow.assignment.WorkflowAssignmentServiceImpl;
import com.clifton.workflow.definition.WorkflowDefinitionServiceImpl;
import com.clifton.workflow.transition.WorkflowTransitionServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>IMSProjectBasicTests</code> class tests loading of full application context.
 *
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class IMSProjectBasicTests extends BasicProjectTests {

	@Resource
	private DataAccessExceptionConverter dataAccessExceptionConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getProjectPrefix() {
		return "ims";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("reconciliation");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Controller");
		basicProjectAllowedSuffixNamesList.add("Listener");
		basicProjectAllowedSuffixNamesList.add("internalAspectJWeavingEnabler");
		basicProjectAllowedSuffixNamesList.add("openSessionInViewInterceptor");
		basicProjectAllowedSuffixNamesList.add("reconcilePositionManyToManyMatcher");
		return basicProjectAllowedSuffixNamesList;
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.beans.PropertyDescriptor");
		imports.add("org.apache.commons.beanutils.PropertyUtils");
		imports.add("org.apache.commons.lang.mutable.MutableBoolean");
		imports.add("org.springframework.beans.BeansException");
		imports.add("org.springframework.beans.factory.config.BeanDefinition");
		imports.add("org.springframework.beans.factory.support.DefaultListableBeanFactory");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("java.beans.PropertyDescriptor");
		imports.add("com.google.common.collect.Maps");
	}


	@Override
	protected void configureRequiredApplicationContextBeans(Map<String, Object> beanMap) {
		beanMap.put("dataAccessExceptionConverter", this.dataAccessExceptionConverter);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testServiceClassAnnotations() {
		Annotation[] annotations = AnnotationUtils.getAnnotations(SystemColumnServiceImpl.class);
		Assertions.assertEquals(1, annotations.length, SystemColumnServiceImpl.class + " should have 1 annotation");
		Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(SystemColumnServiceImpl.class, Service.class));

		annotations = AnnotationUtils.getAnnotations(WorkflowTransitionServiceImpl.class);
		Assertions.assertEquals(1, annotations.length, WorkflowTransitionServiceImpl.class + " should have 1 annotation");
		Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(WorkflowTransitionServiceImpl.class, Service.class));

		annotations = AnnotationUtils.getAnnotations(WorkflowDefinitionServiceImpl.class);
		Assertions.assertEquals(1, annotations.length, WorkflowDefinitionServiceImpl.class + " should have 1 annotation");
		Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(WorkflowDefinitionServiceImpl.class, Service.class));
	}


	@Test
	public void testServiceMethodAnnotations() {
		Method[] methods = SystemColumnServiceImpl.class.getMethods();
		for (Method method : methods) {
			if ("assignSystemColumnsToTemplate".equals(method.getName())) {
				Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(method, RequestMapping.class));
				RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
				String[] values = rm.value();
				Assertions.assertEquals("systemColumnsAssignToTemplate", values[0]);
			}
		}

		methods = WorkflowTransitionServiceImpl.class.getMethods();
		for (Method method : methods) {
			if ("executeWorkflowTransitionByTransition".equals(method.getName())) {
				Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(method, RequestMapping.class));
				RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
				String[] values = rm.value();
				Assertions.assertEquals("workflowTransitionExecuteByTransition", values[0]);
			}
			else if ("executeWorkflowTransition".equals(method.getName())) {
				Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(method, RequestMapping.class), "RequestMapping annotation is required for " + method);
				RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
				String[] values = rm.value();
				Assertions.assertEquals("workflowTransitionExecute", values[0]);
			}
		}

		methods = WorkflowAssignmentServiceImpl.class.getMethods();
		for (Method method : methods) {
			if ("assignWorkflow".equals(method.getName())) {
				Assertions.assertFalse(AnnotationUtils.isAnnotationPresent(method, RequestMapping.class));
			}
		}
	}
}
