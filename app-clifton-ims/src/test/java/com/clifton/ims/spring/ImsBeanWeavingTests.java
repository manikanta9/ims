package com.clifton.ims.spring;

import com.clifton.core.context.spring.AbstractBeanWeavingTests;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * The {@link ImsBeanWeavingTests} attempts to validate that all types are properly woven during context initialization.
 *
 * @author MikeH
 */
@ExtendWith(SpringExtension.class)
@TestPropertySource(locations = {"classpath:/com/clifton/ims/spring/ims-bean-weaving-tests.properties"})
@ContextConfiguration
@Tag("memory-db") // Run with in-memory database tests so that load-time weaving for transactions is enabled; weaving is disabled during standard unit tests
public class ImsBeanWeavingTests extends AbstractBeanWeavingTests {

	public ImsBeanWeavingTests(ApplicationContext applicationContext) {
		super(applicationContext);
	}
}
