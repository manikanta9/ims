package com.clifton.ims.integration.event.corporate.action;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.CSVFileToDataTableConverter;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.EventHandler;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.incoming.client.event.IntegrationImportEvent;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateAction;
import com.clifton.integration.incoming.corporate.action.IntegrationCorporateActionService;
import com.clifton.integration.incoming.corporate.action.search.IntegrationCorporateActionSearchForm;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.FractionalShares;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventStatus;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Tests whether a Pentaho transformed Markit data file containing {@link IntegrationCorporateAction} entities can be processed by the
 * event listener into the correct {@link InvestmentSecurityEvent} and {@link InvestmentSecurityEventPayout}.
 */
@ContextConfiguration("classpath:com/clifton/ims/integration/event/corporate/action/CorporateActionEventListenerTest-context.xml")
public class CorporateActionEventListenerTests extends BaseInMemoryDatabaseTests {

	@Resource
	private EventHandler eventHandler;

	@Resource
	private IntegrationCorporateActionService integrationCorporateActionService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityEventService investmentSecurityEventService;

	@Resource
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;

	@Resource
	private CorporateActionEventUtilHandler corporateActionEventUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveCorporateActionEvent() throws IOException {
		loadCorporateActions();

		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setLimit(2000);
		Map<Long, InvestmentSecurityEvent> eventMap = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm).stream()
				.collect(Collectors.toMap(InvestmentSecurityEvent::getCorporateActionIdentifier, Function.identity()));
		Assertions.assertEquals(111, eventMap.values().size());

		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setLimit(2000);
		Map<InvestmentSecurityEvent, List<InvestmentSecurityEventPayout>> payoutMap = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm).stream()
				.collect(Collectors.groupingBy(InvestmentSecurityEventPayout::getSecurityEvent));
		Assertions.assertEquals(122, payoutMap.values().stream().mapToLong(Collection::size).sum());

		Set<InvestmentSecurityEvent> eventsWithoutFractionalSharesPayout = new HashSet<>();
		eventMap.values().forEach(securityEvent -> {
			if (securityEvent.getCorporateActionIdentifier() != null && !securityEvent.getType().isSinglePayoutOnly()) {
				InvestmentSecurityEventPayoutSearchForm eventPayoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
				eventPayoutSearchForm.setSecurityEventId(securityEvent.getId());
				eventPayoutSearchForm.setOrderBy("payoutNumber:asc");
				List<InvestmentSecurityEventPayout> eventPayoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(eventPayoutSearchForm);
				Assertions.assertFalse(CollectionUtils.isEmpty(eventPayoutList), "Expected at least one payout for event: " + securityEvent);

				Optional<InvestmentSecurityEventPayout> firstPayout = eventPayoutList.stream().filter(payout -> payout.isDefaultElection() && !payout.isDeleted()).findFirst();
				firstPayout.ifPresent(payout -> {
					Assertions.assertEquals(0, securityEvent.getBeforeEventValue().compareTo(payout.getBeforeEventValue()), "Expected event's before value to equal the first default, non-deleted payout's; event: " + securityEvent);
					Assertions.assertEquals(0, securityEvent.getAfterEventValue().compareTo(payout.getAfterEventValue()), "Expected event's after value to equal the first default, non-deleted payout's; event: " + securityEvent);
					if (securityEvent.getType().isAdditionalSecurityAllowed()) {
						Assertions.assertEquals(securityEvent.getAdditionalSecurity(), payout.getPayoutSecurity(), "Expected event's additional security to equal the first default, non-deleted payout's payout security; event: " + securityEvent);
					}
				});

				if (!StringUtils.isEmpty(securityEvent.getEventDescription())) {
					FractionalShares fractionalShares = FractionalShares.forNameStrict(securityEvent.getEventDescription());
					if (fractionalShares != null) {
						List<InvestmentSecurityEventPayout> payoutWithFractionalSharesList = eventPayoutList.stream()
								.filter(payout -> fractionalShares == payout.getFractionalSharesMethod())
								.collect(Collectors.toList());
						if (CollectionUtils.isEmpty(payoutWithFractionalSharesList)) {
							eventsWithoutFractionalSharesPayout.add(securityEvent);
						}
					}
				}
			}
		});

		Assertions.assertTrue(CollectionUtils.isEmpty(eventsWithoutFractionalSharesPayout), eventsWithoutFractionalSharesPayout.stream().map(InvestmentSecurityEvent::getLabel).collect(Collectors.joining("\n", "Unable to find payout with expected Fractional Shares Method defined for events:\n", "")));

		// Additional tests
		testSymbolChangeEventLoaded();
		testFractionalSharesStrict();
		testVoluntaryEventHasPayoutInformation();
		testReAddOfPayouts();
		testUpdateOfPayouts();
		testUpdateOfExistingEventForPayout();
		testEventSecuritySetOnInitialLoadOnly();
		testEventDateMatchesExDateForMergers();
		testInvestmentSecurityEventPayoutIsDtcOnly(payoutMap);
	}


	private void loadCorporateActions() throws IOException {
		loadCorporateActions(null);
	}


	private void loadCorporateActions(Consumer<IntegrationCorporateAction> corporateActionModifier) throws IOException {
		CorporateActionLoader corporateActionLoader = new CorporateActionLoader();
		Map<Long, List<IntegrationCorporateAction>> csvCorporateActionMap = corporateActionLoader.getTestCorporateActions().stream()
				.collect(Collectors.groupingBy(IntegrationCorporateAction::getCorporateActionIdentifier));
		List<IntegrationCorporateAction> corporateActionList = csvCorporateActionMap.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		Assertions.assertEquals(corporateActionLoader.getInputLineCount() - 1, corporateActionList.size());
		Assertions.assertEquals(129, corporateActionLoader.getInputLineCount() - 1);
		Mockito.when(this.integrationCorporateActionService.getIntegrationCorporateActionList(Mockito.any(IntegrationCorporateActionSearchForm.class)))
				.thenReturn(corporateActionList);

		if (corporateActionModifier != null) {
			corporateActionList.forEach(corporateActionModifier);
		}

		IntegrationImportEvent integrationImportEvent = new IntegrationImportEvent();
		integrationImportEvent.setEventName(CorporateActionEventListener.EVENT_NAME);
		IntegrationImportRun integrationImportRun = new IntegrationImportRun();
		integrationImportRun.setId(1);
		IntegrationImportDefinition integrationImportDefinition = new IntegrationImportDefinition();
		IntegrationSource source = new IntegrationSource();
		IntegrationFileDefinition fileDefinition = new IntegrationFileDefinition();
		fileDefinition.setSource(source);
		integrationImportDefinition.setFileDefinition(fileDefinition);
		integrationImportRun.setIntegrationImportDefinition(integrationImportDefinition);
		integrationImportEvent.setTarget(integrationImportRun);

		try {
			this.eventHandler.raiseEvent(integrationImportEvent);
		}
		catch (RuntimeException e) {
			Assertions.assertTrue(e.getMessage().startsWith("Failed to import [3] of [129] corporate actions because of these errors:"));
		}
	}


	private void testSymbolChangeEventLoaded() {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setCorporateActionIdentifiers(new Long[]{598346225L, 603809855L, 607289293L});
		List<InvestmentSecurityEvent> symbolChangeEventList = this.investmentSecurityEventService.getInvestmentSecurityEventList(searchForm);
		Assertions.assertFalse(CollectionUtils.isEmpty(symbolChangeEventList), "Failed to load Symbol Change events");
	}


	private void testFractionalSharesStrict() {
		// Event with corporate action ID 580353204 has a spoofed fractional share name to ensure it is not defaulted.
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setCorporateActionIdentifier(580353204L);
		List<InvestmentSecurityEventPayout> payoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm);
		Assertions.assertFalse(CollectionUtils.isEmpty(payoutList), "Unable to find expected payout for event with Corporate Action ID: 580353204");

		CollectionUtils.getStream(payoutList).forEach(payout -> Assertions.assertNull(payout.getFractionalSharesMethod(), "Did not expect a payout fractional shares method"));
	}


	private void testVoluntaryEventHasPayoutInformation() {
		// Voluntary event with multiple payouts: one is a default election so its payout info should be copied to the event even though it does not have the lowest payout number
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setCorporateActionIdentifier(602640529L);
		List<InvestmentSecurityEventPayout> payoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm);
		Assertions.assertEquals(CollectionUtils.getSize(payoutList), 2, "Expected [2] payouts for event with Corporate Action ID: 602640529");

		Assertions.assertEquals(CollectionUtils.getFirstElementStrict(payoutList).getSecurityEvent().getAdditionalSecurity().getSymbol(), "EUR",
				"Additional Security was not properly populated from the Payout Security for voluntary Event with default election and Corporate Action ID: 602640529");

		// Voluntary event WITHOUT a default election, payout info should be copied to the event from the payout with the LOWEST payout number
		payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm.setCorporateActionIdentifier(580517539L);
		payoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm);
		Assertions.assertEquals(CollectionUtils.getSize(payoutList), 2, "Expected [2] payouts for event with Corporate Action ID: 580517539");

		Assertions.assertEquals(CollectionUtils.getFirstElementStrict(payoutList).getSecurityEvent().getAdditionalSecurity().getSymbol(), "USD",
				"Additional Security was not properly populated from the Payout Security for voluntary Event with Corporate Action ID: 580517539");
	}


	private void testReAddOfPayouts() throws IOException {
		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setCorporateActionIdentifier(566546355L);
		InvestmentSecurityEvent event = CollectionUtils.getOnlyElement(this.investmentSecurityEventService.getInvestmentSecurityEventList(securityEventSearchForm));
		Assertions.assertNotNull(event);
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm1 = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm1.setSecurityEventId(event.getId());
		List<InvestmentSecurityEventPayout> eventPayoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm1);
		Assertions.assertEquals(2, eventPayoutList.size());
		// remove payouts to reload
		eventPayoutList.forEach(payout -> this.investmentSecurityEventPayoutService.deleteInvestmentSecurityEventPayout(payout.getId(), false));
		// update event value
		event.setBeforeAndAfterEventValue(new BigDecimal(5));
		event = this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		Assertions.assertEquals(new BigDecimal(5), event.getBeforeEventValue());
		// reload corporate actions
		loadCorporateActions();
		// validate event updated for default payout
		event = this.investmentSecurityEventService.getInvestmentSecurityEvent(event.getId());
		payoutSearchForm1 = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm1.setSecurityEventId(event.getId());
		eventPayoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm1);
		Assertions.assertEquals(2, eventPayoutList.size());
		Optional<InvestmentSecurityEventPayout> defaultPayout = eventPayoutList.stream().filter(InvestmentSecurityEventPayout::isDefaultElection).findFirst();
		Assertions.assertTrue(defaultPayout.isPresent());
		Assertions.assertEquals(defaultPayout.get().getBeforeEventValue(), event.getBeforeEventValue());
	}


	private void testUpdateOfPayouts() throws IOException {
		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setCorporateActionIdentifier(566546355L);
		InvestmentSecurityEvent event = CollectionUtils.getOnlyElement(this.investmentSecurityEventService.getInvestmentSecurityEventList(securityEventSearchForm));
		Assertions.assertNotNull(event);
		InvestmentSecurityEventPayoutSearchForm payoutSearchForm1 = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm1.setSecurityEventId(event.getId());
		List<InvestmentSecurityEventPayout> eventPayoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm1);
		Assertions.assertEquals(2, eventPayoutList.size());
		// remove payouts to reload
		eventPayoutList.forEach(payout -> {
			if (payout.isDefaultElection()) {
				payout.setDefaultElection(false);
				this.investmentSecurityEventPayoutService.saveInvestmentSecurityEventPayout(payout);
			}
		});
		// update event value
		event.setBeforeAndAfterEventValue(new BigDecimal(5));
		event = this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		Assertions.assertEquals(new BigDecimal(5), event.getBeforeEventValue());
		// reload corporate actions
		loadCorporateActions();
		// validate event updated for default payout
		event = this.investmentSecurityEventService.getInvestmentSecurityEvent(event.getId());
		payoutSearchForm1 = new InvestmentSecurityEventPayoutSearchForm();
		payoutSearchForm1.setSecurityEventId(event.getId());
		eventPayoutList = this.investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm1);
		Assertions.assertEquals(2, eventPayoutList.size());
		Optional<InvestmentSecurityEventPayout> defaultPayout = eventPayoutList.stream().filter(InvestmentSecurityEventPayout::isDefaultElection).findFirst();
		Assertions.assertTrue(defaultPayout.isPresent());
		Assertions.assertEquals(defaultPayout.get().getBeforeEventValue(), event.getBeforeEventValue());
	}


	private void testUpdateOfExistingEventForPayout() throws IOException {
		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setCorporateActionIdentifier(566546355L);
		InvestmentSecurityEvent event = CollectionUtils.getOnlyElement(this.investmentSecurityEventService.getInvestmentSecurityEventList(securityEventSearchForm));
		Assertions.assertNotNull(event);
		// update event value
		event.setStatus(this.investmentSecurityEventService.getInvestmentSecurityEventStatusByName(InvestmentSecurityEventStatus.STATUS_INCOMPLETE));
		event.setRecordDate(null);
		event = this.investmentSecurityEventService.saveInvestmentSecurityEvent(event);
		Assertions.assertEquals(InvestmentSecurityEventStatus.STATUS_INCOMPLETE, event.getStatus().getName());
		Assertions.assertNull(event.getRecordDate());
		// reload corporate actions
		loadCorporateActions();
		// validate event updated for default payout
		event = this.investmentSecurityEventService.getInvestmentSecurityEvent(event.getId());
		Assertions.assertEquals(InvestmentSecurityEventStatus.STATUS_IN_CONFLICT, event.getStatus().getName());
		Assertions.assertNotNull(event.getRecordDate());
	}


	private void testEventSecuritySetOnInitialLoadOnly() throws IOException {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurity(26359);

		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setCorporateActionIdentifier(568364623L);
		InvestmentSecurityEvent event = CollectionUtils.getOnlyElement(this.investmentSecurityEventService.getInvestmentSecurityEventList(securityEventSearchForm));
		Assertions.assertNotNull(event);
		Assertions.assertEquals(security, event.getSecurity());

		// reload corporate actions
		loadCorporateActions(integrationCorporateAction -> {
			if (integrationCorporateAction.getCorporateActionIdentifier() == 568364623L) {
				integrationCorporateAction.setInvestmentSecurityIdentifier(111006);
			}
		});
		// validate event updated for default payout
		event = this.investmentSecurityEventService.getInvestmentSecurityEvent(event.getId());
		Assertions.assertEquals(security, event.getSecurity());
	}


	private void testEventDateMatchesExDateForMergers() {
		Long corporateActionIdentifier = 574873880L;
		InvestmentSecurityEventSearchForm securityEventSearchForm = new InvestmentSecurityEventSearchForm();
		securityEventSearchForm.setCorporateActionIdentifier(corporateActionIdentifier);
		InvestmentSecurityEvent event = CollectionUtils.getOnlyElement(this.investmentSecurityEventService.getInvestmentSecurityEventList(securityEventSearchForm));
		Assertions.assertNotNull(event);
		Assertions.assertEquals(event.getEventDate(), event.getExDate());
		Assertions.assertEquals(event.getEventDate(), event.getRecordDate());

		// update event date
		IntegrationCorporateAction action = CollectionUtils.getStream(this.integrationCorporateActionService.getIntegrationCorporateActionList(new IntegrationCorporateActionSearchForm()))
				.filter(corporateAction -> corporateAction.getCorporateActionIdentifier().equals(corporateActionIdentifier) && corporateAction.getPayoutNumber() == 1)
				.findFirst()
				.orElse(null);
		Assertions.assertNotNull(action);
		Date newEventDate = DateUtils.toDate("05/01/2019");
		action.setEventDate(newEventDate);
		this.corporateActionEventUtilHandler.updateInvestmentSecurityEvent(action, event);
		// validate Record Date and Ex Date match Event Date
		Assertions.assertEquals(event.getEventDate(), newEventDate);
		Assertions.assertEquals(event.getExDate(), newEventDate);
		Assertions.assertEquals(event.getRecordDate(), newEventDate);
	}


	/**
	 * Checks the each InvestmentSecurityEventPayout entity's isDTCOnly property value to ensure it matches
	 * the isDTCOnly property value of the corresponding value in the IntegrationCorporateAction event as set in the test data file.
	 *
	 * @param payoutMap -- map constructed in parent test case.
	 */
	private void testInvestmentSecurityEventPayoutIsDtcOnly(Map<InvestmentSecurityEvent, List<InvestmentSecurityEventPayout>> payoutMap) {
		payoutMap.forEach((InvestmentSecurityEvent key, List<InvestmentSecurityEventPayout> payoutList) -> {
			if (key.getCorporateActionIdentifier() == (long) 580351944) {
				// corporate action with ID = 580351944 (in CSV data) has DtcOnly set to true
				Assertions.assertFalse(CollectionUtils.isEmpty(payoutList));
				Assertions.assertTrue(checkPayoutEventsDtcOnlyPropertyValues(payoutList, true));
			}
			else {
				if (!CollectionUtils.isEmpty(payoutList)) {
					Assertions.assertTrue(checkPayoutEventsDtcOnlyPropertyValues(payoutList, false));
				}
			}
		});
	}


	private boolean checkPayoutEventsDtcOnlyPropertyValues(List<InvestmentSecurityEventPayout> payoutList, boolean expectedValue) {
		if (CollectionUtils.isEmpty(payoutList)) {
			return false;
		}

		for (InvestmentSecurityEventPayout payout : CollectionUtils.getIterable(payoutList)) {
			if (payout.isDtcOnly() != expectedValue) {
				return false;
			}
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Process csv with its own processor, migrations could eventually be used if someone makes the integration project in-memory capable.
	 */
	private static class CorporateActionLoader {

		public static final Function<String, Long> longConverter = (value) -> StringUtils.isEmpty(value) ? null : Long.parseLong(value);
		public static final Function<String, Short> ShortConverter = (value) -> StringUtils.isEmpty(value) ? null : Short.parseShort(value);
		public static final Function<String, Integer> IntegerConverter = (value) -> StringUtils.isEmpty(value) ? null : Integer.parseInt(value);
		public static final Function<String, Date> DateConverter = (value) -> StringUtils.isEmpty(value) ? null : DateUtils.toDate(value, "yyyy/MM/dd");
		public static final Function<String, Boolean> BooleanConverter = (value) -> StringUtils.isEmpty(value) ? false : BooleanUtils.isTrue(value);
		public static final Function<String, BigDecimal> BigDecimalConverter = (value) -> StringUtils.isEmpty(value) ? null : new BigDecimal(value);
		public static final Function<Object, String> StringConverter = (value) -> Objects.isNull(value) ? null : value.toString().trim();
		private static final ClassPathResource inputFile = new ClassPathResource("com/clifton/ims/integration/event/corporate/action/data/CorporateActionEventData.csv");
		private final Map<String, BiConsumer<IntegrationCorporateAction, String>> mappings = new HashMap<>();


		CorporateActionLoader() {
			this.mappings.put("CorporateActionIdentifier", (action, value) -> action.setCorporateActionIdentifier(longConverter.apply(value)));
			this.mappings.put("ElectionNumber", (action, value) -> action.setElectionNumber(Objects.isNull(ShortConverter.apply(value)) ? (short) 0 : ShortConverter.apply(value)));
			this.mappings.put("PayoutNumber", (action, value) -> action.setPayoutNumber(ShortConverter.apply(value)));
			this.mappings.put("EventTypeName", (action, value) -> action.setEventTypeName(StringConverter.apply(value)));
			this.mappings.put("EventStatusName", (action, value) -> action.setEventStatusName(StringConverter.apply(value)));
			this.mappings.put("InvestmentSecurityID", (action, value) -> action.setInvestmentSecurityIdentifier(IntegerConverter.apply(value)));
			this.mappings.put("EventDeclareDate", (action, value) -> action.setDeclareDate(DateConverter.apply(value)));
			this.mappings.put("EventExDate", (action, value) -> action.setExDate(DateConverter.apply(value)));
			this.mappings.put("EventRecordDate", (action, value) -> action.setRecordDate(DateConverter.apply(value)));
			this.mappings.put("EventDate", (action, value) -> action.setEventDate(DateConverter.apply(value)));
			this.mappings.put("AdditionalEventDate", (action, value) -> action.setAdditionalEventDate(DateConverter.apply(value)));
			this.mappings.put("AdditionalEventValue", (action, value) -> action.setAdditionalEventValue(BigDecimalConverter.apply(value)));
			this.mappings.put("EventDescription", IntegrationCorporateAction::setEventDescription);
			this.mappings.put("PayoutSecurityIdentifier", IntegrationCorporateAction::setPayoutSecurityIdentifier);
			this.mappings.put("PayoutSecurityIdentifierType", (action, value) -> action.setPayoutSecurityTypeIdentifier(ShortConverter.apply(value)));
			this.mappings.put("BeforeEventValue", (action, value) -> action.setBeforeEventValue(BigDecimalConverter.apply(value)));
			this.mappings.put("AfterEventValue", (action, value) -> action.setAfterEventValue(BigDecimalConverter.apply(value)));
			this.mappings.put("AdditionalPayoutValue", (action, value) -> action.setAdditionalPayoutValue(BigDecimalConverter.apply(value)));
			this.mappings.put("AdditionalPayoutValue2", (action, value) -> action.setAdditionalPayoutValue2(BigDecimalConverter.apply(value)));
			this.mappings.put("ProrationRate", (action, value) -> action.setProrationRate(BigDecimalConverter.apply(value)));
			this.mappings.put("AdditionalPayoutDate", (action, value) -> action.setAdditionalPayoutDate(DateConverter.apply(value)));
			this.mappings.put("PayoutDescription", IntegrationCorporateAction::setPayoutDescription);
			this.mappings.put("IsVoluntary", (action, value) -> action.setVoluntary(BooleanConverter.apply(value)));
			this.mappings.put("IsTaxable", (action, value) -> action.setTaxable(BooleanConverter.apply(value)));
			this.mappings.put("IsDeleted", (action, value) -> action.setDeleted(BooleanConverter.apply(value)));
			this.mappings.put("IsDefaultElection", (action, value) -> action.setDefaultElection(BooleanConverter.apply(value)));
			this.mappings.put("PayoutTypeIdentifier", (action, value) -> action.setPayoutTypeIdentifier(ShortConverter.apply(value)));
			this.mappings.put("FractionalSharesMethodName", IntegrationCorporateAction::setFractionalSharesMethodName);
			this.mappings.put("IsDtcOnly", (action, value) -> action.setDtcOnly(BooleanConverter.apply(value)));
		}


		public List<IntegrationCorporateAction> getTestCorporateActions() throws IOException {
			List<IntegrationCorporateAction> testCorporateActions = new ArrayList<>();

			CSVFileToDataTableConverter csvFileToDataTableConverter = new CSVFileToDataTableConverter();
			DataTable dataTable = csvFileToDataTableConverter.convert(inputFile.getFile());
			List<DataColumn> columns = CollectionUtils.createList(dataTable.getColumnList());
			List<String> missing = columns.stream().map(DataColumn::getColumnName).filter(c -> !this.mappings.containsKey(c)).collect(Collectors.toList());
			Assertions.assertTrue(missing.isEmpty(), "Unmapped columns:  " + missing);
			for (int r = 0; r < dataTable.getTotalRowCount(); r++) {
				DataRow row = dataTable.getRow(r);
				List<Object> values = row.getRowValueList(true);
				Assertions.assertEquals(columns.size(), values.size());
				IntegrationCorporateAction integrationCorporateAction = new IntegrationCorporateAction();
				for (int c = 0; c < columns.size(); c++) {
					DataColumn column = columns.get(c);
					Assertions.assertTrue(this.mappings.containsKey(column.getColumnName()));
					String value = StringConverter.apply(values.get(c));
					BiConsumer<IntegrationCorporateAction, String> mapper = this.mappings.get(column.getColumnName());
					mapper.accept(integrationCorporateAction, value);
				}
				testCorporateActions.add(integrationCorporateAction);
			}

			return testCorporateActions;
		}


		public long getInputLineCount() throws IOException {
			return Files.lines(inputFile.getFile().toPath()).count();
		}
	}
}
