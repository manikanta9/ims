SELECT 'InvestmentSecurityEventStatus' AS entityTableName, InvestmentSecurityEventStatusID AS entityId
FROM InvestmentSecurityEventStatus
UNION
SELECT 'InvestmentSecurityEventType' AS entityTableName, InvestmentSecurityEventTypeID AS entityId
FROM InvestmentSecurityEventType
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId
FROM InvestmentSecurity
WHERE InvestmentSecurityID IN
	  (23661, 23661, 23661, 23661, 98577, 23661, 23661, 23661, 23661, 98577, 111006, 98307, 108151, 45058, 26359, 111006, 9696, 112043, 108151, 45058, 45058, 22016, 104667, 107933,
	   10129, 12597, 104666, 11710, 17555, 11471, 18786, 22042, 22039, 22041, 9364, 31283, 19196, 96443, 98031, 101056, 11660, 11659, 11705, 18180, 21979, 22017, 21974, 20977,
	   10473, 107907, 10220, 17586, 20743, 17554, 40484, 22020, 107903, 21679, 13219, 10110, 45279, 9346, 11352, 2572, 3271, 10128, 22009, 96232, 102030, 25574, 22645, 25559,
	   25574, 102030, 80498, 22644, 15440, 102030, 99066, 15437, 98517, 98053, 112038, 98215, 98569, 98256, 98446, 96395, 96672, 96768, 96519, 107916, 23776, 96398, 96270, 1403,
	   1224, 96398, 96270, 25591, 80517, 18029, 104089, 101631, 98082, 107003, 98396, 350, 430, 104852, 104083, 26418, 1040, 767, 22745, 103367, 18029, 18029, 11034, 98630, 23721,
	   112041, 123723, 98745, 125095, 103279)
UNION
SELECT 'InvestmentSecurityEventType' AS entityTableName, InvestmentSecurityEventTypeID AS entityId
FROM InvestmentSecurityEventType
UNION
SELECT 'InvestmentSecurityEventTypeHierarchy' AS entityTableName, InvestmentSecurityEventTypeHierarchyID AS entityId
FROM InvestmentSecurityEventTypeHierarchy
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId
FROM InvestmentSecurity
WHERE IsCurrency = 1
  AND Symbol IN ('GBP', 'USD', 'CAD', 'EUR', 'TRY', 'PHP', 'CHF')
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId
FROM InvestmentSecurity
WHERE Sedol IN ('2940375', '0287580', 'BZ07PN3', '2412001', 'BD8Q1B8', '2126335', '2024644', '2400341', '2142621', 'BHZSKR4', 'B669WX9', 'B53C331', 'BVGCLQ9', 'BJLTRC9', 'BLCF3J9', 'BKRN595')
UNION
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId
FROM InvestmentSecurity
WHERE Cusip IN ('74017N105')
UNION
SELECT 'InvestmentSecurityEventPayoutType' AS entityTableName, InvestmentSecurityEventPayoutTypeID AS entityId
FROM InvestmentSecurityEventPayoutType
UNION
SELECT 'InvestmentSecurityEventPayoutTypeAssignment' AS entityTableName, InvestmentSecurityEventPayoutTypeAssignmentID AS entityId
FROM InvestmentSecurityEventPayoutTypeAssignment
UNION
SELECT 'InvestmentSecurityEventTypeHierarchy' AS entityTableNamer, InvestmentSecurityEventTypeHierarchyID AS entityId
FROM InvestmentSecurityEventTypeHierarchy;
