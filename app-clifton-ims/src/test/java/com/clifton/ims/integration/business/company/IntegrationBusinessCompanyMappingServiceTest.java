package com.clifton.ims.integration.business.company;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.integration.incoming.business.IntegrationBusinessCompany;
import com.clifton.integration.incoming.business.IntegrationBusinessCompanyService;
import com.google.common.collect.Maps;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


//TODO - convert to be an integration test (would depend on integration being up and accessible for proxied calls?), or maybe InMemory with two dbs?
@Disabled
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class IntegrationBusinessCompanyMappingServiceTest {

	@Resource
	private IntegrationBusinessCompanyMappingService integrationBusinessCompanyMappingService;

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private IntegrationBusinessCompanyService integrationBusinessCompanyService;


	@Test
	public void testGetBusinessCompanyMappedColumns() {
		// make sure the schema or property names haven't changed
		List<Column> columnList = this.integrationBusinessCompanyMappingService.getBusinessCompanyMappedColumns();
		Assertions.assertNotNull(columnList);
		Assertions.assertFalse(columnList.isEmpty());
	}


	@Test
	public void testGetIntegrationBusinessCompanyMappedColumns() {
		// make sure the schema or property names haven't changed
		List<Column> columnList = this.integrationBusinessCompanyMappingService.getIntegrationBusinessCompanyMappedColumns();
		Assertions.assertNotNull(columnList);
		Assertions.assertFalse(columnList.isEmpty());
	}


	@Test
	public void testBusinessCompanyMappedProperties() {
		BusinessCompany businessCompany = this.businessCompanyService.getBusinessCompany(7886);
		Assertions.assertNotNull(businessCompany);
		Map<String, Object> values = BeanUtils.describeWithRequiredSetter(businessCompany);
		List<Column> columnList = this.integrationBusinessCompanyMappingService.getBusinessCompanyMappedColumns();
		List<String> beanPropertyList = columnList.stream().map(Column::getBeanPropertyName).collect(Collectors.toList());
		Assertions.assertTrue(values.keySet().containsAll(beanPropertyList));
		List<String> nullValuesList = values.entrySet().stream()
				.filter(e -> beanPropertyList.contains(e.getKey()))
				.filter(e -> Objects.isNull(e.getValue()))
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
		// the test data has values for all mapped properties
		Assertions.assertTrue(nullValuesList.isEmpty(), "Should cover all mapped properties:  " + nullValuesList);
	}


	@Test
	public void testIntegrationBusinessCompanyMappedProperties() {
		IntegrationBusinessCompany integrationBusinessCompany = this.integrationBusinessCompanyService.getIntegrationBusinessCompany(102);
		Assertions.assertNotNull(integrationBusinessCompany);
		Map<String, Object> values = BeanUtils.describe(integrationBusinessCompany);
		List<Column> columnList = this.integrationBusinessCompanyMappingService.getIntegrationBusinessCompanyMappedColumns();
		List<String> beanPropertyList = columnList.stream().map(Column::getBeanPropertyName).collect(Collectors.toList());
		Assertions.assertTrue(values.keySet().containsAll(beanPropertyList));
		List<String> nullValuesList = values.entrySet().stream()
				.filter(e -> beanPropertyList.contains(e.getKey()))
				.filter(e -> Objects.isNull(e.getValue()))
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
		// the test data has values for all mapped properties
		Assertions.assertTrue(nullValuesList.isEmpty(), "Should cover all mapped properties:  " + nullValuesList);
	}


	@Test
	public void testExcludingAllMappedProperties() {
		BusinessCompany originalBusinessCompany = this.businessCompanyService.getBusinessCompany(7886);
		Map<String, Object> originalValues = BeanUtils.describe(originalBusinessCompany);
		List<Column> excludedColumnList = this.integrationBusinessCompanyMappingService.getBusinessCompanyMappedColumns();
		IntegrationBusinessCompanyMappingResults results = this.integrationBusinessCompanyMappingService.getBusinessCompanyFromIntegrationBusinessCompanyWithExclusions(7886, excludedColumnList);
		Assertions.assertTrue(results.getValidationList().isEmpty(), results.getValidationList().toString());
		Assertions.assertFalse(results.isChanged());
		BusinessCompany updatedBusinessCompany = results.getBusinessCompany();
		Map<String, Object> updatedValues = BeanUtils.describe(updatedBusinessCompany);
		Assertions.assertTrue(Maps.difference(originalValues, updatedValues).areEqual(), "Entities should not be different " + Maps.difference(originalValues, updatedValues).toString());
	}


	@Test
	public void testAllMappedPropertiesAreSet() {
		IntegrationBusinessCompanyMappingResults results = this.integrationBusinessCompanyMappingService.getBusinessCompanyFromIntegrationBusinessCompany(7886);
		Assertions.assertTrue(results.getValidationList().isEmpty(), results.getValidationList().toString());
		BusinessCompany businessCompany = results.getBusinessCompany();
		Assertions.assertNotNull(businessCompany);
		IntegrationBusinessCompany integrationBusinessCompany = this.integrationBusinessCompanyService.getIntegrationBusinessCompanyByBloombergCompanyId(businessCompany.getBloombergCompanyIdentifier());
		Assertions.assertNotNull(integrationBusinessCompany);
		Assertions.assertTrue(results.isChanged());

		MatcherAssert.assertThat(businessCompany.getCompanyLegalName(), IsEqual.equalTo(integrationBusinessCompany.getCompanyLegalName()));
		MatcherAssert.assertThat(businessCompany.isUltimateParent(), IsEqual.equalTo(integrationBusinessCompany.isUltimateParent()));
		MatcherAssert.assertThat(businessCompany.isPrivateCompany(), IsEqual.equalTo(integrationBusinessCompany.isPrivateCompany()));
		MatcherAssert.assertThat(businessCompany.isAcquiredByParent(), IsEqual.equalTo(integrationBusinessCompany.isAcquiredByParent()));
		MatcherAssert.assertThat(businessCompany.getCountryOfIncorporation(), IsEqual.equalTo(integrationBusinessCompany.getCountryOfIncorporation()));
		MatcherAssert.assertThat(businessCompany.getCountryOfRisk(), IsEqual.equalTo(integrationBusinessCompany.getCountryOfRisk()));
		MatcherAssert.assertThat(businessCompany.getStateOfIncorporation(), IsEqual.equalTo(integrationBusinessCompany.getStateOfIncorporation()));
		MatcherAssert.assertThat(businessCompany.getCompanyCorporateTicker(), IsEqual.equalTo(integrationBusinessCompany.getCompanyCorporateTicker()));
		MatcherAssert.assertThat(businessCompany.getLegalEntityIdentifier(), IsEqual.equalTo(integrationBusinessCompany.getLegalEntityIdentifier()));
		MatcherAssert.assertThat(businessCompany.getName(), IsEqual.equalTo(integrationBusinessCompany.getLongCompanyName()));
		MatcherAssert.assertThat(businessCompany.getUltimateParentEquityTicker(), IsEqual.equalTo(integrationBusinessCompany.getUltimateParentTickerExchange()));
		MatcherAssert.assertThat(businessCompany.getLeiEntityStatus(), IsEqual.equalTo(integrationBusinessCompany.getLegalEntityStatus()));
		MatcherAssert.assertThat(businessCompany.getLeiEntityStartDate(), IsEqual.equalTo(integrationBusinessCompany.getLegalEntityAssignedDate()));
		MatcherAssert.assertThat(businessCompany.getLeiEntityEndDate(), IsEqual.equalTo(integrationBusinessCompany.getLegalEntityDisabledDate()));
		MatcherAssert.assertThat(businessCompany.getFaxNumber(), IsEqual.equalTo(integrationBusinessCompany.getCompanyFaxNumber()));
		MatcherAssert.assertThat(businessCompany.getPhoneNumber(), IsEqual.equalTo(integrationBusinessCompany.getCompanyTelephoneNumber()));
		MatcherAssert.assertThat(businessCompany.getWebAddress(), IsEqual.equalTo(integrationBusinessCompany.getCompanyWebAddress()));
		MatcherAssert.assertThat(businessCompany.getParent().getBloombergCompanyIdentifier(), IsEqual.equalTo(integrationBusinessCompany.getParentBloombergCompanyIdentifier()));
		MatcherAssert.assertThat(businessCompany.getObligorBusinessCompany().getBloombergCompanyIdentifier(), IsEqual.equalTo(integrationBusinessCompany.getObligorBloombergCompanyIdentifier()));
		MatcherAssert.assertThat(businessCompany.getAlternateCompanyName(), IsEqual.equalTo(integrationBusinessCompany.getAlternateCompanyName()));
		MatcherAssert.assertThat(businessCompany.getParentRelationshipType().getName(), IsEqual.equalTo(integrationBusinessCompany.getCompanyToParentRelationship()));

		MatcherAssert.assertThat(businessCompany.getAddressLine1(), IsEqual.equalTo("2 Mexican Road"));
		MatcherAssert.assertThat(businessCompany.getAddressLine2(), IsEqual.equalTo(null));
		MatcherAssert.assertThat(businessCompany.getAddressLine3(), IsEqual.equalTo(null));
		MatcherAssert.assertThat(businessCompany.getCity(), IsEqual.equalTo("Columbus"));
		MatcherAssert.assertThat(businessCompany.getState(), IsEqual.equalTo("MN"));
		MatcherAssert.assertThat(businessCompany.getPostalCode(), IsEqual.equalTo("43123-8155"));
		MatcherAssert.assertThat(businessCompany.getCountry(), IsEqual.equalTo("US"));

		MatcherAssert.assertThat(businessCompany.getRegistrationAddressLine1(), IsEqual.equalTo("1 American Road"));
		MatcherAssert.assertThat(businessCompany.getRegistrationAddressLine2(), IsEqual.equalTo("Cleveland"));
		MatcherAssert.assertThat(businessCompany.getRegistrationAddressLine3(), IsEqual.equalTo("OH 44144-8151"));
		MatcherAssert.assertThat(businessCompany.getRegistrationCity(), IsEqual.equalTo(null));
		MatcherAssert.assertThat(businessCompany.getRegistrationState(), IsEqual.equalTo(null));
		MatcherAssert.assertThat(businessCompany.getRegistrationPostalCode(), IsEqual.equalTo(null));
		MatcherAssert.assertThat(businessCompany.getRegistrationCountry(), IsEqual.equalTo("CA"));
	}


	@Test
	public void testPopulatePropertyNonEmptyString() {
		IntegrationBusinessCompanyMappingResults results = this.integrationBusinessCompanyMappingService.getBusinessCompanyFromIntegrationBusinessCompany(7888);
		Assertions.assertTrue(results.getValidationList().isEmpty(), results.getValidationList().toString());
		BusinessCompany businessCompany = results.getBusinessCompany();
		Assertions.assertNotNull(businessCompany);
		IntegrationBusinessCompany integrationBusinessCompany = this.integrationBusinessCompanyService.getIntegrationBusinessCompanyByBloombergCompanyId(businessCompany.getBloombergCompanyIdentifier());
		Assertions.assertNotNull(integrationBusinessCompany);
		Assertions.assertFalse(results.isChanged());

		Assertions.assertNull(integrationBusinessCompany.getCompanyFaxNumber());
		Assertions.assertNull(integrationBusinessCompany.getCompanyTelephoneNumber());
		Assertions.assertNull(integrationBusinessCompany.getCompanyWebAddress());

		MatcherAssert.assertThat(businessCompany.getFaxNumber(), IsEqual.equalTo(businessCompany.getFaxNumber()));
		MatcherAssert.assertThat(businessCompany.getPhoneNumber(), IsEqual.equalTo(businessCompany.getPhoneNumber()));
		MatcherAssert.assertThat(businessCompany.getWebAddress(), IsEqual.equalTo(businessCompany.getWebAddress()));

		Assertions.assertNull(businessCompany.getCompanyLegalName());
		MatcherAssert.assertThat(businessCompany.isUltimateParent(), IsEqual.equalTo(integrationBusinessCompany.isUltimateParent()));
		MatcherAssert.assertThat(businessCompany.isPrivateCompany(), IsEqual.equalTo(integrationBusinessCompany.isPrivateCompany()));
		MatcherAssert.assertThat(businessCompany.isAcquiredByParent(), IsEqual.equalTo(integrationBusinessCompany.isAcquiredByParent()));
		Assertions.assertNull(businessCompany.getCountryOfIncorporation());
		Assertions.assertNull(businessCompany.getCountryOfRisk());
		Assertions.assertNull(businessCompany.getStateOfIncorporation());
		Assertions.assertNull(businessCompany.getCompanyCorporateTicker());
		Assertions.assertNull(businessCompany.getLegalEntityIdentifier());
		Assertions.assertNull(businessCompany.getName());
		Assertions.assertNull(businessCompany.getUltimateParentEquityTicker());
		Assertions.assertNull(businessCompany.getLeiEntityStatus());
		Assertions.assertNull(businessCompany.getLeiEntityStartDate());
		Assertions.assertNull(businessCompany.getLeiEntityEndDate());
		Assertions.assertNull(businessCompany.getParent());
		Assertions.assertNull(businessCompany.getObligorBusinessCompany());
		Assertions.assertNull(businessCompany.getAlternateCompanyName());
		Assertions.assertNull(businessCompany.getParentRelationshipType());

		Assertions.assertNull(businessCompany.getAddressLine1());
		Assertions.assertNull(businessCompany.getAddressLine2());
		Assertions.assertNull(businessCompany.getAddressLine3());
		Assertions.assertNull(businessCompany.getCity());
		Assertions.assertNull(businessCompany.getState());
		Assertions.assertNull(businessCompany.getPostalCode());
		Assertions.assertNull(businessCompany.getCountry());

		Assertions.assertNull(businessCompany.getRegistrationAddressLine1());
		Assertions.assertNull(businessCompany.getRegistrationAddressLine2());
		Assertions.assertNull(businessCompany.getRegistrationAddressLine3());
		Assertions.assertNull(businessCompany.getRegistrationCity());
		Assertions.assertNull(businessCompany.getRegistrationState());
		Assertions.assertNull(businessCompany.getRegistrationPostalCode());
		Assertions.assertNull(businessCompany.getRegistrationCountry());
	}
}
