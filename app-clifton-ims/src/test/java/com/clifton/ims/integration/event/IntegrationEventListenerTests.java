package com.clifton.ims.integration.event;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author theodorez
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class IntegrationEventListenerTests {

	@Resource
	private BusinessCompanyService businessCompanyService;

	@Resource
	private MarketDataFieldValueEventListener marketDataFieldValueEventListener;

	@Resource
	private MarketDataSourceService marketDataSourceService;


	@Test
	public void testGetMarketDataSourceForCompany() {
		BusinessCompany company = this.businessCompanyService.getBusinessCompany(1);
		MarketDataSource dataSource = this.marketDataFieldValueEventListener.getMarketDataSourceForCompany(company);
		Assertions.assertNotNull(dataSource);
	}


	//Tests when both child and parent company have data sources the data source for the specified company will win (parent will not override)
	@Test
	public void testGetMarketDataSourceForCompanyWithParent() {
		BusinessCompany company = this.businessCompanyService.getBusinessCompany(4);
		MarketDataSource dataSource = this.marketDataFieldValueEventListener.getMarketDataSourceForCompany(company);
		Assertions.assertNotNull(dataSource);
	}


	//Tests to make sure the logic traverses upwards through the tree structure
	@Test
	public void testGetMarketDataSourceForCompanyParent() {
		MarketDataSource expectedSource = this.marketDataSourceService.getMarketDataSource(new Short("4"));
		BusinessCompany company = this.businessCompanyService.getBusinessCompany(6);
		MarketDataSource dataSource = this.marketDataFieldValueEventListener.getMarketDataSourceForCompany(company);
		Assertions.assertNotNull(dataSource);
		Assertions.assertEquals(expectedSource, dataSource);

		company = this.businessCompanyService.getBusinessCompany(7);
		dataSource = this.marketDataFieldValueEventListener.getMarketDataSourceForCompany(company);
		Assertions.assertNotNull(dataSource);
		Assertions.assertEquals(expectedSource, dataSource);
	}
}
