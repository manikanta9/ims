package com.clifton.ims.billing.invoice.process.schedule.amount.proraters;

import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.billing.definition.BillingDefinition;
import com.clifton.billing.definition.BillingFrequencies;
import com.clifton.billing.definition.BillingSchedule;
import com.clifton.billing.definition.BillingScheduleFrequencies;
import com.clifton.billing.definition.BillingScheduleType;
import com.clifton.billing.invoice.BillingInvoice;
import com.clifton.billing.invoice.BillingInvoiceType;
import com.clifton.billing.invoice.process.schedule.BillingScheduleProcessConfig;
import com.clifton.billing.invoice.process.schedule.amount.proraters.BillingAmountProrater;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BillingAmountForPositionsOnProraterTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BillingAmountForPositionsOnProraterTests {

	@Resource
	private AccountingPositionHandler accountingPositionHandler;

	@Resource
	private ApplicationContext applicationContext;

	private static final Date INVOICE_PERIOD_START_DATE = DateUtils.toDate("07/01/2011");
	private static final Date INVOICE_PERIOD_END_DATE = DateUtils.toDate("09/30/2011");
	private static final Date ADVANCE_INVOICE_PERIOD_START_DATE = DateUtils.toDate("10/01/2011");
	private static final Date ADVANCE_INVOICE_PERIOD_END_DATE = DateUtils.toDate("12/31/2011");
	private static final Date INVOICE_DATE = DateUtils.toDate("09/30/2011");


	@Test
	public void testProrateAmount() {
		BillingAmountProrater prorater = getProrater();

		Mockito.when(
				this.accountingPositionHandler.getClientAccountPositionsOnDays(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class),
						ArgumentMatchers.any(Date.class), ArgumentMatchers.any())).thenReturn(60);

		BillingInvoice invoice = setupBillingInvoice("05/05/1998", null, null, false);
		BillingSchedule schedule = setupSchedule(BillingScheduleFrequencies.MONTHLY, 5000.00, false);

		StringBuilder note = new StringBuilder(16);
		BillingScheduleProcessConfig config = new BillingScheduleProcessConfig(schedule, CollectionUtils.createList(invoice));
		BigDecimal amount = prorater.prorateCalculatedAmount(BigDecimal.valueOf(15000), config, null, note);

		Assertions.assertEquals(9783.0, MathUtils.round(amount, 0).doubleValue(), 0.0d);
		Assertions.assertEquals("Prorated for 60 of 92 days", note.toString().trim());
	}


	@Test
	public void testProrateAmount_InAdvance() {
		BillingAmountProrater prorater = getProrater();

		Mockito.when(
				this.accountingPositionHandler.getClientAccountPositionsOnDays(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class),
						ArgumentMatchers.any(Date.class), ArgumentMatchers.any())).thenReturn(46);

		BillingInvoice invoice = setupBillingInvoice("05/05/1998", null, null, true);
		BillingSchedule schedule = setupSchedule(BillingScheduleFrequencies.MONTHLY, 5000.00, false);

		StringBuilder note = new StringBuilder(16);
		BillingScheduleProcessConfig config = new BillingScheduleProcessConfig(schedule, CollectionUtils.createList(invoice));
		BigDecimal amount = prorater.prorateCalculatedAmount(BigDecimal.valueOf(15000), config, null, note);

		Assertions.assertEquals(7500.0, MathUtils.round(amount, 0).doubleValue(), 0.0d);
		Assertions.assertEquals("Prorated for 46 of 92 days", note.toString().trim());
	}


	@Test
	public void testProrateAmount_AnnualInvoicePeriod() {
		BillingAmountProrater prorater = getProrater();

		Mockito.when(
				this.accountingPositionHandler.getClientAccountPositionsOnDays(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class),
						ArgumentMatchers.any(Date.class), ArgumentMatchers.any())).thenReturn(180);

		BillingInvoice invoice = setupBillingInvoice("05/05/1998", "7/01/1998", null, false);
		BillingSchedule schedule = setupSchedule(BillingScheduleFrequencies.ANNUALLY, 50000.00, true);

		StringBuilder note = new StringBuilder(16);
		BillingScheduleProcessConfig config = new BillingScheduleProcessConfig(schedule, CollectionUtils.createList(invoice));
		BigDecimal amount = prorater.prorateCalculatedAmount(BigDecimal.valueOf(50000.00), config, null, note);

		Assertions.assertEquals(24590.00, MathUtils.round(amount, 0).doubleValue(), 0.0d);
		Assertions.assertEquals("Prorated for 180 of 366 days", note.toString().trim());
	}


	@Test
	public void testProrateAmount_AnnualInvoicePeriod_InAdvance() {
		BillingAmountProrater prorater = getProrater();

		Mockito.when(
				this.accountingPositionHandler.getClientAccountPositionsOnDays(ArgumentMatchers.anyInt(), ArgumentMatchers.any(Date.class),
						ArgumentMatchers.any(Date.class), ArgumentMatchers.any())).thenReturn(200);

		BillingInvoice invoice = setupBillingInvoice("05/05/1998", "7/01/1998", null, true);
		BillingSchedule schedule = setupSchedule(BillingScheduleFrequencies.ANNUALLY, 50000.00, true);

		StringBuilder note = new StringBuilder(16);

		BillingScheduleProcessConfig config = new BillingScheduleProcessConfig(schedule, CollectionUtils.createList(invoice));
		BigDecimal amount = prorater.prorateCalculatedAmount(BigDecimal.valueOf(50000.00), config, null, note);

		Assertions.assertEquals(27322.0, MathUtils.round(amount, 0).doubleValue(), 0.0d);
		Assertions.assertEquals("Prorated for 200 of 366 days", note.toString().trim());
	}


	//////////////////////////////////////////////////////////


	private BillingAmountProrater getProrater() {
		BillingAmountProrater prorater = new BillingAmountForPositionsOnProrater();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(prorater, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return prorater;
	}


	private BillingInvoice setupBillingInvoice(String billingStartDate, String billingSecondYearStartDate, String billingEndDate, boolean advance) {
		BillingInvoice invoice = new BillingInvoice();
		invoice.setInvoicePeriodStartDate((advance ? ADVANCE_INVOICE_PERIOD_START_DATE : INVOICE_PERIOD_START_DATE));
		invoice.setInvoicePeriodEndDate((advance ? ADVANCE_INVOICE_PERIOD_END_DATE : INVOICE_PERIOD_END_DATE));
		invoice.setInvoiceDate(INVOICE_DATE);

		BillingDefinition definition = new BillingDefinition();
		definition.setStartDate(DateUtils.toDate(billingStartDate));
		definition.setSecondYearStartDate(billingSecondYearStartDate == null ? null : DateUtils.toDate(billingSecondYearStartDate));
		definition.setEndDate(billingEndDate == null ? null : DateUtils.toDate(billingEndDate));
		definition.setBillingFrequency(BillingFrequencies.QUARTERLY);
		definition.setInvoiceType(new BillingInvoiceType());
		definition.getInvoiceType().setBillingAmountPrecision((short) 0);

		invoice.setInvoiceType(definition.getInvoiceType());
		InvestmentSecurity usd = new InvestmentSecurity();
		usd.setId(1);
		usd.setSymbol("USD");
		definition.setBillingCurrency(usd);

		invoice.setBillingDefinition(definition);
		return invoice;
	}


	private BillingSchedule setupSchedule(BillingScheduleFrequencies frequency, double amount, boolean annual) {
		BillingSchedule schedule = new BillingSchedule();
		schedule.setScheduleFrequency(frequency);
		schedule.setScheduleAmount(BigDecimal.valueOf(amount));

		BillingScheduleType type = new BillingScheduleType();
		if (annual) {
			type.setAnnualFee(true);
		}
		type.setName("Test Type");
		schedule.setScheduleType(type);

		InvestmentAccount acct = new InvestmentAccount();
		acct.setId(1);
		acct.setNumber("123456");

		schedule.setInvestmentAccount(acct);

		return schedule;
	}


	//////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
