/**
SELECT r.ProductOverlayRunID, a.AccountNumber, a.AccountName, r.BalanceDate FROM ProductOverlayRun r 
INNER JOIN InvestmentAccount a ON r.InvestmentAccountID = a.InvestmentAccountID
ORDER BY BalanceDate DESC
**/

DECLARE @RunID INT
SET @RunID = 189


SET NOCOUNT ON

-- ======================================================================
-- HEADER INFO
-- ======================================================================
SELECT '********** HEADER **********'

SELECT ia.AccountName
	, ia.AccountNumber AS 'Account No.'
	, r.BalanceDate AS 'Close Of'
FROM ProductOverlayRun r 
INNER JOIN InvestmentAccount ia ON r.InvestmentAccountID = ia.InvestmentAccountID
WHERE r.ProductOverlayRunID = @RunID


-- ======================================================================
-- ======================================================================
-- PAGE 1 - EXECUTIVE SUMMARY
-- ======================================================================
-- ======================================================================

SELECT '********** PAGE 1: EXECUTIVE SUMMARY **********'

-- ======================================================================
-- Position Summary
-- ======================================================================

SELECT '********** POSITION SUMMARY **********'
SELECT 
	r.PortfolioTotalValue AS 'Portfolio Value'
	, MathUtil.GetPercentChange(pr.PortfolioTotalValue, r.PortfolioTotalValue) AS 'Daily Change'
FROM ProductOverlayRun r 
LEFT JOIN ProductOverlayRun pr ON pr.ProductOverlayRunID = (SELECT TOP 1 pr2.ProductOverlayRunID FROM ProductOverlayRun pr2 WHERE pr2.InvestmentAccountID = r.InvestmentAccountID AND  pr.BalanceDate = DATEADD(D, -1, r.BalanceDate))
WHERE r.ProductOverlayRunID = @RunID



-- ======================================================================
-- Funded Status
-- ======================================================================
SELECT '********** FUNDED STATUS **********'
SELECT 
	SUM(oac.ActualAllocation) AS 'Assets'
	, PIOS.GetUpdatedValue(MAX(v.ColumnValue), MAX(v3.ColumnValue), MAX(r.BalanceDate), MAX(v2.ColumnValue), MAX(v4.ColumnValue)) AS 'Liabilities'
	, SUM(oac.ActualAllocation) -  PIOS.GetUpdatedValue(MAX(v.ColumnValue), MAX(v3.ColumnValue), MAX(r.BalanceDate), MAX(v2.ColumnValue), MAX(v4.ColumnValue)) AS 'Difference'
	, CASE 
		WHEN SUM(oac.ActualAllocation) = 0 THEN 0
		ELSE (SUM(oac.ActualAllocation)/(SUM(oac.ActualAllocation) -  PIOS.GetUpdatedValue(MAX(v.ColumnValue), MAX(v3.ColumnValue), MAX(r.BalanceDate), MAX(v2.ColumnValue), MAX(v4.ColumnValue)))) * 100
	END AS '% Funded'
FROM ProductOverlayRun r 
INNER JOIN ProductOverlayAssetClass oac ON r.ProductOverlayRunID = oac.ProductOverlayRunID
INNER JOIN InvestmentAccountAssetClass iac ON oac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
INNER JOIN InvestmentAssetClass cl ON iac.InvestmentAssetClassID = cl.InvestmentAssetClassID AND cl.IsMainCash = 0
INNER JOIN InvestmentAccount ia ON r.InvestmentAccountID = ia.InvestmentAccountID
INNER JOIN SystemTable t ON t.TableName = 'InvestmentAccount'
INNER JOIN SystemColumn c ON t.SystemTableID = c.SystemTableID AND c.ColumnName = 'Liability Value'
LEFT JOIN SystemColumnValue v ON c.SystemColumnID = v.SystemColumnID AND v.FKFieldID = ia.InvestmentAccountID
LEFT JOIN SystemColumn c2 ON t.SystemTableID = c2.SystemTableID AND c2.ColumnName = 'Liability Benchmark'
LEFT JOIN SystemColumnValue v2 ON c2.SystemColumnID = v2.SystemColumnID AND v2.FKFieldID = ia.InvestmentAccountID
LEFT JOIN SystemColumn c3 ON t.SystemTableID = c3.SystemTableID AND c3.ColumnName = 'Liability Value Date'
LEFT JOIN SystemColumnValue v3 ON c3.SystemColumnID = v3.SystemColumnID AND v3.FKFieldID = ia.InvestmentAccountID
LEFT JOIN SystemColumn c4 ON t.SystemTableID = c4.SystemTableID AND c4.ColumnName = 'Invert liability value growth based on benchmark'
LEFT JOIN SystemColumnValue v4 ON c4.SystemColumnID = v4.SystemColumnID AND v4.FKFieldID = ia.InvestmentAccountID
WHERE r.ProductOverlayRunID = @RunID




-- ======================================================================	
-- Overlay Index Exposure
-- ======================================================================
SELECT '********** OVERLAY INDEX EXPOSURE **********'
SELECT 
	c.AssetClassName
	, ac.OverlayExposure
FROM ProductOverlayAssetClass ac
INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
WHERE ac.ProductOverlayRunID = @RunID AND ac.ParentProductOverlayAssetClassID IS NULL
ORDER BY iac.AssetClassOrder, c.AssetClassName


SELECT 
	SUM(CASE WHEN ac.OverlayExposure > 0 THEN ac.OverlayExposure ELSE 0 END) AS 'Total Long Index Exposure'
	, SUM(CASE WHEN ac.OverlayExposure < 0 THEN ac.OverlayExposure ELSE 0 END) AS 'Total Short Index Exposure'
	, SUM(CASE WHEN ac.OverlayExposure > 0 THEN ac.OverlayExposure ELSE 0 END) + SUM(CASE WHEN ac.OverlayExposure < 0 THEN ac.OverlayExposure ELSE 0 END) AS 'Net Index Exposure'
	, SUM(CASE WHEN ac.OverlayExposure > 0 THEN ac.OverlayExposure ELSE 0 END) - SUM(CASE WHEN ac.OverlayExposure < 0 THEN ac.OverlayExposure ELSE 0 END) AS 'Gross Index Exposure'
FROM ProductOverlayAssetClass ac
INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
WHERE ac.ProductOverlayRunID = @RunID AND ac.ParentProductOverlayAssetClassID IS NULL AND c.IsMainCash = 0
GROUP BY ac.ProductOverlayRunID

-- ======================================================================
-- Margin Summary
-- ======================================================================
SELECT '********** MARGIN SUMMARY **********'
SELECT a.AccountNumber + ': ' + a.AccountName + CASE WHEN ac.InvestmentAssetClassID IS NOT NULL THEN ' (' + ac.AssetClassName + ')' ELSE '' END AS 'Broker Account'
	, m.OurAccountValue AS 'The Clifton Group Account Value $'
	, MathUtil.GetPercentValue(m.OurAccountValue, m.OverlayTarget) AS 'Clifton - % of Overlay'
	, m.MarginInitialValue AS 'Required Initial Margin'
	, MathUtil.GetPercentValue(m.MarginInitialValue, m.OverlayTarget) AS 'Initial - % of Overlay'
	, m.OurAccountValue - m.MarginInitialValue AS 'Variation Margin Available'
	, MathUtil.GetPercentValue(m.OurAccountValue, m.OverlayTarget) - MathUtil.GetPercentValue(m.MarginInitialValue, m.OverlayTarget) AS 'Variation - % of Overlay'
FROM ProductOverlayMargin m
INNER JOIN InvestmentAccount a ON m.BrokerInvestmentAccountID = a.InvestmentAccountID
LEFT JOIN InvestmentAccountAssetClass iac ON m.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
LEFT JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
WHERE m.ProductOverlayRunID = @RunID

-- ======================================================================
-- Gain/Loss Summary
-- ======================================================================
SELECT '********** GAIN / LOSS SUMMARY **********'
SELECT
	'TODO' AS 'Daily Gain/Loss'
	, 'TODO' AS 'Monthly Gain/Loss'
	, 'TODO' AS 'Yearly Gain/Loss'
	, 'TODO' AS 'Since Inception Gain/Loss'

-- ======================================================================
-- Fund Exposure Summary
-- ======================================================================
SELECT '********** FUND EXPOSURE SUMMARY **********'
SELECT
	c.AssetClassName
	, ac.TargetAllocationPercent AS 'Actual Target'
	, ac.TargetAllocationAdjustedPercent AS 'Adjusted Target'
	, ac.ActualAllocationPercent AS 'Physical Exposure'
	, ac.ActualAllocationPercent - ac.TargetAllocationAdjustedPercent AS 'Physical Deviation From Adj. Target'
	, MathUtil.GetPercentValue(ac.OverlayExposure, r.PortfolioTotalValue) AS 'Overlay Exposure'
	, ac.ActualAllocationPercent + MathUtil.GetPercentValue(ac.OverlayExposure, r.PortfolioTotalValue) AS 'Total Exposure'
	, (ac.ActualAllocationPercent + MathUtil.GetPercentValue(ac.OverlayExposure, r.PortfolioTotalValue)) - ac.TargetAllocationAdjustedPercent AS 'Total Exposure Deviation From Adj. Target'
	, MathUtil.GetPercentValue(ac.RebalanceTriggerMin, r.PortfolioTotalValue) AS 'Min Rebalance Trigger'
	, MathUtil.GetPercentValue(ac.RebalanceTriggerMax, r.PortfolioTotalValue) AS 'Max Rebalance Trigger'
FROM ProductOverlayAssetClass ac
	INNER JOIN ProductOverlayRun r ON ac.ProductOverlayRunID = r.ProductOverlayRunID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
WHERE ac.ProductOverlayRunID = @RunID AND ac.ParentProductOverlayAssetClassID IS NULL
ORDER BY iac.AssetClassOrder, c.AssetClassName

-- ======================================================================
-- Physical Exposure
-- ======================================================================
SELECT '********** PHYSICAL EXPOSURE **********'
SELECT
	c.AssetClassName
	, ac.ActualAllocationPercent AS 'Physical Exposure'
FROM ProductOverlayAssetClass ac
	INNER JOIN ProductOverlayRun r ON ac.ProductOverlayRunID = r.ProductOverlayRunID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
WHERE ac.ProductOverlayRunID = @RunID AND ac.ParentProductOverlayAssetClassID IS NULL
ORDER BY iac.AssetClassOrder

-- ======================================================================
-- Total Exposure Comparison
-- ======================================================================
SELECT '********** TOTAL EXPOSURE COMPARISON **********'
SELECT
	c.AssetClassName
	, ac.ActualAllocationPercent - ac.TargetAllocationAdjustedPercent AS 'Physical Deviation From Adj. Target'
	, MathUtil.GetPercentValue(ac.OverlayExposure, r.PortfolioTotalValue) AS 'Overlay Exposure'
	, (ac.ActualAllocationPercent + MathUtil.GetPercentValue(ac.OverlayExposure, r.PortfolioTotalValue)) - ac.TargetAllocationAdjustedPercent AS 'Total Exposure Deviation From Adj. Target'
FROM ProductOverlayAssetClass ac
	INNER JOIN ProductOverlayRun r ON ac.ProductOverlayRunID = r.ProductOverlayRunID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
WHERE ac.ProductOverlayRunID = @RunID AND ac.ParentProductOverlayAssetClassID IS NULL
ORDER BY iac.AssetClassOrder, c.AssetClassName


-- ======================================================================
-- ======================================================================
-- PAGE 2 - ASSETS SUMMARY
-- ======================================================================
-- ======================================================================
SELECT '********** PAGE 2: ASSETS SUMMARY **********'

SELECT '********** ASSETS SUMMARY (ROLLUP VIEW) **********'
SELECT
	ac.AssetClassName + COALESCE(' - ' + s.InvestmentSecurityName, '') AS 'AssetClassName'
	, CASE WHEN s.InvestmentSecurityID IS NOT NULL THEN MathUtil.GetPercentChange(PIOS.GetSecurityPrice(s.InvestmentSecurityID, DATEADD(day, -1, r.BalanceDate)), PIOS.GetSecurityPrice(s.InvestmentSecurityID, r.BalanceDate)) ELSE NULL END AS 'Asset Class One Day % Change'
	, CASE WHEN s.InvestmentSecurityID IS NOT NULL THEN MathUtil.GetPercentChange(PIOS.GetSecurityPrice(s.InvestmentSecurityID, DATEADD(day, -1, CAST (MONTH(r.BalanceDate) AS VARCHAR(2)) + '/01/' + CAST(YEAR(r.BalanceDate) AS VARCHAR(4)))), PIOS.GetSecurityPrice(s.InvestmentSecurityID, r.BalanceDate)) ELSE NULL END AS 'Asset Class MTD % Change'
	, COALESCE(ma.DisplayName, mc.CompanyName) AS 'Manager Name'
	, m.SecuritiesAllocation AS 'Security Total'
	, m.CashAllocation AS 'Cash'
	, m.SecuritiesAllocation + m.CashAllocation AS 'Total Market Value'
	, m.PreviousDayCashAllocation
	,m.PreviousDaySecuritiesAllocation
	,m.PreviousMonthEndCashAllocation
	,m.PreviousMonthEndSecuritiesAllocation
	, MathUtil.GetPercentChange((m.PreviousDayCashAllocation + m.PreviousDaySecuritiesAllocation), m.CashAllocation + m.SecuritiesAllocation) AS 'One Day % Change'
	, MathUtil.GetPercentChange((m.PreviousMonthEndCashAllocation + m.PreviousMonthEndSecuritiesAllocation), m.CashAllocation + m.SecuritiesAllocation) AS 'MTD % Change'
	, MathUtil.GetPercentValue(m.CashAllocation, m.CashAllocation + m.SecuritiesAllocation) AS 'Cash %'
FROM ProductOverlayManagerAccount m
INNER JOIN ProductOverlayRun r ON m.ProductOverlayRunID = r.ProductOverlayRunID
INNER JOIN InvestmentAccountAssetClass iac ON m.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
INNER JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
INNER JOIN InvestmentManagerAccountAssignment ma ON m.InvestmentManagerAccountAssignmentID = ma.InvestmentManagerAccountAssignmentID
INNER JOIN InvestmentManagerAccount mgr ON ma.InvestmentManagerAccountID = mgr.InvestmentManagerAccountID
INNER JOIN BusinessCompany mc ON mgr.ManagerBusinessCompanyID = mc.BusinessCompanyID
LEFT JOIN InvestmentSecurity s ON iac.BenchmarkInvestmentSecurityID = s.InvestmentSecurityID
WHERE m.ProductOverlayRunID = @RunID AND m.ParentProductOverlayManagerAccountID IS NULL
ORDER BY iac.AssetClassOrder, ac.AssetClassName, COALESCE(ma.DisplayName, mc.CompanyName)

SELECT '********** ASSETS SUMMARY (NON-ROLLUP VIEW) **********'
-- SAME SQL AS ABOVE, EXCEPT REPLACE IN WHERE CLAUSE: AND m.ParentProductOverlayManagerAccountID IS NULL WITH AND m.IsRollupAssetClass = 0

-- ======================================================================
-- Fund Total
-- ======================================================================
SELECT '********** FUND TOTAL **********'
SELECT 
	SUM(m.SecuritiesAllocation) AS 'Security Total'
	, SUM(m.CashAllocation) AS 'Cash'
	, SUM(m.SecuritiesAllocation) + SUM(m.CashAllocation) AS 'Total Market Value'
	, MathUtil.GetPercentChange(SUM(m.PreviousDaySecuritiesAllocation) + SUM(m.PreviousDayCashAllocation), SUM(m.SecuritiesAllocation) + SUM(m.CashAllocation)) AS 'One Day % Change'
	, MathUtil.GetPercentChange(SUM(m.PreviousMonthEndSecuritiesAllocation) + SUM(m.PreviousMonthEndCashAllocation), SUM(m.SecuritiesAllocation) + SUM(m.CashAllocation)) AS 'MTD % Change'
	, MathUtil.GetPercentValue(SUM(m.CashAllocation), SUM(m.SecuritiesAllocation) + SUM(m.CashAllocation)) AS 'Cash %'
FROM ProductOverlayManagerAccount m
WHERE m.ProductOverlayRunID = @RunID AND m.ParentProductOverlayManagerAccountID IS NULL



-- ======================================================================
-- ======================================================================
-- PAGE 3 - EXPOSURE SUMMARY REPORT
-- ======================================================================
-- ======================================================================
SELECT '********** PAGE 3: EXPOSURE SUMMARY REPORT **********'

-- ======================================================================
-- ACTUAL VALUES VIEW 
-- ======================================================================
SELECT '********** ACTUAL VALUES VIEW **********'
SELECT
	c.AssetClassName
	, ac.TargetAllocation AS 'Actual Target'
	, ac.TargetAllocationAdjusted AS 'Adjusted Target'
	, ac.ActualAllocation AS 'Physical Exposure'
	, ac.ActualAllocation - ac.TargetAllocationAdjusted AS 'Physical Deviation From Adj. Target'
	, ac.OverlayExposure AS 'Overlay Exposure'
	, ac.ActualAllocation + ac.OverlayExposure AS 'Total Exposure'
	, (ac.ActualAllocation + ac.OverlayExposure) - ac.TargetAllocationAdjustedPercent AS 'Total Exposure Deviation From Adj. Target'
	, MathUtil.GetPercentValue(ac.RebalanceTriggerMin, r.PortfolioTotalValue) AS 'Min Rebalance Trigger'
	, ac.RebalanceTriggerMax AS 'Max Rebalance Trigger'
FROM ProductOverlayAssetClass ac
	INNER JOIN ProductOverlayRun r ON ac.ProductOverlayRunID = r.ProductOverlayRunID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
WHERE ac.ProductOverlayRunID = @RunID AND ac.ParentProductOverlayAssetClassID IS NULL
ORDER BY iac.AssetClassOrder, c.AssetClassName

-- ======================================================================
-- NOTE PERCENT VIEW SAME AS ON FIRST PAGE
-- ======================================================================

SELECT '********** PERCENT VIEW (SEE PAGE 1 - FUND EXPOSURE SUMMARY) **********'

-- ======================================================================
-- Cash Allocation
-- ======================================================================

SELECT '********** CASH ALLOCATION **********'
SELECT 
	c.AssetClassName
	, COALESCE(ma.DisplayName, mc.CompanyName) AS "Manager Name"
	, SUM(m.CashAllocation) AS 'Cash'
	, SUM(m.PreviousDayCashAllocation) AS 'Previous Day''s Cash'
	, SUM(m.CashAllocation) - SUM(m.PreviousDayCashAllocation) AS 'Difference'
FROM ProductOverlayManagerAccount m
	INNER JOIN InvestmentAccountAssetClass iac ON m.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID 
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
	INNER JOIN InvestmentManagerAccountAssignment ma ON m.InvestmentManagerAccountAssignmentID = ma.InvestmentManagerAccountAssignmentID
	INNER JOIN InvestmentManagerAccount mgr ON ma.InvestmentManagerAccountID = mgr.InvestmentManagerAccountID
	INNER JOIN BusinessCompany mc ON mgr.ManagerBusinessCompanyID = mc.BusinessCompanyID
WHERE m.ProductOverlayRunID = @RunID AND m.ParentProductOverlayManagerAccountID IS NULL
GROUP BY iac.AssetClassOrder, c.AssetClassName, COALESCE(ma.DisplayName, mc.CompanyName)
HAVING SUM(m.CashAllocation) <> 0
ORDER BY iac.AssetClassOrder, c.AssetClassName, COALESCE(ma.DisplayName, mc.CompanyName)



-- ======================================================================
-- ======================================================================
-- PAGE 4 - SYNTHETIC EXPOSURE REPORT
-- ======================================================================
-- ======================================================================
SELECT '********** PAGE 4: SYNTHETIC EXPOSURE **********'

SELECT
	c.AssetClassName
	, rep.ReplicationName
	, i.InvestmentInstrumentName AS 'Instrument Name'
	, s.Symbol AS 'Contract Symbol'
	, r.TargetExposureAdjusted AS 'Overlay Target'
	, PIOS.GetContractValue(r.ProductOverlayAssetClassReplicationID) * COALESCE(r.ActualContracts, 0) AS 'Overlay Exposure' 
	, r.TargetExposureAdjusted - PIOS.GetContractValue(r.ProductOverlayAssetClassReplicationID) * COALESCE(r.ActualContracts, 0) AS 'Overlay Exposure Difference'
	, r.TargetExposureAdjusted / PIOS.GetContractValue(r.ProductOverlayAssetClassReplicationID) AS 'Target Contracts'
	, r.ActualContracts AS 'Actual Contracts'
	, (r.TargetExposureAdjusted / PIOS.GetContractValue(r.ProductOverlayAssetClassReplicationID)) - r.ActualContracts AS 'Difference'
FROM ProductOverlayAssetClassReplication r
	INNER JOIN ProductOverlayAssetClass ac ON r.ProductOverlayAssetClassID = ac.ProductOverlayAssetClassID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
	INNER JOIN InvestmentReplication rep ON r.InvestmentReplicationID = rep.InvestmentReplicationID
	INNER JOIN InvestmentSecurity s ON r.InvestmentSecurityID = s.InvestmentSecurityID
	INNER JOIN InvestmentInstrument i ON i.InvestmentInstrumentID = s.InvestmentInstrumentID
WHERE ac.ProductOverlayRunID = @RunID 
ORDER BY iac.AssetClassOrder, c.AssetClassName, r.InvestmentReplicationID, r.AllocationOrder

-- ======================================================================
-- Cash Exposure Recap
-- ======================================================================

SELECT '********** CASH EXPOSURE RECAP **********'

SELECT
	1, 'Cash Total' As 'Category'
	, SUM(CASE WHEN ac.IsMainCash = 1 THEN a.ActualAllocation ELSE 0 END) As 'Value'
	, MathUtil.GetPercentValue(SUM(CASE WHEN ac.IsMainCash = 1 THEN a.ActualAllocation ELSE 0 END), MAX(r.PortfolioTotalValue)) AS 'Percent'
FROM ProductOverlayAssetClass a 
		INNER JOIN InvestmentAccountAssetClass iac ON a.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
		INNER JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
		INNER JOIN ProductOverlayRun r ON a.ProductOverlayRunID = r.ProductOverlayRunID	
WHERE a.ProductOverlayRunID = @RunID AND a.ParentProductOverlayAssetClassID IS NULL
GROUP BY r.ProductOverlayRunID

UNION


SELECT
	2, 'Net Overlay'
	, SUM(CASE WHEN ac.IsMainCash = 1 THEN 0 ELSE a.OverlayExposure END)
	, MathUtil.GetPercentValue(SUM(CASE WHEN ac.IsMainCash = 1 THEN 0 ELSE a.OverlayExposure END), MAX(r.PortfolioTotalValue)) AS '%'
FROM ProductOverlayAssetClass a 
		INNER JOIN InvestmentAccountAssetClass iac ON a.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
		INNER JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
		INNER JOIN ProductOverlayRun r ON a.ProductOverlayRunID = r.ProductOverlayRunID	
WHERE a.ProductOverlayRunID = @RunID AND a.ParentProductOverlayAssetClassID IS NULL
GROUP BY r.ProductOverlayRunID

UNION

SELECT
	3, 'Cash Target'
	, SUM(CASE WHEN ac.IsCash = 1 THEN a.ActualAllocation ELSE 0 END) - (SUM(a.ManagerCash) + SUM(a.FundCash) + SUM(a.RebalanceCash) + SUM(a.TransitionCash))  '$'
	, MathUtil.GetPercentValue(SUM(CASE WHEN ac.IsCash = 1 THEN a.ActualAllocation ELSE 0 END) - (SUM(a.ManagerCash) + SUM(a.FundCash) + SUM(a.RebalanceCash) + SUM(a.TransitionCash)), MAX(r.PortfolioTotalValue)) AS '%'
FROM ProductOverlayAssetClass a 
	INNER JOIN InvestmentAccountAssetClass iac ON a.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
	INNER JOIN ProductOverlayRun r ON a.ProductOverlayRunID = r.ProductOverlayRunID	
WHERE a.ProductOverlayRunID = @RunID 
GROUP BY r.ProductOverlayRunID
	

UNION

SELECT
	4, 'Cash Exposure'
	, (SUM(CASE WHEN ac.IsMainCash = 1 THEN a.ActualAllocation ELSE 0 END)) - SUM(CASE WHEN ac.IsMainCash = 1 THEN 0 ELSE a.OverlayExposure END) - (SUM(CASE WHEN ac.IsCash = 1 THEN a.ActualAllocation ELSE 0 END) - (SUM(a.ManagerCash) + SUM(a.FundCash) + SUM(a.RebalanceCash) + SUM(a.TransitionCash)))
	, MathUtil.GetPercentValue((SUM(CASE WHEN ac.IsMainCash = 1 THEN a.ActualAllocation ELSE 0 END)) - SUM(CASE WHEN ac.IsMainCash = 1 THEN 0 ELSE a.OverlayExposure END) - (SUM(CASE WHEN ac.IsCash = 1 THEN a.ActualAllocation ELSE 0 END) - (SUM(a.ManagerCash) + SUM(a.FundCash) + SUM(a.RebalanceCash) + SUM(a.TransitionCash))), MAX(r.PortfolioTotalValue)) AS '%'
FROM ProductOverlayAssetClass a 
	INNER JOIN InvestmentAccountAssetClass iac ON a.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
	INNER JOIN ProductOverlayRun r ON a.ProductOverlayRunID = r.ProductOverlayRunID	
WHERE a.ProductOverlayRunID = @RunID 
GROUP BY r.ProductOverlayRunID
ORDER BY 1

/**

Cash Total: (All cash in the report)
Net Overlay (Sum of Overlay Exposure)
Cash Target (Difference of Cash Total and Overlay Target)  This will most of the time be $0 but not always when the client tells us to not invest some cash
Cash Exposure: (This is the Cash Total minus Net Overlay minus Cash Target)  We need this in dollar and percent.  The percent will be based off the total value of the entire portfolio.

**/




-- ======================================================================
-- ======================================================================
-- PAGE 5 - SYNTHETIC EXPOSURE REPORT
-- ======================================================================
-- ======================================================================

SELECT '********** PAGE 5: SYNTHETIC EXPOSURE **********'

-- ======================================================================
-- Long Positions
-- ======================================================================
SELECT '********** LONG POSITIONS **********'
SELECT
	  s.InvestmentSecurityName AS 'Contract'
	, SUM(r.ActualContracts) AS 'Quantity'
	, SUM(r.ActualContracts * PIOS.GetContractValue(r.ProductOverlayAssetClassReplicationID)) AS 'Accounting Notional'
FROM ProductOverlayAssetClassReplication r
	INNER JOIN ProductOverlayAssetClass ac ON r.ProductOverlayAssetClassID = ac.ProductOverlayAssetClassID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
	INNER JOIN InvestmentReplication rep ON r.InvestmentReplicationID = rep.InvestmentReplicationID
	INNER JOIN InvestmentSecurity s ON r.InvestmentSecurityID = s.InvestmentSecurityID
WHERE ac.ProductOverlayRunID = @RunID 
GROUP BY s.InvestmentSecurityName
HAVING SUM(r.ActualContracts) > 0


-- ======================================================================
-- Short Positions
-- ======================================================================
SELECT '********** SHORT POSITIONS **********'
SELECT
	  s.InvestmentSecurityName AS 'Contract'
	, SUM(r.ActualContracts) AS 'Quantity'
	, SUM(r.ActualContracts * PIOS.GetContractValue(r.ProductOverlayAssetClassReplicationID)) AS 'Accounting Notional'
FROM ProductOverlayAssetClassReplication r
	INNER JOIN ProductOverlayAssetClass ac ON r.ProductOverlayAssetClassID = ac.ProductOverlayAssetClassID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
	INNER JOIN InvestmentReplication rep ON r.InvestmentReplicationID = rep.InvestmentReplicationID
	INNER JOIN InvestmentSecurity s ON r.InvestmentSecurityID = s.InvestmentSecurityID
WHERE ac.ProductOverlayRunID = @RunID 
GROUP BY s.InvestmentSecurityName
HAVING SUM(r.ActualContracts) < 0

-- ======================================================================
-- Variation Margin Scenario Test
-- ======================================================================
SELECT '********** TODO VARIATION MARGIN SCENARIO TEST **********'


-- ======================================================================
-- Historic Portfolio Results
-- ======================================================================
SELECT '********** TODO HISTORIC PORTFOLIO RESULTS **********'


-- ======================================================================
-- Margin Summary
-- ======================================================================
SELECT '********** MARGIN SUMMARY **********'
SELECT a.AccountNumber + ': ' + a.AccountName + CASE WHEN ac.InvestmentAssetClassID IS NOT NULL THEN ' (' + ac.AssetClassName + ')' ELSE '' END AS 'Broker Account'
	, m.OurAccountValue AS 'The Clifton Group Account Value $'
	, MathUtil.GetPercentValue(m.OurAccountValue, m.OverlayTarget) AS 'Clifton - % of Overlay'
	, m.MarginInitialValue AS 'Required Initial Margin'
	, MathUtil.GetPercentValue(m.MarginInitialValue, m.OverlayTarget) AS 'Initial - % of Overlay'
	, m.OurAccountValue - m.MarginInitialValue AS 'Variation Margin Available'
	, MathUtil.GetPercentValue(m.OurAccountValue, m.OverlayTarget) - MathUtil.GetPercentValue(m.MarginInitialValue, m.OverlayTarget) AS 'Variation - % of Overlay'
	, m.MarginRecommendedValue AS 'Recommended Variation Margin'
	, MathUtil.GetPercentValue(m.MarginRecommendedValue, m.OverlayTarget) AS 'Recommended Variation - % of Overlay'
	, m.OurAccountValue - m.MarginInitialValue - m.MarginRecommendedValue AS 'Surplus Variation Margin'
	, MathUtil.GetPercentValue(m.OurAccountValue - m.MarginInitialValue - m.MarginRecommendedValue, m.OverlayTarget) AS 'Surplus Variation Margin - % of Overlay'
FROM ProductOverlayMargin m
INNER JOIN InvestmentAccount a ON m.BrokerInvestmentAccountID = a.InvestmentAccountID
LEFT JOIN InvestmentAccountAssetClass iac ON m.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
LEFT JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
WHERE m.ProductOverlayRunID = @RunID



SELECT a.AccountNumber + ': ' + a.AccountName + CASE WHEN ac.InvestmentAssetClassID IS NOT NULL THEN ' (' + ac.AssetClassName + ')' ELSE '' END AS 'Broker Account'
	  , m.BrokerCollateralValue AS 'Posted Broker Collateral'
	  , m.MarginInitialValue AS 'Required Initial Margin'
	  , m.BrokerCollateralValue - m.MarginInitialValue AS 'Excess Broker Collateral'
FROM ProductOverlayMargin m
INNER JOIN InvestmentAccount a ON m.BrokerInvestmentAccountID = a.InvestmentAccountID
LEFT JOIN InvestmentAccountAssetClass iac ON m.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
LEFT JOIN InvestmentAssetClass ac ON iac.InvestmentAssetClassID = ac.InvestmentAssetClassID
WHERE m.ProductOverlayRunID = @RunID





-- ======================================================================
-- Gain/Loss Summary
-- ======================================================================
SELECT '********** GAIN / LOSS SUMMARY **********'

SELECT
	  c.AssetClassName
	, s.InvestmentSecurityName
	, 'TODO' AS 'Dollar Gain/Loss'
	, 'TODO' AS 'Monthly Gain/Loss'
	, 'TODO' AS 'Yearly Gain/Loss'
	, 'TODO' AS 'Since Inception Gain/Loss'
FROM ProductOverlayAssetClassReplication r
	INNER JOIN ProductOverlayAssetClass ac ON r.ProductOverlayAssetClassID = ac.ProductOverlayAssetClassID
	INNER JOIN InvestmentAccountAssetClass iac ON ac.InvestmentAccountAssetClassID = iac.InvestmentAccountAssetClassID
	INNER JOIN InvestmentAssetClass c ON iac.InvestmentAssetClassID = c.InvestmentAssetClassID
	INNER JOIN InvestmentReplication rep ON r.InvestmentReplicationID = rep.InvestmentReplicationID
	INNER JOIN InvestmentSecurity s ON r.InvestmentSecurityID = s.InvestmentSecurityID
WHERE ac.ProductOverlayRunID = @RunID 
ORDER BY iac.AssetClassOrder, c.AssetClassName




-- ======================================================================
-- END
-- ======================================================================


SET NOCOUNT OFF
