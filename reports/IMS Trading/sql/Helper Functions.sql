
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'MathUtil')
BEGIN
	EXECUTE ('CREATE SCHEMA MathUtil');
END

GO


IF OBJECT_ID (N'MathUtil.GetPercentValue', N'FN') IS NOT NULL
    DROP FUNCTION MathUtil.GetPercentValue;

GO

CREATE FUNCTION [MathUtil].[GetPercentValue]	(

	 @Value Decimal(28,12)
	,@Total Decimal(28,12)

) RETURNS Decimal(28,12) AS


BEGIN

DECLARE
	  @PercentValue Decimal(28,12)
	  SET @PercentValue = 0
	  
	  IF (NULLIF(@Value,0) != 0 AND NULLIF(@Total,0) != 0)
	  BEGIN
		SET @PercentValue = (@Value * 100) / @Total
	  END
	  

RETURN
	 
	@PercentValue
 
END

GO

IF OBJECT_ID (N'MathUtil.GetPercentChange', N'FN') IS NOT NULL
    DROP FUNCTION MathUtil.GetPercentChange;


GO

CREATE FUNCTION [MathUtil].[GetPercentChange]	(

	 @FromValue Decimal(28,12)
	,@ToValue Decimal(28,12)

) RETURNS Decimal(28,12) AS


BEGIN

DECLARE
	  @PercentValue Decimal(28,12)
	  
	  IF (@ToValue IS NULL OR @ToValue = 0)
	  BEGIN
		SET @PercentValue = 0
	  END 
	  
	  IF (@FromValue IS NULL OR @FromValue = 0)
	  BEGIN
		SET @PercentValue = 100
	  END 
	  
	  IF (NULLIF(@FromValue,0) != 0 AND NULLIF(@ToValue,0) != 0)
	  BEGIN
		SET @PercentValue = (@ToValue * 100) / @FromValue
		SET @PercentValue = @Percentvalue - 100
	  END

RETURN
	 
	@PercentValue
 
END

GO


IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'Calendar')
BEGIN
	EXECUTE ('CREATE SCHEMA Calendar');
END

GO


IF OBJECT_ID (N'Calendar.IsBusinessDay', N'FN') IS NOT NULL
    DROP FUNCTION Calendar.IsBusinessDay;

GO

CREATE FUNCTION [Calendar].[IsBusinessDay]	(

	 @Date DATETIME,
	 @CalendarID INT
	

) RETURNS BIT AS


BEGIN
	DECLARE @BusinessDay BIT 
	
	SELECT @BusinessDay = 
		CASE WHEN DATEPART(dw, @Date) IN (1, 7) THEN 0
		ELSE 1
	END	
	
	IF (@BusinessDay = 1)
	BEGIN
	
		IF (@CalendarID IS NULL)
		BEGIN
			SELECT @CalendarID = CalendarID FROM Calendar WHERE IsDefaultSystemCalendar = 1
		END
	
		DECLARE @CalendarDayID INT
		SELECT @CalendarDayID = CalendarDayID FROM CalendarDay WHERE @Date >= StartDate AND @Date <= EndDate
	
		SELECT @BusinessDay = 
			CASE WHEN EXISTS (SELECT * FROM CalendarHolidayDay WHERE CalendarID = @CalendarID AND CalendarDayID = @CalendarDayID) THEN 0
			ELSE 1 END
	END
	
	
RETURN
	 
	@BusinessDay
 
END

GO





IF OBJECT_ID (N'Calendar.GetBusinessDayFrom', N'FN') IS NOT NULL
    DROP FUNCTION Calendar.GetBusinessDayFrom;

GO

CREATE FUNCTION [Calendar].[GetBusinessDayFrom]	(

	 @Date DATETIME,
	 @CalendarID INT,
	 @DaysFrom INT
	

) RETURNS DATETIME AS


BEGIN
	DECLARE @BusinessDayDate DATETIME
	
	IF (@DaysFrom = 0)
	BEGIN
		SET @BusinessDayDate = @Date
	END
	ELSE 
	BEGIN
		DECLARE @Increment INT
		SELECT @Increment = CASE WHEN @DaysFrom > 0 THEN 1 ELSE -1 END
	
		DECLARE @CurrentDays INT
		SET @CurrentDays = 0
		
		SET @BusinessDayDate = @Date
		WHILE (@DaysFrom != @CurrentDays)
		BEGIN
			SET @BusinessDayDate = DATEADD(day, @Increment, @BusinessDayDate)
			IF (Calendar.IsBusinessDay(@Date, @CalendarID) = 1)
			BEGIN
				SET @CurrentDays = @CurrentDays + @Increment
			END
		END
	END
	
	
RETURN
	 
	@BusinessDayDate
 
END

GO
	

	

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'PIOS')
BEGIN
	EXECUTE ('CREATE SCHEMA PIOS');
END

GO


IF OBJECT_ID (N'PIOS.GetContractValue', N'FN') IS NOT NULL
    DROP FUNCTION PIOS.GetContractValue;

GO

CREATE FUNCTION [PIOS].[GetContractValue]	(

	 @ProductOverlayAssetClassReplicationID INT

) RETURNS Decimal(28,12) AS


BEGIN

	DECLARE @ContractValue Decimal(28,12)
	SET @ContractValue = 0

	-- Equity Future: If Syn Adj Factor Use Formula: (Multiplier * Index Price * 1)/Synthetic Adjustment Factor
	-- Fixed Income Future: ((Future Price * Multiplier) * (Contract Duration / Benchmark Duration))
	-- Others: Currency Future (Security Price * Price Multiplier)
	-- ALL MULTIPLE CALCULATED VALUE * EXCHANGE RATE
	SELECT @ContractValue = 
		r.ExchangeRate * (
			CASE WHEN repT.ReplicationTypeName = 'Equity Replication' THEN ((i.PriceMultiplier * r.UnderlyingSecurityPrice) / r.SyntheticAdjustmentFactor) 
			WHEN repT.ReplicationTypeName = 'Fixed Income Replication' THEN ((r.Duration / a.BenchmarkDuration) * r.SecurityPrice * i.PriceMultiplier)
			ELSE (r.SecurityPrice * i.PriceMultiplier) END)
		FROM ProductOverlayAssetClassReplication r
		INNER JOIN InvestmentReplication rep ON r.InvestmentReplicationID = rep.InvestmentReplicationID
		INNER JOIN InvestmentReplicationType repT ON rep.InvestmentReplicationTypeID = repT.InvestmentReplicationTypeID
		INNER JOIN InvestmentSecurity s ON r.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID
		INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID
		INNER JOIN ProductOverlayAssetClass a ON r.ProductOverlayAssetClassID = a.ProductOverlayAssetClassID
		LEFT JOIN InvestmentSecurity us ON r.UnderlyingInvestmentSecurityID = us.InvestmentSecurityID
		WHERE r.ProductOverlayAssetClassReplicationID = @ProductOverlayAssetClassReplicationID


RETURN
	 
	@ContractValue
 
END

GO




IF OBJECT_ID (N'PIOS.GetSecurityPrice', N'FN') IS NOT NULL
    DROP FUNCTION PIOS.GetSecurityPrice;

GO

CREATE FUNCTION [PIOS].[GetSecurityPrice]	(

	 @SecurityID INT
	 , @PriceDate DATETIME

) RETURNS Decimal(28,12) AS


BEGIN

	DECLARE @SecurityPrice Decimal(28,12)
	
	-- Clear time 
	SET @PriceDate = CAST(FLOOR(CAST(@PriceDate AS FLOAT)) AS DATETIME)
	
	
	
	SELECT @SecurityPrice = 
		v.MeasureValue
	FROM MarketDataValue v
	INNER JOIN MarketDataField f ON v.MarketDataFieldID = f.MarketDataFieldID
	WHERE f.DataFieldName = 'Settlement Price' AND v.MeasureDate = @PriceDate AND v.InvestmentSecurityID = @SecurityID
	ORDER BY v.MeasureTime DESC
	
	IF (@SecurityPrice IS NULL) 
	BEGIN
		SELECT @SecurityPrice = 
			v.MeasureValue
		FROM MarketDataValue v
		INNER JOIN MarketDataField f ON v.MarketDataFieldID = f.MarketDataFieldID
		WHERE f.DataFieldName = 'Last Trade Price' AND v.MeasureDate = @PriceDate AND v.InvestmentSecurityID = @SecurityID
		ORDER BY v.MeasureTime DESC
	END
	
	
	-- Check if a Holiday, and if so, get the Previous Business Day and get the price on that date
	IF (@SecurityPrice IS NULL) 
	BEGIN
		DECLARE @ExchangeID INT
		SELECT @ExchangeID = i.InvestmentExchangeID FROM InvestmentInstrument i INNER JOIN InvestmentSecurity s ON i.InvestmentInstrumentID = s.InvestmentInstrumentID WHERE InvestmentSecurityID = @SecurityID
		
		DECLARE @CalendarID INT
		SELECT @CalendarID = CalendarID FROM InvestmentExchange WHERE InvestmentExchangeID = @ExchangeID
		
		IF (Calendar.isBusinessDay(@PriceDate, @CalendarID) = 0)
		BEGIN
			SET @PriceDate = Calendar.getBusinessDayFrom(@PriceDate, @CalendarID, -1)
			SET @SecurityPrice = PIOS.GetSecurityPrice(@SecurityId, @PriceDate)
		END
	END
	
RETURN
	 
	@SecurityPrice
 
END

GO







IF OBJECT_ID (N'PIOS.GetUpdatedValue', N'FN') IS NOT NULL
    DROP FUNCTION PIOS.GetUpdatedValue;

GO

CREATE FUNCTION [PIOS].[GetUpdatedValue]	(

	 @Value DECIMAL(28,12) = 0
	 , @ValueDate DATETIME = NULL
	 , @ToDate DATETIME = NULL
	 , @SecurityID INT = NULL
	 , @InvertValue BIT = 0

) RETURNS Decimal(28,12) AS


BEGIN

	DECLARE @ValueAdjusted DECIMAL(28,12) = 0
	
	IF (@SecurityID IS NOT NULL AND @ValueDate IS NOT NULL AND @ToDate IS NOT NULL)
	BEGIN
		DECLARE @PercentChange DECIMAL(28,12)
		SET @PercentChange = MathUtil.GetPercentChange(PIOS.GetSecurityPrice(@SecurityID, @ValueDate), PIOS.GetSecurityPrice(@SecurityID, @ToDate))
		
		IF (@InvertValue = 1) 
		BEGIN
			SET @PercentChange = @PercentChange * -1
		END
		
		SET @ValueAdjusted = @ValueAdjusted * (@PercentChange/100)
	
	END
		
	
	
RETURN
	 
	COALESCE(@Value,0) + COALESCE(@ValueAdjusted,0)
 
END

GO




