﻿<?xml version="1.0" encoding="utf-8"?>
<Report xmlns:rd="http://schemas.microsoft.com/SQLServer/reporting/reportdesigner" xmlns="http://schemas.microsoft.com/sqlserver/reporting/2008/01/reportdefinition">
  <DataSources>
    <DataSource Name="CliftonIMS">
      <DataSourceReference>CliftonIMS</DataSourceReference>
      <rd:DataSourceID>db7a1d24-aa63-4434-a612-9ff69f888654</rd:DataSourceID>
      <rd:SecurityType>None</rd:SecurityType>
    </DataSource>
  </DataSources>
  <DataSets>
    <DataSet Name="ExposureLimit">
      <Fields>
        <Field Name="ExposureLimit">
          <DataField>ExposureLimit</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
        <Field Name="Exposure">
          <DataField>Exposure</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
        <Field Name="Total">
          <DataField>Total</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
      </Fields>
      <Query>
        <DataSourceName>CliftonIMS</DataSourceName>
        <CommandText>SELECT
      r.PortfolioTotalValue*@ExposurePercent/100 AS ExposureLimit
      , SUM(poac.ActualAllocationAdjusted + poac.OverlayExposure) AS Exposure
      , r.PortfolioTotalValue*@ExposurePercent/100 - SUM(poac.ActualAllocationAdjusted + poac.OverlayExposure) AS Total
FROM ProductOverlayAssetClass poac
      INNER JOIN PortfolioRun r ON r.PortfolioRunID = poac.ProductOverlayRunID
      INNER JOIN InvestmentReplication ir ON (poac.PrimaryInvestmentReplicationID = ir.InvestmentReplicationID OR poac.SecondaryInvestmentReplicationID = ir.InvestmentReplicationID)
WHERE r.PortfolioRunID = @RunID AND ir.InvestmentReplicationTypeID = @InvestmentReplicationTypeID
GROUP BY r.PortfolioTotalValue</CommandText>
        <QueryParameters>
          <QueryParameter Name="@RunID">
            <Value>=Parameters!RunID.Value</Value>
          </QueryParameter>
          <QueryParameter Name="@ExposurePercent">
            <Value>=Parameters!PercentOfTPV.Value</Value>
          </QueryParameter>
          <QueryParameter Name="@InvestmentReplicationTypeID">
            <Value>=Parameters!InvestmentReplicationTypeID.Value</Value>
          </QueryParameter>
        </QueryParameters>
        <rd:UseGenericDesigner>true</rd:UseGenericDesigner>
      </Query>
    </DataSet>
  </DataSets>
  <Body>
    <ReportItems>
      <Tablix Name="tblPositions">
        <TablixBody>
          <TablixColumns>
            <TablixColumn>
              <Width>2.54375in</Width>
            </TablixColumn>
            <TablixColumn>
              <Width>1.5in</Width>
            </TablixColumn>
          </TablixColumns>
          <TablixRows>
            <TablixRow>
              <Height>0.18in</Height>
              <TablixCells>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="AssetClassName">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>=Format(Parameters!PercentOfTPV.Value, "00.#") + "% of TPV"</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <FontWeight>Bold</FontWeight>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Left</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>AssetClassName</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <ColSpan>2</ColSpan>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
                <TablixCell />
              </TablixCells>
            </TablixRow>
            <TablixRow>
              <Height>0.18in</Height>
              <TablixCells>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="Textbox42">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>=Parameters!ExposureName.Value + " Exposure Limit"</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Left</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>Textbox42</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="ExposureLimit">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>=Fields!ExposureLimit.Value</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <Format>#,##</Format>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Right</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>ExposureLimit</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
              </TablixCells>
            </TablixRow>
            <TablixRow>
              <Height>0.18in</Height>
              <TablixCells>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="Textbox44">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>=Parameters!ExposureName.Value + " Exposure"</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Left</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>Textbox44</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="Exposure">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>=Fields!Exposure.Value</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <Format>#,##</Format>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Right</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>Exposure</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
              </TablixCells>
            </TablixRow>
            <TablixRow>
              <Height>0.18in</Height>
              <TablixCells>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="Textbox46">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>Total</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Left</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>Textbox46</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
                <TablixCell>
                  <CellContents>
                    <Textbox Name="Total">
                      <CanGrow>true</CanGrow>
                      <KeepTogether>true</KeepTogether>
                      <Paragraphs>
                        <Paragraph>
                          <TextRuns>
                            <TextRun>
                              <Value>=Fields!Total.Value</Value>
                              <Style>
                                <FontFamily>Calibri</FontFamily>
                                <FontSize>7pt</FontSize>
                                <Format>#,##</Format>
                                <Color>#010101</Color>
                              </Style>
                            </TextRun>
                          </TextRuns>
                          <Style>
                            <TextAlign>Right</TextAlign>
                          </Style>
                        </Paragraph>
                      </Paragraphs>
                      <rd:DefaultName>Total</rd:DefaultName>
                      <Style>
                        <Border>
                          <Style>None</Style>
                        </Border>
                        <PaddingLeft>2pt</PaddingLeft>
                        <PaddingRight>4pt</PaddingRight>
                        <PaddingTop>2pt</PaddingTop>
                        <PaddingBottom>2pt</PaddingBottom>
                      </Style>
                    </Textbox>
                    <rd:Selected>true</rd:Selected>
                  </CellContents>
                </TablixCell>
              </TablixCells>
            </TablixRow>
          </TablixRows>
        </TablixBody>
        <TablixColumnHierarchy>
          <TablixMembers>
            <TablixMember />
            <TablixMember />
          </TablixMembers>
        </TablixColumnHierarchy>
        <TablixRowHierarchy>
          <TablixMembers>
            <TablixMember>
              <TablixMembers>
                <TablixMember>
                  <TablixMembers>
                    <TablixMember>
                      <Group Name="table1_Details_Group">
                        <DataElementName>Detail</DataElementName>
                      </Group>
                      <TablixMembers>
                        <TablixMember />
                        <TablixMember />
                        <TablixMember />
                        <TablixMember />
                      </TablixMembers>
                      <DataElementName>Detail_Collection</DataElementName>
                      <DataElementOutput>Output</DataElementOutput>
                      <KeepTogether>true</KeepTogether>
                    </TablixMember>
                  </TablixMembers>
                </TablixMember>
              </TablixMembers>
            </TablixMember>
          </TablixMembers>
        </TablixRowHierarchy>
        <RepeatRowHeaders>true</RepeatRowHeaders>
        <KeepTogether>true</KeepTogether>
        <DataSetName>ExposureLimit</DataSetName>
        <Top>0.2in</Top>
        <Height>0.72in</Height>
        <Width>4.04375in</Width>
        <Style>
          <Border>
            <Style>Solid</Style>
          </Border>
        </Style>
      </Tablix>
      <Textbox Name="textbox25">
        <CanGrow>true</CanGrow>
        <KeepTogether>true</KeepTogether>
        <Paragraphs>
          <Paragraph>
            <TextRuns>
              <TextRun>
                <Value>=Parameters!ExposureName.Value + " Exposure Limit"</Value>
                <Style>
                  <FontFamily>Calibri</FontFamily>
                  <FontSize>8pt</FontSize>
                  <FontWeight>Bold</FontWeight>
                  <Color>#010101</Color>
                </Style>
              </TextRun>
            </TextRuns>
            <Style />
          </Paragraph>
        </Paragraphs>
        <rd:DefaultName>textbox1</rd:DefaultName>
        <Height>0.14in</Height>
        <Width>4.04375in</Width>
        <ZIndex>1</ZIndex>
        <Style>
          <Border>
            <Color>#010101</Color>
            <Style>None</Style>
          </Border>
          <BottomBorder>
            <Style>Solid</Style>
          </BottomBorder>
          <PaddingLeft>4pt</PaddingLeft>
          <PaddingRight>2pt</PaddingRight>
        </Style>
      </Textbox>
    </ReportItems>
    <Height>0.92in</Height>
    <Style />
  </Body>
  <ReportParameters>
    <ReportParameter Name="RunID">
      <DataType>String</DataType>
      <DefaultValue>
        <Values>
          <Value>3713</Value>
        </Values>
      </DefaultValue>
      <Prompt>Run ID</Prompt>
    </ReportParameter>
    <ReportParameter Name="PercentOfTPV">
      <DataType>Float</DataType>
      <DefaultValue>
        <Values>
          <Value>75</Value>
        </Values>
      </DefaultValue>
      <Prompt>Percent of TPV</Prompt>
    </ReportParameter>
    <ReportParameter Name="InvestmentReplicationTypeID">
      <DataType>String</DataType>
      <DefaultValue>
        <Values>
          <Value>3</Value>
        </Values>
      </DefaultValue>
      <Prompt>Investment Replication Type ID</Prompt>
    </ReportParameter>
    <ReportParameter Name="ExposureName">
      <DataType>String</DataType>
      <DefaultValue>
        <Values>
          <Value>Equity</Value>
        </Values>
      </DefaultValue>
      <Prompt>ExposureName</Prompt>
    </ReportParameter>
  </ReportParameters>
  <Width>4.04375in</Width>
  <Page>
    <PageHeight>8.5in</PageHeight>
    <PageWidth>11in</PageWidth>
    <LeftMargin>0.75in</LeftMargin>
    <RightMargin>0.75in</RightMargin>
    <BottomMargin>0.1in</BottomMargin>
    <Style />
  </Page>
  <Language>en-US</Language>
  <ConsumeContainerWhitespace>true</ConsumeContainerWhitespace>
  <rd:ReportID>589e7982-48bf-4895-bcdb-a183525b5170</rd:ReportID>
  <rd:ReportUnitType>Inch</rd:ReportUnitType>
</Report>