﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportViewer.aspx.cs" Inherits="IMSReportServer.ReportViewer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE HTML>
<html>
    <head id="headID" runat="server">
        <title>Report Viewer</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <link href="ReportServer.css" rel="stylesheet" type="text/css" />
        <style>
            table#ReportViewerControl_fixedTable{
                width: 100%;
            }
        </style>
    </head>
    <body>
        <form runat="server" ID="ReportViewerForm">
             <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="Release"></asp:ScriptManager>
             <div style="height:100vh; width: 100vw">
                <rsweb:ReportViewer  ID="ReportViewerControl" runat="server"  Height="100%"  Width="100%" ShowCredentialPrompts="false" BackColor="#DEE7F2" BorderColor="#A4B7D8" SplitterBackColor="164, 183, 216" 
                    AsyncRendering="False" SizeToReportContent="true" ValidateRequestMode="Disabled"/>
             </div>
        </form>
    </body>
</html>
