﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using IMSReportServer.Code.Report;

namespace IMSReportServer.Code.Data
{
	public class ReportComponent
	{
		public int ID { get; set; }
		public string ComponentName { get; set; }
		public string ComponentDescription { get; set; }
		public string ExternalReportName { get; set; }
		public bool PageBreakBefore { get; set; }
		public int ComponentOrder { get; set; }
		public int ReportTemplateComponentID { get; set; }
		public double SubReportWidth { get; set; }
		public double? TopMargin { get; set; }
		public bool PrivateComponent { get; set; }

		public static List<ReportComponent> LoadComponents(ReportParameters parameters)
		{
			String sql = @"SELECT c.ReportComponentID, tc.*, c.IsPageBreakBefore, c.IsPrivateComponent,c.ComponentOrder,rt.SubReportWidth, c.TopMargin
				FROM dbo.ReportComponent c
					INNER JOIN dbo.ReportTemplateComponent tc ON c.ReportTemplateComponentID = tc.ReportTemplateComponentID
					INNER JOIN dbo.ReportTemplate rt ON rt.ReportTemplateID = tc.ReportTemplateID
					INNER JOIN dbo.ReportTemplateComponentType tct ON tc.ReportTemplateComponentTypeID = tct.ReportTemplateComponentTypeID
				WHERE ReportID = " + parameters.ReportId + " AND ComponentTypeName='Standard Component' {0} ORDER BY ComponentOrder ASC";

			String filters = "";

			if (parameters.OrderList != null && parameters.OrderList.Length > 0)
				filters = " AND ComponentOrder IN (" + String.Join(",", parameters.OrderList) + ")";
			else
				filters = parameters.OrderStart == -1 || parameters.OrderStop == -1 ? String.Empty : (" AND ComponentOrder BETWEEN " + parameters.OrderStart + " AND " + parameters.OrderStop);

			if (parameters.ExcludedComponentList != null && parameters.ExcludedComponentList.Length > 0)
				filters += " AND ReportComponentID NOT IN (" + String.Join(",", parameters.ExcludedComponentList) + ")";

			sql = String.Format(sql, filters);
			return LoadComponents(parameters.ReportId.Value, sql);
		}

		private static List<ReportComponent> LoadComponents(int reportID, String sql)
		{
			List<ReportComponent> components = new List<ReportComponent>();

			SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
			SqlCommand cmd = new SqlCommand(sql);
			IDataReader reader = null;
			try
			{
				cmd.Connection = conn;
				conn.Open();

				reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					ReportComponent rc = new ReportComponent();
					rc.ID = Convert.ToInt32(reader["ReportComponentID"]);
					rc.ComponentName = reader["ComponentDescription"].ToString();
					rc.ComponentDescription = reader["ComponentDescription"].ToString();
					rc.ExternalReportName = reader["ExternalReportName"].ToString();
					rc.PageBreakBefore = Convert.ToBoolean(reader["IsPageBreakBefore"]);
					rc.ComponentOrder = Convert.ToInt32(reader["ComponentOrder"]);
					rc.ReportTemplateComponentID = Convert.ToInt32(reader["ReportTemplateComponentID"]);
					rc.SubReportWidth = Convert.ToDouble(reader["SubReportWidth"]);
					rc.PageBreakBefore = Convert.ToBoolean(reader["IsPageBreakBefore"]);
					rc.PrivateComponent = Convert.ToBoolean(reader["IsPrivateComponent"]);

					if (reader["TopMargin"] != DBNull.Value)
						rc.TopMargin = Convert.ToDouble(reader["TopMargin"]);

					components.Add(rc);
				}
			}
			finally
			{
				if (reader != null)
					reader.Close();
				if (conn != null)
					conn.Close();
			}

			return components;
		}
	}

	public class ReportComponentTemplateParameter
	{
		public string ParameterName { get; set; }
		public string TemplateParameterName { get; set; }
		public string ParameterDescription { get; set; }
		public bool IsStaticValue { get; set; }
		public string Value { get; set; }

		public static List<ReportComponentTemplateParameter> LoadParameters(int reportTemplateComponentID, int reportComponentID)
		{
			List<ReportComponentTemplateParameter> parameters = new List<ReportComponentTemplateParameter>();

			SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectionString"]);
			SqlCommand cmd = new SqlCommand(@"SELECT rtc.[ReportTemplateComponentParameterID]
				  ,rtc.[ReportTemplateComponentID]
				  ,rtc.[ParameterName]
				  ,rtc.[TemplateParameterName]
				  ,rtc.[ParameterDescription]
				  ,rtc.[CreateUserID]
				  ,rtc.[CreateDate]
				  ,rtc.[UpdateUserID]
				  ,rtc.[UpdateDate]
				  ,rtc.[rv]
				  ,rtc.[IsStaticValue]
				  ,Value = coalesce(tc.ParameterValue, [DefaultValue])
			FROM dbo.ReportTemplateComponentParameter rtc
				LEFT JOIN ReportComponentParameter tc ON tc.ReportTemplateComponentParameterID = rtc.ReportTemplateComponentParameterID AND tc.ReportComponentID = " + reportComponentID +
			"WHERE rtc.ReportTemplateComponentID = " + reportTemplateComponentID);

			IDataReader reader = null;
			try
			{
				cmd.Connection = conn;
				conn.Open();

				reader = cmd.ExecuteReader();
				while (reader.Read())
				{
					ReportComponentTemplateParameter rc = new ReportComponentTemplateParameter();
					rc.ParameterName = reader["ParameterName"].ToString();
					rc.TemplateParameterName = reader["TemplateParameterName"].ToString();
					rc.ParameterDescription = reader["ParameterDescription"].ToString();
					rc.IsStaticValue = Convert.ToBoolean(reader["IsStaticValue"]);
					rc.Value = reader["Value"].ToString();

					parameters.Add(rc);
				}
			}
			finally
			{
				if (reader != null)
					reader.Close();
				if (conn != null)
					conn.Close();
			}

			return parameters;
		}
	}
}
