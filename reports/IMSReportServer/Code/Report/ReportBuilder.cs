﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using IMSReportServer.Code.Data;
using IMSReportServer.Code.Report;
using IMSReportServer.ReportService;

namespace IMSReportServer.Code.Report
{
	/// <summary>
	/// The report build does:
	/// 1. Loads the xml report definition from the report server.
	/// 2. Adds the sub reports to the SubreportRectangle report item.
	/// 3. Replaces any shared datasources in the report with embedded datasources. (Not sure why this is needed, but the report will fail if you do not do this)
	/// 4. Generates the new xml report definition.
	/// 
	/// </summary>
	public class ReportBuilder
	{
		private static ConcurrentDictionary<String, String> DataSourceMap = new ConcurrentDictionary<String, String>();
		private static int DataSourceMapCount = 0;
		private string reportItemPageBreakXml;
		private string ReportItemPageBreakXml
		{
			get
			{
				if (String.IsNullOrEmpty(this.reportItemPageBreakXml))
				{
					using (StreamReader sr = new StreamReader(HttpContext.Current.Server.MapPath("~/ReportItem.xml")))
					{
						this.reportItemPageBreakXml = sr.ReadToEnd();
					}
				}
				return this.reportItemPageBreakXml;
			}
		}

		public ReportBuilderResult BuildReportXml(ReportParameters parameters)
		{
			// load the parent report xml
			XmlDocument reportDocument = LoadReportXmlDoc(parameters.ReportUrl);

			// add the sub reports
			if (parameters.ReportComponentList.Count > 0)
				AddSubReportsToXml(reportDocument, parameters);

			List<String> dataSourceNames = GetDataSourceNames(reportDocument);
			SwitchSharedToEmbeddedDataSource(reportDocument);

			return new ReportBuilderResult(WriteReportXml(reportDocument), dataSourceNames);
		}

		private void AddSubReportsToXml(XmlDocument doc, ReportParameters reportParameters)
		{

			XmlNodeList list = doc.GetElementsByTagName("Rectangle");
			XmlNode replaceNode = null;
			XmlNode parent = null;
			for (int i = 0; i < list.Count; i++)
			{
				if ("SubreportRectangle".Equals(list[i].Attributes["Name"].Value))
				{
					replaceNode = list[i];
					parent = replaceNode.ParentNode;
					break;
				}
			}

			if (replaceNode != null)
			{
				int zIndex = 2 * reportParameters.ReportComponentList.Count - 1;
				double top = 0;
				XmlNode lastNode = replaceNode;
				List<ReportComponent> components = reportParameters.ReportComponentList;
				for (int i = 0; i < components.Count; i++)
				{
					if (components[i].PrivateComponent && reportParameters.ExcludePrivate)
						continue;

					// add the spacing rectangle
					if (!components[i].PageBreakBefore && (components[i].TopMargin != null) && (components[i].TopMargin.Value > 0))
					{
						XmlElement spacer = CreateSubReportSpacer(doc, zIndex, components[i].TopMargin.Value, components[i].SubReportWidth, i + components.Count, top);
						parent.InsertAfter(spacer, lastNode);
						top += components[i].TopMargin.Value;
						zIndex--;
						lastNode = spacer;
					}

					bool addPageBreak;
					string parameters = GetSubReportParameters(doc, components[i], out addPageBreak);//, components[i].ExternalReportName);
					XmlElement subReportElement = CreateSubReportElement(components[i].ExternalReportName, doc, i + 1, zIndex, parameters, addPageBreak ? components[i].PageBreakBefore : false, components[i].SubReportWidth, top);
					top += .125;
					parent.InsertAfter(subReportElement, lastNode);
					zIndex--;

					lastNode = subReportElement;
				}

				parent.RemoveChild(replaceNode);
			}
		}

		private List<String> SwitchSharedToEmbeddedDataSource(XmlDocument doc)
		{
			List<String> result = new List<string>();
			XmlNodeList list = doc.GetElementsByTagName("DataSource");
			foreach (XmlNode node in list)
			{
				if (!node.HasChildNodes)
				{
					continue;
				}
				XmlNode connectionNode = null;
				foreach (XmlNode childNode in node.ChildNodes)
				{
					if (childNode.Name == "DataSourceReference")
					{
						connectionNode = childNode;
						break;
					}
				}
				if (connectionNode != null)
				{
					XmlNode parent = connectionNode.ParentNode;

					XmlElement element = doc.CreateElement("ConnectionProperties", "");
					element.RemoveAllAttributes();
					element.InnerXml = "<DataProvider>SQL</DataProvider><ConnectString>" + GetDataSourceConnectionString(connectionNode.InnerText) + "</ConnectString>";

					parent.ReplaceChild(element, connectionNode);
				}
			}
			return result;
		}

		private XmlDocument LoadReportXmlDoc(string reportPath)
		{
			ReportingService2010 rs = new ReportingService2010();
			rs.Credentials = CredentialCache.DefaultCredentials;


			System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
			MemoryStream stream = null; ;
			try
			{
                stream = new MemoryStream(rs.GetItemDefinition(reportPath));
				doc.Load(stream);
			}
            catch(Exception e)
            {
                throw new Exception("Failed to load report [" + reportPath + "].", e);
            }
			finally
			{
				if (stream != null)
					stream.Close();
			}
			return doc;
		}



		protected string WriteReportXml(XmlDocument reportDocument)
		{
			// save the new xml to a memory stream then write it our to xml
			StringBuilder sb = new StringBuilder();
			MemoryStream ms = null;
			StreamReader sr = null;
			XmlWriter xw = null;
			try
			{
				ms = new MemoryStream();

				xw = new XmlTextWriter(ms, Encoding.UTF8);
				reportDocument.Save(xw);
				ms.Position = 0;

				sr = new StreamReader(ms, Encoding.UTF8);
				sb.Append(sr.ReadToEnd());
			}
			finally
			{
				if (sr != null)
					sr.Close();
			}

			// load the document on the reporting server
			return sb.ToString().Replace("xmlns=\"\"", "");
		}

		private string GetDataSourceConnectionString(string dataSource)
		{
			if (!DataSourceMap.ContainsKey(dataSource))
			{
				ReportingService2010 rs = new ReportingService2010();
				rs.Credentials = CredentialCache.DefaultCredentials;

				DataSourceDefinition dsd = rs.GetDataSourceContents("/Data Sources/" + dataSource);
				DataSourceMap[dataSource] = dsd.ConnectString;

				if (DataSourceMapCount == 300)
				{
					DataSourceMap.Clear();
					DataSourceMapCount = 0;
				}
				else
					DataSourceMapCount++;
			}
			return DataSourceMap[dataSource];
		}


		private List<String> GetDataSourceNames(XmlDocument doc)
		{
			List<String> result = new List<string>();
			XmlNodeList list = doc.GetElementsByTagName("DataSource");
			foreach (XmlNode node in list)
			{
				if (!result.Contains(node.Attributes["Name"].Value))
					result.Add(node.Attributes["Name"].Value);
			}
			return result;
		}


		private XmlElement CreateSubReportSpacer(XmlDocument doc, int zIndex, double spacing, double width, int count, double top)
		{
			XmlElement element = doc.CreateElement("Rectangle", "");
			element.SetAttribute("Name", "Rectangle" + (count + 1));
			element.InnerXml = String.Format("<Top>{1}in</Top><Height>{2}in</Height><Width>{3}in</Width><ZIndex>{0}</ZIndex>", zIndex, top, spacing, width);
			return element;
		}

		private XmlElement CreateSubReportElement(string reportName, XmlDocument doc, int count, int zIndex, string reportParameters, bool pageBreakBefore, double width, double top)
		{
			XmlElement element = doc.CreateElement("Rectangle", "");
			element.RemoveAllAttributes();
			element.SetAttribute("Name", "Rectangle" + count);
			string strPageBreak = pageBreakBefore ? "<PageBreak><BreakLocation>Start</BreakLocation></PageBreak>" : "";
			element.InnerXml = String.Format(this.ReportItemPageBreakXml, count, reportName, reportParameters, strPageBreak, zIndex, top, width);
			return element;
		}

		private string GetSubReportParameters(XmlDocument doc, ReportComponent component, out bool addPageBreak)
		{
			addPageBreak = true;
			List<ReportComponentTemplateParameter> parameters = ReportComponentTemplateParameter.LoadParameters(component.ReportTemplateComponentID, component.ID);

			string parameterString = "";
			string parameterFormat = "<Parameter Name=\"{0}\">" +
								  "<Value>{1}</Value>" +
							   "</Parameter>";

			List<string> addedParameters = new List<string>();

			// if no parameters are defined pass all
			if (parameters.Count <= 0)
			{
				XmlNodeList reportParameterNodes = doc.GetElementsByTagName("ReportParameters");
				foreach (XmlNode reportParameterNode in reportParameterNodes)
				{
					foreach (XmlNode parameterNode in reportParameterNode.ChildNodes)
					{
						string parameterName = parameterNode.Attributes["Name"].Value;
						if (!addedParameters.Contains(parameterName))
						{
							string pStr = String.Format("=Parameters!{0}.Value", parameterName);
							parameterString += String.Format(parameterFormat, parameterName, pStr);
						}
					}
				}
			}
			else
			{
				foreach (ReportComponentTemplateParameter parameter in parameters)
				{
					if (addedParameters.Contains(parameter.ParameterName))
						continue;
					if ("PageBreakBefore".Equals(parameter.ParameterName))
					{
						parameterString += String.Format(parameterFormat, parameter.ParameterName, component.PageBreakBefore);
						addPageBreak = false;
					}
					else
					{
						if (parameter.IsStaticValue && !String.IsNullOrEmpty(parameter.Value))
							parameterString += String.Format(parameterFormat, parameter.ParameterName, parameter.Value);
						else
						{
							string pStr = String.Format("=Parameters!{0}.Value", parameter.TemplateParameterName);
							parameterString += String.Format(parameterFormat, parameter.ParameterName, pStr);
						}
					}
				}
			}

			return parameterString;
		}

		private string GetNodeValue(XmlNode node, string name)
		{
			foreach (XmlNode childNode in node.ChildNodes)
			{
				if (childNode.Name == name)
					return childNode.InnerText;
			}
			return null;
		}

	}
}