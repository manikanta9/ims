﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IMSReportServer.Code.Report
{
	public class ReportBuilderResult
	{
		public string ReportXml { get; private set; }
		public List<String> DataSourceNames { get; private set; }

		public ReportBuilderResult(string reportXml, List<String> dataSourceNames)
		{
			ReportXml = reportXml;
			DataSourceNames = dataSourceNames;
		}

	}
}