﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Text;
using IMSReportServer.ReportExecutionService;
using System.Configuration;
using IMSReportServer.Code.Report;

namespace IMSReportServer
{
	public class ReportUtils
	{
		public static readonly string[] ReservedQueryParameters = new string[] { "reportid", "showparameters", "exportformat", "inline", "orderstart", "orderstop", "orderlist", "excludeprivate", "excludedcomponentlist" };

		public static void ExportReport(HttpRequest request, HttpResponse response, ReportParameters reportParameters, String reportPath)
		{
			ExportReport(request, response, reportParameters, null, reportPath);
		}

		public static void ExportReport(HttpRequest request, HttpResponse response, ReportParameters reportParameters, ReportBuilderResult builderResult)
		{
			ExportReport(request, response, reportParameters, builderResult, null);
		}

		public static void ExportReport(HttpRequest request, HttpResponse response, ReportParameters reportParameters, ReportBuilderResult builderResult, String reportPath)
		{
			ParameterValue[] parameters = LoadReportParametersForExport(request);
			MemoryStream ms = null;

			string content = "attachment";
			if (request.Params["inline"] != null)
				content = "inline";
			
			try
			{
				byte[] byteArray = doReportExport(reportPath, builderResult, reportParameters, parameters);
				WriteBinaryResponse(byteArray, response);
				string contentType = GetContentType(reportParameters.Format);
				if (!"unknown".Equals(contentType))
					response.ContentType = contentType;
				response.AddHeader("Content-Disposition", content + "; filename=report." + GetContentExtension(reportParameters.Format));
				response.Flush();
				response.End();
			}
			finally
			{
				if (ms != null)
					ms.Close();
			}
		}

		protected static byte[] doReportExport(String reportPath, ReportBuilderResult builderResult, ReportParameters reportParameters, ParameterValue[] parameters)
		{
			ReportExecutionService.ReportExecutionService rs = new ReportExecutionService.ReportExecutionService();
			rs.Credentials = CredentialCache.DefaultCredentials;

			string encoding;
			string mimeType;
			string extension;
			Warning[] warnings = null;
			string[] streamIDs = null;
			string historyID = null;

			ExecutionInfo execInfo = null;
			if (reportParameters.HasComponents && (builderResult != null))
				execInfo = rs.LoadReportDefinition(System.Text.ASCIIEncoding.UTF8.GetBytes(builderResult.ReportXml), out warnings);
			else
				execInfo = rs.LoadReport(reportPath, historyID);

			if ((warnings != null) && (warnings.Length > 0))
			{
				string error = "";
				foreach (Warning warning in warnings)
				{
					if (!"Warning".Equals(warning.Severity))
						error += warning.ToString();
				}
				if (!String.IsNullOrEmpty(error))
					throw new Exception(error);
			}


			if (reportParameters.HasComponents && (builderResult != null) && (builderResult.DataSourceNames != null))
			{
				DataSourceCredentials[] credentials = new DataSourceCredentials[builderResult.DataSourceNames.Count];
				for (int i = 0; i < builderResult.DataSourceNames.Count; i++)
				{
					credentials[i] = new DataSourceCredentials();
					credentials[i].DataSourceName = builderResult.DataSourceNames[i];
					credentials[i].UserName = ConfigurationManager.AppSettings["DatasourceUsername"];
					credentials[i].Password = ConfigurationManager.AppSettings["DatasourcePassword"];
				}
				rs.SetExecutionCredentials(credentials);
			}
			
			 
			rs.SetExecutionParameters(parameters, "en-us");

			String SessionId = rs.ExecutionHeaderValue.ExecutionID;
			return rs.Render(GetFormatString(reportParameters.Format), null, out extension, out encoding, out mimeType, out warnings, out streamIDs);
		}

		protected static void WriteBinaryResponse(byte[] buffer, HttpResponse response)
		{
			response.Buffer = true;
			response.ClearContent();
			response.ClearHeaders();
			response.AppendHeader("content-length", buffer.GetLength(0).ToString());
			response.BinaryWrite(buffer);
		}

		public static ParameterValue[] LoadReportParametersForExport(HttpRequest request)
		{
			List<ParameterValue> parameters = new List<ParameterValue>();
			foreach (string key in request.QueryString.AllKeys)
			{
				if (String.IsNullOrEmpty(key) || ReservedQueryParameters.Contains(key.ToLower()))
					continue;
				ParameterValue param = new ParameterValue();
				param.Name = key;
				param.Value = request.QueryString[key];

				parameters.Add(param);
			}
			return parameters.ToArray();
		}
		protected static string GetFormatString(string format)
		{
			switch (format.ToLower())
			{
				case "excel":
				case "xls":
					return "EXCEL";
				case "excelopenxml":
				case "xlsx":
					return "EXCELOPENXML";
				case "doc":
					return "WORD";
				case "word":
				case "docx":
				case "wordopenxml":
					return "WORDOPENXML";
				default:
					return format;
			}
		}
		protected static string GetContentType(string format)
		{
			string contentType;
			switch (format.ToLower())
			{
				case "pdf":
					contentType = "application/pdf";
					break;
				case "excel":
				case "excelopenxml":
				case "xls":
				case "xlsx":
					contentType = "application/ms-excel";
					break;
				case "tiff":
					contentType = "image/tiff";
					break;
				case "csv":
					contentType = "application/csv";
					break;
				case "mhtml":
					contentType = "application/mhtml";
					break;
				case "xml":
					contentType = "application/xml";
					break;
				case "doc":
				case "docx":
				case "word":
				case "wordopenxml":
					contentType = "application/ms-word";
					break;
				default:
					contentType = "unknown";
					break;
			}
			return contentType;
		}
		protected static string GetContentExtension(string format)
		{
			string extension;
			switch (format.ToLower())
			{
				case "pdf":
					extension = "pdf";
					break;
				case "excelopenxml":
				case "xlsx":
					extension = "xlsx";
					break;
				case "excel":
				case "xls":
					extension = "xls";
					break;
				case "tiff":
					extension = "tif";
					break;
				case "csv":
					extension = "csv";
					break;
				case "mhtml":
					extension = "mhtml";
					break;
				case "xml":
					extension = "xml";
					break;
				case "docx":
				case "word":
					extension = "docx";
					break;
				case "doc":
					extension = "doc";
					break;
				default:
					extension = "unknown";
					break;
			}
			return extension;
		}
	}
}
