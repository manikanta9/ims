﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IMSReportServer.Code.Data;

namespace IMSReportServer.Code.Report
{
	public class ReportParameters
	{
		public List<ReportComponent> ReportComponentList { get; set; }

		public String ReportUrl { get; set; }

		public String Password { get; set; }
		public String Username { get; set; }
		public String ServerUrl { get; set; }
		public String Format { get; set; }
		public bool ExcludePrivate { get; set; }

		public int? ReportId { get; set; }


		public int[] OrderList { get; set; }
		public int[] ExcludedComponentList { get; set; }

		public int OrderStart { get; set; }
		public int OrderStop { get; set; }

		public bool HasComponents
		{
			get
			{
				return (ReportComponentList != null) && (ReportComponentList.Count > 0);
			}
		}
	}
}