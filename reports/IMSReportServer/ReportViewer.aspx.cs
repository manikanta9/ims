﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using Microsoft.Reporting.WebForms;
using System.Xml;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using IMSReportServer.ReportService;
using IMSReportServer.Code.Data;
using IMSReportServer.Code.Report;

namespace IMSReportServer
{
	public partial class ReportViewer : System.Web.UI.Page
	{

		protected void Page_Load(object sender, EventArgs e)
		{			
			if (!IsPostBack)
			{
				ReportParameters parameters = BuildReportParameters();

				ReportViewerControl.ProcessingMode = ProcessingMode.Remote;
				ReportViewerControl.ServerReport.ReportServerUrl = new Uri(parameters.ServerUrl);

				
				if ((Request.Params[null] == null) || String.IsNullOrEmpty(Request.Params[null]))
				{
					return;
				}
				else if (string.IsNullOrEmpty(Request.Params["ReportID"]))
				{
					ReportViewerControl.ServerReport.ReportPath = Request.Params[null];
				}
				else
				{
					if (parameters.ReportId==null)
						throw new Exception("'" + Request.Params["ReportID"] + "' is not a valid integer.");

					// load report components
					parameters.ReportComponentList = ReportComponent.LoadComponents(parameters);

					if (parameters.ReportComponentList.Count > 0)
					{
						ReportBuilder builder = new ReportBuilder();
						ReportBuilderResult builderResult = builder.BuildReportXml(parameters);

						if (!String.IsNullOrEmpty(parameters.Format))
						{
							ReportUtils.ExportReport(Request, Response, parameters, builderResult);
							return;
						}
						else
						{
							if (parameters.ReportComponentList.Count == 0)
							{
								ReportViewerControl.ServerReport.ReportPath = Request.Params[null];
								return;
							}

							TextReader tr = new StringReader(builderResult.ReportXml);
							this.ReportViewerControl.ServerReport.LoadReportDefinition(tr);


							// add credentials to all DataSources
							List<Microsoft.Reporting.WebForms.DataSourceCredentials> credentials = new List<Microsoft.Reporting.WebForms.DataSourceCredentials>();
							foreach (ReportDataSourceInfo rdsi in ReportViewerControl.ServerReport.GetDataSources())
							{
								Microsoft.Reporting.WebForms.DataSourceCredentials acctDsc = new Microsoft.Reporting.WebForms.DataSourceCredentials();
								acctDsc.Name = rdsi.Name;
								acctDsc.Password = parameters.Password;
								acctDsc.UserId = parameters.Username;

								credentials.Add(acctDsc);
							}

							ReportViewerControl.ServerReport.SetDataSourceCredentials(credentials.ToArray());

						}
					}
					else
					{
						if (!String.IsNullOrEmpty(parameters.Format))
						{
							ReportUtils.ExportReport(Request, Response, parameters, Request.Params[null]);
							return;
						}
						else
						{
							ReportViewerControl.ServerReport.ReportPath = Request.Params[null];
							ReportViewerControl.ProcessingMode = ProcessingMode.Remote;
						}
					}
				}

				SetParametersFromUrl();
				SetReportViewerPropertiesFromUrl();
				ReportViewerControl.ServerReport.Refresh();
			}
		}

		private ReportParameters BuildReportParameters()
		{
			ReportParameters result = new ReportParameters();

			result.ReportUrl = Request.Params[null];

			result.Password = ConfigurationManager.AppSettings["DatasourcePassword"];
			result.Username = ConfigurationManager.AppSettings["DatasourceUsername"];
			result.ServerUrl = ConfigurationManager.AppSettings["ServerUrl"];



			result.Format = Request.Params["exportFormat"];

			bool excludePrivate;
			if (bool.TryParse(Request.Params["ExcludePrivate"], out excludePrivate))
				result.ExcludePrivate = excludePrivate;
			else
				result.ExcludePrivate = false;


			int reportId;
			if (int.TryParse(Request.Params["ReportID"], out reportId))
				result.ReportId = reportId;

			result.OrderList = parseIntArray(Request.Params["orderList"]);
			result.ExcludedComponentList = parseIntArray(Request.Params["excludedComponentList"]);

			int orderStart;
			if (!int.TryParse(Request.Params["orderStart"], out orderStart))
				orderStart = -1;
			result.OrderStart = orderStart;

			int orderStop;
			if (!int.TryParse(Request.Params["orderStop"], out orderStop))
				orderStop = -1;
			result.OrderStop = orderStop;


			return result;
		}

		private int[] parseIntArray(string arrayString)
		{
			if (!String.IsNullOrEmpty(arrayString))
			{
				string[] intStringList = arrayString.Split(',');
				List<int> result = new List<int>();
				for (int i = 0; i < intStringList.Length; i++)
				{
					int value;
					if (int.TryParse(intStringList[i], out value))
						result.Add(value);
				}
				return result.ToArray();
			}
			return null;
		}

		private void SetParametersFromUrl()
		{
			List<Microsoft.Reporting.WebForms.ReportParameter> parameters = new List<Microsoft.Reporting.WebForms.ReportParameter>();
			foreach (string key in Request.QueryString.AllKeys)
			{
				if (String.IsNullOrEmpty(key) || ReportUtils.ReservedQueryParameters.Contains(key.ToLower()))
					continue;
				string value = Request.QueryString[key];
				parameters.Add(new Microsoft.Reporting.WebForms.ReportParameter(key, String.IsNullOrEmpty(value) ? null : value));
			}
			ReportViewerControl.ServerReport.SetParameters(parameters.ToArray());
		}

		private void SetReportViewerPropertiesFromUrl()
		{
			if (Request.QueryString.AllKeys.Contains("showparameters"))
			{
				bool showparameters;
				if (bool.TryParse(Request.QueryString["showparameters"], out showparameters))
					ReportViewerControl.ShowParameterPrompts = showparameters;
			}
		}


	}
}
