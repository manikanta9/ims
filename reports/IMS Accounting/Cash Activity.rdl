﻿<?xml version="1.0" encoding="utf-8"?>
<Report xmlns="http://schemas.microsoft.com/sqlserver/reporting/2016/01/reportdefinition" xmlns:rd="http://schemas.microsoft.com/SQLServer/reporting/reportdesigner">
  <AutoRefresh>0</AutoRefresh>
  <DataSources>
    <DataSource Name="CliftonDW">
      <DataSourceReference>CliftonDW</DataSourceReference>
      <rd:SecurityType>None</rd:SecurityType>
      <rd:DataSourceID>05ecb6e9-0c50-424c-a717-b9d73d3b904b</rd:DataSourceID>
    </DataSource>
  </DataSources>
  <DataSets>
    <DataSet Name="CashActivity">
      <Query>
        <DataSourceName>CliftonDW</DataSourceName>
        <QueryParameters>
          <QueryParameter Name="@GenerateMonthly">
            <Value>=Parameters!GenerateMonthly.Value</Value>
          </QueryParameter>
          <QueryParameter Name="@ReportingDate">
            <Value>=Parameters!ReportingDate.Value</Value>
          </QueryParameter>
          <QueryParameter Name="@ClientInvestmentAccountID">
            <Value>=Parameters!ClientInvestmentAccountID.Value</Value>
          </QueryParameter>
          <QueryParameter Name="@IncludeSubAccounts">
            <Value>=Parameters!IncludeSubAccounts.Value</Value>
          </QueryParameter>
        </QueryParameters>
        <CommandText>DECLARE @PreviousPeriodEnd AS DATE

IF @GenerateMonthly = 1
	SET @PreviousPeriodEnd = calendar.GetPreviousMonthEnd(@ReportingDate)
ELSE
	SET @PreviousPeriodEnd = calendar.GetPreviousQuarterEnd(@ReportingDate)

SELECT
	ha.AccountNumber "HoldingAccountNumber", ha.AccountName "HoldingAccountName", ha.IssuingCompanyName "HoldingAccountIssuer", a.AccountingAccountName,
	t.TransactionDate, t.TransactionDescription, SUM(t.BaseDebitCredit) "BaseDebitCredit", ISNULL(MAX(bs.PreviousQuarterEndDebitCredit), 0) "PreviousQuarterEndDebitCredit"
FROM AccountingTransaction t
	 INNER JOIN AccountingAccountDimension a ON t.AccountingAccountID = a.AccountingAccountID
	 INNER JOIN InvestmentAccountDimension ha ON t.HoldingInvestmentAccountID = ha.InvestmentAccountID
	 INNER JOIN Report.GetInvestmentAccountsForPurpose(@ClientInvestmentAccountID, 'Clifton Child-Account', 1, @IncludeSubAccounts) sa ON t.ClientInvestmentAccountID = sa.InvestmentAccountID
	 LEFT JOIN (
		SELECT s.HoldingInvestmentAccountID, s.AccountingAccountID, SUM(LocalDebitCreditBalance) "PreviousQuarterEndDebitCredit"
		FROM AccountingBalanceSnapshot s
			 INNER JOIN AccountingAccountDimension a ON s.AccountingAccountID = a.AccountingAccountID
			 INNER JOIN Report.GetInvestmentAccountsForPurpose(@ClientInvestmentAccountID, 'Clifton Child-Account', 1, @IncludeSubAccounts) sa ON s.ClientInvestmentAccountID = sa.InvestmentAccountID
		WHERE SnapshotDate = @PreviousPeriodEnd AND a.IsCash = 1
		GROUP BY s.HoldingInvestmentAccountID, s.AccountingAccountID
	) bs ON t.AccountingAccountID = bs.AccountingAccountID AND t.HoldingInvestmentAccountID = bs.HoldingInvestmentAccountID
WHERE
		t.TransactionDate &gt; @PreviousPeriodEnd AND t.TransactionDate &lt;= @ReportingDate
  AND a.IsCash = 1 AND t.IsDeleted = 0
GROUP BY ha.AccountNumber, ha.AccountName, ha.IssuingCompanyName, a.AccountingAccountName, t.TransactionDate, t.TransactionDescription
ORDER BY ha.IssuingCompanyName, ha.AccountNumber, a.AccountingAccountName, t.TransactionDate, t.TransactionDescription</CommandText>
        <rd:UseGenericDesigner>true</rd:UseGenericDesigner>
      </Query>
      <Fields>
        <Field Name="HoldingAccountNumber">
          <DataField>HoldingAccountNumber</DataField>
          <rd:TypeName>System.String</rd:TypeName>
        </Field>
        <Field Name="HoldingAccountName">
          <DataField>HoldingAccountName</DataField>
          <rd:TypeName>System.String</rd:TypeName>
        </Field>
        <Field Name="AccountingAccountName">
          <DataField>AccountingAccountName</DataField>
          <rd:TypeName>System.String</rd:TypeName>
        </Field>
        <Field Name="HoldingAccountIssuer">
          <DataField>HoldingAccountIssuer</DataField>
          <rd:TypeName>System.String</rd:TypeName>
        </Field>
        <Field Name="TransactionDate">
          <DataField>TransactionDate</DataField>
          <rd:TypeName>System.DateTime</rd:TypeName>
        </Field>
        <Field Name="TransactionDescription">
          <DataField>TransactionDescription</DataField>
          <rd:TypeName>System.String</rd:TypeName>
        </Field>
        <Field Name="PreviousQuarterEndDebitCredit">
          <DataField>PreviousQuarterEndDebitCredit</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
        <Field Name="BaseDebitCredit">
          <DataField>BaseDebitCredit</DataField>
          <rd:TypeName>System.Decimal</rd:TypeName>
        </Field>
      </Fields>
    </DataSet>
  </DataSets>
  <ReportSections>
    <ReportSection>
      <Body>
        <ReportItems>
          <Tablix Name="tablixPerformanceSummary2">
            <TablixBody>
              <TablixColumns>
                <TablixColumn>
                  <Width>1.94167in</Width>
                </TablixColumn>
                <TablixColumn>
                  <Width>7.5in</Width>
                </TablixColumn>
                <TablixColumn>
                  <Width>1.05833in</Width>
                </TablixColumn>
              </TablixColumns>
              <TablixRows>
                <TablixRow>
                  <Height>0.25in</Height>
                  <TablixCells>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox14">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>Cash Activity</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>13pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Center</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox13</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                            <PaddingTop>2pt</PaddingTop>
                            <PaddingBottom>2pt</PaddingBottom>
                          </Style>
                        </Textbox>
                        <ColSpan>3</ColSpan>
                      </CellContents>
                    </TablixCell>
                    <TablixCell />
                    <TablixCell />
                  </TablixCells>
                </TablixRow>
                <TablixRow>
                  <Height>0.27in</Height>
                  <TablixCells>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Level2GroupItemName1">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Fields!HoldingAccountIssuer.Value + " (" + Fields!HoldingAccountNumber.Value + ")"</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>11pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Center</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Level2GroupItemName1</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                        <ColSpan>3</ColSpan>
                      </CellContents>
                    </TablixCell>
                    <TablixCell />
                    <TablixCell />
                  </TablixCells>
                </TablixRow>
                <TablixRow>
                  <Height>0.13in</Height>
                  <TablixCells>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox52">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>Date</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>8pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Left</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox52</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>20pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox28">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>Description</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>8pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Left</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox28</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox60">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>Amount</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>8pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Right</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox60</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                  </TablixCells>
                </TablixRow>
                <TablixRow>
                  <Height>0.18in</Height>
                  <TablixCells>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="AccountingAccountName">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Fields!AccountingAccountName.Value</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>9pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style />
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>AccountingAccountName</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox1">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>Opening Balance: </Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>7pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Right</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox1</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="PreviousQuarterEndDebitCredit">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Min(Fields!PreviousQuarterEndDebitCredit.Value)</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>7pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Format>N2</Format>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style />
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>PreviousQuarterEndDebitCredit</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                  </TablixCells>
                </TablixRow>
                <TablixRow>
                  <Height>0.12in</Height>
                  <TablixCells>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="AccountGroupItemName2">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Fields!TransactionDate.Value</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>6.5pt</FontSize>
                                    <Format>d</Format>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Left</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>AccountGroupItemName1</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <BackgroundColor>=iif(RowNumber(Nothing) mod 2,"White","#EFF3F4")</BackgroundColor>
                            <PaddingLeft>20pt</PaddingLeft>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="TransactionDescription">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Fields!TransactionDescription.Value</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>6.5pt</FontSize>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Left</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>TransactionDescription</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <BackgroundColor>=iif(RowNumber(Nothing) mod 2,"White","#EFF3F4")</BackgroundColor>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="BaseDebitCredit">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Fields!BaseDebitCredit.Value</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>6.5pt</FontSize>
                                    <Format>N2</Format>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Right</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>BaseDebitCredit</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>LightGrey</Color>
                            </Border>
                            <BackgroundColor>=iif(RowNumber(Nothing) mod 2,"White","#EFF3F4")</BackgroundColor>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                  </TablixCells>
                </TablixRow>
                <TablixRow>
                  <Height>0.2in</Height>
                  <TablixCells>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox46">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value />
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>6.5pt</FontSize>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style />
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox46</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>DarkGray</Color>
                              <Style>None</Style>
                            </Border>
                            <TopBorder>
                              <Style>Solid</Style>
                            </TopBorder>
                            <PaddingLeft>15pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                            <PaddingTop>2pt</PaddingTop>
                            <PaddingBottom>2pt</PaddingBottom>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="Textbox30">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>Closing Balance: </Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>7pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Right</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>Textbox30</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>DarkGray</Color>
                              <Style>None</Style>
                            </Border>
                            <TopBorder>
                              <Style>Solid</Style>
                            </TopBorder>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                            <PaddingTop>2pt</PaddingTop>
                            <PaddingBottom>5pt</PaddingBottom>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                    <TablixCell>
                      <CellContents>
                        <Textbox Name="BaseGainLoss1">
                          <CanGrow>true</CanGrow>
                          <KeepTogether>true</KeepTogether>
                          <Paragraphs>
                            <Paragraph>
                              <TextRuns>
                                <TextRun>
                                  <Value>=Parameters!BaseCurrencySymbol.Value + " " + Format(Min(Fields!PreviousQuarterEndDebitCredit.Value) + Sum(Fields!BaseDebitCredit.Value), "N2")</Value>
                                  <Style>
                                    <FontFamily>Calibri</FontFamily>
                                    <FontSize>7pt</FontSize>
                                    <FontWeight>Bold</FontWeight>
                                    <Color>#010101</Color>
                                  </Style>
                                </TextRun>
                              </TextRuns>
                              <Style>
                                <TextAlign>Right</TextAlign>
                              </Style>
                            </Paragraph>
                          </Paragraphs>
                          <rd:DefaultName>BaseGainLoss1</rd:DefaultName>
                          <Style>
                            <Border>
                              <Color>DarkGray</Color>
                              <Style>None</Style>
                            </Border>
                            <TopBorder>
                              <Style>Solid</Style>
                            </TopBorder>
                            <PaddingLeft>2pt</PaddingLeft>
                            <PaddingRight>2pt</PaddingRight>
                            <PaddingTop>2pt</PaddingTop>
                            <PaddingBottom>5pt</PaddingBottom>
                          </Style>
                        </Textbox>
                      </CellContents>
                    </TablixCell>
                  </TablixCells>
                </TablixRow>
              </TablixRows>
            </TablixBody>
            <TablixColumnHierarchy>
              <TablixMembers>
                <TablixMember />
                <TablixMember />
                <TablixMember />
              </TablixMembers>
            </TablixColumnHierarchy>
            <TablixRowHierarchy>
              <TablixMembers>
                <TablixMember>
                  <FixedData>true</FixedData>
                  <RepeatOnNewPage>true</RepeatOnNewPage>
                  <KeepTogether>true</KeepTogether>
                </TablixMember>
                <TablixMember>
                  <Group Name="HoldingAccount">
                    <GroupExpressions>
                      <GroupExpression>=Fields!HoldingAccountName.Value</GroupExpression>
                      <GroupExpression>=Fields!HoldingAccountNumber.Value</GroupExpression>
                    </GroupExpressions>
                  </Group>
                  <SortExpressions>
                    <SortExpression>
                      <Value>=Fields!HoldingAccountName.Value</Value>
                    </SortExpression>
                    <SortExpression>
                      <Value>=Fields!HoldingAccountNumber.Value</Value>
                    </SortExpression>
                  </SortExpressions>
                  <TablixMembers>
                    <TablixMember>
                      <KeepWithGroup>After</KeepWithGroup>
                      <RepeatOnNewPage>true</RepeatOnNewPage>
                    </TablixMember>
                    <TablixMember>
                      <KeepWithGroup>After</KeepWithGroup>
                      <RepeatOnNewPage>true</RepeatOnNewPage>
                    </TablixMember>
                    <TablixMember>
                      <Group Name="AccountingAccount">
                        <GroupExpressions>
                          <GroupExpression>=Fields!AccountingAccountName.Value</GroupExpression>
                        </GroupExpressions>
                      </Group>
                      <SortExpressions>
                        <SortExpression>
                          <Value>=Fields!AccountingAccountName.Value</Value>
                        </SortExpression>
                      </SortExpressions>
                      <TablixMembers>
                        <TablixMember>
                          <KeepWithGroup>After</KeepWithGroup>
                          <RepeatOnNewPage>true</RepeatOnNewPage>
                        </TablixMember>
                        <TablixMember>
                          <Group Name="Details2" />
                          <SortExpressions>
                            <SortExpression>
                              <Value>=Fields!TransactionDate.Value</Value>
                            </SortExpression>
                          </SortExpressions>
                        </TablixMember>
                        <TablixMember>
                          <KeepWithGroup>Before</KeepWithGroup>
                          <RepeatOnNewPage>true</RepeatOnNewPage>
                        </TablixMember>
                      </TablixMembers>
                    </TablixMember>
                  </TablixMembers>
                </TablixMember>
              </TablixMembers>
            </TablixRowHierarchy>
            <RepeatRowHeaders>true</RepeatRowHeaders>
            <DataSetName>CashActivity</DataSetName>
            <Top>0.001in</Top>
            <Height>1.15in</Height>
            <Width>10.5in</Width>
            <Visibility>
              <Hidden>=Iif(Count(Fields!HoldingAccountNumber.Value, "CashActivity") &gt; 0, False, True)</Hidden>
            </Visibility>
            <Style>
              <Border>
                <Style>None</Style>
              </Border>
            </Style>
          </Tablix>
          <Rectangle Name="PageBreakAfterRectangle">
            <PageBreak>
              <BreakLocation>End</BreakLocation>
            </PageBreak>
            <KeepTogether>true</KeepTogether>
            <Height>0.001in</Height>
            <Width>10.5in</Width>
            <ZIndex>1</ZIndex>
            <Visibility>
              <Hidden>=Iif(Parameters!PageBreakBefore.Value = "True", False, True)</Hidden>
            </Visibility>
            <Style>
              <Border>
                <Style>None</Style>
              </Border>
            </Style>
          </Rectangle>
        </ReportItems>
        <Height>1.151in</Height>
        <Style />
      </Body>
      <Width>10.5in</Width>
      <Page>
        <PageHeight>8.5in</PageHeight>
        <PageWidth>11in</PageWidth>
        <LeftMargin>0.25in</LeftMargin>
        <RightMargin>0.25in</RightMargin>
        <Style />
      </Page>
    </ReportSection>
  </ReportSections>
  <ReportParameters>
    <ReportParameter Name="ReportingDate">
      <DataType>DateTime</DataType>
      <DefaultValue>
        <Values>
          <Value>12/31/2010 00:00:00</Value>
        </Values>
      </DefaultValue>
      <Prompt>Reporting Date</Prompt>
    </ReportParameter>
    <ReportParameter Name="ClientInvestmentAccountID">
      <DataType>Integer</DataType>
      <DefaultValue>
        <Values>
          <Value>1084</Value>
        </Values>
      </DefaultValue>
      <Prompt>Client Investment Account ID</Prompt>
    </ReportParameter>
    <ReportParameter Name="BaseCurrencySymbol">
      <DataType>String</DataType>
      <DefaultValue>
        <Values>
          <Value>$</Value>
        </Values>
      </DefaultValue>
      <Prompt>BaseCurrencySymbol</Prompt>
    </ReportParameter>
    <ReportParameter Name="InvestmentGroupID">
      <DataType>Integer</DataType>
      <DefaultValue>
        <Values>
          <Value>1</Value>
        </Values>
      </DefaultValue>
      <Prompt>InvestmentGroupID</Prompt>
    </ReportParameter>
    <ReportParameter Name="PageBreakBefore">
      <DataType>Boolean</DataType>
      <DefaultValue>
        <Values>
          <Value>False</Value>
        </Values>
      </DefaultValue>
      <Prompt>PageBreakBefore</Prompt>
    </ReportParameter>
    <ReportParameter Name="IncludeSubAccounts">
      <DataType>Boolean</DataType>
      <DefaultValue>
        <Values>
          <Value>False</Value>
        </Values>
      </DefaultValue>
      <Prompt>Include Sub Accounts</Prompt>
    </ReportParameter>
    <ReportParameter Name="GenerateMonthly">
      <DataType>Boolean</DataType>
      <DefaultValue>
        <Values>
          <Value>false</Value>
        </Values>
      </DefaultValue>
      <Prompt>Generate Monthly</Prompt>
    </ReportParameter>
  </ReportParameters>
  <ReportParametersLayout>
    <GridLayoutDefinition>
      <NumberOfColumns>2</NumberOfColumns>
      <NumberOfRows>4</NumberOfRows>
      <CellDefinitions>
        <CellDefinition>
          <ColumnIndex>0</ColumnIndex>
          <RowIndex>0</RowIndex>
          <ParameterName>ReportingDate</ParameterName>
        </CellDefinition>
        <CellDefinition>
          <ColumnIndex>1</ColumnIndex>
          <RowIndex>0</RowIndex>
          <ParameterName>ClientInvestmentAccountID</ParameterName>
        </CellDefinition>
        <CellDefinition>
          <ColumnIndex>0</ColumnIndex>
          <RowIndex>1</RowIndex>
          <ParameterName>BaseCurrencySymbol</ParameterName>
        </CellDefinition>
        <CellDefinition>
          <ColumnIndex>1</ColumnIndex>
          <RowIndex>1</RowIndex>
          <ParameterName>InvestmentGroupID</ParameterName>
        </CellDefinition>
        <CellDefinition>
          <ColumnIndex>0</ColumnIndex>
          <RowIndex>2</RowIndex>
          <ParameterName>PageBreakBefore</ParameterName>
        </CellDefinition>
        <CellDefinition>
          <ColumnIndex>1</ColumnIndex>
          <RowIndex>2</RowIndex>
          <ParameterName>IncludeSubAccounts</ParameterName>
        </CellDefinition>
        <CellDefinition>
          <ColumnIndex>0</ColumnIndex>
          <RowIndex>3</RowIndex>
          <ParameterName>GenerateMonthly</ParameterName>
        </CellDefinition>
      </CellDefinitions>
    </GridLayoutDefinition>
  </ReportParametersLayout>
  <Language>en-US</Language>
  <ConsumeContainerWhitespace>true</ConsumeContainerWhitespace>
  <rd:ReportUnitType>Inch</rd:ReportUnitType>
  <rd:ReportID>9b566dac-0b37-4ab5-8d58-89ac4dadcb9b</rd:ReportID>
</Report>