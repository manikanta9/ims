import com.clifton.gradle.plugin.build.usingVariant

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	// for Windows service
	implementation("winrun4j:winrun4j:0.4.5")

	// springIntegrationVersion is configured in gradle.properties so it can be used globally
	implementation("org.springframework.integration:spring-integration-core")
	implementation("org.springframework.integration:spring-integration-file")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	implementation(project(":clifton-core"))
	implementation(project(":clifton-core:clifton-core-messaging"))
	implementation(project(":clifton-swift")) { usingVariant("server") }

	///////////////////////////////////////////////////////////////////////////
	///////////////            Test Dependencies               ////////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
	testFixturesApi(testFixtures(project(":clifton-swift")))
}
