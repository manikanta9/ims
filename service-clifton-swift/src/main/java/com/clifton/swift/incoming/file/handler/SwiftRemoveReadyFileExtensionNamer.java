package com.clifton.swift.incoming.file.handler;

import com.clifton.core.dataaccess.file.FileUtils;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.messaging.Message;

import java.io.File;


/**
 * <code>SwiftRemoveReadyFileExtensionNamer</code> changes the name of the file by
 * removing the *.ready extension.
 */
public class SwiftRemoveReadyFileExtensionNamer implements FileNameGenerator {

	@Override
	public String generateFileName(Message<?> message) {
		String fileName = ((File) message.getPayload()).getName();
		if (fileName.endsWith(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION)) {
			return FileUtils.getFileNameWithoutExtension(fileName);
		}
		return fileName;
	}
}
