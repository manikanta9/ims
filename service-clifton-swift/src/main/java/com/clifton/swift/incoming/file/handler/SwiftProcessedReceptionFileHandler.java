package com.clifton.swift.incoming.file.handler;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.incoming.file.handler.lau.SwiftMessageAuthenticator;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.log.SwiftMessageLog;
import com.clifton.swift.message.log.SwiftMessageLogHandler;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.server.router.SwiftServerMessageRouterCommand;
import com.clifton.swift.util.SwiftUtilHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.springframework.messaging.Message;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>SwiftProcessedReceptionFileHandler</code> invoked when a file is moved from the reception folder to the processed folder.
 */
public class SwiftProcessedReceptionFileHandler<M> implements SwiftFileHandler {

	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;
	private SwiftServerMessageRouter<M> swiftServerMessageRouter;
	private SwiftMessageLogHandler swiftMessageLogHandler;
	private SwiftUtilHandler<M> swiftUtilHandler;
	private SwiftMessageAuthenticator swiftMessageAuthenticator;
	private SwiftDiscardFileHandler SwiftMessageLauDiscardHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message<File> handleFile(Message<File> fileMessage) {
		AssertUtils.assertNotNull(fileMessage, "Supplied message cannot be null.");
		AssertUtils.assertNotNull(fileMessage.getPayload(), "Supplied message file cannot be null.");
		AssertUtils.assertTrue(fileMessage.getPayload().exists(), "Supplied message file must exist [" + fileMessage.getPayload().getAbsolutePath() + "].");

		File messageFile = fileMessage.getPayload();
		String fileName = messageFile.getName();

		final SwiftMessageFormats messageFormat = SwiftMessageFileUtils.getMessageFormatFromExtension(fileName);
		ValidationUtils.assertNotNull(messageFormat, "Could not determine swift message format from its file name " + fileName);
		ValidationUtils.assertTrue(messageFormat == SwiftMessageFormats.MT, SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);

		if (SwiftMessageFileUtils.isRegularMessage(fileName)) {
			String fileContents = SwiftMessageFileUtils.getMessageFileContents(fileMessage);
			boolean authenticated = this.swiftMessageAuthenticator.authenticate(fileMessage, fileContents);

			if (authenticated) {
				// save the entire file contents as a log message.
				SwiftMessageLog logMessage = getSwiftMessageLogHandler().logSwiftMessage(fileContents, true, messageFormat);
				// split out and save the individual log messages
				final List<SwiftMessageLog> swiftMessageLogList = new ArrayList<>();
				Boolean successful = getSwiftMessageLogHandler().processSwiftMessage(logMessage, log -> {
					boolean processedAll;
					Map<String, M> messageList = getSwiftMessageConverterHandler().parse(logMessage.getText(), messageFormat);
					for (Map.Entry<String, M> entry : messageList.entrySet()) {
						SwiftMessageLog swiftMessageLog = getSwiftMessageLogHandler().logSwiftMessage(entry.getKey(), true, messageFormat);
						swiftMessageLogList.add(swiftMessageLog);
					}
					processedAll = true;
					return processedAll;
				});

				// (null return value is false).
				if (BooleanUtils.isTrue(successful)) {
					// process each log entry
					for (SwiftMessageLog swiftMessageLog : swiftMessageLogList) {
						getSwiftMessageLogHandler().processSwiftMessage(swiftMessageLog, log -> {
							M message = getSwiftUtilHandler().parseSingleMessage(swiftMessageLog.getText(), swiftMessageLog.getMessageFormat());
							getSwiftServerMessageRouter().receive(SwiftServerMessageRouterCommand.ofMessageAndString(message, swiftMessageLog.getText()));
							return log;
						});
					}
				}
			}
			else {
				getSwiftMessageLauDiscardHandler().handleDiscardFile(fileMessage);
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageConverterHandler<M> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<M> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftServerMessageRouter<M> getSwiftServerMessageRouter() {
		return this.swiftServerMessageRouter;
	}


	public void setSwiftServerMessageRouter(SwiftServerMessageRouter<M> swiftServerMessageRouter) {
		this.swiftServerMessageRouter = swiftServerMessageRouter;
	}


	public SwiftMessageLogHandler getSwiftMessageLogHandler() {
		return this.swiftMessageLogHandler;
	}


	public void setSwiftMessageLogHandler(SwiftMessageLogHandler swiftMessageLogHandler) {
		this.swiftMessageLogHandler = swiftMessageLogHandler;
	}


	public SwiftUtilHandler<M> getSwiftUtilHandler() {
		return this.swiftUtilHandler;
	}


	public void setSwiftUtilHandler(SwiftUtilHandler<M> swiftUtilHandler) {
		this.swiftUtilHandler = swiftUtilHandler;
	}


	public SwiftMessageAuthenticator getSwiftMessageAuthenticator() {
		return this.swiftMessageAuthenticator;
	}


	public void setSwiftMessageAuthenticator(SwiftMessageAuthenticator swiftMessageAuthenticator) {
		this.swiftMessageAuthenticator = swiftMessageAuthenticator;
	}


	public SwiftDiscardFileHandler getSwiftMessageLauDiscardHandler() {
		return this.SwiftMessageLauDiscardHandler;
	}


	public void setSwiftMessageLauDiscardHandler(SwiftDiscardFileHandler swiftMessageLauDiscardHandler) {
		this.SwiftMessageLauDiscardHandler = swiftMessageLauDiscardHandler;
	}
}
