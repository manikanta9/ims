package com.clifton.swift.incoming.file.handler;

import org.springframework.messaging.Message;

import java.io.File;


public interface SwiftFileHandler {

	public Message<File> handleFile(Message<File> fileMessage);
}
