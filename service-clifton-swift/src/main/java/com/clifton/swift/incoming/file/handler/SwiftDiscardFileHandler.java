package com.clifton.swift.incoming.file.handler;

import org.springframework.messaging.Message;

import java.io.File;


/**
 * <code>SwiftDiscardFileHandler</code> handle timeouts and errors that may occur in during spring integration processing.
 */
public interface SwiftDiscardFileHandler {

	public void handleDiscardFile(Message<File> fileMessage);
}
