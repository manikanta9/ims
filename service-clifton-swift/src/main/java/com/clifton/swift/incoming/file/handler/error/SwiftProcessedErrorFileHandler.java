package com.clifton.swift.incoming.file.handler.error;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.SystemUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.incoming.file.handler.SwiftDiscardFileHandler;
import com.clifton.swift.incoming.file.handler.SwiftFileHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.messaging.error.SwiftMessagingErrorService;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.springframework.messaging.Message;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>SwiftProcessedErrorFileHandler</code> Handle a file placed in the swift error folder.  The {@link SwiftMessageFileHeaderAugmenter} will place
 * correlationId, sequenceNumber, and sequenceSize on the message header so Spring Integration resequencer will wait until two messages with the same
 * base filename (all extensions removed) are received and then release them so the error file is always sent last.
 */
public class SwiftProcessedErrorFileHandler<M> implements SwiftFileHandler, SwiftDiscardFileHandler {

	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;
	private SwiftIdentifierHandler<M> swiftIdentifierHandler;
	private SwiftServerStatusHandler swiftServerStatusHandler;
	private SwiftMessagingErrorService swiftMessagingErrorService;
	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message<File> handleFile(Message<File> fileMessage) {
		AssertUtils.assertNotNull(fileMessage, "Supplied message cannot be null.");
		AssertUtils.assertNotNull(fileMessage.getPayload(), "Supplied message file cannot be null.");

		File messageFile = fileMessage.getPayload();
		String fileName = messageFile.getName();
		try {
			SwiftMessageFormats messageFormat = SwiftMessageFileUtils.getMessageFormatFromExtension(fileName);
			ValidationUtils.assertNotNull(messageFormat, "Could not determine swift message format from its file name " + fileName);

			if (SwiftMessageFormats.MT == messageFormat) {
				if (SwiftMessageFileUtils.isErrorMessage(fileName)) {
					String errorFileContents = SwiftMessageFileUtils.getMessageFileContents(fileMessage);
					File regularMessageFile = SwiftMessageFileUtils.findRegularFromErrorFile(messageFile);
					outputSwiftMessageStatusMessages(regularMessageFile, messageFormat, errorFileContents);
				}
			}
			else {
				throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
			}
		}
		catch (Exception ex) {
			String message = "Failed to process error message file " + fileName;
			LogUtils.error(getClass(), message, ex);
			outputMessageToErrorChannel(message, ex, fileName);
		}
		return null;
	}


	/**
	 * The error folder did not receive both '.err' and '.fin.err' files within the 'send-timeout' duration; attempt to
	 * process the single file.
	 */
	@Override
	public void handleDiscardFile(Message<File> fileMessage) {
		AssertUtils.assertNotNull(fileMessage, "Supplied message cannot be null.");
		AssertUtils.assertNotNull(fileMessage.getPayload(), "Supplied message file cannot be null.");

		File messageFile = fileMessage.getPayload();
		String fileName = messageFile.getName();
		try {
			SwiftMessageFormats messageFormat = SwiftMessageFileUtils.getMessageFormatFromExtension(fileName);
			ValidationUtils.assertNotNull(messageFormat, "Could not determine swift message format from its file name " + fileName);

			if (SwiftMessageFormats.MT == messageFormat) {
				if (SwiftMessageFileUtils.isErrorMessage(fileName)) {
					String errorFileContents = SwiftMessageFileUtils.getMessageFileContents(fileMessage);
					throw new RuntimeException(errorFileContents);
				}
				else {
					File regularMessageFile = fileMessage.getPayload();
					outputSwiftMessageStatusMessages(regularMessageFile, messageFormat, "Unknown error:  could not retrieve error file for this message.");
				}
			}
			else {
				throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
			}
		}
		catch (Exception ex) {
			String message = "Failed to process timed out error message file " + fileName;
			LogUtils.error(getClass(), message, ex);
			outputMessageToErrorChannel(message, ex, fileName);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void outputSwiftMessageStatusMessages(File regularMessageFile, SwiftMessageFormats messageFormat, String errorFileContents) {
		String regularFileContents = FileUtils.readFileToString(regularMessageFile);
		Map<String, M> messageList = getSwiftMessageConverterHandler().parse(regularFileContents, messageFormat);
		List<SwiftIdentifier> identifiers = messageList.values()
				.stream()
				.map(m -> getSwiftIdentifierHandler().getSwiftIdentifierForMessage(m))
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		identifiers.forEach(i -> getSwiftServerStatusHandler().sendSwiftExportStatus(
				i.getUniqueStringIdentifier(),
				i.getSourceSystem().getName(),
				i.getSourceSystemModule().getName(),
				InstructionStatuses.FAILED,
				errorFileContents
		));
		updateSwiftMessageErrorStatus(identifiers);
	}


	/**
	 * Only output to the JMS error channel when something completely unexpected occurs, every attempt should be made to update the status on the exception's Swift Message.
	 */
	private void outputMessageToErrorChannel(String text, Exception ex, String fileName) {
		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();
		swiftMessagingMessage.setMachineName(SystemUtils.getMachineName());
		swiftMessagingMessage.setMessageText(text);
		swiftMessagingMessage.setUniqueStringIdentifier(fileName);
		getSwiftMessagingErrorService().postErrorMessage(swiftMessagingMessage, ex);
	}


	/**
	 * Update the status of all messages listed in the regular fin file accompanying the err file to 'ERROR'.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void updateSwiftMessageErrorStatus(List<SwiftIdentifier> identifiers) {
		SwiftMessageStatus swiftMessageStatus = getSwiftServerMessageService().getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name());
		AssertUtils.assertNotNull(swiftMessageStatus, "Swift Message Status %s is not found", SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name());

		SwiftMessageSearchForm searchForm = new SwiftMessageSearchForm();
		searchForm.setIdentifierIds(identifiers.stream().map(SwiftIdentifier::getId).toArray(Long[]::new));
		List<SwiftMessage> swiftMessages = getSwiftServerMessageService().getSwiftMessageList(searchForm);

		if (!CollectionUtils.isEmpty(swiftMessages)) {
			swiftMessages.forEach(msg -> {
				msg.setMessageStatus(swiftMessageStatus);
				getSwiftServerMessageService().saveSwiftMessage(msg);
			});
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageConverterHandler<M> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<M> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftIdentifierHandler<M> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<M> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}


	public SwiftServerStatusHandler getSwiftServerStatusHandler() {
		return this.swiftServerStatusHandler;
	}


	public void setSwiftServerStatusHandler(SwiftServerStatusHandler swiftServerStatusHandler) {
		this.swiftServerStatusHandler = swiftServerStatusHandler;
	}


	public SwiftMessagingErrorService getSwiftMessagingErrorService() {
		return this.swiftMessagingErrorService;
	}


	public void setSwiftMessagingErrorService(SwiftMessagingErrorService swiftMessagingErrorService) {
		this.swiftMessagingErrorService = swiftMessagingErrorService;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
