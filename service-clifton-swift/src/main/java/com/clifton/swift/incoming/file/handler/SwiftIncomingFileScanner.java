package com.clifton.swift.incoming.file.handler;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.integration.file.DefaultDirectoryScanner;

import java.io.File;
import java.util.Objects;


/**
 * <code>SwiftIncomingFileScanner</code> scans the input directory for files, when a file
 * is encountered without the *.ready extension it is renamed (if it can), lastly all #.ready
 * files are passed along to the output channel where the file is copied to the processed folder
 * with the *.ready extension removed.
 * <p>
 * The file is renamed to make sure the file is done copying, this depends on windows placing an
 * exclusive lock on the file during the rename operation.
 */
public class SwiftIncomingFileScanner extends DefaultDirectoryScanner {

	public static final String FILE_NAME_READY_EXTENSION = "ready";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The results are passed along to the filter(s).
	 */
	@Override
	protected File[] listEligibleFiles(File directory) {
		return CollectionUtils.createList(directory.listFiles())
				.stream()
				.map(this::getFileIfReady)
				.filter(Objects::nonNull)
				.toArray(File[]::new);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private File getFileIfReady(File file) {
		File result;
		if (isFileReady(file)) {
			result = file;
		}
		else {
			result = tryAddReadyExtensionToFile(file);
		}
		return result;
	}


	private boolean isFileReady(File file) {
		return file.getName().endsWith(FILE_NAME_READY_EXTENSION);
	}


	private File tryAddReadyExtensionToFile(File file) {
		try {
			File renamedFile = new File(file.getAbsolutePath() + "." + FILE_NAME_READY_EXTENSION);
			if (file.renameTo(renamedFile)) {
				return renamedFile;
			}
		}
		catch (Throwable e) {
			// do nothing because this means the file is not ready
			LogUtils.warn(getClass(), "Failed to rename [" + file.getName() + "].", e);
		}
		return null;
	}
}
