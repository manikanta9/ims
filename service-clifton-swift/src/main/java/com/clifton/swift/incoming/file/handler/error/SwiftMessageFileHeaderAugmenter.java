package com.clifton.swift.incoming.file.handler.error;

import com.clifton.swift.incoming.file.handler.SwiftIncomingFileScanner;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.utils.SwiftMessageFileExtensions;
import com.clifton.swift.utils.SwiftMessageFileUtils;

import java.io.File;
import java.util.regex.Pattern;


/**
 * <code>SwiftMessageFileHeaderAugmenter</code> Holds the file messages in groups based on the base file name until both file messages have arrived (.fin and .err).
 * When both messages have arrived, the error (.err) message is always sent to the output channel last.
 */
public class SwiftMessageFileHeaderAugmenter {

	private static final String FILE_REGULAR_EXPRESSION = "^(\\d+)\\.(\\d{8})_(\\d{6})\\.(.)*(\\.fin|\\.FIN)(\\.err|\\.ERR)?$";

	private Pattern fileNamePattern = Pattern.compile(FILE_REGULAR_EXPRESSION);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Process (.ready) files, return the file name after all file extensions have been removed and the SWIFT appended processed timestamp is truncated.  The base file
	 * name (without the appended timestamp) should be the same for both '.fin' and '.fin.err' message files.  The correlation ID is used for the group name, only files
	 * with the same group name are released together in sequence number order.
	 * <p>
	 * Errors files should be sent in pairs:
	 * <filename>.<timestamp>.fin
	 * <filename>.<timestamp>.fin.err
	 *
	 * <filename> is the filename used when the message was originally sent, <timestamp> is the time SWIFT processed it, <timestamp> can be different between
	 * the '.fin' and '.fin.err' files.
	 * <p>
	 * example:
	 * 1.20171117_100443.20171117_100718.fin
	 * 1.20171117_100443.20171117_100720.fin.err
	 */
	public String getMessageFileCorrelationId(File file) {
		if (file != null) {
			try {
				String fileName = file.getName();
				if (fileName.endsWith(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION)) {
					fileName = fileName.substring(0, fileName.indexOf(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION) - 1);
					SwiftMessageFormats swiftMessageFormats = SwiftMessageFileUtils.getMessageFormat(fileName);
					if (swiftMessageFormats != null) {
						SwiftMessageFileExtensions swiftMessageFileExtensions = SwiftMessageFileUtils.findExtensionForFormat(swiftMessageFormats);
						if (swiftMessageFileExtensions != null) {
							String regularExtension = "." + swiftMessageFileExtensions.getRegularExtension();
							int indexOf = fileName.indexOf(regularExtension);
							// must be either a .fin.ready or fin.err.ready file.
							if (indexOf >= 0) {
								String noExtensionName = fileName.substring(0, indexOf);

								// only parse file names that start with the expected file name pattern: '[count].[date]_[timestamp].'
								if (this.fileNamePattern.matcher(fileName).matches()) {
									// the end of the file name may repeat if the file is re-processed
									// get the beginning of the file up to the second '.'
									String baseFileName = noExtensionName.substring(0, noExtensionName.indexOf('.', noExtensionName.indexOf('.') + 1));
									// make sure this base file name matches our outgoing file format.
									if (swiftMessageFileExtensions.isValidFilename(baseFileName + regularExtension)) {
										return baseFileName;
									}
								}
								else {
									return noExtensionName;
								}
							}
						}
					}
				}
			}
			catch (Exception ex) {
				// swallow all exceptions.
			}
		}
		return null;
	}


	/**
	 * for (.ready) files, return 2 for '.fin.err' and 1 for '.fin' message files.  Make sure the error file is released second, this
	 * guarantees to the handler that both files have arrived and can be processed.
	 */
	public Integer getSequenceNumber(File file) {
		if (file != null) {
			String fileName = file.getName();
			if (fileName.endsWith(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION)) {
				fileName = fileName.substring(0, fileName.indexOf(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION) - 1);
				if (SwiftMessageFileUtils.isLauMessage(fileName)) {
					return 3;
				}
				else if (SwiftMessageFileUtils.isErrorMessage(fileName)) {
					return 2;
				}
				else if (SwiftMessageFileUtils.isRegularMessage(fileName)) {
					return 1;
				}
				else {
					return 0;
				}
			}
		}
		return null;
	}
}
