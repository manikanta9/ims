package com.clifton.swift.incoming.file.handler.error;

import org.springframework.integration.aggregator.ReleaseStrategy;
import org.springframework.integration.store.MessageGroup;


/**
 * <code>SwiftAggregationReleaseStrategy</code> set the expected group size when aggregating incoming messages based on authentication requirements.
 */
public class SwiftAggregationReleaseStrategy implements ReleaseStrategy {

	private int groupSize = 2;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean canRelease(MessageGroup group) {
		return group.size() == this.groupSize;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getGroupSize() {
		return this.groupSize;
	}


	public void setGroupSize(int groupSize) {
		this.groupSize = groupSize;
	}
}
