package com.clifton.swift.incoming.file.handler.lau;

import com.clifton.swift.incoming.file.handler.SwiftIncomingFileScanner;
import org.springframework.messaging.Message;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <code>SwiftMessageShortTermFilter</code>  Filter out messages based on whether they've been seen for the last (timeout) minutes.
 */
public class SwiftMessageShortTermFilter {

	private Map<String, LocalDateTime> messageList = new ConcurrentHashMap<>();
	private Duration timeout;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean accept(Message<File> message) {
		if (!message.getPayload().getName().endsWith(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION)) {
			return false;
		}
		reap(LocalDateTime.now());
		if (this.messageList.containsKey(message.getPayload().getName())) {
			return false;
		}
		else {
			this.messageList.put(message.getPayload().getName(), LocalDateTime.now());
			return true;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void reap(LocalDateTime now) {
		for (Map.Entry<String, LocalDateTime> groupEntry : this.messageList.entrySet()) {
			if (Duration.between(groupEntry.getValue(), now).compareTo(this.timeout) > 0) {
				this.messageList.remove(groupEntry.getKey());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Duration getTimeout() {
		return this.timeout;
	}


	public void setTimeout(Duration timeout) {
		this.timeout = timeout;
	}
}
