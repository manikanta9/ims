package com.clifton.swift.incoming.file.handler.lau;

import org.springframework.integration.router.AbstractMessageRouter;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * <code>SwiftLauDynamicMessageRouter</code> Allow the routes to be dynamically changed based on the authentication property.
 */
public class SwiftLauDynamicMessageRouter extends AbstractMessageRouter {

	private boolean localAuthenticationEnabled;

	private MessageChannel authenticateChannel;
	private MessageChannel nonAuthenticatedChannel;

	private List<MessageChannel> authenticateChannelList = new ArrayList<>();
	private List<MessageChannel> nonAuthenticatedChannelList = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void initialize() {
		this.authenticateChannelList.add(this.authenticateChannel);
		this.nonAuthenticatedChannelList.add(this.nonAuthenticatedChannel);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Collection<MessageChannel> determineTargetChannels(Message<?> message) {
		if (isLocalAuthenticationEnabled()) {
			return this.authenticateChannelList;
		}
		else {
			return this.nonAuthenticatedChannelList;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLocalAuthenticationEnabled() {
		return this.localAuthenticationEnabled;
	}


	public void setLocalAuthenticationEnabled(boolean localAuthenticationEnabled) {
		this.localAuthenticationEnabled = localAuthenticationEnabled;
	}


	public MessageChannel getAuthenticateChannel() {
		return this.authenticateChannel;
	}


	public void setAuthenticateChannel(MessageChannel authenticateChannel) {
		this.authenticateChannel = authenticateChannel;
	}


	public MessageChannel getNonAuthenticatedChannel() {
		return this.nonAuthenticatedChannel;
	}


	public void setNonAuthenticatedChannel(MessageChannel nonAuthenticatedChannel) {
		this.nonAuthenticatedChannel = nonAuthenticatedChannel;
	}
}
