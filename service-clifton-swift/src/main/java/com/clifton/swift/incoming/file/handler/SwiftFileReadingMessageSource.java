package com.clifton.swift.incoming.file.handler;


import org.springframework.integration.file.FileReadingMessageSource;


/**
 * Override the default source, we need our own scanner without the default filters.
 */
public class SwiftFileReadingMessageSource extends FileReadingMessageSource {

	@Override
	protected void onInit() {
		// Do nothing as there is a bug in the onInit method that prevents the use of a custom scanner completely.
	}
}
