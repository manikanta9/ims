package com.clifton.swift.incoming.file.handler.lau;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.incoming.file.handler.SwiftIncomingFileScanner;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import com.clifton.swift.utils.SwiftMtAuthenticationUtils;
import org.springframework.messaging.Message;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>SwiftMessageAuthenticator</code>
 */
public class SwiftMessageAuthenticator {

	public static final String AUTHENTICATION_HEADER = "authenticationHash";
	public static final int MAX_HASH_LENGTH = 1024;

	private boolean localAuthenticationEnabled;
	private String keyPath;
	private String key;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void initialize() {
		if (isLocalAuthenticationEnabled()) {
			this.key = SwiftMtAuthenticationUtils.readLauKey(this.keyPath);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean authenticate(Message<File> fileMessage, String fileContents) {
		boolean authenticated = false;
		try {
			if (isLocalAuthenticationEnabled()) {
				String authenticationHash = (String) fileMessage.getHeaders().get(AUTHENTICATION_HEADER);
				String calculatedHash = new String(SwiftMtAuthenticationUtils.encode(this.getKey(), fileContents), StandardCharsets.US_ASCII);
				authenticated = StringUtils.isEqual(authenticationHash, calculatedHash) && !StringUtils.isEmpty(authenticationHash);
			}
			else {
				authenticated = true;
			}
		}
		catch (Exception e) {
			// empty
		}
		return authenticated;
	}


	public String getAuthenticationHeader(Message<List<File>> messageFileList) {
		String authenticationHash = null;
		if (isLocalAuthenticationEnabled()) {
			authenticationHash = CollectionUtils.getFirstElementStrict(messageFileList.getPayload().stream()
					.map(this::readAuthenticationHash).filter(Objects::nonNull).collect(Collectors.toList()));
			ValidationUtils.assertTrue(!StringUtils.isEmpty(authenticationHash) && authenticationHash.length() <= MAX_HASH_LENGTH,
					"Authentication Hash from lau file is too long " + authenticationHash);
		}
		return authenticationHash;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String readAuthenticationHash(File file) {
		String fileName = file.getName();
		if (fileName.endsWith(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION)) {
			fileName = fileName.substring(0, fileName.indexOf(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION) - 1);
		}
		SwiftMessageFormats messageFormat = SwiftMessageFileUtils.getMessageFormat(fileName);
		ValidationUtils.assertTrue(messageFormat == SwiftMessageFormats.MT, SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);

		if (SwiftMessageFileUtils.isLauMessage(fileName)) {
			return FileUtils.readFileToString(file);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLocalAuthenticationEnabled() {
		return this.localAuthenticationEnabled;
	}


	public void setLocalAuthenticationEnabled(boolean localAuthenticationEnabled) {
		this.localAuthenticationEnabled = localAuthenticationEnabled;
	}


	public String getKeyPath() {
		return this.keyPath;
	}


	public void setKeyPath(String keyPath) {
		this.keyPath = keyPath;
	}


	public String getKey() {
		return this.key;
	}
}
