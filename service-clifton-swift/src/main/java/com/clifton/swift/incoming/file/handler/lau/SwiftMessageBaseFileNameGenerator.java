package com.clifton.swift.incoming.file.handler.lau;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.incoming.file.handler.SwiftIncomingFileScanner;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.springframework.messaging.Message;

import java.io.File;
import java.util.Objects;


/**
 * <code>SwiftMessageBaseFileNameGenerator</code>
 * calculate the base file name (remove .lau.ready or .err.ready).
 */
public class SwiftMessageBaseFileNameGenerator {

	public String generate(Message<File> message) {
		String fileName = message.getPayload().getName();
		if (fileName.endsWith(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION)) {
			fileName = fileName.substring(0, fileName.indexOf(SwiftIncomingFileScanner.FILE_NAME_READY_EXTENSION) - 1);
		}
		SwiftMessageFormats messageFormat = SwiftMessageFileUtils.getMessageFormat(fileName);
		ValidationUtils.assertTrue(Objects.equals(messageFormat, SwiftMessageFormats.MT), SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);

		if (SwiftMessageFileUtils.isRegularMessage(fileName)) {
			return fileName;
		}
		else {
			String extension = fileName.substring(fileName.lastIndexOf('.') + 1);
			ValidationUtils.assertTrue(SwiftMessageFileUtils.isErrorExtension(extension) || SwiftMessageFileUtils.isLauExtension(extension),
					String.format("SWIFT incoming file [%s] has an unknown file extension [%s].", fileName, extension));
			return fileName.substring(0, fileName.lastIndexOf('.'));
		}
	}
}
