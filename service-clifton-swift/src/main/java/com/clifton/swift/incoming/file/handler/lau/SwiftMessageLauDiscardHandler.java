package com.clifton.swift.incoming.file.handler.lau;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.incoming.file.handler.SwiftDiscardFileHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.log.SwiftMessageLog;
import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.springframework.messaging.Message;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


/**
 * <code>SwiftMessageLauTimeoutHandlerImpl</code> handles timeout or SHA mismatches from the message grouping latch.
 * If the (*.fin and *.fin.lau) do not BOTH arrive within the timeout period or there is a mismatch between the supplied and calculated SHA hash codes, the message
 * files will be parsed, logged and an error message sent back to IMS.
 */
public class SwiftMessageLauDiscardHandler<M> implements SwiftDiscardFileHandler {

	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;
	private SwiftMessageService swiftServerMessageService;
	private SwiftServerStatusHandler swiftServerStatusHandler;
	private SwiftIdentifierHandler<M> swiftIdentifierHandler;
	private SwiftMessageLogService swiftServerMessageLogService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void handleDiscardFile(Message<File> fileMessage) {
		LogUtils.error(LogCommand.ofMessage(SwiftMessageLauDiscardHandler.class, "SWIFT authentication failed for file [%s]",
				fileMessage.getPayload().getName()));

		final SwiftMessageFormats messageFormat = SwiftMessageFileUtils.getMessageFormatFromExtension(fileMessage.getPayload().getName());
		ValidationUtils.assertNotNull(messageFormat, "Could not determine swift message format from its file name " + fileMessage.getPayload().getName());

		if (SwiftMessageFileUtils.isRegularMessage(fileMessage.getPayload().getName())) {
			String fileContents = SwiftMessageFileUtils.getMessageFileContents(fileMessage);
			SwiftMessageLog swiftMessageLog = saveSwiftLogMessage(fileContents, true, messageFormat, String.format("SWIFT local authentication failure for file [%s]", fileMessage.getPayload().getName()));
			sendSwiftFailedMessages(swiftMessageLog);
		}
		else if (SwiftMessageFileUtils.isLauMessage(fileMessage.getPayload().getName())) {
			LogUtils.error(LogCommand.ofMessage(SwiftMessageLauDiscardHandler.class, "SWIFT local authentication timed out waiting for '*.fin' file [%s]",
					fileMessage.getPayload().getName()));
		}
		else {
			throw new RuntimeException("Unknown reception file type " + fileMessage.getPayload().getName());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void sendFailedStatusMessage(long swiftMessageId) {
		SwiftMessage swiftMessage = getSwiftServerMessageService().getSwiftMessage(swiftMessageId);
		if (swiftMessage != null) {
			SwiftIdentifier swiftIdentifier = swiftMessage.getIdentifier();
			if (swiftIdentifier != null) {
				getSwiftServerStatusHandler().sendSwiftExportStatus(
						swiftIdentifier.getUniqueStringIdentifier(),
						swiftIdentifier.getSourceSystemModule().getSourceSystem().getName(),
						swiftIdentifier.getSourceSystemModule().getName(),
						InstructionStatuses.FAILED,
						null
				);
			}
		}
	}


	private SwiftMessageLog saveSwiftLogMessage(String text, boolean incoming, SwiftMessageFormats messageFormat, String errorMessage) {
		SwiftMessageLog messageLog = new SwiftMessageLog();
		messageLog.setText(text);
		messageLog.setMessageDateAndTime(new Date());
		messageLog.setIncoming(incoming);
		messageLog.setMessageFormat(messageFormat);
		messageLog.setErrorMessage(errorMessage);
		return getSwiftServerMessageLogService().saveSwiftMessageLog(messageLog);
	}


	private void sendSwiftFailedMessages(SwiftMessageLog swiftMessageLog) {
		if (!StringUtils.isEmpty(swiftMessageLog.getText())) {
			Map<String, M> messageList = getSwiftMessageConverterHandler().parse(swiftMessageLog.getText(), swiftMessageLog.getMessageFormat());
			for (Map.Entry<String, M> entry : messageList.entrySet()) {
				saveSwiftLogMessage(entry.getKey(), true, swiftMessageLog.getMessageFormat(), swiftMessageLog.getErrorMessage());
				SwiftIdentifier swiftIdentifier = getSwiftIdentifierHandler().getSwiftIdentifierForMessage(entry.getValue());
				if (Objects.nonNull(swiftIdentifier)) {
					SwiftMessageSearchForm swiftMessageSearchForm = new SwiftMessageSearchForm();
					swiftMessageSearchForm.setIdentifierId(swiftIdentifier.getId());
					List<SwiftMessage> swiftMessages = CollectionUtils.asNonNullList(getSwiftServerMessageService().getSwiftMessageList(swiftMessageSearchForm));
					for (SwiftMessage swiftMessage : swiftMessages) {
						sendFailedStatusMessage(swiftMessage.getId());
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageConverterHandler<M> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<M> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}


	public SwiftServerStatusHandler getSwiftServerStatusHandler() {
		return this.swiftServerStatusHandler;
	}


	public void setSwiftServerStatusHandler(SwiftServerStatusHandler swiftServerStatusHandler) {
		this.swiftServerStatusHandler = swiftServerStatusHandler;
	}


	public SwiftIdentifierHandler<M> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<M> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}


	public SwiftMessageLogService getSwiftServerMessageLogService() {
		return this.swiftServerMessageLogService;
	}


	public void setSwiftServerMessageLogService(SwiftMessageLogService swiftServerMessageLogService) {
		this.swiftServerMessageLogService = swiftServerMessageLogService;
	}
}
