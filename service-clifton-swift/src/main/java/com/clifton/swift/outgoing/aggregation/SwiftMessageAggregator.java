package com.clifton.swift.outgoing.aggregation;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.outgoing.AggregatedSwiftMessage;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import org.springframework.integration.aggregator.AbstractAggregatingMessageGroupProcessor;
import org.springframework.integration.aggregator.CorrelationStrategy;
import org.springframework.integration.aggregator.ReleaseStrategy;
import org.springframework.integration.store.MessageGroup;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * <code>SwiftMessageAggregator</code>  The Aggregator combines a group of Swift messages, by correlating and storing them, until the group is deemed complete.
 * At that point, the Aggregator will create a single message by processing the whole group, and will send that aggregated message as output.
 * <p>
 * public final Object processMessageGroup(MessageGroup group)
 */
public class SwiftMessageAggregator extends AbstractAggregatingMessageGroupProcessor implements CorrelationStrategy, ReleaseStrategy {

	@Override
	protected Object aggregatePayloads(MessageGroup group, Map<String, Object> defaultHeaders) {
		AssertUtils.assertNotNull(group, "Expected a Message Group");
		AssertUtils.assertNotNull(group.getMessages(), "Expected a message list");

		Message<?> firstMessage = group.getOne();
		AssertUtils.assertNotNull(firstMessage, "Expected at least one message per file.");
		AssertUtils.assertNotNull(firstMessage.getPayload(), "Expected a message payload");

		SwiftMessage swiftMessage = SwiftMessage.class.cast(firstMessage.getPayload());
		if (swiftMessage.getMessageFormat() == SwiftMessageFormats.MT) {
			final List<Long> swiftMessageIds = new ArrayList<>();
			String concatenatedMsgText = group.getMessages()
					.stream()
					.map(Message::getPayload)
					.map(com.clifton.swift.message.SwiftMessage.class::cast)
					.filter(Objects::nonNull)
					.peek(s -> swiftMessageIds.add(s.getId()))
					.map(SwiftMessage::getText)
					.filter(s -> !StringUtils.isEmpty(s))
					.collect(Collectors.joining("$"));
			AggregatedSwiftMessage aggregatedSwiftMessage = new AggregatedSwiftMessage();
			aggregatedSwiftMessage.setMessageFormat(swiftMessage.getMessageFormat());
			aggregatedSwiftMessage.setText(concatenatedMsgText);
			aggregatedSwiftMessage.setAggregatedMessageList(swiftMessageIds);
			return MessageBuilder
					.withPayload(aggregatedSwiftMessage)
					.setHeader(SwiftServerOutgoingFileMessageHandler.PAYLOAD_MESSAGE_COUNT_KEY, group.getMessages().size())
					.build();
		}
		else {
			throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object getCorrelationKey(Message<?> message) {
		return message.getHeaders().get(SwiftServerOutgoingFileMessageHandler.PAYLOAD_GROUP_HEADER_KEY);
	}


	@Override
	public boolean canRelease(MessageGroup group) {
		return group.isComplete();
	}
}
