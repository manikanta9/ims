package com.clifton.swift.outgoing.aggregation;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessageFormats;

import java.io.Serializable;
import java.util.Objects;


/**
 * <code>SwiftMessageGroupKey</code> a group key consisting of the message format type and
 * a sequence number, normally, a sequence of '0' is used as the default, only when the
 * group size threshold has been reached will a new format specific message key be
 * generated with an incremented sequence.  The key with the non-zero sequence will
 * then reside in the store until it times out, at which time, the corresponding
 * group messages will be concatenated into an output file.
 */
public class SwiftMessageGroupKey implements Serializable {

	private final SwiftMessageFormats swiftMessageFormats;
	private final int sequence;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// should be constructed by the factory in order to keep the sequence numbers
	SwiftMessageGroupKey(SwiftMessageFormats swiftMessageFormats, int sequence) {
		AssertUtils.assertNotNull(swiftMessageFormats, "Swift Message Format is required.");
		AssertUtils.assertTrue(sequence >= 0, "Sequence must be greater or equal to zero.");

		this.swiftMessageFormats = swiftMessageFormats;
		this.sequence = sequence;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Get a new instance of a SwiftMessageGroupKey for the specified type, but using the default sequence (0).
	 */
	public static SwiftMessageGroupKey of(SwiftMessageFormats swiftMessageFormats) {
		return new SwiftMessageGroupKey(swiftMessageFormats, 0);
	}


	public static SwiftMessageGroupKey cast(Object groupObject) {
		AssertUtils.assertNotNull(groupObject, "Group key cannot be null.");
		AssertUtils.assertTrue(SwiftMessageGroupKey.class.isAssignableFrom(groupObject.getClass()), "Must be a SwiftMessageGroupKey");
		return SwiftMessageGroupKey.class.cast(groupObject);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		SwiftMessageGroupKey that = (SwiftMessageGroupKey) o;
		return this.swiftMessageFormats == that.swiftMessageFormats &&
				Objects.equals(this.sequence, that.sequence);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.swiftMessageFormats, this.sequence);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageFormats getSwiftMessageFormats() {
		return this.swiftMessageFormats;
	}


	public int getSequence() {
		return this.sequence;
	}
}
