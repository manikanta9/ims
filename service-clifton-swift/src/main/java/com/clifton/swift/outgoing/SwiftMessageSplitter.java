package com.clifton.swift.outgoing;

import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.utils.SwiftMtAuthenticationUtils;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * <code>SwiftMessageSplitter</code> accepts an Aggregated Message and outputs the supplied message unchanged.
 * If Local Authentication is enabled, an additional message is cloned from the supplied message, its
 * message text is set to the local authentication hash calculated from the supplied message message's text and
 * is outputted along with the supplied message.
 */
public class SwiftMessageSplitter {

	private String keyPath;
	private String key;
	private boolean localAuthenticationEnabled;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void init() {
		if (isLocalAuthenticationEnabled()) {
			this.key = SwiftMtAuthenticationUtils.readLauKey(this.keyPath);
		}
	}


	public List<Message<AggregatedSwiftMessage>> split(Message<AggregatedSwiftMessage> emissionMessage) {
		if (isLocalAuthenticationEnabled()) {
			AggregatedSwiftMessage aggregatedSwiftMessage = emissionMessage.getPayload();
			AggregatedSwiftMessage lauAggregatedSwiftMessage = new AggregatedSwiftMessage(aggregatedSwiftMessage, true);
			String lau = new String(SwiftMtAuthenticationUtils.encode(this.key, aggregatedSwiftMessage.getText()), StandardCharsets.UTF_8);
			lauAggregatedSwiftMessage.setText(lau);
			Message<AggregatedSwiftMessage> lauMessage = MessageBuilder
					.withPayload(lauAggregatedSwiftMessage)
					.setHeader(SwiftServerOutgoingFileMessageHandler.PAYLOAD_MESSAGE_COUNT_KEY, aggregatedSwiftMessage.getAggregatedMessageList().size())
					.build();
			return Arrays.asList(emissionMessage, lauMessage);
		}
		return Collections.singletonList(emissionMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setKeyPath(String keyPath) {
		this.keyPath = keyPath;
	}


	public boolean isLocalAuthenticationEnabled() {
		return this.localAuthenticationEnabled;
	}


	public void setLocalAuthenticationEnabled(boolean localAuthenticationEnabled) {
		this.localAuthenticationEnabled = localAuthenticationEnabled;
	}
}
