package com.clifton.swift.outgoing.aggregation;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessageFormats;

import java.util.concurrent.atomic.AtomicInteger;


/**
 * <code>SwiftMessageGroupKeyFactory</code> generate a format specific unique group message
 * key using the thread-safe AtomicInteger objects for sequence numbers.
 */
public class SwiftMessageGroupKeyFactory {

	private final AtomicInteger nextMtGroupSequence = new AtomicInteger(1);
	private final AtomicInteger nextMxGroupSequence = new AtomicInteger(1);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageGroupKey createGroupKey(SwiftMessageFormats swiftMessageFormats) {
		AssertUtils.assertNotNull(swiftMessageFormats, "SwiftMessageFormats cannot be null");

		int sequence;
		if (SwiftMessageFormats.MT == swiftMessageFormats) {
			sequence = this.nextMtGroupSequence.getAndIncrement();
		}
		else if (SwiftMessageFormats.MX == swiftMessageFormats) {
			sequence = this.nextMxGroupSequence.getAndIncrement();
		}
		else {
			throw new RuntimeException("Message format not supported ");
		}

		return new SwiftMessageGroupKey(swiftMessageFormats, sequence);
	}
}
