package com.clifton.swift.outgoing;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.outgoing.aggregation.SwiftMessageGroupKey;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;


/**
 * <code>SwiftEmissionMessageHandler</code> take a Swift message, convert the message to a string, save to  a file, name the file appropriately and place in the
 * emissions folder. All these operations are performed by Spring Integration, this only calls the sendMessage on the Spring integration wrapped class {@link
 * SwiftEmissionMessageGateway}
 */
@Component
public class SwiftServerOutgoingFileMessageHandlerImpl implements SwiftServerOutgoingFileMessageHandler {

	private SwiftEmissionMessageGateway swiftFinEmissionMessageGateway;
	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void send(SwiftMessage swiftMessage) {
		AssertUtils.assertNotNull(swiftMessage, "Swift Message cannot be null.");
		try {
			Message<SwiftMessage> emissionMessage = MessageBuilder
					.withPayload(swiftMessage)
					.setHeader(SwiftServerOutgoingFileMessageHandler.PAYLOAD_BYTES_HEADER_KEY, SwiftMessageFileUtils.getSwiftMessageTextByteSize(swiftMessage))
					// should be the default for the format, specifically, sequence number 0
					.setHeader(SwiftServerOutgoingFileMessageHandler.PAYLOAD_GROUP_HEADER_KEY, SwiftMessageGroupKey.of(swiftMessage.getMessageFormat()))
					.build();
			swiftMessage.setMessageStatus(getSwiftServerMessageService().getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.QUEUED.name()));
			swiftMessage = getSwiftServerMessageService().saveSwiftMessage(swiftMessage);
			getSwiftFinEmissionMessageGateway().sendMessage(emissionMessage);
		}
		catch (Exception e) {
			// allow this transaction to commit, the swift message status needs to be 'ERROR'
			swiftMessage.setMessageStatus(getSwiftServerMessageService().getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name()));
			getSwiftServerMessageService().saveSwiftMessage(swiftMessage);
			LogUtils.error(getClass(), "Failed to send swift message to emission folder.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftEmissionMessageGateway getSwiftFinEmissionMessageGateway() {
		return this.swiftFinEmissionMessageGateway;
	}


	public void setSwiftFinEmissionMessageGateway(SwiftEmissionMessageGateway swiftFinEmissionMessageGateway) {
		this.swiftFinEmissionMessageGateway = swiftFinEmissionMessageGateway;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
