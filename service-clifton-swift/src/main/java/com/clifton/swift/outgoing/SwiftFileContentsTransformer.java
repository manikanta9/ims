package com.clifton.swift.outgoing;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import org.springframework.integration.transformer.AbstractPayloadTransformer;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * <code>SwiftFileContentsTransformer</code> transformer used by the 'FileWritingMessageHandler' to get a String from
 * a Swift Message.
 * Trivial implementation, this is used by Spring integration to convert a SwiftMessage to a String in the format necessary
 * for the Swift file.
 */
public class SwiftFileContentsTransformer extends AbstractPayloadTransformer<AggregatedSwiftMessage, String> {

	private SwiftMessageService swiftServerMessageService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String transformPayload(AggregatedSwiftMessage aggregatedSwiftMessage) {
		AssertUtils.assertNotNull(aggregatedSwiftMessage, "Expected an aggregated swiftMessage");
		updateMessageStatus(aggregatedSwiftMessage);
		return aggregatedSwiftMessage.getText();
	}


	/**
	 * Update all the message statuses to COMPLETE.
	 * WARNING:  Failure to update messages to COMPLETE will result in the message being sent more than once upon server restart.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void updateMessageStatus(AggregatedSwiftMessage aggregatedSwiftMessage) {
		AssertUtils.assertNotNull(aggregatedSwiftMessage, "Aggregated message parameter is required.");

		SwiftMessageStatus completedStatus = getSwiftServerMessageService().getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.COMPLETE.name());
		AssertUtils.assertNotNull(completedStatus, "The SWIFT message status [%s] could not be found.", SwiftMessageStatus.SwiftMessageStatusNames.COMPLETE);

		for (long id : aggregatedSwiftMessage.getAggregatedMessageList()) {
			SwiftMessage swiftMessage = getSwiftServerMessageService().getSwiftMessage(id);
			swiftMessage.setMessageStatus(completedStatus);
			getSwiftServerMessageService().saveSwiftMessage(swiftMessage);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}
}
