package com.clifton.swift.outgoing;

import com.clifton.core.logging.LogUtils;
import com.clifton.swift.error.handler.SwiftExceptionHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessagingException;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageBuilder;


/**
 * <code>SwiftEmissionChannelInterceptor</code> handle errors attempting to write a file to the Emissions folder (Windows share is unavailable) and marks all effected messages with 'ERROR'.
 */
public class SwiftEmissionChannelInterceptor implements ChannelInterceptor {

	private SwiftExceptionHandler swiftExceptionHandler;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
		if (!sent) {
			if (ex != null) {
				LogUtils.error(this.getClass(), String.format("Exception when writing to output channel [%s].", channel.toString()), ex);
			}
			else {
				LogUtils.error(this.getClass(), String.format("Failed to send SWIFT message to output channel [%s].", channel.toString()));
			}
			// update all affected messages to 'ERROR'.
			MessagingException messagingException = new MessagingException(message);
			Message<Exception> exceptionMessage = MessageBuilder.createMessage(messagingException, message.getHeaders());
			getSwiftExceptionHandler().handleExceptionMessage(exceptionMessage);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftExceptionHandler getSwiftExceptionHandler() {
		return this.swiftExceptionHandler;
	}


	public void setSwiftExceptionHandler(SwiftExceptionHandler swiftExceptionHandler) {
		this.swiftExceptionHandler = swiftExceptionHandler;
	}
}
