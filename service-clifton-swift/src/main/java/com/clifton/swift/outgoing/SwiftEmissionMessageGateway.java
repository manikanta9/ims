package com.clifton.swift.outgoing;

import com.clifton.swift.message.SwiftMessage;
import org.springframework.messaging.Message;


/**
 * <code>SwiftEmissionFileService</code> no implementation class, this is used by Spring integration and is
 * wrapped in an AOP aspect.
 */
public interface SwiftEmissionMessageGateway {

	public void sendMessage(Message<SwiftMessage> emissionMessage);
}
