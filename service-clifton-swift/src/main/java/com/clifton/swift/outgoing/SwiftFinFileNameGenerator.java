package com.clifton.swift.outgoing;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.utils.SwiftMessageFileExtensions;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.springframework.messaging.Message;


/**
 * <code>SwiftFinFileNameGenerator</code>
 * The Swift file name has the format:
 * [node][random integer > 0 and < bound].[yyyymmdd]_[hhmmss].fin
 * or
 * [node][random integer > 0 and < bound].[yyyymmdd]_[hhmmss].fin.lau
 * The .lau file should have the same filename as its associated fin.lau file.
 */
public class SwiftFinFileNameGenerator {

	public String generate(Message<AggregatedSwiftMessage> emissionMessage) {
		AssertUtils.assertNotNull(emissionMessage, "Message cannot be null.");
		AssertUtils.assertNotNull(emissionMessage.getPayload(), "Message payload cannot be null.");

		AggregatedSwiftMessage aggregatedSwiftMessage = emissionMessage.getPayload();
		SwiftMessageFormats messageFormat = aggregatedSwiftMessage.getMessageFormat();
		AssertUtils.assertNotNull(messageFormat, "Could not determine file type from message.");
		if (messageFormat == SwiftMessageFormats.MT) {
			SwiftMessageFileExtensions fileExtensions = SwiftMessageFileUtils.findExtensionForFormat(messageFormat);
			AssertUtils.assertNotNull(fileExtensions, "Could not determine file extension for format " + messageFormat);
			if (fileExtensions != null) {
				String filename = fileExtensions.generateFilename(aggregatedSwiftMessage.getSequence(), aggregatedSwiftMessage.getCreated());
				AssertUtils.assertTrue(fileExtensions.isValidFilename(filename), "The Swift generated file name is not valid " + filename);
				return filename + (aggregatedSwiftMessage.isLauMessageText() ? ".lau" : "");
			}
		}
		throw new RuntimeException(SwiftMessageConverterHandler.SWIFT_MESSAGE_FORMAT_TYPE_IS_NOT_SUPPORTED);
	}
}
