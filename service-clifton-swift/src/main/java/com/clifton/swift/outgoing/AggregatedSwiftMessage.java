package com.clifton.swift.outgoing;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessageFormats;

import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * <code>AggregatedSwiftMessage</code> stores the concatenated list of Swift Messages after it is released by
 * the {@link com.clifton.swift.outgoing.aggregation.SwiftMessageAggregator}, unless Local Authentication is
 * enabled, in which case, the original message is cloned and the message text is replaced with the Local
 * Authentication Hash.
 */
public class AggregatedSwiftMessage {

	// generate sequence number for generating outgoing file names. (0 to Short.MAX_VALUE inclusive)
	public static final SecureRandom random = new SecureRandom();

	private long id;
	// necessary to build file name extension.
	private SwiftMessageFormats messageFormat;
	// concatenated messages.
	private String text;
	// the individual message IDs for the concatenated messages.
	private List<Long> aggregatedMessageList;
	// is this a local authentication message.
	private boolean lauMessageText;
	// used to build the filename, must be the same for both the .fin and .fin.lau files.
	private LocalDateTime created = LocalDateTime.now();
	// used to build the filename, must be the same for both the .fin and .fin.lau files.
	private final String sequence;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Called from the SwiftMessageAggregator, build a normal message.
	 */
	public AggregatedSwiftMessage() {
		this(1);
	}


	/**
	 * Called to change the default node name "1".  Currently, the node must be numeric.
	 * The node is prepended to the outgoing file names to ensure multiple swift instances
	 * generate unique file names - move to property file when necessary
	 */
	public AggregatedSwiftMessage(int node) {
		// building the file sequence here so the lau and fin can be outputted in any order.
		this.sequence = Integer.toString(node) + Integer.toString(random.nextInt(Short.MAX_VALUE + 1));
	}


	/**
	 * Called by the Spring Integration Splitter method, this is the Local Authentication
	 * version of the normal aggregated message.
	 */
	public AggregatedSwiftMessage(AggregatedSwiftMessage copy, boolean lauMessageText) {
		AssertUtils.assertNotNull(copy, "Expected a source message.");
		AssertUtils.assertTrue(lauMessageText, "Expected this constructor to be used for lau.");

		this.id = copy.id;
		this.messageFormat = copy.messageFormat;
		this.text = copy.text;
		this.aggregatedMessageList = new ArrayList<>(copy.aggregatedMessageList);

		// file name components for lau must match the associated fin
		this.created = copy.created;
		this.sequence = copy.sequence;

		this.lauMessageText = lauMessageText;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getId() {
		return this.id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public SwiftMessageFormats getMessageFormat() {
		return this.messageFormat;
	}


	public void setMessageFormat(SwiftMessageFormats messageFormat) {
		this.messageFormat = messageFormat;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public List<Long> getAggregatedMessageList() {
		return this.aggregatedMessageList;
	}


	public void setAggregatedMessageList(List<Long> aggregatedMessageList) {
		this.aggregatedMessageList = aggregatedMessageList;
	}


	public boolean isLauMessageText() {
		return this.lauMessageText;
	}


	public void setLauMessageText(boolean lauMessageText) {
		this.lauMessageText = lauMessageText;
	}


	public LocalDateTime getCreated() {
		return this.created;
	}


	public String getSequence() {
		return this.sequence;
	}
}
