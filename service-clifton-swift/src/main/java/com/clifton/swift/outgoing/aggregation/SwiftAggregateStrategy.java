package com.clifton.swift.outgoing.aggregation;

import com.clifton.swift.message.SwiftMessageFormats;


/**
 * <code>SwiftAggregateStrategy</code> Determines whether the message represented by the provided bytes can
 * be added to the the group represented by the provided bytes based on maximum group bytes plus a group
 * size tolerance.
 */
public class SwiftAggregateStrategy {

	private int maximumPayloadBytes;
	private int tolerancePayloadBytes;
	private SwiftMessageFormats swiftMessageFormats;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean canAddMessageToGroup(int groupBytes, int messageBytes) {
		return (groupBytes + messageBytes + this.tolerancePayloadBytes) <= this.maximumPayloadBytes;
	}


	public boolean appliesToFormat(SwiftMessageFormats swiftMessageFormats) {
		return this.swiftMessageFormats == swiftMessageFormats;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getMaximumPayloadBytes() {
		return this.maximumPayloadBytes;
	}


	public void setMaximumPayloadBytes(int maximumPayloadBytes) {
		this.maximumPayloadBytes = maximumPayloadBytes;
	}


	public int getTolerancePayloadBytes() {
		return this.tolerancePayloadBytes;
	}


	public void setTolerancePayloadBytes(int tolerancePayloadBytes) {
		this.tolerancePayloadBytes = tolerancePayloadBytes;
	}


	public SwiftMessageFormats getSwiftMessageFormats() {
		return this.swiftMessageFormats;
	}


	public void setSwiftMessageFormats(SwiftMessageFormats swiftMessageFormats) {
		this.swiftMessageFormats = swiftMessageFormats;
	}
}
