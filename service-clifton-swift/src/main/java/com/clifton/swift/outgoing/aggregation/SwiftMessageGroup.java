package com.clifton.swift.outgoing.aggregation;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessageFormats;
import org.springframework.integration.store.MessageGroup;
import org.springframework.messaging.Message;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * <code>SwiftMessageGroup</code> maintain a list of messages that will be concatenated together
 * in the same message file.
 */
public class SwiftMessageGroup implements MessageGroup {

	public final BlockingQueue<Message<?>> messages = new LinkedBlockingQueue<>();
	private final SwiftMessageGroupKey swiftMessageGroupKey;
	private final long timestamp = System.currentTimeMillis();

	private volatile long lastModified;
	private volatile boolean complete;
	private final AtomicInteger payloadBytes = new AtomicInteger(0);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageGroup(SwiftMessageGroupKey swiftMessageGroupKey) {
		this.swiftMessageGroupKey = swiftMessageGroupKey;

		this.lastModified = 0;
		this.complete = false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// SwiftMessageStore is the only one that should call this
	boolean addMessage(Message<?> message, int messageBytes) {
		AssertUtils.assertNotNull(message, "Message cannot be null.");
		AssertUtils.assertNotNull(message.getPayload(), "Message payload cannot be null.");

		this.messages.add(message);
		// increment the cumulative group byte count by the message size.
		this.payloadBytes.addAndGet(messageBytes);
		setLastModified(System.currentTimeMillis());

		// always add, the store should have determined if the size constraints are correct.
		return true;
	}


	SwiftMessageGroup clone(SwiftMessageGroupKey newMessageGroupKey) {
		AssertUtils.assertNotNull(newMessageGroupKey, "MessageGroupKey cannot be null.");
		AssertUtils.assertFalse(newMessageGroupKey.equals(this.swiftMessageGroupKey), "The cloned message group must have a unique key.");

		SwiftMessageGroup clonedSwiftMessageGroup = new SwiftMessageGroup(newMessageGroupKey);
		clonedSwiftMessageGroup.lastModified = this.lastModified;
		clonedSwiftMessageGroup.complete = this.complete;
		clonedSwiftMessageGroup.setPayloadBytes(this.payloadBytes.get());
		clonedSwiftMessageGroup.messages.addAll(this.getMessages());

		return clonedSwiftMessageGroup;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void add(Message<?> messageToAdd) {
		// SwiftMessageStore should be the only thing adding messages, and it should do so via #addMessage
		throw new UnsupportedOperationException("The \"add\" method is not supported.");
	}


	@Override
	public boolean canAdd(Message<?> message) {
		// this must return true or it will be directed to the discarded or null stream.
		return true;
	}


	@Override
	public boolean remove(Message<?> messageToRemove) {
		throw new UnsupportedOperationException("The \"remove\" method is not supported.");
	}


	@Override
	public void clear() {
		// Groups are not reused
		throw new UnsupportedOperationException("The \"clear\" method is not supported.");
	}


	@Override
	public Collection<Message<?>> getMessages() {
		return Collections.unmodifiableCollection(this.messages);
	}


	@Override
	public Object getGroupId() {
		return this.swiftMessageGroupKey;
	}


	@Override
	public void setLastReleasedMessageSequenceNumber(int sequenceNumber) {
		// We don't use the sequence number for aggregation
		// noop
	}


	@Override
	public int getLastReleasedMessageSequenceNumber() {
		return 0;
	}


	@Override
	public boolean isComplete() {
		return this.complete;
	}


	public void setComplete(boolean complete) {
		this.complete = complete;
	}


	@Override
	public void complete() {
		this.complete = true;
	}


	@Override
	public int getSequenceSize() {
		return 0;
	}


	@Override
	public int size() {
		return this.messages.size();
	}


	@Override
	public Message<?> getOne() {
		return this.messages.peek();
	}


	@Override
	public long getTimestamp() {
		return this.timestamp;
	}


	@Override
	public long getLastModified() {
		return this.lastModified;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}


	public SwiftMessageFormats getSwiftMessageFormats() {
		return this.swiftMessageGroupKey.getSwiftMessageFormats();
	}


	public int getPayloadBytes() {
		// add in the size of the delimiters.
		return this.payloadBytes.get() + this.messages.size() - 1;
	}


	public void setPayloadBytes(int payloadBytes) {
		this.payloadBytes.set(payloadBytes);
	}
}
