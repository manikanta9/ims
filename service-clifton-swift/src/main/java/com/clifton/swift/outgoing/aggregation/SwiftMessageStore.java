package com.clifton.swift.outgoing.aggregation;

import com.clifton.core.util.AssertUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.store.AbstractMessageGroupStore;
import org.springframework.integration.store.ChannelMessageStore;
import org.springframework.integration.store.MessageGroup;
import org.springframework.integration.store.MessageGroupMetadata;
import org.springframework.integration.store.MessageMetadata;
import org.springframework.integration.store.MessageStore;
import org.springframework.integration.support.locks.DefaultLockRegistry;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;


/**
 * <code>SwiftMessageStore</code>
 * Based on the {@link org.springframework.integration.store.SimpleMessageStore}, the store will keep format segregated groups until
 * a payload byte-size threshold has been reached.  The threshold will be reached when subsequent messages cannot be added to the group
 * without exceeding the maximum payload size in bytes.  The full group will be cloned to a new group with a unique key and will reside
 * in the store long enough to timeout.  Subsequent messages are added to a newly constructed default, empty message format specific
 * group with sequence number zero.
 */
public class SwiftMessageStore extends AbstractMessageGroupStore implements MessageStore, ChannelMessageStore, ApplicationContextAware {

	private final ConcurrentMap<SwiftMessageGroupKey, SwiftMessageGroup> groupIdToMessageGroup;

	private final LockRegistry groupLockRegistry;

	private SwiftMessageGroupKeyFactory swiftMessageGroupKeyFactory;
	private List<SwiftAggregateStrategy> swiftAggregateStrategies;

	private String reprocessChannelName;
	private final MessagingTemplate messagingTemplate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageStore() {
		this.groupLockRegistry = new DefaultLockRegistry();
		this.groupIdToMessageGroup = new ConcurrentHashMap<>();
		this.messagingTemplate = new MessagingTemplate();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@ManagedAttribute
	public long getMessageCount() {
		return this.groupIdToMessageGroup.values().stream().mapToLong(g -> g.getMessages().size()).sum();
	}


	@Override
	public MessageGroup getMessageGroup(Object groupId) {
		Lock lock = this.groupLockRegistry.obtain(groupId);
		try {
			lock.lockInterruptibly();
			try {
				SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.cast(groupId);
				SwiftMessageGroup group = this.groupIdToMessageGroup.get(swiftMessageGroupKey);
				if (group == null) {
					group = new SwiftMessageGroup(swiftMessageGroupKey);
					this.groupIdToMessageGroup.putIfAbsent(swiftMessageGroupKey, group);
				}
				return group;
			}
			finally {
				lock.unlock();
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new MessagingException("Interrupted while obtaining lock", e);
		}
	}


	public SwiftMessageGroup cloneMessageGroup(SwiftMessageGroup swiftMessageGroup, SwiftMessageGroupKey groupId) {
		Lock lock = this.groupLockRegistry.obtain(groupId);
		try {
			lock.lockInterruptibly();
			try {
				SwiftMessageGroup group = this.groupIdToMessageGroup.get(groupId);
				if (group == null) {
					group = swiftMessageGroup.clone(groupId);
					group.setComplete(true);
					this.groupIdToMessageGroup.putIfAbsent(groupId, group);
				}
				else {
					throw new RuntimeException("The group with " + group + " should not be in the store already.");
				}
				return group;
			}
			finally {
				lock.unlock();
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new MessagingException("Interrupted while obtaining lock", e);
		}
	}


	@Override
	public MessageGroup addMessageToGroup(Object groupId, Message<?> message) {
		Lock lock = this.groupLockRegistry.obtain(groupId);
		try {
			lock.lockInterruptibly();
			try {
				SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.cast(groupId);
				SwiftMessageGroup group = this.groupIdToMessageGroup.get(swiftMessageGroupKey);
				if (group == null) {
					group = new SwiftMessageGroup(swiftMessageGroupKey);
					this.groupIdToMessageGroup.putIfAbsent(swiftMessageGroupKey, group);
				}
				int messageBytes = getMessageByteHeaderValue(message);
				if (canAddToGroup(group, message)) {
					// there is enough space to add this to an existing group
					group.addMessage(message, messageBytes);
				}
				else {
					// clone this group to a new one (sequence > 0) and set it to complete.
					SwiftMessageGroupKey completedGroupId = getSwiftMessageGroupKeyFactory().createGroupKey(group.getSwiftMessageFormats());
					// make sure sequence numbers are different, this determines which group is deleted.
					SwiftMessageGroup completedGroup = cloneMessageGroup(group, completedGroupId);
					// add a new group with the default group id (sequence 0).
					group = new SwiftMessageGroup(swiftMessageGroupKey);
					// replace old group with new (sequence 0).
					this.groupIdToMessageGroup.replace(swiftMessageGroupKey, group);
					// recursive call to create new group and add overflow message to it (sequence == 0).
					this.messagingTemplate.send(this.reprocessChannelName, message);
					// after recursive call, reset group to the completed (sequence > 0)
					group = completedGroup;
				}
				return group;
			}
			finally {
				lock.unlock();
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new MessagingException(message, "Interrupted while obtaining lock", e);
		}
	}


	@Override
	public void addMessagesToGroup(Object groupId, Message<?>... messages) {
		// This is only called by CorrelatingMessageBarrier#handleMessageInternal, which is currently not used by the system
		throw new UnsupportedOperationException("The \"addMessagesToGroup\" method is not supported.");
	}


	@Override
	public Collection<Message<?>> getMessagesForGroup(Object groupId) {
		return getMessageGroup(groupId).getMessages();
	}


	@Override
	public void removeMessageGroup(Object groupId) {
		Lock lock = this.groupLockRegistry.obtain(groupId);
		try {
			lock.lockInterruptibly();
			try {
				SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.cast(groupId);
				if (!this.groupIdToMessageGroup.containsKey(swiftMessageGroupKey)) {
					return;
				}

				this.groupIdToMessageGroup.remove(swiftMessageGroupKey);
			}
			finally {
				lock.unlock();
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new MessagingException("Interrupted while obtaining lock", e);
		}
	}


	@Override
	public void completeGroup(Object groupId) {
		Lock lock = this.groupLockRegistry.obtain(groupId);
		try {
			lock.lockInterruptibly();
			try {
				SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.cast(groupId);
				SwiftMessageGroup group = this.groupIdToMessageGroup.get(swiftMessageGroupKey);
				AssertUtils.assertNotNull(group, "MessageGroup for groupId '" + groupId + "' " +
						"can not be located while attempting to complete the MessageGroup");
				group.complete();
				group.setLastModified(System.currentTimeMillis());
			}
			finally {
				lock.unlock();
			}
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			throw new MessagingException("Interrupted while obtaining lock", e);
		}
	}


	@Override
	public MessageGroupMetadata getGroupMetadata(Object groupId) {
		return new MessageGroupMetadata(this.getMessageGroup(groupId));
	}


	@Override
	public Message<?> getOneMessageFromGroup(Object groupId) {
		return this.getMessageGroup(groupId).getOne();
	}


	@Override
	public Iterator<MessageGroup> iterator() {
		return new HashSet<MessageGroup>(this.groupIdToMessageGroup.values()).iterator();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MessageGroup removeMessageFromGroup(Object groupId, Message<?> messageToRemove) {
		AssertUtils.fail("Removing a message is not supported.");
		return null;
	}


	@Override
	public void removeMessagesFromGroup(Object groupId, Collection<Message<?>> messages) {
		AssertUtils.fail("Removing a messages from group is not supported.");
	}


	@Override
	public void setLastReleasedSequenceNumberForGroup(Object groupId, int sequenceNumber) {
		AssertUtils.fail("Sequence numbers are not supported.");
	}


	@Override
	public Message<?> pollMessageFromGroup(Object groupId) {
		AssertUtils.fail("Removing a message is not supported.");
		return null;
	}


	@Override
	public int messageGroupSize(Object groupId) {
		return this.getMessageGroup(groupId).size();
	}


	@Override
	protected MessageGroup copy(MessageGroup group) {
		AssertUtils.fail("Copying a group is not supported.");
		return null;
	}


	@Override
	public <T> Message<T> addMessage(Message<T> message) {
		AssertUtils.fail("Only allows adding to a group.");
		return null;
	}


	@Override
	public Message<?> getMessage(UUID key) {
		AssertUtils.fail("Cannot fetch messages directly.");
		return null;
	}


	@Override
	public Message<?> removeMessage(UUID key) {
		AssertUtils.fail("Removing a message is not supported.");
		return null;
	}


	@Override
	public MessageMetadata getMessageMetadata(UUID id) {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean canAddToGroup(SwiftMessageGroup messageGroup, Message<?> message) {
		AssertUtils.assertNotNull(messageGroup, "MessageGroup cannot be null");
		AssertUtils.assertNotNull(message, "Message cannot be null.");

		SwiftMessageFormats format = getSwiftMessageFormat(message);
		if (format != messageGroup.getSwiftMessageFormats()) {
			return false;
		}
		for (SwiftAggregateStrategy swiftAggregateStrategy : getSwiftAggregateStrategies()) {
			if (swiftAggregateStrategy.appliesToFormat(format)) {
				return swiftAggregateStrategy.canAddMessageToGroup(messageGroup.getPayloadBytes(), getMessageByteHeaderValue(message));
			}
		}
		throw new RuntimeException("Expected an Aggregation Strategy for the message format " + format);
	}


	private int getMessageByteHeaderValue(Message<?> message) {
		AssertUtils.assertNotNull(message, "Message cannot be null.");
		AssertUtils.assertNotNull(message.getPayload(), "Message payload cannot be null.");
		AssertUtils.assertTrue(message.getHeaders().containsKey(SwiftServerOutgoingFileMessageHandler.PAYLOAD_BYTES_HEADER_KEY), "Message must have bytes header.");
		Integer bytes = message.getHeaders().get(SwiftServerOutgoingFileMessageHandler.PAYLOAD_BYTES_HEADER_KEY, Integer.class);
		AssertUtils.assertNotNull(bytes, "Message must have a bytes header value.");

		return bytes;
	}


	private SwiftMessageFormats getSwiftMessageFormat(Message<?> message) {
		AssertUtils.assertNotNull(message, "Message cannot be null.");
		AssertUtils.assertNotNull(message.getPayload(), "Message payload cannot be null.");
		AssertUtils.assertTrue(SwiftMessage.class.isAssignableFrom(message.getPayload().getClass()), "Expected a SwiftMessage Payload. " + message.getPayload().getClass());

		return SwiftMessage.class.cast(message.getPayload()).getMessageFormat();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageGroupKeyFactory getSwiftMessageGroupKeyFactory() {
		return this.swiftMessageGroupKeyFactory;
	}


	public void setSwiftMessageGroupKeyFactory(SwiftMessageGroupKeyFactory swiftMessageGroupKeyFactory) {
		this.swiftMessageGroupKeyFactory = swiftMessageGroupKeyFactory;
	}


	public List<SwiftAggregateStrategy> getSwiftAggregateStrategies() {
		return this.swiftAggregateStrategies;
	}


	public void setSwiftAggregateStrategies(List<SwiftAggregateStrategy> swiftAggregateStrategies) {
		this.swiftAggregateStrategies = swiftAggregateStrategies;
	}


	public String getReprocessChannelName() {
		return this.reprocessChannelName;
	}


	public void setReprocessChannelName(String reprocessChannelName) {
		this.reprocessChannelName = reprocessChannelName;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.messagingTemplate.setBeanFactory(applicationContext);
	}
}
