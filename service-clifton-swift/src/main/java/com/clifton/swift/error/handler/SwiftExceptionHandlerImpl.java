package com.clifton.swift.error.handler;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.SystemUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.converter.SwiftMessageConverterHandler;
import com.clifton.swift.message.search.SwiftMessageSearchForm;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.messaging.error.SwiftMessagingErrorService;
import com.clifton.swift.outgoing.AggregatedSwiftMessage;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;


/**
 * <code>SwiftExceptionHandlerImpl</code> handles Spring integration exceptions by updating the Swift Message Status to FAILED.
 * If anything goes wrong, skip the Swift Message and move on to the next, if any unexpected errors occur, log the error and put an
 * error message on the JMS error queue.
 */
public class SwiftExceptionHandlerImpl<M> implements SwiftExceptionHandler {

	private SwiftIdentifierHandler<M> swiftIdentifierHandler;
	private SwiftMessageConverterHandler<M> swiftMessageConverterHandler;
	private SwiftMessageService swiftServerMessageService;
	private SwiftMessagingErrorService swiftMessagingErrorService;
	private SwiftServerStatusHandler swiftServerStatusHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void handleExceptionMessage(Message<Exception> exceptionMessage) {
		try {
			List<Long> swiftMessageIdList = new ArrayList<>();
			if (isAggregatedSwiftMessage(exceptionMessage)) {
				MessagingException messagingException = MessagingException.class.cast(exceptionMessage.getPayload());
				AggregatedSwiftMessage aggregatedMsg = AggregatedSwiftMessage.class.cast(AssertUtils.assertNotNull(messagingException.getFailedMessage(), "Aggregated swift messaging exception is null.").getPayload());
				AssertUtils.assertNotNull(aggregatedMsg, "Expected an Aggregated Swift Message.");
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), messagingException, () -> getFailedMessageText(messagingException) + aggregatedMsg.getText()));
				swiftMessageIdList.addAll(aggregatedMsg.getAggregatedMessageList());
			}
			else if (isSwiftMessage(exceptionMessage)) {
				MessagingException messagingException = MessagingException.class.cast(exceptionMessage.getPayload());
				SwiftMessage swiftMessage = SwiftMessage.class.cast(AssertUtils.assertNotNull(messagingException.getFailedMessage(), "Swift message exception is null.").getPayload());
				AssertUtils.assertNotNull(swiftMessage, "Expected a Swift Message.");
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), messagingException, () -> getFailedMessageText(messagingException) + swiftMessage.getText()));
				swiftMessageIdList.add(swiftMessage.getId());
			}
			else if (isStringSwiftMessage(exceptionMessage)) {
				MessagingException messagingException = MessagingException.class.cast(exceptionMessage.getPayload());
				String stringMessage = String.class.cast(AssertUtils.assertNotNull(messagingException.getFailedMessage(), "String swift messaging exception is null.").getPayload());
				AssertUtils.assertNotNull(stringMessage, "Expected a Swift Message String.");
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), messagingException, () -> getFailedMessageText(messagingException) + stringMessage));
				swiftMessageIdList.addAll(getSwiftMessageIdentifierList(stringMessage));
			}
			else {
				throw exceptionMessage.getPayload();
			}
			updateFailedSwiftMessages(swiftMessageIdList);
		}
		catch (Exception ex) {
			UUID uuid = UUID.randomUUID();
			String failedMessage = "Failed to process messages during an exception, writing to error channel with uuid " + uuid;
			LogUtils.error(getClass(), failedMessage, ex);
			failedMessage += ex.getMessage();
			outputMessageToErrorChannel(exceptionMessage, ex, uuid, failedMessage);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getFailedMessageText(MessagingException exceptionMessage) {
		if (exceptionMessage != null) {
			return exceptionMessage.getMessage();
		}
		return StringUtils.EMPTY_STRING;
	}


	private boolean isAggregatedSwiftMessage(Message<Exception> exceptionMessage) {
		if (exceptionMessage != null && exceptionMessage.getPayload() instanceof MessagingException) {
			MessagingException exceptionMessagePayload = ((MessagingException) exceptionMessage.getPayload());
			return exceptionMessagePayload.getFailedMessage() != null && exceptionMessagePayload.getFailedMessage().getPayload() instanceof AggregatedSwiftMessage;
		}
		return false;
	}


	private boolean isSwiftMessage(Message<Exception> exceptionMessage) {
		if (exceptionMessage != null && exceptionMessage.getPayload() instanceof MessagingException) {
			MessagingException exceptionMessagePayload = ((MessagingException) exceptionMessage.getPayload());
			return exceptionMessagePayload.getFailedMessage() != null && exceptionMessagePayload.getFailedMessage().getPayload() instanceof SwiftMessage;
		}
		return false;
	}


	private boolean isStringSwiftMessage(Message<Exception> exceptionMessage) {
		if (exceptionMessage != null && exceptionMessage.getPayload() instanceof MessagingException) {
			MessagingException exceptionMessagePayload = ((MessagingException) exceptionMessage.getPayload());
			return exceptionMessagePayload.getFailedMessage() != null && exceptionMessagePayload.getFailedMessage().getPayload() instanceof String;
		}
		return false;
	}


	/**
	 * From the Swift message string:
	 * parse into Abstract Swift Messages
	 * get the reference string
	 * get swift identifiers for each reference string
	 * using identifier IDs get the SwiftMessage IDs.
	 */
	private List<Long> getSwiftMessageIdentifierList(String stringMessage) {
		if (!StringUtils.isEmpty(stringMessage)) {
			SwiftMessageFormats messageFormat = stringMessage.trim().startsWith("{") ? SwiftMessageFormats.MT : SwiftMessageFormats.MX;
			Map<String, M> messageList = getSwiftMessageConverterHandler().parse(stringMessage, messageFormat);
			// handles both identifier formats.
			List<Long> identifiers = messageList.values()
					.stream()
					.map(m -> getSwiftIdentifierHandler().getSwiftIdentifierForMessage(m))
					.filter(Objects::nonNull)
					.map(BaseSimpleEntity::getId)
					.collect(Collectors.toList());
			return identifiers
					.stream()
					.flatMap(id -> {
						SwiftMessageSearchForm swiftMessageSearchForm = new SwiftMessageSearchForm();
						swiftMessageSearchForm.setIdentifierId(id);
						return CollectionUtils.asNonNullList(getSwiftServerMessageService().getSwiftMessageList(swiftMessageSearchForm)).stream();
					})
					.filter(Objects::nonNull)
					.map(SwiftMessage::getId)
					.collect(Collectors.toList());
		}
		else {
			return Collections.emptyList();
		}
	}


	/**
	 * Update the provided list of Swift Messages' status to ERROR.
	 */
	private void updateFailedSwiftMessages(List<Long> messageIdList) {
		SwiftMessageStatus swiftMessageStatus = getSwiftServerMessageService().getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name());
		for (Long id : CollectionUtils.asNonNullList(messageIdList)) {
			try {
				updateSwiftMessage(id, swiftMessageStatus);
				sendFailedStatusMessage(id);
			}
			catch (Exception ex) {
				UUID uuid = UUID.randomUUID();
				LogUtils.error(getClass(), "Failed to update the Swift Message status to FAILED. ( ID:" + id + ") (UUID: " + uuid + ")", ex);
				outputMessageToErrorChannel("Failed to update the SwiftMessage status to FAILED " + "( ID:" + id + ")", ex, uuid);
			}
		}
	}


	/**
	 * Update Swift message to the provided status, this is done in its own transaction in order to facilitate update statuses on
	 * an aggregated list of Swift messages.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected SwiftMessage updateSwiftMessage(Long id, SwiftMessageStatus swiftMessageStatus) {
		if (id != null) {
			SwiftMessage swiftMessage = getSwiftServerMessageService().getSwiftMessage(id);
			if (swiftMessage != null) {
				swiftMessage.setMessageStatus(swiftMessageStatus);
				return getSwiftServerMessageService().saveSwiftMessage(swiftMessage);
			}
		}
		return null;
	}


	/**
	 * Send 'Failed' status message back for IMS for this message.
	 */
	private void sendFailedStatusMessage(long swiftMessageId) {
		SwiftMessage swiftMessage = getSwiftServerMessageService().getSwiftMessage(swiftMessageId);
		if (swiftMessage != null) {
			SwiftIdentifier swiftIdentifier = swiftMessage.getIdentifier();
			if (swiftIdentifier != null) {
				getSwiftServerStatusHandler().sendSwiftExportStatus(
						swiftIdentifier.getUniqueStringIdentifier(),
						swiftIdentifier.getSourceSystemModule().getSourceSystem().getName(),
						swiftIdentifier.getSourceSystemModule().getName(),
						InstructionStatuses.FAILED,
						null
				);
			}
		}
	}


	/**
	 * Only output to the JMS error channel when something completely unexpected occurs, every attempt should be made to update the status on the exception's Swift Message.
	 */
	private void outputMessageToErrorChannel(Message<Exception> exceptionMessage, Exception ex, UUID uuid, String failedMessage) {
		List<Long> messages = null;
		AggregatedSwiftMessage aggregatedMsg = isAggregatedSwiftMessage(exceptionMessage)
				? AggregatedSwiftMessage.class.cast(AssertUtils.assertNotNull(((MessagingException) exceptionMessage.getPayload()).getFailedMessage(), "Aggregated swift message failed message is null.").getPayload()) : null;
		if (aggregatedMsg != null) {
			messages = aggregatedMsg.getAggregatedMessageList();
		}
		else if (isSwiftMessage(exceptionMessage)) {
			SwiftMessage swiftMessage = SwiftMessage.class.cast(AssertUtils.assertNotNull(((MessagingException) exceptionMessage.getPayload()).getFailedMessage(), "Swift message failed message is null.").getPayload());
			if (swiftMessage != null) {
				messages = Collections.singletonList(swiftMessage.getId());
			}
		}
		else if (isStringSwiftMessage(exceptionMessage)) {
			MessagingException messagingException = MessagingException.class.cast(exceptionMessage.getPayload());
			String stringMessage = String.class.cast(AssertUtils.assertNotNull(messagingException.getFailedMessage(), "String swift message failed message is null.").getPayload());
			List<Long> identifiers = getSwiftMessageIdentifierList(stringMessage);
			if (!CollectionUtils.isEmpty(identifiers)) {
				messages = identifiers;
			}
		}
		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();
		swiftMessagingMessage.setMachineName(SystemUtils.getMachineName());
		swiftMessagingMessage.setMessageText(messages == null ? failedMessage : failedMessage + " Failed Swift IDs " + messages);
		swiftMessagingMessage.setUniqueStringIdentifier(uuid.toString());
		getSwiftMessagingErrorService().postErrorMessage(swiftMessagingMessage, ex);
	}


	/**
	 * Only output to the JMS error channel when something completely unexpected occurs, every attempt should be made to update the status on the exception's Swift Message.
	 */
	private void outputMessageToErrorChannel(String text, Exception ex, UUID uuid) {
		SwiftMessagingMessage swiftMessagingMessage = new SwiftMessagingMessage();
		swiftMessagingMessage.setMachineName(SystemUtils.getMachineName());
		swiftMessagingMessage.setMessageText(text);
		swiftMessagingMessage.setUniqueStringIdentifier(uuid.toString());
		getSwiftMessagingErrorService().postErrorMessage(swiftMessagingMessage, ex);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftIdentifierHandler<M> getSwiftIdentifierHandler() {
		return this.swiftIdentifierHandler;
	}


	public void setSwiftIdentifierHandler(SwiftIdentifierHandler<M> swiftIdentifierHandler) {
		this.swiftIdentifierHandler = swiftIdentifierHandler;
	}


	public SwiftMessageConverterHandler<M> getSwiftMessageConverterHandler() {
		return this.swiftMessageConverterHandler;
	}


	public void setSwiftMessageConverterHandler(SwiftMessageConverterHandler<M> swiftMessageConverterHandler) {
		this.swiftMessageConverterHandler = swiftMessageConverterHandler;
	}


	public SwiftMessageService getSwiftServerMessageService() {
		return this.swiftServerMessageService;
	}


	public void setSwiftServerMessageService(SwiftMessageService swiftServerMessageService) {
		this.swiftServerMessageService = swiftServerMessageService;
	}


	public SwiftMessagingErrorService getSwiftMessagingErrorService() {
		return this.swiftMessagingErrorService;
	}


	public void setSwiftMessagingErrorService(SwiftMessagingErrorService swiftMessagingErrorService) {
		this.swiftMessagingErrorService = swiftMessagingErrorService;
	}


	public SwiftServerStatusHandler getSwiftServerStatusHandler() {
		return this.swiftServerStatusHandler;
	}


	public void setSwiftServerStatusHandler(SwiftServerStatusHandler swiftServerStatusHandler) {
		this.swiftServerStatusHandler = swiftServerStatusHandler;
	}
}
