package com.clifton.swift.error.handler;

import org.springframework.messaging.Message;


public interface SwiftExceptionHandler {

	public void handleExceptionMessage(Message<Exception> exceptionMessage);
}
