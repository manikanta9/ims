package com.clifton.swift;


import org.boris.winrun4j.AbstractService;
import org.boris.winrun4j.ServiceException;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * The <code>FixServiceRunner</code> is a WinRun4j extended java service launcher for windows.  WinRun4J can hook into this service
 * implementation to start, stop, and generally control the flow of the service.
 *
 * @author apopp
 */
public class SwiftServiceRunner extends AbstractService {

	@Override
	public int serviceMain(@SuppressWarnings("unused") String[] arg0) throws ServiceException {
		@SuppressWarnings({"unused", "resource"})
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:META-INF/spring/service-clifton-swift-context.xml");
		ctx.registerShutdownHook(); // Register spring hook to shutdown the context upon java kill (SIGTERM when kill PID)

		while (!isShutdown()) {
			try {
				Thread.sleep(6000);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				this.shutdown = true;
			}
		}
		ctx.close();
		return 0;
	}
}
