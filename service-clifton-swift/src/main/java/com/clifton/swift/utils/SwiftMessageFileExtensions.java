package com.clifton.swift.utils;

import com.clifton.core.util.StringUtils;
import com.clifton.swift.message.SwiftMessageFormats;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Pattern;


/**
 * <code>SwiftMessageFileExtensions</code> extensions used by the swift messaging service.
 * <p>
 * Original sent file name:   29.20170718_160441.fin
 * Received error file name:  29.20170718_160441.20170718_160722.fin.err
 */
public enum SwiftMessageFileExtensions {

	MT(
			SwiftMessageFormats.MT,
			"fin",
			"err",
			"lau",
			Pattern.compile("^(?<count>\\d+)\\.(?<date>\\d{8})_(?<time>\\d{6})(?<extension>\\.fin|\\.FIN)(?<error>\\.err|\\.ERR)?$"),
			Pattern.compile("^(?<original>\\d+\\.\\d{8}_\\d{6})\\.(?<date>\\d{8})_(?<time>\\d{6})(?<extension>\\.fin|\\.FIN)(?<error>\\.err|\\.ERR)?$"),
			Pattern.compile("^(?<original>\\d+\\.\\d{8}_\\d{6})\\.(?<date>\\d{8})_(?<time>\\d{6})(?<extension>\\.fin|\\.FIN)(?<lau>\\.lau|\\.LAU)?$")
	);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// immutable and thread-safe date formatter for generating the date and timestamp portion of the outgoing file.
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss");

	private SwiftMessageFormats format;
	private String regularExtension;
	private String errorExtension;
	private String lauExtension;
	private Pattern filenamePattern;
	private Pattern errorFilenamePattern;
	private Pattern lauFilenamePattern;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	SwiftMessageFileExtensions(SwiftMessageFormats format, String regularExtension, String errorExtension, String lauExtension, Pattern filenamePattern, Pattern errorFilenamePattern, Pattern lauFilenamePattern) {
		this.format = format;
		this.regularExtension = regularExtension;
		this.errorExtension = errorExtension;
		this.lauExtension = lauExtension;
		this.filenamePattern = filenamePattern;
		this.errorFilenamePattern = errorFilenamePattern;
		this.lauFilenamePattern = lauFilenamePattern;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String generateFilename(String sequence, LocalDateTime created) {
		String filename = sequence;
		filename += ".";
		filename += formatter.format(created);
		filename += ".";
		filename += this.getRegularExtension();

		return filename;
	}


	public boolean isValidFilename(String filename) {
		if (StringUtils.isEmpty(filename)) {
			return false;
		}
		return this.getFilenamePattern().matcher(filename).matches();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftMessageFormats getFormat() {
		return this.format;
	}


	public String getRegularExtension() {
		return this.regularExtension;
	}


	public String getErrorExtension() {
		return this.errorExtension;
	}


	public String getLauExtension() {
		return this.lauExtension;
	}


	public Pattern getFilenamePattern() {
		return this.filenamePattern;
	}


	public Pattern getErrorFilenamePattern() {
		return this.errorFilenamePattern;
	}


	public Pattern getLauFilenamePattern() {
		return this.lauFilenamePattern;
	}
}
