package com.clifton.swift.utils;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.regex.Pattern;


/**
 * <code>SwiftMtAuthenticationUtils</code> generates the 256-bit (32 byte) binary signature value (Base 64 encoded)
 * for the provided FIN message.
 * <p>
 * https://www2.swift.com/uhbonline/books/public/en_uk/al2_ac_1_2_1_ug/con_110948.htm#prct110948__section_twx_p24_mx
 */
public class SwiftMtAuthenticationUtils {

	private static final int LAU_KEY_MAX_CHARS = 32;
	private static final int LAU_KEY_MIN_CHARS = 17;

	private static final String HMAC_SHA256 = "HmacSHA256";

	private static final Pattern VALID_LAU_KEY = Pattern.compile("^(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])(?!.*[^a-zA-Z0-9\\p{Punct}])(.{17,32})$");


	private SwiftMtAuthenticationUtils() {
		// utility class only.
	}


	public static byte[] encode(String key, String data) {
		AssertUtils.assertNotNull(key, "The encryption key cannot be null.");
		AssertUtils.assertFalse(key.length() > LAU_KEY_MAX_CHARS || key.length() < LAU_KEY_MIN_CHARS, "LAU key must be between %d and %d characters in length", LAU_KEY_MIN_CHARS, LAU_KEY_MAX_CHARS);

		try {
			final byte[] keyBytes = new byte[LAU_KEY_MAX_CHARS];
			Arrays.fill(keyBytes, (byte) 0);
			System.arraycopy(key.getBytes(StandardCharsets.UTF_8), 0, keyBytes, 0, key.getBytes(StandardCharsets.UTF_8).length);
			SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, HMAC_SHA256);
			final Mac mac = Mac.getInstance(HMAC_SHA256);
			mac.init(secretKeySpec);
			return Base64.getEncoder().encode(mac.doFinal(data.getBytes(StandardCharsets.UTF_8)));
		}
		catch (Exception e) {
			throw new RuntimeException("Could not generate signature for input string.", e);
		}
	}


	public static boolean isValidKey(String key) {
		return VALID_LAU_KEY.matcher(key).matches();
	}


	public static String trimAll(String key) {
		if (!StringUtils.isEmpty(key)) {
			return key.replaceAll("\\s", "");
		}
		return key;
	}


	public static String readLauKey(String keyPath) {
		String key = FileUtils.readFileToString(keyPath);
		key = SwiftMtAuthenticationUtils.trimAll(key);
		ValidationUtils.assertTrue(SwiftMtAuthenticationUtils.isValidKey(key), "The lau key [" + key + "] is invalid, it must be 17-32 characters long, must contain at least " +
				"one capitol letter, one lowercase letter, one digit and any of the following special characters [~!@#$%^&*()_+`-={}|[]\\:\";'<>?,./]");
		return key;
	}
}
