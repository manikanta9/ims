package com.clifton.swift.utils;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import org.springframework.messaging.Message;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * {@inheritDoc}
 */

public class SwiftMessageFileUtils {

	public static final String FILE_NAME_IS_REQUIRED = "File name is required.";


	private SwiftMessageFileUtils() {
		// utility class only
	}


	public static SwiftMessageFormats getMessageFormatFromExtension(String fileName) {
		SwiftMessageFormats format = SwiftMessageFileUtils.getMessageFormat(fileName);
		ValidationUtils.assertNotNull(format, "Could not determine Swift message format from file extension " + fileName);
		return format;
	}


	public static boolean isErrorMessage(String fileName) {
		ValidationUtils.assertNotEmpty(fileName, FILE_NAME_IS_REQUIRED);
		return SwiftMessageFileUtils.isErrorExtension(FileUtils.getFileExtension(fileName));
	}


	public static boolean isLauMessage(String fileName) {
		ValidationUtils.assertNotEmpty(fileName, FILE_NAME_IS_REQUIRED);
		return SwiftMessageFileUtils.isLauExtension(FileUtils.getFileExtension(fileName));
	}


	public static boolean isRegularMessage(String fileName) {
		ValidationUtils.assertNotEmpty(fileName, FILE_NAME_IS_REQUIRED);
		return SwiftMessageFileUtils.isRegularExtension(FileUtils.getFileExtension(fileName));
	}


	public static String getMessageFileContents(Message<File> message) {
		ValidationUtils.assertNotNull(message, "Incoming Swift message cannot be null");
		ValidationUtils.assertNotNull(message.getPayload(), "Incoming Swift message payload file cannot be null");
		ValidationUtils.assertTrue(message.getPayload().exists(), "The Swift Message file does not exist.");

		return FileUtils.readFileToString(message.getPayload());
	}


	public static int getSwiftMessageTextByteSize(SwiftMessage swiftMessage) {
		return Stream.of(swiftMessage.getText().getBytes(StandardCharsets.UTF_8))
				.mapToInt(a -> a.length)
				.sum();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SwiftMessageFileExtensions findExtension(String extension) {
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getRegularExtension().equalsIgnoreCase(extension)) {
				return swiftMessageFileExtensions;
			}
		}
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getErrorExtension().equalsIgnoreCase(extension)) {
				return swiftMessageFileExtensions;
			}
		}
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getLauExtension().equalsIgnoreCase(extension)) {
				return swiftMessageFileExtensions;
			}
		}

		return null;
	}


	public static SwiftMessageFileExtensions findExtensionForFormat(SwiftMessageFormats format) {
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getFormat() == format) {
				return swiftMessageFileExtensions;
			}
		}

		return null;
	}


	public static boolean isErrorExtension(String extension) {
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getErrorExtension().equalsIgnoreCase(extension)) {
				return true;
			}
		}

		return false;
	}


	public static boolean isLauExtension(String extension) {
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getLauExtension().equalsIgnoreCase(extension)) {
				return true;
			}
		}

		return false;
	}


	public static boolean isRegularExtension(String extension) {
		for (SwiftMessageFileExtensions swiftMessageFileExtensions : SwiftMessageFileExtensions.values()) {
			if (swiftMessageFileExtensions.getRegularExtension().equalsIgnoreCase(extension)) {
				return true;
			}
		}

		return false;
	}


	public static SwiftMessageFormats getMessageFormat(String filename) {
		String extension = FileUtils.getFileExtension(filename);
		SwiftMessageFileExtensions target = SwiftMessageFileUtils.findExtension(extension);
		return (target == null) ? null : target.getFormat();
	}


	public static String getMessageCount(String filename) {
		List<String> parts = CollectionUtils.createList(getFileNameParts(filename));
		if (parts.size() > 1) {
			return parts.get(0);
		}
		return null;
	}


	public static File findRegularFromErrorFile(File errorFile) {
		if (errorFile != null) {
			String errorFileName = errorFile.getName();
			String extension = FileUtils.getFileExtension(errorFileName);
			SwiftMessageFileExtensions target = SwiftMessageFileUtils.findExtension(extension);
			if (target != null) {
				try {
					List<String> fileParts = CollectionUtils.createList(getErrorFileNameParts(errorFileName));
					if (!fileParts.isEmpty()) {
						String pattern = "^(" + fileParts.get(0) + ")" + "\\.\\d{8}_\\d{6}\\.fin|FIN$";
						try (Stream<Path> pathStream = Files.find(errorFile.getParentFile().toPath(), 1, (path, attributes)
								-> attributes.isRegularFile() && path.toFile().getName().matches(pattern))) {
							List<File> fileList = pathStream.map(Path::toFile)
									.collect(Collectors.toList());
							if (fileList.size() != 1) {
								throw new RuntimeException("Too many matching *.fin files for *.fin.err " + errorFileName);
							}
							return fileList.get(0);
						}
					}
					else {
						throw new RuntimeException("Pattern matching for error file failed " + errorFileName);
					}
				}
				catch (IOException e) {
					throw new RuntimeException("Could not fetch the regular *.fin file for the *.fin.err file " + errorFileName, e);
				}
			}
			else {
				throw new RuntimeException("Could not determine file extension for " + errorFileName);
			}
		}
		return null;
	}


	public static String[] getErrorFileNameParts(String filename) {
		String extension = FileUtils.getFileExtension(filename);
		SwiftMessageFileExtensions swiftMessageFileExtension = SwiftMessageFileUtils.findExtension(extension);
		if (swiftMessageFileExtension != null) {
			Matcher matcher = swiftMessageFileExtension.getErrorFilenamePattern().matcher(filename);
			if (matcher.matches()) {
				String[] parts = new String[matcher.groupCount() + 1];
				for (int i = 0; i < parts.length; i++) {
					parts[i] = (matcher.group(i) == null) ? "null" : matcher.group(i);
				}
				return Arrays.copyOfRange(parts, 1, parts.length);
			}
		}
		return null;
	}


	public static String[] getFileNameParts(String filename) {
		String extension = FileUtils.getFileExtension(filename);
		SwiftMessageFileExtensions swiftMessageFileExtension = SwiftMessageFileUtils.findExtension(extension);
		if (swiftMessageFileExtension != null) {
			Matcher matcher = swiftMessageFileExtension.getFilenamePattern().matcher(filename);
		if (matcher.matches()) {
			String[] parts = new String[matcher.groupCount() + 1];
			for (int i = 0; i < parts.length; i++) {
				parts[i] = (matcher.group(i) == null) ? "null" : matcher.group(i);
			}
			return Arrays.copyOfRange(parts, 1, parts.length);
		}
		}
		return null;
	}
}
