package com.clifton.swift;


import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class SwiftServerRunner {

	@SuppressWarnings({"unused", "resource"})
	public static void main(String[] args) {
		ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("classpath:META-INF/spring/service-clifton-swift-context.xml");
		context.registerShutdownHook(); // Register spring hook to shutdown the context upon java kill (SIGTERM when kill PID)
	}
}
