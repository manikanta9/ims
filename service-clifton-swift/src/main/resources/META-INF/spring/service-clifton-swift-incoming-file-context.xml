<?xml version="1.0" encoding="UTF-8"?>
<beans default-lazy-init="true" default-autowire="byName"
       xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:int="http://www.springframework.org/schema/integration"
       xmlns:file="http://www.springframework.org/schema/integration/file"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/integration
		http://www.springframework.org/schema/integration/spring-integration.xsd
		http://www.springframework.org/schema/integration/file
		http://www.springframework.org/schema/integration/file/spring-integration-file.xsd">

	<!--
	*******************************************************************************************************************************************************************************
	Common components
	-->

	<!--
	The channel adapter poller, override for test class milliseconds
	-->
	<int:poller id="swiftFileChannelPoller" fixed-delay="5000" />

	<!--
	Directory Scanner, only returns a list of *.ready files; previously renamed or renamed during this polling period.
	Custom Scanners to not allow filters, remove the defaults.
	-->
	<bean id="swiftIncomingFileScanner" class="com.clifton.swift.incoming.file.handler.SwiftIncomingFileScanner">
		<property name="filter">
			<null />
		</property>
	</bean>

	<!--
	File name generator, removes the ready extension when placed in the output folder.
	-->
	<bean id="swiftRemoveReadyFileExtensionNamer" class="com.clifton.swift.incoming.file.handler.SwiftRemoveReadyFileExtensionNamer" />

	<!--
	Swift message authenticator
	-->
	<bean id="swiftMessageAuthenticator" class="com.clifton.swift.incoming.file.handler.lau.SwiftMessageAuthenticator" init-method="initialize">
		<property name="localAuthenticationEnabled" value="${swift.lau.enabled}" />
		<property name="keyPath" value="${swift.lau.key.path}" />
	</bean>

	<!--
	Determine base file name without .lau, .err, .lau.ready, .err.ready
	-->
	<bean id="swiftMessageBaseFileNameGenerator" class="com.clifton.swift.incoming.file.handler.lau.SwiftMessageBaseFileNameGenerator" />


	<!--
	*******************************************************************************************************************************************************************************
	Reception:  channels definitions
	-->
	<int:channel id="swiftReceptionChannel" />

	<int:channel id="swiftReceptionLauChannel" />
	<int:channel id="swiftReceptionFinChannel" />

	<int:channel id="swiftProcessedReceptionChannel" />
	<int:channel id="swiftReceptionDiscardChannel" />
	<int:channel id="swiftProcessedReceptionDiscardChannel" />

	<!--
	Reception Folder:  Monitor the reception folder and create messages upon the arrival of files.
	-->
	<bean id="swiftReceptionFileReadingMessageSource" class="com.clifton.swift.incoming.file.handler.SwiftFileReadingMessageSource">
		<property name="directory" value="file:${swift.file.reception.path}" />
		<property name="scanner" ref="swiftIncomingFileScanner" />
	</bean>

	<int:inbound-channel-adapter
			id="swiftReceptionChannelAdapter"
			channel="swiftReceptionChannel"
			expression="@swiftReceptionFileReadingMessageSource.receive()">
		<int:poller ref="swiftFileChannelPoller" />
	</int:inbound-channel-adapter>


	<!--
	Reception:  route message based on authentication
	-->
	<bean id="swiftReceptionDynamicMessageRouter" class="com.clifton.swift.incoming.file.handler.lau.SwiftLauDynamicMessageRouter" init-method="initialize">
		<property name="localAuthenticationEnabled" value="${swift.lau.enabled}" />
		<property name="authenticateChannel" ref="swiftReceptionLauChannel" />
		<property name="nonAuthenticatedChannel" ref="swiftReceptionFinChannel" />
	</bean>

	<int:router id="swiftReceptionMessageRouter" input-channel="swiftReceptionChannel" ref="swiftReceptionDynamicMessageRouter" />

	<!--
	Reception: aggregator for .fin and .lau pairs.
	-->
	<bean id="swiftReceptionFolderShortTermFilter" class="com.clifton.swift.incoming.file.handler.lau.SwiftMessageShortTermFilter">
		<property name="timeout" value="#{ T(java.time.Duration).ofMinutes(5)}" />
	</bean>

	<int:chain id="swiftReceptionChannelChain" input-channel="swiftReceptionLauChannel" output-channel="swiftReceptionFinChannel">
		<int:filter ref="swiftReceptionFolderShortTermFilter" method="accept" />
		<int:header-enricher>
			<int:header name="correlationId" ref="swiftMessageBaseFileNameGenerator" method="generate" />
		</int:header-enricher>
		<int:aggregator
				id="swiftReceptionFileAggregator"
				discard-channel="swiftReceptionDiscardChannel"
				release-strategy-expression="size() == 2"
				expire-groups-upon-completion="true"
				group-timeout="#{ T(java.time.Duration).ofMinutes(5).toMillis()}"
				expire-groups-upon-timeout="true"
		/>
		<int:header-enricher>
			<int:header name="authenticationHash" ref="swiftMessageAuthenticator" method="getAuthenticationHeader" />
		</int:header-enricher>
		<int:splitter />
	</int:chain>

	<!--
	Reception (Discard) Channel.
	-->
	<bean id="swiftMessageLauDiscardHandler" class="com.clifton.swift.incoming.file.handler.lau.SwiftMessageLauDiscardHandler" />

	<int:service-activator id="swiftReceptionDiscardLauService"
	                       input-channel="swiftProcessedReceptionDiscardChannel"
	                       ref="swiftMessageLauDiscardHandler"
	                       method="handleDiscardFile" />
	<!--
	Processed (Reception):  handle files moved from reception to processed.
	-->
	<bean id="swiftProcessedReceptionFileHandler" class="com.clifton.swift.incoming.file.handler.SwiftProcessedReceptionFileHandler" />

	<int:service-activator id="swiftProcessedReceptionService" input-channel="swiftProcessedReceptionChannel" ref="swiftProcessedReceptionFileHandler" method="handleFile" />

	<!--
	Reception -> processed:  Move a file to the processed reception folder
	-->
	<file:outbound-gateway
			id="swiftReceptionToProcessedGateway"
			request-channel="swiftReceptionFinChannel"
			reply-channel="swiftProcessedReceptionChannel"
			directory="file:${swift.file.processed.reception.path}"
			filename-generator="swiftRemoveReadyFileExtensionNamer"
			delete-source-files="true">
	</file:outbound-gateway>

	<!--
	Reception (Discard) -> processed:  Move a file to the processed reception folder
	-->
	<file:outbound-gateway
			id="swiftReceptionDiscardToProcessedGateway"
			request-channel="swiftReceptionDiscardChannel"
			reply-channel="swiftProcessedReceptionDiscardChannel"
			directory="file:${swift.file.processed.reception.path}"
			filename-generator="swiftRemoveReadyFileExtensionNamer"
			delete-source-files="true" />


	<!--
	*******************************************************************************************************************************************************************************
	Error:  channels definitions
	-->
	<int:channel id="swiftErrorChannel" />

	<int:channel id="swiftErrorFinChannel" />
	<int:channel id="swiftProcessedErrorChannel" />

	<int:channel id="swiftErrorDiscardChannel" />
	<int:channel id="swiftProcessedErrorDiscardChannel" />

	<!--
	Error Folder:  Monitor the error folder and create messages upon the arrival of files.
	-->
	<bean id="swiftErrorFileReadingMessageSource" class="com.clifton.swift.incoming.file.handler.SwiftFileReadingMessageSource">
		<property name="directory" value="file:${swift.file.error.path}" />
		<property name="scanner" ref="swiftIncomingFileScanner" />
	</bean>

	<int:inbound-channel-adapter
			id="swiftErrorChannelAdapter"
			channel="swiftErrorChannel"
			expression="@swiftErrorFileReadingMessageSource.receive()">
		<int:poller ref="swiftFileChannelPoller" />
	</int:inbound-channel-adapter>

	<!--
	Error: aggregator for .fin, .fin.err and .lau.
	-->
	<bean id="swiftErrorFolderShortTermFilter" class="com.clifton.swift.incoming.file.handler.lau.SwiftMessageShortTermFilter">
		<property name="timeout" value="#{ T(java.time.Duration).ofMinutes(5)}" />
	</bean>

	<bean id="swiftAggregationErrorReleaseStrategy" class="com.clifton.swift.incoming.file.handler.error.SwiftAggregationReleaseStrategy">
		<property name="groupSize" value="2" />
	</bean>

	<bean id="swiftErrorMessageFileHeaderAugmenter" class="com.clifton.swift.incoming.file.handler.error.SwiftMessageFileHeaderAugmenter" />

	<int:chain id="swiftErrorChannelChain" input-channel="swiftErrorChannel" output-channel="swiftErrorFinChannel">
		<int:filter ref="swiftErrorFolderShortTermFilter" method="accept" />
		<int:header-enricher>
			<int:header name="correlationId" expression="@swiftErrorMessageFileHeaderAugmenter.getMessageFileCorrelationId(payload)" overwrite="true" />
			<int:header name="sequenceNumber" expression="@swiftErrorMessageFileHeaderAugmenter.getSequenceNumber(payload)" overwrite="true" />
			<int:header name="sequenceSize" expression="2" overwrite="true" />
		</int:header-enricher>
		<int:resequencer
				id="swiftErrorFileAggregator"
				discard-channel="swiftErrorDiscardChannel"
				release-strategy="swiftAggregationErrorReleaseStrategy"
				group-timeout="#{ T(java.time.Duration).ofMinutes(5).toMillis()}"
				expire-groups-upon-timeout="true"
		/>
		<int:splitter />
	</int:chain>

	<!--
	Error -> processed:  Move a file to the processed error folder
	-->
	<file:outbound-gateway
			id="swiftErrorToProcessedGateway"
			request-channel="swiftErrorFinChannel"
			reply-channel="swiftProcessedErrorChannel"
			directory="file:${swift.file.processed.error.path}"
			filename-generator="swiftRemoveReadyFileExtensionNamer"
			delete-source-files="true" />

	<!--
	Error (Discard) -> processed:  Move a file to the processed error folder
	-->
	<file:outbound-gateway
			id="swiftErrorDiscardToProcessedGateway"
			request-channel="swiftErrorDiscardChannel"
			reply-channel="swiftProcessedErrorDiscardChannel"
			directory="file:${swift.file.processed.error.path}"
			filename-generator="swiftRemoveReadyFileExtensionNamer"
			delete-source-files="true" />

	<!--
	Processed (Error):  handle error and move to error folder
	-->
	<bean id="swiftProcessedErrorFileHandler" class="com.clifton.swift.incoming.file.handler.error.SwiftProcessedErrorFileHandler" />

	<int:service-activator id="swiftProcessedErrorService" input-channel="swiftProcessedErrorChannel" ref="swiftProcessedErrorFileHandler" method="handleFile" />

	<!--
	Processed Error (Discard): handle timed out error message and move to processed error folder
	-->
	<int:service-activator id="swiftProcessErrorDiscardService" input-channel="swiftProcessedErrorDiscardChannel" ref="swiftProcessedErrorFileHandler" method="handleDiscardFile" />

	<!--
	*******************************************************************************************************************************************************************************
	ANY: Global Error Channel, the input-channel name must be errorChannel.  This is only called when an
	exception is thrown during asynchronous messaging, when the sender and handler are separate threads, this
	is not a Direct Channel.
	-->
	<bean id="swiftExceptionHandler" class="com.clifton.swift.error.handler.SwiftExceptionHandlerImpl" />

	<int:service-activator id="swiftGlobalExceptionHandler" input-channel="errorChannel" ref="swiftExceptionHandler" method="handleExceptionMessage" />

</beans>
