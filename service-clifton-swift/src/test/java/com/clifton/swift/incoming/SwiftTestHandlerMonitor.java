package com.clifton.swift.incoming;

import com.clifton.swift.error.handler.SwiftExceptionHandler;
import com.clifton.swift.incoming.file.handler.SwiftDiscardFileHandler;
import com.clifton.swift.incoming.file.handler.SwiftFileHandler;
import org.springframework.messaging.Message;

import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


public class SwiftTestHandlerMonitor<M> implements SwiftFileHandler, SwiftDiscardFileHandler, SwiftExceptionHandler {

	private SwiftFileHandler swiftFileHandler;
	private SwiftDiscardFileHandler swiftDiscardFileHandler;
	private SwiftExceptionHandler swiftExceptionHandler;

	private BlockingQueue<File> messageQueue = new LinkedBlockingQueue<>();

	private boolean throwException = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message<File> handleFile(Message<File> fileMessage) {
		try {
			getSwiftFileHandler().handleFile(fileMessage);
			if (this.throwException) {
				throw new RuntimeException("Exception purposely thrown");
			}
		}
		finally {
			putFileToQueue(fileMessage.getPayload());
		}
		return null;
	}


	@Override
	public void handleDiscardFile(Message<File> fileMessage) {
		try {
			getSwiftDiscardFileHandler().handleDiscardFile(fileMessage);
			if (this.throwException) {
				throw new RuntimeException("Exception purposely thrown");
			}
		}
		finally {
			putFileToQueue(fileMessage.getPayload());
		}
	}


	@Override
	public void handleExceptionMessage(Message<Exception> message) {
		try {
			getSwiftExceptionHandler().handleExceptionMessage(message);
		}
		finally {
			putFileToQueue(new File("exception"));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public File awaitMessage(long seconds) throws InterruptedException {
		return this.messageQueue.poll(seconds, TimeUnit.SECONDS);
	}


	public void reset() {
		this.throwException = false;
		this.messageQueue.clear();
	}


	public boolean isEmptyQueue() {
		return this.messageQueue.isEmpty();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void putFileToQueue(File file) {
		try {
			this.messageQueue.put(file);
		}
		catch (InterruptedException e) {
			throw new RuntimeException("Error putting File to queue.", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SwiftFileHandler getSwiftFileHandler() {
		return this.swiftFileHandler;
	}


	public void setSwiftFileHandler(SwiftFileHandler swiftFileHandler) {
		this.swiftFileHandler = swiftFileHandler;
	}


	public SwiftDiscardFileHandler getSwiftDiscardFileHandler() {
		return this.swiftDiscardFileHandler;
	}


	public void setSwiftDiscardFileHandler(SwiftDiscardFileHandler swiftDiscardFileHandler) {
		this.swiftDiscardFileHandler = swiftDiscardFileHandler;
	}


	public SwiftExceptionHandler getSwiftExceptionHandler() {
		return this.swiftExceptionHandler;
	}


	public void setSwiftExceptionHandler(SwiftExceptionHandler swiftExceptionHandler) {
		this.swiftExceptionHandler = swiftExceptionHandler;
	}


	public void setThrowException(boolean throwException) {
		this.throwException = throwException;
	}
}
