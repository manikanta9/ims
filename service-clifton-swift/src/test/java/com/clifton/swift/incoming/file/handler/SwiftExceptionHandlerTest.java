package com.clifton.swift.incoming.file.handler;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.security.user.SecurityUser;
import com.clifton.swift.error.handler.SwiftExceptionHandlerImpl;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.messaging.error.SwiftMessagingErrorService;
import com.clifton.swift.outgoing.AggregatedSwiftMessage;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Transactional
public class SwiftExceptionHandlerTest<M> {

	private static final String MTM_OUTGOING_MESSAGE = "{1:F01PPSCUS60AXXX0000000000}{2:I210IRVTUS3NXIBKN}{4:\n" +
			":20:IMS522058MTM\n" +
			":25:653084\n" +
			":30:170524\n" +
			":21:MARG\n" +
			":32B:USD26648,56\n" +
			":52A:MSNYUS33\n" +
			":56A:IRVTUS3NXXX\n" +
			"-}";

	@Resource
	private AdvancedUpdatableDAO<SwiftMessageStatus, Criteria> swiftMessageStatusDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessageType, Criteria> swiftMessageTypeDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessage, Criteria> swiftMessageDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftSourceSystem, Criteria> swiftSourceSystemDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftSourceSystemModule, Criteria> swiftSourceSystemModuleDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftIdentifier, Criteria> swiftIdentifierDAO;

	@Resource
	private SwiftExceptionHandlerImpl<M> swiftExceptionFileHandler;

	@Resource
	private SwiftMessageService swiftServerMessageService;

	@Autowired
	private ContextHandler contextHandler;

	@Mock
	private SwiftMessagingErrorService swiftMessagingErrorService;

	@Resource
	private SwiftServerStatusHandler swiftServerStatusHandler;

	private SwiftMessageService spiedSwiftMessageService;

	private Map<Class<? extends IdentityObject>, Map<String, IdentityObject>> testEntities = new HashMap<>();


	@BeforeEach
	public void setup() {
		if (this.testEntities.isEmpty()) {
			MockitoAnnotations.initMocks(this);

			this.spiedSwiftMessageService = Mockito.spy(this.swiftServerMessageService);
			this.swiftExceptionFileHandler.setSwiftServerMessageService(this.spiedSwiftMessageService);
			this.swiftExceptionFileHandler.setSwiftMessagingErrorService(this.swiftMessagingErrorService);

			SecurityUser user = new SecurityUser();
			user.setId(new Integer(7).shortValue());
			user.setUserName("TestUser");
			this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

			SwiftMessageStatus swiftMessageStatus = new SwiftMessageStatus();
			swiftMessageStatus.setName("ERROR");
			swiftMessageStatus.setDescription("An error prevented a messages from being processed successfully.");
			this.swiftMessageStatusDAO.save(swiftMessageStatus);

			this.testEntities.putIfAbsent(SwiftMessageType.class, new HashMap<>());
			this.testEntities.get(SwiftMessageType.class).put("ERROR", swiftMessageStatus);

			swiftMessageStatus = new SwiftMessageStatus();
			swiftMessageStatus.setName("LOGGED");
			swiftMessageStatus.setDescription("The message has been logged.");
			this.swiftMessageStatusDAO.save(swiftMessageStatus);

			this.testEntities.putIfAbsent(SwiftMessageStatus.class, new HashMap<>());
			this.testEntities.get(SwiftMessageStatus.class).put("LOGGED", swiftMessageStatus);

			SwiftMessageType swiftMessageType = new SwiftMessageType();
			swiftMessageType.setName("Notice To Receive Transfer Message");
			swiftMessageType.setDescription("Notice To Receive Transfer Message");
			swiftMessageType.setMtVersionName("MT210");
			this.swiftMessageTypeDAO.save(swiftMessageType);

			this.testEntities.putIfAbsent(SwiftMessageType.class, new HashMap<>());
			this.testEntities.get(SwiftMessageType.class).put("MT210", swiftMessageType);

			SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
			swiftIdentifier.setUniqueStringIdentifier("IMS522058MTM");
			swiftIdentifier.setSourceSystemFkFieldId((long) 522058);

			SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
			swiftSourceSystem.setName("IMS");
			swiftSourceSystem.setBusinessIdentifierCode("PPSCUS60");
			this.swiftSourceSystemDAO.save(swiftSourceSystem);

			this.testEntities.putIfAbsent(SwiftSourceSystem.class, new HashMap<>());
			this.testEntities.get(SwiftSourceSystem.class).put("IMS", swiftSourceSystem);

			swiftIdentifier.setSourceSystem(swiftSourceSystem);

			SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
			swiftSourceSystemModule.setName("M2M");
			swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);
			swiftSourceSystemModule.setTargetMessageFormat(SwiftMessageFormats.MT);
			this.swiftSourceSystemModuleDAO.save(swiftSourceSystemModule);

			this.testEntities.putIfAbsent(SwiftSourceSystemModule.class, new HashMap<>());
			this.testEntities.get(SwiftSourceSystemModule.class).put("M2M", swiftSourceSystemModule);

			swiftIdentifier.setSourceSystemModule(swiftSourceSystemModule);
			this.swiftIdentifierDAO.save(swiftIdentifier);

			this.testEntities.putIfAbsent(SwiftIdentifier.class, new HashMap<>());
			this.testEntities.get(SwiftIdentifier.class).put("IMS522058MTM", swiftIdentifier);

			SwiftMessage swiftMessage = new SwiftMessage();
			swiftMessage.setType(swiftMessageType);
			swiftMessage.setText(MTM_OUTGOING_MESSAGE);
			swiftMessage.setMessageFormat(SwiftMessageFormats.MT);
			swiftMessage.setIdentifier(swiftIdentifier);
			swiftMessage.setIncoming(false);
			swiftMessage.setSenderBusinessIdentifierCode("PPSCUS66");
			swiftMessage.setReceiverBusinessIdentifierCode("IRVTUS3NXIBK");
			swiftMessage.setMessageStatus((SwiftMessageStatus) this.testEntities.get(SwiftMessageStatus.class).get("LOGGED"));
			this.swiftMessageDAO.save(swiftMessage);

			this.testEntities.putIfAbsent(SwiftMessage.class, new HashMap<>());
			this.testEntities.get(SwiftMessage.class).put("IMS522058MTM", swiftMessage);
		}
		else {
			Mockito.reset(this.spiedSwiftMessageService, this.swiftMessagingErrorService, this.swiftServerStatusHandler);

			SwiftMessageStatus logged = (SwiftMessageStatus) this.testEntities.get(SwiftMessageStatus.class).get("LOGGED");
			this.testEntities.get(SwiftMessage.class).values().forEach(s -> {
				SwiftMessage swiftMessage = (SwiftMessage) s;
				((SwiftMessage) s).setMessageStatus(logged);
				this.swiftMessageDAO.save(swiftMessage);
			});
		}
	}


	/**
	 * This is an error somewhere in the Spring Integration Code, usually trying to connect to the destination.
	 * By the time the Spring Integration Code is called the SwiftMessage and Identifier have been saved.
	 */
	@Test
	public void testStringHandleFailedMessage() {
		MessageBuilder<String> builder = MessageBuilder.withPayload(MTM_OUTGOING_MESSAGE);
		Message<String> testMessage = builder.build();

		Throwable nullPointerException = new NullPointerException("Some Null pointer exception");
		Exception exception = new MessagingException(testMessage, "Failed message handling", nullPointerException);
		Message<Exception> errorMessage = MessageBuilder.withPayload(exception).build();

		this.swiftExceptionFileHandler.handleExceptionMessage(errorMessage);

		SwiftMessage swiftMessage = this.swiftMessageDAO.findByPrimaryKey(this.testEntities.get(SwiftMessage.class).get("IMS522058MTM").getIdentity());
		Assertions.assertNotNull(swiftMessage);
		MatcherAssert.assertThat(swiftMessage.getMessageStatus().getName(), IsEqual.equalTo(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.toString()));

		Mockito.verify(this.spiedSwiftMessageService).saveSwiftMessage(swiftMessage);
		Mockito.verifyZeroInteractions(this.swiftMessagingErrorService);
	}


	/**
	 * This is an error somewhere in the Spring Integration Code, usually trying to connect to the destination.
	 * By the time the Spring Integration Code is called the SwiftMessage and Identifier have been saved.
	 */
	@Test
	public void testSwiftMessageHandleFailedMessage() {
		SwiftMessage swiftMessage = (SwiftMessage) this.testEntities.get(SwiftMessage.class).get("IMS522058MTM");
		MessageBuilder<SwiftMessage> builder = MessageBuilder.withPayload(swiftMessage);
		Message<SwiftMessage> testMessage = builder.build();

		Throwable nullPointerException = new NullPointerException("Some Null pointer exception");
		Exception exception = new MessagingException(testMessage, "Failed message handling", nullPointerException);
		Message<Exception> errorMessage = MessageBuilder.withPayload(exception).build();

		this.swiftExceptionFileHandler.handleExceptionMessage(errorMessage);

		SwiftMessage updatedSwiftMessage = this.swiftMessageDAO.findByPrimaryKey(swiftMessage.getIdentity());
		Assertions.assertNotNull(updatedSwiftMessage);
		MatcherAssert.assertThat(updatedSwiftMessage.getMessageStatus().getName(), IsEqual.equalTo(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.toString()));

		Mockito.verify(this.spiedSwiftMessageService).saveSwiftMessage(swiftMessage);
		Mockito.verifyZeroInteractions(this.swiftMessagingErrorService);
		Mockito.verify(this.swiftServerStatusHandler, Mockito.times(1))
				.sendSwiftExportStatus(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.any(), ArgumentMatchers.isNull());
	}


	/**
	 * This is an error somewhere in the Spring Integration Code, usually trying to connect to the destination.
	 * By the time the Spring Integration Code is called the SwiftMessage and Identifier have been saved.
	 */
	@Test
	public void testAggregatedMessageHandleFailedMessage() {
		SwiftMessage swiftMessage = (SwiftMessage) this.testEntities.get(SwiftMessage.class).get("IMS522058MTM");

		AggregatedSwiftMessage aggregatedSwiftMessage = new AggregatedSwiftMessage();
		aggregatedSwiftMessage.setMessageFormat(swiftMessage.getMessageFormat());
		aggregatedSwiftMessage.setText(MTM_OUTGOING_MESSAGE);
		aggregatedSwiftMessage.setAggregatedMessageList(Collections.singletonList(swiftMessage.getId()));

		MessageBuilder<AggregatedSwiftMessage> builder = MessageBuilder.withPayload(aggregatedSwiftMessage);
		Message<AggregatedSwiftMessage> testMessage = builder.build();

		Throwable nullPointerException = new NullPointerException("Some Null pointer exception");
		Exception exception = new MessagingException(testMessage, "Failed message handling", nullPointerException);
		Message<Exception> errorMessage = MessageBuilder.withPayload(exception).build();

		this.swiftExceptionFileHandler.handleExceptionMessage(errorMessage);

		SwiftMessage updatedSwiftMessage = this.swiftMessageDAO.findByPrimaryKey(swiftMessage.getIdentity());
		Assertions.assertNotNull(updatedSwiftMessage);
		MatcherAssert.assertThat(updatedSwiftMessage.getMessageStatus().getName(), IsEqual.equalTo(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.toString()));

		Mockito.verify(this.spiedSwiftMessageService).saveSwiftMessage(swiftMessage);
		Mockito.verifyZeroInteractions(this.swiftMessagingErrorService);
	}
}
