package com.clifton.swift.incoming.file.handler;

import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.core.messaging.jms.JmsUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.beans.Lazy;
import com.thoughtworks.xstream.XStream;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


public class SwiftIncomingMessageListener implements MessageListener {

	private JmsTemplate jmsTemplate;

	private final Lazy<XStream> xStreamLazy = new Lazy<>(this::generateXStream);
	private BlockingQueue<AsynchronousMessage> messageQueue = new LinkedBlockingQueue<>();
	private List<MessagingXStreamConfigurer> xmlXStreamConfigurerList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AsynchronousMessage awaitMessage(long seconds) throws InterruptedException {
		return this.messageQueue.poll(seconds, TimeUnit.SECONDS);
	}


	@Override
	public void onMessage(Message message) {
		try {
			AsynchronousMessage asynchronousMessage = (AsynchronousMessage) JmsUtils.deserializeMessage(message, getXStream(), getJmsTemplate().getMessageConverter());
			this.messageQueue.put(asynchronousMessage);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public XStream getXStream() {
		return getXStreamLazy().get();
	}


	private XStream generateXStream() {
		XStream stream = new XStream();
		for (MessagingXStreamConfigurer config : CollectionUtils.getIterable(getXmlXStreamConfigurerList())) {
			config.configure(stream);
		}
		return stream;
	}


	// Private getter
	private Lazy<XStream> getXStreamLazy() {
		return this.xStreamLazy;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public BlockingQueue<AsynchronousMessage> getMessageQueue() {
		return this.messageQueue;
	}


	public void setMessageQueue(BlockingQueue<AsynchronousMessage> messageQueue) {
		this.messageQueue = messageQueue;
	}


	public List<MessagingXStreamConfigurer> getXmlXStreamConfigurerList() {
		return this.xmlXStreamConfigurerList;
	}


	public void setXmlXStreamConfigurerList(List<MessagingXStreamConfigurer> xmlXStreamConfigurerList) {
		this.xmlXStreamConfigurerList = xmlXStreamConfigurerList;
	}
}
