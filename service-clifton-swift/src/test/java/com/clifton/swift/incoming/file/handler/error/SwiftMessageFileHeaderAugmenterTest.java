package com.clifton.swift.incoming.file.handler.error;

import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.hamcrest.core.IsNot;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Arrays;


public class SwiftMessageFileHeaderAugmenterTest {

	private static final String[][] testCases = {
			{"1.20171117_100443.20171117_100718.fin.ready", "1.20171117_100443.20171117_100720.fin.err.ready", "true"},
			{"2.20170809_093243.20170809_093553.fin.ready", "2.20170809_093243.20170809_093553.fin.err.ready", "true"},
			{"3.20161212_125247.20161212_135228.fin.ready", "3.20161212_155247.20161212_135228.fin.err.ready", "false"},
			{"3.20161212_125247.20161212_135228.fin.ready", "3.20161213_125247.20161212_135228.fin.err.ready", "false"},
			{"3.20161212_125247.20161212_135229_135444.fin.ready", "3.20161212_125247.20161212_135228_135555.fin.err.ready", "true"}
	};


	@Test
	public void getMessageFileCorrelationIdTest() {
		SwiftMessageFileHeaderAugmenter swiftMessageFileHeaderAugmenter = new SwiftMessageFileHeaderAugmenter();
		Arrays.stream(testCases).forEach(a -> {
			File finFile = new File(a[0]);
			File errFile = new File(a[1]);
			String finCorrelationId = swiftMessageFileHeaderAugmenter.getMessageFileCorrelationId(finFile);
			String errCorrelationId = swiftMessageFileHeaderAugmenter.getMessageFileCorrelationId(errFile);
			Assertions.assertNotNull(finCorrelationId);
			if (Boolean.parseBoolean(a[2])) {
				MatcherAssert.assertThat(finCorrelationId, IsEqual.equalTo(errCorrelationId));
			}
			else {
				MatcherAssert.assertThat(finCorrelationId, IsNot.not(IsEqual.equalTo(errCorrelationId)));
			}
			MatcherAssert.assertThat(1, IsEqual.equalTo(swiftMessageFileHeaderAugmenter.getSequenceNumber(finFile)));
			MatcherAssert.assertThat(2, IsEqual.equalTo(swiftMessageFileHeaderAugmenter.getSequenceNumber(errFile)));
		});
	}
}
