package com.clifton.swift.incoming.file.handler;

import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.instruction.messaging.message.InstructionMessagingStatusMessage;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.identifier.SwiftIdentifierHandlerImpl;
import com.clifton.swift.identifier.SwiftIdentifierService;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.message.log.SwiftMessageLog;
import com.clifton.swift.message.log.SwiftMessageLogHandlerImpl;
import com.clifton.swift.message.log.SwiftMessageLogService;
import com.clifton.swift.prowide.server.router.ProwideSwiftServerMessageRouterImpl;
import com.clifton.swift.server.router.SwiftServerMessageRouter;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.system.SwiftSourceSystemService;
import org.apache.commons.io.FileUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftProcessedReceptionFileHandlerTest {

	public static final String SWIFT_MESSAGE_202 = "{1:F21PPSCUS60AXXX0009000001}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000001}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMSCOL4598301\n" +
			":21:MARG\n" +
			":32A:161110USD900222,64\n" +
			":53B:/SUR55\n" +
			":57A:CNORSGSG\n" +
			":58A:/15700043\n" +
			"GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:BDCB376335F5}{TNG:}}";

	public static final String SWIFT_MESSAGE_210 = "{1:F21PPSCUS60AXXX0009000002}{4:{177:1611170742}{451:0}}{1:F01PPSCUS60AXXX0009000002}{2:I210CNORUS40XCOLN}{4:\n" +
			":20:IMS4594491MTM\n" +
			":25:ATV02\n" +
			":30:161109\n" +
			":21:MARG\n" +
			":32B:USD166737,5\n" +
			":52A:PPSCUS60\n" +
			":56A:GOLDUS33\n" +
			"-}{5:{MAC:00000000}{CHK:73B9CDA2F754}{TNG:}}";

	public static final String SWIFT_MULTIPLE = SWIFT_MESSAGE_202 + "$" + SWIFT_MESSAGE_210;

	private static final String FIN_FILE_NAME = "123456.20161123_082528.fin";

	@TempDir
	public File folder;

	@Resource
	private SwiftIncomingMessageListener swiftIncomingMessageListener;

	@Resource
	private SwiftProcessedReceptionFileHandler<?> swiftProcessedReceptionFileHandler;

	@Mock
	private SwiftMessageService swiftMessageService;

	@Mock
	private SwiftIdentifierService swiftIdentifierService;

	@Mock
	private SwiftSourceSystemService swiftSourceSystemService;

	@Mock
	private SwiftMessageLogService swiftMessageLogService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings({"unchecked", "rawtypes"})
	@BeforeEach
	public void mockServices() {
		MockitoAnnotations.initMocks(this);
		Mockito.reset(this.swiftMessageService, this.swiftIdentifierService, this.swiftSourceSystemService, this.swiftMessageLogService);

		SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
		swiftSourceSystem.setId((short) 1);
		swiftSourceSystem.setName("IMS");

		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		swiftSourceSystemModule.setId((short) 1);
		swiftSourceSystemModule.setName("M2M");
		swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);
		swiftSourceSystemModule.setTargetMessageFormat(SwiftMessageFormats.MT);

		SwiftIdentifier swiftIdentifier202 = new SwiftIdentifier();
		swiftIdentifier202.setId(1L);
		swiftIdentifier202.setUniqueStringIdentifier("IMSCOL4598301");
		swiftIdentifier202.setSourceSystemFkFieldId((long) 1);
		swiftIdentifier202.setSourceSystemModule(swiftSourceSystemModule);
		swiftIdentifier202.setSourceSystemFkFieldId(swiftSourceSystem.getId().longValue());

		SwiftMessage swiftMessage202 = new SwiftMessage();
		swiftMessage202.setText(SWIFT_MESSAGE_202);
		swiftMessage202.setIdentifier(swiftIdentifier202);

		SwiftMessageType swiftMessageType202 = new SwiftMessageType();
		swiftMessageType202.setMtVersionName("MT202");

		SwiftIdentifier swiftIdentifier210 = new SwiftIdentifier();
		swiftIdentifier210.setId(2L);
		swiftIdentifier210.setUniqueStringIdentifier("IMS4594491MTM");
		swiftIdentifier210.setSourceSystemFkFieldId((long) 1);
		swiftIdentifier210.setSourceSystemModule(swiftSourceSystemModule);
		swiftIdentifier210.setSourceSystemFkFieldId(swiftSourceSystem.getId().longValue());

		SwiftMessageType swiftMessageType210 = new SwiftMessageType();
		swiftMessageType210.setMtVersionName("MT210");

		SwiftMessageType swiftMessageTypeAck = new SwiftMessageType();
		swiftMessageTypeAck.setMtVersionName("MT ACK/NAK");

		Answer<SwiftMessageType> swiftMessageTypeAnswer = invocation -> {
			String type = (String) invocation.getArguments()[0];
			if ("MT202".equals(type)) {
				return swiftMessageType202;
			}
			if ("MT210".equals(type)) {
				return swiftMessageType210;
			}
			if ("MT ACK/NAK".equals(type)) {
				return swiftMessageTypeAck;
			}
			return null;
		};

		//router
		Assertions.assertTrue(this.swiftProcessedReceptionFileHandler.getSwiftServerMessageRouter() instanceof ProwideSwiftServerMessageRouterImpl);
		ProwideSwiftServerMessageRouterImpl swiftServerMessageRouter = (ProwideSwiftServerMessageRouterImpl) this.swiftProcessedReceptionFileHandler.getSwiftServerMessageRouter();
		swiftServerMessageRouter.setSwiftServerMessageService(this.swiftMessageService);
		SwiftServerMessageRouter router = Mockito.spy(this.swiftProcessedReceptionFileHandler.getSwiftServerMessageRouter());
		this.swiftProcessedReceptionFileHandler.setSwiftServerMessageRouter(router);

		// log service
		Assertions.assertTrue(this.swiftProcessedReceptionFileHandler.getSwiftMessageLogHandler() instanceof SwiftMessageLogHandlerImpl);
		SwiftMessageLogHandlerImpl swiftMessageLogHandler = (SwiftMessageLogHandlerImpl) this.swiftProcessedReceptionFileHandler.getSwiftMessageLogHandler();
		swiftMessageLogHandler.setSwiftServerMessageLogService(this.swiftMessageLogService);
		Mockito.when(this.swiftMessageLogService.saveSwiftMessageLog(Mockito.any(SwiftMessageLog.class))).thenAnswer(invocation -> {
			SwiftMessageLog log = (SwiftMessageLog) invocation.getArguments()[0];
			log.setId((long) 1);
			return log;
		});

		// message service
		SwiftIdentifierHandlerImpl swiftIdentifierHandler = (SwiftIdentifierHandlerImpl) swiftServerMessageRouter.getSwiftIdentifierHandler();
		swiftIdentifierHandler.setSwiftServerMessageService(this.swiftMessageService);
		Mockito.when(this.swiftMessageService.getSwiftMessageTypeVersionCode(Mockito.anyString())).thenAnswer(swiftMessageTypeAnswer);
		Mockito.when(this.swiftMessageService.saveSwiftMessage(Mockito.any())).thenAnswer(invocation -> invocation.getArguments()[0]);
		Mockito.when(this.swiftMessageService.getSwiftMessageTypeByName(Mockito.anyString())).thenAnswer(swiftMessageTypeAnswer);

		// mock source system service
		swiftIdentifierHandler.setSwiftSourceSystemService(this.swiftSourceSystemService);
		Mockito.when(this.swiftSourceSystemService.getSwiftSourceSystemByBIC(Mockito.anyString())).thenReturn(swiftSourceSystem);

		// mock identifier service.
		swiftIdentifierHandler.setSwiftIdentifierService(this.swiftIdentifierService);
		Mockito.when(this.swiftIdentifierService.getSwiftIdentifierByUniqueId(Mockito.anyString(), Mockito.anyShort(), Mockito.any())).thenAnswer(invocation -> {
			String uniqueId = invocation.getArguments()[0].toString();
			if ("IMSCOL4598301".equals(uniqueId)) {
				return swiftIdentifier202;
			}
			if ("IMS4594491MTM".equals(uniqueId)) {
				return swiftIdentifier210;
			}
			return null;
		});
	}


	@Test
	public void handleFile() throws IOException, InterruptedException {
		Assertions.assertNotNull(this.swiftProcessedReceptionFileHandler);

		File testFile = new File(this.folder, FIN_FILE_NAME);
		Assertions.assertTrue(testFile.createNewFile());
		FileUtils.write(testFile, SWIFT_MULTIPLE, StandardCharsets.US_ASCII);
		MessageBuilder<File> builder = MessageBuilder.withPayload(testFile);
		Message<File> testMessage = builder.build();
		this.swiftProcessedReceptionFileHandler.handleFile(testMessage);

		AsynchronousMessage message = this.swiftIncomingMessageListener.awaitMessage(10);
		Assertions.assertNotNull(message);
		Assertions.assertTrue(message instanceof InstructionMessagingStatusMessage);
		InstructionMessagingStatusMessage swiftMessagingMessage = (InstructionMessagingStatusMessage) message;
		MatcherAssert.assertThat(swiftMessagingMessage.getUniqueStringIdentifier(), IsEqual.equalTo("IMSCOL4598301"));
		MatcherAssert.assertThat(swiftMessagingMessage.getStatus(), IsEqual.equalTo(InstructionStatuses.PROCESSED));

		message = this.swiftIncomingMessageListener.awaitMessage(10);
		Assertions.assertNotNull(message);
		Assertions.assertTrue(message instanceof InstructionMessagingStatusMessage);
		swiftMessagingMessage = (InstructionMessagingStatusMessage) message;
		MatcherAssert.assertThat(swiftMessagingMessage.getUniqueStringIdentifier(), IsEqual.equalTo("IMS4594491MTM"));
		MatcherAssert.assertThat(swiftMessagingMessage.getStatus(), IsEqual.equalTo(InstructionStatuses.PROCESSED));

		Mockito.verify(this.swiftProcessedReceptionFileHandler.getSwiftServerMessageRouter(), Mockito.times(2)).receive(Mockito.any());
		Mockito.verify(this.swiftMessageService, Mockito.times(2)).saveSwiftMessage(Mockito.any(SwiftMessage.class));
	}
}
