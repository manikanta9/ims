package com.clifton.swift.incoming;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.error.handler.SwiftExceptionHandler;
import com.clifton.swift.incoming.file.handler.SwiftFileHandler;
import com.clifton.swift.incoming.file.handler.error.SwiftProcessedErrorFileHandler;
import com.clifton.swift.incoming.file.handler.lau.SwiftLauDynamicMessageRouter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * Test Spring integration workflow for incoming SWIFT message files.
 * This does not test the endpoint service handlers.
 * It only verifies the endpoint is called for each incoming file and no undesired errors were handled.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftIncomingSwiftFileTests<M> {

	@Resource
	private File temp;

	// Directories
	@Resource
	private File receptionDirectory;

	@Resource
	private File errorDirectory;

	@Resource
	private File processedReceptionDirectory;

	@Resource
	private File processedErrorDirectory;

	// Monitors
	@Resource
	private SwiftTestHandlerMonitor<M> swiftProcessedReceptionFileHandler;

	@Resource
	private SwiftTestHandlerMonitor<M> swiftProcessedErrorFileHandler;

	@Resource
	private SwiftTestHandlerMonitor<M> swiftProcessingExceptionHandler;

	// mocked handlers
	@Resource
	private SwiftFileHandler swiftProcessedReceptionFileHandlerMock;

	@Resource
	private SwiftProcessedErrorFileHandler<M> swiftProcessedErrorFileHandlerMock;

	@Resource
	private SwiftExceptionHandler swiftExceptionHandler;

	@Resource
	private SwiftLauDynamicMessageRouter swiftReceptionDynamicMessageRouter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings({"ConstantConditions"})
	@BeforeEach
	public void testDirectoryState() {
		Assertions.assertTrue(this.temp.exists());
		Assertions.assertTrue(this.receptionDirectory.exists());
		Assertions.assertEquals(0, this.receptionDirectory.list().length);
		Assertions.assertTrue(this.errorDirectory.exists());
		Assertions.assertEquals(0, this.errorDirectory.list().length);
		Assertions.assertTrue(this.processedReceptionDirectory.exists());
		Assertions.assertEquals(0, this.processedReceptionDirectory.list().length);
		Assertions.assertTrue(this.processedErrorDirectory.exists());
		Assertions.assertEquals(0, this.processedErrorDirectory.list().length);
	}


	@AfterEach
	public void resetDirectoryState() {
		// clear processed reception and error
		recursiveDeleteChildren(this.processedReceptionDirectory);
		recursiveDeleteChildren(this.processedErrorDirectory);
		recursiveDeleteChildren(this.errorDirectory);
		this.swiftProcessedReceptionFileHandler.reset();
		this.swiftProcessedErrorFileHandler.reset();

		Mockito.reset(this.swiftProcessedReceptionFileHandlerMock, this.swiftProcessedErrorFileHandlerMock);
		enableLauChannel(false);
	}


	@Test
	public void testFileProcessing() throws Exception {
		// watch service gives spring integration time before we assert on directory state
		try (WatchService receptionWatch = FileSystems.getDefault().newWatchService();
		     WatchService errorWatch = FileSystems.getDefault().newWatchService();
		     WatchService processedReceptionWatch = FileSystems.getDefault().newWatchService();
		     WatchService processedErrorWatch = FileSystems.getDefault().newWatchService()) {
			// watch for deletions
			WatchKey receptionDirectoryKey = this.receptionDirectory.toPath().register(receptionWatch, StandardWatchEventKinds.ENTRY_DELETE);
			WatchKey errorDirectoryKey = this.errorDirectory.toPath().register(errorWatch, StandardWatchEventKinds.ENTRY_DELETE);

			// watch for additions
			WatchKey processedReceptionDirectoryKey = this.processedReceptionDirectory.toPath().register(processedReceptionWatch, StandardWatchEventKinds.ENTRY_CREATE);
			WatchKey processedErrorDirectoryKey = this.processedErrorDirectory.toPath().register(processedErrorWatch, StandardWatchEventKinds.ENTRY_CREATE);

			// put files to the reception folder
			org.springframework.core.io.Resource reception = new ClassPathResource("com/clifton/swift/incoming/files/manual/reception");
			File[] manualReceptionFiles = FileUtils.getFilesFromDirectory(FilePath.forFile(reception.getFile()), true).keySet().stream().map(FilePath::toFile).toArray(File[]::new);
			copyFilesToDirectory(manualReceptionFiles, this.receptionDirectory);
			File processedResponse = this.swiftProcessedReceptionFileHandler.awaitMessage(10);
			Assertions.assertNotNull(processedResponse, "Timed out waiting for response");
			Mockito.verify(this.swiftProcessedReceptionFileHandlerMock, Mockito.times(1)).handleFile(ArgumentMatchers.any());
			Mockito.verify(this.swiftExceptionHandler, Mockito.never()).handleExceptionMessage(ArgumentMatchers.any());

			// put files to the error folder
			org.springframework.core.io.Resource error = new ClassPathResource("com/clifton/swift/incoming/files/manual/error");
			File[] manualErrorFiles = FileUtils.getFilesFromDirectory(FilePath.forFile(error.getFile()), true).keySet().stream().map(FilePath::toFile).toArray(File[]::new);
			copyFilesToDirectory(manualErrorFiles, this.errorDirectory);
			File errorResponse = this.swiftProcessedErrorFileHandler.awaitMessage(10);
			Assertions.assertNotNull(errorResponse, "Timed out waiting for first response");
			// make no assumptions on which file will be processed first.
			Set<String> extensions = new HashSet<>();
			copyErrorFileExtension(errorResponse.getName(), extensions);
			Assertions.assertEquals(1, extensions.size());
			errorResponse = this.swiftProcessedErrorFileHandler.awaitMessage(10);
			Assertions.assertNotNull(errorResponse, "Timed out waiting for second response");
			copyErrorFileExtension(errorResponse.getName(), extensions);
			Assertions.assertEquals(2, extensions.size());

			Mockito.verify(this.swiftProcessedErrorFileHandlerMock, Mockito.times(2)).handleFile(ArgumentMatchers.any());
			Mockito.verify(this.swiftExceptionHandler, Mockito.never()).handleExceptionMessage(ArgumentMatchers.any());

			Assertions.assertTrue(this.swiftProcessedReceptionFileHandler.isEmptyQueue());
			Assertions.assertTrue(this.swiftProcessedErrorFileHandler.isEmptyQueue());

			// assert on empty source error folder.
			WatchKey returnedKey = errorWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(errorDirectoryKey, returnedKey);
			Assertions.assertEquals(0, CollectionUtils.createList(this.errorDirectory.listFiles()).size());

			// assert on empty source reception folder.
			returnedKey = receptionWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(receptionDirectoryKey, returnedKey);
			Assertions.assertEquals(0, CollectionUtils.createList(this.receptionDirectory.listFiles()).size());

			// assert on target error folder.
			returnedKey = processedErrorWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(processedErrorDirectoryKey, returnedKey);
			Assertions.assertEquals(2, CollectionUtils.createList(this.processedErrorDirectory.listFiles()).size());

			// assert on target reception folder.
			returnedKey = processedReceptionWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(processedReceptionDirectoryKey, returnedKey);
			Assertions.assertEquals(1, CollectionUtils.createList(this.processedReceptionDirectory.listFiles()).size());
		}
	}


	@Test
	public void testLauFileProcessing() throws Exception {
		// watch service gives spring integration time before we assert on directory state
		try (WatchService receptionWatch = FileSystems.getDefault().newWatchService(); WatchService processedReceptionWatch = FileSystems.getDefault().newWatchService()) {
			// enable local authentication
			enableLauChannel(true);

			// watch for deletions
			WatchKey receptionDirectoryKey = this.receptionDirectory.toPath().register(receptionWatch, StandardWatchEventKinds.ENTRY_DELETE);
			// watch for additions
			WatchKey processedReceptionDirectoryKey = this.processedReceptionDirectory.toPath().register(processedReceptionWatch, StandardWatchEventKinds.ENTRY_CREATE);

			org.springframework.core.io.Resource lau = new ClassPathResource("com/clifton/swift/incoming/files/manual/lau");
			File[] manualLauFiles = FileUtils.getFilesFromDirectory(FilePath.forFile(lau.getFile()), true).keySet().stream().map(FilePath::toFile).toArray(File[]::new);
			copyFilesToDirectory(manualLauFiles, this.receptionDirectory);
			File lauResponse = this.swiftProcessedReceptionFileHandler.awaitMessage(10);
			Assertions.assertNotNull(lauResponse, "Timed out waiting for first response");
			// make no assumptions on which file will be processed first.
			Set<String> extensions = new HashSet<>();
			copyLauFileExtension(lauResponse.getName(), extensions);
			Assertions.assertEquals(1, extensions.size());
			lauResponse = this.swiftProcessedReceptionFileHandler.awaitMessage(10);
			Assertions.assertNotNull(lauResponse, "Timed out waiting for second response");
			copyLauFileExtension(lauResponse.getName(), extensions);
			Assertions.assertEquals(2, extensions.size());

			Mockito.verify(this.swiftProcessedReceptionFileHandlerMock, Mockito.times(2)).handleFile(ArgumentMatchers.any());
			Mockito.verify(this.swiftExceptionHandler, Mockito.never()).handleExceptionMessage(ArgumentMatchers.any());

			Assertions.assertTrue(this.swiftProcessedReceptionFileHandler.isEmptyQueue());
			Assertions.assertTrue(this.swiftProcessedErrorFileHandler.isEmptyQueue());

			// assert on empty source reception folder.
			WatchKey returnedKey = receptionWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(receptionDirectoryKey, returnedKey);
			Assertions.assertEquals(0, CollectionUtils.createList(this.receptionDirectory.listFiles()).size());

			// assert on target reception folder.
			returnedKey = processedReceptionWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(processedReceptionDirectoryKey, returnedKey);
			Assertions.assertEquals(2, CollectionUtils.createList(this.processedReceptionDirectory.listFiles()).size());
		}
		finally {
			enableLauChannel(false);
		}
	}


	@Test
	public void testExceptionHandling() throws Exception {
		// watch service gives spring integration time before we assert on directory state
		try (WatchService receptionWatchService = FileSystems.getDefault().newWatchService();
		     WatchService processedReceptionWatch = FileSystems.getDefault().newWatchService()) {
			WatchKey receptionDirectoryKey = this.receptionDirectory.toPath().register(receptionWatchService, StandardWatchEventKinds.ENTRY_DELETE);
			WatchKey processedReceptionDirectoryKey = this.processedReceptionDirectory.toPath().register(processedReceptionWatch, StandardWatchEventKinds.ENTRY_CREATE);

			org.springframework.core.io.Resource reception = new ClassPathResource("com/clifton/swift/incoming/files/manual/reception");
			File[] manualReceptionFiles = FileUtils.getFilesFromDirectory(FilePath.forFile(reception.getFile()), true).keySet().stream().map(FilePath::toFile).toArray(File[]::new);
			this.swiftProcessedReceptionFileHandler.setThrowException(true);
			copyFilesToDirectory(manualReceptionFiles, this.receptionDirectory);
			File processedResponse = this.swiftProcessingExceptionHandler.awaitMessage(10);

			Assertions.assertNull(processedResponse, "Should not return a response");

			Mockito.verify(this.swiftProcessedReceptionFileHandlerMock, Mockito.times(1)).handleFile(ArgumentMatchers.any());
			Mockito.verify(this.swiftExceptionHandler, Mockito.times(1)).handleExceptionMessage(ArgumentMatchers.any());

			Assertions.assertTrue(this.swiftProcessingExceptionHandler.isEmptyQueue());

			// assert on empty source reception folder.
			WatchKey returnedKey = receptionWatchService.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(receptionDirectoryKey, returnedKey);
			Assertions.assertEquals(0, CollectionUtils.createList(this.receptionDirectory.listFiles()).size());

			// assert on target reception folder.
			returnedKey = processedReceptionWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertNotNull(returnedKey, "Timed out waiting for response");
			Assertions.assertEquals(processedReceptionDirectoryKey, returnedKey);
			Assertions.assertEquals(1, CollectionUtils.createList(this.processedReceptionDirectory.listFiles()).size());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void enableLauChannel(boolean enable) {
		this.swiftReceptionDynamicMessageRouter.setLocalAuthenticationEnabled(enable);
		this.swiftReceptionDynamicMessageRouter.initialize();
	}


	private File[] copyFilesToDirectory(File[] files, File directory) throws IOException {
		File[] returnVal = new File[files.length];
		for (int i = 0; i < files.length; i++) {
			File sourceFile = files[i];
			File targetFile = new File(directory.getAbsolutePath() + "/" + sourceFile.getName()).getAbsoluteFile();
			FileUtils.copyFileCreatePath(sourceFile, targetFile);
			returnVal[i] = targetFile;
		}
		return returnVal;
	}


	private void copyErrorFileExtension(String filename, Set<String> extensions) {
		if (filename.endsWith(".fin")) {
			extensions.add(".fin");
		}
		if (filename.endsWith(".fin.err")) {
			extensions.add(".fin.err");
		}
	}


	private void copyLauFileExtension(String filename, Set<String> extensions) {
		if (filename.endsWith(".fin")) {
			extensions.add(".fin");
		}
		if (filename.endsWith(".fin.lau")) {
			extensions.add(".fin.lau");
		}
	}


	private void recursiveDeleteChildren(File file) {
		File[] files = file.listFiles();
		if (files != null) {
			for (File each : files) {
				recursiveDelete(each);
			}
		}
	}


	private void recursiveDelete(File file) {
		recursiveDeleteChildren(file);
		file.delete();
	}
}
