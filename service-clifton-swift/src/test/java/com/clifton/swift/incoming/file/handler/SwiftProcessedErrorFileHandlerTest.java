package com.clifton.swift.incoming.file.handler;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.instruction.messaging.InstructionStatuses;
import com.clifton.security.user.SecurityUser;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.incoming.SwiftTestHandlerMonitor;
import com.clifton.swift.incoming.file.handler.error.SwiftProcessedErrorFileHandler;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.messaging.error.SwiftMessagingErrorService;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.common.LiteralExpression;
import org.springframework.integration.aggregator.ResequencingMessageHandler;
import org.springframework.integration.endpoint.EventDrivenConsumer;
import org.springframework.integration.handler.MessageHandlerChain;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Transactional
public class SwiftProcessedErrorFileHandlerTest<M> {

	private static final String TEST_INSTRUCTION_MESSAGE = "{1:F01PPSCUS60AXXX0000000000}{2:I202CNORUS40XCOLN}{4:\n" +
			":20:IMS469142MTM\n" +
			":21:MARG\n" +
			":32A:161212USD1289206,8\n" +
			":53B:/ATV04\n" +
			":57A:CNORUS40COL\n" +
			":58A:/157-00414\n" +
			"GOLDUS33\n" +
			"-}";

	private static final String TEST_INSTRUCTION_MESSAGE_ERR = "AC_ERR_11 : AC_ERR_11 : The file [3.20161212_125247.fin] with local timestamp [12/12/16 7:22 PM] and SHA-256 digest [LDr0BWuTS4fZIQBQsZOt+3H+6M9ZWP+ujdTchA6Npc4=] is a duplicate and will not be sent.";


	private static final String FIN_FILE_NAME = "123456.20161123_082528.fin";

	@Resource
	private AdvancedUpdatableDAO<SwiftMessageStatus, Criteria> swiftMessageStatusDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessageType, Criteria> swiftMessageTypeDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftMessage, Criteria> swiftMessageDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftSourceSystem, Criteria> swiftSourceSystemDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftSourceSystemModule, Criteria> swiftSourceSystemModuleDAO;

	@Resource
	private AdvancedUpdatableDAO<SwiftIdentifier, Criteria> swiftIdentifierDAO;

	@Autowired
	private ContextHandler contextHandler;

	@Resource
	private SwiftProcessedErrorFileHandler<M> swiftProcessedErrorFileHandler;

	@Resource
	private SwiftProcessedErrorFileHandler<M> actualSwiftProcessedErrorFileHandler;

	@Resource
	private File errorDirectory;

	@Resource
	private SwiftTestHandlerMonitor<M> swiftTestHandlerMonitor;

	@Resource
	private EventDrivenConsumer swiftErrorChannelChain;

	private Map<Class<? extends IdentityObject>, Map<String, IdentityObject>> testEntities = new HashMap<>();

	@Mock
	private SwiftServerStatusHandler mockSwiftServerStatusHandler;

	@Mock
	private SwiftMessagingErrorService mockSwiftMessagingErrorService;


	@BeforeEach
	public void setup() {
		if (this.testEntities.isEmpty()) {
			MockitoAnnotations.initMocks(this);
			this.actualSwiftProcessedErrorFileHandler.setSwiftServerStatusHandler(this.mockSwiftServerStatusHandler);
			this.actualSwiftProcessedErrorFileHandler.setSwiftMessagingErrorService(this.mockSwiftMessagingErrorService);

			SecurityUser user = new SecurityUser();
			user.setId(new Integer(7).shortValue());
			user.setUserName("TestUser");
			this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

			SwiftMessageStatus swiftMessageStatus = new SwiftMessageStatus();
			swiftMessageStatus.setName("ERROR");
			swiftMessageStatus.setDescription("An error prevented a messages from being processed successfully.");
			this.swiftMessageStatusDAO.save(swiftMessageStatus);

			this.testEntities.putIfAbsent(SwiftMessageType.class, new HashMap<>());
			this.testEntities.get(SwiftMessageType.class).put("ERROR", swiftMessageStatus);

			swiftMessageStatus = new SwiftMessageStatus();
			swiftMessageStatus.setName("LOGGED");
			swiftMessageStatus.setDescription("The message has been logged.");
			this.swiftMessageStatusDAO.save(swiftMessageStatus);

			this.testEntities.putIfAbsent(SwiftMessageStatus.class, new HashMap<>());
			this.testEntities.get(SwiftMessageStatus.class).put("LOGGED", swiftMessageStatus);

			SwiftMessageType swiftMessageType = new SwiftMessageType();
			swiftMessageType.setName("General Financial Institution Transfer Message");
			swiftMessageType.setDescription("General Financial Institution Transfer Message");
			swiftMessageType.setMtVersionName("MT202");
			this.swiftMessageTypeDAO.save(swiftMessageType);

			this.testEntities.putIfAbsent(SwiftMessageType.class, new HashMap<>());
			this.testEntities.get(SwiftMessageType.class).put("MT202", swiftMessageType);

			SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
			swiftIdentifier.setUniqueStringIdentifier("IMS469142MTM");
			swiftIdentifier.setSourceSystemFkFieldId((long) 469142);

			SwiftSourceSystem swiftSourceSystem = new SwiftSourceSystem();
			swiftSourceSystem.setName("IMS");
			swiftSourceSystem.setBusinessIdentifierCode("PPSCUS60");
			this.swiftSourceSystemDAO.save(swiftSourceSystem);

			this.testEntities.putIfAbsent(SwiftSourceSystem.class, new HashMap<>());
			this.testEntities.get(SwiftSourceSystem.class).put("IMS", swiftSourceSystem);

			swiftIdentifier.setSourceSystem(swiftSourceSystem);

			SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
			swiftSourceSystemModule.setName("M2M");
			swiftSourceSystemModule.setSourceSystem(swiftSourceSystem);
			swiftSourceSystemModule.setTargetMessageFormat(SwiftMessageFormats.MT);
			this.swiftSourceSystemModuleDAO.save(swiftSourceSystemModule);

			this.testEntities.putIfAbsent(SwiftSourceSystemModule.class, new HashMap<>());
			this.testEntities.get(SwiftSourceSystemModule.class).put("M2M", swiftSourceSystemModule);

			swiftIdentifier.setSourceSystemModule(swiftSourceSystemModule);
			this.swiftIdentifierDAO.save(swiftIdentifier);

			this.testEntities.putIfAbsent(SwiftIdentifier.class, new HashMap<>());
			this.testEntities.get(SwiftIdentifier.class).put("IMS469142MTM", swiftIdentifier);

			SwiftMessage swiftMessage = new SwiftMessage();
			swiftMessage.setType(swiftMessageType);
			swiftMessage.setText(TEST_INSTRUCTION_MESSAGE);
			swiftMessage.setMessageFormat(SwiftMessageFormats.MT);
			swiftMessage.setIdentifier(swiftIdentifier);
			swiftMessage.setIncoming(true);
			swiftMessage.setMessageStatus((SwiftMessageStatus) this.testEntities.get(SwiftMessageStatus.class).get("LOGGED"));
			this.swiftMessageDAO.save(swiftMessage);

			this.testEntities.putIfAbsent(SwiftMessage.class, new HashMap<>());
			this.testEntities.get(SwiftMessage.class).put("IMS469142MTM", swiftMessage);
		}
		else {
			Mockito.reset(this.mockSwiftServerStatusHandler, this.mockSwiftMessagingErrorService);

			SwiftMessageStatus logged = (SwiftMessageStatus) this.testEntities.get(SwiftMessageStatus.class).get("LOGGED");
			this.testEntities.get(SwiftMessage.class).values().forEach(s -> {
				SwiftMessage swiftMessage = (SwiftMessage) s;
				((SwiftMessage) s).setMessageStatus(logged);
				this.swiftMessageDAO.save(swiftMessage);
			});
		}
	}


	/**
	 * Test that two messages are moved from error to processed/error, and in doing so, the SwiftProcessedErrorFileHandler was called twice.
	 * This must use a mock swiftProcessedErrorFileHandler since the Integration code will do this handler call in its own thread with no
	 * transactions.
	 */
	@Test
	public void testSpringIntegration() throws IOException, InterruptedException {
		Path errorFilePath = this.errorDirectory.toPath().toAbsolutePath().resolve(FIN_FILE_NAME + ".err");
		Path errFile = Files.createFile(errorFilePath);
		Files.write(errFile, TEST_INSTRUCTION_MESSAGE_ERR.getBytes());

		Path datFilePath = this.errorDirectory.toPath().toAbsolutePath().resolve(FIN_FILE_NAME);
		Path datFile = Files.createFile(datFilePath);
		Files.write(datFile, TEST_INSTRUCTION_MESSAGE.getBytes());

		this.swiftTestHandlerMonitor.awaitMessage(10);
		Assertions.assertTrue(this.swiftTestHandlerMonitor.isEmptyQueue());

		Mockito.verify(this.swiftProcessedErrorFileHandler, Mockito.times(2)).handleFile(ArgumentMatchers.any());
	}


	@Test
	public void testSpringIntegrationGroupTimeout() throws IOException, InterruptedException {
		ResequencingMessageHandler resequencingMessageHandler = Optional.of(this.swiftErrorChannelChain)
				.map(EventDrivenConsumer::getHandler)
				.map(MessageHandlerChain.class::cast)
				.map(h -> h.getHandlers().stream()
						.filter(mh -> mh instanceof ResequencingMessageHandler)
						.map(ResequencingMessageHandler.class::cast)
						.findFirst()
						.orElse(null)
				)
				.orElseThrow(RuntimeException::new);
		resequencingMessageHandler.setGroupTimeoutExpression(new LiteralExpression(Long.toString(Duration.ofSeconds(1).toMillis())));
		Path datFilePath = this.errorDirectory.toPath().toAbsolutePath().resolve("123456.20161123_082533.fin");
		Path datFile = Files.createFile(datFilePath);
		Files.write(datFile, TEST_INSTRUCTION_MESSAGE.getBytes());

		this.swiftTestHandlerMonitor.awaitMessage(3);
		Assertions.assertTrue(this.swiftTestHandlerMonitor.isEmptyQueue());

		Mockito.verify(this.swiftProcessedErrorFileHandler, Mockito.times(1)).handleDiscardFile(ArgumentMatchers.any());
	}


	/**
	 * Test parsing message a Swift text file, looking up the Identifier and putting a status response back to JMS.
	 */
	@Test
	public void testActualSwiftProcessedErrorFileHandler() throws IOException {
		Path errorFilePath = this.errorDirectory.toPath().toAbsolutePath().resolveSibling(FileUtils.getFileNameWithoutExtension(FIN_FILE_NAME) + ".20161123_092728" + ".fin" + ".err");
		Path errFile = Files.createFile(errorFilePath);
		Files.write(errFile, TEST_INSTRUCTION_MESSAGE_ERR.getBytes());

		Path datFilePath = this.errorDirectory.toPath().toAbsolutePath().resolveSibling(FileUtils.getFileNameWithoutExtension(FIN_FILE_NAME) + ".20161123_042428" + ".fin");
		Path datFile = Files.createFile(datFilePath);
		Files.write(datFile, TEST_INSTRUCTION_MESSAGE.getBytes());

		MessageBuilder<File> builder = MessageBuilder.withPayload(errFile.toFile());
		this.actualSwiftProcessedErrorFileHandler.handleFile(builder.build());

		Mockito.verifyZeroInteractions(this.mockSwiftMessagingErrorService);
		Mockito.verify(this.mockSwiftServerStatusHandler).sendSwiftExportStatus("IMS469142MTM", "IMS", "M2M", InstructionStatuses.FAILED, TEST_INSTRUCTION_MESSAGE_ERR);
	}
}
