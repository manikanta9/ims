package com.clifton.swift;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * The {@link SwiftServiceProjectBasicTests} are disabled due to the current package structure. The {@link BasicProjectTests}
 * find relevant beans for testing by using the package name - i.e. com.clifton.swift, which is identical to the package name
 * used in the clifton-swift module. These tests may currently fail, or may fail after future updates, because of beans from
 * the clifton-swift module. Rather than adding skipMethods/skipServices, we'll leave this disabled until the package structure
 * can be updated (For example, using com.clifton.swift.service)
 *
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
@Disabled
public class SwiftServiceProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "swift";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.springframework.messaging.");
		imports.add("org.springframework.integration.");
		imports.add("java.security.SecureRandom");
		imports.add("org.springframework.beans.BeansException");
		imports.add("org.springframework.jmx.export.annotation.ManagedAttribute");
		imports.add("org.springframework.context.");
		imports.add("org.boris.winrun4j.");
		imports.add("javax.crypto.");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("com.thoughtworks.xstream.XStream");
		imports.add("org.springframework.jms.core.JmsTemplate");
		imports.add("javax.jms.");
		imports.add("org.springframework.expression.");
		imports.add("org.apache.commons.io.FileUtils");
		imports.add("com.prowidesoftware.swift.model.mt.AbstractMT");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();
		basicProjectAllowedSuffixNamesList.add("Router");
		basicProjectAllowedSuffixNamesList.add("Visitor");
		basicProjectAllowedSuffixNamesList.add("Enabler");
		basicProjectAllowedSuffixNamesList.add("Interceptor");
		return basicProjectAllowedSuffixNamesList;
	}
}
