package com.clifton.swift.utils;

import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.identifier.SwiftIdentifier;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;


public class SwiftMessageFileExtensionsTest {

	@Test
	public void generateFilename() {
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId(12345L);

		LocalTime startTime = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);
		String filename = SwiftMessageFileExtensions.MT.generateFilename("154321", LocalDateTime.now());
		LocalTime endTime = LocalTime.now().truncatedTo(ChronoUnit.SECONDS);

		List<String> fileParts = CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts(filename));
		Assertions.assertEquals(5, fileParts.size());
		MatcherAssert.assertThat(fileParts.get(0), IsEqual.equalTo("154321"));

		LocalDate today = LocalDate.now();
		String datePart = "";
		datePart += today.getYear();
		datePart += (today.getMonthValue() <= 9) ? "0" + today.getMonthValue() : today.getMonthValue();
		datePart += (today.getDayOfMonth() <= 9) ? "0" + today.getDayOfMonth() : today.getDayOfMonth();
		MatcherAssert.assertThat(fileParts.get(1), IsEqual.equalTo(datePart));

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HHmmss");
		LocalTime parsed = LocalTime.parse(fileParts.get(2), formatter);

		Assertions.assertTrue(parsed.equals(startTime) || parsed.isAfter(startTime));
		Assertions.assertTrue(parsed.equals(endTime) || parsed.isBefore(endTime));

		MatcherAssert.assertThat(fileParts.get(3), IsEqual.equalTo("." + SwiftMessageFileExtensions.MT.getRegularExtension()));
	}


	@Test
	public void isValidFilename() {
		SwiftMessageFileExtensions mt = SwiftMessageFileExtensions.MT;
		Assertions.assertTrue(mt.isValidFilename("123456.20161117_082528.fin"));
		Assertions.assertTrue(mt.isValidFilename("123456.20161117_082528.fin.err"));
		Assertions.assertTrue(mt.isValidFilename("123456.20161117_082528.FIN.ERR"));
		Assertions.assertTrue(mt.isValidFilename("123456.20161117_082528.fin.ERR"));

		Assertions.assertFalse(mt.isValidFilename("123456.20161117_082528.fin.text"));
		Assertions.assertFalse(mt.isValidFilename("123456.20161117_082528.finerr"));
		Assertions.assertFalse(mt.isValidFilename("123456.20161117_082528fin"));
		Assertions.assertFalse(mt.isValidFilename("123456.20161117_082528"));
		Assertions.assertFalse(mt.isValidFilename("123456.20161117_082528.err"));
	}
}
