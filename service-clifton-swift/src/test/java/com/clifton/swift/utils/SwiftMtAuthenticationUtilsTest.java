package com.clifton.swift.utils;

import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;


public class SwiftMtAuthenticationUtilsTest {

	public static final String MSG_1 = "{1:F21PPSCUS60AXXX0003000024}{4:{177:1610110702}{451:0}}{1:F01PPSCUS60AXXX0003000024}{2:O0810702161011EAEAXXXXGXXX00000960261610110002S}" +
			"{4:{202:0001}{203:0001}{305:A}{332:000000000000}}{5:{CHK:D8539E643A6A}{SYS:}{TNG:}}{S:{COP:P}}${1:A21PPSCUS60AXXX0003000003}{4:{177:1610110702}{451:0}}" +
			"{1:A01PPSCUS60AXXX0003000003}{2:O0810702161011EAEAXXXXGXXX00001507101610110002}{4:{202:0001}{203:0001}{305:A}{332:000000000000}}{5:{CHK:D8539E643A6A}{SYS:}{TNG:}}{S:{COP:P}}";

	public static final String MSG_2 = "{1:F21PPSCUS60AXXX0014000156}{4:{177:1701080802}{451:0}}{1:F01PPSCUS60AXXX0014000156}{2:O0810802170108EAEAXXXXGXXX00026837571701080002S}" +
			"{4:{202:0001}{203:0001}{305:A}{331:001316121714461701070920S03000127000038000047000173000117000154}{332:000127000038}}{5:{CHK:1A01C6D38BB3}{SYS:}{TNG:}}{S:{COP:P}}$" +
			"{1:A21PPSCUS60AXXX0014000001}{4:{177:1701080802}{451:0}}{1:A01PPSCUS60AXXX0014000001}{2:O0810802170108EAEAXXXXGXXX00014793681701080002}{4:{202:0001}{203:0001}{305:A}" +
			"{331:001316121714451701070920S03000008000021000001000008000001000021}{332:000008000021}}{5:{CHK:967FC6D38F6D}{SYS:}{TNG:}}{S:{COP:P}}";

	public static final String MSG_3 = "{1:F01PPSCUS66AXXX0000000000}{2:I541CNORUS44XETDN}{4: :16R:GENL :20C::SEME//57927 :23G:NEWM :16S:GENL :16R:TRADDET :98A::SETT//20181221 :98A::TRAD//20181219 :90B::DEAL//ACTU/USD251,74 :35B:ISIN US4642872000 :16R:FIA :12A::CLAS/ISIT/ETF :11A::DENO//USD :36B::SIZE//UNIT/1, :16S:FIA :16S:TRADDET :16R:FIAC :36B::SETT//UNIT/9900, :97A::SAFE//MCDO05 :16S:FIAC :16R:SETDET :22F::SETR//TRAD :16R:SETPRTY :95P::PSET//DTCYUS33 :16S:SETPRTY :16R:SETPRTY :95R::DEAG/DTCYID/161 :16S:SETPRTY :16R:SETPRTY :95R::SELL/DTCYID/161 :16S:SETPRTY :16R:AMT :19A::SETT//USD2492325, :16S:AMT :16R:AMT :19A::DEAL//USD2492226, :16S:AMT :16R:AMT :19A::EXEC//USD99, :16S:AMT :16R:AMT :19A::REGF//USD0, :16S:AMT :16S:SETDET -}";

	public static final String key_short = "1234567890123456789012345";
	public static final String key_long = "12345678901234567890123456789012";
	public static final String key_test = "dkJwwkK~~OgPz/E9u4ux";

	// base-64 encoded string, written to the .lau file.
	public static final String EXPECTED_SHORT_MSG_1 = "ceT6hqfK5Dv9qxhnarFHvvCYcMTf4swEctskoiWoyok=";
	public static final String EXPECTED_LONG_MSG_1 = "1xbL0fyFRoNC+mkay0utuucTYaz+qV0LWikIHSbpqdY=";
	public static final String EXPECTED_SHORT_MSG_2 = "IkWlDWPGGKWWXuJCve/zf2XqQbM3+EhJRTkEYQKqT1E=";
	public static final String EXPECTED_LONG_MSG_2 = "pExCCyGpqrQ6H0wNwy6EMe/vLRraeqJJAswpnPD7sZY=";
	public static final String EXPECTED_TEST_MSG_3 = "IeoBGzalXYF8CUZW+/wQGzcP+uQxxFMR/yq0xsRbibM=";


	@Test
	public void testEncodingShort() {
		byte[] signature = SwiftMtAuthenticationUtils.encode(key_short, MSG_1);
		MatcherAssert.assertThat(new String(signature, StandardCharsets.UTF_8), IsEqual.equalTo(EXPECTED_SHORT_MSG_1));
		signature = SwiftMtAuthenticationUtils.encode(key_short, MSG_2);
		MatcherAssert.assertThat(new String(signature, StandardCharsets.UTF_8), IsEqual.equalTo(EXPECTED_SHORT_MSG_2));
	}


	@Test
	public void testEncodingLong() {
		byte[] signature = SwiftMtAuthenticationUtils.encode(key_long, MSG_1);
		MatcherAssert.assertThat(new String(signature, StandardCharsets.UTF_8), IsEqual.equalTo(EXPECTED_LONG_MSG_1));
		signature = SwiftMtAuthenticationUtils.encode(key_long, MSG_2);
		MatcherAssert.assertThat(new String(signature, StandardCharsets.UTF_8), IsEqual.equalTo(EXPECTED_LONG_MSG_2));
	}


	@Test
	public void testEncodingTest() {
		byte[] signature = SwiftMtAuthenticationUtils.encode(key_test, MSG_3);
		MatcherAssert.assertThat(new String(signature, StandardCharsets.UTF_8), IsEqual.equalTo(EXPECTED_TEST_MSG_3));
	}


	@Test
	public void testKeys() {
		Assertions.assertTrue(SwiftMtAuthenticationUtils.isValidKey("!@#$%^&*()_+{}[]\\|/.,<>0aA"));
		Assertions.assertTrue(SwiftMtAuthenticationUtils.isValidKey("dkJwwkK~~OgPz/E9u4ux"));
		Assertions.assertTrue(SwiftMtAuthenticationUtils.isValidKey("dkJwwkK~~OgPz/E9u"));
		Assertions.assertTrue(SwiftMtAuthenticationUtils.isValidKey("dkJwwkK~~OgPz/E9uaB843cDjsl;adbe"));

		Assertions.assertFalse(SwiftMtAuthenticationUtils.isValidKey("dkJwwkK~~OgPz/E9"), "too short");
		Assertions.assertFalse(SwiftMtAuthenticationUtils.isValidKey("ABCDEfghijklmnopqrstuvwxyz0123456"), "too long");
		Assertions.assertFalse(SwiftMtAuthenticationUtils.isValidKey("dkjwwkk~~ogpz/e9d"), "no caps");
		Assertions.assertFalse(SwiftMtAuthenticationUtils.isValidKey("DKJWWKK~~OGPZ/E9D"), "no lower");
		Assertions.assertFalse(SwiftMtAuthenticationUtils.isValidKey("dkJwwkK~~OgPz/E*u"), "no digits");
		Assertions.assertFalse(SwiftMtAuthenticationUtils.isValidKey("dkJwwkK~~OgPz/E9u\n"), "white space");
	}


	@Test
	public void trimAll() {
		MatcherAssert.assertThat(SwiftMtAuthenticationUtils.trimAll("abcdefghi\t\n\f\r"), IsEqual.equalTo("abcdefghi"));
		MatcherAssert.assertThat(SwiftMtAuthenticationUtils.trimAll("abcdefghi\t\n\f\rafasdfasdfasd"), IsEqual.equalTo("abcdefghiafasdfasdfasd"));
	}
}
