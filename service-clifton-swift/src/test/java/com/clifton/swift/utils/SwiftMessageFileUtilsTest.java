package com.clifton.swift.utils;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.io.File;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;


public class SwiftMessageFileUtilsTest {

	@TempDir
	public File folder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetMessageFormatFromExtension() {
		Assertions.assertEquals(SwiftMessageFormats.MT, SwiftMessageFileUtils.getMessageFormatFromExtension("123456.20161117_082528.fin"));
		Assertions.assertEquals(SwiftMessageFormats.MT, SwiftMessageFileUtils.getMessageFormatFromExtension("123456.20161117_082528.fin.err"));
		Assertions.assertEquals(SwiftMessageFormats.MT, SwiftMessageFileUtils.getMessageFormatFromExtension("123456.20161117_082528.fin.FIN"));
		Assertions.assertEquals(SwiftMessageFormats.MT, SwiftMessageFileUtils.getMessageFormatFromExtension("123456.20161117_082528.fin.ERR"));
		Assertions.assertEquals(SwiftMessageFormats.MT, SwiftMessageFileUtils.getMessageFormatFromExtension("123456.20161117_082528.fin"));
	}


	@Test
	public void testGetMessageFormatFromExtensionValidationException() {
		Assertions.assertThrows(ValidationException.class, () -> SwiftMessageFileUtils.getMessageFormatFromExtension("123456.20161117_082528.fin.text"));
	}


	@Test
	public void testIsErrorMessage() throws Exception {
		Assertions.assertTrue(new File(this.folder, "testIsErrorMessage_082521.fin.err").createNewFile());
		Assertions.assertTrue(new File(this.folder, "testIsErrorMessage_082522.fin.FIN").createNewFile());
		Assertions.assertTrue(new File(this.folder, "testIsErrorMessage_082523.fin.ERR").createNewFile());
		Assertions.assertTrue(new File(this.folder, "testIsErrorMessage_082524.fin").createNewFile());

		Assertions.assertTrue(SwiftMessageFileUtils.isErrorMessage("testIsErrorMessage_082521.fin.err"));
		Assertions.assertFalse(SwiftMessageFileUtils.isErrorMessage("testIsErrorMessage_082522.fin.FIN"));
		Assertions.assertTrue(SwiftMessageFileUtils.isErrorMessage("testIsErrorMessage_082523.fin.ERR"));
		Assertions.assertFalse(SwiftMessageFileUtils.isErrorMessage("testIsErrorMessage_082524.fin"));
	}


	@Test
	public void testGetMessageFileContents() throws Exception {
		byte[] testBytes = new byte[]{
				0b001,
				0b010,
				0b011,
				0b100,
				0b101,
				0b110,
				0b111,
				0b1000,
				0b1001,
				0b1010,
				0b1011,
				0b1100,
				0b1101,
				0b1110,
				0b1111,
				0b10000,
				0b10001,
				0b10010,
				0b10011,
				0b10100
		};
		File file = new File(this.folder, "testGetMessageFileContents");
		Assertions.assertTrue(file.createNewFile());
		Files.write(file.toPath(), testBytes);
		Message<File> message = new Message<File>() {
			@Override
			public File getPayload() {
				return file;
			}


			@Override
			public MessageHeaders getHeaders() {
				return new MessageHeaders(Collections.emptyMap());
			}
		};
		String contents = SwiftMessageFileUtils.getMessageFileContents(message);
		MatcherAssert.assertThat(testBytes, IsEqual.equalTo(contents.getBytes()));
	}


	@Test
	public void testGetSwiftMessageTextByteSize() {
		byte[] testBytes = new byte[]{
				0b001,
				0b010,
				0b011,
				0b100,
				0b101,
				0b110,
				0b111,
				0b1000,
				0b1001,
				0b1010,
				0b1011,
				0b1100,
				0b1101,
				0b1110,
				0b1111,
				0b10000,
				0b10001,
				0b10010,
				0b10011,
				0b10100
		};
		SwiftMessage swiftMessage = new SwiftMessage();
		swiftMessage.setText(new String(testBytes));
		MatcherAssert.assertThat(SwiftMessageFileUtils.getSwiftMessageTextByteSize(swiftMessage), IsEqual.equalTo(testBytes.length));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFindExtension() {
		Assertions.assertNotNull(SwiftMessageFileUtils.findExtension("fin"));
		Assertions.assertNotNull(SwiftMessageFileUtils.findExtension("err"));
		Assertions.assertNotNull(SwiftMessageFileUtils.findExtension("FIN"));
		Assertions.assertNotNull(SwiftMessageFileUtils.findExtension("ERR"));
		Assertions.assertNull(SwiftMessageFileUtils.findExtension("text"));
		Assertions.assertEquals(SwiftMessageFileExtensions.MT, SwiftMessageFileUtils.findExtension("fin"));
	}


	@Test
	public void testFindExtensionForFormat() {
		Assertions.assertNotNull(SwiftMessageFileUtils.findExtensionForFormat(SwiftMessageFormats.MT));
		Assertions.assertNull(SwiftMessageFileUtils.findExtensionForFormat(SwiftMessageFormats.MX));
	}


	@Test
	public void testIsErrorExtension() {
		Assertions.assertTrue(SwiftMessageFileUtils.isErrorExtension("err"));
		Assertions.assertTrue(SwiftMessageFileUtils.isErrorExtension("ERR"));
		Assertions.assertFalse(SwiftMessageFileUtils.isErrorExtension(".err"));
		Assertions.assertFalse(SwiftMessageFileUtils.isErrorExtension("fin"));
	}


	@Test
	public void testGetMessageFormat() {
		Assertions.assertNotNull(SwiftMessageFileUtils.getMessageFormat("123456.20161117_082528.fin"));
		Assertions.assertNotNull(SwiftMessageFileUtils.getMessageFormat("123456.20161117_082528.fin.err"));
		Assertions.assertNotNull(SwiftMessageFileUtils.getMessageFormat("123456.20161117_082528.fin.FIN"));
		Assertions.assertNotNull(SwiftMessageFileUtils.getMessageFormat("123456.20161117_082528.fin.ERR"));
		Assertions.assertNull(SwiftMessageFileUtils.getMessageFormat("123456.20161117_082528.fin.text"));
		Assertions.assertEquals(SwiftMessageFormats.MT, SwiftMessageFileUtils.getMessageFormat("123456.20161117_082528.fin"));
	}


	@Test
	public void testGetMessageCount() {
		Assertions.assertEquals("123456", SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.fin"));
		Assertions.assertEquals("123456", SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.fin.err"));
		Assertions.assertEquals("123456", SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.FIN.ERR"));
		Assertions.assertEquals("123456", SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.fin.ERR"));
		Assertions.assertNull(SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.fin.text"));
		Assertions.assertNull(SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.finerr"));
		Assertions.assertNull(SwiftMessageFileUtils.getMessageCount("123456.20161117_082528fin"));
		Assertions.assertNull(SwiftMessageFileUtils.getMessageCount("123456.20161117_082528"));
		Assertions.assertNull(SwiftMessageFileUtils.getMessageCount("123456.20161117_082528.err"));
	}


	@Test
	public void testFindRegularFromErrorFile() throws Exception {
		File regular = new File(this.folder, "33.20170726_154955.20170726_155354.fin");
		Assertions.assertTrue(regular.createNewFile());
		File error = new File(this.folder, "33.20170726_154955.20170726_155354.fin.err");
		Assertions.assertTrue(error.createNewFile());
		File found = SwiftMessageFileUtils.findRegularFromErrorFile(error);
		Assertions.assertNotNull(found);
		Assertions.assertEquals(regular, found);

		regular = new File(this.folder, "29.20170718_160441.20170718_160720.fin");
		Assertions.assertTrue(regular.createNewFile());
		error = new File(this.folder, "29.20170718_160441.20170718_160722.fin.err");
		Assertions.assertTrue(error.createNewFile());
		found = SwiftMessageFileUtils.findRegularFromErrorFile(error);
		Assertions.assertNotNull(found);
		Assertions.assertEquals(regular, found);

		regular = new File(this.folder, "255.20161221_084919.20161221_092910.fin");
		Assertions.assertTrue(regular.createNewFile());
		error = new File(this.folder, "255.20161221_084919.20161221_092911.fin.err");
		Assertions.assertTrue(error.createNewFile());
		found = SwiftMessageFileUtils.findRegularFromErrorFile(error);
		Assertions.assertNotNull(found);
		Assertions.assertEquals(regular, found);

		regular = new File(this.folder, "289.20161221_084924.20161221_091712.fin");
		Assertions.assertTrue(regular.createNewFile());
		error = new File(this.folder, "289.20161221_084924.20161221_091711.fin.err");
		Assertions.assertTrue(error.createNewFile());
		found = SwiftMessageFileUtils.findRegularFromErrorFile(error);
		Assertions.assertNotNull(found);
		Assertions.assertEquals(regular, found);

		regular = new File(this.folder, "283.20161221_084923.20161221_090911.fin");
		Assertions.assertTrue(regular.createNewFile());
		error = new File(this.folder, "283.20161221_084923.20161221_090910.fin.err");
		Assertions.assertTrue(error.createNewFile());
		found = SwiftMessageFileUtils.findRegularFromErrorFile(error);
		Assertions.assertNotNull(found);
		Assertions.assertEquals(regular, found);
	}


	@Test
	public void testGetFileNameParts() {
		Assertions.assertEquals(Arrays.asList("123456", "20161117", "082528", ".fin", "null"), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.fin")));
		Assertions.assertEquals(Arrays.asList("123456", "20161117", "082528", ".FIN", "null"), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.FIN")));
		Assertions.assertEquals(Arrays.asList("123456", "20161117", "082528", ".fin", ".err"), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.fin.err")));
		Assertions.assertEquals(Arrays.asList("123456", "20161117", "082528", ".FIN", ".ERR"), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.FIN.ERR")));
		Assertions.assertEquals(Arrays.asList("123456", "20161117", "082528", ".fin", ".ERR"), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.fin.ERR")));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.fin.text")));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528.finerr")));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.createList(SwiftMessageFileUtils.getFileNameParts("123456.20161117_082528fin")));
	}
}
