package com.clifton.swift.outgoing;

import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.outgoing.aggregation.SwiftMessageGroupKey;
import com.clifton.swift.outgoing.aggregation.SwiftMessageStore;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftServerOutgoingFileMessageHandlerTest {

	public static final Function<String, SwiftMessage> Swifter = string -> {
		SwiftMessage swiftMessage = new SwiftMessage();
		swiftMessage.setId((long) 1);
		swiftMessage.setMessageFormat(SwiftMessageFormats.MT);
		swiftMessage.setText(string);
		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftMessage.setIdentifier(swiftIdentifier);
		return swiftMessage;
	};
	public static final Function<String, AbstractMT> Parser = string -> {
		try {
			return AbstractMT.parse(string);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not parse");
		}
	};

	@Resource
	private File emissionDirectory;

	@Resource
	private SwiftServerOutgoingFileMessageHandler swiftServerOutgoingFileMessageHandler;

	@Resource
	private SwiftMessageStore swiftMessageStore;

	@Resource
	private SwiftMessageService swiftMessageService;

	@Value("#{T(java.lang.Integer).parseInt(${swift.aggregation.group.timeout.milliseconds})}")
	private int groupTimeoutMilliseconds;

	@Value("#{T(java.lang.Integer).parseInt(${swift.aggregation.mt.payload.max.bytes})}")
	private int maximumBytes;

	@Value("#{T(java.lang.Integer).parseInt(${swift.aggregation.mt.payload.tolerance.bytes})}")
	private int toleranceBytes;

	private List<String> messages;

	private int expectedFileCount;


	@BeforeEach
	public void loadMessageFile() throws IOException {
		ClassPathResource classPathResource = new ClassPathResource("com/clifton/swift/outgoing/aggregation/swift-messages.txt");
		String fileString = new String(Files.readAllBytes(classPathResource.getFile().toPath()), StandardCharsets.UTF_8);
		this.messages = Arrays.stream(fileString.split("\\$")).collect(Collectors.toList());
		int expectedBytes = this.messages.stream().mapToInt(String::length).sum() + (this.messages.size() - 1);
		this.expectedFileCount = (expectedBytes / (this.maximumBytes - this.toleranceBytes)) + 1;

		SwiftMessageStatus completeMessageStatus = new SwiftMessageStatus();
		completeMessageStatus.setId((short) 5);
		completeMessageStatus.setName("COMPLETE");
		completeMessageStatus.setDescription("The message has been successfully processed.");
		Mockito.when(this.swiftMessageService.getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.COMPLETE.name())).thenReturn(completeMessageStatus);
		Mockito.when(this.swiftMessageService.getSwiftMessage(1)).thenReturn(new SwiftMessage());
	}


	@AfterEach
	public void clearEmissionDirectory() throws IOException {
		List<File> files = CollectionUtils.createList(this.emissionDirectory.listFiles());
		for (File file : files) {
			Files.delete(file.toPath());
		}
		Mockito.reset(this.swiftMessageService);
	}


	@Test
	public void processManyMTMessages() throws IOException, InterruptedException {
		try (WatchService emissionWatchService = FileSystems.getDefault().newWatchService()) {
			// key for deletions of the .writing file, this means its done writing a file.
			WatchKey emissionWatchKey = this.emissionDirectory.toPath().register(emissionWatchService, StandardWatchEventKinds.ENTRY_DELETE);
			this.messages.stream().map(Swifter).forEach(m -> this.swiftServerOutgoingFileMessageHandler.send(m));
			emissionWatchService.poll(this.groupTimeoutMilliseconds, TimeUnit.MILLISECONDS);
			int pollEvents = emissionWatchKey.pollEvents().size();
			long maximumPollTime = System.currentTimeMillis() + 2 * this.groupTimeoutMilliseconds;
			long startTime = System.currentTimeMillis();
			// will get all but the last group immediately, the last group will need to timeout first.
			while (pollEvents < this.expectedFileCount && System.currentTimeMillis() < maximumPollTime) {
				this.emissionDirectory.toPath().register(emissionWatchService, StandardWatchEventKinds.ENTRY_DELETE);
				emissionWatchService.poll(1, TimeUnit.SECONDS);
				pollEvents += emissionWatchKey.pollEvents().size();
			}
			// should be about the same as the group timeout, 5 seconds.
			long duration = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - startTime);
			Assertions.assertTrue(duration <= TimeUnit.MILLISECONDS.toSeconds(maximumPollTime), "Timeout exceeded waiting for last group.");
			Assertions.assertEquals(this.expectedFileCount, pollEvents, "Unexpected number of created files");
			List<File> files = CollectionUtils.createList(this.emissionDirectory.listFiles());
			Assertions.assertEquals(this.expectedFileCount, files.size(), "Unexpected outgoing file count.");
			List<String> fileContents = new ArrayList<>();
			for (File file : files) {
				Assertions.assertTrue(file.length() < this.maximumBytes);
				fileContents.add(new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8));
			}
			List<String> fileMessages = fileContents.stream().flatMap(s -> Arrays.stream(s.split("\\$"))).collect(Collectors.toList());
			Assertions.assertEquals(this.messages.size(), fileMessages.size(), "The messages in the outgoing files should be the same as incoming.");
			fileMessages.stream().peek(Parser::apply);
			Assertions.assertEquals(0, this.swiftMessageStore.getMessageGroupCount(), "Expected the message store to be empty");
			fileMessages.removeAll(this.messages);
			Assertions.assertTrue(fileMessages.isEmpty(), "The messages going in should be the same as in the files.");
		}
	}


	@Test
	public void checkSingleMessageRollover() throws IOException, InterruptedException {
		SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.of(SwiftMessageFormats.MT);
		this.expectedFileCount = 2;

		int messageCount = 0;
		try (WatchService emissionWatchService = FileSystems.getDefault().newWatchService()) {
			this.emissionDirectory.toPath().register(emissionWatchService, StandardWatchEventKinds.ENTRY_DELETE);
			for (String message : this.messages) {
				SwiftMessage swiftMessage = Swifter.apply(message);
				this.swiftServerOutgoingFileMessageHandler.send(swiftMessage);
				messageCount++;
				if (messageCount > 1 && this.swiftMessageStore.messageGroupSize(swiftMessageGroupKey) == 1) {
					break;
				}
			}

			long timeout = System.currentTimeMillis() + 2 * (this.groupTimeoutMilliseconds);
			int count = 0;
			while (System.currentTimeMillis() < timeout && count < 2) {
				WatchKey watchKey = emissionWatchService.poll(this.groupTimeoutMilliseconds, TimeUnit.MILLISECONDS);
				if (watchKey != null) {
					List<WatchEvent<?>> events = watchKey.pollEvents();
					count += events.size();
				}
			}
		}

		List<File> files = CollectionUtils.createList(this.emissionDirectory.listFiles());
		Assertions.assertEquals(this.expectedFileCount, files.size(), "Unexpected outgoing file count.");
		List<String> fileContents = new ArrayList<>();
		for (File file : files) {
			Assertions.assertTrue(file.length() < this.maximumBytes);
			fileContents.add(new String(Files.readAllBytes(file.toPath()), StandardCharsets.UTF_8));
		}
		List<String> fileMessages = fileContents.stream().flatMap(s -> Arrays.stream(s.split("\\$"))).collect(Collectors.toList());
		Assertions.assertEquals(messageCount, fileMessages.size(), "The messages in the outgoing files should be the same as incoming.");
		Assertions.assertEquals(0, this.swiftMessageStore.getMessageGroupCount(), "Expected the message store to be empty");
	}
}
