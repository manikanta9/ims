package com.clifton.swift.outgoing.aggregation;

import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.integration.store.MessageGroup;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = "classpath:com/clifton/swift/outgoing/aggregation/swift-test-outgoing-context.xml")
@DirtiesContext
public class SwiftMessageStoreTest {

	@Resource
	private SwiftMessageStore swiftMessageStore;

	@Resource
	private SwiftAggregateStrategy swiftMtAggregateStrategy;

	private List<String> messages;


	@BeforeEach
	public void loadMessageFile() throws IOException {
		ClassPathResource classPathResource = new ClassPathResource("com/clifton/swift/outgoing/aggregation/swift-messages.txt");
		String fileString = new String(Files.readAllBytes(classPathResource.getFile().toPath()), StandardCharsets.UTF_8);
		this.messages = Arrays.stream(fileString.split("\\$")).collect(Collectors.toList());
	}


	@Test
	public void testAggregation() {
		SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.of(SwiftMessageFormats.MT);
		this.swiftMessageStore.getMessageGroup(swiftMessageGroupKey);
		this.messages.stream()
				.map(SwiftMessageAggregatorTest.Swifter)
				.map(swiftMessage -> MessageBuilder
						.withPayload(swiftMessage)
						.setHeader(SwiftServerOutgoingFileMessageHandler.PAYLOAD_BYTES_HEADER_KEY, SwiftMessageFileUtils.getSwiftMessageTextByteSize(swiftMessage))
						.setHeader(SwiftServerOutgoingFileMessageHandler.PAYLOAD_GROUP_HEADER_KEY, SwiftMessageGroupKey.of(swiftMessage.getMessageFormat()))
						.build())
				.forEach(m -> this.swiftMessageStore.addMessageToGroup(swiftMessageGroupKey, m));
		MatcherAssert.assertThat(SwiftMessageAggregatorTest.FILE_MESSAGE_COUNT, IsEqual.equalTo(Long.valueOf(this.swiftMessageStore.getMessageCount()).intValue()));
		for (MessageGroup messageGroup : this.swiftMessageStore) {
			SwiftMessageGroup group = (SwiftMessageGroup) messageGroup;
			Assertions.assertTrue(group.getPayloadBytes() < this.swiftMtAggregateStrategy.getMaximumPayloadBytes());
			Object groupId = group.getGroupId();
			MatcherAssert.assertThat(SwiftMessageGroupKey.class, IsEqual.equalTo(groupId.getClass()));
			SwiftMessageGroupKey groupKey = SwiftMessageGroupKey.cast(groupId);
			Assertions.assertTrue((groupKey.getSequence() == 0 && !group.isComplete() || (groupKey.getSequence() > 0 && group.isComplete())));
		}
	}
}
