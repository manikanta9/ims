package com.clifton.swift.outgoing.aggregation;

import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.outgoing.AggregatedSwiftMessage;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.utils.SwiftMessageFileUtils;
import com.prowidesoftware.swift.model.mt.AbstractMT;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.integration.support.AbstractIntegrationMessageBuilder;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = "classpath:com/clifton/swift/outgoing/aggregation/swift-test-outgoing-context.xml")
public class SwiftMessageAggregatorTest {

	public static final int FILE_MESSAGE_COUNT = 359;

	public static final Function<String, AbstractMT> Parser = string -> {
		try {
			return AbstractMT.parse(string);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not parse");
		}
	};

	public static final Function<String, SwiftMessage> Swifter = string -> {
		SwiftMessage swiftMessage = new SwiftMessage();
		swiftMessage.setMessageFormat(SwiftMessageFormats.MT);
		swiftMessage.setText(string);
		return swiftMessage;
	};

	@Resource
	private SwiftMessageAggregator swiftMessageAggregator;

	private String fileString;
	private List<String> messages;
	private int fileBytes;


	@BeforeEach
	public void loadMessageFile() throws IOException {
		// extracted message column as flat file from database with no delimiters and '$' line endings.
		ClassPathResource classPathResource = new ClassPathResource("com/clifton/swift/outgoing/aggregation/swift-messages.txt");
		this.fileBytes = Long.valueOf(classPathResource.getFile().length()).intValue();
		this.fileString = new String(Files.readAllBytes(classPathResource.getFile().toPath()), StandardCharsets.UTF_8);
		this.messages = Arrays.stream(this.fileString.split("\\$")).collect(Collectors.toList());
	}


	@Test
	public void testAggregation() {
		Assertions.assertNotNull(this.swiftMessageAggregator);
		MatcherAssert.assertThat("Expected messages from the file.", this.messages.size(), IsEqual.equalTo(FILE_MESSAGE_COUNT));
		// makes sure all messages can be parsed.
		this.messages.forEach(Parser::apply);
		SwiftMessageGroupKey swiftMessageGroupKey = SwiftMessageGroupKey.of(SwiftMessageFormats.MT);
		final SwiftMessageGroup messageGroup = new SwiftMessageGroup(swiftMessageGroupKey);
		final AtomicInteger totalBytes = new AtomicInteger(0);
		this.messages
				.stream()
				.map(Swifter)
				.forEach(s -> {
					int bytes = SwiftMessageFileUtils.getSwiftMessageTextByteSize(s);
					totalBytes.addAndGet(bytes);
					Message<?> message = MessageBuilder.withPayload(s).build();
					messageGroup.addMessage(message, bytes);
				});
		MatcherAssert.assertThat(this.messages.size(), IsEqual.equalTo(messageGroup.getMessages().size()));
		Object result = this.swiftMessageAggregator.processMessageGroup(messageGroup);
		Assertions.assertNotNull(result);
		Message<?> messageResults = ((AbstractIntegrationMessageBuilder<?>) result).popSequenceDetails().build();
		Assertions.assertTrue(messageResults.getHeaders().containsKey(SwiftServerOutgoingFileMessageHandler.PAYLOAD_MESSAGE_COUNT_KEY));
		Assertions.assertTrue(messageResults.getHeaders().containsValue(FILE_MESSAGE_COUNT));
		AggregatedSwiftMessage swiftMessageResults = (AggregatedSwiftMessage) messageResults.getPayload();
		MatcherAssert.assertThat(this.fileString, IsEqual.equalTo(swiftMessageResults.getText()));
		// include the delimiters '$' in the byte count.
		MatcherAssert.assertThat(this.fileString.getBytes(StandardCharsets.UTF_8).length, IsEqual.equalTo(totalBytes.addAndGet(this.messages.size() - 1)));
		MatcherAssert.assertThat(this.fileBytes, IsEqual.equalTo(this.fileString.getBytes(StandardCharsets.UTF_8).length));
	}
}
