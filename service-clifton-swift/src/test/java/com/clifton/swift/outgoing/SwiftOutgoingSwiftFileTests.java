package com.clifton.swift.outgoing;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.folder.TestFolderWatcher;
import com.clifton.core.util.CollectionUtils;
import com.clifton.swift.error.handler.SwiftExceptionHandler;
import com.clifton.swift.identifier.SwiftIdentifier;
import com.clifton.swift.incoming.SwiftTestHandlerMonitor;
import com.clifton.swift.message.SwiftMessage;
import com.clifton.swift.message.SwiftMessageFormats;
import com.clifton.swift.message.SwiftMessageService;
import com.clifton.swift.message.SwiftMessageStatus;
import com.clifton.swift.message.SwiftMessageType;
import com.clifton.swift.messaging.asynchronous.SwiftMessagingMessage;
import com.clifton.swift.messaging.error.SwiftMessagingErrorService;
import com.clifton.swift.server.handler.SwiftServerOutgoingFileMessageHandler;
import com.clifton.swift.server.status.SwiftServerStatusHandler;
import com.clifton.swift.system.SwiftSourceSystem;
import com.clifton.swift.system.SwiftSourceSystemModule;
import com.clifton.swift.utils.SwiftMtAuthenticationUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessagingException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SwiftOutgoingSwiftFileTests<M> {

	private static final String TEST_INSTRUCTION_MESSAGE = "{1:F01FOOSEDR0AXXX0000000000}{2:I202FOORECV0XXXXN}{4:\n" +
			":20:IMS:M2M:56872\n" +
			":21:MARG\n" +
			":32A:161026EUR10120,\n" +
			":53B:/12345678901234567890\n" +
			":57A:BANKANC0XXX\n" +
			":58A:/12345678901234567XXX\n" +
			"BANKANC0XXX\n}";

	@Resource
	private File temp;

	@Resource
	private File emissionDirectory;

	@Resource
	private SwiftTestHandlerMonitor<M> swiftEmissionHandlerMonitor;

	@Resource
	private SwiftServerOutgoingFileMessageHandler swiftServerOutgoingFileMessageHandler;

	@Resource
	private SwiftMessageSplitter swiftMessageSplitter;

	@Resource
	private SwiftExceptionHandler swiftExceptionHandler;

	@Resource
	private SwiftMessagingErrorService swiftMessagingErrorService;

	@Resource
	private SwiftMessageService swiftMessageService;

	@Resource
	private SwiftServerStatusHandler swiftServerStatusHandler;

	@Resource
	private File keyFile;

	// Test message
	private SwiftMessage swiftMessage;

	private String key;

	private static Map<String, TestState> testStateMap = new HashMap<>();


	@SuppressWarnings({"ConstantConditions"})
	@BeforeEach
	public void buildSwiftMessage(TestInfo testInfo) {
		TestState testState = new TestState(SwiftOutgoingSwiftFileTests.this.temp, testInfo.getDisplayName());
		testStateMap.put(testInfo.getDisplayName(), testState);

		this.key = FileUtils.readFileToString(this.keyFile);
		this.swiftMessageSplitter.setLocalAuthenticationEnabled(true);
		this.swiftMessageSplitter.init();
		this.swiftMessageSplitter.setLocalAuthenticationEnabled(false);

		this.swiftMessage = new SwiftMessage();
		this.swiftMessage.setId((long) 2345);
		this.swiftMessage.setSenderBusinessIdentifierCode("SENDER");
		this.swiftMessage.setReceiverBusinessIdentifierCode("RECEIVER");
		Mockito.when(this.swiftMessageService.getSwiftMessage(2345L)).thenReturn(this.swiftMessage);

		SwiftMessageType swiftMessageType = new SwiftMessageType();
		swiftMessageType.setName("202");
		swiftMessageType.setDescription("Swift Message Type");
		swiftMessageType.setMtVersionName("33");
		swiftMessageType.setId((short) 22);
		this.swiftMessage.setType(swiftMessageType);

		SwiftIdentifier swiftIdentifier = new SwiftIdentifier();
		swiftIdentifier.setId((long) 8999);
		swiftIdentifier.setSourceSystemFkFieldId((long) 332);
		SwiftSourceSystemModule swiftSourceSystemModule = new SwiftSourceSystemModule();
		swiftIdentifier.setSourceSystemModule(swiftSourceSystemModule);
		swiftSourceSystemModule.setSourceSystem(new SwiftSourceSystem());
		swiftIdentifier.setUniqueStringIdentifier("123456");

		SwiftMessageStatus completeMessageStatus = new SwiftMessageStatus();
		completeMessageStatus.setId((short) 5);
		completeMessageStatus.setName("COMPLETE");
		completeMessageStatus.setDescription("The message has been successfully processed.");
		Mockito.when(this.swiftMessageService.getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.COMPLETE.name())).thenReturn(completeMessageStatus);

		this.swiftMessage.setIdentifier(swiftIdentifier);
		this.swiftMessage.setIncoming(false);
		this.swiftMessage.setMessageDateAndTime(new Date());
		this.swiftMessage.setText(TEST_INSTRUCTION_MESSAGE);
		this.swiftMessage.setMessageFormat(SwiftMessageFormats.MT);

		SwiftMessageStatus swiftMessageStatus = new SwiftMessageStatus();
		swiftMessageStatus.setId((short) 1);
		swiftMessageStatus.setName(SwiftMessageStatus.SwiftMessageStatusNames.QUEUED.name());
		this.swiftMessage.setMessageStatus(swiftMessageStatus);

		Assertions.assertTrue(this.temp.exists());
		Assertions.assertTrue(this.emissionDirectory.exists());
		Assertions.assertEquals(0, this.emissionDirectory.list().length);

		Mockito.reset(this.swiftServerStatusHandler);
	}


	@AfterEach
	public void afterTest(TestInfo testInfo) {
		if (testStateMap.containsKey(testInfo.getDisplayName())) {
			testStateMap.get(testInfo.getDisplayName()).setEndTime(LocalDateTime.now());
		}
		recursiveDeleteChildren(this.emissionDirectory);
		// reset mocks
		this.swiftEmissionHandlerMonitor.reset();
		Mockito.reset(this.swiftExceptionHandler, this.swiftMessagingErrorService, this.swiftMessageService);
	}


	/**
	 * send message through the gateway
	 * Bypasses Spring Integration by using a file watcher on the emission folder.
	 */
	@Test
	public void testFileProcessing() throws Exception {
		try (WatchService emissionWatch = FileSystems.getDefault().newWatchService()) {
			// actually watch for the delete event, this occurs when the *.writing files are deleted and the copy process is done.
			WatchKey deleteFileKey = this.emissionDirectory.toPath().register(emissionWatch, StandardWatchEventKinds.ENTRY_DELETE);
			this.swiftServerOutgoingFileMessageHandler.send(this.swiftMessage);
			WatchKey returnedKey = emissionWatch.poll(10, TimeUnit.SECONDS);
			Assertions.assertEquals(deleteFileKey, returnedKey);
			List<WatchEvent<?>> deleteEvents = returnedKey.pollEvents();
			Assertions.assertEquals(1, deleteEvents.size());

			Assertions.assertEquals(1, CollectionUtils.createList(this.emissionDirectory.listFiles()).size());
			File emissionResponse = CollectionUtils.createList(this.emissionDirectory.listFiles()).iterator().next();
			Assertions.assertNotNull(emissionResponse);
			Assertions.assertTrue(emissionResponse.getName().startsWith("1"));
			Assertions.assertTrue(emissionResponse.getName().endsWith(".fin"));

			MatcherAssert.assertThat(FileUtils.readFileToString(emissionResponse.getAbsolutePath()), IsEqual.equalTo(TEST_INSTRUCTION_MESSAGE));
			Assertions.assertEquals(1, CollectionUtils.createList(this.emissionDirectory.listFiles()).size());
			String emissionFileName = CollectionUtils.createList(this.emissionDirectory.listFiles()).get(0).getName();
			Assertions.assertTrue(emissionFileName.startsWith("1"));
			Assertions.assertTrue(emissionFileName.endsWith(".fin"));

			// wait long enough to make sure aggregation group times out
			File msg = this.swiftEmissionHandlerMonitor.awaitMessage(4);
			Assertions.assertNotNull(msg);
		}
	}


	/**
	 * throw a {@link RuntimeException}
	 * Make sure the {@link SwiftExceptionHandler} and {@link SwiftMessagingErrorService} were called.
	 * Make sure the message never makes it to handler.
	 */
	@Test
	public void testRuntimeExceptionHandler() throws Exception {
		// Aggregator exceptions go to the exception handler.
		SwiftMessage spied = Mockito.spy(this.swiftMessage);
		AtomicInteger count = new AtomicInteger(0);
		Mockito.when(spied.getText()).thenAnswer(invocation -> {
			if (count.getAndIncrement() < 1) {
				return this.swiftMessage.getText();
			}
			throw new RuntimeException("Expected Exception Was Thrown.");
		});
		this.swiftServerOutgoingFileMessageHandler.send(spied);

		// wait long enough to make sure aggregation group times out
		File emissionResponse = this.swiftEmissionHandlerMonitor.awaitMessage(4);
		Assertions.assertNull(emissionResponse, testStateMap.toString());

		Mockito.verify(this.swiftExceptionHandler, Mockito.times(1)).handleExceptionMessage(ArgumentMatchers.any());
		Mockito.verify(this.swiftMessagingErrorService, Mockito.times(1)).postErrorMessage(ArgumentMatchers.any(SwiftMessagingMessage.class), ArgumentMatchers.any(Throwable.class));
	}


	/**
	 * throw a {@link MessagingException}
	 * Make sure the {@link SwiftExceptionHandler} and {@link SwiftMessagingErrorService} were called.
	 * Make sure the message never makes it to handler.
	 */
	@Test
	public void testSwiftMessageExceptionHandler() throws Exception {
		// Aggregator exceptions go to the exception handler.
		SwiftMessage spied = Mockito.spy(this.swiftMessage);
		AtomicInteger count = new AtomicInteger(1);
		Mockito.when(spied.getMessageFormat()).thenAnswer(invocation -> {
			if (count.getAndIncrement() <= 2) {
				return this.swiftMessage.getMessageFormat();
			}
			throw new MessagingException(MessageBuilder.withPayload(spied).build(), "EXPECTED EXCEPTION", new RuntimeException("Expected Runtime Exception."));
		});
		Mockito.when(this.swiftMessageService.getSwiftMessage(spied.getId())).thenReturn(spied);
		this.swiftServerOutgoingFileMessageHandler.send(spied);

		// wait long enough to make sure aggregation group times out
		File emissionResponse = this.swiftEmissionHandlerMonitor.awaitMessage(4);
		Assertions.assertNull(emissionResponse, testStateMap.toString());

		Mockito.verify(this.swiftExceptionHandler, Mockito.times(1)).handleExceptionMessage(ArgumentMatchers.any());
		Mockito.verify(this.swiftMessagingErrorService, Mockito.never()).postErrorMessage(ArgumentMatchers.any(SwiftMessagingMessage.class), ArgumentMatchers.any(Throwable.class));
		Mockito.verify(this.swiftMessageService, Mockito.times(2)).saveSwiftMessage(spied);
		Mockito.verify(this.swiftServerStatusHandler, Mockito.times(1)).sendSwiftExportStatus(ArgumentMatchers.anyString(), ArgumentMatchers.isNull(), ArgumentMatchers.isNull(), ArgumentMatchers.any(), ArgumentMatchers.isNull());
	}


	/**
	 * throw a {@link MessagingException} within the aggregator
	 * Make sure the {@link SwiftExceptionHandler} and {@link SwiftMessagingErrorService} were called.
	 * Make sure the message never makes it to handler.
	 */
	@Test
	public void testAggregatedSwiftMessageExceptionHandler() throws Exception {
		// Aggregator exceptions go to the exception handler.
		SwiftMessage spied = Mockito.spy(this.swiftMessage);
		AtomicInteger count = new AtomicInteger(1);
		Mockito.when(spied.getMessageFormat()).thenAnswer(invocation -> {
			if (count.getAndIncrement() <= 2) {
				return this.swiftMessage.getMessageFormat();
			}
			AggregatedSwiftMessage aggregatedSwiftMessage = new AggregatedSwiftMessage();
			aggregatedSwiftMessage.setAggregatedMessageList(Arrays.asList(spied.getId(), spied.getId(), spied.getId()));
			throw new MessagingException(MessageBuilder.withPayload(aggregatedSwiftMessage).build(), "EXPECTED EXCEPTION", new RuntimeException("Expected Runtime Exception."));
		});
		Mockito.when(this.swiftMessageService.getSwiftMessage(spied.getId())).thenReturn(spied);
		this.swiftServerOutgoingFileMessageHandler.send(spied);

		// wait long enough to make sure aggregation group times out
		File emissionResponse = this.swiftEmissionHandlerMonitor.awaitMessage(4);
		Assertions.assertNull(emissionResponse, testStateMap.toString());

		Mockito.verify(this.swiftExceptionHandler, Mockito.times(1)).handleExceptionMessage(ArgumentMatchers.any());
		Mockito.verify(this.swiftMessagingErrorService, Mockito.never()).postErrorMessage(ArgumentMatchers.any(SwiftMessagingMessage.class), ArgumentMatchers.any(Throwable.class));
		Mockito.verify(this.swiftMessageService, Mockito.times(4)).saveSwiftMessage(spied);
	}


	/**
	 * throw a {@link MessagingException} within the aggregator
	 * Make sure the {@link SwiftExceptionHandler} and {@link SwiftMessagingErrorService} were called
	 * Make sure the second message makes it to the handler.
	 */
	@Test
	public void testMessageStoreException() throws Exception {
		// message store exceptions do not go to the exception handler.
		SwiftMessage spied = Mockito.spy(this.swiftMessage);
		MatcherAssert.assertThat(spied.getMessageStatus().getName(), IsEqual.equalTo(SwiftMessageStatus.SwiftMessageStatusNames.QUEUED.name()));
		AtomicInteger count = new AtomicInteger(1);
		Mockito.when(spied.getMessageFormat()).thenAnswer(invocation -> {
			if (count.getAndIncrement() <= 1) {
				return this.swiftMessage.getMessageFormat();
			}
			throw new MessagingException(MessageBuilder.withPayload(spied).build(), "EXPECTED EXCEPTION", new RuntimeException("Expected Runtime Exception."));
		});
		Mockito.when(this.swiftMessageService.getSwiftMessage(spied.getId())).thenReturn(spied);
		SwiftMessageStatus swiftMessageStatus = new SwiftMessageStatus();
		swiftMessageStatus.setId((short) 2);
		swiftMessageStatus.setName(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name());
		Mockito.when(this.swiftMessageService.getSwiftMessageStatusByName(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name())).thenReturn(swiftMessageStatus);
		// send two messages the first will throw an exception in message store and rollback, the second will be sent out.
		this.swiftServerOutgoingFileMessageHandler.send(spied);
		this.swiftServerOutgoingFileMessageHandler.send(this.swiftMessage);

		MatcherAssert.assertThat(spied.getMessageStatus().getName(), IsEqual.equalTo(SwiftMessageStatus.SwiftMessageStatusNames.ERROR.name()));

		// wait long enough to make sure aggregation group times out, the second message must make it to the handler.
		File emissionResponse = this.swiftEmissionHandlerMonitor.awaitMessage(4);
		Assertions.assertNotNull(emissionResponse);

		// make sure the message count is '1'.
		Assertions.assertTrue(emissionResponse.getName().startsWith("1"));
		Assertions.assertTrue(emissionResponse.getName().endsWith(".fin"));

		MatcherAssert.assertThat(FileUtils.readFileToString(emissionResponse.getAbsolutePath()), IsEqual.equalTo(TEST_INSTRUCTION_MESSAGE));
		Assertions.assertFalse(CollectionUtils.createList(this.emissionDirectory.listFiles()).isEmpty());
		Assertions.assertEquals(1, CollectionUtils.createList(this.emissionDirectory.listFiles()).size());
		String emissionFileName = CollectionUtils.createList(this.emissionDirectory.listFiles()).get(0).getName();
		Assertions.assertTrue(emissionFileName.startsWith("1"));
		Assertions.assertTrue(emissionFileName.endsWith(".fin"));
	}


	/**
	 * Output a message with local authentication turned on.
	 * Bypasses Spring Integration by using a file watcher on the emission folder.
	 */
	@Test
	public void testLauFileProcessing() throws Exception {
		this.swiftMessageSplitter.setLocalAuthenticationEnabled(true);
		// actually watch for the delete event, this occurs when the *.writing files are deleted and the copy process is done.
		TestFolderWatcher.watchForFolderEvents(
				this.emissionDirectory.toPath(),
				() -> this.swiftServerOutgoingFileMessageHandler.send(this.swiftMessage),
				2,
				Duration.of(20, ChronoUnit.SECONDS),
				(Path path) -> Boolean.TRUE,
				StandardWatchEventKinds.ENTRY_DELETE
		);
		Set<String> fileExtensions = new HashSet<>();

		List<File> files = CollectionUtils.createList(this.emissionDirectory.listFiles());
		Assertions.assertEquals(2, files.size());

		for (File file : files) {
			assertFileContents(file);
			String extension = FileUtils.getFileExtension(file.getName());
			fileExtensions.add(extension);
		}
		Assertions.assertEquals(2, fileExtensions.size());
		fileExtensions.remove("fin");
		fileExtensions.remove("lau");
		Assertions.assertTrue(fileExtensions.isEmpty());

		// wait long enough to make sure aggregation group times out
		this.swiftEmissionHandlerMonitor.awaitMessage(4);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertFileContents(File file) {
		String emissionFileName = file.getName();
		Assertions.assertTrue(emissionFileName.startsWith("1"));
		String extension = FileUtils.getFileExtension(file.getName());
		Assertions.assertTrue("fin".equals(extension) || "lau".equals(extension));
		if ("fin".equals(extension)) {
			MatcherAssert.assertThat(FileUtils.readFileToString(file.getAbsolutePath()), IsEqual.equalTo(TEST_INSTRUCTION_MESSAGE));
		}
		if ("lau".equals(extension)) {
			String signature = new String(SwiftMtAuthenticationUtils.encode(this.key, this.swiftMessage.getText()), StandardCharsets.UTF_8);
			MatcherAssert.assertThat(FileUtils.readFileToString(file.getAbsolutePath()), IsEqual.equalTo(signature));
		}
	}


	private void recursiveDeleteChildren(File file) {
		File[] files = file.listFiles();
		if (files != null) {
			for (File each : files) {
				recursiveDelete(each);
			}
		}
	}


	private void recursiveDelete(File file) {
		recursiveDeleteChildren(file);
		file.delete();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class TestState {

		private File tempFile;
		private String testMethod;
		private LocalDateTime startTime;
		private LocalDateTime endTime;

		private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuuMMdd_HHmmss");


		public TestState(File tempFile, String testMethod) {
			this.tempFile = tempFile;
			this.testMethod = testMethod;
			this.startTime = LocalDateTime.now();
		}


		void setEndTime(LocalDateTime endTime) {
			this.endTime = endTime;
		}


		@Override
		public String toString() {
			return "TestState{" +
					"tempFile='" + this.tempFile.getName() + '\'' +
					"testMethod='" + this.testMethod + '\'' +
					", startTime=" + (this.startTime == null ? "" : this.formatter.format(this.startTime)) +
					", endTime=" + (this.endTime == null ? "" : this.formatter.format(this.endTime)) +
					'}';
		}
	}
}
