package com.clifton.swift.error.handler;

import org.junit.jupiter.api.Assertions;
import org.springframework.messaging.Message;


public class SwiftExceptionHandlerFake implements SwiftExceptionHandler {

	@Override
	public void handleExceptionMessage(Message<Exception> exceptionMessage) {
		Assertions.fail("Did not expect a call to the exception handler.");
	}
}
