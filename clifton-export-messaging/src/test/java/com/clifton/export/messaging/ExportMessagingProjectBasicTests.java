package com.clifton.export.messaging;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ExportMessagingProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "export-messaging";
	}
}
