package com.clifton.export.messaging.message;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ExportMessagingMessageList</code> is an asynchronous message used to
 * send Export message to a Message Broker. e.g ActiveMQ.
 * <p>
 * It is used to send all of the same CONTENT without archiving duplicate files
 */
@MessageType(MessageTypes.XML)
public class ExportMessagingMessageList extends AbstractMessagingMessage {

	/**
	 * The list of messages that the content will be sent along with
	 */
	private List<ExportMessagingMessage> messageList = new ArrayList<>();


	public List<ExportMessagingMessage> getMessageList() {
		return this.messageList;
	}


	public void setMessageList(List<ExportMessagingMessage> messageList) {
		this.messageList = messageList;
	}
}
