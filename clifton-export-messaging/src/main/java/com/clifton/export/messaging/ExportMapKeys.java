package com.clifton.export.messaging;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ftp.FtpProtocols;


/**
 * A central location to store constants for the Map keys which are used to
 * exchange data from IMS and external services e.g. Integration Service.
 * IMS puts the map with these keys, and the accompanying data on a queue, from
 * where Integration Service picks it up and processes the Export of that data.
 */
public enum ExportMapKeys {

	/**
	 * Used by integration tests to specify the SMTP server hostname
	 */
	SMTP_HOSTNAME,

	/**
	 * Used by integration tests to specify the SMTP server port
	 */
	SMTP_PORT,
	/**
	 * Map key for a single Email Subject string as value.
	 */
	MESSAGE_SUBJECT,

	/**
	 * Map key for a single Email body/content string as value.
	 */
	MESSAGE_TEXT,


	EMAIL_FROM_NAME,
	/**
	 * Map key for a single From Email Address string as value.
	 */
	EMAIL_FROM(EMAIL_FROM_NAME) {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.EMAIL_FROM_ADDRESS.name();
		}
	},

	EMAIL_SENDER_NAME,
	/**
	 * Map key for a single Sender Email Address string as value.
	 */
	EMAIL_SENDER(EMAIL_SENDER_NAME) {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.EMAIL_SENDER_ADDRESS.name();
		}
	},

	/**
	 * Map key for multiple TO Email Addresses strings as values, separated by {@link ExportConstants#STRING_DELIMITER}.
	 */
	EMAIL_TO {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.EMAIL_TO.name();
		}
	},

	/**
	 * Map key for multiple CC Email Addresses strings as values, separated by {@link ExportConstants#STRING_DELIMITER}.
	 */
	EMAIL_CC {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.EMAIL_CC.name();
		}
	},

	/**
	 * Map key for multiple BCC Email Addresses strings as values, separated by {@link ExportConstants#STRING_DELIMITER}.
	 */
	EMAIL_BCC {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.EMAIL_BCC.name();
		}
	},

	/**
	 * Map key for a single FAX recipient name string as value.
	 */
	FAX_RECIPIENT_NAME,

	/**
	 * Map key for a single FAX number string as value.
	 */
	FAX_RECIPIENT(FAX_RECIPIENT_NAME) {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.FAX_RECIPIENT.name();
		}
	},

	/**
	 * Number of the FAX sender
	 */
	FAX_SENDER_NUMBER,

	/**
	 * Name of the FAX sender
	 */
	FAX_SENDER_NAME,

	/**
	 * Email address of the FAX sender
	 */
	FAX_SENDER(FAX_SENDER_NAME) {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.FAX_SENDER.name();
		}
	},


	/**
	 * Map key for a single FTP URL string as value.
	 */
	FTP_URL,

	/**
	 * Map key for a single FTP port string as value.
	 */
	FTP_PORT,

	/**
	 * Map key for a single FTP protocol string as value. See {@link FtpProtocols}
	 */
	FTP_PROTOCOL() {
		/**
		 * For FTP the value in the map should be FTP, FTPS or SFTP which is the subtype name.
		 */
		@Override
		public String getSubTypeName(String value) {
			return value;
		}
	},

	/**
	 * Whether FTP connection is Active Mode as opposed to Passive Mode.
	 */
	FTP_ACTIVE_MODE,

	/**
	 * Map key for a single FTP User string as value.
	 */
	FTP_USER,

	/**
	 * Map key for a single FTP Password string as value.
	 */
	FTP_PASSWORD(true),

	/**
	 * Map key for a single FTP Remote Folder string as value.
	 */
	FTP_REMOTE_FOLDER,

	/**
	 * PGP Public key used to encrypt outgoing files.
	 */
	FTP_PGP_PUBLIC_KEY,


	/**
	 * SSH Private Key for logging into an external FTP server
	 */
	FTP_SSH_PRIVATE_KEY(true),


	/**
	 * If enabled, causes PGP to emit cipher text or keys in ASCII Radix-64 format suitable for transporting through E-mail channels.
	 */
	FTP_PGP_ARMOR,

	/**
	 * If enabled, indicates that an integrity check is enabled.
	 */
	FTP_PGP_INTEGRITY_CHECK,

	/**
	 * If populated, this string will be prepended to the filename before being sent
	 */
	FILENAME_PREFIX,
	/**
	 * A network directory to place to exported file.
	 */
	NETWORK_PATH {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.NETWORK_FILE.name();
		}
	},

	/**
	 * Whether a network file should be overwritten it one with the same name already exists.
	 */
	NETWORK_OVERWRITE,

	/**
	 * Whether a network file should uses a .write extension while it is being copied.
	 */
	NETWORK_WRITE_EXTENSION,

	/**
	 * Unique S3 bucket name.
	 */
	AWS_S3_BUCKET{
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.AWS_S3_BUCKET.name();
		}
	},

	/**
	 * AWS region associated with the bucket (i.e. us-east-1).
	 */
	AWS_S3_REGION {
		@Override
		public String getSubTypeName(String value) {
			return ExportDestinationSubTypes.AWS_S3_REGION.name();
		}
	},

	/**
	 * The S3 access key id of a user policy. A user will be associated with each env (sandbox, nonprod, prod).
	 */
	AWS_S3_ACCESS_KEY_ID,

	/**
	 * The S3 secret key of a user policy.
	 */
	AWS_S3_SECRET_KEY(true),

	/**
	 * Used by integration tests to specify the SMTP server hostname
	 */
	AWS_S3_ENDPOINT_OVERRIDE;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String exportTextValueKey;
	private boolean valueEncrypted = false;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ExportMapKeys() {
		//Nothing - default constructor
	}


	ExportMapKeys(ExportMapKeys exportTextValueKey) {
		this.exportTextValueKey = exportTextValueKey.name();
	}


	ExportMapKeys(boolean valueEncrypted) {
		this.valueEncrypted = valueEncrypted;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used by integration to determine the name of the subtype for the specific key.
	 * <p>
	 * NOTE: The key does not represent a sub destination type, it should return null.
	 *
	 * @param value
	 */
	public String getSubTypeName(@SuppressWarnings("unused") String value) {
		return StringUtils.EMPTY_STRING;
	}


	public String getExportTextValueKey() {
		return this.exportTextValueKey;
	}


	public boolean isValueEncrypted() {
		return this.valueEncrypted;
	}
}
