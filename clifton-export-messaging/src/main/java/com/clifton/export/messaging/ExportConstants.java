package com.clifton.export.messaging;


/**
 * A central location to store constants for the {@code clifton-export} project.
 */
public class ExportConstants {

	/**
	 * Private constructor to prevent instantiation.
	 */
	private ExportConstants() {
	}


	/**
	 * The delimiter to be used where strings are to be delimited in the
	 * {@code clifton-export} project.
	 */
	public static final String STRING_DELIMITER = ";";
}
