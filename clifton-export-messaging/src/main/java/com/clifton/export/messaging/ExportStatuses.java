package com.clifton.export.messaging;

/**
 * @author mwacker
 */
public enum ExportStatuses {
	RECEIVED, PROCESSED, REPROCESSED, FAILED
}
