package com.clifton.export.messaging;

import com.clifton.export.messaging.message.ExportMessagingStatusMessage;


/**
 * Defines methods used to process incoming export status messages.
 *
 * @author mwacker
 */
public interface ExportMessagingResponseHandler {

	public void process(ExportMessagingStatusMessage message);


	/**
	 * The name of the system (or section of the system) that generated the outgoing export.
	 * <p>
	 * For example, standard exports in IMS have sourceSystemName = "IMS" and instruction exports have sourceSystemName = "IMS_INSTRUCTION"
	 */
	public String getSourceSystemName();
}
