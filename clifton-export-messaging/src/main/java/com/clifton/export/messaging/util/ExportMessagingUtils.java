package com.clifton.export.messaging.util;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.export.messaging.ExportMapKeys;

import java.util.EnumMap;
import java.util.Map;


/**
 * Utilities for export messaging
 */
public class ExportMessagingUtils {

	public static Map<ExportMapKeys, String> generateFtpDestinationMap(FtpConfig config, String password, String sshPrivateKey) {
		Map<ExportMapKeys, String> result = new EnumMap<>(ExportMapKeys.class);
		result.put(ExportMapKeys.FTP_URL, config.getUrl());
		result.put(ExportMapKeys.FTP_PORT, StringUtils.toNullableString(config.getPort()));
		result.put(ExportMapKeys.FTP_PROTOCOL, config.getFtpProtocol().name());
		result.put(ExportMapKeys.FTP_USER, config.getUserName());
		result.put(ExportMapKeys.FTP_PASSWORD, password);
		result.put(ExportMapKeys.FTP_REMOTE_FOLDER, config.getRemoteFolder());
		result.put(ExportMapKeys.FTP_PGP_PUBLIC_KEY, config.getPgpPublicKey());
		result.put(ExportMapKeys.FTP_SSH_PRIVATE_KEY, sshPrivateKey);
		result.put(ExportMapKeys.FTP_ACTIVE_MODE, StringUtils.toString(config.isActiveFtp()));
		result.put(ExportMapKeys.FTP_PGP_ARMOR, StringUtils.toString(config.getPgpArmor()));
		result.put(ExportMapKeys.FTP_PGP_INTEGRITY_CHECK, StringUtils.toString(config.getPgpIntegrityCheck()));

		//Set message/body for user readability
		result.put(ExportMapKeys.MESSAGE_SUBJECT, "FTP to " + config.getUrl());
		result.put(ExportMapKeys.MESSAGE_TEXT, "FTP to " + config.getUrl());
		result.put(ExportMapKeys.FILENAME_PREFIX, config.getFilenamePrefix());

		return result;
	}
}
