package com.clifton.export.messaging.message;


import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;


/**
 * @author mwacker
 */
public class ExportMessagingDestination implements Serializable {

	private final static long serialVersionUID = 1L;
	private ExportDestinationTypes type;

	private final Map<ExportMapKeys, String> propertyList = new EnumMap<>(ExportMapKeys.class);


	public ExportDestinationTypes getType() {
		return this.type;
	}


	public void setType(ExportDestinationTypes type) {
		this.type = type;
	}


	public Map<ExportMapKeys, String> getPropertyList() {
		return this.propertyList;
	}
}
