package com.clifton.export.messaging.message;


public enum ExportMessagingActions {
	/**
	 * Resend an export.
	 */
	RESEND,
	/**
	 * Send a new export.
	 */
	SEND_NEW
}
