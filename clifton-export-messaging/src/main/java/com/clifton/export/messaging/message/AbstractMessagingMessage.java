package com.clifton.export.messaging.message;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.asynchronous.AbstractAsynchronousMessage;

import java.util.ArrayList;
import java.util.List;


@MessageType(MessageTypes.XML)
public abstract class AbstractMessagingMessage extends AbstractAsynchronousMessage {

	private ExportMessagingActions action = ExportMessagingActions.SEND_NEW;

	private List<ExportMessagingContent> contentList = new ArrayList<>();


	private String baseArchivePath;

	/**
	 * Unique name for the source system
	 */
	private String sourceSystemName;

	/**
	 * A name supplied by the client used to identify the export.
	 */
	private String sourceSystemDefinitionName;

	/**
	 * Supplied by the client. Used to associate the export to a source system.
	 */
	private String sourceSystemIdentifier;


	/**
	 * The archive strategy to be used
	 */
	private String archiveStrategyName;


	public String getSourceSystemDefinitionName() {
		return this.sourceSystemDefinitionName;
	}


	public void setSourceSystemDefinitionName(String sourceSystemDefinitionName) {
		this.sourceSystemDefinitionName = sourceSystemDefinitionName;
	}


	public String getArchiveStrategyName() {
		return this.archiveStrategyName;
	}


	public void setArchiveStrategyName(String archiveStrategyName) {
		this.archiveStrategyName = archiveStrategyName;
	}


	public ExportMessagingActions getAction() {
		return this.action;
	}


	public void setAction(ExportMessagingActions action) {
		this.action = action;
	}


	public List<ExportMessagingContent> getContentList() {
		return this.contentList;
	}


	public void setContentList(List<ExportMessagingContent> contentList) {
		this.contentList = contentList;
	}


	public String getBaseArchivePath() {
		return this.baseArchivePath;
	}


	public void setBaseArchivePath(String baseArchivePath) {
		this.baseArchivePath = baseArchivePath;
	}


	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public String getSourceSystemIdentifier() {
		return this.sourceSystemIdentifier;
	}


	public void setSourceSystemIdentifier(String sourceSystemIdentifier) {
		this.sourceSystemIdentifier = sourceSystemIdentifier;
	}
}
