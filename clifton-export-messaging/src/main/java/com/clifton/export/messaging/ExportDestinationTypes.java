package com.clifton.export.messaging;


/**
 * The types of Export supported by the system. Currently, {@code EMAIL} and
 * {@code FTP} and {@code PHONE}.
 */
public enum ExportDestinationTypes {
	EMAIL(true),
	BY_ROW_EMAIL(true),
	FTP(true),
	PHONE(true),
	NETWORK_FILE(true),
	PORTAL(false),
	AWS_S3(true);

	/**
	 * Denotes if the destination is external (sent via JMS) or internal (not via jms)
	 */
	private boolean externalDestination;


	ExportDestinationTypes(boolean externalDestination) {
		this.externalDestination = externalDestination;
	}


	public boolean isExternalDestination() {
		return this.externalDestination;
	}
}
