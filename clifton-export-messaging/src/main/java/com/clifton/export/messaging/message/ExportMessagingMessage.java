package com.clifton.export.messaging.message;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ExportMessagingMessage</code> is an asynchronous message used to
 * send Export message to a Message Broker. e.g ActiveMQ.
 */
@MessageType(MessageTypes.XML)
public class ExportMessagingMessage extends AbstractMessagingMessage {

	private List<ExportMessagingDestination> destinationList = new ArrayList<>();

	private Integer retryCount;

	private Integer retryDelayInSeconds;


	public Integer getRetryCount() {
		return this.retryCount;
	}


	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}


	public Integer getRetryDelayInSeconds() {
		return this.retryDelayInSeconds;
	}


	public void setRetryDelayInSeconds(Integer retryDelayInSeconds) {
		this.retryDelayInSeconds = retryDelayInSeconds;
	}


	public List<ExportMessagingDestination> getDestinationList() {
		return this.destinationList;
	}


	public void setDestinationList(List<ExportMessagingDestination> destinationList) {
		this.destinationList = destinationList;
	}
}
