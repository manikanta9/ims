package com.clifton.export.messaging.message;


import java.io.Serializable;


public class ExportMessagingContent implements Serializable {

	private final static long serialVersionUID = 1L;

	/**
	 * Relative path
	 */
	private String fileNameWithPath;
	private String password;
	private boolean deleteSourceFile;


	public String getFileNameWithPath() {
		return this.fileNameWithPath;
	}


	public void setFileNameWithPath(String fileNameWithPath) {
		this.fileNameWithPath = fileNameWithPath;
	}


	public String getPassword() {
		return this.password;
	}


	public void setPassword(String fileNameWithPath) {
		this.password = fileNameWithPath;
	}


	public boolean isDeleteSourceFile() {
		return this.deleteSourceFile;
	}


	public void setDeleteSourceFile(boolean deleteSourceFile) {
		this.deleteSourceFile = deleteSourceFile;
	}
}
