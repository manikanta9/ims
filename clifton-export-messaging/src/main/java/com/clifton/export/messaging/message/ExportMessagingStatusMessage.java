package com.clifton.export.messaging.message;

import com.clifton.core.messaging.asynchronous.AbstractAsynchronousMessage;
import com.clifton.export.messaging.ExportStatuses;


/**
 * @author mwacker
 */
public class ExportMessagingStatusMessage extends AbstractAsynchronousMessage {

	/**
	 * Supplied by the client. Used to associate the export to a source system.
	 */
	private String sourceSystemIdentifier;
	private ExportStatuses status;
	private String message;
	/**
	 * Unique name for the source system
	 */
	private String sourceSystemName;


	public String getSourceSystemIdentifier() {
		return this.sourceSystemIdentifier;
	}


	public void setSourceSystemIdentifier(String sourceSystemIdentifier) {
		this.sourceSystemIdentifier = sourceSystemIdentifier;
	}


	public ExportStatuses getStatus() {
		return this.status;
	}


	public void setStatus(ExportStatuses status) {
		this.status = status;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}
}
