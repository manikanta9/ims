package com.clifton.performance.account.process;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountProcessService</code> ...
 *
 * @author manderson
 */
public interface PerformanceInvestmentAccountProcessService {

	/////////////////////////////////////////////////////////////////////////////
	//////   PerformanceInvestmentAccount Processing Methods         ////
	/////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("result")
	public String processPerformanceInvestmentAccountList(final PerformanceInvestmentAccountListCommand command);


	/**
	 * Allow re-processing the performance account (optionally specific processing type)
	 * Examples: Re-Process the PerformanceInvestmentAccount in full, or Re-Process Benchmarks Only, or Recalculate History (For Upload Support)
	 */
	public void processPerformanceInvestmentAccount(int performanceAccountId);


	/**
	 * Re-processes and returns the PerformanceInvestmentAccount, however nothing is actually saved
	 */
	public PerformanceInvestmentAccount previewPerformanceInvestmentAccount(int performanceAccountId);


	/**
	 * Re-processes historical (QTD, YTD, ITD) and returns the PerformanceInvestmentAccount, however nothing is actually saved
	 */
	public PerformanceInvestmentAccount previewPerformanceInvestmentAccountHistoricalOnly(int performanceAccountId);


	/**
	 * TEMPORARY METHOD TO BACK FILL ITD CUMULATIVE RETURNS
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void processPerformanceInvestmentAccountHistoricalMissingCumulative();


	/**
	 * Allow re-processing the performance account and optionally for a specific date, and re-calculates monthly totals
	 * Used for cases where overrides can be set for a specific day, but we don't want to re-process the entire month again
	 * just the date that changes and how it affects monthly total.
	 * <p>
	 * This is not exposed to UI and called through code when something else occurs - i.e. add daily overrides
	 */
	@DoNotAddRequestMapping
	public void processPerformanceInvestmentAccountForDate(PerformanceInvestmentAccount performanceAccount, Date date);


	/**
	 * Allows recalculating historical (QTD, YTD, ITD) returns only.  Called from code only and doesn't perform any actual processing
	 * Used for Uploads where data is manually entered - including MTD return, but we need to then re-calc historical returns
	 * <p>
	 * NOTE: Any processing will always recalculate these
	 */
	@DoNotAddRequestMapping
	public void recalculatePerformanceInvestmentAccountHistoricalReturns(PerformanceInvestmentAccount performanceAccount);


	/**
	 * Runs warnings for the performance account
	 */
	@SecureMethod(dtoClass = PerformanceInvestmentAccount.class)
	public void processPerformanceInvestmentAccountWarnings(int performanceAccountId);


	public void deletePerformanceInvestmentAccount(int id);
}
