package com.clifton.performance.account.securitytarget.process.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.performance.account.process.calculators.PerformanceInvestmentAccountCalculator;
import com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetDailyReturn;
import com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetService;
import com.clifton.performance.account.securitytarget.process.PerformanceInvestmentAccountSecurityTargetProcessConfig;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountSecurityTargetDailyReturnCalculator</code> populates the {@link com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetDailyReturn} for each day
 * for the {@link com.clifton.performance.account.PerformanceInvestmentAccount} based on {@link com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget} in the system for the account
 * <p/>
 *
 * @author manderson
 */
@Component
public class PerformanceInvestmentAccountSecurityTargetDailyReturnCalculator implements PerformanceInvestmentAccountCalculator<PerformanceInvestmentAccountSecurityTargetProcessConfig> {

	// Warning: Needs to be addressed (Outlined on WIKI page: https://wiki.paraport.com/display/Risk/OTI+-+What+do+we+have+left)
	// CAN'T PULL FROM INVOICES BECAUSE OF DEPENDENCIES IF WE POST TO GL THE FEES THEN WE CAN USE THAT
	private AccountingPeriodService accountingPeriodService;

	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	private InvestmentSecurityEventService investmentSecurityEventService;
	private InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService;

	private MarketDataRetriever marketDataRetriever;

	private PerformanceInvestmentAccountSecurityTargetService performanceInvestmentAccountSecurityTargetService;

	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(PerformanceInvestmentAccountSecurityTargetProcessConfig config, Date date) {
		if (date == null) {
			config.setPerformanceSecurityTargetDailyReturnList(generatePerformanceSecurityTargetDailyReturnList(config));
		}
		else {
			// Do Nothing - Doesn't apply here unless we add override fields
		}
	}


	@Override
	public boolean calculateHistoricalReturns(@SuppressWarnings("unused") PerformanceInvestmentAccountSecurityTargetProcessConfig config) {
		// DO NOTHING - NO HISTORICAL RETURNS AT DAILY DETAIL LEVEL
		return false;
	}


	@Override
	public void saveResults(PerformanceInvestmentAccountSecurityTargetProcessConfig config, Date date) {
		if (date == null) {
			getPerformanceInvestmentAccountSecurityTargetService().savePerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(config.getPerformanceAccount().getId(), config.getPerformanceSecurityTargetDailyReturnList());
		}
		// If only editing one date - then nothing was actually processed at this point so nothing to save
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<PerformanceInvestmentAccountSecurityTargetDailyReturn> generatePerformanceSecurityTargetDailyReturnList(PerformanceInvestmentAccountSecurityTargetProcessConfig config) {
		List<PerformanceInvestmentAccountSecurityTargetDailyReturn> newList = new ArrayList<>();
		List<PerformanceInvestmentAccountSecurityTargetDailyReturn> existingList = getPerformanceInvestmentAccountSecurityTargetService().getPerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(config.getPerformanceAccount().getId());
		Map<String, PerformanceInvestmentAccountSecurityTargetDailyReturn> existingMap = BeanUtils.getBeanMap(existingList, securityTargetDailyReturn -> DateUtils.fromDateShort(securityTargetDailyReturn.getMeasureDate()));

		Date date = config.getStartDate();
		// If the account started mid-month move start date forward
		if (DateUtils.isDateBetween(config.getPerformanceInceptionDate(), config.getStartDate(), config.getEndDate(), false)) {
			date = config.getPerformanceInceptionDate();
			Date previousDate = DateUtils.getPreviousWeekday(date);
			// When starting mid month we need the previous day's value
			// We don't add it to the list of results since we just care about the security target market value for calculations
			generatePerformanceSecurityTargetInitialPreviousValue(config, previousDate, existingMap, newList);
		}
		if (!DateUtils.isWeekday(date)) {
			date = DateUtils.getNextWeekday(date);
		}
		Date endDate = config.getEndDate();


		while (DateUtils.compare(date, endDate, false) <= 0) {
			PerformanceInvestmentAccountSecurityTargetDailyReturn targetDailyReturn = generatePerformanceSecurityTargetDailyReturnForDate(config, date, existingMap);
			newList.add(targetDailyReturn);

			// Move to Next Weekday
			date = DateUtils.getNextWeekday(date);
		}

		return newList;
	}


	/**
	 * For inception date performance will use the inception date's target with previous day's price to generate and save the initial security target market value - return that initial value
	 * Else returns the target for the last weekday of the previous month but doesn't save it on the performance
	 */
	private BigDecimal generatePerformanceSecurityTargetInitialPreviousValue(PerformanceInvestmentAccountSecurityTargetProcessConfig config, Date previousDate, Map<String, PerformanceInvestmentAccountSecurityTargetDailyReturn> existingMap, List<PerformanceInvestmentAccountSecurityTargetDailyReturn> newList) {
		BigDecimal previousValue = BigDecimal.ZERO;

		Date targetDate = previousDate;
		if (DateUtils.isDateBefore(previousDate, config.getPerformanceInceptionDate(), false)) {
			targetDate = config.getPerformanceInceptionDate();
		}

		for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(getInvestmentAccountSecurityTargetList(config, targetDate))) {
			// Target Values
			TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDateWithPriceDate(securityTarget, targetDate, previousDate);
			previousValue = MathUtils.add(previousValue, tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs());
		}

		if (DateUtils.isDateBetween(previousDate, config.getStartDate(), config.getEndDate(), false)) {
			PerformanceInvestmentAccountSecurityTargetDailyReturn performanceDaily = new PerformanceInvestmentAccountSecurityTargetDailyReturn();
			performanceDaily.setPerformanceInvestmentAccount(config.getPerformanceAccount());
			performanceDaily.setMeasureDate(previousDate);

			// If it already exists - keep the id so we "update" instead of delete and insert
			if (existingMap.get(DateUtils.fromDateShort(previousDate)) != null) {
				performanceDaily.setId(existingMap.get(DateUtils.fromDateShort(previousDate)).getId());
			}
			performanceDaily.setSecurityTargetMarketValue(previousValue);
			newList.add(performanceDaily);
		}
		return previousValue;
	}


	private PerformanceInvestmentAccountSecurityTargetDailyReturn generatePerformanceSecurityTargetDailyReturnForDate(PerformanceInvestmentAccountSecurityTargetProcessConfig config, Date date, Map<String, PerformanceInvestmentAccountSecurityTargetDailyReturn> existingMap) {
		PerformanceInvestmentAccountSecurityTargetDailyReturn performanceDaily = new PerformanceInvestmentAccountSecurityTargetDailyReturn();
		performanceDaily.setPerformanceInvestmentAccount(config.getPerformanceAccount());
		performanceDaily.setMeasureDate(date);

		// If it already exists - keep the id so we "update" instead of delete and insert
		if (existingMap.get(DateUtils.fromDateShort(date)) != null) {
			performanceDaily.setId(existingMap.get(DateUtils.fromDateShort(date)).getId());
		}

		if (DateUtils.isEqualWithoutTime(date, DateUtils.getLastWeekdayOfMonth(config.getEndDate()))) {
			AccountingPeriodClosing periodClosing = getAccountingPeriodService().getAccountingPeriodClosingByPeriodAndAccount(config.getPerformanceAccount().getAccountingPeriod().getId(), config.getPerformanceAccount().getClientAccount().getId());
			if (periodClosing != null) {
				// TODO: These all use monthly invoices so this will match for now but should do this a better way
				// Uses Gross Revenue Only
				performanceDaily.setManagementFee(periodClosing.getPeriodProjectedRevenueTotal());
			}
		}

		// Get the Security Targets
		BigDecimal previousValue = BigDecimal.ZERO;
		// In case there is a target change on EX date and we are adding in Divs we need to track the Div return separately.  See: PERFORMANC-324
		BigDecimal dividendAmount = BigDecimal.ZERO;
		BigDecimal dividendDenominator = BigDecimal.ZERO;
		Date previousDate = DateUtils.getPreviousWeekday(date);
		for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(getInvestmentAccountSecurityTargetList(config, date))) {
			// Target Values
			TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDate(securityTarget, date);
			performanceDaily.setSecurityTargetMarketValue(MathUtils.add(performanceDaily.getSecurityTargetMarketValue(), tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs()));
			// If Calculating Using Units: Calculate Today's Target Using Previous Day's price to determine gain/loss amount
			// Previous Value is used to Calculate Daily Returns
			TradeSecurityTarget previousTradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDateWithPriceDate(securityTarget, date, previousDate);
			previousValue = MathUtils.add(previousValue, previousTradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs());

			if (!config.isDoNotCalculateBenchmarkReturn()) {
				if (!MathUtils.isNullOrZero(securityTarget.getTargetUnderlyingQuantity())) {
					performanceDaily.setSecurityTargetGainLoss(MathUtils.add(performanceDaily.getSecurityTargetGainLoss(), MathUtils.subtract(tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs(), previousTradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs())));
					// Add Cash Div Amount on Ex Date to Gain/Loss

					BigDecimal targetDivAmount = getCashDividendAmountForSecurityTargetAndExDate(config, securityTarget, date);
					if (!MathUtils.isNullOrZero(targetDivAmount)) {
						// If target changed today, track div amount and return separately (different denominator - previous day's target at previous day's price)
						if (DateUtils.isEqualWithoutTime(securityTarget.getStartDate(), date)) {
							dividendAmount = MathUtils.add(dividendAmount, targetDivAmount);
							dividendDenominator = MathUtils.add(dividendDenominator, getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDate(getInvestmentAccountSecurityTargetForSecurityAndDate(config, securityTarget.getTargetUnderlyingSecurity(), previousDate), previousDate).getTargetUnderlyingNotionalAdjustedAbs());
						}
						// Else NO change in target today, just add it to the gain/loss amount - denominator is the same
						else {
							performanceDaily.setSecurityTargetGainLoss(MathUtils.add(performanceDaily.getSecurityTargetGainLoss(), targetDivAmount));
						}
					}
				}
				else {
					// If date before today, we should NOT do a flexible price lookup
					boolean flexible = !(DateUtils.isDateBefore(date, new Date(), false));
					BigDecimal benchmarkReturn = (flexible ? getMarketDataRetriever().getInvestmentSecurityReturnFlexible(securityTarget.getClientInvestmentAccount(), securityTarget.getTargetUnderlyingSecurity(), previousDate, date, null, false) : getMarketDataRetriever().getInvestmentSecurityReturn(securityTarget.getClientInvestmentAccount(), securityTarget.getTargetUnderlyingSecurity(), previousDate, date, null, false));
					performanceDaily.setSecurityTargetGainLoss(MathUtils.add(performanceDaily.getSecurityTargetGainLoss(), MathUtils.getPercentageOf(benchmarkReturn, previousTradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs())));
				}
			}
			else {
				// Set gain loss to zero so returns are still properly calculated for the portfolio ( we add target to options and NULL causes issues, so better to default to 0)
				performanceDaily.setSecurityTargetGainLoss(BigDecimal.ZERO);
			}
		}

		// Option Values
		List<AccountingPositionDaily> positionDailyList = config.getPositionListForDate(date);

		// If it's the last weekday of the month, but not the last day - include gain/loss for the last day (rare and likely not applied to options) but some
		// securities have events that occur that incur gain/loss on the last day of the month
		// But we don't want to double count market value
		boolean checkDateForMarketValue = (DateUtils.isEqual(date, DateUtils.getLastWeekdayOfMonth(date)) && !DateUtils.isEqual(date, DateUtils.getLastDayOfMonth(date)));

		for (AccountingPositionDaily accountingPositionDaily : CollectionUtils.getIterable(positionDailyList)) {
			if (InvestmentUtils.isOption(accountingPositionDaily.getInvestmentSecurity())) {
				if (!checkDateForMarketValue || DateUtils.isEqual(accountingPositionDaily.getPositionDate(), date)) {
					performanceDaily.setOptionsMarketValue(MathUtils.add(performanceDaily.getOptionsMarketValue(), accountingPositionDaily.getMarketValueBase()));
				}
				performanceDaily.setOptionsGainLoss(MathUtils.add(performanceDaily.getOptionsGainLoss(), accountingPositionDaily.getDailyGainLossBase()));
			}
		}

		// Calculate Returns (Security Target Gain / Loss divided by Previous Value) + (Div Amount / Div Denominator)
		performanceDaily.setSecurityTargetReturn(MathUtils.add(CoreMathUtils.getPercentValue(performanceDaily.getSecurityTargetGainLoss(), previousValue, true), CoreMathUtils.getPercentValue(dividendAmount, dividendDenominator, true)));
		// Put Div Amount into Security Target Gain/Loss
		performanceDaily.setSecurityTargetGainLoss(MathUtils.add(performanceDaily.getSecurityTargetGainLoss(), dividendAmount));
		performanceDaily.setOptionsGrossReturn(CoreMathUtils.getPercentValue(performanceDaily.getOptionsGainLoss(), previousValue, true));
		performanceDaily.setOptionsNetReturn(CoreMathUtils.getPercentValue(MathUtils.subtract(performanceDaily.getOptionsGainLoss(), performanceDaily.getManagementFee()), previousValue, true));
		return performanceDaily;
	}


	private List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(PerformanceInvestmentAccountSecurityTargetProcessConfig config, Date date) {
		List<InvestmentAccountSecurityTarget> securityTargetList = config.getAccountSecurityTargetList(getInvestmentAccountSecurityTargetService());
		return BeanUtils.filter(securityTargetList, securityTarget -> DateUtils.isDateBetween(date, securityTarget.getStartDate(), securityTarget.getEndDate(), false));
	}


	private InvestmentAccountSecurityTarget getInvestmentAccountSecurityTargetForSecurityAndDate(PerformanceInvestmentAccountSecurityTargetProcessConfig config, InvestmentSecurity investmentSecurity, Date date) {
		List<InvestmentAccountSecurityTarget> dateTargetList = getInvestmentAccountSecurityTargetList(config, date);
		return CollectionUtils.getOnlyElement(BeanUtils.filter(dateTargetList, dateTarget -> CompareUtils.isEqual(investmentSecurity, dateTarget.getTargetUnderlyingSecurity())));
	}


	private BigDecimal getCashDividendAmountForSecurityTargetAndExDate(PerformanceInvestmentAccountSecurityTargetProcessConfig config, InvestmentAccountSecurityTarget securityTarget, Date date) {
		BigDecimal divAmount = BigDecimal.ZERO;
		if (!MathUtils.isNullOrZero(securityTarget.getTargetUnderlyingQuantity())) {
			List<InvestmentSecurityEvent> eventList = config.getInvestmentSecurityEventList(securityTarget.getTargetUnderlyingSecurity(), date, InvestmentSecurityEventType.CASH_DIVIDEND_PAYMENT, getInvestmentSecurityEventService());
			for (InvestmentSecurityEvent event : CollectionUtils.getIterable(eventList)) {

				// If we have events, then confirm quantity
				BigDecimal quantity = securityTarget.getTargetUnderlyingQuantity();
				// If security target STARTS on Ex Date - get previous quantity. We calculate divs on quantity on the day before EX Date (matches booking rules)
				if (DateUtils.isEqualWithoutTime(date, securityTarget.getStartDate())) {
					InvestmentAccountSecurityTarget previousTarget = getInvestmentAccountSecurityTargetForSecurityAndDate(config, securityTarget.getTargetUnderlyingSecurity(), event.getDayBeforeExDate());
					quantity = previousTarget == null ? BigDecimal.ZERO : previousTarget.getTargetUnderlyingQuantity();
				}
				if (!MathUtils.isNullOrZero(quantity)) {
					List<InvestmentSecurityEventPayout> payoutList = config.getInvestmentSecurityEventPayoutListForEvent(event, getInvestmentSecurityEventPayoutService());
					if (!CollectionUtils.isEmpty(payoutList)) {
						for (InvestmentSecurityEventPayout payout : payoutList) {
							divAmount = MathUtils.add(divAmount, MathUtils.multiply(payout.getAfterEventValue(), quantity));
						}
					}
					else {
						divAmount = MathUtils.add(divAmount, MathUtils.multiply(event.getAfterEventValue(), quantity));
					}
				}
			}
		}
		return divAmount;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////               Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public InvestmentSecurityEventPayoutService getInvestmentSecurityEventPayoutService() {
		return this.investmentSecurityEventPayoutService;
	}


	public void setInvestmentSecurityEventPayoutService(InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		this.investmentSecurityEventPayoutService = investmentSecurityEventPayoutService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public PerformanceInvestmentAccountSecurityTargetService getPerformanceInvestmentAccountSecurityTargetService() {
		return this.performanceInvestmentAccountSecurityTargetService;
	}


	public void setPerformanceInvestmentAccountSecurityTargetService(PerformanceInvestmentAccountSecurityTargetService performanceInvestmentAccountSecurityTargetService) {
		this.performanceInvestmentAccountSecurityTargetService = performanceInvestmentAccountSecurityTargetService;
	}


	public TradeSecurityTargetUtilHandler getTradeSecurityTargetUtilHandler() {
		return this.tradeSecurityTargetUtilHandler;
	}


	public void setTradeSecurityTargetUtilHandler(TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler) {
		this.tradeSecurityTargetUtilHandler = tradeSecurityTargetUtilHandler;
	}
}
