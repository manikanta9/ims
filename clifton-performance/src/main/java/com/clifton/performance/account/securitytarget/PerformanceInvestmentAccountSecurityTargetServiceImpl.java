package com.clifton.performance.account.securitytarget;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author manderson
 */
@Service
public class PerformanceInvestmentAccountSecurityTargetServiceImpl implements PerformanceInvestmentAccountSecurityTargetService {

	private AdvancedUpdatableDAO<PerformanceInvestmentAccountSecurityTargetDailyReturn, Criteria> performanceInvestmentAccountSecurityTargetDailyReturnDAO;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceInvestmentAccountSecurityTargetDailyReturn getPerformanceInvestmentAccountSecurityTargetDailyReturn(int id) {
		return getPerformanceInvestmentAccountSecurityTargetDailyReturnDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceInvestmentAccountSecurityTargetDailyReturn> getPerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(int performanceInvestmentAccountId) {
		return getPerformanceInvestmentAccountSecurityTargetDailyReturnDAO().findByField("performanceInvestmentAccount.id", performanceInvestmentAccountId);
	}


	@Override
	public List<PerformanceInvestmentAccountSecurityTargetDailyReturn> getPerformanceInvestmentAccountSecurityTargetDailyReturnList(PerformanceInvestmentAccountSecurityTargetDailyReturnSearchForm searchForm) {
		return getPerformanceInvestmentAccountSecurityTargetDailyReturnDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Transactional
	@Override
	public void savePerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(int performanceInvestmentAccountId, List<PerformanceInvestmentAccountSecurityTargetDailyReturn> saveList) {
		getPerformanceInvestmentAccountSecurityTargetDailyReturnDAO().saveList(saveList, getPerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(performanceInvestmentAccountId));
	}


	@Transactional
	@Override
	public void deletePerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(int performanceInvestmentAccountId) {
		getPerformanceInvestmentAccountSecurityTargetDailyReturnDAO().deleteList(getPerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(performanceInvestmentAccountId));
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods            ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformanceInvestmentAccountSecurityTargetDailyReturn, Criteria> getPerformanceInvestmentAccountSecurityTargetDailyReturnDAO() {
		return this.performanceInvestmentAccountSecurityTargetDailyReturnDAO;
	}


	public void setPerformanceInvestmentAccountSecurityTargetDailyReturnDAO(AdvancedUpdatableDAO<PerformanceInvestmentAccountSecurityTargetDailyReturn, Criteria> performanceInvestmentAccountSecurityTargetDailyReturnDAO) {
		this.performanceInvestmentAccountSecurityTargetDailyReturnDAO = performanceInvestmentAccountSecurityTargetDailyReturnDAO;
	}
}
