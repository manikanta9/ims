package com.clifton.performance.account.process.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BasePerformanceInvestmentAccountCalculator</code> populates
 * the QTD, YTD, and ITD values on the {@link PerformanceInvestmentAccount} bean
 * <p>
 * This class is abstract and should be extended by the calculator that populates the MTD values
 * then calls the historical methods to fill in history
 *
 * @author manderson
 */
public abstract class BasePerformanceInvestmentAccountCalculator<T extends PerformanceInvestmentAccountConfig> implements PerformanceInvestmentAccountCalculator<T> {

	public static final String PORTFOLIO_RETURN_PREFIX = "PORTFOLIO_RETURN_";
	public static final String QUARTER_TO_DATE = "QTD";
	public static final String YEAR_TO_DATE = "YTD";
	public static final String INCEPTION_TO_DATE = "ITD";


	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;
	private PerformanceInvestmentAccountService performanceInvestmentAccountService;


	@Override
	public void calculate(T config, Date date) {
		recalculateAccountReturns(config);
		calculateHistoricalReturns(config);
	}


	@Override
	public boolean calculateHistoricalReturns(T config) {
		recalculateHistoricalSummaryReturns(config);
		return true;
	}


	@Override
	public void saveResults(T config, Date date) {
		getPerformanceInvestmentAccountService().savePerformanceInvestmentAccount(config.getPerformanceAccount());
	}


	/**
	 * Override for specific MTD calculations
	 */
	protected abstract void recalculateAccountReturns(T config);


	private void recalculateHistoricalSummaryReturns(T config) {
		PerformanceInvestmentAccount bean = config.getPerformanceAccount();
		Map<String, List<PerformanceInvestmentAccount>> historicalMap = getPerformanceInvestmentAccountHistoricalMap(bean, config);

		bean.setQuarterToDateReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(QUARTER_TO_DATE), PerformanceInvestmentAccount::getMonthToDateReturn, true));
		bean.setQuarterToDateBenchmarkReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(QUARTER_TO_DATE), PerformanceInvestmentAccount::getMonthToDateBenchmarkReturn, true));
		if (historicalMap.containsKey(PORTFOLIO_RETURN_PREFIX + QUARTER_TO_DATE)) {
			bean.setQuarterToDatePortfolioReturn(CoreMathUtils.getTotalPercentChange(
					historicalMap.get(PORTFOLIO_RETURN_PREFIX + QUARTER_TO_DATE), PerformanceInvestmentAccount::getMonthToDatePortfolioReturn, true));
		}
		else {
			bean.setQuarterToDatePortfolioReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(QUARTER_TO_DATE), PerformanceInvestmentAccount::getMonthToDatePortfolioReturn, true));
		}

		bean.setYearToDateReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(YEAR_TO_DATE), PerformanceInvestmentAccount::getMonthToDateReturn, true));
		bean.setYearToDateBenchmarkReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(YEAR_TO_DATE), PerformanceInvestmentAccount::getMonthToDateBenchmarkReturn, true));
		if (historicalMap.containsKey(PORTFOLIO_RETURN_PREFIX + YEAR_TO_DATE)) {
			bean.setYearToDatePortfolioReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(PORTFOLIO_RETURN_PREFIX + YEAR_TO_DATE), PerformanceInvestmentAccount::getMonthToDatePortfolioReturn,
					true));
		}
		else {
			bean.setYearToDatePortfolioReturn(CoreMathUtils.getTotalPercentChange(historicalMap.get(YEAR_TO_DATE), PerformanceInvestmentAccount::getMonthToDatePortfolioReturn, true));
		}

		// Inception To Date Returns
		List<PerformanceInvestmentAccount> inceptionSummaries = historicalMap.get(INCEPTION_TO_DATE);
		Date inceptionDate = config.getPerformanceInceptionDate();

		List<PerformanceInvestmentAccount> portfolioReturnInceptionSummaries = inceptionSummaries;
		Date portfolioInceptionDate = inceptionDate;
		if (historicalMap.containsKey(PORTFOLIO_RETURN_PREFIX + INCEPTION_TO_DATE)) {
			portfolioReturnInceptionSummaries = historicalMap.get(PORTFOLIO_RETURN_PREFIX + INCEPTION_TO_DATE);
			portfolioInceptionDate = (Date) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(bean.getClientAccount(),
					PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_PORTFOLIO_RETURN_INCEPTION_OVERRIDE);
		}

		// We only annualize if we have over 12 months of returns - if not annualized, then annualized return = cumulative return
		BigDecimal inceptionToDateReturn = CoreMathUtils.getTotalPercentChange(inceptionSummaries, PerformanceInvestmentAccount::getMonthToDateReturn, true);
		BigDecimal inceptionToDateAnnualizedReturn = (isAnnualizeReturn(inceptionSummaries) ? getAnnualizedDailyReturn("Account Return", inceptionToDateReturn, inceptionDate, bean.getMeasureDate()) : inceptionToDateReturn);
		BigDecimal inceptionToDateBenchmarkReturn = CoreMathUtils.getTotalPercentChange(inceptionSummaries, PerformanceInvestmentAccount::getMonthToDateBenchmarkReturn, true);
		BigDecimal inceptionToDateBenchmarkAnnualizedReturn = (isAnnualizeReturn(inceptionSummaries) ? getAnnualizedDailyReturn("Benchmark Return", inceptionToDateBenchmarkReturn, inceptionDate, bean.getMeasureDate()) : inceptionToDateBenchmarkReturn);
		BigDecimal inceptionToDatePortfolioReturn = CoreMathUtils.getTotalPercentChange(portfolioReturnInceptionSummaries, PerformanceInvestmentAccount::getMonthToDatePortfolioReturn, true);
		BigDecimal inceptionToDatePortfolioAnnualizedReturn = (isAnnualizeReturn(portfolioReturnInceptionSummaries) ? getAnnualizedDailyReturn("Portfolio Return", inceptionToDatePortfolioReturn, portfolioInceptionDate, bean.getMeasureDate()) : inceptionToDatePortfolioReturn);

		bean.setInceptionToDateReturnCumulative(inceptionToDateReturn);
		bean.setInceptionToDateReturn(inceptionToDateAnnualizedReturn);

		bean.setInceptionToDateBenchmarkReturnCumulative(inceptionToDateBenchmarkReturn);
		bean.setInceptionToDateBenchmarkReturn(inceptionToDateBenchmarkAnnualizedReturn);

		bean.setInceptionToDatePortfolioReturnCumulative(inceptionToDatePortfolioReturn);
		bean.setInceptionToDatePortfolioReturn(inceptionToDatePortfolioAnnualizedReturn);

		config.setPerformanceAccount(bean);
	}


	private boolean isAnnualizeReturn(List<PerformanceInvestmentAccount> inceptionSummaries) {
		return CollectionUtils.getSize(inceptionSummaries) > 12;
	}


	private BigDecimal getAnnualizedDailyReturn(String propertyLabel, BigDecimal cumulativeReturn, Date startDate, Date endDate) {
		try {
			return CoreMathUtils.annualizeByDateRange(cumulativeReturn, startDate, endDate, true, true);
		}
		catch (Throwable e) {
			String msg = ExceptionUtils.getOriginalMessage(e);
			if (StringUtils.isEmpty(msg)) {
				msg = e.getMessage();
				if (StringUtils.isEmpty(msg)) {
					msg = ExceptionUtils.getOriginalException(e).getClass().getName();
				}
			}
			msg = "Error calculating annualized return for " + propertyLabel + ": " + msg;
			LogUtils.errorOrInfo(getClass(), msg, e);
			throw new ValidationException(msg, e);
		}
	}


	private Map<String, List<PerformanceInvestmentAccount>> getPerformanceInvestmentAccountHistoricalMap(PerformanceInvestmentAccount summary, T config) {
		// Get Full Historical List
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		searchForm.setClientAccountId(summary.getClientAccount().getId());
		searchForm.setEndMeasureDate(summary.getAccountingPeriod().getStartDate());

		// See if there is an account return override
		Date portfolioReturnInceptionDate = (Date) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(summary.getClientAccount(),
				PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_PORTFOLIO_RETURN_INCEPTION_OVERRIDE);

		boolean useDifferentPortfolioReturnDate = false;
		if (portfolioReturnInceptionDate != null) {
			useDifferentPortfolioReturnDate = true;
			if (config.getPerformanceInceptionDate() != null) {
				// If both set, filter on the earliest one
				if (DateUtils.compare(portfolioReturnInceptionDate, config.getPerformanceInceptionDate(), false) < 0) {
					searchForm.setStartMeasureDate(portfolioReturnInceptionDate);
				}
				else {
					searchForm.setStartMeasureDate(config.getPerformanceInceptionDate());
				}
			}
		}
		else if (config.getPerformanceInceptionDate() != null) {
			searchForm.setStartMeasureDate(config.getPerformanceInceptionDate());
		}

		Map<String, List<PerformanceInvestmentAccount>> historicalSummaryMap = new HashMap<>();
		List<PerformanceInvestmentAccount> historicalList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);
		parseHistoricalSummaryList(historicalSummaryMap, historicalList, summary, (useDifferentPortfolioReturnDate ? config.getPerformanceInceptionDate() : null), "");
		if (useDifferentPortfolioReturnDate) {
			parseHistoricalSummaryList(historicalSummaryMap, historicalList, summary, portfolioReturnInceptionDate, PORTFOLIO_RETURN_PREFIX);
		}
		return historicalSummaryMap;
	}


	/**
	 * Populates the historicalSummaryMap with QTD, YTD, and ITD summaries to be used. Because some cases the account return inception to date can be different,
	 * allows a prefix, so the method that uses this map should know to check for the key with the Account Return Prefix and if it doesn't exist, then use the
	 * default keys
	 */
	@DoNotAddRequestMapping
	public void parseHistoricalSummaryList(Map<String, List<PerformanceInvestmentAccount>> historicalSummaryMap, List<PerformanceInvestmentAccount> historicalList,
	                                       PerformanceInvestmentAccount currentSummary, Date filterInceptionDate, String mapKeyPrefix) {
		List<PerformanceInvestmentAccount> quarterSummaries = new ArrayList<>();
		List<PerformanceInvestmentAccount> yearSummaries = new ArrayList<>();
		List<PerformanceInvestmentAccount> inceptionSummaries = new ArrayList<>();

		Date endDate = currentSummary.getMeasureDate();
		Date quarterStartDate = DateUtils.getFirstDayOfQuarter(endDate);
		Date yearlyStartDate = DateUtils.getFirstDayOfYear(endDate);

		quarterSummaries.add(currentSummary);
		yearSummaries.add(currentSummary);
		inceptionSummaries.add(currentSummary);

		for (PerformanceInvestmentAccount hs : CollectionUtils.getIterable(historicalList)) {
			Date hsDate = hs.getMeasureDate();
			// If filtering on a specific inception date, ensure the summary is after that historical date otherwise skip it
			if (filterInceptionDate != null) {
				if (DateUtils.compare(hsDate, filterInceptionDate, false) < 0) {
					continue;
				}
			}
			if (DateUtils.isDateBetween(hsDate, quarterStartDate, endDate, false)) {
				quarterSummaries.add(hs);
				yearSummaries.add(hs);
				inceptionSummaries.add(hs);
			}
			else if (DateUtils.isDateBetween(hsDate, yearlyStartDate, endDate, false)) {
				yearSummaries.add(hs);
				inceptionSummaries.add(hs);
			}
			else {
				inceptionSummaries.add(hs);
			}
		}

		historicalSummaryMap.put(mapKeyPrefix + QUARTER_TO_DATE, BeanUtils.sortWithFunction(quarterSummaries, PerformanceInvestmentAccount::getMeasureDate, true));
		historicalSummaryMap.put(mapKeyPrefix + YEAR_TO_DATE, BeanUtils.sortWithFunction(yearSummaries, PerformanceInvestmentAccount::getMeasureDate, true));
		historicalSummaryMap.put(mapKeyPrefix + INCEPTION_TO_DATE, BeanUtils.sortWithFunction(inceptionSummaries, PerformanceInvestmentAccount::getMeasureDate, true));
	}


	////////////////////////////////////////////////////////////
	//////////       Getter and Setter Methods        //////////
	////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}
}
