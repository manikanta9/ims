package com.clifton.performance.account.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Rule Evaluator that evaluates performanceAccountInfo and generates a violation if related InvestmentAccountAssetClasses do not have a benchmarkSecurity.
 *
 * @author jonathanr
 */
public class PerformanceAccountAssetClassMissingBenchmarkRuleEvaluator extends BaseRuleEvaluator<PerformanceAccountInfo, PerformanceInvestmentAccountRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PerformanceAccountInfo performanceAccountInfo, RuleConfig ruleConfig, PerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<InvestmentAccountAssetClass> investmentAccountAssetClasses = context.getInvestmentAccountAssetClassList(performanceAccountInfo);
			List<String> violationNotes = new ArrayList<>();
			for (InvestmentAccountAssetClass investmentAccountAssetClass : CollectionUtils.getIterable(investmentAccountAssetClasses)) {
				if (investmentAccountAssetClass.getBenchmarkSecurity() == null && !investmentAccountAssetClass.getAssetClass().isCash()) {
					violationNotes.add("Missing Benchmark for Asset Class [" + investmentAccountAssetClass.getAssetClass().getName() + "].");
				}
			}
			if (!violationNotes.isEmpty()) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNotes", violationNotes);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceAccountInfo.getId(), contextValues));
			}
		}
		return ruleViolationList;
	}
}
