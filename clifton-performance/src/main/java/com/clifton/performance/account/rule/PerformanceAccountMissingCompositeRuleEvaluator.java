package com.clifton.performance.account.rule;

import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PerformanceAccountMissingCompositeRuleEvaluator</code> generates violations for Performance Investment Accounts where the
 * account has GIPS discretion selection, has positions on for the full period, but is not assigned to a composite.
 *
 * @author apopp
 */
public class PerformanceAccountMissingCompositeRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, PerformanceInvestmentAccountRuleEvaluatorContext> {

	private AccountingPositionHandler accountingPositionHandler;
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	/////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceAccount, RuleConfig ruleConfig, PerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			boolean fullPeriod = getAccountingPositionHandler().isClientAccountPositionsOnForDateRange(performanceAccount.getClientAccount().getId(), performanceAccount.getAccountingPeriod().getStartDate(), performanceAccount.getAccountingPeriod().getEndDate());
			if (fullPeriod) {
				// Is this account assigned to a composite?
				if (CollectionUtils.isEmpty(getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(performanceAccount.getClientAccount().getId(), performanceAccount.getAccountingPeriod().getId()))) {
					// No assignments found.  This account requires GIPs discretion and has a full period.
					violationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceAccount.getId()));
				}
			}
		}
		return violationList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods            //////////////
	/////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}
}
