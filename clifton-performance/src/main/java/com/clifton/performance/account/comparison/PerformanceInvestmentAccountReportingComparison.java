package com.clifton.performance.account.comparison;

import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.performance.report.PerformanceReportService;

import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountReportingComparison</code> can be used for  {@link com.clifton.performance.account.PerformanceInvestmentAccount}
 * and is used based on account performance setup to determine if the account performance report should be posted
 * Can be used to auto post and/or auto complete without posting summaries
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountReportingComparison implements Comparison<PerformanceInvestmentAccount> {

	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;
	private PerformanceReportService performanceReportService;

	/**
	 * Used to drive whether this comparison returns true if the report is expected to be posted or not
	 * Could be used to auto post (not currently) or to auto transition to a completed not posted state
	 */
	private boolean performanceReportExpectedToPost;


	/**
	 * Can be used to limit true result based on posting or not posting based on date
	 * i.e. if performanceReportExpectedToPost = true, then summary date must be on or after x months back else before
	 */
	private Integer postMinPreviousMonthEndsBack;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(PerformanceInvestmentAccount performanceInvestmentAccount, ComparisonContext context) {
		Boolean doNotPost = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(performanceInvestmentAccount.getClientAccount(), PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_REPORT_DO_NOT_POST);
		if (BooleanUtils.isTrue(doNotPost)) {
			return recordContextMessageAndReturn(context, false, "Account setup has Do Not Post Performance Report option set.");
		}

		if (getPostMinPreviousMonthEndsBack() != null) {
			Date minPostDate = DateUtils.addDays(DateUtils.addMonths(DateUtils.getFirstDayOfMonth(new Date()), -getPostMinPreviousMonthEndsBack()), -1);
			if (DateUtils.isDateBefore(performanceInvestmentAccount.getMeasureDate(), minPostDate, false)) {
				return recordContextMessageAndReturn(context, false, "Reporting Date appears to be historical report, not auto-posting.");
			}
		}

		PerformanceReportFrequencies frequency = getPerformanceReportService().getPerformanceInvestmentAccountReportFrequency(performanceInvestmentAccount);
		if (frequency == PerformanceReportFrequencies.QUARTERLY) {
			if (DateUtils.isEqualWithoutTime(DateUtils.getLastDayOfQuarter(performanceInvestmentAccount.getAccountingPeriod().getEndDate()), performanceInvestmentAccount.getAccountingPeriod().getEndDate())) {
				return recordContextMessageAndReturn(context, true, "Reporting Frequency is Quarterly and current period is quarter end.");
			}
			return recordContextMessageAndReturn(context, false, "Reporting Frequency is Quarterly and current period is not quarter end.");
		}
		// Else MONTHLY
		return recordContextMessageAndReturn(context, true, "Reporting Frequency is Monthly.");
	}


	private boolean recordContextMessageAndReturn(ComparisonContext context, boolean postExpected, String message) {
		boolean result = (postExpected == isPerformanceReportExpectedToPost());
		if (context != null) {
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformanceReportService getPerformanceReportService() {
		return this.performanceReportService;
	}


	public void setPerformanceReportService(PerformanceReportService performanceReportService) {
		this.performanceReportService = performanceReportService;
	}


	public boolean isPerformanceReportExpectedToPost() {
		return this.performanceReportExpectedToPost;
	}


	public void setPerformanceReportExpectedToPost(boolean performanceReportExpectedToPost) {
		this.performanceReportExpectedToPost = performanceReportExpectedToPost;
	}


	public Integer getPostMinPreviousMonthEndsBack() {
		return this.postMinPreviousMonthEndsBack;
	}


	public void setPostMinPreviousMonthEndsBack(Integer postMinPreviousMonthEndsBack) {
		this.postMinPreviousMonthEndsBack = postMinPreviousMonthEndsBack;
	}
}
