package com.clifton.performance.account;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.core.util.MathUtils;
import com.clifton.workflow.BaseWorkflowAwareEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccount</code> contains the portfolio performance for an account for a given accounting period.
 * <p>
 * Some values, like Portfolio MTD Return can be plugged into the PerformanceCompositeInvestmentAccount performance table
 * for use in Composite account calculations.
 * <p>
 *
 * @author manderson
 */
public class PerformanceInvestmentAccount extends BaseWorkflowAwareEntity<Integer> implements PerformanceAccountInfo, PortalFileAware {

	public static final String TABLE_PERFORMANCE_INVESTMENT_ACCOUNT = "PerformanceInvestmentAccount";
	public static final String RULE_CATEGORY_PERFORMANCE_INVESTMENT_ACCOUNT = "Performance Summary Rules";

	private InvestmentAccount clientAccount;

	/**
	 * For accounts that perform Portfolio Runs with Asset Classes (Service types: Overlay, Structured Options, Fully Funded)
	 * we have the concept of parent/child accounts through the Sub-Account relationship.  For these cases, we may only
	 * generate one run for the parent and all child accounts.  If that is the case, we use this account to determine what runs
	 * we can pull data from.
	 */
	private InvestmentAccount parentClientAccount;

	/**
	 * The month this performance for this account is associated with
	 */
	private AccountingPeriod accountingPeriod;

	private boolean postedToPortal;

	/**
	 * Overlay: Account Returns
	 * Security Target: Portfolio Gross Returns (Security Target + Options)
	 */
	private BigDecimal monthToDateReturn; // If Linked (Overlay): (1 + Day 1 Return) * (1 + Day 2 Return) * ... * (1 + Day N Return) - 1  Else Sum of Daily Returns (Security Targets)
	private BigDecimal quarterToDateReturn; // can use the formula above using monthToDateReturn for up to 3 month in the quarter
	private BigDecimal yearToDateReturn; // same as above (monthly returns for the year)

	/**
	 * same as above (monthly returns from inception)
	 * If > 1 Year inceptionToDateReturn is annualized, else = cumulative
	 */
	private BigDecimal inceptionToDateReturn;
	private BigDecimal inceptionToDateReturnCumulative;

	/**
	 * Overlay: Benchmark Returns
	 * Security Target: Security Target Returns
	 */
	private BigDecimal monthToDateBenchmarkReturn;
	private BigDecimal quarterToDateBenchmarkReturn;
	private BigDecimal yearToDateBenchmarkReturn;
	private BigDecimal inceptionToDateBenchmarkReturn;
	private BigDecimal inceptionToDateBenchmarkReturnCumulative;

	/**
	 * Overlay: SUM(Gain/Loss) for the month divided by the Last Day of the Previous Month's TPV
	 * Security Target: Portfolio Net Return
	 */
	private BigDecimal monthToDatePortfolioReturn;
	private BigDecimal quarterToDatePortfolioReturn;
	private BigDecimal yearToDatePortfolioReturn;
	private BigDecimal inceptionToDatePortfolioReturn;
	private BigDecimal inceptionToDatePortfolioReturnCumulative;

	private RuleViolationStatus violationStatus;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getMeasureDate() {
		if (getAccountingPeriod() != null) {
			return getAccountingPeriod().getEndDate();
		}
		return null;
	}


	/**
	 * Get label of Client Account label prefixed to Accounting Period labeled
	 */
	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder();
		if (getClientAccount() != null) {
			sb.append(getClientAccount().getLabel());
			sb.append(": ");
		}
		if (getAccountingPeriod() != null) {
			sb.append(DateUtils.fromDate(getAccountingPeriod().getEndDate(), DateUtils.DATE_FORMAT_MONTH_YEAR));
		}
		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getMonthToDateReturnDifference() {
		return MathUtils.subtract(getMonthToDateReturn(), getMonthToDateBenchmarkReturn());
	}


	public BigDecimal getQuarterToDateReturnDifference() {
		return MathUtils.subtract(getQuarterToDateReturn(), getQuarterToDateBenchmarkReturn());
	}


	public BigDecimal getYearToDateReturnDifference() {
		return MathUtils.subtract(getYearToDateReturn(), getYearToDateBenchmarkReturn());
	}


	public BigDecimal getInceptionToDateReturnDifference() {
		return MathUtils.subtract(getInceptionToDateReturn(), getInceptionToDateBenchmarkReturn());
	}


	public BigDecimal getInceptionToDateReturnDifferenceCumulative() {
		return MathUtils.subtract(getInceptionToDateReturnCumulative(), getInceptionToDateBenchmarkReturnCumulative());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentAccount getParentClientAccount() {
		return this.parentClientAccount;
	}


	public void setParentClientAccount(InvestmentAccount parentClientAccount) {
		this.parentClientAccount = parentClientAccount;
	}


	@Override
	public AccountingPeriod getAccountingPeriod() {
		return this.accountingPeriod;
	}


	public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
		this.accountingPeriod = accountingPeriod;
	}


	public BigDecimal getMonthToDateReturn() {
		return this.monthToDateReturn;
	}


	public void setMonthToDateReturn(BigDecimal monthToDateReturn) {
		this.monthToDateReturn = monthToDateReturn;
	}


	public BigDecimal getQuarterToDateReturn() {
		return this.quarterToDateReturn;
	}


	public void setQuarterToDateReturn(BigDecimal quarterToDateReturn) {
		this.quarterToDateReturn = quarterToDateReturn;
	}


	public BigDecimal getYearToDateReturn() {
		return this.yearToDateReturn;
	}


	public void setYearToDateReturn(BigDecimal yearToDateReturn) {
		this.yearToDateReturn = yearToDateReturn;
	}


	public BigDecimal getInceptionToDateReturn() {
		return this.inceptionToDateReturn;
	}


	public void setInceptionToDateReturn(BigDecimal inceptionToDateReturn) {
		this.inceptionToDateReturn = inceptionToDateReturn;
	}


	public BigDecimal getInceptionToDateReturnCumulative() {
		return this.inceptionToDateReturnCumulative;
	}


	public void setInceptionToDateReturnCumulative(BigDecimal inceptionToDateReturnCumulative) {
		this.inceptionToDateReturnCumulative = inceptionToDateReturnCumulative;
	}


	public BigDecimal getMonthToDateBenchmarkReturn() {
		return this.monthToDateBenchmarkReturn;
	}


	public void setMonthToDateBenchmarkReturn(BigDecimal monthToDateBenchmarkReturn) {
		this.monthToDateBenchmarkReturn = monthToDateBenchmarkReturn;
	}


	public BigDecimal getQuarterToDateBenchmarkReturn() {
		return this.quarterToDateBenchmarkReturn;
	}


	public void setQuarterToDateBenchmarkReturn(BigDecimal quarterToDateBenchmarkReturn) {
		this.quarterToDateBenchmarkReturn = quarterToDateBenchmarkReturn;
	}


	public BigDecimal getYearToDateBenchmarkReturn() {
		return this.yearToDateBenchmarkReturn;
	}


	public void setYearToDateBenchmarkReturn(BigDecimal yearToDateBenchmarkReturn) {
		this.yearToDateBenchmarkReturn = yearToDateBenchmarkReturn;
	}


	public BigDecimal getInceptionToDateBenchmarkReturn() {
		return this.inceptionToDateBenchmarkReturn;
	}


	public void setInceptionToDateBenchmarkReturn(BigDecimal inceptionToDateBenchmarkReturn) {
		this.inceptionToDateBenchmarkReturn = inceptionToDateBenchmarkReturn;
	}


	public BigDecimal getInceptionToDateBenchmarkReturnCumulative() {
		return this.inceptionToDateBenchmarkReturnCumulative;
	}


	public void setInceptionToDateBenchmarkReturnCumulative(BigDecimal inceptionToDateBenchmarkReturnCumulative) {
		this.inceptionToDateBenchmarkReturnCumulative = inceptionToDateBenchmarkReturnCumulative;
	}


	public BigDecimal getMonthToDatePortfolioReturn() {
		return this.monthToDatePortfolioReturn;
	}


	public void setMonthToDatePortfolioReturn(BigDecimal monthToDatePortfolioReturn) {
		this.monthToDatePortfolioReturn = monthToDatePortfolioReturn;
	}


	public BigDecimal getQuarterToDatePortfolioReturn() {
		return this.quarterToDatePortfolioReturn;
	}


	public void setQuarterToDatePortfolioReturn(BigDecimal quarterToDatePortfolioReturn) {
		this.quarterToDatePortfolioReturn = quarterToDatePortfolioReturn;
	}


	public BigDecimal getYearToDatePortfolioReturn() {
		return this.yearToDatePortfolioReturn;
	}


	public void setYearToDatePortfolioReturn(BigDecimal yearToDatePortfolioReturn) {
		this.yearToDatePortfolioReturn = yearToDatePortfolioReturn;
	}


	public BigDecimal getInceptionToDatePortfolioReturn() {
		return this.inceptionToDatePortfolioReturn;
	}


	public void setInceptionToDatePortfolioReturn(BigDecimal inceptionToDatePortfolioReturn) {
		this.inceptionToDatePortfolioReturn = inceptionToDatePortfolioReturn;
	}


	public BigDecimal getInceptionToDatePortfolioReturnCumulative() {
		return this.inceptionToDatePortfolioReturnCumulative;
	}


	public void setInceptionToDatePortfolioReturnCumulative(BigDecimal inceptionToDatePortfolioReturnCumulative) {
		this.inceptionToDatePortfolioReturnCumulative = inceptionToDatePortfolioReturnCumulative;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	@Override
	public boolean isPostedToPortal() {
		return this.postedToPortal;
	}


	@Override
	public void setPostedToPortal(boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}
}
