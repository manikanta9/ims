package com.clifton.performance.account.benchmark.process;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.benchmark.PerformanceBenchmark;
import com.clifton.performance.account.benchmark.PerformanceBenchmarkService;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;
import com.clifton.performance.account.benchmark.search.PerformanceBenchmarkSearchForm;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceBenchmarkServiceImpl</code> handles processing of benchmarks for summaries {@link PerformanceInvestmentAccount}
 * Because these are independent and can be loaded based on the benchmark historically across summaries - these methods are exposed to UI
 *
 * @author Mary Anderson
 */
@Service
public class PerformanceBenchmarkProcessServiceImpl implements PerformanceBenchmarkProcessService {

	public static final String QUARTER_TO_DATE = "QTD";
	public static final String YEAR_TO_DATE = "YTD";
	public static final String INCEPTION_TO_DATE = "ITD";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private DataTableRetrievalHandler dataTableRetrievalHandler;

	private MarketDataFieldService marketDataFieldService;
	private MarketDataRetriever marketDataRetriever;
	private MarketDataRatesRetriever marketDataRatesRetriever;

	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;
	private PerformanceInvestmentAccountService performanceInvestmentAccountService;

	private PerformanceBenchmarkService performanceBenchmarkService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public PerformanceSummaryBenchmark savePerformanceSummaryBenchmark(PerformanceSummaryBenchmark bean) {
		getPerformanceInvestmentAccountService().validatePerformanceInvestmentAccountModify(bean.getReferenceOne().getId());
		bean = getPerformanceBenchmarkService().savePerformanceSummaryBenchmark(bean);

		processPerformanceSummaryBenchmarkHistoricalCalculations(bean);
		return bean;
	}


	@Override
	@Transactional
	public void processPerformanceSummaryBenchmarkListForSummary(int summaryId) {
		// Validate Editing Is Allowed
		getPerformanceInvestmentAccountService().validatePerformanceInvestmentAccountModify(summaryId);
		List<PerformanceSummaryBenchmark> newList = generatePerformanceSummaryBenchmarkListForSummary(summaryId);
		getPerformanceBenchmarkService().savePerformanceSummaryBenchmarkListForSummary(summaryId, newList);
	}


	@Override
	@Transactional(readOnly = true)
	public List<PerformanceSummaryBenchmark> generatePerformanceSummaryBenchmarkListForSummary(int summaryId) {
		PerformanceInvestmentAccount summary = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(summaryId);
		PerformanceBenchmarkSearchForm searchForm = new PerformanceBenchmarkSearchForm();
		searchForm.setClientAccountId(summary.getClientAccount().getId());
		searchForm.setActiveOnStartDate(DateUtils.getFirstDayOfMonth(summary.getMeasureDate()));
		searchForm.setActiveOnEndDate(summary.getMeasureDate());
		List<PerformanceBenchmark> benchmarkList = getPerformanceBenchmarkService().getPerformanceBenchmarkList(searchForm);
		List<PerformanceSummaryBenchmark> existingList = getPerformanceBenchmarkService().getPerformanceSummaryBenchmarkListBySummary(summary.getId());
		List<PerformanceSummaryBenchmark> newList = new ArrayList<>();
		for (PerformanceBenchmark performanceBenchmark : CollectionUtils.getIterable(benchmarkList)) {
			PerformanceSummaryBenchmark existingPerformanceSummaryBenchmark = CollectionUtils.getFirstElement(BeanUtils.filter(existingList, PerformanceSummaryBenchmark::getReferenceTwo, performanceBenchmark));
			// Always create new summary benchmark to avoid ORM caching issues on save when within transactional of calling code.
			// Reusing the existing entity for the new list can cause issues in upsert as the entities are the same.
			PerformanceSummaryBenchmark performanceSummaryBenchmark = new PerformanceSummaryBenchmark();
			if (existingPerformanceSummaryBenchmark == null) {
				performanceSummaryBenchmark.setReferenceOne(summary);
				performanceSummaryBenchmark.setReferenceTwo(performanceBenchmark);
			}
			else {
				BeanUtils.copyProperties(existingPerformanceSummaryBenchmark, performanceSummaryBenchmark);
			}
			// Get/Recalculate MTD, QTD, YTD, ITD
			try {
				performanceSummaryBenchmark.setMonthToDateReturn(calculatePerformanceBenchmarkMTD(performanceSummaryBenchmark.getReferenceTwo(), summary.getMeasureDate()));
			}
			catch (ValidationExceptionWithCause e) {
				performanceSummaryBenchmark.setMonthToDateReturn(null);
				// Ignore these validation exceptions - will be generated by warning generation
			}
			processPerformanceSummaryBenchmarkHistoricalCalculations(performanceSummaryBenchmark);
			newList.add(performanceSummaryBenchmark);
		}
		return newList;
	}


	@Override
	@Transactional
	public void processPerformanceSummaryBenchmarkListForBenchmark(int benchmarkId, boolean reload, Date reloadFromDate) {
		PerformanceBenchmark benchmark = getPerformanceBenchmarkService().getPerformanceBenchmark(benchmarkId);
		Date inceptionDate = getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountInceptionDate(benchmark.getClientAccount().getId());

		PerformanceInvestmentAccountSearchForm summarySearchForm = new PerformanceInvestmentAccountSearchForm();
		summarySearchForm.setClientAccountId(benchmark.getClientAccount().getId());
		// Ensure in date order
		summarySearchForm.setOrderBy("measureDate:ASC");
		List<PerformanceInvestmentAccount> summaryList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(summarySearchForm);

		List<PerformanceSummaryBenchmark> existingList = getPerformanceBenchmarkService().getPerformanceSummaryBenchmarkListByBenchmark(benchmarkId);
		List<PerformanceSummaryBenchmark> newList = new ArrayList<>();
		for (PerformanceInvestmentAccount summary : CollectionUtils.getIterable(summaryList)) {
			// If not active during the summary date - skip it
			if (!DateUtils.isOverlapInDates(benchmark.getStartDate(), benchmark.getEndDate(), summary.getAccountingPeriod().getStartDate(), summary.getAccountingPeriod().getEndDate())) {
				continue;
			}
			PerformanceSummaryBenchmark summaryBenchmark = CollectionUtils.getFirstElement(BeanUtils.filter(existingList, performanceSummaryBenchmark -> performanceSummaryBenchmark.getReferenceOne().getId(), summary.getId()));

			// Only Load where missing (unless reloading all), otherwise leave as it is. When loading for a summary, calculation is smart enough to lookup market data if summary benchmark is missing historically.  Filling it in now won't change history.
			// Note: reload will keep any explicit overrides entered
			if (summaryBenchmark == null || reload) {
				// If doesn't exist and before client inception date skip it - this is really old data that we don't need
				if (DateUtils.compare(summary.getMeasureDate(), inceptionDate, false) <= 0) {
					continue;
				}
				if (summaryBenchmark == null) {
					summaryBenchmark = new PerformanceSummaryBenchmark();
					summaryBenchmark.setReferenceOne(summary);
					summaryBenchmark.setReferenceTwo(benchmark);
				}
				// If reloading from a specified date, and this summary is before that date, keep it in the list so subsequent dates can be recalculated using it
				else if (reloadFromDate != null && DateUtils.compare(summary.getMeasureDate(), reloadFromDate, false) < 0) {
					newList.add(summaryBenchmark);
					continue;
				}
				// If reusing existing - clear calculated values(keep mtd override)
				else {
					summaryBenchmark.setMonthToDateReturn(null);
					summaryBenchmark.setQuarterToDateReturn(null);
					summaryBenchmark.setYearToDateReturn(null);
					summaryBenchmark.setInceptionToDateReturn(null);
				}

				// Get/Calculate MTD
				try {
					summaryBenchmark.setMonthToDateReturn(calculatePerformanceBenchmarkMTD(summaryBenchmark.getReferenceTwo(), summary.getMeasureDate()));
				}
				catch (ValidationExceptionWithCause e) {
					summaryBenchmark.setMonthToDateReturn(null);
					// Ignore these validation exceptions - will be generated by warning generation
				}
				processPerformanceSummaryBenchmarkHistoricalCalculationsUsingList(summaryBenchmark, newList, inceptionDate);
				newList.add(summaryBenchmark);
			}
		}

		// Save List
		getPerformanceBenchmarkService().savePerformanceSummaryBenchmarkList(newList);
	}


	private void processPerformanceSummaryBenchmarkHistoricalCalculationsUsingList(PerformanceSummaryBenchmark bean, List<PerformanceSummaryBenchmark> sbList,
	                                                                               Date clientAccountInceptionDate) {

		List<PerformanceSummaryBenchmark> quarterSummaries = new ArrayList<>();
		List<PerformanceSummaryBenchmark> yearSummaries = new ArrayList<>();
		List<PerformanceSummaryBenchmark> inceptionSummaries = new ArrayList<>();

		Date quarterStartDate = DateUtils.getFirstDayOfQuarter(bean.getReferenceOne().getMeasureDate());
		Date yearlyStartDate = DateUtils.getFirstDayOfYear(bean.getReferenceOne().getMeasureDate());

		// Add Current Summary's MTD Value
		quarterSummaries.add(bean);
		yearSummaries.add(bean);
		inceptionSummaries.add(bean);

		for (PerformanceSummaryBenchmark sb : CollectionUtils.getIterable(sbList)) {
			Date hsDate = sb.getReferenceOne().getMeasureDate();

			if (DateUtils.isDateBetween(hsDate, quarterStartDate, bean.getReferenceOne().getMeasureDate(), false)) {
				quarterSummaries.add(sb);
				yearSummaries.add(sb);
				inceptionSummaries.add(sb);
			}
			else if (DateUtils.isDateBetween(hsDate, yearlyStartDate, bean.getReferenceOne().getMeasureDate(), false)) {
				yearSummaries.add(sb);
				inceptionSummaries.add(sb);
			}
			else {
				inceptionSummaries.add(sb);
			}
		}

		bean.setQuarterToDateReturn(CoreMathUtils.getTotalPercentChange(quarterSummaries, PerformanceSummaryBenchmark::getCoalesceMonthToDateReturnOverride, true));
		bean.setYearToDateReturn(CoreMathUtils.getTotalPercentChange(yearSummaries, PerformanceSummaryBenchmark::getCoalesceMonthToDateReturnOverride, true));

		// Use Annualized Rates for Inception To Date when we have over a year's worth of data
		if (CollectionUtils.getSize(inceptionSummaries) > 12) {
			// If more than 12 months worth of data - annualize it but use days
			// First start with the Benchmark Inception Date, if Not explicitly defined, then use the Client Account Inception Date.
			Date iDate = bean.getReferenceTwo().getStartDate();
			if (iDate == null) {
				iDate = clientAccountInceptionDate;
			}
			Integer totalDays = DateUtils.getDaysDifference(bean.getReferenceOne().getMeasureDate(), iDate);
			bean.setInceptionToDateReturn(CoreMathUtils.getTotalPercentChangeAnnualizedDaily(inceptionSummaries, PerformanceSummaryBenchmark::getCoalesceMonthToDateReturnOverride, true, totalDays));
		}
		else {
			bean.setInceptionToDateReturn(CoreMathUtils.getTotalPercentChange(inceptionSummaries, PerformanceSummaryBenchmark::getCoalesceMonthToDateReturnOverride, true));
		}
	}


	private BigDecimal calculatePerformanceBenchmarkMTD(PerformanceBenchmark benchmark, Date monthEndDate) {
		BigDecimal monthToDateValue = null;
		// Actually start on the last day of the previous month so we include performance from the first day of the month
		Date monthStartDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(monthEndDate), -1);

		// If Benchmark Setup starts or ends within the month - try to calculate a partial month return if possible (doesn't apply to synthetics when we use Monthly Return market data field)
		// If benchmark partial month - use those dates instead
		Date benchmarkMonthStart = monthStartDate;
		Date benchmarkMonthEnd = monthEndDate;
		if (benchmark.getStartDate() != null && DateUtils.isDateBetween(benchmark.getStartDate(), monthStartDate, monthEndDate, false)) {
			benchmarkMonthStart = benchmark.getStartDate();
		}
		if (benchmark.getEndDate() != null && DateUtils.isDateBetween(benchmark.getEndDate(), monthStartDate, monthEndDate, false)) {
			benchmarkMonthEnd = benchmark.getEndDate();
		}

		if (benchmark.getBenchmarkSecurity() != null) {
			// Look up MTD - Since in order for it to apply to that month, need to set start/end dates for search
			MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
			searchForm.setMinMeasureDate(monthStartDate);
			searchForm.setMaxMeasureDate(DateUtils.addDays(monthEndDate, 1)); // Does a less than search so move to next day
			searchForm.setInvestmentSecurityId(benchmark.getBenchmarkSecurity().getId());
			searchForm.setDataFieldName(MarketDataField.FIELD_MONTHLY_RETURN);
			searchForm.setOrderBy("measureDate:desc");
			// Get the most recent one to the summary measure date
			MarketDataValue mr = CollectionUtils.getFirstElement(getMarketDataFieldService().getMarketDataValueList(searchForm));
			// If it exists, use it
			if (mr != null) {
				monthToDateValue = mr.getMeasureValue();
			}
			// otherwise try to calculate it based on prices - using benchmark start/end dates if different
			else {
				monthToDateValue = getMarketDataRetriever().getInvestmentSecurityReturnFlexible(benchmark.getClientAccount(), benchmark.getBenchmarkSecurity(), benchmarkMonthStart, benchmarkMonthEnd,
						"Performance Summary Benchmark", true);
			}
		}
		else if (benchmark.getBenchmarkInterestRateIndex() != null) {
			monthToDateValue = getMarketDataRatesRetriever().getInvestmentInterestRateIndexReturn(benchmark.getBenchmarkInterestRateIndex(), benchmarkMonthStart, benchmarkMonthEnd, true);
		}
		return monthToDateValue;
	}


	private void processPerformanceSummaryBenchmarkHistoricalCalculations(PerformanceSummaryBenchmark bean) {
		Date accountInceptionDate = getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountInceptionDate(bean.getReferenceOne().getClientAccount().getId());
		Map<String, List<BigDecimal>> historicalMap = getPerformanceSummaryBenchmarkHistoricalMap(bean, accountInceptionDate);
		bean.setQuarterToDateReturn(MathUtils.getTotalPercentChange(historicalMap.get(QUARTER_TO_DATE).toArray(), true));
		bean.setYearToDateReturn(MathUtils.getTotalPercentChange(historicalMap.get(YEAR_TO_DATE).toArray(), true));

		// Use Annualized Rates for Inception To Date when we have over a year's worth of data
		List<BigDecimal> inceptionList = historicalMap.get(INCEPTION_TO_DATE);
		if (CollectionUtils.getSize(inceptionList) > 12) {
			// If more than 12 months worth of data - annualize it but use days
			// First start with the Benchmark Inception Date, if Not explicitly defined, then use the Client Account Inception Date.
			Date iDate = bean.getReferenceTwo().getStartDate();
			if (iDate == null) {
				iDate = accountInceptionDate;
			}
			BigDecimal inceptionReturn = MathUtils.getTotalPercentChange(inceptionList.toArray(), true);
			bean.setInceptionToDateReturn(CoreMathUtils.annualizeByDateRange(inceptionReturn, iDate, bean.getReferenceOne().getMeasureDate(), true, true));
		}
		else {
			bean.setInceptionToDateReturn(MathUtils.getTotalPercentChange(inceptionList.toArray(), true));
		}
	}


	private Map<String, List<BigDecimal>> getPerformanceSummaryBenchmarkHistoricalMap(PerformanceSummaryBenchmark summaryBenchmark, Date accountInceptionDate) {
		PerformanceInvestmentAccount summary = summaryBenchmark.getReferenceOne();
		PerformanceBenchmark benchmark = summaryBenchmark.getReferenceTwo();

		// Need to include months that may not have summary data for benchmarks, easiest way it to just run query and add in missing monthly data
		// Get Full Historical List - Missing Months will return dates with no MTD value
		DataTable results = getDataTableRetrievalHandler().findDataTable(new SqlSelectCommand(
				"SELECT cd.StartDate \"Last Day Of Month\" " //
						+ ", m.MTD AS \"MTD Value\" " //
						+ " FROM InvestmentAccount a " //
						+ " INNER JOIN CalendarDay cd ON ? <= cd.StartDate " //
						+ " INNER JOIN PerformanceBenchmark b ON a.InvestmentAccountID = b.ClientInvestmentAccountID " //
						+ " LEFT JOIN ( " //
						+ " SELECT sbs.ClientInvestmentAccountID \"ClientAccountID\" " //
						+ ", sb.PerformanceBenchmarkID \"BenchmarkID\" " //
						+ ", MONTH(p.EndDate) \"Month\" " //
						+ ", YEAR(p.EndDate) \"Year\" " //
						+ ", COALESCE(sb.MonthToDateReturnOverride, sb.MonthToDateReturn) \"MTD\" " //
						+ " FROM PerformanceSummaryBenchmark sb " //
						+ " INNER JOIN PerformanceInvestmentAccount sbs ON sb.PerformanceInvestmentAccountID = sbs.PerformanceInvestmentAccountID " //
						+ " INNER JOIN AccountingPeriod p ON sbs.AccountingPeriodID = p.AccountingPeriodID "
						+ ") m ON a.InvestmentAccountID = m.ClientAccountID AND cd.CalendarMonthID = m.Month AND cd.CalendarYearID = m.Year AND b.PerformanceBenchmarkID = m.BenchmarkID " //
						+ " WHERE a.InvestmentAccountID = ? " //
						+ " AND cd.StartDate < ? " //
						+ " AND cd.StartDate >= COALESCE(b.StartDate, (SELECT MIN(p.EndDate) FROM PerformanceInvestmentAccount s INNER JOIN AccountingPeriod p ON s.AccountingPeriodID = p.AccountingPeriodID WHERE s.ClientInvestmentAccountID = a.InvestmentAccountID)) " //
						+ " AND cd.StartDate = (SELECT MAX(d.StartDate) FROM CalendarDay d WHERE d.CalendarYearID = cd.CalendarYearID AND d.CalendarMonthID = cd.CalendarMonthID) " //
						+ " AND b.PerformanceBenchmarkID = ? " //
						+ " ORDER BY cd.StartDate ASC")
				.addDateParameterValue(accountInceptionDate)
				.addIntegerParameterValue(summary.getClientAccount().getId())
				.addDateParameterValue(DateUtils.getFirstDayOfMonth(summary.getMeasureDate()))
				.addIntegerParameterValue(benchmark.getId())
		);

		List<BigDecimal> quarterSummaries = new ArrayList<>();
		List<BigDecimal> yearSummaries = new ArrayList<>();
		List<BigDecimal> inceptionSummaries = new ArrayList<>();

		Date quarterStartDate = DateUtils.getFirstDayOfQuarter(summary.getMeasureDate());
		Date yearlyStartDate = DateUtils.getFirstDayOfYear(summary.getMeasureDate());

		// Add Current Summary's MTD Value
		quarterSummaries.add(summaryBenchmark.getCoalesceMonthToDateReturnOverride());
		yearSummaries.add(summaryBenchmark.getCoalesceMonthToDateReturnOverride());
		inceptionSummaries.add(summaryBenchmark.getCoalesceMonthToDateReturnOverride());

		if (results != null) {
			for (int i = 0; i < results.getTotalRowCount(); i++) {
				DataRow row = results.getRow(i);
				Date hsDate = (Date) row.getValue(0);
				BigDecimal val = (BigDecimal) row.getValue(1);

				if (val == null) {
					val = calculatePerformanceBenchmarkMTD(benchmark, hsDate);
				}
				if (DateUtils.isDateBetween(hsDate, quarterStartDate, summary.getMeasureDate(), false)) {
					quarterSummaries.add(val);
					yearSummaries.add(val);
					inceptionSummaries.add(val);
				}
				else if (DateUtils.isDateBetween(hsDate, yearlyStartDate, summary.getMeasureDate(), false)) {
					yearSummaries.add(val);
					inceptionSummaries.add(val);
				}
				else {
					inceptionSummaries.add(val);
				}
			}
		}
		Map<String, List<BigDecimal>> historicalMap = new HashMap<>();
		historicalMap.put(QUARTER_TO_DATE, quarterSummaries);
		historicalMap.put(YEAR_TO_DATE, yearSummaries);
		historicalMap.put(INCEPTION_TO_DATE, inceptionSummaries);

		return historicalMap;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public DataTableRetrievalHandler getDataTableRetrievalHandler() {
		return this.dataTableRetrievalHandler;
	}


	public void setDataTableRetrievalHandler(DataTableRetrievalHandler dataTableRetrievalHandler) {
		this.dataTableRetrievalHandler = dataTableRetrievalHandler;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public PerformanceBenchmarkService getPerformanceBenchmarkService() {
		return this.performanceBenchmarkService;
	}


	public void setPerformanceBenchmarkService(PerformanceBenchmarkService performanceBenchmarkService) {
		this.performanceBenchmarkService = performanceBenchmarkService;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}
}
