package com.clifton.performance.account.securitytarget.process.calculators;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.performance.account.process.calculators.BasePerformanceInvestmentAccountCalculator;
import com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetDailyReturn;
import com.clifton.performance.account.securitytarget.process.PerformanceInvestmentAccountSecurityTargetProcessConfig;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PerformanceInvestmentAccountSecurityTargetCalculator</code> calculates the period results for an account performance (populated {@link com.clifton.performance.account.PerformanceInvestmentAccount}
 * based on the results of the daily returns in the the {@link com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetDailyReturn}s for the period
 * <p>
 * Benchmark Return = Security Target Return (Compounded/Linked)
 * Portfolio Return Gross = Benchmark Target Return + (Sum Daily Options Returns)
 * Portfolio Return Net = Benchmark Target Return + Sum Daily Options Returns - last day of month includes management fee
 * <p>
 * Quarterly, YTD, etc. all compound the monthly returns
 *
 * @author manderson
 */
@Component
public class PerformanceInvestmentAccountSecurityTargetCalculator extends BasePerformanceInvestmentAccountCalculator<PerformanceInvestmentAccountSecurityTargetProcessConfig> {


	@Override
	protected void recalculateAccountReturns(PerformanceInvestmentAccountSecurityTargetProcessConfig config) {
		List<PerformanceInvestmentAccountSecurityTargetDailyReturn> dailyReturnList = config.getPerformanceSecurityTargetDailyReturnList();
		// Link Target Returns
		config.getPerformanceAccount().setMonthToDateBenchmarkReturn(CoreMathUtils.getTotalPercentChange(dailyReturnList, PerformanceInvestmentAccountSecurityTargetDailyReturn::getSecurityTargetReturn, true));
		// Gross: Monthly Target Return + Sum Daily Option (Gross) Returns
		config.getPerformanceAccount().setMonthToDateReturn(MathUtils.add(config.getPerformanceAccount().getMonthToDateBenchmarkReturn(), CoreMathUtils.sumProperty(dailyReturnList, PerformanceInvestmentAccountSecurityTargetDailyReturn::getOptionsGrossReturn)));
		// Net: Monthly Target Return + Sum Daily Option (Net) Returns
		config.getPerformanceAccount().setMonthToDatePortfolioReturn(MathUtils.add(config.getPerformanceAccount().getMonthToDateBenchmarkReturn(), CoreMathUtils.sumProperty(dailyReturnList, PerformanceInvestmentAccountSecurityTargetDailyReturn::getOptionsNetReturn)));
	}
}
