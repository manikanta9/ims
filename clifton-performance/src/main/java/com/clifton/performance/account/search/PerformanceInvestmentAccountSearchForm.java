package com.clifton.performance.account.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountSearchForm</code> ...
 *
 * @author manderson
 */
@SearchForm(skippedPathsForValidation = {"clientAccount.relatedRelationshipList.referenceTwo.issuingCompany.name", "clientAccount.relatedRelationshipList.purpose.id", "clientAccount.relatedRelationshipList.startDate", "clientAccount.relatedRelationshipList.endDate"})
public class PerformanceInvestmentAccountSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField(searchFieldPath = "clientAccount", searchField = "name,number")
	private String searchPattern;

	@SearchField(searchField = "clientAccount.id", sortField = "clientAccount.number")
	private Integer clientAccountId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "teamSecurityGroup.id")
	private Short teamSecurityGroupId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "clientAccount.businessService.id,clientAccount.businessService.parent.id,clientAccount.businessService.parent.parent.id,clientAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "clientAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "clientAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer clientAccountGroupId;

	@SearchField(searchField = "performanceInceptionDate,inceptionDate", searchFieldPath = "clientAccount", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Date coalesceClientInvestmentAccountPerformanceInceptionDate;

	@SearchField(searchField = "violationStatus.id")
	private Short violationStatusId;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;

	@SearchField(searchField = "accountingPeriod.id", sortField = "accountingPeriod.endDate")
	private Integer accountingPeriodId;

	@SearchField
	private Boolean postedToPortal;

	@SearchField(searchFieldPath = "accountingPeriod", searchField = "endDate")
	private Date measureDate;

	@SearchField(searchFieldPath = "accountingPeriod", searchField = "endDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date startMeasureDate;

	@SearchField(searchFieldPath = "accountingPeriod", searchField = "endDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date endMeasureDate;

	@SearchField
	private BigDecimal monthToDateReturn;

	@SearchField
	private BigDecimal quarterToDateReturn;

	@SearchField
	private BigDecimal yearToDateReturn;

	@SearchField
	private BigDecimal inceptionToDateReturn;

	@SearchField
	private BigDecimal inceptionToDateReturnCumulative;

	@SearchField(searchField = "inceptionToDateReturnCumulative", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean inceptionToDateReturnCumulativeIsNull;

	@SearchField
	private BigDecimal monthToDateBenchmarkReturn;

	@SearchField
	private BigDecimal quarterToDateBenchmarkReturn;

	@SearchField
	private BigDecimal yearToDateBenchmarkReturn;

	@SearchField
	private BigDecimal inceptionToDateBenchmarkReturn;

	@SearchField
	private BigDecimal inceptionToDateBenchmarkReturnCumulative;

	@SearchField
	private BigDecimal monthToDatePortfolioReturn;

	@SearchField
	private BigDecimal quarterToDatePortfolioReturn;

	@SearchField
	private BigDecimal yearToDatePortfolioReturn;

	@SearchField
	private BigDecimal inceptionToDatePortfolioReturn;

	@SearchField
	private BigDecimal inceptionToDatePortfolioReturnCumulative;

	// Calculated Columns

	@SearchField(searchField = "monthToDateReturn,monthToDateBenchmarkReturn", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal monthToDateReturnDifference;

	@SearchField(searchField = "quarterToDateReturn,quarterToDateBenchmarkReturn", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal quarterToDateReturnDifference;

	@SearchField(searchField = "yearToDateReturn,yearToDateBenchmarkReturn", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal yearToDateReturnDifference;

	@SearchField(searchField = "inceptionToDateReturn,inceptionToDateBenchmarkReturn", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal inceptionToDateReturnDifference;

	@SearchField(searchField = "inceptionToDateReturnCumulative,inceptionToDateBenchmarkReturnCumulative", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal inceptionToDateReturnDifferenceCumulative;

	// This field can be used in conjunction with clientAccount processing type purpose type criterion
	@SearchField(searchField = "relatedRelationshipList.referenceTwo.issuingCompany.id", searchFieldPath = "clientAccount", comparisonConditions = ComparisonConditions.EXISTS, sortField = "relatedRelationshipList.referenceTwo.issuingCompany.name")
	private Integer clientAccountProcessingTypePurposeAccountIssuingCompanyId;

	// Processing type filters can be used in conjunction with the processing type purpose issuing company for finding performance summaries
	@SearchField(searchField = "relatedRelationshipList.purpose.id", searchFieldPath = "clientAccount")
	private Short clientAccountProcessingTypePurposeId;

	// This value is provided from the client for accounts/issuers by purpose (e.g. the "Security Targets" view uses "Trading: Options") and is converted to the purpose ID using caches.
	private String clientAccountProcessingTypePurposeName;

	// Purposes have start and end dates, the next two parameters allow for filtering for relationships within the performance range (start and end measure dates)
	@SearchField(searchField = "relatedRelationshipList.startDate", searchFieldPath = "clientAccount")
	private Date clientAccountProcessingTypePurposeStartDate;

	@SearchField(searchField = "relatedRelationshipList.endDate", searchFieldPath = "clientAccount")
	private Date clientAccountProcessingTypePurposeEndDate;

	@SearchField(searchField = "workflowStateEffectiveStartDate", searchFieldPath = "clientAccount")
	private Date clientAccountClosedDate;

	// Custom fields

	// Custom Search Field = Find summary for a Specific Account or any of its SubAccounts
	private Integer clientInvestmentAccountIdOrSubAccount;

	// Custom flag used to load a client account's related account based on processing type purpose
	private Boolean clientAccountProcessingTypePurposeAccount;

	// Custom flag to filter to client account company contact
	// Example: Security Targets view client side filters by a Financial Advisor to list all that need to be sent to the one contact
	private Integer clientAccountBusinessCompanyContactId;

	// Custom flag to filter to client account company contact type name, can be optionally added with the clientAccountBusinessCompanyContactId
	// Example: Security Targets view client side filters by a Financial Advisor to list all that need to be sent to the one contact
	private String clientAccountBusinessCompanyContactTypeName;

	// Custom search field set based on the restriction value used with clientAccountClosedDate
	private Boolean clientAccountClosed;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}


	public Date getCoalesceClientInvestmentAccountPerformanceInceptionDate() {
		return this.coalesceClientInvestmentAccountPerformanceInceptionDate;
	}


	public void setCoalesceClientInvestmentAccountPerformanceInceptionDate(Date coalesceClientInvestmentAccountPerformanceInceptionDate) {
		this.coalesceClientInvestmentAccountPerformanceInceptionDate = coalesceClientInvestmentAccountPerformanceInceptionDate;
	}


	public Short getViolationStatusId() {
		return this.violationStatusId;
	}


	public void setViolationStatusId(Short violationStatusId) {
		this.violationStatusId = violationStatusId;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public Integer getAccountingPeriodId() {
		return this.accountingPeriodId;
	}


	public void setAccountingPeriodId(Integer accountingPeriodId) {
		this.accountingPeriodId = accountingPeriodId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Date getStartMeasureDate() {
		return this.startMeasureDate;
	}


	public void setStartMeasureDate(Date startMeasureDate) {
		this.startMeasureDate = startMeasureDate;
	}


	public Date getEndMeasureDate() {
		return this.endMeasureDate;
	}


	public void setEndMeasureDate(Date endMeasureDate) {
		this.endMeasureDate = endMeasureDate;
	}


	public BigDecimal getMonthToDateReturn() {
		return this.monthToDateReturn;
	}


	public void setMonthToDateReturn(BigDecimal monthToDateReturn) {
		this.monthToDateReturn = monthToDateReturn;
	}


	public BigDecimal getQuarterToDateReturn() {
		return this.quarterToDateReturn;
	}


	public void setQuarterToDateReturn(BigDecimal quarterToDateReturn) {
		this.quarterToDateReturn = quarterToDateReturn;
	}


	public BigDecimal getYearToDateReturn() {
		return this.yearToDateReturn;
	}


	public void setYearToDateReturn(BigDecimal yearToDateReturn) {
		this.yearToDateReturn = yearToDateReturn;
	}


	public BigDecimal getInceptionToDateReturn() {
		return this.inceptionToDateReturn;
	}


	public void setInceptionToDateReturn(BigDecimal inceptionToDateReturn) {
		this.inceptionToDateReturn = inceptionToDateReturn;
	}


	public BigDecimal getMonthToDateBenchmarkReturn() {
		return this.monthToDateBenchmarkReturn;
	}


	public void setMonthToDateBenchmarkReturn(BigDecimal monthToDateBenchmarkReturn) {
		this.monthToDateBenchmarkReturn = monthToDateBenchmarkReturn;
	}


	public BigDecimal getQuarterToDateBenchmarkReturn() {
		return this.quarterToDateBenchmarkReturn;
	}


	public void setQuarterToDateBenchmarkReturn(BigDecimal quarterToDateBenchmarkReturn) {
		this.quarterToDateBenchmarkReturn = quarterToDateBenchmarkReturn;
	}


	public BigDecimal getYearToDateBenchmarkReturn() {
		return this.yearToDateBenchmarkReturn;
	}


	public void setYearToDateBenchmarkReturn(BigDecimal yearToDateBenchmarkReturn) {
		this.yearToDateBenchmarkReturn = yearToDateBenchmarkReturn;
	}


	public BigDecimal getInceptionToDateBenchmarkReturn() {
		return this.inceptionToDateBenchmarkReturn;
	}


	public void setInceptionToDateBenchmarkReturn(BigDecimal inceptionToDateBenchmarkReturn) {
		this.inceptionToDateBenchmarkReturn = inceptionToDateBenchmarkReturn;
	}


	public BigDecimal getMonthToDatePortfolioReturn() {
		return this.monthToDatePortfolioReturn;
	}


	public void setMonthToDatePortfolioReturn(BigDecimal monthToDatePortfolioReturn) {
		this.monthToDatePortfolioReturn = monthToDatePortfolioReturn;
	}


	public BigDecimal getQuarterToDatePortfolioReturn() {
		return this.quarterToDatePortfolioReturn;
	}


	public void setQuarterToDatePortfolioReturn(BigDecimal quarterToDatePortfolioReturn) {
		this.quarterToDatePortfolioReturn = quarterToDatePortfolioReturn;
	}


	public BigDecimal getYearToDatePortfolioReturn() {
		return this.yearToDatePortfolioReturn;
	}


	public void setYearToDatePortfolioReturn(BigDecimal yearToDatePortfolioReturn) {
		this.yearToDatePortfolioReturn = yearToDatePortfolioReturn;
	}


	public BigDecimal getInceptionToDatePortfolioReturn() {
		return this.inceptionToDatePortfolioReturn;
	}


	public void setInceptionToDatePortfolioReturn(BigDecimal inceptionToDatePortfolioReturn) {
		this.inceptionToDatePortfolioReturn = inceptionToDatePortfolioReturn;
	}


	public Boolean getPostedToPortal() {
		return this.postedToPortal;
	}


	public void setPostedToPortal(Boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}


	public BigDecimal getInceptionToDateReturnCumulative() {
		return this.inceptionToDateReturnCumulative;
	}


	public void setInceptionToDateReturnCumulative(BigDecimal inceptionToDateReturnCumulative) {
		this.inceptionToDateReturnCumulative = inceptionToDateReturnCumulative;
	}


	public BigDecimal getInceptionToDateBenchmarkReturnCumulative() {
		return this.inceptionToDateBenchmarkReturnCumulative;
	}


	public void setInceptionToDateBenchmarkReturnCumulative(BigDecimal inceptionToDateBenchmarkReturnCumulative) {
		this.inceptionToDateBenchmarkReturnCumulative = inceptionToDateBenchmarkReturnCumulative;
	}


	public BigDecimal getInceptionToDatePortfolioReturnCumulative() {
		return this.inceptionToDatePortfolioReturnCumulative;
	}


	public void setInceptionToDatePortfolioReturnCumulative(BigDecimal inceptionToDatePortfolioReturnCumulative) {
		this.inceptionToDatePortfolioReturnCumulative = inceptionToDatePortfolioReturnCumulative;
	}


	public Boolean getInceptionToDateReturnCumulativeIsNull() {
		return this.inceptionToDateReturnCumulativeIsNull;
	}


	public void setInceptionToDateReturnCumulativeIsNull(Boolean inceptionToDateReturnCumulativeIsNull) {
		this.inceptionToDateReturnCumulativeIsNull = inceptionToDateReturnCumulativeIsNull;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public BigDecimal getMonthToDateReturnDifference() {
		return this.monthToDateReturnDifference;
	}


	public void setMonthToDateReturnDifference(BigDecimal monthToDateReturnDifference) {
		this.monthToDateReturnDifference = monthToDateReturnDifference;
	}


	public BigDecimal getQuarterToDateReturnDifference() {
		return this.quarterToDateReturnDifference;
	}


	public void setQuarterToDateReturnDifference(BigDecimal quarterToDateReturnDifference) {
		this.quarterToDateReturnDifference = quarterToDateReturnDifference;
	}


	public BigDecimal getYearToDateReturnDifference() {
		return this.yearToDateReturnDifference;
	}


	public void setYearToDateReturnDifference(BigDecimal yearToDateReturnDifference) {
		this.yearToDateReturnDifference = yearToDateReturnDifference;
	}


	public BigDecimal getInceptionToDateReturnDifference() {
		return this.inceptionToDateReturnDifference;
	}


	public void setInceptionToDateReturnDifference(BigDecimal inceptionToDateReturnDifference) {
		this.inceptionToDateReturnDifference = inceptionToDateReturnDifference;
	}


	public BigDecimal getInceptionToDateReturnDifferenceCumulative() {
		return this.inceptionToDateReturnDifferenceCumulative;
	}


	public void setInceptionToDateReturnDifferenceCumulative(BigDecimal inceptionToDateReturnDifferenceCumulative) {
		this.inceptionToDateReturnDifferenceCumulative = inceptionToDateReturnDifferenceCumulative;
	}


	public Integer getClientAccountProcessingTypePurposeAccountIssuingCompanyId() {
		return this.clientAccountProcessingTypePurposeAccountIssuingCompanyId;
	}


	public void setClientAccountProcessingTypePurposeAccountIssuingCompanyId(Integer clientAccountProcessingTypePurposeAccountIssuingCompanyId) {
		this.clientAccountProcessingTypePurposeAccountIssuingCompanyId = clientAccountProcessingTypePurposeAccountIssuingCompanyId;
	}


	public Short getClientAccountProcessingTypePurposeId() {
		return this.clientAccountProcessingTypePurposeId;
	}


	public void setClientAccountProcessingTypePurposeId(Short clientAccountProcessingTypePurposeId) {
		this.clientAccountProcessingTypePurposeId = clientAccountProcessingTypePurposeId;
	}


	public String getClientAccountProcessingTypePurposeName() {
		return this.clientAccountProcessingTypePurposeName;
	}


	public void setClientAccountProcessingTypePurposeName(String clientAccountProcessingTypePurposeName) {
		this.clientAccountProcessingTypePurposeName = clientAccountProcessingTypePurposeName;
	}


	public Date getClientAccountProcessingTypePurposeStartDate() {
		return this.clientAccountProcessingTypePurposeStartDate;
	}


	public void setClientAccountProcessingTypePurposeStartDate(Date clientAccountProcessingTypePurposeStartDate) {
		this.clientAccountProcessingTypePurposeStartDate = clientAccountProcessingTypePurposeStartDate;
	}


	public Date getClientAccountProcessingTypePurposeEndDate() {
		return this.clientAccountProcessingTypePurposeEndDate;
	}


	public void setClientAccountProcessingTypePurposeEndDate(Date clientAccountProcessingTypePurposeEndDate) {
		this.clientAccountProcessingTypePurposeEndDate = clientAccountProcessingTypePurposeEndDate;
	}


	public Date getClientAccountClosedDate() {
		return this.clientAccountClosedDate;
	}


	public void setClientAccountClosedDate(Date clientAccountClosedDate) {
		this.clientAccountClosedDate = clientAccountClosedDate;
	}


	public Integer getClientInvestmentAccountIdOrSubAccount() {
		return this.clientInvestmentAccountIdOrSubAccount;
	}


	public void setClientInvestmentAccountIdOrSubAccount(Integer clientInvestmentAccountIdOrSubAccount) {
		this.clientInvestmentAccountIdOrSubAccount = clientInvestmentAccountIdOrSubAccount;
	}


	public Boolean getClientAccountProcessingTypePurposeAccount() {
		return this.clientAccountProcessingTypePurposeAccount;
	}


	public void setClientAccountProcessingTypePurposeAccount(Boolean clientAccountProcessingTypePurposeAccount) {
		this.clientAccountProcessingTypePurposeAccount = clientAccountProcessingTypePurposeAccount;
	}


	public Integer getClientAccountBusinessCompanyContactId() {
		return this.clientAccountBusinessCompanyContactId;
	}


	public void setClientAccountBusinessCompanyContactId(Integer clientAccountBusinessCompanyContactId) {
		this.clientAccountBusinessCompanyContactId = clientAccountBusinessCompanyContactId;
	}


	public String getClientAccountBusinessCompanyContactTypeName() {
		return this.clientAccountBusinessCompanyContactTypeName;
	}


	public void setClientAccountBusinessCompanyContactTypeName(String clientAccountBusinessCompanyContactTypeName) {
		this.clientAccountBusinessCompanyContactTypeName = clientAccountBusinessCompanyContactTypeName;
	}


	public Boolean getClientAccountClosed() {
		return this.clientAccountClosed;
	}


	public void setClientAccountClosed(Boolean clientAccountClosed) {
		this.clientAccountClosed = clientAccountClosed;
	}
}
