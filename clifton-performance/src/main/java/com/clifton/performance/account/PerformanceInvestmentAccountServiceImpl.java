package com.clifton.performance.account;


import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchFormConfigurer;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceInvestmentAccountServiceImpl</code> handles the basic get/set/save/delete methods
 * for PerformanceInvestmentAccount table
 *
 * @author manderson
 */
@Service
public class PerformanceInvestmentAccountServiceImpl implements PerformanceInvestmentAccountService {

	private AdvancedUpdatableDAO<PerformanceInvestmentAccount, Criteria> performanceInvestmentAccountDAO;

	private BusinessContactService businessContactService;

	private InvestmentAccountRelationshipService investmentAccountRelationshipService;

	private ReportPostingController<PerformanceInvestmentAccount> reportPostingController;

	private SystemColumnValueService systemColumnValueService;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	private WorkflowDefinitionService workflowDefinitionService;


	////////////////////////////////////////////////////////////////////////////
	//////       Performance Investment Account Business Methods         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceInvestmentAccount getPerformanceInvestmentAccount(int id) {
		return getPerformanceInvestmentAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceInvestmentAccount> getPerformanceInvestmentAccountList(final PerformanceInvestmentAccountSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new PerformanceInvestmentAccountSearchFormConfigurer(searchForm, getWorkflowDefinitionService(), getInvestmentAccountRelationshipService(), getBusinessContactService());
		List<PerformanceInvestmentAccount> performanceInvestmentAccountList = getPerformanceInvestmentAccountDAO().findBySearchCriteria(config);

		if (BooleanUtils.isTrue(searchForm.getClientAccountProcessingTypePurposeAccount())) {
			CollectionUtils.getIterable(performanceInvestmentAccountList).forEach(performanceInvestmentAccount -> {
				BusinessServiceProcessingType processingType = performanceInvestmentAccount.getClientAccount().getServiceProcessingType();
				if (processingType != null && performanceInvestmentAccount.getClientAccount().getProcessingTypePurposeAccount() == null) {
					Integer purposeId = (Integer) getSystemColumnValueService().getSystemColumnCustomValue(processingType.getId(), BusinessServiceProcessingType.BUSINESS_SERVICE_PROCESSING_TYPE_COLUMN_GROUP_NAME, BusinessServiceProcessingType.ACCOUNT_PURPOSE_REPORTING_ACCOUNT_NUMBER);
					if (purposeId != null) {
						InvestmentAccountRelationshipPurpose purpose = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipPurpose(purposeId.shortValue());
						Date relationshipDate = performanceInvestmentAccount.getMeasureDate();
						InvestmentAccount relatedAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(performanceInvestmentAccount.getClientAccount().getId(), purpose.getName(), relationshipDate);
						if (relatedAccount == null) {
							// try first day of the month
							relationshipDate = DateUtils.getFirstDayOfMonth(relationshipDate);
							relatedAccount = getInvestmentAccountRelationshipService().getInvestmentAccountRelatedForPurpose(performanceInvestmentAccount.getClientAccount().getId(), purpose.getName(), relationshipDate);
						}
						if (relatedAccount != null) {
							performanceInvestmentAccount.getClientAccount().setProcessingTypePurposeAccount(relatedAccount);
						}
					}
				}
			});
		}

		return performanceInvestmentAccountList;
	}


	@Override
	public PerformanceInvestmentAccount savePerformanceInvestmentAccount(PerformanceInvestmentAccount bean) {
		return getPerformanceInvestmentAccountDAO().save(bean);
	}


	/**
	 * NOTE: URL THAT IS EXPOSED TO UI FOR DELETION CAN BE FOUND ON {@link PerformanceInvestmentAccountProcessService}
	 */
	@Override
	public void deletePerformanceInvestmentAccount(int id) {
		getPerformanceInvestmentAccountDAO().delete(id);
	}


	@Override
	public void validatePerformanceInvestmentAccountModify(int id) {
		PerformanceInvestmentAccount summary = getPerformanceInvestmentAccount(id);
		if (summary != null && summary.getWorkflowState() != null && summary.getWorkflowState().getEntityUpdateCondition() != null) {
			String errorMessage = getSystemConditionEvaluationHandler().getConditionFalseMessage(summary.getWorkflowState().getEntityUpdateCondition(), summary);
			if (!StringUtils.isEmpty(errorMessage)) {
				throw new ValidationException("Cannot modify performance summary in state [" + summary.getWorkflowState().getName() + "] because " + errorMessage);
			}
		}
	}


	/**
	 * While old and new portal are supported, need a way to delete from both portals, or delete from existing portal and just un-approve from new portal
	 */
	@Override
	public void unpostPerformanceInvestmentAccountReport(int performanceInvestmentAccountId, boolean deleteFromNewPortal) {
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccount(performanceInvestmentAccountId);

		ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
		postingConfig.setSourceEntityTableName(PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT);
		postingConfig.setSourceEntityFkFieldId(performanceInvestmentAccountId);

		getReportPostingController().unpostClientFile(postingConfig, performanceInvestmentAccount, deleteFromNewPortal);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformanceInvestmentAccount, Criteria> getPerformanceInvestmentAccountDAO() {
		return this.performanceInvestmentAccountDAO;
	}


	public void setPerformanceInvestmentAccountDAO(AdvancedUpdatableDAO<PerformanceInvestmentAccount, Criteria> performanceInvestmentAccountDAO) {
		this.performanceInvestmentAccountDAO = performanceInvestmentAccountDAO;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public void setInvestmentAccountRelationshipService(InvestmentAccountRelationshipService investmentAccountRelationshipService) {
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public ReportPostingController<PerformanceInvestmentAccount> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<PerformanceInvestmentAccount> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}
}
