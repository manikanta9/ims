package com.clifton.performance.account.process;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.performance.account.PerformanceInvestmentAccount;

import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountListCommand</code> is a command object for
 * processing a list of {@link PerformanceInvestmentAccount} objects for given properties
 *
 * @author rbrooks
 */
public class PerformanceInvestmentAccountListCommand {

	private Integer investmentAccountId;

	private Integer investmentAccountGroupId;

	private Date measureDate;

	private AccountingPeriod accountingPeriod;

	private boolean processWarnings;

	private boolean processAll;

	// Specifies whether rebuild should be executed asynchronously (runners) or synchronously.
	private boolean synchronous;

	// allows to send "live" updates of rebuild status back to the caller (assume the caller sets this field)
	private Status status;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getUniqueRunId() {
		StringBuilder id = new StringBuilder(16);
		id.append((getInvestmentAccountGroupId() == null) ? "A-" + getInvestmentAccountId() : "G-" + getInvestmentAccountGroupId());
		if (getMeasureDate() != null) {
			id.append("-").append(DateUtils.fromDateShort(getMeasureDate()));
		}
		if (isProcessWarnings()) {
			id.append("-Warnings");
		}
		return id.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public boolean isProcessWarnings() {
		return this.processWarnings;
	}


	public void setProcessWarnings(boolean processWarnings) {
		this.processWarnings = processWarnings;
	}


	public boolean isProcessAll() {
		return this.processAll;
	}


	public void setProcessAll(boolean processAll) {
		this.processAll = processAll;
	}


	public AccountingPeriod getAccountingPeriod() {
		return this.accountingPeriod;
	}


	public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
		this.accountingPeriod = accountingPeriod;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
