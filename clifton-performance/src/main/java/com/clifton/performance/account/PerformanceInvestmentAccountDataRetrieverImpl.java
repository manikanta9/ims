package com.clifton.performance.account;


import com.clifton.accounting.gl.balance.AccountingBalanceCommand;
import com.clifton.accounting.gl.balance.AccountingBalanceService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.business.service.BusinessService;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.lending.api.repo.LendingRepoApiService;
import com.clifton.lending.api.repo.Repo;
import com.clifton.lending.api.repo.RepoSearchCommand;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountDataRetrieverImpl</code> ...
 *
 * @author manderson
 */
@Component
public class PerformanceInvestmentAccountDataRetrieverImpl implements PerformanceInvestmentAccountDataRetriever {

	public static final String PERFORMANCE_INVESTMENT_ACCOUNT_CUSTOM_FIELD_GROUP = "Performance Summary Fields";
	public static final String PERFORMANCE_BUSINESS_SERVICE_CUSTOM_FIELD_GROUP = "Service Performance Summary Fields";

	/**
	 * Account Level Custom Fields
	 */
	public static final String FIELD_PERFORMANCE_REPORT = "Performance Report";
	public static final String FIELD_PERFORMANCE_REPORT_DO_NOT_POST = "Do Not Post Performance Report";
	public static final String FIELD_PERFORMANCE_REPORT_QUARTERLY_FREQUENCY = "Performance Report Post Quarterly"; // If false assumes we post monthly
	public static final String FIELD_PERFORMANCE_PORTFOLIO_RETURN_INCEPTION_OVERRIDE = "Portfolio Return Inception Date Override";
	public static final String FIELD_PERFORMANCE_DO_NOT_USE_FINANCING_RATE = "Do Not Use Financing Rate";
	public static final String FIELD_PERFORMANCE_IGNORE_CLIFTON_SUB_ACCOUNT_RELATIONSHIPS = "Ignore Clifton Sub-Account Relationships For Performance Summaries";
	public static final String FIELD_PERFORMANCE_INCLUDE_GL_OTHER_IN_DAILY_ACCOUNT_RETURN_CALCULATIONS = "Include Gain/Loss (Other) in Daily Account Return";
	public static final String FIELD_PERFORMANCE_INCLUDE_GL_OTHER_IN_PORTFOLIO_RETURN_CALCULATIONS = "Include Gain/Loss (Other) in Portfolio Return";
	public static final String FIELD_PERFORMANCE_USE_ACCOUNT_FOR_PORTFOLIO_RETURN = "Total Portfolio Value Account";
	public static final String FIELD_PERFORMANCE_USE_VALUE_FOR_PORTFOLIO_RETURN = "Static Total Portfolio Value";

	/**
	 * Asset Class Level Custom Fields
	 */
	public static final String FIELD_ASSET_CLASS_FINANCING_RATE_OVERRIDE = "Financing Rate Override";

	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingBalanceService accountingBalanceService;

	private LendingRepoApiService lendingRepoApiService;

	private SystemColumnValueHandler systemColumnValueHandler;

	private SqlHandler sqlHandler;

	////////////////////////////////////////////////////////////////
	//////       Performance Custom Field Lookup Methods      //////
	////////////////////////////////////////////////////////////////


	@Override
	public Object getPerformanceInvestmentAccountCustomFieldValue(InvestmentAccount account, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(account, PERFORMANCE_INVESTMENT_ACCOUNT_CUSTOM_FIELD_GROUP, fieldName, true);
	}


	@Override
	public Object getPerformanceBusinessServiceCustomFieldValue(BusinessService service, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(service, PERFORMANCE_BUSINESS_SERVICE_CUSTOM_FIELD_GROUP, fieldName, true);
	}


	@Override
	public Date getPerformanceInvestmentAccountInceptionDate(int clientAccountId) {
		// Parameters are client account id, Summary Inception Date Override, Chart Inception Date Override
		return getSqlHandler().queryForDate(new SqlSelectCommand(
				"SELECT CAST(Investment.GetInvestmentAccountInceptionDate(?, ?, ?) AS DATETIME)")
				.addIntegerParameterValue(clientAccountId)
				.addBooleanParameterValue(true)
				.addBooleanParameterValue(false)
		);
	}

	////////////////////////////////////////////////////////////////
	/////   Performance Position Related Data Lookup Methods  //////
	////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////
	/// Accounting Positions (Gain/Loss)
	/////////////////////////////////////////////////////////////


	@Override
	public Map<String, List<AccountingPositionDaily>> getAccountingPositionDailyMapForPerformanceAccount(InvestmentAccount investmentAccount, AccountingPeriod accountingPeriod, boolean includeLastDayOfPreviousMonth, boolean includeLastDayOfMonthInLastWeekday, boolean includeCurrency) {
		Date startDate = accountingPeriod.getStartDate();
		if (includeLastDayOfPreviousMonth) {
			// It is important to note that some calculations may require position inclusion from previous weekday
			startDate = DateUtils.getLastDayOfPreviousMonth(startDate);
		}

		List<AccountingPositionDaily> positionList = getAccountingPositionDailyListForPerformanceAccount(investmentAccount, startDate, accountingPeriod.getEndDate(), includeCurrency);
		Map<String, List<AccountingPositionDaily>> positionMap = new HashMap<>();

		Date lastWeekday = DateUtils.getLastWeekdayOfMonth(accountingPeriod.getEndDate());

		for (AccountingPositionDaily position : CollectionUtils.getIterable(positionList)) {
			Date mapDate = position.getPositionDate();

			// APD is built on all days except for weekends (except Month End).
			// So if Month End Weekday - include up to month end day's gain/loss
			if (DateUtils.compare(mapDate, lastWeekday, false) > 0 && includeLastDayOfMonthInLastWeekday) {
				mapDate = lastWeekday;
			}

			List<AccountingPositionDaily> dailyList = positionMap.computeIfAbsent(DateUtils.fromDateShort(mapDate), k -> new ArrayList<>());
			dailyList.add(position);
		}

		return positionMap;
	}


	@Override
	public List<AccountingPositionDaily> getAccountingPositionDailyListForPerformanceAccount(InvestmentAccount investmentAccount, Date startDate, Date endDate, boolean includeCurrency) {
		// Used for Position Lookup
		AccountingPositionDailySearchForm searchForm = new AccountingPositionDailySearchForm();
		searchForm.setClientInvestmentAccountId(investmentAccount.getId());
		searchForm.setStartPositionDate(startDate);
		searchForm.setEndPositionDate(endDate);

		List<AccountingPositionDaily> positionList = getAccountingPositionDailyService().getAccountingPositionDailyList(searchForm);
		if (positionList == null) {
			positionList = new ArrayList<>();
		}

		if (includeCurrency) {
			// Used for CCY Lookup
			AccountingPositionDailyLiveSearchForm liveSearchForm = new AccountingPositionDailyLiveSearchForm();
			liveSearchForm.setClientAccountId(investmentAccount.getId());
			liveSearchForm.setFromSnapshotDate(startDate);
			liveSearchForm.setSnapshotDate(endDate);
			positionList.addAll(getAccountingPositionDailyService().getAccountingPositionDailyLiveForCurrencyList(liveSearchForm));
		}
		return positionList;
	}


	@Override
	public Map<String, BigDecimal> getCashValueDailyMapForPerformanceAccount(InvestmentAccount investmentAccount, AccountingPeriod accountingPeriod, boolean includeLastWeekdayOfPreviousMonth, boolean includeAllNonPositionAssetsAndLiabilities) {
		Date startDate = accountingPeriod.getStartDate();

		if (includeLastWeekdayOfPreviousMonth) {
			//It is important to note that some calculations may require cash inclusion from previous weekday
			startDate = DateUtils.getLastWeekdayOfPreviousMonth(startDate);
		}

		AccountingBalanceCommand command = AccountingBalanceCommand.forClientAccountWithDateRange(investmentAccount.getId(), startDate, accountingPeriod.getEndDate())
				.withExcludeCashCollateral(includeAllNonPositionAssetsAndLiabilities);

		Map<Date, BigDecimal> cashValueDailyMap = includeAllNonPositionAssetsAndLiabilities ? getAccountingBalanceService().getAccountingNonPositionAssetsAndLiabilities(command) : getAccountingBalanceService().getAccountingCashBalanceMapIncludeReceivables(command);
		Map<String, BigDecimal> cashValueDailyDateStringMap = new HashMap<>();

		if (cashValueDailyMap != null) {
			for (Map.Entry<Date, BigDecimal> dateBigDecimalEntry : cashValueDailyMap.entrySet()) {
				cashValueDailyDateStringMap.put(DateUtils.fromDateShort(dateBigDecimalEntry.getKey()), dateBigDecimalEntry.getValue());
			}
		}

		return cashValueDailyDateStringMap;
	}


	@Override
	public Map<String, BigDecimal> getLendingRepoAccruedInterestMapForPerformanceAccount(PerformanceInvestmentAccount performanceAccount) {
		List<Repo> repoList = getLendingRepoListForPerformanceInvestmentAccount(performanceAccount);
		Map<String, BigDecimal> repoAccruedInterestMap = new LinkedHashMap<>();

		// No Repos - No Interest to Add - Return an Empty Map
		if (CollectionUtils.isEmpty(repoList)) {
			return repoAccruedInterestMap;
		}

		// Repo Interest applies Non-Business Day Interest to Previous Business Day
		// Open Repos have interest manually entered and this is the convention that is used
		// So lookup is changed to apply following non-business day interest to a date,
		// i.e. Friday interest also includes Saturday and Sunday
		// Monday is just for Monday
		// Day before a holiday includes the calculated interest for the holiday.
		// Moving the date to the correct date is handling by the LendingRepoService

		// NOTE: CHANGED COMPARISON OF CURRENT TO PREVIOUS TO USE WEEKDAY LOGIC

		Date date = performanceAccount.getAccountingPeriod().getStartDate();
		Date previousDate = DateUtils.addDays(date, -1);
		Date endDate = performanceAccount.getMeasureDate();

		// Build an Empty Map First
		while (DateUtils.compare(date, endDate, false) <= 0) {
			repoAccruedInterestMap.put(DateUtils.fromDateShort(date), BigDecimal.ZERO);
			date = DateUtils.getNextWeekday(date);
		}

		for (Repo repo : CollectionUtils.getIterable(repoList)) {
			// Previous Interest Amount
			Date lastDate = previousDate;
			BigDecimal lastInterestAmount = null;

			for (Map.Entry<String, BigDecimal> stringBigDecimalEntry : repoAccruedInterestMap.entrySet()) {
				Date d = DateUtils.toDate(stringBigDecimalEntry.getKey());
				Date nextWeekday = DateUtils.getNextWeekday(d);
				// REPO was traded before the next weekday
				if (repo.getTradeDate() != null && DateUtils.compare(repo.getTradeDate(), nextWeekday, false) < 0) {
					// And Maturity Settlement Date is NULL OR After next business day
					// NOTE: maturityDate FIELD ON SCREEN IS THE "ACTION DATE"
					// maturitySettlementDate FIELD ON SCREEN IS THE MATURITY DATE
					// Need to use the maturitySettlementDate since that is really the "Maturity Date"
					if (repo.getMaturitySettlementDate() == null || DateUtils.compare(repo.getMaturitySettlementDate(), nextWeekday, false) > 0) {
						if (lastInterestAmount == null) {
							lastInterestAmount = getLendingRepoApiService().getRepoAccruedInterest(repo.getId(), lastDate);
						}

						BigDecimal currentInterest = getLendingRepoApiService().getRepoAccruedInterest(repo.getId(), d);

						repoAccruedInterestMap.put(stringBigDecimalEntry.getKey(), MathUtils.add(stringBigDecimalEntry.getValue(), MathUtils.subtract(currentInterest, lastInterestAmount)));
						lastInterestAmount = currentInterest;
					}

					else {
						// Stop processing the repo if it already matured
						break;
					}
				}
				lastDate = d;
			}
		}
		return repoAccruedInterestMap;
	}


	private List<Repo> getLendingRepoListForPerformanceInvestmentAccount(PerformanceInvestmentAccount performanceAccount) {
		RepoSearchCommand searchCommand = new RepoSearchCommand();
		searchCommand.setClientInvestmentAccountId(performanceAccount.getClientAccount().getId());
		searchCommand.setExcludeWorkflowStateName("Cancelled");
		searchCommand.setTradeDateBefore(performanceAccount.getAccountingPeriod().getEndDate());
		searchCommand.setMaturitySettlementDateAfterOrNull(performanceAccount.getAccountingPeriod().getStartDate());

		return getLendingRepoApiService().getRepoList(searchCommand);
	}

	///////////////////////////////////////////////////////////
	//////           Getter and Setter Methods           //////
	///////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public LendingRepoApiService getLendingRepoApiService() {
		return this.lendingRepoApiService;
	}


	public void setLendingRepoApiService(LendingRepoApiService lendingRepoApiService) {
		this.lendingRepoApiService = lendingRepoApiService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public AccountingBalanceService getAccountingBalanceService() {
		return this.accountingBalanceService;
	}


	public void setAccountingBalanceService(AccountingBalanceService accountingBalanceService) {
		this.accountingBalanceService = accountingBalanceService;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
