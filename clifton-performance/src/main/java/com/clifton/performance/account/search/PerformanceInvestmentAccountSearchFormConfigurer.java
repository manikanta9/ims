package com.clifton.performance.account.search;

import com.clifton.business.company.contact.BusinessCompanyContact;
import com.clifton.business.contact.BusinessContactService;
import com.clifton.business.contact.BusinessContactType;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;

import java.util.List;


/**
 * <code>PerformanceInvestmentAccountSearchFormConfigurer</code> configures and applies criterion for {@link PerformanceInvestmentAccountSearchForm}s.
 *
 * @author nickk
 */
public class PerformanceInvestmentAccountSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {

	private final InvestmentAccountRelationshipService investmentAccountRelationshipService;
	private final BusinessContactService businessContactService;


	public PerformanceInvestmentAccountSearchFormConfigurer(BaseEntitySearchForm searchForm, WorkflowDefinitionService workflowDefinitionService, InvestmentAccountRelationshipService investmentAccountRelationshipService, BusinessContactService businessContactService) {
		super(searchForm, workflowDefinitionService, true);
		this.investmentAccountRelationshipService = investmentAccountRelationshipService;
		this.businessContactService = businessContactService;
		configureSearchForm();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void configureSearchForm() {
		PerformanceInvestmentAccountSearchForm searchForm = (PerformanceInvestmentAccountSearchForm) getSortableSearchForm();

		SearchRestriction issuingCompanyIdRestriction = searchForm.removeSearchRestriction("clientAccountProcessingTypePurposeAccountIssuingCompanyId");
		if (issuingCompanyIdRestriction != null) {
			searchForm.setClientAccountProcessingTypePurposeAccountIssuingCompanyId(Integer.valueOf((String) issuingCompanyIdRestriction.getValue()));
		}
		SearchRestriction clientClosedDateRestriction = searchForm.getSearchRestriction("clientAccountClosedDate");
		if (clientClosedDateRestriction != null) {
			if (clientClosedDateRestriction.getComparison() == ComparisonConditions.IS_NULL) {
				searchForm.setClientAccountClosed(Boolean.FALSE);
				searchForm.removeSearchRestriction("clientAccountClosedDate");
			}
			else {
				searchForm.setClientAccountClosed(Boolean.TRUE);
			}
		}
		if (searchForm.getClientAccountProcessingTypePurposeName() != null || searchForm.getClientAccountProcessingTypePurposeId() != null) {
			if (searchForm.getClientAccountProcessingTypePurposeId() == null) {
				InvestmentAccountRelationshipPurpose purpose = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipPurposeByName(searchForm.getClientAccountProcessingTypePurposeName());
				ValidationUtils.assertNotNull(purpose, "Unable to find Investment Account Relationship Purpose with name: " + searchForm.getClientAccountProcessingTypePurposeName());
				searchForm.setClientAccountProcessingTypePurposeId(purpose.getId());
				searchForm.setClientAccountProcessingTypePurposeName(null);
			}
			if (searchForm.getEndMeasureDate() != null) {
				searchForm.addSearchRestriction("clientAccountProcessingTypePurposeStartDate", ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, searchForm.getEndMeasureDate());
			}
			if (searchForm.getStartMeasureDate() != null) {
				searchForm.addSearchRestriction("clientAccountProcessingTypePurposeEndDate", ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL, searchForm.getStartMeasureDate());
			}
		}
		super.configureSearchForm();
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		PerformanceInvestmentAccountSearchForm searchForm = (PerformanceInvestmentAccountSearchForm) getSortableSearchForm();

		if (searchForm.getClientInvestmentAccountIdOrSubAccount() != null) {
			List<InvestmentAccountRelationship> relList = getInvestmentAccountRelationshipService().getInvestmentAccountRelationshipListForPurposeActive(
					searchForm.getClientInvestmentAccountIdOrSubAccount(), InvestmentAccountRelationshipPurpose.CLIFTON_SUB_ACCOUNT_PURPOSE_NAME, true);
			if (CollectionUtils.isEmpty(relList)) {
				criteria.add(Restrictions.eq("clientAccount.id", searchForm.getClientInvestmentAccountIdOrSubAccount()));
			}
			else {
				Object[] accountIds = BeanUtils.getPropertyValues(relList, "referenceTwo.id");
				accountIds = ArrayUtils.add(accountIds, searchForm.getClientInvestmentAccountIdOrSubAccount());
				criteria.add(Restrictions.in("clientAccount.id", accountIds));
			}
		}
		if (searchForm.getClientAccountBusinessCompanyContactId() != null && searchForm.getClientAccountBusinessCompanyContactTypeName() != null) {
			DetachedCriteria businessContactCriteria = DetachedCriteria.forClass(BusinessCompanyContact.class, "bcc");
			businessContactCriteria.setProjection(Projections.id());
			businessContactCriteria.add(Restrictions.eqProperty("bcc.referenceOne.id", getPathAlias("clientAccount.businessClient.company", criteria) + ".id"));
			if (searchForm.getClientAccountBusinessCompanyContactId() != null) {
				businessContactCriteria.add(Restrictions.eq("bcc.referenceTwo.id", searchForm.getClientAccountBusinessCompanyContactId()));
			}
			if (searchForm.getClientAccountBusinessCompanyContactTypeName() != null) {
				BusinessContactType contactType = getBusinessContactService().getBusinessContactTypeByName(searchForm.getClientAccountBusinessCompanyContactTypeName());
				businessContactCriteria.add(Restrictions.eq("bcc.contactType.id", contactType.getId()));
			}
			// date restrictions for active contacts during performance period
			businessContactCriteria.add(Restrictions.or(Restrictions.isNull("bcc.endDate"), Restrictions.gtProperty("bcc.endDate", getPathAlias("accountingPeriod", criteria) + ".startDate")));
			businessContactCriteria.add(Restrictions.or(Restrictions.isNull("bcc.startDate"), Restrictions.ltProperty("bcc.startDate", getPathAlias("accountingPeriod", criteria) + ".endDate")));

			criteria.add(Subqueries.exists(businessContactCriteria));
		}
		if (searchForm.getClientAccountClosed() != null) {
			Criterion closedClientCriterion = Restrictions.or(
					Restrictions.like(getPathAlias("clientAccount.workflowState", criteria) + ".name", "%Closed%"),
					Restrictions.like(getPathAlias("clientAccount.workflowState", criteria) + ".name", "%Terminat%")
			);
			if (searchForm.getClientAccountClosed()) {
				criteria.add(closedClientCriterion);
			}
			else {
				criteria.add(Restrictions.not(closedClientCriterion));
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentAccountRelationshipService getInvestmentAccountRelationshipService() {
		return this.investmentAccountRelationshipService;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}
}
