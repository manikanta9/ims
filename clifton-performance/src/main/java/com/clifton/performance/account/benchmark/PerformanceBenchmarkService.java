package com.clifton.performance.account.benchmark;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.performance.account.benchmark.search.PerformanceBenchmarkSearchForm;
import com.clifton.performance.account.benchmark.search.PerformanceSummaryBenchmarkSearchForm;

import java.util.List;


public interface PerformanceBenchmarkService {

	////////////////////////////////////////////////////////////////////////////
	///////             Performance Benchmark Business Methods           ///////
	////////////////////////////////////////////////////////////////////////////


	public PerformanceBenchmark getPerformanceBenchmark(int id);


	public List<PerformanceBenchmark> getPerformanceBenchmarkList(PerformanceBenchmarkSearchForm searchForm);


	public PerformanceBenchmark savePerformanceBenchmark(PerformanceBenchmark bean);


	public void deletePerformanceBenchmark(int id);

	////////////////////////////////////////////////////////////////////////////
	/////          Performance Summary Benchmark Business Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	public PerformanceSummaryBenchmark getPerformanceSummaryBenchmark(int id);


	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkList(PerformanceSummaryBenchmarkSearchForm searchForm);


	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkListBySummary(int summaryId);


	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkListByBenchmark(int benchmarkId);


	@DoNotAddRequestMapping
	public PerformanceSummaryBenchmark savePerformanceSummaryBenchmark(PerformanceSummaryBenchmark bean);


	@DoNotAddRequestMapping
	public void savePerformanceSummaryBenchmarkList(List<PerformanceSummaryBenchmark> list);


	@DoNotAddRequestMapping
	public void savePerformanceSummaryBenchmarkListForSummary(int summaryId, List<PerformanceSummaryBenchmark> list);


	public void deletePerformanceSummaryBenchmarkListBySummary(int summaryId);
}
