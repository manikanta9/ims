package com.clifton.performance.account.benchmark.process;

import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;

import java.util.List;


/**
 * @author nickk
 */
public class PerformanceInvestmentAccountBenchmarkConfig extends PerformanceInvestmentAccountConfig {

	private List<PerformanceSummaryBenchmark> summaryBenchmarkList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountBenchmarkConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		super(performanceAccount, processingType);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<PerformanceSummaryBenchmark> getSummaryBenchmarkList() {
		return this.summaryBenchmarkList;
	}


	public void setSummaryBenchmarkList(List<PerformanceSummaryBenchmark> summaryBenchmarkList) {
		this.summaryBenchmarkList = summaryBenchmarkList;
	}
}
