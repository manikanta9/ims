package com.clifton.performance.account.process.processor;


import com.clifton.core.comparison.Ordered;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;

import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountProcessor</code> interface should be implemented by each type of
 * performance calculation that we perform. Handles service type specific processing and proper handling of deletes.
 * The handler implementations are mapped to the service type's processing type to determine what to call
 * from {@link PerformanceInvestmentAccountProcessService}
 *
 * @author manderson
 */
public interface PerformanceInvestmentAccountProcessor<T extends PerformanceInvestmentAccountConfig> extends Ordered {

	/**
	 * Handles deleting of all the many side tables associated with the performance for the account
	 */
	public void deletePerformanceInvestmentAccountProcessing(int performanceAccountId);


	/**
	 * Creates an instance of {@link PerformanceInvestmentAccountConfig} that can be used for processing the {@link PerformanceInvestmentAccount}.
	 */
	public T createPerformanceInvestmentAccountConfig(PerformanceInvestmentAccount performanceInvestmentAccount, PerformanceInvestmentAccountProcessingTypes processingType);


	/**
	 * Processes a {@link PerformanceInvestmentAccount} according to the provided configuration and date.
	 */
	public void processPerformanceInvestmentAccount(T performanceInvestmentAccountConfig, Date date);
}
