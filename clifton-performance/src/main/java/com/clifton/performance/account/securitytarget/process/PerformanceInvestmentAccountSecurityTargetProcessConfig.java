package com.clifton.performance.account.securitytarget.process;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayout;
import com.clifton.investment.instrument.event.payout.InvestmentSecurityEventPayoutService;
import com.clifton.investment.instrument.event.payout.search.InvestmentSecurityEventPayoutSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetDailyReturn;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.search.TradeSearchForm;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountSecurityTargetProcessConfig</code> object is used for processing
 * {@link PerformanceInvestmentAccount}s {@link com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetDailyReturn}s and associated tables.  It pre-loads necessary data which can be
 * passed around to various processing methods so look ups can be re-used.
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountSecurityTargetProcessConfig extends PerformanceInvestmentAccountConfig {


	/**
	 * Custom field set on the processing type if the benchmark / security target return should NOT be calculated
	 * This should only be set for types of accounts that report on Options only returns (OARS, RPS, DPS)
	 */
	private boolean doNotCalculateBenchmarkReturn;

	private List<InvestmentAccountSecurityTarget> accountSecurityTargetList;

	private List<PerformanceInvestmentAccountSecurityTargetDailyReturn> performanceSecurityTargetDailyReturnList;

	private Map<Date, List<Trade>> optionAssignmentTradeDateListMap;

	private final Map<Integer, List<InvestmentSecurityEvent>> securityEventListMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountSecurityTargetProcessConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		super(performanceAccount, processingType);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<InvestmentAccountSecurityTarget> getAccountSecurityTargetList(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		if (this.accountSecurityTargetList == null) {
			AccountingPeriod accountingPeriod = getPerformanceAccount().getAccountingPeriod();
			InvestmentAccount account = getPerformanceAccount().getClientAccount();
			InvestmentAccountSecurityTargetSearchForm searchForm = new InvestmentAccountSecurityTargetSearchForm();
			searchForm.setClientInvestmentAccountId(account.getId());
			// Pull All Active during the period (start on last day of previous month)
			searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, accountingPeriod.getEndDate()));
			searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL, DateUtils.getLastDayOfPreviousMonth(accountingPeriod.getStartDate())));
			this.accountSecurityTargetList = investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(searchForm);
		}
		return this.accountSecurityTargetList;
	}


	public List<Trade> getOptionAssignmentTradeListForDate(Date date, TradeService tradeService) {
		if (this.optionAssignmentTradeDateListMap == null) {
			TradeSearchForm searchForm = new TradeSearchForm();
			searchForm.setClientInvestmentAccountId(getPerformanceAccount().getClientAccount().getId());
			searchForm.setIncludeTradeGroupTypeName(TradeGroupType.GroupTypes.OPTION_ASSIGNMENT.getTypeName());
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, getPerformanceAccount().getAccountingPeriod().getStartDate()));
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, getPerformanceAccount().getAccountingPeriod().getEndDate()));
			searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
			List<Trade> tradeList = tradeService.getTradeList(searchForm);
			this.optionAssignmentTradeDateListMap = BeanUtils.getBeansMap(tradeList, Trade::getTradeDate);
		}
		return this.optionAssignmentTradeDateListMap.get(date);
	}


	public List<InvestmentSecurityEvent> getInvestmentSecurityEventList(InvestmentSecurity security, Date exDate, String eventTypeName, InvestmentSecurityEventService investmentSecurityEventService) {
		if (!this.securityEventListMap.containsKey(security.getId())) {
			InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
			searchForm.setSecurityId(security.getId());
			searchForm.addSearchRestriction(new SearchRestriction("exDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.getPreviousWeekday(getPerformanceAccount().getAccountingPeriod().getStartDate())));
			searchForm.addSearchRestriction(new SearchRestriction("exDate", ComparisonConditions.LESS_THAN_OR_EQUALS, getPerformanceAccount().getAccountingPeriod().getEndDate()));
			searchForm.setStatusEventJournalAllowed(true); // Only include where status is "approved" and would allow event journals to be generated
			this.securityEventListMap.put(security.getId(), investmentSecurityEventService.getInvestmentSecurityEventList(searchForm));
		}
		return BeanUtils.filter(this.securityEventListMap.get(security.getId()), securityEvent -> (DateUtils.isEqualWithoutTime(exDate, securityEvent.getExDate()) && (StringUtils.isEmpty(eventTypeName) || StringUtils.isEqualIgnoreCase(eventTypeName, securityEvent.getType().getName()))));
	}


	/**
	 * Returns the payouts for the event.  If nothing, then we need to use the event.
	 * Note: This is not cached as an event would only be included once and most cases just one cash div event that we'll need to include.
	 */
	public List<InvestmentSecurityEventPayout> getInvestmentSecurityEventPayoutListForEvent(InvestmentSecurityEvent event, InvestmentSecurityEventPayoutService investmentSecurityEventPayoutService) {
		// Only look it up if potentially NOT a single payment type
		if (!event.getType().isSinglePayoutOnly()) {
			InvestmentSecurityEventPayoutSearchForm payoutSearchForm = new InvestmentSecurityEventPayoutSearchForm();
			payoutSearchForm.setSecurityEventId(event.getId());
			payoutSearchForm.setDeleted(false);
			payoutSearchForm.setDefaultElection(true);
			return investmentSecurityEventPayoutService.getInvestmentSecurityEventPayoutList(payoutSearchForm);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isDoNotCalculateBenchmarkReturn() {
		return this.doNotCalculateBenchmarkReturn;
	}


	public void setDoNotCalculateBenchmarkReturn(boolean doNotCalculateBenchmarkReturn) {
		this.doNotCalculateBenchmarkReturn = doNotCalculateBenchmarkReturn;
	}


	public List<PerformanceInvestmentAccountSecurityTargetDailyReturn> getPerformanceSecurityTargetDailyReturnList() {
		return this.performanceSecurityTargetDailyReturnList;
	}


	public void setPerformanceSecurityTargetDailyReturnList(List<PerformanceInvestmentAccountSecurityTargetDailyReturn> performanceSecurityTargetDailyReturnList) {
		this.performanceSecurityTargetDailyReturnList = performanceSecurityTargetDailyReturnList;
	}
}
