package com.clifton.performance.account;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.Entity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.rule.violation.RuleViolationAware;

import java.util.Date;


/**
 * The <code>PerformanceAccountInfo</code> contains definitions to be used by Account Performance objects
 * See {@link com.clifton.performance.account.PerformanceInvestmentAccount} and {@link PerformanceCompositeInvestmentAccountPerformance}
 *
 * @author apopp
 */
public interface PerformanceAccountInfo extends LabeledObject, RuleViolationAware, Entity<Integer> {

	/**
	 * Returns the investment account associated to this performance
	 */
	public InvestmentAccount getClientAccount();


	/**
	 * Returns the accounting period associated to this performance
	 */
	public AccountingPeriod getAccountingPeriod();


	/**
	 * AccountingPeriod End Date
	 */
	public Date getMeasureDate();
}
