package com.clifton.performance.account.securitytarget;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountSecurityTargetDailyReturn</code> contains performance return information for accounts that utilize security targets.
 * The daily values are a sum of all of the targets for the account.  For reporting we don't need to break them out if there are multiple.
 * <p>
 * NOTE: Target Monthly Returns are Linked, but Options Monthly Returns (Net and Gross) are Summed
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountSecurityTargetDailyReturn extends BaseEntity<Integer> {

	private PerformanceInvestmentAccount performanceInvestmentAccount;

	private Date measureDate;

	/**
	 * The total value of the security targets for the account on the measure date
	 * plus all dividend income from the GL for the target securities since inception to measure date.
	 */
	private BigDecimal securityTargetMarketValue;

	/**
	 * Gain/Loss of the security target underlying securities on the measure date
	 * Calculated by retrieving the measure date's targets using previous day's price and that change = gain/loss
	 * If there is a Cash Dividend Payment Ex Date = measure date and the security target is a quantity target the calculated div amount is also included as gain/loss
	 */
	private BigDecimal securityTargetGainLoss;

	/**
	 * Return = Security Target Gain/Loss / PREVIOUS Day's security target market value
	 */
	private BigDecimal securityTargetReturn;

	/**
	 * Market Value of all Options in the account
	 */
	private BigDecimal optionsMarketValue;

	/**
	 * Gain/Loss for the Options
	 * Note: If there is an assignment, the gain/loss as calculated by the position is re-calculated so it doesn't skew performance
	 * Because: In GL assigned options are generally BC @ 0 and stock/ETF is sold at strike price.
	 * For performance reporting we need to track option "intrinsic" at time of assignment and use that to calculate an accurate gain/loss for the option
	 */
	private BigDecimal optionsGainLoss;

	/**
	 * Return = Options Gain/Loss / PREVIOUS Day's security target market value
	 */
	private BigDecimal optionsGrossReturn;


	/**
	 * Return = Options Gain/Loss - Management Fee / PREVIOUS Day's security target market value
	 */
	private BigDecimal optionsNetReturn;

	/**
	 * Applies only on the last day of the month
	 * WARNING: Currently this is pulling Projected Revenue amount from Accounting Period Closing
	 * This is not necessarily the actual amount billed - however without posting invoice amounts to the GL or adding dependency on billing here it's the only way to calculate
	 * The Westport accounts that use this currently are billed monthly, so projected should be actual revenue for the month.
	 */
	private BigDecimal managementFee;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getPerformanceInvestmentAccount() != null) {
			return getPerformanceInvestmentAccount().getClientAccount().getLabel() + " - " + DateUtils.fromDateShort(getMeasureDate());
		}
		return null;
	}


	public BigDecimal getPortfolioGrossReturn() {
		return MathUtils.add(getSecurityTargetReturn(), getOptionsGrossReturn());
	}


	public BigDecimal getPortfolioNetReturn() {
		return MathUtils.add(getSecurityTargetReturn(), getOptionsNetReturn());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccount getPerformanceInvestmentAccount() {
		return this.performanceInvestmentAccount;
	}


	public void setPerformanceInvestmentAccount(PerformanceInvestmentAccount performanceInvestmentAccount) {
		this.performanceInvestmentAccount = performanceInvestmentAccount;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public BigDecimal getSecurityTargetMarketValue() {
		return this.securityTargetMarketValue;
	}


	public void setSecurityTargetMarketValue(BigDecimal securityTargetMarketValue) {
		this.securityTargetMarketValue = securityTargetMarketValue;
	}


	public BigDecimal getSecurityTargetGainLoss() {
		return this.securityTargetGainLoss;
	}


	public void setSecurityTargetGainLoss(BigDecimal securityTargetGainLoss) {
		this.securityTargetGainLoss = securityTargetGainLoss;
	}


	public BigDecimal getSecurityTargetReturn() {
		return this.securityTargetReturn;
	}


	public void setSecurityTargetReturn(BigDecimal securityTargetReturn) {
		this.securityTargetReturn = securityTargetReturn;
	}


	public BigDecimal getOptionsMarketValue() {
		return this.optionsMarketValue;
	}


	public void setOptionsMarketValue(BigDecimal optionsMarketValue) {
		this.optionsMarketValue = optionsMarketValue;
	}


	public BigDecimal getOptionsGainLoss() {
		return this.optionsGainLoss;
	}


	public void setOptionsGainLoss(BigDecimal optionsGainLoss) {
		this.optionsGainLoss = optionsGainLoss;
	}


	public BigDecimal getManagementFee() {
		return this.managementFee;
	}


	public void setManagementFee(BigDecimal managementFee) {
		this.managementFee = managementFee;
	}


	public BigDecimal getOptionsGrossReturn() {
		return this.optionsGrossReturn;
	}


	public void setOptionsGrossReturn(BigDecimal optionsGrossReturn) {
		this.optionsGrossReturn = optionsGrossReturn;
	}


	public BigDecimal getOptionsNetReturn() {
		return this.optionsNetReturn;
	}


	public void setOptionsNetReturn(BigDecimal optionsNetReturn) {
		this.optionsNetReturn = optionsNetReturn;
	}
}
