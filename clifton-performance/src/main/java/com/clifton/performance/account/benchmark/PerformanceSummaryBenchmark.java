package com.clifton.performance.account.benchmark;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.performance.account.PerformanceInvestmentAccount;

import java.math.BigDecimal;


/**
 * The <code>PerformanceSummaryBenchmark</code> relates a
 * {@link PerformanceInvestmentAccount} to a {@link PerformanceBenchmark}
 *
 * @author Mary Anderson
 */
public class PerformanceSummaryBenchmark extends ManyToManyEntity<PerformanceInvestmentAccount, PerformanceBenchmark, Integer> {

	private BigDecimal monthToDateReturn;
	private BigDecimal monthToDateReturnOverride;

	private BigDecimal quarterToDateReturn;
	private BigDecimal yearToDateReturn;
	private BigDecimal inceptionToDateReturn;

	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////


	public BigDecimal getCoalesceMonthToDateReturnOverride() {
		if (getMonthToDateReturnOverride() != null) {
			return getMonthToDateReturnOverride();
		}
		return getMonthToDateReturn();
	}

	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////


	public BigDecimal getMonthToDateReturn() {
		return this.monthToDateReturn;
	}


	public void setMonthToDateReturn(BigDecimal monthToDateReturn) {
		this.monthToDateReturn = monthToDateReturn;
	}


	public BigDecimal getQuarterToDateReturn() {
		return this.quarterToDateReturn;
	}


	public void setQuarterToDateReturn(BigDecimal quarterToDateReturn) {
		this.quarterToDateReturn = quarterToDateReturn;
	}


	public BigDecimal getYearToDateReturn() {
		return this.yearToDateReturn;
	}


	public void setYearToDateReturn(BigDecimal yearToDateReturn) {
		this.yearToDateReturn = yearToDateReturn;
	}


	public BigDecimal getInceptionToDateReturn() {
		return this.inceptionToDateReturn;
	}


	public void setInceptionToDateReturn(BigDecimal inceptionToDateReturn) {
		this.inceptionToDateReturn = inceptionToDateReturn;
	}


	public BigDecimal getMonthToDateReturnOverride() {
		return this.monthToDateReturnOverride;
	}


	public void setMonthToDateReturnOverride(BigDecimal monthToDateReturnOverride) {
		this.monthToDateReturnOverride = monthToDateReturnOverride;
	}
}
