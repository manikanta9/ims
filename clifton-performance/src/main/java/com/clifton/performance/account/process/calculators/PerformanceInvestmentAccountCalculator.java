package com.clifton.performance.account.process.calculators;


import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;

import java.util.Date;


/**
 * The <code>PerformanceInvestmentAccountCalculator</code> ...
 *
 * @author manderson
 */
public interface PerformanceInvestmentAccountCalculator<T extends PerformanceInvestmentAccountConfig> {

	/**
	 * Performance Full processing of this step.  If date is passed will only re-process information that applies to that specific date
	 * Example: Performance Summaries - users can switch the run for a date or add overrides.  We would re-process that specific date's values
	 * and then also for monthly total will update monthly totals.
	 */
	public void calculate(T config, Date date);


	/**
	 * Applies historical return calculations - Likely only used by the calculation for the {@link PerformanceInvestmentAccount} calculator
	 * to populate QTD, YTD, and ITD values
	 * <p/>
	 * Returns true to indicate that something was updated/processed so we know to call save only in that case
	 */
	public boolean calculateHistoricalReturns(T config);


	/**
	 * Save step is done independently so we can re-run performance
	 * and either "preview" or use for comparing current value to saved value (for testing)
	 */
	public void saveResults(T config, Date date);
}
