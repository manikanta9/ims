package com.clifton.performance.account.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.List;


/**
 * PerformanceAccountAssetClassBenchmarkMissingRuleEvaluator checks all active asset classes for the account that they have a benchmark selected
 * <p>
 *
 * @author manderson
 */
public class PerformanceAccountAssetClassBenchmarkMissingRuleEvaluator extends BaseRuleEvaluator<PerformanceInvestmentAccount, PerformanceInvestmentAccountRuleEvaluatorContext> {


	@Override
	public List<RuleViolation> evaluateRule(PerformanceInvestmentAccount performanceInvestmentAccount, RuleConfig ruleConfig, PerformanceInvestmentAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		List<InvestmentAccountAssetClass> accountAssetClassList = context.getInvestmentAccountAssetClassList(performanceInvestmentAccount);

		for (InvestmentAccountAssetClass accountAssetClass : CollectionUtils.getIterable(accountAssetClassList)) {
			EntityConfig entityConfig = ruleConfig.getEntityConfig(BeanUtils.getIdentityAsLong(accountAssetClass));
			if (entityConfig != null && !entityConfig.isExcluded()) {
				if (!accountAssetClass.getAssetClass().isCash() && accountAssetClass.getBenchmarkSecurity() == null) {
					violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceInvestmentAccount.getId(), accountAssetClass.getId()));
				}
			}
		}
		return violationList;
	}
}
