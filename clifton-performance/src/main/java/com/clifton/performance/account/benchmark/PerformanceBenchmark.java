package com.clifton.performance.account.benchmark;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.rates.InvestmentInterestRateIndex;

import java.util.Date;


/**
 * The <code>PerformanceBenchmark</code> class defines a single benchmark used by client's investment account.
 * Each client account can have multiple benchmarks.
 *
 * @author vgomelsky
 */
public class PerformanceBenchmark extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentAccount clientAccount;

	// only benchmark security or index can/must be set
	private InvestmentSecurity benchmarkSecurity;
	private InvestmentInterestRateIndex benchmarkInterestRateIndex;

	// optional name that can override benchmark name
	private String name;
	private int benchmarkOrder;

	/**
	 * Dates this benchmark applies
	 */
	private Date startDate;
	private Date endDate;

	/**
	 * Applies only if Start Date entered.
	 * Used in Performance Summary reports to display N/A if the benchmark started
	 * after the period start.
	 * Example Benchmark Started on 3/1/2012.
	 * With this option checked, Summary on 3/31/2012 will have MTD value, N/A for QTD, YTD, MTD
	 * On or after 4/31/2012 summary, benchmark will have MTD, QTD and N/A for YTD, ITD values
	 * On or after 1/31/2013 summary and anything after, benchmark will have MTD, QTD, YTD and N/A ITD value
	 */
	private boolean hideReturnIfStartAfterPeriod;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(20);
		if (getClientAccount() != null) {
			sb.append(getClientAccount().getLabel());
		}
		sb.append(" - ");
		if (getBenchmarkSecurity() != null) {
			sb.append(getBenchmarkSecurity().getLabel());
		}
		else if (getBenchmarkInterestRateIndex() != null) {
			sb.append(getBenchmarkInterestRateIndex().getLabel());
		}
		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	public String getDateLabel() {
		return DateUtils.fromDateRange(getStartDate(), getEndDate(), true, false);
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public InvestmentInterestRateIndex getBenchmarkInterestRateIndex() {
		return this.benchmarkInterestRateIndex;
	}


	public void setBenchmarkInterestRateIndex(InvestmentInterestRateIndex benchmarkInterestRateIndex) {
		this.benchmarkInterestRateIndex = benchmarkInterestRateIndex;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getBenchmarkOrder() {
		return this.benchmarkOrder;
	}


	public void setBenchmarkOrder(int benchmarkOrder) {
		this.benchmarkOrder = benchmarkOrder;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public boolean isHideReturnIfStartAfterPeriod() {
		return this.hideReturnIfStartAfterPeriod;
	}


	public void setHideReturnIfStartAfterPeriod(boolean hideReturnIfStartAfterPeriod) {
		this.hideReturnIfStartAfterPeriod = hideReturnIfStartAfterPeriod;
	}
}
