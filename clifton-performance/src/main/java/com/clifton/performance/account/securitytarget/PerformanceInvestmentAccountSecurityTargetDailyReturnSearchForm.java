package com.clifton.performance.account.securitytarget;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
public class PerformanceInvestmentAccountSecurityTargetDailyReturnSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(searchField = "performanceInvestmentAccount.id")
	private Integer performanceInvestmentAccountId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPerformanceInvestmentAccountId() {
		return this.performanceInvestmentAccountId;
	}


	public void setPerformanceInvestmentAccountId(Integer performanceInvestmentAccountId) {
		this.performanceInvestmentAccountId = performanceInvestmentAccountId;
	}
}
