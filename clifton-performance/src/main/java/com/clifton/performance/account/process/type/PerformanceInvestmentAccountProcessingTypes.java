package com.clifton.performance.account.process.type;


/**
 * The <code>PerformanceInvestmentAccountProcessingTypes</code> ...
 *
 * @author manderson
 */
public enum PerformanceInvestmentAccountProcessingTypes {

	PROCESS(true, false, true), //
	CALCULATE_HISTORICAL_RETURNS_ONLY(false, true, true), //
	PREVIEW_HISTORICAL_RETURNS_ONLY(false, true, false), //
	PREVIEW(true, false, false);


	PerformanceInvestmentAccountProcessingTypes(boolean calculate, boolean calculateHistoricalReturnsOnly, boolean saveResults) {
		this.calculate = calculate;
		this.calculateHistoricalReturnsOnly = calculateHistoricalReturnsOnly;
		this.saveResults = saveResults;
	}


	private final boolean calculate;
	private final boolean calculateHistoricalReturnsOnly;
	private final boolean saveResults;


	public boolean isCalculate() {
		return this.calculate;
	}


	public boolean isCalculateHistoricalReturnsOnly() {
		return this.calculateHistoricalReturnsOnly;
	}


	public boolean isSaveResults() {
		return this.saveResults;
	}
}
