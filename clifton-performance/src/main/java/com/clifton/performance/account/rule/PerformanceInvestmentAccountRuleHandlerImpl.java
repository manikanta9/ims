package com.clifton.performance.account.rule;

import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.rule.evaluator.RuleEvaluatorService;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>PerformanceInvestmentAccountRuleHandlerImpl</code> is a helper class for workings with Rules and PerformanceInvestmentAccounts
 *
 * @author manderson
 */
@Component
public class PerformanceInvestmentAccountRuleHandlerImpl implements PerformanceInvestmentAccountRuleHandler {

	private static final String RULE_DEFINITION_NAME_VALIDATION_EXCEPTION = "Performance Summary Validation Warning";

	private RuleEvaluatorService ruleEvaluatorService;
	private RuleViolationService ruleViolationService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void clearPerformanceInvestmentAccountNotIgnorableWarnings(int performanceAccountId) {
		getRuleViolationService().deleteRuleViolationListByLinkedEntityNotIgnorable(PerformanceInvestmentAccount.RULE_CATEGORY_PERFORMANCE_INVESTMENT_ACCOUNT, performanceAccountId);
	}


	@Override
	public void processPerformanceInvestmentAccountRules(int performanceAccountId) {
		getRuleEvaluatorService().executeRules(PerformanceInvestmentAccount.RULE_CATEGORY_PERFORMANCE_INVESTMENT_ACCOUNT, MathUtils.getNumberAsLong(performanceAccountId), null);
	}


	@Override
	public void savePerformanceInvestmentAccountRuleViolationFromException(ValidationExceptionWithCause exception, int performanceAccountId) {
		getRuleViolationService().saveRuleViolationFromException(RULE_DEFINITION_NAME_VALIDATION_EXCEPTION, performanceAccountId, exception);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public RuleEvaluatorService getRuleEvaluatorService() {
		return this.ruleEvaluatorService;
	}


	public void setRuleEvaluatorService(RuleEvaluatorService ruleEvaluatorService) {
		this.ruleEvaluatorService = ruleEvaluatorService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
