package com.clifton.performance.account.process.jobs;

import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.util.runner.Task;


/**
 * @author terrys
 */
public abstract class BasePerformanceAccountProcessorJob implements Task {

	/**
	 * Required: Account group to create/process summaries for
	 */
	private Integer investmentAccountGroupId;

	/**
	 * Allows to process current period for partial period reporting
	 * If false, will process previous month accounting period.
	 */
	private boolean processCurrentAccountingPeriod;

	/**
	 * If true, uses "today" to determine current accounting period, else uses Previous Weekday
	 * The difference would be determined on when the process runs - if it runs in the morning, then we want to use previous weekday, if it runs at night then we would use today
	 * Example - on the morning of 5/1 we still want to process April summaries to Month End - on 5/2 we'll transition to the May ones
	 */
	private boolean usePreviousWeekdayForAccountPeriodRetrieval;

	private AccountingPeriodService accountingPeriodService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public boolean isProcessCurrentAccountingPeriod() {
		return this.processCurrentAccountingPeriod;
	}


	public void setProcessCurrentAccountingPeriod(boolean processCurrentAccountingPeriod) {
		this.processCurrentAccountingPeriod = processCurrentAccountingPeriod;
	}


	public boolean isUsePreviousWeekdayForAccountPeriodRetrieval() {
		return this.usePreviousWeekdayForAccountPeriodRetrieval;
	}


	public void setUsePreviousWeekdayForAccountPeriodRetrieval(boolean usePreviousWeekdayForAccountPeriodRetrieval) {
		this.usePreviousWeekdayForAccountPeriodRetrieval = usePreviousWeekdayForAccountPeriodRetrieval;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}
}
