package com.clifton.performance.account.process;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.process.processor.PerformanceInvestmentAccountProcessor;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import com.clifton.performance.account.rule.PerformanceInvestmentAccountRuleHandler;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import com.clifton.rule.violation.RuleViolationUtil;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.rule.violation.status.RuleViolationStatusService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountProcessServiceImpl</code> handles
 * the processing of PerformanceInvestmentAccount table
 *
 * @author manderson
 */
@Service
public class PerformanceInvestmentAccountProcessServiceImpl<T extends PerformanceInvestmentAccountConfig> implements PerformanceInvestmentAccountProcessService {

	private AccountingPeriodService accountingPeriodService;

	private InvestmentAccountService investmentAccountService;

	private PerformanceInvestmentAccountRuleHandler performanceInvestmentAccountRuleHandler;
	private PerformanceInvestmentAccountService performanceInvestmentAccountService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private RuleViolationStatusService ruleViolationStatusService;

	private RunnerHandler runnerHandler;


	private Map<String, PerformanceInvestmentAccountProcessor<T>> performanceInvestmentAccountProcessorMap;

	/////////////////////////////////////////////////////////////////////////////
	//////   PerformanceInvestmentAccount Processing Methods         ////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a runner to perform bulk PerformanceInvestmentAccount processing.
	 *
	 * @param command - object contains account and processing information on what to process
	 */
	@Override
	public String processPerformanceInvestmentAccountList(final PerformanceInvestmentAccountListCommand command) {
		ValidationUtils.assertMutuallyExclusive("An account or account group selection is required.", command.getInvestmentAccountId(), command.getInvestmentAccountGroupId());
		if (command.isSynchronous()) {
			if (command.isProcessWarnings()) {
				return doProcessPerformanceInvestmentAccountWarningList(command, command.getStatus()).getMessage();
			}
			else {
				return doProcessPerformanceInvestmentAccountList(command, command.getStatus()).getMessage();
			}
		}

		final String runId = command.getUniqueRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + DateUtils.fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("PERFORMANCE-ACCOUNT", runId, now, statusHolder) {

			@Override
			public void run() {
				if (command.isProcessWarnings()) {
					doProcessPerformanceInvestmentAccountWarningList(command, getStatus());
				}
				else {
					doProcessPerformanceInvestmentAccountList(command, getStatus());
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Started processing requested performance summaries. Processing will be completed shortly.";
	}


	private Status doProcessPerformanceInvestmentAccountWarningList(PerformanceInvestmentAccountListCommand command, Status status) {
		if (status == null) {
			status = Status.ofEmptyMessage();
		}
		PerformanceInvestmentAccountSearchForm searchForm = getPerformanceInvestmentAccountSearchFormForCommand(command);
		searchForm.setWorkflowStateName("Draft");
		searchForm.setViolationStatusNames(new String[]{RuleViolationStatus.RuleViolationStatusNames.PROCESSED_FAILED.name(), RuleViolationStatus.RuleViolationStatusNames.PROCESSED_VIOLATIONS.name()});

		List<PerformanceInvestmentAccount> existingSummaryList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);

		int totalCount = CollectionUtils.getSize(existingSummaryList);
		if (totalCount == 0) {
			status.setMessage("There are no draft summaries with warnings that match selected filters");
			return status;
		}

		int errorCount = 0;
		int failedCount = 0;
		int successCount = 0;
		StringBuilder errors = new StringBuilder(16);
		for (PerformanceInvestmentAccount summary : CollectionUtils.getIterable(existingSummaryList)) {

			try {
				processPerformanceInvestmentAccountWarnings(summary.getId());
				summary = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(summary.getId());

				if (!RuleViolationUtil.isRuleViolationAwareReadyForUse(summary)) {
					failedCount++;
				}
				else {
					successCount++;
				}

				// Update Runner status
				int totalRemaining = totalCount - (successCount + failedCount);

				status.setMessage("Total Remaining: " + totalRemaining + " out of " + totalCount + "; Errors Resolved: " + successCount + "; Errors Persisting: " + failedCount
						+ "]. Please click on Summary History tab see updated warning statuses. " + (errorCount > 0 ? "Errors: " + errors : ""));
			}
			catch (Throwable e) {
				errorCount++;
				String errorMsg = "[" + summary.getClientAccount().getNumber() + "]:" + ExceptionUtils.getOriginalMessage(e);
				status.addError(errorMsg);
				LogUtils.errorOrInfo(getClass(), "Error processing performance for account " + errorMsg, e);
				errors.append("\n\n");
				errors.append("Failed processing warnings for account [").append(summary.getClientAccount().getNumber()).append("]: ");
				errors.append(ExceptionUtils.getDetailedMessage(e));
			}
		}

		// Return final status
		status.setMessage("Processing Complete. Total Summaries: " + totalCount + "; Errors Resolved: " + successCount + "; Errors Persisting: " + failedCount
				+ ". Please click on Summary History tab see updated warning statuses. " + (errorCount > 0 ? "Errors: " + errors : ""));
		return status;
	}


	/**
	 * Performs bulk Performance Summary processing.  Called by an instance of Runner in
	 * processProductPerformanceOverlayRunListForSummaryList() method.
	 *
	 * @param command Command object encapsulating "investmentAccountId", "investmentAccountGroupId", "measureDate", and "processAll" parameters
	 * @param status  Status string that updates the Runner's status field.
	 * @return Final status string after processing is complete
	 */
	private Status doProcessPerformanceInvestmentAccountList(PerformanceInvestmentAccountListCommand command, Status status) {
		if (status == null) {
			status = Status.ofEmptyMessage();
		}
		PerformanceInvestmentAccountSearchForm searchForm = getPerformanceInvestmentAccountSearchFormForCommand(command);

		// List of accounts to run summaries for.  If only one account is chosen by the user, list will contain only one
		List<InvestmentAccount> accountList;
		if (command.getInvestmentAccountId() != null) {
			accountList = CollectionUtils.createList(getInvestmentAccountService().getInvestmentAccount(command.getInvestmentAccountId()));
		}
		else {
			InvestmentAccountSearchForm accountSearchForm = new InvestmentAccountSearchForm();
			accountSearchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
			accountList = getInvestmentAccountService().getInvestmentAccountList(accountSearchForm);
		}

		int totalAccounts = CollectionUtils.getSize(accountList);
		if (totalAccounts == 0) {
			status.setMessage("No investment accounts match search criteria.");
			return status;
		}

		// Get list of existing summaries for the specified accounting period
		List<PerformanceInvestmentAccount> existingSummaryList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);

		int errorCount = 0;
		int failedCount = 0;
		int successCount = 0;
		int createdCount = 0;
		StringBuilder errors = new StringBuilder(16);
		for (InvestmentAccount account : accountList) {
			try {
				// For each account in accountList, filter existing summaries
				PerformanceInvestmentAccount summary = CollectionUtils.getOnlyElement(BeanUtils.filter(existingSummaryList, PerformanceInvestmentAccount::getClientAccount, account));

				//If no existing summary, create a new one and save
				if (summary == null) {
					// If period STARTS after account terminated - skip it.
					if (account.isAfterTerminationDate(command.getAccountingPeriod().getStartDate())) {
						status.addSkipped("Account [" + account.getLabel() + "] skipped because it has been terminated prior to [" + DateUtils.fromDateShort(command.getAccountingPeriod().getStartDate()) + "].");
						continue;
					}

					summary = new PerformanceInvestmentAccount();
					InvestmentAccount clientAccount = getInvestmentAccountService().getInvestmentAccount(account.getId());
					summary.setClientAccount(clientAccount);
					summary.setAccountingPeriod(command.getAccountingPeriod());
					getPerformanceInvestmentAccountService().savePerformanceInvestmentAccount(summary);
					createdCount++;
				}

				// If user chose to create and process, and if workflowState == "Draft", process run list for each summary
				if (command.isProcessAll() && StringUtils.isEqual("Draft", summary.getWorkflowState().getName())) {
					processPerformanceInvestmentAccountForDate(summary, null);
					summary = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(summary.getId());
					if (!RuleViolationUtil.isRuleViolationAwareReadyForUse(summary)) {
						failedCount++;
					}
					else {
						successCount++;
					}
				}
				// Update Runner status
				int totalRemaining = totalAccounts - (createdCount + successCount + failedCount);
				int processedCount = successCount + failedCount;

				status.setMessage("Total Remaining: " + totalRemaining + " out of " + totalAccounts + "; Created: " + createdCount + "; Processed: " + processedCount + " [Success: "
						+ successCount + "; Failed: " + failedCount + "]. " + (errorCount > 0 ? "Errors: " + errors : ""));
			}

			catch (Throwable e) {
				errorCount++;
				status.addError("[" + account.getNumber() + "]: " + ExceptionUtils.getOriginalMessage(e));
				LogUtils.errorOrInfo(getClass(), "Error processing performance for account [" + account.getNumber() + "]: " + ExceptionUtils.getOriginalMessage(e), e);
				errors.append("\n\n");
				errors.append("Failed processing performance for account [").append(account.getLabel()).append("]: ");
				errors.append(ExceptionUtils.getDetailedMessage(e));
			}
		}

		// Return final status
		status.setMessage("Processing Complete. Total Accounts: " + totalAccounts + "; Created: " + createdCount + "; Processed: " + (successCount + failedCount) + " [Success: " + successCount + "; Failed: "
				+ failedCount + "]. " + (errorCount > 0 ? "Errors: " + errors : ""));
		return status;
	}


	private PerformanceInvestmentAccountSearchForm getPerformanceInvestmentAccountSearchFormForCommand(PerformanceInvestmentAccountListCommand command) {
		ValidationUtils.assertNotNull(command, "Command object not instantiated.");
		ValidationUtils.assertNotNull(command.getMeasureDate(), "Measure Date is required");

		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
		AccountingPeriod accountingPeriod = getAccountingPeriodService().getAccountingPeriodForDate(command.getMeasureDate());
		command.setAccountingPeriod(accountingPeriod); // Set for re-use later
		searchForm.setAccountingPeriodId(accountingPeriod.getId());

		// If only one account, restrict summaries to only that account. Else restrict summaries to the account group
		if (command.getInvestmentAccountId() != null) {
			searchForm.setClientAccountId(command.getInvestmentAccountId());
		}
		else {
			searchForm.setClientAccountGroupId(command.getInvestmentAccountGroupId());
		}
		return searchForm;
	}

	////////////////////////////////////////////////////////////////
	//////              Summary Processing                    //////
	////////////////////////////////////////////////////////////////


	@Override
	public void processPerformanceInvestmentAccount(int performanceAccountId) {
		PerformanceInvestmentAccount performanceAccount = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(performanceAccountId);
		processPerformanceInvestmentAccountForDate(performanceAccount, null);
	}


	@Override
	public PerformanceInvestmentAccount previewPerformanceInvestmentAccount(int performanceAccountId) {
		return doProcessPerformanceInvestmentAccount(performanceAccountId, PerformanceInvestmentAccountProcessingTypes.PREVIEW, null);
	}


	@Override
	public PerformanceInvestmentAccount previewPerformanceInvestmentAccountHistoricalOnly(int performanceAccountId) {
		return doProcessPerformanceInvestmentAccount(performanceAccountId, PerformanceInvestmentAccountProcessingTypes.PREVIEW_HISTORICAL_RETURNS_ONLY, null);
	}


	@Override
	public void processPerformanceInvestmentAccountHistoricalMissingCumulative() {
		final String runId = "HISTORICAL_CUMULATIVE";
		final Date now = new Date();
		Runner runner = new AbstractStatusAwareRunner("PERFORMANCE-ACCOUNT", runId, now) {

			@Override
			public void run() {
				int count = 0;
				int successCount = 0;
				int errorCount = 0;

				PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();
				searchForm.setInceptionToDateReturnCumulativeIsNull(true);
				List<PerformanceInvestmentAccount> performanceInvestmentAccountList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);
				for (PerformanceInvestmentAccount performanceInvestmentAccount : CollectionUtils.getIterable(performanceInvestmentAccountList)) {
					try {
						count++;
						getStatus().setMessage("Processing " + count + " of " + CollectionUtils.getSize(performanceInvestmentAccountList) + " performance records.");
						PerformanceInvestmentAccount previewPerformance = previewPerformanceInvestmentAccountHistoricalOnly(performanceInvestmentAccount.getId());
						performanceInvestmentAccount.setInceptionToDateReturnCumulative(previewPerformance.getInceptionToDateReturnCumulative());
						performanceInvestmentAccount.setInceptionToDateBenchmarkReturnCumulative(previewPerformance.getInceptionToDateBenchmarkReturnCumulative());
						performanceInvestmentAccount.setInceptionToDatePortfolioReturnCumulative(previewPerformance.getInceptionToDatePortfolioReturnCumulative());
						DaoUtils.executeWithAllObserversDisabled(() -> getPerformanceInvestmentAccountService().savePerformanceInvestmentAccount(performanceInvestmentAccount));
						successCount++;
					}
					catch (Throwable e) {
						errorCount++;
						String errorMessage = "Error calculating historical cumulative returns for " + performanceInvestmentAccount.getLabel() + ": " + ExceptionUtils.getOriginalMessage(e);
						getStatus().addError(errorMessage);
						LogUtils.errorOrInfo(getClass(), errorMessage, e);
					}
				}
				getStatus().setMessage("Processing completed.  " + successCount + " Successful, " + errorCount + " Errors.");
			}
		};
		getRunnerHandler().runNow(runner);
	}


	@Override
	public void processPerformanceInvestmentAccountForDate(PerformanceInvestmentAccount performanceAccount, Date date) {
		getPerformanceInvestmentAccountService().validatePerformanceInvestmentAccountModify(performanceAccount.getId());

		// MANUALLY DELETE NOT IGNORABLE WARNINGS SO CAN REPROCESS
		getPerformanceInvestmentAccountRuleHandler().clearPerformanceInvestmentAccountNotIgnorableWarnings(performanceAccount.getId());

		performanceAccount.setViolationStatus(getRuleViolationStatusService().getRuleViolationStatus(RuleViolationStatus.RuleViolationStatusNames.PROCESSED_SUCCESS));
		doProcessPerformanceInvestmentAccount(performanceAccount, PerformanceInvestmentAccountProcessingTypes.PROCESS, date);

		processPerformanceInvestmentAccountWarnings(performanceAccount.getId());
	}


	@Override
	public void recalculatePerformanceInvestmentAccountHistoricalReturns(PerformanceInvestmentAccount performanceAccount) {
		doProcessPerformanceInvestmentAccount(performanceAccount, PerformanceInvestmentAccountProcessingTypes.CALCULATE_HISTORICAL_RETURNS_ONLY, null);
	}


	@Override
	public void processPerformanceInvestmentAccountWarnings(int performanceAccountId) {
		getPerformanceInvestmentAccountRuleHandler().processPerformanceInvestmentAccountRules(performanceAccountId);
	}


	@Transactional
	@Override
	public void deletePerformanceInvestmentAccount(int id) {
		PerformanceInvestmentAccount performanceAccount = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(id);
		PerformanceInvestmentAccountProcessor<T> processor = getPerformanceInvestmentAccountProcessor(performanceAccount);
		processor.deletePerformanceInvestmentAccountProcessing(id);
		getPerformanceInvestmentAccountService().deletePerformanceInvestmentAccount(id);
	}

	////////////////////////////////////////////////////////////////
	//////       PortfolioRunProcessHandler Methods          ///////
	////////////////////////////////////////////////////////////////


	private PerformanceInvestmentAccount doProcessPerformanceInvestmentAccount(int performanceAccountId, PerformanceInvestmentAccountProcessingTypes processingType, Date processDate) {
		PerformanceInvestmentAccount performanceAccount = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(performanceAccountId);
		return doProcessPerformanceInvestmentAccount(performanceAccount, processingType, processDate);
	}


	private PerformanceInvestmentAccount doProcessPerformanceInvestmentAccount(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType, Date processDate) {
		PerformanceInvestmentAccountProcessor<T> processor = getPerformanceInvestmentAccountProcessor(performanceAccount);
		T config = processor.createPerformanceInvestmentAccountConfig(performanceAccount, processingType);
		processor.processPerformanceInvestmentAccount(config, processDate);
		return performanceAccount;
	}


	private PerformanceInvestmentAccountProcessor<T> getPerformanceInvestmentAccountProcessor(PerformanceInvestmentAccount performanceAccount) {
		return getPortfolioAccountDataRetriever().getClientAccountObjectFromContextMap(performanceAccount.getClientAccount(), null, getPerformanceInvestmentAccountProcessorMap(), "Performance Investment Account Processor");
	}

	////////////////////////////////////////////////////////////////
	///////            Getter and Setter Methods           /////////
	////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public Map<String, PerformanceInvestmentAccountProcessor<T>> getPerformanceInvestmentAccountProcessorMap() {
		return this.performanceInvestmentAccountProcessorMap;
	}


	public void setPerformanceInvestmentAccountProcessorMap(Map<String, PerformanceInvestmentAccountProcessor<T>> performanceInvestmentAccountProcessorMap) {
		this.performanceInvestmentAccountProcessorMap = performanceInvestmentAccountProcessorMap;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public PerformanceInvestmentAccountRuleHandler getPerformanceInvestmentAccountRuleHandler() {
		return this.performanceInvestmentAccountRuleHandler;
	}


	public void setPerformanceInvestmentAccountRuleHandler(PerformanceInvestmentAccountRuleHandler performanceInvestmentAccountRuleHandler) {
		this.performanceInvestmentAccountRuleHandler = performanceInvestmentAccountRuleHandler;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public RuleViolationStatusService getRuleViolationStatusService() {
		return this.ruleViolationStatusService;
	}


	public void setRuleViolationStatusService(RuleViolationStatusService ruleViolationStatusService) {
		this.ruleViolationStatusService = ruleViolationStatusService;
	}
}
