package com.clifton.performance.account.process.jobs;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.performance.account.process.PerformanceInvestmentAccountListCommand;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;

import java.util.Date;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountProcessorJob</code> creates and/or re-processes (if existing, then Draft only) performance summaries for the selected account group and either current month (for partial period performance) or previous month.
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountProcessorJob extends BasePerformanceAccountProcessorJob {

	private PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofMessage("Starting Performance Summary Processing");
		PerformanceInvestmentAccountListCommand command = new PerformanceInvestmentAccountListCommand();
		command.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
		command.setProcessAll(true);
		command.setSynchronous(true);
		command.setStatus(status);

		Date asOfDate = (isUsePreviousWeekdayForAccountPeriodRetrieval() ? DateUtils.getPreviousWeekday(new Date()) : new Date());
		if (isProcessCurrentAccountingPeriod()) {
			command.setAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(asOfDate));
		}
		else {
			command.setAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(DateUtils.getLastDayOfPreviousMonth(asOfDate)));
		}
		command.setMeasureDate(command.getAccountingPeriod().getEndDate());

		getPerformanceInvestmentAccountProcessService().processPerformanceInvestmentAccountList(command);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountProcessService getPerformanceInvestmentAccountProcessService() {
		return this.performanceInvestmentAccountProcessService;
	}


	public void setPerformanceInvestmentAccountProcessService(PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService) {
		this.performanceInvestmentAccountProcessService = performanceInvestmentAccountProcessService;
	}
}
