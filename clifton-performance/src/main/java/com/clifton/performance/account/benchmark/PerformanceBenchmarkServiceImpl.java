package com.clifton.performance.account.benchmark;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.performance.account.benchmark.search.PerformanceBenchmarkSearchForm;
import com.clifton.performance.account.benchmark.search.PerformanceSummaryBenchmarkSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>PerformanceBenchmarkServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class PerformanceBenchmarkServiceImpl implements PerformanceBenchmarkService {

	private AdvancedUpdatableDAO<PerformanceBenchmark, Criteria> performanceBenchmarkDAO;
	private AdvancedUpdatableDAO<PerformanceSummaryBenchmark, Criteria> performanceSummaryBenchmarkDAO;

	////////////////////////////////////////////////////////////////////////////
	///////            Performance Benchmark Business Methods            ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceBenchmark getPerformanceBenchmark(int id) {
		return getPerformanceBenchmarkDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceBenchmark> getPerformanceBenchmarkList(final PerformanceBenchmarkSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveOnDate() != null) {
					criteria.add(ActiveExpressionForDates.forActiveOnDate(true, searchForm.getActiveOnDate()));
				}
				else if (searchForm.getActiveOnStartDate() != null && searchForm.getActiveOnEndDate() != null) {
					LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull("startDate"), Restrictions.le("startDate", searchForm.getActiveOnEndDate()));
					LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull("endDate"), Restrictions.ge("endDate", searchForm.getActiveOnStartDate()));
					criteria.add(startDateFilter);
					criteria.add(endDateFilter);
				}
			}
		};
		return getPerformanceBenchmarkDAO().findBySearchCriteria(config);
	}


	@Override
	public PerformanceBenchmark savePerformanceBenchmark(PerformanceBenchmark bean) {
		return getPerformanceBenchmarkDAO().save(bean);
	}


	@Override
	public void deletePerformanceBenchmark(int id) {
		getPerformanceBenchmarkDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////         Performance Summary Benchmark Business Methods         ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceSummaryBenchmark getPerformanceSummaryBenchmark(int id) {
		return getPerformanceSummaryBenchmarkDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkList(final PerformanceSummaryBenchmarkSearchForm searchForm) {
		return getPerformanceSummaryBenchmarkDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkListBySummary(int summaryId) {
		return getPerformanceSummaryBenchmarkDAO().findByField("referenceOne.id", summaryId);
	}


	@Override
	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkListByBenchmark(int benchmarkId) {
		return getPerformanceSummaryBenchmarkDAO().findByField("referenceTwo.id", benchmarkId);
	}


	/**
	 * UI Save is exposed via {@link com.clifton.performance.account.benchmark.process.PerformanceBenchmarkProcessServiceImpl} for recalculating
	 */
	@Override
	public PerformanceSummaryBenchmark savePerformanceSummaryBenchmark(PerformanceSummaryBenchmark bean) {
		return getPerformanceSummaryBenchmarkDAO().save(bean);
	}


	@Override
	public void savePerformanceSummaryBenchmarkList(List<PerformanceSummaryBenchmark> list) {
		getPerformanceSummaryBenchmarkDAO().saveList(list);
	}


	@Override
	public void savePerformanceSummaryBenchmarkListForSummary(int summaryId, List<PerformanceSummaryBenchmark> list) {
		List<PerformanceSummaryBenchmark> originalList = getPerformanceSummaryBenchmarkListBySummary(summaryId);
		getPerformanceSummaryBenchmarkDAO().saveList(list, originalList);
	}


	@Override
	public void deletePerformanceSummaryBenchmarkListBySummary(int summaryId) {
		getPerformanceSummaryBenchmarkDAO().deleteList(getPerformanceSummaryBenchmarkListBySummary(summaryId));
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformanceBenchmark, Criteria> getPerformanceBenchmarkDAO() {
		return this.performanceBenchmarkDAO;
	}


	public void setPerformanceBenchmarkDAO(AdvancedUpdatableDAO<PerformanceBenchmark, Criteria> performanceBenchmarkDAO) {
		this.performanceBenchmarkDAO = performanceBenchmarkDAO;
	}


	public AdvancedUpdatableDAO<PerformanceSummaryBenchmark, Criteria> getPerformanceSummaryBenchmarkDAO() {
		return this.performanceSummaryBenchmarkDAO;
	}


	public void setPerformanceSummaryBenchmarkDAO(AdvancedUpdatableDAO<PerformanceSummaryBenchmark, Criteria> performanceSummaryBenchmarkDAO) {
		this.performanceSummaryBenchmarkDAO = performanceSummaryBenchmarkDAO;
	}
}
