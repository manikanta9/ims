package com.clifton.performance.account.securitytarget.process;

import com.clifton.business.service.BusinessServiceProcessingType;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.process.processor.BasePerformanceInvestmentAccountProcessorImpl;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import com.clifton.performance.account.securitytarget.PerformanceInvestmentAccountSecurityTargetService;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.util.Date;


/**
 * @author manderson
 */
public class PerformanceInvestmentAccountSecurityTargetProcessorImpl extends BasePerformanceInvestmentAccountProcessorImpl<PerformanceInvestmentAccountSecurityTargetProcessConfig> {

	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;

	private PerformanceInvestmentAccountSecurityTargetService performanceInvestmentAccountSecurityTargetService;

	private SystemColumnValueService systemColumnValueService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void deletePerformanceInvestmentAccountProcessing(int performanceAccountId) {
		getPerformanceInvestmentAccountSecurityTargetService().deletePerformanceInvestmentAccountSecurityTargetDailyReturnListForPerformanceInvestmentAccount(performanceAccountId);
	}


	@Override
	public PerformanceInvestmentAccountSecurityTargetProcessConfig createPerformanceInvestmentAccountConfig(PerformanceInvestmentAccount performanceInvestmentAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		return new PerformanceInvestmentAccountSecurityTargetProcessConfig(performanceInvestmentAccount, processingType);
	}


	@Override
	protected PerformanceInvestmentAccountSecurityTargetProcessConfig setupPerformanceInvestmentAccountConfig(PerformanceInvestmentAccountSecurityTargetProcessConfig config, Date date) {
		InvestmentAccount clientAccount = config.getPerformanceAccount().getClientAccount();
		config.setPerformanceInceptionDate(ObjectUtils.coalesce(clientAccount.getPerformanceInceptionDate(), clientAccount.getInceptionDate()));
		config.setDailyPositionMap(getPerformanceInvestmentAccountDataRetriever().getAccountingPositionDailyMapForPerformanceAccount(config.getPerformanceAccount().getClientAccount(), config.getPerformanceAccount().getAccountingPeriod(), true, true, false));

		Boolean doNotCalculateBenchmarkReturn = false;
		if (config.getPerformanceAccount().getClientAccount().getServiceProcessingType() != null) {
			doNotCalculateBenchmarkReturn = (Boolean) getSystemColumnValueService().getSystemColumnCustomValue(config.getPerformanceAccount().getClientAccount().getServiceProcessingType().getId(), BusinessServiceProcessingType.BUSINESS_SERVICE_PROCESSING_TYPE_COLUMN_GROUP_NAME, BusinessServiceProcessingType.PERFORMANCE_DO_NOT_CALCULATE_BENCHMARK_RETURN);
		}
		config.setDoNotCalculateBenchmarkReturn(BooleanUtils.isTrue(doNotCalculateBenchmarkReturn));
		return config;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformanceInvestmentAccountSecurityTargetService getPerformanceInvestmentAccountSecurityTargetService() {
		return this.performanceInvestmentAccountSecurityTargetService;
	}


	public void setPerformanceInvestmentAccountSecurityTargetService(PerformanceInvestmentAccountSecurityTargetService performanceInvestmentAccountSecurityTargetService) {
		this.performanceInvestmentAccountSecurityTargetService = performanceInvestmentAccountSecurityTargetService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
