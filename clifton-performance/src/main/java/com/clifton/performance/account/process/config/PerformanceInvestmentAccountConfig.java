package com.clifton.performance.account.process.config;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountConfig</code> class contains information about the
 * account that can be passed through processing for re-usable information.  This class
 * is extended by service specific config objects that have their specific information
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountConfig {

	private PerformanceInvestmentAccount performanceAccount;

	private PerformanceInvestmentAccountProcessingTypes processingType;

	/**
	 * Inception Date, a.k.a. Positions On Date for the Account
	 * For some cases this could utilize custom field overrides
	 * Set by calling: getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountInceptionDate(summary.getClientAccount().getId());
	 */
	private Date performanceInceptionDate;


	private Map<String, List<AccountingPositionDaily>> dailyPositionMap = null;

	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		this.performanceAccount = performanceAccount;
		this.processingType = processingType;
	}

	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public Date getStartDate() {
		if (getPerformanceAccount() != null && getPerformanceAccount().getAccountingPeriod() != null) {
			return getPerformanceAccount().getAccountingPeriod().getStartDate();
		}
		return null;
	}


	public Date getEndDate() {
		if (getPerformanceAccount() != null && getPerformanceAccount().getAccountingPeriod() != null) {
			return getPerformanceAccount().getAccountingPeriod().getEndDate();
		}
		return null;
	}


	public List<AccountingPositionDaily> getPositionListForDate(Date date) {
		return this.dailyPositionMap != null ? this.dailyPositionMap.get(DateUtils.fromDateShort(date)) : null;
	}

	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public PerformanceInvestmentAccount getPerformanceAccount() {
		return this.performanceAccount;
	}


	public void setPerformanceAccount(PerformanceInvestmentAccount performanceAccount) {
		this.performanceAccount = performanceAccount;
	}


	public PerformanceInvestmentAccountProcessingTypes getProcessingType() {
		return this.processingType;
	}


	public void setProcessingType(PerformanceInvestmentAccountProcessingTypes processingType) {
		this.processingType = processingType;
	}


	public Date getPerformanceInceptionDate() {
		return this.performanceInceptionDate;
	}


	public void setPerformanceInceptionDate(Date performanceInceptionDate) {
		this.performanceInceptionDate = performanceInceptionDate;
	}


	public Map<String, List<AccountingPositionDaily>> getDailyPositionMap() {
		return this.dailyPositionMap;
	}


	public void setDailyPositionMap(Map<String, List<AccountingPositionDaily>> dailyPositionMap) {
		this.dailyPositionMap = dailyPositionMap;
	}
}
