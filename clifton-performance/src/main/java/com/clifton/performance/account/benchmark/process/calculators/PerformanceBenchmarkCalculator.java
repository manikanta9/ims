package com.clifton.performance.account.benchmark.process.calculators;


import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.PerformanceBenchmarkService;
import com.clifton.performance.account.benchmark.process.PerformanceBenchmarkProcessService;
import com.clifton.performance.account.benchmark.process.PerformanceInvestmentAccountBenchmarkConfig;
import com.clifton.performance.account.process.calculators.PerformanceInvestmentAccountCalculator;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>PerformanceBenchmarkCalculator</code> processes the benchmarks for the {@link PerformanceInvestmentAccount}
 * These are often processed independently and we can also load history for a benchmark across summaries, so there is a separate processing service
 * with all relevant processing there and this calculator just calls that processing method.
 *
 * @author manderson
 */
@Component
public class PerformanceBenchmarkCalculator<T extends PerformanceInvestmentAccountBenchmarkConfig> implements PerformanceInvestmentAccountCalculator<T> {

	private PerformanceBenchmarkService performanceBenchmarkService;
	private PerformanceBenchmarkProcessService performanceBenchmarkProcessService;


	@Override
	public void calculate(T config, @SuppressWarnings("unused") Date date) {
		config.setSummaryBenchmarkList(getPerformanceBenchmarkProcessService().generatePerformanceSummaryBenchmarkListForSummary(config.getPerformanceAccount().getId()));
	}


	@Override
	public boolean calculateHistoricalReturns(@SuppressWarnings("unused") T config) {
		// DO NOTHING - BENCHMARK HISTORICAL RETURNS ARE ONLY DONE THROUGH PROCESSING
		return false;
	}


	@Override
	public void saveResults(T config, @SuppressWarnings("unused") Date date) {
		getPerformanceBenchmarkService().savePerformanceSummaryBenchmarkListForSummary(config.getPerformanceAccount().getId(), config.getSummaryBenchmarkList());
	}

	////////////////////////////////////////////////////////////
	////////         Getter and Setter Methods          ////////
	////////////////////////////////////////////////////////////


	public PerformanceBenchmarkProcessService getPerformanceBenchmarkProcessService() {
		return this.performanceBenchmarkProcessService;
	}


	public void setPerformanceBenchmarkProcessService(PerformanceBenchmarkProcessService performanceBenchmarkProcessService) {
		this.performanceBenchmarkProcessService = performanceBenchmarkProcessService;
	}


	public PerformanceBenchmarkService getPerformanceBenchmarkService() {
		return this.performanceBenchmarkService;
	}


	public void setPerformanceBenchmarkService(PerformanceBenchmarkService performanceBenchmarkService) {
		this.performanceBenchmarkService = performanceBenchmarkService;
	}
}
