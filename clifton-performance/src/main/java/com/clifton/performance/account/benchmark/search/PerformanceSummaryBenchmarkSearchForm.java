package com.clifton.performance.account.benchmark.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


/**
 * The <code>PerformanceSummaryBenchmarkSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class PerformanceSummaryBenchmarkSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.id")
	private Integer summaryId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchFieldPath = "referenceOne.accountingPeriod", searchField = "endDate")
	private Date summaryMeasureDate;

	@SearchField(searchFieldPath = "referenceOne.accountingPeriod", searchField = "endDate", comparisonConditions = ComparisonConditions.GREATER_THAN_OR_EQUALS)
	private Date startSummaryMeasureDate;

	@SearchField(searchFieldPath = "referenceOne.accountingPeriod", searchField = "endDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date endSummaryMeasureDate;

	@SearchField(searchField = "referenceTwo.id")
	private Integer benchmarkId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String benchmarkName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "benchmarkInterestRateIndex.id")
	private Integer benchmarkInterestRateIndex;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "benchmarkSecurity.id")
	private Integer benchmarkSecurityId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSummaryId() {
		return this.summaryId;
	}


	public void setSummaryId(Integer summaryId) {
		this.summaryId = summaryId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getBenchmarkId() {
		return this.benchmarkId;
	}


	public void setBenchmarkId(Integer benchmarkId) {
		this.benchmarkId = benchmarkId;
	}


	public Date getSummaryMeasureDate() {
		return this.summaryMeasureDate;
	}


	public void setSummaryMeasureDate(Date summaryMeasureDate) {
		this.summaryMeasureDate = summaryMeasureDate;
	}


	public Integer getBenchmarkInterestRateIndex() {
		return this.benchmarkInterestRateIndex;
	}


	public void setBenchmarkInterestRateIndex(Integer benchmarkInterestRateIndex) {
		this.benchmarkInterestRateIndex = benchmarkInterestRateIndex;
	}


	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	public Date getStartSummaryMeasureDate() {
		return this.startSummaryMeasureDate;
	}


	public void setStartSummaryMeasureDate(Date startSummaryMeasureDate) {
		this.startSummaryMeasureDate = startSummaryMeasureDate;
	}


	public Date getEndSummaryMeasureDate() {
		return this.endSummaryMeasureDate;
	}


	public void setEndSummaryMeasureDate(Date endSummaryMeasureDate) {
		this.endSummaryMeasureDate = endSummaryMeasureDate;
	}


	public String getBenchmarkName() {
		return this.benchmarkName;
	}


	public void setBenchmarkName(String benchmarkName) {
		this.benchmarkName = benchmarkName;
	}
}
