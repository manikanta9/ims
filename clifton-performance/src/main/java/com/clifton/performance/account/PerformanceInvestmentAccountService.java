package com.clifton.performance.account;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>PerformanceInvestmentAccountService</code> ...
 *
 * @author manderson
 */
public interface PerformanceInvestmentAccountService {

	///////////////////////////////////////////////////////////////////////////////
	///////         Performance Investment Account Business Methods         ///////
	///////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccount getPerformanceInvestmentAccount(int id);


	public List<PerformanceInvestmentAccount> getPerformanceInvestmentAccountList(PerformanceInvestmentAccountSearchForm searchForm);


	public PerformanceInvestmentAccount savePerformanceInvestmentAccount(PerformanceInvestmentAccount bean);


	@DoNotAddRequestMapping
	public void deletePerformanceInvestmentAccount(int id);


	/**
	 * Validates the current workflow state modify condition if changes can be made
	 */
	@DoNotAddRequestMapping
	public void validatePerformanceInvestmentAccountModify(int id);


	/**
	 * While old and new portal are supported, need a way to delete from both portals, or delete from existing portal and just un-approve from new portal
	 */
	@RequestMapping("performanceInvestmentAccountReportUnpost")
	@SecureMethod(dtoClass = PerformanceInvestmentAccount.class, permissions = SecurityPermission.PERMISSION_READ)
	public void unpostPerformanceInvestmentAccountReport(int performanceInvestmentAccountId, boolean deleteFromNewPortal);
}
