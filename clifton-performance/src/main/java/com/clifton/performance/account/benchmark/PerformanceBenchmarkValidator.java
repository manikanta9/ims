package com.clifton.performance.account.benchmark;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PerformanceBenchmarkValidator</code> ...
 *
 * @author Mary Anderson
 */
@Component
public class PerformanceBenchmarkValidator extends SelfRegisteringDaoValidator<PerformanceBenchmark> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PerformanceBenchmark bean, @SuppressWarnings("unused") DaoEventTypes config, ReadOnlyDAO<PerformanceBenchmark> dao) throws ValidationException {
		// Security or Interest Rate Index is Required - Can't select both
		ValidationUtils.assertTrue((bean.getBenchmarkInterestRateIndex() == null) != (bean.getBenchmarkSecurity() == null), "A benchmark security or interest rate index is required, but not both.");

		// Validate Start/End Dates
		List<PerformanceBenchmark> list = dao.findByFields(new String[]{"clientAccount.id", "benchmarkSecurity.id", "benchmarkInterestRateIndex.id"}, new Object[]{
				bean.getClientAccount().getId(), (bean.getBenchmarkSecurity() != null ? bean.getBenchmarkSecurity().getId() : null),
				(bean.getBenchmarkInterestRateIndex() != null ? bean.getBenchmarkInterestRateIndex().getId() : null)});
		for (PerformanceBenchmark b : CollectionUtils.getIterable(list)) {
			if (b.equals(bean)) {
				continue;
			}
			if (DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), b.getStartDate(), b.getEndDate())) {
				throw new ValidationException("This benchmark is already used for this account by [" + b.getName() + "] on " + b.getDateLabel() + ". Please enter a unique date range.");
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void validate(PerformanceBenchmark bean, DaoEventTypes config) throws ValidationException {
		// unused - uses dao method
	}
}
