package com.clifton.performance.account.process.jobs;

import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;

import java.util.Date;
import java.util.Map;


/**
 * @author BrianH
 */
public class PerformanceClientInvestmentAccountPerformanceProcessorJob implements Task {

	/**
	 * Calculate the daily performance and base monthly return values (current period net,gross,etc)
	 */
	private boolean calculateAccountDailyMonthlyBasePerformance;

	private Integer[] clientAccountIds;

	/**
	 * Allows to process current period for partial period reporting
	 * If false, will process previous month accounting period.
	 */
	private boolean processCurrentAccountingPeriod;

	private AccountingPeriodService accountingPeriodService;

	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofMessage("Starting Performance Client Investment Account Performance processing.");

		PerformanceCompositeAccountRebuildCommand command = new PerformanceCompositeAccountRebuildCommand();
		command.setSynchronous(true);
		command.setStatus(status);

		Date asOfDate = DateUtils.getPreviousWeekday(new Date());
		if (isProcessCurrentAccountingPeriod()) {
			command.setToAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(asOfDate));
		}
		else {
			command.setToAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(DateUtils.getLastDayOfPreviousMonth(asOfDate)));
		}
		command.setCalculateAccountDailyMonthlyBasePerformance(isCalculateAccountDailyMonthlyBasePerformance());
		command.setMetricProcessingType(PerformanceCompositeMetricProcessingTypes.ALL);

		for (Integer clientAccountId : clientAccountIds) {
			command.setInvestmentAccountId(clientAccountId);
			getPerformanceCompositePerformanceRebuildService().rebuildPerformanceCompositeAccountPerformance(command);
		}

		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer[] getClientAccountIds() {
		return this.clientAccountIds;
	}


	public void setClientAccountIds(Integer[] clientAccountIds) {
		this.clientAccountIds = clientAccountIds;
	}


	public boolean isCalculateAccountDailyMonthlyBasePerformance() {
		return this.calculateAccountDailyMonthlyBasePerformance;
	}


	public void setCalculateAccountDailyMonthlyBasePerformance(boolean calculateAccountDailyMonthlyBasePerformance) {
		this.calculateAccountDailyMonthlyBasePerformance = calculateAccountDailyMonthlyBasePerformance;
	}


	public boolean isProcessCurrentAccountingPeriod() {
		return this.processCurrentAccountingPeriod;
	}


	public void setProcessCurrentAccountingPeriod(boolean processCurrentAccountingPeriod) {
		this.processCurrentAccountingPeriod = processCurrentAccountingPeriod;
	}


	public PerformanceCompositePerformanceRebuildService getPerformanceCompositePerformanceRebuildService() {
		return this.performanceCompositePerformanceRebuildService;
	}


	public void setPerformanceCompositePerformanceRebuildService(PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService) {
		this.performanceCompositePerformanceRebuildService = performanceCompositePerformanceRebuildService;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}
}
