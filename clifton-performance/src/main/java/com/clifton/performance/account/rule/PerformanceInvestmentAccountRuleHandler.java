package com.clifton.performance.account.rule;

import com.clifton.core.validation.ValidationExceptionWithCause;


/**
 * @author manderson
 */
public interface PerformanceInvestmentAccountRuleHandler {


	/**
	 * Deletes all non-ignorable violations - used prior to re-processing a performance summary
	 */
	public void clearPerformanceInvestmentAccountNotIgnorableWarnings(int performanceAccountId);


	public void processPerformanceInvestmentAccountRules(int performanceAccountId);


	public void savePerformanceInvestmentAccountRuleViolationFromException(ValidationExceptionWithCause exception, int performanceAccountId);
}
