package com.clifton.performance.account.process.processor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.calculators.PerformanceInvestmentAccountCalculator;
import com.clifton.performance.account.process.calculators.PerformanceInvestmentAccountCalculatorStep;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.rule.PerformanceInvestmentAccountRuleHandler;

import java.util.Date;
import java.util.List;


/**
 * The <code>BasePortfolioRunProcessHandlerImpl</code> contains common methods/look up that can be used
 * across the various service portfolio processors
 *
 * @author manderson
 */
public abstract class BasePerformanceInvestmentAccountProcessorImpl<T extends PerformanceInvestmentAccountConfig> implements PerformanceInvestmentAccountProcessor<T> {

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;
	private PerformanceInvestmentAccountRuleHandler performanceInvestmentAccountRuleHandler;


	private List<PerformanceInvestmentAccountCalculatorStep<T>> calculatorStepList;

	/**
	 * @see #getOrder()
	 */
	private int order;

	/////////////////////////////////////////////////////
	//////        Performance Setup Methods       ///////
	/////////////////////////////////////////////////////


	protected abstract T setupPerformanceInvestmentAccountConfig(T config, Date date);

	/////////////////////////////////////////////////////
	/////           Processing Methods              /////
	/////////////////////////////////////////////////////


	@Override
	public void processPerformanceInvestmentAccount(T performanceInvestmentAccountConfig, Date date) {
		performanceInvestmentAccountConfig = setupPerformanceInvestmentAccountConfig(performanceInvestmentAccountConfig, date);
		List<PerformanceInvestmentAccountCalculatorStep<T>> stepList = getCalculatorStepList();
		if (!CollectionUtils.isEmpty(stepList)) {
			stepList = BeanUtils.sortWithFunction(stepList, PerformanceInvestmentAccountCalculatorStep::getOrder, true);

			processPerformanceInvestmentAccountCalculatorStepList(performanceInvestmentAccountConfig, stepList, date);
		}
	}

	/////////////////////////////////////////////////////
	/////////////////////////////////////////////////////


	protected void processPerformanceInvestmentAccountCalculatorStepList(T config, List<PerformanceInvestmentAccountCalculatorStep<T>> stepList, Date date) {
		try {
			for (PerformanceInvestmentAccountCalculatorStep<T> step : stepList) {
				PerformanceInvestmentAccountCalculator<T> calculator = step.getCalculatorBean();
				processPerformanceInvestmentAccountCalculator(config, calculator, date);
			}
		}
		catch (ValidationException e) {
			ValidationException updatedException = e;
			// Don't save the exception as a warning if we aren't saving the results
			if (config.getProcessingType().isSaveResults()) {
				// There have been times when the run was deleted while processing
				// Warnings create a "cannot transition exception" and the saveProductOverlayRun throws a Null Pointer
				// Before creating warning and saving run - ensure that it does exist.
				config.setPerformanceAccount(getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(config.getPerformanceAccount().getId()));
				if (config.getPerformanceAccount() != null) {
					// If not already a SystemWarningValidationException, then let's make it one so we can create the exception as a warning
					if (!(updatedException instanceof ValidationExceptionWithCause)) {
						// Validation Exceptions generated when warnings are already present, don't re-log as another warning
						if (!e.getMessage().startsWith("Error while processing")) {
							updatedException = new ValidationExceptionWithCause(e.getMessage(), e);
						}
					}
					if (updatedException instanceof ValidationExceptionWithCause) {
						ValidationExceptionWithCause se = (ValidationExceptionWithCause) updatedException;
						getPerformanceInvestmentAccountRuleHandler().savePerformanceInvestmentAccountRuleViolationFromException(se, config.getPerformanceAccount().getId());
					}
				}
				getPerformanceInvestmentAccountService().savePerformanceInvestmentAccount(config.getPerformanceAccount());
				throw updatedException;
			}
		}
	}


	protected void processPerformanceInvestmentAccountCalculator(T config, PerformanceInvestmentAccountCalculator<T> calculator, Date date) {
		if (calculator != null) {
			if (config.getProcessingType().isCalculate()) {
				calculator.calculate(config, date);
				if (config.getProcessingType().isSaveResults()) {
					calculator.saveResults(config, date);
				}
			}
			else if (config.getProcessingType().isCalculateHistoricalReturnsOnly() && calculator.calculateHistoricalReturns(config)) {
				if (config.getProcessingType().isSaveResults()) {
					calculator.saveResults(config, date);
				}
			}
		}
	}

	/////////////////////////////////////////////////////
	////////        Getter & Setter Methods      ////////
	/////////////////////////////////////////////////////


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public PerformanceInvestmentAccountRuleHandler getPerformanceInvestmentAccountRuleHandler() {
		return this.performanceInvestmentAccountRuleHandler;
	}


	public void setPerformanceInvestmentAccountRuleHandler(PerformanceInvestmentAccountRuleHandler performanceInvestmentAccountRuleHandler) {
		this.performanceInvestmentAccountRuleHandler = performanceInvestmentAccountRuleHandler;
	}


	public List<PerformanceInvestmentAccountCalculatorStep<T>> getCalculatorStepList() {
		return this.calculatorStepList;
	}


	public void setCalculatorStepList(List<PerformanceInvestmentAccountCalculatorStep<T>> calculatorStepList) {
		this.calculatorStepList = calculatorStepList;
	}


	/**
	 * Used to sort processors (currently for ensuring that DEFAULT processor is processed before the PORTFOLIO_TARGET processor).
	 */
	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
