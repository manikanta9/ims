package com.clifton.performance.account.rule;

import com.clifton.accounting.positiontransfer.AccountingPositionTransfer;
import com.clifton.accounting.positiontransfer.AccountingPositionTransferService;
import com.clifton.accounting.positiontransfer.search.AccountingPositionTransferSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code></code>PerformanceAccountPositionTransferRuleEvaluator</code> generates a violation for any position transfer that occurred during the month.  This
 * will allow accounting to review each transfer to confirm if they should be moved at cost or MOC.  The transfers are only included if the transfer is between client accounts
 * because that is when the position date needs to be reviewed to ensure the transfer was processed correctly for position valuation and gain/loss.
 * <p>
 * This rule uses the {@link PerformanceAccountInfo} interface so it can be used by both {@link com.clifton.performance.account.PerformanceInvestmentAccount} a.k.a performance summaries
 * and {@link PerformanceCompositeInvestmentAccountPerformance}
 *
 * @author manderson
 */
public class PerformanceAccountPositionTransferRuleEvaluator extends BaseRuleEvaluator<PerformanceAccountInfo, PerformanceAccountInfoRuleEvaluatorContext> {

	private AccountingPositionTransferService accountingPositionTransferService;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceAccountInfo performanceAccountInfo, RuleConfig ruleConfig, PerformanceAccountInfoRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<AccountingPositionTransfer> positionTransferList = getAccountingPositionTransferList(performanceAccountInfo);
			for (AccountingPositionTransfer positionTransfer : CollectionUtils.getIterable(positionTransferList)) {
				// Pull it Individually so detail list is populated
				positionTransfer = getAccountingPositionTransferService().getAccountingPositionTransfer(positionTransfer.getId());
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put("detailList", positionTransfer.getDetailList());
				violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceAccountInfo.getId(), positionTransfer.getId(), null, templateValues));
			}
		}
		return violationList;
	}


	/**
	 * Note: This list is not retrieved/stored in the context because it's currently the only rule that uses position transfers
	 */
	private List<AccountingPositionTransfer> getAccountingPositionTransferList(PerformanceAccountInfo performanceAccountInfo) {
		AccountingPositionTransferSearchForm searchForm = new AccountingPositionTransferSearchForm();
		searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, performanceAccountInfo.getAccountingPeriod().getStartDate()));
		searchForm.addSearchRestriction(new SearchRestriction("transactionDate", ComparisonConditions.LESS_THAN_OR_EQUALS, performanceAccountInfo.getAccountingPeriod().getEndDate()));
		searchForm.setFromOrToClientInvestmentAccountId(performanceAccountInfo.getClientAccount().getId());

		List<AccountingPositionTransfer> positionTransferList = getAccountingPositionTransferService().getAccountingPositionTransferList(searchForm);

		// Filter the List so that From Client Account != To Client Account
		return BeanUtils.filter(positionTransferList, accountingPositionTransfer -> (accountingPositionTransfer.getFromClientInvestmentAccount() != null && accountingPositionTransfer.getToClientInvestmentAccount() != null && !accountingPositionTransfer.getToClientInvestmentAccount().equals(accountingPositionTransfer.getFromClientInvestmentAccount())));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionTransferService getAccountingPositionTransferService() {
		return this.accountingPositionTransferService;
	}


	public void setAccountingPositionTransferService(AccountingPositionTransferService accountingPositionTransferService) {
		this.accountingPositionTransferService = accountingPositionTransferService;
	}
}
