package com.clifton.performance.account.rule;

import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.account.AccountingAccountService;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailyLiveSearchForm;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceAccountInfoRuleEvaluatorContext</code> holds information that can be reused
 * across different account/date combinations for performance calculations.
 *
 * @author apopp
 */
public class PerformanceAccountInfoRuleEvaluatorContext extends BaseRuleEvaluatorContext {

	private static final String ACCOUNTING_ACCOUNT_GROUP_ACCOUNTING_ACCOUNT_LIST = "ACCOUNTING_ACCOUNT_GROUP_ACCOUNTING_ACCOUNT_LIST";

	private static final String INVESTMENT_ACCOUNT_PERIOD_CLOSING_LIST = "INVESTMENT_ACCOUNT_PERIOD_CLOSING_LIST";
	private static final String INVESTMENT_ACCOUNT_POSITION_MAP = "INVESTMENT_ACCOUNT_POSITION_MAP";

	private static final String PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_LIST = "PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_LIST";
	private static final String PERFORMANCE_COMPOSITE_PERFORMANCE_INVESTMENT_ACCOUNT = "PERFORMANCE_COMPOSITE_PERFORMANCE_INVESTMENT_ACCOUNT";

	private static final String SECURITY_DATE_PRICES_MAP = "SecurityDatePricesMap";
	private static final String SECURITY_DATE_INDEX_RATIO_MAP = "SecurityDateIndexRatioMap";

	private static final String TRANSACTION_LIST_BY_ACCOUNTING_ACCOUNT_DAILY_MAP = "TRANSACTION_LIST_BY_ACCOUNTING_ACCOUNT_DAILY_MAP";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingTransactionService accountingTransactionService;
	private AccountingAccountService accountingAccountService;
	private AccountingPeriodService accountingPeriodService;

	private MarketDataRetriever marketDataRetriever;
	private MarketDataService marketDataService;

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceCompositeSetupService performanceCompositeSetupService;
	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	// Note: Referenced in System Bean Property for System Bean: Performance Investment Account Period Closing Entity Comparison so it must return a list
	public AccountingPeriodClosing getAccountingPeriodClosingForAccountPerformance(PerformanceAccountInfo performanceAccountInfo) {
		String beanKey = BeanUtils.createKeyFromBeans(INVESTMENT_ACCOUNT_PERIOD_CLOSING_LIST, performanceAccountInfo);
		return getContext().getOrSupplyBean(beanKey, () -> getAccountingPeriodService().getAccountingPeriodClosingByPeriodAndAccount(performanceAccountInfo.getAccountingPeriod().getId(), performanceAccountInfo.getClientAccount().getId()));
	}

	/////////////////////////////////////////////////////////////////////////////


	public Map<String, List<AccountingPositionDaily>> getInvestmentAccountPositionMap(PerformanceAccountInfo performanceAccount) {
		return getContext().getOrSupplyBean(INVESTMENT_ACCOUNT_POSITION_MAP, () ->
						getPerformanceInvestmentAccountDataRetriever().getAccountingPositionDailyMapForPerformanceAccount(performanceAccount.getClientAccount(), performanceAccount.getAccountingPeriod(), true, false, true)
				, HashMap::new
		);
	}


	protected List<AccountingPositionDaily> getInvestmentAccountPositionListForDate(PerformanceAccountInfo performanceAccount, Date date) {
		Map<String, List<AccountingPositionDaily>> map = getInvestmentAccountPositionMap(performanceAccount);
		String dateString = DateUtils.fromDateShort(date);
		// Snapshots should exist on all weekdays -
		if (!map.containsKey(dateString)) {
			// so if it's missing and not a week day or outside of the accounting period (For some calculators (like Options Unfunded, we need to pull positions from a day in the next month which wouldn't be stored in the map by default))
			// Then pull live positions and store in the map for later use
			if (!DateUtils.isWeekday(date) || !DateUtils.isDateBetween(date, DateUtils.getLastDayOfPreviousMonth(performanceAccount.getAccountingPeriod().getStartDate()), performanceAccount.getAccountingPeriod().getEndDate(), false)) {
				List<AccountingPositionDaily> list = getPositionListLiveForDate(performanceAccount, date);
				map.put(dateString, list);
			}
		}
		return map.get(dateString);
	}


	private List<AccountingPositionDaily> getPositionListLiveForDate(PerformanceAccountInfo performanceAccount, Date date) {
		AccountingPositionDailyLiveSearchForm liveSearchForm = new AccountingPositionDailyLiveSearchForm();
		liveSearchForm.setClientAccountId(performanceAccount.getClientAccount().getId());
		liveSearchForm.setSnapshotDate(date);
		return getAccountingPositionDailyService().getAccountingPositionDailyLiveList(liveSearchForm);
	}

	/////////////////////////////////////////////////////////////////////////////


	public BigDecimal getInvestmentSecurityPriceForDate(InvestmentSecurity security, Date date, boolean throwException) {
		Map<Integer, Map<Date, BigDecimal>> securityDatePriceMap = getContext().getOrSupplyBean(SECURITY_DATE_PRICES_MAP, HashMap::new);
		Map<Date, BigDecimal> datePriceMapForSecurity = securityDatePriceMap.computeIfAbsent(security.getId(), k -> new HashMap<>());
		return datePriceMapForSecurity.computeIfAbsent(date, d -> getMarketDataRetriever().getPrice(security, date, throwException, null));
	}

	/////////////////////////////////////////////////////////////////////////////


	public BigDecimal getInvestmentSecurityIndexRatioForDate(InvestmentSecurity security, Date date) {
		Map<Integer, Map<Date, BigDecimal>> securityDateIndexRatioMap = getContext().getOrSupplyBean(SECURITY_DATE_INDEX_RATIO_MAP, HashMap::new);
		Map<Date, BigDecimal> dateIndexRatioMapForSecurity = securityDateIndexRatioMap.computeIfAbsent(security.getId(), k -> new HashMap<>());
		return dateIndexRatioMapForSecurity.computeIfAbsent(date, d -> getMarketDataService().getMarketDataIndexRatioForSecurity(security.getId(), date, false));
	}

	/////////////////////////////////////////////////////////////////////////////


	public BigDecimal getAccountingAccountGroupTransactionValue(PerformanceAccountInfo accountInfo, Date date, Short accountingAccountGroupId) {
		return getAccountingAccountGroupTransactionValue(accountInfo, date, accountingAccountGroupId, false);
	}


	/**
	 * When used for additional gain/loss, should have excludePositionsFromAccountingAccountGroup = true as that gain/loss associated with a position would already be included
	 * with the position gain/loss.  For that case, we include cash only (no parent or parent transaction is not a position gl account).
	 */
	public BigDecimal getAccountingAccountGroupTransactionValue(PerformanceAccountInfo accountInfo, Date date, Short accountingAccountGroupId, boolean excludePositionsFromAccountingAccountGroup) {
		if (accountingAccountGroupId == null) {
			return BigDecimal.ZERO;
		}
		List<AccountingAccount> accountingAccountList = getAccountingAccountListByGroupId(accountingAccountGroupId);
		Map<String, Map<AccountingAccount, List<AccountingTransaction>>> cashFlowByDate = getTransactionListByAccountingAccountDailyMap(accountInfo.getClientAccount(), accountInfo.getAccountingPeriod().getStartDate(), accountInfo.getAccountingPeriod().getEndDate(), accountingAccountList, true);
		Map<AccountingAccount, List<AccountingTransaction>> cashFlowForDate = cashFlowByDate.get(DateUtils.fromDateShort(date));
		Collection<List<AccountingTransaction>> collections = CollectionUtils.getValues(cashFlowForDate);
		List<AccountingTransaction> transactionList = CollectionUtils.combineCollectionOfCollections(collections);
		if (excludePositionsFromAccountingAccountGroup) {
			// Included where No Parent or Parent is NOT Associated with a Position
			transactionList = BeanUtils.filter(transactionList, accountingTransaction -> (accountingTransaction.getParentTransaction() == null || !accountingTransaction.getParentTransaction().getAccountingAccount().isPosition()));
		}
		return MathUtils.negate(CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit));
	}


	private List<AccountingAccount> getAccountingAccountListByGroupId(Short accountingAccountGroupId) {
		if (accountingAccountGroupId == null) {
			return null;
		}
		String lookupKey = BeanUtils.createKeyFromBeans(ACCOUNTING_ACCOUNT_GROUP_ACCOUNTING_ACCOUNT_LIST, accountingAccountGroupId);
		return getContext().getOrSupplyBean(lookupKey, () -> getAccountingAccountService().getAccountingAccountListForGroup(accountingAccountGroupId), ArrayList::new);
	}


	private Map<String, Map<AccountingAccount, List<AccountingTransaction>>> getTransactionListByAccountingAccountDailyMap(InvestmentAccount investmentAccount, Date startDate, Date endDate, List<AccountingAccount> accountingAccountList, boolean includeLastDayOfPreviousMonth) {
		String lookupKey = BeanUtils.createKeyFromBeans(TRANSACTION_LIST_BY_ACCOUNTING_ACCOUNT_DAILY_MAP, investmentAccount, startDate, endDate, includeLastDayOfPreviousMonth, accountingAccountList);
		return getContext().getOrSupplyBean(lookupKey, () ->
				lookupTransactionListByAccountingAccountDailyMap(investmentAccount, startDate, endDate, accountingAccountList, includeLastDayOfPreviousMonth), HashMap::new);
	}


	private Map<String, Map<AccountingAccount, List<AccountingTransaction>>> lookupTransactionListByAccountingAccountDailyMap(InvestmentAccount investmentAccount, Date startDate, Date endDate, List<AccountingAccount> accountingAccountList, boolean includeLastDayOfPreviousMonth) {
		if (CollectionUtils.isEmpty(accountingAccountList)) {
			return null;
		}

		if (includeLastDayOfPreviousMonth) {
			//It is important to note that some calculations may require inclusion from previous weekday
			startDate = DateUtils.getLastDayOfPreviousMonth(startDate);
		}

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setMinTransactionDate(startDate);
		searchForm.setMaxTransactionDate(endDate);
		searchForm.setClientInvestmentAccountId(investmentAccount.getId());
		//Get a list of all cash flows over the date range. These cash flow accounting account look ups are cached by name
		searchForm.setAccountingAccountIds(BeanUtils.getPropertyValues(accountingAccountList, AccountingAccount::getId, Short.class));
		List<AccountingTransaction> transactionList = getAccountingTransactionService().getAccountingTransactionList(searchForm);

		Map<Date, List<AccountingTransaction>> transactionValueDailyListMap = BeanUtils.getBeansMap(transactionList, this::getAccountingTransactionDateForPerformance);
		Map<String, Map<AccountingAccount, List<AccountingTransaction>>> transactionAccountingAccountDailyMap = new HashMap<>();

		//Build a map of date to total cash flow for the day
		for (Map.Entry<Date, List<AccountingTransaction>> dateListEntry : transactionValueDailyListMap.entrySet()) {
			List<AccountingTransaction> dayTransactionList = dateListEntry.getValue();
			Map<AccountingAccount, List<AccountingTransaction>> accountingAccountTransactionMap = transactionAccountingAccountDailyMap.computeIfAbsent(DateUtils.fromDateShort(dateListEntry.getKey()), k -> new HashMap<>());

			for (AccountingAccount accountingAccount : CollectionUtils.getIterable(accountingAccountList)) {
				accountingAccountTransactionMap.put(accountingAccount, BeanUtils.filter(dayTransactionList, AccountingTransaction::getAccountingAccount, accountingAccount));
			}
		}
		return transactionAccountingAccountDailyMap;
	}


	private Date getAccountingTransactionDateForPerformance(AccountingTransaction transaction) {
		Date date = transaction.getTransactionDate();
		// If it's not the Last day of the month but it is a weekend, move the date forward
		if (!DateUtils.isLastDayOfMonth(date) && DateUtils.isWeekend(date)) {
			// Move forward to the next week day (OR to the last day of the month)
			if (DateUtils.isDateAfter(DateUtils.getNextWeekday(date), DateUtils.getLastDayOfMonth(date))) {
				date = DateUtils.getLastDayOfMonth(date);
			}
			else {
				date = DateUtils.getNextWeekday(date);
			}
		}
		return date;
	}

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns Active Performance Composite Assignments for the Given Performance Account Info Object
	 * NOTE: Referenced in System Bean Property for System Bean: 2 - Performance Account Dependent Composite Assignment Comparison
	 */
	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeInvestmentAccountList(PerformanceAccountInfo performanceAccountInfo) {
		return getContext().getOrSupplyBean(PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_LIST, () -> getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(performanceAccountInfo.getClientAccount().getId(), performanceAccountInfo.getAccountingPeriod().getId()), ArrayList::new);
	}


	public PerformanceCompositeInvestmentAccount getPerformanceCompositeInvestmentAccount(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		return CollectionUtils.getFirstElement(getPerformanceCompositeInvestmentAccountList(accountPerformance));
	}

	////////////////////////////////////////////////////////////////////////////////


	// NOTE: Referenced in System Bean Property for System Bean: Performance Composite Account Composite Performance Entity Comparison
	public List<PerformanceCompositePerformance> getPerformanceCompositePerformanceForAccountPerformance(PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount, PerformanceAccountInfo performanceAccountInfo) {
		String beanKey = BeanUtils.createKeyFromBeans(PERFORMANCE_COMPOSITE_PERFORMANCE_INVESTMENT_ACCOUNT, performanceCompositeInvestmentAccount, performanceAccountInfo);
		return getContext().getOrSupplyBean(beanKey, () -> {
			PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
			searchForm.setPerformanceCompositeId(performanceCompositeInvestmentAccount.getPerformanceComposite().getId());
			searchForm.setAccountingPeriodId(performanceAccountInfo.getAccountingPeriod().getId());
			return getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(searchForm);
		}, ArrayList::new);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods             ////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public AccountingAccountService getAccountingAccountService() {
		return this.accountingAccountService;
	}


	public void setAccountingAccountService(AccountingAccountService accountingAccountService) {
		this.accountingAccountService = accountingAccountService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}
}
