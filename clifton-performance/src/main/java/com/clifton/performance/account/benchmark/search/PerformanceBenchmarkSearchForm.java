package com.clifton.performance.account.benchmark.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class PerformanceBenchmarkSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchFieldPath = "clientAccount", searchField = "teamSecurityGroup.id")
	private Short teamSecurityGroupId;

	@SearchField(searchField = "benchmarkSecurity.id")
	private Integer benchmarkSecurityId;

	@SearchField(searchField = "benchmarkInterestRateIndex.id")
	private Integer benchmarkInterestRateIndexId;

	@SearchField
	private String name;

	@SearchField
	private Integer benchmarkOrder;

	// Custom Search Field
	private Date activeOnDate;

	// Custom Search Field - used with activeOnEndDate to find those active within a date range
	private Date activeOnStartDate;

	// Custom Search Field - used with activeOnStartDate to find those active within a date range
	private Date activeOnEndDate;

	@SearchField
	private Date startDate;

	@SearchField
	private Date endDate;


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	public Integer getBenchmarkInterestRateIndexId() {
		return this.benchmarkInterestRateIndexId;
	}


	public void setBenchmarkInterestRateIndexId(Integer benchmarkInterestRateIndexId) {
		this.benchmarkInterestRateIndexId = benchmarkInterestRateIndexId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Integer getBenchmarkOrder() {
		return this.benchmarkOrder;
	}


	public void setBenchmarkOrder(Integer benchmarkOrder) {
		this.benchmarkOrder = benchmarkOrder;
	}


	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Short getTeamSecurityGroupId() {
		return this.teamSecurityGroupId;
	}


	public void setTeamSecurityGroupId(Short teamSecurityGroupId) {
		this.teamSecurityGroupId = teamSecurityGroupId;
	}


	public Date getActiveOnStartDate() {
		return this.activeOnStartDate;
	}


	public void setActiveOnStartDate(Date activeOnStartDate) {
		this.activeOnStartDate = activeOnStartDate;
	}


	public Date getActiveOnEndDate() {
		return this.activeOnEndDate;
	}


	public void setActiveOnEndDate(Date activeOnEndDate) {
		this.activeOnEndDate = activeOnEndDate;
	}
}
