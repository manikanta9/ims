package com.clifton.performance.account.process.calculators;


import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;


/**
 * The <code>PerformanceInvestmentAccountCalculatorStep</code> defines the step
 * Each step can have a section name for independent processing, the calculator to process or recalculate,
 * and the order of the step
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountCalculatorStep<T extends PerformanceInvestmentAccountConfig> {

	private short order;

	private PerformanceInvestmentAccountCalculator<T> calculatorBean;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public short getOrder() {
		return this.order;
	}


	public void setOrder(short order) {
		this.order = order;
	}


	public PerformanceInvestmentAccountCalculator<T> getCalculatorBean() {
		return this.calculatorBean;
	}


	public void setCalculatorBean(PerformanceInvestmentAccountCalculator<T> calculatorBean) {
		this.calculatorBean = calculatorBean;
	}
}
