package com.clifton.performance.account;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.business.service.BusinessService;
import com.clifton.investment.account.InvestmentAccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceInvestmentAccountDataRetriever</code> handles account related look up
 * related to performance.  i.e. custom fields, account inception date, etc.
 *
 * @author manderson
 */
public interface PerformanceInvestmentAccountDataRetriever {

	////////////////////////////////////////////////////////////////
	//////       Performance Custom Field Lookup Methods      //////
	////////////////////////////////////////////////////////////////


	/**
	 * Looks up the custom field value for the given account and field name under Performance Summary Fields column group
	 */
	public Object getPerformanceInvestmentAccountCustomFieldValue(InvestmentAccount account, String fieldName);


	/**
	 * Looks up the custom field value for the given business service and field name under Service Performance Summary Fields column group
	 */
	public Object getPerformanceBusinessServiceCustomFieldValue(BusinessService service, String fieldName);


	public Date getPerformanceInvestmentAccountInceptionDate(int clientAccountId);


	////////////////////////////////////////////////////////////////
	/////   Performance Position Related Data Lookup Methods  //////
	////////////////////////////////////////////////////////////////


	/**
	 * Currently uses the AccountingPositionDaily snapshots for actual position gain/loss and merges with CCY AccountingPositionDaily Live
	 * lookup for CCY gain/loss
	 * <p>
	 * Looks up entire month and returns a map of dates -> List of AccountingPositionDaily objects
	 * includeLastDayOfMonthInLastWeekday - If true and last weekday of month != last day of month will include everything from last weekday to last day in the last weekday
	 * <p>
	 * Uses DateFormat Short for Date to prevent Date vs. Timestamp object comparison issues
	 */
	public Map<String, List<AccountingPositionDaily>> getAccountingPositionDailyMapForPerformanceAccount(InvestmentAccount investmentAccount, AccountingPeriod accountingPeriod, boolean includeLastDayOfPreviousMonth, boolean includeLastDayOfMonthInLastWeekday, boolean includeCurrency);


	public List<AccountingPositionDaily> getAccountingPositionDailyListForPerformanceAccount(InvestmentAccount investmentAccount, Date startDate, Date endDate, boolean includeCurrency);


	/**
	 * Returns a map of Date (represented as a String using short format because of comparison issues) to the accrued interest value
	 * from Lending Repos.  This accrued interest can be included in gain/loss calculations
	 */
	public Map<String, BigDecimal> getLendingRepoAccruedInterestMapForPerformanceAccount(PerformanceInvestmentAccount performanceAccount);


	/**
	 * Returns cash balance mapped by date for an account - if includeAllNonPositionAssetsAndLiabilities is true, returns truly all Non Position Assets And Liabilities
	 * Otherwise returns all Cash + Receivables
	 */
	public Map<String, BigDecimal> getCashValueDailyMapForPerformanceAccount(InvestmentAccount investmentAccount, AccountingPeriod accountingPeriod, boolean includeLastWeekdayOfPreviousMonth, boolean includeAllNonPositionAssetsAndLiabilities);
}
