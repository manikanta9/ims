package com.clifton.performance.account.benchmark.process;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceBenchmarkProcessService</code> ...
 *
 * @author manderson
 */
public interface PerformanceBenchmarkProcessService {

	////////////////////////////////////////////////////////////////////////////
	/////        Performance Summary Benchmark Processing Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Exposed UI method that also recalculates historical returns for updated Benchmark (used when MTD Override is updated)
	 *
	 * @param bean
	 */
	public PerformanceSummaryBenchmark savePerformanceSummaryBenchmark(PerformanceSummaryBenchmark bean);


	/**
	 * Independent load/processing of benchmarks from the UI
	 *
	 * @param summaryId
	 */
	public void processPerformanceSummaryBenchmarkListForSummary(int summaryId);


	/**
	 * Generates the list of {@link PerformanceSummaryBenchmark} for the summary
	 * DOES NOT perform any saves - just recalculates and returns the applicable list
	 *
	 * @param summaryId
	 */
	@DoNotAddRequestMapping
	public List<PerformanceSummaryBenchmark> generatePerformanceSummaryBenchmarkListForSummary(int summaryId);


	/**
	 * Option to reload/recalc existing (will not overwrite MTD overrides but will reset calculated value)
	 *
	 * @param benchmarkId
	 * @param reload
	 * @param reloadFromDate - used with reload to only recalculate from a specific date
	 */
	public void processPerformanceSummaryBenchmarkListForBenchmark(int benchmarkId, boolean reload, Date reloadFromDate);
}
