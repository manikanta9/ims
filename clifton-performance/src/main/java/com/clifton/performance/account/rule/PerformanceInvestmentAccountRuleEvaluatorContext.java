package com.clifton.performance.account.rule;

import com.clifton.investment.account.assetclass.InvestmentAccountAssetClass;
import com.clifton.investment.account.assetclass.InvestmentAccountAssetClassService;
import com.clifton.investment.account.assetclass.search.InvestmentAccountAssetClassSearchForm;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.PerformanceBenchmarkService;
import com.clifton.performance.account.benchmark.PerformanceSummaryBenchmark;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Used by all Performance Summary Rule evaluation and contains common information (easily stored/retrieved from context) and re-used across various rule evaluations
 * <p>
 * Note: There is extended class in product project that is actually instantiated there because of detail list dependencies
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountRuleEvaluatorContext extends PerformanceAccountInfoRuleEvaluatorContext {

	private static final String INVESTMENT_ACCOUNT_PERFORMANCE_INCEPTION_DATE = "INVESTMENT_ACCOUNT_PERFORMANCE_INCEPTION_DATE";
	private static final String INVESTMENT_ACCOUNT_ASSET_CLASS_LIST = "INVESTMENT_ACCOUNT_ASSET_CLASS_LIST";
	private static final String PERFORMANCE_SUMMARY_BENCHMARK_LIST = "PERFORMANCE_SUMMARY_BENCHMARK_LIST";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private InvestmentAccountAssetClassService investmentAccountAssetClassService;

	private PerformanceBenchmarkService performanceBenchmarkService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getInvestmentAccountPerformanceInceptionDate(PerformanceAccountInfo performanceInvestmentAccount) {
		return getContext().getOrSupplyBean(INVESTMENT_ACCOUNT_PERFORMANCE_INCEPTION_DATE, () -> getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountInceptionDate(performanceInvestmentAccount.getClientAccount().getId()));
	}


	/**
	 * Note: Includes asset classes that are currently active, and excludes roll-up asset classes
	 */
	public List<InvestmentAccountAssetClass> getInvestmentAccountAssetClassList(PerformanceAccountInfo performanceAccount) {
		return getContext().getOrSupplyBean(INVESTMENT_ACCOUNT_ASSET_CLASS_LIST, () -> {
			InvestmentAccountAssetClassSearchForm searchForm = new InvestmentAccountAssetClassSearchForm();
			searchForm.setAccountId(performanceAccount.getClientAccount().getId());
			searchForm.setRollupAssetClass(false);
			return getInvestmentAccountAssetClassService().getInvestmentAccountAssetClassList(searchForm);
		}, ArrayList::new);
	}


	public List<PerformanceSummaryBenchmark> getPerformanceSummaryBenchmarkList(PerformanceInvestmentAccount performanceInvestmentAccount) {
		@SuppressWarnings("unchecked")
		List<PerformanceSummaryBenchmark> summaryBenchmarkList = (List<PerformanceSummaryBenchmark>) getContext().getBean(PERFORMANCE_SUMMARY_BENCHMARK_LIST);

		if (summaryBenchmarkList == null) {
			summaryBenchmarkList = getPerformanceBenchmarkService().getPerformanceSummaryBenchmarkListBySummary(performanceInvestmentAccount.getId());
			if (summaryBenchmarkList == null) {
				summaryBenchmarkList = new ArrayList<>();
			}
			getContext().setBean(PERFORMANCE_SUMMARY_BENCHMARK_LIST, summaryBenchmarkList);
		}
		return summaryBenchmarkList;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods            ////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountAssetClassService getInvestmentAccountAssetClassService() {
		return this.investmentAccountAssetClassService;
	}


	public void setInvestmentAccountAssetClassService(InvestmentAccountAssetClassService investmentAccountAssetClassService) {
		this.investmentAccountAssetClassService = investmentAccountAssetClassService;
	}


	public PerformanceBenchmarkService getPerformanceBenchmarkService() {
		return this.performanceBenchmarkService;
	}


	public void setPerformanceBenchmarkService(PerformanceBenchmarkService performanceBenchmarkService) {
		this.performanceBenchmarkService = performanceBenchmarkService;
	}
}
