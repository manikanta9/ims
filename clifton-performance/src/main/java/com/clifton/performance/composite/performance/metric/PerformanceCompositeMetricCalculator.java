package com.clifton.performance.composite.performance.metric;


import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeMetricContext;


/**
 * The <code>PerformanceCompositeMetricCalculator</code> calculates performance metric property values
 *
 * @author apopp
 */
public interface PerformanceCompositeMetricCalculator {

	/**
	 * Calculate all calculation types provided for each entity within the performance metric list
	 */
	public <T extends PerformanceCompositeMetric> void calculatePerformanceMetric(PerformanceCompositeMetricCalculatorCommand<T> calculatorCommand, PerformanceCompositeMetricContext<T> performanceCompositeMetricContext);
}
