package com.clifton.performance.composite.performance.rebuild;

import java.util.Date;


/**
 * @author manderson
 */
public class PerformanceCompositeAccountRebuildCommand extends PerformanceCompositeRebuildCommand {


	private Integer performanceCompositeInvestmentAccountId;

	private Integer investmentAccountId;
	private Integer investmentAccountGroupId;


	// Used to preview specific date's values
	private Date measureDate;


	/**
	 * Calculate the daily performance and base monthly return values (current period net,gross,etc)
	 */
	private boolean calculateAccountDailyMonthlyBasePerformance;


	// Ability During Preview to preview using system calculated values only - excluding overrides
	private boolean previewIgnoreOverrides;

	/**
	 * If metricProcessingType (see parent class) is BENCHMARKS_ONLY and a specific composite is selected
	 * then after processing the account benchmarks, the composite benchmarks will also be re-processed
	 */
	private boolean processCompositeBenchmarks;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getUniqueRunId() {
		StringBuilder id = new StringBuilder(16);
		if (getInvestmentAccountGroupId() != null) {
			id.append("G-").append(getInvestmentAccountGroupId());
		}
		if (getInvestmentAccountId() != null) {
			id.append("A-").append(getInvestmentAccountId());
		}
		if (getPerformanceCompositeInvestmentAccountId() != null) {
			id.append("ASSIGN-").append(getPerformanceCompositeInvestmentAccountId());
		}
		id.append(super.getUniqueRunId());
		return id.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPerformanceCompositeInvestmentAccountId() {
		return this.performanceCompositeInvestmentAccountId;
	}


	public void setPerformanceCompositeInvestmentAccountId(Integer performanceCompositeInvestmentAccountId) {
		this.performanceCompositeInvestmentAccountId = performanceCompositeInvestmentAccountId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public boolean isCalculateAccountDailyMonthlyBasePerformance() {
		return this.calculateAccountDailyMonthlyBasePerformance;
	}


	public void setCalculateAccountDailyMonthlyBasePerformance(boolean calculateAccountDailyMonthlyBasePerformance) {
		this.calculateAccountDailyMonthlyBasePerformance = calculateAccountDailyMonthlyBasePerformance;
	}


	public boolean isPreviewIgnoreOverrides() {
		return this.previewIgnoreOverrides;
	}


	public void setPreviewIgnoreOverrides(boolean previewIgnoreOverrides) {
		this.previewIgnoreOverrides = previewIgnoreOverrides;
	}


	public boolean isProcessCompositeBenchmarks() {
		return this.processCompositeBenchmarks;
	}


	public void setProcessCompositeBenchmarks(boolean processCompositeBenchmarks) {
		this.processCompositeBenchmarks = processCompositeBenchmarks;
	}
}
