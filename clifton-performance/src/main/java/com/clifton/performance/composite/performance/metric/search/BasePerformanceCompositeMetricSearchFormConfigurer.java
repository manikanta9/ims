package com.clifton.performance.composite.performance.metric.search;

import com.clifton.core.dataaccess.search.hibernate.expression.InExpressionForNumbers;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowStatus;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaQuery;


/**
 * The <code>BasePerformanceCompositeMetricSearchFormConfigurer</code> class configures search criteria for the BasePerformanceCompositeMetricSearchForm search form.
 * <p>
 * NOTE: Used for PerformanceCompositePerformance and PerformanceCompositeInvestmentAccountPerformance
 *
 * @author vgomelsky
 */

public class BasePerformanceCompositeMetricSearchFormConfigurer extends WorkflowAwareSearchFormConfigurer {

	private SecurityAuthorizationService securityAuthorizationService;

	private static final String SECURITY_RESOURCE_NAME = "Performance Composite Performance";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public BasePerformanceCompositeMetricSearchFormConfigurer(BasePerformanceCompositeMetricSearchForm searchForm, WorkflowDefinitionService workflowDefinitionService, SecurityAuthorizationService securityAuthorizationService) {
		super(searchForm, workflowDefinitionService, true);
		this.securityAuthorizationService = securityAuthorizationService;
		configureSearchForm();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		BasePerformanceCompositeMetricSearchForm searchForm = (BasePerformanceCompositeMetricSearchForm) getSortableSearchForm();

		if (searchForm.getPerformanceFrequency() != null) {
			criteria.add(new InExpressionForNumbers(getPathAlias("accountingPeriod", criteria) + "." + "endDate", searchForm.getPerformanceFrequency().getMonths()) {
				             @Override
				             public String[] getColumns(Criteria criteria, CriteriaQuery criteriaQuery) {
					             String[] columns = super.getColumns(criteria, criteriaQuery);

					             if (columns != null) {
						             for (int i = 0; i < columns.length; i++) {
							             columns[i] = "MONTH(" + columns[i] + ")";
						             }
					             }
					             return columns;
				             }
			             }
			);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void configureSearchForm() {
		// Needs to run first so that the ids are set prior to super method
		BasePerformanceCompositeMetricSearchForm searchForm = (BasePerformanceCompositeMetricSearchForm) getSortableSearchForm();
		if (BooleanUtils.isTrue(searchForm.getRequireFullControlForDraftStatus())) {
			// Ignore for Admins
			if (!this.securityAuthorizationService.isSecurityUserAdmin()) {
				if (!this.securityAuthorizationService.isSecurityAccessAllowed(SECURITY_RESOURCE_NAME, SecurityPermission.PERMISSION_FULL_CONTROL)) {
					WorkflowStatus draftStatus = getWorkflowDefinitionService().getWorkflowStatusByName(WorkflowStatus.STATUS_DRAFT);
					// Add it if not filtering on exclude workflow status
					if (searchForm.getExcludeWorkflowStatusId() == null) {
						searchForm.setExcludeWorkflowStatusId(draftStatus.getId());
					}
					// Ignore if already excluding draft - otherwise move both values to the array
					else if (!MathUtils.isEqual(searchForm.getExcludeWorkflowStatusId(), draftStatus.getId())) {
						Short[] excludeStatuses = new Short[]{draftStatus.getId(), searchForm.getExcludeWorkflowStatusId()};
						searchForm.setExcludeWorkflowStatusId(null);
						if (!ArrayUtils.isEmpty(searchForm.getExcludeWorkflowStatusIds())) {
							excludeStatuses = ArrayUtils.addAll(excludeStatuses, searchForm.getExcludeWorkflowStatusIds());
						}
						searchForm.setExcludeWorkflowStatusIds(excludeStatuses);
					}
				}
			}
		}

		super.configureSearchForm();
	}
}
