package com.clifton.performance.composite.performance.rule.context;

import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.rule.evaluator.BaseRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeRuleEvaluatorContext</code> allows for optimized rule evaluation by caching and helper methods.
 *
 * @author apopp
 */
public class PerformanceCompositeRuleEvaluatorContext extends BaseRuleEvaluatorContext implements PerformanceCompositeMetricContext<PerformanceCompositePerformance> {

	private static final String PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_PERFORMANCE_LIST = "PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_PERFORMANCE_LIST";
	private static final String PERFORMANCE_COMPOSITE_PERFORMANCE_PREVIOUS_LIST = "PERFORMANCE_COMPOSITE_PERFORMANCE_PREVIOUS_LIST";

	////////////////////////////////////////////////////////////////////////////////

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Return Active Account Performance Used to Compute this Performance Composite Performance Object
	 */
	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceList(PerformanceCompositePerformance performanceCompositePerformance) {
		return getContext().getOrSupplyBean(PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_PERFORMANCE_LIST, () -> {
					PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
					searchForm.setPerformanceCompositeId(performanceCompositePerformance.getPerformanceComposite().getId());
					searchForm.setAccountingPeriodId(performanceCompositePerformance.getAccountingPeriod().getId());
					return getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);
				}, ArrayList<PerformanceCompositeInvestmentAccountPerformance>::new
		);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////            Performance Composite Metric Methods           ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getBenchmarkStartDateOverride(PerformanceCompositePerformance performanceCompositeMetric) {
		// No Overrides at the composite level
		return null;
	}


	@Override
	public boolean isModelFeeCalculatedFromGross(PerformanceCompositePerformance compositePerformance) {
		return false;
	}


	@Override
	public BigDecimal getModelFee(PerformanceCompositePerformance compositePerformance) {
		return getPerformanceCompositeSetupService().getModelFeeForPerformanceCompositeAndDate(compositePerformance.getPerformanceComposite(), compositePerformance.getAccountingPeriod().getStartDate());
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PerformanceCompositePerformance> getPreviousPerformanceCompositeMetricList(PerformanceCompositePerformance compositePerformance) {
		return getContext().getOrSupplyBean(PERFORMANCE_COMPOSITE_PERFORMANCE_PREVIOUS_LIST, () -> {
					PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
					searchForm.setPerformanceCompositeId(compositePerformance.getPerformanceComposite().getId());
					searchForm.setBeforePeriodEndDate(compositePerformance.getAccountingPeriod().getStartDate());
					return getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(searchForm);
				}, ArrayList<PerformanceCompositePerformance>::new
		);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////           Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}
}
