package com.clifton.performance.composite.performance.metric;

import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * @author manderson
 */
public enum PerformanceMetricPeriods {

	MONTH_TO_DATE(1, false), //
	QUARTER_TO_DATE {
		@Override
		public Date getPeriodDateFromDate(Date fromDate) {
			return DateUtils.getFirstDayOfQuarter(fromDate);
		}
	}, //
	YEAR_TO_DATE {
		@Override
		public Date getPeriodDateFromDate(Date fromDate) {
			return DateUtils.getFirstDayOfYear(fromDate);
		}
	}, //
	INCEPTION_TO_DATE, //
	ONE_YEAR(12, true), //
	THREE_YEAR(36, true), //
	FIVE_YEAR(60, true), //
	TEN_YEAR(120, true); //

	////////////////////////////////////////////////////////////////////////////////

	private final Integer monthsInPeriod;

	/**
	 * If true, will only process the period calculation if inception date is on or before the period date
	 * i.e. Inception to Date, Year to Date, Quarter to Date will always process, but One Year, 3 Year, etc. will only process if there is one full year, 3 full years, etc. of performance
	 */
	private boolean validateInceptionDate;


	////////////////////////////////////////////////////////////////////////////////


	PerformanceMetricPeriods() {
		this(null, false);
	}


	PerformanceMetricPeriods(Integer monthsInPeriod, boolean validateInceptionDate) {

		this.monthsInPeriod = monthsInPeriod;
		this.validateInceptionDate = validateInceptionDate;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getPeriodDateFromDate(Date fromDate) {
		if (getMonthsInPeriod() != null) {
			// We subtract one month because we are including current month in the calculation
			return DateUtils.addMonths(fromDate, -(getMonthsInPeriod() - 1));
		}
		return null;
	}


	public boolean isPeriodValidForInceptionDate(Date inceptionDate, Date fromDate) {
		return !isValidateInceptionDate() || DateUtils.isDateAfterOrEqual(getPeriodDateFromDate(fromDate), inceptionDate);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getMonthsInPeriod() {
		return this.monthsInPeriod;
	}


	public boolean isValidateInceptionDate() {
		return this.validateInceptionDate;
	}
}
