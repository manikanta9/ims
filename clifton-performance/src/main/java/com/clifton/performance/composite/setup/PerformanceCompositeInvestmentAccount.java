package com.clifton.performance.composite.setup;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.report.definition.Report;
import com.clifton.system.bean.SystemBean;

import java.util.Date;


/**
 * The <code>PerformanceCompositeInvestmentAccount</code> class assigns an investment account to a composite
 * for the specified date range.  The same account maybe assigned to multiple composites.
 *
 * @author vgomelsky
 */
public class PerformanceCompositeInvestmentAccount extends BaseEntity<Integer> implements LabeledObject {

	private PerformanceComposite performanceComposite;
	private InvestmentAccount investmentAccount;

	/**
	 * Account level benchmark overrides
	 */
	private InvestmentSecurity benchmarkSecurity;
	private InvestmentSecurity secondaryBenchmarkSecurity;
	private InvestmentSecurity thirdBenchmarkSecurity;

	private Date startDate;
	private Date endDate;


	/**
	 * Optional override to the account performance calculator bean defined on the composite
	 */
	private SystemBean accountPerformanceCalculatorBean;

	// Reporting/Posting Configuration

	/**
	 * Optional override to the account report selection on the composite
	 */
	private Report accountReport;

	/**
	 * Optional override to the accountReportFrequency on the composite
	 */
	private PerformanceReportFrequencies accountReportFrequency;

	/**
	 * Optional override to the accountReportAppendAttachments on the composite
	 * This feature will append any file attachments on the account performance record to the generated report
	 * Note: Currently only used for Investor Reports
	 */
	private Boolean accountReportAppendAttachments;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}


	@Override
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		if (getPerformanceComposite() != null) {
			label.append(getPerformanceComposite().getName());
		}
		label.append(" - ");
		if (getInvestmentAccount() != null) {
			label.append(getInvestmentAccount().getLabel());
		}
		label.append(" ");
		label.append(DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true));
		return label.toString();
	}


	/**
	 * Note: Used as the CAPS Identifier - Account #_Composite Code
	 */
	public String getShortLabel() {
		StringBuilder label = new StringBuilder();
		if (getInvestmentAccount() != null) {
			label.append(getInvestmentAccount().getNumber());
		}
		label.append("_");
		if (getPerformanceComposite() != null) {
			label.append(getPerformanceComposite().getCode());
		}
		return label.toString();
	}


	public String getLabelWithoutAccount() {
		StringBuilder label = new StringBuilder();
		if (getPerformanceComposite() != null) {
			label.append(getPerformanceComposite().getName());
		}
		label.append(" ");
		label.append(DateUtils.fromDateRange(getStartDate(), getEndDate(), true, true));
		return label.toString();
	}


	public SystemBean getCoalesceAccountPerformanceCalculatorBean() {
		if (getAccountPerformanceCalculatorBean() != null) {
			return getAccountPerformanceCalculatorBean();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getAccountPerformanceCalculatorBean();
		}
		return null;
	}


	public void setCoalesceAccountPerformanceCalculatorBean(SystemBean coalesceAccountPerformanceCalculatorBean) {
		if (coalesceAccountPerformanceCalculatorBean == null || (getPerformanceComposite() != null && coalesceAccountPerformanceCalculatorBean.equals(getPerformanceComposite().getAccountPerformanceCalculatorBean()))) {
			setAccountPerformanceCalculatorBean(null);
		}
		else {
			setAccountPerformanceCalculatorBean(coalesceAccountPerformanceCalculatorBean);
		}
	}


	public InvestmentSecurity getCoalesceBenchmarkSecurity() {
		if (getBenchmarkSecurity() != null) {
			return getBenchmarkSecurity();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getBenchmarkSecurity();
		}
		return null;
	}


	public InvestmentSecurity getCoalesceSecondaryBenchmarkSecurity() {
		if (getSecondaryBenchmarkSecurity() != null) {
			return getSecondaryBenchmarkSecurity();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getSecondaryBenchmarkSecurity();
		}
		return null;
	}


	public InvestmentSecurity getCoalesceThirdBenchmarkSecurity() {
		if (getThirdBenchmarkSecurity() != null) {
			return getThirdBenchmarkSecurity();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getThirdBenchmarkSecurity();
		}
		return null;
	}


	public Report getCoalesceAccountReport() {
		if (getAccountReport() != null) {
			return getAccountReport();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getAccountReport();
		}
		return null;
	}


	public void setCoalesceAccountReport(Report coalesceAccountReport) {
		if (coalesceAccountReport == null || (getPerformanceComposite() != null && coalesceAccountReport.equals(getPerformanceComposite().getAccountReport()))) {
			setAccountReport(null);
		}
		else {
			setAccountReport(coalesceAccountReport);
		}
	}


	public PerformanceReportFrequencies getCoalesceAccountReportFrequency() {
		if (getAccountReportFrequency() != null) {
			return getAccountReportFrequency();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getAccountReportFrequency();
		}
		return null;
	}


	public void setCoalesceAccountReportFrequency(PerformanceReportFrequencies coalesceAccountReportFrequency) {
		if (coalesceAccountReportFrequency == null || (getPerformanceComposite() != null && coalesceAccountReportFrequency == getPerformanceComposite().getAccountReportFrequency())) {
			setAccountReportFrequency(null);
		}
		else {
			setAccountReportFrequency(coalesceAccountReportFrequency);
		}
	}


	public boolean isCoalesceAccountReportAppendAttachments() {
		if (getAccountReportAppendAttachments() != null) {
			return getAccountReportAppendAttachments();
		}
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().isAccountReportAppendAttachments();
		}
		return false;
	}


	public void setCoalesceAccountReportAppendAttachments(boolean coalesceAccountReportAppendAttachments) {
		if (getPerformanceComposite() != null && getPerformanceComposite().isAccountReportAppendAttachments() == coalesceAccountReportAppendAttachments) {
			setAccountReportAppendAttachments(null);
		}
		else {
			setAccountReportAppendAttachments(coalesceAccountReportAppendAttachments);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public PerformanceComposite getPerformanceComposite() {
		return this.performanceComposite;
	}


	public void setPerformanceComposite(PerformanceComposite performanceComposite) {
		this.performanceComposite = performanceComposite;
	}


	public InvestmentAccount getInvestmentAccount() {
		return this.investmentAccount;
	}


	public void setInvestmentAccount(InvestmentAccount investmentAccount) {
		this.investmentAccount = investmentAccount;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public InvestmentSecurity getSecondaryBenchmarkSecurity() {
		return this.secondaryBenchmarkSecurity;
	}


	public void setSecondaryBenchmarkSecurity(InvestmentSecurity secondaryBenchmarkSecurity) {
		this.secondaryBenchmarkSecurity = secondaryBenchmarkSecurity;
	}


	public InvestmentSecurity getThirdBenchmarkSecurity() {
		return this.thirdBenchmarkSecurity;
	}


	public void setThirdBenchmarkSecurity(InvestmentSecurity thirdBenchmarkSecurity) {
		this.thirdBenchmarkSecurity = thirdBenchmarkSecurity;
	}


	public Report getAccountReport() {
		return this.accountReport;
	}


	public void setAccountReport(Report accountReport) {
		this.accountReport = accountReport;
	}


	public PerformanceReportFrequencies getAccountReportFrequency() {
		return this.accountReportFrequency;
	}


	public void setAccountReportFrequency(PerformanceReportFrequencies accountReportFrequency) {
		this.accountReportFrequency = accountReportFrequency;
	}


	public Boolean getAccountReportAppendAttachments() {
		return this.accountReportAppendAttachments;
	}


	public void setAccountReportAppendAttachments(Boolean accountReportAppendAttachments) {
		this.accountReportAppendAttachments = accountReportAppendAttachments;
	}


	public SystemBean getAccountPerformanceCalculatorBean() {
		return this.accountPerformanceCalculatorBean;
	}


	public void setAccountPerformanceCalculatorBean(SystemBean accountPerformanceCalculatorBean) {
		this.accountPerformanceCalculatorBean = accountPerformanceCalculatorBean;
	}
}
