package com.clifton.performance.composite.performance.metric;


import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceMetricFieldTypes</code> contains metadata and support methods for calculating different properties on a performance metric entity.
 * <p>
 * Each type has information on which field the calculation is for and which field is used to perform the calculation.  This is useful for entities where properties are rollups
 * across entities of the same type for another property.
 * <p>
 * It also specifies how many entities back we need to go to perform the calculation. Period value states go back X entities to perform this calculation.  (Which usually corresponds to months)
 * Period month states a specific month to go back to (YTD)
 *
 * @author apopp
 */
public enum PerformanceMetricFieldTypes {

	// Passive fields - these fields are calculated independently based on composite or account set up which have their own calculations
	PERIOD_GROSS_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodGrossReturn", null),
	PERIOD_NET_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodNetReturn", null),
	PERIOD_MODEL_NET_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodModelNetReturn", null),
	MARKET_VALUE(PerformanceMetricPeriods.MONTH_TO_DATE, "periodEndMarketValue", null), //

	// Considered passive and calculated separately as the definition of the benchmarks is pulled from different locations for accounts and composites (accounts need to look at the assignments)
	PERIOD_BENCHMARK_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodBenchmarkReturn", true),
	PERIOD_SECONDARY_BENCHMARK_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodSecondaryBenchmarkReturn", true),
	PERIOD_THIRD_BENCHMARK_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodThirdBenchmarkReturn", true),
	PERIOD_RISK_FREE_SECURITY_RETURN(PerformanceMetricPeriods.MONTH_TO_DATE, "periodRiskFreeSecurityReturn", true),

	GROSS_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateGrossReturn", PERIOD_GROSS_RETURN),
	NET_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateNetReturn", PERIOD_NET_RETURN),
	MODEL_NET_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateModelNetReturn", PERIOD_MODEL_NET_RETURN),
	BENCHMARK_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateBenchmarkReturn", PERIOD_BENCHMARK_RETURN),
	SECONDARY_BENCHMARK_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN),
	THIRD_BENCHMARK_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN),
	RISK_FREE_SECURITY_RETURN_QTD(PerformanceMetricPeriods.QUARTER_TO_DATE, "quarterToDateRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN),

	GROSS_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateGrossReturn", PERIOD_GROSS_RETURN),
	NET_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateNetReturn", PERIOD_NET_RETURN),
	MODEL_NET_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateModelNetReturn", PERIOD_MODEL_NET_RETURN),
	BENCHMARK_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateBenchmarkReturn", PERIOD_BENCHMARK_RETURN),
	SECONDARY_BENCHMARK_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN),
	THIRD_BENCHMARK_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN),
	RISK_FREE_SECURITY_RETURN_YTD(PerformanceMetricPeriods.YEAR_TO_DATE, "yearToDateRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN),

	GROSS_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateGrossReturn", PERIOD_GROSS_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),
	NET_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateNetReturn", PERIOD_NET_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),
	MODEL_NET_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateModelNetReturn", PERIOD_MODEL_NET_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),
	BENCHMARK_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateBenchmarkReturn", PERIOD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),
	SECONDARY_BENCHMARK_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),
	THIRD_BENCHMARK_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),
	RISK_FREE_SECURITY_RETURN_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN, PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY),

	GROSS_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearGrossReturn", PERIOD_GROSS_RETURN),
	NET_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearNetReturn", PERIOD_NET_RETURN),
	MODEL_NET_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearModelNetReturn", PERIOD_MODEL_NET_RETURN),
	BENCHMARK_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearBenchmarkReturn", PERIOD_BENCHMARK_RETURN),
	SECONDARY_BENCHMARK_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN),
	THIRD_BENCHMARK_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN),
	RISK_FREE_SECURITY_RETURN_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN),

	GROSS_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearGrossReturn", PERIOD_GROSS_RETURN),
	NET_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearNetReturn", PERIOD_NET_RETURN),
	MODEL_NET_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearModelNetReturn", PERIOD_MODEL_NET_RETURN),
	BENCHMARK_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearBenchmarkReturn", PERIOD_BENCHMARK_RETURN),
	SECONDARY_BENCHMARK_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN),
	THIRD_BENCHMARK_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN),
	RISK_FREE_SECURITY_RETURN_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN),

	GROSS_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearGrossReturn", PERIOD_GROSS_RETURN),
	NET_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearNetReturn", PERIOD_NET_RETURN),
	MODEL_NET_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearModelNetReturn", PERIOD_MODEL_NET_RETURN),
	BENCHMARK_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearBenchmarkReturn", PERIOD_BENCHMARK_RETURN),
	SECONDARY_BENCHMARK_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN),
	THIRD_BENCHMARK_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN),
	RISK_FREE_SECURITY_RETURN_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN),

	GROSS_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearGrossReturn", PERIOD_GROSS_RETURN),
	NET_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearNetReturn", PERIOD_NET_RETURN),
	MODEL_NET_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearModelNetReturn", PERIOD_MODEL_NET_RETURN),
	BENCHMARK_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearBenchmarkReturn", PERIOD_BENCHMARK_RETURN),
	SECONDARY_BENCHMARK_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearSecondaryBenchmarkReturn", PERIOD_SECONDARY_BENCHMARK_RETURN),
	THIRD_BENCHMARK_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearThirdBenchmarkReturn", PERIOD_THIRD_BENCHMARK_RETURN),
	RISK_FREE_SECURITY_RETURN_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearRiskFreeSecurityReturn", PERIOD_RISK_FREE_SECURITY_RETURN),

	//Standard Deviation Fields
	BENCHMARK_STD_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateBenchmarkStandardDeviation", PERIOD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	SECONDARY_BENCHMARK_STD_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateSecondaryBenchmarkStandardDeviation", PERIOD_SECONDARY_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	THIRD_BENCHMARK_STD_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateThirdBenchmarkStandardDeviation", PERIOD_THIRD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	BENCHMARK_STD_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearBenchmarkStandardDeviation", PERIOD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	SECONDARY_BENCHMARK_STD_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearSecondaryBenchmarkStandardDeviation", PERIOD_SECONDARY_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	THIRD_BENCHMARK_STD_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearThirdBenchmarkStandardDeviation", PERIOD_THIRD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	BENCHMARK_STD_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearBenchmarkStandardDeviation", PERIOD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	SECONDARY_BENCHMARK_STD_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearSecondaryBenchmarkStandardDeviation", PERIOD_SECONDARY_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	THIRD_BENCHMARK_STD_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearThirdBenchmarkStandardDeviation", PERIOD_THIRD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	BENCHMARK_STD_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearBenchmarkStandardDeviation", PERIOD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	BENCHMARK_STD_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearBenchmarkStandardDeviation", PERIOD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	SECONDARY_BENCHMARK_STD_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearSecondaryBenchmarkStandardDeviation", PERIOD_SECONDARY_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	THIRD_BENCHMARK_STD_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearThirdBenchmarkStandardDeviation", PERIOD_THIRD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	SECONDARY_BENCHMARK_STD_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearSecondaryBenchmarkStandardDeviation", PERIOD_SECONDARY_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	THIRD_BENCHMARK_STD_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearThirdBenchmarkStandardDeviation", PERIOD_THIRD_BENCHMARK_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),

	GROSS_STD_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateGrossStandardDeviation", PERIOD_GROSS_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	GROSS_STD_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearGrossStandardDeviation", PERIOD_GROSS_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	GROSS_STD_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearGrossStandardDeviation", PERIOD_GROSS_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	GROSS_STD_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearGrossStandardDeviation", PERIOD_GROSS_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	GROSS_STD_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearGrossStandardDeviation", PERIOD_GROSS_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),

	NET_STD_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateNetStandardDeviation", PERIOD_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	MODEL_NET_STD_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateModelNetStandardDeviation", PERIOD_MODEL_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	NET_STD_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearNetStandardDeviation", PERIOD_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	MODEL_NET_STD_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearModelNetStandardDeviation", PERIOD_MODEL_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	NET_STD_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearNetStandardDeviation", PERIOD_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	MODEL_NET_STD_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearModelNetStandardDeviation", PERIOD_MODEL_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	NET_STD_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearNetStandardDeviation", PERIOD_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	MODEL_NET_STD_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearModelNetStandardDeviation", PERIOD_MODEL_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	NET_STD_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearNetStandardDeviation", PERIOD_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),
	MODEL_NET_STD_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearModelNetStandardDeviation", PERIOD_MODEL_NET_RETURN, PerformanceMetricCalculatorTypes.STANDARD_DEVIATION_ANNUALIZED),

	//Net Sharpe Ratio
	NET_SHARPE_RATIO_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateNetSharpeRatio", NET_RETURN_ITD, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.NET_STD_ITD, "inceptionToDateRiskFreeSecurityReturn"),
	MODEL_NET_SHARPE_RATIO_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateModelNetSharpeRatio", MODEL_NET_RETURN_ITD, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.MODEL_NET_STD_ITD, "inceptionToDateRiskFreeSecurityReturn"),
	NET_SHARPE_RATIO_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearNetSharpeRatio", NET_RETURN_ONE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.NET_STD_ONE_YEAR, "oneYearRiskFreeSecurityReturn"),
	MODEL_NET_SHARPE_RATIO_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearModelNetSharpeRatio", MODEL_NET_RETURN_ONE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.MODEL_NET_STD_ONE_YEAR, "oneYearRiskFreeSecurityReturn"),
	NET_SHARPE_RATIO_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearNetSharpeRatio", NET_RETURN_THREE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.NET_STD_THREE_YEAR, "threeYearRiskFreeSecurityReturn"),
	MODEL_NET_SHARPE_RATIO_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearModelNetSharpeRatio", MODEL_NET_RETURN_THREE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.MODEL_NET_STD_THREE_YEAR, "threeYearRiskFreeSecurityReturn"),
	NET_SHARPE_RATIO_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearNetSharpeRatio", NET_RETURN_FIVE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.NET_STD_FIVE_YEAR, "fiveYearRiskFreeSecurityReturn"),
	MODEL_NET_SHARPE_RATIO_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearModelNetSharpeRatio", MODEL_NET_RETURN_FIVE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.MODEL_NET_STD_FIVE_YEAR, "fiveYearRiskFreeSecurityReturn"),
	NET_SHARPE_RATIO_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearNetSharpeRatio", NET_RETURN_TEN_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.NET_STD_TEN_YEAR, "tenYearRiskFreeSecurityReturn"),
	MODEL_NET_SHARPE_RATIO_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearModelNetSharpeRatio", MODEL_NET_RETURN_TEN_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.MODEL_NET_STD_TEN_YEAR, "tenYearRiskFreeSecurityReturn"),

	//Gross Sharpe Ratio
	GROSS_SHARPE_RATIO_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateGrossSharpeRatio", GROSS_RETURN_ITD, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.GROSS_STD_ITD, "inceptionToDateRiskFreeSecurityReturn"),
	GROSS_SHARPE_RATIO_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearGrossSharpeRatio", GROSS_RETURN_ONE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.GROSS_STD_ONE_YEAR, "oneYearRiskFreeSecurityReturn"),
	GROSS_SHARPE_RATIO_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearGrossSharpeRatio", GROSS_RETURN_THREE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.GROSS_STD_THREE_YEAR, "threeYearRiskFreeSecurityReturn"),
	GROSS_SHARPE_RATIO_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearGrossSharpeRatio", GROSS_RETURN_FIVE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.GROSS_STD_FIVE_YEAR, "fiveYearRiskFreeSecurityReturn"),
	GROSS_SHARPE_RATIO_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearGrossSharpeRatio", GROSS_RETURN_TEN_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.GROSS_STD_TEN_YEAR, "tenYearRiskFreeSecurityReturn"),

	//Benchmark Sharpe Ratio
	BENCHMARK_SHARPE_RATIO_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateBenchmarkSharpeRatio", BENCHMARK_RETURN_ITD, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.BENCHMARK_STD_ITD, "inceptionToDateRiskFreeSecurityReturn"),
	SECONDARY_BENCHMARK_SHARPE_RATIO_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateSecondaryBenchmarkSharpeRatio", SECONDARY_BENCHMARK_RETURN_ITD, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.SECONDARY_BENCHMARK_STD_ITD, "inceptionToDateRiskFreeSecurityReturn"),
	THIRD_BENCHMARK_SHARPE_RATIO_ITD(PerformanceMetricPeriods.INCEPTION_TO_DATE, "inceptionToDateThirdBenchmarkSharpeRatio", THIRD_BENCHMARK_RETURN_ITD, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.THIRD_BENCHMARK_STD_ITD, "inceptionToDateRiskFreeSecurityReturn"),
	BENCHMARK_SHARPE_RATIO_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearBenchmarkSharpeRatio", BENCHMARK_RETURN_ONE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.BENCHMARK_STD_ONE_YEAR, "oneYearRiskFreeSecurityReturn"),
	SECONDARY_BENCHMARK_SHARPE_RATIO_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearSecondaryBenchmarkSharpeRatio", SECONDARY_BENCHMARK_RETURN_ONE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.SECONDARY_BENCHMARK_STD_ONE_YEAR, "oneYearRiskFreeSecurityReturn"),
	THIRD_BENCHMARK_SHARPE_RATIO_ONE_YEAR(PerformanceMetricPeriods.ONE_YEAR, "oneYearThirdBenchmarkSharpeRatio", THIRD_BENCHMARK_RETURN_ONE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.THIRD_BENCHMARK_STD_ONE_YEAR, "oneYearRiskFreeSecurityReturn"),
	BENCHMARK_SHARPE_RATIO_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearBenchmarkSharpeRatio", BENCHMARK_RETURN_THREE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.BENCHMARK_STD_THREE_YEAR, "threeYearRiskFreeSecurityReturn"),
	SECONDARY_BENCHMARK_SHARPE_RATIO_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearSecondaryBenchmarkSharpeRatio", SECONDARY_BENCHMARK_RETURN_THREE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.SECONDARY_BENCHMARK_STD_THREE_YEAR, "threeYearRiskFreeSecurityReturn"),
	THIRD_BENCHMARK_SHARPE_RATIO_THREE_YEAR(PerformanceMetricPeriods.THREE_YEAR, "threeYearThirdBenchmarkSharpeRatio", THIRD_BENCHMARK_RETURN_THREE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.THIRD_BENCHMARK_STD_THREE_YEAR, "threeYearRiskFreeSecurityReturn"),
	BENCHMARK_SHARPE_RATIO_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearBenchmarkSharpeRatio", BENCHMARK_RETURN_FIVE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.BENCHMARK_STD_FIVE_YEAR, "fiveYearRiskFreeSecurityReturn"),
	BENCHMARK_SHARPE_RATIO_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearBenchmarkSharpeRatio", BENCHMARK_RETURN_TEN_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.BENCHMARK_STD_TEN_YEAR, "tenYearRiskFreeSecurityReturn"),
	SECONDARY_BENCHMARK_SHARPE_RATIO_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearSecondaryBenchmarkSharpeRatio", SECONDARY_BENCHMARK_RETURN_FIVE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.SECONDARY_BENCHMARK_STD_FIVE_YEAR, "fiveYearRiskFreeSecurityReturn"),
	THIRD_BENCHMARK_SHARPE_RATIO_FIVE_YEAR(PerformanceMetricPeriods.FIVE_YEAR, "fiveYearThirdBenchmarkSharpeRatio", THIRD_BENCHMARK_RETURN_FIVE_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.THIRD_BENCHMARK_STD_FIVE_YEAR, "fiveYearRiskFreeSecurityReturn"),
	SECONDARY_BENCHMARK_SHARPE_RATIO_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearSecondaryBenchmarkSharpeRatio", SECONDARY_BENCHMARK_RETURN_TEN_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.SECONDARY_BENCHMARK_STD_TEN_YEAR, "tenYearRiskFreeSecurityReturn"),
	THIRD_BENCHMARK_SHARPE_RATIO_TEN_YEAR(PerformanceMetricPeriods.TEN_YEAR, "tenYearThirdBenchmarkSharpeRatio", THIRD_BENCHMARK_RETURN_TEN_YEAR, PerformanceMetricCalculatorTypes.SHARPE_RATIO, PerformanceMetricFieldTypes.THIRD_BENCHMARK_STD_TEN_YEAR, "tenYearRiskFreeSecurityReturn");

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The name of the property this calculation is for
	 */
	private String beanPropertyName;

	/**
	 * The property we pull from other entities to aggregate for the calculation of this type
	 * i.e. QuarterToDateNetReturn depends on PeriodNetReturn
	 */
	private PerformanceMetricFieldTypes dependentFieldType;

	/**
	 * Used by complex calculations that require properties beyond the base bean property name for period
	 */
	private String beanPropertyNameForComplexCalculation;

	/**
	 * Notes as a benchmark return - these calculated values are not required
	 */
	private boolean benchmarkReturn;

	/**
	 * The calculator to use for this metric field type.
	 */
	private PerformanceMetricCalculatorTypes performanceMetricCalculatorTypes;

	/**
	 * A complex metric field type is a field type that contains another deeper field within itself.
	 * This is useful for calculations such as SHARP_RATIO that requires multiple fields to be computed
	 * within the equation.
	 */
	private PerformanceMetricFieldTypes complexMetricFieldType;


	private PerformanceMetricPeriods performanceMetricPeriod;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PerformanceMetricFieldTypes(PerformanceMetricPeriods performanceMetricPeriod, String beanPropertyName, PerformanceMetricFieldTypes dependentFieldType, PerformanceMetricCalculatorTypes performanceMetricCalculatorTypes) {
		this.performanceMetricPeriod = performanceMetricPeriod;
		this.beanPropertyName = beanPropertyName;
		this.dependentFieldType = dependentFieldType;
		this.performanceMetricCalculatorTypes = performanceMetricCalculatorTypes;
		this.benchmarkReturn = (dependentFieldType != null && dependentFieldType.isBenchmarkReturn());
	}


	PerformanceMetricFieldTypes(PerformanceMetricPeriods performanceMetricPeriod, String beanPropertyName, PerformanceMetricFieldTypes dependentFieldType, PerformanceMetricCalculatorTypes performanceMetricCalculatorTypes,
	                            PerformanceMetricFieldTypes complexMetricFieldType, String beanPropertyNameForComplexCalculation) {
		this.performanceMetricPeriod = performanceMetricPeriod;
		this.beanPropertyNameForComplexCalculation = beanPropertyNameForComplexCalculation;
		this.beanPropertyName = beanPropertyName;
		this.dependentFieldType = dependentFieldType;
		this.performanceMetricCalculatorTypes = performanceMetricCalculatorTypes;
		this.complexMetricFieldType = complexMetricFieldType;
		this.benchmarkReturn = (dependentFieldType != null && dependentFieldType.isBenchmarkReturn());
	}


	PerformanceMetricFieldTypes(PerformanceMetricPeriods performanceMetricPeriod, String beanPropertyName, PerformanceMetricFieldTypes dependentFieldType) {
		this.performanceMetricPeriod = performanceMetricPeriod;
		this.beanPropertyName = beanPropertyName;
		this.dependentFieldType = dependentFieldType;
		this.benchmarkReturn = (dependentFieldType != null && dependentFieldType.isBenchmarkReturn());
		this.performanceMetricCalculatorTypes = PerformanceMetricCalculatorTypes.TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_MONTHLY;
	}


	PerformanceMetricFieldTypes(PerformanceMetricPeriods performanceMetricPeriod, String beanPropertyName, boolean benchmarkReturn) {
		this.performanceMetricPeriod = performanceMetricPeriod;
		this.beanPropertyName = beanPropertyName;
		this.benchmarkReturn = benchmarkReturn;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isTypeValidForInceptionDate(Date inceptionDate, Date fromDate) {
		return this.performanceMetricPeriod.isPeriodValidForInceptionDate(inceptionDate, fromDate);
	}


	/**
	 * Given a from date calculate the period date based on
	 * if this type utilizes period value or period month
	 */
	public Date getPeriodDateFromFromDate(Date fromDate) {
		return this.performanceMetricPeriod.getPeriodDateFromDate(fromDate);
	}


	/**
	 * For this calculation type return the sub-list of entities required from the total list to
	 * perform the calculation for the startIndex entity. There will never be entities included prior to the
	 * inception Adjustment Date. Inception Adjustment Date is used to handle account performance time period breaks.
	 */
	public <T extends PerformanceCompositeMetric> List<T> getPerformanceMetricSubList(List<T> performanceMetricList, int startIndex, Date inceptionAdjustmentDate) {
		//Find the start date of the entity we want to fill
		Date startDate = performanceMetricList.get(startIndex).getAccountingPeriod().getStartDate();
		//Find the date of the entity back to which we would need to subList to accomplish this calculation
		Date periodDate = getPeriodDateFromFromDate(startDate);

		//If true, this calculation wanted to go back further than the inception date allowed.
		//  Therefore set the period date to the inception date so it goes back only to inception
		if (inceptionAdjustmentDate != null && (DateUtils.isDateAfterOrEqual(inceptionAdjustmentDate, periodDate) || periodDate == null)) {
			periodDate = inceptionAdjustmentDate;
		}

		int fromIndex = -1;

		//Find first entity in sorted list where it's in our desired date range
		for (int i = 0; i <= startIndex; i++) {
			Date currentDate = performanceMetricList.get(i).getAccountingPeriod().getStartDate();
			if (periodDate == null || DateUtils.compare(DateUtils.getFirstDayOfMonth(periodDate), currentDate, false) == 0 || DateUtils.isDateAfterOrEqual(currentDate, periodDate)) {
				fromIndex = i;
				break;
			}
		}

		if (fromIndex == -1) {
			throwValidationException(performanceMetricList.get(startIndex), startDate);
		}

		//We need to subList all entities between periodDate to startDate indices
		return performanceMetricList.subList(fromIndex, startIndex + 1);
	}


	private void throwValidationException(PerformanceCompositeMetric failedPerformanceMetric, Date errorDate) {
		if (failedPerformanceMetric != null) {
			throw new ValidationException("Performance composite metric [" + failedPerformanceMetric + "] and accounting period [" + failedPerformanceMetric.getAccountingPeriod().getLabel()
					+ "] does not have sufficient history for [" + getBeanPropertyName() + "] calculation on date [" + DateUtils.fromDateSmart(errorDate) + "].");
		}

		throw new ValidationException("Missing performance metric entry history for [" + getBeanPropertyName() + "] calculation on date [" + DateUtils.fromDateSmart(errorDate) + "].");
	}


	/**
	 * Passive calculations are calculated on the side and should not be dynamically evaluated
	 * All MTD calculations are considered to be passive
	 */
	public boolean isPassiveCalculation() {
		return PerformanceMetricPeriods.MONTH_TO_DATE == this.getPerformanceMetricPeriod();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public PerformanceMetricFieldTypes getDependentFieldType() {
		return ObjectUtils.coalesce(this.dependentFieldType, this);
	}


	public PerformanceMetricCalculatorTypes getPerformanceMetricCalculatorTypes() {
		return this.performanceMetricCalculatorTypes;
	}


	public PerformanceMetricFieldTypes getComplexMetricFieldType() {
		return this.complexMetricFieldType;
	}


	public String getBeanPropertyNameForComplexCalculation() {
		return this.beanPropertyNameForComplexCalculation;
	}


	public PerformanceMetricPeriods getPerformanceMetricPeriod() {
		return this.performanceMetricPeriod;
	}


	public boolean isBenchmarkReturn() {
		return this.benchmarkReturn;
	}
}
