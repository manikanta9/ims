package com.clifton.performance.composite.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.performance.report.PerformanceReportFrequencies;


public class PerformanceCompositeInvestmentAccountSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "performanceComposite.name,investmentAccount.number,investmentAccount.name", searchFieldCustomType = SearchFieldCustomTypes.OR)
	private String searchPattern;

	@SearchField(searchField = "performanceComposite.id", sortField = "performanceComposite.name")
	private Short performanceCompositeId;

	@SearchField(searchField = "performanceComposite.id", sortField = "performanceComposite.name", comparisonConditions = ComparisonConditions.IN)
	private Short[] performanceCompositeIds;

	@SearchField(searchField = "name", searchFieldPath = "performanceComposite", comparisonConditions = ComparisonConditions.EQUALS)
	private String compositeNameEquals;

	@SearchField(searchField = "investmentAccount.id", sortField = "investmentAccount.number")
	private Integer investmentAccountId;

	@SearchField(searchField = "investmentAccount.id", sortField = "investmentAccount.number", comparisonConditions = ComparisonConditions.IN)
	private Integer[] investmentAccountIds;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	// If set, sets activeOnDateRangeStartDate = first day of the accounting period
	// and activeOnDateRangeEndDate = last day of the accounting period
	private Integer accountingPeriodId;

	@SearchField(searchField = "benchmarkSecurity.id,performanceComposite.benchmarkSecurity.id", sortField = "benchmarkSecurity.symbol,performanceComposite.benchmarkSecurity.symbol", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer benchmarkSecurityId;

	@SearchField(searchField = "secondaryBenchmarkSecurity.id,performanceComposite.secondaryBenchmarkSecurity.id", sortField = "secondaryBenchmarkSecurity.symbol,performanceComposite.secondaryBenchmarkSecurity.symbol", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer secondaryBenchmarkSecurityId;

	@SearchField(searchField = "thirdBenchmarkSecurity.id,performanceComposite.thirdBenchmarkSecurity.id", sortField = "thirdBenchmarkSecurity.symbol,performanceComposite.thirdBenchmarkSecurity.symbol", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer thirdBenchmarkSecurityId;

	// Custom Search Filter: sets all children as performance composite ids
	private Short rollupPerformanceCompositeId;

	@SearchField(searchField = "accountPerformanceCalculatorBean.name,performanceComposite.accountPerformanceCalculatorBean.name", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private String coalesceAccountPerformanceCalculatorBeanName;

	@SearchField(searchField = "accountPerformanceCalculatorBean.id,performanceComposite.accountPerformanceCalculatorBean.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private Integer coalesceAccountPerformanceCalculatorBeanId;

	@SearchField(searchField = "accountReport.id,performanceComposite.accountReport.id", sortField = "accountReport.name,performanceComposite.accountReport.name", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer coalesceAccountReportId;

	@SearchField(searchField = "accountReportFrequency,performanceComposite.accountReportFrequency", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private PerformanceReportFrequencies coalesceAccountReportFrequency;

	@SearchField(searchField = "accountReportAppendAttachments,performanceComposite.accountReportAppendAttachments", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Boolean coalesceAccountReportAppendAttachments;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getPerformanceCompositeId() {
		return this.performanceCompositeId;
	}


	public void setPerformanceCompositeId(Short performanceCompositeId) {
		this.performanceCompositeId = performanceCompositeId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public Short[] getPerformanceCompositeIds() {
		return this.performanceCompositeIds;
	}


	public void setPerformanceCompositeIds(Short[] performanceCompositeIds) {
		this.performanceCompositeIds = performanceCompositeIds;
	}


	public Short getRollupPerformanceCompositeId() {
		return this.rollupPerformanceCompositeId;
	}


	public void setRollupPerformanceCompositeId(Short rollupPerformanceCompositeId) {
		this.rollupPerformanceCompositeId = rollupPerformanceCompositeId;
	}


	public Integer[] getInvestmentAccountIds() {
		return this.investmentAccountIds;
	}


	public void setInvestmentAccountIds(Integer[] investmentAccountIds) {
		this.investmentAccountIds = investmentAccountIds;
	}


	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	public Integer getSecondaryBenchmarkSecurityId() {
		return this.secondaryBenchmarkSecurityId;
	}


	public void setSecondaryBenchmarkSecurityId(Integer secondaryBenchmarkSecurityId) {
		this.secondaryBenchmarkSecurityId = secondaryBenchmarkSecurityId;
	}


	public Integer getThirdBenchmarkSecurityId() {
		return this.thirdBenchmarkSecurityId;
	}


	public void setThirdBenchmarkSecurityId(Integer thirdBenchmarkSecurityId) {
		this.thirdBenchmarkSecurityId = thirdBenchmarkSecurityId;
	}


	public Integer getCoalesceAccountReportId() {
		return this.coalesceAccountReportId;
	}


	public void setCoalesceAccountReportId(Integer coalesceAccountReportId) {
		this.coalesceAccountReportId = coalesceAccountReportId;
	}


	public PerformanceReportFrequencies getCoalesceAccountReportFrequency() {
		return this.coalesceAccountReportFrequency;
	}


	public void setCoalesceAccountReportFrequency(PerformanceReportFrequencies coalesceAccountReportFrequency) {
		this.coalesceAccountReportFrequency = coalesceAccountReportFrequency;
	}


	public Boolean getCoalesceAccountReportAppendAttachments() {
		return this.coalesceAccountReportAppendAttachments;
	}


	public void setCoalesceAccountReportAppendAttachments(Boolean coalesceAccountReportAppendAttachments) {
		this.coalesceAccountReportAppendAttachments = coalesceAccountReportAppendAttachments;
	}


	public String getCoalesceAccountPerformanceCalculatorBeanName() {
		return this.coalesceAccountPerformanceCalculatorBeanName;
	}


	public void setCoalesceAccountPerformanceCalculatorBeanName(String coalesceAccountPerformanceCalculatorBeanName) {
		this.coalesceAccountPerformanceCalculatorBeanName = coalesceAccountPerformanceCalculatorBeanName;
	}


	public Integer getAccountingPeriodId() {
		return this.accountingPeriodId;
	}


	public void setAccountingPeriodId(Integer accountingPeriodId) {
		this.accountingPeriodId = accountingPeriodId;
	}


	public String getCompositeNameEquals() {
		return this.compositeNameEquals;
	}


	public void setCompositeNameEquals(String compositeNameEquals) {
		this.compositeNameEquals = compositeNameEquals;
	}


	public Integer getCoalesceAccountPerformanceCalculatorBeanId() {
		return this.coalesceAccountPerformanceCalculatorBeanId;
	}


	public void setCoalesceAccountPerformanceCalculatorBeanId(Integer coalesceAccountPerformanceCalculatorBeanId) {
		this.coalesceAccountPerformanceCalculatorBeanId = coalesceAccountPerformanceCalculatorBeanId;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}
}
