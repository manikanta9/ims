package com.clifton.performance.composite.performance;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.document.DocumentFileCountAware;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.performance.composite.performance.metric.BasePerformanceCompositeMetric;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.report.definition.Report;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceCompositeInvestmentAccountPerformance</code> class represents returns for a given
 * investment account for a specific accounting period.
 *
 * @author vgomelsky
 */

public class PerformanceCompositeInvestmentAccountPerformance extends BasePerformanceCompositeMetric implements PerformanceAccountInfo, DocumentFileCountAware {

	public static final String PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_PERFORMANCE_TABLE_NAME = "PerformanceCompositeInvestmentAccountPerformance";

	private PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount;

	/**
	 * Management fee defined as Projected Revenue Total on accounting period closing is used
	 * for calculating actual net return for the period. This management fee is treated as gain loss
	 * on the last day of the month for net return calculation only. This override can be used to true up the fee across
	 * quarters if necessary.
	 * If the calculator supports it, the system will automatically populate the override for the first month
	 * month of the quarter as the true up of projected vs. actual for the PREVIOUS quarter.
	 * i.e. April performance = April projected + (Jan/Feb/March Actual - Jan/Feb/March Projected)
	 */
	private BigDecimal managementFeeOverride;

	/**
	 * Used for handling stub periods. Normally positions are already on at the start of the accounting period
	 * If this is set it implies positions and performance started on this date instead of accounting period start date.
	 */
	private Date positionsOnDate;


	private BigDecimal periodTotalNetCashflow;
	/**
	 * Used in calculations. As opposed to total Cash Flows, Weighted Cash Flow is calculated based on
	 * the number of days in a period that this cash flow was present in the account.
	 */
	private BigDecimal periodWeightedNetCashflow;


	private BigDecimal periodGainLoss;

	/**
	 * Total number of attachments
	 */
	private Short documentFileCount;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PerformanceCompositeInvestmentAccountPerformance ofAccountAssignmentAndPeriod(PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount, AccountingPeriod accountingPeriod) {
		PerformanceCompositeInvestmentAccountPerformance bean = new PerformanceCompositeInvestmentAccountPerformance();
		bean.setPerformanceCompositeInvestmentAccount(performanceCompositeInvestmentAccount);
		bean.setAccountingPeriod(accountingPeriod);
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getMeasureDate() {
		if (getAccountingPeriod() != null) {
			return getAccountingPeriod().getEndDate();
		}
		return null;
	}


	@Override
	public Date getStartDate() {
		if (getPositionsOnDate() != null) {
			return getPositionsOnDate();
		}
		return getClientAccount() != null ? getClientAccount().getStartDate() : null;
	}


	public Date getCoalescePositionsOnDate() {
		if (getPositionsOnDate() != null) {
			return getPositionsOnDate();
		}
		if (getClientAccount() != null) {
			if (getClientAccount().getPerformanceInceptionDate() != null) {
				return getClientAccount().getPerformanceInceptionDate();
			}
			return getClientAccount().getInceptionDate();
		}
		return null;
	}


	/**
	 * Get label of Client Account label prefixed to Accounting Period labeled
	 */
	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder();
		if (getClientAccount() != null) {
			sb.append(getClientAccount().getLabel());
			sb.append(": ");
		}
		if (getAccountingPeriod() != null) {
			sb.append(DateUtils.fromDate(getAccountingPeriod().getEndDate(), DateUtils.DATE_FORMAT_MONTH_YEAR));
		}
		return sb.toString();
	}


	public boolean isUpdatable() {
		return getWorkflowStatus() == null || WorkflowStatus.STATUS_DRAFT.equals(getWorkflowStatus().getName());
	}


	@Override
	public InvestmentAccount getClientAccount() {
		//Needed for PerformanceAccountInfo interface
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getInvestmentAccount();
		}
		return null;
	}


	@Override
	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	@Override
	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	@Override
	public PerformanceReportFrequencies getReportingFrequency() {
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getCoalesceAccountReportFrequency();
		}
		return null;
	}


	@Override
	public Report getReport() {
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getCoalesceAccountReport();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getBenchmarkSecurity() {
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getCoalesceBenchmarkSecurity();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getSecondaryBenchmarkSecurity() {
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getCoalesceSecondaryBenchmarkSecurity();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getThirdBenchmarkSecurity() {
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getCoalesceThirdBenchmarkSecurity();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getRiskFreeSecurity() {
		if (getPerformanceCompositeInvestmentAccount() != null) {
			return getPerformanceCompositeInvestmentAccount().getPerformanceComposite().getRiskFreeSecurity();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccount getPerformanceCompositeInvestmentAccount() {
		return this.performanceCompositeInvestmentAccount;
	}


	public void setPerformanceCompositeInvestmentAccount(PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount) {
		this.performanceCompositeInvestmentAccount = performanceCompositeInvestmentAccount;
	}


	public BigDecimal getPeriodTotalNetCashflow() {
		return this.periodTotalNetCashflow;
	}


	public void setPeriodTotalNetCashflow(BigDecimal periodTotalNetCashflow) {
		this.periodTotalNetCashflow = periodTotalNetCashflow;
	}


	public BigDecimal getPeriodWeightedNetCashflow() {
		return this.periodWeightedNetCashflow;
	}


	public void setPeriodWeightedNetCashflow(BigDecimal periodWeightedNetCashflow) {
		this.periodWeightedNetCashflow = periodWeightedNetCashflow;
	}


	public BigDecimal getPeriodGainLoss() {
		return this.periodGainLoss;
	}


	public void setPeriodGainLoss(BigDecimal periodGainLoss) {
		this.periodGainLoss = periodGainLoss;
	}


	public BigDecimal getManagementFeeOverride() {
		return this.managementFeeOverride;
	}


	public void setManagementFeeOverride(BigDecimal managementFeeOverride) {
		this.managementFeeOverride = managementFeeOverride;
	}


	public Date getPositionsOnDate() {
		return this.positionsOnDate;
	}


	public void setPositionsOnDate(Date positionsOnDate) {
		this.positionsOnDate = positionsOnDate;
	}
}
