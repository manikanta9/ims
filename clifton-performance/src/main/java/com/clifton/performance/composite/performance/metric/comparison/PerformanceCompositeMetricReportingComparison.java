package com.clifton.performance.composite.performance.metric.comparison;

import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.composite.performance.metric.BasePerformanceCompositeMetric;
import com.clifton.performance.report.PerformanceReportFrequencies;


/**
 * The <code>PerformanceCompositeMetricReportingComparison</code> can be used for both {@link com.clifton.performance.composite.performance.PerformanceCompositePerformance}
 * and {@link com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance} and is used based on composite and composite assignment setup
 * to determine if the performance needs reporting to be executed/posted.
 * <p>
 * Some composites/accounts don't use reporting, others may be monthly or quarterly
 *
 * @author manderson
 */
public class PerformanceCompositeMetricReportingComparison implements Comparison<BasePerformanceCompositeMetric> {


	@Override
	public boolean evaluate(BasePerformanceCompositeMetric metric, ComparisonContext context) {
		PerformanceReportFrequencies frequency = metric.getReportingFrequency();

		if (frequency == null || frequency == PerformanceReportFrequencies.NONE) {
			recordContextMessage(context, false, "Reporting Frequency is undefined or set to NONE.");
			return false;
		}
		if (frequency == PerformanceReportFrequencies.QUARTERLY) {
			if (DateUtils.isEqualWithoutTime(DateUtils.getLastDayOfQuarter(metric.getAccountingPeriod().getEndDate()), metric.getAccountingPeriod().getEndDate())) {
				recordContextMessage(context, true, "Reporting Frequency is Quarterly and current period is quarter end.");
				return true;
			}
			recordContextMessage(context, false, "Reporting Frequency is Quarterly and current period is not quarter end.");
			return false;
		}
		// Else MONTHLY
		recordContextMessage(context, true, "Reporting Frequency is Monthly.");
		return true;
	}


	private void recordContextMessage(ComparisonContext context, boolean trueMessage, String message) {
		if (context != null) {
			if (trueMessage) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}
	}
}
