package com.clifton.performance.composite.performance;


import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.composite.performance.metric.BasePerformanceCompositeMetric;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.report.definition.Report;

import java.util.Date;


/**
 * The <code>PerformanceCompositePerformance</code> class represents performance data
 * for a given composite for a specific accounting periods.
 *
 * @author vgomelsky
 */
public class PerformanceCompositePerformance extends BasePerformanceCompositeMetric {

	public static final String PERFORMANCE_COMPOSITE_PERFORMANCE_TABLE_NAME = "PerformanceCompositePerformance";


	private PerformanceComposite performanceComposite;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getStartDate() {
		return getPerformanceComposite().getStartDate();
	}


	@Override
	public String getLabel() {
		StringBuilder sb = new StringBuilder(16);
		if (getPerformanceComposite() != null) {
			sb.append(getPerformanceComposite().getLabel());
			sb.append(": ");
		}
		if (getAccountingPeriod() != null) {
			sb.append(DateUtils.fromDate(getAccountingPeriod().getEndDate(), DateUtils.DATE_FORMAT_MONTH_YEAR));
		}
		return sb.toString();
	}


	@Override
	public PerformanceReportFrequencies getReportingFrequency() {
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getCompositeReportFrequency();
		}
		return null;
	}


	@Override
	public Report getReport() {
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getCompositeReport();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getBenchmarkSecurity() {
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getBenchmarkSecurity();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getSecondaryBenchmarkSecurity() {
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getSecondaryBenchmarkSecurity();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getThirdBenchmarkSecurity() {
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getThirdBenchmarkSecurity();
		}
		return null;
	}


	@Override
	public InvestmentSecurity getRiskFreeSecurity() {
		if (getPerformanceComposite() != null) {
			return getPerformanceComposite().getRiskFreeSecurity();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////


	public PerformanceComposite getPerformanceComposite() {
		return this.performanceComposite;
	}


	public void setPerformanceComposite(PerformanceComposite performanceComposite) {
		this.performanceComposite = performanceComposite;
	}
}
