package com.clifton.performance.composite.performance.rule;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountInvestmentSecurityPriceMissingRuleEvaluator</code> validates for those accounts
 * that use monthly priced securities, but have some sub-period calculations, that we have prices for those securities on the required dates.
 * <p>
 * Example: Modified Dietz Monthly Calculator would evaluate the positions at month end, where we should have prices loaded, however there could be a large cash flow during the month
 * that would also require prices to be manually loaded on specific dates.
 *
 * @author manderson
 */
public class PerformanceCompositeAccountInvestmentSecurityPriceMissingRuleEvaluator implements RuleEvaluator<PerformanceCompositeInvestmentAccountPerformance, PerformanceCompositeAccountRuleEvaluatorContext> {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentCalculator investmentCalculator;

	private MarketDataRetriever marketDataRetriever;

	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance, RuleConfig ruleConfig, PerformanceCompositeAccountRuleEvaluatorContext context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		Map<InvestmentSecurity, List<Date>> securityPriceMissingDateMap = new HashMap<>();
		if (entityConfig != null && !entityConfig.isExcluded()) {
			context.setPerformanceCompositeInvestmentAccountPerformance(performanceCompositeInvestmentAccountPerformance);
			List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList = context.getPerformanceCompositeInvestmentAccountDailyPerformanceList();
			for (PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance : CollectionUtils.getIterable(dailyPerformanceList)) {
				Map<InvestmentSecurity, List<AccountingPositionDaily>> securityPositionListMap = BeanUtils.getBeansMap(
						//Filter out closed positions (remainingQuantity=0)
						BeanUtils.filter(context.getPositionListForDate(dailyPerformance.getMeasureDate()), accountingPositionDaily -> accountingPositionDaily.getRemainingQuantity().compareTo(BigDecimal.ZERO) != 0),
						accountingPositionDaily -> accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity());
				for (InvestmentSecurity security : securityPositionListMap.keySet()) {
					Date priceDate = getMarketDataRetriever().getPriceDate(security, dailyPerformance.getMeasureDate(), false, null);
					if (priceDate == null) {
						addSecurityMissingPriceToMap(securityPriceMissingDateMap, security, dailyPerformance.getMeasureDate());
					}
					else if (DateUtils.compare(priceDate, dailyPerformance.getMeasureDate(), false) < 0) {
						// Check if it's a holiday - only if not already equal
						Date expectedPriceDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(DateUtils.addDays(dailyPerformance.getMeasureDate(), 1), getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId()), -1);
						if (DateUtils.compare(priceDate, expectedPriceDate, false) < 0) {
							addSecurityMissingPriceToMap(securityPriceMissingDateMap, security, expectedPriceDate);
						}
					}
				}
			}
			for (Map.Entry<InvestmentSecurity, List<Date>> investmentSecurityListEntry : securityPriceMissingDateMap.entrySet()) {
				Map<String, Object> templateValues = new HashMap<>();
				templateValues.put("dateList", investmentSecurityListEntry.getValue());
				violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositeInvestmentAccountPerformance.getId(), (investmentSecurityListEntry.getKey()).getId(), null, templateValues));
			}
		}
		return violationList;
	}


	private void addSecurityMissingPriceToMap(Map<InvestmentSecurity, List<Date>> securityPriceMissingDateMap, InvestmentSecurity security, Date date) {
		if (!securityPriceMissingDateMap.containsKey(security)) {
			securityPriceMissingDateMap.put(security, CollectionUtils.createList(date));
		}
		else {
			List<Date> dateList = securityPriceMissingDateMap.get(security);
			dateList.add(date);
			securityPriceMissingDateMap.put(security, dateList);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
