package com.clifton.performance.composite.setup.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.performance.composite.setup.PerformanceCompositeHistory;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class PerformanceCompositeHistoryByCompositeCache extends SelfRegisteringSingleKeyDaoListCache<PerformanceCompositeHistory, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "performanceComposite.id";
	}


	@Override
	protected Short getBeanKeyValue(PerformanceCompositeHistory bean) {
		if (bean.getPerformanceComposite() != null) {
			return bean.getPerformanceComposite().getId();
		}
		return null;
	}
}
