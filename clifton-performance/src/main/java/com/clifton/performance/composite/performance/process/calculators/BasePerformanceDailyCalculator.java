package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * The <code>BasePerformanceCompositeAccountCalculator</code> is a base abstract class that houses the basic
 * daily account return calculation flow and supporting common methods
 *
 * @author apopp
 */
public abstract class BasePerformanceDailyCalculator extends BasePerformanceCalculator {


	/**
	 * By default we geometrically link the daily returns to get the monthly gross and net returns.
	 * In some cases, we may instead need to sum them
	 */
	private boolean sumDailyReturns;

	/**
	 * When calculating current period start value - will compare to the previous month's actual ending value
	 * For all cases, if there is a difference a violation will be generated.
	 * However, if this option is true, will set the current starting value to the actual previous and apply the difference as gain/loss
	 * for the current month.
	 * The reason for this is often there is accrued interest that is updated after the month end is completed, and the next month ends up
	 * using a different starting value than the previous ending value.  Eventually this violation will be used to determine what data needs to be locked.
	 */
	private boolean usePreviousEndingActual;

	private Short cashFlowAccountingAccountGroupId;
	private Short gainLossAdditionalAccountingAccountGroupId;
	// This should never be false as that position value would be included in the position gain/loss already
	private boolean excludePositionsFromGainLossAdditionalAccountingAccountGroup = true;

	private Short gainLossExcludedAccountingAccountGroupId;

	// Ability to easily include or exclude specific positions from position value calculations
	private Short includePositionInvestmentGroupId;
	private Short excludePositionInvestmentGroupId;

	// Ability to easily include or exclude specific positions from position gain/loss calculations
	private Short includePositionGainLossInvestmentGroupId;
	private Short excludePositionGainLossInvestmentGroupId;

	/**
	 * For the used for the "Cash Balance" instead of just cash + receivables, can be used to include all Non Position Assets and Liabilities
	 * This is most often necessary to be used for the Funds, Accounts that include Currency, etc.
	 */
	private boolean includeAllNonPositionAssetsAndLiabilities;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void calculateMonthlyMetrics(PerformanceCompositeAccountRuleEvaluatorContext config) {
		calculateDailyAccountReturns(config);
		calculateMonthlyGrossReturnAndMarketValue(config);
		calculateActualNetReturn(config);
	}


	/**
	 * Base method that controls all basic processing flow.
	 * <p>
	 * 1) Process each day if is daily calculation
	 * a)   calculate daily gain loss
	 * b)   calculate daily market value
	 * c)   calculate daily cash flows
	 * d)   calculate daily benchmark returns (primary, secondary)
	 * e)   calculate other daily values via concrete calculator implementation
	 */
	protected void calculateDailyAccountReturns(PerformanceCompositeAccountRuleEvaluatorContext config) {
		// Daily is not necessarily DAILY but can have sub-periods
		List<Date> dateList = getDaysToProcessForMonth(config);
		Date previousDate = DateUtils.addDays(config.getAccountingPeriod().getStartDate(), -1);
		BigDecimal previousMonthEndPositionValueAdjustment = null;
		for (Date dateToCalculate : CollectionUtils.getIterable(getDaysToProcessForMonth(config))) {
			calculateDailyPositionValue(config, dateToCalculate);
			if (DateUtils.isEqualWithoutTime(dateToCalculate, DateUtils.getLastDayOfPreviousMonth(config.getAccountingPeriod().getStartDate()))) {
				previousMonthEndPositionValueAdjustment = getPreviousMonthEndPositionValueAdjustment(config, dateToCalculate);
			}
			// Skip Last Day of Previous Month - Only Used to Display the Position Value
			if (DateUtils.isDateAfterOrEqual(dateToCalculate, config.getAccountingPeriod().getStartDate())) {
				calculateDailyGainLoss(config, dateToCalculate, previousDate, previousMonthEndPositionValueAdjustment);
				previousMonthEndPositionValueAdjustment = null; // Once applied - clear it
				calculateDailyNetCashFlowValue(config, dateToCalculate);
				calculateDailyBenchmarkValues(config, dateToCalculate, previousDate);
				calculateDailyMetrics(config, dateToCalculate, previousDate);
			}
			previousDate = dateToCalculate;
		}
		// Clear Out Unused Values
		clearUnusedDailyValues(config, dateList);

		//We can calculate the monthly total cash flow as a sum of the underlying daily calculations
		calculateMonthlyNetCashFlowValue(config);

		//We can calculate the monthly gain/loss as a sum of the underlying daily values
		calculateMonthlyGainLoss(config);
	}


	/**
	 * Get all weekdays in month to process for performance.
	 * Also include the last day of the previous month (Not Necessarily Weekday)
	 */
	protected List<Date> getDaysToProcessForMonth(PerformanceCompositeAccountRuleEvaluatorContext context) {
		AccountingPeriod accountingPeriod = context.getAccountingPeriod();
		List<Date> dayList = DateUtils.getWeekdayListForRange(accountingPeriod.getStartDate(), accountingPeriod.getEndDate());
		dayList.add(DateUtils.getLastDayOfPreviousMonth(accountingPeriod.getStartDate()));
		// If month end lands on a weekend we need to include it to prevent missing gain/loss over the weekend that should be included within that month
		if (!dayList.contains(accountingPeriod.getEndDate())) {
			dayList.add(accountingPeriod.getEndDate());
		}
		Collections.sort(dayList);
		dayList = filterDaysToProcessForCurrentMonth(context, dayList);
		return dayList;
	}


	/**
	 * Only include dates before today for current accounting period - MTD calculations
	 */
	protected List<Date> filterDaysToProcessForCurrentMonth(PerformanceCompositeAccountRuleEvaluatorContext context, List<Date> dayList) {
		AssertUtils.assertNotNull(context, "Evaluator context is required.");
		AssertUtils.assertNotNull(dayList, "The day list is required.");

		Date today = DateUtils.clearTime(new Date());
		if (DateUtils.isDateBetween(today, context.getAccountingPeriod().getStartDate(), context.getAccountingPeriod().getEndDate(), false)) {
			addPartialCalculationForCurrentMonthViolation(context);
			int index;
			Collections.sort(dayList);
			for (index = 0; index < dayList.size(); index++) {
				if (DateUtils.isDateAfterOrEqual(dayList.get(index), today)) {
					break;
				}
			}
			return dayList.subList(0, index);
		}
		return dayList;
	}


	protected void clearUnusedDailyValues(PerformanceCompositeAccountRuleEvaluatorContext context, List<Date> dateList) {
		// Clear Out Unused Values
		context.getPerformanceCompositeInvestmentAccountDailyPerformanceMap().keySet().removeIf(performanceDate -> !dateList.contains(performanceDate) && !DateUtils.isLastDayOfMonth(performanceDate));
	}


	/**
	 * Perform specific daily calculations such as gross return
	 */
	protected void calculateDailyMetrics(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date previousDate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		PerformanceCompositeInvestmentAccountDailyPerformance previousPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(previousDate);
		BigDecimal grossReturn = calculateDailyReturn(dailyPerformance.getCoalesceGainLossGainLossOverride(), previousPerformance.getCoalescePositionValuePositionValueOverride(), dailyPerformance.getCoalesceNetCashFlowNetCashFlowOverride(), null);
		dailyPerformance.setGrossReturn(grossReturn);
	}


	protected abstract BigDecimal calculateDailyReturn(BigDecimal gainLoss, BigDecimal positionValue, BigDecimal netCashFlow, BigDecimal weightedNetCashFlow);


	/**
	 * Simply the sum of the daily gain loss base across all positions for that day
	 */
	protected void calculateDailyGainLoss(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date previousDate, BigDecimal gainLossAdjustmentFromPreviousValue) {
		// Calls default calculation method and assumes dateToCalculate is the same date as the daily performance to add the value to
		// See {@link com.clifton.performance.composite.performance.process.calculators.PerformanceCompositeAccountDailyOptionsUnfundedCalculator} for example where we use different dates
		// i.e. dateToCalculate value includes previous dates, and dateToCalculate is pushed forward to the next sub-period (this is because we are using previous price for valuations)
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		calculateDailyGainLossImpl(config, dailyPerformance, dateToCalculate, previousDate, gainLossAdjustmentFromPreviousValue);
	}


	protected void calculateDailyGainLossImpl(PerformanceCompositeAccountRuleEvaluatorContext config, PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance, Date dateToCalculate, Date previousDate, BigDecimal gainLossAdjustmentFromPreviousValue) {
		BigDecimal gainLoss = BigDecimal.ZERO;
		Date date = DateUtils.getNextWeekday(previousDate);
		// If date to calculate happens to fall on a weekend/month end, then just use that date
		// Otherwise need to loop through WEEKDAYS since Monday snapshots include weekend data
		if (DateUtils.compare(date, dateToCalculate, false) > 0) {
			date = dateToCalculate;
		}
		while (DateUtils.compare(date, dateToCalculate, false) <= 0) {
			gainLoss = MathUtils.add(gainLoss, calculateDailyGainLossForDate(config, date));
			date = DateUtils.addDays(date, 1);
			// If date is a weekend and NOT the last day of the month - move forward to the next day
			while (DateUtils.isWeekend(date) && !DateUtils.isLastDayOfMonth(date)) {
				date = DateUtils.addDays(date, 1);
			}
		}
		// If previous date was a weekend - back out that date's gain/loss so it's not double counted on Monday since we are comparing to weekend position value
		// i.e. Month End Lands on a Weekend - 4/30 gain/loss is also included in 5/2 gain loss on the snapshots
		if (DateUtils.isWeekend(previousDate)) {
			gainLoss = MathUtils.subtract(gainLoss, calculateDailyGainLossForDate(config, previousDate));
		}
		dailyPerformance.setGainLoss(gainLoss);
		if (!MathUtils.isNullOrZero(gainLossAdjustmentFromPreviousValue)) {
			// Apply the adjustment
			if (isUsePreviousEndingActual()) {
				dailyPerformance.setGainLoss(MathUtils.add(dailyPerformance.getGainLoss(), gainLossAdjustmentFromPreviousValue));
			}
			// In all cases - generate the violation
			addPreviousMonthEndPositionValueViolation(config, dailyPerformance, gainLossAdjustmentFromPreviousValue);
		}
	}


	private BigDecimal calculateDailyGainLossForDate(PerformanceCompositeAccountRuleEvaluatorContext config, Date date) {
		BigDecimal additionalGainLoss = config.getAccountingAccountGroupTransactionValue(config.getPerformanceCompositeInvestmentAccountPerformance(), date, getGainLossAdditionalAccountingAccountGroupId(), isExcludePositionsFromGainLossAdditionalAccountingAccountGroup());
		BigDecimal excludedGainLoss = config.getAccountingAccountGroupTransactionValue(config.getPerformanceCompositeInvestmentAccountPerformance(), date, getGainLossExcludedAccountingAccountGroupId());
		BigDecimal positionGainLoss = CoreMathUtils.sumProperty(filterAccountingPositionListForGainLoss(config.getPositionListForDate(date, getIncludePositionGainLossInvestmentGroupId(), getExcludePositionGainLossInvestmentGroupId())), accountingPositionDaily -> getAccountingPositionDailyBaseGainLoss(config, accountingPositionDaily));

		return MathUtils.add(positionGainLoss, MathUtils.subtract(additionalGainLoss, excludedGainLoss));
	}


	/**
	 * Calculate monthly gain/loss as the sum of the underlying daily values
	 */
	private void calculateMonthlyGainLoss(PerformanceCompositeAccountRuleEvaluatorContext config) {
		PerformanceCompositeInvestmentAccountPerformance monthlyPerformance = config.getPerformanceCompositeInvestmentAccountPerformance();
		BigDecimal gainLoss = CoreMathUtils.sumProperty(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList(), PerformanceCompositeInvestmentAccountDailyPerformance::getCoalesceGainLossGainLossOverride);
		monthlyPerformance.setPeriodGainLoss(gainLoss);
	}


	protected List<AccountingPositionDaily> filterAccountingPositionListForGainLoss(List<AccountingPositionDaily> accountingPositionDailyList) {
		// Allows for additional filtering by children - i.e. Security Targets can limit to options of a specific underlying as well as Puts vs. Calls
		return accountingPositionDailyList;
	}


	/**
	 * For most cases we just return the base gain/loss value from AccountingPositionDaily
	 * See Override for Security Target calculations that include Options only gain/loss we need to adjust the gain/loss for assignments or else it looks like a large loss
	 */
	@SuppressWarnings("unused")
	protected BigDecimal getAccountingPositionDailyBaseGainLoss(PerformanceCompositeAccountRuleEvaluatorContext config, AccountingPositionDaily accountingPositionDaily) {
		return accountingPositionDaily.getDailyGainLossBase();
	}


	/**
	 * Beginning of day base market value calculated as the base market value of the end of the previous weekday
	 */
	protected void calculateDailyPositionValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		BigDecimal cashValue = config.getCashValueForDate(dateToCalculate, isIncludeAllNonPositionAssetsAndLiabilities());
		BigDecimal positionValue = CoreMathUtils.sumProperty(config.getPositionListForDate(dateToCalculate, getIncludePositionInvestmentGroupId(), getExcludePositionInvestmentGroupId()), AccountingPositionDaily::getMarketValueBase);
		dailyPerformance.setPositionValue(MathUtils.add(positionValue, cashValue));
	}


	/**
	 * When calculating for the last day of the previous month - will also retrieve actual value from previous performance ending value
	 * If there is a mismatch - will use the actual from the previous month, and return the difference to be applied as gain/loss for the following day
	 * This will also generate a violation for users to acknowledge/ignore.
	 */
	protected BigDecimal getPreviousMonthEndPositionValueAdjustment(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		if (DateUtils.isEqualWithoutTime(dateToCalculate, DateUtils.getLastDayOfPreviousMonth(config.getAccountingPeriod().getStartDate()))) {
			PerformanceCompositeInvestmentAccountPerformance previousPerformance = config.getPreviousPerformanceCompositeInvestmentAccountPerformance();
			if (previousPerformance != null) {
				BigDecimal actualPreviousValue = previousPerformance.getPeriodEndMarketValue();
				PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
				if (!MathUtils.isEqual(actualPreviousValue, dailyPerformance.getPositionValue())) {
					BigDecimal difference = MathUtils.subtract(dailyPerformance.getPositionValue(), actualPreviousValue);
					if (isUsePreviousEndingActual()) {
						dailyPerformance.setPositionValue(actualPreviousValue);
					}
					return difference;
				}
			}
		}
		return null;
	}


	protected void addPreviousMonthEndPositionValueViolation(PerformanceCompositeAccountRuleEvaluatorContext config, PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance, BigDecimal gainLossAdjustmentFromPreviousValue) {
		if (!MathUtils.isNullOrZero(gainLossAdjustmentFromPreviousValue)) {
			String violationNote = "Previous Month End Position Value does not match actual ending value from previous account performance.  Difference [" + CoreMathUtils.formatNumberMoney(gainLossAdjustmentFromPreviousValue) + "]. ";
			if (isUsePreviousEndingActual()) {
				violationNote += "The position value for [" + DateUtils.fromDateShort(DateUtils.getLastDayOfPreviousMonth(dailyPerformance.getMeasureDate())) + "] was updated to match previous actual and the difference was applied to [" + DateUtils.fromDateShort(dailyPerformance.getMeasureDate()) + "] gain/loss.";
			}
			else {
				violationNote += "<b>No automatic adjustments were made.  Please review and add overrides as necessary.</b>";
			}
			config.addSystemDefinedRuleViolationNote(PerformanceCompositeAccountRuleEvaluatorContext.RULE_PREVIOUS_MONTH_END_GAIN_LOSS_ADJUSTMENT, violationNote);
		}
	}


	protected void addPartialCalculationForCurrentMonthViolation(PerformanceCompositeAccountRuleEvaluatorContext config) {
		String violationNote = String.format("Month account performance for the current accounting period %s is not complete until month end.", config.getAccountingPeriod().getLabel());
		config.addSystemDefinedRuleViolationNote(PerformanceCompositeAccountRuleEvaluatorContext.RULE_CURRENT_MONTH_END_PARTIAL_PERFORMANCE_DATA, violationNote);
	}


	/**
	 * Calculate net cash flow as total credit - total debit for cash contributions and distributions
	 */
	protected void calculateDailyNetCashFlowValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		dailyPerformance.setNetCashFlow(config.getAccountingAccountGroupTransactionValue(config.getPerformanceCompositeInvestmentAccountPerformance(), dateToCalculate, getCashFlowAccountingAccountGroupId()));
		dailyPerformance.setWeightedNetCashFlow(calculateDailyWeightedNetCashFlowValue(dateToCalculate, dailyPerformance.getCoalesceNetCashFlowNetCashFlowOverride()));
	}


	/**
	 * Calculate weighted net cash flow as ((# of days in month) - (Day of cash flow +1)) / (# of days in month) * net cash flow
	 * We may have a regular daily net cash flow but we also want to keep track of a weighted adjustment of this flow.
	 * In other words, given a cash flow adjust it so that we can see it's affect on the month instead of treating it as if it
	 * was provided at the beginning of the period.
	 */
	protected BigDecimal calculateDailyWeightedNetCashFlowValue(Date dateToCalculate, BigDecimal netCashFlow) {
		//If there was a cash flow on this day then calculate it's weighted flow
		if (!MathUtils.isNullOrZero(netCashFlow)) {
			//Determine how many days this cash will be in the month (How many days it would affect)
			int cashFlowDaysInMonth = DateUtils.getDaysDifferenceInclusive(DateUtils.getLastDayOfMonth(dateToCalculate), dateToCalculate);
			int daysInMonth = DateUtils.getDaysDifferenceInclusive(DateUtils.getLastDayOfMonth(dateToCalculate), DateUtils.getFirstDayOfMonth(dateToCalculate));
			return CoreMathUtils.getValueProrated(new BigDecimal(cashFlowDaysInMonth), new BigDecimal(daysInMonth), netCashFlow);
		}
		return null;
	}


	/**
	 * Calculate monthly net cash flow as the sum of the underlying daily cash flows
	 */
	private void calculateMonthlyNetCashFlowValue(PerformanceCompositeAccountRuleEvaluatorContext config) {
		PerformanceCompositeInvestmentAccountPerformance monthlyPerformance = config.getPerformanceCompositeInvestmentAccountPerformance();
		BigDecimal cashFlowSum = CoreMathUtils.sumProperty(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList(), PerformanceCompositeInvestmentAccountDailyPerformance::getTotalNetCashFlow);
		BigDecimal weightedCashFlowSum = CoreMathUtils.sumProperty(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList(), PerformanceCompositeInvestmentAccountDailyPerformance::getCoalesceWeightedNetCashFlowWeightedNetCashFlowOverride);
		monthlyPerformance.setPeriodTotalNetCashflow(cashFlowSum);
		monthlyPerformance.setPeriodWeightedNetCashflow(weightedCashFlowSum);
	}


	/**
	 * Calculate the monthly metrics for period end market value and period gross return
	 */
	private void calculateMonthlyGrossReturnAndMarketValue(PerformanceCompositeAccountRuleEvaluatorContext config) {
		//perform monthly calculations
		BigDecimal[] dailyGrossReturns = BeanUtils.getPropertyValuesExcludeNull(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList(), PerformanceCompositeInvestmentAccountDailyPerformance::getGrossReturn, BigDecimal.class);
		BigDecimal periodGrossReturn = calculateMonthlyReturnFromDailyReturns(dailyGrossReturns);
		config.getPerformanceCompositeInvestmentAccountPerformance().setPeriodGrossReturn(periodGrossReturn);

		//monthly market value is last day of month position value
		PerformanceCompositeInvestmentAccountDailyPerformance lastDayPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(config.getAccountingPeriod().getEndDate());
		BigDecimal marketValue = lastDayPerformance.getCoalescePositionValuePositionValueOverride();
		config.getPerformanceCompositeInvestmentAccountPerformance().setPeriodEndMarketValue(marketValue);
	}


	/**
	 * We compute actual net return from the underlying stream of gross returns. The only difference is we need to recompute
	 * the last days gross return with an included gain/loss that is the management fee.
	 */
	protected void calculateActualNetReturn(PerformanceCompositeAccountRuleEvaluatorContext config) {
		PerformanceCompositeInvestmentAccountDailyPerformance lastDailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceList().get(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList().size() - 1);

		//Recompute the last day gross return with gain/loss decreased by the management fee
		BigDecimal managementFee = calculateManagementFee(config);
		BigDecimal lastDayNetReturn = calculateDailyReturn(MathUtils.subtract(lastDailyPerformance.getCoalesceGainLossGainLossOverride(), managementFee), config.getPreviousPositionValueFromDateFlexible(lastDailyPerformance.getMeasureDate()), lastDailyPerformance.getCoalesceNetCashFlowNetCashFlowOverride(), lastDailyPerformance.getCoalesceWeightedNetCashFlowWeightedNetCashFlowOverride());
		calculateActualNetReturn(config, lastDayNetReturn);
	}


	/**
	 * We compute actual net return from the underlying stream of gross returns. The only difference is we need to recompute
	 * the last days gross return with an included gain/loss that is the management fee.
	 */
	protected void calculateActualNetReturn(PerformanceCompositeAccountRuleEvaluatorContext config, BigDecimal lastDayNetReturnWithManagementFee) {
		//Get the current gross returns
		BigDecimal[] returns = BeanUtils.getPropertyValuesExcludeNull(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList(), PerformanceCompositeInvestmentAccountDailyPerformance::getGrossReturn, BigDecimal.class);

		if (returns.length == 0) {
			return;
		}
		//Set last day to this new return
		returns[returns.length - 1] = lastDayNetReturnWithManagementFee;

		//Compute the total percent change for the actual net return
		BigDecimal netReturn = calculateMonthlyReturnFromDailyReturns(returns);
		config.getPerformanceCompositeInvestmentAccountPerformance().setPeriodNetReturn(netReturn);
	}


	/**
	 * Calculates the overall return for the list of returns by either summing them or geometrically linking them
	 * based on the option selected.
	 */
	private BigDecimal calculateMonthlyReturnFromDailyReturns(BigDecimal[] returns) {
		if (isSumDailyReturns()) {
			return CoreMathUtils.sum(CollectionUtils.createList(returns));
		}
		return MathUtils.getTotalPercentChange(returns, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isIncludeAllNonPositionAssetsAndLiabilities() {
		return this.includeAllNonPositionAssetsAndLiabilities;
	}


	public void setIncludeAllNonPositionAssetsAndLiabilities(boolean includeAllNonPositionAssetsAndLiabilities) {
		this.includeAllNonPositionAssetsAndLiabilities = includeAllNonPositionAssetsAndLiabilities;
	}


	public Short getCashFlowAccountingAccountGroupId() {
		return this.cashFlowAccountingAccountGroupId;
	}


	public void setCashFlowAccountingAccountGroupId(Short cashFlowAccountingAccountGroupId) {
		this.cashFlowAccountingAccountGroupId = cashFlowAccountingAccountGroupId;
	}


	public Short getGainLossAdditionalAccountingAccountGroupId() {
		return this.gainLossAdditionalAccountingAccountGroupId;
	}


	public void setGainLossAdditionalAccountingAccountGroupId(Short gainLossAdditionalAccountingAccountGroupId) {
		this.gainLossAdditionalAccountingAccountGroupId = gainLossAdditionalAccountingAccountGroupId;
	}


	public boolean isExcludePositionsFromGainLossAdditionalAccountingAccountGroup() {
		return this.excludePositionsFromGainLossAdditionalAccountingAccountGroup;
	}


	public void setExcludePositionsFromGainLossAdditionalAccountingAccountGroup(boolean excludePositionsFromGainLossAdditionalAccountingAccountGroup) {
		this.excludePositionsFromGainLossAdditionalAccountingAccountGroup = excludePositionsFromGainLossAdditionalAccountingAccountGroup;
	}


	public Short getGainLossExcludedAccountingAccountGroupId() {
		return this.gainLossExcludedAccountingAccountGroupId;
	}


	public void setGainLossExcludedAccountingAccountGroupId(Short gainLossExcludedAccountingAccountGroupId) {
		this.gainLossExcludedAccountingAccountGroupId = gainLossExcludedAccountingAccountGroupId;
	}


	public Short getIncludePositionInvestmentGroupId() {
		return this.includePositionInvestmentGroupId;
	}


	public void setIncludePositionInvestmentGroupId(Short includePositionInvestmentGroupId) {
		this.includePositionInvestmentGroupId = includePositionInvestmentGroupId;
	}


	public Short getExcludePositionInvestmentGroupId() {
		return this.excludePositionInvestmentGroupId;
	}


	public void setExcludePositionInvestmentGroupId(Short excludePositionInvestmentGroupId) {
		this.excludePositionInvestmentGroupId = excludePositionInvestmentGroupId;
	}


	public Short getIncludePositionGainLossInvestmentGroupId() {
		return this.includePositionGainLossInvestmentGroupId;
	}


	public void setIncludePositionGainLossInvestmentGroupId(Short includePositionGainLossInvestmentGroupId) {
		this.includePositionGainLossInvestmentGroupId = includePositionGainLossInvestmentGroupId;
	}


	public Short getExcludePositionGainLossInvestmentGroupId() {
		return this.excludePositionGainLossInvestmentGroupId;
	}


	public void setExcludePositionGainLossInvestmentGroupId(Short excludePositionGainLossInvestmentGroupId) {
		this.excludePositionGainLossInvestmentGroupId = excludePositionGainLossInvestmentGroupId;
	}


	public boolean isUsePreviousEndingActual() {
		return this.usePreviousEndingActual;
	}


	public void setUsePreviousEndingActual(boolean usePreviousEndingActual) {
		this.usePreviousEndingActual = usePreviousEndingActual;
	}


	public boolean isSumDailyReturns() {
		return this.sumDailyReturns;
	}


	public void setSumDailyReturns(boolean sumDailyReturns) {
		this.sumDailyReturns = sumDailyReturns;
	}
}
