package com.clifton.performance.composite.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.performance.report.PerformanceReportFrequencies;

import java.math.BigDecimal;


public class PerformanceCompositeSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "parentList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short parentCompositeId;

	@SearchField(searchField = "childList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short childCompositeId;

	@SearchField(searchField = "childList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short[] childCompositeIds;

	@SearchField(searchField = "name,description,code,compositeShortName")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private String code;

	@SearchField
	private String compositeShortName;

	@SearchField
	private Boolean rollupComposite;


	@SearchField(searchField = "currency.id")
	private Integer currencyId;

	@SearchField(searchField = "benchmarkSecurity.id")
	private Integer benchmarkSecurityId;

	@SearchField(searchField = "secondaryBenchmarkSecurity.id")
	private Integer secondaryBenchmarkSecurityId;

	@SearchField(searchField = "thirdBenchmarkSecurity.id")
	private Integer thirdBenchmarkSecurityId;

	@SearchField
	private Boolean actualNetReturnUsed;

	@SearchField
	private BigDecimal modelFee;

	@SearchField(searchField = "compositeReport.id", sortField = "compositeReport.name")
	private Integer compositeReportId;

	@SearchField
	private PerformanceReportFrequencies compositeReportFrequency;

	@SearchField(searchFieldPath = "accountPerformanceCalculatorBean", searchField = "name")
	private String accountPerformanceCalculatorBeanName;

	@SearchField(searchField = "accountReport.id", sortField = "accountReport.name")
	private Integer accountReportId;

	@SearchField
	private PerformanceReportFrequencies accountReportFrequency;

	@SearchField
	private Boolean accountReportAppendAttachments;

	@SearchField
	private Boolean includePartialPeriodAccounts;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	// Custom Search Filter - An active PerformanceComposite is one where the current date is between the start and end dates.
	private String compositeStatus;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getCompositeShortName() {
		return this.compositeShortName;
	}


	public void setCompositeShortName(String compositeShortName) {
		this.compositeShortName = compositeShortName;
	}


	public Integer getCurrencyId() {
		return this.currencyId;
	}


	public void setCurrencyId(Integer currencyId) {
		this.currencyId = currencyId;
	}


	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	public Integer getSecondaryBenchmarkSecurityId() {
		return this.secondaryBenchmarkSecurityId;
	}


	public void setSecondaryBenchmarkSecurityId(Integer secondaryBenchmarkSecurityId) {
		this.secondaryBenchmarkSecurityId = secondaryBenchmarkSecurityId;
	}


	public Integer getThirdBenchmarkSecurityId() {
		return this.thirdBenchmarkSecurityId;
	}


	public void setThirdBenchmarkSecurityId(Integer thirdBenchmarkSecurityId) {
		this.thirdBenchmarkSecurityId = thirdBenchmarkSecurityId;
	}


	public Boolean getActualNetReturnUsed() {
		return this.actualNetReturnUsed;
	}


	public void setActualNetReturnUsed(Boolean actualNetReturnUsed) {
		this.actualNetReturnUsed = actualNetReturnUsed;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short getParentCompositeId() {
		return this.parentCompositeId;
	}


	public void setParentCompositeId(Short parentCompositeId) {
		this.parentCompositeId = parentCompositeId;
	}


	public Short getChildCompositeId() {
		return this.childCompositeId;
	}


	public void setChildCompositeId(Short childCompositeId) {
		this.childCompositeId = childCompositeId;
	}


	public BigDecimal getModelFee() {
		return this.modelFee;
	}


	public void setModelFee(BigDecimal modelFee) {
		this.modelFee = modelFee;
	}


	public Short[] getChildCompositeIds() {
		return this.childCompositeIds;
	}


	public void setChildCompositeIds(Short[] childCompositeIds) {
		this.childCompositeIds = childCompositeIds;
	}


	public Integer getCompositeReportId() {
		return this.compositeReportId;
	}


	public void setCompositeReportId(Integer compositeReportId) {
		this.compositeReportId = compositeReportId;
	}


	public PerformanceReportFrequencies getCompositeReportFrequency() {
		return this.compositeReportFrequency;
	}


	public void setCompositeReportFrequency(PerformanceReportFrequencies compositeReportFrequency) {
		this.compositeReportFrequency = compositeReportFrequency;
	}


	public Integer getAccountReportId() {
		return this.accountReportId;
	}


	public void setAccountReportId(Integer accountReportId) {
		this.accountReportId = accountReportId;
	}


	public PerformanceReportFrequencies getAccountReportFrequency() {
		return this.accountReportFrequency;
	}


	public void setAccountReportFrequency(PerformanceReportFrequencies accountReportFrequency) {
		this.accountReportFrequency = accountReportFrequency;
	}


	public Boolean getAccountReportAppendAttachments() {
		return this.accountReportAppendAttachments;
	}


	public void setAccountReportAppendAttachments(Boolean accountReportAppendAttachments) {
		this.accountReportAppendAttachments = accountReportAppendAttachments;
	}


	public String getAccountPerformanceCalculatorBeanName() {
		return this.accountPerformanceCalculatorBeanName;
	}


	public void setAccountPerformanceCalculatorBeanName(String accountPerformanceCalculatorBeanName) {
		this.accountPerformanceCalculatorBeanName = accountPerformanceCalculatorBeanName;
	}


	public Boolean getRollupComposite() {
		return this.rollupComposite;
	}


	public void setRollupComposite(Boolean rollupComposite) {
		this.rollupComposite = rollupComposite;
	}


	public Boolean getIncludePartialPeriodAccounts() {
		return this.includePartialPeriodAccounts;
	}


	public void setIncludePartialPeriodAccounts(Boolean includePartialPeriodAccounts) {
		this.includePartialPeriodAccounts = includePartialPeriodAccounts;
	}


	public String getCompositeStatus() {
		return this.compositeStatus;
	}


	public void setCompositeStatus(String compositeStatus) {
		this.compositeStatus = compositeStatus;
	}
}
