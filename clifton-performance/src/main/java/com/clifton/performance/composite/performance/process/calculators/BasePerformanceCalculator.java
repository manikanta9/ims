package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.accounting.period.AccountingPeriodClosing;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.process.calculators.fee.PerformanceCompositeAccountManagementFeeOverrideCalculator;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>BasePerformanceCompositeAccountCalculator</code> is a base abstract class that houses the basic
 * account return calculation flow and supporting common methods
 *
 * @author manderson
 */
public abstract class BasePerformanceCalculator implements PerformanceCalculator {

	private MarketDataRetriever marketDataRetriever;

	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private SystemBean managementFeeOverrideCalculatorBean;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Base method that controls all basic processing flow.
	 * <p>
	 * 1) Process super logic which is processing daily values if is a daily calculation
	 * 2) Process monthly net cash flow total for calculations with daily values
	 * 3) Process monthly period benchmark returns (primary, secondary, and risk free returns)
	 * 4) Process monthly metrics (gross, total net cash flows, market value, etc)
	 * Note: Period Model Net Return and Benchmark Returns are calculated by the {@link com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricCalculatorImpl}
	 * so that account calculators that use external data for uploads can still allow the system to calculate their values.  Those calculations are the same across all accounts.
	 */
	@Override
	public final void calculate(PerformanceCompositeAccountRuleEvaluatorContext config, PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		// 1. Set up the Context
		config.setPerformanceCompositeInvestmentAccountPerformance(accountPerformance);

		// 2. Calculate account monthly metrics, includes daily/sub-period values (if needed), period gross return and period end market value, and actual net return
		calculateMonthlyMetrics(config);
	}


	@Override
	public final void calculateBenchmarkReturns(PerformanceCompositeAccountRuleEvaluatorContext config, PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		// 1. Set up the Context
		config.setPerformanceCompositeInvestmentAccountPerformance(accountPerformance);

		// 2. Recalc Benchmark Returns
		List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList = BeanUtils.sortWithFunction(config.getPerformanceCompositeInvestmentAccountDailyPerformanceList(), PerformanceCompositeInvestmentAccountDailyPerformance::getMeasureDate, true);
		Date previousDate = null;
		for (PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance : CollectionUtils.getIterable(dailyPerformanceList)) {
			// First one has no benchmark return - place holder for previous month ending value
			if (previousDate == null) {
				previousDate = dailyPerformance.getMeasureDate();
			}
			else {
				calculateDailyBenchmarkValues(config, dailyPerformance.getMeasureDate(), previousDate);
			}
		}
	}


	/**
	 * Calculate primary and secondary benchmark daily return
	 */
	protected void calculateDailyBenchmarkValues(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date previousDate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);

		// Check if dateToCalculate is within the bounds of the assignment start/end date, and is after performance inception date and after positions on date.
		if (DateUtils.isDateBetween(dateToCalculate, config.getPerformanceCompositeInvestmentAccountPerformance().getPerformanceCompositeInvestmentAccount().getStartDate(), config.getPerformanceCompositeInvestmentAccountPerformance().getPerformanceCompositeInvestmentAccount().getEndDate(), false) && DateUtils.isDateBeforeOrEqual(config.getPerformanceCompositeInvestmentAccountPerformance().getClientAccount().getPerformanceInceptionDate(), dateToCalculate, false) && DateUtils.isDateAfterOrEqual(dateToCalculate, config.getPerformanceCompositeInvestmentAccountPerformance().getCoalescePositionsOnDate())) {
			dailyPerformance.setBenchmarkReturn(calculateAccountDailyPerformanceBenchmarkReturn(dateToCalculate, config.getPerformanceCompositeInvestmentAccountPerformance().getBenchmarkSecurity(), previousDate));
			dailyPerformance.setSecondaryBenchmarkReturn(calculateAccountDailyPerformanceBenchmarkReturn(dateToCalculate, config.getPerformanceCompositeInvestmentAccountPerformance().getSecondaryBenchmarkSecurity(), previousDate));
			dailyPerformance.setThirdBenchmarkReturn(calculateAccountDailyPerformanceBenchmarkReturn(dateToCalculate, config.getPerformanceCompositeInvestmentAccountPerformance().getThirdBenchmarkSecurity(), previousDate));
		}
		else {
			dailyPerformance.setBenchmarkReturn(BigDecimal.ZERO);
			dailyPerformance.setSecondaryBenchmarkReturn(BigDecimal.ZERO);
			dailyPerformance.setThirdBenchmarkReturn(BigDecimal.ZERO);
		}
	}


	private BigDecimal calculateAccountDailyPerformanceBenchmarkReturn(Date processDate, InvestmentSecurity benchmarkSecurity, Date previousDate) {
		return (benchmarkSecurity != null) ? getMarketDataRetriever().getInvestmentSecurityReturn(null, benchmarkSecurity,
				previousDate, processDate, "", true) : null;
	}


	////////////////////////////////////////////////////////////////////////////////


	protected BigDecimal calculateManagementFee(PerformanceCompositeAccountRuleEvaluatorContext config) {
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = config.getPerformanceCompositeInvestmentAccountPerformance();
		AccountingPeriodClosing periodClosing = config.getAccountingPeriodClosingForAccountPerformance(accountPerformance);
		BigDecimal periodProjectedRevenue = (periodClosing != null) ? periodClosing.getPeriodProjectedRevenue() : BigDecimal.ZERO;

		if (getManagementFeeOverrideCalculatorBean() != null) {
			PerformanceCompositeAccountManagementFeeOverrideCalculator calculator = (PerformanceCompositeAccountManagementFeeOverrideCalculator) getSystemBeanService().getBeanInstance(getManagementFeeOverrideCalculatorBean());
			calculator.calculateManagementFeeOverride(config, periodProjectedRevenue);
		}
		return ObjectUtils.coalesce(accountPerformance.getManagementFeeOverride(), periodProjectedRevenue);
	}


	/**
	 * Calculate the monthly metrics for period end market value and period gross return
	 */
	protected abstract void calculateMonthlyMetrics(PerformanceCompositeAccountRuleEvaluatorContext config);


	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Ensures the monthly entity {@link PerformanceCompositeInvestmentAccountPerformance} exists and that the
	 * previous month end and last of month position values exist.
	 */
	protected void validateMonthlyAndDailyBaseValuesExist(PerformanceCompositeAccountRuleEvaluatorContext config) {
		PerformanceCompositeInvestmentAccountPerformance monthlyPerformance = config.getPerformanceCompositeInvestmentAccountPerformance();
		ValidationUtils.assertNotNull(monthlyPerformance, "Must load monthly and daily base values (External Data) for this calculator type.");

		PerformanceCompositeInvestmentAccountDailyPerformance previousMonthEndDailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOnDate(DateUtils.getLastDayOfPreviousMonth(config.getAccountingPeriod().getStartDate()));
		ValidationUtils.assertNotNull(previousMonthEndDailyPerformance, "Must load previous month ends daily market value to use as the base market value.");

		PerformanceCompositeInvestmentAccountDailyPerformance lastDayOfMonthPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOnDate(config.getAccountingPeriod().getEndDate());
		ValidationUtils.assertNotNull(lastDayOfMonthPerformance, "Must load last day of month daily market value.");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemBean getManagementFeeOverrideCalculatorBean() {
		return this.managementFeeOverrideCalculatorBean;
	}


	public void setManagementFeeOverrideCalculatorBean(SystemBean managementFeeOverrideCalculatorBean) {
		this.managementFeeOverrideCalculatorBean = managementFeeOverrideCalculatorBean;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
