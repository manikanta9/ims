package com.clifton.performance.composite.performance.search;

import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;


/**
 * The <code>PerformanceAccountSearchFormRestrictionConfigurer</code> class allows to define cross-project form restriction
 * for accounts missing an active on date composite assignment.
 *
 * @author apopp
 */
@Component
public class PerformanceAccountSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {


	@Override
	public Class<?> getSearchFormClass() {
		return InvestmentAccountSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "accountsMissingCompositeAssignmentActiveOnDate";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (restriction.getValue() != null) {
			//The list of accounts WHERE NOT EXIST an active composite assignment
			DetachedCriteria ne = DetachedCriteria.forClass(PerformanceCompositeInvestmentAccount.class, "performanceCompositeInvestmentAccount");
			ne.setProjection(Projections.property("id"));
			ne.add(Restrictions.eqProperty("investmentAccount.id", criteria.getAlias() + ".id"));
			ne.add(ActiveExpressionForDates.forActiveOnDate(true, DateUtils.toDate((String) restriction.getValue())));
			criteria.add(Subqueries.notExists(ne));
		}
	}
}
