package com.clifton.performance.composite.process.jobs;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.performance.account.process.jobs.BasePerformanceAccountProcessorJob;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;

import java.util.Date;
import java.util.Map;


/**
 * @author terrys
 */
public class PerformanceCompositeInvestmentAccountPerformanceProcessorJob extends BasePerformanceAccountProcessorJob {

	/**
	 * Calculate the daily performance and base monthly return values (current period net,gross,etc)
	 */
	private boolean calculateAccountDailyMonthlyBasePerformance;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = Status.ofMessage("Starting Performance Composite Account Performance processing.");

		PerformanceCompositeAccountRebuildCommand command = new PerformanceCompositeAccountRebuildCommand();
		command.setInvestmentAccountGroupId(getInvestmentAccountGroupId());
		command.setSynchronous(true);
		command.setStatus(status);

		Date asOfDate = (isUsePreviousWeekdayForAccountPeriodRetrieval() ? DateUtils.getPreviousWeekday(new Date()) : new Date());
		if (isProcessCurrentAccountingPeriod()) {
			command.setToAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(asOfDate));
		}
		else {
			command.setToAccountingPeriod(getAccountingPeriodService().getAccountingPeriodForDate(DateUtils.getLastDayOfPreviousMonth(asOfDate)));
		}
		command.setCalculateAccountDailyMonthlyBasePerformance(isCalculateAccountDailyMonthlyBasePerformance());
		command.setMetricProcessingType(PerformanceCompositeMetricProcessingTypes.ALL);

		getPerformanceCompositePerformanceRebuildService().rebuildPerformanceCompositeAccountPerformance(command);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isCalculateAccountDailyMonthlyBasePerformance() {
		return this.calculateAccountDailyMonthlyBasePerformance;
	}


	public void setCalculateAccountDailyMonthlyBasePerformance(boolean calculateAccountDailyMonthlyBasePerformance) {
		this.calculateAccountDailyMonthlyBasePerformance = calculateAccountDailyMonthlyBasePerformance;
	}


	public PerformanceCompositePerformanceRebuildService getPerformanceCompositePerformanceRebuildService() {
		return this.performanceCompositePerformanceRebuildService;
	}


	public void setPerformanceCompositePerformanceRebuildService(PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService) {
		this.performanceCompositePerformanceRebuildService = performanceCompositePerformanceRebuildService;
	}
}
