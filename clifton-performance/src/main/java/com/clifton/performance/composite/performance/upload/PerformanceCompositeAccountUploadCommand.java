package com.clifton.performance.composite.performance.upload;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.system.upload.SystemUploadCommand;


/**
 * The <code>PerformanceCompositeAccountUploadCommand</code> is not a simple upload - as it uses the generic upload, but adds additional functionality for
 * cleaning up daily values and automatically processing the returns that were uploaded.
 *
 * @author manderson
 */
public class PerformanceCompositeAccountUploadCommand extends SystemUploadCommand {

	/**
	 * Used mainly for cases where IMS calculation is used for preliminary data, but then external values
	 * are uploaded for final data. In that case, we want to clear the daily IMS data, and just upload the returns given.
	 */
	private boolean deleteDailyPerformance;

	/**
	 * If true, will process the performance record after returns are uploaded.  i.e. sets benchmark returns, model net, and all QTD, YTD, etc.
	 */
	private boolean processPerformance;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "PerformanceCompositeInvestmentAccountPerformance";
	}


	////////////////////////////////////////////////////////////////////////////////


	public boolean isDeleteDailyPerformance() {
		return this.deleteDailyPerformance;
	}


	public void setDeleteDailyPerformance(boolean deleteDailyPerformance) {
		this.deleteDailyPerformance = deleteDailyPerformance;
	}


	public boolean isProcessPerformance() {
		return this.processPerformance;
	}


	public void setProcessPerformance(boolean processPerformance) {
		this.processPerformance = processPerformance;
	}
}
