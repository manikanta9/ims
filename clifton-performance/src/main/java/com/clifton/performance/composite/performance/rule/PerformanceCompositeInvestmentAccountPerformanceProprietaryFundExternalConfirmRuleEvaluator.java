package com.clifton.performance.composite.performance.rule;

import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Rule Evaluator that checks external data for final returns. Specifically generates a Violation if Performance Composite Investment Account Daily Performance List is empty.
 *
 * @author jonathanr
 */
public class PerformanceCompositeInvestmentAccountPerformanceProprietaryFundExternalConfirmRuleEvaluator extends BaseRuleEvaluator<PerformanceCompositeInvestmentAccountPerformance, PerformanceCompositeAccountRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance, RuleConfig ruleConfig, PerformanceCompositeAccountRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformanceCompositeInvestmentAccountDailyPerformance> performanceCompositeInvestmentAccountDailyPerformances = context.getPerformanceCompositeInvestmentAccountDailyPerformanceList(performanceCompositeInvestmentAccountPerformance);
			if (!CollectionUtils.isEmpty(performanceCompositeInvestmentAccountDailyPerformances)) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNote", "Account uses calculated values for preliminary data only and must use external data for final returns.  Please confirm (by ignoring this violation) that external data has been uploaded and is being used.");
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceCompositeInvestmentAccountPerformance.getId(), contextValues));
			}
		}
		return ruleViolationList;
	}
}
