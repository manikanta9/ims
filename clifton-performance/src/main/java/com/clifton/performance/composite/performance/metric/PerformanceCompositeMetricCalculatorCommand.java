package com.clifton.performance.composite.performance.metric;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeMetricCalculatorCommand</code> is used to process the metrics for the given list with the following options
 *
 * @author manderson
 */
public class PerformanceCompositeMetricCalculatorCommand<T extends PerformanceCompositeMetric> {

	private final List<T> performanceCompositeMetricList;

	private final Date fromDate;
	private final Date toDate;

	/**
	 * Note: This is not final and can be reset during processing when a break in performance is found
	 */
	private Date performanceInceptionDate;

	/**
	 * What we are calculating - all, benchmarks only, linked returns only
	 */
	private final PerformanceCompositeMetricProcessingTypes processingType;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeMetricCalculatorCommand(List<T> performanceCompositeMetricList, Date fromDate, Date toDate, PerformanceCompositeMetricProcessingTypes processingType) {
		ValidationUtils.assertNotNull(processingType, "Processing Type is Required");
		this.performanceCompositeMetricList = performanceCompositeMetricList;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.processingType = processingType;
		this.performanceInceptionDate = CollectionUtils.getFirstElementStrict(performanceCompositeMetricList).getStartDate();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected Integer getPerformanceCompositeMetricStartIndex() {
		Integer startIndex = findPerformanceMetricStartIndexFromDate(getFromDate());
		if (getProcessingType().isCalculateAllMetricReturns()) {
			ValidationUtils.assertNotNull(startIndex, "No performance metrics exists on [" + DateUtils.fromDateShort(getFromDate()) + "]");
		}
		// Only processing what we have so start at first available - end index will be -1 if nothing in the list
		return startIndex == null ? 0 : startIndex;
	}


	protected Integer getPerformanceCompositeMetricEndIndex() {
		Integer endIndex = findPerformanceMetricStartIndexFromDate(getToDate());
		if (endIndex == null) {
			endIndex = getPerformanceCompositeMetricList().size() - 1;
		}
		return endIndex;
	}


	/**
	 * When there is a break in performance returns the accounting period start date after the last break
	 * Returns null if there is no break in performance
	 */
	protected Date getPerformanceInceptionDateOverride(int currentIndexId) {
		if (currentIndexId == 0) {
			return this.performanceInceptionDate;
		}

		for (int i = currentIndexId; i > 0; i--) {
			Date currentDate = getPerformanceCompositeMetricList().get(i).getAccountingPeriod().getStartDate();
			Date previousDate = getPerformanceCompositeMetricList().get(i - 1).getAccountingPeriod().getEndDate();
			if (!DateUtils.isEqualWithoutTime(previousDate, DateUtils.getLastDayOfPreviousMonth(currentDate))) {
				Date startDate = getPerformanceCompositeMetricList().get(i).getStartDate();
				if (DateUtils.isDateAfter(currentDate, startDate)) {
					return currentDate;
				}
				return startDate;
			}
		}
		return this.performanceInceptionDate;
	}


	/**
	 * Given a list of performance composite metric entities sorted by start date,
	 * find the index of the first entity greater than the from date.
	 */
	private Integer findPerformanceMetricStartIndexFromDate(Date date) {
		for (int i = 0; i < CollectionUtils.getSize(getPerformanceCompositeMetricList()); i++) {
			if (DateUtils.isDateAfterOrEqual(getPerformanceCompositeMetricList().get(i).getAccountingPeriod().getStartDate(), DateUtils.getFirstDayOfMonth(date))) {
				return i;
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<T> getPerformanceCompositeMetricList() {
		return this.performanceCompositeMetricList;
	}


	public Date getFromDate() {
		return this.fromDate;
	}


	public Date getToDate() {
		return this.toDate;
	}


	public PerformanceCompositeMetricProcessingTypes getProcessingType() {
		return this.processingType;
	}
}
