package com.clifton.performance.composite.setup.cache;

import com.clifton.performance.composite.setup.PerformanceCompositeRollup;

import java.util.List;


/**
 * The <code>PerformanceCompositeRollupCache</code> caches rollup relationships for a composite as either the parent or the child.
 * <p>
 * Cache stores ID values of the {@link com.clifton.performance.composite.setup.PerformanceCompositeRollup} and when retrieved looks up by id values
 * <p>
 * Calling methods can then filter out on parent or child = composite id to get the list of parents or the list of children
 * <p>
 * Cache is cleared for parent and child when rollup is updated
 *
 * @author manderson
 */
public interface PerformanceCompositeRollupCache {

	public Short[] getPerformanceCompositeRollupList(short performanceCompositeId);


	public void storePerformanceCompositeRollupList(short performanceCompositeId, List<PerformanceCompositeRollup> list);
}
