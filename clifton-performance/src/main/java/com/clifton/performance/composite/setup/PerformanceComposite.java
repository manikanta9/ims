package com.clifton.performance.composite.setup;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.report.definition.Report;
import com.clifton.system.bean.SystemBean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceComposite</code> class represents a GIPS composite: aggregate of or or more investment accounts.
 *
 * @author vgomelsky
 */
public class PerformanceComposite extends NamedEntity<Short> {

	public static final String PERFORMANCE_COMPOSITE_TABLE_NAME = "PerformanceComposite";

	private InvestmentSecurity currency;
	private InvestmentSecurity benchmarkSecurity;
	private InvestmentSecurity secondaryBenchmarkSecurity;
	private InvestmentSecurity thirdBenchmarkSecurity;
	private InvestmentSecurity riskFreeSecurity;

	/**
	 * A short name used in reports
	 */
	private String compositeShortName;

	/**
	 * Unique code assigned to this composite. For example, CAPS code.
	 */
	private String code;

	/**
	 * Specifies whether this composite is a rollup of other composite(s).
	 * A rollup composite cannot have accounts assigned to it directly.
	 * Instead it inherits account assignments from its children.
	 */
	private boolean rollupComposite;

	/**
	 * Specifies whether performance for this composite uses Actual (true) or Model (false) Net Returns.
	 */
	private boolean actualNetReturnUsed;

	/**
	 * Model fees take account level monthly gross performance and calculate a monthly net return based off of the model fee %.
	 */
	private BigDecimal modelFee;

	private Date startDate;
	private Date endDate;

	private List<PerformanceComposite> childCompositeList;


	/**
	 * Required for non rollup composites
	 * This is the default performance calculator the account assignments for this composite will use which can be overridden on the assignment
	 */
	private SystemBean accountPerformanceCalculatorBean;


	/**
	 * By default we only include an account's performance in the composite if they had positions on for the entire period
	 * Some composites we want to include all even if the account took positions off during the period
	 * Note: This check is already excluded for for Client Group Accounts (i.e. isOurAccountsOnly = true) as they never hold positions themselves and are used for externally loaded values
	 */
	private boolean includePartialPeriodAccounts;


	// Reporting/Posting Configuration

	/**
	 * Default Report used for the composite performance
	 */
	private Report compositeReport;

	/**
	 * Used to determine when composite performance needs to move through reporting workflow states as reports need to be posted.
	 * There can be cases where some don't post reports at all, some monthly, and some quarterly.
	 */
	private PerformanceReportFrequencies compositeReportFrequency;

	/**
	 * Any account assigned to this composite would use the following report by default.
	 * Each assignment can override the report that it wants to use
	 */
	private Report accountReport;

	/**
	 * Any account assigned to this composite would use the following report frequency by default.
	 * Each assignment can override the frequency that it wants to use
	 */
	private PerformanceReportFrequencies accountReportFrequency;

	/**
	 * Any account assigned to this composite would use the following attachments option by default.
	 * Each assignment can override this option. This feature will append any file attachments on the account performance record to the generated report
	 * Note: Currently only used for Investor Reports
	 */
	private boolean accountReportAppendAttachments;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActive() {
		return DateUtils.isCurrentDateBetween(getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getCurrency() {
		return this.currency;
	}


	public void setCurrency(InvestmentSecurity currency) {
		this.currency = currency;
	}


	public String getCode() {
		return this.code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public InvestmentSecurity getSecondaryBenchmarkSecurity() {
		return this.secondaryBenchmarkSecurity;
	}


	public void setSecondaryBenchmarkSecurity(InvestmentSecurity secondaryBenchmarkSecurity) {
		this.secondaryBenchmarkSecurity = secondaryBenchmarkSecurity;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public InvestmentSecurity getThirdBenchmarkSecurity() {
		return this.thirdBenchmarkSecurity;
	}


	public void setThirdBenchmarkSecurity(InvestmentSecurity thirdBenchmarkSecurity) {
		this.thirdBenchmarkSecurity = thirdBenchmarkSecurity;
	}


	public boolean isActualNetReturnUsed() {
		return this.actualNetReturnUsed;
	}


	public void setActualNetReturnUsed(boolean actualNetReturnUsed) {
		this.actualNetReturnUsed = actualNetReturnUsed;
	}


	public boolean isRollupComposite() {
		return this.rollupComposite;
	}


	public void setRollupComposite(boolean rollupComposite) {
		this.rollupComposite = rollupComposite;
	}


	public List<PerformanceComposite> getChildCompositeList() {
		return this.childCompositeList;
	}


	public void setChildCompositeList(List<PerformanceComposite> childCompositeList) {
		this.childCompositeList = childCompositeList;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public InvestmentSecurity getRiskFreeSecurity() {
		return this.riskFreeSecurity;
	}


	public void setRiskFreeSecurity(InvestmentSecurity riskFreeSecurity) {
		this.riskFreeSecurity = riskFreeSecurity;
	}


	public BigDecimal getModelFee() {
		return this.modelFee;
	}


	public String getCompositeShortName() {
		return this.compositeShortName;
	}


	public void setCompositeShortName(String compositeShortName) {
		this.compositeShortName = compositeShortName;
	}


	public void setModelFee(BigDecimal modelFee) {
		this.modelFee = modelFee;
	}


	public Report getCompositeReport() {
		return this.compositeReport;
	}


	public void setCompositeReport(Report compositeReport) {
		this.compositeReport = compositeReport;
	}


	public PerformanceReportFrequencies getCompositeReportFrequency() {
		return this.compositeReportFrequency;
	}


	public void setCompositeReportFrequency(PerformanceReportFrequencies compositeReportFrequency) {
		this.compositeReportFrequency = compositeReportFrequency;
	}


	public Report getAccountReport() {
		return this.accountReport;
	}


	public void setAccountReport(Report accountReport) {
		this.accountReport = accountReport;
	}


	public PerformanceReportFrequencies getAccountReportFrequency() {
		return this.accountReportFrequency;
	}


	public void setAccountReportFrequency(PerformanceReportFrequencies accountReportFrequency) {
		this.accountReportFrequency = accountReportFrequency;
	}


	public boolean isAccountReportAppendAttachments() {
		return this.accountReportAppendAttachments;
	}


	public void setAccountReportAppendAttachments(boolean accountReportAppendAttachments) {
		this.accountReportAppendAttachments = accountReportAppendAttachments;
	}


	public SystemBean getAccountPerformanceCalculatorBean() {
		return this.accountPerformanceCalculatorBean;
	}


	public void setAccountPerformanceCalculatorBean(SystemBean accountPerformanceCalculatorBean) {
		this.accountPerformanceCalculatorBean = accountPerformanceCalculatorBean;
	}


	public boolean isIncludePartialPeriodAccounts() {
		return this.includePartialPeriodAccounts;
	}


	public void setIncludePartialPeriodAccounts(boolean includePartialPeriodAccounts) {
		this.includePartialPeriodAccounts = includePartialPeriodAccounts;
	}
}
