package com.clifton.performance.composite.performance.rebuild;


import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;
import com.clifton.performance.composite.performance.process.PerformanceCompositePerformanceProcessor;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import com.clifton.performance.workflow.PerformanceWorkflowHandler;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class PerformanceCompositePerformanceRebuildServiceImpl implements PerformanceCompositePerformanceRebuildService {

	private AccountingPeriodService accountingPeriodService;

	private PerformanceCompositePerformanceProcessor performanceCompositePerformanceProcessor;
	private PerformanceCompositeSetupService performanceCompositeSetupService;
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceWorkflowHandler performanceWorkflowHandler;

	private RunnerHandler runnerHandler;


	////////////////////////////////////////////////////////////////////////////////
	////////////          Performance Composite Performance             ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String rebuildPerformanceCompositePerformance(final PerformanceCompositeRebuildCommand command) {
		if (command.isSynchronous()) {
			doProcessPerformanceCompositePerformanceList(command);
			return command.getStatus().getMessage();
		}

		final Date now = new Date();
		final String runId = command.getUniqueRunId();

		Runner runner = new AbstractStatusAwareRunner("COMPOSITE-PERFORMANCE-RUN", runId, now, new StatusHolder(command.getStatus())) {

			@Override
			public void run() {
				try {
					doProcessPerformanceCompositePerformanceList(command);
					command.getStatus().setMessageWithErrors(command.getStatus().getMessage(), 5);
				}
				catch (Throwable e) {
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error rebuilding performance for " + runId, e);
				}
			}
		};

		getRunnerHandler().runNow(runner);
		return "Started performance rebuild. Processing will be completed shortly.";
	}


	@Override
	public PerformanceCompositePerformance previewPerformanceCompositePerformance(PerformanceCompositeRebuildCommand command) {
		command.setPreview(true);
		ValidationUtils.assertNotNull(command.getPerformanceCompositeId(), "Performance Composite Selection is Required for Preview.");
		return doProcessPerformanceCompositePerformanceList(command);
	}


	/**
	 * Note: Only returns PerformanceCompositePerformance object if previewing a specific performance entity
	 */
	private PerformanceCompositePerformance doProcessPerformanceCompositePerformanceList(PerformanceCompositeRebuildCommand command) {

		validatePerformanceCompositeRebuildCommandDates(command);

		// Rebuilds can be done for One Composite or ALL
		List<PerformanceComposite> performanceCompositeList;
		if (command.getPerformanceCompositeId() != null) {
			performanceCompositeList = CollectionUtils.createList(getPerformanceCompositeSetupService().getPerformanceComposite(command.getPerformanceCompositeId()));
		}
		else {
			performanceCompositeList = getPerformanceCompositeSetupService().getPerformanceCompositeList(new PerformanceCompositeSearchForm());
		}

		int totalCount = CollectionUtils.getSize(performanceCompositeList);
		if (totalCount == 0) {
			command.getStatus().setMessage("No performance composite entities were found");
			command.getStatus().setActionPerformed(false);
			return null;
		}

		command.getStatus().setMessage("Processing started.");

		int failedCount = 0;
		int successCount = 0;


		for (PerformanceComposite performanceComposite : CollectionUtils.getIterable(performanceCompositeList)) {
			try {
				// If needed, move selected composite performance back to Draft
				if (command.isTransitionToDraftPriorToProcessing()) {
					PerformanceCompositePerformanceSearchForm compositePerformanceSearchForm = new PerformanceCompositePerformanceSearchForm();
					compositePerformanceSearchForm.setPerformanceCompositeId(performanceComposite.getId());
					compositePerformanceSearchForm.setAfterPeriodStartDate(command.getFromAccountingPeriod().getStartDate());
					compositePerformanceSearchForm.setBeforePeriodEndDate(command.getToAccountingPeriod().getEndDate());
					getPerformanceWorkflowHandler().transitionPerformanceCompositePerformanceList(getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(compositePerformanceSearchForm), PerformanceWorkflowHandler.PERFORMANCE_DRAFT_WORKFLOW_STATE_NAME);
				}

				PerformanceCompositePerformance performanceCompositePerformance = getPerformanceCompositePerformanceProcessor().processPerformanceCompositePerformance(performanceComposite, command);
				if (command.isPreview()) {
					return performanceCompositePerformance;
				}
				successCount++;

				// Update Runner status
				int totalRemaining = totalCount - (successCount + failedCount);
				command.getStatus().setMessage("Total composites Remaining: " + totalRemaining + " out of " + totalCount + "; Completed: " + successCount + "; Errors Persisting: " + failedCount
						+ "].");
			}
			catch (Throwable e) {
				if (command.isPreview()) {
					throw e;
				}
				String error = "Failed processing performance composite [" + performanceComposite.getLabel() + "]: " + ExceptionUtils.getOriginalMessage(e);
				if (!CoreExceptionUtils.isValidationException(e)) {
					LogUtils.error(getClass(), error, e);
				}
				command.getStatus().addError(error);

				failedCount++;
			}
		}

		command.getStatus().setMessage("Processing Complete. Total composites: " + totalCount + "; Completed: " + successCount + "; Errors: " + failedCount);
		return null;
	}


	/**
	 * Validates the command date selections, and will also populate accounting period from the database so we have access to dates on the periods
	 */
	private void validatePerformanceCompositeRebuildCommandDates(PerformanceCompositeRebuildCommand command) {
		ValidationUtils.assertNotNull(command, "Command object not instantiated.");
		ValidationUtils.assertNotNull(command.getToAccountingPeriod(), "Process Period Required");
		ValidationUtils.assertNotNull(command.getToAccountingPeriod().getId(), "Process Period Required");

		// Because it's not a real object, binding wouldn't occur, so as long as Id is set, populate it with the real accounting period
		command.setToAccountingPeriod(getAccountingPeriodService().getAccountingPeriod(command.getToAccountingPeriod().getId()));

		// If From Date is not specified - only process the one period (Set From Period to To Period)
		if (command.getFromAccountingPeriod() == null || command.getFromAccountingPeriod().getId() == null) {
			command.setFromAccountingPeriod(command.getToAccountingPeriod());
		}
		else {
			command.setFromAccountingPeriod(getAccountingPeriodService().getAccountingPeriod(command.getFromAccountingPeriod().getId()));
			ValidationUtils.assertTrue(command.getToAccountingPeriod().getEndDate().after(command.getFromAccountingPeriod().getStartDate()), "Invalid Start Process Period Selected. Start period must be before end process period");
		}

		if (command.isPreview()) {
			ValidationUtils.assertTrue(command.getToAccountingPeriod().equals(command.getFromAccountingPeriod()), "Previewing Performance is only allowed for one accounting period.");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////    Performance Investment Account Composite Performance       /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String rebuildPerformanceCompositeAccountPerformance(final PerformanceCompositeAccountRebuildCommand command) {
		if (command.isSynchronous()) {
			doProcessPerformanceCompositeAccountPerformanceList(command);
			return command.getStatus().getMessage();
		}

		final Date now = new Date();
		final String runId = command.getUniqueRunId();

		Runner runner = new AbstractStatusAwareRunner("ACCOUNT-PERFORMANCE-RUN", runId, now, new StatusHolder(command.getStatus())) {
			@Override
			public void run() {
				try {
					doProcessPerformanceCompositeAccountPerformanceList(command);
					command.getStatus().setMessageWithErrors(command.getStatus().getMessage(), 5);
				}
				catch (Throwable e) {
					command.getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					command.getStatus().setMessageWithErrors(command.getStatus().getMessage(), 5);
					LogUtils.errorOrInfo(getClass(), "Error rebuilding performance for " + runId, e);
				}
			}
		};

		getRunnerHandler().runNow(runner);
		return "Started performance rebuild. Processing will be completed shortly.";
	}


	@Override
	public PerformanceCompositeInvestmentAccountPerformance previewPerformanceCompositeAccountPerformance(PerformanceCompositeAccountRebuildCommand command) {
		command.setPreview(true);
		ValidationUtils.assertNotNull(command.getPerformanceCompositeInvestmentAccountId(), "Account Assignment Selection is Required for Preview.");
		// Validate here selection is active during the selected period
		PerformanceCompositeInvestmentAccountPerformance result = doProcessPerformanceCompositeAccountPerformanceList(command);
		if (result == null) {
			throw new ValidationException("Error previewing performance: " + command.getStatus().getMessageWithErrors());
		}
		return result;
	}


	private PerformanceCompositeInvestmentAccountPerformance doProcessPerformanceCompositeAccountPerformanceList(PerformanceCompositeAccountRebuildCommand command) {
		validatePerformanceCompositeRebuildCommandDates(command);

		List<PerformanceCompositeInvestmentAccount> accountAssignmentList = getPerformanceCompositeInvestmentAccountListForRebuildCommand(command);

		int totalCount = CollectionUtils.getSize(accountAssignmentList);
		if (totalCount == 0) {
			command.getStatus().setMessage("No account assignments were found for selected filters and processing period.");
			command.getStatus().setActionPerformed(false);
			return null;
		}

		command.getStatus().setMessage("Processing started.");

		int failedCount = 0;
		int successCount = 0;

		for (PerformanceCompositeInvestmentAccount accountAssignment : CollectionUtils.getIterable(accountAssignmentList)) {
			try {
				// If needed, move selected account performance back to Draft
				if (command.isTransitionToDraftPriorToProcessing()) {
					PerformanceCompositeInvestmentAccountPerformanceSearchForm accountPerformanceSearchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();

					accountPerformanceSearchForm.setPerformanceCompositeInvestmentAccountId(accountAssignment.getId());

					// Check account assignment to ensure we aren't pulling in performance outside of the assignment range
					if (accountAssignment.getStartDate() != null && DateUtils.isDateBefore(command.getFromAccountingPeriod().getStartDate(), accountAssignment.getStartDate(), false)) {
						accountPerformanceSearchForm.setAfterPeriodStartDate(accountAssignment.getStartDate());
					}
					else {
						accountPerformanceSearchForm.setAfterPeriodStartDate(command.getFromAccountingPeriod().getStartDate());
					}

					if (accountAssignment.getEndDate() != null && DateUtils.isDateBefore(accountAssignment.getEndDate(), command.getFromAccountingPeriod().getEndDate(), false)) {
						accountPerformanceSearchForm.setBeforePeriodEndDate(accountAssignment.getEndDate());
					}
					else {
						accountPerformanceSearchForm.setBeforePeriodEndDate(command.getToAccountingPeriod().getEndDate());
					}

					getPerformanceWorkflowHandler().transitionPerformanceCompositeInvestmentAccountPerformanceList(getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(accountPerformanceSearchForm), PerformanceWorkflowHandler.PERFORMANCE_DRAFT_WORKFLOW_STATE_NAME);
				}
				// Note: This returns ONLY the last one, which is all that is allowed for Preview Feature
				PerformanceCompositeInvestmentAccountPerformance performance = getPerformanceCompositePerformanceProcessor().processPerformanceCompositeInvestmentAccountPerformance(accountAssignment, command);
				if (command.isPreview()) {
					return performance;
				}
				successCount++;

				// Update Runner status
				int totalRemaining = totalCount - (successCount + failedCount);
				command.getStatus().setMessage("Total Account Assignments Remaining: " + totalRemaining + " out of " + totalCount + "; Completed: " + successCount + "; Skipped Records: " + command.getStatus().getSkippedCount() + "; Errors Persisting: " + failedCount + "].");
			}
			catch (Throwable e) {
				if (command.isPreview() || command.isSynchronous()) {
					throw e;
				}
				String error = "Failed processing account performance [" + accountAssignment.getLabel() + "]: " + ExceptionUtils.getOriginalMessage(e);
				command.getStatus().addError(error);
				if (!CoreExceptionUtils.isValidationException(e)) {
					LogUtils.error(getClass(), error, e);
				}

				failedCount++;
			}
		}

		String completedMessage = "Processing Complete. Total Account Assignments: " + totalCount + "; Completed: " + successCount + "; Skipped Records: " + command.getStatus().getSkippedCount() + "; Errors: " + failedCount;
		if (command.isProcessCompositeBenchmarks() && command.getMetricProcessingType() == PerformanceCompositeMetricProcessingTypes.BENCHMARKS_ONLY && command.getPerformanceCompositeId() != null) {
			PerformanceCompositeRebuildCommand compositeRebuildCommand = new PerformanceCompositeRebuildCommand();
			BeanUtils.copyProperties(command, compositeRebuildCommand);
			compositeRebuildCommand.setStatus(Status.ofEmptyMessage());
			command.getStatus().setMessage("Account " + completedMessage + " Started processing composite benchmarks.");
			doProcessPerformanceCompositePerformanceList(compositeRebuildCommand);
			if (compositeRebuildCommand.getStatus().getErrorCount() > 0) {
				command.getStatus().getDetailList().addAll(compositeRebuildCommand.getStatus().getErrorList());
			}
			completedMessage = "Account " + completedMessage + ". Composite " + compositeRebuildCommand.getStatus().getMessage();
		}
		command.getStatus().setMessage(completedMessage);
		return null;
	}


	private List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeInvestmentAccountListForRebuildCommand(PerformanceCompositeAccountRebuildCommand command) {
		PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		searchForm.setId(command.getPerformanceCompositeInvestmentAccountId());
		searchForm.setInvestmentAccountId(command.getInvestmentAccountId());
		searchForm.setInvestmentAccountGroupId(command.getInvestmentAccountGroupId());
		searchForm.setPerformanceCompositeId(command.getPerformanceCompositeId());
		searchForm.setActiveOnDateRangeStartDate(command.getFromAccountingPeriod().getStartDate());
		searchForm.setActiveOnDateRangeEndDate(command.getToAccountingPeriod().getEndDate());
		return getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////   Performance Investment Account Daily Composite Performance     ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PerformanceCompositeInvestmentAccountDailyPerformance> previewPerformanceCompositeAccountDailyPerformanceRun(final PerformanceCompositeAccountRebuildCommand command) {
		command.setPreview(true);
		command.setCalculateAccountDailyMonthlyBasePerformance(true);
		ValidationUtils.assertNotNull(command.getPerformanceCompositeInvestmentAccountId(), "Account Assignment Selection is Required for Preview.");
		validatePerformanceCompositeRebuildCommandDates(command);
		return getPerformanceCompositePerformanceProcessor().processPerformanceCompositeAccountPerformanceByAssignmentAndPeriod(getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccount(command.getPerformanceCompositeInvestmentAccountId()), command.getToAccountingPeriod(), command).getPerformanceCompositeInvestmentAccountDailyPerformanceList();
	}


	@Override
	public PerformanceCompositeInvestmentAccountDailyPerformance previewPerformanceCompositeAccountDailyPerformanceRunForMeasureDate(PerformanceCompositeAccountRebuildCommand command) {
		ValidationUtils.assertNotNull(command.getMeasureDate(), "Measure Date not specified");
		return CollectionUtils.getOnlyElement(BeanUtils.filter(previewPerformanceCompositeAccountDailyPerformanceRun(command), PerformanceCompositeInvestmentAccountDailyPerformance::getMeasureDate, command.getMeasureDate()));
	}


	/**
	 * Daily performance save method that is exposed to UI so any changes made (overrides) to the daily values will automatically kick off
	 * the monthly rebuild of performance.
	 */
	@Override
	public PerformanceCompositeInvestmentAccountDailyPerformance savePerformanceCompositeInvestmentAccountDailyPerformance(PerformanceCompositeInvestmentAccountDailyPerformance bean) {
		getPerformanceCompositePerformanceService().savePerformanceCompositeInvestmentAccountDailyPerformance(bean);

		PerformanceCompositeAccountRebuildCommand rebuildCommand = new PerformanceCompositeAccountRebuildCommand();
		rebuildCommand.setSynchronous(true);
		rebuildCommand.setCalculateAccountDailyMonthlyBasePerformance(true);
		rebuildCommand.setPerformanceCompositeInvestmentAccountId(bean.getPerformanceCompositeInvestmentAccountPerformance().getPerformanceCompositeInvestmentAccount().getId());
		rebuildCommand.setToAccountingPeriod(bean.getPerformanceCompositeInvestmentAccountPerformance().getAccountingPeriod());
		rebuildPerformanceCompositeAccountPerformance(rebuildCommand);

		// Return the re-calculated bean
		return getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountDailyPerformance(bean.getId());
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////             Getter and Setter Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public PerformanceCompositePerformanceProcessor getPerformanceCompositePerformanceProcessor() {
		return this.performanceCompositePerformanceProcessor;
	}


	public void setPerformanceCompositePerformanceProcessor(PerformanceCompositePerformanceProcessor performanceCompositePerformanceProcessor) {
		this.performanceCompositePerformanceProcessor = performanceCompositePerformanceProcessor;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public PerformanceWorkflowHandler getPerformanceWorkflowHandler() {
		return this.performanceWorkflowHandler;
	}


	public void setPerformanceWorkflowHandler(PerformanceWorkflowHandler performanceWorkflowHandler) {
		this.performanceWorkflowHandler = performanceWorkflowHandler;
	}
}
