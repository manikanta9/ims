package com.clifton.performance.composite.performance.process;

import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.accounting.period.search.AccountingPeriodSearchForm;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricCalculator;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricCalculatorCommand;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;
import com.clifton.performance.composite.performance.process.calculators.PerformanceCalculator;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeRebuildCommand;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeMetricContext;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeRuleEvaluatorContext;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Component
public class PerformanceCompositePerformanceProcessorImpl implements PerformanceCompositePerformanceProcessor {

	private AccountingPeriodService accountingPeriodService;
	private AccountingPositionHandler accountingPositionHandler;

	private ApplicationContextService applicationContextService;

	private PerformanceCompositeMetricCalculator performanceCompositeMetricCalculator;
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	private RuleViolationService ruleViolationService;

	private SystemBeanService systemBeanService;

	public static final String PERFORMANCE_COMPOSITE_ACCOUNT_PROCESSING_CONTEXT_NAME = "Performance Composite Account Rule Evaluator Context";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositePerformance processPerformanceCompositePerformance(PerformanceComposite performanceComposite, PerformanceCompositeRebuildCommand command) {
		Date fromDate = command.getFromAccountingPeriod().getStartDate();
		Date toDate = command.getToAccountingPeriod().getEndDate();

		// Ensure From Date is not before the start of the composite
		if (DateUtils.isDateAfterOrEqual(performanceComposite.getStartDate(), fromDate)) {
			fromDate = performanceComposite.getStartDate();
		}

		//Get a list of all account performance associated to the accounts under this composite
		List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceListByComposite = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceListByPerformanceComposite(
				performanceComposite.getId(), DateUtils.addMonths(fromDate, -1), toDate);

		if (CollectionUtils.isEmpty(accountPerformanceListByComposite)) {
			command.getStatus().addSkipped("Skipped [" + performanceComposite.getLabel() + "]. No Account Performance Records Exist.");
			return null;
		}

		if (!StringUtils.isEmpty(command.getAccountRequiredWorkflowStatusName())) {
			for (PerformanceCompositeInvestmentAccountPerformance accountPerformance : CollectionUtils.getIterable(accountPerformanceListByComposite)) {
				if (!StringUtils.isEqualIgnoreCase(command.getAccountRequiredWorkflowStatusName(), accountPerformance.getWorkflowStatus().getName())) {
					// Skip Processing - all accounts NOT in required status
					command.getStatus().addSkipped("Skipped [" + performanceComposite.getLabel() + "]. Account Performance for [" + accountPerformance.getPerformanceCompositeInvestmentAccount().getInvestmentAccount().getLabel() + "] not in status [" + command.getAccountRequiredWorkflowStatusName() + "].");
					return null;
				}
			}
		}

		//Get a list of all composite performance under this composite
		List<PerformanceCompositePerformance> performanceCompositePerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceListByPerformanceComposite(performanceComposite.getId());

		//Process composite
		doProcessPerformanceComposite(performanceComposite, performanceCompositePerformanceList, accountPerformanceListByComposite, fromDate, toDate, command.getMetricProcessingType());

		performanceCompositePerformanceList = filterPerformanceForPeriod(performanceCompositePerformanceList, command.getFromAccountingPeriod(), command.getToAccountingPeriod());

		//Save the results
		if (!command.isPreview()) {
			getPerformanceCompositePerformanceService().savePerformanceCompositePerformanceList(performanceCompositePerformanceList);
		}
		else {
			return CollectionUtils.getFirstElement(performanceCompositePerformanceList);
		}
		return null;
	}


	private void doProcessPerformanceComposite(PerformanceComposite performanceComposite, List<PerformanceCompositePerformance> performanceCompositePerformanceList,
	                                           List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceListByComposite, Date fromDate, Date toDate, PerformanceCompositeMetricProcessingTypes metricProcessingType) {

		// If calculating all - not just benchmarks or linked returns only - Process composite performance period metrics from related account level performance
		// i.e. Linked returns only (QTD, YTD, etc) are processed automatically when a previous performance is submitted for approval so those changes flow forward without manual re-processing
		if (metricProcessingType.isCalculateAllMetricReturns()) {
			doProcessCompositePerformancePeriodMetric(performanceComposite, performanceCompositePerformanceList, accountPerformanceListByComposite, fromDate, toDate);
		}

		//Sort ascending
		performanceCompositePerformanceList = BeanUtils.sortWithFunction(performanceCompositePerformanceList, performanceCompositePerformance -> performanceCompositePerformance.getAccountingPeriod().getStartDate(), true);

		//Now that base period fields are populated, populate all direct fields
		PerformanceCompositeMetricCalculatorCommand<PerformanceCompositePerformance> command = new PerformanceCompositeMetricCalculatorCommand<>(performanceCompositePerformanceList, fromDate, toDate, metricProcessingType);
		calculatePerformanceCompositeMetric(command, new PerformanceCompositeRuleEvaluatorContext());
	}


	private void doProcessCompositePerformancePeriodMetric(PerformanceComposite performanceComposite, List<PerformanceCompositePerformance> performanceCompositePerformanceList,
	                                                       List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceListByComposite, Date fromDate, Date toDate) {

		accountPerformanceListByComposite = BeanUtils.sortWithFunction(accountPerformanceListByComposite, accountPerformance -> accountPerformance.getAccountingPeriod().getStartDate(), true);
		Map<AccountingPeriod, List<PerformanceCompositeInvestmentAccountPerformance>> accountPerformanceByStartDateMap = BeanUtils.getBeansMap(accountPerformanceListByComposite, PerformanceCompositeInvestmentAccountPerformance::getAccountingPeriod);

		Date nextDate = fromDate;

		//There must be a single performance composite performance for each month inclusively between
		//  the from and to dates specified by the user... If it doesn't exist, create it
		while (DateUtils.compare(nextDate, toDate, false) <= 0) {
			AccountingPeriod processPeriod = getAccountingPeriodService().getAccountingPeriodForDate(nextDate);
			//Create performance entity if there does not exist one yet
			if (CollectionUtils.isEmpty(BeanUtils.filter(performanceCompositePerformanceList, PerformanceCompositePerformance::getAccountingPeriod, processPeriod))) {
				//We should only create a composite performance entity if there is account performance for this entity
				ValidationUtils.assertNotNull(accountPerformanceByStartDateMap.get(processPeriod), "Calculating composite performance [" + performanceComposite.getLabel() + "], no account performance found for period [" + processPeriod + "]");
				//Composite performance does not exist, create it
				PerformanceCompositePerformance compositePerformance = new PerformanceCompositePerformance();
				compositePerformance.setAccountingPeriod(processPeriod);
				compositePerformance.setPerformanceComposite(performanceComposite);
				performanceCompositePerformanceList.add(compositePerformance);
			}

			//Move on to next month
			nextDate = DateUtils.addMonths(nextDate, 1);
		}

		//Sort ascending
		performanceCompositePerformanceList = BeanUtils.sortWithFunction(performanceCompositePerformanceList, performanceCompositePerformance -> performanceCompositePerformance.getAccountingPeriod().getStartDate(), true);

		//Calculate base period values on composite performance
		calculateCompositePerformancePeriodValues(
				getPerformanceCompositeSetupService().getPerformanceCompositeAccountListByComposite(performanceComposite.getId()),
				performanceCompositePerformanceList,
				accountPerformanceListByComposite,
				fromDate, toDate);
	}


	/**
	 * Calculate composite performance period values from account level performance entities
	 */
	private void calculateCompositePerformancePeriodValues(List<PerformanceCompositeInvestmentAccount> compositeAccountList, List<PerformanceCompositePerformance> performanceCompositePerformanceList,
	                                                       List<PerformanceCompositeInvestmentAccountPerformance> performanceCompositeAccountMetricList, Date fromDate, Date toDate) {

		// Process each composite performance
		for (PerformanceCompositePerformance compositePerformance : CollectionUtils.getIterable(performanceCompositePerformanceList)) {
			// Only process if within process date range
			if (!DateUtils.isOverlapInDates(compositePerformance.getAccountingPeriod().getStartDate(), compositePerformance.getAccountingPeriod().getEndDate(), fromDate, toDate)) {
				continue;
			}

			// Get all performance composite investment account performance that is applicable to the date range on this
			//  composite performance.  These are all entities required to process the period values
			List<PerformanceCompositeInvestmentAccountPerformance> currentAccountMetricList = getAccountPerformanceSubListByDateRange(compositeAccountList,
					performanceCompositeAccountMetricList, compositePerformance.getAccountingPeriod().getStartDate(), compositePerformance.getAccountingPeriod().getEndDate(), true);

			// Validate to ensure that there is one account level performance per activate account assignment on this composite
			validateAccountLevelDataForCompositePerformance(compositePerformance, compositeAccountList, currentAccountMetricList);

			// Calculate performance composite performance period values
			if (CollectionUtils.getSize(currentAccountMetricList) > 0) {
				// Market Value - Sum of Account's Period Period Market Values
				compositePerformance.setPeriodEndMarketValue(CoreMathUtils.sumProperty(currentAccountMetricList, PerformanceCompositeInvestmentAccountPerformance::getPeriodEndMarketValue));

				// Rest of calculations are weighted averages
				// In order to calculate period weights we must get every single prior month account performance for each current account performance
				List<PerformanceCompositeInvestmentAccountPerformance> priorMonthAccountMetricList = getAccountPerformanceSubListByDateRange(compositeAccountList, performanceCompositeAccountMetricList,
						DateUtils.addMonths(compositePerformance.getAccountingPeriod().getStartDate(), -1), DateUtils.addDays(compositePerformance.getAccountingPeriod().getStartDate(), -1), false);

				// Calculate weights: (account’s Prior month’s MV + accounts current month weighted net cash flow)/(SUM of all account’s Prior month’s MV + all accounts current month weighted net cash flow)
				List<BigDecimal> weightList = calculateCompositePerformancePeriodWeightList(currentAccountMetricList, priorMonthAccountMetricList);

				// Period Gross Return
				BigDecimal[] values = BeanUtils.getPropertyValues(currentAccountMetricList, PerformanceCompositeInvestmentAccountPerformance::getPeriodGrossReturn, BigDecimal.class);
				compositePerformance.setPeriodGrossReturn(CoreMathUtils.sum(CoreMathUtils.weightAdjustedValueList(Arrays.asList(values), weightList)));

				// Period Net Return
				values = BeanUtils.getPropertyValues(currentAccountMetricList, PerformanceCompositeInvestmentAccountPerformance::getPeriodNetReturn, BigDecimal.class);
				compositePerformance.setPeriodNetReturn(CoreMathUtils.sum(CoreMathUtils.weightAdjustedValueList(Arrays.asList(values), weightList)));

				// Period Model Net Return
				values = BeanUtils.getPropertyValues(currentAccountMetricList, PerformanceCompositeInvestmentAccountPerformance::getPeriodModelNetReturn, BigDecimal.class);
				compositePerformance.setPeriodModelNetReturn(CoreMathUtils.sum(CoreMathUtils.weightAdjustedValueList(Arrays.asList(values), weightList)));
			}
		}
	}


	/**
	 * Ensure we have one account level performance per active composite account assignment on a given composite performance date
	 */
	private void validateAccountLevelDataForCompositePerformance(PerformanceCompositeMetric compositePerformance, List<PerformanceCompositeInvestmentAccount> compositeAccountList,
	                                                             List<PerformanceCompositeInvestmentAccountPerformance> currentAccountMetricList) {

		// For each active composite account assignment
		for (PerformanceCompositeInvestmentAccount compositeAccount : CollectionUtils.getIterable(compositeAccountList)) {
			if (DateUtils.isDateAfterOrEqual(compositePerformance.getAccountingPeriod().getStartDate(), compositeAccount.getStartDate())
					&& DateUtils.isDateAfterOrEqual(compositeAccount.getEndDate(), compositePerformance.getAccountingPeriod().getEndDate())) {
				if (isAccountIncludedForPerformanceCompositePositionsOn(compositeAccount.getPerformanceComposite(), compositeAccount.getInvestmentAccount(), compositePerformance.getAccountingPeriod())) {
					boolean accountPerformanceExistsForCompositeAccount = false;

					// Ensure we have an account level performance for the active composite account assignment on the given date
					for (PerformanceCompositeInvestmentAccountPerformance accountPerformance : CollectionUtils.getIterable(currentAccountMetricList)) {
						if (accountPerformance.getClientAccount().getId().equals(compositeAccount.getInvestmentAccount().getId())) {
							accountPerformanceExistsForCompositeAccount = true;
						}
					}

					ValidationUtils.assertTrue(accountPerformanceExistsForCompositeAccount, "Composite [" + compositePerformance.getLabel() + "] performance calculation missing account performance for ["
							+ compositeAccount.getInvestmentAccount().getLabel() + "] on date [" + compositePerformance.getAccountingPeriod() + "]");
				}
			}
		}
	}


	private List<BigDecimal> calculateCompositePerformancePeriodWeightList(List<PerformanceCompositeInvestmentAccountPerformance> currentAccountMetricList,
	                                                                       List<PerformanceCompositeInvestmentAccountPerformance> priorMonthAccountMetricList) {

		List<BigDecimal> weightList = new ArrayList<>();

		List<BigDecimal> numeratorWeightList = new ArrayList<>();
		for (int i = 0; i < CollectionUtils.getSize(currentAccountMetricList); i++) {
			PerformanceCompositeInvestmentAccountPerformance currentMetric = currentAccountMetricList.get(i);
			PerformanceCompositeInvestmentAccountPerformance previousMetric = null;

			//Find the associated prior month metric if it exists
			for (PerformanceCompositeInvestmentAccountPerformance priorMonthMetric : CollectionUtils.getIterable(priorMonthAccountMetricList)) {
				if (priorMonthMetric.getClientAccount().getId().equals(currentMetric.getClientAccount().getId())) {
					previousMetric = priorMonthMetric;
				}
			}

			// If previous metric is still null - try to look it up by account and calculator - if the account switched composites but still used the same calculator
			// then we can still use that period end value for this calc

			if (previousMetric == null) {
				previousMetric = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceByAccountAndPeriod(currentMetric.getClientAccount().getId(), getAccountingPeriodService().getAccountingPeriodForDate(DateUtils.addDays(currentMetric.getAccountingPeriod().getStartDate(), -1)).getId(), currentMetric.getPerformanceCompositeInvestmentAccount().getCoalesceAccountPerformanceCalculatorBean().getId());
			}

			ValidationUtils.assertNotNull(previousMetric,
					"Cannot find previous month account performance for [" + currentMetric.getClientAccount().getLabel() + "] using calculator [" + currentMetric.getPerformanceCompositeInvestmentAccount().getCoalesceAccountPerformanceCalculatorBean().getLabelShort() + "] prior to [" + currentMetric.getAccountingPeriod() + "] the previous period market value is required for weighting calculation.");

			//(account’s Prior month’s MV+ accounts current month weighted net cash flow)
			numeratorWeightList.add(MathUtils.add(previousMetric.getPeriodEndMarketValue(), currentMetric.getPeriodWeightedNetCashflow()));
		}

		//(SUM of all account’s  Prior month’s MV+ all accounts current month weighted net cash flow)
		BigDecimal denominator = CoreMathUtils.sum(numeratorWeightList);

		for (BigDecimal numerator : CollectionUtils.getIterable(numeratorWeightList)) {
			weightList.add(MathUtils.divide(numerator, denominator));
		}

		return weightList;
	}


	/**
	 * SubList composite account performance list by a specific date range.  This is usually used to get all account performance across
	 * several accounts for a given accounting period
	 */
	private List<PerformanceCompositeInvestmentAccountPerformance> getAccountPerformanceSubListByDateRange(List<PerformanceCompositeInvestmentAccount> compositeAccountList,
	                                                                                                       List<PerformanceCompositeInvestmentAccountPerformance> accountMetricList, Date startDate, Date endDate, boolean excludeInActiveAccountPerformance) {
		List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceSubList = new ArrayList<>();
		for (PerformanceCompositeInvestmentAccountPerformance accountMetric : CollectionUtils.getIterable(accountMetricList)) {
			if (DateUtils.isOverlapInDates(accountMetric.getAccountingPeriod().getStartDate(), accountMetric.getAccountingPeriod().getEndDate(), startDate, endDate)) {
				if (!excludeInActiveAccountPerformance || (isAccountPerformanceActiveForCompositeAccountList(compositeAccountList, accountMetric))) {
					accountPerformanceSubList.add(accountMetric);
				}
			}
		}

		return accountPerformanceSubList;
	}


	/**
	 * Check if an account level performance is active by checking if the associated account assignment is active.  This allows us to know
	 * if account performance should be used in calculations.
	 */
	private boolean isAccountPerformanceActiveForCompositeAccountList(List<PerformanceCompositeInvestmentAccount> compositeAccountList, PerformanceCompositeInvestmentAccountPerformance accountMetric) {
		for (PerformanceCompositeInvestmentAccount compositeAccountAssignment : CollectionUtils.getIterable(compositeAccountList)) {
			if (compositeAccountAssignment.getInvestmentAccount().getId().equals(accountMetric.getClientAccount().getId())) {
				if (DateUtils.isOverlapInDates(compositeAccountAssignment.getStartDate(), compositeAccountAssignment.getEndDate(), accountMetric.getAccountingPeriod().getStartDate(),
						accountMetric.getAccountingPeriod().getEndDate())) {
					return isAccountIncludedForPerformanceCompositePositionsOn(compositeAccountAssignment.getPerformanceComposite(), compositeAccountAssignment.getInvestmentAccount(), accountMetric.getAccountingPeriod());
				}
			}
		}
		return false;
	}


	/**
	 * Returns true if account should be included in the composite based on the composite properties and the account having positions on during the period:
	 * If composite allows partial period accounts - Always returns true
	 * If account is a client account under a Client Group (i.e. can only contain our accounts - no holding accounts) (these accounts cannot hold positions are are used for external account data loading) - Always Returns True
	 * Else Returns true if account has positions on for the entire period
	 */
	private boolean isAccountIncludedForPerformanceCompositePositionsOn(PerformanceComposite composite, InvestmentAccount investmentAccount, AccountingPeriod accountingPeriod) {
		if (composite.isIncludePartialPeriodAccounts()) {
			return true;
		}
		if (investmentAccount.getBusinessClient().getCategory().isOurAccountsOnly()) {
			return true;
		}
		return getAccountingPositionHandler().isClientAccountPositionsOnForDateRange(investmentAccount.getId(), accountingPeriod.getStartDate(), accountingPeriod.getEndDate());
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositeInvestmentAccountPerformance processPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount, PerformanceCompositeAccountRebuildCommand command) {
		if (!command.isPreview() && (command.isCalculateAccountDailyMonthlyBasePerformance() || command.getMetricProcessingType().isCalculateBenchmarkReturns())) {
			processCompositeAccountBasePerformance(performanceCompositeInvestmentAccount, command);
		}

		PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
		searchForm.setInvestmentAccountId(performanceCompositeInvestmentAccount.getInvestmentAccount().getId());
		// Rely on assignment end date or period end date
		if (performanceCompositeInvestmentAccount.getEndDate() != null && DateUtils.isDateBefore(performanceCompositeInvestmentAccount.getEndDate(), command.getFromAccountingPeriod().getEndDate(), false)) {
			searchForm.setBeforePeriodEndDate(performanceCompositeInvestmentAccount.getEndDate());
		}
		else {
			searchForm.setBeforePeriodEndDate(command.getToAccountingPeriod().getEndDate());
		}
		searchForm.setAccountPerformanceCalculatorBeanId(performanceCompositeInvestmentAccount.getCoalesceAccountPerformanceCalculatorBean().getId());

		//Get all relevant performance composite investment account performance entities
		List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);

		Date fromDate = command.getFromAccountingPeriod().getStartDate();

		//Ensure fromDate is not before inception on the account and not before assignment start date.
		Date accountInceptionDate = performanceCompositeInvestmentAccount.getInvestmentAccount().getInceptionDate();
		if (accountInceptionDate != null && DateUtils.isDateAfterOrEqual(accountInceptionDate, fromDate)) {
			fromDate = accountInceptionDate;
		}

		accountPerformanceList = BeanUtils.sortWithFunction(accountPerformanceList, accountPerformance -> accountPerformance.getAccountingPeriod().getStartDate(), true);

		// If preview and we aren't recalculating account performance, then looking for historical changes (QTD, YTD, etc)
		if (command.isPreview() && (command.isCalculateAccountDailyMonthlyBasePerformance() || command.getMetricProcessingType().isCalculateBenchmarkReturns())) {
			setPerformancePreviewEntityInList(accountPerformanceList, performanceCompositeInvestmentAccount, command.getToAccountingPeriod(), command);
		}

		// Remove all performance prior to account performance start date
		accountPerformanceList = filterAccountPerformancePriorToDate(accountPerformanceList, (performanceCompositeInvestmentAccount.getInvestmentAccount().getPerformanceInceptionDate() != null) ? performanceCompositeInvestmentAccount.getInvestmentAccount().getPerformanceInceptionDate() :
				accountInceptionDate != null ? accountInceptionDate : performanceCompositeInvestmentAccount.getInvestmentAccount().getWorkflowStateEffectiveStartDate());

		//This service will iterate over all possible values of PerformanceCompositeAccountPerformanceFieldTypes performing their
		//  calculations on each property of the account performance entity
		PerformanceCompositeMetricCalculatorCommand<PerformanceCompositeInvestmentAccountPerformance> metricCommand = new PerformanceCompositeMetricCalculatorCommand<>(accountPerformanceList, fromDate, command.getToAccountingPeriod().getEndDate(), command.getMetricProcessingType());
		calculatePerformanceCompositeMetric(metricCommand, new PerformanceCompositeAccountRuleEvaluatorContext());
		accountPerformanceList = filterPerformanceForPeriod(accountPerformanceList, command.getFromAccountingPeriod(), command.getToAccountingPeriod());

		//Save the results
		if (!command.isPreview()) {
			getPerformanceCompositePerformanceService().savePerformanceCompositeInvestmentAccountPerformanceList(accountPerformanceList);
		}
		else {
			return CollectionUtils.getFirstElement(accountPerformanceList);
		}
		return null;
	}


	private void processCompositeAccountBasePerformance(PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount, PerformanceCompositeAccountRebuildCommand command) {
		PerformanceCompositeAccountRebuildCommand accountProcessCommand = new PerformanceCompositeAccountRebuildCommand();
		accountProcessCommand.setPerformanceCompositeInvestmentAccountId(performanceCompositeInvestmentAccount.getId());
		accountProcessCommand.setSynchronous(true);
		accountProcessCommand.setMetricProcessingType(command.getMetricProcessingType());
		accountProcessCommand.setCalculateAccountDailyMonthlyBasePerformance(command.isCalculateAccountDailyMonthlyBasePerformance());

		AccountingPeriodSearchForm periodSearchForm = new AccountingPeriodSearchForm();

		//figure out which is later
		Date accountStartDate = DateUtils.getFirstDayOfMonth(performanceCompositeInvestmentAccount.getStartDate());
		Date startDate = DateUtils.isDateAfter(accountStartDate, command.getFromAccountingPeriod().getStartDate()) ? accountStartDate : command.getFromAccountingPeriod().getStartDate();

		//figure out which is earlier
		Date endDate = command.getToAccountingPeriod().getEndDate();
		if (performanceCompositeInvestmentAccount.getEndDate() != null && DateUtils.isDateAfter(endDate, performanceCompositeInvestmentAccount.getEndDate())) {
			endDate = performanceCompositeInvestmentAccount.getEndDate();
		}
		// Is this in the future? We also need some performance data, so the first day of the month is also too soon
		Date futureDate= DateUtils.getFirstWeekdayOfMonth(DateUtils.getFirstDayOfNextMonth(new Date()));
		if(endDate.after(futureDate)){
			endDate = DateUtils.getLastDayOfMonth(new Date());
		}

		periodSearchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		periodSearchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));

		List<AccountingPeriod> accountingPeriodList = getAccountingPeriodService().getAccountingPeriodList(periodSearchForm);

		for (AccountingPeriod accountingPeriod : CollectionUtils.getIterable(accountingPeriodList)) {
			processPerformanceCompositeAccountPerformanceByAssignmentAndPeriod(performanceCompositeInvestmentAccount, accountingPeriod, accountProcessCommand);
		}
		// Copy Details to Original Command Status Object
		for (StatusDetail statusDetail : CollectionUtils.getIterable(accountProcessCommand.getStatus().getDetailList())) {
			command.getStatus().addDetail(statusDetail.getCategory(), statusDetail.getNote());
		}
	}


	@Override
	public PerformanceCompositeAccountRuleEvaluatorContext processPerformanceCompositeAccountPerformanceByAssignmentAndPeriod(PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount, AccountingPeriod accountingPeriod, PerformanceCompositeAccountRebuildCommand command) {
		SystemBean calculatorBean = performanceCompositeInvestmentAccount.getCoalesceAccountPerformanceCalculatorBean();
		ValidationUtils.assertNotNull(calculatorBean, "Performance Calculator is not set for Assignment [" + performanceCompositeInvestmentAccount.getLabel() + "]");
		PerformanceCalculator calculator = (PerformanceCalculator) getSystemBeanService().getBeanInstance(calculatorBean);
		ValidationUtils.assertNotNull(calculator, "Could not create Performance Calculator for assignment [" + performanceCompositeInvestmentAccount.getLabel() + "]");

		PerformanceCompositeInvestmentAccountPerformance accountPerformance = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(performanceCompositeInvestmentAccount.getId(), accountingPeriod.getId());
		PerformanceCompositeAccountRuleEvaluatorContext evaluatorContext = getEvaluatorContext(accountPerformance, command.isCalculateAccountDailyMonthlyBasePerformance(), (command.isPreview() && command.isPreviewIgnoreOverrides()));

		if (command.isCalculateAccountDailyMonthlyBasePerformance()) {
			calculator.calculate(evaluatorContext, (accountPerformance == null) ? PerformanceCompositeInvestmentAccountPerformance.ofAccountAssignmentAndPeriod(performanceCompositeInvestmentAccount, accountingPeriod) : accountPerformance);
		}
		// The only other option would be benchmark returns only
		else if (command.getMetricProcessingType().isCalculateBenchmarkReturns()) {
			// If it is benchmarks only - do NOT create a new account performance record - just calculate if it already exists
			if (accountPerformance != null) {
				calculator.calculateBenchmarkReturns(evaluatorContext, accountPerformance);
			}
			else {
				// Otherwise add skipped message and just return
				command.getStatus().addSkipped("Skipping [" + performanceCompositeInvestmentAccount.getLabel() + "] for [" + accountingPeriod.getEndDate() + "] because performance metric doesn't exist and rebuilding benchmarks only.");
				return evaluatorContext;
			}
		}

		if (!command.isPreview() && evaluatorContext.getPerformanceCompositeInvestmentAccountPerformance() != null) {
			// Save the results
			getPerformanceCompositePerformanceService().savePerformanceCompositeAccountMonthlyDailyPerformance(evaluatorContext.getPerformanceCompositeInvestmentAccountPerformance(), evaluatorContext.getPerformanceCompositeInvestmentAccountDailyPerformanceList());
			// Save any System Defined Rule Violations Generated During Processing
			// Always save by definition, so if the violation no longer applies it will be deleted
			getRuleViolationService().deleteRuleViolationListByLinkedEntity("PerformanceCompositeInvestmentAccountPerformance", evaluatorContext.getPerformanceCompositeInvestmentAccountPerformance().getId());
			for (Map.Entry<String, String> ruleDefinitionNameEntry : evaluatorContext.getSystemDefinedRuleViolationNoteMap().entrySet()) {
				List<RuleViolation> ruleViolationList = null;
				if (!StringUtils.isEmpty(ruleDefinitionNameEntry.getValue())) {
					ruleViolationList = CollectionUtils.createList(getRuleViolationService().createRuleViolationSystemDefined(ruleDefinitionNameEntry.getValue(), ruleDefinitionNameEntry.getKey(), evaluatorContext.getPerformanceCompositeInvestmentAccountPerformance().getId()));
				}
				getRuleViolationService().updateRuleViolationListForEntityAndDefinition(ruleViolationList, "PerformanceCompositeInvestmentAccountPerformance", evaluatorContext.getPerformanceCompositeInvestmentAccountPerformance().getId(), ruleDefinitionNameEntry.getKey());
			}
		}
		return evaluatorContext;
	}


	private PerformanceCompositeAccountRuleEvaluatorContext getEvaluatorContext(PerformanceCompositeInvestmentAccountPerformance accountPerformance, boolean resetFieldsForProcessing, boolean previewIgnoreOverrides) {
		PerformanceCompositeAccountRuleEvaluatorContext evaluatorContext = ((PerformanceCompositeAccountRuleEvaluatorContext) getSystemBeanService().getBeanInstance(getSystemBeanService().getSystemBeanByName(PERFORMANCE_COMPOSITE_ACCOUNT_PROCESSING_CONTEXT_NAME)));
		if (accountPerformance != null && !accountPerformance.isNewBean()) {
			evaluatorContext.setPerformanceCompositeInvestmentAccountDailyPerformanceList(getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(accountPerformance.getId()), resetFieldsForProcessing, previewIgnoreOverrides);
		}
		return evaluatorContext;
	}


	private void setPerformancePreviewEntityInList(List<PerformanceCompositeInvestmentAccountPerformance> performanceList, PerformanceCompositeInvestmentAccount performanceCompositeInvestmentAccount, AccountingPeriod accountingPeriod, PerformanceCompositeAccountRebuildCommand command) {
		PerformanceCompositeInvestmentAccountPerformance previewEntity = processPerformanceCompositeAccountPerformanceByAssignmentAndPeriod(performanceCompositeInvestmentAccount, accountingPeriod, command).getPerformanceCompositeInvestmentAccountPerformance();

		int index = CollectionUtils.getSize(performanceList);
		if (performanceList != null) {
			for (int i = 0; i < CollectionUtils.getSize(performanceList); i++) {
				PerformanceCompositeInvestmentAccountPerformance performance = performanceList.get(i);
				if (CompareUtils.isEqual(performance.getAccountingPeriod().getId(), previewEntity.getAccountingPeriod().getId())) {
					index = i;
					break;
				}
			}
			if (index != CollectionUtils.getSize(performanceList)) {
				//This entity may already exist so we will replace it with our preview entity
				performanceList.remove(index);
			}
			performanceList.add(index, previewEntity);
		}
	}


	private <T extends PerformanceCompositeMetric> void calculatePerformanceCompositeMetric(PerformanceCompositeMetricCalculatorCommand<T> command, PerformanceCompositeMetricContext<T> compositeMetricContext) {
		getApplicationContextService().autowireBean(compositeMetricContext);
		getPerformanceCompositeMetricCalculator().calculatePerformanceMetric(command, compositeMetricContext);
	}


	/**
	 * Used for finding the specific performance records we wish to save or preview
	 */
	private <T extends PerformanceCompositeMetric> List<T> filterPerformanceForPeriod(List<T> performanceList, AccountingPeriod fromPeriod, AccountingPeriod toPeriod) {
		return BeanUtils.filter(performanceList, performanceMetric -> (DateUtils.isDateBetween(performanceMetric.getAccountingPeriod().getStartDate(), fromPeriod.getStartDate(), toPeriod.getStartDate(), false)));
	}


	/**
	 * Removes all performance prior to specified date
	 */
	private List<PerformanceCompositeInvestmentAccountPerformance> filterAccountPerformancePriorToDate(List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceList, Date date) {
		Date compareTo = DateUtils.getFirstDayOfMonth(date);
		return BeanUtils.filter(accountPerformanceList, accountPerformance -> (DateUtils.isDateAfterOrEqual(accountPerformance.getAccountingPeriod().getStartDate(), compareTo)));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}


	public PerformanceCompositeMetricCalculator getPerformanceCompositeMetricCalculator() {
		return this.performanceCompositeMetricCalculator;
	}


	public void setPerformanceCompositeMetricCalculator(PerformanceCompositeMetricCalculator performanceCompositeMetricCalculator) {
		this.performanceCompositeMetricCalculator = performanceCompositeMetricCalculator;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
