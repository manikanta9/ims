package com.clifton.performance.composite.performance.rule;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeMetricContext;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PerformanceCompositeMetricBenchmarkReturnMissingRuleEvaluator</code> can be used for both composite account performance and composite performance ({@link PerformanceCompositeMetric} objects
 * to ensure if the benchmark security, secondary benchmark security, or risk free security is populated, then the period return value is also populated
 *
 * @author manderson
 */
public class PerformanceCompositeMetricBenchmarkReturnMissingRuleEvaluator<T extends PerformanceCompositeMetric> implements RuleEvaluator<T, PerformanceCompositeMetricContext<T>> {


	private PerformanceCompositeSetupService performanceCompositeSetupService;
	private RuleViolationService ruleViolationService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T performanceCompositeMetric, RuleConfig ruleConfig, PerformanceCompositeMetricContext<T> context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			InvestmentSecurity benchmarkSecurity = performanceCompositeMetric.getBenchmarkSecurity();
			InvestmentSecurity secondaryBenchmarkSecurity = performanceCompositeMetric.getSecondaryBenchmarkSecurity();
			InvestmentSecurity thirdBenchmarkSecurity = performanceCompositeMetric.getThirdBenchmarkSecurity();
			InvestmentSecurity riskFreeSecurity = performanceCompositeMetric.getRiskFreeSecurity();

			if (benchmarkSecurity != null && performanceCompositeMetric.getPeriodBenchmarkReturn() == null) {
				violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositeMetric.getId(), benchmarkSecurity.getId()));
			}
			if (secondaryBenchmarkSecurity != null && performanceCompositeMetric.getPeriodSecondaryBenchmarkReturn() == null) {
				violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositeMetric.getId(), secondaryBenchmarkSecurity.getId()));
			}
			if (thirdBenchmarkSecurity != null && performanceCompositeMetric.getPeriodThirdBenchmarkReturn() == null) {
				violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositeMetric.getId(), thirdBenchmarkSecurity.getId()));
			}
			if (riskFreeSecurity != null && performanceCompositeMetric.getPeriodRiskFreeSecurityReturn() == null) {
				violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositeMetric.getId(), riskFreeSecurity.getId()));
			}
		}
		return violationList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}
}
