package com.clifton.performance.composite.setup.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.performance.composite.setup.PerformanceCompositeRollup;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class PerformanceCompositeRollupCacheImpl extends SelfRegisteringSimpleDaoCache<PerformanceCompositeRollup, Short, Short[]> implements PerformanceCompositeRollupCache {


	@Override
	public Short[] getPerformanceCompositeRollupList(short performanceCompositeId) {
		return getCacheHandler().get(getCacheName(), performanceCompositeId);
	}


	@Override
	public void storePerformanceCompositeRollupList(short performanceCompositeId, List<PerformanceCompositeRollup> list) {
		Short[] ids = BeanUtils.getBeanIdentityArray(list, Short.class);
		getCacheHandler().put(getCacheName(), performanceCompositeId, ids);
	}


	private void clearPerformanceCompositeRollupCache(short performanceCompositeId) {
		getCacheHandler().remove(getCacheName(), performanceCompositeId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<PerformanceCompositeRollup> dao, DaoEventTypes event, PerformanceCompositeRollup bean, Throwable e) {
		if (e == null) {
			boolean clear = false;
			if (event.isUpdate()) {
				PerformanceCompositeRollup originalBean = getOriginalBean(dao, bean);
				if (originalBean != null && !CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(originalBean, bean, false))) {
					clear = true;
					// Clear the original reference's cache, not just the new one
					clearPerformanceCompositeRollupCache(originalBean.getReferenceOne().getId());
					clearPerformanceCompositeRollupCache(originalBean.getReferenceTwo().getId());
				}
			}
			if (clear || event.isInsert() || event.isDelete()) {
				clearPerformanceCompositeRollupCache(bean.getReferenceOne().getId());
				clearPerformanceCompositeRollupCache(bean.getReferenceTwo().getId());
			}
		}
	}


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<PerformanceCompositeRollup> dao, DaoEventTypes event, PerformanceCompositeRollup bean) {
		if (event.isUpdate()) {
			getOriginalBean(dao, bean);
		}
	}
}
