package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;


/**
 * The <code>PerformanceCompositeAccountCalculator</code> interface defines methods for calculating performance values
 *
 * @author apopp
 */
public interface PerformanceCalculator {


	/**
	 * Process performance fields defined by the provided config.  Results are populated within the config.
	 */
	public void calculate(PerformanceCompositeAccountRuleEvaluatorContext evaluatorContext, PerformanceCompositeInvestmentAccountPerformance accountPerformance);


	/**
	 * The above calculate method does this automatically.  In some cases, we need to rebuild just the benchmark returns
	 */
	public void calculateBenchmarkReturns(PerformanceCompositeAccountRuleEvaluatorContext evaluatorContext, PerformanceCompositeInvestmentAccountPerformance accountPerformance);
}
