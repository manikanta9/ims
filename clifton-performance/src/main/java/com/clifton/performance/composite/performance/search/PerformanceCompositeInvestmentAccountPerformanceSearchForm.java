package com.clifton.performance.composite.performance.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.performance.composite.performance.metric.search.BasePerformanceCompositeMetricSearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class PerformanceCompositeInvestmentAccountPerformanceSearchForm extends BasePerformanceCompositeMetricSearchForm {

	@SearchField(searchField = "performanceCompositeInvestmentAccount.id", sortField = "performanceCompositeInvestmentAccount.investmentAccount.number")
	private Integer performanceCompositeInvestmentAccountId;

	@SearchField(searchField = "performanceCompositeInvestmentAccount.id", sortField = "performanceCompositeInvestmentAccount.investmentAccount.number")
	private Integer[] performanceCompositeInvestmentAccountIds;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "investmentAccount.id", sortField = "investmentAccount.number")
	private Integer investmentAccountId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "investmentAccount.id", sortField = "investmentAccount.number")
	private Integer[] investmentAccountIds;

	@SearchField(searchField = "groupList.referenceOne.id", searchFieldPath = "performanceCompositeInvestmentAccount.investmentAccount", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer investmentAccountGroupId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "investmentAccount.number", comparisonConditions = {ComparisonConditions.EQUALS})
	private String investmentAccountNumber;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "investmentAccount.name")
	private String[] investmentAccountNames;

	@SearchField
	private BigDecimal periodTotalNetCashflow;

	@SearchField
	private BigDecimal periodWeightedNetCashflow;

	@SearchField
	private BigDecimal periodGainLoss;

	@SearchField(searchField = "positionsOnDate,performanceCompositeInvestmentAccount.investmentAccount.performanceInceptionDate,performanceCompositeInvestmentAccount.investmentAccount.inceptionDate", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Date coalescePositionsOnDate;

	@SearchField(searchField = "businessService.id", searchFieldPath = "performanceCompositeInvestmentAccount.investmentAccount")
	private Short businessServiceId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "performanceComposite.id", sortField = "performanceComposite.name")
	private Short performanceCompositeId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "performanceComposite.id", sortField = "performanceComposite.name")
	private Short[] performanceCompositeIds;

	// Custom: if selected composite is not a rollup will set the performanceCompositeId filter
	// otherwise get's it's children and sets performanceCompositeIds filter
	private Short performanceCompositeOrRollupId;

	// Only need 4 levels, because service component would be at max 5 and they can't be assigned to the account (use businessServiceComponentId filter for that)
	@SearchField(searchField = "performanceCompositeInvestmentAccount.investmentAccount.businessService.id,performanceCompositeInvestmentAccount.investmentAccount.businessService.parent.id,performanceCompositeInvestmentAccount.investmentAccount.businessService.parent.parent.id,performanceCompositeInvestmentAccount.investmentAccount.businessService.parent.parent.parent.id", leftJoin = true, sortField = "performanceCompositeInvestmentAccount.investmentAccount.businessService.name")
	private Short businessServiceOrParentId;

	@SearchField
	private Short documentFileCount;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "benchmarkSecurity.id,performanceComposite.benchmarkSecurity.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer benchmarkSecurityId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "secondaryBenchmarkSecurity.id,performanceComposite.secondaryBenchmarkSecurity.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer secondaryBenchmarkSecurityId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount", searchField = "thirdBenchmarkSecurity.id,performanceComposite.thirdBenchmarkSecurity.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private Integer thirdBenchmarkSecurityId;

	@SearchField(searchFieldPath = "performanceCompositeInvestmentAccount.performanceComposite", searchField = "riskFreeSecurity.id")
	private Integer riskFreeSecurityId;


	@SearchField(searchField = "performanceCompositeInvestmentAccount.accountPerformanceCalculatorBean.id,performanceCompositeInvestmentAccount.performanceComposite.accountPerformanceCalculatorBean.id", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
	private Integer accountPerformanceCalculatorBeanId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPerformanceCompositeInvestmentAccountId() {
		return this.performanceCompositeInvestmentAccountId;
	}


	public void setPerformanceCompositeInvestmentAccountId(Integer performanceCompositeInvestmentAccountId) {
		this.performanceCompositeInvestmentAccountId = performanceCompositeInvestmentAccountId;
	}


	public Integer getInvestmentAccountId() {
		return this.investmentAccountId;
	}


	public void setInvestmentAccountId(Integer investmentAccountId) {
		this.investmentAccountId = investmentAccountId;
	}


	public BigDecimal getPeriodTotalNetCashflow() {
		return this.periodTotalNetCashflow;
	}


	public void setPeriodTotalNetCashflow(BigDecimal periodTotalNetCashflow) {
		this.periodTotalNetCashflow = periodTotalNetCashflow;
	}


	public BigDecimal getPeriodWeightedNetCashflow() {
		return this.periodWeightedNetCashflow;
	}


	public void setPeriodWeightedNetCashflow(BigDecimal periodWeightedNetCashflow) {
		this.periodWeightedNetCashflow = periodWeightedNetCashflow;
	}


	public BigDecimal getPeriodGainLoss() {
		return this.periodGainLoss;
	}


	public void setPeriodGainLoss(BigDecimal periodGainLoss) {
		this.periodGainLoss = periodGainLoss;
	}


	public Integer getInvestmentAccountGroupId() {
		return this.investmentAccountGroupId;
	}


	public void setInvestmentAccountGroupId(Integer investmentAccountGroupId) {
		this.investmentAccountGroupId = investmentAccountGroupId;
	}


	public Short getPerformanceCompositeId() {
		return this.performanceCompositeId;
	}


	public void setPerformanceCompositeId(Short performanceCompositeId) {
		this.performanceCompositeId = performanceCompositeId;
	}


	public Short getBusinessServiceOrParentId() {
		return this.businessServiceOrParentId;
	}


	public void setBusinessServiceOrParentId(Short businessServiceOrParentId) {
		this.businessServiceOrParentId = businessServiceOrParentId;
	}


	public Short getBusinessServiceId() {
		return this.businessServiceId;
	}


	public void setBusinessServiceId(Short businessServiceId) {
		this.businessServiceId = businessServiceId;
	}


	public Date getCoalescePositionsOnDate() {
		return this.coalescePositionsOnDate;
	}


	public void setCoalescePositionsOnDate(Date coalescePositionsOnDate) {
		this.coalescePositionsOnDate = coalescePositionsOnDate;
	}


	public Short getDocumentFileCount() {
		return this.documentFileCount;
	}


	public void setDocumentFileCount(Short documentFileCount) {
		this.documentFileCount = documentFileCount;
	}


	public String[] getInvestmentAccountNames() {
		return this.investmentAccountNames;
	}


	public void setInvestmentAccountNames(String[] investmentAccountNames) {
		this.investmentAccountNames = investmentAccountNames;
	}


	public String getInvestmentAccountNumber() {
		return this.investmentAccountNumber;
	}


	public void setInvestmentAccountNumber(String investmentAccountNumber) {
		this.investmentAccountNumber = investmentAccountNumber;
	}


	public Short getPerformanceCompositeOrRollupId() {
		return this.performanceCompositeOrRollupId;
	}


	public void setPerformanceCompositeOrRollupId(Short performanceCompositeOrRollupId) {
		this.performanceCompositeOrRollupId = performanceCompositeOrRollupId;
	}


	public Short[] getPerformanceCompositeIds() {
		return this.performanceCompositeIds;
	}


	public void setPerformanceCompositeIds(Short[] performanceCompositeIds) {
		this.performanceCompositeIds = performanceCompositeIds;
	}


	@Override
	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	@Override
	public Integer getSecondaryBenchmarkSecurityId() {
		return this.secondaryBenchmarkSecurityId;
	}


	public void setSecondaryBenchmarkSecurityId(Integer secondaryBenchmarkSecurityId) {
		this.secondaryBenchmarkSecurityId = secondaryBenchmarkSecurityId;
	}


	@Override
	public Integer getThirdBenchmarkSecurityId() {
		return this.thirdBenchmarkSecurityId;
	}


	public void setThirdBenchmarkSecurityId(Integer thirdBenchmarkSecurityId) {
		this.thirdBenchmarkSecurityId = thirdBenchmarkSecurityId;
	}


	@Override
	public Integer getRiskFreeSecurityId() {
		return this.riskFreeSecurityId;
	}


	public void setRiskFreeSecurityId(Integer riskFreeSecurityId) {
		this.riskFreeSecurityId = riskFreeSecurityId;
	}


	public Integer[] getInvestmentAccountIds() {
		return this.investmentAccountIds;
	}


	public void setInvestmentAccountIds(Integer[] investmentAccountIds) {
		this.investmentAccountIds = investmentAccountIds;
	}


	public Integer[] getPerformanceCompositeInvestmentAccountIds() {
		return this.performanceCompositeInvestmentAccountIds;
	}


	public void setPerformanceCompositeInvestmentAccountIds(Integer[] performanceCompositeInvestmentAccountIds) {
		this.performanceCompositeInvestmentAccountIds = performanceCompositeInvestmentAccountIds;
	}


	public Integer getAccountPerformanceCalculatorBeanId() {
		return this.accountPerformanceCalculatorBeanId;
	}


	public void setAccountPerformanceCalculatorBeanId(Integer accountPerformanceCalculatorBeanId) {
		this.accountPerformanceCalculatorBeanId = accountPerformanceCalculatorBeanId;
	}
}
