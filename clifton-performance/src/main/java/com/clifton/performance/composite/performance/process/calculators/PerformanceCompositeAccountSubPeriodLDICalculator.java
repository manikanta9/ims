package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.trade.Trade;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountSubPeriodLDICalculator</code> is a method used for measuring an accounts return using various calculations based on the investment type. (i.e. Futures = Notional, Swaptions = ABS(Quantity)).
 * Returns can be periodically linked based on contract level changes - when the quantity of contracts change (so excludes Rolls) AND that change is 10% or greater.
 * This works the same as the PerformanceCompositeAccountSubPeriodModifiedDietzCalculator (linking at flows over 10%) except that:
 * 1. Positions are valued differently
 * 2. Cash Flows recorded are actually Trade Flows - not Cash
 * 3. Uses standard gain/loss calculation (not just change in position value)
 * <p>
 * (Gain/Loss)/(Beginning Value of Positions + Weighted Trade Flow (entered as Cash Flows))
 * <p>
 *
 * @author manderson
 */
public class PerformanceCompositeAccountSubPeriodLDICalculator extends PerformanceCompositeAccountSubPeriodModifiedDietzCalculator {

	private InvestmentCalculator investmentCalculator;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Overrides so calculator calls real gain/loss calculation - not just change in value
	 */
	@Override
	protected boolean isCalculateGainLossAsChangeInPositionValue() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Trade Flow is included for a date when all trades for the instrument on that date causes a change in quantity (i.e. excludes rolls or intraday trades that net to 0)
	 * If the trade opens a position - the trade flow is positive, else negative
	 */
	@Override
	protected void calculateDailyNetCashFlowValue(PerformanceCompositeAccountRuleEvaluatorContext context, Date date) {
		BigDecimal totalTradeFlow = BigDecimal.ZERO;
		List<Trade> tradeList = context.getTradeListForDate(date, getIncludePositionInvestmentGroupId(), getExcludePositionInvestmentGroupId());
		if (!CollectionUtils.isEmpty(tradeList)) {
			Map<Integer, List<Trade>> instrumentTradeListMap = BeanUtils.getBeansMap(tradeList, trade -> trade.getInvestmentSecurity().getInstrument().getId());
			for (Map.Entry<Integer, List<Trade>> integerListEntry : instrumentTradeListMap.entrySet()) {
				BigDecimal instrumentChange = CoreMathUtils.sumProperty(integerListEntry.getValue(), trade -> trade.isBuy() ? trade.getQuantity() : MathUtils.negate(trade.getQuantity()));
				if (!MathUtils.isNullOrZero(instrumentChange)) {
					BigDecimal tradeFlow = CoreMathUtils.sumProperty(integerListEntry.getValue(), trade -> calculateTradeImpact(context, trade));
					totalTradeFlow = MathUtils.add(totalTradeFlow, tradeFlow);
				}
			}
		}
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = context.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(date);
		dailyPerformance.setNetCashFlow(totalTradeFlow);
		dailyPerformance.setWeightedNetCashFlow(calculateDailyWeightedNetCashFlowValue(date, totalTradeFlow));
	}


	private BigDecimal calculateTradeImpact(PerformanceCompositeAccountRuleEvaluatorContext config, Trade trade) {
		BigDecimal tradeQuantity = (trade.isBuy() ? trade.getQuantity() : MathUtils.negate(trade.getQuantity()));
		BigDecimal priorQuantity = CoreMathUtils.sumProperty(BeanUtils.filter(config.getPositionListForDate(trade.getTradeDate()), accountingPositionDaily -> accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getId(), trade.getInvestmentSecurity().getId()), AccountingPositionDaily::getPriorQuantity);
		boolean openingPosition = isTradeOpeningPosition(priorQuantity, tradeQuantity);
		BigDecimal tradeImpact;
		// Swaptions: Quantity
		if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.SWAPTIONS)) {
			tradeImpact = tradeQuantity;
		}
		// Swaps: ABS(Quantity) + Accrued Interest + Notional
		else if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.SWAPS)) {
			tradeImpact = MathUtils.add(MathUtils.add(MathUtils.abs(tradeQuantity), trade.getAccrualAmount()), trade.getAccountingNotional());
		}
		// Bonds: Notional + Accrued Interest
		else if (InvestmentUtils.isSecurityOfType(trade.getInvestmentSecurity(), InvestmentType.BONDS)) {
			tradeImpact = MathUtils.add(trade.getAccountingNotional(), trade.getAccrualAmount());
		}
		else {
			// Futures: Notional
			tradeImpact = trade.getAccountingNotional();
		}
		// If we are opening the position then trade flow should be positive - closing the position, then negative.
		tradeImpact = MathUtils.abs(tradeImpact);
		if (!openingPosition) {
			tradeImpact = MathUtils.negate(tradeImpact);
		}
		return tradeImpact;
	}


	private boolean isTradeOpeningPosition(BigDecimal priorQuantity, BigDecimal tradeQuantity) {
		if (MathUtils.isNullOrZero(priorQuantity)) {
			return true;
		}
		// Both Negative or Both Positive - Opening
		if (MathUtils.isLessThan(priorQuantity, BigDecimal.ZERO) == MathUtils.isLessThan(tradeQuantity, BigDecimal.ZERO)) {
			return true;
		}
		// Opposite Signs - then only opening if trade quantity is larger than prior quantity
		// May potentially need a partial calculation for this - no examples yet of this for LDI accounts
		if (MathUtils.isLessThan(priorQuantity, BigDecimal.ZERO) != MathUtils.isLessThan(tradeQuantity, BigDecimal.ZERO) && MathUtils.isGreaterThan(MathUtils.abs(tradeQuantity), MathUtils.abs(priorQuantity))) {
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void calculateDailyPositionValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		List<AccountingPositionDaily> positionList = config.getPositionListForDate(dateToCalculate, getIncludePositionInvestmentGroupId(), getExcludePositionInvestmentGroupId());
		BigDecimal positionValue = CoreMathUtils.sumProperty(positionList, this::calculatePositionValue);

		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		dailyPerformance.setPositionValue(MathUtils.abs(positionValue));
	}


	private BigDecimal calculatePositionValue(AccountingPositionDaily accountingPositionDaily) {
		if (MathUtils.isNullOrZero(accountingPositionDaily.getRemainingQuantity())) {
			return BigDecimal.ZERO;
		}
		// Swaptions: ABS(Quantity)
		if (InvestmentUtils.isSecurityOfType(accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), InvestmentType.SWAPTIONS)) {
			return MathUtils.abs(accountingPositionDaily.getRemainingQuantity());
		}
		// Swaps: ABS(Quantity) + Market Value
		else if (InvestmentUtils.isSecurityOfType(accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), InvestmentType.SWAPS)) {
			return MathUtils.add(MathUtils.abs(accountingPositionDaily.getRemainingQuantity()), accountingPositionDaily.getMarketValueBase());
		}
		// Bonds: Market Value
		else if (InvestmentUtils.isSecurityOfType(accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity(), InvestmentType.BONDS)) {
			// Note: Market Value Already Includes the Accrual
			return accountingPositionDaily.getMarketValueBase();
		}
		// Futures: Notional
		return accountingPositionDaily.getNotionalValueBase();
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////            Getter and Setter Methods               //////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}
}
