package com.clifton.performance.composite.performance.metric;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeMetricContext;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class PerformanceCompositeMetricCalculatorImpl implements PerformanceCompositeMetricCalculator {


	private MarketDataRetriever marketDataRetriever;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends PerformanceCompositeMetric> void calculatePerformanceMetric(PerformanceCompositeMetricCalculatorCommand<T> calculatorCommand, PerformanceCompositeMetricContext<T> performanceCompositeMetricContext) {
		// If there is a break in performance we need to reset the inception date dynamically.
		// if we do this we want to keep track that we did it. So we can process accordingly
		Date inceptionDateOverride;

		// Determine index of user provided fromDate
		Integer startIndex = calculatorCommand.getPerformanceCompositeMetricStartIndex();
		Integer endIndex = calculatorCommand.getPerformanceCompositeMetricEndIndex();

		// For each performance metric entity
		for (int i = startIndex; i <= endIndex; i++) {
			// If there was no performance during the previous month, then it's a break
			T currentMetric = calculatorCommand.getPerformanceCompositeMetricList().get(i);
			inceptionDateOverride = calculatorCommand.getPerformanceInceptionDateOverride(i);

			if (calculatorCommand.getProcessingType().isCalculateAllMetricReturns()) {
				// Calculate Model Net
				calculateAccountModelNetReturn(currentMetric, performanceCompositeMetricContext);
			}
			if (calculatorCommand.getProcessingType().isCalculateBenchmarkReturns()) {
				// Populate Period Benchmark Returns
				calculatePeriodBenchmarkReturns(currentMetric, performanceCompositeMetricContext);
			}

			//Find all fields that are relevant for processing
			List<PerformanceMetricFieldTypes> fieldTypesForProcessingList = filterPassiveAndUnusedFieldTypes(currentMetric, calculatorCommand.getProcessingType());

			//Calculate each calculation type for performance entity
			for (PerformanceMetricFieldTypes fieldType : fieldTypesForProcessingList) {
				//This is the base value all other calculations are based off of... don't perform calculation if it's null
				Object dependentValue = BeanUtils.getPropertyValue(currentMetric, fieldType.getDependentFieldType().getBeanPropertyName());

				Date currentFromDate = currentMetric.getAccountingPeriod().getStartDate();
				boolean isTypeValidForInceptionDate = fieldType.isTypeValidForInceptionDate(inceptionDateOverride, currentFromDate);

				//If we are updating the metric let's clear it's value to start with to ensure it is sanitized
				BeanUtils.setPropertyValue(currentMetric, fieldType.getBeanPropertyName(), null);

				//Only perform a calculation type if it's applicable date range is valid for the inception date of the account and
				if (dependentValue != null && isTypeValidForInceptionDate && fieldType.getPerformanceMetricCalculatorTypes() != null) {
					calculatePerformanceMetricByPerformanceListAndCalculationType(currentMetric, calculatorCommand.getPerformanceCompositeMetricList(), fieldType, i,
							inceptionDateOverride);
				}
			}
		}
	}


	private <T extends PerformanceCompositeMetric> void calculateAccountModelNetReturn(T performanceCompositeMetric, PerformanceCompositeMetricContext<T> performanceCompositeMetricContext) {
		// Account Level Only. For Composite Level - Do Nothing
		// This is here because those that use external account calculators, the system will still calculate the model net automatically and we don't need to rely on users uploading that data
		if (performanceCompositeMetricContext.isModelFeeCalculatedFromGross(performanceCompositeMetric)) {
			BigDecimal modelReturn = performanceCompositeMetric.getPeriodGrossReturn();
			BigDecimal modelFee = performanceCompositeMetricContext.getModelFee(performanceCompositeMetric);
			if (!MathUtils.isNullOrZero(modelFee)) {
				modelReturn = MathUtils.modelFeeReturn(modelFee, modelReturn);
			}
			performanceCompositeMetric.setPeriodModelNetReturn(modelReturn);
		}
	}


	private <T extends PerformanceCompositeMetric> void calculatePeriodBenchmarkReturns(T performanceCompositeMetric, PerformanceCompositeMetricContext<T> performanceCompositeMetricContext) {
		// Benchmark Returns: Last Day of Previous Month - Last Day of Current Month for MTD Return
		Date startDate = ObjectUtils.coalesce(performanceCompositeMetricContext.getBenchmarkStartDateOverride(performanceCompositeMetric), DateUtils.addDays(performanceCompositeMetric.getAccountingPeriod().getStartDate(), -1));
		Date endDate = performanceCompositeMetric.getAccountingPeriod().getEndDate();
		// Benchmark Returns:  Yesterday's Date when in current accounting period - for MTD Return
		if (DateUtils.isDateBetween(new Date(), performanceCompositeMetric.getAccountingPeriod().getStartDate(), performanceCompositeMetric.getAccountingPeriod().getEndDate(), false)) {
			endDate = DateUtils.addDays(DateUtils.clearTime(new Date()), -1);
		}
		// Period Benchmark Return
		calculateBenchmarkField(performanceCompositeMetric, performanceCompositeMetric.getBenchmarkSecurity(), PerformanceMetricFieldTypes.PERIOD_BENCHMARK_RETURN, startDate, endDate);
		// Period Secondary Benchmark Return
		calculateBenchmarkField(performanceCompositeMetric, performanceCompositeMetric.getSecondaryBenchmarkSecurity(), PerformanceMetricFieldTypes.PERIOD_SECONDARY_BENCHMARK_RETURN, startDate, endDate);
		// Period Third Benchmark Return
		calculateBenchmarkField(performanceCompositeMetric, performanceCompositeMetric.getThirdBenchmarkSecurity(), PerformanceMetricFieldTypes.PERIOD_THIRD_BENCHMARK_RETURN, startDate, endDate);
		// Period Risk Free Security Return
		calculateBenchmarkField(performanceCompositeMetric, performanceCompositeMetric.getRiskFreeSecurity(), PerformanceMetricFieldTypes.PERIOD_RISK_FREE_SECURITY_RETURN, startDate, endDate);
	}


	private void calculateBenchmarkField(PerformanceCompositeMetric metric, InvestmentSecurity benchmarkSecurity, PerformanceMetricFieldTypes fieldType, Date startDate, Date endDate) {
		BigDecimal benchmarkReturn = null;
		if (benchmarkSecurity != null) {
			benchmarkReturn = getMarketDataRetriever().getInvestmentSecurityReturn(null, benchmarkSecurity, startDate, endDate, "Performance Benchmark", true);
		}
		// Need to always set it, even if null because if the benchmark security is removed we need to clear the return
		BeanUtils.setPropertyValue(metric, fieldType.getBeanPropertyName(), benchmarkReturn);
	}


	/**
	 * Filter out all passive and unused fields (Fields that do not exist on this entity
	 * All passive fields are critical. If any of them are missing then we will return no fields for processing.
	 * A passive field can only be missing if there was a break in performance which would mean we don't want to continue processing
	 */
	private List<PerformanceMetricFieldTypes> filterPassiveAndUnusedFieldTypes(PerformanceCompositeMetric metric, PerformanceCompositeMetricProcessingTypes processingType) {
		List<PerformanceMetricFieldTypes> fieldTypeList = new ArrayList<>();

		for (PerformanceMetricFieldTypes fieldType : PerformanceMetricFieldTypes.values()) {
			if (fieldType.isPassiveCalculation() && !fieldType.isBenchmarkReturn() && processingType.isProcessPerformanceMetricFieldType(fieldType)) {
				// This is the base value all other calculations are based off of... don't perform calculation if it's null
				Object beanPropertyPeriodValue = BeanUtils.getPropertyValue(metric, fieldType.getBeanPropertyName());

				//One of the base passive fields has not been set. This means that there must have been a break in performance so we will return no field types to process
				if (beanPropertyPeriodValue == null) {
					fieldTypeList.clear();
					return fieldTypeList;
				}
			}

			// If the field exists on this entity and we should calculate it, then add it for processing
			if (!fieldType.isPassiveCalculation() && BeanUtils.isPropertyPresent(metric.getClass(), fieldType.getBeanPropertyName()) && processingType.isProcessPerformanceMetricFieldType(fieldType)) {
				fieldTypeList.add(fieldType);
			}
		}
		return fieldTypeList;
	}


	private <T extends PerformanceCompositeMetric> void calculatePerformanceMetricByPerformanceListAndCalculationType(T currentMetric, List<T> performanceMetricList,
	                                                                                                                  PerformanceMetricFieldTypes performanceMetricFieldType, int currentMetricIndex, Date inceptionAdjustmentDate) {

		//Create a subList of entities that are applicable to this calculation
		List<T> performanceMetricSubList = performanceMetricFieldType.getPerformanceMetricSubList(performanceMetricList, currentMetricIndex, inceptionAdjustmentDate);

		BigDecimal value = performanceMetricFieldType.getPerformanceMetricCalculatorTypes().calculateValue(performanceMetricFieldType, performanceMetricSubList);

		//Calculation is complete, set the value on the account performance entity
		BeanUtils.setPropertyValue(currentMetric, performanceMetricFieldType.getBeanPropertyName(), value);
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////            Getter and Setter Methods               ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
