package com.clifton.performance.composite.performance.metric.search;


import org.springframework.util.StringUtils;


/**
 * The <code>PerformanceFrequencies</code> enum defines various frequencies you want
 * to view performance over.
 *
 * @author apopp
 */
public enum PerformanceFrequencies {

	QUARTERLY_RETURN(3, 6, 9, 12), //
	ANNUAL_RETURN(12);

	////////////////////////////////////////////////////////////////////////////////

	private final Integer[] months;

	////////////////////////////////////////////////////////////////////////////////


	PerformanceFrequencies(Integer... months) {
		this.months = months;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getMonthsAsCommaDelimitedString() {
		return StringUtils.arrayToCommaDelimitedString(getMonths());
	}

	////////////////////////////////////////////////////////////////////////////////


	public Integer[] getMonths() {
		return this.months;
	}
}
