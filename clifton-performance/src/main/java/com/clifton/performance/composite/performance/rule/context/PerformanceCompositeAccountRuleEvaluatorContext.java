package com.clifton.performance.composite.performance.rule.context;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.account.securitytarget.search.InvestmentAccountSecurityTargetSearchForm;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.performance.account.rule.PerformanceAccountInfoRuleEvaluatorContext;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeSearchForm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountRuleEvaluatorContext</code> allows for optimized rule evaluation by caching and helper methods.
 *
 * @author apopp
 */
public class PerformanceCompositeAccountRuleEvaluatorContext extends PerformanceAccountInfoRuleEvaluatorContext implements PerformanceCompositeMetricContext<PerformanceCompositeInvestmentAccountPerformance> {

	private static final String EXCLUDE_MODEL_FEE_ACCOUNT_GROUP_NAME = "Performance: Exclude Model Fee Accounts";

	public static final String RULE_MANAGEMENT_FEE_OVERRIDE_VIOLATION = "Management Fee Override Violations";
	public static final String RULE_PREVIOUS_MONTH_END_GAIN_LOSS_ADJUSTMENT = "Previous Month End Position Value Gain Loss Adjustment";
	public static final String RULE_CURRENT_MONTH_END_PARTIAL_PERFORMANCE_DATA = "Account Performance: Partial Performance Values";

	private static final String PERFORMANCE_COMPOSITE_ACCOUNT_PERFORMANCE_PREVIOUS_LIST = "PERFORMANCE_COMPOSITE_ACCOUNT_PERFORMANCE_PREVIOUS_LIST";
	private static final String OPTION_TRADE_DATE_QUANTITY_MAP = "OPTION_TRADE_DATE_QUANTITY_MAP";
	private static final String SECURITY_TARGET_LIST = "SECURITY_TARGET_LIST";

	/**
	 * Used for cases where net return calc is more complicated (Security Targets) so it's calculated when last day's gross is calculated, but stored here for use when monthly net is calculated
	 */
	private static final String LAST_DAY_NET_RETURN = "LAST_DAY_NET_RETURN";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentAccountGroupService investmentAccountGroupService;
	private InvestmentGroupService investmentGroupService;
	private InvestmentSetupService investmentSetupService;

	private TradeService tradeService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	// Note: For Easier Retrieval During Processing, these are stored as individual properties instead of as generic objects in the context

	private PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance;

	private Map<Date, PerformanceCompositeInvestmentAccountDailyPerformance> performanceCompositeInvestmentAccountDailyPerformanceMap;

	// Rule Violation Note Map of System Defined Definitions and the Violation to be Saved
	private Map<String, String> systemDefinedRuleViolationNoteMap;

	private Map<String, BigDecimal> cashValueDailyMap = null;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformance() {
		if (this.performanceCompositeInvestmentAccountPerformance == null) {
			throw new ValidationException("Performance Composite Investment Account Performance Bean wasn't set in the context when it was set up.");
		}
		return this.performanceCompositeInvestmentAccountPerformance;
	}


	public InvestmentAccount getInvestmentAccount() {
		return getPerformanceCompositeInvestmentAccountPerformance().getClientAccount();
	}


	public AccountingPeriod getAccountingPeriod() {
		return getPerformanceCompositeInvestmentAccountPerformance().getAccountingPeriod();
	}

	////////////////////////////////////////////////////////////////////////////////
	/////    PerformanceCompositeInvestmentAccountDailyPerformance Methods     /////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccountDailyPerformance getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(Date dayToCalculate) {
		PerformanceCompositeInvestmentAccountDailyPerformance performance = getPerformanceCompositeInvestmentAccountDailyPerformanceMap().get(dayToCalculate);

		if (performance == null) {
			//No performance found for this day so create it, populate basic values, add to list, and return it
			performance = new PerformanceCompositeInvestmentAccountDailyPerformance(getPerformanceCompositeInvestmentAccountPerformance(), dayToCalculate);
			getPerformanceCompositeInvestmentAccountDailyPerformanceMap().put(dayToCalculate, performance);
		}
		return performance;
	}


	public PerformanceCompositeInvestmentAccountDailyPerformance getPerformanceCompositeInvestmentAccountDailyPerformanceOnDate(Date date) {
		return getPerformanceCompositeInvestmentAccountDailyPerformanceMap().get(date);
	}


	public Map<Date, PerformanceCompositeInvestmentAccountDailyPerformance> getPerformanceCompositeInvestmentAccountDailyPerformanceMap() {
		if (this.performanceCompositeInvestmentAccountDailyPerformanceMap == null) {
			this.performanceCompositeInvestmentAccountDailyPerformanceMap = new HashMap<>();
		}
		return this.performanceCompositeInvestmentAccountDailyPerformanceMap;
	}


	/**
	 * This method is used during processing, since the account performance object is set in the context
	 */
	public List<PerformanceCompositeInvestmentAccountDailyPerformance> getPerformanceCompositeInvestmentAccountDailyPerformanceList() {
		return getPerformanceCompositeInvestmentAccountDailyPerformanceList(getPerformanceCompositeInvestmentAccountPerformance());
	}


	/**
	 * This method is used by the rule evaluation, since the account performance object is not set in the context
	 */
	public List<PerformanceCompositeInvestmentAccountDailyPerformance> getPerformanceCompositeInvestmentAccountDailyPerformanceList(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList = new ArrayList<>();
		if (this.performanceCompositeInvestmentAccountDailyPerformanceMap == null) {
			// If map is NULL - then never retrieved - When retrieving for rules - data is not pre-populated, so we need to retrieve the list if not a new bean and not set
			if (accountPerformance != null && !accountPerformance.isNewBean()) {
				dailyPerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(accountPerformance.getId());
				setPerformanceCompositeInvestmentAccountDailyPerformanceList(dailyPerformanceList, false, false);
			}
		}
		else {
			dailyPerformanceList.addAll(getPerformanceCompositeInvestmentAccountDailyPerformanceMap().values());
		}
		return BeanUtils.sortWithFunction(dailyPerformanceList, PerformanceCompositeInvestmentAccountDailyPerformance::getMeasureDate, true);
	}


	public void setPerformanceCompositeInvestmentAccountDailyPerformanceList(List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList, boolean resetFieldsForProcessing, boolean previewIgnoreOverrides) {
		for (PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance : CollectionUtils.getIterable(dailyPerformanceList)) {
			if (resetFieldsForProcessing) {
				dailyPerformance.resetFieldsForProcessing();
			}
			if (previewIgnoreOverrides) {
				dailyPerformance.setPositionValueOverride(null);
				dailyPerformance.setGainLossOverride(null);
				dailyPerformance.setNetCashFlowOverride(null);
				dailyPerformance.setWeightedNetCashFlowOverride(null);
			}
			getPerformanceCompositeInvestmentAccountDailyPerformanceMap().put(dailyPerformance.getMeasureDate(), dailyPerformance);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////            System Defined Rule Violation Helpers           //////////
	////////////////////////////////////////////////////////////////////////////////


	public Map<String, String> getSystemDefinedRuleViolationNoteMap() {
		if (this.systemDefinedRuleViolationNoteMap == null) {
			this.systemDefinedRuleViolationNoteMap = new HashMap<>();
			this.systemDefinedRuleViolationNoteMap.put(RULE_PREVIOUS_MONTH_END_GAIN_LOSS_ADJUSTMENT, null);
		}
		return this.systemDefinedRuleViolationNoteMap;
	}


	public void addSystemDefinedRuleViolationNote(String definitionName, String message) {
		getSystemDefinedRuleViolationNoteMap().put(definitionName, message);
	}

	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccountPerformance getPreviousPerformanceCompositeInvestmentAccountPerformance() {
		return CollectionUtils.getFirstElement(BeanUtils.filter(getPreviousPerformanceCompositeMetricList(getPerformanceCompositeInvestmentAccountPerformance()), accountPerformance -> DateUtils.isEqualWithoutTime(accountPerformance.getAccountingPeriod().getEndDate(), DateUtils.addDays(getAccountingPeriod().getStartDate(), -1))));
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////                  Position Helper Methods                    //////////
	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getPreviousPositionValueFromDateFlexible(Date date) {
		BigDecimal positionValue;
		Date processDate = date;
		Date maxPreviousDate = DateUtils.addDays(getAccountingPeriod().getStartDate(), -1);
		do {
			positionValue = getPreviousPositionValueFromDate(processDate);
			processDate = DateUtils.addDays(processDate, -1);
		}
		while (positionValue == null && DateUtils.compare(processDate, maxPreviousDate, false) > 0);

		return positionValue;
	}


	private BigDecimal getPreviousPositionValueFromDate(Date date) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = getPerformanceCompositeInvestmentAccountDailyPerformanceOnDate(DateUtils.addDays(date, -1));
		return (dailyPerformance != null) ? (dailyPerformance.getCoalescePositionValuePositionValueOverride()) : null;
	}

	////////////////////////////////////////////////////////////////////////////////


	public List<AccountingPositionDaily> getPositionListForDate(Date date) {
		return getPositionListForDate(date, null, null);
	}


	public List<AccountingPositionDaily> getPositionListForDate(Date date, Short includeInvestmentGroupId, Short excludeInvestmentGroupId) {
		List<AccountingPositionDaily> positionList = getInvestmentAccountPositionListForDate(getPerformanceCompositeInvestmentAccountPerformance(), date);

		if (includeInvestmentGroupId != null) {
			InvestmentGroup includeGroup = getInvestmentGroup(includeInvestmentGroupId);
			positionList = BeanUtils.filter(positionList, accountingPositionDaily -> getInvestmentGroupService().isInvestmentInstrumentInGroup(includeGroup.getName(), accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getInstrument().getId()));
		}
		if (excludeInvestmentGroupId != null) {
			InvestmentGroup excludeGroup = getInvestmentGroup(excludeInvestmentGroupId);
			positionList = BeanUtils.filter(positionList, accountingPositionDaily -> !getInvestmentGroupService().isInvestmentInstrumentInGroup(excludeGroup.getName(), accountingPositionDaily.getAccountingTransaction().getInvestmentSecurity().getInstrument().getId()));
		}
		return positionList;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////                    Trade Helper Methods                     //////////
	////////////////////////////////////////////////////////////////////////////////


	public List<Trade> getTradeListForDate(Date date) {
		return getTradeListForDate(date, null, null);
	}


	public List<Trade> getTradeListForDate(Date date, Short includeInvestmentGroupId, Short excludeInvestmentGroupId) {
		String key = "TRADE_LIST_MAP_" + (includeInvestmentGroupId == null ? "ALL" : includeInvestmentGroupId + "");

		Map<Date, List<Trade>> tradeListMap = getContext().getOrSupplyBean(key, () -> {
			TradeSearchForm searchForm = new TradeSearchForm();
			searchForm.setClientInvestmentAccountId(getInvestmentAccount().getId());
			searchForm.setInvestmentGroupId(includeInvestmentGroupId);
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, getAccountingPeriod().getStartDate()));
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, getAccountingPeriod().getEndDate()));
			searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
			List<Trade> tradeList = getTradeService().getTradeList(searchForm);
			return BeanUtils.getBeansMap(tradeList, Trade::getTradeDate);
		}, HashMap<Date, List<Trade>>::new);

		List<Trade> dateTradeList = tradeListMap.get(date);
		if (excludeInvestmentGroupId != null && !CollectionUtils.isEmpty(dateTradeList)) {
			InvestmentGroup excludeGroup = getInvestmentGroup(excludeInvestmentGroupId);
			dateTradeList = BeanUtils.filter(dateTradeList, trade -> !getInvestmentGroupService().isInvestmentInstrumentInGroup(excludeGroup.getName(), trade.getInvestmentSecurity().getInstrument().getId()));
		}
		return dateTradeList;
	}


	public Map<Date, BigDecimal> getOptionTradeDateQuantityMap() {
		return getContext().getOrSupplyBean(OPTION_TRADE_DATE_QUANTITY_MAP, () -> {
			AccountingPeriod accountingPeriod = getAccountingPeriod();
			InvestmentAccount account = getInvestmentAccount();

			TradeSearchForm searchForm = new TradeSearchForm();
			searchForm.setClientInvestmentAccountId(account.getId());
			searchForm.setInvestmentTypeId(getInvestmentSetupService().getInvestmentTypeByName(InvestmentType.OPTIONS).getId());
			// In case month end falls on a weekend and there were Friday and Monday trades - get trades from previous business day of month start to next day after month end
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(accountingPeriod.getStartDate()), -1)));
			searchForm.addSearchRestriction(new SearchRestriction("tradeDate", ComparisonConditions.LESS_THAN_OR_EQUALS, getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(accountingPeriod.getEndDate()), 1)));
			searchForm.setExcludeWorkflowStateName(TradeService.TRADES_CANCELLED_STATE_NAME);
			List<Trade> tradeList = getTradeService().getTradeList(searchForm);

			Map<Date, BigDecimal> tradeDateQuantityMap = new HashMap<>();
			for (Trade trade : CollectionUtils.getIterable(tradeList)) {
				BigDecimal existingQuantity = tradeDateQuantityMap.get(trade.getTradeDate());
				existingQuantity = MathUtils.add(existingQuantity, (trade.isBuy() ? trade.getQuantity() : MathUtils.negate(trade.getQuantity())));
				tradeDateQuantityMap.put(trade.getTradeDate(), existingQuantity);
			}

			for (Map.Entry<Date, BigDecimal> dateBigDecimalEntry : tradeDateQuantityMap.entrySet()) {
				// Move Quantities Backwards to the first of consecutive trading days
				Date firstTradeDate = getFirstConsecutiveTradeDate(dateBigDecimalEntry.getKey(), tradeDateQuantityMap.keySet());
				if (!DateUtils.isEqualWithoutTime(dateBigDecimalEntry.getKey(), firstTradeDate)) {
					tradeDateQuantityMap.put(firstTradeDate, MathUtils.add(dateBigDecimalEntry.getValue(), tradeDateQuantityMap.get(firstTradeDate)));
					tradeDateQuantityMap.put(dateBigDecimalEntry.getKey(), BigDecimal.ZERO);
				}
			}
			return tradeDateQuantityMap;
		});
	}


	public Date getFirstConsecutiveTradeDate(Date tradeDate, Collection<Date> tradeDateList) {
		Date previousDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(tradeDate), -1);
		if (tradeDateList.contains(previousDay)) {
			return getFirstConsecutiveTradeDate(previousDay, tradeDateList);
		}
		return tradeDate;
	}


	public Date getLastConsecutiveTradeDate(Date tradeDate, Collection<Date> tradeDateList) {
		Date nextDay = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(tradeDate), 1);
		if (tradeDateList.contains(nextDay)) {
			return getLastConsecutiveTradeDate(nextDay, tradeDateList);
		}
		return tradeDate;
	}


	public List<InvestmentAccountSecurityTarget> getAccountSecurityTargetList(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		return getContext().getOrSupplyBean(SECURITY_TARGET_LIST, () -> {
			AccountingPeriod accountingPeriod = getAccountingPeriod();
			InvestmentAccount account = getInvestmentAccount();
			InvestmentAccountSecurityTargetSearchForm searchForm = new InvestmentAccountSecurityTargetSearchForm();
			searchForm.setClientInvestmentAccountId(account.getId());
			// Pull All Active during the period (start on last day of previous month)
			searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, accountingPeriod.getEndDate()));
			searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL, DateUtils.getLastDayOfPreviousMonth(accountingPeriod.getStartDate())));
			return investmentAccountSecurityTargetService.getInvestmentAccountSecurityTargetList(searchForm);
		});
	}

	////////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCashValueForDate(Date date, boolean includeAllNonPositionAssetsAndLiabilities) {
		if (this.cashValueDailyMap == null) {
			this.cashValueDailyMap = getPerformanceInvestmentAccountDataRetriever().getCashValueDailyMapForPerformanceAccount(getInvestmentAccount(), getAccountingPeriod(), true, includeAllNonPositionAssetsAndLiabilities);
		}
		return this.cashValueDailyMap != null ? this.cashValueDailyMap.get(DateUtils.fromDateShort(date)) : null;
	}


	////////////////////////////////////////////////////////////////////////////////


	public InvestmentGroup getInvestmentGroup(Short investmentGroupId) {
		return getContext().getOrSupplyBean("INVESTMENT_GROUP_" + investmentGroupId, () -> getInvestmentGroupService().getInvestmentGroup(investmentGroupId));
	}


	////////////////////////////////////////////////////////////////////////////////


	public void setLastDayNetReturn(BigDecimal lastDayNetReturn) {
		getContext().setBean(LAST_DAY_NET_RETURN, lastDayNetReturn);
	}


	public BigDecimal getLastDayNetReturn() {
		return (BigDecimal) getContext().getBean(LAST_DAY_NET_RETURN);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////            Performance Composite Metric Methods           ///////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getBenchmarkStartDateOverride(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		if (accountPerformance.getPositionsOnDate() != null) {
			return accountPerformance.getPositionsOnDate();
		}
		return null;
	}


	@Override
	public boolean isModelFeeCalculatedFromGross(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		return true;
	}


	/**
	 * Includes additional check if the account is in a group that should NOT apply the model fee to it
	 */
	@Override
	public BigDecimal getModelFee(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		// Don't look it up if the account should exclude the model fee (i.e. seed accounts not funded by us)
		if (!getInvestmentAccountGroupService().isInvestmentAccountInGroup(accountPerformance.getClientAccount().getId(), EXCLUDE_MODEL_FEE_ACCOUNT_GROUP_NAME)) {
			return getPerformanceCompositeSetupService().getModelFeeForPerformanceCompositeAndDate(accountPerformance.getPerformanceCompositeInvestmentAccount().getPerformanceComposite(), accountPerformance.getAccountingPeriod().getStartDate());
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PerformanceCompositeInvestmentAccountPerformance> getPreviousPerformanceCompositeMetricList(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		return getContext().getOrSupplyBean(PERFORMANCE_COMPOSITE_ACCOUNT_PERFORMANCE_PREVIOUS_LIST, () -> {
			PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
			// Historical Account Performance - Include same account and same calculator
			searchForm.setInvestmentAccountId(accountPerformance.getClientAccount().getId());
			searchForm.setAccountPerformanceCalculatorBeanId(accountPerformance.getPerformanceCompositeInvestmentAccount().getCoalesceAccountPerformanceCalculatorBean().getId());
			searchForm.setBeforePeriodEndDate(accountPerformance.getAccountingPeriod().getStartDate());
			return getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);
		}, ArrayList::new);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////           Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public void setPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance) {
		this.performanceCompositeInvestmentAccountPerformance = performanceCompositeInvestmentAccountPerformance;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}
}
