package com.clifton.performance.composite.setup;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceCompositeHistory</code> class is used to track the value of the {@link PerformanceComposite} field for a given time period.
 * <p>
 * Current use case is the Model Fee value
 *
 * @author manderson
 */
public class PerformanceCompositeHistory extends BaseEntity<Short> {


	private PerformanceComposite performanceComposite;

	private Date startDate;

	private Date endDate;

	private BigDecimal modelFee;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isActiveOnDate(Date date) {
		return DateUtils.isDateBetween(date, getStartDate(), getEndDate(), false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceComposite getPerformanceComposite() {
		return this.performanceComposite;
	}


	public void setPerformanceComposite(PerformanceComposite performanceComposite) {
		this.performanceComposite = performanceComposite;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public BigDecimal getModelFee() {
		return this.modelFee;
	}


	public void setModelFee(BigDecimal modelFee) {
		this.modelFee = modelFee;
	}
}
