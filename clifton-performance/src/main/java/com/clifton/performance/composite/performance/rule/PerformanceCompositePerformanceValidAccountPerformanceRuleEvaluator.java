package com.clifton.performance.composite.performance.rule;


import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.workflow.definition.WorkflowStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PerformanceCompositePerformanceValidAccountPerformanceRuleEvaluator extends BaseRuleEvaluator<PerformanceCompositePerformance, PerformanceCompositeRuleEvaluatorContext> {

	@Override
	public List<RuleViolation> evaluateRule(PerformanceCompositePerformance performanceCompositePerformance, RuleConfig ruleConfig, PerformanceCompositeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformanceCompositeInvestmentAccountPerformance> accountPerformances = context.getPerformanceCompositeInvestmentAccountPerformanceList(performanceCompositePerformance);
			List<String> violationNotes = new ArrayList<>();
			for (PerformanceCompositeInvestmentAccountPerformance accountPerformance : CollectionUtils.getIterable(accountPerformances)) {
				if (!WorkflowStatus.STATUS_APPROVED.equals(accountPerformance.getWorkflowStatus().getName())) {
					violationNotes.add("[" + accountPerformance.getLabel() + "] Not in Approved State: " + accountPerformance.getWorkflowStatus().getName() + " instead of Approved");
				}
			}
			if (!violationNotes.isEmpty()) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNotes", violationNotes);
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositePerformance.getId(), null, null, contextValues));
			}
		}
		return ruleViolationList;
	}
}
