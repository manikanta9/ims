package com.clifton.performance.composite.setup.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeObserver</code> observes changes to start/end dates to PerformanceComposites for automatically updating rollup composite dates from children
 * If a rollup itself is created or updated, dates are automatically updated in that rollup's save.
 *
 * @author manderson
 */
@Component
public class PerformanceCompositeObserver extends SelfRegisteringDaoObserver<PerformanceComposite> {

	private PerformanceCompositeSetupService performanceCompositeSetupService;


	private static final String SKIP_ROLLUP_DATE_CHECK = "SKIP_ROLLUP_DATE_CHECK";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<PerformanceComposite> dao, DaoEventTypes event, PerformanceComposite bean) {
		if (event.isUpdate()) {
			// Get original bean so we have it for after
			getOriginalBean(dao, bean);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<PerformanceComposite> dao, DaoEventTypes event, PerformanceComposite bean, Throwable e) {
		if (e == null) {
			PerformanceComposite originalBean = (event.isUpdate() ? getOriginalBean(dao, bean) : null);

			if (bean.isRollupComposite() && !BooleanUtils.isTrue(getDaoEventContext().getBeanAttribute(bean, SKIP_ROLLUP_DATE_CHECK))) {
				getDaoEventContext().setBeanAttribute(bean, SKIP_ROLLUP_DATE_CHECK, true);
				updateRollupStartAndEndDates(bean, dao);
				getDaoEventContext().removeBeanAttribute(bean, SKIP_ROLLUP_DATE_CHECK);
			}

			List<String> notEqualProperties = (originalBean != null ? CoreCompareUtils.getNoEqualProperties(bean, originalBean, false, "parentList", "childList") : null);
			if (!CollectionUtils.isEmpty(notEqualProperties) && (notEqualProperties.contains("startDate") || notEqualProperties.contains("endDate"))) {
				List<PerformanceComposite> parentList = getPerformanceCompositeSetupService().getPerformanceCompositeParentList(bean.getId());
				for (PerformanceComposite parent : CollectionUtils.getIterable(parentList)) {
					// Retrieve so that the child list is populated
					parent = getPerformanceCompositeSetupService().getPerformanceComposite(parent.getId());
					updateRollupStartAndEndDates(parent, dao);
				}
			}
		}
	}


	/**
	 * For a rollup composite, will re-calculate start and end dates based on children.  If any changes, will save the composite
	 */
	private void updateRollupStartAndEndDates(PerformanceComposite performanceComposite, ReadOnlyDAO<PerformanceComposite> dao) {
		if (performanceComposite.isRollupComposite()) {
			if (!CollectionUtils.isEmpty(performanceComposite.getChildCompositeList())) {
				// set rollup composite start/end dates to match children
				Date startDate = null;
				Date endDate = null;
				boolean noEndDate = false;
				for (PerformanceComposite child : performanceComposite.getChildCompositeList()) {

					if (startDate == null || DateUtils.isDateBefore(child.getStartDate(), startDate, true)) {
						startDate = child.getStartDate();
					}
					if (!noEndDate) {
						if (child.getEndDate() == null) {
							noEndDate = true;
						}
						else if (endDate == null || endDate.before(child.getEndDate())) {
							endDate = child.getEndDate();
						}
					}
				}
				if (noEndDate) {
					endDate = null;
				}
				if (DateUtils.compare(performanceComposite.getStartDate(), startDate, false) != 0 || DateUtils.compare(performanceComposite.getEndDate(), endDate, false) != 0) {
					performanceComposite.setStartDate(startDate);
					performanceComposite.setEndDate(endDate);
					((UpdatableDAO<PerformanceComposite>) dao).save(performanceComposite);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}
}
