package com.clifton.performance.composite.performance;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceCompositeInvestmentAccountDailyPerformance</code> class represents daily returns for a given
 * investment account for a specific day within an accounting period.
 *
 * @author apopp
 */
public class PerformanceCompositeInvestmentAccountDailyPerformance extends BaseEntity<Integer> {

	/**
	 * Tie the daily values to the monthly performance
	 */
	private PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance;

	//The day within the account period these returns are for
	private Date measureDate;

	private BigDecimal gainLoss;
	private BigDecimal gainLossOverride;

	private BigDecimal positionValue;
	private BigDecimal positionValueOverride;

	private BigDecimal netCashFlow;
	private BigDecimal netCashFlowOverride;

	/**
	 * Cash flows are considered beginning of day, however in some cases the cash flow should be considered end of day
	 * When this happens, accounting will manually enter the cash flow amount into the moc value, and back it out of the netCashFlow (via the override)
	 * The return calculation would then ignore that cash flow - but it would be included in the sum of the cash flow for the month
	 */
	private BigDecimal marketOnCloseNetCashFlow;

	private BigDecimal weightedNetCashFlow;
	private BigDecimal weightedNetCashFlowOverride;

	private BigDecimal grossReturn;

	private BigDecimal benchmarkReturn;
	private BigDecimal secondaryBenchmarkReturn;
	private BigDecimal thirdBenchmarkReturn;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccountDailyPerformance() {
		// empty
	}


	public PerformanceCompositeInvestmentAccountDailyPerformance(PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance, Date measureDate) {
		setPerformanceCompositeInvestmentAccountPerformance(performanceCompositeInvestmentAccountPerformance);
		setMeasureDate(measureDate);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		if (getPerformanceCompositeInvestmentAccountPerformance() != null) {
			if (getPerformanceCompositeInvestmentAccountPerformance().getClientAccount() != null) {
				return getPerformanceCompositeInvestmentAccountPerformance().getClientAccount().getLabel() + ": " + DateUtils.fromDateShort(getMeasureDate());
			}
		}
		return null;
	}


	public BigDecimal getTotalNetCashFlow() {
		return MathUtils.add(getCoalesceNetCashFlowNetCashFlowOverride(), getMarketOnCloseNetCashFlow());
	}


	public BigDecimal getCoalesceNetCashFlowNetCashFlowOverride() {
		return ObjectUtils.coalesce(getNetCashFlowOverride(), getNetCashFlow());
	}


	public BigDecimal getCoalesceWeightedNetCashFlowWeightedNetCashFlowOverride() {
		return ObjectUtils.coalesce(getWeightedNetCashFlowOverride(), getWeightedNetCashFlow());
	}


	public BigDecimal getCoalesceGainLossGainLossOverride() {
		return ObjectUtils.coalesce(getGainLossOverride(), getGainLoss());
	}


	public BigDecimal getCoalescePositionValuePositionValueOverride() {
		return ObjectUtils.coalesce(getPositionValueOverride(), getPositionValue());
	}


	/**
	 * Certain fields should be cleared before reprocessing
	 */
	public void resetFieldsForProcessing() {
		setGrossReturn(null);
		setGainLoss(null);
		setNetCashFlow(null);
		setWeightedNetCashFlow(null);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static PerformanceCompositeInvestmentAccountDailyPerformance newInstance(PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance, Date measureDate) {
		return new PerformanceCompositeInvestmentAccountDailyPerformance(performanceCompositeInvestmentAccountPerformance, measureDate);
	}


	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformance() {
		return this.performanceCompositeInvestmentAccountPerformance;
	}


	public void setPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance performanceCompositeInvestmentAccountPerformance) {
		this.performanceCompositeInvestmentAccountPerformance = performanceCompositeInvestmentAccountPerformance;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public BigDecimal getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(BigDecimal gainLoss) {
		this.gainLoss = gainLoss;
	}


	public BigDecimal getPositionValue() {
		return this.positionValue;
	}


	public void setPositionValue(BigDecimal positionValue) {
		this.positionValue = positionValue;
	}


	public BigDecimal getNetCashFlow() {
		return this.netCashFlow;
	}


	public void setNetCashFlow(BigDecimal netCashFlow) {
		this.netCashFlow = netCashFlow;
	}


	public BigDecimal getGrossReturn() {
		return this.grossReturn;
	}


	public void setGrossReturn(BigDecimal grossReturn) {
		this.grossReturn = grossReturn;
	}


	public BigDecimal getBenchmarkReturn() {
		return this.benchmarkReturn;
	}


	public void setBenchmarkReturn(BigDecimal benchmarkReturn) {
		this.benchmarkReturn = benchmarkReturn;
	}


	public BigDecimal getSecondaryBenchmarkReturn() {
		return this.secondaryBenchmarkReturn;
	}


	public void setSecondaryBenchmarkReturn(BigDecimal secondaryBenchmarkReturn) {
		this.secondaryBenchmarkReturn = secondaryBenchmarkReturn;
	}


	public BigDecimal getThirdBenchmarkReturn() {
		return this.thirdBenchmarkReturn;
	}


	public void setThirdBenchmarkReturn(BigDecimal thirdBenchmarkReturn) {
		this.thirdBenchmarkReturn = thirdBenchmarkReturn;
	}


	public BigDecimal getPositionValueOverride() {
		return this.positionValueOverride;
	}


	public void setPositionValueOverride(BigDecimal positionValueOverride) {
		this.positionValueOverride = positionValueOverride;
	}


	public BigDecimal getNetCashFlowOverride() {
		return this.netCashFlowOverride;
	}


	public void setNetCashFlowOverride(BigDecimal netCashFlowOverride) {
		this.netCashFlowOverride = netCashFlowOverride;
	}


	public BigDecimal getWeightedNetCashFlow() {
		return this.weightedNetCashFlow;
	}


	public void setWeightedNetCashFlow(BigDecimal weightedNetCashFlow) {
		this.weightedNetCashFlow = weightedNetCashFlow;
	}


	public BigDecimal getWeightedNetCashFlowOverride() {
		return this.weightedNetCashFlowOverride;
	}


	public void setWeightedNetCashFlowOverride(BigDecimal weightedNetCashFlowOverride) {
		this.weightedNetCashFlowOverride = weightedNetCashFlowOverride;
	}


	public BigDecimal getGainLossOverride() {
		return this.gainLossOverride;
	}


	public void setGainLossOverride(BigDecimal gainLossOverride) {
		this.gainLossOverride = gainLossOverride;
	}


	public BigDecimal getMarketOnCloseNetCashFlow() {
		return this.marketOnCloseNetCashFlow;
	}


	public void setMarketOnCloseNetCashFlow(BigDecimal marketOnCloseNetCashFlow) {
		this.marketOnCloseNetCashFlow = marketOnCloseNetCashFlow;
	}
}
