package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeAccountSubPeriodModifiedDietzCalculator</code> is a method used for measuring an accounts return
 * based on position values and cash flows.
 * <p>
 * These accounts have securities with manually uploaded prices at the end of the month or whenever a greater than 10% cash flow occurs.  The large cash flows
 * mark a sub period return calculation.  The gain loss is calculated over the range of the sub period dates instead of daily.
 * <p>
 * Sub period returns are geometrically linked to produce the monthly return
 *
 * @author manderson
 */
public class PerformanceCompositeAccountSubPeriodModifiedDietzCalculator extends BasePerformanceDailyCalculator {

	private static final double CASH_FLOW_PERCENT_TRIGGER = 10.0; // 10% cash flow triggers a sub period calculation

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Method is overridden because the days to process is determined during processing
	 * Would only have results on month end and the day before a cash flow greater than 10% threshold
	 */
	@Override
	protected void calculateDailyAccountReturns(PerformanceCompositeAccountRuleEvaluatorContext config) {

		// We need to iterate through each day in the period, but will only save "daily" values for month end and when there is a break
		List<Date> dateList = getDaysToProcessForMonth(config);
		List<Date> subPeriodDateList = new ArrayList<>();

		Date previousDate = DateUtils.addDays(config.getAccountingPeriod().getStartDate(), -1);
		Date startSubPeriodDate = previousDate;
		BigDecimal previousMonthEndPositionValueAdjustment = null;

		// First go through each date and compile a list of cash flow dates that we will need to keep (calculate returns the day BEFORE the cash flow)
		for (Date dateToCalculate : CollectionUtils.getIterable(dateList)) {
			PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
			calculateDailyPositionValue(config, dateToCalculate);

			if (DateUtils.isEqualWithoutTime(dateToCalculate, DateUtils.getLastDayOfPreviousMonth(config.getAccountingPeriod().getStartDate()))) {
				previousMonthEndPositionValueAdjustment = getPreviousMonthEndPositionValueAdjustment(config, dateToCalculate);
			}

			/// Skip Last Day of Previous Month - Only Used to Display the Position Value
			if (DateUtils.isDateAfterOrEqual(dateToCalculate, config.getAccountingPeriod().getStartDate())) {
				calculateDailyNetCashFlowValue(config, dateToCalculate);

				if (isSubPeriodBreak(dailyPerformance) && !DateUtils.isLastDayOfMonth(previousDate)) {
					subPeriodDateList.add(previousDate);
				}
				if (DateUtils.isLastDayOfMonth(dateToCalculate)) {
					subPeriodDateList.add(dateToCalculate);
				}
			}
			previousDate = dateToCalculate;
		}

		BigDecimal monthlyTotalCashFlow = BigDecimal.ZERO;
		BigDecimal monthlyTotalWeightedCashFlow = BigDecimal.ZERO;
		BigDecimal monthlyGainLoss = BigDecimal.ZERO;

		Collections.sort(subPeriodDateList);
		for (Date subPeriodEndDate : subPeriodDateList) {
			BigDecimal daysInPeriod = new BigDecimal(DateUtils.getDaysDifference(subPeriodEndDate, startSubPeriodDate));
			BigDecimal subPeriodTotalCashFlow = BigDecimal.ZERO;
			BigDecimal subPeriodTotalWeightedCashFlow = BigDecimal.ZERO;

			// Start Cash Flow Addition on the Next Day so we don't double count when a date ends and starts a new period
			Date startCashFlowDate = DateUtils.addDays(startSubPeriodDate, 1);
			for (Date date : dateList) {
				if (DateUtils.isDateBetween(date, startCashFlowDate, subPeriodEndDate, false)) {
					PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(date);
					//If there was a cash flow on this day then calculate it's sub period weighted flow
					if (!MathUtils.isNullOrZero(dailyPerformance.getNetCashFlow())) {
						subPeriodTotalCashFlow = MathUtils.add(subPeriodTotalCashFlow, dailyPerformance.getNetCashFlow());
						// Pro-rate the cash flow for the period
						subPeriodTotalWeightedCashFlow = MathUtils.add(subPeriodTotalWeightedCashFlow, CoreMathUtils.getValueProrated(new BigDecimal(DateUtils.getDaysDifferenceInclusive(subPeriodEndDate, date)), daysInPeriod, dailyPerformance.getNetCashFlow()));
					}
				}
			}
			PerformanceCompositeInvestmentAccountDailyPerformance endPeriodPerformance = calculateSubPeriodReturn(config, startSubPeriodDate, subPeriodEndDate, subPeriodTotalCashFlow, subPeriodTotalWeightedCashFlow, previousMonthEndPositionValueAdjustment);
			previousMonthEndPositionValueAdjustment = null; // Once we set it - clear it
			// Because overrides could be entered, we track monthly cash flow/weighted cash flow from each period
			monthlyTotalCashFlow = MathUtils.add(monthlyTotalCashFlow, endPeriodPerformance.getTotalNetCashFlow());
			if (!MathUtils.isNullOrZero(endPeriodPerformance.getCoalesceWeightedNetCashFlowWeightedNetCashFlowOverride())) {
				// Add Sub Period Weighted to Monthly Weighted Based on Days in Period for Month Start vs. Days In Month
				monthlyTotalWeightedCashFlow = MathUtils.add(monthlyTotalWeightedCashFlow, calculateDailyWeightedNetCashFlowValue(startCashFlowDate, endPeriodPerformance.getCoalesceWeightedNetCashFlowWeightedNetCashFlowOverride()));
			}
			monthlyGainLoss = MathUtils.add(monthlyGainLoss, endPeriodPerformance.getCoalesceGainLossGainLossOverride());
			startSubPeriodDate = subPeriodEndDate;
		}

		// Clear Out Unused Values
		clearUnusedDailyValues(config, subPeriodDateList);

		PerformanceCompositeInvestmentAccountPerformance monthlyPerformance = config.getPerformanceCompositeInvestmentAccountPerformance();
		monthlyPerformance.setPeriodTotalNetCashflow(monthlyTotalCashFlow);
		monthlyPerformance.setPeriodWeightedNetCashflow(monthlyTotalWeightedCashFlow);
		monthlyPerformance.setPeriodGainLoss(monthlyGainLoss);
	}


	/**
	 * Returns true if there was a cash flow greater than 10% threshold
	 * Then we calculate returns for the previous date
	 */
	private boolean isSubPeriodBreak(PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance) {
		// Original Values
		if (!MathUtils.isNullOrZero(dailyPerformance.getNetCashFlow()) && MathUtils.isGreaterThanOrEqual(MathUtils.abs(CoreMathUtils.getPercentValue(dailyPerformance.getNetCashFlow(), dailyPerformance.getPositionValue(), true)), CASH_FLOW_PERCENT_TRIGGER)) {
			return true;
		}
		// Or With Overrides
		if (!MathUtils.isNullOrZero(dailyPerformance.getCoalesceNetCashFlowNetCashFlowOverride()) && MathUtils.isGreaterThanOrEqual(MathUtils.abs(CoreMathUtils.getPercentValue(dailyPerformance.getCoalesceNetCashFlowNetCashFlowOverride(), dailyPerformance.getCoalescePositionValuePositionValueOverride(), true)), CASH_FLOW_PERCENT_TRIGGER)) {
			return true;
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////


	private PerformanceCompositeInvestmentAccountDailyPerformance calculateSubPeriodReturn(PerformanceCompositeAccountRuleEvaluatorContext config, Date startSubPeriodDate, Date endSubPeriodDate, BigDecimal cashFlowTotal, BigDecimal weightedCashFlowTotal, BigDecimal previousMonthEndPositionValueAdjustment) {
		// Gain/Loss is the Change in Position Value (End Value - Start Value)
		// Return Calculation is: [(Gain/Loss – net cash flow) / (Beginning Market Value + weighted net cash flow)]
		PerformanceCompositeInvestmentAccountDailyPerformance startPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(startSubPeriodDate);
		PerformanceCompositeInvestmentAccountDailyPerformance endPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(endSubPeriodDate);

		endPerformance.setNetCashFlow(cashFlowTotal);
		endPerformance.setWeightedNetCashFlow(weightedCashFlowTotal);
		calculateDailyGainLoss(config, endSubPeriodDate, startSubPeriodDate, previousMonthEndPositionValueAdjustment);

		BigDecimal grossReturn = calculateDailyReturn(endPerformance.getCoalesceGainLossGainLossOverride(), startPerformance.getCoalescePositionValuePositionValueOverride(), endPerformance.getCoalesceNetCashFlowNetCashFlowOverride(), endPerformance.getCoalesceWeightedNetCashFlowWeightedNetCashFlowOverride());
		endPerformance.setGrossReturn(grossReturn);

		calculateDailyBenchmarkValues(config, endSubPeriodDate, startSubPeriodDate);
		return endPerformance;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void calculateDailyGainLoss(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date previousDate, BigDecimal previousMonthEndPositionValueAdjustment) {
		if (isCalculateGainLossAsChangeInPositionValue()) {
			PerformanceCompositeInvestmentAccountDailyPerformance startPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(previousDate);
			PerformanceCompositeInvestmentAccountDailyPerformance endPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
			BigDecimal gainLoss = MathUtils.subtract(MathUtils.subtract(endPerformance.getCoalescePositionValuePositionValueOverride(), startPerformance.getCoalescePositionValuePositionValueOverride()), endPerformance.getCoalesceNetCashFlowNetCashFlowOverride());
			endPerformance.setGainLoss(gainLoss);
			// Do Not need to add the adjustment manually here - calculated as a change in position value, so if position value was automatically adjusted
			// the the gain/loss will reflect that. The following will just add the appropriate violation note
			addPreviousMonthEndPositionValueViolation(config, endPerformance, previousMonthEndPositionValueAdjustment);
		}
		else {
			super.calculateDailyGainLoss(config, dateToCalculate, previousDate, previousMonthEndPositionValueAdjustment);
		}
	}


	/**
	 * LDI calculator which extends this, needs to be able to call real gain/loss calculation - not just change in value
	 */
	protected boolean isCalculateGainLossAsChangeInPositionValue() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected BigDecimal calculateDailyReturn(BigDecimal gainLoss, BigDecimal positionValue, BigDecimal netCashFlow, BigDecimal weightedNetCashFlow) {
		//(Gain/Loss)/(Notional + Weighted Net Cash Flow)
		return CoreMathUtils.getPercentValue(gainLoss, MathUtils.add(positionValue, weightedNetCashFlow), true);
	}
}
