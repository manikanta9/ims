package com.clifton.performance.composite.performance.process.calculators.fee;

import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;

import java.math.BigDecimal;


/**
 * The <code>PerformanceCompositeAccountZeroManagementFeeOverrideCalculator</code> allows for the ability to override the management fee to always be zero
 * This is currently only used for accounts that are in admin type composites that generate a zero return - we don't want to show a negative return for the actual net
 *
 * @author manderson
 */
public class PerformanceCompositeAccountZeroManagementFeeOverrideCalculator implements PerformanceCompositeAccountManagementFeeOverrideCalculator {


	@Override
	public void calculateManagementFeeOverride(PerformanceCompositeAccountRuleEvaluatorContext config, BigDecimal managementFee) {
		config.getPerformanceCompositeInvestmentAccountPerformance().setManagementFeeOverride(BigDecimal.ZERO);
	}
}
