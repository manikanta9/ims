package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTarget;
import com.clifton.investment.account.securitytarget.InvestmentAccountSecurityTargetService;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.trade.Trade;
import com.clifton.trade.group.TradeGroupType;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTarget;
import com.clifton.trade.options.securitytarget.util.TradeSecurityTargetUtilHandler;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class PerformanceCompositeAccountDailySecurityTargetCalculator extends BasePerformanceDailyCalculator {

	private InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService;

	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler;

	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used for cases like manager balances where this adjustment doesn't apply
	 */
	private boolean doNotAdjustPreviousValueForReturn;

	/**
	 * Optionally add position market value to target (optionally include specific instrument group)
	 */
	private boolean addPositionMarketValueToTarget;

	/**
	 * Optionally add cash balance to target
	 */
	private boolean addCashValueToTarget;

	/**
	 * If true will add the security target position value change for price changes to the gain/loss in the return calculation
	 * i.e. Re-calculates current day's target using previous day price - applies the difference from previous day's position value to the denominator,
	 * then also applies the change from current day to previous day to the numerator of the return calculation.
	 */
	private boolean includeSecurityTargetPriceChangeInReturn;

	/**
	 * If populated, limits security targets to only those for selected underlying securities.
	 * Will also limit Option's gain/loss to those for the same underlying
	 */
	private List<Integer> underlyingSecurityIds;

	/**
	 * If populated will limit option's gain/loss inclusion to those of the selected option type
	 */
	private InvestmentSecurityOptionTypes optionType;

	/**
	 * When including options only gain/loss we need to adjust the gain/loss calculation for assignments because the gain/loss get's skewed (large gain or loss)
	 */
	private boolean adjustOptionGainLossForAssignments;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void calculateDailyMetrics(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date previousDate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		PerformanceCompositeInvestmentAccountDailyPerformance previousPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(previousDate);
		BigDecimal gainLoss = dailyPerformance.getCoalesceGainLossGainLossOverride();
		// Get the Original Previous Value
		BigDecimal previousPositionValue = previousPerformance.getCoalescePositionValuePositionValueOverride();

		if (!isDoNotAdjustPreviousValueForReturn()) {
			// For Security Targets, the denominator is always the "opening value" - i.e. current day's target at previous day's price
			// We just add the change to the existing previous value, because if we are including positions other than security targets we don't need to re-calculate them
			BigDecimal previousTargetValue = getSecurityTargetPositionValue(config, previousDate, previousDate);
			BigDecimal previousTargetValueAdjusted = getSecurityTargetPositionValue(config, dateToCalculate, previousDate);
			// Get the Security Target Change For Prices Only
			BigDecimal securityTargetChange = MathUtils.subtract(previousTargetValueAdjusted, previousTargetValue);
			// Adjust Previous Value for that change (we don't just overwrite it in case we include market value as well)
			previousPositionValue = MathUtils.add(previousPositionValue, securityTargetChange);
		}

		if (isIncludeSecurityTargetPriceChangeInReturn()) {
			// Add the change from CURRENT to PREVIOUS to the Gain/Loss for the Return Calculation
			gainLoss = MathUtils.add(gainLoss, MathUtils.subtract(dailyPerformance.getCoalescePositionValuePositionValueOverride(), previousPositionValue));
		}
		BigDecimal grossReturn = calculateDailyReturn(gainLoss, previousPositionValue, null, null);
		dailyPerformance.setGrossReturn(grossReturn);

		// If last day of the month - calculate last day net return and store it in the config for use later
		if (DateUtils.isLastDayOfMonth(dateToCalculate)) {
			BigDecimal managementFee = calculateManagementFee(config);
			config.setLastDayNetReturn(calculateDailyReturn(MathUtils.subtract(gainLoss, managementFee), previousPositionValue, null, null));
		}
	}


	/**
	 * We compute actual net return from the underlying stream of gross returns. The only difference is we need to recompute
	 * the last days gross return with an included gain/loss that is the management fee. (Pre-calculated on last day of the month in calculateDailyMetrics method)
	 */
	@Override
	protected void calculateActualNetReturn(PerformanceCompositeAccountRuleEvaluatorContext config) {
		BigDecimal lastDayNetReturn = config.getLastDayNetReturn();
		calculateActualNetReturn(config, lastDayNetReturn);
	}


	@Override
	protected BigDecimal calculateDailyReturn(BigDecimal gainLoss, BigDecimal positionValue, BigDecimal netCashFlow, BigDecimal weightedNetCashFlow) {
		//(Gain/Loss)/(Security Target)
		return CoreMathUtils.getPercentValue(gainLoss, positionValue, true);
	}


	@Override
	protected void calculateDailyPositionValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		BigDecimal positionValue = BigDecimal.ZERO;
		// Set the Market Value Portion
		if (isAddPositionMarketValueToTarget()) {
			positionValue = CoreMathUtils.sumProperty(config.getPositionListForDate(dateToCalculate, getIncludePositionInvestmentGroupId(), getExcludePositionInvestmentGroupId()), AccountingPositionDaily::getMarketValueBase);
		}
		if (isAddCashValueToTarget()) {
			positionValue = MathUtils.add(positionValue, config.getCashValueForDate(dateToCalculate, isIncludeAllNonPositionAssetsAndLiabilities()));
		}
		positionValue = MathUtils.add(positionValue, getSecurityTargetPositionValue(config, dateToCalculate, dateToCalculate));

		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		dailyPerformance.setPositionValue(positionValue);
	}


	private BigDecimal getSecurityTargetPositionValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date priceDate) {
		BigDecimal positionValue = BigDecimal.ZERO;
		List<InvestmentAccountSecurityTarget> securityTargetList = getInvestmentAccountSecurityTargetList(config, dateToCalculate);
		for (InvestmentAccountSecurityTarget securityTarget : CollectionUtils.getIterable(securityTargetList)) {
			// NOTE: Leaving for now for historical processing, but likely might want to prevent flexible manager retrieval
			TradeSecurityTarget tradeSecurityTarget = getTradeSecurityTargetUtilHandler().getTradeSecurityTargetForDateWithPriceDate(securityTarget, dateToCalculate, priceDate);
			positionValue = MathUtils.add(positionValue, tradeSecurityTarget.getTargetUnderlyingNotionalAdjustedAbs());
		}
		return positionValue;
	}


	@Override
	protected List<AccountingPositionDaily> filterAccountingPositionListForGainLoss(List<AccountingPositionDaily> accountingPositionDailyList) {
		return BeanUtils.filter(accountingPositionDailyList, this::isAccountingPositionDailyIncludedInGainLoss);
	}


	private boolean isAccountingPositionDailyIncludedInGainLoss(AccountingPositionDaily accountingPositionDaily) {
		if (InvestmentUtils.isOption(accountingPositionDaily.getInvestmentSecurity())) {
			if (getUnderlyingSecurityIds() != null && !getUnderlyingSecurityIds().contains(accountingPositionDaily.getInvestmentSecurity().getUnderlyingSecurity().getId())) {
				return false;
			}
			if (getOptionType() != null) {
				InvestmentSecurityOptionTypes securityOptionType = accountingPositionDaily.getInvestmentSecurity().getOptionType();
				return getOptionType() == securityOptionType;
			}
		}
		return true;
	}


	/**
	 * For most cases we just return the base gain/loss value from AccountingPositionDaily
	 * For Security Target calculations that include Options only gain/loss we need to adjust the gain/loss for assignments or else it looks like a large loss
	 */
	@Override
	protected BigDecimal getAccountingPositionDailyBaseGainLoss(PerformanceCompositeAccountRuleEvaluatorContext config, AccountingPositionDaily accountingPositionDaily) {
		// If security is an option and we closed positions today, check to see if any of the closings were due to an assignment
		if (isAdjustOptionGainLossForAssignments() && InvestmentUtils.isOption(accountingPositionDaily.getInvestmentSecurity()) && !MathUtils.isNullOrZero(accountingPositionDaily.getTodayClosedQuantity())) {
			List<Trade> securityTradeList = BeanUtils.filter(config.getTradeListForDate(accountingPositionDaily.getPositionDate()), trade -> CompareUtils.isEqual(accountingPositionDaily.getInvestmentSecurity(), trade.getInvestmentSecurity()));
			for (Trade trade : CollectionUtils.getIterable(securityTradeList)) {
				if (trade.getTradeGroup() != null && trade.getTradeGroup().getTradeGroupType().getGroupType() == TradeGroupType.GroupTypes.OPTION_ASSIGNMENT) {
					// Intrinsic Value: (Previous Day Underlying Price - Option Strike) * (Quantity * Price Multiplier)
					BigDecimal intrinsicValue = MathUtils.multiply(MathUtils.subtract(config.getInvestmentSecurityPriceForDate(trade.getInvestmentSecurity().getUnderlyingSecurity(), DateUtils.addDays(trade.getTradeDate(), -1), false), trade.getInvestmentSecurity().getOptionStrikePrice()), MathUtils.multiply(trade.getQuantity(), trade.getInvestmentSecurity().getPriceMultiplier()));
					// Less Option Premium
					BigDecimal optionPremium = MathUtils.multiply(MathUtils.multiply(trade.getQuantity(), accountingPositionDaily.getAccountingTransaction().getPrice()), trade.getInvestmentSecurity().getPriceMultiplier());
					// Unrealized Gain/Loss Override (Base OTE)
					BigDecimal unrealized = MathUtils.negate(MathUtils.subtract(intrinsicValue, optionPremium));
					// Net Previous Day
					return MathUtils.subtract(unrealized, accountingPositionDaily.getPriorOpenTradeEquityBase());
				}
			}
		}
		// Default
		return accountingPositionDaily.getDailyGainLossBase();
	}


	private List<InvestmentAccountSecurityTarget> getInvestmentAccountSecurityTargetList(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		List<InvestmentAccountSecurityTarget> securityTargetList = config.getAccountSecurityTargetList(getInvestmentAccountSecurityTargetService());
		return BeanUtils.filter(securityTargetList, securityTarget -> {
			if (DateUtils.isDateBetween(dateToCalculate, securityTarget.getStartDate(), securityTarget.getEndDate(), false)) {
				if (getUnderlyingSecurityIds() != null) {
					return getUnderlyingSecurityIds().contains(securityTarget.getTargetUnderlyingSecurity().getId());
				}
				return true;
			}
			return false;
		});
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountSecurityTargetService getInvestmentAccountSecurityTargetService() {
		return this.investmentAccountSecurityTargetService;
	}


	public void setInvestmentAccountSecurityTargetService(InvestmentAccountSecurityTargetService investmentAccountSecurityTargetService) {
		this.investmentAccountSecurityTargetService = investmentAccountSecurityTargetService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public TradeSecurityTargetUtilHandler getTradeSecurityTargetUtilHandler() {
		return this.tradeSecurityTargetUtilHandler;
	}


	public void setTradeSecurityTargetUtilHandler(TradeSecurityTargetUtilHandler tradeSecurityTargetUtilHandler) {
		this.tradeSecurityTargetUtilHandler = tradeSecurityTargetUtilHandler;
	}


	public List<Integer> getUnderlyingSecurityIds() {
		return this.underlyingSecurityIds;
	}


	public void setUnderlyingSecurityIds(List<Integer> underlyingSecurityIds) {
		this.underlyingSecurityIds = underlyingSecurityIds;
	}


	public InvestmentSecurityOptionTypes getOptionType() {
		return this.optionType;
	}


	public void setOptionType(InvestmentSecurityOptionTypes optionType) {
		this.optionType = optionType;
	}


	public boolean isAddPositionMarketValueToTarget() {
		return this.addPositionMarketValueToTarget;
	}


	public void setAddPositionMarketValueToTarget(boolean addPositionMarketValueToTarget) {
		this.addPositionMarketValueToTarget = addPositionMarketValueToTarget;
	}


	public boolean isAddCashValueToTarget() {
		return this.addCashValueToTarget;
	}


	public void setAddCashValueToTarget(boolean addCashValueToTarget) {
		this.addCashValueToTarget = addCashValueToTarget;
	}


	public boolean isIncludeSecurityTargetPriceChangeInReturn() {
		return this.includeSecurityTargetPriceChangeInReturn;
	}


	public void setIncludeSecurityTargetPriceChangeInReturn(boolean includeSecurityTargetPriceChangeInReturn) {
		this.includeSecurityTargetPriceChangeInReturn = includeSecurityTargetPriceChangeInReturn;
	}


	public boolean isAdjustOptionGainLossForAssignments() {
		return this.adjustOptionGainLossForAssignments;
	}


	public void setAdjustOptionGainLossForAssignments(boolean adjustOptionGainLossForAssignments) {
		this.adjustOptionGainLossForAssignments = adjustOptionGainLossForAssignments;
	}


	public boolean isDoNotAdjustPreviousValueForReturn() {
		return this.doNotAdjustPreviousValueForReturn;
	}


	public void setDoNotAdjustPreviousValueForReturn(boolean doNotAdjustPreviousValueForReturn) {
		this.doNotAdjustPreviousValueForReturn = doNotAdjustPreviousValueForReturn;
	}
}
