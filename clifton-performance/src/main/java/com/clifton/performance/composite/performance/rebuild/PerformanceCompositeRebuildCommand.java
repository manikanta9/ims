package com.clifton.performance.composite.performance.rebuild;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.util.status.Status;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;


/**
 * The <code>PerformanceCompositeRebuildCommand</code> controls how performance metrics are rebuilt
 *
 * @author apopp
 */
public class PerformanceCompositeRebuildCommand {

	private Short performanceCompositeId;

	/**
	 * Only needs to be set if calculating for more than one period
	 */
	private AccountingPeriod fromAccountingPeriod;

	/**
	 * If from is not set, only calculates for the selected period
	 */
	private AccountingPeriod toAccountingPeriod;

	/**
	 * If not checked and performance is already started approval process would get an error during processing that the performance is not allowed to be edited in that state
	 * If checked, and the performance is not in Draft state currently, it would first be moved to Draft state before re-processing
	 */
	private boolean transitionToDraftPriorToProcessing;


	/**
	 * i.e. Calculate All Metric Fields, Benchmarks Only, or Linked Only
	 * Defaults to all, but can be customized for other cases
	 */
	private PerformanceCompositeMetricProcessingTypes metricProcessingType = PerformanceCompositeMetricProcessingTypes.ALL;


	// Synchronous support
	private boolean synchronous = false;

	private Status status = Status.ofEmptyMessage();

	private boolean preview;

	// Used for auto rebuilding composite performance on the account performance approval.  Will ONLY re-process if all accounts are in required status - i.e. Approved
	// This will prevent extra unnecessary processing and only re-process the composite automatically when all accounts have been approved.
	private String accountRequiredWorkflowStatusName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getUniqueRunId() {
		StringBuilder id = new StringBuilder(16);
		if (getPerformanceCompositeId() != null) {
			id.append("C-").append(getPerformanceCompositeId());
		}
		if (getFromAccountingPeriod() != null) {
			id.append("From-").append(getFromAccountingPeriod().getId());
		}
		if (getToAccountingPeriod() != null) {
			id.append("To-").append(getToAccountingPeriod().getId());
		}
		return id.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriod getFromAccountingPeriod() {
		return this.fromAccountingPeriod;
	}


	public void setFromAccountingPeriod(AccountingPeriod fromAccountingPeriod) {
		this.fromAccountingPeriod = fromAccountingPeriod;
	}


	public AccountingPeriod getToAccountingPeriod() {
		return this.toAccountingPeriod;
	}


	public void setToAccountingPeriod(AccountingPeriod toAccountingPeriod) {
		this.toAccountingPeriod = toAccountingPeriod;
	}


	public Short getPerformanceCompositeId() {
		return this.performanceCompositeId;
	}


	public void setPerformanceCompositeId(Short performanceCompositeId) {
		this.performanceCompositeId = performanceCompositeId;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public boolean isPreview() {
		return this.preview;
	}


	public void setPreview(boolean preview) {
		this.preview = preview;
	}


	public boolean isTransitionToDraftPriorToProcessing() {
		return this.transitionToDraftPriorToProcessing;
	}


	public void setTransitionToDraftPriorToProcessing(boolean transitionToDraftPriorToProcessing) {
		this.transitionToDraftPriorToProcessing = transitionToDraftPriorToProcessing;
	}


	public PerformanceCompositeMetricProcessingTypes getMetricProcessingType() {
		return this.metricProcessingType;
	}


	public void setMetricProcessingType(PerformanceCompositeMetricProcessingTypes metricProcessingType) {
		this.metricProcessingType = metricProcessingType;
	}


	public String getAccountRequiredWorkflowStatusName() {
		return this.accountRequiredWorkflowStatusName;
	}


	public void setAccountRequiredWorkflowStatusName(String accountRequiredWorkflowStatusName) {
		this.accountRequiredWorkflowStatusName = accountRequiredWorkflowStatusName;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
