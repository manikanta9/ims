package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;

import java.util.Date;


/**
 * The <code>PerformanceCompositeAccountSubPeriodModifiedDietzExternalCalculator</code> calculation strategy is the same as the normal
 * {@link PerformanceCompositeAccountSubPeriodModifiedDietzCalculator} except using externally uploaded data with validation.
 *
 * @author apopp
 */
public class PerformanceCompositeAccountSubPeriodModifiedDietzExternalCalculator extends PerformanceCompositeAccountSubPeriodModifiedDietzCalculator {

	@Override
	public void calculateDailyAccountReturns(PerformanceCompositeAccountRuleEvaluatorContext config) {
		validateMonthlyAndDailyBaseValuesExist(config);
		super.calculateDailyAccountReturns(config);
	}


	/**
	 * Beginning of day base market value calculated as the base market value of the end of the previous weekday
	 */
	@Override
	protected void calculateDailyPositionValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		// DO NOTHING VALUES ARE EXTERNALLY LOADED
	}


	/**
	 * Calculate net cash flow as total credit - total debit for cash contributions and distributions
	 */
	@Override
	protected void calculateDailyNetCashFlowValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		// Net Cash Flow is Externally Loaded - Just Re-calculate Weighted Value
		dailyPerformance.setWeightedNetCashFlow(calculateDailyWeightedNetCashFlowValue(dateToCalculate, dailyPerformance.getCoalesceNetCashFlowNetCashFlowOverride()));
	}
}
