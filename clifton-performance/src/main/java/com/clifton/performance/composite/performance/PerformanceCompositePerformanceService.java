package com.clifton.performance.composite.performance;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositePerformanceService</code> interface defines methods for working with PerformanceComposite
 * and related objects.
 *
 * @author vgomelsky
 */
public interface PerformanceCompositePerformanceService {


	////////////////////////////////////////////////////////////////////////////////
	//////    Performance Composite Investment Account Performance Methods    //////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformance(int id);


	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformanceByAccountAndPeriod(Integer investmentAccountId, Integer accountingPeriodId, Integer calculatorBeanId);


	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(Integer performanceCompositeInvestmentAccountId, Integer accountingPeriodId);


	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceList(PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm);


	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceListByPerformanceComposite(short performanceCompositeId, Date fromDate, Date toDate);


	@DoNotAddRequestMapping
	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceByAccountNameAndEndDateSimilar(String accountName, Date endDate, String... otherAccountNames);


	public void savePerformanceCompositeInvestmentAccountPerformanceList(List<PerformanceCompositeInvestmentAccountPerformance> beanList);


	public PerformanceCompositeInvestmentAccountPerformance savePerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance bean);


	/**
	 * Used during processing to save both the PerformanceCompositeInvestmentAccountPerformance bean and it's sub-period details
	 * If not a new bean, will get the original sub-period details so properly inserts/updates/deletes where necessary
	 */
	@DoNotAddRequestMapping
	public PerformanceCompositeInvestmentAccountPerformance savePerformanceCompositeAccountMonthlyDailyPerformance(PerformanceCompositeInvestmentAccountPerformance bean, List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList);


	public void deletePerformanceCompositeInvestmentAccountPerformance(int id);


	////////////////////////////////////////////////////////////////////////////////
	/////   Performance Composite Investment Account Performance Daily Methods /////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccountDailyPerformance getPerformanceCompositeInvestmentAccountDailyPerformance(int id);


	public void savePerformanceCompositeInvestmentAccountDailyPerformanceList(List<PerformanceCompositeInvestmentAccountDailyPerformance> beanList);


	public List<PerformanceCompositeInvestmentAccountDailyPerformance> getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(Integer performanceCompositeInvestmentAccountPerformanceId);


	/**
	 * Note: This method is NOT exposed to the UI
	 * There is a save method in {@link com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService} that is exposed to the UI
	 * for saving daily overrides.  Changes to daily runs will automatically re-process the monthly performance
	 */
	@DoNotAddRequestMapping
	public PerformanceCompositeInvestmentAccountDailyPerformance savePerformanceCompositeInvestmentAccountDailyPerformance(PerformanceCompositeInvestmentAccountDailyPerformance bean);


	public void deletePerformanceCompositeInvestmentAccountDailyPerformance(int id);

	////////////////////////////////////////////////////////////////////////////////
	///////            Performance Composite Performance Methods             ///////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformance getPerformanceCompositePerformance(int id);


	public List<PerformanceCompositePerformance> getPerformanceCompositePerformanceListByPerformanceComposite(short performanceCompositeId);


	public List<PerformanceCompositePerformance> getPerformanceCompositePerformanceList(PerformanceCompositePerformanceSearchForm searchForm);


	public void savePerformanceCompositePerformanceList(List<PerformanceCompositePerformance> beanList);


	public PerformanceCompositePerformance savePerformanceCompositePerformance(PerformanceCompositePerformance bean);


	public void deletePerformanceCompositePerformance(int id);
}
