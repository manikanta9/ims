package com.clifton.performance.composite.setup;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>PerformanceCompositeRollup</code> class allows grouping multiple composites into a single roll up.
 * The same composite can belong to more than one rollup composite.  But a rollup cannot be both a rollup and a child
 *
 * @author vgomelsky
 */
public class PerformanceCompositeRollup extends ManyToManyEntity<PerformanceComposite, PerformanceComposite, Short> {
	// empty
}
