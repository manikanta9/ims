package com.clifton.performance.composite.performance.upload;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.document.setup.locator.DocumentFileFkFieldLocator;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * @author KellyJ
 */
public class DocumentFileBulkUploadAccountPerformanceLocatorBean implements DocumentFileFkFieldLocator {

	private String fileNamePattern;
	private String fileNamePatternMessage;
	private String patternListDelimitter;
	private String accountNamePatternList;
	private boolean accountNameIndexFromEnd;
	private String datePatternList;
	private boolean dateIndexFromEnd;
	private String fileNameFlexibleMatchReplaceFirst;

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;


	@Override
	public Long getDocumentFileFkFieldIdFromFileName(String fileName) {
		ValidationUtils.assertNotEmpty(fileName, "Must provide a file name.");
		ValidationUtils.assertTrue(Pattern.matches(getFileNamePattern(), fileName), String.format("Invalid file name [%s].  It must be in the following format: %s", fileName, getFileNamePatternMessage()));

		String[] parsedFileName = parseFileName(fileName);

		List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceList = null;
		if (parsedFileName != null && parsedFileName.length > 1) {
			String account = StringUtils.trim(parsedFileName[0]);
			String fileDate = parsedFileName[1];
			Date endDate = DateUtils.toDate(fileDate, DateUtils.FIX_DATE_FORMAT_INPUT);

			// First try to find by account number
			PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
			searchForm.setPeriodEndDate(endDate);
			searchForm.setInvestmentAccountNumber(account);
			accountPerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);

			// If it fails, try the account name
			if (CollectionUtils.isEmpty(accountPerformanceList)) {
				// First try to find exact matches
				searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
				searchForm.setPeriodEndDate(endDate);
				searchForm.setInvestmentAccountNames(getFileNamesToCheck(account));
				accountPerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);

				// If it fails to find an exact match, try similar matches.  We want to avoid too many variations at once to avoid returning multiple records.
				if (CollectionUtils.isEmpty(accountPerformanceList)) {
					accountPerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceByAccountNameAndEndDateSimilar(account, endDate, getFileNamesToCheck(account));
				}
			}
		}
		ValidationUtils.assertNotEmpty(accountPerformanceList, String.format("No Account Performance found for file [%s]", fileName));
		ValidationUtils.assertTrue(accountPerformanceList.size() == 1, String.format("More than one Account Performance found for file [%s]", fileName));
		return BeanUtils.getIdentityAsLong(CollectionUtils.getOnlyElement(accountPerformanceList));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Matches on exact matches of the following along with similar matches (using LIKE)
	 * (1) Account Name with no commas
	 * (2) Account Name with no 'The', 'A', or 'An' at the beginning
	 * (3) Account Name with no 'The', 'A', or 'An' at the beginning and no commas
	 *
	 * @param accountName
	 */
	protected String[] getFileNamesToCheck(String accountName) {
		Set<String> result = new HashSet<>();
		result.add(accountName);
		result.add(accountName.replaceAll(",", accountName));
		if (!StringUtils.isEmpty(getFileNameFlexibleMatchReplaceFirst())) {
			String noTheAAn = accountName.replaceFirst(getFileNameFlexibleMatchReplaceFirst(), StringUtils.EMPTY_STRING).trim();
			result.add(noTheAAn);
			result.add(noTheAAn.replaceAll(",", StringUtils.EMPTY_STRING).trim());
		}
		return result.stream().toArray(String[]::new);
	}


	/**
	 * Filename is in the format of <account>_<date>__.<ext>
	 *
	 * @param fileName
	 */
	protected String[] parseFileName(String fileName) {
		String[] result = null;
		try {
			String date = parse(fileName, getDatePatternList().split(getPatternListDelimitter()), isDateIndexFromEnd());
			String account = parse(fileName, getAccountNamePatternList().split(getPatternListDelimitter()), isAccountNameIndexFromEnd());
			result = new String[]{account, date};
		}
		catch (Exception e) {
			ValidationUtils.fail(String.format("Could not parse file name [%s].  It must be in the following format: %S", fileName, getFileNamePatternMessage()));
		}
		return result;
	}


	private String parse(String fileName, String[] patterns, boolean indexFromEnd) {
		String result = null;
		if (patterns.length > 1) {
			for (int i = 0; i < patterns.length; i++) {
				if (i == patterns.length - 1) {
					String[] parts = fileName.split(patterns[i]);
					result = indexFromEnd ? parts[parts.length - 1] : parts[0];
				}
				fileName = fileName.split(patterns[i])[0];
			}
		}
		else {
			String[] parts = fileName.split(patterns[0]);
			result = indexFromEnd ? parts[parts.length - 1] : parts[0];
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public String getFileNamePattern() {
		return this.fileNamePattern;
	}


	public void setFileNamePattern(String fileNamePattern) {
		this.fileNamePattern = fileNamePattern;
	}


	public String getFileNamePatternMessage() {
		return this.fileNamePatternMessage;
	}


	public void setFileNamePatternMessage(String fileNamePatternMessage) {
		this.fileNamePatternMessage = fileNamePatternMessage;
	}


	public String getPatternListDelimitter() {
		return this.patternListDelimitter;
	}


	public void setPatternListDelimitter(String patternListDelimitter) {
		this.patternListDelimitter = patternListDelimitter;
	}


	public String getAccountNamePatternList() {
		return this.accountNamePatternList;
	}


	public void setAccountNamePatternList(String accountNamePatternList) {
		this.accountNamePatternList = accountNamePatternList;
	}


	public boolean isAccountNameIndexFromEnd() {
		return this.accountNameIndexFromEnd;
	}


	public void setAccountNameIndexFromEnd(boolean accountNameIndexFromEnd) {
		this.accountNameIndexFromEnd = accountNameIndexFromEnd;
	}


	public String getDatePatternList() {
		return this.datePatternList;
	}


	public void setDatePatternList(String datePatternList) {
		this.datePatternList = datePatternList;
	}


	public boolean isDateIndexFromEnd() {
		return this.dateIndexFromEnd;
	}


	public void setDateIndexFromEnd(boolean dateIndexFromEnd) {
		this.dateIndexFromEnd = dateIndexFromEnd;
	}


	public String getFileNameFlexibleMatchReplaceFirst() {
		return this.fileNameFlexibleMatchReplaceFirst;
	}


	public void setFileNameFlexibleMatchReplaceFirst(String fileNameFlexibleMatchReplaceFirst) {
		this.fileNameFlexibleMatchReplaceFirst = fileNameFlexibleMatchReplaceFirst;
	}
}
