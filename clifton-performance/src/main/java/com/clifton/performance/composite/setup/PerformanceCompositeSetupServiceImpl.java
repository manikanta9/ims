package com.clifton.performance.composite.setup;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.setup.cache.PerformanceCompositeRollupCache;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class PerformanceCompositeSetupServiceImpl implements PerformanceCompositeSetupService {

	private AdvancedUpdatableDAO<PerformanceComposite, Criteria> performanceCompositeDAO;
	private AdvancedUpdatableDAO<PerformanceCompositeHistory, Criteria> performanceCompositeHistoryDAO;
	private AdvancedUpdatableDAO<PerformanceCompositeRollup, Criteria> performanceCompositeRollupDAO;
	private AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccount, Criteria> performanceCompositeInvestmentAccountDAO;

	private DaoSingleKeyListCache<PerformanceCompositeHistory, Short> performanceCompositeHistoryByCompositeCache;
	private PerformanceCompositeRollupCache performanceCompositeRollupCache;

	private AccountingPeriodService accountingPeriodService;

	////////////////////////////////////////////////////////////////////////////////
	///////////            Performance Composite Methods                   /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceComposite getPerformanceComposite(short id) {
		PerformanceComposite composite = getPerformanceCompositeDAO().findByPrimaryKey(id);
		populatePerformanceCompositeRollupChildList(composite);
		return composite;
	}


	@Override
	public List<PerformanceComposite> getPerformanceCompositeList(PerformanceCompositeSearchForm searchForm) {
		return getPerformanceCompositeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!"Both".equals(searchForm.getCompositeStatus()) && searchForm.getCompositeStatus() != null && !("".equals(searchForm.getCompositeStatus()))) {
					criteria.add(ActiveExpressionForDates.forActiveOnDateWithAliasAndProperties(("Active".equals(searchForm.getCompositeStatus())), null, "startDate", "endDate", new Date()));
				}
			}
		});
	}


	/**
	 * When looking for parent list, should use these methods as they make use of the cache on the PerformanceCompositeRollup table
	 */
	@Override
	public List<PerformanceComposite> getPerformanceCompositeParentList(short performanceCompositeId) {
		return getPerformanceCompositeListByParentOrChild(performanceCompositeId, true);
	}


	/**
	 * When looking child list, should use these methods as they make use of the cache on the PerformanceCompositeRollup table
	 */
	private List<PerformanceComposite> getPerformanceCompositeChildList(short performanceCompositeId) {
		return getPerformanceCompositeListByParentOrChild(performanceCompositeId, false);
	}


	/**
	 * Helper method for parent/child rollup look ups.  Cache stores full list where composite is a parent or a child, this method gets/sets from the cache and
	 * returns filtered list based on parent flag (if parentList = true, returns parents of given composite, else returns children)
	 */
	private List<PerformanceComposite> getPerformanceCompositeListByParentOrChild(final short performanceCompositeId, boolean parentList) {
		List<PerformanceCompositeRollup> rollupList = getPerformanceCompositeRollupListByParentOrChild(performanceCompositeId, parentList);
		if (!CollectionUtils.isEmpty(rollupList)) {
			return CollectionUtils.createList(BeanUtils.getPropertyValues(rollupList, (parentList ? PerformanceCompositeRollup::getReferenceOne : PerformanceCompositeRollup::getReferenceTwo), PerformanceComposite.class));
		}
		return null;
	}


	@Override
	@Transactional
	public PerformanceComposite savePerformanceComposite(PerformanceComposite bean) {
		// validation
		ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
		if (!bean.isNewBean()) {
			List<PerformanceCompositeInvestmentAccount> accountList = getPerformanceCompositeInvestmentAccountDAO().findByField("performanceComposite.id", bean.getId());
			for (PerformanceCompositeInvestmentAccount account : CollectionUtils.getIterable(accountList)) {
				ValidationUtils.assertFalse(account.getStartDate().before(bean.getStartDate()), "Account assignment Start Date " + DateUtils.fromDateSmart(account.getStartDate())
						+ " cannot be before composite Start Date of " + DateUtils.fromDateSmart(bean.getStartDate()));
			}
		}

		if (bean.isRollupComposite()) {
			ValidationUtils.assertNotEmpty(bean.getChildCompositeList(), "A rollup composite must have at least one child composite.");
			// Rollup Start dates are populated in the {@link com.clifton.performance.composite.setup.observers.PerformanceCompositeObserver}
			// If the value is blank, set it to the first one found and the observer will reset it
			if (bean.getStartDate() == null) {
				bean.setStartDate(bean.getChildCompositeList().get(0).getStartDate());
			}
			ValidationUtils.assertNull(bean.getModelFee(), "Model fee on rollup composites is not allowed.");
		}
		else {
			ValidationUtils.assertNotNull(bean.getStartDate(), "Start Date is required for a composite.", "startDate");
			ValidationUtils.assertNotNull(bean.getModelFee(), "Model Fee is required");
		}

		List<PerformanceCompositeRollup> rollupList = (bean.isNewBean() ? null : getPerformanceCompositeRollupDAO().findByField("referenceOne.id", bean.getId()));
		List<PerformanceCompositeRollup> newRollupList = new ArrayList<>();

		// Validate Children Composites
		List<PerformanceComposite> childCompositeList = bean.getChildCompositeList();
		if (bean.isRollupComposite()) {
			for (PerformanceComposite child : childCompositeList) {
				ValidationUtils.assertFalse(child.equals(bean), "A rollup composite cannot contain itself as a child.");
				ValidationUtils.assertFalse(child.isRollupComposite(), "Child composite [" + child.getName() + "] is invalid.  You cannot have children composites that are also rollup composites.");
				boolean found = false;
				for (PerformanceCompositeRollup rollup : CollectionUtils.getIterable(rollupList)) {
					if (rollup.getReferenceTwo().equals(child)) {
						found = true;
						newRollupList.add(rollup);
						break;
					}
				}
				if (!found) {
					PerformanceCompositeRollup rollup = new PerformanceCompositeRollup();
					rollup.setReferenceOne(bean);
					rollup.setReferenceTwo(child);
					newRollupList.add(rollup);
				}
			}
		}

		bean = getPerformanceCompositeDAO().save(bean);
		getPerformanceCompositeRollupDAO().saveList(newRollupList, rollupList);
		bean.setChildCompositeList(childCompositeList);
		return bean;
	}


	@Override
	@Transactional
	public void deletePerformanceComposite(short id) {
		PerformanceComposite composite = getPerformanceComposite(id);
		if (composite != null) {
			if (composite.isRollupComposite()) {
				getPerformanceCompositeRollupDAO().deleteList(getPerformanceCompositeRollupDAO().findByField("referenceOne.id", id));
			}
			getPerformanceCompositeDAO().delete(composite);
		}
	}


	private void populatePerformanceCompositeRollupChildList(PerformanceComposite parent) {
		if (parent != null && parent.isRollupComposite()) {
			parent.setChildCompositeList(getPerformanceCompositeChildList(parent.getId()));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////          Performance Composite History Methods                 ////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getModelFeeForPerformanceCompositeAndDate(PerformanceComposite performanceComposite, Date date) {
		List<PerformanceCompositeHistory> historyList = getPerformanceCompositeHistoryListForComposite(performanceComposite.getId());
		if (!CollectionUtils.isEmpty(historyList)) {
			PerformanceCompositeHistory history = CollectionUtils.getFirstElement(BeanUtils.filter(historyList, performanceCompositeHistory -> performanceCompositeHistory.isActiveOnDate(date)));
			if (history != null) {
				return history.getModelFee();
			}
		}
		return performanceComposite.getModelFee();
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositeHistory getPerformanceCompositeHistory(short id) {
		return getPerformanceCompositeHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceCompositeHistory> getPerformanceCompositeHistoryListForComposite(short performanceCompositeId) {
		return getPerformanceCompositeHistoryByCompositeCache().getBeanListForKeyValue(getPerformanceCompositeHistoryDAO(), performanceCompositeId);
	}


	@Transactional
	@Override
	public PerformanceCompositeHistory savePerformanceCompositeHistory(PerformanceCompositeHistory bean) {
		ValidationUtils.assertTrue(bean.isNewBean(), "History Records can only be inserted, not updated.");

		PerformanceCompositeHistory current = CollectionUtils.getFirstElement(BeanUtils.filter(getPerformanceCompositeHistoryListForComposite(bean.getPerformanceComposite().getId()), history -> history.getEndDate() == null));

		// If No Start Date, then updating an incorrectly entered model fee
		if (bean.getStartDate() == null) {
			PerformanceComposite composite = bean.getPerformanceComposite();
			composite.setModelFee(bean.getModelFee());
			savePerformanceComposite(composite);

			if (current != null) {
				current.setModelFee(bean.getModelFee());
				getPerformanceCompositeHistoryDAO().save(current);
			}
		}

		// Otherwise need to end existing model fee on start date - 1 and start new history on start date and update fee on the composite itself to the latest one.
		else {
			if (current == null) {
				current = new PerformanceCompositeHistory();
				current.setPerformanceComposite(bean.getPerformanceComposite());
				current.setModelFee(bean.getPerformanceComposite().getModelFee());
				current.setStartDate(bean.getPerformanceComposite().getStartDate());
			}
			current.setEndDate(DateUtils.addDays(bean.getStartDate(), -1));

			PerformanceComposite composite = bean.getPerformanceComposite();
			composite.setModelFee(bean.getModelFee());

			getPerformanceCompositeHistoryDAO().save(current);
			bean = getPerformanceCompositeHistoryDAO().save(bean);
			getPerformanceCompositeDAO().save(composite);
		}
		return bean;
	}


	@Override
	@Transactional
	public void deletePerformanceCompositeHistory(short id) {
		PerformanceCompositeHistory history = getPerformanceCompositeHistory(id);
		PerformanceComposite composite = history.getPerformanceComposite();

		if (history.getEndDate() != null) {
			throw new ValidationException("You can only delete the latest history record for Performance Composites.");
		}

		getPerformanceCompositeHistoryDAO().delete(id);

		// Get the Previous Record to this one so we can clear end date and update model fee on the composite itself to that one
		PerformanceCompositeHistory previousHistory = CollectionUtils.getFirstElement(BeanUtils.filter(getPerformanceCompositeHistoryListForComposite(history.getPerformanceComposite().getId()), compositeHistory -> DateUtils.isEqualWithoutTime(compositeHistory.getEndDate(), DateUtils.addDays(history.getStartDate(), -1))));
		if (previousHistory != null) {
			previousHistory.setEndDate(null);
			composite.setModelFee(previousHistory.getModelFee());

			getPerformanceCompositeHistoryDAO().save(previousHistory);
			getPerformanceCompositeDAO().save(composite);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	//////              Performance Composite Roll Up Methods               ////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Helper method for parent/child rollup look ups.  Cache stores full list where composite is a parent or a child, this method gets/sets from the cache and
	 * returns filtered list based on parent flag (if parentList = true, returns parents of given composite, else returns children)
	 */
	private List<PerformanceCompositeRollup> getPerformanceCompositeRollupListByParentOrChild(final short performanceCompositeId, boolean parentList) {
		List<PerformanceCompositeRollup> rollupList = null;
		Short[] rollupIds = getPerformanceCompositeRollupCache().getPerformanceCompositeRollupList(performanceCompositeId);
		// Nothing cached yet - look it up
		if (rollupIds == null) {
			rollupList = getPerformanceCompositeRollupDAO().findBySearchCriteria((HibernateSearchConfigurer) criteria -> criteria.add(Restrictions.or(Restrictions.eq("referenceOne.id", performanceCompositeId), Restrictions.eq("referenceTwo.id", performanceCompositeId))));
			getPerformanceCompositeRollupCache().storePerformanceCompositeRollupList(performanceCompositeId, rollupList);
		}
		// Otherwise lookup rollups by id to get real list (only if length of array > 0
		else if (rollupIds.length > 0) {
			rollupList = getPerformanceCompositeRollupDAO().findByPrimaryKeys(rollupIds);
		}
		// Filter out the list where composite = parent or = child depending on which one looking for.
		if (!CollectionUtils.isEmpty(rollupList)) {
			rollupList = BeanUtils.filter(rollupList, performanceCompositeRollup -> (parentList ? performanceCompositeRollup.getReferenceTwo().getId() : performanceCompositeRollup.getReferenceOne().getId()), performanceCompositeId);
		}
		return rollupList;
	}

	////////////////////////////////////////////////////////////////////////////
	/////         Performance Composite Investment Account Methods         /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositeInvestmentAccount getPerformanceCompositeInvestmentAccount(int id) {
		return getPerformanceCompositeInvestmentAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeAccountListByComposite(short performanceCompositeId) {
		PerformanceComposite performanceComposite = getPerformanceComposite(performanceCompositeId);
		PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		if (performanceComposite.isRollupComposite()) {
			searchForm.setRollupPerformanceCompositeId(performanceComposite.getId());
		}
		else {
			searchForm.setPerformanceCompositeId(performanceComposite.getId());
		}
		return getPerformanceCompositeInvestmentAccountList(searchForm);
	}


	@Override
	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(int investmentAccountId, int accountingPeriodId) {
		PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setAccountingPeriodId(accountingPeriodId);
		return getPerformanceCompositeInvestmentAccountList(searchForm);
	}


	@Override
	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeInvestmentAccountList(PerformanceCompositeInvestmentAccountSearchForm searchForm) {
		// If searching for a specific assignment valid for an accounting period set active on date range filters
		if (searchForm.getAccountingPeriodId() != null) {
			AccountingPeriod accountingPeriod = getAccountingPeriodService().getAccountingPeriod(searchForm.getAccountingPeriodId());
			searchForm.setActiveOnDateRangeStartDate(accountingPeriod.getStartDate());
			searchForm.setActiveOnDateRangeEndDate(accountingPeriod.getEndDate());
		}

		if (searchForm.getRollupPerformanceCompositeId() != null) {
			PerformanceComposite composite = getPerformanceComposite(searchForm.getRollupPerformanceCompositeId());
			searchForm.setPerformanceCompositeIds(BeanUtils.getBeanIdentityArray(composite.getChildCompositeList(), Short.class));
		}

		return getPerformanceCompositeInvestmentAccountDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * @see com.clifton.performance.composite.setup.observers.PerformanceCompositeInvestmentAccountValidator for validation
	 */
	@Override
	public PerformanceCompositeInvestmentAccount savePerformanceCompositeInvestmentAccount(PerformanceCompositeInvestmentAccount bean) {
		return getPerformanceCompositeInvestmentAccountDAO().save(bean);
	}


	@Override
	public void deletePerformanceCompositeInvestmentAccount(int id) {
		getPerformanceCompositeInvestmentAccountDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformanceComposite, Criteria> getPerformanceCompositeDAO() {
		return this.performanceCompositeDAO;
	}


	public void setPerformanceCompositeDAO(AdvancedUpdatableDAO<PerformanceComposite, Criteria> performanceCompositeDAO) {
		this.performanceCompositeDAO = performanceCompositeDAO;
	}


	public AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccount, Criteria> getPerformanceCompositeInvestmentAccountDAO() {
		return this.performanceCompositeInvestmentAccountDAO;
	}


	public void setPerformanceCompositeInvestmentAccountDAO(AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccount, Criteria> performanceCompositeInvestmentAccountDAO) {
		this.performanceCompositeInvestmentAccountDAO = performanceCompositeInvestmentAccountDAO;
	}


	public AdvancedUpdatableDAO<PerformanceCompositeRollup, Criteria> getPerformanceCompositeRollupDAO() {
		return this.performanceCompositeRollupDAO;
	}


	public void setPerformanceCompositeRollupDAO(AdvancedUpdatableDAO<PerformanceCompositeRollup, Criteria> performanceCompositeRollupDAO) {
		this.performanceCompositeRollupDAO = performanceCompositeRollupDAO;
	}


	public PerformanceCompositeRollupCache getPerformanceCompositeRollupCache() {
		return this.performanceCompositeRollupCache;
	}


	public void setPerformanceCompositeRollupCache(PerformanceCompositeRollupCache performanceCompositeRollupCache) {
		this.performanceCompositeRollupCache = performanceCompositeRollupCache;
	}


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public AdvancedUpdatableDAO<PerformanceCompositeHistory, Criteria> getPerformanceCompositeHistoryDAO() {
		return this.performanceCompositeHistoryDAO;
	}


	public void setPerformanceCompositeHistoryDAO(AdvancedUpdatableDAO<PerformanceCompositeHistory, Criteria> performanceCompositeHistoryDAO) {
		this.performanceCompositeHistoryDAO = performanceCompositeHistoryDAO;
	}


	public DaoSingleKeyListCache<PerformanceCompositeHistory, Short> getPerformanceCompositeHistoryByCompositeCache() {
		return this.performanceCompositeHistoryByCompositeCache;
	}


	public void setPerformanceCompositeHistoryByCompositeCache(DaoSingleKeyListCache<PerformanceCompositeHistory, Short> performanceCompositeHistoryByCompositeCache) {
		this.performanceCompositeHistoryByCompositeCache = performanceCompositeHistoryByCompositeCache;
	}
}
