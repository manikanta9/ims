package com.clifton.performance.composite.performance.metric;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceMetricCalculatorTypes</code> are various calculations used in
 * determining performance metrics
 *
 * @author apopp
 */
public enum PerformanceMetricCalculatorTypes {

	SHARPE_RATIO() {
		@Override
		public <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList) {
			ValidationUtils.assertNotNull(metricType.getComplexMetricFieldType(), "Sharp ratio requires complex metric field definition");
			ValidationUtils.assertNotNull(subValueList, "Sharpe ratio requires at least one entity for processing");
			PerformanceCompositeMetric compositeMetric = CollectionUtils.getObjectAtIndex(subValueList, CollectionUtils.getSize(subValueList) - 1);

			BigDecimal value = (BigDecimal) BeanUtils.getPropertyValue(compositeMetric, metricType.getDependentFieldType().getBeanPropertyName());
			BigDecimal riskFreeReturn = (BigDecimal) BeanUtils.getPropertyValue(compositeMetric, metricType.getBeanPropertyNameForComplexCalculation());
			BigDecimal standardDeviation = (BigDecimal) BeanUtils.getPropertyValue(compositeMetric, metricType.getComplexMetricFieldType().getBeanPropertyName());

			if (MathUtils.isNullOrZero(standardDeviation)) {
				standardDeviation = BigDecimal.ONE;
			}

			return MathUtils.sharpeRatio(value, riskFreeReturn, standardDeviation);
		}
	},
	STANDARD_DEVIATION() {
		@Override
		public <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList) {
			BigDecimal[] values = BeanUtils.getPropertyValues(subValueList, metricType.getDependentFieldType().getBeanPropertyName(), BigDecimal.class);
			return CoreMathUtils.standardDeviation(Arrays.asList(values));
		}
	},
	STANDARD_DEVIATION_ANNUALIZED() {
		@Override
		public <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList) {
			BigDecimal[] values = BeanUtils.getPropertyValues(subValueList, metricType.getDependentFieldType().getBeanPropertyName(), BigDecimal.class);
			return MathUtils.multiply(CoreMathUtils.standardDeviation(Arrays.asList(values)), MathUtils.sqrt(new BigDecimal(12)));
		}
	},
	TOTAL_PERCENT_CHANGE() {
		@Override
		public <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList) {
			return CoreMathUtils.getTotalPercentChange(subValueList, metric -> (BigDecimal) BeanUtils.getPropertyValue(metric, metricType.getDependentFieldType().getBeanPropertyName()), true);
		}
	},
	TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_DAILY() {
		@Override
		public <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList) {
			if (subValueList == null || subValueList.size() < 2) {
				return TOTAL_PERCENT_CHANGE.calculateValue(metricType, subValueList);
			}

			//All ITD calc are annualized daily except for composite performance
			if (CollectionUtils.getFirstElement(subValueList) instanceof PerformanceCompositePerformance) {
				return TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_MONTHLY.calculateValue(metricType, subValueList);
			}

			Date period = subValueList.get(subValueList.size() - 1).getAccountingPeriod().getEndDate();
			Integer totalDays = DateUtils.getDaysDifferenceInclusive(period, determineStartDate(subValueList.get(0)));
			boolean leap = DateUtils.getMonthOfYear(period) == 2 && DateUtils.getDayOfMonth(period) == 29;

			//Full year or leap year
			if (totalDays <= (leap ? 366 : 365)) {
				return TOTAL_PERCENT_CHANGE.calculateValue(metricType, subValueList);
			}

			return CoreMathUtils.getTotalPercentChangeAnnualizedDaily(subValueList, metric -> (BigDecimal) BeanUtils.getPropertyValue(metric, metricType.getDependentFieldType().getBeanPropertyName()), true, totalDays);
		}
	},
	TOTAL_PERCENT_CHANGE_CHECK_ANNUALIZED_MONTHLY() {
		@Override
		public <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList) {
			if (subValueList == null || subValueList.size() < 2) {
				return TOTAL_PERCENT_CHANGE.calculateValue(metricType, subValueList);
			}

			Integer totalMonths = DateUtils.getMonthsDifference(subValueList.get(subValueList.size() - 1).getAccountingPeriod().getEndDate(), determineStartDate(subValueList.get(0))) + 1;

			if (totalMonths < 12) {
				return TOTAL_PERCENT_CHANGE.calculateValue(metricType, subValueList);
			}

			return CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(subValueList, metric -> (BigDecimal) BeanUtils.getPropertyValue(metric, metricType.getDependentFieldType().getBeanPropertyName()), true, totalMonths);
		}
	};


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract <T extends PerformanceCompositeMetric> BigDecimal calculateValue(PerformanceMetricFieldTypes metricType, List<T> subValueList);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Determine start date for total days count.  If inception date is equal to the start date of the first entity,
	 * then we must base the total days off inception date. This is because it is a partial accounting period
	 */

	private static Date determineStartDate(PerformanceCompositeMetric startAccountPerformance) {
		if (DateUtils.isDateBetween(startAccountPerformance.getStartDate(), startAccountPerformance.getAccountingPeriod().getStartDate(), startAccountPerformance.getAccountingPeriod()
				.getEndDate(), false)) {
			return startAccountPerformance.getStartDate();
		}

		return startAccountPerformance.getAccountingPeriod().getStartDate();
	}
}
