package com.clifton.performance.composite.performance.upload;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class PerformanceCompositeUploadServiceImpl implements PerformanceCompositeUploadService {

	private AccountingPeriodService accountingPeriodService;

	private InvestmentAccountService investmentAccountService;

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;

	private PerformanceCompositeSetupService performanceCompositeSetupService;

	private SystemUploadHandler systemUploadHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void uploadPerformanceCompositeInvestmentAccountPerformanceUploadFile(PerformanceCompositeAccountUploadCommand uploadCommand) {
		Map<Integer, PerformanceCompositeAccountRebuildCommand> accountProcessCommandMap = new HashMap<>();
		uploadPerformanceCompositeInvestmentAccountPerformanceUploadFileImpl(uploadCommand, accountProcessCommandMap);
		processPerformanceCompositeInvestmentAccountPerformanceUploadFile(uploadCommand, accountProcessCommandMap);
	}


	/**
	 * The upload of the data in the file is handled within one transaction, then the asynchronous call to rebuild performance is handled separately
	 */
	@Transactional
	private void uploadPerformanceCompositeInvestmentAccountPerformanceUploadFileImpl(PerformanceCompositeAccountUploadCommand uploadCommand, Map<Integer, PerformanceCompositeAccountRebuildCommand> accountProcessCommandMap) {
		uploadCommand.setSimple(true);
		// Don't perform dao lookups because we want to try to figure out the assignment that is being used automatically if possible
		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, false);

		Map<Date, AccountingPeriod> accountingPeriodDateMap = new HashMap<>();
		int skipCount = 0;
		for (IdentityObject bean : CollectionUtils.getIterable(beanList)) {
			PerformanceCompositeInvestmentAccountPerformance accountPerformance = (PerformanceCompositeInvestmentAccountPerformance) bean;
			// Apply the Accounting Period
			applyAccountingPeriodToAccountPerformance(accountPerformance, accountingPeriodDateMap);
			// Apply the Assignment
			applyAssignmentToAccountPerformance(accountPerformance);

			// See if this is replacing existing performance record, if so update the id on the new record
			if (FileUploadExistingBeanActions.INSERT != uploadCommand.getExistingBeans()) {
				PerformanceCompositeInvestmentAccountPerformance existingPerformance = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(accountPerformance.getPerformanceCompositeInvestmentAccount().getId(), accountPerformance.getAccountingPeriod().getId());
				if (existingPerformance != null) {
					if (FileUploadExistingBeanActions.SKIP == uploadCommand.getExistingBeans()) {
						skipCount++;
						continue;
					}
					accountPerformance.setId(existingPerformance.getId());
				}
			}

			if (uploadCommand.isDeleteDailyPerformance()) {
				getPerformanceCompositePerformanceService().savePerformanceCompositeAccountMonthlyDailyPerformance(accountPerformance, null);
			}
			else {
				getPerformanceCompositePerformanceService().savePerformanceCompositeInvestmentAccountPerformance(accountPerformance);
			}
			if (uploadCommand.isProcessPerformance()) {
				PerformanceCompositeAccountRebuildCommand accountRebuildCommand;
				if (accountProcessCommandMap.containsKey(accountPerformance.getClientAccount().getId())) {
					accountRebuildCommand = accountProcessCommandMap.get(accountPerformance.getClientAccount().getId());
					if (DateUtils.compare(accountPerformance.getMeasureDate(), accountRebuildCommand.getFromAccountingPeriod().getEndDate(), false) < 0) {
						accountRebuildCommand.setFromAccountingPeriod(accountPerformance.getAccountingPeriod());
					}
					else if (DateUtils.compare(accountPerformance.getMeasureDate(), accountRebuildCommand.getToAccountingPeriod().getEndDate(), false) > 0) {
						accountRebuildCommand.setToAccountingPeriod(accountPerformance.getAccountingPeriod());
					}
				}
				else {
					accountRebuildCommand = new PerformanceCompositeAccountRebuildCommand();
					accountRebuildCommand.setCalculateAccountDailyMonthlyBasePerformance(false);
					accountRebuildCommand.setInvestmentAccountId(accountPerformance.getClientAccount().getId());
					accountRebuildCommand.setFromAccountingPeriod(accountPerformance.getAccountingPeriod());
					accountRebuildCommand.setToAccountingPeriod(accountPerformance.getAccountingPeriod());
				}
				accountProcessCommandMap.put(accountPerformance.getClientAccount().getId(), accountRebuildCommand);
			}
		}

		if (skipCount > 0) {
			uploadCommand.getUploadResult().addUploadResultsSkipped("PerformanceCompositeInvestmentAccountPerformance", skipCount, null);
		}
		uploadCommand.getUploadResult().addUploadResults("PerformanceCompositeInvestmentAccountPerformance", CollectionUtils.getSize(beanList) - skipCount, true);
	}


	private void processPerformanceCompositeInvestmentAccountPerformanceUploadFile(PerformanceCompositeAccountUploadCommand uploadCommand, Map<Integer, PerformanceCompositeAccountRebuildCommand> accountProcessCommandMap) {
		if (uploadCommand.isProcessPerformance() && !CollectionUtils.isEmpty(accountProcessCommandMap)) {
			// Run each accounts processing: Asynchronously
			for (Map.Entry<Integer, PerformanceCompositeAccountRebuildCommand> integerPerformanceCompositeAccountRebuildCommandEntry : accountProcessCommandMap.entrySet()) {
				getPerformanceCompositePerformanceRebuildService().rebuildPerformanceCompositeAccountPerformance(integerPerformanceCompositeAccountRebuildCommandEntry.getValue());
			}
			uploadCommand.getUploadResult().addUploadResults("Performance Processing", "Account Re-Processing started and will be completed shortly.  Please check the Composite Account Performance Workflow administration tab for current status.");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void applyAccountingPeriodToAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance, Map<Date, AccountingPeriod> accountingPeriodDateMap) {
		if (accountPerformance.getAccountingPeriod() == null || accountPerformance.getAccountingPeriod().getEndDate() == null) {
			throw new ValidationException("Accounting Period is missing. Please make sure AccountingPeriod-EndDate is supplied");
		}
		Date endDate = accountPerformance.getAccountingPeriod().getEndDate();
		if (accountingPeriodDateMap.containsKey(endDate)) {
			accountPerformance.setAccountingPeriod(accountingPeriodDateMap.get(endDate));
		}
		else {
			// Note: Throws exception if missing
			AccountingPeriod accountingPeriod = getAccountingPeriodService().getAccountingPeriodForDate(endDate);
			accountingPeriodDateMap.put(endDate, accountingPeriod);
			accountPerformance.setAccountingPeriod(accountingPeriod);
		}
	}


	private void applyAssignmentToAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		// Look up the Account
		InvestmentAccount investmentAccount = getInvestmentAccountForAccountPerformance(accountPerformance);
		// Find the assignment that is active on the accounting period for the account
		String compositeName = (accountPerformance.getPerformanceCompositeInvestmentAccount().getPerformanceComposite() != null) ? accountPerformance.getPerformanceCompositeInvestmentAccount().getPerformanceComposite().getName() : null;
		PerformanceCompositeInvestmentAccountSearchForm assignmentSearchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		assignmentSearchForm.setInvestmentAccountId(investmentAccount.getId());
		assignmentSearchForm.setAccountingPeriodId(accountPerformance.getAccountingPeriod().getId());
		if (!StringUtils.isEmpty(compositeName)) {
			assignmentSearchForm.setCompositeNameEquals(compositeName);
		}
		List<PerformanceCompositeInvestmentAccount> accountAssignmentList = getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(assignmentSearchForm);
		if (CollectionUtils.getSize(accountAssignmentList) == 0) {
			throw new ValidationException("There are no composite assignments for account " + investmentAccount.getLabel() + (!StringUtils.isEmpty(compositeName) ? " and composite " + compositeName : "") + " active during accounting period " + accountPerformance.getAccountingPeriod().getLabel());
		}
		if (CollectionUtils.getSize(accountAssignmentList) > 1) {
			throw new ValidationException("Found " + accountAssignmentList.size() + " composite assignments for account " + investmentAccount.getLabel() + (!StringUtils.isEmpty(compositeName) ? " and composite " + compositeName : "") + " active during accounting period " + accountPerformance.getAccountingPeriod().getLabel() + ". Please make sure a composite name is entered, or check assignment setup.");
		}
		accountPerformance.setPerformanceCompositeInvestmentAccount(accountAssignmentList.get(0));
	}


	private InvestmentAccount getInvestmentAccountForAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		if (accountPerformance.getPerformanceCompositeInvestmentAccount() == null || accountPerformance.getPerformanceCompositeInvestmentAccount().getInvestmentAccount() == null || StringUtils.isEmpty(accountPerformance.getPerformanceCompositeInvestmentAccount().getInvestmentAccount().getNumber())) {
			throw new ValidationException("Account Number is required.");
		}
		InvestmentAccount account = accountPerformance.getPerformanceCompositeInvestmentAccount().getInvestmentAccount();
		String searchString = "Account #: " + account.getNumber() + ", Our Account = true";
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setOurAccount(true);
		searchForm.setNumberEquals(account.getNumber());
		if (account.getIssuingCompany() != null) {
			if (!StringUtils.isEmpty(account.getIssuingCompany().getName())) {
				searchString = ", Issuing Company: " + account.getIssuingCompany().getName();
				searchForm.setIssuingCompanyName(accountPerformance.getPerformanceCompositeInvestmentAccount().getInvestmentAccount().getIssuingCompany().getName());
			}
			if (account.getIssuingCompany().getType() != null && !StringUtils.isEmpty(account.getIssuingCompany().getType().getName())) {
				searchString = ", Issuing Company Type: " + account.getIssuingCompany().getType().getName();
				searchForm.setIssuingCompanyTypeName(accountPerformance.getPerformanceCompositeInvestmentAccount().getInvestmentAccount().getIssuingCompany().getType().getName());
			}
		}
		List<InvestmentAccount> accountList = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		int size = CollectionUtils.getSize(accountList);
		if (size == 0) {
			throw new ValidationException("Cannot find any accounts that match the following search: " + searchString);
		}
		else if (size > 1) {
			throw new ValidationException("Found find any accounts that match the following search: " + searchString);
		}
		return accountList.get(0);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods             ///////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriodService getAccountingPeriodService() {
		return this.accountingPeriodService;
	}


	public void setAccountingPeriodService(AccountingPeriodService accountingPeriodService) {
		this.accountingPeriodService = accountingPeriodService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceCompositePerformanceRebuildService getPerformanceCompositePerformanceRebuildService() {
		return this.performanceCompositePerformanceRebuildService;
	}


	public void setPerformanceCompositePerformanceRebuildService(PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService) {
		this.performanceCompositePerformanceRebuildService = performanceCompositePerformanceRebuildService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
