package com.clifton.performance.composite.performance;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.metric.search.BasePerformanceCompositeMetricSearchFormConfigurer;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import org.hibernate.Criteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class PerformanceCompositePerformanceServiceImpl implements PerformanceCompositePerformanceService {

	private AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccountPerformance, Criteria> performanceCompositeInvestmentAccountPerformanceDAO;
	private AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccountDailyPerformance, Criteria> performanceCompositeInvestmentAccountDailyPerformanceDAO;
	private AdvancedUpdatableDAO<PerformanceCompositePerformance, Criteria> performanceCompositePerformanceDAO;

	private PerformanceCompositeSetupService performanceCompositeSetupService;

	private SecurityAuthorizationService securityAuthorizationService;

	private WorkflowDefinitionService workflowDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	/////   Performance Composite Investment Account Performance Methods   /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformance(int id) {
		return getPerformanceCompositeInvestmentAccountPerformanceDAO().findByPrimaryKey(id);
	}


	@Override
	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformanceByAccountAndPeriod(Integer investmentAccountId, Integer accountingPeriodId, Integer calculatorBeanId) {
		PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
		searchForm.setInvestmentAccountId(investmentAccountId);
		searchForm.setAccountingPeriodId(accountingPeriodId);
		searchForm.setAccountPerformanceCalculatorBeanId(calculatorBeanId);
		return CollectionUtils.getOnlyElement(getPerformanceCompositeInvestmentAccountPerformanceList(searchForm));
	}


	@Override
	public PerformanceCompositeInvestmentAccountPerformance getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(Integer performanceCompositeInvestmentAccountId, Integer accountingPeriodId) {
		return getPerformanceCompositeInvestmentAccountPerformanceDAO().findOneByFields(new String[]{"performanceCompositeInvestmentAccount.id", "accountingPeriod.id"}, new Object[]{performanceCompositeInvestmentAccountId, accountingPeriodId});
	}


	/**
	 * Finds an account performance by the accounting period end date and using various forms of the account name
	 * (1) like on the account name (anywhere)
	 * (2) like on the supplied account name against characters removed from the persistent source
	 * (3) like on the supplied other account names against characters removed from the persistent source
	 *
	 * @param accountName
	 * @param endDate
	 */
	@Override
	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceByAccountNameAndEndDateSimilar(String accountName, Date endDate, String... otherAccountNames) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			String replaceSqlPart = "replace(replace({alias}.accountName, '''', ''), ',', '') like ?";
			Criteria investmentAccountAlias = criteria.createCriteria("investmentAccount", "ia");
			criteria.createAlias("accountingPeriod", "ap");
			criteria.add(Restrictions.eq("ap.endDate", endDate));

			Disjunction or = Restrictions.disjunction();
			or.add(Restrictions.like("ia.name", accountName, MatchMode.ANYWHERE));
			or.add(Restrictions.sqlRestriction(replaceSqlPart, String.format("%%%s%%", accountName), StringType.INSTANCE));
			if (!ArrayUtils.isEmpty(otherAccountNames)) {
				for (String otherAccountName : otherAccountNames) {
					or.add(Restrictions.sqlRestriction(replaceSqlPart, String.format("%%%s%%", otherAccountName), StringType.INSTANCE));
				}
			}
			investmentAccountAlias.add(or);
		};
		return getPerformanceCompositeInvestmentAccountPerformanceDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceList(final PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm) {
		// If sorting not defined, default to account asc, end date desc
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("investmentAccountId:asc#periodEndDate:desc");
		}

		if (searchForm.getPerformanceCompositeOrRollupId() != null) {
			PerformanceComposite composite = getPerformanceCompositeSetupService().getPerformanceComposite(searchForm.getPerformanceCompositeOrRollupId());
			// If not a roll up then look for direct assignment only
			if (!composite.isRollupComposite()) {
				searchForm.setPerformanceCompositeId(searchForm.getPerformanceCompositeOrRollupId());
			}
			// Otherwise add in all of its children (note: rollups of rollups are not supported)
			else {
				searchForm.setPerformanceCompositeIds(BeanUtils.getBeanIdentityArray(composite.getChildCompositeList(), Short.class));
			}
		}
		return getPerformanceCompositeInvestmentAccountPerformanceDAO().findBySearchCriteria(new BasePerformanceCompositeMetricSearchFormConfigurer(searchForm, getWorkflowDefinitionService(), getSecurityAuthorizationService()));
	}


	@Override
	public List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceListByPerformanceComposite(short performanceCompositeId,
	                                                                                                                                            Date fromDate, Date toDate) {
		List<PerformanceCompositeInvestmentAccountPerformance> accountPerformanceList = new ArrayList<>();
		PerformanceComposite performanceComposite = getPerformanceCompositeSetupService().getPerformanceComposite(performanceCompositeId);

		if (performanceComposite.isRollupComposite()) {
			for (PerformanceComposite childComposite : CollectionUtils.getIterable(performanceComposite.getChildCompositeList())) {
				accountPerformanceList.addAll(getPerformanceCompositeInvestmentAccountPerformanceListByPerformanceComposite(childComposite.getId(), fromDate, toDate));
			}
		}
		else {
			accountPerformanceList = getPerformanceCompositeInvestmentAccountPerformanceListByPerformanceComposite(performanceComposite.getId(), fromDate, toDate);
		}
		// Ensure sorted order
		return BeanUtils.sortWithFunctions(accountPerformanceList, CollectionUtils.createList(
				accountPerformance -> accountPerformance.getAccountingPeriod().getStartDate(),
				accountPerformance -> accountPerformance.getClientAccount().getId()),
				CollectionUtils.createList(true, true));
	}


	private List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceListByPerformanceComposite(Short performanceCompositeId, Date fromDate,
	                                                                                                                                             Date toDate) {
		// Pull all assignments for the composite during the date range
		PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		searchForm.setPerformanceCompositeId(performanceCompositeId);
		searchForm.setActiveOnDateRangeStartDate(fromDate);
		searchForm.setActiveOnDateRangeEndDate(toDate);
		List<PerformanceCompositeInvestmentAccount> accountAssignmentList = getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(searchForm);

		if (CollectionUtils.isEmpty(accountAssignmentList)) {
			return new ArrayList<>();
		}

		// Pull all performance for those assignments
		PerformanceCompositeInvestmentAccountPerformanceSearchForm performanceSearchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
		performanceSearchForm.setPerformanceCompositeInvestmentAccountIds(BeanUtils.getBeanIdentityArray(accountAssignmentList, Integer.class));
		performanceSearchForm.setAfterPeriodStartDate(fromDate);
		performanceSearchForm.setBeforePeriodEndDate(toDate);
		return getPerformanceCompositeInvestmentAccountPerformanceList(performanceSearchForm);
	}


	@Override
	public PerformanceCompositeInvestmentAccountPerformance savePerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance bean) {
		// Validate the assignment is active on the accounting period
		ValidationUtils.assertTrue(DateUtils.isOverlapInDates(bean.getAccountingPeriod().getStartDate(), bean.getAccountingPeriod().getEndDate(), bean.getPerformanceCompositeInvestmentAccount().getStartDate(), bean.getPerformanceCompositeInvestmentAccount().getEndDate()), "Composite Assignment selected is not active during accounting period.");
		return getPerformanceCompositeInvestmentAccountPerformanceDAO().save(bean);
	}


	@Override
	@Transactional
	public PerformanceCompositeInvestmentAccountPerformance savePerformanceCompositeAccountMonthlyDailyPerformance(PerformanceCompositeInvestmentAccountPerformance bean, List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList) {
		List<PerformanceCompositeInvestmentAccountDailyPerformance> originalDailyPerformanceList = (bean.isNewBean() ? null : getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(bean.getId()));
		bean = getPerformanceCompositeInvestmentAccountPerformanceDAO().save(bean);
		getPerformanceCompositeInvestmentAccountDailyPerformanceDAO().saveList(dailyPerformanceList, originalDailyPerformanceList);
		return bean;
	}


	@Override
	public void savePerformanceCompositeInvestmentAccountPerformanceList(List<PerformanceCompositeInvestmentAccountPerformance> beanList) {
		getPerformanceCompositeInvestmentAccountPerformanceDAO().saveList(beanList);
	}


	@Override
	@Transactional
	public void deletePerformanceCompositeInvestmentAccountPerformance(int id) {
		getPerformanceCompositeInvestmentAccountDailyPerformanceDAO().deleteList(getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(id));
		getPerformanceCompositeInvestmentAccountPerformanceDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////  Performance Composite Investment Account Performance Daily Methods  /////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositeInvestmentAccountDailyPerformance getPerformanceCompositeInvestmentAccountDailyPerformance(int id) {
		return getPerformanceCompositeInvestmentAccountDailyPerformanceDAO().findByPrimaryKey(id);
	}


	@Override
	@Transactional
	public void savePerformanceCompositeInvestmentAccountDailyPerformanceList(List<PerformanceCompositeInvestmentAccountDailyPerformance> beanList) {
		for (PerformanceCompositeInvestmentAccountDailyPerformance bean : CollectionUtils.getIterable(beanList)) {
			savePerformanceCompositeInvestmentAccountDailyPerformance(bean);
		}
	}


	@Override
	public List<PerformanceCompositeInvestmentAccountDailyPerformance> getPerformanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance(Integer performanceCompositeInvestmentAccountPerformanceId) {
		return getPerformanceCompositeInvestmentAccountDailyPerformanceDAO().findByField("performanceCompositeInvestmentAccountPerformance.id", performanceCompositeInvestmentAccountPerformanceId);
	}


	@Override
	public PerformanceCompositeInvestmentAccountDailyPerformance savePerformanceCompositeInvestmentAccountDailyPerformance(PerformanceCompositeInvestmentAccountDailyPerformance bean) {
		ValidationUtils.assertTrue(bean.getPerformanceCompositeInvestmentAccountPerformance().isUpdatable(), "Cannot update daily performance because parent monthly performance is not editable. Please move parent performance to draft to edit daily performance.");
		return getPerformanceCompositeInvestmentAccountDailyPerformanceDAO().save(bean);
	}


	@Override
	public void deletePerformanceCompositeInvestmentAccountDailyPerformance(int id) {
		getPerformanceCompositeInvestmentAccountDailyPerformanceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////            Performance Composite Performance Methods             /////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceCompositePerformance getPerformanceCompositePerformance(int id) {
		return getPerformanceCompositePerformanceDAO().findByPrimaryKey(id);
	}


	@Override
	public List<PerformanceCompositePerformance> getPerformanceCompositePerformanceListByPerformanceComposite(short performanceCompositeId) {
		PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
		searchForm.setPerformanceCompositeId(performanceCompositeId);
		return getPerformanceCompositePerformanceList(searchForm);
	}


	@Override
	public List<PerformanceCompositePerformance> getPerformanceCompositePerformanceList(PerformanceCompositePerformanceSearchForm searchForm) {
		// If sorting not defined, default to composite asc, end date desc
		if (searchForm.getOrderBy() == null) {
			searchForm.setOrderBy("performanceCompositeName:asc#periodEndDate:desc");
		}

		return getPerformanceCompositePerformanceDAO().findBySearchCriteria(new BasePerformanceCompositeMetricSearchFormConfigurer(searchForm, getWorkflowDefinitionService(), getSecurityAuthorizationService()));
	}


	@Override
	public void savePerformanceCompositePerformanceList(List<PerformanceCompositePerformance> beanList) {
		getPerformanceCompositePerformanceDAO().saveList(beanList);
	}


	@Override
	public PerformanceCompositePerformance savePerformanceCompositePerformance(PerformanceCompositePerformance bean) {
		return getPerformanceCompositePerformanceDAO().save(bean);
	}


	@Override
	public void deletePerformanceCompositePerformance(int id) {
		getPerformanceCompositePerformanceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccountPerformance, Criteria> getPerformanceCompositeInvestmentAccountPerformanceDAO() {
		return this.performanceCompositeInvestmentAccountPerformanceDAO;
	}


	public void setPerformanceCompositeInvestmentAccountPerformanceDAO(
			AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccountPerformance, Criteria> performanceCompositeInvestmentAccountPerformanceDAO) {
		this.performanceCompositeInvestmentAccountPerformanceDAO = performanceCompositeInvestmentAccountPerformanceDAO;
	}


	public AdvancedUpdatableDAO<PerformanceCompositePerformance, Criteria> getPerformanceCompositePerformanceDAO() {
		return this.performanceCompositePerformanceDAO;
	}


	public void setPerformanceCompositePerformanceDAO(AdvancedUpdatableDAO<PerformanceCompositePerformance, Criteria> performanceCompositePerformanceDAO) {
		this.performanceCompositePerformanceDAO = performanceCompositePerformanceDAO;
	}


	public AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccountDailyPerformance, Criteria> getPerformanceCompositeInvestmentAccountDailyPerformanceDAO() {
		return this.performanceCompositeInvestmentAccountDailyPerformanceDAO;
	}


	public void setPerformanceCompositeInvestmentAccountDailyPerformanceDAO(AdvancedUpdatableDAO<PerformanceCompositeInvestmentAccountDailyPerformance, Criteria> performanceCompositeInvestmentAccountDailyPerformanceDAO) {
		this.performanceCompositeInvestmentAccountDailyPerformanceDAO = performanceCompositeInvestmentAccountDailyPerformanceDAO;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
