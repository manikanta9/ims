package com.clifton.performance.composite.performance.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.performance.composite.performance.metric.search.BasePerformanceCompositeMetricSearchForm;


public class PerformanceCompositePerformanceSearchForm extends BasePerformanceCompositeMetricSearchForm {


	@SearchField(searchField = "performanceComposite.id", sortField = "performanceComposite.name")
	private Short performanceCompositeId;

	@SearchField(searchField = "performanceComposite.id", sortField = "performanceComposite.name")
	private Short[] performanceCompositeIds;

	@SearchField(searchField = "name", searchFieldPath = "performanceComposite")
	private String performanceCompositeName;

	@SearchField(searchField = "name", searchFieldPath = "performanceComposite", comparisonConditions = {ComparisonConditions.EQUALS})
	private String performanceCompositeNameEquals;

	@SearchField(searchFieldPath = "performanceComposite", searchField = "benchmarkSecurity.id")
	private Integer benchmarkSecurityId;

	@SearchField(searchFieldPath = "performanceComposite", searchField = "secondaryBenchmarkSecurity.id")
	private Integer secondaryBenchmarkSecurityId;

	@SearchField(searchFieldPath = "performanceComposite", searchField = "thirdBenchmarkSecurity.id")
	private Integer thirdBenchmarkSecurityId;

	@SearchField(searchFieldPath = "performanceComposite", searchField = "riskFreeSecurity.id")
	private Integer riskFreeSecurityId;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Short getPerformanceCompositeId() {
		return this.performanceCompositeId;
	}


	public void setPerformanceCompositeId(Short performanceCompositeId) {
		this.performanceCompositeId = performanceCompositeId;
	}


	public Short[] getPerformanceCompositeIds() {
		return this.performanceCompositeIds;
	}


	public void setPerformanceCompositeIds(Short[] performanceCompositeIds) {
		this.performanceCompositeIds = performanceCompositeIds;
	}


	public String getPerformanceCompositeName() {
		return this.performanceCompositeName;
	}


	public void setPerformanceCompositeName(String performanceCompositeName) {
		this.performanceCompositeName = performanceCompositeName;
	}


	public String getPerformanceCompositeNameEquals() {
		return this.performanceCompositeNameEquals;
	}


	public void setPerformanceCompositeNameEquals(String performanceCompositeNameEquals) {
		this.performanceCompositeNameEquals = performanceCompositeNameEquals;
	}


	@Override
	public Integer getBenchmarkSecurityId() {
		return this.benchmarkSecurityId;
	}


	public void setBenchmarkSecurityId(Integer benchmarkSecurityId) {
		this.benchmarkSecurityId = benchmarkSecurityId;
	}


	@Override
	public Integer getSecondaryBenchmarkSecurityId() {
		return this.secondaryBenchmarkSecurityId;
	}


	public void setSecondaryBenchmarkSecurityId(Integer secondaryBenchmarkSecurityId) {
		this.secondaryBenchmarkSecurityId = secondaryBenchmarkSecurityId;
	}


	@Override
	public Integer getThirdBenchmarkSecurityId() {
		return this.thirdBenchmarkSecurityId;
	}


	public void setThirdBenchmarkSecurityId(Integer thirdBenchmarkSecurityId) {
		this.thirdBenchmarkSecurityId = thirdBenchmarkSecurityId;
	}


	@Override
	public Integer getRiskFreeSecurityId() {
		return this.riskFreeSecurityId;
	}


	public void setRiskFreeSecurityId(Integer riskFreeSecurityId) {
		this.riskFreeSecurityId = riskFreeSecurityId;
	}
}
