package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>PerformanceCompositeAccountDailyModifiedDietzCalculator</code> is a method used for measuring an accounts return when IMS is able to value a portfolio daily.
 * Daily returns are geometrically linked together to calculate the monthly return.
 * (Gain/Loss)/(BaseMarketValue + Net Cash Flow)
 *
 * @author apopp
 */
public class PerformanceCompositeAccountDailyModifiedDietzCalculator extends BasePerformanceDailyCalculator {

	@Override
	protected BigDecimal calculateDailyReturn(BigDecimal gainLoss, BigDecimal positionValue, BigDecimal netCashFlow, BigDecimal weightedNetCashFlow) {
		//(Gain/Loss)/(BaseMarketValue + Net Cash Flow)
		return CoreMathUtils.getPercentValue(gainLoss, MathUtils.add(positionValue, netCashFlow), true);
	}
}
