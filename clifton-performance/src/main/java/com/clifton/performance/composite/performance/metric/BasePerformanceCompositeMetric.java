package com.clifton.performance.composite.performance.metric;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.portal.file.PortalFileAware;
import com.clifton.rule.violation.RuleViolationAware;
import com.clifton.rule.violation.status.RuleViolationStatus;
import com.clifton.workflow.BaseWorkflowAwareEntity;

import java.math.BigDecimal;


/**
 * @author manderson
 */
public abstract class BasePerformanceCompositeMetric extends BaseWorkflowAwareEntity<Integer> implements PerformanceCompositeMetric, RuleViolationAware, PortalFileAware {

	private AccountingPeriod accountingPeriod;
	private RuleViolationStatus violationStatus;

	private boolean postedToPortal;

	private BigDecimal periodEndMarketValue;

	private BigDecimal periodGrossReturn;
	private BigDecimal periodNetReturn;
	private BigDecimal periodModelNetReturn;
	private BigDecimal periodBenchmarkReturn;
	private BigDecimal periodSecondaryBenchmarkReturn;
	private BigDecimal periodThirdBenchmarkReturn;
	private BigDecimal periodRiskFreeSecurityReturn;

	private BigDecimal quarterToDateGrossReturn;
	private BigDecimal quarterToDateNetReturn;
	private BigDecimal quarterToDateModelNetReturn;
	private BigDecimal quarterToDateBenchmarkReturn;
	private BigDecimal quarterToDateSecondaryBenchmarkReturn;
	private BigDecimal quarterToDateThirdBenchmarkReturn;
	private BigDecimal quarterToDateRiskFreeSecurityReturn;

	private BigDecimal yearToDateGrossReturn;
	private BigDecimal yearToDateNetReturn;
	private BigDecimal yearToDateModelNetReturn;
	private BigDecimal yearToDateBenchmarkReturn;
	private BigDecimal yearToDateSecondaryBenchmarkReturn;
	private BigDecimal yearToDateThirdBenchmarkReturn;
	private BigDecimal yearToDateRiskFreeSecurityReturn;

	private BigDecimal inceptionToDateGrossReturn;
	private BigDecimal inceptionToDateNetReturn;
	private BigDecimal inceptionToDateModelNetReturn;
	private BigDecimal inceptionToDateBenchmarkReturn;
	private BigDecimal inceptionToDateSecondaryBenchmarkReturn;
	private BigDecimal inceptionToDateThirdBenchmarkReturn;
	private BigDecimal inceptionToDateRiskFreeSecurityReturn;


	private BigDecimal inceptionToDateGrossStandardDeviation;
	private BigDecimal inceptionToDateNetStandardDeviation;
	private BigDecimal inceptionToDateModelNetStandardDeviation;
	private BigDecimal inceptionToDateBenchmarkStandardDeviation;
	private BigDecimal inceptionToDateSecondaryBenchmarkStandardDeviation;
	private BigDecimal inceptionToDateThirdBenchmarkStandardDeviation;

	private BigDecimal inceptionToDateGrossSharpeRatio;
	private BigDecimal inceptionToDateNetSharpeRatio;
	private BigDecimal inceptionToDateModelNetSharpeRatio;
	private BigDecimal inceptionToDateBenchmarkSharpeRatio;
	private BigDecimal inceptionToDateSecondaryBenchmarkSharpeRatio;
	private BigDecimal inceptionToDateThirdBenchmarkSharpeRatio;

	private BigDecimal oneYearGrossReturn;
	private BigDecimal oneYearNetReturn;
	private BigDecimal oneYearModelNetReturn;
	private BigDecimal oneYearBenchmarkReturn;
	private BigDecimal oneYearSecondaryBenchmarkReturn;
	private BigDecimal oneYearThirdBenchmarkReturn;
	private BigDecimal oneYearRiskFreeSecurityReturn;


	private BigDecimal oneYearGrossStandardDeviation;
	private BigDecimal oneYearNetStandardDeviation;
	private BigDecimal oneYearModelNetStandardDeviation;
	private BigDecimal oneYearBenchmarkStandardDeviation;
	private BigDecimal oneYearSecondaryBenchmarkStandardDeviation;
	private BigDecimal oneYearThirdBenchmarkStandardDeviation;

	private BigDecimal oneYearGrossSharpeRatio;
	private BigDecimal oneYearNetSharpeRatio;
	private BigDecimal oneYearModelNetSharpeRatio;
	private BigDecimal oneYearBenchmarkSharpeRatio;
	private BigDecimal oneYearSecondaryBenchmarkSharpeRatio;
	private BigDecimal oneYearThirdBenchmarkSharpeRatio;

	private BigDecimal threeYearGrossReturn;
	private BigDecimal threeYearNetReturn;
	private BigDecimal threeYearModelNetReturn;
	private BigDecimal threeYearBenchmarkReturn;
	private BigDecimal threeYearSecondaryBenchmarkReturn;
	private BigDecimal threeYearThirdBenchmarkReturn;
	private BigDecimal threeYearRiskFreeSecurityReturn;

	private BigDecimal threeYearGrossStandardDeviation;
	private BigDecimal threeYearNetStandardDeviation;
	private BigDecimal threeYearModelNetStandardDeviation;
	private BigDecimal threeYearBenchmarkStandardDeviation;
	private BigDecimal threeYearSecondaryBenchmarkStandardDeviation;
	private BigDecimal threeYearThirdBenchmarkStandardDeviation;

	private BigDecimal threeYearGrossSharpeRatio;
	private BigDecimal threeYearNetSharpeRatio;
	private BigDecimal threeYearModelNetSharpeRatio;
	private BigDecimal threeYearBenchmarkSharpeRatio;
	private BigDecimal threeYearSecondaryBenchmarkSharpeRatio;
	private BigDecimal threeYearThirdBenchmarkSharpeRatio;

	private BigDecimal fiveYearGrossReturn;
	private BigDecimal fiveYearNetReturn;
	private BigDecimal fiveYearModelNetReturn;
	private BigDecimal fiveYearBenchmarkReturn;
	private BigDecimal fiveYearSecondaryBenchmarkReturn;
	private BigDecimal fiveYearThirdBenchmarkReturn;
	private BigDecimal fiveYearRiskFreeSecurityReturn;

	private BigDecimal fiveYearGrossStandardDeviation;
	private BigDecimal fiveYearNetStandardDeviation;
	private BigDecimal fiveYearModelNetStandardDeviation;
	private BigDecimal fiveYearSecondaryBenchmarkStandardDeviation;
	private BigDecimal fiveYearThirdBenchmarkStandardDeviation;
	private BigDecimal fiveYearBenchmarkStandardDeviation;

	private BigDecimal fiveYearGrossSharpeRatio;
	private BigDecimal fiveYearNetSharpeRatio;
	private BigDecimal fiveYearModelNetSharpeRatio;
	private BigDecimal fiveYearBenchmarkSharpeRatio;
	private BigDecimal fiveYearSecondaryBenchmarkSharpeRatio;
	private BigDecimal fiveYearThirdBenchmarkSharpeRatio;

	private BigDecimal tenYearGrossReturn;
	private BigDecimal tenYearNetReturn;
	private BigDecimal tenYearModelNetReturn;
	private BigDecimal tenYearBenchmarkReturn;
	private BigDecimal tenYearSecondaryBenchmarkReturn;
	private BigDecimal tenYearThirdBenchmarkReturn;
	private BigDecimal tenYearRiskFreeSecurityReturn;

	private BigDecimal tenYearGrossStandardDeviation;
	private BigDecimal tenYearNetStandardDeviation;
	private BigDecimal tenYearModelNetStandardDeviation;
	private BigDecimal tenYearSecondaryBenchmarkStandardDeviation;
	private BigDecimal tenYearThirdBenchmarkStandardDeviation;
	private BigDecimal tenYearBenchmarkStandardDeviation;

	private BigDecimal tenYearGrossSharpeRatio;
	private BigDecimal tenYearNetSharpeRatio;
	private BigDecimal tenYearModelNetSharpeRatio;
	private BigDecimal tenYearBenchmarkSharpeRatio;
	private BigDecimal tenYearSecondaryBenchmarkSharpeRatio;
	private BigDecimal tenYearThirdBenchmarkSharpeRatio;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public AccountingPeriod getAccountingPeriod() {
		return this.accountingPeriod;
	}


	@Override
	public void setAccountingPeriod(AccountingPeriod accountingPeriod) {
		this.accountingPeriod = accountingPeriod;
	}


	@Override
	public RuleViolationStatus getViolationStatus() {
		return this.violationStatus;
	}


	@Override
	public void setViolationStatus(RuleViolationStatus violationStatus) {
		this.violationStatus = violationStatus;
	}


	@Override
	public BigDecimal getPeriodEndMarketValue() {
		return this.periodEndMarketValue;
	}


	@Override
	public void setPeriodEndMarketValue(BigDecimal periodEndMarketValue) {
		this.periodEndMarketValue = periodEndMarketValue;
	}


	@Override
	public BigDecimal getPeriodGrossReturn() {
		return this.periodGrossReturn;
	}


	@Override
	public void setPeriodGrossReturn(BigDecimal periodGrossReturn) {
		this.periodGrossReturn = periodGrossReturn;
	}


	@Override
	public BigDecimal getPeriodNetReturn() {
		return this.periodNetReturn;
	}


	@Override
	public void setPeriodNetReturn(BigDecimal periodNetReturn) {
		this.periodNetReturn = periodNetReturn;
	}


	@Override
	public BigDecimal getPeriodBenchmarkReturn() {
		return this.periodBenchmarkReturn;
	}


	@Override
	public void setPeriodBenchmarkReturn(BigDecimal periodBenchmarkReturn) {
		this.periodBenchmarkReturn = periodBenchmarkReturn;
	}


	@Override
	public BigDecimal getPeriodSecondaryBenchmarkReturn() {
		return this.periodSecondaryBenchmarkReturn;
	}


	@Override
	public void setPeriodSecondaryBenchmarkReturn(BigDecimal periodSecondaryBenchmarkReturn) {
		this.periodSecondaryBenchmarkReturn = periodSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getPeriodRiskFreeSecurityReturn() {
		return this.periodRiskFreeSecurityReturn;
	}


	@Override
	public void setPeriodRiskFreeSecurityReturn(BigDecimal periodRiskFreeSecurityReturn) {
		this.periodRiskFreeSecurityReturn = periodRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getQuarterToDateGrossReturn() {
		return this.quarterToDateGrossReturn;
	}


	@Override
	public void setQuarterToDateGrossReturn(BigDecimal quarterToDateGrossReturn) {
		this.quarterToDateGrossReturn = quarterToDateGrossReturn;
	}


	@Override
	public BigDecimal getQuarterToDateNetReturn() {
		return this.quarterToDateNetReturn;
	}


	@Override
	public void setQuarterToDateNetReturn(BigDecimal quarterToDateNetReturn) {
		this.quarterToDateNetReturn = quarterToDateNetReturn;
	}


	@Override
	public BigDecimal getQuarterToDateBenchmarkReturn() {
		return this.quarterToDateBenchmarkReturn;
	}


	@Override
	public void setQuarterToDateBenchmarkReturn(BigDecimal quarterToDateBenchmarkReturn) {
		this.quarterToDateBenchmarkReturn = quarterToDateBenchmarkReturn;
	}


	@Override
	public BigDecimal getQuarterToDateSecondaryBenchmarkReturn() {
		return this.quarterToDateSecondaryBenchmarkReturn;
	}


	@Override
	public void setQuarterToDateSecondaryBenchmarkReturn(BigDecimal quarterToDateSecondaryBenchmarkReturn) {
		this.quarterToDateSecondaryBenchmarkReturn = quarterToDateSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getQuarterToDateRiskFreeSecurityReturn() {
		return this.quarterToDateRiskFreeSecurityReturn;
	}


	@Override
	public void setQuarterToDateRiskFreeSecurityReturn(BigDecimal quarterToDateRiskFreeSecurityReturn) {
		this.quarterToDateRiskFreeSecurityReturn = quarterToDateRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getYearToDateGrossReturn() {
		return this.yearToDateGrossReturn;
	}


	@Override
	public void setYearToDateGrossReturn(BigDecimal yearToDateGrossReturn) {
		this.yearToDateGrossReturn = yearToDateGrossReturn;
	}


	@Override
	public BigDecimal getYearToDateNetReturn() {
		return this.yearToDateNetReturn;
	}


	@Override
	public void setYearToDateNetReturn(BigDecimal yearToDateNetReturn) {
		this.yearToDateNetReturn = yearToDateNetReturn;
	}


	@Override
	public BigDecimal getYearToDateBenchmarkReturn() {
		return this.yearToDateBenchmarkReturn;
	}


	@Override
	public void setYearToDateBenchmarkReturn(BigDecimal yearToDateBenchmarkReturn) {
		this.yearToDateBenchmarkReturn = yearToDateBenchmarkReturn;
	}


	@Override
	public BigDecimal getYearToDateSecondaryBenchmarkReturn() {
		return this.yearToDateSecondaryBenchmarkReturn;
	}


	@Override
	public void setYearToDateSecondaryBenchmarkReturn(BigDecimal yearToDateSecondaryBenchmarkReturn) {
		this.yearToDateSecondaryBenchmarkReturn = yearToDateSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getYearToDateRiskFreeSecurityReturn() {
		return this.yearToDateRiskFreeSecurityReturn;
	}


	@Override
	public void setYearToDateRiskFreeSecurityReturn(BigDecimal yearToDateRiskFreeSecurityReturn) {
		this.yearToDateRiskFreeSecurityReturn = yearToDateRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getInceptionToDateGrossReturn() {
		return this.inceptionToDateGrossReturn;
	}


	@Override
	public void setInceptionToDateGrossReturn(BigDecimal inceptionToDateGrossReturn) {
		this.inceptionToDateGrossReturn = inceptionToDateGrossReturn;
	}


	@Override
	public BigDecimal getInceptionToDateNetReturn() {
		return this.inceptionToDateNetReturn;
	}


	@Override
	public void setInceptionToDateNetReturn(BigDecimal inceptionToDateNetReturn) {
		this.inceptionToDateNetReturn = inceptionToDateNetReturn;
	}


	@Override
	public BigDecimal getInceptionToDateBenchmarkReturn() {
		return this.inceptionToDateBenchmarkReturn;
	}


	@Override
	public void setInceptionToDateBenchmarkReturn(BigDecimal inceptionToDateBenchmarkReturn) {
		this.inceptionToDateBenchmarkReturn = inceptionToDateBenchmarkReturn;
	}


	@Override
	public BigDecimal getInceptionToDateSecondaryBenchmarkReturn() {
		return this.inceptionToDateSecondaryBenchmarkReturn;
	}


	@Override
	public void setInceptionToDateSecondaryBenchmarkReturn(BigDecimal inceptionToDateSecondaryBenchmarkReturn) {
		this.inceptionToDateSecondaryBenchmarkReturn = inceptionToDateSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getInceptionToDateRiskFreeSecurityReturn() {
		return this.inceptionToDateRiskFreeSecurityReturn;
	}


	@Override
	public void setInceptionToDateRiskFreeSecurityReturn(BigDecimal inceptionToDateRiskFreeSecurityReturn) {
		this.inceptionToDateRiskFreeSecurityReturn = inceptionToDateRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getInceptionToDateGrossStandardDeviation() {
		return this.inceptionToDateGrossStandardDeviation;
	}


	@Override
	public void setInceptionToDateGrossStandardDeviation(BigDecimal inceptionToDateGrossStandardDeviation) {
		this.inceptionToDateGrossStandardDeviation = inceptionToDateGrossStandardDeviation;
	}


	@Override
	public BigDecimal getInceptionToDateNetStandardDeviation() {
		return this.inceptionToDateNetStandardDeviation;
	}


	@Override
	public void setInceptionToDateNetStandardDeviation(BigDecimal inceptionToDateNetStandardDeviation) {
		this.inceptionToDateNetStandardDeviation = inceptionToDateNetStandardDeviation;
	}


	@Override
	public BigDecimal getInceptionToDateBenchmarkStandardDeviation() {
		return this.inceptionToDateBenchmarkStandardDeviation;
	}


	@Override
	public void setInceptionToDateBenchmarkStandardDeviation(BigDecimal inceptionToDateBenchmarkStandardDeviation) {
		this.inceptionToDateBenchmarkStandardDeviation = inceptionToDateBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getInceptionToDateSecondaryBenchmarkStandardDeviation() {
		return this.inceptionToDateSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public void setInceptionToDateSecondaryBenchmarkStandardDeviation(BigDecimal inceptionToDateSecondaryBenchmarkStandardDeviation) {
		this.inceptionToDateSecondaryBenchmarkStandardDeviation = inceptionToDateSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getInceptionToDateGrossSharpeRatio() {
		return this.inceptionToDateGrossSharpeRatio;
	}


	@Override
	public void setInceptionToDateGrossSharpeRatio(BigDecimal inceptionToDateGrossSharpeRatio) {
		this.inceptionToDateGrossSharpeRatio = inceptionToDateGrossSharpeRatio;
	}


	@Override
	public BigDecimal getInceptionToDateNetSharpeRatio() {
		return this.inceptionToDateNetSharpeRatio;
	}


	@Override
	public void setInceptionToDateNetSharpeRatio(BigDecimal inceptionToDateNetSharpeRatio) {
		this.inceptionToDateNetSharpeRatio = inceptionToDateNetSharpeRatio;
	}


	@Override
	public BigDecimal getInceptionToDateBenchmarkSharpeRatio() {
		return this.inceptionToDateBenchmarkSharpeRatio;
	}


	@Override
	public void setInceptionToDateBenchmarkSharpeRatio(BigDecimal inceptionToDateBenchmarkSharpeRatio) {
		this.inceptionToDateBenchmarkSharpeRatio = inceptionToDateBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getInceptionToDateSecondaryBenchmarkSharpeRatio() {
		return this.inceptionToDateSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public void setInceptionToDateSecondaryBenchmarkSharpeRatio(BigDecimal inceptionToDateSecondaryBenchmarkSharpeRatio) {
		this.inceptionToDateSecondaryBenchmarkSharpeRatio = inceptionToDateSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getOneYearGrossReturn() {
		return this.oneYearGrossReturn;
	}


	@Override
	public void setOneYearGrossReturn(BigDecimal oneYearGrossReturn) {
		this.oneYearGrossReturn = oneYearGrossReturn;
	}


	@Override
	public BigDecimal getOneYearNetReturn() {
		return this.oneYearNetReturn;
	}


	@Override
	public void setOneYearNetReturn(BigDecimal oneYearNetReturn) {
		this.oneYearNetReturn = oneYearNetReturn;
	}


	@Override
	public BigDecimal getOneYearBenchmarkReturn() {
		return this.oneYearBenchmarkReturn;
	}


	@Override
	public void setOneYearBenchmarkReturn(BigDecimal oneYearBenchmarkReturn) {
		this.oneYearBenchmarkReturn = oneYearBenchmarkReturn;
	}


	@Override
	public BigDecimal getOneYearSecondaryBenchmarkReturn() {
		return this.oneYearSecondaryBenchmarkReturn;
	}


	@Override
	public void setOneYearSecondaryBenchmarkReturn(BigDecimal oneYearSecondaryBenchmarkReturn) {
		this.oneYearSecondaryBenchmarkReturn = oneYearSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getOneYearRiskFreeSecurityReturn() {
		return this.oneYearRiskFreeSecurityReturn;
	}


	@Override
	public void setOneYearRiskFreeSecurityReturn(BigDecimal oneYearRiskFreeSecurityReturn) {
		this.oneYearRiskFreeSecurityReturn = oneYearRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getOneYearGrossStandardDeviation() {
		return this.oneYearGrossStandardDeviation;
	}


	@Override
	public void setOneYearGrossStandardDeviation(BigDecimal oneYearGrossStandardDeviation) {
		this.oneYearGrossStandardDeviation = oneYearGrossStandardDeviation;
	}


	@Override
	public BigDecimal getOneYearNetStandardDeviation() {
		return this.oneYearNetStandardDeviation;
	}


	@Override
	public void setOneYearNetStandardDeviation(BigDecimal oneYearNetStandardDeviation) {
		this.oneYearNetStandardDeviation = oneYearNetStandardDeviation;
	}


	@Override
	public BigDecimal getOneYearBenchmarkStandardDeviation() {
		return this.oneYearBenchmarkStandardDeviation;
	}


	@Override
	public void setOneYearBenchmarkStandardDeviation(BigDecimal oneYearBenchmarkStandardDeviation) {
		this.oneYearBenchmarkStandardDeviation = oneYearBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getOneYearSecondaryBenchmarkStandardDeviation() {
		return this.oneYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public void setOneYearSecondaryBenchmarkStandardDeviation(BigDecimal oneYearSecondaryBenchmarkStandardDeviation) {
		this.oneYearSecondaryBenchmarkStandardDeviation = oneYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getOneYearGrossSharpeRatio() {
		return this.oneYearGrossSharpeRatio;
	}


	@Override
	public void setOneYearGrossSharpeRatio(BigDecimal oneYearGrossSharpeRatio) {
		this.oneYearGrossSharpeRatio = oneYearGrossSharpeRatio;
	}


	@Override
	public BigDecimal getOneYearNetSharpeRatio() {
		return this.oneYearNetSharpeRatio;
	}


	@Override
	public void setOneYearNetSharpeRatio(BigDecimal oneYearNetSharpeRatio) {
		this.oneYearNetSharpeRatio = oneYearNetSharpeRatio;
	}


	@Override
	public BigDecimal getOneYearBenchmarkSharpeRatio() {
		return this.oneYearBenchmarkSharpeRatio;
	}


	@Override
	public void setOneYearBenchmarkSharpeRatio(BigDecimal oneYearBenchmarkSharpeRatio) {
		this.oneYearBenchmarkSharpeRatio = oneYearBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getOneYearSecondaryBenchmarkSharpeRatio() {
		return this.oneYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public void setOneYearSecondaryBenchmarkSharpeRatio(BigDecimal oneYearSecondaryBenchmarkSharpeRatio) {
		this.oneYearSecondaryBenchmarkSharpeRatio = oneYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getThreeYearGrossReturn() {
		return this.threeYearGrossReturn;
	}


	@Override
	public void setThreeYearGrossReturn(BigDecimal threeYearGrossReturn) {
		this.threeYearGrossReturn = threeYearGrossReturn;
	}


	@Override
	public BigDecimal getThreeYearNetReturn() {
		return this.threeYearNetReturn;
	}


	@Override
	public void setThreeYearNetReturn(BigDecimal threeYearNetReturn) {
		this.threeYearNetReturn = threeYearNetReturn;
	}


	@Override
	public BigDecimal getThreeYearBenchmarkReturn() {
		return this.threeYearBenchmarkReturn;
	}


	@Override
	public void setThreeYearBenchmarkReturn(BigDecimal threeYearBenchmarkReturn) {
		this.threeYearBenchmarkReturn = threeYearBenchmarkReturn;
	}


	@Override
	public BigDecimal getThreeYearSecondaryBenchmarkReturn() {
		return this.threeYearSecondaryBenchmarkReturn;
	}


	@Override
	public void setThreeYearSecondaryBenchmarkReturn(BigDecimal threeYearSecondaryBenchmarkReturn) {
		this.threeYearSecondaryBenchmarkReturn = threeYearSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getThreeYearRiskFreeSecurityReturn() {
		return this.threeYearRiskFreeSecurityReturn;
	}


	@Override
	public void setThreeYearRiskFreeSecurityReturn(BigDecimal threeYearRiskFreeSecurityReturn) {
		this.threeYearRiskFreeSecurityReturn = threeYearRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getThreeYearGrossStandardDeviation() {
		return this.threeYearGrossStandardDeviation;
	}


	@Override
	public void setThreeYearGrossStandardDeviation(BigDecimal threeYearGrossStandardDeviation) {
		this.threeYearGrossStandardDeviation = threeYearGrossStandardDeviation;
	}


	@Override
	public BigDecimal getThreeYearNetStandardDeviation() {
		return this.threeYearNetStandardDeviation;
	}


	@Override
	public void setThreeYearNetStandardDeviation(BigDecimal threeYearNetStandardDeviation) {
		this.threeYearNetStandardDeviation = threeYearNetStandardDeviation;
	}


	@Override
	public BigDecimal getThreeYearBenchmarkStandardDeviation() {
		return this.threeYearBenchmarkStandardDeviation;
	}


	@Override
	public void setThreeYearBenchmarkStandardDeviation(BigDecimal threeYearBenchmarkStandardDeviation) {
		this.threeYearBenchmarkStandardDeviation = threeYearBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getThreeYearSecondaryBenchmarkStandardDeviation() {
		return this.threeYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public void setThreeYearSecondaryBenchmarkStandardDeviation(BigDecimal threeYearSecondaryBenchmarkStandardDeviation) {
		this.threeYearSecondaryBenchmarkStandardDeviation = threeYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getThreeYearGrossSharpeRatio() {
		return this.threeYearGrossSharpeRatio;
	}


	@Override
	public void setThreeYearGrossSharpeRatio(BigDecimal threeYearGrossSharpeRatio) {
		this.threeYearGrossSharpeRatio = threeYearGrossSharpeRatio;
	}


	@Override
	public BigDecimal getThreeYearNetSharpeRatio() {
		return this.threeYearNetSharpeRatio;
	}


	@Override
	public void setThreeYearNetSharpeRatio(BigDecimal threeYearNetSharpeRatio) {
		this.threeYearNetSharpeRatio = threeYearNetSharpeRatio;
	}


	@Override
	public BigDecimal getThreeYearBenchmarkSharpeRatio() {
		return this.threeYearBenchmarkSharpeRatio;
	}


	@Override
	public void setThreeYearBenchmarkSharpeRatio(BigDecimal threeYearBenchmarkSharpeRatio) {
		this.threeYearBenchmarkSharpeRatio = threeYearBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getThreeYearSecondaryBenchmarkSharpeRatio() {
		return this.threeYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public void setThreeYearSecondaryBenchmarkSharpeRatio(BigDecimal threeYearSecondaryBenchmarkSharpeRatio) {
		this.threeYearSecondaryBenchmarkSharpeRatio = threeYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getFiveYearGrossReturn() {
		return this.fiveYearGrossReturn;
	}


	@Override
	public void setFiveYearGrossReturn(BigDecimal fiveYearGrossReturn) {
		this.fiveYearGrossReturn = fiveYearGrossReturn;
	}


	@Override
	public BigDecimal getFiveYearNetReturn() {
		return this.fiveYearNetReturn;
	}


	@Override
	public void setFiveYearNetReturn(BigDecimal fiveYearNetReturn) {
		this.fiveYearNetReturn = fiveYearNetReturn;
	}


	@Override
	public BigDecimal getFiveYearBenchmarkReturn() {
		return this.fiveYearBenchmarkReturn;
	}


	@Override
	public void setFiveYearBenchmarkReturn(BigDecimal fiveYearBenchmarkReturn) {
		this.fiveYearBenchmarkReturn = fiveYearBenchmarkReturn;
	}


	@Override
	public BigDecimal getFiveYearSecondaryBenchmarkReturn() {
		return this.fiveYearSecondaryBenchmarkReturn;
	}


	@Override
	public void setFiveYearSecondaryBenchmarkReturn(BigDecimal fiveYearSecondaryBenchmarkReturn) {
		this.fiveYearSecondaryBenchmarkReturn = fiveYearSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getFiveYearRiskFreeSecurityReturn() {
		return this.fiveYearRiskFreeSecurityReturn;
	}


	@Override
	public void setFiveYearRiskFreeSecurityReturn(BigDecimal fiveYearRiskFreeSecurityReturn) {
		this.fiveYearRiskFreeSecurityReturn = fiveYearRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getFiveYearGrossStandardDeviation() {
		return this.fiveYearGrossStandardDeviation;
	}


	@Override
	public void setFiveYearGrossStandardDeviation(BigDecimal fiveYearGrossStandardDeviation) {
		this.fiveYearGrossStandardDeviation = fiveYearGrossStandardDeviation;
	}


	@Override
	public BigDecimal getFiveYearNetStandardDeviation() {
		return this.fiveYearNetStandardDeviation;
	}


	@Override
	public void setFiveYearNetStandardDeviation(BigDecimal fiveYearNetStandardDeviation) {
		this.fiveYearNetStandardDeviation = fiveYearNetStandardDeviation;
	}


	@Override
	public BigDecimal getFiveYearSecondaryBenchmarkStandardDeviation() {
		return this.fiveYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public void setFiveYearSecondaryBenchmarkStandardDeviation(BigDecimal fiveYearSecondaryBenchmarkStandardDeviation) {
		this.fiveYearSecondaryBenchmarkStandardDeviation = fiveYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getFiveYearBenchmarkStandardDeviation() {
		return this.fiveYearBenchmarkStandardDeviation;
	}


	@Override
	public void setFiveYearBenchmarkStandardDeviation(BigDecimal fiveYearBenchmarkStandardDeviation) {
		this.fiveYearBenchmarkStandardDeviation = fiveYearBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getFiveYearGrossSharpeRatio() {
		return this.fiveYearGrossSharpeRatio;
	}


	@Override
	public void setFiveYearGrossSharpeRatio(BigDecimal fiveYearGrossSharpeRatio) {
		this.fiveYearGrossSharpeRatio = fiveYearGrossSharpeRatio;
	}


	@Override
	public BigDecimal getFiveYearNetSharpeRatio() {
		return this.fiveYearNetSharpeRatio;
	}


	@Override
	public void setFiveYearNetSharpeRatio(BigDecimal fiveYearNetSharpeRatio) {
		this.fiveYearNetSharpeRatio = fiveYearNetSharpeRatio;
	}


	@Override
	public BigDecimal getFiveYearBenchmarkSharpeRatio() {
		return this.fiveYearBenchmarkSharpeRatio;
	}


	@Override
	public void setFiveYearBenchmarkSharpeRatio(BigDecimal fiveYearBenchmarkSharpeRatio) {
		this.fiveYearBenchmarkSharpeRatio = fiveYearBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getFiveYearSecondaryBenchmarkSharpeRatio() {
		return this.fiveYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public void setFiveYearSecondaryBenchmarkSharpeRatio(BigDecimal fiveYearSecondaryBenchmarkSharpeRatio) {
		this.fiveYearSecondaryBenchmarkSharpeRatio = fiveYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getTenYearGrossReturn() {
		return this.tenYearGrossReturn;
	}


	@Override
	public void setTenYearGrossReturn(BigDecimal tenYearGrossReturn) {
		this.tenYearGrossReturn = tenYearGrossReturn;
	}


	@Override
	public BigDecimal getTenYearNetReturn() {
		return this.tenYearNetReturn;
	}


	@Override
	public void setTenYearNetReturn(BigDecimal tenYearNetReturn) {
		this.tenYearNetReturn = tenYearNetReturn;
	}


	@Override
	public BigDecimal getTenYearBenchmarkReturn() {
		return this.tenYearBenchmarkReturn;
	}


	@Override
	public void setTenYearBenchmarkReturn(BigDecimal tenYearBenchmarkReturn) {
		this.tenYearBenchmarkReturn = tenYearBenchmarkReturn;
	}


	@Override
	public BigDecimal getTenYearSecondaryBenchmarkReturn() {
		return this.tenYearSecondaryBenchmarkReturn;
	}


	@Override
	public void setTenYearSecondaryBenchmarkReturn(BigDecimal tenYearSecondaryBenchmarkReturn) {
		this.tenYearSecondaryBenchmarkReturn = tenYearSecondaryBenchmarkReturn;
	}


	@Override
	public BigDecimal getTenYearRiskFreeSecurityReturn() {
		return this.tenYearRiskFreeSecurityReturn;
	}


	@Override
	public void setTenYearRiskFreeSecurityReturn(BigDecimal tenYearRiskFreeSecurityReturn) {
		this.tenYearRiskFreeSecurityReturn = tenYearRiskFreeSecurityReturn;
	}


	@Override
	public BigDecimal getTenYearGrossStandardDeviation() {
		return this.tenYearGrossStandardDeviation;
	}


	@Override
	public void setTenYearGrossStandardDeviation(BigDecimal tenYearGrossStandardDeviation) {
		this.tenYearGrossStandardDeviation = tenYearGrossStandardDeviation;
	}


	@Override
	public BigDecimal getTenYearNetStandardDeviation() {
		return this.tenYearNetStandardDeviation;
	}


	@Override
	public void setTenYearNetStandardDeviation(BigDecimal tenYearNetStandardDeviation) {
		this.tenYearNetStandardDeviation = tenYearNetStandardDeviation;
	}


	@Override
	public BigDecimal getTenYearSecondaryBenchmarkStandardDeviation() {
		return this.tenYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public void setTenYearSecondaryBenchmarkStandardDeviation(BigDecimal tenYearSecondaryBenchmarkStandardDeviation) {
		this.tenYearSecondaryBenchmarkStandardDeviation = tenYearSecondaryBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getTenYearBenchmarkStandardDeviation() {
		return this.tenYearBenchmarkStandardDeviation;
	}


	@Override
	public void setTenYearBenchmarkStandardDeviation(BigDecimal tenYearBenchmarkStandardDeviation) {
		this.tenYearBenchmarkStandardDeviation = tenYearBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getTenYearGrossSharpeRatio() {
		return this.tenYearGrossSharpeRatio;
	}


	@Override
	public void setTenYearGrossSharpeRatio(BigDecimal tenYearGrossSharpeRatio) {
		this.tenYearGrossSharpeRatio = tenYearGrossSharpeRatio;
	}


	@Override
	public BigDecimal getTenYearNetSharpeRatio() {
		return this.tenYearNetSharpeRatio;
	}


	@Override
	public void setTenYearNetSharpeRatio(BigDecimal tenYearNetSharpeRatio) {
		this.tenYearNetSharpeRatio = tenYearNetSharpeRatio;
	}


	@Override
	public BigDecimal getTenYearBenchmarkSharpeRatio() {
		return this.tenYearBenchmarkSharpeRatio;
	}


	@Override
	public void setTenYearBenchmarkSharpeRatio(BigDecimal tenYearBenchmarkSharpeRatio) {
		this.tenYearBenchmarkSharpeRatio = tenYearBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getTenYearSecondaryBenchmarkSharpeRatio() {
		return this.tenYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public void setTenYearSecondaryBenchmarkSharpeRatio(BigDecimal tenYearSecondaryBenchmarkSharpeRatio) {
		this.tenYearSecondaryBenchmarkSharpeRatio = tenYearSecondaryBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getPeriodModelNetReturn() {
		return this.periodModelNetReturn;
	}


	@Override
	public void setPeriodModelNetReturn(BigDecimal periodModelNetReturn) {
		this.periodModelNetReturn = periodModelNetReturn;
	}


	@Override
	public BigDecimal getQuarterToDateModelNetReturn() {
		return this.quarterToDateModelNetReturn;
	}


	@Override
	public void setQuarterToDateModelNetReturn(BigDecimal quarterToDateModelNetReturn) {
		this.quarterToDateModelNetReturn = quarterToDateModelNetReturn;
	}


	@Override
	public BigDecimal getYearToDateModelNetReturn() {
		return this.yearToDateModelNetReturn;
	}


	@Override
	public void setYearToDateModelNetReturn(BigDecimal yearToDateModelNetReturn) {
		this.yearToDateModelNetReturn = yearToDateModelNetReturn;
	}


	@Override
	public BigDecimal getInceptionToDateModelNetReturn() {
		return this.inceptionToDateModelNetReturn;
	}


	@Override
	public void setInceptionToDateModelNetReturn(BigDecimal inceptionToDateModelNetReturn) {
		this.inceptionToDateModelNetReturn = inceptionToDateModelNetReturn;
	}


	@Override
	public BigDecimal getInceptionToDateModelNetStandardDeviation() {
		return this.inceptionToDateModelNetStandardDeviation;
	}


	@Override
	public void setInceptionToDateModelNetStandardDeviation(BigDecimal inceptionToDateModelNetStandardDeviation) {
		this.inceptionToDateModelNetStandardDeviation = inceptionToDateModelNetStandardDeviation;
	}


	@Override
	public BigDecimal getInceptionToDateModelNetSharpeRatio() {
		return this.inceptionToDateModelNetSharpeRatio;
	}


	@Override
	public void setInceptionToDateModelNetSharpeRatio(BigDecimal inceptionToDateModelNetSharpeRatio) {
		this.inceptionToDateModelNetSharpeRatio = inceptionToDateModelNetSharpeRatio;
	}


	@Override
	public BigDecimal getOneYearModelNetReturn() {
		return this.oneYearModelNetReturn;
	}


	@Override
	public void setOneYearModelNetReturn(BigDecimal oneYearModelNetReturn) {
		this.oneYearModelNetReturn = oneYearModelNetReturn;
	}


	@Override
	public BigDecimal getOneYearModelNetStandardDeviation() {
		return this.oneYearModelNetStandardDeviation;
	}


	@Override
	public void setOneYearModelNetStandardDeviation(BigDecimal oneYearModelNetStandardDeviation) {
		this.oneYearModelNetStandardDeviation = oneYearModelNetStandardDeviation;
	}


	@Override
	public BigDecimal getOneYearModelNetSharpeRatio() {
		return this.oneYearModelNetSharpeRatio;
	}


	@Override
	public void setOneYearModelNetSharpeRatio(BigDecimal oneYearModelNetSharpeRatio) {
		this.oneYearModelNetSharpeRatio = oneYearModelNetSharpeRatio;
	}


	@Override
	public BigDecimal getThreeYearModelNetReturn() {
		return this.threeYearModelNetReturn;
	}


	@Override
	public void setThreeYearModelNetReturn(BigDecimal threeYearModelNetReturn) {
		this.threeYearModelNetReturn = threeYearModelNetReturn;
	}


	@Override
	public BigDecimal getThreeYearModelNetStandardDeviation() {
		return this.threeYearModelNetStandardDeviation;
	}


	@Override
	public void setThreeYearModelNetStandardDeviation(BigDecimal threeYearModelNetStandardDeviation) {
		this.threeYearModelNetStandardDeviation = threeYearModelNetStandardDeviation;
	}


	@Override
	public BigDecimal getThreeYearModelNetSharpeRatio() {
		return this.threeYearModelNetSharpeRatio;
	}


	@Override
	public void setThreeYearModelNetSharpeRatio(BigDecimal threeYearModelNetSharpeRatio) {
		this.threeYearModelNetSharpeRatio = threeYearModelNetSharpeRatio;
	}


	@Override
	public BigDecimal getFiveYearModelNetReturn() {
		return this.fiveYearModelNetReturn;
	}


	@Override
	public void setFiveYearModelNetReturn(BigDecimal fiveYearModelNetReturn) {
		this.fiveYearModelNetReturn = fiveYearModelNetReturn;
	}


	@Override
	public BigDecimal getFiveYearModelNetStandardDeviation() {
		return this.fiveYearModelNetStandardDeviation;
	}


	@Override
	public void setFiveYearModelNetStandardDeviation(BigDecimal fiveYearModelNetStandardDeviation) {
		this.fiveYearModelNetStandardDeviation = fiveYearModelNetStandardDeviation;
	}


	@Override
	public BigDecimal getFiveYearModelNetSharpeRatio() {
		return this.fiveYearModelNetSharpeRatio;
	}


	@Override
	public void setFiveYearModelNetSharpeRatio(BigDecimal fiveYearModelNetSharpeRatio) {
		this.fiveYearModelNetSharpeRatio = fiveYearModelNetSharpeRatio;
	}


	@Override
	public BigDecimal getTenYearModelNetReturn() {
		return this.tenYearModelNetReturn;
	}


	@Override
	public void setTenYearModelNetReturn(BigDecimal tenYearModelNetReturn) {
		this.tenYearModelNetReturn = tenYearModelNetReturn;
	}


	@Override
	public BigDecimal getTenYearModelNetStandardDeviation() {
		return this.tenYearModelNetStandardDeviation;
	}


	@Override
	public void setTenYearModelNetStandardDeviation(BigDecimal tenYearModelNetStandardDeviation) {
		this.tenYearModelNetStandardDeviation = tenYearModelNetStandardDeviation;
	}


	@Override
	public BigDecimal getTenYearModelNetSharpeRatio() {
		return this.tenYearModelNetSharpeRatio;
	}


	@Override
	public void setTenYearModelNetSharpeRatio(BigDecimal tenYearModelNetSharpeRatio) {
		this.tenYearModelNetSharpeRatio = tenYearModelNetSharpeRatio;
	}


	@Override
	public BigDecimal getPeriodThirdBenchmarkReturn() {
		return this.periodThirdBenchmarkReturn;
	}


	@Override
	public void setPeriodThirdBenchmarkReturn(BigDecimal periodThirdBenchmarkReturn) {
		this.periodThirdBenchmarkReturn = periodThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getQuarterToDateThirdBenchmarkReturn() {
		return this.quarterToDateThirdBenchmarkReturn;
	}


	@Override
	public void setQuarterToDateThirdBenchmarkReturn(BigDecimal quarterToDateThirdBenchmarkReturn) {
		this.quarterToDateThirdBenchmarkReturn = quarterToDateThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getYearToDateThirdBenchmarkReturn() {
		return this.yearToDateThirdBenchmarkReturn;
	}


	@Override
	public void setYearToDateThirdBenchmarkReturn(BigDecimal yearToDateThirdBenchmarkReturn) {
		this.yearToDateThirdBenchmarkReturn = yearToDateThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getInceptionToDateThirdBenchmarkReturn() {
		return this.inceptionToDateThirdBenchmarkReturn;
	}


	@Override
	public void setInceptionToDateThirdBenchmarkReturn(BigDecimal inceptionToDateThirdBenchmarkReturn) {
		this.inceptionToDateThirdBenchmarkReturn = inceptionToDateThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getInceptionToDateThirdBenchmarkStandardDeviation() {
		return this.inceptionToDateThirdBenchmarkStandardDeviation;
	}


	@Override
	public void setInceptionToDateThirdBenchmarkStandardDeviation(BigDecimal inceptionToDateThirdBenchmarkStandardDeviation) {
		this.inceptionToDateThirdBenchmarkStandardDeviation = inceptionToDateThirdBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getInceptionToDateThirdBenchmarkSharpeRatio() {
		return this.inceptionToDateThirdBenchmarkSharpeRatio;
	}


	@Override
	public void setInceptionToDateThirdBenchmarkSharpeRatio(BigDecimal inceptionToDateThirdBenchmarkSharpeRatio) {
		this.inceptionToDateThirdBenchmarkSharpeRatio = inceptionToDateThirdBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getOneYearThirdBenchmarkReturn() {
		return this.oneYearThirdBenchmarkReturn;
	}


	@Override
	public void setOneYearThirdBenchmarkReturn(BigDecimal oneYearThirdBenchmarkReturn) {
		this.oneYearThirdBenchmarkReturn = oneYearThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getOneYearThirdBenchmarkStandardDeviation() {
		return this.oneYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public void setOneYearThirdBenchmarkStandardDeviation(BigDecimal oneYearThirdBenchmarkStandardDeviation) {
		this.oneYearThirdBenchmarkStandardDeviation = oneYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getOneYearThirdBenchmarkSharpeRatio() {
		return this.oneYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public void setOneYearThirdBenchmarkSharpeRatio(BigDecimal oneYearThirdBenchmarkSharpeRatio) {
		this.oneYearThirdBenchmarkSharpeRatio = oneYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getThreeYearThirdBenchmarkReturn() {
		return this.threeYearThirdBenchmarkReturn;
	}


	@Override
	public void setThreeYearThirdBenchmarkReturn(BigDecimal threeYearThirdBenchmarkReturn) {
		this.threeYearThirdBenchmarkReturn = threeYearThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getThreeYearThirdBenchmarkStandardDeviation() {
		return this.threeYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public void setThreeYearThirdBenchmarkStandardDeviation(BigDecimal threeYearThirdBenchmarkStandardDeviation) {
		this.threeYearThirdBenchmarkStandardDeviation = threeYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getThreeYearThirdBenchmarkSharpeRatio() {
		return this.threeYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public void setThreeYearThirdBenchmarkSharpeRatio(BigDecimal threeYearThirdBenchmarkSharpeRatio) {
		this.threeYearThirdBenchmarkSharpeRatio = threeYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getFiveYearThirdBenchmarkReturn() {
		return this.fiveYearThirdBenchmarkReturn;
	}


	@Override
	public void setFiveYearThirdBenchmarkReturn(BigDecimal fiveYearThirdBenchmarkReturn) {
		this.fiveYearThirdBenchmarkReturn = fiveYearThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getFiveYearThirdBenchmarkStandardDeviation() {
		return this.fiveYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public void setFiveYearThirdBenchmarkStandardDeviation(BigDecimal fiveYearThirdBenchmarkStandardDeviation) {
		this.fiveYearThirdBenchmarkStandardDeviation = fiveYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getFiveYearThirdBenchmarkSharpeRatio() {
		return this.fiveYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public void setFiveYearThirdBenchmarkSharpeRatio(BigDecimal fiveYearThirdBenchmarkSharpeRatio) {
		this.fiveYearThirdBenchmarkSharpeRatio = fiveYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public BigDecimal getTenYearThirdBenchmarkReturn() {
		return this.tenYearThirdBenchmarkReturn;
	}


	@Override
	public void setTenYearThirdBenchmarkReturn(BigDecimal tenYearThirdBenchmarkReturn) {
		this.tenYearThirdBenchmarkReturn = tenYearThirdBenchmarkReturn;
	}


	@Override
	public BigDecimal getTenYearThirdBenchmarkStandardDeviation() {
		return this.tenYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public void setTenYearThirdBenchmarkStandardDeviation(BigDecimal tenYearThirdBenchmarkStandardDeviation) {
		this.tenYearThirdBenchmarkStandardDeviation = tenYearThirdBenchmarkStandardDeviation;
	}


	@Override
	public BigDecimal getTenYearThirdBenchmarkSharpeRatio() {
		return this.tenYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public void setTenYearThirdBenchmarkSharpeRatio(BigDecimal tenYearThirdBenchmarkSharpeRatio) {
		this.tenYearThirdBenchmarkSharpeRatio = tenYearThirdBenchmarkSharpeRatio;
	}


	@Override
	public boolean isPostedToPortal() {
		return this.postedToPortal;
	}


	@Override
	public void setPostedToPortal(boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}
}
