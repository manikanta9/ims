package com.clifton.performance.composite.performance.process.calculators.fee;

import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;

import java.math.BigDecimal;


/**
 * The <code>PerformanceCompositeAccountManagementFeeOverrideCalculator</code> interface defines
 * the calculator for calculating the management fee override value for accounts
 * <p>
 * This is used to "true up" the management fee which is projected revenue to the actual revenue
 *
 * @author manderson
 */
public interface PerformanceCompositeAccountManagementFeeOverrideCalculator {


	/**
	 * Calculates and sets the management fee override value, which would be the existing management fee + adjustment.
	 */
	public void calculateManagementFeeOverride(PerformanceCompositeAccountRuleEvaluatorContext config, BigDecimal managementFee);
}
