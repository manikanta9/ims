package com.clifton.performance.composite.setup;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeSetupService</code> interface defines methods for working with Performance Composite Setup:
 * i.e. PerformanceComposite and PerformanceCompositeInvestmentAccount
 *
 * @author vgomelsky
 */
public interface PerformanceCompositeSetupService {

	////////////////////////////////////////////////////////////////////////////////
	///////////              Performance Composite Methods               ///////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceComposite getPerformanceComposite(short id);


	public List<PerformanceComposite> getPerformanceCompositeList(PerformanceCompositeSearchForm searchForm);


	/**
	 * When looking for parent list, should use these methods as they make use of the cache on the PerformanceCompositeRollup table
	 */
	public List<PerformanceComposite> getPerformanceCompositeParentList(short performanceCompositeId);


	public PerformanceComposite savePerformanceComposite(PerformanceComposite bean);


	public void deletePerformanceComposite(short id);


	////////////////////////////////////////////////////////////////////////////////
	////////          Performance Composite History Methods                 ////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks history table first for date specific model fee values, otherwise returns the composite model fee value.
	 */
	@DoNotAddRequestMapping
	public BigDecimal getModelFeeForPerformanceCompositeAndDate(PerformanceComposite performanceComposite, Date date);


	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeHistory getPerformanceCompositeHistory(short id);


	public List<PerformanceCompositeHistory> getPerformanceCompositeHistoryListForComposite(short performanceCompositeId);


	/**
	 * Specialized save method that will also update the {@link PerformanceComposite} and any previous history record (if it exists)
	 */
	public PerformanceCompositeHistory savePerformanceCompositeHistory(PerformanceCompositeHistory bean);


	/**
	 * Specialized delete method that will also update the {@link PerformanceComposite} and any previous history record (if it exists)
	 * Only allowed to delete the latest entry
	 */
	public void deletePerformanceCompositeHistory(short id);


	////////////////////////////////////////////////////////////////////////////////
	///////         Performance Composite Investment Account Methods         ///////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositeInvestmentAccount getPerformanceCompositeInvestmentAccount(int id);


	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeAccountListByComposite(short performanceCompositeId);


	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(int investmentAccountId, int accountingPeriodId);


	public List<PerformanceCompositeInvestmentAccount> getPerformanceCompositeInvestmentAccountList(PerformanceCompositeInvestmentAccountSearchForm searchForm);


	public PerformanceCompositeInvestmentAccount savePerformanceCompositeInvestmentAccount(PerformanceCompositeInvestmentAccount bean);


	public void deletePerformanceCompositeInvestmentAccount(int id);
}
