package com.clifton.performance.composite.performance.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeMetricContext;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluator;
import com.clifton.rule.violation.RuleViolation;
import com.clifton.rule.violation.RuleViolationService;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>PerformanceCompositeMetricPreviousPerformanceRuleEvaluator</code> can be used for both composite account performance and composite performance ({@link PerformanceCompositeMetric} objects
 * It's used to check 1. If Previous Month's performance exists, or 2. ANY Previous Performance is Still in Draft State
 *
 * @author manderson
 */
public class PerformanceCompositeMetricPreviousPerformanceRuleEvaluator<T extends PerformanceCompositeMetric> implements RuleEvaluator<T, PerformanceCompositeMetricContext<T>> {

	private RuleViolationService ruleViolationService;


	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, will only check previous month exists
	 * If false, will check all previous history for those in an invalid state
	 */
	private boolean checkPreviousPeriodExists;

	private List<String> previousPerformanceInvalidWorkflowStateList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(T performanceCompositeMetric, RuleConfig ruleConfig, PerformanceCompositeMetricContext<T> context) {
		List<RuleViolation> violationList = new ArrayList<>();

		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<T> previousList = context.getPreviousPerformanceCompositeMetricList(performanceCompositeMetric);
			// If there is no history, then it's the first one, so nothing to check
			if (!CollectionUtils.isEmpty(previousList)) {
				if (isCheckPreviousPeriodExists()) {
					T previousPeriod = CollectionUtils.getFirstElement(BeanUtils.filter(previousList, performance -> DateUtils.isEqualWithoutTime(performance.getAccountingPeriod().getEndDate(), DateUtils.addDays(performanceCompositeMetric.getAccountingPeriod().getStartDate(), -1))));
					if (previousPeriod == null) {
						violationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceCompositeMetric.getId()));
					}
				}
				else {
					List<T> previousInvalidList = BeanUtils.filter(previousList, performance -> getPreviousPerformanceInvalidWorkflowStateList().contains(performance.getWorkflowState().getName()));
					for (T previousPerformance : CollectionUtils.getIterable(previousInvalidList)) {
						violationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositeMetric.getId(), previousPerformance.getId()));
					}
				}
			}
		}
		return violationList;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public RuleViolationService getRuleViolationService() {
		return this.ruleViolationService;
	}


	public void setRuleViolationService(RuleViolationService ruleViolationService) {
		this.ruleViolationService = ruleViolationService;
	}


	public boolean isCheckPreviousPeriodExists() {
		return this.checkPreviousPeriodExists;
	}


	public void setCheckPreviousPeriodExists(boolean checkPreviousPeriodExists) {
		this.checkPreviousPeriodExists = checkPreviousPeriodExists;
	}


	public List<String> getPreviousPerformanceInvalidWorkflowStateList() {
		return this.previousPerformanceInvalidWorkflowStateList;
	}


	public void setPreviousPerformanceInvalidWorkflowStateList(List<String> previousPerformanceInvalidWorkflowStateList) {
		this.previousPerformanceInvalidWorkflowStateList = previousPerformanceInvalidWorkflowStateList;
	}
}
