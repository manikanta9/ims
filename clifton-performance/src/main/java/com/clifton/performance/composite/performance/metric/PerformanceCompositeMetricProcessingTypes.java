package com.clifton.performance.composite.performance.metric;

/**
 * @author manderson
 */
public enum PerformanceCompositeMetricProcessingTypes {

	ALL(true, true, true),
	BENCHMARKS_ONLY(false, true, true) {
		@Override
		public boolean isProcessPerformanceMetricFieldType(PerformanceMetricFieldTypes fieldType) {
			return fieldType.isBenchmarkReturn();
		}
	},
	LINKED_RETURNS_ONLY(false, false, true) {
		@Override
		public boolean isProcessPerformanceMetricFieldType(PerformanceMetricFieldTypes fieldType) {
			return fieldType.getPerformanceMetricPeriod() != PerformanceMetricPeriods.MONTH_TO_DATE;
		}
	};

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final boolean calculateAllMetricReturns;

	private final boolean calculateBenchmarkReturns;

	private final boolean calculateLinkedReturns;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	PerformanceCompositeMetricProcessingTypes(boolean calculateAllMetricReturns, boolean calculateBenchmarkReturns, boolean calculateLinkedReturns) {
		this.calculateAllMetricReturns = calculateAllMetricReturns;
		this.calculateBenchmarkReturns = calculateBenchmarkReturns;
		this.calculateLinkedReturns = calculateLinkedReturns;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isProcessPerformanceMetricFieldType(PerformanceMetricFieldTypes fieldType) {
		return true;
	}


	public boolean isCalculateAllMetricReturns() {
		return this.calculateAllMetricReturns;
	}


	public boolean isCalculateBenchmarkReturns() {
		return this.calculateBenchmarkReturns;
	}


	public boolean isCalculateLinkedReturns() {
		return this.calculateLinkedReturns;
	}
}
