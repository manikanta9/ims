package com.clifton.performance.composite.setup.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.setup.PerformanceCompositeHistory;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>PerformanceCompositeHistoryValidator</code> class validates on saves in history table that there isn't another history record
 * with overlapping date range
 *
 * @author manderson
 */
@Component
public class PerformanceCompositeHistoryValidator extends SelfRegisteringDaoValidator<PerformanceCompositeHistory> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(PerformanceCompositeHistory bean, DaoEventTypes config) throws ValidationException {
		// Nothing Here: Uses DAO validate method
	}


	@Override
	public void validate(PerformanceCompositeHistory bean, DaoEventTypes config, @SuppressWarnings("unused") ReadOnlyDAO<PerformanceCompositeHistory> dao) throws ValidationException {
		ValidationUtils.assertNotNull(bean.getPerformanceComposite(), "Performance Composite is Required");
		ValidationUtils.assertNotNull(bean.getStartDate(), "Start Date is Required");
		if (bean.getPerformanceComposite().isRollupComposite()) {
			throw new ValidationException("Model fee on rollup composites is not allowed.");
		}
		List<PerformanceCompositeHistory> historyList = dao.findByField("performanceComposite.id", bean.getPerformanceComposite().getId());
		for (PerformanceCompositeHistory history : CollectionUtils.getIterable(historyList)) {
			if (config.isInsert() || !bean.equals(history)) {
				if (DateUtils.isOverlapInDates(history.getStartDate(), history.getEndDate(), bean.getStartDate(), bean.getEndDate())) {
					throw new ValidationException("Performance Composite History record already exists for overlapping date range " + DateUtils.fromDateRange(history.getStartDate(), history.getEndDate(), true, false));
				}
			}
		}
	}
}
