package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceCompositeAccountDailyOptionsUnfundedCalculator</code> is a method used for measuring an accounts return using Option Notional (notional calculated using underlying security price).
 * <p>
 * <p>
 * (Gain/Loss)/(Notional Value of OPTIONS Positions)
 * <p>
 * This calculator will reprice the option positions using the notional calculator and the underlying security price and take the absolute value. (Usually : ABS(Quantity*Price*Multiplier))
 * <p>
 *
 * @author apopp
 */
public class PerformanceCompositeAccountDailyOptionsUnfundedCalculator extends BasePerformanceDailyCalculator {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * There are some accounts that ALWAYS use the same target value for all position value calculations
	 * Ex: 183050 use 50MM target
	 */
	private BigDecimal staticPositionValue;


	/**
	 * Returns can be periodically linked based on Option exposure changes - when the quantity of Options change (so excludes Rolls of expired to new)
	 * Since trading is done for these accounts (Defensive Equity) over a two day period (usually Friday and Monday) we track trade quantities and roll them forward to the next business day if there was a trade
	 * on that next business day to see if the net trades for that period was zero.
	 */
	private boolean useSubPeriodsForOptionsExposureChange;


	/**
	 * Used only if useSubPeriodsForTradeDates = true
	 * If true, when calculating options exposure, will use previous price.  This will NOT apply to calculations for the last day of the month.
	 * Note: For newly traded options, that don't have a previous price the position date's price will be used.
	 * This also affects gain/loss and pushes the end of the period's gain loss forward to the next daily period.
	 */
	private boolean usePreviousDayPrice;


	/**
	 * Used only if useSubPeriodsForTradeDates = false
	 * When there is a trade date (uses same logic as the sub period breaks above) recalculates position value using the previous price
	 * This value is saved as the override value to show the change because of the trade
	 */
	private boolean adjustPositionValueOnTradeDatesUsingPreviousPrice;

	/**
	 * Used only if useSubPeriods is false and adjustPositionValueOnTradeDatesUsingPreviousPrice is false
	 * Calculates each daily position value as next weekday's quantity with today's price
	 * Exception is on Current Month End where it uses that day's quantity
	 */
	private boolean useNextDayQuantity;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Get all Option trade dates in month to process for performance, combining consecutive trade dates together
	 * and ONLY include where there is an actual change in quantity (not just rolling one option position into another)
	 */
	@Override
	protected List<Date> getDaysToProcessForMonth(PerformanceCompositeAccountRuleEvaluatorContext context) {
		// If not breaking into sub-periods based on Option trades and not using static position value, return each day in the month (super call)
		if (!isUseSubPeriodsForOptionsExposureChange() && MathUtils.isNullOrZero(getStaticPositionValue())) {
			return super.getDaysToProcessForMonth(context);
		}
		AccountingPeriod accountingPeriod = context.getAccountingPeriod();
		List<Date> dayList = new ArrayList<>();

		// If using a static value, there would be no need to retrieve trades
		if (MathUtils.isNullOrZero(getStaticPositionValue())) {
			Map<Date, BigDecimal> tradeDateQuantityMap = context.getOptionTradeDateQuantityMap();

			// Set up Dates for ONLY those with quantities
			for (Map.Entry<Date, BigDecimal> dateBigDecimalEntry : tradeDateQuantityMap.entrySet()) {
				if (!MathUtils.isNullOrZero(dateBigDecimalEntry.getValue()) && DateUtils.isDateBetween(dateBigDecimalEntry.getKey(), accountingPeriod.getStartDate(), accountingPeriod.getEndDate(), false)) {
					dayList.add(dateBigDecimalEntry.getKey());
				}
			}
		}

		Date lastDayOfPreviousMonth = DateUtils.getLastDayOfPreviousMonth(accountingPeriod.getStartDate());
		// Don't want to include a break at the beginning of the month if coming into the month using that value
		Date lastDayOfPreviousMonthPositionDate = context.getLastConsecutiveTradeDate(lastDayOfPreviousMonth, dayList);
		if (!DateUtils.isEqualWithoutTime(lastDayOfPreviousMonth, lastDayOfPreviousMonthPositionDate)) {
			dayList.remove(lastDayOfPreviousMonthPositionDate);
		}
		dayList.add(lastDayOfPreviousMonth);


		// If month end lands on a weekend we need to include it to prevent missing gain/loss over the weekend that should be included within that month
		if (!dayList.contains(accountingPeriod.getEndDate())) {
			dayList.add(accountingPeriod.getEndDate());
		}
		Collections.sort(dayList);
		dayList = filterDaysToProcessForCurrentMonth(context, dayList);
		return dayList;
	}


	/**
	 * Beginning of day base notional value calculated as the base notional value of the end of the previous weekday
	 */
	@Override
	protected void calculateDailyPositionValue(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		if (MathUtils.isNullOrZero(getStaticPositionValue())) {
			BigDecimal dailyNotionalTotal = calculateNotionalValueOfUnderlyingPositionList(config, dateToCalculate, false);
			dailyPerformance.setPositionValue(dailyNotionalTotal);
			if (isAdjustPositionValueOnTradeDatesUsingPreviousPrice() && !DateUtils.isEqualWithoutTime(dateToCalculate, DateUtils.getLastDayOfPreviousMonth(config.getAccountingPeriod().getStartDate()))) {
				if (config.getOptionTradeDateQuantityMap().containsKey(dateToCalculate)) {
					dailyPerformance.setPositionValueOverride(calculateNotionalValueOfUnderlyingPositionList(config, dateToCalculate, true));
				}
				else {
					dailyPerformance.setPositionValueOverride(null);
				}
			}
		}
		else {
			dailyPerformance.setPositionValue(getStaticPositionValue());
		}
	}


	/**
	 * Calculate the notional of the underlying to determine the amount of exposure we have to it.
	 */
	private BigDecimal calculateNotionalValueOfUnderlyingPositionList(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, boolean forcePreviousPrice) {
		Map<Date, BigDecimal> optionTradeDateQuantityMap = config.getOptionTradeDateQuantityMap();

		Date positionDate = dateToCalculate;
		// If there was a trade on the position date (or if not a business day and was a trade on previous business day), make sure we move position date forward
		if (isUseSubPeriodsForOptionsExposureChange() && (optionTradeDateQuantityMap.containsKey(dateToCalculate) || !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(dateToCalculate)) && optionTradeDateQuantityMap.containsKey(getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(dateToCalculate), -1)))) {
			positionDate = config.getLastConsecutiveTradeDate(dateToCalculate, optionTradeDateQuantityMap.keySet());
		}
		// If using next day quantity and NOT the last day of the current month
		else if (isUseNextDayQuantity() && !DateUtils.isEqualWithoutTime(dateToCalculate, config.getAccountingPeriod().getEndDate())) {
			// Move position day forward one week day
			positionDate = DateUtils.addWeekDays(dateToCalculate, 1);
			// If that more forward brings us beyond the current period - move it back to the last day of the current period
			if (DateUtils.isDateAfter(positionDate, DateUtils.getLastDayOfMonth(config.getAccountingPeriod().getEndDate()))) {
				positionDate = config.getAccountingPeriod().getEndDate();
			}
		}

		Date priceDate = getPriceDateForPositionDate(dateToCalculate, forcePreviousPrice);

		List<AccountingPositionDaily> positionList = config.getPositionListForDate(positionDate, getIncludePositionInvestmentGroupId(), getExcludePositionInvestmentGroupId());
		BigDecimal totalNotional = BigDecimal.ZERO;

		for (AccountingPositionDaily position : CollectionUtils.getIterable(positionList)) {
			if (InvestmentUtils.isSecurityOfType(position.getAccountingTransaction().getInvestmentSecurity(), InvestmentType.OPTIONS)) {
				InvestmentSecurity investmentSecurity = position.getAccountingTransaction().getInvestmentSecurity();

				BigDecimal price = config.getInvestmentSecurityPriceForDate(investmentSecurity.getUnderlyingSecurity(), priceDate, DateUtils.compare(dateToCalculate, investmentSecurity.getStartDate(), false) >= 0);
				if (price == null) {
					// Use current price if new security and underlying price is missing
					price = config.getInvestmentSecurityPriceForDate(investmentSecurity.getUnderlyingSecurity(), positionDate, true);
				}
				totalNotional = MathUtils.add(totalNotional, MathUtils.abs(getInvestmentCalculator().calculateNotional(investmentSecurity, price, position.getRemainingQuantity(), dateToCalculate)));
			}
		}
		return totalNotional;
	}


	private Date getPriceDateForPositionDate(Date positionDate, boolean forcePreviousPrice) {
		if (forcePreviousPrice || isUsePreviousDayPrice()) {
			// Don't use Previous Day Price for the Last Day of the Month
			if (DateUtils.isLastDayOfMonth(positionDate)) {
				return positionDate;
			}
			return DateUtils.addDays(positionDate, -1);
		}
		return positionDate;
	}


	/**
	 * If using previous price, then we need to exclude dateToCalculate's gain/loss from the gain/loss value which will push forward to the next date
	 */
	@Override
	protected void calculateDailyGainLoss(PerformanceCompositeAccountRuleEvaluatorContext config, Date dateToCalculate, Date previousDate, BigDecimal gainLossAdjustmentFromPreviousValue) {
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = config.getPerformanceCompositeInvestmentAccountDailyPerformanceOrCreate(dateToCalculate);
		if (!isUsePreviousDayPrice()) {
			super.calculateDailyGainLossImpl(config, dailyPerformance, dateToCalculate, previousDate, gainLossAdjustmentFromPreviousValue);
		}
		else {
			Date priceDate = getPriceDateForPositionDate(dateToCalculate, false);
			super.calculateDailyGainLossImpl(config, dailyPerformance, priceDate, DateUtils.addDays(previousDate, -1), gainLossAdjustmentFromPreviousValue);
		}
	}


	@Override
	protected BigDecimal calculateDailyReturn(BigDecimal gainLoss, BigDecimal positionValue, BigDecimal netCashFlow, BigDecimal weightedNetCashFlow) {
		//(Gain/Loss)/(Notional Value of Options)
		return CoreMathUtils.getPercentValue(gainLoss, positionValue, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public BigDecimal getStaticPositionValue() {
		return this.staticPositionValue;
	}


	public void setStaticPositionValue(BigDecimal staticPositionValue) {
		this.staticPositionValue = staticPositionValue;
	}


	public boolean isUseSubPeriodsForOptionsExposureChange() {
		return this.useSubPeriodsForOptionsExposureChange;
	}


	public void setUseSubPeriodsForOptionsExposureChange(boolean useSubPeriodsForOptionsExposureChange) {
		this.useSubPeriodsForOptionsExposureChange = useSubPeriodsForOptionsExposureChange;
	}


	public boolean isUsePreviousDayPrice() {
		return this.usePreviousDayPrice;
	}


	public void setUsePreviousDayPrice(boolean usePreviousDayPrice) {
		this.usePreviousDayPrice = usePreviousDayPrice;
	}


	public boolean isAdjustPositionValueOnTradeDatesUsingPreviousPrice() {
		return this.adjustPositionValueOnTradeDatesUsingPreviousPrice;
	}


	public void setAdjustPositionValueOnTradeDatesUsingPreviousPrice(boolean adjustPositionValueOnTradeDatesUsingPreviousPrice) {
		this.adjustPositionValueOnTradeDatesUsingPreviousPrice = adjustPositionValueOnTradeDatesUsingPreviousPrice;
	}


	public boolean isUseNextDayQuantity() {
		return this.useNextDayQuantity;
	}


	public void setUseNextDayQuantity(boolean useNextDayQuantity) {
		this.useNextDayQuantity = useNextDayQuantity;
	}
}
