package com.clifton.performance.composite.performance.rule.context;

import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.rule.evaluator.RuleEvaluatorContext;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceCompositeMetricContext</code> interface defines methods that the context should implement for working with a {@link com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric} bean
 * Where the {@link com.clifton.performance.composite.performance.PerformanceCompositePerformance} data comes from the composite
 * and the {@link com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance} data comes from either the assignment or the assigned composite
 *
 * @author manderson
 */
public interface PerformanceCompositeMetricContext<T extends PerformanceCompositeMetric> extends RuleEvaluatorContext {


	/**
	 * Used for account performance only since accounts can have partial periods
	 */
	public Date getBenchmarkStartDateOverride(T performanceCompositeMetric);


	/**
	 * Used to distinguish if the system should calculate the model net from the gross and model fee
	 * That is done for accounts, but composites don't do this and instead use weighted values of children model net values.
	 */
	public boolean isModelFeeCalculatedFromGross(T performanceCompositeMetric);


	public BigDecimal getModelFee(T performanceCompositeMetric);


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<T> getPreviousPerformanceCompositeMetricList(T performanceCompositeMetric);
}
