package com.clifton.performance.composite.setup.observers;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;

import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class PerformanceCompositeInvestmentAccountValidator extends SelfRegisteringDaoValidator<PerformanceCompositeInvestmentAccount> {

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	private PerformanceCompositeSetupService performanceCompositeSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(PerformanceCompositeInvestmentAccount bean, DaoEventTypes config) throws ValidationException {
		// USES DAO METHOD
	}


	@Override
	public void validate(PerformanceCompositeInvestmentAccount bean, DaoEventTypes config, ReadOnlyDAO<PerformanceCompositeInvestmentAccount> dao) throws ValidationException {
		// If Insert or Update - Validate No Overlapping Date range (by month)
		if (!config.isDelete()) {
			ValidationUtils.assertFalse(bean.getPerformanceComposite().isRollupComposite(), "Investment accounts cannot be assigned to Roll Up performance composites.");
			ValidationUtils.assertBefore(bean.getStartDate(), bean.getEndDate(), "endDate");
			ValidationUtils.assertFalse(bean.getStartDate().before(bean.getPerformanceComposite().getStartDate()), "Account assignment Start Date " + DateUtils.fromDateSmart(bean.getStartDate())
					+ " cannot be before composite Start Date of " + DateUtils.fromDateSmart(bean.getPerformanceComposite().getStartDate()));

			validateOverlappingDateRange(bean);
		}

		// If Update (Date fields) or Delete - Confirm date changes don't affect existing performance records (i.e. no longer apply to what they are assigned to)
		PerformanceCompositeInvestmentAccount originalBean = (config.isUpdate()) ? getOriginalBean(dao, bean) : null;
		if (config.isDelete() || (originalBean != null && (!DateUtils.isEqualWithoutTime(originalBean.getStartDate(), bean.getStartDate()) || !DateUtils.isEqualWithoutTime(originalBean.getEndDate(), bean.getEndDate())))) {
			// See if there is any existing performance for this assignment
			List<PerformanceCompositeInvestmentAccountPerformance> assignmentPerformanceList = getPerformanceCompositeInvestmentAccountPerformanceForAssignment(bean.getId());
			if (!CollectionUtils.isEmpty(assignmentPerformanceList)) {
				if (config.isDelete()) {
					throw new ValidationException("Cannot delete assignment [" + bean.getLabel() + "] because there is existing account performance associated with it.");
				}
				else {
					// Need to validate for the entire month of the period
					assignmentPerformanceList.forEach(performance -> {
						if (!DateUtils.isOverlapInDates(bean.getStartDate(), bean.getEndDate(), performance.getAccountingPeriod().getStartDate(), performance.getAccountingPeriod().getEndDate())) {
							throw new ValidationException("Cannot change assignment dates, because there is existing performance for a month outside of the new date range [" + performance.getLabel() + "].");
						}
					});
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateOverlappingDateRange(PerformanceCompositeInvestmentAccount bean) {
		PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		searchForm.setInvestmentAccountId(bean.getInvestmentAccount().getId());
		// Partial Period Performance still applies to the entire month so check as of the first day of the month of the start date
		searchForm.setActiveOnDateRangeStartDate(DateUtils.getFirstDayOfMonth(bean.getStartDate()));
		// Until the last day of the month of the end date (null = use today unless start date in the future)
		Date endDate = bean.getEndDate();
		if (endDate == null) {
			if (DateUtils.isDateAfter(bean.getStartDate(), new Date())) {
				endDate = bean.getStartDate();
			}
			else {
				endDate = new Date();
			}
		}
		searchForm.setActiveOnDateRangeEndDate(DateUtils.getLastDayOfMonth(endDate));
		// Uses same calculator
		searchForm.setCoalesceAccountPerformanceCalculatorBeanId(bean.getCoalesceAccountPerformanceCalculatorBean().getId());

		List<PerformanceCompositeInvestmentAccount> assignmentList = getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(searchForm);
		for (PerformanceCompositeInvestmentAccount accountAssignment : CollectionUtils.getIterable(assignmentList)) {
			if (!CompareUtils.isEqual(accountAssignment.getId(), bean.getId())) {
				String accountName = accountAssignment.getInvestmentAccount().getName();
				ValidationUtils.assertFalse(DateUtils.isOverlapInDates(accountAssignment.getStartDate(), accountAssignment.getEndDate(), bean.getStartDate(), bean.getEndDate()),
						"Active assignment for account [" + accountName + "] using calculator [" + bean.getCoalesceAccountPerformanceCalculatorBean().getLabelShort() + "] already exists. Please end it first for composite [" + accountAssignment.getPerformanceComposite().getLabel()
								+ "] before creating a new assignment.");
			}
		}
	}


	private List<PerformanceCompositeInvestmentAccountPerformance> getPerformanceCompositeInvestmentAccountPerformanceForAssignment(int performanceCompositeInvestmentAccountId) {
		PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
		searchForm.setPerformanceCompositeInvestmentAccountId(performanceCompositeInvestmentAccountId);
		return getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}
}
