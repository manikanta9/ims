package com.clifton.performance.composite.performance.rule;


import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.performance.account.PerformanceAccountInfo;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.evaluator.RuleEvaluatorContext;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PerformanceAccountPartialPeriodRuleEvaluator extends BaseRuleEvaluator<PerformanceAccountInfo, RuleEvaluatorContext> {

	private AccountingPositionHandler accountingPositionHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceAccountInfo performanceAccountInfo, RuleConfig ruleConfig, RuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);
		if (entityConfig != null && !entityConfig.isExcluded()) {
			StringBuilder positionsOnOffMessage = new StringBuilder(10);
			// Populate On/Off Message
			getAccountingPositionHandler().getClientAccountPositionsOnDays(performanceAccountInfo.getClientAccount().getId(), performanceAccountInfo.getAccountingPeriod().getStartDate(), performanceAccountInfo.getAccountingPeriod().getEndDate(), positionsOnOffMessage);

			// Use Flexible check to check for on/off full period, because if they put positions on on the first business day of the month, then it's still considered a full period
			if (!getAccountingPositionHandler().isClientAccountPositionsOnForDateRange(performanceAccountInfo.getClientAccount().getId(), performanceAccountInfo.getAccountingPeriod().getStartDate(), performanceAccountInfo.getAccountingPeriod().getEndDate())) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNote", positionsOnOffMessage);
				ruleViolationList.add(getRuleViolationService().createRuleViolation(entityConfig, performanceAccountInfo.getIdentity(), contextValues));
			}
		}
		return ruleViolationList;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}
}
