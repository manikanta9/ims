package com.clifton.performance.composite.performance.upload;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;


/**
 * The <code>PerformanceCompositeUploadService</code> defined methods for uploading data related to composites, accounts, and performance.
 *
 * @author manderson
 */
public interface PerformanceCompositeUploadService {


	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class)
	public void uploadPerformanceCompositeInvestmentAccountPerformanceUploadFile(PerformanceCompositeAccountUploadCommand uploadCommand);
}
