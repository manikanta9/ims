package com.clifton.performance.composite.performance.rule;

import com.clifton.accounting.gl.position.AccountingPositionHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeRuleEvaluatorContext;
import com.clifton.rule.evaluator.BaseRuleEvaluator;
import com.clifton.rule.evaluator.EntityConfig;
import com.clifton.rule.evaluator.RuleConfig;
import com.clifton.rule.violation.RuleViolation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Rule Evaluator that generates a violation if any performance composite investment account performances related to a performance composite performance contain partial periods.
 * Uses {@Link AccountingPositionHandler}
 *
 * @author jonathanr
 */
public class PerformanceCompositePerformancePartialPeriodRuleEvaluator extends BaseRuleEvaluator<PerformanceCompositePerformance, PerformanceCompositeRuleEvaluatorContext> {

	private AccountingPositionHandler accountingPositionHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<RuleViolation> evaluateRule(PerformanceCompositePerformance performanceCompositePerformance, RuleConfig ruleConfig, PerformanceCompositeRuleEvaluatorContext context) {
		List<RuleViolation> ruleViolationList = new ArrayList<>();
		EntityConfig entityConfig = ruleConfig.getEntityConfig(null);

		if (entityConfig != null && !entityConfig.isExcluded()) {
			List<PerformanceCompositeInvestmentAccountPerformance> accountPerformances = context.getPerformanceCompositeInvestmentAccountPerformanceList(performanceCompositePerformance);
			List<String> violationNotes = new ArrayList<>();
			for (PerformanceCompositeInvestmentAccountPerformance accountPerformance : CollectionUtils.getIterable(accountPerformances)) {
				StringBuilder positionsOnOffMessage = new StringBuilder(10);
				// Populate On/Off Message
				getAccountingPositionHandler().getClientAccountPositionsOnDays(accountPerformance.getClientAccount().getId(), performanceCompositePerformance.getAccountingPeriod().getStartDate(), performanceCompositePerformance.getAccountingPeriod().getEndDate(), positionsOnOffMessage);

				// Use Flexible check to check for on/off full period, because if they put positions on on the first business day of the month, then it's still considered a full period
				if (!getAccountingPositionHandler().isClientAccountPositionsOnForDateRange(accountPerformance.getClientAccount().getId(), accountPerformance.getAccountingPeriod().getStartDate(), accountPerformance.getAccountingPeriod().getEndDate())) {
					//Exclude accounts at the Client Group level (they only have client accounts and cannot have positions)
					if (!accountPerformance.getClientAccount().getBusinessClient().getCategory().isOurAccountsOnly()) {
						violationNotes.add("[" + accountPerformance.getLabel() + "] : " + positionsOnOffMessage.toString());
					}
				}
			}
			if (!violationNotes.isEmpty()) {
				Map<String, Object> contextValues = new HashMap<>();
				contextValues.put("violationNotes", violationNotes);
				contextValues.put("includePartialPeriodAccounts", performanceCompositePerformance.getPerformanceComposite().isIncludePartialPeriodAccounts());
				ruleViolationList.add(getRuleViolationService().createRuleViolationWithCause(entityConfig, performanceCompositePerformance.getId(), null, null, contextValues));
			}
		}
		return ruleViolationList;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getters and Setters                        //////////
	////////////////////////////////////////////////////////////////////////////


	public AccountingPositionHandler getAccountingPositionHandler() {
		return this.accountingPositionHandler;
	}


	public void setAccountingPositionHandler(AccountingPositionHandler accountingPositionHandler) {
		this.accountingPositionHandler = accountingPositionHandler;
	}
}
