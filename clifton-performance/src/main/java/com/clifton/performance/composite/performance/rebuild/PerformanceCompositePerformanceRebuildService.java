package com.clifton.performance.composite.performance.rebuild;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;


/**
 * The <code>PerformanceCompositeAccountBenchmarkHandler</code> acts as the entry point for executing rebuilds
 * of performance metrics
 *
 * @author apopp
 */
public interface PerformanceCompositePerformanceRebuildService {

	////////////////////////////////////////////////////////////////////////////////
	////////////          Performance Composite Performance             ////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuild composite performance based on the command parameters
	 */
	@ModelAttribute("data")
	public String rebuildPerformanceCompositePerformance(final PerformanceCompositeRebuildCommand command);


	/**
	 * Rebuild composite performance based on the command parameters
	 */
	@ModelAttribute("data")
	public PerformanceCompositePerformance previewPerformanceCompositePerformance(PerformanceCompositeRebuildCommand command);


	////////////////////////////////////////////////////////////////////////////////
	////////    Performance Investment Account Composite Performance       /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Rebuild account performance based on the command parameters
	 */
	@ModelAttribute("data")
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class)
	public String rebuildPerformanceCompositeAccountPerformance(final PerformanceCompositeAccountRebuildCommand command);


	/**
	 * Preview monthly account performance
	 */
	@ModelAttribute("data")
	public PerformanceCompositeInvestmentAccountPerformance previewPerformanceCompositeAccountPerformance(PerformanceCompositeAccountRebuildCommand command);


	////////////////////////////////////////////////////////////////////////////////
	///////   Performance Investment Account Daily Composite Performance     ///////
	////////////////////////////////////////////////////////////////////////////////


	@ModelAttribute("data")
	public List<PerformanceCompositeInvestmentAccountDailyPerformance> previewPerformanceCompositeAccountDailyPerformanceRun(PerformanceCompositeAccountRebuildCommand command);


	@ModelAttribute("data")
	public PerformanceCompositeInvestmentAccountDailyPerformance previewPerformanceCompositeAccountDailyPerformanceRunForMeasureDate(PerformanceCompositeAccountRebuildCommand command);


	/**
	 * Daily performance save method that is exposed to UI so any changes made (overrides) to the daily values will automatically kick off
	 * the monthly rebuild of performance.
	 */
	public PerformanceCompositeInvestmentAccountDailyPerformance savePerformanceCompositeInvestmentAccountDailyPerformance(PerformanceCompositeInvestmentAccountDailyPerformance bean);
}
