package com.clifton.performance.composite.performance.metric.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author manderson
 */
public abstract class BasePerformanceCompositeMetricSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] ids;

	@SearchField(searchField = "accountingPeriod.id")
	private Integer accountingPeriodId;

	@SearchField
	private Boolean postedToPortal;

	/**
	 * Custom search field, set to true from UI calls. https://jira.paraport.com/browse/PERFORMANC-287
	 * If true, then any user that doesn't have Full control is automatically filtered out of viewing Draft status performance
	 */
	private Boolean requireFullControlForDraftStatus;

	@SearchField(searchField = "startDate", searchFieldPath = "accountingPeriod")
	private Date periodStartDate;
	@SearchField(searchField = "endDate", searchFieldPath = "accountingPeriod")
	private Date periodEndDate;

	@SearchField(searchField = "startDate", searchFieldPath = "accountingPeriod", comparisonConditions = {ComparisonConditions.GREATER_THAN_OR_EQUALS})
	private Date afterPeriodStartDate;
	@SearchField(searchField = "endDate", searchFieldPath = "accountingPeriod", comparisonConditions = {ComparisonConditions.LESS_THAN_OR_EQUALS})
	private Date beforePeriodEndDate;

	//custom to show specific frequency returns
	private PerformanceFrequencies performanceFrequency;

	@SearchField(searchField = "violationStatus.id")
	private Short violationStatusId;

	@SearchField(searchFieldPath = "violationStatus", searchField = "name")
	private String[] violationStatusNames;

	@SearchField
	private BigDecimal periodEndMarketValue;


	@SearchField
	private BigDecimal periodGrossReturn;
	@SearchField
	private BigDecimal periodNetReturn;
	@SearchField
	private BigDecimal periodModelNetReturn;
	@SearchField
	private BigDecimal periodBenchmarkReturn;

	@SearchField
	private BigDecimal quarterToDateGrossReturn;
	@SearchField
	private BigDecimal quarterToDateNetReturn;
	@SearchField
	private BigDecimal quarterToDateModelNetReturn;
	@SearchField
	private BigDecimal quarterToDateBenchmarkReturn;

	@SearchField
	private BigDecimal yearToDateGrossReturn;
	@SearchField
	private BigDecimal yearToDateNetReturn;
	@SearchField
	private BigDecimal yearToDateModelNetReturn;
	@SearchField
	private BigDecimal yearToDateBenchmarkReturn;

	@SearchField
	private BigDecimal inceptionToDateGrossReturn;
	@SearchField
	private BigDecimal inceptionToDateNetReturn;
	@SearchField
	private BigDecimal inceptionToDateModelNetReturn;
	@SearchField
	private BigDecimal inceptionToDateBenchmarkReturn;

	@SearchField
	private BigDecimal oneYearGrossReturn;
	@SearchField
	private BigDecimal oneYearNetReturn;
	@SearchField
	private BigDecimal oneYearModelNetReturn;
	@SearchField
	private BigDecimal oneYearBenchmarkReturn;
	@SearchField
	private BigDecimal oneYearBenchmarkStandardDeviation;
	@SearchField
	private BigDecimal oneYearGrossStandardDeviation;
	@SearchField
	private BigDecimal oneYearNetStandardDeviation;
	@SearchField
	private BigDecimal oneYearModelNetStandardDeviation;
	@SearchField
	private BigDecimal oneYearGrossSharpeRatio;
	@SearchField
	private BigDecimal oneYearNetSharpeRatio;
	@SearchField
	private BigDecimal oneYearModelNetSharpeRatio;

	@SearchField
	private BigDecimal threeYearGrossReturn;
	@SearchField
	private BigDecimal threeYearNetReturn;
	@SearchField
	private BigDecimal threeYearModelNetReturn;
	@SearchField
	private BigDecimal threeYearBenchmarkReturn;
	@SearchField
	private BigDecimal threeYearBenchmarkStandardDeviation;
	@SearchField
	private BigDecimal threeYearGrossStandardDeviation;
	@SearchField
	private BigDecimal threeYearNetStandardDeviation;
	@SearchField
	private BigDecimal threeYearModelNetStandardDeviation;
	@SearchField
	private BigDecimal threeYearGrossSharpeRatio;
	@SearchField
	private BigDecimal threeYearNetSharpeRatio;
	@SearchField
	private BigDecimal threeYearModelNetSharpeRatio;

	@SearchField
	private BigDecimal fiveYearGrossReturn;
	@SearchField
	private BigDecimal fiveYearNetReturn;
	@SearchField
	private BigDecimal fiveYearModelNetReturn;
	@SearchField
	private BigDecimal fiveYearBenchmarkReturn;
	@SearchField
	private BigDecimal fiveYearBenchmarkStandardDeviation;
	@SearchField
	private BigDecimal fiveYearGrossStandardDeviation;
	@SearchField
	private BigDecimal fiveYearNetStandardDeviation;
	@SearchField
	private BigDecimal fiveYearModelNetStandardDeviation;
	@SearchField
	private BigDecimal fiveYearGrossSharpeRatio;
	@SearchField
	private BigDecimal fiveYearNetSharpeRatio;
	@SearchField
	private BigDecimal fiveYearModelNetSharpeRatio;

	@SearchField
	private BigDecimal tenYearGrossReturn;
	@SearchField
	private BigDecimal tenYearNetReturn;
	@SearchField
	private BigDecimal tenYearModelNetReturn;
	@SearchField
	private BigDecimal tenYearBenchmarkReturn;
	@SearchField
	private BigDecimal tenYearBenchmarkStandardDeviation;
	@SearchField
	private BigDecimal tenYearGrossStandardDeviation;
	@SearchField
	private BigDecimal tenYearNetStandardDeviation;
	@SearchField
	private BigDecimal tenYearModelNetStandardDeviation;
	@SearchField
	private BigDecimal tenYearGrossSharpeRatio;
	@SearchField
	private BigDecimal tenYearNetSharpeRatio;
	@SearchField
	private BigDecimal tenYearModelNetSharpeRatio;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract Integer getBenchmarkSecurityId();


	public abstract Integer getSecondaryBenchmarkSecurityId();


	public abstract Integer getThirdBenchmarkSecurityId();


	public abstract Integer getRiskFreeSecurityId();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer getAccountingPeriodId() {
		return this.accountingPeriodId;
	}


	public void setAccountingPeriodId(Integer accountingPeriodId) {
		this.accountingPeriodId = accountingPeriodId;
	}


	public Date getPeriodStartDate() {
		return this.periodStartDate;
	}


	public void setPeriodStartDate(Date periodStartDate) {
		this.periodStartDate = periodStartDate;
	}


	public Date getPeriodEndDate() {
		return this.periodEndDate;
	}


	public void setPeriodEndDate(Date periodEndDate) {
		this.periodEndDate = periodEndDate;
	}


	public Date getAfterPeriodStartDate() {
		return this.afterPeriodStartDate;
	}


	public void setAfterPeriodStartDate(Date afterPeriodStartDate) {
		this.afterPeriodStartDate = afterPeriodStartDate;
	}


	public Date getBeforePeriodEndDate() {
		return this.beforePeriodEndDate;
	}


	public void setBeforePeriodEndDate(Date beforePeriodEndDate) {
		this.beforePeriodEndDate = beforePeriodEndDate;
	}


	public PerformanceFrequencies getPerformanceFrequency() {
		return this.performanceFrequency;
	}


	public void setPerformanceFrequency(PerformanceFrequencies performanceFrequency) {
		this.performanceFrequency = performanceFrequency;
	}


	public Short getViolationStatusId() {
		return this.violationStatusId;
	}


	public void setViolationStatusId(Short violationStatusId) {
		this.violationStatusId = violationStatusId;
	}


	public String[] getViolationStatusNames() {
		return this.violationStatusNames;
	}


	public void setViolationStatusNames(String[] violationStatusNames) {
		this.violationStatusNames = violationStatusNames;
	}


	public BigDecimal getPeriodEndMarketValue() {
		return this.periodEndMarketValue;
	}


	public void setPeriodEndMarketValue(BigDecimal periodEndMarketValue) {
		this.periodEndMarketValue = periodEndMarketValue;
	}


	public BigDecimal getPeriodGrossReturn() {
		return this.periodGrossReturn;
	}


	public void setPeriodGrossReturn(BigDecimal periodGrossReturn) {
		this.periodGrossReturn = periodGrossReturn;
	}


	public BigDecimal getPeriodNetReturn() {
		return this.periodNetReturn;
	}


	public void setPeriodNetReturn(BigDecimal periodNetReturn) {
		this.periodNetReturn = periodNetReturn;
	}


	public BigDecimal getPeriodBenchmarkReturn() {
		return this.periodBenchmarkReturn;
	}


	public void setPeriodBenchmarkReturn(BigDecimal periodBenchmarkReturn) {
		this.periodBenchmarkReturn = periodBenchmarkReturn;
	}


	public BigDecimal getQuarterToDateGrossReturn() {
		return this.quarterToDateGrossReturn;
	}


	public void setQuarterToDateGrossReturn(BigDecimal quarterToDateGrossReturn) {
		this.quarterToDateGrossReturn = quarterToDateGrossReturn;
	}


	public BigDecimal getQuarterToDateNetReturn() {
		return this.quarterToDateNetReturn;
	}


	public void setQuarterToDateNetReturn(BigDecimal quarterToDateNetReturn) {
		this.quarterToDateNetReturn = quarterToDateNetReturn;
	}


	public BigDecimal getQuarterToDateBenchmarkReturn() {
		return this.quarterToDateBenchmarkReturn;
	}


	public void setQuarterToDateBenchmarkReturn(BigDecimal quarterToDateBenchmarkReturn) {
		this.quarterToDateBenchmarkReturn = quarterToDateBenchmarkReturn;
	}


	public BigDecimal getYearToDateGrossReturn() {
		return this.yearToDateGrossReturn;
	}


	public void setYearToDateGrossReturn(BigDecimal yearToDateGrossReturn) {
		this.yearToDateGrossReturn = yearToDateGrossReturn;
	}


	public BigDecimal getYearToDateNetReturn() {
		return this.yearToDateNetReturn;
	}


	public void setYearToDateNetReturn(BigDecimal yearToDateNetReturn) {
		this.yearToDateNetReturn = yearToDateNetReturn;
	}


	public BigDecimal getYearToDateBenchmarkReturn() {
		return this.yearToDateBenchmarkReturn;
	}


	public void setYearToDateBenchmarkReturn(BigDecimal yearToDateBenchmarkReturn) {
		this.yearToDateBenchmarkReturn = yearToDateBenchmarkReturn;
	}


	public BigDecimal getInceptionToDateGrossReturn() {
		return this.inceptionToDateGrossReturn;
	}


	public void setInceptionToDateGrossReturn(BigDecimal inceptionToDateGrossReturn) {
		this.inceptionToDateGrossReturn = inceptionToDateGrossReturn;
	}


	public BigDecimal getInceptionToDateNetReturn() {
		return this.inceptionToDateNetReturn;
	}


	public void setInceptionToDateNetReturn(BigDecimal inceptionToDateNetReturn) {
		this.inceptionToDateNetReturn = inceptionToDateNetReturn;
	}


	public BigDecimal getInceptionToDateBenchmarkReturn() {
		return this.inceptionToDateBenchmarkReturn;
	}


	public void setInceptionToDateBenchmarkReturn(BigDecimal inceptionToDateBenchmarkReturn) {
		this.inceptionToDateBenchmarkReturn = inceptionToDateBenchmarkReturn;
	}


	public BigDecimal getOneYearGrossReturn() {
		return this.oneYearGrossReturn;
	}


	public void setOneYearGrossReturn(BigDecimal oneYearGrossReturn) {
		this.oneYearGrossReturn = oneYearGrossReturn;
	}


	public BigDecimal getOneYearBenchmarkReturn() {
		return this.oneYearBenchmarkReturn;
	}


	public void setOneYearBenchmarkReturn(BigDecimal oneYearBenchmarkReturn) {
		this.oneYearBenchmarkReturn = oneYearBenchmarkReturn;
	}


	public BigDecimal getOneYearBenchmarkStandardDeviation() {
		return this.oneYearBenchmarkStandardDeviation;
	}


	public void setOneYearBenchmarkStandardDeviation(BigDecimal oneYearBenchmarkStandardDeviation) {
		this.oneYearBenchmarkStandardDeviation = oneYearBenchmarkStandardDeviation;
	}


	public BigDecimal getOneYearGrossStandardDeviation() {
		return this.oneYearGrossStandardDeviation;
	}


	public void setOneYearGrossStandardDeviation(BigDecimal oneYearGrossStandardDeviation) {
		this.oneYearGrossStandardDeviation = oneYearGrossStandardDeviation;
	}


	public BigDecimal getOneYearNetStandardDeviation() {
		return this.oneYearNetStandardDeviation;
	}


	public void setOneYearNetStandardDeviation(BigDecimal oneYearNetStandardDeviation) {
		this.oneYearNetStandardDeviation = oneYearNetStandardDeviation;
	}


	public BigDecimal getOneYearGrossSharpeRatio() {
		return this.oneYearGrossSharpeRatio;
	}


	public void setOneYearGrossSharpeRatio(BigDecimal oneYearGrossSharpeRatio) {
		this.oneYearGrossSharpeRatio = oneYearGrossSharpeRatio;
	}


	public BigDecimal getOneYearNetSharpeRatio() {
		return this.oneYearNetSharpeRatio;
	}


	public void setOneYearNetSharpeRatio(BigDecimal oneYearNetSharpeRatio) {
		this.oneYearNetSharpeRatio = oneYearNetSharpeRatio;
	}


	public BigDecimal getThreeYearGrossReturn() {
		return this.threeYearGrossReturn;
	}


	public void setThreeYearGrossReturn(BigDecimal threeYearGrossReturn) {
		this.threeYearGrossReturn = threeYearGrossReturn;
	}


	public BigDecimal getThreeYearBenchmarkReturn() {
		return this.threeYearBenchmarkReturn;
	}


	public void setThreeYearBenchmarkReturn(BigDecimal threeYearBenchmarkReturn) {
		this.threeYearBenchmarkReturn = threeYearBenchmarkReturn;
	}


	public BigDecimal getThreeYearBenchmarkStandardDeviation() {
		return this.threeYearBenchmarkStandardDeviation;
	}


	public void setThreeYearBenchmarkStandardDeviation(BigDecimal threeYearBenchmarkStandardDeviation) {
		this.threeYearBenchmarkStandardDeviation = threeYearBenchmarkStandardDeviation;
	}


	public BigDecimal getThreeYearGrossStandardDeviation() {
		return this.threeYearGrossStandardDeviation;
	}


	public void setThreeYearGrossStandardDeviation(BigDecimal threeYearGrossStandardDeviation) {
		this.threeYearGrossStandardDeviation = threeYearGrossStandardDeviation;
	}


	public BigDecimal getThreeYearNetStandardDeviation() {
		return this.threeYearNetStandardDeviation;
	}


	public void setThreeYearNetStandardDeviation(BigDecimal threeYearNetStandardDeviation) {
		this.threeYearNetStandardDeviation = threeYearNetStandardDeviation;
	}


	public BigDecimal getThreeYearGrossSharpeRatio() {
		return this.threeYearGrossSharpeRatio;
	}


	public void setThreeYearGrossSharpeRatio(BigDecimal threeYearGrossSharpeRatio) {
		this.threeYearGrossSharpeRatio = threeYearGrossSharpeRatio;
	}


	public BigDecimal getThreeYearNetSharpeRatio() {
		return this.threeYearNetSharpeRatio;
	}


	public void setThreeYearNetSharpeRatio(BigDecimal threeYearNetSharpeRatio) {
		this.threeYearNetSharpeRatio = threeYearNetSharpeRatio;
	}


	public BigDecimal getFiveYearGrossReturn() {
		return this.fiveYearGrossReturn;
	}


	public void setFiveYearGrossReturn(BigDecimal fiveYearGrossReturn) {
		this.fiveYearGrossReturn = fiveYearGrossReturn;
	}


	public BigDecimal getFiveYearBenchmarkReturn() {
		return this.fiveYearBenchmarkReturn;
	}


	public void setFiveYearBenchmarkReturn(BigDecimal fiveYearBenchmarkReturn) {
		this.fiveYearBenchmarkReturn = fiveYearBenchmarkReturn;
	}


	public BigDecimal getFiveYearBenchmarkStandardDeviation() {
		return this.fiveYearBenchmarkStandardDeviation;
	}


	public void setFiveYearBenchmarkStandardDeviation(BigDecimal fiveYearBenchmarkStandardDeviation) {
		this.fiveYearBenchmarkStandardDeviation = fiveYearBenchmarkStandardDeviation;
	}


	public BigDecimal getFiveYearGrossStandardDeviation() {
		return this.fiveYearGrossStandardDeviation;
	}


	public void setFiveYearGrossStandardDeviation(BigDecimal fiveYearGrossStandardDeviation) {
		this.fiveYearGrossStandardDeviation = fiveYearGrossStandardDeviation;
	}


	public BigDecimal getFiveYearNetStandardDeviation() {
		return this.fiveYearNetStandardDeviation;
	}


	public void setFiveYearNetStandardDeviation(BigDecimal fiveYearNetStandardDeviation) {
		this.fiveYearNetStandardDeviation = fiveYearNetStandardDeviation;
	}


	public BigDecimal getFiveYearGrossSharpeRatio() {
		return this.fiveYearGrossSharpeRatio;
	}


	public void setFiveYearGrossSharpeRatio(BigDecimal fiveYearGrossSharpeRatio) {
		this.fiveYearGrossSharpeRatio = fiveYearGrossSharpeRatio;
	}


	public BigDecimal getFiveYearNetSharpeRatio() {
		return this.fiveYearNetSharpeRatio;
	}


	public void setFiveYearNetSharpeRatio(BigDecimal fiveYearNetSharpeRatio) {
		this.fiveYearNetSharpeRatio = fiveYearNetSharpeRatio;
	}


	public BigDecimal getTenYearGrossReturn() {
		return this.tenYearGrossReturn;
	}


	public void setTenYearGrossReturn(BigDecimal tenYearGrossReturn) {
		this.tenYearGrossReturn = tenYearGrossReturn;
	}


	public BigDecimal getTenYearBenchmarkReturn() {
		return this.tenYearBenchmarkReturn;
	}


	public void setTenYearBenchmarkReturn(BigDecimal tenYearBenchmarkReturn) {
		this.tenYearBenchmarkReturn = tenYearBenchmarkReturn;
	}


	public BigDecimal getTenYearBenchmarkStandardDeviation() {
		return this.tenYearBenchmarkStandardDeviation;
	}


	public void setTenYearBenchmarkStandardDeviation(BigDecimal tenYearBenchmarkStandardDeviation) {
		this.tenYearBenchmarkStandardDeviation = tenYearBenchmarkStandardDeviation;
	}


	public BigDecimal getTenYearGrossStandardDeviation() {
		return this.tenYearGrossStandardDeviation;
	}


	public void setTenYearGrossStandardDeviation(BigDecimal tenYearGrossStandardDeviation) {
		this.tenYearGrossStandardDeviation = tenYearGrossStandardDeviation;
	}


	public BigDecimal getTenYearNetStandardDeviation() {
		return this.tenYearNetStandardDeviation;
	}


	public void setTenYearNetStandardDeviation(BigDecimal tenYearNetStandardDeviation) {
		this.tenYearNetStandardDeviation = tenYearNetStandardDeviation;
	}


	public BigDecimal getTenYearGrossSharpeRatio() {
		return this.tenYearGrossSharpeRatio;
	}


	public void setTenYearGrossSharpeRatio(BigDecimal tenYearGrossSharpeRatio) {
		this.tenYearGrossSharpeRatio = tenYearGrossSharpeRatio;
	}


	public BigDecimal getTenYearNetSharpeRatio() {
		return this.tenYearNetSharpeRatio;
	}


	public void setTenYearNetSharpeRatio(BigDecimal tenYearNetSharpeRatio) {
		this.tenYearNetSharpeRatio = tenYearNetSharpeRatio;
	}


	public BigDecimal getPeriodModelNetReturn() {
		return this.periodModelNetReturn;
	}


	public void setPeriodModelNetReturn(BigDecimal periodModelNetReturn) {
		this.periodModelNetReturn = periodModelNetReturn;
	}


	public BigDecimal getQuarterToDateModelNetReturn() {
		return this.quarterToDateModelNetReturn;
	}


	public void setQuarterToDateModelNetReturn(BigDecimal quarterToDateModelNetReturn) {
		this.quarterToDateModelNetReturn = quarterToDateModelNetReturn;
	}


	public BigDecimal getYearToDateModelNetReturn() {
		return this.yearToDateModelNetReturn;
	}


	public void setYearToDateModelNetReturn(BigDecimal yearToDateModelNetReturn) {
		this.yearToDateModelNetReturn = yearToDateModelNetReturn;
	}


	public BigDecimal getInceptionToDateModelNetReturn() {
		return this.inceptionToDateModelNetReturn;
	}


	public void setInceptionToDateModelNetReturn(BigDecimal inceptionToDateModelNetReturn) {
		this.inceptionToDateModelNetReturn = inceptionToDateModelNetReturn;
	}


	public BigDecimal getOneYearNetReturn() {
		return this.oneYearNetReturn;
	}


	public void setOneYearNetReturn(BigDecimal oneYearNetReturn) {
		this.oneYearNetReturn = oneYearNetReturn;
	}


	public BigDecimal getOneYearModelNetReturn() {
		return this.oneYearModelNetReturn;
	}


	public void setOneYearModelNetReturn(BigDecimal oneYearModelNetReturn) {
		this.oneYearModelNetReturn = oneYearModelNetReturn;
	}


	public BigDecimal getOneYearModelNetStandardDeviation() {
		return this.oneYearModelNetStandardDeviation;
	}


	public void setOneYearModelNetStandardDeviation(BigDecimal oneYearModelNetStandardDeviation) {
		this.oneYearModelNetStandardDeviation = oneYearModelNetStandardDeviation;
	}


	public BigDecimal getOneYearModelNetSharpeRatio() {
		return this.oneYearModelNetSharpeRatio;
	}


	public void setOneYearModelNetSharpeRatio(BigDecimal oneYearModelNetSharpeRatio) {
		this.oneYearModelNetSharpeRatio = oneYearModelNetSharpeRatio;
	}


	public BigDecimal getThreeYearNetReturn() {
		return this.threeYearNetReturn;
	}


	public void setThreeYearNetReturn(BigDecimal threeYearNetReturn) {
		this.threeYearNetReturn = threeYearNetReturn;
	}


	public BigDecimal getThreeYearModelNetReturn() {
		return this.threeYearModelNetReturn;
	}


	public void setThreeYearModelNetReturn(BigDecimal threeYearModelNetReturn) {
		this.threeYearModelNetReturn = threeYearModelNetReturn;
	}


	public BigDecimal getThreeYearModelNetStandardDeviation() {
		return this.threeYearModelNetStandardDeviation;
	}


	public void setThreeYearModelNetStandardDeviation(BigDecimal threeYearModelNetStandardDeviation) {
		this.threeYearModelNetStandardDeviation = threeYearModelNetStandardDeviation;
	}


	public BigDecimal getThreeYearModelNetSharpeRatio() {
		return this.threeYearModelNetSharpeRatio;
	}


	public void setThreeYearModelNetSharpeRatio(BigDecimal threeYearModelNetSharpeRatio) {
		this.threeYearModelNetSharpeRatio = threeYearModelNetSharpeRatio;
	}


	public BigDecimal getFiveYearNetReturn() {
		return this.fiveYearNetReturn;
	}


	public void setFiveYearNetReturn(BigDecimal fiveYearNetReturn) {
		this.fiveYearNetReturn = fiveYearNetReturn;
	}


	public BigDecimal getFiveYearModelNetReturn() {
		return this.fiveYearModelNetReturn;
	}


	public void setFiveYearModelNetReturn(BigDecimal fiveYearModelNetReturn) {
		this.fiveYearModelNetReturn = fiveYearModelNetReturn;
	}


	public BigDecimal getFiveYearModelNetStandardDeviation() {
		return this.fiveYearModelNetStandardDeviation;
	}


	public void setFiveYearModelNetStandardDeviation(BigDecimal fiveYearModelNetStandardDeviation) {
		this.fiveYearModelNetStandardDeviation = fiveYearModelNetStandardDeviation;
	}


	public BigDecimal getFiveYearModelNetSharpeRatio() {
		return this.fiveYearModelNetSharpeRatio;
	}


	public void setFiveYearModelNetSharpeRatio(BigDecimal fiveYearModelNetSharpeRatio) {
		this.fiveYearModelNetSharpeRatio = fiveYearModelNetSharpeRatio;
	}


	public BigDecimal getTenYearNetReturn() {
		return this.tenYearNetReturn;
	}


	public void setTenYearNetReturn(BigDecimal tenYearNetReturn) {
		this.tenYearNetReturn = tenYearNetReturn;
	}


	public BigDecimal getTenYearModelNetReturn() {
		return this.tenYearModelNetReturn;
	}


	public void setTenYearModelNetReturn(BigDecimal tenYearModelNetReturn) {
		this.tenYearModelNetReturn = tenYearModelNetReturn;
	}


	public BigDecimal getTenYearModelNetStandardDeviation() {
		return this.tenYearModelNetStandardDeviation;
	}


	public void setTenYearModelNetStandardDeviation(BigDecimal tenYearModelNetStandardDeviation) {
		this.tenYearModelNetStandardDeviation = tenYearModelNetStandardDeviation;
	}


	public BigDecimal getTenYearModelNetSharpeRatio() {
		return this.tenYearModelNetSharpeRatio;
	}


	public void setTenYearModelNetSharpeRatio(BigDecimal tenYearModelNetSharpeRatio) {
		this.tenYearModelNetSharpeRatio = tenYearModelNetSharpeRatio;
	}


	public Boolean getPostedToPortal() {
		return this.postedToPortal;
	}


	public void setPostedToPortal(Boolean postedToPortal) {
		this.postedToPortal = postedToPortal;
	}


	public Boolean getRequireFullControlForDraftStatus() {
		return this.requireFullControlForDraftStatus;
	}


	public void setRequireFullControlForDraftStatus(Boolean requireFullControlForDraftStatus) {
		this.requireFullControlForDraftStatus = requireFullControlForDraftStatus;
	}
}
