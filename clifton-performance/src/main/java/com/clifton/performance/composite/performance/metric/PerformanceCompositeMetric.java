package com.clifton.performance.composite.performance.metric;


import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.core.beans.SimpleEntity;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.report.definition.Report;
import com.clifton.workflow.WorkflowAware;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformanceCompositeMetric</code> is any metric that needs generic performance calculations
 * performed on it.
 * <p/>
 * Ex. PerformanceCompositePerformance, PerformanceCompositeInvestmentAccountPerformance
 *
 * @author apopp
 */
public interface PerformanceCompositeMetric extends WorkflowAware, SimpleEntity<Integer> {

	/**
	 * Determine start date of underlying performance metric entity.
	 * <p/>
	 * Ex. Used for determining total days to annualize calculations or if
	 * certain calculations are applicable
	 */
	public Date getStartDate();


	/**
	 * Return label of entity
	 */
	public String getLabel();


	/**
	 * Reporting frequency (used for workflow as to when to move the entity to the reporting states)
	 * Some accounts/composites we don't publish reports - others are monthly or quarterly
	 */
	public PerformanceReportFrequencies getReportingFrequency();


	public Report getReport();


	public InvestmentSecurity getBenchmarkSecurity();


	public InvestmentSecurity getSecondaryBenchmarkSecurity();


	public InvestmentSecurity getThirdBenchmarkSecurity();


	public InvestmentSecurity getRiskFreeSecurity();

	////////////////////////////////////////////////////////////////////////////////


	public AccountingPeriod getAccountingPeriod();


	public void setAccountingPeriod(AccountingPeriod accountingPeriod);


	public BigDecimal getPeriodEndMarketValue();


	public void setPeriodEndMarketValue(BigDecimal periodEndMarketValue);


	public BigDecimal getPeriodGrossReturn();


	public void setPeriodGrossReturn(BigDecimal periodGrossReturn);


	public BigDecimal getPeriodNetReturn();


	public void setPeriodNetReturn(BigDecimal periodNetReturn);


	public BigDecimal getPeriodBenchmarkReturn();


	public void setPeriodBenchmarkReturn(BigDecimal periodBenchmarkReturn);


	public BigDecimal getPeriodSecondaryBenchmarkReturn();


	public void setPeriodSecondaryBenchmarkReturn(BigDecimal periodSecondaryBenchmarkReturn);


	public BigDecimal getPeriodRiskFreeSecurityReturn();


	public void setPeriodRiskFreeSecurityReturn(BigDecimal periodRiskFreeSecurityReturn);


	public BigDecimal getQuarterToDateGrossReturn();


	public void setQuarterToDateGrossReturn(BigDecimal quarterToDateGrossReturn);


	public BigDecimal getQuarterToDateNetReturn();


	public void setQuarterToDateNetReturn(BigDecimal quarterToDateNetReturn);


	public BigDecimal getQuarterToDateBenchmarkReturn();


	public void setQuarterToDateBenchmarkReturn(BigDecimal quarterToDateBenchmarkReturn);


	public BigDecimal getQuarterToDateSecondaryBenchmarkReturn();


	public void setQuarterToDateSecondaryBenchmarkReturn(BigDecimal quarterToDateSecondaryBenchmarkReturn);


	public BigDecimal getQuarterToDateRiskFreeSecurityReturn();


	public void setQuarterToDateRiskFreeSecurityReturn(BigDecimal quarterToDateRiskFreeSecurityReturn);


	public BigDecimal getYearToDateGrossReturn();


	public void setYearToDateGrossReturn(BigDecimal yearToDateGrossReturn);


	public BigDecimal getYearToDateNetReturn();


	public void setYearToDateNetReturn(BigDecimal yearToDateNetReturn);


	public BigDecimal getYearToDateBenchmarkReturn();


	public void setYearToDateBenchmarkReturn(BigDecimal yearToDateBenchmarkReturn);


	public BigDecimal getYearToDateSecondaryBenchmarkReturn();


	public void setYearToDateSecondaryBenchmarkReturn(BigDecimal yearToDateSecondaryBenchmarkReturn);


	public BigDecimal getYearToDateRiskFreeSecurityReturn();


	public void setYearToDateRiskFreeSecurityReturn(BigDecimal yearToDateRiskFreeSecurityReturn);


	public BigDecimal getInceptionToDateGrossReturn();


	public void setInceptionToDateGrossReturn(BigDecimal inceptionToDateGrossReturn);


	public BigDecimal getInceptionToDateNetReturn();


	public void setInceptionToDateNetReturn(BigDecimal inceptionToDateNetReturn);


	public BigDecimal getInceptionToDateBenchmarkReturn();


	public void setInceptionToDateBenchmarkReturn(BigDecimal inceptionToDateBenchmarkReturn);


	public BigDecimal getInceptionToDateSecondaryBenchmarkReturn();


	public void setInceptionToDateSecondaryBenchmarkReturn(BigDecimal inceptionToDateSecondaryBenchmarkReturn);


	public BigDecimal getInceptionToDateRiskFreeSecurityReturn();


	public void setInceptionToDateRiskFreeSecurityReturn(BigDecimal inceptionToDateRiskFreeSecurityReturn);


	public BigDecimal getInceptionToDateGrossStandardDeviation();


	public void setInceptionToDateGrossStandardDeviation(BigDecimal inceptionToDateGrossStandardDeviation);


	public BigDecimal getInceptionToDateNetStandardDeviation();


	public void setInceptionToDateNetStandardDeviation(BigDecimal inceptionToDateNetStandardDeviation);


	public BigDecimal getInceptionToDateBenchmarkStandardDeviation();


	public void setInceptionToDateBenchmarkStandardDeviation(BigDecimal inceptionToDateBenchmarkStandardDeviation);


	public BigDecimal getInceptionToDateSecondaryBenchmarkStandardDeviation();


	public void setInceptionToDateSecondaryBenchmarkStandardDeviation(BigDecimal inceptionToDateSecondaryBenchmarkStandardDeviation);


	public BigDecimal getInceptionToDateGrossSharpeRatio();


	public void setInceptionToDateGrossSharpeRatio(BigDecimal inceptionToDateGrossSharpeRatio);


	public BigDecimal getInceptionToDateNetSharpeRatio();


	public void setInceptionToDateNetSharpeRatio(BigDecimal inceptionToDateNetSharpeRatio);


	public BigDecimal getInceptionToDateBenchmarkSharpeRatio();


	public void setInceptionToDateBenchmarkSharpeRatio(BigDecimal inceptionToDateBenchmarkSharpeRatio);


	public BigDecimal getInceptionToDateSecondaryBenchmarkSharpeRatio();


	public void setInceptionToDateSecondaryBenchmarkSharpeRatio(BigDecimal inceptionToDateSecondaryBenchmarkSharpeRatio);


	public BigDecimal getOneYearGrossReturn();


	public void setOneYearGrossReturn(BigDecimal oneYearGrossReturn);


	public BigDecimal getOneYearNetReturn();


	public void setOneYearNetReturn(BigDecimal oneYearNetReturn);


	public BigDecimal getOneYearBenchmarkReturn();


	public void setOneYearBenchmarkReturn(BigDecimal oneYearBenchmarkReturn);


	public BigDecimal getOneYearSecondaryBenchmarkReturn();


	public void setOneYearSecondaryBenchmarkReturn(BigDecimal oneYearSecondaryBenchmarkReturn);


	public BigDecimal getOneYearRiskFreeSecurityReturn();


	public void setOneYearRiskFreeSecurityReturn(BigDecimal oneYearRiskFreeSecurityReturn);


	public BigDecimal getOneYearGrossStandardDeviation();


	public void setOneYearGrossStandardDeviation(BigDecimal oneYearGrossStandardDeviation);


	public BigDecimal getOneYearNetStandardDeviation();


	public void setOneYearNetStandardDeviation(BigDecimal oneYearNetStandardDeviation);


	public BigDecimal getOneYearBenchmarkStandardDeviation();


	public void setOneYearBenchmarkStandardDeviation(BigDecimal oneYearBenchmarkStandardDeviation);


	public BigDecimal getOneYearSecondaryBenchmarkStandardDeviation();


	public void setOneYearSecondaryBenchmarkStandardDeviation(BigDecimal oneYearSecondaryBenchmarkStandardDeviation);


	public BigDecimal getOneYearGrossSharpeRatio();


	public void setOneYearGrossSharpeRatio(BigDecimal oneYearGrossSharpeRatio);


	public BigDecimal getOneYearNetSharpeRatio();


	public void setOneYearNetSharpeRatio(BigDecimal oneYearNetSharpeRatio);


	public BigDecimal getOneYearBenchmarkSharpeRatio();


	public void setOneYearBenchmarkSharpeRatio(BigDecimal oneYearBenchmarkSharpeRatio);


	public BigDecimal getOneYearSecondaryBenchmarkSharpeRatio();


	public void setOneYearSecondaryBenchmarkSharpeRatio(BigDecimal oneYearSecondaryBenchmarkSharpeRatio);


	public BigDecimal getThreeYearGrossReturn();


	public void setThreeYearGrossReturn(BigDecimal threeYearGrossReturn);


	public BigDecimal getThreeYearNetReturn();


	public void setThreeYearNetReturn(BigDecimal threeYearNetReturn);


	public BigDecimal getThreeYearBenchmarkReturn();


	public void setThreeYearBenchmarkReturn(BigDecimal threeYearBenchmarkReturn);


	public BigDecimal getThreeYearSecondaryBenchmarkReturn();


	public void setThreeYearSecondaryBenchmarkReturn(BigDecimal threeYearSecondaryBenchmarkReturn);


	public BigDecimal getThreeYearRiskFreeSecurityReturn();


	public void setThreeYearRiskFreeSecurityReturn(BigDecimal threeYearRiskFreeSecurityReturn);


	public BigDecimal getThreeYearGrossStandardDeviation();


	public void setThreeYearGrossStandardDeviation(BigDecimal threeYearGrossStandardDeviation);


	public BigDecimal getThreeYearNetStandardDeviation();


	public void setThreeYearNetStandardDeviation(BigDecimal threeYearNetStandardDeviation);


	public BigDecimal getThreeYearBenchmarkStandardDeviation();


	public void setThreeYearBenchmarkStandardDeviation(BigDecimal threeYearBenchmarkStandardDeviation);


	public BigDecimal getThreeYearSecondaryBenchmarkStandardDeviation();


	public void setThreeYearSecondaryBenchmarkStandardDeviation(BigDecimal threeYearSecondaryBenchmarkStandardDeviation);


	public BigDecimal getThreeYearGrossSharpeRatio();


	public void setThreeYearGrossSharpeRatio(BigDecimal threeYearGrossSharpeRatio);


	public BigDecimal getThreeYearNetSharpeRatio();


	public void setThreeYearNetSharpeRatio(BigDecimal threeYearNetSharpeRatio);


	public BigDecimal getThreeYearBenchmarkSharpeRatio();


	public void setThreeYearBenchmarkSharpeRatio(BigDecimal threeYearBenchmarkSharpeRatio);


	public BigDecimal getThreeYearSecondaryBenchmarkSharpeRatio();


	public void setThreeYearSecondaryBenchmarkSharpeRatio(BigDecimal threeYearSecondaryBenchmarkSharpeRatio);


	public BigDecimal getFiveYearGrossReturn();


	public void setFiveYearGrossReturn(BigDecimal fiveYearGrossReturn);


	public BigDecimal getFiveYearNetReturn();


	public void setFiveYearNetReturn(BigDecimal fiveYearNetReturn);


	public BigDecimal getFiveYearBenchmarkReturn();


	public void setFiveYearBenchmarkReturn(BigDecimal fiveYearBenchmarkReturn);


	public BigDecimal getFiveYearSecondaryBenchmarkReturn();


	public void setFiveYearSecondaryBenchmarkReturn(BigDecimal fiveYearSecondaryBenchmarkReturn);


	public BigDecimal getFiveYearRiskFreeSecurityReturn();


	public void setFiveYearRiskFreeSecurityReturn(BigDecimal fiveYearRiskFreeSecurityReturn);


	public BigDecimal getFiveYearGrossStandardDeviation();


	public void setFiveYearGrossStandardDeviation(BigDecimal fiveYearGrossStandardDeviation);


	public BigDecimal getFiveYearNetStandardDeviation();


	public void setFiveYearNetStandardDeviation(BigDecimal fiveYearNetStandardDeviation);


	public BigDecimal getFiveYearSecondaryBenchmarkStandardDeviation();


	public void setFiveYearSecondaryBenchmarkStandardDeviation(BigDecimal fiveYearSecondaryBenchmarkStandardDeviation);


	public BigDecimal getFiveYearBenchmarkStandardDeviation();


	public void setFiveYearBenchmarkStandardDeviation(BigDecimal fiveYearBenchmarkStandardDeviation);


	public BigDecimal getFiveYearGrossSharpeRatio();


	public void setFiveYearGrossSharpeRatio(BigDecimal fiveYearGrossSharpeRatio);


	public BigDecimal getFiveYearNetSharpeRatio();


	public void setFiveYearNetSharpeRatio(BigDecimal fiveYearNetSharpeRatio);


	public BigDecimal getFiveYearBenchmarkSharpeRatio();


	public void setFiveYearBenchmarkSharpeRatio(BigDecimal fiveYearBenchmarkSharpeRatio);


	public BigDecimal getFiveYearSecondaryBenchmarkSharpeRatio();


	public void setFiveYearSecondaryBenchmarkSharpeRatio(BigDecimal fiveYearSecondaryBenchmarkSharpeRatio);


	public BigDecimal getTenYearGrossReturn();


	public void setTenYearGrossReturn(BigDecimal tenYearGrossReturn);


	public BigDecimal getTenYearNetReturn();


	public void setTenYearNetReturn(BigDecimal tenYearNetReturn);


	public BigDecimal getTenYearBenchmarkReturn();


	public void setTenYearBenchmarkReturn(BigDecimal tenYearBenchmarkReturn);


	public BigDecimal getTenYearSecondaryBenchmarkReturn();


	public void setTenYearSecondaryBenchmarkReturn(BigDecimal tenYearSecondaryBenchmarkReturn);


	public BigDecimal getTenYearRiskFreeSecurityReturn();


	public void setTenYearRiskFreeSecurityReturn(BigDecimal tenYearRiskFreeSecurityReturn);


	public BigDecimal getTenYearGrossStandardDeviation();


	public void setTenYearGrossStandardDeviation(BigDecimal tenYearGrossStandardDeviation);


	public BigDecimal getTenYearNetStandardDeviation();


	public void setTenYearNetStandardDeviation(BigDecimal tenYearNetStandardDeviation);


	public BigDecimal getTenYearSecondaryBenchmarkStandardDeviation();


	public void setTenYearSecondaryBenchmarkStandardDeviation(BigDecimal tenYearSecondaryBenchmarkStandardDeviation);


	public BigDecimal getTenYearBenchmarkStandardDeviation();


	public void setTenYearBenchmarkStandardDeviation(BigDecimal tenYearBenchmarkStandardDeviation);


	public BigDecimal getTenYearGrossSharpeRatio();


	public void setTenYearGrossSharpeRatio(BigDecimal tenYearGrossSharpeRatio);


	public BigDecimal getTenYearNetSharpeRatio();


	public void setTenYearNetSharpeRatio(BigDecimal tenYearNetSharpeRatio);


	public BigDecimal getTenYearBenchmarkSharpeRatio();


	public void setTenYearBenchmarkSharpeRatio(BigDecimal tenYearBenchmarkSharpeRatio);


	public BigDecimal getTenYearSecondaryBenchmarkSharpeRatio();


	public void setTenYearSecondaryBenchmarkSharpeRatio(BigDecimal tenYearSecondaryBenchmarkSharpeRatio);


	public BigDecimal getPeriodModelNetReturn();


	public void setPeriodModelNetReturn(BigDecimal periodModelNetReturn);


	public BigDecimal getQuarterToDateModelNetReturn();


	public void setQuarterToDateModelNetReturn(BigDecimal quarterToDateModelNetReturn);


	public BigDecimal getYearToDateModelNetReturn();


	public void setYearToDateModelNetReturn(BigDecimal yearToDateModelNetReturn);


	public BigDecimal getInceptionToDateModelNetReturn();


	public void setInceptionToDateModelNetReturn(BigDecimal inceptionToDateModelNetReturn);


	public BigDecimal getInceptionToDateModelNetStandardDeviation();


	public void setInceptionToDateModelNetStandardDeviation(BigDecimal inceptionToDateModelNetStandardDeviation);


	public BigDecimal getInceptionToDateModelNetSharpeRatio();


	public void setInceptionToDateModelNetSharpeRatio(BigDecimal inceptionToDateModelNetSharpeRatio);


	public BigDecimal getOneYearModelNetReturn();


	public void setOneYearModelNetReturn(BigDecimal oneYearModelNetReturn);


	public BigDecimal getOneYearModelNetStandardDeviation();


	public void setOneYearModelNetStandardDeviation(BigDecimal oneYearModelNetStandardDeviation);


	public BigDecimal getOneYearModelNetSharpeRatio();


	public void setOneYearModelNetSharpeRatio(BigDecimal oneYearModelNetSharpeRatio);


	public BigDecimal getThreeYearModelNetReturn();


	public void setThreeYearModelNetReturn(BigDecimal threeYearModelNetReturn);


	public BigDecimal getThreeYearModelNetStandardDeviation();


	public void setThreeYearModelNetStandardDeviation(BigDecimal threeYearModelNetStandardDeviation);


	public BigDecimal getThreeYearModelNetSharpeRatio();


	public void setThreeYearModelNetSharpeRatio(BigDecimal threeYearModelNetSharpeRatio);


	public BigDecimal getFiveYearModelNetReturn();


	public void setFiveYearModelNetReturn(BigDecimal fiveYearModelNetReturn);


	public BigDecimal getFiveYearModelNetStandardDeviation();


	public void setFiveYearModelNetStandardDeviation(BigDecimal fiveYearModelNetStandardDeviation);


	public BigDecimal getFiveYearModelNetSharpeRatio();


	public void setFiveYearModelNetSharpeRatio(BigDecimal fiveYearModelNetSharpeRatio);


	public BigDecimal getTenYearModelNetReturn();


	public void setTenYearModelNetReturn(BigDecimal tenYearModelNetReturn);


	public BigDecimal getTenYearModelNetStandardDeviation();


	public void setTenYearModelNetStandardDeviation(BigDecimal tenYearModelNetStandardDeviation);


	public BigDecimal getTenYearModelNetSharpeRatio();


	public void setTenYearModelNetSharpeRatio(BigDecimal tenYearModelNetSharpeRatio);


	public BigDecimal getPeriodThirdBenchmarkReturn();


	public void setPeriodThirdBenchmarkReturn(BigDecimal periodThirdBenchmarkReturn);


	public BigDecimal getQuarterToDateThirdBenchmarkReturn();


	public void setQuarterToDateThirdBenchmarkReturn(BigDecimal quarterToDateThirdBenchmarkReturn);


	public BigDecimal getYearToDateThirdBenchmarkReturn();


	public void setYearToDateThirdBenchmarkReturn(BigDecimal yearToDateThirdBenchmarkReturn);


	public BigDecimal getInceptionToDateThirdBenchmarkReturn();


	public void setInceptionToDateThirdBenchmarkReturn(BigDecimal inceptionToDateThirdBenchmarkReturn);


	public BigDecimal getInceptionToDateThirdBenchmarkStandardDeviation();


	public void setInceptionToDateThirdBenchmarkStandardDeviation(BigDecimal inceptionToDateThirdBenchmarkStandardDeviation);


	public BigDecimal getInceptionToDateThirdBenchmarkSharpeRatio();


	public void setInceptionToDateThirdBenchmarkSharpeRatio(BigDecimal inceptionToDateThirdBenchmarkSharpeRatio);


	public void setOneYearThirdBenchmarkStandardDeviation(BigDecimal oneYearThirdBenchmarkStandardDeviation);


	public BigDecimal getOneYearThirdBenchmarkReturn();


	public void setOneYearThirdBenchmarkReturn(BigDecimal oneYearThirdBenchmarkReturn);


	public BigDecimal getOneYearThirdBenchmarkStandardDeviation();


	public BigDecimal getOneYearThirdBenchmarkSharpeRatio();


	public void setOneYearThirdBenchmarkSharpeRatio(BigDecimal oneYearThirdBenchmarkSharpeRatio);


	public BigDecimal getThreeYearThirdBenchmarkReturn();


	public void setThreeYearThirdBenchmarkReturn(BigDecimal threeYearThirdBenchmarkReturn);


	public BigDecimal getThreeYearThirdBenchmarkStandardDeviation();


	public void setThreeYearThirdBenchmarkStandardDeviation(BigDecimal threeYearThirdBenchmarkStandardDeviation);


	public BigDecimal getThreeYearThirdBenchmarkSharpeRatio();


	public void setThreeYearThirdBenchmarkSharpeRatio(BigDecimal threeYearThirdBenchmarkSharpeRatio);


	public BigDecimal getFiveYearThirdBenchmarkReturn();


	public void setFiveYearThirdBenchmarkReturn(BigDecimal fiveYearThirdBenchmarkReturn);


	public BigDecimal getFiveYearThirdBenchmarkStandardDeviation();


	public void setFiveYearThirdBenchmarkStandardDeviation(BigDecimal fiveYearThirdBenchmarkStandardDeviation);


	public BigDecimal getFiveYearThirdBenchmarkSharpeRatio();


	public void setFiveYearThirdBenchmarkSharpeRatio(BigDecimal fiveYearThirdBenchmarkSharpeRatio);


	public BigDecimal getTenYearThirdBenchmarkReturn();


	public void setTenYearThirdBenchmarkReturn(BigDecimal tenYearThirdBenchmarkReturn);


	public BigDecimal getTenYearThirdBenchmarkStandardDeviation();


	public void setTenYearThirdBenchmarkStandardDeviation(BigDecimal tenYearThirdBenchmarkStandardDeviation);


	public BigDecimal getTenYearThirdBenchmarkSharpeRatio();


	public void setTenYearThirdBenchmarkSharpeRatio(BigDecimal tenYearThirdBenchmarkSharpeRatio);
}
