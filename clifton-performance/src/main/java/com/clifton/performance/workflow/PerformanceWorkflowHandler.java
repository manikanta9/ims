package com.clifton.performance.workflow;

import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;

import java.util.List;


/**
 * The <code>PerformanceWorkflowHandler</code> is used for working directly workflow and various performance objects
 *
 * @author manderson
 */
public interface PerformanceWorkflowHandler {

	public static final String PERFORMANCE_DRAFT_WORKFLOW_STATE_NAME = "Draft";


	/**
	 * Attempts to transition each {@link PerformanceCompositePerformance} entity in the list to the given workflow state.  Will skip if the entity is already in the given state
	 * and will return true if at least one entity in the list is transitioned
	 */
	public boolean transitionPerformanceCompositePerformanceList(List<PerformanceCompositePerformance> compositePerformanceList, String workflowStateName);


	/**
	 * Attempts to transition each {@link PerformanceCompositeInvestmentAccountPerformance} entity in the list to the given workflow state.  Will skip if the entity is already in the given state
	 * and will return true if at least one entity in the list is transitioned
	 */
	public boolean transitionPerformanceCompositeInvestmentAccountPerformanceList(List<PerformanceCompositeInvestmentAccountPerformance> compositeAccountPerformanceList, String workflowStateName);
}
