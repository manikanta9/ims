package com.clifton.performance.workflow.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.performance.workflow.PerformanceWorkflowHandler;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>PerformanceCompositeMetricTransitionWorkflowAction</code> can be used during the transition of an entity that extends {@link PerformanceCompositeMetric}
 * to transition the associated "parent" entity
 * <p>
 * Example, when a {@link PerformanceCompositeInvestmentAccountPerformance} is transitioned back to Draft, we automatically move the associated {@link com.clifton.performance.composite.setup.PerformanceComposite} back to Draft
 * Or, when a {@link com.clifton.performance.composite.setup.PerformanceComposite} is transitioned back to Draft, we automatically move any rollup composites back to Draft
 *
 * @author manderson
 */
public class PerformanceCompositeMetricTransitionWorkflowAction<T extends PerformanceCompositeMetric> implements WorkflowTransitionActionHandler<T>, ValidationAware {

	private PerformanceWorkflowHandler performanceWorkflowHandler;
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	////////////////////////////////////////////////////////////////////////////////

	private String workflowStateName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getWorkflowStateName(), "Workflow State Name to Transition to Is Required");
	}


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		validate();
		if (bean != null) {
			List<PerformanceCompositePerformance> performanceCompositePerformanceList;
			if (bean instanceof PerformanceCompositePerformance) {
				performanceCompositePerformanceList = getPerformanceCompositePerformanceListForChildCompositePerformance((PerformanceCompositePerformance) bean);
			}
			else if (bean instanceof PerformanceCompositeInvestmentAccountPerformance) {
				performanceCompositePerformanceList = getPerformanceCompositePerformanceListForCompositeAccountPerformance((PerformanceCompositeInvestmentAccountPerformance) bean);
			}
			else {
				throw new ValidationException(getClass().getName() + " is not supported to process workflow action for bean class [" + bean.getClass().getName() + "]");
			}
			if (!CollectionUtils.isEmpty(performanceCompositePerformanceList)) {
				getPerformanceWorkflowHandler().transitionPerformanceCompositePerformanceList(performanceCompositePerformanceList, getWorkflowStateName());
			}
		}
		return bean;
	}


	private List<PerformanceCompositePerformance> getPerformanceCompositePerformanceListForChildCompositePerformance(PerformanceCompositePerformance compositePerformance) {
		List<PerformanceComposite> parentCompositeList = getPerformanceCompositeSetupService().getPerformanceCompositeParentList(compositePerformance.getPerformanceComposite().getId());
		if (!CollectionUtils.isEmpty(parentCompositeList)) {
			PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
			searchForm.setAccountingPeriodId(compositePerformance.getAccountingPeriod().getId());
			if (parentCompositeList.size() == 1) {
				searchForm.setPerformanceCompositeId(parentCompositeList.get(0).getId());
			}
			else {
				searchForm.setPerformanceCompositeIds(BeanUtils.getBeanIdentityArray(parentCompositeList, Short.class));
			}
			return getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(searchForm);
		}
		return null;
	}


	private List<PerformanceCompositePerformance> getPerformanceCompositePerformanceListForCompositeAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		PerformanceCompositeInvestmentAccountSearchForm searchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		searchForm.setInvestmentAccountId(accountPerformance.getClientAccount().getId());
		searchForm.setActiveOnDate(accountPerformance.getAccountingPeriod().getStartDate());
		List<PerformanceCompositeInvestmentAccount> assignmentList = getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(searchForm);
		if (!CollectionUtils.isEmpty(assignmentList)) {
			PerformanceCompositePerformanceSearchForm compositePerformanceSearchForm = new PerformanceCompositePerformanceSearchForm();
			compositePerformanceSearchForm.setAccountingPeriodId(accountPerformance.getAccountingPeriod().getId());
			if (assignmentList.size() == 1) {
				compositePerformanceSearchForm.setPerformanceCompositeId(assignmentList.get(0).getPerformanceComposite().getId());
			}
			else {
				compositePerformanceSearchForm.setPerformanceCompositeIds(BeanUtils.getPropertyValues(assignmentList, performanceCompositeInvestmentAccount -> performanceCompositeInvestmentAccount.getPerformanceComposite().getId(), Short.class));
			}
			return getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(compositePerformanceSearchForm);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceWorkflowHandler getPerformanceWorkflowHandler() {
		return this.performanceWorkflowHandler;
	}


	public void setPerformanceWorkflowHandler(PerformanceWorkflowHandler performanceWorkflowHandler) {
		this.performanceWorkflowHandler = performanceWorkflowHandler;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public String getWorkflowStateName() {
		return this.workflowStateName;
	}


	public void setWorkflowStateName(String workflowStateName) {
		this.workflowStateName = workflowStateName;
	}
}
