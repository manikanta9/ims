package com.clifton.performance.workflow;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class PerformanceWorkflowHandlerImpl implements PerformanceWorkflowHandler {


	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean transitionPerformanceCompositePerformanceList(List<PerformanceCompositePerformance> compositePerformanceList, String workflowStateName) {
		return transitionPerformanceCompositeMetricList(compositePerformanceList, PerformanceCompositePerformance.PERFORMANCE_COMPOSITE_PERFORMANCE_TABLE_NAME, workflowStateName);
	}


	@Override
	public boolean transitionPerformanceCompositeInvestmentAccountPerformanceList(List<PerformanceCompositeInvestmentAccountPerformance> compositeAccountPerformanceList, String workflowStateName) {
		return transitionPerformanceCompositeMetricList(compositeAccountPerformanceList, "PerformanceCompositeInvestmentAccountPerformance", workflowStateName);
	}


	private <T extends PerformanceCompositeMetric> boolean transitionPerformanceCompositeMetricList(List<T> performanceCompositeMetricList, String tableName, String workflowStateName) {
		WorkflowState toWorkflowState = null;
		for (T performanceCompositeMetric : CollectionUtils.getIterable(performanceCompositeMetricList)) {
			if (performanceCompositeMetric.getWorkflowState() != null && !StringUtils.isEqual(performanceCompositeMetric.getWorkflowState().getName(), workflowStateName)) {
				if (toWorkflowState == null) {
					toWorkflowState = getWorkflowDefinitionService().getWorkflowStateByName(performanceCompositeMetric.getWorkflowState().getWorkflow().getId(), workflowStateName);
					if (toWorkflowState == null) {
						throw new ValidationException("Cannot transition " + tableName + " entity : " + performanceCompositeMetric.getLabel() + ". Unable to find workflow state with name [" + workflowStateName + "] under workflow [" + performanceCompositeMetric.getWorkflowState().getWorkflow().getName() + "]");
					}
				}
				getWorkflowTransitionService().executeWorkflowTransition(tableName, performanceCompositeMetric.getId(), toWorkflowState.getId());
			}
		}
		// If to state is not null then we transitioned at least one performance record.  If so, return true
		return toWorkflowState != null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}
}
