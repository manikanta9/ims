package com.clifton.performance.workflow.action;


import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.report.PerformanceReportFrequencies;
import com.clifton.performance.report.PerformanceReportService;
import com.clifton.report.posting.ReportPostingConfiguration;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.report.posting.ReportPostingService;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.LinkedHashMap;


/**
 * The <code>ProductPerformanceReportPostWorkflowAction</code> handles posting the report
 * associated with the {@link PerformanceInvestmentAccount} to the portal.
 *
 * @author manderson
 */
public class PerformanceInvestmentAccountReportPostWorkflowAction implements WorkflowTransitionActionHandler<PerformanceInvestmentAccount> {

	private static final String PORTAL_FILE_GROUP_PERFORMANCE_SUMMARY = "Performance Summary";
	private static final String PORTAL_FILE_PERFORMANCE_SUMMARY_SOURCE_SECTION_NAME = "Performance Summary";

	/**
	 * Custom field on the account that can be populated with an additional format to post the Performance Summary report as.
	 * All reports are always posted as PDF, however some accounts also post the report in Excel format
	 */
	private static final String PORTAL_FILE_ADDITIONAL_REPORT_FORMAT_COLUMN_NAME = "Performance Summary Post Additional Format";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;
	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;
	private PerformanceReportService performanceReportService;
	private ReportPostingService<PerformanceInvestmentAccount> reportPostingService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceInvestmentAccount processAction(PerformanceInvestmentAccount bean, WorkflowTransition transition) {
		// Get which report to run for the client account for this run - will throw an exception if can't find one
		Integer reportId = getPerformanceReportService().getPerformanceInvestmentAccountReportId(bean);
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("ClientAccountID", bean.getClientAccount().getId());
		parameters.put("ReportingDate", DateUtils.fromDateShort(bean.getMeasureDate()));

		ReportPostingFileConfiguration postingConfig = new ReportPostingFileConfiguration();
		postingConfig.setReportDate(bean.getMeasureDate());
		postingConfig.setFileDuplicateAction(FileDuplicateActions.REPLACE); // Existing files are unapproved when run is sent back, so we want to replace the existing file with the new one when posting again
		postingConfig.setPortalFileCategoryName(PORTAL_FILE_GROUP_PERFORMANCE_SUMMARY);
		postingConfig.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		postingConfig.setPostEntitySourceFkFieldId(bean.getClientAccount().getId());
		postingConfig.setSourceEntitySectionName(PORTAL_FILE_PERFORMANCE_SUMMARY_SOURCE_SECTION_NAME);
		postingConfig.setSourceEntityFkFieldId(bean.getId());

		// IF REPORTING FREQUENCY IS MONTHLY - THEN USE MONTHLY ALWAYS
		PerformanceReportFrequencies reportFrequency = getPerformanceReportService().getPerformanceInvestmentAccountReportFrequency(bean);
		postingConfig.setPortalFileFrequency(reportFrequency.name());
		// If it's NOT monthly, check the date (so if posting May it doesn't show up as a quarterly post)
		if (PerformanceReportFrequencies.MONTHLY != reportFrequency) {
			if (!DateUtils.isEqualWithoutTime(DateUtils.getLastDayOfQuarter(postingConfig.getReportDate()), postingConfig.getReportDate())) {
				postingConfig.setPortalFileFrequency(PerformanceReportFrequencies.MONTHLY.name());
			}
		}

		ReportPostingConfiguration config = new ReportPostingConfiguration();
		config.setReportId(reportId);
		config.setParameterMap(parameters);
		config.setCacheIgnored(true);
		config.setReportPostingFileConfiguration(postingConfig);
		bean = getReportPostingService().postReportPostingClientWithSource(config, bean);

		// See if the account posts a secondary file type as well
		String additionalFormatString = (String) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(bean.getClientAccount(), PORTAL_FILE_ADDITIONAL_REPORT_FORMAT_COLUMN_NAME);
		if (!StringUtils.isEmpty(additionalFormatString)) {
			FileFormats additionalFormat = FileFormats.valueOf(additionalFormatString);
			if (additionalFormat != config.getExportFormat()) {
				config.setExportFormat(additionalFormat);
				bean = getReportPostingService().postReportPostingClientWithSource(config, bean);
			}
		}
		return bean;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReportPostingService<PerformanceInvestmentAccount> getReportPostingService() {
		return this.reportPostingService;
	}


	public void setReportPostingService(ReportPostingService<PerformanceInvestmentAccount> reportPostingService) {
		this.reportPostingService = reportPostingService;
	}


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public PerformanceReportService getPerformanceReportService() {
		return this.performanceReportService;
	}


	public void setPerformanceReportService(PerformanceReportService performanceReportService) {
		this.performanceReportService = performanceReportService;
	}
}
