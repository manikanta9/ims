package com.clifton.performance.workflow.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric;
import com.clifton.performance.composite.performance.metric.PerformanceCompositeMetricProcessingTypes;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeAccountRebuildCommand;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositePerformanceRebuildService;
import com.clifton.performance.composite.performance.rebuild.PerformanceCompositeRebuildCommand;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.search.PerformanceCompositeInvestmentAccountSearchForm;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;

import java.util.List;


/**
 * The <code>PerformanceCompositeMetricRebuildDependenciesWorkflowAction</code> can be used during the approval transition of an entity that extends {@link com.clifton.performance.composite.performance.metric.PerformanceCompositeMetric}
 * to process the future performance for that composite or account's linked returns, and/or for accounts to automatically re-process the existing composite returns for the same period
 * <p>
 * Note: This is currently set up to run when the performance enters Pending Approval workflow state
 * <p>
 * Future Linked Returns Example: April, May, and June performance exists
 * April is moved back to Draft, and re-processed.  On Approval of April, May and June's QTD, YTD, ITD, One Year, etc. returns need to be updated to reflect April's changes
 * <p>
 * Composite Re-processing for same period:
 * Account X was reprocessed for April and gross return was updated - Account X is in Composite Y, so Composite Y will be rebuilt
 * Composite Y is a child of Composite Z - so Composite Z will also be rebuilt for April
 * <p>
 * <p>
 * Example, when a {@link com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance} is transitioned back to Draft, we automatically move the associated {@link com.clifton.performance.composite.setup.PerformanceComposite} back to Draft
 * Or, when a {@link com.clifton.performance.composite.setup.PerformanceComposite} is transitioned back to Draft, we automatically move any rollup composites back to Draft
 *
 * @author manderson
 */
public class PerformanceCompositeMetricRebuildDependenciesWorkflowAction<T extends PerformanceCompositeMetric> implements WorkflowTransitionActionHandler<T>, ValidationAware {


	private PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService;
	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, will move the entities we are re-processing back to draft.
	 * We shouldn't need this as the transition back to draft should move back what we need {@link PerformanceCompositeMetricTransitionWorkflowAction}
	 * And at the account level, the future returns for QTD, YTD, etc changes would not require a Draft performance
	 */
	private boolean transitionToDraftPriorToProcessing;

	/**
	 * Applies to composites and accounts
	 */
	private boolean rebuildFutureLinkedReturns;

	/**
	 * Applies to accounts to rebuild any composites that account is in (included roll up composites)
	 */
	private boolean rebuildCompositeReturnsForSamePeriod;

	/**
	 * Used with rebuildCompositeReturnsForSamePeriod, however will only rebuild a composite if all the accounts are
	 * in a specific status.  This is used to prevent additional unnecessary processing and will only re-process the composite when the last account
	 * reaches the an Approved status.
	 */
	private String rebuildCompositeRequiredAccountPerformanceStatus;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(isRebuildFutureLinkedReturns() || isRebuildCompositeReturnsForSamePeriod(), "At least one rebuild option is required.");
	}


	@Override
	public T processAction(T bean, WorkflowTransition transition) {
		if (bean != null) {
			if (bean instanceof PerformanceCompositePerformance) {
				if (isRebuildFutureLinkedReturns()) {
					rebuildFuturePerformanceCompositePerformanceLinkedReturns((PerformanceCompositePerformance) bean);
				}
			}
			else if (bean instanceof PerformanceCompositeInvestmentAccountPerformance) {
				if (isRebuildFutureLinkedReturns()) {
					rebuildFuturePerformanceCompositeInvestmentAccountPerformanceLinkedReturns((PerformanceCompositeInvestmentAccountPerformance) bean);
				}
				if (isRebuildCompositeReturnsForSamePeriod()) {
					rebuildCompositeReturnsForPerformanceCompositeInvestmentAccountPerformance((PerformanceCompositeInvestmentAccountPerformance) bean);
				}
			}
			else {
				throw new ValidationException(getClass().getName() + " is not supported to process workflow action for bean class [" + bean.getClass().getName() + "]");
			}
		}
		return bean;
	}


	/**
	 * Rebuilds Subsequent existing account performance linked returns QTD, YTD, ITD, etc.
	 */
	private void rebuildFuturePerformanceCompositeInvestmentAccountPerformanceLinkedReturns(PerformanceCompositeInvestmentAccountPerformance bean) {
		PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
		searchForm.setInvestmentAccountId(bean.getClientAccount().getId());
		searchForm.setAfterPeriodStartDate(bean.getAccountingPeriod().getEndDate());
		searchForm.setOrderBy("periodEndDate:ASC");
		List<PerformanceCompositeInvestmentAccountPerformance> performanceList = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);
		if (!CollectionUtils.isEmpty(performanceList)) {
			PerformanceCompositeAccountRebuildCommand rebuildCommand = new PerformanceCompositeAccountRebuildCommand();
			rebuildCommand.setCalculateAccountDailyMonthlyBasePerformance(false); // DO NOT REBUILD DAILY/MONTHLY VALUES JUST HISTORICAL QTD, YTD, ETC.
			rebuildCommand.setMetricProcessingType(PerformanceCompositeMetricProcessingTypes.LINKED_RETURNS_ONLY);
			rebuildCommand.setPerformanceCompositeInvestmentAccountId(bean.getPerformanceCompositeInvestmentAccount().getId());
			rebuildCommand.setFromAccountingPeriod(performanceList.get(0).getAccountingPeriod());
			rebuildCommand.setToAccountingPeriod(performanceList.get(performanceList.size() - 1).getAccountingPeriod());
			rebuildCommand.setTransitionToDraftPriorToProcessing(isTransitionToDraftPriorToProcessing());
			rebuildCommand.setSynchronous(true);
			getPerformanceCompositePerformanceRebuildService().rebuildPerformanceCompositeAccountPerformance(rebuildCommand);
		}
	}


	/**
	 * Rebuilds Subsequent existing composite performance linked returns QTD, YTD, ITD, etc.
	 */
	private void rebuildFuturePerformanceCompositePerformanceLinkedReturns(PerformanceCompositePerformance bean) {
		PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
		searchForm.setPerformanceCompositeId(bean.getPerformanceComposite().getId());
		searchForm.setAfterPeriodStartDate(bean.getAccountingPeriod().getEndDate());
		searchForm.setOrderBy("periodEndDate:ASC");
		List<PerformanceCompositePerformance> performanceList = getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(searchForm);
		if (!CollectionUtils.isEmpty(performanceList)) {
			PerformanceCompositeRebuildCommand rebuildCommand = new PerformanceCompositeRebuildCommand();
			rebuildCommand.setPerformanceCompositeId(bean.getPerformanceComposite().getId());
			rebuildCommand.setFromAccountingPeriod(performanceList.get(0).getAccountingPeriod());
			rebuildCommand.setToAccountingPeriod(performanceList.get(performanceList.size() - 1).getAccountingPeriod());
			rebuildCommand.setTransitionToDraftPriorToProcessing(isTransitionToDraftPriorToProcessing());
			rebuildCommand.setMetricProcessingType(PerformanceCompositeMetricProcessingTypes.LINKED_RETURNS_ONLY);
			rebuildCommand.setSynchronous(true);
			getPerformanceCompositePerformanceRebuildService().rebuildPerformanceCompositePerformance(rebuildCommand);
		}
	}


	/**
	 * Rebuilds Composite Performance (Including Roll Ups) for same period as the account performance
	 */
	private void rebuildCompositeReturnsForPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance bean) {
		PerformanceCompositeInvestmentAccountSearchForm assignmentSearchForm = new PerformanceCompositeInvestmentAccountSearchForm();
		assignmentSearchForm.setInvestmentAccountId(bean.getClientAccount().getId());
		assignmentSearchForm.setActiveOnDate(bean.getAccountingPeriod().getStartDate());
		// Direct Assignments
		List<PerformanceCompositeInvestmentAccount> assignmentList = getPerformanceCompositeSetupService().getPerformanceCompositeInvestmentAccountList(assignmentSearchForm);

		if (!CollectionUtils.isEmpty(assignmentList)) {
			Short[] compositeIds = BeanUtils.getPropertyValues(assignmentList, assignment -> assignment.getPerformanceComposite().getId(), Short.class);
			// Include Roll Ups
			PerformanceCompositeSearchForm compositeSearchForm = new PerformanceCompositeSearchForm();
			compositeSearchForm.setChildCompositeIds(compositeIds);
			List<PerformanceComposite> rollupCompositeList = getPerformanceCompositeSetupService().getPerformanceCompositeList(compositeSearchForm);
			if (!CollectionUtils.isEmpty(rollupCompositeList)) {
				compositeIds = ArrayUtils.addAll(compositeIds, BeanUtils.getBeanIdentityArray(rollupCompositeList, Short.class));
			}
			// Get the performance for each composite
			PerformanceCompositePerformanceSearchForm compositePerformanceSearchForm = new PerformanceCompositePerformanceSearchForm();
			compositePerformanceSearchForm.setAccountingPeriodId(bean.getAccountingPeriod().getId());
			if (compositeIds.length == 1) {
				compositePerformanceSearchForm.setPerformanceCompositeId(assignmentList.get(0).getPerformanceComposite().getId());
			}
			else {
				compositePerformanceSearchForm.setPerformanceCompositeIds(compositeIds);
			}
			List<PerformanceCompositePerformance> compositePerformanceList = getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(compositePerformanceSearchForm);
			for (PerformanceCompositePerformance compositePerformance : CollectionUtils.getIterable(compositePerformanceList)) {
				PerformanceCompositeRebuildCommand rebuildCommand = new PerformanceCompositeRebuildCommand();
				rebuildCommand.setPerformanceCompositeId(compositePerformance.getPerformanceComposite().getId());
				rebuildCommand.setFromAccountingPeriod(compositePerformance.getAccountingPeriod());
				rebuildCommand.setToAccountingPeriod(compositePerformance.getAccountingPeriod());
				rebuildCommand.setTransitionToDraftPriorToProcessing(isTransitionToDraftPriorToProcessing());
				rebuildCommand.setSynchronous(true);
				rebuildCommand.setAccountRequiredWorkflowStatusName(getRebuildCompositeRequiredAccountPerformanceStatus());
				getPerformanceCompositePerformanceRebuildService().rebuildPerformanceCompositePerformance(rebuildCommand);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////            Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformanceRebuildService getPerformanceCompositePerformanceRebuildService() {
		return this.performanceCompositePerformanceRebuildService;
	}


	public void setPerformanceCompositePerformanceRebuildService(PerformanceCompositePerformanceRebuildService performanceCompositePerformanceRebuildService) {
		this.performanceCompositePerformanceRebuildService = performanceCompositePerformanceRebuildService;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceCompositeSetupService getPerformanceCompositeSetupService() {
		return this.performanceCompositeSetupService;
	}


	public void setPerformanceCompositeSetupService(PerformanceCompositeSetupService performanceCompositeSetupService) {
		this.performanceCompositeSetupService = performanceCompositeSetupService;
	}


	public boolean isTransitionToDraftPriorToProcessing() {
		return this.transitionToDraftPriorToProcessing;
	}


	public void setTransitionToDraftPriorToProcessing(boolean transitionToDraftPriorToProcessing) {
		this.transitionToDraftPriorToProcessing = transitionToDraftPriorToProcessing;
	}


	public boolean isRebuildFutureLinkedReturns() {
		return this.rebuildFutureLinkedReturns;
	}


	public void setRebuildFutureLinkedReturns(boolean rebuildFutureLinkedReturns) {
		this.rebuildFutureLinkedReturns = rebuildFutureLinkedReturns;
	}


	public boolean isRebuildCompositeReturnsForSamePeriod() {
		return this.rebuildCompositeReturnsForSamePeriod;
	}


	public void setRebuildCompositeReturnsForSamePeriod(boolean rebuildCompositeReturnsForSamePeriod) {
		this.rebuildCompositeReturnsForSamePeriod = rebuildCompositeReturnsForSamePeriod;
	}


	public String getRebuildCompositeRequiredAccountPerformanceStatus() {
		return this.rebuildCompositeRequiredAccountPerformanceStatus;
	}


	public void setRebuildCompositeRequiredAccountPerformanceStatus(String rebuildCompositeRequiredAccountPerformanceStatus) {
		this.rebuildCompositeRequiredAccountPerformanceStatus = rebuildCompositeRequiredAccountPerformanceStatus;
	}
}
