package com.clifton.performance.workflow.action;


import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.metric.BasePerformanceCompositeMetric;
import com.clifton.performance.report.PerformanceReportService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>PerformanceCompositeMetricReportPostWorkflowAction</code> handles posting the report
 * associated with the {@link com.clifton.performance.composite.performance.PerformanceCompositePerformance} or the {@link com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance} to the portal.
 *
 * @author manderson
 */
public class PerformanceCompositeMetricReportPostWorkflowAction implements WorkflowTransitionActionHandler<BasePerformanceCompositeMetric> {

	private PerformanceReportService performanceReportService;

	private ReportPostingController<BasePerformanceCompositeMetric> reportPostingController;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BasePerformanceCompositeMetric processAction(BasePerformanceCompositeMetric bean, WorkflowTransition transition) {
		ReportExportConfiguration exportConfiguration = new ReportExportConfiguration();
		exportConfiguration.setExportFormat(FileFormats.PDF);
		exportConfiguration.setKeepSubReportPageNumbers(true);

		if (bean != null) {
			if (bean instanceof PerformanceCompositePerformance) {
				getPerformanceReportService().postPerformanceCompositePerformanceReportReplace(bean.getId(), exportConfiguration);
			}
			else if (bean instanceof PerformanceCompositeInvestmentAccountPerformance) {
				getPerformanceReportService().postPerformanceCompositeInvestmentAccountPerformanceReportReplace(bean.getId(), exportConfiguration);
			}
			else {
				throw new ValidationException(getClass().getName() + " is not supported to process workflow action for bean class [" + bean.getClass().getName() + "]");
			}
		}
		return bean;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public PerformanceReportService getPerformanceReportService() {
		return this.performanceReportService;
	}


	public void setPerformanceReportService(PerformanceReportService performanceReportService) {
		this.performanceReportService = performanceReportService;
	}


	public ReportPostingController<BasePerformanceCompositeMetric> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<BasePerformanceCompositeMetric> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}
}
