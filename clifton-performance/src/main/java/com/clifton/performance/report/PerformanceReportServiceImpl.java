package com.clifton.performance.report;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.document.DocumentManagementService;
import com.clifton.document.DocumentRecord;
import com.clifton.document.setup.DocumentFile;
import com.clifton.document.setup.DocumentSetupService;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.metric.BasePerformanceCompositeMetric;
import com.clifton.performance.composite.setup.PerformanceComposite;
import com.clifton.portal.file.PortalFileDuplicateException;
import com.clifton.portal.file.setup.PortalFileCategory;
import com.clifton.portal.file.setup.PortalFileCategoryTypes;
import com.clifton.portal.file.setup.PortalFileFrequencies;
import com.clifton.portal.file.setup.PortalFileSetupService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.export.ReportExportService;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.posting.ReportPostingController;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import com.clifton.system.query.search.SystemQuerySearchForm;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * The <code>PerformanceReportServiceImpl</code> is used for working with performance related entities and their reports.  Also contains methods for posting reports to the portal
 *
 * @author manderson
 */
@Service
public class PerformanceReportServiceImpl implements PerformanceReportService {

	// GIPS PERFORMANCE REPORTING FILE CATEGORY NAME
	private static final String PORTAL_FILE_GROUP_PERFORMANCE_REPORTS = "Performance Reports";
	private static final String PORTAL_FILE_GROUP_PRELIMINARY_PERFORMANCE_REPORTS = "Preliminary Performance Reports";

	/**
	 * Private Fund Investor Accounts Post their Account Performance Reports to a separate file group on the portal
	 * The group is system defined and where the files post to is defined by that group.  If the account isn't in the group
	 * the files post to the default group of Performance Reports
	 **/
	private static final String PORTAL_FILE_GROUP_PRIVATE_FUNDS_STATEMENT = "Private Funds Statement";
	private static final String ACCOUNT_GROUP_PRIVATE_FUNDS_STATEMENT = "GIPS Performance Posting: Private Funds Statement";

	private static final String PORTAL_FILE_PERFORMANCE_COMPOSITE_PERFORMANCE_SOURCE_SECTION_NAME = "GIPS Composite Performance";
	private static final String PORTAL_FILE_PERFORMANCE_COMPOSITE_ACCOUNT_PERFORMANCE_SOURCE_SECTION_NAME = "GIPS Account Performance";

	private static final String DEFAULT_REPORT_FILENAME = "Report";

	private DocumentManagementService documentManagementService;
	private DocumentSetupService documentSetupService;

	private InvestmentAccountGroupService investmentAccountGroupService;

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;
	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;

	private PortalFileSetupService portalFileSetupService;

	private ReportExportService reportExportService;
	private ReportPostingController<BasePerformanceCompositeMetric> reportPostingController;

	private SystemQueryExecutionService systemQueryExecutionService;
	private SystemQueryService systemQueryService;


	//////////////////////////////////////////////////////////////////////////////
	/////   Performance Investment Account Query Export Business Methods   ///////
	//////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(timeout = 180)
	public DataTable getPerformanceInvestmentAccountSystemQueryResult(String queryName, int performanceAccountId) {
		SystemQuerySearchForm searchForm = new SystemQuerySearchForm();
		searchForm.setTableName(PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT);
		searchForm.setName(queryName);
		SystemQuery query = CollectionUtils.getOnlyElement(getSystemQueryService().getSystemQueryList(searchForm));

		ValidationUtils.assertNotNull(query, "Cannot find query for table " + PerformanceInvestmentAccount.TABLE_PERFORMANCE_INVESTMENT_ACCOUNT + " and name [" + queryName + "]");
		List<SystemQueryParameter> parameterList = getSystemQueryService().getSystemQueryParameterListByQuery(query.getId());
		ValidationUtils.assertTrue(CollectionUtils.getSize(parameterList) == 1, "System Query " + queryName + " should only have one parameter that is the summary id.");
		SystemQueryParameterValue value = new SystemQueryParameterValue();
		value.setParameter(parameterList.get(0));
		value.setValue(Integer.toString(performanceAccountId));
		query.setParameterValueList(CollectionUtils.createList(value));

		return getSystemQueryExecutionService().getSystemQueryResult(query);
	}


	////////////////////////////////////////////////////////////////////////////
	///////   Performance Investment Account Report Business Methods   /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadPerformanceInvestmentAccountReport(int performanceAccountId, ReportExportConfiguration exportConfiguration) {
		return downloadPerformanceInvestmentAccountReportWithDate(performanceAccountId, null, exportConfiguration);
	}


	@Override
	public FileWrapper downloadPerformanceInvestmentAccountReportWithDate(int performanceAccountId, Date reportingDate, ReportExportConfiguration exportConfiguration) {
		PerformanceInvestmentAccount bean = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(performanceAccountId);
		Integer reportId = getPerformanceInvestmentAccountReportId(bean);
		LinkedHashMap<String, Object> parameters = new LinkedHashMap<>();
		parameters.put("ClientAccountID", bean.getClientAccount().getId());
		parameters.put("ReportingDate", DateUtils.fromDateShort(reportingDate == null ? bean.getAccountingPeriod().getEndDate() : reportingDate));
		exportConfiguration.setParameterMap(parameters);
		exportConfiguration.setReportId(reportId);
		exportConfiguration.setThrowErrorOnBadReport(false);
		return getReportExportService().downloadReportFile(exportConfiguration);
	}


	@Override
	public Integer getPerformanceInvestmentAccountReportId(PerformanceInvestmentAccount performanceInvestmentAccount) {
		// Get which report to run for the client account for this summary
		InvestmentAccount account = performanceInvestmentAccount.getClientAccount();
		if (account != null) {
			// First - Account Specific
			Integer reportId = (Integer) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(account, PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_REPORT);
			if (reportId == null) {
				// Then try the service
				if (account.getBusinessService() != null) {
					reportId = (Integer) getPerformanceInvestmentAccountDataRetriever().getPerformanceBusinessServiceCustomFieldValue(account.getBusinessService(), PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_REPORT);
				}
			}
			if (reportId == null) {
				throw new ValidationException("Performance Report is not available for account " + account.getLabel());
			}
			return reportId;
		}
		throw new ValidationException("Cannot determine performance report for " + performanceInvestmentAccount.getLabel());
	}


	@Override
	public PerformanceReportFrequencies getPerformanceInvestmentAccountReportFrequency(PerformanceInvestmentAccount performanceInvestmentAccount) {
		// Find if the client account is expected to post quarterly
		InvestmentAccount account = performanceInvestmentAccount.getClientAccount();
		if (account != null) {
			Boolean quarterlyFrequency = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(account, PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_REPORT_QUARTERLY_FREQUENCY);
			if (BooleanUtils.isTrue(quarterlyFrequency)) {
				return PerformanceReportFrequencies.QUARTERLY;
			}
		}
		// Otherwise default is Monthly
		return PerformanceReportFrequencies.MONTHLY;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////           Performance Composite Performance Reports          /////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadPerformanceCompositePerformanceReport(int performanceCompositePerformanceId, ReportExportConfiguration exportConfiguration) {
		return downloadPerformanceCompositePerformanceReportList(new Integer[]{performanceCompositePerformanceId}, exportConfiguration);
	}


	@Override
	public FileWrapper downloadPerformanceCompositePerformanceReportList(Integer[] performanceCompositePerformanceIds, ReportExportConfiguration exportConfiguration) {
		ReportConfigParameter param = new ReportConfigParameter();
		param.setName("performanceCompositePerformanceIds");
		param.setValue("" + ArrayUtils.toString(performanceCompositePerformanceIds));
		exportConfiguration.setAdditionalReportParameter(param);
		return getReportExportService().downloadReportFile(exportConfiguration);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////        Performance Composite Performance Posting Reports         ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void postPerformanceCompositePerformanceReportReplace(int performanceCompositePerformanceId, ReportExportConfiguration exportConfiguration) {
		postPerformanceCompositePerformanceReport(performanceCompositePerformanceId, exportConfiguration, FileDuplicateActions.REPLACE);
	}


	@Override
	public void postPerformanceCompositePerformanceReport(int performanceCompositePerformanceId, ReportExportConfiguration exportConfiguration, FileDuplicateActions fileDuplicateAction) {
		PerformanceCompositePerformance compositePerformance = getPerformanceCompositePerformanceService().getPerformanceCompositePerformance(performanceCompositePerformanceId);

		// Until this is part of workflow, we'll need to require the performance to be in Approved Status to ensure it follows the dual approval process
		if (!StringUtils.isEqual(WorkflowStatus.STATUS_APPROVED, compositePerformance.getWorkflowStatus().getName())) {
			throw new ValidationException("You can only post composite performance once it has reached an approved status.");
		}

		// First Get the File
		FileWrapper report = downloadPerformanceCompositePerformanceReport(performanceCompositePerformanceId, exportConfiguration);

		if (report == null || report.getFile() == null) {
			throw new ValidationException("Unable to generate Performance Report for Performance Composite Performance ID: " + performanceCompositePerformanceId);
		}

		ReportPostingFileConfiguration postingFileConfiguration = new ReportPostingFileConfiguration();
		postingFileConfiguration.setFileDuplicateAction(fileDuplicateAction); // Existing files are unapproved when sent back, so we want to replace the existing file with the new one when posting again
		postingFileConfiguration.setPortalFileCategoryName(PORTAL_FILE_GROUP_PERFORMANCE_REPORTS);

		postingFileConfiguration.setPostEntitySourceTableName(PerformanceComposite.PERFORMANCE_COMPOSITE_TABLE_NAME);
		postingFileConfiguration.setPostEntitySourceFkFieldId(BeanUtils.getIdentityAsInteger(compositePerformance.getPerformanceComposite()));

		postingFileConfiguration.setSourceEntitySectionName(PORTAL_FILE_PERFORMANCE_COMPOSITE_PERFORMANCE_SOURCE_SECTION_NAME);
		postingFileConfiguration.setSourceEntityFkFieldId(compositePerformance.getId());
		postingFileConfiguration.setReportDate(compositePerformance.getAccountingPeriod().getEndDate());
		// IF REPORTING FREQUENCY IS MONTHLY - THEN USE MONTHLY ALWAYS
		postingFileConfiguration.setPortalFileFrequency(PerformanceReportFrequencies.QUARTERLY.name());
		if (PerformanceReportFrequencies.MONTHLY == compositePerformance.getReportingFrequency()) {
			postingFileConfiguration.setPortalFileFrequency(PerformanceReportFrequencies.MONTHLY.name());
		}
		// OTHERWISE USE THE DATE TO DETERMINE
		else {
			if (!DateUtils.isEqualWithoutTime(DateUtils.getLastDayOfQuarter(postingFileConfiguration.getReportDate()), postingFileConfiguration.getReportDate())) {
				postingFileConfiguration.setPortalFileFrequency(PerformanceReportFrequencies.MONTHLY.name());
			}
		}
		postingFileConfiguration.setFileWrapper(report);
		try {
			getReportPostingController().postClientFile(postingFileConfiguration, compositePerformance);
		}
		catch (Throwable e) {
			Throwable original = ExceptionUtils.getOriginalException(e);
			if (original instanceof PortalFileDuplicateException) {
				if (((PortalFileDuplicateException) original).isReplaceAllowed()) {
					throw new UserIgnorableValidationException(getClass(), "postPerformanceCompositePerformanceReportReplace", original.getMessage() + "<br><br>Note: Clicking Yes will post this file using the Replace option.");
				}
			}
			throw e;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////   Performance Composite Investment Account Performance Reports   ///////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper downloadPerformanceCompositeInvestmentAccountPerformanceReport(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration) {
		return downloadPerformanceCompositeInvestmentAccountPerformanceReportList(new Integer[]{performanceCompositeInvestmentAccountPerformanceId}, exportConfiguration);
	}


	@Override
	public FileWrapper downloadPerformanceCompositeInvestmentAccountPerformanceReportList(Integer[] performanceCompositeInvestmentAccountPerformanceIds, ReportExportConfiguration exportConfiguration) {
		ReportConfigParameter param = new ReportConfigParameter();
		param.setName("performanceCompositeInvestmentAccountPerformanceIds");
		param.setValue("" + ArrayUtils.toString(performanceCompositeInvestmentAccountPerformanceIds));
		exportConfiguration.setAdditionalReportParameter(param);
		return getReportExportService().downloadReportFile(exportConfiguration);
	}


	/**
	 * Retrieves a FileWrapper of attachments for a PerformanceCompositeInvestmentAccountPerformance record
	 */
	@Override
	public FileWrapper downloadPerformanceCompositeInvestmentAccountPerformanceReportAttachments(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration) {
		FileWrapper result = null;
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformance(performanceCompositeInvestmentAccountPerformanceId);
		if (accountPerformance.getPerformanceCompositeInvestmentAccount().isCoalesceAccountReportAppendAttachments()) {
			result = downloadAttachments(performanceCompositeInvestmentAccountPerformanceId, PerformanceCompositeInvestmentAccountPerformance.PERFORMANCE_COMPOSITE_INVESTMENT_ACCOUNT_PERFORMANCE_TABLE_NAME);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	/////  Performance Composite Investment Account Performance Post Reports   /////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void postPerformanceCompositeInvestmentAccountPerformanceReport(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration) {
		postPerformanceCompositeInvestmentAccountPerformanceReportImpl(performanceCompositeInvestmentAccountPerformanceId, exportConfiguration, FileDuplicateActions.ERROR);
	}


	@Override
	public void postPerformanceCompositeInvestmentAccountPerformanceReportReplace(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration) {
		postPerformanceCompositeInvestmentAccountPerformanceReportImpl(performanceCompositeInvestmentAccountPerformanceId, exportConfiguration, FileDuplicateActions.REPLACE);
	}


	private void postPerformanceCompositeInvestmentAccountPerformanceReportImpl(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration, FileDuplicateActions fileDuplicateAction) {
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformance(performanceCompositeInvestmentAccountPerformanceId);

		// Until this is part of workflow, we'll need to require the performance to be in Approved Status to ensure it follows the dual approval process
		if (!StringUtils.isEqual(WorkflowStatus.STATUS_APPROVED, accountPerformance.getWorkflowStatus().getName())) {
			throw new ValidationException("You can only post account performance once it has reached an approved status.");
		}

		// First Get the File
		FileWrapper report = downloadPerformanceCompositeInvestmentAccountPerformanceReport(performanceCompositeInvestmentAccountPerformanceId, exportConfiguration);

		if (report == null || report.getFile() == null) {
			throw new ValidationException("Unable to generate Performance Report for Performance Composite Account Performance ID: " + performanceCompositeInvestmentAccountPerformanceId);
		}

		ReportPostingFileConfiguration postingFileConfiguration = new ReportPostingFileConfiguration();
		postingFileConfiguration.setFileDuplicateAction(fileDuplicateAction);
		if (getInvestmentAccountGroupService().isInvestmentAccountInGroup(accountPerformance.getClientAccount().getId(), ACCOUNT_GROUP_PRIVATE_FUNDS_STATEMENT)) {
			postingFileConfiguration.setPortalFileCategoryName(PORTAL_FILE_GROUP_PRIVATE_FUNDS_STATEMENT);
		}
		else {
			postingFileConfiguration.setPortalFileCategoryName(PORTAL_FILE_GROUP_PERFORMANCE_REPORTS);
		}

		postingFileConfiguration.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
		postingFileConfiguration.setPostEntitySourceFkFieldId(accountPerformance.getClientAccount().getId());

		postingFileConfiguration.setSourceEntitySectionName(PORTAL_FILE_PERFORMANCE_COMPOSITE_ACCOUNT_PERFORMANCE_SOURCE_SECTION_NAME);
		postingFileConfiguration.setSourceEntityFkFieldId(accountPerformance.getId());
		postingFileConfiguration.setReportDate(accountPerformance.getAccountingPeriod().getEndDate());

		// IF REPORTING FREQUENCY IS MONTHLY - THEN USE MONTHLY ALWAYS
		postingFileConfiguration.setPortalFileFrequency(PerformanceReportFrequencies.QUARTERLY.name());
		if (PerformanceReportFrequencies.MONTHLY == accountPerformance.getReportingFrequency()) {
			postingFileConfiguration.setPortalFileFrequency(PerformanceReportFrequencies.MONTHLY.name());
		}
		// OTHERWISE USE THE DATE TO DETERMINE
		else {
			if (!DateUtils.isEqualWithoutTime(DateUtils.getLastDayOfQuarter(postingFileConfiguration.getReportDate()), postingFileConfiguration.getReportDate())) {
				postingFileConfiguration.setPortalFileFrequency(PerformanceReportFrequencies.MONTHLY.name());
			}
		}

		postingFileConfiguration.setFileWrapper(report);
		try {
			getReportPostingController().postClientFile(postingFileConfiguration, accountPerformance);
		}
		catch (Throwable e) {
			Throwable original = ExceptionUtils.getOriginalException(e);
			if (original instanceof PortalFileDuplicateException) {
				if (((PortalFileDuplicateException) original).isReplaceAllowed()) {
					throw new UserIgnorableValidationException(getClass(), "postPerformanceCompositeInvestmentAccountPerformanceReportReplace", original.getMessage() + "<br><br>Note: Clicking Yes will post this file using the Replace option.");
				}
			}
			throw e;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///  Performance Composite and Account Preliminary Performance Post Reports ////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformanceReportPostingFileConfiguration getPerformanceReportPostingFileConfigurationPreliminary(Integer performanceCompositePerformanceId, Integer performanceCompositeInvestmentAccountPerformanceId) {
		PerformanceReportPostingFileConfiguration postingFileConfiguration = new PerformanceReportPostingFileConfiguration();
		postingFileConfiguration.setExportConfiguration(new ReportExportConfiguration());
		PortalFileCategory category = getPortalFileSetupService().getPortalFileCategoryByCategoryTypeAndName(PortalFileCategoryTypes.FILE_GROUP, PORTAL_FILE_GROUP_PRELIMINARY_PERFORMANCE_REPORTS);
		postingFileConfiguration.setPortalFileCategoryId(category.getId());
		postingFileConfiguration.setPortalFileCategoryName(category.getName());
		postingFileConfiguration.setPortalFileFrequency(PortalFileFrequencies.MONTHLY.name());

		if (performanceCompositePerformanceId != null) {
			PerformanceCompositePerformance compositePerformance = getPerformanceCompositePerformanceService().getPerformanceCompositePerformance(performanceCompositePerformanceId);
			ValidationUtils.assertNotNull(compositePerformance, "Cannot find composite performance with id " + performanceCompositePerformanceId);
			if (!StringUtils.isEqual(WorkflowStatus.STATUS_PENDING, compositePerformance.getWorkflowStatus().getName())) {
				throw new ValidationException("You can only customize posting composite performance for preliminary performance.");
			}
			postingFileConfiguration.setPerformanceCompositePerformance(compositePerformance);
			postingFileConfiguration.setReportDate(compositePerformance.getAccountingPeriod().getEndDate());
			postingFileConfiguration.setReport(compositePerformance.getPerformanceComposite().getCompositeReport());
		}
		else if (performanceCompositeInvestmentAccountPerformanceId != null) {
			PerformanceCompositeInvestmentAccountPerformance accountPerformance = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformance(performanceCompositeInvestmentAccountPerformanceId);
			ValidationUtils.assertNotNull(accountPerformance, "Cannot find composite account performance with id " + performanceCompositeInvestmentAccountPerformanceId);
			if (!StringUtils.isEqual(WorkflowStatus.STATUS_PENDING, accountPerformance.getWorkflowStatus().getName())) {
				throw new ValidationException("You can only customize posting composite performance for preliminary performance.");
			}
			postingFileConfiguration.setPerformanceCompositeInvestmentAccountPerformance(accountPerformance);
			postingFileConfiguration.setReportDate(accountPerformance.getAccountingPeriod().getEndDate());
			postingFileConfiguration.setReport(accountPerformance.getReport());
		}
		else {
			throw new ValidationException("Either a composite performance record, composite account performance record must be selected");
		}
		return postingFileConfiguration;
	}


	@Override
	public PerformanceReportPostingFileConfiguration postPerformanceReportPostingFileConfigurationPreliminary(PerformanceReportPostingFileConfiguration postingFileConfiguration) {
		BasePerformanceCompositeMetric metric;
		FileWrapper report;

		if (postingFileConfiguration.getPerformanceCompositePerformanceId() != null) {
			metric = getPerformanceCompositePerformanceService().getPerformanceCompositePerformance(postingFileConfiguration.getPerformanceCompositePerformanceId());
			report = downloadPerformanceCompositePerformanceReport(metric.getId(), postingFileConfiguration.getExportConfiguration());
			if (postingFileConfiguration.getPostPortalEntityId() == null) {
				postingFileConfiguration.setPostEntitySourceTableName(PerformanceComposite.PERFORMANCE_COMPOSITE_TABLE_NAME);
				postingFileConfiguration.setPostEntitySourceFkFieldId(((PerformanceCompositePerformance) metric).getPerformanceComposite().getId().intValue());
			}
			postingFileConfiguration.setSourceEntitySectionName(PORTAL_FILE_PERFORMANCE_COMPOSITE_PERFORMANCE_SOURCE_SECTION_NAME);
		}
		else if (postingFileConfiguration.getPerformanceCompositeInvestmentAccountPerformanceId() != null) {
			metric = getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformance(postingFileConfiguration.getPerformanceCompositeInvestmentAccountPerformanceId());
			report = downloadPerformanceCompositeInvestmentAccountPerformanceReport(metric.getId(), postingFileConfiguration.getExportConfiguration());
			if (postingFileConfiguration.getPostPortalEntityId() == null) {
				postingFileConfiguration.setPostEntitySourceTableName(InvestmentAccount.INVESTMENT_ACCOUNT_TABLE_NAME);
				postingFileConfiguration.setPostEntitySourceFkFieldId(((PerformanceCompositeInvestmentAccountPerformance) metric).getClientAccount().getId());
			}
			postingFileConfiguration.setSourceEntitySectionName(PORTAL_FILE_PERFORMANCE_COMPOSITE_ACCOUNT_PERFORMANCE_SOURCE_SECTION_NAME);
		}
		else {
			throw new ValidationException("Missing composite performance or composite account performance entity.");
		}

		if (!StringUtils.isEqual(WorkflowStatus.STATUS_PENDING, metric.getWorkflowStatus().getName())) {
			throw new ValidationException("You can only customize posting for preliminary performance.");
		}
		if (report == null || report.getFile() == null) {
			throw new ValidationException("Unable to generate Performance Report for: " + metric.getLabel());
		}

		postingFileConfiguration.setSourceEntityFkFieldId(metric.getId());
		postingFileConfiguration.setFileWrapper(report);
		getReportPostingController().postClientFile(postingFileConfiguration, metric);
		// Clear the file before returning
		postingFileConfiguration.setFileWrapper(null);
		return postingFileConfiguration;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////////


	private FileWrapper downloadAttachments(int id, String tableName) {
		List<DocumentFile> documentFileList = getDocumentSetupService().getDocumentFileListForEntity(tableName, MathUtils.getNumberAsLong(id));
		documentFileList = BeanUtils.filter(documentFileList, DocumentFile::isActive, true);

		List<File> fileList = new ArrayList<>();
		for (DocumentFile documentFile : documentFileList) {
			DocumentRecord record = getDocumentManagementService().getDocumentRecord(DocumentFile.DOCUMENT_FILE_TABLE_NAME, documentFile.getId());
			// Might be missing file in doc system.  This would be likely to happen if not running in a Production environment
			if (record != null && !StringUtils.isEmpty(record.getDocumentId())) {
				FileWrapper document = getDocumentManagementService().downloadDocumentRecordConversion(record.getDocumentId(), FileFormats.PDF.name());
				ValidationUtils.assertNotNull(document, "Cannot download document for " + documentFile.getName());
				fileList.add(new File(document.getFile().getPath()));
			}
			else {
				throw new ValidationException("Cannot find file for " + documentFile.getName());
			}
		}

		if (!CollectionUtils.isEmpty(fileList)) {
			File file = CollectionUtils.getSize(fileList) == 1 ? fileList.get(0) : FileUtils.concatenatePDFs(DEFAULT_REPORT_FILENAME, FileFormats.PDF.getExtension(), fileList, true, false);
			file.deleteOnExit();
			return new FileWrapper(file, DEFAULT_REPORT_FILENAME, true);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public DocumentManagementService getDocumentManagementService() {
		return this.documentManagementService;
	}


	public void setDocumentManagementService(DocumentManagementService documentManagementService) {
		this.documentManagementService = documentManagementService;
	}


	public DocumentSetupService getDocumentSetupService() {
		return this.documentSetupService;
	}


	public void setDocumentSetupService(DocumentSetupService documentSetupService) {
		this.documentSetupService = documentSetupService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.investmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.investmentAccountGroupService = investmentAccountGroupService;
	}


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public ReportExportService getReportExportService() {
		return this.reportExportService;
	}


	public void setReportExportService(ReportExportService reportExportService) {
		this.reportExportService = reportExportService;
	}


	public ReportPostingController<BasePerformanceCompositeMetric> getReportPostingController() {
		return this.reportPostingController;
	}


	public void setReportPostingController(ReportPostingController<BasePerformanceCompositeMetric> reportPostingController) {
		this.reportPostingController = reportPostingController;
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}


	public SystemQueryService getSystemQueryService() {
		return this.systemQueryService;
	}


	public void setSystemQueryService(SystemQueryService systemQueryService) {
		this.systemQueryService = systemQueryService;
	}


	public PortalFileSetupService getPortalFileSetupService() {
		return this.portalFileSetupService;
	}


	public void setPortalFileSetupService(PortalFileSetupService portalFileSetupService) {
		this.portalFileSetupService = portalFileSetupService;
	}
}
