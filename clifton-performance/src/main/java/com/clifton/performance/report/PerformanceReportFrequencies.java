package com.clifton.performance.report;

/**
 * Used by GIPS Performance Composite Performance Reporting and GIPS Performance Composite Account Performance Reporting to determine
 * when the performance needs to move to "Pending Reporting"
 *
 * @author manderson
 */
public enum PerformanceReportFrequencies {

	NONE,
	MONTHLY,
	QUARTERLY
}
