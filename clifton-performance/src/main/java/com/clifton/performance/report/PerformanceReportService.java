package com.clifton.performance.report;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileDuplicateActions;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.report.export.ReportExportConfiguration;

import java.util.Date;


/**
 * The <code>PerformanceReportService</code> is used for working with reports and various performance objects
 *
 * @author manderson
 */
public interface PerformanceReportService {


	////////////////////////////////////////////////////////////////////////////////
	//////   Performance Investment Account Query Export Business Methods   ////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns system query result for specified query name associated with PerformanceInvestmentAccount table
	 * These are queries added via UI as shortcuts and require the query itself to take only one parameter which is the performanceInvestmentAccountId
	 */
	@SecureMethod(dtoClass = PerformanceInvestmentAccount.class)
	public DataTable getPerformanceInvestmentAccountSystemQueryResult(String queryName, int performanceAccountId);


	////////////////////////////////////////////////////////////////////////////////
	////////     Performance Investment Account Report Business Methods    /////////
	////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = PerformanceInvestmentAccount.class)
	public FileWrapper downloadPerformanceInvestmentAccountReport(int performanceAccountId, ReportExportConfiguration exportConfiguration);


	/**
	 * Currently only used for Performance summary reports for VRP (Security Target) services.  Allows running the report "as of" a specific date
	 * so that we can report on partial period performance
	 */
	@SecureMethod(dtoClass = PerformanceInvestmentAccount.class)
	public FileWrapper downloadPerformanceInvestmentAccountReportWithDate(int performanceAccountId, Date reportingDate, ReportExportConfiguration exportConfiguration);


	/**
	 * Returns the report id to use for the selected performance account record.
	 * Checks account level custom field first, if none, will check service level custom field
	 * Note: If account level uses a global default, then the service level will NOT be checked.
	 */
	@DoNotAddRequestMapping
	public Integer getPerformanceInvestmentAccountReportId(PerformanceInvestmentAccount performanceInvestmentAccount);


	/**
	 * Returns the default frequency the performance is posted.
	 */
	@DoNotAddRequestMapping
	public PerformanceReportFrequencies getPerformanceInvestmentAccountReportFrequency(PerformanceInvestmentAccount performanceInvestmentAccount);

	////////////////////////////////////////////////////////////////////////////////
	/////////           Performance Composite Performance Reports          /////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a report for the performance item
	 */
	@SecureMethod(dtoClass = PerformanceCompositePerformance.class)
	public FileWrapper downloadPerformanceCompositePerformanceReport(int performanceCompositePerformanceId, ReportExportConfiguration exportConfiguration);


	/**
	 * Returns a concatenated report for all of the selected performance items
	 */
	@SecureMethod(dtoClass = PerformanceCompositePerformance.class)
	public FileWrapper downloadPerformanceCompositePerformanceReportList(Integer[] performanceCompositePerformanceIds, ReportExportConfiguration exportConfiguration);


	////////////////////////////////////////////////////////////////////////////////
	///////        Performance Composite Performance Posting Reports         ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Post the GIPS Composite Report to the portal.  Called when there is an existing version of the report on the portal and replacement is available
	 */
	@SecureMethod(dtoClass = PerformanceCompositePerformance.class, permissions = SecurityPermission.PERMISSION_READ)
	public void postPerformanceCompositePerformanceReportReplace(int performanceCompositePerformanceId, ReportExportConfiguration exportConfiguration);


	/**
	 * Post the GIPS Composite Report to the portal.
	 */
	@SecureMethod(dtoClass = PerformanceCompositePerformance.class, permissions = SecurityPermission.PERMISSION_READ)
	public void postPerformanceCompositePerformanceReport(int performanceCompositePerformanceId, ReportExportConfiguration exportConfiguration, FileDuplicateActions fileDuplicateAction);


	////////////////////////////////////////////////////////////////////////////////
	///////   Performance Composite Investment Account Performance Reports   ///////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a report for the supplied performance and concatenates attachments if indicated by isCompositeAccountReportAppendAttachments on the PerformanceCompositeInvestmentAccount
	 */
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class)
	public FileWrapper downloadPerformanceCompositeInvestmentAccountPerformanceReport(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration);


	/**
	 * Returns a concatenated report for all of the selected performance items
	 */
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class)
	public FileWrapper downloadPerformanceCompositeInvestmentAccountPerformanceReportList(Integer[] performanceCompositeInvestmentAccountPerformanceIds, ReportExportConfiguration exportConfiguration);


	/**
	 * Returns a FileWrapper of concatenated attachments for the performance account if indicated by isCompositeAccountReportAppendAttachments on the PerformanceCompositeInvestmentAccount
	 */
	@DoNotAddRequestMapping
	public FileWrapper downloadPerformanceCompositeInvestmentAccountPerformanceReportAttachments(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration);

	////////////////////////////////////////////////////////////////////////////////
	/////  Performance Composite Investment Account Performance Post Reports   /////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Post the GIPS Account Report to the portal.
	 */
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class, permissions = SecurityPermission.PERMISSION_READ)
	public void postPerformanceCompositeInvestmentAccountPerformanceReport(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration);


	/**
	 * Post the GIPS Account Report to the portal.  Called when there is an existing version of the report on the portal and replacement is available
	 */
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class, permissions = SecurityPermission.PERMISSION_READ)
	public void postPerformanceCompositeInvestmentAccountPerformanceReportReplace(int performanceCompositeInvestmentAccountPerformanceId, ReportExportConfiguration exportConfiguration);


	////////////////////////////////////////////////////////////////////////////////
	///  Performance Composite and Account Preliminary Performance Post Reports ////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns pre-populated default options for report posting of composite OR account performance
	 * Note: ONLY allowed to be called for prelim performance - approved performance must use the above methods
	 */
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class)
	public PerformanceReportPostingFileConfiguration getPerformanceReportPostingFileConfigurationPreliminary(Integer performanceCompositePerformanceId, Integer performanceCompositeInvestmentAccountPerformanceId);


	/**
	 * Used to POST the preliminary performance report
	 */
	@SecureMethod(dtoClass = PerformanceCompositeInvestmentAccountPerformance.class, permissions = SecurityPermission.PERMISSION_READ)
	public PerformanceReportPostingFileConfiguration postPerformanceReportPostingFileConfigurationPreliminary(PerformanceReportPostingFileConfiguration postingFileConfiguration);
}
