package com.clifton.performance.report.params;

import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositeInvestmentAccountPerformanceSearchForm;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.report.PerformanceReportService;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceCompositeInvestmentAccountPerformanceReportParameterConfigurer</code> is used to generate a report, or concatenation of reports for selected
 * {@link PerformanceCompositeInvestmentAccountPerformance} objects
 *
 * @author manderson
 */
@Component
public class PerformanceCompositeInvestmentAccountPerformanceReportParameterConfigurer implements ReportParameterConfigurer<PerformanceCompositeInvestmentAccountPerformance> {

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;
	private PerformanceReportService performanceReportService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getParameterName() {
		return "performanceCompositeInvestmentAccountPerformanceIds";
	}


	@Override
	public List<PerformanceCompositeInvestmentAccountPerformance> getReportParameterValueList(ReportConfigParameter configParameter, @SuppressWarnings("unused") Date reportDate) {
		if (configParameter == null || StringUtils.isEmpty(configParameter.getValue())) {
			return null;
		}
		String performanceCompositeInvestmentAccountPerformanceIdString = configParameter.getValue();
		String[] performanceCompositeInvestmentAccountPerformanceIds = performanceCompositeInvestmentAccountPerformanceIdString.split(",");

		if (performanceCompositeInvestmentAccountPerformanceIds.length == 0) {
			return null;
		}

		PerformanceCompositeInvestmentAccountPerformanceSearchForm searchForm = new PerformanceCompositeInvestmentAccountPerformanceSearchForm();
		if (performanceCompositeInvestmentAccountPerformanceIds.length == 1) {
			searchForm.setId(Integer.parseInt(performanceCompositeInvestmentAccountPerformanceIds[0]));
		}
		else {
			Integer[] performanceCompositeInvestmentAccountPerformanceId = new Integer[performanceCompositeInvestmentAccountPerformanceIds.length];
			for (int i = 0; i < performanceCompositeInvestmentAccountPerformanceIds.length; i++) {
				performanceCompositeInvestmentAccountPerformanceId[i] = Integer.parseInt(performanceCompositeInvestmentAccountPerformanceIds[i].trim());
			}
			searchForm.setIds(performanceCompositeInvestmentAccountPerformanceId);
		}
		return getPerformanceCompositePerformanceService().getPerformanceCompositeInvestmentAccountPerformanceList(searchForm);
	}


	@Override
	public void populateReportParameterMap(Map<String, Object> parameterMap, PerformanceCompositeInvestmentAccountPerformance value) {
		PerformanceCompositeInvestmentAccount assignment = value.getPerformanceCompositeInvestmentAccount();
		// Note: THIS CAN BE SIMPLIFIED AS WE ONLY NEED THE PerformanceCompositeAccountPerformanceID
		parameterMap.put("CompositeAssignmentID", assignment.getId());
		parameterMap.put("PerformanceCompositeAccountPerformanceID", value.getId());
		parameterMap.put("CompositeID", assignment.getPerformanceComposite().getId());
		parameterMap.put("AccountingPeriodID", value.getAccountingPeriod().getId());
	}


	@SuppressWarnings("unused")
	@Override
	public void populateReportPostingFileConfiguration(ReportPostingFileConfiguration postConfig, PerformanceCompositeInvestmentAccountPerformance value) {
		// NO POSTING SUPPORTED
		throw new IllegalStateException("Posting Not Supported for Performance Composite Investment Account Performance Reports.");
	}


	@Override
	public Integer getReportIdForValue(ReportExportConfiguration exportConfig, PerformanceCompositeInvestmentAccountPerformance value) {
		if (exportConfig.getReportId() != null) {
			return exportConfig.getReportId();
		}
		if (value.getReport() != null) {
			return value.getReport().getId();
		}
		// Required field at composite level so this should never happen
		throw new ValidationException("Please select a specific report.  Account Performance [" + value.getLabel() + "] does not have a default account report selected at either the assignment or composite level.");
	}


	@Override
	public String getLabelForValue(PerformanceCompositeInvestmentAccountPerformance value) {
		return value.getLabel();
	}


	/**
	 * Format:  [InvestmentAccountName] Performance Report - [yyyy-MM-dd]
	 * Example: "Catholic Health East-Consolidated Master Retirement Trust Performance Report - 2017-09-30"
	 */
	@Override
	public String generateFileNameWithoutExtension(PerformanceCompositeInvestmentAccountPerformance value, Date reportDate) {
		return String.format("%s Performance Report - %s%s",
				value.getClientAccount().getName(),
				DateUtils.fromDateISO(value.getAccountingPeriod().getEndDate()),
				!WorkflowStatus.STATUS_APPROVED.equals(value.getWorkflowStatus().getLabel()) ? " - Preliminary" : StringUtils.EMPTY_STRING);
	}


	@Override
	public FileWrapper getAttachments(PerformanceCompositeInvestmentAccountPerformance value, ReportExportConfiguration configuration) {
		return getPerformanceReportService().downloadPerformanceCompositeInvestmentAccountPerformanceReportAttachments(value.getId(), configuration);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}


	public PerformanceReportService getPerformanceReportService() {
		return this.performanceReportService;
	}


	public void setPerformanceReportService(PerformanceReportService performanceReportService) {
		this.performanceReportService = performanceReportService;
	}
}
