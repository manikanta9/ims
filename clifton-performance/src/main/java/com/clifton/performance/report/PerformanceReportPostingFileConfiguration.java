package com.clifton.performance.report;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.report.definition.Report;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.posting.ReportPostingFileConfiguration;


/**
 * The <code>PerformanceReportPostingFileConfiguration</code> is used to post composite performance and composite account performance to the portal.
 * Used for preliminary reports only for now.  (Approved performance uses pre-defined settings) Preliminary reports the options can be customized for specific use cases.
 * Prelim custom example - Posting the Fund Account 800020 prelim performance to a user selected file group and it posts to the Commingled Vehicle so all investors in the fund get the report
 *
 * @author manderson
 */
@NonPersistentObject
public class PerformanceReportPostingFileConfiguration extends ReportPostingFileConfiguration {

	/**
	 * Either performanceCompositePerformanceId or performanceCompositeAccountPerformanceId can be selected and one MUST be selected
	 */
	private Integer performanceCompositePerformanceId;

	private Integer performanceCompositeInvestmentAccountPerformanceId;

	/**
	 * User friendly label for UI
	 */
	private String label;

	private String reportName;

	private ReportExportConfiguration exportConfiguration;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected void setReport(Report report) {
		if (report != null) {
			if (this.exportConfiguration == null) {
				this.exportConfiguration = new ReportExportConfiguration();
			}
			this.setReportName(report.getName());
			this.exportConfiguration.setReportId(report.getId());
		}
	}


	protected void setPerformanceCompositePerformance(PerformanceCompositePerformance performanceCompositePerformance) {
		if (performanceCompositePerformance != null) {
			setPerformanceCompositePerformanceId(performanceCompositePerformance.getId());
			setLabel(performanceCompositePerformance.getLabel());
		}
	}


	protected void setPerformanceCompositeInvestmentAccountPerformance(PerformanceCompositeInvestmentAccountPerformance accountPerformance) {
		if (accountPerformance != null) {
			setPerformanceCompositeInvestmentAccountPerformanceId(accountPerformance.getId());
			setLabel(accountPerformance.getLabel());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getPerformanceCompositePerformanceId() {
		return this.performanceCompositePerformanceId;
	}


	public void setPerformanceCompositePerformanceId(Integer performanceCompositePerformanceId) {
		this.performanceCompositePerformanceId = performanceCompositePerformanceId;
	}


	public Integer getPerformanceCompositeInvestmentAccountPerformanceId() {
		return this.performanceCompositeInvestmentAccountPerformanceId;
	}


	public void setPerformanceCompositeInvestmentAccountPerformanceId(Integer performanceCompositeInvestmentAccountPerformanceId) {
		this.performanceCompositeInvestmentAccountPerformanceId = performanceCompositeInvestmentAccountPerformanceId;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public ReportExportConfiguration getExportConfiguration() {
		return this.exportConfiguration;
	}


	public void setExportConfiguration(ReportExportConfiguration exportConfiguration) {
		this.exportConfiguration = exportConfiguration;
	}


	public String getReportName() {
		return this.reportName;
	}


	public void setReportName(String reportName) {
		this.reportName = reportName;
	}
}
