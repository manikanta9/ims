package com.clifton.performance.report.params;

import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.composite.performance.PerformanceCompositePerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.search.PerformanceCompositePerformanceSearchForm;
import com.clifton.report.export.ReportExportConfiguration;
import com.clifton.report.params.ReportConfigParameter;
import com.clifton.report.params.ReportParameterConfigurer;
import com.clifton.report.posting.ReportPostingFileConfiguration;
import com.clifton.workflow.definition.WorkflowStatus;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>PerformanceCompositePerformanceReportParameterConfigurer</code> is used to generate a report, or concatenation of reports for selected
 * {@link PerformanceCompositePerformance} objects
 *
 * @author manderson
 */
@Component
public class PerformanceCompositePerformanceReportParameterConfigurer implements ReportParameterConfigurer<PerformanceCompositePerformance> {

	private PerformanceCompositePerformanceService performanceCompositePerformanceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getParameterName() {
		return "performanceCompositePerformanceIds";
	}


	@Override
	public List<PerformanceCompositePerformance> getReportParameterValueList(ReportConfigParameter configParameter, @SuppressWarnings("unused") Date reportDate) {
		if (configParameter == null || StringUtils.isEmpty(configParameter.getValue())) {
			return null;
		}
		String performanceCompositePerformanceIdString = configParameter.getValue();
		String[] performanceCompositePerformanceIds = performanceCompositePerformanceIdString.split(",");

		if (performanceCompositePerformanceIds.length == 0) {
			return null;
		}

		PerformanceCompositePerformanceSearchForm searchForm = new PerformanceCompositePerformanceSearchForm();
		if (performanceCompositePerformanceIds.length == 1) {
			searchForm.setId(Integer.parseInt(performanceCompositePerformanceIds[0]));
		}
		else {
			Integer[] performanceCompositePerformanceId = new Integer[performanceCompositePerformanceIds.length];
			for (int i = 0; i < performanceCompositePerformanceIds.length; i++) {
				performanceCompositePerformanceId[i] = Integer.parseInt(performanceCompositePerformanceIds[i].trim());
			}
			searchForm.setIds(performanceCompositePerformanceId);
		}
		return getPerformanceCompositePerformanceService().getPerformanceCompositePerformanceList(searchForm);
	}


	@Override
	public void populateReportParameterMap(Map<String, Object> parameterMap, PerformanceCompositePerformance value) {
		// Note: THIS CAN BE SIMPLIFIED AS WE ONLY NEED THE PerformanceCompositePerformanceID
		parameterMap.put("CompositeID", value.getPerformanceComposite().getId());
		parameterMap.put("AccountingPeriodID", value.getAccountingPeriod().getId());
	}


	@SuppressWarnings("unused")
	@Override
	public void populateReportPostingFileConfiguration(ReportPostingFileConfiguration postConfig, PerformanceCompositePerformance value) {
		// NO POSTING SUPPORTED
		throw new IllegalStateException("Posting Not Supported for Performance Composite Performance Reports.");
	}


	@Override
	public Integer getReportIdForValue(@SuppressWarnings("unused") ReportExportConfiguration exportConfig, PerformanceCompositePerformance value) {
		if (exportConfig.getReportId() != null) {
			return exportConfig.getReportId();
		}
		if (value.getPerformanceComposite() != null && value.getPerformanceComposite().getCompositeReport() != null) {
			return value.getPerformanceComposite().getCompositeReport().getId();
		}
		// Required field so this should never happen
		throw new ValidationException("Please select a specific report.  Composite Performance [" + value.getLabel() + "] does not have a default composite report selected.");
	}


	@Override
	public String getLabelForValue(PerformanceCompositePerformance value) {
		return value.getLabel();
	}


	// Format:  [PerformanceCompositeName] Performance Report - [yyyy-MM-dd]
	// Example: "Commodity Composite II Performance Report - 2017-09-30"
	@Override
	public String generateFileNameWithoutExtension(PerformanceCompositePerformance value, Date reportDate) {
		String shortName = value.getPerformanceComposite().getCompositeShortName();
		return String.format("%s Performance Report - %s%s",
				!StringUtils.isEmpty(shortName) ? shortName : value.getPerformanceComposite().getName(),
				DateUtils.fromDateISO(value.getAccountingPeriod().getEndDate()),
				!WorkflowStatus.STATUS_APPROVED.equals(value.getWorkflowStatus().getLabel()) ? " - Preliminary" : StringUtils.EMPTY_STRING);
	}


	@Override
	public FileWrapper getAttachments(PerformanceCompositePerformance value, ReportExportConfiguration configuration) {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////           Getter & Setter Methods                //////////////
	////////////////////////////////////////////////////////////////////////////


	public PerformanceCompositePerformanceService getPerformanceCompositePerformanceService() {
		return this.performanceCompositePerformanceService;
	}


	public void setPerformanceCompositePerformanceService(PerformanceCompositePerformanceService performanceCompositePerformanceService) {
		this.performanceCompositePerformanceService = performanceCompositePerformanceService;
	}
}
