Clifton.performance.account.securitytarget.PerformanceAccountSecurityTargetWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Client Account Performance',
	iconCls: 'chart-bar',
	width: 1000,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		items: [{
			title: 'Period Info',
			tbar: {
				xtype: 'workflow-toolbar',
				tableName: 'PerformanceInvestmentAccount',
				finalState: 'Posted (Archive)',
				reloadFormPanelAfterTransition: true
			},
			items: [{
				xtype: 'formpanel',
				url: 'performanceInvestmentAccount.json',
				labelWidth: 175,
				getWarningMessage: function(form) {
					return Clifton.rule.violation.getRuleViolationWarningMessage('performance', form.formValues);
				},
				loadDefaultDataAfterRender: true,

				reload: function() {
					const w = this.getWindow();
					this.getForm().setValues(TCG.data.getData('performanceInvestmentAccount.json?id=' + w.getMainFormId(), this), true);
					this.fireEvent('afterload', this);
				},

				executeProcessingAction: function(url, params) {
					const f = this;
					const w = this.getWindow();
					if (w.isModified() || !w.isMainFormSaved()) {
						TCG.showError('Please save your changes before processing.');
						return false;
					}

					if (TCG.isNull(params)) {
						params = {performanceAccountId: w.getMainFormId()};
					}
					const loader = new TCG.data.JsonLoader({
						waitTarget: this,
						timeout: 40000,
						waitMsg: 'Processing...',
						params: params,
						conf: params,
						onLoad: function(record, conf) {
							f.reload();
						},
						onFailure: function() {
							// Even if the Processing Fails, still need to reload so that values are properly updated, and warnings adjusted
							f.reload();
						}

					});
					loader.load(url);
				},

				executeReport: function(format) {
					const w = this.getWindow();
					const id = w.getMainFormId();
					// Client Name + "Performance" + Year + Month Name
					const reportFileName = this.getFormValue('clientAccount.businessClient.name') + ' Performance ' + TCG.parseDate(this.getFormValue('accountingPeriod.endDate')).format('Y F');
					const params = {
						'performanceAccountId': id,
						'exportFormat': format,
						'reportFileName': reportFileName
					};
					TCG.downloadFile('performanceInvestmentAccountReportDownload.json', params, this);
				},

				items: [
					{fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', detailPageClass: 'Clifton.investment.account.AccountWindow', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
					{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', hiddenName: 'accountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label'},
					{name: 'accountingPeriod.startDate', xtype: 'datefield', hidden: true, submitValue: false},
					{name: 'accountingPeriod.endDate', xtype: 'datefield', hidden: true, submitValue: false},
					{name: 'postedToPortal', boxLabel: 'Posted to Portal', xtype: 'checkbox', doNotSubmitValue: true, disabled: true, qtip: 'If checked, there is at least one approved file on the Portal associated with this performance summary.  Click Portal Files button to view more information.'},
					{xtype: 'label', html: '&nbsp;'},

					{
						fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
						defaults: {
							xtype: 'displayfield',
							style: 'text-align: right',
							flex: 1
						},
						items: [
							{value: 'Security Target Return'},
							{xtype: 'label', html: '&nbsp;', flex: 0.2},
							{value: 'Portfolio Return (Gross)'},
							{value: 'Portfolio Return (Net)'}
						]
					},
					{
						fieldLabel: 'MTD Return', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							decimalPrecision: 4,
							readOnly: true,
							submitValue: false,
							flex: 1
						},
						items: [
							{name: 'monthToDateBenchmarkReturn'},
							{xtype: 'label', html: '&nbsp;', flex: 0.2},
							{name: 'monthToDateReturn'},
							{name: 'monthToDatePortfolioReturn'}
						]
					},
					{
						fieldLabel: 'QTD Return', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							decimalPrecision: 4,
							readOnly: true,
							submitValue: false,
							flex: 1
						},
						items: [
							{name: 'quarterToDateBenchmarkReturn'},
							{xtype: 'label', html: '&nbsp;', flex: 0.2},
							{name: 'quarterToDateReturn'},
							{name: 'quarterToDatePortfolioReturn'}
						]
					},

					{
						fieldLabel: 'YTD Return', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							decimalPrecision: 4,
							readOnly: true,
							submitValue: false,
							flex: 1
						},
						items: [
							{name: 'yearToDateBenchmarkReturn'},
							{xtype: 'label', html: '&nbsp;', flex: 0.2},
							{name: 'yearToDateReturn'},
							{name: 'yearToDatePortfolioReturn'}
						]
					},
					{
						fieldLabel: 'ITD Return (Cumulative)', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							decimalPrecision: 4,
							readOnly: true,
							submitValue: false,
							flex: 1
						},
						items: [
							{name: 'inceptionToDateBenchmarkReturnCumulative'},
							{xtype: 'label', html: '&nbsp;', flex: 0.2},
							{name: 'inceptionToDateReturnCumulative'},
							{name: 'inceptionToDatePortfolioReturnCumulative'}
						]
					},
					{
						fieldLabel: 'ITD Return (Annualized)', xtype: 'compositefield',
						defaults: {
							xtype: 'currencyfield',
							decimalPrecision: 4,
							readOnly: true,
							submitValue: false,
							flex: 1
						},
						items: [
							{name: 'inceptionToDateBenchmarkReturn'},
							{xtype: 'label', html: '&nbsp;', flex: 0.2},
							{name: 'inceptionToDateReturn'},
							{name: 'inceptionToDatePortfolioReturn'}
						]
					}

				],
				enableDisableButton: function(btn, enableWorkflowStatusOnly) {

					let enable = true; // By Default After Load Button Can be enabled
					if (enableWorkflowStatusOnly) {
						const dt = TCG.getValue('workflowStatus.name', this.getForm().formValues);
						if (dt !== enableWorkflowStatusOnly) {
							enable = false;
						}
					}
					if (TCG.isTrue(enable)) {
						btn.enable();
					}
					else {
						btn.disable();
					}
				},
				buttons: [
					{
						text: 'Rebuild Snapshots',
						xtype: 'button',
						iconCls: 'run',
						width: 120,
						disabled: true,
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							const startSnapshotDate = TCG.getChildByName(fp, 'accountingPeriod.startDate').getValue().format('m/d/Y');
							const endSnapshotDate = TCG.getChildByName(fp, 'accountingPeriod.endDate').getValue().format('m/d/Y');
							const params = {
								'clientAccountId': fp.getForm().formValues.clientAccount.id,
								'startSnapshotDate': startSnapshotDate,
								'endSnapshotDate': endSnapshotDate,
								'synchronous': true,
								'doNotThrowExceptionForFutureSnapshots': true
							};
							fp.executeProcessingAction('accountingPositionDailyRebuild.json?UI_SOURCE=PerformanceAccountSecurityTarget', params);
							fp.executeProcessingAction('dwAccountingSnapshotsRebuild.json?UI_SOURCE=PerformanceAccountSecurityTarget', params);
						},
						listeners: {
							afterRender: function() {
								const btn = this;
								const fp = TCG.getParentFormPanel(this);
								fp.on('afterload', function() {
									fp.enableDisableButton(btn, 'Draft');
								}, this);
							}
						}
					},
					{
						text: 'Process Returns',
						xtype: 'button',
						iconCls: 'run',
						disabled: true,
						width: 120,
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							f.executeProcessingAction('performanceInvestmentAccountProcess.json');
						},
						listeners: {
							afterRender: function() {
								const btn = this;
								const fp = TCG.getParentFormPanel(this);
								fp.on('afterload', function() {
									fp.enableDisableButton(btn, 'Draft');
								}, this);
							}
						}
					},

					{
						text: 'Process Violations',
						xtype: 'button',
						iconCls: 'warning',
						width: 120,
						disabled: true,
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							f.executeProcessingAction('performanceInvestmentAccountWarningsProcess.json');
						},
						listeners: {
							afterRender: function() {
								const btn = this;
								const fp = TCG.getParentFormPanel(this);
								fp.on('afterload', function() {
									fp.enableDisableButton(btn, 'Draft');
								}, this);
							}
						}
					},

					{
						text: 'Performance Report',
						xtype: 'splitbutton',
						iconCls: 'pdf',
						disabled: true,
						width: 120,
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							f.executeReport('PDF');
						},
						listeners: {
							afterRender: function() {
								const btn = this;
								const fp = TCG.getParentFormPanel(this);
								fp.on('afterload', function() {
									fp.enableDisableButton(btn);
								}, this);
							}
						},
						menu: {
							items: [{
								text: 'Excel',
								iconCls: 'excel_open_xml',
								tooltip: 'Generate the excel version of the report.',
								handler: function() {
									const f = TCG.getParentFormPanel(this);
									f.executeReport('EXCEL_OPEN_XML');
								}
							}, {
								text: 'Excel (97-2003)',
								iconCls: 'excel',
								tooltip: 'Generate the excel version of the report.',
								handler: function() {
									const f = TCG.getParentFormPanel(this);
									f.executeReport('EXCEL');
								}
							}]
						}
					},
					{
						text: 'Portal Files',
						xtype: 'button',
						iconCls: 'www',
						tooltip: 'View Portal Files associated with this performance summary.',
						width: 120,
						handler: function() {
							const f = TCG.getParentFormPanel(this);
							const className = 'Clifton.portal.file.FileListForSourceEntityWindow';
							const id = f.getFormValue('id');
							const cmpId = TCG.getComponentId(className, id);
							TCG.createComponent(className, {
								id: cmpId,
								sourceLabel: f.getFormValue('label'),
								sourceTableName: 'PerformanceInvestmentAccount',
								sourceFkFieldId: id
								// Looks like we only post P-Sums through workflow so no post url
							});
						}
					}
				]
			}]
		},

			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'PerformanceInvestmentAccount'

				}]
			},


			{
				title: 'Daily Returns',
				items: [{
					name: 'performanceInvestmentAccountSecurityTargetDailyReturnListFind',
					xtype: 'gridpanel',
					instructions: 'Monthly performance is calculated by summing daily performance of security targets.',
					appendStandardColumns: false,
					getLoadParams: function() {
						return {
							performanceInvestmentAccountId: this.getWindow().getMainFormId()
						};
					},
					isPagingEnabled: function() {
						return false;
					},
					pageSize: 50,
					getTopToolbarFilters: function(toolBar) {
						const gridPanel = this;
						const menu = new Ext.menu.Menu();
						menu.add({
							text: 'Performance Summary Setup',
							handler: function() {
								gridPanel.openCustomFieldWindow('Performance Summary Fields');
							}
						});

						toolBar.add({
							text: 'Setup',
							tooltip: 'Account Setup',
							iconCls: 'account',
							menu: menu
						});
						toolBar.add('-');

						toolBar.add(
								{
									text: 'Partial Period Performance Report',
									tooltip: 'Run Performance Report as of selected date.  This allows for partial period performance.  Please ensure performance summary has returns processed up until the selected date.',
									xtype: 'splitbutton',
									iconCls: 'pdf',
									width: 120,
									handler: function() {
										gridPanel.executeReport('PDF');
									},
									menu: {
										items: [{
											text: 'Excel',
											iconCls: 'excel_open_xml',
											tooltip: 'Generate the excel version of the report.',
											handler: function() {
												gridPanel.executeReport('EXCEL_OPEN_XML');
											}
										}, {
											text: 'Excel (97-2003)',
											iconCls: 'excel',
											tooltip: 'Generate the excel version of the report.',
											handler: function() {
												gridPanel.executeReport('EXCEL');
											}
										}]
									}
								});
						toolBar.add('-');
					},
					openCustomFieldWindow: function(groupName) {
						const accountId = this.getWindow().getMainForm().formValues.clientAccount.id;
						Clifton.investment.account.openCliftonAccountCustomFieldWindow(groupName, accountId);
					},
					executeReport: function(format) {
						const gridPanel = this;
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select one row to download partial period performance report for.', 'No Row(s) Selected');
							return;
						}
						else if (sm.getCount() > 1) {
							TCG.showError('Please select only one row.', 'Multiple Rows Selected');
							return;
						}
						const reportDate = sm.getSelected().json.measureDate;

						const w = this.getWindow();
						const id = w.getMainFormId();
						// Client Name + "Performance" + Year + Month Name
						const reportFileName = w.getMainFormPanel().getFormValue('clientAccount.businessClient.name') + ' Partial Period Performance ' + TCG.parseDate(reportDate).format('Y F d');
						const params = {
							'performanceAccountId': id,
							'reportingDate': TCG.parseDate(reportDate).format('m/d/Y'),
							'exportFormat': format,
							'reportFileName': reportFileName
						};
						TCG.downloadFile('performanceInvestmentAccountReportWithDateDownload.json', params, gridPanel);
					},
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Measure Date', width: 75, dataIndex: 'measureDate', defaultSortColumn: true},

						{header: 'Target Value', width: 90, dataIndex: 'securityTargetMarketValue', type: 'currency', useNull: true},
						{header: 'Target Gain Loss', width: 90, dataIndex: 'securityTargetGainLoss', type: 'currency', useNull: true, summaryType: 'sum', negativeInRed: true},
						{header: 'Target Return', width: 90, dataIndex: 'securityTargetReturn', useNull: true, type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},

						{header: 'Options Market Value', width: 90, dataIndex: 'optionsMarketValue', type: 'currency', useNull: true, hidden: true},
						{header: 'Options Gain Loss', width: 90, dataIndex: 'optionsGainLoss', type: 'currency', useNull: true, summaryType: 'sum', negativeInRed: true},
						{header: 'Options Return', width: 90, dataIndex: 'optionsGrossReturn', useNull: true, type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},

						{header: 'Management Fee', width: 90, dataIndex: 'managementFee', type: 'currency', useNull: true, hidden: true},

						{header: 'Portfolio Return (Gross)', width: 90, dataIndex: 'portfolioGrossReturn', useNull: true, type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
						{header: 'Portfolio Return (Net)', width: 90, dataIndex: 'portfolioNetReturn', useNull: true, type: 'percent', numberFormat: '0,000.0000', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'}
					],
					plugins: {ptype: 'gridsummary'},

					editor: {
						detailPageClass: 'Clifton.performance.account.securitytarget.PerformanceAccountSecurityTargetDailyReturnWindow',
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'Service Objectives',
				items: [{
					name: 'investmentAccountServiceObjectiveListFind',
					xtype: 'gridpanel',
					instructions: 'The following Service Objectives have been identified for selected client account.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Objective Name', width: 150, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'objectiveName'}},
						{
							header: 'Description', width: 300, dataIndex: 'objectiveDescription', filter: {searchFieldName: 'coalesceObjectiveDescription'},
							renderer: function(v, metaData, r) {
								if (TCG.isTrue(r.json.objectiveDescriptionOverridden)) {
									metaData.css = 'amountAdjusted';
								}
								return v;
							}
						},
						{header: 'Overridden', width: 50, dataIndex: 'objectiveDescriptionOverridden', type: 'boolean'}
					],
					getLoadParams: function() {
						return {investmentAccountId: TCG.getValue('clientAccount.id', this.getWindow().getMainForm().formValues)};
					},
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountServiceObjectiveWindow',
						deleteURL: 'investmentAccountServiceObjectiveDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								referenceOne: TCG.getValue('clientAccount', gridPanel.getWindow().getMainForm().formValues)
							};
						}
					}
				}]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'PerformanceInvestmentAccount',
					showOrderInfo: true,

					getEntityId: function() {
						return this.getWindow().getMainFormId();
					}
				}]
			},


			{
				title: 'Portal Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-grid-for-source-entity',
						sourceTableName: 'PerformanceInvestmentAccount'
					}
				]
			}
		]
	}]
});
