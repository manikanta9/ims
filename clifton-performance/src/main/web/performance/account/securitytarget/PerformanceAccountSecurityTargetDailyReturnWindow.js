Clifton.performance.account.securitytarget.PerformanceAccountSecurityTargetDailyReturnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Security Target Daily Return',
	iconCls: 'chart-bar',
	height: 500,
	width: 900,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Daily Return',
				items: [{
					xtype: 'formpanel',
					url: 'performanceInvestmentAccountSecurityTargetDailyReturn.json',
					readOnly: true,
					labelWidth: 150,
					items: [
						{fieldLabel: 'Measure Date', name: 'measureDate', xtype: 'datefield'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Target Value', name: 'securityTargetMarketValue', xtype: 'currencyfield'},
										{fieldLabel: 'Target Gain Loss', name: 'securityTargetGainLoss', xtype: 'currencyfield'},
										{fieldLabel: 'Target Return', name: 'securityTargetReturn', xtype: 'currencyfield', decimalPrecision: 4}
									]
								}, {
									rows: [
										{fieldLabel: 'Options Market Value', name: 'optionsMarketValue', xtype: 'currencyfield'},
										{fieldLabel: 'Options Gain Loss', name: 'optionsGainLoss', xtype: 'currencyfield'},
										{fieldLabel: 'Options Return', name: 'optionsReturn', xtype: 'currencyfield', decimalPrecision: 4}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr />'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{xtype: 'label', html: '&nbsp;'},
										{fieldLabel: 'Management Fee', name: 'managementFee', xtype: 'currencyfield'}
									]
								}, {
									rows: [
										{fieldLabel: 'Portfolio Return (Gross)', name: 'portfolioGrossReturn', xtype: 'currencyfield', decimalPrecision: 4},
										{fieldLabel: 'Portfolio Return (Net)', name: 'portfolioNetReturn', xtype: 'currencyfield', decimalPrecision: 4}
									]
								}
							]
						}
					]
				}]

			},


			{
				title: 'Client Positions',
				xtype: 'accounting-positionAnalysisGrid',
				pageSize: 1000,
				reloadOnRender: true,
				viewNames: undefined,
				columnOverrides: [
					{dataIndex: 'accountingTransaction.clientInvestmentAccount.number', hidden: true},
					{dataIndex: 'accountingTransaction.clientInvestmentAccount.name', hidden: true},
					{dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', hidden: true},
					{dataIndex: 'dailyGainLossBase', hidden: false, width: 140}
				],

				getLoadParams: function(firstLoad) {
					const params = {};
					if (firstLoad) {
						const measureDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('measureDate'));
						if (measureDate) {
							this.setFilterValue('positionDate', {'on': measureDate});
						}
						else {
							return false;
						}
					}

					const toolbar = this.getTopToolbar();
					const instrumentGroupId = TCG.getChildByName(toolbar, 'investmentGroupId').getValue();
					if (TCG.isNotBlank(instrumentGroupId)) {
						params.investmentGroupId = instrumentGroupId;
					}
					params.clientInvestmentAccountId = this.getWindow().getMainFormPanel().getFormValue('performanceInvestmentAccount.clientAccount.id');
					return params;
				},
				getTopToolbarFilters: function(toolbar) {
					return [{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'}];
				}
			}
		]
	}]
});
