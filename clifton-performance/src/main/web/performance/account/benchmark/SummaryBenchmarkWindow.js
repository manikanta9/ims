Clifton.performance.account.benchmark.SummaryBenchmarkWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Summary Benchmark',
	iconCls: 'coins',
	width: 700,

	items: [{
		xtype: 'formpanel',
		url: 'performanceSummaryBenchmark.json',
		labelWidth: 180,
		items: [
			{fieldLabel: 'Summary', name: 'referenceOne.label', xtype: 'linkfield', detailIdField: 'referenceOne.id', detailPageClass: 'Clifton.performance.account.PerformanceAccountWindow'},
			{fieldLabel: 'Benchmark', name: 'referenceTwo.name', xtype: 'linkfield', detailIdField: 'referenceTwo.id', detailPageClass: 'Clifton.performance.account.benchmark.BenchmarkWindow'},
			{fieldLabel: 'MTD Return', xtype: 'floatfield', name: 'monthToDateReturn', readOnly: true},
			{fieldLabel: 'MTD Return Override', xtype: 'floatfield', name: 'monthToDateReturnOverride'},
			{fieldLabel: 'QTD Return', xtype: 'floatfield', name: 'quarterToDateReturn', readOnly: true},
			{fieldLabel: 'YTD Return', xtype: 'floatfield', name: 'yearToDateReturn', readOnly: true},
			{fieldLabel: 'ITD Return', xtype: 'floatfield', name: 'inceptionToDateReturn', readOnly: true}
		]
	}]
});
