Clifton.performance.account.benchmark.BenchmarkWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Benchmark',
	iconCls: 'coins',

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,

		items: [{
			title: 'Benchmark Setup',
			items: [{
				xtype: 'formpanel',
				url: 'performanceBenchmark.json',
				labelWidth: 120,

				items: [
					{fieldLabel: 'Client Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', detailPageClass: 'Clifton.investment.account.AccountWindow', disableAddNewItem: true, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label'},
					{fieldLabel: 'Name', name: 'name'},
					{fieldLabel: 'Order', name: 'benchmarkOrder', xtype: 'spinnerfield'},
					{fieldLabel: 'Benchmark Security', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow', mutuallyExclusiveFields: ['benchmarkInterestRateIndex.id']},
					{fieldLabel: 'Benchmark Rate', name: 'benchmarkInterestRateIndex.label', hiddenName: 'benchmarkInterestRateIndex.id', xtype: 'combo', url: 'investmentInterestRateIndexListFind.json?active=true', displayField: 'label', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow', mutuallyExclusiveFields: ['benchmarkSecurity.id']},
					{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
					{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
					{
						boxLabel: 'Hide Historical Returns if Start Date after Period Start', name: 'hideReturnIfStartAfterPeriod', xtype: 'checkbox', requiredFields: ['startDate'],
						qtip: 'If checked, displays <i>N/A</i> for benchmark returns on the report if the benchmark start date is after the period start (QTD, YTD, ITD).<br /><br /><b>Note:</b>&nbsp;Processing and calculations are still evaluated.  This option is used for reports only.'
					}
				]
			}]

		},


			{

				title: 'Historical Returns',
				items: [{
					name: 'performanceSummaryBenchmarkListFind',
					xtype: 'gridpanel',
					instructions: 'The following are the historical returns for the selected benchmark.  The load button allows you to reload history, which is useful when new benchmarks are setup but full history is needed.',
					getLoadParams: function() {
						return {benchmarkId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'Summary Date', width: 100, dataIndex: 'referenceOne.measureDate', filter: {searchFieldName: 'summaryMeasureDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{
							header: 'MTD (Calculated)', dataIndex: 'monthToDateReturn', type: 'currency', hidden: true, useNull: true, numberFormat: '0,000.0000 %',
							renderer: function(v, p, r) {
								return TCG.renderAmount(v, true, '0,000.0000 %');
							}
						},
						{
							header: 'MTD (Override)', dataIndex: 'monthToDateReturnOverride', type: 'currency', hidden: true, useNull: true, numberFormat: '0,000.0000 %',
							renderer: function(v, p, r) {
								return TCG.renderAmount(v, true, '0,000.0000 %');
							}
						},
						{
							header: 'MTD', width: 100, dataIndex: 'coalesceMonthToDateReturnOverride', type: 'currency', useNull: true, numberFormat: '0,000.0000 %',
							renderer: function(v, p, r) {
								return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['monthToDateReturnOverride']), r.data['monthToDateReturn'], '0,000.0000 %', 'Adjustment', true);
							}
						},
						{
							header: 'QTD', dataIndex: 'quarterToDateReturn', type: 'currency', useNull: true, numberFormat: '0,000.0000 %',
							renderer: function(v, p, r) {
								return TCG.renderAmount(v, true, '0,000.0000 %');
							}
						},
						{
							header: 'YTD', dataIndex: 'yearToDateReturn', type: 'currency', useNull: true, numberFormat: '0,000.0000 %',
							renderer: function(v, p, r) {
								return TCG.renderAmount(v, true, '0,000.0000 %');
							}
						},
						{
							header: 'ITD', dataIndex: 'inceptionToDateReturn', type: 'currency', useNull: true, numberFormat: '0,000.0000 %',
							renderer: function(v, p, r) {
								return TCG.renderAmount(v, true, '0,000.0000 %');
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.performance.account.benchmark.SummaryBenchmarkWindow',
						drillDownOnly: true,
						addEditButtons: function(t) {
							const grid = this.grid;
							const editor = this;

							t.add({
								text: 'Load',
								tooltip: 'Load all missing benchmark return history for summaries tied to the client account.',
								iconCls: 'run',
								scope: this,
								handler: function() {
									editor.loadHistory(false);
								}
							});
							t.add('-');

							t.add({
								text: 'Recalculate From Selected',
								tooltip: 'Recalculates selected return as well as all historical returns following selected return.',
								iconCls: 'run',
								scope: this,
								handler: function() {
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to recalculate from  to be executed.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection is not supported.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										const reloadFromDate = sm.getSelected().get('referenceOne.measureDate');
										Ext.Msg.confirm('Recalculate History', 'Are you sure you would like to recalculate history for this benchmark since ' + reloadFromDate.format('m/d/Y') + '?  Note: Recalculated rows will not clear any MTD override values entered.', function(a) {
											if (a === 'yes') {
												editor.loadHistory(true, reloadFromDate);
											}
										});
									}
								}
							});
							t.add('-');
						},

						loadHistory: function(reload, reloadFromDate) {
							const grid = this.getGridPanel();
							const params = grid.getLoadParams();
							params.reload = reload;
							if (reloadFromDate) {
								params.reloadFromDate = reloadFromDate.format('m/d/Y');
							}

							const loader = new TCG.data.JsonLoader({
								waitTarget: grid,
								timeout: 40000,
								params: params,
								scope: grid,
								conf: params,
								onLoad: function(record, conf) {
									grid.reload();
								}
							});
							loader.load('performanceSummaryBenchmarkListForBenchmarkProcess.json');
						}
					}
				}]
			}]
	}]
});
