// Window Selector
Clifton.performance.account.PerformanceAccountWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'performanceInvestmentAccount.json',

	getClassName: function(config, entity) {
		let processingType = undefined;
		if (entity) {
			processingType = TCG.getValue('clientAccount.serviceProcessingType.processingType', entity);
		}
		else if (config && config.defaultData && config.defaultData.clientAccount) {
			const act = TCG.data.getData('investmentAccount.json?id=' + config.defaultData.clientAccount.id, this);
			processingType = TCG.getValue('serviceProcessingType.processingType', act);
		}
		let className = undefined;
		if (processingType) {
			className = Clifton.performance.account.PerformanceAccountWindowOverrides[processingType];
		}
		if (!className) {
			className = Clifton.performance.account.PerformanceAccountWindowOverrides['DEFAULT'];
		}
		return className;
	}
});
